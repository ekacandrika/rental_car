<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'first_name' => 'Admin',
                'no_tlp' => 123456789,
                'lisensi' => null,
                'jk' => 'Laki-Laki',
                'email_verified_at' => '2022-12-09 19:35:58',
                'role' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('password'),
                'profile_photo_path' => null
            ],
            [
                'first_name' => 'User',
                'no_tlp' => 12345678910,
                'lisensi' => null,
                'jk' => 'Laki-Laki',
                'email_verified_at' => '2022-12-09 19:35:58',
                'role' => 'user',
                'email' => 'user@gmail.com',
                'password' => bcrypt('password'),
                'profile_photo_path' => null
            ],
            [
                'first_name' => 'Seller',
                'no_tlp' => 12345678911,
                'lisensi' => null,
                'jk' => 'Laki-Laki',
                'email_verified_at' => '2022-12-09 19:35:58',
                'role' => 'seller',
                'email' => 'seller@gmail.com',
                'password' => bcrypt('password'),
                'profile_photo_path' => null
            ],
            [
                'first_name' => 'Toko',
                'no_tlp' => 12345678912,
                'lisensi' => null,
                'jk' => 'Laki-Laki',
                'email_verified_at' => '2022-12-09 19:35:58',
                'role' => 'toko',
                'email' => 'toko@gmail.com',
                'password' => bcrypt('password'),
                'profile_photo_path' => null
            ],
            [
                'first_name' => 'Agent',
                'no_tlp' => 12345678913,
                'lisensi' => null,
                'jk' => 'Laki-Laki',
                'email_verified_at' => '2022-12-09 19:35:58',
                'role' => 'agent',
                'email' => 'agent@gmail.com',
                'password' => bcrypt('password'),
                'profile_photo_path' => null
            ],
            [
                'first_name' => 'Corporate',
                'no_tlp' => 12345678914,
                'lisensi' => null,
                'jk' => 'Laki-Laki',
                'email_verified_at' => '2022-12-09 19:35:58',
                'role' => 'corporate',
                'email' => 'corporate@gmail.com',
                'password' => bcrypt('password'),
                'profile_photo_path' => null
            ],
            [
                'first_name' => 'Traveller',
                'no_tlp' => 12345678915,
                'lisensi' => null,
                'jk' => 'Laki-Laki',
                'email_verified_at' => '2022-12-09 19:35:58',
                'role' => 'traveller',
                'email' => 'traveller@gmail.com',
                'password' => bcrypt('password'),
                'profile_photo_path' => null
            ],
            [
                'first_name' => 'Visitor',
                'no_tlp' => 12345678916,
                'lisensi' => null,
                'jk' => 'Laki-Laki',
                'email_verified_at' => '2022-12-09 19:35:58',
                'role' => 'visitor',
                'email' => 'visitor@gmail.com',
                'password' => bcrypt('password'),
                'profile_photo_path' => null
            ],
            [
                'first_name' => 'Defri',
                'no_tlp' => 9879797979,
                'lisensi' => 'Personal Vehicle Rental Seller',
                'jk' => 'Laki-Laki',
                'email_verified_at' => '2022-12-09 19:35:58',
                'role' => 'seller',
                'email' => 'defri.surya@gmail.com',
                'password' => bcrypt('password'),
                'profile_photo_path' => 'FotoProfileSeller/1670695104FotoProfileSellerapi_key.png'
            ],
        ]);
    }
}
