<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KelolaTokoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kelolatokos')->insert([
            [
                'nama_toko' => "Kwanstar",
                'nama_banner' => "kwanstar-banner",
                'tautan_banner' => "https://www.google.com/kwanstar-banner.jpg"
            ],
        ]);
    }
}
