<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DetailUserCorporateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('edit_akun_corporates')->insert([
            [ 
                'nama_depan' => 'Neanthal',
                'nama_belakang' => 'Wahan',
                'alamat_email' => 'admin@web.com',
                'no_telepon' => '08831274889',
                'negara' => 'Indonesia',
                'id_user' => '12345',
                'nama_dagang' => 'Dagang Apa Saja1',
                'nama_perusahaan' => 'PT. Jual Apa Saja',
                'no_telp' => '08578398293',
                'alamat' => 'Jalan Taman Siswa nomor 12',
                'kota' => 'Kota Yogyakarta',
                'kode_pos' => '55151',
                'website' => 'https://wwww.jualinaja.co.id/',
            ],
        ]);
    }
}
