<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TravellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('user_traveller')->insert([
            [
                // 'kecamatan_id' => 1,
                'user_id' => 7,
                'nama_depan' => 'Nama depan',
                'nama_belakang' => 'Nama belakang',
                'jenis_kelamin' => 'laki-laki',
                'tempat_lahir' => 'Yogyakarta',
                'tanggal_lahir' => '2022-12-01',
                // 'nik' => '1234567890123456',
                'no_telp' => json_encode([123456789017]),
                'no_telp_primary' => '123456789017',
                'email' => json_encode(['namadepan@email.com']),
                'email_primary' => 'namadepan@email.com',
                'status_newsletter' => true,
                // 'password' => bcrypt('password'),
                'address' => 'Jalan Taman Siswa nomor 12',
                // 'province' => 'Daerah Istimewa Yogyakarta',
                // 'regency' => 'Kota Yogyakarta',
                // 'district' => 'Mergangsan',
                // 'village' => 'Wirogunan',
                'kode_pos' => '55151',
                'akun_booking_status' => false,
                'status_residen' => 'residen',
                'kewarganegaraan' => 'Indonesia',
            ],
            // [
            //     // 'kecamatan_id' => 13,
            //     'user_id' => 13,
            //     'nama_depan' => 'Bima',
            //     'nama_belakang' => 'Nama',
            //     'jenis_kelamin' => 'laki-laki',
            //     'tempat_lahir' => 'Magelang',
            //     'tanggal_lahir' => '2022-10-19',
            //     'no_telp' => json_encode([1234567890]),
            //     'no_telp_primary' => '1234567890',
            //     'email' => json_encode(['bimanugraha732@gmail.com']),
            //     'email_primary' => 'bimanugraha732@gmail.com',
            //     'status_newsletter' => true,
            //     'alamat' => 'Alamat A',
            //     // 'password' => null,
            //     // 'province' => 'Daerah Istimewa Yogyakarta',
            //     // 'regency' => 'Kota Yogyakarta',
            //     // 'district' => 'Mergangsan',
            //     // 'village' => 'Wirogunan',
            //     'kode_pos' => '1234567',
            //     'akun_booking' => false,
            //     'status_residen' => 'residen',
            //     'kewarganegaraan' => 'indonesia',
            //     // 'foto_ktp' => 'User/FotoKTP/1671085926FotoKTPben-sweet-2LowviVHZ-E-unsplash.jpg',
            //     // 'foto_passport' => 'User/FotoPassport/1671085948FotoPassportgirl-with-red-hat-TQDqIXp2ULU-unsplash.jpg',
            // ],
        ]);
    }
}
