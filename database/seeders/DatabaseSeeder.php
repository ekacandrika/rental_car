<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $this->call([UserSeeder::class]);
        $this->call([IndoRegionProvinceSeeder::class]);
        $this->call([IndoRegionRegencySeeder::class]);
        $this->call([IndoRegionDistrictSeeder::class]);
        $this->call([IndoRegionVillageSeeder::class]);
        // $this->call([TravellerSeeder::class]);
        $this->call([KelolaTokoSeeder::class]);
        $this->call([DetailUserCorporateSeeder::class]);
        $this->call([IslandSeeder::class]);
        $this->call([KategoriSeeder::class]);
    }
}
