<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IslandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pulau')->insert([
            [
                'island_name' => 'Pulau Sumatra',
                'island_group' => 1
            ],
            [
                'island_name' => 'Pulau Jawa',
                'island_group' => 3
            ],
            [
                'island_name' => 'Pulau Kalimantan',
                'island_group' => 6
            ],
            [
                'island_name' => 'Pulau Papua',
                'island_group' => 9
            ],
            [
                'island_name' => 'Pulau Sulawesi',
                'island_group' => 7
            ],
            [
                'island_name' => 'Pulau Maluku',
                'island_group' => 8
            ],
            [
                'island_name' => 'Pulau Bali dan Nusa Tenggara',
                'island_group' => 5
            ],
        ]);
    }
}
