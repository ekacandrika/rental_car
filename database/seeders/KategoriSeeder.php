<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('kategori')->insert([
            [
                'nama_kategori' => 'tour',
                'kode_kategori' => 'TO',
            ],
            [
                'nama_kategori' => 'transfer',
                'kode_kategori' => 'TR',
            ],
            [
                'nama_kategori' => 'rental',
                'kode_kategori' => 'RT',
            ],
            [
                'nama_kategori' => 'activity',
                'kode_kategori' => 'AC',
            ],
            [
                'nama_kategori' => 'hotel',
                'kode_kategori' => 'HT',
            ],
            [
                'nama_kategori' => 'stay',
                'kode_kategori' => 'ST',
            ],
            [
                'nama_kategori' => 'custom arrangement',
                'kode_kategori' => 'CA',
            ],
        ]);
    }
}
