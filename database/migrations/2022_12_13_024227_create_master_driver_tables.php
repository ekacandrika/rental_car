<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_driver', function (Blueprint $table) {
            $table->id('id_driver');
            $table->bigInteger('id_seller')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('nama_driver')->nullable();
            $table->bigInteger('no_telp')->nullable();
            $table->string('alamat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_driver');
    }
};
