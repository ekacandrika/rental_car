<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulir_produk_transfers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('tanggalPergi')->nullable();
            $table->string('jamPergi')->nullable();
            $table->string('transferDari')->nullable();
            $table->string('kotaDari')->nullable();
            $table->string('provinsiDari')->nullable();
            $table->string('embedMapsDari')->nullable();
            $table->string('transferKe')->nullable();
            $table->string('kotaKe')->nullable();
            $table->string('provinsiKe')->nullable();
            $table->string('embedMapsKe')->nullable();
            $table->string('coupon_question')->nullable();
            $table->string('tanggalPulang')->nullable();
            $table->string('jamPulang')->nullable();
            $table->string('transport')->nullable();
            $table->string('dewasa')->nullable();
            $table->string('anak')->nullable();
            $table->string('balita')->nullable();
            $table->string('barangBesar')->nullable();
            $table->string('barangKecil')->nullable();
            $table->string('memo')->nullable();
            $table->string('kamtuu')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulir_produk_transfers');
    }
};
