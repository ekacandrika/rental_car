<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulir_produk_turs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('tanggalMulai')->nullable();
            $table->string('waktuMulai')->nullable();
            $table->string('durasi')->nullable();
            $table->string('lokasi')->nullable();
            $table->string('dewasa')->nullable();
            $table->string('anak')->nullable();
            $table->string('balita')->nullable();
            $table->string('transportasi')->nullable();
            $table->string('jumlahPeserta')->nullable();
            $table->string('makanan')->nullable();
            $table->string('namaObjek')->nullable();
            $table->json('jenisKendaraan')->nullable();
            $table->json('deskripsiCustom')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulir_produk_turs');
    }
};
