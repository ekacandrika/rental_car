<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pulaus', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->enum('objek', ['Objek','Provinsi','Kabupaten']);
            $table->string('slug')->nullable();
            $table->string('nama_pulau')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('foto_menteri')->nullable();
            $table->string('nama_menteri')->nullable();
            $table->string('kata_sambutan')->nullable();
            $table->string('deskripsi')->nullable();
            $table->string('bandara')->nullable();
            $table->string('ibukota')->nullable();
            $table->string('terminal')->nullable();
            $table->string('pelabuhan')->nullable();
            $table->string('transportasi')->nullable();
            $table->string('gallery_pulau')->nullable();
            $table->string('embed_maps')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pulaus');
    }
};
