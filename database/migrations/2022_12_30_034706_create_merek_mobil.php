<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merek_mobil', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('jenis_id')->constrained('jenis_mobil')->onDelete('cascade')->onUpdate('cascade');
            $table->string('merek_mobil')->nullable();
            $table->string('plat_mobil')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merek_mobil');
    }
};
