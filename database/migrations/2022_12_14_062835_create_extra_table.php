<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra', function (Blueprint $table) {
            $table->id();
            $table->string('nama_ekstra');
            $table->text('detail_ekstra');
            $table->text('note_ekstra');
            $table->text('harga_ekstra');
            $table->enum('status_ekstra', ['on', 'off'])->default('off');
            $table->text('foto_ekstra')->nullable();
            $table->text('thumbnail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra');
    }
};
