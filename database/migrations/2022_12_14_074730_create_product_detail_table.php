<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_detail', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained('product')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('province_id')->nullable();
            $table->foreignId('province_id_1')->nullable();
            $table->foreignId('province_id_2')->nullable();
            $table->foreignId('regency_id')->nullable();
            $table->foreignId('regency_id_1')->nullable();
            $table->foreignId('regency_id_2')->nullable();
            $table->foreignId('district_id')->nullable();
            $table->foreignId('village_id')->nullable();
            $table->foreignId('rute_id')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('kamar_id')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('discount_id')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('paket_id')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('extra_id')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('kategori_id')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('itenenary_id')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('harga_id')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('pilihan_id')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('mobil_id')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('detailmobil_id')->nullable();
            $table->foreignId('id_detail_kendaraan')->nullable();
            $table->foreignId('transfer_id')->nullable();
            $table->string('alamat')->nullable();
            $table->string('product_price')->nullable();
            $table->string('product_photo')->nullable();
            $table->string('confirmation')->nullable();
            $table->string('jml_hari')->nullable();
            $table->string('jml_malam')->nullable();
            $table->string('tipe_tur')->nullable();
            $table->string('durasi')->nullable();
            $table->string('icon_fasilitas')->nullable();
            $table->string('deskripsi_fasilitas')->nullable();
            $table->string('icon_amenitas')->nullable();
            $table->string('judul_amenitas')->nullable();
            $table->string('isi_amenitas')->nullable();
            $table->string('makanan')->nullable();
            $table->string('bahasa')->nullable();
            $table->string('tingkat_petualangan')->nullable();
            $table->text('ringkasan')->nullable();
            $table->text('deskripsi')->nullable();
            $table->text('paket_termasuk')->nullable();
            $table->text('paket_tidak_termasuk')->nullable();
            $table->text('catatan')->nullable();
            $table->text('komplemen')->nullable();
            $table->text('pilihan_tambahan')->nullable();
            $table->string('batas_pembayaran')->nullable();
            $table->string('kebijakan_pembatalan_sebelumnya')->nullable();
            $table->string('kebijakan_pembatalan_sesudah')->nullable();
            $table->string('kebijakan_pembatalan_potongan')->nullable();
            $table->text('link_maps')->nullable();
            $table->text('foto_maps_1')->nullable();
            $table->text('foto_maps_2')->nullable();
            $table->enum('izin_ekstra', ['Bolehkan', 'Tidak Usah'])->nullable();
            $table->text('gallery')->nullable();
            $table->text('thumbnail')->nullable();
            $table->text('faq')->nullable();
            $table->text('base_url')->nullable();
            $table->string('available')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_detail');
    }
};
