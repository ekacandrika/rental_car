<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pajak_admins', function (Blueprint $table) {
            $table->id();
            $table->string('komisi')->default(1);
            $table->string('komisiAgent')->default(1);
            $table->string('komisiCorporate')->default(1);
            $table->string('markup')->default(1);
            $table->string('total_pajak')->default(1);
            $table->unsignedBigInteger('user_id');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pajak_admins');
    }
};
