<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_kamars', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->nullable();
            $table->longText('nama_kamar')->nullable();
            $table->string('kode_kamar')->nullable();
            $table->string('jenis_kamar')->nullable();
            $table->string('harga_kamar')->nullable();
            $table->string('foto_kamar')->nullable();
            $table->string('stok')->nullable();
            $table->enum('refundable', ['full', 'partial'])->nullable();
            $table->string('refundable_amount')->nullable();
            $table->longText('nama_ekstra')->nullable();
            $table->longText('harga_ekstra')->nullable();
            $table->longText('icon_amenitas')->nullable();
            $table->longText('nama_amenitas')->nullable();
            $table->string('fasilitas')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_kamars');
    }
};
