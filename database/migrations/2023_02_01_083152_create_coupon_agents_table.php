<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_agents', function (Blueprint $table) {
            $table->id();
            $table->string('coupon_name');
            $table->string('coupon_address');
            $table->string('coupon_phone');
            $table->string('coupon_logo')->nullable();
            $table->text('coupon_message');
            $table->text('coupon_list');
            $table->integer('product_id')->nullable();
            $table->text('notes');
            $table->decimal('amount',2)->nullable();
            $table->decimal('tax_amount',12,2)->nullable();
            $table->decimal('total_amount',12,2)->nullable();
            $table->decimal('booking_fee',12,2)->nullable();
            $table->string('payment_notes',12,2)->nullable();
            $table->integer('user_input');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_agents');
    }
};
