<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merek_bus', function (Blueprint $table) {
            $table->id('id_merek');
            $table->bigInteger('user_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('id_jenis')->constrained('jenis_bus')->onDelete('cascade')->onUpdate('cascade');
            $table->string('merek_bus')->nullable();
            $table->string('plat_bus')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merek_bus');
    }
};
