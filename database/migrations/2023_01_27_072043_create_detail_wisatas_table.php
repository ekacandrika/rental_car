<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_wisatas', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->string('namaObjek')->nullable();
            $table->string('namaWisata')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('deskripsi')->nullable();
            $table->string('kategori')->nullable();
            $table->string('hari')->nullable();
            $table->string('hargaTiket')->nullable();
            $table->string('jamOperasional')->nullable();
            $table->string('transportasi')->nullable();
            $table->string('gallery_pulau')->nullable();
            $table->string('embed_maps')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_wisatas');
    }
};
