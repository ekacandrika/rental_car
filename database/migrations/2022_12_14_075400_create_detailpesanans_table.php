<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detailpesanans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('toko_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('customer_id')->nullable()->constrained('user_traveller')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('agent_id')->nullable()->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('product_detail_id')->constrained('product_detail')->onDelete('cascade')->onUpdate('cascade');

            $table->foreignId('paket_id')->nullable()->constrained('paket')->onDelete('cascade')->onUpdate('cascade');
            $table->string('paket_index')->nullable();

            $table->string('booking_code');
            $table->text('activity_date')->nullable();

            $table->date('check_in_date')->nullable();
            $table->date('check_out_date')->nullable();

            $table->foreignId('pilihan_id')->nullable()->constrained('pilihan')->onDelete('cascade')->onUpdate('cascade');
            $table->longText('index_pilihan')->nullable();
            $table->longText('jumlah_pilihan')->nullable();

            $table->longtext('extras_id')->nullable();
            $table->longText('catatan_umum_extra')->nullable();
            $table->longText('jumlah_extra')->nullable();
            $table->longText('catatan_per_extra')->nullable();
            $table->string('harga_ekstra')->nullable();

            $table->string('harga_asli');
            $table->string('total_price');
            $table->string('catatan')->nullable();
            $table->string('Ekstra_note')->nullable();
            $table->string('peserta_dewasa')->nullable();
            $table->string('peserta_anak_anak')->nullable();
            $table->string('peserta_balita')->nullable();
            $table->string('ekstra_sarapan')->nullable();
            $table->string('type')->nullable();
            $table->enum('status', ['0', '1']);

            $table->longText('kategori_peserta')->nullable();
            $table->longText('first_name')->nullable();
            $table->longText('last_name')->nullable();
            $table->longText('date_of_birth')->nullable();
            $table->longText('jk')->nullable();
            $table->longText('status_residen')->nullable();
            $table->longText('citizenship')->nullable();
            $table->longText('phone_number_order')->nullable();
            $table->longText('email_order')->nullable();
            $table->longText('ID_card_photo_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detailpesanans');
    }
};
