<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_rute', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_rute')->nullable();
            $table->json('judul_rute')->nullable();
            $table->string('naik_long')->nullable();
            $table->string('naik_lat')->nullable();
            $table->string('turun_long')->nullable();
            $table->string('turun_lat')->nullable();
            $table->integer('harga')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_rute');
    }
};
