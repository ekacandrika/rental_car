<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_mobil', function (Blueprint $table) {
            $table->id();
            $table->foreignId('merek_id')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->string('nama_mobil')->nullable();
            $table->string('kapasitas_kursi')->nullable();
            $table->string('kapasitas_koper')->nullable();
            $table->string('harga_akomodasi')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_mobil');
    }
};
