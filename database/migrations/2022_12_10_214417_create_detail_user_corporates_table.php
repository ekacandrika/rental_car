<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edit_akun_corporates', function (Blueprint $table) {
            $table->id();
            $table->string('nama_depan');
            $table->string('nama_belakang')->nullable();
            $table->string('alamat_email');
            $table->string('no_telepon');
            $table->string('negara');
            $table->string('id_user');
            $table->string('nama_dagang');
            $table->string('nama_perusahaan');
            $table->string('no_telp');
            $table->text('alamat');
            $table->string('kota')->nullable();
            $table->string('kode_pos');
            $table->string('website');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edit_akun_corporates');
    }
};
