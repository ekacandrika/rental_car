<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained('product')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('toko_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('agent_id')->nullable()->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('booking_code')->nullable();
            $table->string('total_price')->nullable();
            $table->string('komisi')->nullable();
            $table->dateTime('tgl_checkout')->nullable();
            $table->text('kegiatan')->nullable();
            $table->text('bukti_pembayaran')->nullable();
            $table->text('tanda_terima')->nullable();
            $table->string('type')->nullable();
            $table->enum('status', ['-', 'Proses', 'Sudah Bayar', 'Gagal']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuans');
    }
};
