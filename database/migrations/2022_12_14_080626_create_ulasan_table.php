<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ulasan', function (Blueprint $table) {
            $table->id();
            $table->foreignId('customer_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('order_invoice_id')->constrained('order_invoice')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('toko_id')->constrained('toko')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('product_detail_id')->constrained('product_detail')->onDelete('cascade')->onUpdate('cascade');
            $table->dateTime('ulasan_datetime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ulasan');
    }
};
