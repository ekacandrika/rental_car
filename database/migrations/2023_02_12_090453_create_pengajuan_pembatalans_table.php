<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_pembatalans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('product_id')->constrained('product')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('toko_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('data_booking_id')->nullable()->constrained('booking')->onDelete('cascade')->onUpdate('cascade');
            $table->string('kode_pengajuan')->nullable();
            $table->string('kode_pembatalan')->nullable();
            $table->string('harga')->nullable();
            $table->dateTime('tgl_pengajuan')->nullable();
            $table->dateTime('tgl_checkout')->nullable();
            $table->string('status_pembayaran')->nullable();
            $table->enum('status_pengajuan', ['Proses', 'Dibatalkan Atas Permintaan Traveller', 'Dibatalkan Oleh Seller', 'Gagal']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_pembatalans');
    }
};
