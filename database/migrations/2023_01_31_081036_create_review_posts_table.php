<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_posts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('detailObjek_id')->nullable();
            $table->unsignedBigInteger('detailWisata_id')->nullable();
            $table->unsignedBigInteger('product_id')->nullable();
            $table->longText('comments')->nullable();
            $table->integer('star_rating');
            $table->enum('status', ['active', 'deactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_posts');
    }
};
