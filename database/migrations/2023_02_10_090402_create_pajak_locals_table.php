<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pajak_locals', function (Blueprint $table) {
            $table->id();
            $table->string('markupLocal')->nullable();
            $table->string('statusMarkupLocal')->nullable();
            $table->string('komisiLocal')->nullable();
            $table->string('statusKomisiLocal')->nullable();
            $table->string('komisiLocalAgent')->nullable();
            $table->string('statusKomisiLocalAgent')->nullable();
            $table->string('komisiLocalCorporate')->nullable();
            $table->string('statusKomisiLocalCorporate')->nullable();
            $table->string('produk_type')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('pajak_id');


            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('pajak_id')->references('id')->on('pajak_admins')->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pajak_locals');
    }
};
