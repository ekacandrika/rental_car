<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelolatokos', function(Blueprint $table){
            $table->id();
            $table->string('nama_toko');
            $table->string('logo_toko', 2048)->nullable();
            $table->string('banner_toko', 2048)->nullable();
            $table->string('nama_banner');
            $table->string('tautan_banner');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelolatokos');
    }
};
