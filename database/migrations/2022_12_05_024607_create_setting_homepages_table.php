<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_homepages', function (Blueprint $table) {
            $table->id();
            $table->string('title_section')->nullable();
            $table->string('image_section')->nullable();
            $table->enum('status_section', ['on', 'off'])->default('off');
            $table->text('detail_section')->nullable();
            $table->enum('section', [
                'Konten_1', 'Traveller', 'Seller', 'Agen',
                'Kamtuu', 'OFStore', 'Corporate', 'Layanan', 'Kerjasama', 'Home'
            ]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_homepages');
    }
};
