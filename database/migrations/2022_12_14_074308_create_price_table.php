<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price', function (Blueprint $table) {
            $table->id();
            $table->string('dewasa_residen')->nullable();
            $table->string('dewasa_non_residen')->nullable();
            $table->string('anak_residen')->nullable();
            $table->string('balita_residen')->nullable();
            $table->string('anak_non_residen')->nullable();
            $table->string('balita_non_residen')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price');
    }
};
