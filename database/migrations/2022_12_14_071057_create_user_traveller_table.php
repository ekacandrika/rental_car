<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_traveller', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('province_id')->nullable();
            $table->foreignId('regency_id')->nullable();
            $table->foreignId('district_id')->nullable();
            $table->foreignId('village_id')->nullable();
            $table->string('last_name')->nullable();
            $table->string('status_residen')->nullable();
            $table->string('citizenship')->nullable();
            $table->string('gender')->nullable();
            $table->string('akun_booking_status')->nullable();
            $table->string('first_name_booking')->nullable();
            $table->string('last_name_booking')->nullable();
            $table->string('email_booking')->unique()->nullable();
            $table->string('phone_number_booking')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('nik')->unique()->nullable();
            $table->string('ID_card_photo')->nullable();
            $table->string('passport_number')->nullable();
            $table->string('passport_active_period')->nullable();
            $table->string('passport_photo')->nullable();
            $table->string('second_phone_number')->nullable();
            $table->string('second_email')->unique()->nullable();
            $table->string('newslatter_status')->nullable();
            $table->text('address')->nullable();
            $table->string('negara')->nullable();
            $table->string('postal_code')->nullable();
            $table->text('alamat_web')->nullable();
            $table->json('kupon_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_traveller');
    }
};
