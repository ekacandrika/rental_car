<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_detail', function (Blueprint $table) {
            //
            $table->string('doc_akta');
            $table->smallInteger('status_doc_akta');
            $table->string('doc_legal');
            $table->smallInteger('status_doc_legal');
            $table->string('doc_directur_id_card');
            $table->smallInteger('status_directur_id_card');
            $table->smallInteger('status_supervisor_id_card');
            $table->string('job_title');
            $table->string('directur_name');
            $table->enum('store_type',["unofficial store","official store"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_detail', function (Blueprint $table) {
            //
        });
    }
};
