<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_kendaraan', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_jenis_bus')->nullable()->constrained('jenis_bus')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('id_jenis_mobil')->nullable()->constrained('jenis_mobil')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('id_merek_mobil')->nullable()->constrained('merek_mobil')->onDelete('cascade')->onUpdate('cascade');
            $table->string('nama_kendaraan')->nullable();
            $table->integer('kapasitas_kursi')->nullable();
            $table->integer('kapasitas_koper')->nullable();
            $table->bigInteger('harga_akomodasi')->nullable();
            $table->string('status')->nullable();
            $table->string('pilihan_tambahan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_kendaraan');
    }
};
