<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pemesanans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('product_id')->onDelete('cascade')->onUpdate('cascade');

            $table->string('total_harga')->nullable();

            $table->foreignId('paket_id')->onDelete('cascade')->onUpdate('cascade');
            $table->string('paket_index')->nullable();

            $table->string('order_code')->nullable();
            $table->string('tanggal_kegiatan')->nullable();
            
            $table->foreignId('pilihan_id')->onDelete('cascade')->onUpdate('cascade');
            $table->longText('index_pilihan')->nullable();
            $table->longText('jumlah_pilihan')->nullable();
            
            $table->longtext('extras_id')->nullable();
            $table->longText('catatan_umum_extra')->nullable();
            $table->longText('jumlah_extra')->nullable();
            $table->longText('catatan_per_extra')->nullable();
            
            $table->longText('kategori_peserta')->nullable();
            $table->longText('first_name')->nullable();
            $table->longText('last_name')->nullable();
            $table->longText('date_of_birth')->nullable();
            $table->longText('jk')->nullable();
            $table->longText('status_residen')->nullable();
            $table->longText('citizenship')->nullable();
            $table->longText('phone_number_order')->nullable();
            $table->longText('email_order')->nullable();
            $table->longText('ID_card_photo_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pemesanans');
    }
};
