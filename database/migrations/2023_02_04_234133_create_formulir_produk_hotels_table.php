<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulir_produk_hotels', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('checkIn')->nullable();
            $table->string('checkOut')->nullable();
            $table->string('lokasi')->nullable();
            $table->string('jenisHotel')->nullable();
            $table->string('fasilitas')->nullable();
            $table->string('kebutuhan')->nullable();
            $table->json('jenisMakan')->nullable();
            $table->json('jumlahOrang')->nullable();
            $table->json('jumlahOrangPerkamar')->nullable();
            $table->json('jenisKamar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulir_produk_hotels');
    }
};
