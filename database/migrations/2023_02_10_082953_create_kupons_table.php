<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kupons', function (Blueprint $table) {
            $table->id();
            $table->string('kodeKupon')->nullable();
            $table->string('diskonKupon')->nullable();
            $table->string('jumlahKupon')->nullable();
            $table->string('status')->nullable();
            $table->string('judulKupon')->nullable();
            $table->date('expiredDate')->nullable();
            $table->json('user_id')->nullable();
            $table->unsignedBigInteger('produk_id')->nullable();

//            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('produk_id')->references('id')->on('product')->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kupons');
    }
};
