CREATE DATABASE  IF NOT EXISTS `rental_car` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `rental_car`;
-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: rental_car
-- ------------------------------------------------------
-- Server version	8.0.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pajak_locals`
--

DROP TABLE IF EXISTS `pajak_locals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pajak_locals` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `markupLocal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statusMarkupLocal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `komisiLocal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statusKomisiLocal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `komisiLocalAgent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statusKomisiLocalAgent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `komisiLocalCorporate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statusKomisiLocalCorporate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint unsigned NOT NULL,
  `pajak_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pajak_locals_user_id_foreign` (`user_id`),
  KEY `pajak_locals_pajak_id_foreign` (`pajak_id`),
  CONSTRAINT `pajak_locals_pajak_id_foreign` FOREIGN KEY (`pajak_id`) REFERENCES `pajak_admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pajak_locals_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pajak_locals`
--

LOCK TABLES `pajak_locals` WRITE;
/*!40000 ALTER TABLE `pajak_locals` DISABLE KEYS */;
/*!40000 ALTER TABLE `pajak_locals` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-13 16:44:32
