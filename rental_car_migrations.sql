CREATE DATABASE  IF NOT EXISTS `rental_car` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `rental_car`;
-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: rental_car
-- ------------------------------------------------------
-- Server version	8.0.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_01_07_073615_create_tagged_table',1),(2,'2014_01_07_073615_create_tags_table',1),(3,'2014_10_12_000000_create_users_table',1),(4,'2014_10_12_100000_create_password_resets_table',1),(5,'2014_10_12_200000_add_two_factor_columns_to_users_table',1),(6,'2016_06_29_073615_create_tag_groups_table',1),(7,'2016_06_29_073615_update_tags_table',1),(8,'2017_05_02_140432_create_provinces_tables',1),(9,'2017_05_02_140444_create_regencies_tables',1),(10,'2017_05_02_142019_create_districts_tables',1),(11,'2017_05_02_143454_create_villages_tables',1),(12,'2019_08_19_000000_create_failed_jobs_table',1),(13,'2019_12_14_000001_create_personal_access_tokens_table',1),(14,'2020_03_13_083515_add_description_to_tags_table',1),(15,'2022_09_21_073517_create_sessions_table',1),(16,'2022_11_26_110620_create_destindonesias_table',1),(17,'2022_12_05_024607_create_setting_homepages_table',1),(18,'2022_12_06_113456_kelola_toko',1),(19,'2022_12_08_144759_create_user_detail_table',1),(20,'2022_12_09_080847_create_master_kamars_table',1),(21,'2022_12_10_214417_create_detail_user_corporates_table',1),(22,'2022_12_12_074642_create_meta_tags_table',1),(23,'2022_12_12_084339_create_newsletters_table',1),(24,'2022_12_13_024227_create_master_driver_tables',1),(25,'2022_12_13_063147_create_seo_table',1),(26,'2022_12_14_055911_create_pulau_table',1),(27,'2022_12_14_062039_create_destiindo_table',1),(28,'2022_12_14_062327_create_artikel_table',1),(29,'2022_12_14_062622_create_product_table',1),(30,'2022_12_14_062755_create_paket_table',1),(31,'2022_12_14_062835_create_extra_table',1),(32,'2022_12_14_062942_create_discount_product_table',1),(33,'2022_12_14_071057_create_user_traveller_table',1),(34,'2022_12_14_071225_create_toko_table',1),(35,'2022_12_14_074038_create_kategori_table',1),(36,'2022_12_14_074126_create_itenenary_table',1),(37,'2022_12_14_074308_create_price_table',1),(38,'2022_12_14_074551_create_pilihan_table',1),(39,'2022_12_14_074640_create_mobil_table',1),(40,'2022_12_14_074730_create_product_detail_table',1),(41,'2022_12_14_075400_create_detailpesanans_table',1),(42,'2022_12_14_075401_create_booking_table',1),(43,'2022_12_14_075539_create_order_table',1),(44,'2022_12_14_075832_create_order_invoice_table',1),(45,'2022_12_14_080201_create_detail_toko_table',1),(46,'2022_12_14_080626_create_ulasan_table',1),(47,'2022_12_14_083035_create_favorit_table',1),(48,'2022_12_14_083154_create_section_homepage_table',1),(49,'2022_12_28_084411_create_setting_generals_table',1),(50,'2022_12_30_032001_create_detail_mobil',1),(51,'2022_12_30_034636_create_jenis_mobil',1),(52,'2022_12_30_034706_create_merek_mobil',1),(53,'2023_01_08_154409_create_masterroutes_table',1),(54,'2023_01_26_070516_create_pulaus_table',1),(55,'2023_01_27_072043_create_detail_wisatas_table',1),(56,'2023_01_31_081036_create_review_posts_table',1),(57,'2023_02_01_083152_create_coupon_agents_table',1),(58,'2023_02_01_115132_create_detail_rute_table',1),(59,'2023_02_04_234126_create_formulir_produk_turs_table',1),(60,'2023_02_04_234133_create_formulir_produk_hotels_table',1),(61,'2023_02_04_234139_create_formulir_produk_transfers_table',1),(62,'2023_02_07_074100_create_detail_messages_table',1),(63,'2023_02_07_082841_create_detail_pemesanans_table',1),(64,'2023_02_08_071356_create_messages_table',1),(65,'2023_02_10_082927_create_pajak_admins_table',1),(66,'2023_02_10_082953_create_kupons_table',1),(67,'2023_02_10_090402_create_pajak_locals_table',1),(68,'2023_02_11_093656_create_pengajuans_table',1),(69,'2023_02_11_174348_create_data_pengajuans_table',1),(70,'2023_02_12_021751_create_like_product_table',1),(71,'2023_02_12_090453_create_pengajuan_pembatalans_table',1),(72,'2023_02_13_023828_create_all_banner_tables',1),(73,'2023_02_13_023931_create_banner_toko_tables',1),(74,'2023_02_13_024001_create_detail_kendaraan_tables',1),(75,'2023_02_13_024113_create_jenis_bus_tables',1),(76,'2023_02_13_024154_create_merek_bus_tables',1),(77,'2023_03_06_025302_create_master_choices_table',1),(78,'2023_04_06_092744_create_branch_stores_table',1),(79,'2023_04_10_013423_add_fields_to_table_user_detail',1),(80,'2023_04_10_021140_add_doc_nib_and_status_doc_nib_to_table_user_detail',1),(81,'2023_04_12_061353_add_field_product_type_to_master-choices',1),(82,'2023_07_12_072546_create_table_receipt',1),(83,'2023_10_13_043617_master_legal_document',1),(84,'2023_10_13_103106_create_table_master_document_value',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-13 16:44:34
