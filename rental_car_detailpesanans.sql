CREATE DATABASE  IF NOT EXISTS `rental_car` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `rental_car`;
-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: rental_car
-- ------------------------------------------------------
-- Server version	8.0.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `detailpesanans`
--

DROP TABLE IF EXISTS `detailpesanans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detailpesanans` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `toko_id` bigint unsigned NOT NULL,
  `customer_id` bigint unsigned DEFAULT NULL,
  `agent_id` bigint unsigned DEFAULT NULL,
  `product_detail_id` bigint unsigned NOT NULL,
  `paket_id` bigint unsigned DEFAULT NULL,
  `paket_index` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `booking_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activity_date` text COLLATE utf8mb4_unicode_ci,
  `check_in_date` date DEFAULT NULL,
  `check_out_date` date DEFAULT NULL,
  `pilihan_id` bigint unsigned DEFAULT NULL,
  `index_pilihan` longtext COLLATE utf8mb4_unicode_ci,
  `jumlah_pilihan` longtext COLLATE utf8mb4_unicode_ci,
  `extras_id` longtext COLLATE utf8mb4_unicode_ci,
  `catatan_umum_extra` longtext COLLATE utf8mb4_unicode_ci,
  `jumlah_extra` longtext COLLATE utf8mb4_unicode_ci,
  `catatan_per_extra` longtext COLLATE utf8mb4_unicode_ci,
  `harga_ekstra` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga_asli` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Ekstra_note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `peserta_dewasa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `peserta_anak_anak` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `peserta_balita` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ekstra_sarapan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori_peserta` longtext COLLATE utf8mb4_unicode_ci,
  `first_name` longtext COLLATE utf8mb4_unicode_ci,
  `last_name` longtext COLLATE utf8mb4_unicode_ci,
  `date_of_birth` longtext COLLATE utf8mb4_unicode_ci,
  `jk` longtext COLLATE utf8mb4_unicode_ci,
  `status_residen` longtext COLLATE utf8mb4_unicode_ci,
  `citizenship` longtext COLLATE utf8mb4_unicode_ci,
  `phone_number_order` longtext COLLATE utf8mb4_unicode_ci,
  `email_order` longtext COLLATE utf8mb4_unicode_ci,
  `ID_card_photo_order` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detailpesanans_toko_id_foreign` (`toko_id`),
  KEY `detailpesanans_customer_id_foreign` (`customer_id`),
  KEY `detailpesanans_agent_id_foreign` (`agent_id`),
  KEY `detailpesanans_product_detail_id_foreign` (`product_detail_id`),
  KEY `detailpesanans_paket_id_foreign` (`paket_id`),
  KEY `detailpesanans_pilihan_id_foreign` (`pilihan_id`),
  CONSTRAINT `detailpesanans_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detailpesanans_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `user_traveller` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detailpesanans_paket_id_foreign` FOREIGN KEY (`paket_id`) REFERENCES `paket` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detailpesanans_pilihan_id_foreign` FOREIGN KEY (`pilihan_id`) REFERENCES `pilihan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detailpesanans_product_detail_id_foreign` FOREIGN KEY (`product_detail_id`) REFERENCES `product_detail` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detailpesanans_toko_id_foreign` FOREIGN KEY (`toko_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detailpesanans`
--

LOCK TABLES `detailpesanans` WRITE;
/*!40000 ALTER TABLE `detailpesanans` DISABLE KEYS */;
/*!40000 ALTER TABLE `detailpesanans` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-13 16:44:36
