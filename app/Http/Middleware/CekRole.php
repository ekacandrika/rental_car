<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class CekRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$roles)
    {
        if (in_array($request->user()->role, $roles)) {
            return $next($request);
        }

        // return redirect('/');
        if (Auth::user()->role == 'super user kamtuu') {
            return Redirect::to('dashboard');
        } elseif (Auth::user()->role == 'user kamtuu produk') {
            return Redirect::to('dashboard/user/produk');
        } elseif (Auth::user()->role == 'seller') {
            return Redirect::to('dashboard/seller');
        } elseif (Auth::user()->role == 'agent') {
            return Redirect::to('dashboard/agent');
        } elseif (Auth::user()->role == 'corporate') {
            return Redirect::to('dashboard/corporate');
        } elseif (Auth::user()->role == 'traveller') {
            return Redirect::to('dashboard/traveller');
        } elseif (Auth::user()->role == 'visitor') {
            return Redirect::to('dashboard/visitor');
        } elseif (Auth::user()->role == 'admin') {
            return Redirect::to('dashboard/admin/kamtuu');
        }
    }
}
