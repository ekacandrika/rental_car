<?php

namespace App\Http\Livewire\Transfer;

use Livewire\Component;
use App\Models\LayoutBus;

class ModalSeat extends Component
{
    public $nama_bus;
    public $sheet_detail;
    public $sheet_id=0;
    public $sheet_count=[];
    public $transfer=[];
    public $galleries=[];
    public $transfer_detail=[];
    public $showingModal = false;
    public $layout_id=0;

    public $listeners = [
        'hideMe'=>'hideModal'
    ];

    public function mount(){
        // dump($this->galleries[0]->gallery);
    
    }

    public function booted(){
        $this->dispatchBrowserEvent('content-updated');
    }

    public function render()
    {
        $layout = LayoutBus::where('id_layout',$this->layout_id)->first();
        $kursi = isset($layout) ? json_decode($layout->layout_kursi, true)['result']: [];

        return view('livewire.transfer.modal-seat',['seats'=>$kursi]);
    }

    public function showModal(){
        $this->showingModal=true;
    }

    public function hideModal(){
        $this->showingModal=false;
    }
}
