<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class ModalAddUser extends Component
{
    public function render()
    {
        return view('livewire.admin.modal-add-user');
    }

    public $showingModal = false;
    public $showingDetail = false;

    public $listeners = [
        'hideMe' => 'hideModal'
    ];

    public function showModal(){
        $this->showingModal = true;
    }

    public function hideModal(){
        $this->showingModal = false;
    }

    public function showDetail(){
        $this->showingDetail = true;
    }

    public function hideDetail(){
        $this->showingDetail = false;
    }
}
