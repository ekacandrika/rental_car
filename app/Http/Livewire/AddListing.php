<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\DetailProduk;

class AddListing extends Component
{
    public $nama_produk;
    public $konfirmasi;
    public $jumlah_hari;
    public $jumlah_malam;
    public $tipe;
    public $makanan;
    public $bahasa;
    public $tingkat_petualangan;
    public $deskripsi;
    public $paket_termasuk;
    public $paket_tidak_termasuk;
    public $paket_catatan;
    public $kebijakan_pembatalan_sebelum;
    public $kebijakan_pembatalan_sesudah;
    public $kebijakan_pembatalan_potongan;

    public $pembayarans =[];

    public $totalSteps = 6;
    public $currentStep = 1;

    protected $rules = [
        'pembayarans.*.kebijakan_pembatalan_sebelum' => 'required',
        'pembayarans.*.kebijakan_pembatalan_sesudah' => 'required',
        'pembayarans.*.kebijakan_pembatalan_potongan' => 'required'
    ];

    public function mount(){
        $this->pembayarans = DetailProduk::all();
        $this->currentStep = 1;
    }

    public function addPembayaran(){
        $this->pembayarans->push(new DetailProduk());
    }
    
    public function render()
    {
        return view('livewire.add-listing');
    }

    public function increaseStep(){
         $this->currentStep++;
         if($this->currentStep > $this->totalSteps){
             $this->currentStep = $this->totalSteps;
         }
    }

    public function decreaseStep(){
        $this->currentStep--;
        if($this->currentStep < 1){
            $this->currentStep = 1;
        }
    }
    
    public function informasi(){
        $values = array(
            "nama_produk"=>$this->nama_produk,
            "konfirmasi"=>$this->konfirmasi,
            "jumlah_hari"=>$this->jumlah_hari,
            "jumlah_malam"=>$this->jumlah_malam,
            "tipe"=>$this->tipe,
            "makanan"=>$this->makanan,
            "bahasa"=>$this->bahasa,
            "tingkat_petualangan"=>$this->tingkat_petualangan,
            "deskripsi"=>$this->deskripsi,
            "paket_termasuk"=>$this->paket_termasuk,
            "paket_tidak_termasuk"=>$this->paket_tidak_termasuk,
            "kebijakan_pembatalan_sebelum"=>$this->kebijakan_pembatalan_sebelum,
            "kebijakan_pembatalan_sesudah"=>$this->kebijakan_pembatalan_sesudah,
            "kebijakan_pembatalan_potongan"=>$this->kebijakan_pembatalan_potongan,
        );

        dd($values);
        DetailProduk::insert($values);
        return redirect()->route('informasi');
    }
}
