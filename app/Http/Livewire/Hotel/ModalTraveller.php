<?php

namespace App\Http\Livewire\Hotel;

use Livewire\Component;

class ModalTraveller extends Component
{
    public function render()
    {
        return view('livewire.hotel.modal-traveller');
    }

    public $showingModal = false;

    public $listeners = [
        'hideMe' => 'hideModal'
    ];

    public function showModal(){
        $this->showingModal = true;
    }

    public function hideModal(){
        $this->showingModal = false;
    }
}
