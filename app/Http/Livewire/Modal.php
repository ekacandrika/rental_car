<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Modal extends Component
{
    public function render()
    {
        return view('livewire.modal');
    }

    public $showingModal = false;

    public $listeners = [
        'hideMe' => 'hideModal'
    ];

    public function showModal(){
        $this->showingModal = true;
    }

    public function hideModal(){
        $this->showingModal = false;
    }
}
