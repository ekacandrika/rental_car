<?php

namespace App\Http\Livewire\Tur;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.tur.index');
    }

    public $showingModal = false;
    public $showingEkstra = false;

    public $listeners = [
        'hideMe' => 'hideModal'
    ];

    public function showModal(){
        $this->showingModal = true;
    }

    public function hideModal(){
        $this->showingModal = false;
    }

    public function showEkstra(){
        $this->showingEkstra = true;
    }

    public function hideEkstra(){
        $this->showingEkstra = false;
    }
}
