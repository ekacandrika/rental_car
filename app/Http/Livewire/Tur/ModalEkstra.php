<?php

namespace App\Http\Livewire\Tur;

use App\Models\Extra;
use Livewire\Component;
use Livewire\WithPagination;

class ModalEkstra extends Component
{
    public $extra_detail;
    public $extra_foto;
    public $extra_id = 0;
    public $extra_count = [];

    use WithPagination;

    public function mount()
    {
        $extra = Extra::where('status_ekstra', 'on')->get();
        foreach ($extra as $item) {
            array_push($this->extra_count, [
                "count" => 0,
                "id_extra" => '',
                "note_extra" => ''
            ]);
        }
    }

    public function booted()
    {
        $this->dispatchBrowserEvent('content-updated');
    }

    public function render()
    {
        return view('livewire.tur.modal-ekstra', ['extras' => Extra::where('status_ekstra', 'on')->get()]);
    }

    public $showingModal = false;
    public $showingDetail = false;

    public $listeners = [
        'hideMe' => 'hideModal'
    ];

    public function showModal()
    {
        $this->showingModal = true;
    }

    public function hideModal()
    {
        $this->showingModal = false;
    }

    public function showDetail($id)
    {
        $this->showingDetail = true;
        $this->extra_id = $id;
        $this->dispatchBrowserEvent('content-updated');
    }

    public function hideDetail()
    {
        $this->showingDetail = false;
    }

    public function decrement($index)
    {
        $this->extra_count[$index]['count'] = $this->extra_count[$index]['count'] == 0 ? 0 : $this->extra_count[$index]['count'] - 1;
        $this->dispatchBrowserEvent('content-updated');
    }
    public function increment($index)
    {
        $this->extra_count[$index]['count']++;
        $this->dispatchBrowserEvent('content-updated');
    }
}
