<?php

namespace App\Http\Livewire\Tur;

use Livewire\Component;

class ModalPilihan extends Component
{
    public $pilihans = [];
    public $pilihan_count = [];

    public function mount()
    {
        foreach (json_decode($this->pilihans['judul_pilihan']) as $item) {
            array_push($this->pilihan_count, [
                "count" => 0,
            ]);
        }
    }

    public function render()
    {
        return view('livewire.tur.modal-pilihan');
    }

    public $showingModal = false;

    public $listeners = [
        'hideMe' => 'hideModal'
    ];

    public function showModal()
    {
        $this->showingModal = true;
    }

    public function hideModal()
    {
        $this->showingModal = false;
    }

    public function decrement($index)
    {
        $this->pilihan_count[$index]['count'] = $this->pilihan_count[$index]['count'] == 0 ? 0 : $this->pilihan_count[$index]['count'] - 1;
    }

    public function increment($index)
    {
        $this->pilihan_count[$index]['count']++;
    }
}
