<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoremetaTagsRequest;
use App\Http\Requests\UpdatemetaTagsRequest;
use App\Models\metaTags;

class MetaTagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoremetaTagsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoremetaTagsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\metaTags  $metaTags
     * @return \Illuminate\Http\Response
     */
    public function show(metaTags $metaTags)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\metaTags  $metaTags
     * @return \Illuminate\Http\Response
     */
    public function edit(metaTags $metaTags)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatemetaTagsRequest  $request
     * @param  \App\Models\metaTags  $metaTags
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatemetaTagsRequest $request, metaTags $metaTags)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\metaTags  $metaTags
     * @return \Illuminate\Http\Response
     */
    public function destroy(metaTags $metaTags)
    {
        //
    }
}
