<?php

namespace App\Http\Controllers;

use App\Models\detailMessage;
use App\Models\message;
use App\Models\Product;
use App\Models\Hotel;
use App\Models\User;
use App\View\Components\Produk\produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class MessageController extends Controller
{
//    with produk detail inbox
    public function index($id, $user_id)
    {
        $admin = User::select('id')->first();
        $produk = Product::findOrFail($id);
        $sender = Auth::user()->id;
        $receiver = $user_id;
        $chatAll = detailMessage::with('produks','users')->where('sender', '=', $sender)
            ->where('receiver', '=', $receiver)
//            ->latest()
            ->get();

        return view('BE.traveller.detail-pesan', compact('produk','chatAll', 'admin', 'sender'));

    }

//    with produk store
    public function inboxStore(Request $request, $id, $user_id)
    {
        // dd($request);
        $produk_id = $id;
        $receiver = $user_id;
        $sender = \Auth::user()->id;

        $chat = new detailMessage;


        $chat->text = $request->text;
        $chat->sender = $sender;
        $chat->receiver = $receiver;
        $chat->produk_id = $produk_id;
        $chat->user = $sender;

//dd($chat);
        $chat->save();

        return back();
    }

//    list
    public function indexSeller()
    {
        $receiver = \Auth::user()->id;
        $chatAll = detailMessage::groupBy('sender')
            ->join('users','detail_messages.sender','=','users.id')
            ->join('product','detail_messages.produk_id','=','product.id')
            ->where('receiver','=', $receiver)
            ->select('detail_messages.*','users.first_name','users.created_at','product.*')
            ->get()
        ;


//        dd($chatAll);
        return view('chat.index-seller',compact('chatAll'));
    }

    public function inboxSeller($user)
    {
        $users = Auth::user()->id;
        $sender = $user;
        $receiver = Auth::user()->id;
        $chatAll = detailMessage::with('produks','users')->where('sender', '=', $sender)
            ->where('receiver', '=', $receiver)
            ->latest()
            ->get();
        $chats = detailMessage::with('produks','users')->where('sender', '=', $sender)
            ->where('receiver', '=', $receiver)
            ->first();
//        dd($chatAll,$sender,$receiver);
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        return view('chat.inbox-seller', compact('chatAll','chats','users', 'data', 'xstay'));
    }

    public function inboxSellerStore(Request $request)
    {


        $chat = new detailMessage;

        $chat->text = $request->text;
        $chat->sender = $request->sender;
        $chat->receiver = $request->receiver;
//        $chat->produk_id = $produk_id;
        $chat->user = \Auth::user()->id;


        $chat->save();
        return back();
    }

    public function indexTraveller()
    {
//        $user = \Auth::user()->id;
//        $chat = message::join('detail_messages', 'messages.detail_messages_id', '=', 'detail_messages.id')
//            ->join('users', 'detail_messages.user1', '=', 'users.id')
//            ->join('product', 'detail_messages.produk_id', '=', 'product.id')->where('user1', '=', $user)
//            ->select()->distinct()->get(['messages.', 'detail_messages.', 'users.id']);
//
//        $getAllInbox = collect($chat);
//        $unique = $getAllInbox->unique('inbox_code');
////        dd($unique);
//        return view('BE.traveller.pesan', compact('unique'));

        $sender = Auth::user()->id;

//        $chatAll = detailMessage::with('produks','users')
//            ->groupBy('receiver')
//            ->where('sender', '=', $sender)
//            ->select('receiver','user')
//            ->get();
        $chatAll = detailMessage::join('users','users.id','=','detail_messages.receiver')

            ->groupBy('receiver')
            ->select('receiver','user','sender','first_name','detail_messages.updated_at')
            ->orderBy('detail_messages.updated_at')
            ->get();

//        dd($chatAll);
        return view('BE.traveller.pesan', compact('chatAll'));
    }

    public function inboxTraveller($receiver)
    {
        $admin = User::select('id')->first();
        // dd($admin);
        $sender = Auth::user()->id;

        if(is_string($receiver)){
            $receivers = Auth::user()->id;
        }else{
            $receivers = $receiver;
        }
        
        $chatAll = detailMessage::with('produks','users')->where('sender', '=', $sender)
            ->where('receiver', '=', $receivers)
            ->latest()
            ->get();

//        dd($chatAll, $admin, $sender);

        return view('BE.traveller.detail-pesan2', compact('chatAll','receivers', 'sender','admin'));
    }

    public function inboxTravellerStore(Request $request, $receiver)
    {
        // dd($request->all(),$receiver);
        $sender = \Auth::user()->id;

        $chat = new detailMessage;


        $chat->text = $request->text;
        $chat->sender = $sender;
        $chat->receiver = $receiver ;
        $chat->user = $sender;

        $chat->save();

        return back();
    }

    public function inboxTravellerAdmin($receiver)
    {
//        $admin = User::select('id')->first();
        $sender = Auth::user()->id;
        $chatAll = detailMessage::with('produks','users')->where('sender', '=', $sender)
            ->where('receiver', '=', $receiver)
            ->latest()
            ->get();
        // dd($chatAll);

        return view('BE.traveller.chat-admin', compact('chatAll','receiver','sender'));
    }

    public function inboxTravellerAdminStore (Request $request, $receiver)
    {
        $sender = \Auth::user()->id;
        // dd($request,$receiver);
        $chat = new detailMessage;


        $chat->text = $request->text;
        $chat->sender = $sender;
        $chat->receiver = $receiver;
        $chat->user = $sender;

        // dd($chat);
        $chat->save();

        return back();
    }

    public function indexMessagesAdmin()
    {
        $receiver = \Auth::user()->id;
//        dd($receiver);

        $chatAll = detailMessage::join('users','users.id','=','detail_messages.sender')
            ->where('receiver','=',$receiver)
            ->groupBy('sender')
            ->select('receiver','user','sender','first_name','detail_messages.updated_at')
            ->orderBy('detail_messages.updated_at')
            ->get();

//        dd($chatAll);

        return view('BE.admin.Messages.index-messages',compact('chatAll'));
    }

    public function indexMessagesAdminInbox($sender)
    {
        $receiver = \Auth::user()->id;
        $chatAll = detailMessage::with('produks','users')
            ->where('sender', '=', $sender)
            ->where('receiver', '=', $receiver)
            ->latest()
            ->get();
//        dd($chatAll);
        return view ('BE.admin.Messages.inbox-message', compact('chatAll','receiver','sender'));
    }

    public function indexMessagesAdminStore(Request $request, $sender)
    {
        $chat = new detailMessage;


        $chat->text = $request->text;
        $chat->sender = $sender;
        $chat->receiver = \Auth::user()->id;
        $chat->user = \Auth::user()->id;

//dd($chat);
        $chat->save();

        return back();
    }
}