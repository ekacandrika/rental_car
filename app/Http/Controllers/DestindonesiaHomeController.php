<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Paket;
use App\Models\Kategori;
use App\Models\Pilihan;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\detailObjek;
use App\Models\detailWisata;
use App\Models\newsletter;
use App\Models\settingGeneral;

use Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class DestindonesiaHomeController extends Controller
{
    function getProduct(String $type)
    {
        if ($type == 'hotel' || $type == 'xstay') {
            return Product::where('type', $type)
                ->has('productdetail.masterkamar')
                ->whereHas('productdetail', function ($query) {
                    $query->where('available', 'publish');
                })
                ->with('productdetail.diskon', 'productdetail.harga', 'productdetail.masterkamar', 'productdetail.detailkendaraan', 'productdetail.regency', 'user')
                ->limit(5)
                ->inRandomOrder()
                ->get();
        }

        return Product::where('type', $type)
            ->whereHas('productdetail', function ($query) {
                $query->where('available', 'publish');
            })
            ->with('productdetail.diskon', 'productdetail.harga', 'productdetail.masterkamar', 'productdetail.detailkendaraan', 'productdetail.regency', 'user')
            ->limit(5)
            ->latest()
            ->get();
    }

    function productTour(String $regency_id)
    {
        // dump($regency_id);
        // die;
        if ($regency_id != null) {
        }
        return Product::join('product_detail as b', 'b.product_id', '=', 'product.id')
            ->join('regencies as c', 'c.id', '=', 'b.regency_id')
            ->where('product.type', 'tour')
            ->where('regency_id', $regency_id)
            ->get()->toArray();
    }

    function productWisata(String $regency_id)
    {
        return detailWisata::where('kabupaten_id', $regency_id)
            ->limit(3)->inRandomOrder()->get()->toArray();
    }

    public function show()
    {
        $provinsi = Province::all();

        $activity = $this->getProduct('activity');
        $tur = $this->getProduct('tour');
        $transfer = $this->getProduct('Transfer');
        $rental = $this->getProduct('rental');
        $hotel = $this->getProduct('hotel');
        $xstay = $this->getProduct('xstay');

        $pulau = 'Pulau';
        $objeks = detailObjek::where('objek', $pulau)->where('sort', 'active')->get();
        $objek1 = isset($objeks[0]) ? $objeks[0]  : null;
        $objek2 = isset($objeks[1]) ? $objeks[1] : null;
        $objek3 = isset($objeks[2]) ? $objeks[2] : null;
        $objek4 = isset($objeks[3]) ? $objeks[3] : null;
        $objek5 = isset($objeks[4]) ? $objeks[4] : null;
        $objek6 = isset($objeks[5]) ? $objeks[5] : null;
        //dd($objek6);
        $sumatraSVG = detailWisata::where('pulau_id', 1)->limit(1)->latest()->get();
        $jawaSVG = detailWisata::where('pulau_id', 2)->limit(1)->latest()->get();
        $kalimantanSVG = detailWisata::where('pulau_id', 3)->limit(1)->latest()->get();
        $papuaSVG = detailWisata::where('pulau_id', 4)->limit(1)->latest()->get();
        $sulawesiSVG = detailWisata::where('pulau_id', 5)->limit(1)->latest()->get();
        $malukuSVG = detailWisata::where('pulau_id', 6)->limit(1)->latest()->get();
        $nusaSVG = detailWisata::where('pulau_id', 7)->limit(1)->latest()->get();
        // dd($sumatraSVG);
        $news = newsletter::where('article_section', 'iklan')->limit(5)->latest()->get();

        $settingGeneral = settingGeneral::select('regency_order')->find(99)->toArray();
        $settingGeneral = json_decode($settingGeneral['regency_order']);

        $regencyName = [];
        foreach ($settingGeneral as $data) {
            $name = Regency::where('id', $data)->first();
            // dump($name);
            if (isset($name->name)) {
                $regencyName[] = [
                    isset($name->name) ? $name->name : null,
                ];
            }
        }
        // die;
        $product_tur_0 = $this->productTour($settingGeneral[0]);
        $product_wisata_0 = $this->productWisata($settingGeneral[0]);
        $product_tur_wisata_0 = array_merge($product_tur_0, $product_wisata_0);

        $product_tur_1 = $this->productTour($settingGeneral[1]);
        $product_wisata_1 = $this->productWisata($settingGeneral[1]);
        $product_tur_wisata_1 = array_merge($product_tur_1, $product_wisata_1);

        $product_tur_2 = $this->productTour($settingGeneral[2]);
        $product_wisata_2 = $this->productWisata($settingGeneral[2]);
        $product_tur_wisata_2 = array_merge($product_tur_2, $product_wisata_2);

        $product_tur_3 = $this->productTour($settingGeneral[3]);
        $product_wisata_3 = $this->productWisata($settingGeneral[3]);
        $product_tur_wisata_3 = array_merge($product_tur_3, $product_wisata_3);

        $product_tur_4 = $this->productTour($settingGeneral[4]);
        $product_wisata_4 = $this->productWisata($settingGeneral[4]);
        $product_tur_wisata_4 = array_merge($product_tur_4, $product_wisata_4);

        $product_tur_5 = $this->productTour($settingGeneral[5]);
        $product_wisata_5 = $this->productWisata($settingGeneral[5]);
        $product_tur_wisata_5 = array_merge($product_tur_5, $product_wisata_5);
        // die;
        return view('Destindonesia.index', compact(
            'activity',
            'provinsi',
            'tur',
            'transfer',
            'rental',
            'hotel',
            'xstay',
            'objek1',
            'objek2',
            'objek3',
            'objek4',
            'objek5',
            'objek6',
            'news',
            'sumatraSVG',
            'jawaSVG',
            'kalimantanSVG',
            'papuaSVG',
            'sulawesiSVG',
            'malukuSVG',
            'nusaSVG',
            'regencyName',
            'product_tur_wisata_0',
            'product_tur_wisata_1',
            'product_tur_wisata_2',
            'product_tur_wisata_3',
            'product_tur_wisata_4',
            'product_tur_wisata_5'
        ));
    }

    public function listDesti()
    {

        $wisata = DB::table('detail_wisatas')->limit(6)->latest()->get();

        $pulauJawa = Province::where('id', '=', '31')
            ->orWhere('id', '=', '32')
            ->orWhere('id', '=', '33')
            ->orWhere('id', '=', '34')
            ->orWhere('id', '=', '35')
            ->orWhere('id', '=', '36')
            ->select('name', 'id')
            ->get();

        $pulauSumatra = Province::where('id', '=', '11')
            ->orWhere('id', '=', '12')
            ->orWhere('id', '=', '13')
            ->orWhere('id', '=', '14')
            ->orWhere('id', '=', '15')
            ->orWhere('id', '=', '16')
            ->orWhere('id', '=', '17')
            ->orWhere('id', '=', '18')
            ->select('name', 'id')
            ->get();

        $pulauKalimantan = Province::where('id', '=', '61')
            ->orWhere('id', '=', '62')
            ->orWhere('id', '=', '63')
            ->orWhere('id', '=', '64')
            ->orWhere('id', '=', '65')
            ->orWhere('id', '=', '66')
            ->select('name', 'id')
            ->get();

        $kepulauan = Province::where('id', '=', '19')
            ->orWhere('id', '=', '21')
            ->orWhere('id', '=', '51')
            ->select('name', 'id')
            ->get();

        $pulauSulawesi = Province::where('id', '=', '71')
            ->orWhere('id', '=', '72')
            ->orWhere('id', '=', '73')
            ->orWhere('id', '=', '74')
            ->orWhere('id', '=', '75')
            ->orWhere('id', '=', '76')
            ->select('name', 'id')
            ->get();

        $maluku = Province::where('id', '=', '81')
            ->orWhere('id', '=', '82')
            ->select('name', 'id')
            ->get();


        $pulauPapua = Province::where('id', '=', '91')
            ->orWhere('id', '=', '94')
            ->select('name', 'id')
            ->get();

        $sumatraSVG = detailWisata::where('pulau_id', 1)->limit(1)->latest()->get();
        $jawaSVG = detailWisata::where('pulau_id', 2)->limit(1)->latest()->get();
        $kalimantanSVG = detailWisata::where('pulau_id', 3)->limit(1)->latest()->get();
        $papuaSVG = detailWisata::where('pulau_id', 4)->limit(1)->latest()->get();
        $sulawesiSVG = detailWisata::where('pulau_id', 5)->limit(1)->latest()->get();
        $malukuSVG = detailWisata::where('pulau_id', 6)->limit(1)->latest()->get();
        $nusaSVG = detailWisata::where('pulau_id', 7)->limit(1)->latest()->get();

        $news = newsletter::where('article_section', 'iklan')->limit(5)->latest()->get();

        return view('Destindonesia.list-destindonesia', compact(
            'wisata',
            'pulauSumatra',
            'pulauJawa',
            'pulauKalimantan',
            'pulauPapua',
            'pulauSulawesi',
            'maluku',
            'kepulauan',
            'sumatraSVG',
            'jawaSVG',
            'kalimantanSVG',
            'papuaSVG',
            'sulawesiSVG',
            'malukuSVG',
            'nusaSVG',
            'news'
        ));
    }

    public function provDestindonesia($id)
    {
        // $objeks = detailWisata::where('provinsi_id', '=', $id)->paginate(12);
        $news = newsletter::where('article_section', 'iklan')->limit(5)->latest()->get();
        
        $categories = detailWisata::select('kategori')->groupBy('kategori')->where('provinsi_id','=',$id)->get();
        $collect = collect(['All']);
        
        $list_kota = Regency::where('province_id','=',$id)->get();
        $collect_list = collect([]);

        foreach($categories as $category){
            $collect->push($category->kategori);
        }

        foreach($list_kota as $list){
            $collect_list->push($list);
        }
        // dd($collect);
        return view('Destindonesia.list-objek', compact('news', 'list_kota', 'collect', 'collect_list','categories','id'));
    }

    public function showById($id)
    {

        $pulau =  DB::table('pulau')
            ->where('id', $id)
            ->select('island_name', 'id')
            ->first();
        // $objeks = detailObjek::where('pulau_id', '=', $id)->where('nama_pulau',$pulau->island_name)->limit(1)->first();
        $objeks = detailObjek::join('pulau','pulau.id','=','detail_objeks.pulau_id')->where('pulau_id', '=', $id)->whereNull('provinsi_id')->whereNull('kabupaten_id')->limit(1)->first();
        $news = newsletter::where('article_section', 'iklan')->limit(5)->latest()->get();
        //dd($objeks);
        
        if ($pulau->island_name === 'Pulau Jawa') {

            $map = 'storage/img/jawa.png';

            $provinces = Province::where('id', '=', '31')
                ->orWhere('id', '=', '32')
                ->orWhere('id', '=', '33')
                ->orWhere('id', '=', '34')
                ->orWhere('id', '=', '35')
                ->orWhere('id', '=', '36')
                ->select('name', 'id')
                ->get();
        }

        if ($pulau->island_name === 'Pulau Sumatra') {

            $map = 'storage/img/sumatra.png';

            $provinces = Province::where('id', '=', '11')
                ->orWhere('id', '=', '12')
                ->orWhere('id', '=', '13')
                ->orWhere('id', '=', '14')
                ->orWhere('id', '=', '15')
                ->orWhere('id', '=', '16')
                ->orWhere('id', '=', '17')
                ->orWhere('id', '=', '18')
                ->select('name', 'id')
                ->get();
        }

        if ($pulau->island_name === 'Pulau Kalimantan') {

            $map = 'storage/img/kalimantan.png';

            $provinces = Province::where('id', '=', '61')
                ->orWhere('id', '=', '62')
                ->orWhere('id', '=', '63')
                ->orWhere('id', '=', '64')
                ->orWhere('id', '=', '65')
                ->orWhere('id', '=', '66')
                ->select('name', 'id')
                ->get();
        }

        if ($pulau->island_name === 'Pulau Papua') {

            $map = 'storage/img/papua.png';

            $provinces = Province::where('id', '=', '91')
                ->orWhere('id', '=', '94')
                ->select('name', 'id')
                ->get();
        }

        if ($pulau->island_name === 'Pulau Sulawesi') {

            $map = 'storage/img/sulawesi.png';

            $provinces = Province::where('id', '=', '71')
                ->orWhere('id', '=', '72')
                ->orWhere('id', '=', '73')
                ->orWhere('id', '=', '74')
                ->orWhere('id', '=', '75')
                ->orWhere('id', '=', '76')
                ->select('name', 'id')
                ->get();
        }

        if ($pulau->island_name === 'Pulau Maluku') {

            $map = 'storage/img/papua.png';

            $provinces = Province::where('id', '=', '81')
                ->orWhere('id', '=', '82')
                ->select('name', 'id')
                ->get();
        }

        if ($pulau->island_name === 'Pulau Bali dan Nusa Tenggara') {

            $map = 'storage/img/nusa.png';

            $provinces = Province::where('id', '=', '19')
                ->orWhere('id', '=', '21')
                ->orWhere('id', '=', '51')
                ->select('name', 'id')
                ->get();
        }

        if ($objeks) {
            // return view('Destindonesia.list-pulau',compact('news','pulau','provinces','map'));
            return redirect()->route('pulauShow', $objeks->slug);
        } else {
            return view('Destindonesia.island-not-found');
        }
    }

    public function filterByRegency(Request $request, $id){

        // dd($request->regency_id);
        $idArr = $request->regency_id;

        $objeks = detailWisata::where(function($q) use ($idArr){
            if($idArr){
                foreach($idArr as $id){
                    $q->orWhere('kabupaten_id',$id['id']);
                }
            }
            
        })
        ->where('provinsi_id', '=', $id)
        ->paginate(12);
        
        return View::make('Destindonesia.card-list-object-dest', compact('objeks'))->render();   
    }

    public function filterByListCategory(Request $request, $id){
        
        $idArr = $request->regency_id != null ? json_decode($request->regency_id,true):null;
        $category = $request->category;
        if($category!='All' && $category !=null){
            $objeks = detailWisata::where('provinsi_id', '=', $id)
            ->where('kategori',$category)
            ->where(function($q) use ($idArr){
                if($idArr){
                    foreach($idArr as $id){
                        $q->orWhere('kabupaten_id',$id['id']);
                    }
                }
            })
            ->get();

        }else{
            $objeks = detailWisata::where('provinsi_id', '=', $id)
            ->where(function($q) use ($idArr){
                if($idArr){
                    foreach($idArr as $id){
                        $q->orWhere('kabupaten_id',$id['id']);
                    }
                }
            })
            ->get();
        }
        
        $collection = collect([]);

        if($objeks){
            foreach($objeks as $key => $objek){
                // dump(Storage::url($objek->header));
                $collection->push([
                    'id'=>$objek->id,
                    'title'=>$objek->title,
                    'slug'=>$objek->slug,
                    'namaWisata'=>$objek->namaWisata,
                    'thumbnail'=>$objek->thumbnail,
                    'deskripsi'=>$objek->deskripsi,
                    'kategori'=>$objek->kategori,
                    'hari'=>$objek->hari,
                    'hargaTiket'=>$objek->hargaTiket,
                    'jamOperasional'=>$objek->jamOperasional,
                    'transportasi'=>$objek->transportasi,
                    'gallery'=>$objek->galleryWisata,
                    'embed_maps'=>$objek->embed_maps,
                    'header'=>Storage::url($objek->header),
                ]);
            }
        }

        $per_page = 12;
        $total = count($objeks);
        $curr_page = $request->input('page') ?? 1;
        
        $start_page = ($curr_page * $per_page) - $per_page;
        
        $objeks = $collection->toArray();
        $objeks = array_slice($objeks, $start_page,$per_page,true);
        $objeks = new Paginator($objeks, $total, $per_page,$curr_page,[
            'page'=>$request->url(),
            'page'=>$request->query()
        ]);

        
        if($objeks->count() > 0){
            $success = true;
        }else{
            $success = false;
        }

        return response()->json([
            'success'=>$success,
            'datas'=>$objeks,
            'status'=>'success'
        ]);
     }
    
    public function searchDestindo(Request $request,$opt=null){

        $search = $request->cari;
        $result_search = collect([]);
        
        $articles=newsletter::all();

        return view('Destindonesia.search-destindo',compact('articles','search'));       
    }

    public function seacrhResult(Request $request){

        $search = $request->cari;
        $result_search = collect([]);

        $newsletter = newsletter::where('description','like','%'.$search.'%')
                      ->orWhere('title','like','%'.$search.'%')
                      ->get();
                      
        if($newsletter->count() > 0){
            foreach($newsletter as $artikel){
                $result_search->push([
                    'judul'=>$artikel->title,
                    'link'=>url($artikel->slug),
                    'tgl_terbit'=> Carbon::parse($artikel->created_at)->toFormattedDateString(),
                    'isi'=>$artikel->description
                ]);
            }
        }
        
        //check lokasi
        $detailObj = detailObjek::where('deskripsi','like','%'.$search.'%')
                     ->orWhere('nama_pulau','like','%'.$search.'%')
                     ->orWhere('title','like','%'.$search.'%')
                     ->get();

        if($detailObj->count() > 0){
            foreach($detailObj as $lokasi){
             
                if($lokasi->objek == 'pulau'){
                    $result_search->push([
                        'judul'=>$lokasi->title,
                        'link'=>route('pulauShow',$lokasi->slug),
                        'tgl_terbit'=>Carbon::parse($lokasi->created_at)->toFormattedDateString(),
                        'isi'=>$lokasi->deskripsi
                    ]);
                }

                if($lokasi->objek == 'Provinsi'){
                    $result_search->push([
                        'judul'=>$lokasi->title,
                        'link'=>route('provinsiShow',$lokasi->slug),
                        'tgl_terbit'=>Carbon::parse($lokasi->created_at)->toFormattedDateString(),
                        'isi'=>$lokasi->deskripsi
                    ]);
                }

                if($lokasi->objek == 'Kabupaten'){
                    $result_search->push([
                        'judul'=>$lokasi->title,
                        'link'=>route('kabupatenShow',$lokasi->slug),
                        'tgl_terbit'=>Carbon::parse($lokasi->created_at)->toFormattedDateString(),
                        'isi'=>$lokasi->deskripsi
                    ]);
                }
            }
        }
        
        //check wisata
        $detailWisata = detailWisata::where('deskripsi','like','%'.$search.'%')
                        ->orWhere('namaWisata','like','%'.$search.'%')
                        ->orWhere('title','like','%'.$search.'%')
                        ->get();

        if($detailWisata->count() > 0){
            foreach($detailWisata as $wisata){
                $result_search->push([
                    'judul'=>$wisata->title,
                    'link'=>route('wisataShow',$wisata->slug),
                    'tgl_terbit'=>Carbon::parse($wisata->created_at)->toFormattedDateString(),
                    'isi'=>$wisata->deskripsi
                ]);
            }
        }

        // manual paginate
        $per_page = 10;
        $total = count($result_search);
        $curr_page = $request->input('cur_page') ?? 1;
        
        $start_page = ($curr_page * $per_page) - $per_page;
        
        $result_search = $result_search->toArray();
        $result_search = array_slice($result_search, $start_page,$per_page,true);
        $result_search = new Paginator($result_search, $total, $per_page,$curr_page,[
            'page'=>$request->url(),
            'page'=>$request->query()
        ]);
        
        return response()->json($result_search);  
    }
}
