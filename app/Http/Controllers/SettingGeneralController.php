<?php

namespace App\Http\Controllers;

use Thumbnail;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Extra;
use App\Models\Product;
use App\Models\Regency;
use App\Models\District;
use App\Models\Province;
use App\Models\newsletter;
use App\Models\reviewPost;
use App\Models\Userdetail;
use App\Models\detailObjek;
use App\Models\LikeProduct;
use App\Models\Masterroute;
use App\Models\detailWisata;
use App\Models\MasterChoice;
use Illuminate\Http\Request;
use App\Models\Productdetail;
use App\Models\Usertraveller;
use App\Models\settingGeneral;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class SettingGeneralController extends Controller
{
    //    FAQs
    public function addDynamics()
    {
        $faqs = settingGeneral::select('title_section')->get();
        $faqs2 = settingGeneral::select('isi_section')->get();

        $tests = json_decode($faqs);
        $titles = json_decode($faqs);
        $tests2 = json_decode($faqs2);
        $isi = json_decode($faqs2);
        foreach ($tests as $value) {
            foreach ($tests2 as $item) {
                $titles = $value->title_section;
                $isi = $item->isi_section;
                // dd($titles);
                // return view('BE.admin.faqs', compact( 'titles','isi'));
            }
        }

        return view('BE.admin.faqs', compact('titles', 'isi'));
    }

    public function storeDynamics(Request $request)
    {
        $request->validate([
            'section' => 'required',
            'addMoreInputFields1.*.title_section' => 'required',
            'addMoreInputFields2.*.isi_section' => 'required',
        ], [
            'section.required' => 'Section tidak boleh kosong',
            'addMoreInputFields1.*.title_section.required' => 'Fields 1 tidak boleh kosong',
            'addMoreInputFields2.*.isi_section.required' => 'Fields 2 tidak boleh kosong',
        ]);


        foreach ($c1 = $request->addMoreInputFields1 as $key => $value) {
            foreach ($c2 = $request->addMoreInputFields2 as $key => $item) {
                $data['section'] = $request->section;
                $data['title_section'] = $c1;
                $data['isi_section'] = $c2;
            }
        }

        settingGeneral::create($data);


        return back()->with('success', 'New subject has been added.');
    }

    //    Objek
    public function objekDestindonesia(Request $request)
    {

        $filter = $request->objek;
        $search = $request->title;

        $pulauDestindonesias = null;

        // if($filter){
        //     $pulauDestindonesias = detailObjek::where('objek','=',$filter)->paginate(5);
        // }
        // else if($search){
        //     //   $query->where('product_name', 'like','%'.$search.'%');
        //     $pulauDestindonesias = detailObjek::where('title','like','%'.$search.'%')
        //     ->orWhere('title','like','%'.ucfirst($search).'%')
        //     ->orWhere('title','like','%'.strtolower($search).'%')
        //     ->orWhere('title','like','%'.strtoupper($search).'%')
        //     ->orWhere('title','like','%'.ucwords($search).'%')
        //     ->paginate(5);
        // }
        // else{
        // }
        $pulauDestindonesias = detailObjek::get();

        // dd($pulauDestindonesias);
        // $pulauDestindonesias = detailObjek::paginate(5);
        //        $provinsiDestindonesias = detailObjek::where([
        //            ['objek', '=', 'Provinsi']])->paginate(2);
        //        $regencyDestindonesias = detailObjek::where([
        //            ['objek', '=', 'Kabupaten']])->paginate(2);

        $pulaus = DB::select('select * from pulau');
        $provinsis = DB::select('select * from provinces');
        $kabupatens = DB::select('select * from regencies');

        return view('BE.admin.Destindonesia.Place.Objek.index', compact(
            'pulauDestindonesias',
            'pulaus',
            'provinsis',
            'kabupatens'
        ));
    }

    //    store objek
    public function objekStore(Request $request)
    {
        // dd($request->all());
        //        validasi
        $this->validate($request, [
            'title' => 'required',
            'objek' => 'required',
            'nama_pulau' => 'required',
            // 'thumbnail' => 'required',
            // 'header' => 'required',
            // 'foto_menteri' => 'required',
            // 'desk_thumbnail' => 'required',
            'nama_menteri' => 'required',
            'kata_sambutan' => 'required',
            'deskripsi' => 'required',
            'bandara' => 'required',
            'ibukota' => 'required',
            'terminal' => 'required',
            'pelabuhan' => 'required',
            'transportasi' => 'required',
            'embed_maps' => 'required',
        ]);


        //        Thumbnail
        if ($request->file('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $input['thumbnail'] = time() . '.' . $thumbnail->extension();
            $destinationPath = ('public' . '/' . $input['thumbnail']);
            $img = Thumbnail::make($thumbnail->path());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save(storage_path('app/public' . '/' . $input['thumbnail']));
        }

        //        foto menteri
        if ($request->foto_menteri) {
            $nameFile = time() . $request->foto_menteri->getClientOriginalName();
            $path = $request->file('foto_menteri')->storeAs('public/images/objek', $nameFile);
        }


        // header
        if ($request->header) {
            $nameFile = time() . $request->header->getClientOriginalName();
            $path2 = $request->file('header')->storeAs('public/images/objek', $nameFile);
        }


        //        logo daerah
        if ($request->logo_daerah) {
            $nameFile = time() . $request->logo_daerah->getClientOriginalName();
            $path3 = $request->file('logo_daerah')->storeAs('public/images/objek', $nameFile);
        }

        //        kata sambutan
        $description = $request->kata_sambutan;
        $dom = new \DomDocument();
        $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $imageFile = $dom->getElementsByTagName('imageFile');

        foreach ($imageFile as $item => $image) {
            $data = $img->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $imgeData = base64_decode($data);
            $image_name = "/upload/" . time() . $item . '.png';
            $path = public_path() . $image_name;
            file_put_contents($path, $imgeData);
            $image->removeAttribute('src');
            $image->setAttribute('src', $image_name);
        }

        //        description
        $description2 = $request->deskripsi;
        $dom2 = new \DomDocument();
        $dom2->loadHtml($description2, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $imageFile = $dom2->getElementsByTagName('imageFile');

        foreach ($imageFile as $item => $image) {
            $data = $img->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $imgeData = base64_decode($data);
            $image_name = "/upload/" . time() . $item . '.png';
            $path = public_path() . $image_name;
            file_put_contents($path, $imgeData);
            $image->removeAttribute('src');
            $image->setAttribute('src', $image_name);
        }

        // thumbnail description
        if ($request->desk_thumbnail) {

            $desk_thumbnail = $request->desk_thumbnail;
            $dom3 = new \DomDocument();
            @$dom3->loadHtml($desk_thumbnail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $imageFile = $dom3->getElementsByTagName('imageFile');

            foreach ($imageFile as $item => $image) {
                $data = $img->getAttribute('src');
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $imgeData = base64_decode($data);
                $image_name = "/upload/" . time() . $item . '.png';
                $path = public_path() . $image_name;
                file_put_contents($path, $imgeData);
                $image->removeAttribute('src');
                $image->setAttribute('src', $image_name);
            }
        }

        // save deskripsi
        $description2  = $dom2->saveHTML();
        // save sambutan
        $description   = $dom->saveHTML();
        // desk_thumbnail
        $desk_thumbnail = $request->desk_thumbnail != null ? $dom3->saveHTML() : null;

        // input new detailObjek
        $fileUpload = new detailObjek;

        //gallery
        $images = [];
        if ($request->images) {
            foreach ($request->images as $key => $image) {
                $imageName = time() . rand(1, 99) . '.' . $image->extension();
                $image->move(storage_path('app/public/gallery'), $imageName);
                $images[]['image'] = $imageName;
            }
        }

        //        foreach ($images as $key => $image) {
        $fileUpload->galleryObjek = collect($images);
        //            dd($fileUpload, $tags, $request);

        $objek = $request->objek;

        if ($request->kabupaten_id) {
            $regency = Regency::where('id', $request->kabupaten_id)->where('name', 'like', '%kota%')->first();

            if ($regency) {
                $objek = 'Kota';
            }
        }

        // dd($objek);

        //        request by form + by variabel
        $fileUpload->nama_pulau = $request->nama_pulau;
        $fileUpload->provinsi_id = $request->provinsi_id;
        $fileUpload->kabupaten_id = $request->kabupaten_id;
        $fileUpload->pulau_id = $request->pulau_id;
        $fileUpload->title = $request->title;
        $fileUpload->objek = $request->objek;
        $fileUpload->nama_menteri = $request->nama_menteri;
        $fileUpload->kata_sambutan = $description;
        $fileUpload->ibukota = $request->ibukota;
        $fileUpload->bandara = $request->bandara;
        $fileUpload->terminal = $request->terminal;
        $fileUpload->pelabuhan = $request->pelabuhan;
        $fileUpload->transportasi = $request->transportasi;
        $fileUpload->deskripsi = $description2;
        $fileUpload->desk_thumbnail = $request->desk_thumbnail != null ? $description2 : null;
        $fileUpload->foto_menteri = $request->foto_menteri != null ? $path : null;
        $fileUpload->header = $request->header != null ? $path2 : null;
        $fileUpload->logo_daerah = $request->logo_daerah != null ? $path3 : null;
        $fileUpload->embed_maps = $request->embed_maps;
        $fileUpload->thumbnail = $request->thumbnail != null ? $destinationPath : null;

        //    dd($fileUpload);

        $fileUpload->save();

        //        tagable
        $tags = explode(",", $request->tags);

        //        $select2['select2'] = implode(',', $request->select2);
        $fileUpload->tag($tags);
        return redirect()->route('objek');

        //        }
    }

    //    edit objek
    public function objekEdit(Request $request, $id)
    {
        $pulauDestindonesias = detailObjek::findOrFail($id);
        // dd(json_decode($pulauDestindonesias));
        $tags = implode(",", $pulauDestindonesias['tag_names']);
        //        dd($tags);
        $request->session()->put('objek.old_image', $pulauDestindonesias->galleryObjek);
        $pulaus = DB::select('select * from pulau');
        $provinsis = DB::select('select * from provinces');
        $kabupatens = DB::select('select * from regencies');

        return view('BE.admin.Destindonesia.Place.Objek.edit', compact(
            'pulauDestindonesias',
            'pulaus',
            'provinsis',
            'kabupatens',
            'tags'
        ));
    }

    //    update objek
    public function objekUpdate(detailObjek $detailObjek, Request $request, $id)
    {
        //dd($request->slug);
        $this->validate($request, [
            'objek' => 'required',
            'nama_pulau' => 'required',
            // 'thumbnail' => 'required',
            // 'header' => 'required',
            // 'foto_menteri' => 'required',
            // 'desk_thumbnail' => 'required',
            'nama_menteri' => 'required',
            'kata_sambutan' => 'required',
            'deskripsi' => 'required',
            'bandara' => 'required',
            'ibukota' => 'required',
            'terminal' => 'required',
            'pelabuhan' => 'required',
            'transportasi' => 'required',
            'embed_maps' => 'required',
        ]);

        $detailObjek = detailObjek::findOrFail($id);
        // dd($detailObjek);

        $session_gallery = $request->session()->get('objek.gallery');
        $new_image = $request->session()->get('objek.new_image');
        $old_image = $request->session()->get('objek.old_image');
        // dd($old_image);
        // IF there's no image on session, return
        if (!isset($session_gallery) && !isset($old_image)) {
            return back()->withErrors(['image_gallery' => 'Gallery wajib diisi!'])->withInput();
        }

        //        kata sambutan
        $description = $request->kata_sambutan;
        $dom = new \DomDocument();
        @$dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $imageFile = $dom->getElementsByTagName('imageFile');
        foreach ($imageFile as $item => $image) {
            $data = $image->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $imgeData = base64_decode($data);
            $image_name = "/upload/" . time() . $item . '.png';
            $path = public_path() . $image_name;
            file_put_contents($path, $imgeData);
            $image->removeAttribute('src');
            $image->setAttribute('src', $image_name);
        }
        $description = $dom->saveHTML();

        //        deskripsi
        $description2 = $request->deskripsi;
        $dom = new \DomDocument();
        @$dom->loadHtml($description2, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $imageFile = $dom->getElementsByTagName('imageFile');
        foreach ($imageFile as $item => $image) {
            $data = $image->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $imgeData = base64_decode($data);
            $image_name = "/upload/" . time() . $item . '.png';
            $path = public_path() . $image_name;
            file_put_contents($path, $imgeData);
            $image->removeAttribute('src');
            $image->setAttribute('src', $image_name);
        }
        $description2 = $dom->saveHTML();

        // thumbnail description
        $desk_thumbnail = $request->desk_thumbnail;

        if ($desk_thumbnail) {

            $dom3 = new \DomDocument();
            @$dom3->loadHtml($desk_thumbnail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $imageFile = $dom3->getElementsByTagName('imageFile');

            foreach ($imageFile as $item => $image) {
                $data = $img->getAttribute('src');
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $imgeData = base64_decode($data);
                $image_name = "/upload/" . time() . $item . '.png';
                $path = public_path() . $image_name;
                file_put_contents($path, $imgeData);
                $image->removeAttribute('src');
                $image->setAttribute('src', $image_name);
            }
            $desk_thumbnail = $dom3->saveHTML();
        }

        $objek = $request->objek;

        if ($request->select2_kabupaten_id) {
            $regency = Regency::where('id', $request->select2_kabupaten_id)->where('name', 'like', '%kota%')->first();

            if ($regency) {
                $objek = 'Kota';
            }
        }
        // dd($objek);

        
        // echo($request->slug);
        // die;
        $detailObjek->kata_sambutan = $description;
        $detailObjek->deskripsi = $description2;
        $detailObjek->desk_thumbnail = isset($desk_thumbnail) ? $desk_thumbnail : null;
        $detailObjek->provinsi_id = $request->provinsi_id ? $request->provinsi_id : $request->select2_provinsi_id;
        $detailObjek->nama_pulau = $request->nama_pulau;
        $detailObjek->kabupaten_id = $request->kabupaten_id ? $request->kabupaten_id : $request->select2_kabupaten_id;
        $detailObjek->pulau_id = $request->pulau_id != null ? $request->pulau_id : $request->pulau_id_select2;
        $detailObjek->title = $request->title;
        $detailObjek->objek = $objek;
        $detailObjek->nama_menteri = $request->nama_menteri;
        $detailObjek->ibukota = $request->ibukota;
        $detailObjek->bandara = $request->bandara;
        $detailObjek->terminal = $request->terminal;
        $detailObjek->pelabuhan = $request->pelabuhan;
        $detailObjek->transportasi = $request->transportasi;
        $detailObjek->embed_maps = $request->embed_maps;
        $detailObjek->sort = $request->sort;
        $tags = explode(",", $request->tags);

        // Thumbnail
        if ($request->hasfile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $input['thumbnail'] = time() . '.' . $thumbnail->extension();
            $destinationPath = ('public' . '/' . $input['thumbnail']);
            if (File::exists($destinationPath)) {
                File::delete($destinationPath);
            }
            $img = Thumbnail::make($thumbnail->path());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save(storage_path('app/public' . '/' . $input['thumbnail']));
            $detailObjek->thumbnail = $destinationPath;
        }

        //        header
        if ($request->hasfile('header')) {
            $destination = 'storage/' . $detailObjek->header;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $file = $request->file('header');
            $extention = $file->getClientOriginalExtension();
            $filename = 'public/images/objek/' . time() . '.' . $extention;
            $file->move('storage/images/objek', $filename);
            $detailObjek->header = $filename;
        }

        //        foto menteri
        if ($request->hasfile('foto_menteri')) {
            $destination = 'storage/' . $detailObjek->foto_menteri;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $file = $request->file('foto_menteri');
            $extention = $file->getClientOriginalExtension();
            $filename = 'public/images/objek/' . time() . '.' . $extention;
            $file->move('storage/images/objek', $filename);
            $detailObjek->foto_menteri = $filename;
        }

        //        logo daerah
        if ($request->hasfile('logo_daerah')) {
            $destination = 'storage/' . $detailObjek->logo_daerah;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $file = $request->file('logo_daerah');
            $extention = $file->getClientOriginalExtension();
            $filename = 'public/images/objek/' . time() . '.' . $extention;
            $file->move('storage/images/objek', $filename);
            $detailObjek->logo_daerah = $filename;
        }
        // Gallery kamar
        // Default if gallery kamar doesn't change
        $gallery_kamar = $detailObjek->galleryObjek;

        // If gallery has change
        if (isset($session_gallery)) {
            $gallery_kamar = collect($session_gallery);
        }

        // dd($gallery_kamar);
        //gallery
        // $images = [];
        // if ($request->images) {
        //     foreach ($request->images as $key => $image) {
        //         $pathWisata = "public/gallery/";
        //         $imageName = time() . rand(1, 99) . '.' . $image->extension();
        //         $image->move(storage_path('app/public/gallery'), $imageName);
        //         $images[]['image'] = $pathWisata . $imageName;
        //     }
        // }


        // Delete some saved gallery iamges
        $old_image = $request->session()->get('objek.old_image');
        // dd($old_image, $new_image, $session_gallery);
        if (isset($detailObjek->galleryObjek)) {
            $gallery = json_decode($detailObjek->galleryObjek, true);
            $gallery_array = [];
            $old_image_array = [];

            if ($new_image) {
                foreach ($new_image as $key => $value) {
                    // dump($value);
                    $gallery_array[] = $value['image'];
                }
            } else {
                foreach ($gallery as $key => $value) {
                    // dump($value);
                    $gallery_array[] = $value['image'];
                }
            }
            // dump($gallery_array);
            // die;
            if ($old_image) {
                if (is_array($old_image)) {
                    foreach ($old_image as $key => $value) {
                        $old_image_array[] = $value['image'];
                    }
                } else {
                    foreach (json_decode($old_image, true) as $key => $value) {
                        //dump($value);
                        $old_image_array[] = $value['image'];
                    }
                }

                // $differences = array_diff($gallery_array, $old_image_array);
                // // dd($differences);
                // foreach ($differences as $key => $value) {
                //     // dump($value);
                //     if(isset($value)){
                //         if (\Storage::exists('public/gallery/' . $value)) {
                //             \Storage::delete('public/gallery/' . $value);
                //         }
                //     }else{
                //         if (\Storage::exists('public/gallery/' . $value['image'])) {
                //             \Storage::delete('public/gallery/' . $value['image']);
                //         }  
                //     }
                // }
            }
        }

        $detailObjek->galleryObjek = $gallery_kamar;
        $detailObjek->slug = $detailObjek->slug != $request->slug ? $request->slug.'-'.rand(1000,9999).date('dmY'):$detailObjek->slug;
        
        $detailObjek->update();
        //        $newsletter->tag($tags);
        if ($request->tags) {
            $detailObjek->retag($tags);
        } else {
            $detailObjek->untag();
        }

        $request->session()->forget('objek.new_image');
        $request->session()->forget('objek.old_image');
        $request->session()->forget('objek.gallery');
        //        $newsletter->untag($tags);
        //        $newsletter->retag($tags);
        return back();
    }

    public function gallery_objek_edit(Request $request, $id)
    {
        // Validate request
        $validator = Validator::make($request->all(), [
            // Validate when saved gallery not exist
            'images_gallery' => 'required_without_all:saved_gallery',
            'images_gallery.*' => 'mimes:jpeg,png,jpg|max:2048'
        ], [
            'images_gallery.required_without_all' => 'Foto Gallery wajib diisi!',
            'images_gallery.*.mimes' => "Foto harus memiliki ekstensi (jpg, jpeg, atau png)",
            'images_gallery.*.max' => 'Foto maksimum berukuran 2MB',
        ]);

        // Return when validate fails on one or more rule(s)
        if ($validator->fails()) {
            return array(
                'status' => 0,
                'message' => $validator->messages()->first(),
            );
        }

        // Execute these code when validate success
        $detail = detailObjek::findOrFail($id);

        $session_new_image = $request->session()->get('objek.new_image');

        $new_image = [];
        $fields = [];

        // Save all new images gallery
        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            // if ($request->images) {
            //     foreach ($request->images as $key => $image) {
            //         $imageName = time() . rand(1, 99) . '.' . $image->extension();
            //         $image->move(storage_path('app/public/gallery'), $imageName);
            //         $images[]['image'] = $imageName;
            //     }
            // }

            foreach ($image as $key => $value) {
                $new_foto = time() . rand(1, 99) . $key . '.' . $value->getClientOriginalExtension();
                $value->move(storage_path('app/public/gallery'), $new_foto);

                // $new_image->push(
                //     $images_gallery,
                // );

                $new_image[]['image'] = $new_foto;

                // Delete all previous new image that stored on Session
                if (!empty($session_new_image)) {
                    foreach ($session_new_image as $key => $value) {

                        if (file_exists($value['image'])) {
                            unlink($value['image']);
                        }
                    }
                }
            }
        }

        // Pindah saat simpan edit
        // // Delete all existing saved gallery
        // if (isset($detail->foto_kamar) && !$request->has('saved_gallery')) {
        //     foreach (json_decode($detail->foto_kamar) as $key => $value) {
        //         if (file_exists($value[0])) {
        //             unlink($value[0]);
        //         }
        //     }
        // }

        if ($request->has('saved_gallery')) {
            foreach ($request->saved_gallery as $key => $value) {
                // $fields->push(
                //     $value,
                // );
                $fields[]['image'] = $value;
            }
        }

        // Delete some saved gallery iamges
        // if (isset($detail->foto_kamar) && $request->has('saved_gallery')) {
        //     $gallery = json_decode($detail->foto_kamar, true)['result'];
        //     $differences = array_diff($gallery, $request->saved_gallery);

        //     // foreach ($differences as $key => $value) {
        //     //     if (file_exists($value)) {
        //     //         unlink($value);
        //     //     }
        //     // }

        //     // foreach ($request->saved_gallery as $key => $value) {
        //     //     $fields->push(
        //     //         $value,
        //     //     );
        //     // }
        // }

        $request->session()->put('objek.new_image', $new_image);
        $request->session()->put('objek.old_image', $fields);
        $image = array_merge($fields, $new_image);
        $request->session()->put('objek.gallery', $image);

        return array(
            'status' => 1,
            'message' => 'Foto Gallery berhasil diperbarui.',
            'data' => $image
        );
    }

    //        wisata
    public function wisataDestindonesia(Request $request)
    {
        $pulaus = DB::select('select * from pulau');
        $provinsis = DB::select('select * from provinces');
        $kabupatens = DB::select('select * from regencies');

        $wisataDestindonesias = detailWisata::where([
            ['namaWisata', '!=', Null]
        ])->get();

        return view('BE.admin.Destindonesia.Place.Wisata.index', compact('wisataDestindonesias', 'pulaus', 'provinsis', 'kabupatens'));
    }

    //    wisata store
    public function wisataStore(Request $request)
    {
        // dd($request->all());
        /* 
        $this->validate($request, [
            'title' => 'required',
            'objek' => 'required',
            'nama_pulau' => 'required',
            // 'thumbnail' => 'required',
            // 'header' => 'required',
            // 'foto_menteri' => 'required',
            // 'desk_thumbnail' => 'required',
            'nama_menteri' => 'required',
            'kata_sambutan' => 'required',
            'deskripsi' => 'required',
            'bandara' => 'required',
            'ibukota' => 'required',
            'terminal' => 'required',
            'pelabuhan' => 'required',
            'transportasi' => 'required',
            'embed_maps' => 'required',
        ]);
        */
        $this->validate($request, [
            'pulau_id' => 'required',
            'pulau_id_select2' => 'required',
            'provinsi_id' => 'required',
            'select2_provinsi_id' => 'required',
            'kabupaten_id' => 'required',
            'title' => 'required',
            'namaWisata' => 'required',
            'header' => 'required',
            'thumbnail' => 'required',
            'deskripsi' => 'required',
            'kategori' => 'required',
            'hargaTiket' => 'required',
            'hari' => 'required',
            'jamOperasional' => 'required',
            'transportasi' => 'required',
            'images' => 'required',
            'embed_maps' => 'required',
        ]);

        //        Thumbnail
        $thumbnail = $request->file('thumbnail');
        $input['thumbnail'] = time() . '.' . $thumbnail->extension();
        $destinationPath = ('public' . '/' . $input['thumbnail']);
        $img = Thumbnail::make($thumbnail->path());
        $img->resize(100, 100, function ($constraint) {
            $constraint->aspectRatio();
        })->save(storage_path('app/public' . '/' . $input['thumbnail']));

        //header
        $nameFile = time() . $request->header->getClientOriginalName();
        $path = $request->file('header')->storeAs('public/images/wisata', $nameFile);

        //textarea
        $description2 = $request->deskripsi;
        $dom2 = new \DomDocument();
        $dom2->loadHtml($description2, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $imageFile = $dom2->getElementsByTagName('imageFile');

        foreach ($imageFile as $item => $image) {
            $data = $img->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $imgeData = base64_decode($data);
            $image_name = "/upload/" . time() . $item . '.png';
            $path = public_path() . $image_name;
            file_put_contents($path, $imgeData);
            $image->removeAttribute('src');
            $image->setAttribute('src', $image_name);
        }
        $description2 = $dom2->saveHTML();

        //        save db
        $fileUpload = new detailWisata();


        //gallery
        $images = [];
        if ($request->images) {
            foreach ($request->images as $key => $image) {
                $pathWisata = "public/gallery/";
                $imageName = time() . rand(1, 99) . '.' . $image->extension();
                $image->move(storage_path('app/public/gallery'), $imageName);
                $images[]['image'] = $imageName;
            }
        }
        //        foreach ($images as $key => $image) {
        $fileUpload->galleryWisata = json_encode(collect($images));

        $fileUpload->provinsi_id = $request->provinsi_id;
        $fileUpload->kabupaten_id = $request->kabupaten_id;
        $fileUpload->pulau_id = $request->pulau_id;
        $fileUpload->namaWisata = $request->namaWisata;
        $fileUpload->title = $request->title;
        $fileUpload->kategori = $request->kategori;
        $fileUpload->hargaTiket = $request->hargaTiket;
        $fileUpload->hari = $request->hari;
        $fileUpload->jamOperasional = $request->jamOperasional;
        $fileUpload->transportasi = $request->transportasi;
        $fileUpload->header = $path;
        $fileUpload->embed_maps = $request->embed_maps;
        $fileUpload->thumbnail = $destinationPath;
        $fileUpload->deskripsi = $description2;
        $fileUpload->save();

        $tags = explode(",", $request->tags);

        //        $select2['select2'] = implode(',', $request->select2);
        $fileUpload->tag($tags);
        return redirect()->route('wisata');
        //        }
    }

    //    wisata edit
    public function wisataEdit($id)
    {


        $wisataShow = detailWisata::findOrFail($id);
        $galleries = detailWisata::findOrFail($id);
        $arrGal = json_decode($galleries->galleryWisata);

        $tags = implode(",", $wisataShow['tag_names']);

        $pulaus = DB::select('select * from pulau');
        $provinsis = DB::select('select * from provinces');
        $kabupatens = DB::select('select * from regencies');

        $galleries = collect([]);

        foreach ($arrGal as $value) {
            // dump($value);
            $galleries->push($value);
        }
        // dd($wisataShow);
        // die;
        return view('BE.admin.Destindonesia.Place.Wisata.edit', compact('galleries', 'wisataShow', 'tags', 'pulaus', 'provinsis', 'kabupatens'));
    }

    //  wisata update
    public function wisataUpdate(detailWisata $detailWisata, $id, Request $request)
    {
        // dd($request->all(),$request->session()->get('wisata.new_image'));
        $validator = Validator::make($request->all(), [
            // 'pulau_id' => 'required',
            'pulau_id_select2' => 'required',
            // 'provinsi_id' => 'required',
            'select2_provinsi_id' => 'required',
            // 'kabupaten_id' => 'required',
            // 'title' => 'required',
            'namaWisata' => 'required',
            'deskripsi' => 'required',
            'kategori' => 'required',
            'hargaTiket' => 'required|numeric',
            'hari' => 'required',
            'jamOperasional' => 'required',
            'transportasi' => 'required',
            'embed_maps' => 'required',
        ], [
            'pulau_id.required' => 'Tidak boleh kosong',
            'pulau_id_select2.required' => 'Tidak boleh kosong',
            'provinsi_id.required' => 'Tidak boleh kosong',
            'select2_provinsi_id.required' => 'Tidak boleh kosong',
            'kabupaten_id.required' => 'Tidak boleh kosong',
            'title.required' => 'Tidak boleh kosong',
            'namaWisata.required' => 'Tidak boleh kosong',
            'deskripsi.required' => 'Tidak boleh kosong',
            'kategori.required' => 'Tidak boleh kosong',
            'hargaTiket.required' => 'Tidak boleh kosong',
            'hargaTiket.numeric' => 'Hanya boleh angka',
            'hari.required' => 'Tidak boleh kosong',
            'jamOperasional.required' => 'Tidak boleh kosong',
            'transportasi.required' => 'Tidak boleh kosong',
            'embed_maps.required' => 'Tidak boleh kosong',
        ]);

        $detailWisata = $detailWisata::findOrFail($id);

        // dd($request->all());
        //        deskripsi
        $description2 = $request->deskripsi;
        $dom = new \DomDocument();
        @$dom->loadHtml($description2, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $imageFile = $dom->getElementsByTagName('imageFile');
        foreach ($imageFile as $item => $image) {
            $data = $image->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $imgeData = base64_decode($data);
            $image_name = "/upload/" . time() . $item . '.png';
            $path = public_path() . $image_name;
            file_put_contents($path, $imgeData);
            $image->removeAttribute('src');
            $image->setAttribute('src', $image_name);
        }
        $description2 = $dom->saveHTML();

        $detailWisata->deskripsi = $description2;
        $detailWisata->provinsi_id = $request->select2_provinsi_id;
        $detailWisata->embed_maps = $request->embed_maps;
        $detailWisata->namaWisata = $request->namaWisata;
        $detailWisata->kabupaten_id = $request->select2_kabupaten_id;
        $detailWisata->pulau_id = $request->pulau_id_select2;
        $detailWisata->title = $request->title;
        $detailWisata->kategori = $request->kategori;
        $detailWisata->hari = $request->hari;
        $detailWisata->hargaTiket = $request->hargaTiket;
        $detailWisata->jamOperasional = $request->jamOperasional;
        $detailWisata->transportasi = $request->transportasi;
        $tags = explode(",", $request->tags);

        //        Thumbnail
        if ($request->hasfile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $input['thumbnail'] = time() . '.' . $thumbnail->extension();
            $destinationPath = ('public' . '/' . $input['thumbnail']);
            if (File::exists($destinationPath)) {
                File::delete($destinationPath);
            }
            $img = Thumbnail::make($thumbnail->path());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save(storage_path('app/public' . '/' . $input['thumbnail']));
            $detailWisata->thumbnail = $destinationPath;
        }

        //header
        if ($request->hasfile('header')) {
            $destination = 'storage/' . $detailWisata->header;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $file = $request->file('header');
            $extention = $file->getClientOriginalExtension();
            $filename = 'public/images/wisata/' . time() . '.' . $extention;
            $file->move('storage/images/wisata', $filename);
            $detailWisata->header = $filename;
        }

        //gallery
        // $images = [];
        // if ($request->images) {
        //     foreach ($request->images as $key => $image) {
        //         $pathWisata = "public/gallery/";
        //         $imageName = time() . rand(1, 99) . '.' . $image->extension();
        //         $image->move(storage_path('app/public/gallery'), $imageName);
        //         $images[]['image'] = $pathWisata . $imageName;
        //     }
        // }


        if ($request->session()->get('wisata.new_image')) {
            $img = json_encode(collect($request->session()->get('wisata.new_image')));

            $detailWisata->galleryWisata = $img;
        }

        // if($detailWisata->slug != $request->slug){
        //     $slug = $request->slug.'-'.rand(1000,9999).date('dmY');
        // }
        
        $detailWisata->slug = $detailWisata->slug != $request->slug ? $request->slug.'-'.rand(1000,9999).date('dmY') : $request->slug;

        $detailWisata->update();
        if ($request->tags) {
            $detailWisata->retag($tags);
        } else {
            $detailWisata->untag();
        }
        return back();
    }

    public function updateGalleryWisata($id, Request $request)
    {

        // dd($request->all());
        $validator = Validator::make($request->all(), [
            // Validate when saved gallery not exist
            'images_gallery' => 'required_without_all:saved_gallery',
            'images_gallery.*' => 'mimes:jpeg,png,jpg|max:2048'
        ], [
            'images_gallery.required_without_all' => 'Foto Gallery wajib diisi!',
            'images_gallery.*.mimes' => "Foto harus memiliki ekstensi (jpg, jpeg, atau png)",
            'images_gallery.*.max' => 'Foto maksimum berukuran 2MB',
        ]);

        // Return when validate fails on one or more rule(s)
        if ($validator->fails()) {
            return array(
                'status' => 0,
                'message' => $validator->messages()->first(),
            );
        }

        // Execute these code when validate success
        $detail = detailWisata::findOrFail($id);

        $session_new_image = $request->session()->get('objek.new_image');

        $new_image = [];
        $fields = [];

        // Save all new images gallery
        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {
                $new_foto = time() . rand(1, 99) . $key . '.' . $value->getClientOriginalExtension();
                $value->move(storage_path('app/public/gallery'), $new_foto);

                $new_image[]['image'] = $new_foto;

                // Delete all previous new image that stored on Session
                if (!empty($session_new_image)) {
                    foreach ($session_new_image as $key => $value) {

                        if (file_exists($value['image'])) {
                            unlink($value['image']);
                        }
                    }
                }
            }
        }

        if ($request->has('saved_gallery')) {
            foreach ($request->saved_gallery as $key => $value) {
                // $fields->push(
                //     $value,
                // );
                $fields[]['image'] = $value;
            }
        }

        $request->session()->put('wisata.new_image', $new_image);
        $request->session()->put('wisata.old_image', $fields);
        $image = array_merge($fields, $new_image);
        $request->session()->put('wisata.gallery', $image);

        return array(
            'status' => 1,
            'message' => 'Foto Gallery berhasil diperbarui.',
            'data' => $image
        );
    }

    public function wisataDelete($id)
    {
        $wisata = detailWisata::findOrFail($id);
        $wisata->delete();

        return redirect()->route('wisata');
    }

    // get pulau 
    private function getPulau($province_id)
    {

        $pulau = "";
        // sumatra
        if ($province_id == 11 || $province_id == 12 || $province_id == 13 || $province_id == 14 || $province_id == 15 || $province_id == 16 ||  $province_id == 17 || $province_id == 18) {
            //  DB::table('pulau')
            $pulau = DB::table('pulau')->where('id', 1)->first();
        }

        // jawa
        if ($province_id == 31 || $province_id == 32 || $province_id == 33 || $province_id == 34 || $province_id == 35 || $province_id == 36) {
            $pulau = DB::table('pulau')->where('id', 2)->first();
        }

        // kalimantan
        if ($province_id == 62 || $province_id == 63 || $province_id == 64 || $province_id == 65 || $province_id == 66) {
            $pulau = DB::table('pulau')->where('id', 3)->first();
        }

        // jawa
        if ($province_id == 91 || $province_id == 94) {
            $pulau = DB::table('pulau')->where('id', 4)->first();
        }

        // jawa
        if ($province_id == 71 || $province_id == 72 || $province_id == 73 || $province_id == 74 || $province_id == 75 || $province_id == 76) {
            $pulau = DB::table('pulau')->where('id', 5)->first();
        }

        // maluku
        if ($province_id == 81 || $province_id == 82) {
            $pulau = DB::table('pulau')->where('id', 6)->first();
        }

        // bali nusa tenggara
        if ($province_id == 19 || $province_id == 21 || $province_id == 51) {
            $pulau = DB::table('pulau')->where('id', 7)->first();
        }
        //dd($province_id,$pulau);
        $objekLokasi = detailObjek::where('pulau_id', $pulau->id)->first();
        return $objekLokasi;
    }



    //    pulau
    public function pulauShow($slug, Request $request)
    {
        $pulaus = detailObjek::where('slug', $slug)->first();
        $id = DB::table('pulau')->where('id', $pulaus->pulau_id)->first();
        $provinces = detailObjek::whereRaw('substr(provinsi_id, 1, 1) = ' . $id->island_group)->limit(4)->get();

        $want = LikeProduct::where('ingin_kesini', '<>', 0)->where('objek', 'pulau')->count();
        $been = LikeProduct::where('pernah_kesini', '<>', 0)->where('objek', 'pulau')->count();

        // dd($provinces);
        $allProvinces = detailObjek::whereRaw('substr(provinsi_id, 1, 1) = ' . $id->island_group)->get();
        
        $reviews = reviewPost::leftJoin('detail_objeks', 'detail_objeks.id', '=', 'review_posts.detailObjek_id')->join('users','users.id','=','review_posts.traveller_id')->where('review_posts.status','active')->where('slug', $slug)->get();
        $counts = reviewPost::leftJoin('detail_objeks', 'detail_objeks.id', '=', 'review_posts.detailObjek_id')->join('users','users.id','=','review_posts.traveller_id')->where('review_posts.status','active')->where('slug', $slug)
            ->select('review_posts.star_rating')->get();
        $var1 = $counts->sum('star_rating');
        $var2 = $counts->count();

        // dd($allProvinces);
        // $request->session()->put('pulau.slug', $pulaus->slug);
        // $request->session()->put('pulau.nama', $pulaus->nama_pulau);

        if ($var1 == 0) {
            $ratings = 0;
            return view('Destindonesia.pulau', compact('pulaus', 'reviews', 'ratings', 'var2', 'provinces', 'allProvinces', 'id', 'want', 'been'));
        } else {
            $ratings = $var1 / $var2;
            return view('Destindonesia.pulau', compact('pulaus', 'reviews', 'ratings', 'var2', 'provinces', 'allProvinces', 'id', 'want', 'been'));
        }
    }

    public function allPulauShow($slug)
    {
        $pulaus = detailObjek::where('slug', $slug)->first();
        // User::whereRaw('SUBSTRING(column, -2,  2) = '.$value)->get();
        $id = DB::table('pulau')->where('id', $pulaus->pulau_id)->first();
        $provinces = detailObjek::whereRaw('substr(provinsi_id, 1, 1) = ' . $id->island_group)->get();
        // dd($provinces);
        // $reviews = reviewPost::leftJoin('detail_objeks', 'detail_objeks.id', '=', 'review_posts.detailObjek_id')->where('slug', $slug)->get();
        // $counts = reviewPost::leftJoin('detail_objeks', 'detail_objeks.id', '=', 'review_posts.detailObjek_id')->where('slug', $slug)
        //     ->select('review_posts.star_rating')->get();
        // $var1 = $counts->sum('star_rating');
        // $var2 = $counts->count();

        // dd($pulaus);
        return view('Destindonesia.all-pulau', compact('provinces', 'id', 'pulaus'));

        // if ($var1 == 0) {
        //     $ratings = 0;
        //     return view('Destindonesia.pulau', compact('pulaus', 'reviews', 'ratings', 'var2', 'provinces', 'id'));
        // } else {
        //     $ratings = $var1 / $var2;
        //     return view('Destindonesia.pulau', compact('pulaus', 'reviews', 'ratings', 'var2', 'provinces'. 'id'));
        // }
    }

    //    provinsi
    public function provinsiShow($slug, Request $request)
    {
        $pulaus = detailObjek::where('slug', $slug)->first();
        $id = DB::table('provinces')->where('id', $pulaus->provinsi_id)->first();
        $info_pulau = $this->getPulau($pulaus->provinsi_id);

        $want = LikeProduct::where('ingin_kesini', '<>', 0)->where('objek', 'provinsi')->count();
        $been = LikeProduct::where('pernah_kesini', '<>', 0)->where('objek', 'provinsi')->count();

        // dd($info_pulau);
        // $provinces = detailObjek::whereRaw('substr(provinsi_id, 1, 1) = ' . $id->island_group)->limit(4)->get();
        $kabupaten = detailObjek::whereRaw('substr(kabupaten_id, 1, 2) = ' . $id->id)->limit(4)->get();
        $allKabupaten = detailObjek::whereRaw('substr(kabupaten_id, 1, 2) = ' . $id->id)->get();
        
        $reviews = reviewPost::leftJoin('detail_objeks', 'detail_objeks.id', '=', 'review_posts.detailObjek_id')->join('users','users.id','=','review_posts.traveller_id')->where('review_posts.status','active')->where('slug', $slug)->get();
        $counts = reviewPost::leftJoin('detail_objeks', 'detail_objeks.id', '=', 'review_posts.detailObjek_id')->join('users','users.id','=','review_posts.traveller_id')->where('review_posts.status','active')->where('slug', $slug)
            ->select('review_posts.star_rating')->get();
        $var1 = $counts->sum('star_rating');
        $var2 = $counts->count();

        $share = \Share::page(route('provinsiShow', $slug))->facebook()->whatsapp();

        //get session from pulau 
        $pulau_slug = $info_pulau->slug;
        $pulau_nama = $info_pulau->nama_pulau;

        // create session to provinsi
        $request->session()->put('provinsi.slug', $pulaus->slug);
        $request->session()->put('provinsi.nama', $pulaus->nama_pulau);

        if ($var1 == 0) {
            $ratings = 0;
            return view('Destindonesia.provinsi', compact('pulaus', 'reviews', 'ratings', 'var2', 'id', 'kabupaten', 'allKabupaten', 'share', 'pulau_slug', 'pulau_nama', 'want', 'been'));
        } else {
            $ratings = $var1 / $var2;
            return view('Destindonesia.provinsi', compact('pulaus', 'reviews', 'ratings', 'var2', 'id', 'kabupaten', 'allKabupaten', 'share', 'pulau_slug', 'pulau_nama', 'want', 'been'));
        }
    }

    public function allProvinsiShow($slug)
    {
        $pulaus = detailObjek::where('slug', $slug)->first();
        // dd($pulaus);
        $id = DB::table('provinces')->where('id', $pulaus->provinsi_id)->first();
        $kabupaten = detailObjek::whereRaw('substr(kabupaten_id, 1, 2) = ' . $id->id)->get();

        return view('Destindonesia.all-kabupaten', compact('kabupaten', 'id', 'pulaus'));

        // if ($var1 == 0) {
        //     $ratings = 0;
        //     return view('Destindonesia.pulau', compact('pulaus', 'reviews', 'ratings', 'var2', 'provinces', 'id'));
        // } else {
        //     $ratings = $var1 / $var2;
        //     return view('Destindonesia.pulau', compact('pulaus', 'reviews', 'ratings', 'var2', 'provinces'. 'id'));
        // }
    }

    public function getLocation($tur_detail, $id)
    {
        $regency = Regency::where('id', $id)->first();

        $province = Province::where('id', $id)->first();
        $district = District::where('id', $id)->first();

        // Check if Location is Province
        if ($province) {
            return $province->name;
        }

        // Check if Location is Province
        if ($regency) {
            return $regency->name;
        }

        // Check if Location is Province
        if ($district) {
            return $district->name;
        }

        return null;
    }

    //    kabupaten
    public function kabupatenShow($slug, Request $request)
    {
        $pulaus = detailObjek::where('slug', $slug)->first();
        $reviews = reviewPost::leftJoin('detail_objeks', 'detail_objeks.id', '=', 'review_posts.detailObjek_id')->join('users','users.id','=','review_posts.traveller_id')->where('review_posts.status','active')->where('slug', $slug)->get();
        $counts = reviewPost::leftJoin('detail_objeks', 'detail_objeks.id', '=', 'review_posts.detailObjek_id')->join('users','users.id','=','review_posts.traveller_id')->where('review_posts.status','active')->where('slug', $slug)
            ->select('review_posts.star_rating')->get();
        $var1 = $counts->sum('star_rating');
        $var2 = $counts->count();
        $want = LikeProduct::where('ingin_kesini', '<>', 0)->where('objek', 'kabupaten')->count();
        $been = LikeProduct::where('pernah_kesini', '<>', 0)->where('objek', 'kabupaten')->count();

        $regencies = Regency::where('id', $pulaus->kabupaten_id)->first();
        $objekProvinsi = detailObjek::where('provinsi_id', $regencies->province_id)->first();
        $objekPulau = $this->getPulau($regencies->province_id);
       
        $pulau_slug = $objekPulau->slug;
        $pulau_nama = $objekPulau->nama_pulau;

        //session from provinsi 
        $provinsi_slug = $objekProvinsi->slug;
        $provinsi_nama = $objekProvinsi->nama_pulau;

        $kategories = detailWisata::select('kategori')->where("kabupaten_id", $pulaus->kabupaten_id)->groupBy('kategori')->get();
        $kategoriWisata = collect(['All']);
        foreach ($kategories as $key => $kategory) {
            # code...
            $kategoriWisata->push(
                $kategory->kategori
            );
        }

        // dd($kategoriWisata);
        $collectWisata = collect([]);        
        $wisata = detailWisata::where("kabupaten_id", $pulaus->kabupaten_id)->limit(4)->get();

        foreach($wisata as $key => $w){
            $collectWisata->push(
                $w
            );
        }
        
        // $tur = Product::where('type', 'tour')->has('productdetail')->with(['productdetail' => function($query) use($pulaus){
        //     $query->where('regency_id', $pulaus->kabupaten_id);}], 'productdetail.harga', 'productdetail.province', 'user')
        //     ->limit(5)->latest()->get();

        // $tur = Product::where('type', 'tour')->with(['productdetail' => function($query) use($pulaus){
        //     $query->where('regency_id', $pulaus->kabupaten_id);}, 'productdetail.harga', 'productdetail.province', 'user'])
        //     ->limit(5)->latest()->get();

        $tur = Product::with('productdetail', 'productdetail.harga', 'productdetail.regency', 'user')
            ->where(function ($q) {
                $q->where('type', 'tour');
            })
            ->whereHas('productdetail', function ($q) use ($pulaus) {
                // dump($pulaus);
                if ($pulaus) {
                    if (isset($pulaus->kabupaten_id)) {
                        $q->where('tag_location_3', $pulaus->kabupaten_id);
                    } else {

                        if (isset($pulaus['tag_location_1'])) {
                            $this->getLocation($pulaus, $pulaus['tag_location_1']);
                        } elseif (isset($pulaus['tag_location_2'])) {
                            $this->getLocation($pulaus, $pulaus['tag_location_2']);
                        } elseif (isset($pulaus['tag_location_3'])) {
                            $this->getLocation($pulaus, $pulaus['tag_location_3']);
                        } elseif (isset($pulaus['tag_location_4'])) {
                            $this->getLocation($pulaus, $pulaus['tag_location_4']);
                        }
                        elseif(isset($pulaus['new_tag_location'])){
                           $new_tag = json_decode($pulaus['new_tag_location'],true)['results'];
                        
                            foreach($new_tag as $key => $tag){
                                $this->getLocation($pulaus, $tag);
                            }
                        }
                        // $tag_location_1 = $this->getLocation($pulaus, $pulaus['tag_location_1']);
                        // $tag_location_2 = $this->getLocation($pulaus, $pulaus['tag_location_2']);
                        // $tag_location_3 = $this->getLocation($pulaus, $pulaus['tag_location_3']);
                        // $tag_location_4 = $this->getLocation($pulaus, $pulaus['tag_location_4']);
                    }
                }
            })
            ->limit(5)->latest()->get();

        // dd($tur);

        $activity = Product::with('productdetail', 'productdetail.harga', 'productdetail.regency', 'user')
            ->where(function ($q) {
                $q->where('type', 'activity');
            })
            ->whereHas('productdetail', function ($q) use ($pulaus) {
                if ($pulaus) {
                    if (isset($pulaus->kabupaten_id)) {
                        $q->where('tag_location_3', $pulaus->kabupaten_id);
                    } else {

                        if (isset($pulaus['tag_location_1'])) {
                            $this->getLocation($pulaus, $pulaus['tag_location_1']);
                        } elseif (isset($pulaus['tag_location_2'])) {
                            $this->getLocation($pulaus, $pulaus['tag_location_2']);
                        } elseif (isset($pulaus['tag_location_3'])) {
                            $this->getLocation($pulaus, $pulaus['tag_location_3']);
                        } elseif (isset($pulaus['tag_location_4'])) {
                            $this->getLocation($pulaus, $pulaus['tag_location_4']);
                        } elseif(isset($pulaus['new_tag_location'])){
                            $new_tag = json_decode($pulaus['new_tag_location'],true)['results'];
                         
                            foreach($new_tag as $key => $tag){
                                $this->getLocation($pulaus, $tag);
                            }
                        }

                        // $tag_location_1 = $this->getLocation($pulaus, $pulaus['tag_location_1']);
                        // $tag_location_2 = $this->getLocation($pulaus, $pulaus['tag_location_2']);
                        // $tag_location_3 = $this->getLocation($pulaus, $pulaus['tag_location_3']);
                        // $tag_location_4 = $this->getLocation($pulaus, $pulaus['tag_location_4']);
                    }
                }
            })
            ->limit(5)->latest()->get();


        $transfer = Product::with('productdetail', 'productdetail.harga', 'productdetail.regency', 'productdetail.detailkendaraan', 'user')
            ->where(function ($q) {
                $q->where('type', 'transfer');
            })
            ->whereHas('productdetail', function ($q) use ($pulaus) {
                if ($pulaus) {
                    $q->where('regency_id', $pulaus->kabupaten_id);
                }
            })
            ->limit(5)->latest()->get();

        // dd($transfer);
        $rental = Product::with('productdetail', 'productdetail.harga', 'productdetail.regency', 'productdetail.detailkendaraan', 'user')
            ->where(function ($q) {
                $q->where('type', 'rental');
            })
            ->whereHas('productdetail', function ($q) use ($pulaus) {
                if ($pulaus) {
                    $q->where('tag_location_3', $pulaus->kabupaten_id);
                }else{

                    if (isset($pulaus['tag_location_1'])) {
                        $this->getLocation($pulaus, $pulaus['tag_location_1']);
                    } elseif (isset($pulaus['tag_location_2'])) {
                        $this->getLocation($pulaus, $pulaus['tag_location_2']);
                    } elseif (isset($pulaus['tag_location_3'])) {
                        $this->getLocation($pulaus, $pulaus['tag_location_3']);
                    } elseif (isset($pulaus['tag_location_4'])) {
                        $this->getLocation($pulaus, $pulaus['tag_location_4']);
                    } elseif(isset($pulaus['new_tag_location'])){
                        $new_tag = json_decode($pulaus['new_tag_location'],true)['results'];
                     
                        foreach($new_tag as $key => $tag){
                            $this->getLocation($pulaus, $tag);
                        }
                    }
                }
            })
            ->limit(5)->latest()->get();


        $hotel = Product::with('productdetail', 'productdetail.harga', 'productdetail.regency', 'productdetail.masterkamar', 'user')
            ->where(function ($q) {
                $q->where('type', 'hotel');
            })
            ->whereHas('productdetail', function ($q) use ($pulaus) {
                if ($pulaus) {
                    $q->where('regency_id', $pulaus->kabupaten_id);
                }
            })
            ->limit(5)->latest()->get();

        // dd($hotel);
        $xstay = Product::with('productdetail', 'productdetail.harga', 'productdetail.regency', 'productdetail.masterkamar', 'user')
            ->where(function ($q) {
                $q->where('type', 'xstay');
            })
            ->whereHas('productdetail', function ($q) use ($pulaus) {
                if ($pulaus) {
                    $q->where('regency_id', $pulaus->kabupaten_id);
                }
            })
            ->limit(5)->latest()->get();

        $news = newsletter::where('article_section', 'iklan')->limit(5)->latest()->get();

        $regencyTur = Regency::all();

        if ($var1 == 0) {
            $ratings = 0;
            return view('Destindonesia.kota-kabupaten', compact(
                'pulaus',
                'reviews',
                'ratings',
                'var2',
                'tur',
                'transfer',
                'rental',
                'hotel',
                'xstay',
                'activity',
                'news',
                'regencyTur',
                'wisata',
                'collectWisata',
                'kategoriWisata',
                'pulau_slug',
                'pulau_nama',
                'provinsi_slug',
                'provinsi_nama',
                'want',
                'been'
            ));
        } else {
            $ratings = $var1 / $var2;
            return view('Destindonesia.kota-kabupaten', compact(
                'pulaus',
                'reviews',
                'ratings',
                'var2',
                'tur',
                'tur',
                'transfer',
                'rental',
                'hotel',
                'xstay',
                'activity',
                'news',
                'regencyTur',
                'wisata',
                'collectWisata',
                'kategoriWisata',
                'pulau_slug',
                'pulau_nama',
                'provinsi_slug',
                'provinsi_nama',
                'want',
                'been'
            ));
        }
    }

    //    wisata
    public function wisataShow($slug, Request $request)
    {
        $objek = new detailObjek();
        $pulaus = detailWisata::where('slug', $slug)->first();
        $reviews = reviewPost::leftJoin('detail_wisatas', 'detail_wisatas.id', '=', 'review_posts.detailWisata_id')->join('users', 'users.id', '=', 'review_posts.traveller_id')->where('status','active')->where('slug', $slug)->get();
        $counts = reviewPost::leftJoin('detail_wisatas', 'detail_wisatas.id', '=', 'review_posts.detailWisata_id')->join('users', 'users.id', '=', 'review_posts.traveller_id')->where('status','active')->where('slug', $slug)
            ->select('review_posts.star_rating')->get();
        $var1 = $counts->sum('star_rating');
        $var2 = $counts->count();

        $want = LikeProduct::where('ingin_kesini', '<>', 0)->where('objek', 'wisata')->count();
        $been = LikeProduct::where('pernah_kesini', '<>', 0)->where('objek', 'wisata')->count();

        $tur = Product::with('productdetail', 'productdetail.harga', 'productdetail.regency', 'user')
            ->where(function ($q) {
                $q->where('type', 'tour');
            })
            ->whereHas('productdetail', function ($q) use ($pulaus) {
                $q->where('tag_location_4', $pulaus->id)
                ->whereJsonContains('results',$pulaus->id);
            })
            ->limit(5)->latest()->get();

        $activity = Product::with('productdetail', 'productdetail.harga', 'productdetail.regency', 'user')
            ->where(function ($q) {
                $q->where('type', 'activity');
            })
            ->whereHas('productdetail', function ($q) use ($pulaus) {
                $q->where('tag_location_4', $pulaus->id)
                ->whereJsonContains('results',$pulaus->id);
            })
            ->limit(5)->latest()->get();

        $transfer = Product::with('productdetail', 'productdetail.harga', 'productdetail.detailkendaraan', 'productdetail.regency', 'user')
            ->where(function ($q) {
                $q->where('type', 'transfer');
            })
            ->whereHas('productdetail', function ($q) use ($pulaus) {
                $q->where('tag_location_4', $pulaus->id);
            })
            ->limit(5)->latest()->get();

        $rental = Product::with('productdetail', 'productdetail.harga', 'productdetail.regency', 'productdetail.detailkendaraan', 'user')
            ->where(function ($q) {
                $q->where('type', 'rental');
            })
            ->whereHas('productdetail', function ($q) use ($pulaus) {
                $q->where('tag_location_4', $pulaus->id)
                ->whereJsonContains('results',$pulaus->id);;
            })
            ->limit(5)->latest()->get();

        $hotel = Product::with('productdetail', 'productdetail.harga', 'productdetail.regency', 'productdetail.masterkamar', 'user')
            ->where(function ($q) {
                $q->where('type', 'hotel');
            })
            ->whereHas('productdetail', function ($q) use ($pulaus) {
                $q->where('regency_id', $pulaus->kabupaten_id);
            })
            ->limit(5)->latest()->get();

        $xstay = Product::with('productdetail', 'productdetail.harga', 'productdetail.regency', 'productdetail.masterkamar', 'user')
            ->where(function ($q) {
                $q->where('type', 'xstay');
            })
            ->whereHas('productdetail', function ($q) use ($pulaus) {
                $q->where('regency_id', $pulaus->kabupaten_id);
            })
            ->limit(5)->latest()->get();

        $news = newsletter::where('article_section', 'iklan')->limit(5)->latest()->get();

        $regencyTur = Regency::all();

        // session from pulau
        $objekPulau = $objek::where('pulau_id', $pulaus->pulau_id)->first();
        $objekProvinsi = $objek::where('provinsi_id', $pulaus->provinsi_id)->first();
        $objekKabupaten = $objek::where('kabupaten_id', $pulaus->kabupaten_id)->first();

        $pulau_slug = $objekPulau->slug;
        $pulau_nama = $objekPulau->nama_pulau;

        //session from provinsi 
        $provinsi_slug = $objekProvinsi->slug;
        $provinsi_nama = $objekProvinsi->nama_pulau;

        // create session to provinsi
        $kota_slug = $objekKabupaten->slug;
        $kota_nama = $objekKabupaten->nama_pulau;

        if ($var1 == 0) {
            $ratings = 0;
            return view('Destindonesia.wisata', compact(
                'pulaus',
                'reviews',
                'ratings',
                'var2',
                'tur',
                'transfer',
                'rental',
                'hotel',
                'xstay',
                'activity',
                'news',
                'regencyTur',
                'pulau_slug',
                'pulau_nama',
                'provinsi_slug',
                'provinsi_nama',
                'kota_slug',
                'kota_nama',
                'want',
                'been'
            ));
        } else {
            $ratings = $var1 / $var2;
            return view('Destindonesia.wisata', compact(
                'pulaus',
                'reviews',
                'ratings',
                'var2',
                'tur',
                'transfer',
                'rental',
                'hotel',
                'xstay',
                'activity',
                'news',
                'regencyTur',
                'pulau_slug',
                'pulau_nama',
                'provinsi_slug',
                'provinsi_nama',
                'kota_slug',
                'kota_nama',
                'want',
                'been'
            ));
        }
    }

    public function allWisataShow($slug)
    {
        $pulaus = detailObjek::where('slug', $slug)->first();
        $wisata = detailWisata::where('kabupaten_id', $pulaus->kabupaten_id)->get();
        
        $kategories = detailWisata::select('kategori')->where("kabupaten_id", $pulaus->kabupaten_id)->groupBy('kategori')->get();
        $kategoriWisata = collect(['All']);
        
        foreach ($kategories as $key => $kategory) {
            # code...
            $kategoriWisata->push(
                $kategory->kategori
            );
        }

        return view('Destindonesia.all-wisata', compact('pulaus', 'wisata', 'kategoriWisata'));
    }

    //    review rating objek
    public function reviewObjekStore(Request $request)
    {   
        // dd($request->all());

        if(auth()->user()->role!='traveller'){
            return false;
        }

        $gallery=[];
        $imgs = collect([]);
        
        if($request->images){
            foreach($request->images as $key => $img){
                $new_file = time().rand(1,99).$key.'.'.$img->getClientOriginalExtension();
               
                $img->move(storage_path('app/public/gallery_post'),$new_file);
                $imgs->push($new_file);
            }
        }

        $gallery['result'] = $imgs;
        // die;
        
        $review = new reviewPost();
        $review->product_id = $request->product_id;
        $review->comments = $request->comment;
        $review->star_rating = $request->rating;
        $review->gallery_post = json_encode($gallery);
        $review->traveller_id = auth()->user()->id;
        $review->status ='deactive';
        
        $review->save();

        return redirect()->back()->with('flash_msg_success', 'Your review has been submitted Successfully,');
    }

    public function viewExtra()
    {
        $extra = Extra::get();

        return view('BE.admin.master-extra', compact('extra'));
    }

    public function addExtra(Request $request)
    {
        $fields = collect([]);
        $image_thumbnail = '';

        $validator = Validator::make(
            $request->all(),
            [
                'nama_ekstra' => 'required',
                'detail_ekstra' => 'required',
                'note_ekstra' => 'required',
                'harga_ekstra' => 'required',
                'images_gallery.*' => 'required|mimes:jpg,jpeg,png,bmp|max:2000',
                'image_thumbnail' => 'required|mimes:jpg,jpeg,png,bmp|max:2000'
            ],
            [
                'nama_ekstra.required' => 'Nama ekstra wajib diisi!',
                'detail_ekstra.required' => 'Detail ekstra wajib diisi!',
                'note_ekstra.required' => 'Note ekstra wajib diisi!',
                'harga_ekstra.required' => 'Harga ekstra wajib diisi!',
                'images_gallery.*.required' => 'Foto ekstra wajib diisi!',
                'images_gallery.*.mimes' => 'Foto harus memiliki ekstensi (jpg, jpeg, atau png)',
                'images_gallery.*.max' => 'Foto maximum berukuran 2MB',
                'image_thumbnail.required' => 'Foto thumbnail wajib diisI!',
                'image_thumbnail.mimes' => 'Foto harus memiliki ekstensi (jpg, jpeg, atau png)',
                'image_thumbnail.max' => 'Foto maximum berukuran 2MB',
            ]
        );

        if ($validator->fails()) {
            return redirect(route('extra.index'))
                ->withErrors($validator)
                ->withInput();
        }


        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {

                $new_foto = time() . 'GalleryExtra' . $value->getClientOriginalName();
                $tujuan_upload = 'Extra/Gallery/';

                $lebar_foto = Image::make($value)->width();
                $lebar_foto = $lebar_foto * 50 / 100;

                Image::make($value)->resize($lebar_foto, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($tujuan_upload . $new_foto);

                // $value->move($tujuan_upload, $new_foto);
                $images_gallery = $tujuan_upload . $new_foto;

                $fields->push(
                    $images_gallery,
                );
            }
        }

        if ($request->hasFile('image_thumbnail')) {
            $image = $request->file('image_thumbnail');

            $new_foto = time() . 'ThumbnailExtra' . $image->getClientOriginalName();
            $tujuan_upload = 'Extra/Thumbnail/';;

            Image::make($image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($tujuan_upload . $new_foto);

            // $image->move($tujuan_upload, $new_foto);
            $image_thumbnail = $tujuan_upload . $new_foto;
        }

        $ekstra = Extra::create([
            'nama_ekstra' => $request->nama_ekstra,
            'detail_ekstra' => $request->detail_ekstra,
            'note_ekstra' => $request->note_ekstra,
            'harga_ekstra' => $request->harga_ekstra,
            'status_ekstra' => isset($request->status_section) ? $request->status_section : 'off',
            'foto_ekstra' => json_encode($fields),
            'thumbnail' => $image_thumbnail,
        ]);

        return redirect()->route('extra.index');
    }

    public function destroyExtra($id)
    {
        $extra = Extra::findOrFail($id);

        if (isset($extra->foto_ekstra)) {
            foreach (json_decode($extra->foto_ekstra) as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }
        }

        if (file_exists($extra->thumbnail)) {
            unlink($extra->thumbnail);
        }

        Extra::findOrFail($id)->delete();

        return redirect()->route('extra.index');
    }

    public function editExtra($id)
    {

        $extra = Extra::findOrFail($id);

        return view('BE.admin.master-extra-edit', compact('extra'));
    }

    public function updateExtra(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'nama_ekstra' => 'required',
                'detail_ekstra' => 'required',
                'note_ekstra' => 'required',
                'harga_ekstra' => 'required',
                'images_gallery.*' => 'max:2000',
                'image_thumbnail' => 'max:2000'
            ],
            [
                'nama_ekstra.required' => 'Nama ekstra wajib diisi!',
                'detail_ekstra.required' => 'Detail ekstra wajib diisi!',
                'note_ekstra.required' => 'Note ekstra wajib diisi!',
                'harga_ekstra.required' => 'Harga ekstra wajib diisi!',
                'images_gallery.*.max' => 'Foto maximum berukuran 2MB',
                'image_thumbnail.max' => 'Foto maximum berukuran 2MB',
            ]
        );

        if ($validator->fails()) {
            return redirect(route('extra.edit', $id))
                ->withErrors($validator)
                ->withInput();
        }

        $extra = Extra::findOrFail($id);

        $fields = collect([]);
        $image_thumbnail = '';

        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {

                $new_foto = time() . 'GalleryExtra' . $value->getClientOriginalName();
                $tujuan_upload = 'Extra/Gallery/';

                $lebar_foto = Image::make($value)->width();
                $lebar_foto = $lebar_foto * 50 / 100;

                Image::make($value)->resize($lebar_foto, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($tujuan_upload . $new_foto);

                // $value->move($tujuan_upload, $new_foto);
                $images_gallery = $tujuan_upload . $new_foto;

                $fields->push(
                    $images_gallery,
                );

                if (isset($extra->foto_ekstra)) {
                    foreach (json_decode($extra->foto_ekstra) as $key => $value) {
                        if (file_exists($value)) {
                            unlink($value);
                        }
                    }
                }
            }
        }

        if ($request->hasFile('image_thumbnail')) {
            $image = $request->file('image_thumbnail');

            $new_foto = time() . 'ThumbnailExtra' . $image->getClientOriginalName();
            $tujuan_upload = 'Extra/Thumbnail/';;

            Image::make($image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($tujuan_upload . $new_foto);

            // $image->move($tujuan_upload, $new_foto);
            $image_thumbnail = $tujuan_upload . $new_foto;

            //delete old image
            if (file_exists($extra->thumbnail)) {
                unlink($extra->thumbnail);
            }
        }

        $extra->update([
            'nama_ekstra' => $request->nama_ekstra,
            'detail_ekstra' => $request->detail_ekstra,
            'note_ekstra' => $request->note_ekstra,
            'harga_ekstra' => $request->harga_ekstra,
            'status_ekstra' => isset($request->status_section) ? $request->status_section : 'off',
            'foto_ekstra' => $request->hasFile('images_gallery') ? json_encode($fields) : $extra->foto_ekstra,
            'thumbnail' => $request->hasFile('image_thumbnail') ? $image_thumbnail : $extra->thumbnail,
        ]);

        return redirect()->route('extra.index');
    }

    public function showUser()
    {
        $users = User::orderBy('id', 'DESC')->get();
        return view('BE.admin.master-user-new', compact('users'));
    }

    public function createUser()
    {
        return view('BE.admin.master-add-user');
    }

    public function saveUser(Request $request)
    {
        $user = new User;
        $usertraveller = new Usertraveller;
        // dd($request->all());
        // start validation
        $message = [
            'required' => ':attribute tidak boleh kosong',
            'min' => ':attribute minimal :min karakter',
            'max' => ':attribute maksimal :max karakter',
            'unique' => ':attribute tidak boleh sama'
        ];

        $this->validate($request, [
            'first_name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
            'no_telp' => 'min:5|max:13'
        ], $message);

        $user->first_name = $request->first_name;
        $user->jenis_registrasi = $request->jenis_registrasi;
        $user->role = $request->role;
        $user->email = $request->email;
        $user->no_tlp = $request->no_tlp;
        $user->lisensi = $request->lisensi;
        $user->jk = $request->jk;
        $user->password = bcrypt($request->password);
        $user->active_status = $request->status_active;
        $user->created_at = Carbon::now();
        $user->email_verified_at = Carbon::now();

        $user->save();

        if ($request->role === 'traveller') {
            $last_name = explode(" ", $request->first_name);
            $usertraveller->user_id = $user->id;
            $usertraveller->last_name = end($last_name);
            $usertraveller->akun_booking_status = 0;
            $usertraveller->newslatter_status = 1;

            // $usertraveller->
            $usertraveller->save();
        }

        return redirect()->route('user.admin-add')->with(['success' => 'User berhasil ditambah']);
    }

    public function editUser($id)
    {
        $user = User::findOrFail($id);
        return view('BE.admin.master-edit-user', compact('user'));
    }

    public function updateUser($id, Request $request)
    {

        $user = User::findOrFail($id);
        $usertraveller = Usertraveller::where('user_id', $id)->first();
        // dd($request->all());
        // start validation
        $message = [
            'required' => ':atrribute tidak boleh kosong',
            'min' => ':attribute minimal :min karakter',
            'max' => ':attribute maksimal :max karakter',
            'unique' => ':attribute tidak boleh sama'
        ];

        if ($user->email != $request->email) {
            $this->validate($request, [
                'email' => 'email|unique:users,email',
            ], $message);
        }

        $this->validate($request, [
            'email' => 'email|unique:users,email',
        ], $message);

        if ($request->email !== $user->email) {
            $user->email = $request->email;
        }

        $user->first_name = $request->first_name;
        $user->jenis_registrasi = $request->jenis_registrasi;
        $user->role = $request->role;
        $user->no_tlp = $request->no_tlp;
        $user->lisensi = $request->lisensi;
        $user->jk = $request->jk;
        $user->password = $request->password;
        $user->active_status = $request->status_active;
        $user->updated_at = Carbon::now();

        $user->save();

        if ($request->role == 'traveller') {
            $last_name = explode(" ", $request->first_name);
            if (!$usertraveller) {

                Usertraveller::create([
                    'user_id' => $id,
                    'last_name' => end($last_name),
                    'akun_booking_status' => 0,
                    'newslatter_status' => 1
                ]);
            } else {
                //update
                Usertraveller::where('id', $usertraveller->id)->update([
                    'last_name' => end($last_name),
                    'akun_booking_status' => 0,
                    'newslatter_status' => 1
                ]);
            }
        }

        return redirect()->route('user.admin-add')->with(['success' => 'User berhasil diedit']);
    }

    public function deleteUser($id)
    {

        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('user.admin-add')->with(['success' => 'User berhasil dihapus']);
    }

    public function orderRegencyStore(settingGeneral $settingGeneral, Request $request)
    {
        // dd($request->all());
        // $this->validate($request,[
        //     'provinsi_id'=>'required'
        // ],[
        //     'provinsi_id.required'=>'Provinsi_id tidak boleh kosong'
        // ]);

        $settingGeneral = settingGeneral::find(99);
        $settingGeneral->regency_order = $request->provinsi_id;
        $settingGeneral->update();

        return back();
    }

    public function orderRegency()
    {
        $regencies = DB::select('select * from regencies');

        return view('BE.admin.order-regency', compact('regencies'));
    }

    public function masterChoiceIndex()
    {
        $choices = MasterChoice::all();
        return view('BE.admin.master-choice', compact('choices'));
    }

    public function masterChoiceStore(Request $request)
    {

        $choice = new MasterChoice;

        $pesan = [
            'required' => ':atrribute harus diisi'
        ];

        $this->validate($request, [
            'choice' => 'required',
            'product_type' => 'required'
        ]);


        $choice->name = $request->choice;
        $choice->product_type = $request->product_type;
        $choice->save();

        return redirect()->back();
    }

    public function masterChoiceEdit($id)
    {

        $choice = MasterChoice::findOrFail($id);
        return view('Be.admin.master-choice-edit', compact('choice'));
    }
    public function masterChoiceUpdate($id, Request $request)
    {

        $choice = MasterChoice::findOrFail($id);

        $pesan = [
            'required' => ':atrribute harus diisi'
        ];

        $this->validate($request, [
            'choice' => 'required',
            'product_type' => 'required'
        ]);

        $choice->name = $request->choice;
        $choice->product_type = $request->product_type;
        $choice->save();

        return redirect()->route('choice.index');
    }

    public function masterChoiceDelete($id)
    {

        $choice = MasterChoice::findOrFail($id);
        $choice->delete();
        return redirect()->route('choice.index');
    }

    // MyListing Seller
    public function accListing(Request $request)
    {
        // trigger to query filter checkbox
        $query = Productdetail::query()
            ->join('product', 'product_detail.product_id', '=', 'product.id');

        // if input name search have a value do this
        if ($request->filled('search')) {
            $data_listing = Productdetail::search($request->search)
                ->paginate(5);
        } elseif
        // else if input name type have a value do this
        ($request->input('type')) {

            // if type or tipe_tur or available have a value query this
            $query->whereIn('type', $request->input('type'))
                ->orWhereIn('tipe_tur', $request->input('type'))
                ->orWhereIn('available', $request->input('type')) //                ->orWhereIn('status', $request->input('type'))
            ;

            // query Product detail with eloquent in model, in the mode have a relationship with discount, etc
            $data_listing = $query->with(
                'discount',
                'product',
                'product.user',
                'DetailKendaraan',
                'DetailKendaraan.jenismobil',
                'DetailKendaraan.merekmobil'
            )->paginate(5);

            // return view and show result search
            return view('BE.admin.accListing', compact('data_listing'));
        } else {
            // else just showing all data in view with query and eloquent

            $data_listing = Productdetail::with(
                'discount',
                'product',
                'product.user',
                'DetailKendaraan',
                'DetailKendaraan.jenismobil',
                'DetailKendaraan.merekmobil'
            )
                ->paginate(5);
        }
        return view('BE.admin.accListing', compact('data_listing'));
    }

    // Update Status Listing
    public function accListingUpdate(Request $request, $id)
    {
        $data = Product::with('productdetail')->findOrFail($id);
        // dd($data);
        $data->productdetail->update([
            'available' => $request->available
        ]);
        // dd($data);

        return redirect()->back()->with('success', 'Status Listing Sudah Diupdate');
    }

    public function confrimationSellerAccountIndex()
    {
        $sellers = User::where('role', 'seller')->get();

        return view('BE.admin.confrim-seller-account-index', compact('sellers'));
    }

    public function confrimationSellerDetail($id)
    {

        $seller = User::where('id', $id)->first();
        $sellerdetail = Userdetail::where('user_id', $seller->id)->first();
        
        $legal_doc = \App\Models\MasterDocumentValue::where('user_id',$id)->with('user','masterdocumentlegal')->get();
        // dd($legal_doc);
        return view('BE.admin.confrim-seller-account-detail', compact('seller', 'sellerdetail','legal_doc'));
    }

    public function updateStatusSeller(Request $request, $id)
    {
        $seller = User::findOrFail($id);

        $seller->active_status = $request->status_active;

        $seller->save();

        return response()->json([
            'success' => true,
            // 'status'=>200,
            'message' => 'Status seller berhasil diubah',
            'url' => route('confrim.seller-account.index')
        ]);
    }

    public function deleteObjek($id)
    {
        $objek = detailObjek::findOrFail($id);
        $objek->delete();

        return redirect()->route('objek');
    }

    public function wontToGo(Request $request)
    {

        $like = new LikeProduct();

        $like->ingin_kesini = 1;
        $like->objek = $request->objek;
        $like->save();

        $wont_to_go = LikeProduct::where('ingin_kesini', '<>', 0)->where('objek', $request->objek)->count();

        return response()->json([
            'succses' => true,
            'data' => $wont_to_go
        ], 200);
    }

    public function beenThere(Request $request)
    {

        $like = new LikeProduct();

        $like->pernah_kesini = 1;
        $like->objek = $request->objek;
        $like->save();

        $been_there = LikeProduct::where('pernah_kesini', '<>', 0)->where('objek', $request->objek)->count();

        return response()->json([
            'succses' => true,
            'data' => $been_there
        ], 200);
    }

    public function objectByCategory(Request $request){
        $kategori = $request->category;
        $kabupaten_id = $request->kabupaten_id;

        $wisata = null;

        if($kategori!='All'){
            if($request->objek=='kabupaten'){
                $wisata = detailWisata::where('kabupaten_id',$kabupaten_id)->where('kategori',$kategori)->limit(4)->get();
            }else{
                $wisata = detailWisata::where('kabupaten_id',$kabupaten_id)->where('kategori',$kategori)->get();
            }
        }else{
            if($request->objek=='kabupaten'){
                $wisata = detailWisata::where('kabupaten_id',$kabupaten_id)->limit(4)->get();
            }else{
                $wisata = detailWisata::where('kabupaten_id',$kabupaten_id)->get();
            }    
        }

        return view('Destindonesia.objek-section', compact('wisata'));
    }

    public function allObjectByCategory(Request $request){
        $kategori = $request->category;
        $kabupaten_id = $request->kabupaten_id;

        $wisata = null;

        if($kategori!='All'){
            $wisata = detailWisata::where('kabupaten_id',$kabupaten_id)->where('kategori',$kategori)->get();
        }else{
            $wisata = detailWisata::where('kabupaten_id',$kabupaten_id)->get();
        }

        return view('Destindonesia.all-wisata-filter', compact('wisata'));
    }

    public function reviewModerator(){
        return view('BE.admin.comments-moderator');
    }

    public function reviewModeratorUpdate(Request $request, $id){
        // dd($request->status);
        $review = reviewPost::findOrFail($id);

        $review->update([
            'status'=>$request->status
        ]);

        return response()->json([
            'success'=>true,
            'message'=>'Status ulasan berhasil diubah',
            'status'=>'success'
        ]);
    }

    public function reviewModeratorDelete($id){

        $review = reviewPost::findOrFail($id);
        $review->delete();

        return response()->json([
            'success'=>true,
            'message'=>'Satu ulasan berhasil dihapus',
            'status'=>'success'
        ]);
    }

    public function reviewMultipeDelete(Request $request){
        foreach($request->deleted_id as $key => $value){

            $id = $value['id'];
            $row = reviewPost::findOrFail($id);

            // dump($row);
            $row->destroy($id);
        }

        return response()->json([
            'success'=>true,
            'status'=>"success",
            'message'=>'Ulasan berhasil dihapus'
        ]);
    }

    public function reviewSearch(Request $request){

        $reviews = reviewPost::select('review_posts.id','review_posts.comments','review_posts.status','users.first_name','product.product_name','detail_objeks.nama_pulau','detail_wisatas.namaWisata')
                   ->join('users','users.id','review_posts.traveller_id')
                   ->leftJoin('product','product.id','=','review_posts.product_id') 
                   ->leftJoin('detail_objeks','detail_objeks.id','=','review_posts.detailObjek_id') 
                   ->leftJoin('detail_wisatas','detail_wisatas.id','=','review_posts.detailWisata_id') 
                   ->whereNotNull('review_posts.product_id')
                   ->whereNotNull('review_posts.detailObjek_id')
                   ->whereNotNull('review_posts.detailWisata_id')
                //    ->orWhere(function($query){
                //         $query->whereNotNull('review_posts.detailObjek_id');
                //    })->orWhere(function($q){
                //         $q->whereNotNull('review_posts.detailWisata_id');
                //    })
                   ->orWhere('users.first_name','like','%'.$request->cari.'%')
                   ->orWhere('detail_objeks.nama_pulau','like','%'.$request->cari.'%')
                   ->orWhere('product.type','like','%'.$request->cari.'%')
                   ->orWhere('detail_wisatas.namaWisata','like','%'.$request->cari.'%')
                   ->get();
        
        $per_page = 10;
        $total = count($reviews);
        $current_page = $request->input('page') ? $request->input('page') : 1;
           
        $start_point = ($current_page * $per_page) - $per_page;
           
        $collect = $reviews->toArray();
        $collect = array_slice($collect, $start_point, $per_page, true);
           
        $collect = new Paginator($collect, $total, $per_page, $current_page,[
            'url'=>$request->url(),
            'query'=>$request->query(),
        ]);
           
        return response()->json([
            'success'=>true,
            'datas'=>$collect
        ]);
    }

    public function reviewsDatas(Request $request){
        $reviews = reviewPost::select('review_posts.id','review_posts.comments','review_posts.status','users.first_name','product.product_name','detail_objeks.nama_pulau','detail_wisatas.namaWisata')
        ->join('users','users.id','review_posts.traveller_id')
        ->leftJoin('product','product.id','=','review_posts.product_id') 
        ->leftJoin('detail_objeks','detail_objeks.id','=','review_posts.detailObjek_id') 
        ->leftJoin('detail_wisatas','detail_wisatas.id','=','review_posts.detailWisata_id') 
        ->whereNotNull('review_posts.product_id')
        ->orWhere('review_posts.detailObjek_id','<>','','or')
        ->orWhere('review_posts.detailWisata_id','<>','','or')
        ->get();

        $per_page = 10;
        $total = count($reviews);
        $current_page = $request->input('page') ? $request->input('page') : 1;

        $start_point = ($current_page * $per_page) - $per_page;

        $datas = $reviews->toArray();
        $datas = array_slice($datas, $start_point, $per_page, true);

        $datas = new Paginator($datas, $total, $per_page, $current_page,[
            'url'=>$request->url(),
            'query'=>$request->query(),
        ]);

        return response()->json([
            'success'=>true,
            'datas'=>$datas
        ]);
    }
}
