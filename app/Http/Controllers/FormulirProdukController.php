<?php

namespace App\Http\Controllers;

use App\Models\detailObjek;
use App\Models\detailWisata;
use App\Models\formulirProdukHotel;
use App\Models\formulirProdukTransfer;
use App\Models\formulirProdukTur;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormulirProdukController extends Controller
{
//    Start Formulir Tur
    public function formulirTur()
    {
        $lokasis = detailWisata::select('namaWisata')->get();
//        dd($lokasi);

        return view('Produk.Tur.formulir', compact('lokasis'));
    }

    public function formulirTurStore(Request $request)
    {
        // input new detailObjek
        $inputData = new formulirProdukTur;
        //        request by form + by variabel
        $inputData->tanggalMulai = $request->tanggalMulai;
        $inputData->waktuMulai = $request->waktuMulai;
        $inputData->durasi = $request->durasi;
        $inputData->lokasi = $request->lokasi;
        $inputData->dewasa = $request->dewasa;
        $inputData->anak = $request->anak;
        $inputData->balita = $request->balita;
        $inputData->transportasi = $request->transportasi;
        $inputData->jumlahPeserta = $request->jumlahPeserta;
        $inputData->makanan = $request->makanan;
        $inputData->namaObjek = $request->namaObjek;
        $inputData['jenisKendaraan'] = $request->jenisKendaraan;
        foreach ($d1 = $request->addMoreInputFields1 as $key => $value) {
            $inputData['deskripsiCustom'] = $d1;
            $inputData->save();

        }

        return back();
    }

//    End Formulir Tur

//  Start Formulir Hotel

    public function formulirHotel(Request $request)
    {
        return view('Produk.Hotel.formulir');
    }

    public function formulirHotelStore(Request $request)
    {
        $inputData = new formulirProdukHotel;

        $inputData->checkIn = $request->checkIn;
        $inputData->checkOut = $request->checkOut;
        $inputData->lokasi = $request->lokasi;
        $inputData->jenisHotel = $request->jenisHotel;
        $inputData->fasilitas = $request->fasilitas;
        $inputData->kebutuhan = $request->kebutuhan;

        $inputData['jenisMakan'] = $request->jenisMakan;
        foreach ($c1 = $request->addMoreInputFields1 as $key => $value) {
            foreach ($c2 = $request->addMoreInputFields2 as $key => $item) {
                foreach ($c3 = $request->addMoreInputFields3 as $key => $value) {
                    $inputData['jumlahOrang'] = $c1;
                    $inputData['jumlahOrangPerkamar'] = $c2;
                    $inputData['jenisKamar'] = $c3;
                    $inputData->save();
                }
            }
        }
        return back();

    }

//    End Formulir Hotel

//Start Formulir Transfer
    public function formulirTransfer()
    {
        $provinsis = DB::select('select * from provinces');
        $kabupatens = DB::select('select * from regencies');
        $districts = DB::select('select * from districts');

        return view('Produk.Transfer.formulir', compact('provinsis', 'kabupatens', 'districts'));
    }

    public function formulirTransferStore(Request $request)
    {
        $inputData = new formulirProdukTransfer;

        $inputData->tanggalPergi = $request->tanggalPergi;
        $inputData->jamPergi = $request->jamPergi;
        $inputData->transferDari = $request->transferDari;
        $inputData->kotaDari = $request->kotaDari;
        $inputData->provinsiDari = $request->provinsiDari;
        $inputData->embedMapsDari = $request->embedMapsDari;
        $inputData->transferKe = $request->transferKe;
        $inputData->kotaKe = $request->kotaKe;
        $inputData->provinsiKe = $request->provinsiKe;
        $inputData->embedMapsKe = $request->embedMapsKe;
        $inputData->coupon_question = $request->coupon_question;
        $inputData->tanggalPulang = $request->tanggalPulang;
        $inputData->jamPulang = $request->jamPulang;
        $inputData->transport = $request->transport;
        $inputData->dewasa = $request->dewasa;
        $inputData->anak = $request->anak;
        $inputData->balita = $request->balita;
        $inputData->barangBesar = $request->barangBesar;
        $inputData->barangKecil = $request->barangKecil;
        $inputData->memo = $request->memo;
        $inputData->kamtuu = $request->kamtuu;

        $inputData->save();

        return back();
    }

    public function formulirList()
    {
        $formulirTurs = formulirProdukTur::where([
            ['id', '!=', Null]])->paginate(2);
        $formulirHotels = formulirProdukHotel::where([
            ['id', '!=', Null]])->paginate(2);
        $formulirTransfers = formulirProdukTransfer::where([
            ['id', '!=', Null]])->paginate(2);

        return view('BE.admin.Formulir.formulir-list', compact('formulirTurs', 'formulirHotels', 'formulirTransfers'));
    }

    public function formulirTurShow($id)
    {
        $formulirTurs = formulirProdukTur::findOrFail($id);

        $a1 = formulirProdukTur::select('jenisKendaraan')->findOrFail($id);
        $a2 = formulirProdukTur::select('deskripsiCustom')->findOrFail($id);

        $jK1 = json_decode($a1);
        $jK2 = json_decode($a2);
        foreach ($jK1 as $value) {
            foreach ($jK2 as $item) {
                $kendaraans = $value;
                $deskripsis = $item;
//                dd($kendaraans);
                return view('BE.admin.formulir.formulirTurs', compact('formulirTurs', 'kendaraans', 'deskripsis'));
            }
        }


    }

    public function formulirHotelShow($id)
    {
        $formulirHotels = formulirProdukHotel::findOrFail($id);


        $a1 = formulirProdukHotel::select('jenisMakan')->findOrFail($id);
        $a2 = formulirProdukHotel::select('jumlahOrang')->findOrFail($id);
        $a3 = formulirProdukHotel::select('jumlahOrangPerkamar')->findOrFail($id);
        $a4 = formulirProdukHotel::select('jenisKamar')->findOrFail($id);

        $makans = json_decode($a1);
        $b2 = json_decode($a2);
        $b3 = json_decode($a3);
        $b4 = json_decode($a4);


        foreach ($b2 as $values) {
            foreach ($b3 as $item) {
                foreach ($b4 as $value) {
                    $jmlOrg = $values;
                    $orgKamar = $item;
                    $jnKamar = $value;

//                    dd($jmlOrg);
                    return view('BE.admin.formulir.formulirHotels', compact(
                        'formulirHotels',
                        'makans',
                        'jmlOrg',
                        'jnKamar',
                        'orgKamar'));
                }
            }
        }
    }

    public function formulirTransferShow($id)
    {
        $formulirTransfers = formulirProdukTransfer::findOrFail($id);

        return view('BE.admin.formulir.formulirTransfers', compact('formulirTransfers'));
    }
}
