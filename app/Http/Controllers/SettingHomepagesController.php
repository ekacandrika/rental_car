<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdatesettingHomepagesRequest;
use App\Models\settingHomepages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class SettingHomepagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = settingHomepages::get();

        $kontens = settingHomepages::where('section', '=', 'Konten_1')->get();
        $kamtuuSets = settingHomepages::where('section', '=', 'Kamtuu')->get();
        $travellerSets = settingHomepages::where('section', '=', 'Traveller')->get();
        $sellerSets = settingHomepages::where('section', '=', 'Seller')->get();
        $agenSets = settingHomepages::where('section', '=', 'Agen')->get();
        $storeSets = settingHomepages::where('section', '=', 'OFStore')->get();
        $corporateSets = settingHomepages::where('section', '=', 'Corporate')->get();
        $layananSets = settingHomepages::where('section', '=', 'Layanan')->get();
        $kerjasamaSets = settingHomepages::where('section', '=', 'Kerjasama')->get();

        $sects = [
            'Konten_1', 'Traveller', 'Seller', 'Agen',
            'Kamtuu', 'OFStore', 'Corporate', 'Layanan', 'Kerjasama'
        ];
        return view('BE.admin.section', compact(
            'sections',
            'sects',
            'kontens',
            'kamtuuSets',
            'travellerSets',
            'sellerSets',
            'agenSets',
            'storeSets',
            'corporateSets',
            'layananSets',
            'kerjasamaSets'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoresettingHomepagesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'title_section' => 'required',
            'image_section' => 'required',
            'detail_section' => 'required',
        ],[
            'title_section.required'=>'Judul section tidak boleh kosong!' ,
            'image_section.required'=>'Gambar section tidak boleh kosong!' ,
            'detail_section.required'=>'Detail section tidak boleh kosong!' ,
        ]);

        if($validate->fails()){
            return redirect()->route('section.index')->withErrors($validate)->withInput();
        }

        $detail_section = $request->detail_section;
        $nameFile = time() . $request->image_section->getClientOriginalName();
        $path = $request->file('image_section')->storeAs('public/images', $nameFile);

        $dom = new \DomDocument();
        $dom->loadHtml($detail_section, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $imageFile = $dom->getElementsByTagName('imageFile');

        foreach ($imageFile as $item => $image) {
            $data = $img->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $imgeData = base64_decode($data);
            $image_name = "/upload/" . time() . $item . '.png';
            $path = public_path() . $image_name;
            // $lebar = Image::make()->;
            // file_put_contents($path, $imgeData);
            $image->removeAttribute('src');
            $image->setAttribute('src', $image_name);
        }

        $detail_section = $dom->saveHTML();
        $fileUpload = new settingHomepages;
        $fileUpload->title_section = $request->title_section;
        $fileUpload->image_section = $request->image_section;
        $fileUpload->section = $request->section;

        if ($fileUpload->status_section === null) {
            $setValue = $fileUpload->status_section = "off";
        } elseif ($fileUpload->status_section === "on") {
            $setValue = $fileUpload->status_section = "on";
        }

        $fileUpload->status_section = $setValue;
        //        $fileUpload->detail_section = $request->detail_section;
        $fileUpload->detail_section = $detail_section;
        $fileUpload->image_section = $path;

        //        dd($fileUpload);
        $fileUpload->save();

        return redirect()->route('section.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\settingHomepages  $settingHomepages
     * @return \Illuminate\Http\Response
     */
    public function show(settingHomepages $settingHomepages, $id)
    {
        $sections = settingHomepages::find($id);
        //        dd($sections);
        return view('Homepage.section-index', compact('sections'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\settingHomepages  $settingHomepages
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sections = settingHomepages::findOrFail($id);
        $alls = settingHomepages::get();
        return view('BE.admin.edit_section', compact('sections', 'alls'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatesettingHomepagesRequest  $request
     * @param  \App\Models\settingHomepages  $settingHomepages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sections = settingHomepages::findOrFail($id);

        $data = [
            'title_section' => $request->title_section,
            'detail_section' => $request->detail_section,
        ];
        if ($request->hasfile('image_section')) {
            $destination = 'storage/' . $sections->image;
            //            dd($destination);
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $file = $request->file('image_section');
            $extention = $file->getClientOriginalExtension();
            $filename = 'images/' . time() . '.' . $extention;
            $file->move('storage/images/', $filename);
            $sections->image_section = $filename;
        }
        $sections->update($data);
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\settingHomepages  $settingHomepages
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, settingHomepages $settingHomepages)
    {
        //
        $setting = $settingHomepages->first();
        $setting::destroy($id);

        return redirect()->route('section.index');
    }

    public function storeImageSlider(Request $request)
    {
        $detail = settingHomepages::where('section', 'Home')->first();

        $fields = collect([]);

        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {

                $new_foto = time() . 'HomeGallerySlider' . $value->getClientOriginalName();
                $tujuan_upload = 'HomeGallerySlider/';

                $lebar = Image::make($value)->width();
                $lebar = $lebar * 50/100;

                Image::make($value)->resize($lebar, null, function ($constraint){
                    $constraint->aspectRatio();
                })->save($tujuan_upload.$new_foto);

                $images_gallery = $tujuan_upload . $new_foto;

                $fields->push(
                    $images_gallery,
                );
            }

            //delete old image
            if (isset($detail) && isset($detail->image_section)) {
                foreach (json_decode($detail->image_section) as $key => $value) {
                    if (file_exists($value)) {
                        unlink($value);
                    }
                }
            }
        }

        //dd($fields);

        if (isset($detail)) {
            $detail->update([
                'title_section' => 'Home Image Slider',
                'image_section' => json_encode($fields),
                'status_section' => 'on',
                'detail_section' => 'Home Image Slider',
                'section' => 'Home',
            ]);

            return redirect()->route('section.index');
        }

        if (!isset($detail)) {
            $settingHomepages = new settingHomepages;

            $settingHomepages->title_section = 'Home Image Slider';
            $settingHomepages->image_section = json_encode($fields);
            $settingHomepages->status_section = 'on';
            $settingHomepages->detail_section = 'Home Image Slider';
            $settingHomepages->section = 'Home';

            $settingHomepages->save();

            return redirect()->route('section.index');
        }
    }

    public function imageSliderCrop(Request $request)
    {
        $folderPath = public_path('upload/');
        $image_parts = explode(";base64,", $request->image);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $imageName = uniqid() . '.png';
        $imageFullPath = $folderPath . $imageName;
        file_put_contents($imageFullPath, $image_base64);
        $saveFile = new Picture;
        $saveFile->name = $imageName;
        $saveFile->save();

        return response()->json(['success' => 'Crop Image Uploaded Successfully']);
    }
}
