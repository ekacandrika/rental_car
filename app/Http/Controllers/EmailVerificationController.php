<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;

class EmailVerificationController extends Controller
{
    // public function via($notifiable)
    // {
    //     return ['mail'];
    // }

    public function sendEmailVerify(Request $request){
        $user = auth()->user();
        $token = $this->urlHashing($user);
       
        Mail::mailer('smtp')->send('auth.email-verify-custom',['id'=>$user->id,'token'=>$token],function($message) use($user){
            $message->to($user->email);
            $message->subject('Verify Email Address');
        });
        
        return back()->with('success','Verification link sent!');
    }

    public function emailVerify(EmailVerificationRequest $request){
        $request->fulfill();
        $this->sendEmailNotify();
        
        if(auth()->user()->role=='seller'){
            return redirect()->to('/dashboard/seller');
        }elseif(auth()->user()->role=='traveller'){
            // dd('res');
            return redirect()->to('/dashboard/traveller');
        }elseif(auth()->user()->role=='agent'){
            return redirect()->to('/dashboard/agent');
        }
    }

    private function sendEmailNotify(){
        
        $to =  Mail::mailer('smtp');
        
        Mail::send('BE.admin.email-notif',['text'=>null],function($message) use($to){
            $message->to('kamtuuofficial@gmail.com');
            $message->subject('User register notification');
        });
    }

    private function urlHashing($notifiable){
        return URL::temporarySignedRoute(
            'email.verifed',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire',60)),
            [
                'id'=>$notifiable->id,
                'hash'=>sha1($notifiable->getEmailForVerification()),
            ]
        );
    }
}
