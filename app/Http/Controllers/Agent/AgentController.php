<?php

namespace App\Http\Controllers\Agent;

use App\Models\User;
use App\Models\Product;
use App\Models\Province;
use App\Models\Pengajuan;
use App\Models\Userdetail;
use App\Models\BookingOrder;
use Illuminate\Http\Request;
use App\Models\Detailpesanan;
use App\Models\Productdetail;
use App\Models\Usertraveller;
use App\Http\Controllers\Controller;

class AgentController extends Controller
{
    public function viewMyAccount()
    {
        $profil = Usertraveller::where('user_id',auth()->user()->id)->first();
      
        $kota = Province::all();
        $data_kota = [];
        
        if($profil){
            $data_kota = Province::where('id', $profil->province_id)->first();
        }else{
            $profil =auth()->user();
        }
       
        return view('BE.agent.my-account-agent', compact('profil', 'kota', 'data_kota'));
    }

    public function updateAccountManager(Request $request, $id)
    {
        // dd($id, $request->all());
        $user = User::findOrFail($id);
        $customer = Usertraveller::where('user_id', $id)->first();

        $user->update([
            'first_name' => $request->first_name,
            'no_tlp' => $request->no_tlp,
        ]);
        if($customer){
            
            $customer->update([
                'last_name' => $request->last_name
            ]);

        }else{
            //create
            $customer = new Usertraveller;

            $customer->user_id  = $id;
            $customer->last_name= $request->last_name;

            $customer->save();

        }


        return redirect()->back();
    }

    public function updateAccountAgent(Request $request, $id)
    {
        $akun_agent = Usertraveller::where('user_id', auth()->user()->id)->first();

        if($akun_agent){
            $akun_agent->update([
                'email_booking' => $request->email_booking,
                'negara' => $request->negara,
                'first_name_booking' => $request->first_name_booking,
                'last_name_booking' => $request->last_name_booking,
                'phone_number_booking' => $request->phone_number_booking,
                'address' => $request->address,
                'province_id' => $request->province_id,
                'postal_code' => $request->postal_code,
                'alamat_web' => $request->alamat_web,
            ]);
        }else{

            $akun_agent = new Usertraveller;

            $akun_agent->email_booking = $request->email_booking;
            $akun_agent->negara = $request->negara;
            $akun_agent->first_name_booking = $request->first_name_booking;
            $akun_agent->last_name_booking = $request->last_name_booking;
            $akun_agent->phone_number_booking = $request->phone_number_booking;
            $akun_agent->address = $request->address;
            $akun_agent->province_id = $request->province_id;
            $akun_agent->postal_code = $request->postal_code;
            $akun_agent->alamat_web = $request->alamat_web;

            $akun_agent->save();
        }



        return redirect()->back();
    }

    public function viewLaporanAgent()
    {
        $hotels = Pengajuan::whereYear('created_at', date('Y'))->where('status', '-')->where('type', 'hotel')->where('agent_id', auth()->user()->id)->get();
        $xstays = Pengajuan::whereYear('created_at', date('Y'))->where('status', '-')->where('type', 'xstay')->where('agent_id', auth()->user()->id)->get();
        $rentals = Pengajuan::whereYear('created_at', date('Y'))->where('status', '-')->where('type', 'Rental')->where('agent_id', auth()->user()->id)->get();
        $transfers = Pengajuan::whereYear('created_at', date('Y'))->where('status', '-')->where('type', 'Transfer')->where('agent_id', auth()->user()->id)->get();
        $months_hotel = [];
        $months_xstay = [];
        $months_rental = [];
        $months_transfer = [];
        $earnings_hotel = [];
        $earnings_xstay = [];
        $earnings_rental = [];
        $earnings_transfer = [];
        $total_hotel = 0;
        $total_xstay = 0;
        $total_rental = 0;
        $total_transfer = 0;

        // Hotel
        foreach ($hotels as $hotel) {
            $month_hotel = $hotel->created_at->format('F');

            if (!in_array($month_hotel, $months_hotel)) {
                array_push($months_hotel, $month_hotel);
                $earnings_hotel[$month_hotel] = 0;
            }

            $earnings_hotel[$month_hotel] += $hotel->komisi;
            $total_hotel += $hotel->komisi;
        }

        // Xstay
        foreach ($xstays as $xstay) {
            $month_xstay = $xstay->created_at->format('F');

            if (!in_array($month_xstay, $months_xstay)) {
                array_push($months_xstay, $month_xstay);
                $earnings_xstay[$month_xstay] = 0;
            }

            $earnings_xstay[$month_xstay] += $xstay->komisi;
            $total_xstay += $xstay->komisi;
        }

        // Rental
        foreach ($rentals as $rental) {
            $month_rental = $rental->created_at->format('F');

            if (!in_array($month_rental, $months_rental)) {
                array_push($months_rental, $month_rental);
                $earnings_rental[$month_rental] = 0;
            }

            $earnings_rental[$month_rental] += $rental->komisi;
            $total_rental += $rental->komisi;
        }

        // Transfer
        foreach ($transfers as $transfer) {
            $month_transfer = $transfer->created_at->format('F');

            if (!in_array($month_transfer, $months_transfer)) {
                array_push($months_transfer, $month_transfer);
                $earnings_transfer[$month_transfer] = 0;
            }

            $earnings_transfer[$month_transfer] += $transfer->komisi;
            $total_transfer += $transfer->komisi;
        }

        $produks_hotel_success = Pengajuan::where('type', 'hotel')->where('status', 'Sudah Bayar')->where('agent_id', auth()->user()->id)->sum('komisi');
        $produks_hotel_proses = Pengajuan::where('type', 'hotel')->where('status', 'Proses' and 'status', '-')->where('agent_id', auth()->user()->id)->sum('komisi');
        $produks_xstay_success = Pengajuan::where('type', 'xstay')->where('status', 'Sudah Bayar')->where('agent_id', auth()->user()->id)->sum('komisi');
        $produks_xstay_proses = Pengajuan::where('type', 'xstay')->where('status', 'Proses' and 'status', '-')->where('agent_id', auth()->user()->id)->sum('komisi');
        $produks_rental_success = Pengajuan::where('type', 'Rental')->where('status', 'Sudah Bayar')->where('agent_id', auth()->user()->id)->sum('komisi');
        $produks_rental_proses = Pengajuan::where('type', 'Rental')->where('status', 'Proses' and 'status', '-')->where('agent_id', auth()->user()->id)->sum('komisi');
        $produks_transfer_success = Pengajuan::where('type', 'Transfer')->where('status', 'Sudah Bayar')->where('agent_id', auth()->user()->id)->sum('komisi');
        $produks_transfer_proses = Pengajuan::where('type', 'Transfer')->where('status', 'Proses' and 'status', '-')->where('agent_id', auth()->user()->id)->sum('komisi');

        return view('BE.agent.laporan-agent', compact(
            'months_hotel',
            'months_xstay',
            'months_rental',
            'months_transfer',
            'earnings_hotel',
            'earnings_xstay',
            'earnings_rental',
            'earnings_transfer',
            'total_hotel',
            'total_xstay',
            'total_rental',
            'total_transfer',
            'produks_hotel_success',
            'produks_hotel_proses',
            'produks_xstay_success',
            'produks_xstay_proses',
            'produks_rental_success',
            'produks_rental_proses',
            'produks_transfer_success',
            'produks_transfer_proses',
        ));
    }

    public function viewMyOrder()
    {
        $user = Usertraveller::where('user_id', auth()->user()->id)->first();
        $data = Detailpesanan::where('customer_id', $user->id)->where('status', '0')->get();
        $getBookingId = BookingOrder::where('customer_id', $user->id)->first();

        return view('BE.agent.my-order-agent', compact('user', 'data', 'getBookingId'));
    }

    public function viewDetailOrder($id)
    {
        $data = Detailpesanan::where('booking_code', $id)->where('status', '0')->first();
        $detail_produk = Productdetail::where('id', $data->product_detail_id)->first();
        $produk = Product::where('id', $detail_produk->product_id)->first();
        $user = User::where('id', $data->toko_id)->first();

        return view('BE.agent.detail-order', compact('data', 'detail_produk', 'produk', 'user'));
    }

    public function addUserAgent(){
        
        $agents =  User::where('role','agent')->get();
        return view('BE.agent.user-agent',compact('agents'));
    }
}
