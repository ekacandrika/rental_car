<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\CouponAgent;
use PDF;
use Thumbnail;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Mail;
use App\Models\BookingOrder;
use App\Models\ProductDetail;
use App\Models\PajakAdmin;
use App\Models\PajakLocal;
use App\Models\Detailpesanan;
use App\Models\Paket;
use App\Models\Extra;
use App\Models\Masterkamar;
use App\Models\Discount;
use App\Models\Pilihan;
use App\Models\Kupon;
use App\Models\User;
// use Illuminate\Support\Facades\DB;

class AgentOrderController extends Controller
{
    public function index(Request $request){

        // echo $request->query('product_id');
    
        $coupon = CouponAgent::where('product_id',$request->query('product_id'))->first();

        if($coupon){
            return redirect()->route('coupon.edit',$coupon->id);
        }else{

            $coupon=['id'=>null];
            $method='add';

            $detail = $this->getProduct($request->query('product_id'));
            $kamar  = $this->getInfoKamar($detail->kamar_id);
            $booking= $this->getBooking($detail->id);
            $user   = User::where('id',$booking->customer_id)->first();
            
            // dd($detail);
            // return view('BE.agent.kupon-agent',compact('coupon','method','detail','kamar','booking','user'));
            return view('BE.agent.kupon-agent',compact('coupon','method','detail','kamar','booking','user'));
        }

        //redirect ke edit jika product id sudah ada 
        // jika belum input baru
        /* $couponCount = CouponAgent::where('product_id',1);
        if($couponCount->count() > 0){
            // dd($couponCount->first());
            $data = $couponCount->first();
            // echo $data->id;
            return redirect()->route('coupon.edit',$data->id);
        } */
        // die;
        // return view('BE.agent.kupon-agent',compact('coupon','method'));
    }

    function getProduct($id){
        $product = ProductDetail::where('product_id',$id)->with('product')->first();
        
        return $product;
    }

    function getInfoKamar($id){
        return Masterkamar::where('id',$id)->first();
    }

    function getBooking($id){
        return BookingOrder::where('product_detail_id',$id)->first();
    }

    public function store(Request $request){
        
        $coupon = new CouponAgent();

        if($request->file('coupon_logo')){
            $thumbnail = $this->imgUpload($request->file('coupon_logo'));
        }

        $pesan = [
            'required'=>':attribute tidak boleh kosong'
        ];

        $this->validate($request,[
            'coupon_name'=>'required',
            'coupon_address'=>'required',
            'coupon_phone'=>'required',
            'coupon_message'=>'required',
            'coupon_list'=>'required',
            'notes'=>'required'
        ],$pesan);

        
        $coupon->coupon_name    = $request->coupon_name;
        $coupon->coupon_address = $request->coupon_address;
        $coupon->coupon_phone   = $request->coupon_phone;
        $coupon->coupon_logo    = $request->coupon_logo !=null ? $thumbnail :null;
        $coupon->coupon_message = $request->coupon_message;
        $coupon->coupon_list    = $request->coupon_list;
        $coupon->product_id     = $request->product_id;
        $coupon->notes          = $request->notes;
        $coupon->amount         = $request->amount;
        $coupon->tax_amount     = $request->tax_amount;
        $coupon->total_amount   = $request->total_amount;
        $coupon->booking_fee    = $request->booking_fee;
        $coupon->payment_notes  = $request->payment_notes;
        $coupon->user_input     = Auth::id();
        $coupon->created_at     = Carbon::now();
        
        $coupon->save();

        return redirect()->route('my-order.index');
    }

    public function imgUpload($files){
        $thumbnail = $files;
        $input['thumbnail'] =time().'.'.$thumbnail->extension();
        $destPath = ('public/thumbnail'.'/'.$input['thumbnail']);
        $img = Thumbnail::make($thumbnail->path());
        $img->resize(50,50,function($constraint){
            $constraint->aspectRatio();
        })->save(storage_path('app/public'.'/'.$input['thumbnail']));
        
        return 'storage/'.$input['thumbnail'];    
    }

    public function edit($id){
        $coupon = CouponAgent::find($id);
        $method = 'edit';
        $detail = $this->getProduct($coupon->product_id);
        $kamar  = $this->getInfoKamar($detail->kamar_id);
        $booking= $this->getBooking($detail->id);
        $user   = User::where('id',$booking->customer_id)->first();
        // dump($kamar);
        return view('BE.agent.kupon-agent',compact('coupon','method','detail','kamar','booking','user'));
    }


    public function update($id, Request $request){
        
        $coupon = CouponAgent::findOrFail($id);

        if($request->file('coupon_logo')){

           //remove img from storage 
           if(FILE::exists($coupon->coupon_logo)){
              FILE::delete($coupon->coupon_logo);
           }

           $thumbnail = $this->imgUpload($request->file('coupon_logo'));
        }
        
        $coupon->coupon_name    = $request->coupon_name;
        $coupon->coupon_address = $request->coupon_address;
        $coupon->coupon_phone   = $request->coupon_phone;
        $coupon->coupon_logo    = $request->coupon_logo !=null ? $thumbnail :null;
        $coupon->coupon_message = $request->coupon_message;
        $coupon->coupon_list    = $request->coupon_list;
        $coupon->product_id     = $request->product_id;
        $coupon->notes          = $request->notes;
        $coupon->amount         = $request->amount;
        $coupon->tax_amount     = $request->tax_amount;
        $coupon->total_amount   = $request->total_amount;
        $coupon->booking_fee    = $request->booking_fee;
        $coupon->payment_notes  = $request->payment_notes;
        $coupon->user_input     = Auth::id();
        $coupon->updated_at     = Carbon::now();

        $coupon->save();

        return redirect()->route('my-order.index');
    }

    public function destoy($id){
        $coupon =CouponAgent::findOrFail($id);
        
        if(FILE::exists($coupon->coupon_logo)){
            FILE::delete($coupon->coupon_logo);
        }
        
        $coupon->delete();
        return redirect()->route('my-order.index')->with('success','Coupon has been deleted successfully');
    }

    public function show($id){
        $data   = CouponAgent::findOrFail($id);
        $detail = $this->getProduct($data->product_id);
        $kamar  = $this->getInfoKamar($detail->kamar_id);
        $booking= $this->getBooking($detail->id);
        $user   = User::where('id',$booking->customer_id)->first();

        return view('BE.agent.kupon-agent-preview',compact('data','detail','kamar','booking','user'));
    }

    public function emailing($id){
        $coupon = CouponAgent::findOrFail($id);

        $data['email'] = "ekacandrika@gmail.com";
        $data['title']  = "Email notifikasi rencana perjalanan";
        $data["body"] = "This is Demo";

        $detail = $this->getProduct($coupon['product_id']);
        $kamar  = $this->getInfoKamar($detail['kamar_id'])->toArray();
        $booking= $this->getBooking($detail['id'])->toArray();
        $user   = User::where('id',$booking['customer_id'])->first()->toArray();

        $data['coupon'] = $coupon;
        $data['detail'] = $detail;
        $data['kamar'] = $kamar;
        $data['booking'] = $booking;
        $data['user'] = $user;

        $pdf = PDF::loadview('BE.agent.kupon-pdf',['data' => $data]);

        Mail::send('BE.agent.mails', $data,function($message) use($data, $pdf){
            $message->to($data['email'],$data['email'])->subject($data['title'])->attachData($pdf->output(),'itinerary.pdf');
        });
    }

    public function pdf($id=null){
        $coupon = CouponAgent::findOrFail($id)->toArray();

        $detail = $this->getProduct($coupon['product_id']);
        $kamar  = $this->getInfoKamar($detail['kamar_id'])->toArray();
        $booking= $this->getBooking($detail['id'])->toArray();
        $user   = User::where('id',$booking['customer_id'])->first()->toArray();

        $data['coupon'] = $coupon;
        $data['detail'] = $detail;
        $data['kamar'] = $kamar;
        $data['booking'] = $booking;
        $data['user'] = $user;

        $data = PDF::loadview('BE.agent.kupon-pdf',['data' => $data]);
        return $data->stream();
    }

    public function download($id=null){
        $coupon = CouponAgent::findOrFail($id);

        $detail = $this->getProduct($coupon['product_id']);
        $kamar  = $this->getInfoKamar($detail['kamar_id'])->toArray();
        $booking= $this->getBooking($detail['id'])->toArray();
        $user   = User::where('id',$booking['customer_id'])->first()->toArray();

        $data['coupon'] = $coupon;
        $data['detail'] = $detail;
        $data['kamar'] = $kamar;
        $data['booking'] = $booking;
        $data['user'] = $user;

        $data = PDF::loadview('BE.agent.kupon-pdf',['data' => $data]);
        return $data->download('kupon-agent'.time().'.pdf');
    }

    function clearhtmlTags($key, $string){
        
    }

    public function orders(){
        
        $data = [];
        $bookings = BookingOrder::where('customer_id',auth::id())->get()->toArray();
        $i=0;
        
        $harga_residen;

        foreach($bookings as $key => $booking){
            // dump($booking);
                $data[$i]['id_booking']=$booking['id'];
                $data[$i]['kode_booking']=$booking['booking_code'];
                $data[$i]['harga_total']=$booking['total_price'];
                $data[$i]['catatan']=$booking['catatan'];
                $data[$i]['tgl_checkot']=$booking['tgl_checkout'];
                $data[$i]['status']=$booking['status'];
                $data[$i]['status_pembayaran']=$booking['status_pembayaran'];
                $data[$i]['catatan_ekstra']=$booking['Ekstra_note'];
                $productDetails = ProductDetail::with('product')->with('harga')->with('diskon')->where('id',$booking['product_detail_id'])->get()->toArray();
                foreach($productDetails as $key => $detail){

                    $data[$i]['detail'] = $detail;
                }
            $i++;
        }
        // dd($data);
        return view('BE.agent.my-order-agent',compact('data'));
    }

    public function orderDetail($id){

        $booking = BookingOrder::where('id',$id)->first();
        $detailPesanan = DetailPesanan::where('product_detail_id',$booking->product_detail_id)->get();
        $detailProduct = ProductDetail::with('product')->with('harga')->with('diskon')->where('id',$booking->product_detail_id)->first();
        $paket = Paket::where('id',$detailProduct->paket_id)->first();        

        $detail = $detailProduct->toArray();
        $product= $detail['product'];
        $harga  = $detail['harga'];
        $diskon = $detail['diskon'];

        $extra  = Extra::where('id',$detail['extra_id'])->first();
        $diskon = Discount::where('id',$detail['discount_id'])->first();
        $pilihan= Pilihan::where('id',$detail['pilihan_id'])->first();
        $pajak_global = PajakAdmin::first();
        // $kupons = kupon::where(['user_id','=', 7])->paginate(2);
        $kupons = Kupon::where([['user_id','=',auth()->id()]])->first();
        // $pajak_lokal  = PajakLocal::where('product_type',$product['type'])->first();
        // $paket = $detail['paket'];
        // dump($pajak_lokal);
        // dd($kupons);
        return view('BE.agent.detail-order',compact('booking','detailPesanan','detailProduct','product',
                    'diskon','paket','harga','extra','diskon','pilihan','pajak_global','kupons'));
    }
}
