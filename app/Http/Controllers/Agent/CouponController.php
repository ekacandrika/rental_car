<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\CouponAgent;
use PDF;
use Thumbnail;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Mail;

class CouponController extends Controller
{
    public function index(Request $request){
        $coupon=['id'=>null];
        $method='add';

        //redirect ke edit jika product id sudah ada 
        // jika belum input baru
        /* $couponCount = CouponAgent::where('product_id',1);
        if($couponCount->count() > 0){
            // dd($couponCount->first());
            $data = $couponCount->first();
            // echo $data->id;
            return redirect()->route('coupon.edit',$data->id);
        } */
        return view('BE.agent.kupon-agent',compact('coupon','method'));
    }

    public function store(Request $request){
        
        $coupon = new CouponAgent();

        if($request->file('coupon_logo')){
            $thumbnail = $this->imgUpload($request->file('coupon_logo'));
        }

        $pesan = [
            'required'=>':attribute tidak boleh kosong'
        ];

        $this->validate($request,[
            'coupon_name'=>'required',
            'coupon_address'=>'required',
            'coupon_phone'=>'required',
            'coupon_message'=>'required',
            'coupon_list'=>'required',
            'notes'=>'required'
        ],$pesan);

        
        $coupon->coupon_name    = $request->coupon_name;
        $coupon->coupon_address = $request->coupon_address;
        $coupon->coupon_phone   = $request->coupon_phone;
        $coupon->coupon_logo    = $request->coupon_logo !=null ? $thumbnail :null;
        $coupon->coupon_message = $request->coupon_message;
        $coupon->coupon_list    = $request->coupon_list;
        $coupon->product_id     = $request->product_id;
        $coupon->notes          = $request->notes;
        $coupon->amount         = $request->amount;
        $coupon->tax_amount     = $request->tax_amount;
        $coupon->total_amount   = $request->total_amount;
        $coupon->booking_fee    = $request->booking_fee;
        $coupon->payment_notes  = $request->payment_notes;
        $coupon->user_input     = Auth::id();
        $coupon->created_at     = Carbon::now();
        
        $coupon->save();

        return redirect()->route('my-order.index');
    }

    public function imgUpload($files){
        $thumbnail = $files;
        $input['thumbnail'] =time().'.'.$thumbnail->extension();
        $destPath = ('public/thumbnail'.'/'.$input['thumbnail']);
        $img = Thumbnail::make($thumbnail->path());
        $img->resize(50,50,function($constraint){
            $constraint->aspectRatio();
        })->save(storage_path('app/public'.'/'.$input['thumbnail']));
        
        return 'storage/'.$input['thumbnail'];    
    }

    public function edit($id){
        $coupon =CouponAgent::find($id);
        $method='edit';
        
        return view('BE.agent.kupon-agent',compact('coupon','method'));
    }


    public function update($id, Request $request){
        
        $coupon = CouponAgent::findOrFail($id);

        if($request->file('coupon_logo')){

           //remove img from storage 
           if(FILE::exists($coupon->coupon_logo)){
              FILE::delete($coupon->coupon_logo);
           }

           $thumbnail = $this->imgUpload($request->file('coupon_logo'));
        }
        
        $coupon->coupon_name    = $request->coupon_name;
        $coupon->coupon_address = $request->coupon_address;
        $coupon->coupon_phone   = $request->coupon_phone;
        $coupon->coupon_logo    = $request->coupon_logo !=null ? $thumbnail :null;
        $coupon->coupon_message = $request->coupon_message;
        $coupon->coupon_list    = $request->coupon_list;
        $coupon->product_id     = $request->product_id;
        $coupon->notes          = $request->notes;
        $coupon->amount         = $request->amount;
        $coupon->tax_amount     = $request->tax_amount;
        $coupon->total_amount   = $request->total_amount;
        $coupon->booking_fee    = $request->booking_fee;
        $coupon->payment_notes  = $request->payment_notes;
        $coupon->user_input     = Auth::id();
        $coupon->updated_at     = Carbon::now();

        $coupon->save();

        return redirect()->route('my-order.index');
    }

    public function destoy($id){
        $coupon =CouponAgent::findOrFail($id);
        
        if(FILE::exists($coupon->coupon_logo)){
            FILE::delete($coupon->coupon_logo);
        }
        
        $coupon->delete();
        return redirect()->route('my-order.index')->with('success','Coupon has been deleted successfully');
    }

    public function show($id){
        $data =CouponAgent::findOrFail($id);
        return view('BE.agent.kupon-agent-preview',compact('data'));
    }

    public function emailing($id){
        $coupon = CouponAgent::findOrFail($id);

        $data['email'] = "ekacandrika@gmail.com";
        $data['title']  = "Email notifikasi rencana perjalanan";
        $data["body"] = "This is Demo";

        $pdf = PDF::loadview('BE.agent.kupon-pdf',['data' => $coupon]);

        Mail::send('BE.agent.mails', $data,function($message) use($data, $pdf){
            $message->to($data['email'],$data['email'])->subject($data['title'])->attachData($pdf->output(),'itinerary.pdf');
        });
    }

    public function pdf($id=null){
        $coupon = CouponAgent::findOrFail($id);

        $data = PDF::loadview('BE.agent.kupon-pdf',['data' => $coupon]);
        return $data->stream();
    }

    public function download($id=null){
        $coupon = CouponAgent::findOrFail($id);

        $data = PDF::loadview('BE.agent.kupon-pdf',['data' => $coupon]);
        return $data->download('itineray'.time().'.pdf');
    }

    function clearhtmlTags($key, $string){
        
    }
}
