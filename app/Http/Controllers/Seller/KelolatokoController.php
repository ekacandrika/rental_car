<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\Kelolatoko;
use App\Models\BannerToko;
use App\Models\Allbanner;
use App\Models\TemporaryFile;
use App\Models\User;
use App\Models\Product;
use App\Models\Productdetail;
use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use App\Models\Village;
use App\Models\BranchStore;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class KelolatokoController extends Controller
{
    public function index()
    {
        $user_id = Auth::user()->id;
        $dekorasi = user::where('id', $user_id)->with('kelolatoko')->first();
        $toko = Kelolatoko::where('id_seller', $dekorasi->id)->first();
        $banner = Allbanner::where('user_id', $dekorasi->id)->first();

        $allbanner = Allbanner::where('user_id', $dekorasi->id)->first();

        $bannerone = BannerToko::where('display_order', 1)->get();
        $bannertwo = BannerToko::where('display_order', 2)->get();
        $bannerthree = BannerToko::where('display_order', 3)->get();
        $bannerfour = BannerToko::where('display_order', 4)->get();
        $bannerfive = BannerToko::where('display_order', 5)->get();
        
        // dd($allbanner);

        if ($allbanner != null) {
            $bannerone   = BannerToko::where('display_order', 1)->where('all_banner',$allbanner->id)->get();
            $bannertwo   = BannerToko::where('display_order', 2)->where('all_banner',$allbanner->id)->get();
            $bannerthree = BannerToko::where('display_order', 3)->where('all_banner',$allbanner->id)->get();
            $bannerfour  = BannerToko::where('display_order', 4)->where('all_banner',$allbanner->id)->get();
            $bannerfive  = BannerToko::where('display_order', 5)->where('all_banner',$allbanner->id)->get();
            $bannersix  = BannerToko::where('display_order', 6)->where('all_banner',$allbanner->id)->get();

        } else {
            $allbanner = '';
            $bannerone = '';
            $bannertwo = '';
            $bannerthree = '';
            $bannerfour = '';
            $bannerfive = '';
            $bannersix = '';
        }

        // dd($toko);
        return view('BE.seller.dekorasi-toko', compact('dekorasi', 'toko', 'allbanner', 'banner', 'bannerone', 'bannertwo', 'bannerthree', 'bannerfour', 'bannerfive','bannersix'));
    }

    public function dekorasiToko(Request $request)
    {

        $dekorasi = '';

        $detail_seller = User::join('user_detail','user_detail.user_id','=','users.id')->where('users.role','seller')->get();

        // dd($detail_seller);

        if ($request->session()->has('kelolatoko.dekorasi')) {
            $dekorasi = $request->session()->get('kelolatoko.dekorasi');
        } else {
            $request->session()->put('kelolatoko.dekorasi', '');
            $dekorasi = '';
        }

        $provinsi = Province::all();

        return view('BE.seller.dekorToko', compact('dekorasi','detail_seller','provinsi'));
    }

    public function addDekorasiToko(Request $request)
    {
        // dd($request->all());
        $logo_toko = $request->session()->has('kelolatoko.dekorasi.logo_toko') ? $request->session()->get('kelolatoko.dekorasi.logo_toko') : '';

        if ($request->hasFile('logo_toko')) {
            $image = $request->file('logo_toko');

            $new_foto = time() . 'GalleryLogoToko' . $image->getClientOriginalName();
            $tujuan_upload = 'Seller/Product/Dekorasi/';

            $lebar_foto = Image::make($image)->width();
            $lebar_foto = $lebar_foto * 50 / 100;

            Image::make($image)->resize($lebar_foto, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($tujuan_upload . $new_foto);

            // $image->move($tujuan_upload, $new_foto);
            $logo_toko = $tujuan_upload . $new_foto;

            //delete old image
            if (file_exists($request->session()->get('kelolatoko.dekorasi.logo_toko'))) {
                unlink($request->session()->get('kelolatoko.dekorasi.logo_toko'));
            }
        }

        $banner_toko = $request->session()->has('kelolatoko.dekorasi.banner_toko') ? $request->session()->get('kelolatoko.dekorasi.banner_toko') : '';

        if ($request->hasFile('banner_toko')) {
            $image = $request->file('banner_toko');

            $new_foto = time() . 'GalleryBannerToko' . $image->getClientOriginalName();
            $tujuan_upload = 'Seller/Product/Dekorasi/';

            $lebar_foto = Image::make($image)->width();
            $lebar_foto = $lebar_foto * 50 / 100;

            Image::make($image)->resize($lebar_foto, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($tujuan_upload . $new_foto);

            // $image->move($tujuan_upload, $new_foto);
            $banner_toko = $tujuan_upload . $new_foto;

            //delete old image
            if (file_exists($request->session()->get('kelolatoko.dekorasi.banner_toko'))) {
                unlink($request->session()->get('kelolatoko.dekorasi.banner_toko'));
            }
        }

        $request->session()->put('kelolatoko.dekorasi.nama_toko', $request->nama_toko);
        $request->session()->put('kelolatoko.dekorasi.nama_banner', $request->nama_banner);
        $request->session()->put('kelolatoko.dekorasi.banner_toko', $banner_toko);
        $request->session()->put('kelolatoko.dekorasi.logo_toko', $logo_toko);

        $user_id = Auth::user()->id;
        
        if($request->tentang_toko){

            $deskripsi = $request->tentang_toko;
            $dom = New \DomDocument();
            @$dom->loadHtml($deskripsi,LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $imgFile = $dom->getElementsByTagName('imageFIle');

            foreach($imgFile as $item =>$image){
                $data = $img->get->Attribute('src');
                list($type, $data)=eplode(';',$data);
                list(,$data) = explode(',',$data);

                $imgData = base64_decode($data);
                $image_name = "/upload/".time().$item.'.png';
                $path = public_path().$image_name;
                file_put_contents($path, $imgData);

                $image->removeAttribute('src');
                $image->setAttribute('src',$image_name);
            }
        }

        if($request->official_store){
            $data_branchs = collect([]);
            
            foreach($request->province_id as $key =>$val){
                $data_branchs->push([
                    'id'=>$request->id[$key],
                    'address'=>$request->address[$key],
                    'province_id'=>$request->province_id[$key],
                    'kabupaten_id'=>$request->regency_id[$key],
                    'kecamatan_id'=>$request->district_id[$key],
                    'kelurahan_id'=>$request->village_id[$key],
                ]);
            }

            $this->branchStore($data_branchs,$user_id);
        }
        // die;
        $kelola = Kelolatoko::create([
            'id_seller' => $user_id,
            'nama_toko' => $request->session()->get('kelolatoko.dekorasi.nama_toko'),
            'logo_toko' => $request->session()->get('kelolatoko.dekorasi.logo_toko'),
            'banner_toko' => $request->session()->get('kelolatoko.dekorasi.banner_toko'),
            'nama_banner' => $request->session()->get('kelolatoko.dekorasi.nama_banner'),
            'deskripsi' => $request->deskripsi,
            'tentang_toko' => $request->tentang_toko !=null ? $deskripsi:null,
            'deskripsi_tambahan' => $request->deskripsi_tambahan,
            'lokasi_toko' => $request->lokasi_toko,
            'official_store'=>$request->official_store != null ? $request->official_store :'un official'
        ]);

        $banner_recent = Allbanner::where('type', 'ok')->orderBy('id', 'desc')->first();
        $code = isset($banner_recent) ? $banner_recent->banner_code : 1;
        
        if ($code != null) {
            if($code != 1){
                $code = explode("BC", $code);
                $code = substr($code[1], 0, -8);
                $code++;
            }    
        }else{
            $code = "BC1";
            $code = explode('BC',$code);
            
            $code =substr($code[1],0,-8);
            $code++;
        }

        // dd($code);

        $allBanner = new Allbanner;


        $allBanner::create([
            'type'=>'ok',
            'user_id'=>$user_id 
        ]);
        
        $banner_one = "";
        $banner_two = "";
        $banner_three = "";
        $banner_four = "";
        $banner_five = "";
        $banner_six = "";

        $id = $allBanner::select('id')->where('type','ok')->where('user_id',$user_id)->first();
        $allBanner::where('id',$id->id)->update([
            'banner_code'=>$id->id.'BC' . $code . date('dmY')
        ]);
        
        $id_all_bannner = $id->id;

        if($request->hasFile('banner_one')){
            
            $img_one = $request->file('banner_one');

            $name_img_one = time().'_GalleryBanner_'.$img_one->getClientOriginalName();
            $tujuan_upload = 'Seller/Product/Dekorasi/';

            $lebarFoto = Image::make($img_one)->width();
            
            $banner_one = $tujuan_upload.$name_img_one;

            Image::make($img_one)->resize($lebarFoto,null,function($constraint){
                $constraint->aspectRatio();
            })->save($banner_one);

            BannerToko::create([
                'all_banner'=>$id_all_bannner,
                'banner'=>$banner_one,
                'link_banner'=>$request->link_banner_one,
                'display_order'=>1,
            ]);
        }


        if($request->hasFile('banner_two')){

            $img_two = $request->file('banner_two');

            $name_img_two = time().'_GalleryBanner_'.$img_two->getClientOriginalName();
            $tujuan_upload = 'Seller/Product/Dekorasi/';

            $lebarFoto = Image::make($img_two)->width();

            $banner_two = $tujuan_upload.$name_img_two;

            Image::make($img_two)->resize($lebarFoto,null,function($constraint){
                $constraint->aspectRatio();
            })->save($banner_two);

            BannerToko::create([
                'all_banner'=>$id_all_bannner,
                'banner'=>$banner_two,
                'link_banner'=>$request->link_banner_two,
                'display_order'=>2,
            ]);
        }

        if($request->hasFile('banner_three')){

            $img_three = $request->file('banner_three');

            $name_img_three = time().'_GalleryBanner_'.$img_three->getClientOriginalName();
            $tujuan_upload = 'Seller/Product/Dekorasi/';

            $path_three = $tujuan_upload.$name_img_three;

            $lebarFoto = Image::make($img_three)->width();

            $banner_three = $tujuan_upload.$name_img_three;

            Image::make($img_three)->resize($lebarFoto,null,function($constraint){
                $constraint->aspectRatio();
            })->save($banner_three);

            BannerToko::create([
                'all_banner'=>$id_all_bannner,
                'banner'=>$banner_three,
                'link_banner'=>$request->link_banner_three,
                'display_order'=>3,
            ]);

        }
        
        if($request->hasFile('banner_four')){

            $img_four = $request->file('banner_four');

            $name_img_four = time().'_GalleryBanner_'.$img_four->getClientOriginalName();
            $tujuan_upload = 'Seller/Product/Dekorasi/';

            $lebarFoto = Image::make($img_four)->width();

            $banner_four = $tujuan_upload.$name_img_four;

            Image::make($img_four)->resize($lebarFoto,null,function($constraint){
                $constraint->aspectRatio();
            })->save($banner_four);

            BannerToko::create([
                'all_banner'=>$id_all_bannner,
                'banner'=>$banner_four,
                'link_banner'=>$request->link_banner_four,
                'display_order'=>4,
            ]);
        }

        if($request->hasFile('banner_five')){

            $img_five = $request->file('banner_five');

            $name_img_five = time().'_GalleryBanner_'.$img_five->getClientOriginalName();
            $tujuan_upload = 'Seller/Product/Dekorasi/';

            $lebarFoto = Image::make($img_five)->width();

            $banner_five = $tujuan_upload.$name_img_five;

            Image::make($img_five)->resize($lebarFoto,null,function($constraint){
                $constraint->aspectRatio();
            })->save($banner_five);

            BannerToko::create([
                'all_banner'=>$id_all_bannner,
                'banner'=>$banner_five,
                'link_banner'=>$request->link_banner_five,
                'display_order'=>5,
            ]);

        }
        
        if($request->hasFile('banner_six')){

            $img_five = $request->file('banner_six');

            $name_img_five = time().'_GalleryBanner_'.$img_five->getClientOriginalName();
            $tujuan_upload = 'Seller/Product/Dekorasi/';

            $lebarFoto = Image::make($img_five)->width();

            $banner_six = $tujuan_upload.$name_img_five;

            Image::make($img_five)->resize($lebarFoto,null,function($constraint){
                $constraint->aspectRatio();
            })->save($banner_six);

            BannerToko::create([
                'all_banner'=>$id_all_bannner,
                'banner'=>$banner_six,
                'link_banner'=>$request->link_banner_six,
                'display_order'=>6,
            ]);

        }

        $request->session()->forget('kelolatoko');

        return redirect()->route('kelolatoko');
    }

    public function viewBannerToko(Request $request)
    {

        $user_id = Auth::user()->id;
        
        $allbanner = Allbanner::where('user_id',$user_id)->first();

        if ($request->session()->missing('allbanner.banner_code')) {
            $user_id = Auth::user()->id;

            $banner_recent = Allbanner::where('type', 'ok')->orderBy('id', 'desc')->first();
            $code = $banner_recent->banner_code !=null ? $banner_recent->banner_code : 1;
            
            // dd($banner_recent);
            
            if ($code != 1) {
                $code = explode("BC", $code);
                $code = substr($code[1], 0, -8);
                $code++;
            }

            if(!$banner_recent){
                $allbanner = Allbanner::create([
                    'user_id' => $user_id,
                    'type' => 'ok',
                ]);    
            }

            $request->session()->put('allbanner.banner.banner_id', $allbanner->id);
            $banner_id = Allbanner::where('id', $allbanner->id)->first();

            $banner_code = $allbanner->id . 'BC' . $code . date('dmY');

            $banner_id->update([
                'user_id' => $user_id,
                'type' => 'ok',
                'banner_code' => $banner_code,
            ]);

            // $banners = BannerToko::where

            $request->session()->put('allbanner.banner_code', $banner_code);
        }

        $allbanner_id = $request->session()->get('allbanner.banner_code');
        $banner = Allbanner::where('user_id', auth()->user()->id)->where('type', 'ok')->first();
        $banners = BannerToko::where('all_banner',$allbanner->id)->get();

        // dd($banners);

        return view('BE.seller.banner-toko', compact('allbanner_id', 'banner','banners'));
    }

    public function addBannerToko(Request $request)
    {
        $galleryImage = new BannerToko();
        $BannerToko = $galleryImage::where('all_banner',$request->all_banner);
        
        if($BannerToko->count() > 0){
            if($request->link_banner){
                $i =0;
                foreach ($request->link_banner as $lb) {
                    $arr_banner = $BannerToko->get()->toArray();
                    $id = $arr_banner[$i]['id'];
                    
                    $galleryImage->where('id','=',$id)->update([
                        'link_banner'=>$lb
                    ]);

                    $i++;
                }
            }

            if (request('gallery-image')) {
                $x = 0;
                $i = 1;
                $do = 1;
                
                foreach ($request->file('gallery-image') as $thisImage) {
                 
                    $arr_banner = $BannerToko->get()->toArray();
                    $id = $arr_banner[$x]['id'];

                    $fileNameWithExtension = $thisImage->getClientOriginalName();
                    $memberNameFolder = 'banner';
                    
                    $extension = $thisImage->getClientOriginalName();
                    $fileNameToStore = $i++ . '_' . time() . '_GalleryBanner_' . $extension;
                    $tujuan_upload = 'Seller/Product/Dekorasi/';
    
                    if (file_exists($tujuan_upload . $fileNameToStore)) {
                        unlink($tujuan_upload . $fileNameToStore);
                    }

                    $lebar_foto = Image::make($thisImage)->width();
    
                    Image::make($thisImage)->resize($lebar_foto, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($tujuan_upload . $fileNameToStore);
    
                    $path = $tujuan_upload . $fileNameToStore;
    

                    $galleryImage->where('id','=',$id)->update([
                        'link_banner'=>$request->link_banner[$x],
                        'banner'=>$path,
                        'display_order'=>$do++
                    ]);

                    $x++;
                }
            }
            
        }else{

            if (request('gallery-image')) {
                $i  = 1;
                $do = 1;
                $x  = 0;
                foreach ($request->file('gallery-image') as $thisImage) {
                    $fileNameWithExtension = $thisImage->getClientOriginalName();
                    $memberNameFolder = 'banner';

                    $extension = $thisImage->getClientOriginalName();
                    $fileNameToStore = $i++ . '_' . time() . '_GalleryBanner_' . $extension;
                    $tujuan_upload = 'Seller/Product/Dekorasi/';

                    $lebar_foto = Image::make($thisImage)->width();

                    Image::make($thisImage)->resize($lebar_foto, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($tujuan_upload . $fileNameToStore);

                    $path = $tujuan_upload . $fileNameToStore;

                    $galleryImage = new BannerToko();
                    $galleryImage->all_banner = request('all_banner');
                    $galleryImage->banner = $path;
                    $galleryImage->link_banner = $request->link_banner[$x++];
                    $galleryImage->display_order = $do++;

                    $galleryImage->save();
                }
            }
        }
        
        return redirect()->route('kelolatoko');
    }

    public function reorder(Request $request)
    {
        $request = request();
        $index = 1;
        $data = $request['data'];
        if (count($data)) {
            for ($i = 0; $i < count($data); $i++) {
                $sport = BannerToko::find($data[$i]);
                if ($index <= count($data)) {
                    $sport->display_order = $index;
                    $sport->save();
                }
                $index++;
            }
        }
        return TRUE;
    }

    public function showDekorasiToko(Request $request, $id)
    {
        $data = DB::table('kelolatokos')->where('id', $id)->get();
        // dd($data);
        $branch_stores = BranchStore::where('user_id',$data[0]->id_seller)->get();

        $collect_branch = collect([]);

        foreach($branch_stores as $key => $branch){

            // get province
            $propinsi  = Province::where('id',$branch->province_id)->first();
            $kabupaten = Regency::where('id',$branch->regency_id)->first();
            $kecamatan = District::where('id',$branch->district_id)->first();
            $kelurahan = Village::where('id',$branch->village_id)->first();

            $collect_branch->push([
                'id'=>$branch->id,
                'alamat'=>$branch->address,
                'provinsi_id'=>$branch->province_id,
                'provinsi'=>$propinsi->name,
                'kabupaten_id'=>$branch->regency_id,
                'kabupaten'=>$kabupaten->name,
                'kecamatan_id'=>$branch->district_id,
                'kecamatan'=>$kecamatan->name,
                'kelurahan_id'=>$branch->village_id,
                'kelurahan'=>$kelurahan->name,
            ]);
        }

        // dd($collect_branch);

        $dekorasi = '';

        if ($request->session()->has('kelolatoko.dekorasi')) {
            $toko = $request->session()->get('kelolatoko.dekorasi');
        } else {
            $request->session()->put('kelolatoko.dekorasi', '');
            $toko = '';
        }

        $provinsi = Province::all();

        return view('BE.seller.editDekorToko', compact('data', 'dekorasi','id','provinsi','collect_branch'));
    }

    public function editDekorasiToko(Request $request, $id)
    {
        // dd($request->all());
        $user_id = Auth::user()->id;

        // $detail = User::where([
        //     'id' => $user_id,
        // ])->has('kelolatoko')->with('kelolatoko')->whereHas('kelolatoko',function($q) use($id){
        //     $q->where('id',$id);
        // })->first();

        $detail = Kelolatoko::where('id',$id)->with('user')->whereHas('user',function($q) use ($user_id){
            $q->where('id',$user_id);
        })->first();
        
        // dd($request->all());
        
        if ($request->hasFile('logo_toko')) {
            $image = $request->file('logo_toko');

            $new_foto = time() . 'GalleryLogoToko' . $image->getClientOriginalName();
            $tujuan_upload = 'Seller/Product/Dekorasi/';

            $lebar_foto = Image::make($image)->width();
            $lebar_foto = $lebar_foto * 50 / 100;

            Image::make($image)->resize($lebar_foto, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($tujuan_upload . $new_foto);

            // $image->move($tujuan_upload, $new_foto);
            $logo_toko = $tujuan_upload . $new_foto;

            $request->session()->put('kelolatoko.dekorasi.logo_toko',$logo_toko);

            //delete old image
            if (file_exists($detail->logo_toko)) {
                unlink($detail->logo_toko);
            }
        }

        $logo_toko = $request->session()->has('kelolatoko.dekorasi.logo_toko') ?$request->session()->get('kelolatoko.dekorasi.logo_toko') : $detail->logo_toko;

        if ($request->hasFile('banner_toko')) {
            $image = $request->file('banner_toko');

            $new_foto = time() . 'GalleryBannerToko' . $image->getClientOriginalName();
            $tujuan_upload = 'Seller/Product/Dekorasi/';

            $lebar_foto = Image::make($image)->width();
            $lebar_foto = $lebar_foto * 50 / 100;

            Image::make($image)->resize($lebar_foto, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($tujuan_upload . $new_foto);

            $image->move($tujuan_upload, $new_foto);
            $banner_toko = $tujuan_upload . $new_foto;

            $request->session()->put('kelolatoko.dekorasi.banner_toko',$banner_toko);
            
            //delete old image
            if (file_exists($detail->banner_toko)) {
                unlink($detail->banner_toko);
            }
        }

        $banner_toko = $request->session()->has('kelolatoko.dekorasi.banner_toko') ? $request->session()->get('kelolatoko.dekorasi.banner_toko') : $detail->banner_toko;

        if($request->tentang_toko){

            $deskripsi = $request->tentang_toko;
            $dom = New \DomDocument();
            @$dom->loadHtml($deskripsi,LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $imgFile = $dom->getElementsByTagName('imageFIle');

            foreach($imgFile as $item =>$image){
                $data = $img->get->Attribute('src');
                list($type, $data)=eplode(';',$data);
                list(,$data) = explode(',',$daata);

                $imgData = base64_decode($data);
                $image_name = "/upload/".time().$item.'.png';
                $path = public_path().$image_name;
                file_put_contents($path, $imgData);

                $image->removeAttribute('src');
                $image->setAttribute('src',$image_name);
            }
        }

        if($request->official_store){
            $data_branchs = collect([]);
            
            foreach($request->province_id as $key =>$val){
                $data_branchs->push([
                    'id'=>$request->id[$key],
                    'address'=>$request->address[$key],
                    'province_id'=>$request->province_id[$key],
                    'kabupaten_id'=>$request->regency_id[$key],
                    'kecamatan_id'=>$request->district_id[$key],
                    'kelurahan_id'=>$request->village_id[$key],
                ]);
            }

            $this->branchStore($data_branchs,$user_id);
        }
        // die;
        DB::table('kelolatokos')->where('id', $id)->update([
            'nama_toko' => $request->nama_toko,
            'logo_toko' => $logo_toko,
            'nama_banner' => $request->nama_banner,
            'banner_toko' => $banner_toko,
            'deskripsi'=>$request->deskripsi,
            'deskripsi_tambahan'=>$request->deskripsi_tambahan != null ? $deskripsi:null,
            'tentang_toko'=>$request->tentang_toko,
            'lokasi_toko'=>$request->lokasi_toko,
            'official_store'=>$request->official_store != null ? $request->official_store :'un official'
        ]);

        $request->session()->forget('kelolatoko');

        return redirect()->route('kelolatoko');
    }

    public function branchStore($data, $user_id){
        // dd($data);
        foreach($data as $key => $d){
            // dump($d);
            $branch = BranchStore::where('id',$d['id'])->where('user_id',$user_id)->get();
            if($branch->count() > 0){
                // dump($branch);
                BranchStore::where('id',$d['id'])->update([
                    'province_id'=>$d['province_id'],
                    'regency_id'=>$d['kabupaten_id'],
                    'district_id'=>$d['kecamatan_id'],
                    'village_id'=>$d['kelurahan_id'],
                    'address'=>$d['address'],
                    'user_id'=>$user_id,
                ]);
                
            }else{
                BranchStore::create([
                    'province_id'=>$d['province_id'],
                    'regency_id'=>$d['kabupaten_id'],
                    'district_id'=>$d['kecamatan_id'],
                    'village_id'=>$d['kelurahan_id'],
                    'address'=>$d['address'],
                    'user_id'=>$user_id,
                ]) ;
                // echo 'Jest';
            }
        }
        // die;
    }

    public function updateDekorasiToko(Request $request,$id){

    }

    public function updateStatus(Request $request)
    {
//        $data = Product::with('productdetail')->findOrFail($id);

//        $data->productdetail->update([
//            'confirmation' => $request->confirmation
//        ]);

//        $data->productdetail->update([
//            'available' => $request->available
//        ]);
//        dd($data);

//        $update = Productdetail::where('product_id', $request->id)->first();

        $update = Productdetail::find($request->id);
        $update->available = $request->available;
        $update->save();

        return redirect()->back()->with('success', 'Status Listing Sudah Diupdate');
    }

    public function deleteBanner($id){
        $banners = BannerToko::findOrFail($id);
        
        if (file_exists($banners->banner)) {
            unlink($banners->banner);
        }

        $banners->delete();

        return response()->json([
            'success'=>true,
            'message'=>'Banner berhasil dihapus',
            'status'=>'success'
        ]);
    }
}
