<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\Regency;
use App\Models\Productdetail;
use App\Models\Discount;
use App\Models\Price;
use App\Models\Masterkamar;
use App\Models\Product;
use App\Models\Attributes;
use Attribute;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class XstayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $code = 'XSTY' . mt_rand(1, 99) . date('dmY');
        $regency = Regency::all();
        $attributes = Attributes::get();

        return view('BE.seller.xstay.informasidasar', compact('hotel', 'xstay', 'regency', 'code', 'attributes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_name' => 'required',
            'confirmation' => 'required',
            'regency_id' => 'required',
            'alamat' => 'required',
            'fasilitas_id' => 'required',
            'fasilitas_id.*' => 'required',
            'amenitas_id' => 'required',
            'amenitas_id.*' => 'required',
            // 'icon_amenitas' => 'required',
            // 'icon_amenitas.*' => 'mimes:jpeg,png,jpg,ico|max:1048',
            // 'judul_amenitas' => 'required',
            // 'judul_amenitas.*' => 'required',
            // 'isi_amenitas' => 'required',
            // 'isi_amenitas.*' => 'required',
            'deskripsi' => 'required',
            // 'catatan' => 'required',
        ], [
            'product_name.required' => 'Judul wajib diisi!',
            'confirmation.required' => 'Konfirmasi paket wajib diisi!',
            'regency_id.required' => 'Tag lokasi wajib diisi!',
            'alamat.required' => 'Alamat wajib diisi!',
            'fasilitas_id' => 'Fasilitas wajib diisi!',
            'fasilitas_id.*' => 'Fasilitas wajib diisi!',
            'amenitas_id' => 'Amenitas wajib diisi!',
            'amenitas_id.*' => 'Amenitas wajib diisi!',
            // 'icon_fasilitas.required' => 'Icon fasilitas wajib diisi!',
            // 'icon_fasilitas.*.required' => 'Icon fasilitas wajib diisi!',
            // 'icon_fasilitas.*.mimes' => 'Icon fasilitas harus memiliki ekstensi (jpg, jpeg, png, atau ico)',
            // 'icon_fasilitas.*.max' => 'Icon fasilitas maksimum berukuran 1MB',
            // 'deskripsi_fasilitas.required' => 'Deskripsi wajib diisi!',
            // 'deskripsi_fasilitas.*.required' => 'Deskripsi wajib diisi!',
            // 'icon_amenitas.required' => 'Icon amenitas wajib diisi!',
            // 'icon_amenitas.*.required' => 'Icon amenitas wajib diisi!',
            // 'icon_amenitas.*.mimes' => 'Icon amenitas harus memiliki ekstensi (jpg, jpeg, png, atau ico)',
            // 'icon_amenitas.*.max' => 'Icon amenitas maksimum berukuran 1MB',
            // 'judul_amenitas.required' => 'Judul amenitas wajib diisi!',
            // 'judul_amenitas.*.required' => 'Judul amenitas wajib diisi!',
            // 'isi_amenitas.required' => 'Isi amenitas wajib diisi!',
            // 'isi_amenitas.*.required' => 'Isi amenitas wajib diisi!',
            'deskripsi.required' => 'Deskripsi wajib diisi!',
            // 'catatan.required' => 'Catatan wajib diisi!',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $fasilitas_id = ['result' => $request->fasilitas_id];
        // foreach ($request->icon_fasilitas as $value) {
        //     $new_icon_fasilitas = time() . 'iconfasilitas' . $value->getClientOriginalName();
        //     $tujuan_uploud_icon_fasilitas = 'IconFasilitas/';
        //     $value->move($tujuan_uploud_icon_fasilitas, $new_icon_fasilitas);

        //     array_push($images_fasilitas, $tujuan_uploud_icon_fasilitas . $new_icon_fasilitas);
        // }

        $amenitas_id = ['result' => $request->amenitas_id];
        // foreach ($request->icon_amenitas as $result) {
        //     $new_icon_amenitas = time() . 'iconamenitas' . $result->getClientOriginalName();
        //     $tujuan_uploud_icon_amenitas = 'IconAmenitas/';
        //     $result->move($tujuan_uploud_icon_amenitas, $new_icon_amenitas);

        //     array_push($images_amenitas, $tujuan_uploud_icon_amenitas . $new_icon_amenitas);
        // }

        // Fasilitas
        // $icon_fasilitas = ['result' => $images_fasilitas];
        // $deskripsi = ['result' => $request->deskripsi_fasilitas];

        // // Amenitas
        // $icon_amenitas = ['result' => $images_amenitas];
        // $judul_amenitas = ['result' => $request->judul_amenitas];
        // $isi_amenitas = ['result' => $request->isi_amenitas];

        // save to Product database
        $product = new Hotel;
        $product->user_id = auth()->user()->id;
        $product->product_code = $request->product_code;
        $product->product_name = $request->product_name;
        $product->type = "xstay";
        $product->save();

        // save to Product Detail database
        $product_detail = new Productdetail;
        $product_detail->product_id = $product->id;
        $product_detail->regency_id = $request->regency_id;
        $product_detail->alamat = $request->alamat;
        $product_detail->confirmation = $request->confirmation;
        // $product_detail->icon_fasilitas = json_encode($icon_fasilitas, true);
        // $product_detail->deskripsi_fasilitas = json_encode($deskripsi, true);
        // $product_detail->icon_amenitas = json_encode($icon_amenitas, true);
        // $product_detail->judul_amenitas = json_encode($judul_amenitas, true);
        $product_detail->video_embed = $request->video_embed;
        $product_detail->id_fasilitas = json_encode($fasilitas_id, true);
        $product_detail->id_amenitas = json_encode($amenitas_id, true);
        $product_detail->deskripsi = $request->deskripsi;
        $product_detail->catatan = $request->catatan;
        $product_detail->base_url = url('/') . '/xstay/' . $product->slug;
        $product_detail->save();

        return redirect()->route('xstay.infokamar.edit', $product->product_code);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Hotel::with('productdetail')->where('product_code', $id)->first();
        $regency = Regency::all();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $product_detail = Productdetail::where('product_id', $data->id)->first();
        $json = json_decode($product_detail, true);
        $attributes = Attributes::get();

        $fasilities = isset($product_detail->id_fasilitas) ? json_decode($product_detail->id_fasilitas, true)['result'] : [];
        $array_fasilitas = collect([]);

        $amenities = isset($product_detail->id_amenitas) ? json_decode($product_detail->id_amenitas, true)['result'] : [];
        $array_amenitas = collect([]);

        // Fasilitas
        $array = json_decode($json['id_fasilitas'], true);  
        $result['result'] = $array['result'];
        
        for($i=0; $i < count($fasilities); $i++){
            $id_fasilitas = Attributes::where('id',$fasilities[$i])->first();

            $array_fasilitas->push([
                'fasilitas_id'=>$id_fasilitas->id,
                'fasilitas_name'=>$id_fasilitas->text,
                'fasilitas_img'=>$id_fasilitas->img,
            ]);
        }

        // Amenitas
        $array1 = json_decode($json['id_amenitas'], true);
        $val['result'] = $array1['result'];

        for($i=0; $i < count($amenities); $i++){
            $id_amenitas = Attributes::where('id',$amenities[$i])->first();

            $array_amenitas->push([
                'amenitas_id'=>$id_amenitas->id,
                'amenitas_name'=>$id_amenitas->text,
                'amenitas_img'=>$id_amenitas->img,
            ]);
        }
        
        // dd($result);
        return view('BE.seller.xstay.informasidasar_edit', compact('data', 'regency', 'hotel', 'xstay', 'result', 'val','attributes','array_fasilitas','array_amenitas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            'product_name' => 'required',
            'confirmation' => 'required',
            'regency_id' => 'required',
            'alamat' => 'required',
            'fasilitas_id' => 'required',
            'fasilitas_id.*' => 'required',
            'amenitas_id' => 'required',
            'amenitas_id.*' => 'required',
            // 'icon_amenitas' => 'required',
            // 'icon_amenitas.*' => 'mimes:jpeg,png,jpg,ico|max:1048',
            // 'judul_amenitas' => 'required',
            // 'judul_amenitas.*' => 'required',
            // 'isi_amenitas' => 'required',
            // 'isi_amenitas.*' => 'required',
            'deskripsi' => 'required',
            // 'catatan' => 'required',
        ], [
            'product_name.required' => 'Judul wajib diisi!',
            'confirmation.required' => 'Konfirmasi paket wajib diisi!',
            'regency_id.required' => 'Tag lokasi wajib diisi!',
            'alamat.required' => 'Alamat wajib diisi!',
            'fasilitas_id' => 'Fasilitas wajib diisi!',
            'fasilitas_id.*' => 'Fasilitas wajib diisi!',
            'amenitas_id' => 'Amenitas wajib diisi!',
            'amenitas_id.*' => 'Amenitas wajib diisi!',
            // 'icon_fasilitas.required' => 'Icon fasilitas wajib diisi!',
            // 'icon_fasilitas.*.required' => 'Icon fasilitas wajib diisi!',
            // 'icon_fasilitas.*.mimes' => 'Icon fasilitas harus memiliki ekstensi (jpg, jpeg, png, atau ico)',
            // 'icon_fasilitas.*.max' => 'Icon fasilitas maksimum berukuran 1MB',
            // 'deskripsi_fasilitas.required' => 'Deskripsi wajib diisi!',
            // 'deskripsi_fasilitas.*.required' => 'Deskripsi wajib diisi!',
            // 'icon_amenitas.required' => 'Icon amenitas wajib diisi!',
            // 'icon_amenitas.*.required' => 'Icon amenitas wajib diisi!',
            // 'icon_amenitas.*.mimes' => 'Icon amenitas harus memiliki ekstensi (jpg, jpeg, png, atau ico)',
            // 'icon_amenitas.*.max' => 'Icon amenitas maksimum berukuran 1MB',
            // 'judul_amenitas.required' => 'Judul amenitas wajib diisi!',
            // 'judul_amenitas.*.required' => 'Judul amenitas wajib diisi!',
            // 'isi_amenitas.required' => 'Isi amenitas wajib diisi!',
            // 'isi_amenitas.*.required' => 'Isi amenitas wajib diisi!',
            'deskripsi.required' => 'Deskripsi wajib diisi!',
            // 'catatan.required' => 'Catatan wajib diisi!',
        ]);


        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $data = Hotel::where('id', $id)->first();
        $data2 = Productdetail::where('product_id', $id)->first();

        $images_fasilitas = [];
        $images_amenitas = [];

        // Updating Fasilitas Icon
        // If no new image, only saved icon
        // if (isset($request->saved_icon_fasilitas) && !$request->hasFile('icon_fasilitas')) {
        //     foreach ($request->saved_icon_fasilitas as $key => $value) {
        //         array_push($images_fasilitas, $value);
        //     }
        // }

        // // If all images are new
        // if (!isset($request->saved_icon_fasilitas) && $request->hasFile('icon_fasilitas')) {

        //     foreach ($request->icon_fasilitas as $key => $value) {
        //         $new_icon_fasilitas = time() . 'iconfasilitas' . $key . '.' . $value->getClientOriginalExtension();
        //         $tujuan_uploud_icon_fasilitas = 'IconFasilitas/';
        //         $value->move($tujuan_uploud_icon_fasilitas, $new_icon_fasilitas);

        //         array_push($images_fasilitas, $tujuan_uploud_icon_fasilitas . $new_icon_fasilitas);
        //     }
        // }

        // // Saved images and new images
        // if (isset($request->saved_icon_fasilitas) && $request->hasFile('icon_fasilitas')) {
        //     // Get the last key of the array, return the bigger key
        //     $length = array_key_last($request->saved_icon_fasilitas) > array_key_last($request->icon_fasilitas) ? array_key_last($request->saved_icon_fasilitas) : array_key_last($request->icon_fasilitas);

        //     for ($i = 0; $i <= $length; $i++) {
        //         if (isset($request->saved_icon_fasilitas[$i])) {
        //             array_push($images_fasilitas, $request->saved_icon_fasilitas[$i]);
        //         }

        //         if (isset($request->icon_fasilitas[$i])) {
        //             $value = $request->icon_fasilitas[$i];
        //             $new_icon_fasilitas = time() . 'iconfasilitas' . $i . '.' . $value->getClientOriginalExtension();
        //             $tujuan_uploud_icon_fasilitas = 'IconFasilitas/';
        //             $value->move($tujuan_uploud_icon_fasilitas, $new_icon_fasilitas);

        //             array_push($images_fasilitas, $tujuan_uploud_icon_fasilitas . $new_icon_fasilitas);
        //         }
        //     }
        // }

        // Updating Amenitas Icon
        // If no new image, only saved icon
        // if (isset($request->saved_icon_amenitas) && !$request->hasFile('icon_amenitas')) {
        //     foreach ($request->saved_icon_amenitas as $key => $value) {
        //         array_push($images_amenitas, $value);
        //     }
        // }

        // // If all images are new
        // if (!isset($request->saved_icon_amenitas) && $request->hasFile('icon_amenitas')) {
        //     foreach ($request->icon_amenitas as $key => $value) {
        //         $new_icon_amenitas = time() . 'iconamenitas' . $key . '.' . $value->getClientOriginalExtension();
        //         $tujuan_uploud_icon_amenitas = 'IconAmenitas/';
        //         $value->move($tujuan_uploud_icon_amenitas, $new_icon_amenitas);

        //         array_push($images_amenitas, $tujuan_uploud_icon_amenitas . $new_icon_amenitas);
        //     }
        // }

        // // Saved images and new images
        // if (isset($request->saved_icon_amenitas) && $request->hasFile('icon_amenitas')) {
        //     // Get the last key of the array, return the bigger key
        //     $length = array_key_last($request->saved_icon_amenitas) > array_key_last($request->icon_amenitas) ? array_key_last($request->saved_icon_amenitas) : array_key_last($request->icon_amenitas);

        //     for ($i = 0; $i <= $length; $i++) {
        //         if (isset($request->saved_icon_amenitas[$i])) {
        //             array_push($images_amenitas, $request->saved_icon_amenitas[$i]);
        //         }

        //         if (isset($request->icon_amenitas[$i])) {
        //             $value = $request->icon_amenitas[$i];
        //             $new_icon_amenitas = time() . 'iconamenitas' . $i . '.' . $value->getClientOriginalExtension();
        //             $tujuan_uploud_icon_amenitas = 'IconAmenitas/';
        //             $value->move($tujuan_uploud_icon_amenitas, $new_icon_amenitas);

        //             array_push($images_amenitas, $tujuan_uploud_icon_amenitas . $new_icon_amenitas);
        //         }
        //     }
        // }

        // Fasilitas
        // $icon_fasilitas = ['result' => $images_fasilitas];
        // $deskripsi = ['result' => $request->deskripsi_fasilitas];

        // // Amenitas
        // $icon_amenitas = ['result' => $images_amenitas];
        // $judul_amenitas = ['result' => $request->judul_amenitas];
        // $isi_amenitas = ['result' => $request->isi_amenitas];

        // $json = json_decode($data2, true);

        //Update post with new image
        $fasilitas_id = ['result' => $request->fasilitas_id];
        $amenitas_id = ['result' => $request->amenitas_id];
        
        $data->update([
            'product_name' => $request->product_name,
        ]);

        $data2->update([
            // 'icon_fasilitas'        => json_encode($icon_fasilitas, true),
            // 'icon_amenitas'         => json_encode($icon_amenitas, true),
            'product_id'            => $data->id,
            'regency_id'           => $request->regency_id,
            'alamat'                => $request->alamat,
            'confirmation'          => $request->confirmation,
            // 'deskripsi_fasilitas'   => json_encode($deskripsi, true),
            // 'judul_amenitas'        => json_encode($judul_amenitas, true),
            // 'isi_amenitas'          => json_encode($isi_amenitas, true),
            'deskripsi'             => $request->deskripsi,
            'catatan'               => $request->catatan,
            'id_fasilitas' => json_encode($fasilitas_id, true),
            'id_amenitas' => json_encode($amenitas_id, true),
        ]);

        // Delete some saved fasilitas and amenitas iamges
        if (isset($json) && isset($data2)) {

            $differences_fasilitas = array_diff(json_decode($json['icon_fasilitas'])->result, json_decode($data2['icon_fasilitas'])->result);
            $differences_amenitas = array_diff(json_decode($json['icon_amenitas'])->result, json_decode($data2['icon_amenitas'])->result);

            foreach ($differences_fasilitas as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }

            foreach ($differences_amenitas as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }
        }

        return redirect()->route('xstay.infokamar.edit', $data->product_code);
    }

    // Informasi Kamar
    public function info_kamar()
    {
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        return view('BE.seller.xstay.informasikamar', compact('hotel', 'xstay'));
    }

    public function info_kamar_edit($id)
    {
        $data = Hotel::with('productdetail')->where('product_code', $id)->first();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $data2 = Productdetail::where('product_id', $data->id)->first();
        $masterKamar = Masterkamar::where('id', $data2->kamar_id)->first();
        $kamar = Masterkamar::where('user_id', auth()->user()->id)->where('jenis_produk','xstay')->get();

        $rooms = isset($data2->id_kamar) ? json_decode($data2->id_kamar,true)['result']:[];
        $rooms_array = collect([]);

        // dd($kamar);

        for($i=0; $i < count($rooms);$i++){
            $id_room = Masterkamar::where('id',$rooms[$i])->first();
            $rooms_array->push([ 
                'kamar_id'=>$id_room->id,
                'kamar_name'=>$id_room->nama_kamar,
                'kamar_code'=>$id_room->kode_kamar,
                'kamar_category'=>$id_room->jenis_kamar,
                'stock'=>$id_room->stock,
                'room_price'=>$id_room->harga_kamar,
            ]);
        }

        if ($masterKamar == null) {
            return view('BE.seller.xstay.informasikamar', compact('data', 'data2', 'hotel', 'xstay', 'masterKamar','kamar','rooms_array'));
        } else {
            $json = json_decode($kamar, true);
           
            $array = [];
            $array1 = [];

            $result =[];
            $val =[];

            for($i=0; $i < count($json);$i++){
                
                $array = json_decode($json[$i]['foto_kamar'], true);
                $array1 = json_decode($json[$i]['fasilitas'], true);
                $result['result'] = $array['result'];
                $val['result'] = isset($array1['result']) ? $array1['result']:[];
            }
            // die;
            return view('BE.seller.xstay.informasikamar_edit', compact('data', 'data2', 'hotel', 'xstay', 'result', 'val', 'masterKamar', 'array', 'array1','kamar','rooms_array'));
        }
    }

    public function info_kamar_update(Request $request, $id)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'kamar_id' => 'required',
        ], [
            'kamar_id.required' => 'Kamar wajib diisi!',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $data = Hotel::where('id', $id)->first();
        $product_detail = Productdetail::where('product_id', $id)->first();

        $kamar_id=['result'=>$request->kamar_id];

        $product_detail->update([
            'id_kamar'=>$kamar_id
        ]);

        return redirect()->route('harga.edit', $data->product_code);
    }

    // Harga
    public function harga()
    {
        return view('BE.seller.xstay.harga');
    }

    public function harga_edit($id)
    {
        $data = Hotel::with('productdetail')->where('product_code', $id)->first();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $data2 = Productdetail::with('harga', 'diskon', 'masterkamar')->where('product_id', $data->id)->first();
        $harga = Price::where('id', $data2->harga_id)->first();
        $diskon = Discount::where('id', $data2->discount_id)->first();

        $rooms = isset($data2->id_kamar) ? json_decode($data2->id_kamar,true)['result']:[];
        $rooms_array = collect([]);   
        // dd($rooms);
        for($i=0; $i < count($rooms);$i++){
            $id_room = Masterkamar::where('id',$rooms[$i])->first();
            $rooms_array->push([ 
                'kamar_id'=>$id_room->kamar_id,
                'kamar_name'=>$id_room->nama_kamar,
                'kamar_code'=>$id_room->kode_kamar,
                'kamar_category'=>$id_room->jenis_kamar,
                'stock'=>$id_room->stock,
                'room_price'=>$id_room->harga_kamar,
            ]);
        }     

        if ($diskon == null) {
            return view('BE.seller.xstay.harga', compact('data', 'data2', 'hotel', 'xstay'));
        } elseif ($diskon['tgl_start'] == null) {
            $json = json_decode($diskon, true);

            $array = json_decode($json['max_orang'], true);
            $array1 = json_decode($json['min_orang'], true);
            $array2 = json_decode($json['diskon_orang'], true);

            return view('BE.seller.xstay.harga_dup', compact('data', 'data2', 'hotel', 'xstay', 'array', 'array1', 'array2','rooms_array'));
        } else {
            $json = json_decode($diskon, true);

            $array = json_decode($json['max_orang'], true);
            $array1 = json_decode($json['min_orang'], true);
            $array2 = json_decode($json['diskon_orang'], true);

            $details = collect([]);
            foreach (json_decode($json['tgl_start']) as $key => $value) {
                $details->push([
                    'tgl_start' => json_decode($json['tgl_start'])[$key],
                    'tgl_end' => json_decode($json['tgl_end'])[$key],
                    'discount' => json_decode($json['discount'])[$key]
                ]);
            }

            return view('BE.seller.xstay.harga_edit', compact('data', 'hotel', 'xstay', 'data2', 'array', 'array1', 'array2', 'details','rooms_array'));
        }
    }

    public function hargaResiden_update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'dewasa_residen' => 'required|integer|gte:0',
            'anak_residen' => 'required|integer|gte:0',
            'balita_residen' => 'required|integer|gte:0',
            'dewasa_non_residen' => 'sometimes|nullable|integer|gte:0',
            'anak_non_residen' => 'sometimes|nullable|integer|gte:0',
            'balita_non_residen' => 'sometimes|nullable|integer|gte:0'
        ], [
            'dewasa_residen.required' => 'Harga Dewasa Residen wajib diisi!',
            'anak_residen.required' => 'Harga Anak Residen wajib diisi!',
            'balita_residen.required' => 'Harga Balita Residen wajib diisi!',
            'dewasa_residen.integer' => 'Harga Dewasa Residen harus berupa bilangan bulat!',
            'anak_residen.integer' => 'Harga Anak Residen harus berupa bilangan bulat!',
            'balita_residen.integer' => 'Harga Balita Residen harus berupa bilangan bulat!',
            'dewasa_non_residen.integer' => 'Harga Dewasa non Residen harus berupa bilangan bulat!',
            'anak_non_residen.integer' => 'Harga Anak non Residen harus berupa bilangan bulat!',
            'balita_non_residen.integer' => 'Harga Balita non Residen harus berupa bilangan bulat!',
            'dewasa_residen.gte' => 'Harga Dewasa Residen minimal 0',
            'anak_residen.gte' => 'Harga Anak Residen minimal 0',
            'balita_residen.gte' => 'Harga Balita Residen minimal 0',
            'dewasa_non_residen.gte' => 'Harga Dewasa non Residen minimal 0',
            'anak_non_residen.gte' => 'Harga Anak non Residen minimal 0',
            'balita_non_residen.gte' => 'Harga Balita non Residen minimal 0',
        ]);

        $data = Hotel::where('id', $id)->first();

        if ($validator->fails()) {
            return redirect()->route('harga.edit', $data->product_code)->withErrors($validator, 'harga');
        }

        $data = Hotel::where('id', $id)->first();
        $data2 = Productdetail::where('product_id', $id)->first();
        $harga = Price::where('id', $data2->harga_id)->first();

        $dewasa_residen = $request->dewasa_residen;
        $anak_residen = $request->anak_residen;
        $balita_residen = $request->balita_residen;
        $dewasa_non_residen = $request->dewasa_non_residen;
        $anak_non_residen = $request->anak_non_residen;
        $balita_non_residen = $request->balita_non_residen;

        if ($harga == null) {
            $price = new Price;
            if ($dewasa_non_residen == null && $anak_non_residen == null && $balita_non_residen == null) {
                $price->dewasa_residen = $dewasa_residen;
                $price->anak_residen = $anak_residen;
                $price->balita_residen = $balita_residen;
                $price->dewasa_non_residen = $dewasa_residen;
                $price->anak_non_residen = $anak_residen;
                $price->balita_non_residen = $balita_residen;
            } else {
                $price->dewasa_residen = $dewasa_residen;
                $price->anak_residen = $anak_residen;
                $price->balita_residen = $balita_residen;
                $price->dewasa_non_residen = $dewasa_non_residen;
                $price->anak_non_residen = $anak_non_residen;
                $price->balita_non_residen = $balita_non_residen;
            }
            $price->save();

            $data2->update([
                'harga_id' => $price->id,
            ]);
        } else {
            $harga->update([
                'dewasa_residen' => $dewasa_residen,
                'anak_residen' => $anak_residen,
                'balita_residen' => $balita_residen,
                'dewasa_non_residen' => $dewasa_non_residen,
                'anak_non_residen' => $anak_non_residen,
                'balita_non_residen' => $balita_non_residen,
            ]);
        }

        return redirect()->route('harga.edit', $data->product_code);
    }

    public function harga_update_diskon_grup(Request $request, $id)
    {
        $data = Hotel::where('id', $id)->first();

        // $validator = Validator::make($request->all(), [
        //     'min_orang' => 'required',
        //     'max_orang' => 'required',
        //     'diskon_orang' => 'required',
        //     'min_orang.*' => 'required|integer|lte:max_orang.*',
        //     'max_orang.*' => 'required|integer|gte:min_orang.*',
        //     'diskon_orang.*' => 'required|present|numeric|gt:0',
        // ],[
        //     'min_orang.required' => 'Minimal jumlah orang wajib diisi!',
        //     'max_orang.required' => 'Maksimal jumlah orang wajib diisi!',
        //     'diskon_orang.required' => 'Diskon wajib diisi!',
        //     'min_orang.*.integer' => 'Minimal jumlah orang harus bilangan bulat!',
        //     'max_orang.*.integer' => 'Maksimal jumlah orang harus bilangan bulat!',
        //     'diskon_orang.*.numeric' => 'Diskon harus berupa angka! (Desimal gunakan titik)',
        //     'min_orang.*.lte' => 'Minimal orang harus lebih kecil dari maksimal orang!',
        //     'max_orang.*.gte' => 'Maksimal orang harus lebih besar dari minimal orang!',
        //     'diskon_orang.*.gt' => 'Diskon harus lebih besar dari 0!'
        // ]);

        // if ($validator->fails())
        // {
        //     return redirect()->route('harga.edit', $data->product_code)->withErrors($validator, 'diskon_orang');
        // }

        $data2 = Productdetail::where('product_id', $id)->first();
        $diskon = Discount::where('id', $data2->discount_id)->first();

        $arr_min = [
            'min_orang' => isset($request->min_orang) ? $request->min_orang : []
        ];
        $arr_max = [
            'max_orang' => isset($request->max_orang) ? $request->max_orang : []
        ];
        $arr_discount = [
            'diskon_orang' => isset($request->diskon_orang) ? $request->diskon_orang : []
        ];

        if ($diskon == null) {
            $discount = new Discount;
            $discount->min_orang = json_encode($arr_min, true);
            $discount->max_orang = json_encode($arr_max, true);
            $discount->diskon_orang = json_encode($arr_discount, true);
            $discount->save();

            $data2->update([
                'discount_id' => $discount->id,
            ]);
        } else {
            $diskon->update([
                'min_orang' => json_encode($arr_min, true),
                'max_orang' => json_encode($arr_max, true),
                'diskon_orang' => json_encode($arr_discount, true),
            ]);
        }

        return redirect()->route('harga.edit', $data->product_code);
    }

    public function harga_update_diskon_date(Request $request, $id)
    {
        $data = Hotel::where('id', $id)->first();

        $validator = Validator::make($request->all(), [
            'details' => 'required',
            'details.*.tgl_start' => 'required|date|before_or_equal:details.*.tgl_end',
            'details.*.tgl_end' => 'required|date|after_or_equal:details.*.tgl_start',
            'details.*.discount' => 'required|numeric',
        ], [
            'details' => 'Data wajib diisi!',
            'details.*.tgl_start.required' => 'Dari tanggal wajib diisi!',
            'details.*.tgl_end.required' => 'Sampai tanggal wajib diisi!',
            'details.*.discount.required' => 'Diskon atau penambahan biaya wajib diisi!',
            'details.*.tgl_start.date' => "Pastikan (Dari tanggal) terisi dengan benar",
            'details.*.tgl_end.date' => 'Pastikan (Sampai tanggal) terisi dengan benar',
            'details.*.discount.numeric' => 'Diskon atau penambahan biaya harus berupa angka!',
            'details.*.tgl_start.before_or_equal' => 'Pastikan tanggal mulai harus lebih kecil dari tanggal selesai!',
            'details.*.tgl_end.after_or_equal' => 'Pastikan Tanggal selesai harus lebih besar dari tanggal mulai!',
        ]);

        if ($validator->fails()) {
            return redirect()->route('harga.edit', $data->product_code)->withErrors($validator, 'diskon_ketersediaan');
        }

        $data2 = Productdetail::where('product_id', $id)->first();
        $disc = Discount::where('id', $data2->discount_id)->first();

        $tgl_start = collect([]);
        $tgl_end = collect([]);
        $discount = collect([]);

        if (isset($request->details)) {
            foreach ($request->details as $key => $value) {
                $tgl_start->push($value['tgl_start']);
                $tgl_end->push($value['tgl_end']);
                $discount->push($value['discount']);
            }
        }

        if ($disc == null) {
            $arr_min = [
                'min_orang' => isset($request->min_orang) ? $request->min_orang : []
            ];
            $arr_max = [
                'max_orang' => isset($request->max_orang) ? $request->max_orang : []
            ];
            $arr_discount = [
                'diskon_orang' => isset($request->diskon_orang) ? $request->diskon_orang : []
            ];

            $diskon = new Discount;

            $diskon->min_orang = json_encode($arr_min, true);
            $diskon->max_orang = json_encode($arr_max, true);
            $diskon->diskon_orang = json_encode($arr_discount, true);

            $diskon->tgl_start = $tgl_start;
            $diskon->tgl_end = $tgl_end;
            $diskon->discount = $discount;
            $diskon->save();

            $data2->update([
                'discount_id' => $diskon->id,
            ]);
        } else {
            $disc->update([
                'tgl_start' => $tgl_start,
                'tgl_end' => $tgl_end,
                'discount' => $discount,
            ]);
        }

        // $disc->update([
        //     'tgl_start' => $tgl_start,
        //     'tgl_end' => $tgl_end,
        //     'discount' => $discount,
        // ]);

        return redirect()->route('bayar.edit', $data->product_code);
    }

    // Batas Bayar
    public function bayar_edit($id)
    {
        $data = Hotel::with('productdetail')->where('product_code', $id)->first();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $data2 = Productdetail::where('product_id', $data->id)->first();

        if ($data2->kebijakan_pembatalan_sebelumnya == null && $data2->kebijakan_pembatalan_sesudah == null && $data2->kebijakan_pembatalan_potongan == null) {
            return view('BE.seller.xstay.batasbayar', compact('data', 'hotel', 'xstay', 'data2'));
        } else {
            // $json = json_decode($data2, true);

            // $array = json_decode($json['kebijakan_pembatalan_sebelumnya'], true);
            // $array1 = json_decode($json['kebijakan_pembatalan_sesudah'], true);
            // $array2 = json_decode($json['kebijakan_pembatalan_potongan'], true);

            // $value['sebelumnya'] = $array['sebelumnya'];
            // $value['sesudah'] = $array1['sesudah'];
            // $value['potongan'] = $array2['potongan'];

            $json = json_decode($data2);

            $value['sebelumnya'] = json_decode($json->kebijakan_pembatalan_sebelumnya)->sebelumnya;
            $value['sesudah'] = json_decode($json->kebijakan_pembatalan_sesudah)->sesudah;
            $value['potongan'] = json_decode($json->kebijakan_pembatalan_potongan)->potongan;

            return view('BE.seller.xstay.batasbayar_edit', compact('data', 'hotel', 'xstay', 'value'));
        }
    }

    public function bayar_update(Request $request, $id)
    {
        $data = Hotel::where('id', $id)->first();
        $data2 = Productdetail::where('product_id', $id)->first();

        $validator = Validator::make($request->all(), [
            'batas_pembayaran' => 'required|integer',
            'kebijakan_pembatalan_sebelumnya' => 'required',
            'kebijakan_pembatalan_sesudah' => 'required',
            'kebijakan_pembatalan_potongan' => 'required',
            'kebijakan_pembatalan_sebelumnya.*' => 'required|integer|lt:batas_pembayaran|gte:kebijakan_pembatalan_sesudah.*',
            'kebijakan_pembatalan_sesudah.*' => 'required|integer|lt:batas_pembayaran|lte:kebijakan_pembatalan_sebelumnya.*',
            'kebijakan_pembatalan_potongan.*' => 'required|integer',
        ], [
            'batas_pembayaran.required' => 'Batas pembayaran wajib diisi!',
            'batas_pembayaran.integer' => 'Pastikan batas pembayaran berupa bilangan bulat',
            // 'batas_pembayaran.gte' => 'Batas pembayaran harus lebih besar daripada batas waktu pembatalan',
            'kebijakan_pembatalan_sebelumnya.required' => 'Data wajib diisi!',
            'kebijakan_pembatalan_sesudah.required' => 'Data wajib diisi!',
            'kebijakan_pembatalan_potongan.required' => 'Potongan wajib diisi!',
            'kebijakan_pembatalan_sebelumnya.*.required' => 'Data wajib diisi!',
            'kebijakan_pembatalan_sesudah.*.required' => 'Data wajib diisi!',
            'kebijakan_pembatalan_potongan.*.required' => 'Potongan wajib diisi!',
            'kebijakan_pembatalan_sebelumnya.*.integer' => "Pastikan jumlah hari kebijakan pembatalan berupa bilangan bulat",
            'kebijakan_pembatalan_sesudah.*.integer' => 'Pastikan jumlah hari kebijakan pembatalan berupa bilangan bulat',
            'kebijakan_pembatalan_potongan.*.integer' => 'Pastikan potongan berupa bilangan bulat',
            'kebijakan_pembatalan_sebelumnya.*.lt' => 'Batas pembayaran harus lebih besar daripada batas waktu pembatalan',
            'kebijakan_pembatalan_sebelumnya.*.gte' => 'Nilai (hari sebelumnya sampai) harus lebih besar daripada nilai (hari sebelumnya)',
            'kebijakan_pembatalan_sesudah.*.lt' => 'Batas pembayaran harus lebih besar daripada batas waktu pembatalan',
            'kebijakan_pembatalan_sesudah.*.lte' => 'Nilai (hari sebelumnya sampai) harus lebih besar daripada nilai (hari sebelumnya)',
        ]);

        if ($validator->fails()) {
            // return redirect()->route('hotelbatasbayar.edit', $data->product_code)->withErrors($validator, 'batas_waktu');
            return back()->withInput()->withErrors($validator, 'batas_waktu');
        }

        $arr = [
            'sebelumnya' => $request->kebijakan_pembatalan_sebelumnya
        ];
        $arr1 = [
            'sesudah' => $request->kebijakan_pembatalan_sesudah
        ];
        $arr2 = [
            'potongan' => $request->kebijakan_pembatalan_potongan
        ];

        $data2->update([
            'batas_pembayaran' => $request->batas_pembayaran,
            'kebijakan_pembatalan_sebelumnya' => json_encode($arr),
            'kebijakan_pembatalan_sesudah' => json_encode($arr1),
            'kebijakan_pembatalan_potongan' => json_encode($arr2)
        ]);

        return redirect()->route('maps.edit', $data->product_code);
    }

    // Maps
    public function maps_edit($id)
    {
        $data = Hotel::with('productdetail')->where('product_code', $id)->first();
        $detail = Productdetail::where('product_id', $data->id)->first();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        if ($detail->link_maps == null && $detail->foto_maps1 == null && $detail->foto_maps2 == null) {
            return view('BE.seller.xstay.mapsfoto', compact('data', 'hotel', 'xstay', 'detail'));
        } else {
            return view('BE.seller.xstay.mapsfoto_edit', compact('data', 'hotel', 'xstay', 'detail'));
        }
    }

    public function maps_update(Request $request, $id)
    {
        $data = Hotel::where('id', $id)->first();
        $data2 = Productdetail::where('product_id', $id)->first();

        $validator = Validator::make($request->all(), [
            'link_maps' => 'required',
            'foto_maps_2' => 'required_without_all:saved_thumbnail|image|mimes:jpeg,png,jpg|max:2048',
        ], [
            'link_maps.required' => 'Link peta google maps wajib diisi!',
            'foto_maps_2.required_without_all' => 'Foto Thumbnail wajib diisi!',
            'foto_maps_2.mimes' => 'Foto harus memiliki ekstensi (jpg, jpeg, atau png)',
            'foto_maps_2.max' => 'Foto maksimum berukuran 2MB',
        ]);

        if ($validator->fails() && !isset($data2->foto_maps_1)) {
            $request->session()->flash('message', 'Foto Gallery harap diisi!');
            return redirect()->route('hotelmapsfoto.edit', $data->product_code)->withErrors($validator);
        }

        if (!isset($data2->foto_maps_1)) {
            $request->session()->flash('message', 'Foto Gallery harap diisi!');
            return redirect()->route('hotelmapsfoto.edit', $data->product_code);
        }

        if ($validator->fails()) {
            return redirect()->route('maps.edit', $data->product_code)->withErrors($validator);
        }

        $foto_maps_2 = isset($data2->foto_maps_2) ? $data2->foto_maps_2 : '';

        $fields = collect([]);

        if ($request->hasFile('foto_maps_2')) {

            $image1 = $request->file('foto_maps_2');
            $new_foto1 = time() . 'fotomaps_2'  . '.' . $image1->getClientOriginalExtension();
            $tujuan_uploud1 = 'FotoMaps_2/';
            $image1->move($tujuan_uploud1, $new_foto1);
            $foto_maps_2 = $tujuan_uploud1 . $new_foto1;
            if (file_exists($data2->foto_maps_2)) {
                unlink($data2->foto_maps_2);
            }
        }

        //Update post with new image
        $data2->update([
            'foto_maps_2'    => $foto_maps_2,
            'link_maps'       => $request->link_maps,
        ]);

        return redirect()->route('ekstra.edit', $data->product_code);
    }

    public function maps_photo_gallery(Request $request, $id)
    {
        // Validate request
        $validator = Validator::make($request->all(), [
            // Validate when saved gallery not exist
            'images_gallery' => 'required_without_all:saved_gallery',
            'images_gallery.*' => 'mimes:jpeg,png,jpg|max:2048'
        ], [
            'images_gallery.required_without_all' => 'Foto Gallery wajib diisi!',
            'images_gallery.*.mimes' => "Foto harus memiliki ekstensi (jpg, jpeg, atau png)",
            'images_gallery.*.max' => 'Foto maksimum berukuran 2MB',
        ]);

        // Return when validate fails on one or more rule(s)
        if ($validator->fails()) {
            return array(
                'status' => 0,
                'message' => $validator->messages()->first(),
            );
        }

        // Execute these code when validate success
        $data2 = Productdetail::where('product_id', $id)->first();
        $session_gallery = $data2->foto_maps_1;
        $fields = collect([]);

        // Save all new images gallery
        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {
                $new_foto = time() . 'fotomaps_1' . $key . '.' . $value->getClientOriginalExtension();
                $tujuan_upload = 'FotoMaps_1/';

                $value->move($tujuan_upload, $new_foto);
                $images_gallery = $tujuan_upload . $new_foto;

                $fields->push(
                    $images_gallery,
                );
            }
        }

        // Delete all existing saved gallery
        if (isset($session_gallery) && !$request->has('saved_gallery')) {
            foreach (json_decode($session_gallery) as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }
        }

        // // Delete some saved gallery iamges
        if (isset($session_gallery) && $request->has('saved_gallery')) {

            $differences = array_diff(json_decode($session_gallery), $request->saved_gallery);

            foreach ($differences as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }

            foreach ($request->saved_gallery as $key => $value) {
                $fields->push(
                    $value,
                );
            }
        }

        // $product_detail = $detail->productdetail()->update([
        //     'gallery' => json_encode($fields),
        // ]);

        // $request->session()->put('activity.peta.images_gallery', $fields);

        //update post with new image
        $data2->update([
            'foto_maps_1'   => json_encode($fields),
        ]);

        return array(
            'status' => 1,
            'message' => 'Foto Gallery berhasil diperbarui.'
        );
    }

    // public function maps_update(Request $request, $id)
    // {
    //     $data = Hotel::where('id', $id)->first();
    //     $data2 = Productdetail::where('product_id', $id)->first();
    //     $fields = collect([]);

    //     if ($request->hasFile('foto_maps_1')) {

    //         //upload new image
    //         $image = $request->file('foto_maps_1');
    //         // $new_foto = time() . 'fotomaps_1' . $image->getClientOriginalName();
    //         // $tujuan_uploud = 'FotoMaps_1/';
    //         // $image->move($tujuan_uploud, $new_foto);

    //         foreach ($image as $key => $value) {

    //             $new_foto = time() . 'fotomaps_1' . $value->getClientOriginalName();
    //             $tujuan_upload = 'FotoMaps_1/';

    //             $value->move($tujuan_upload, $new_foto);
    //             $images_gallery = $tujuan_upload . $new_foto;

    //             $fields->push(
    //                 $images_gallery,
    //             );
    //         }

    //         $tujuan_uploud1 = '';
    //         $new_foto1 = '';

    //         if ($request->hasFile('foto_maps_2')) {
    //             $image1 = $request->file('foto_maps_2');
    //             $new_foto1 = time() . 'fotomaps_2' . $image1->getClientOriginalName();
    //             $tujuan_uploud1 = 'FotoMaps_2/';
    //             $image1->move($tujuan_uploud1, $new_foto1);
    //         }

    //         //delete old image
    //         if (isset($data2->foto_maps_1)) {
    //             foreach (json_decode($data2->foto_maps_1) as $key => $value) {
    //                 if (file_exists($value[0])) {
    //                     unlink($value[0]);
    //                 }
    //             }
    //         }
    //         if (file_exists(($data2->foto_maps_2))) {
    //             unlink(($data2->foto_maps_2));
    //         }

    //         //update post with new image
    //         $data2->update([
    //             'foto_maps_1'   => json_encode($fields),
    //             'foto_maps_2'    => $tujuan_uploud1 . $new_foto1,
    //             'link_maps'       => $request->link_maps,
    //         ]);
    //     } else {
    //         //update post without image
    //         $data2->update([
    //             'link_maps'       => $request->link_maps,
    //         ]);
    //     }

    //     return redirect()->route('ekstra.edit', $data->product_code);
    // }

    // Pilihan Ekstra
    public function ekstra_edit($id)
    {
        $data = Hotel::with('productdetail')->where('product_code', $id)->first();
        $product_detail = Productdetail::where('product_id', $data->id)->first();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        if ($product_detail->izin_ekstra == null) {
            return view('BE.seller.xstay.pilihanekstra', compact('data', 'hotel', 'xstay', 'product_detail'));
        } else {
            return view('BE.seller.xstay.pilihanekstra_edit', compact('data', 'hotel', 'xstay', 'product_detail'));
        }
    }

    public function ekstra_update(Request $request, $id)
    {
        $data2 = Productdetail::where('product_id', $id)->first();
        $data = Hotel::where('id', $id)->first();

        $validator = Validator::make($request->all(), [
            'izin_ekstra' => 'required',
            'tnc' => 'accepted|required'
        ], [
            'izin_ekstra.required' => 'Ekstra wajib diisi!',
            'tnc.accepted' => 'Setujui syarat dan ketentuan untuk melanjutkan.',
            'tnc.required' => 'Setujui syarat dan ketentuan untuk melanjutkan.',
        ]);

        if ($validator->fails()) {
            // dd($validator);
            return back()->withErrors($validator, 'pilihan_ekstra');
        }

        $data2->update([
            'izin_ekstra' => $request->izin_ekstra,
        ]);
        $request->session()->flash('url', url()->to('/') . '/xstayDetail/' . $data->slug);

        return redirect()->route('dashboardmyListing');
    }

    // FAQ
    public function faq()
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'xstay'
        ])->has('productdetail')->with('productdetail')->get();

        return view('BE.seller.xstay.faq', compact('data', 'xstay', 'faqs'));
    }

    public function faq_code(Request $request)
    {
        if ($request->product_xstay == null) {
            return redirect()->route('faq.index');
        }

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $product = Product::where('id', $request->product_xstay)->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'xstay'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('id', $request->product_xstay)->with('productdetail')->first();

        // return view('BE.seller.hotel.faq', compact('data', 'xstay', 'faqs', 'faq_content'));
        return redirect()->route('faq.edit', $product->product_code);
    }

    public function faq_edit($code)
    {
        if ($code == null) {
            return redirect()->route('faq.index');
        }

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        // dd($data);
        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'xstay'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('product_code', $code)->where('user_id', auth()->user()->id)->with('productdetail')->first();

        return view('BE.seller.xstay.faq', compact('data', 'xstay', 'faqs', 'faq_content'));
    }

    public function faq_update(Request $request, $id)
    {
        $data = Hotel::where('id', $id)->first();
        $data2 = Productdetail::where('product_id', $id)->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'xstay'
        ])->has('productdetail')->with('productdetail')->get();

        $data2->update([
            'faq' => $request->faq
        ]);

        return redirect()->route('faq.index');
    }

    public function viewRoom()
    {
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $rooms = Masterkamar::where('user_id', auth()->user()->id)->paginate(5);

        return view('BE.seller.xstay.masterkamar', compact('xstay', 'rooms'));
    }

    public function viewAddRoom()
    {
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $attributes = Attributes::get();

        return view('BE.seller.xstay.tambahkamar', compact('xstay', 'attributes'));
    }

    public function addRoom(Request $request)
    {
        $session_gallery = $request->session()->has('kamar.xstay.gallery') ? $request->session()->get('kamar.xstay.gallery') : null;
        $validator = Validator::make($request->all(), [
            'room_name' => 'required',
            'room_code' => 'required',
            'room_category' => 'required',
            'min_capacity' => 'required|lte:max_capacity',
            'max_capacity' => 'required|gte:min_capacity',
            'price_type' => 'required',
            'room_price' => 'required|numeric|min:0',
            'room_stock' => 'required|numeric|min:0',
            'refundable' => 'required',
            'refundable_amount' => 'required|numeric|min:0|max:100',
            'extra_name' => 'required',
            'extra_name.*' => 'required',
            'extra_price' => 'required|min:0',
            'extra_price.*' => 'required|numeric|min:0',
            'attributes' => 'required',
            // 'amenitas_id' => 'required',
            // 'amenitas_id.*' => 'required'
        ], [
            'room_name.required' => 'Nama kamar wajib diisi!',
            'room_code.required' => 'Kode kamar wajib diisi!',
            'room_category.required' => 'Jenis Kamar wajib diisi!',
            'min_capacity.required' => 'Min kapasitas wajib diisi!',
            'min_capacity.lte' => 'Min kapasitas harus lebih kecil dari maks kapasitas',
            'max_capacity.required' => 'Maks kapasitas Kamar wajib diisi!',
            'max_capacity.gte' => 'Maks kapasitas harus lebih kecil dari min kapasitas',
            'price_type.required' => 'Jenis harga kamar wajib diisi!',
            'room_price.required' => 'Harga kamar diisi!',
            'room_price.numeric' => 'Harga kamar harus berupa angka!',
            'room_price.min' => 'Harga kamar minimal 0',
            'room_stock.required' => 'Stok kamar diisi!',
            'room_stock.numeric' => 'Stok kamar harus berupa angka!',
            'room_stock.min' => 'Stok kamar minimal 0',
            'refundable.required' => 'Refundable wajib diisi!',
            'refundable_amount.required' => 'Persen refund wajib diisi!',
            'refundable_amount.numeric' => 'Persen refund harus berupa angka!',
            'refundable_amount.min' => 'Persen refund minimal 0',
            'refundable_amount.max' => 'Persen refund maksimal 100',
            'extra_name' => 'Nama ekstra diisi!',
            'extra_name.*' => 'Nama ekstra diisi!',
            'extra_price.required' => 'Harga ekstra diisi!',
            'extra_price.numeric' => 'Harga ekstra harus berupa angka!',
            'extra_price.min' => 'Harga ekstra minimal 0',
            'extra_price.*.required' => 'Harga ekstra wajib diisi!',
            'extra_price.*.min' => 'Harga ekstra minimal 0',
            'attributes.required' => 'Amenitas wajib diisi!',
            // 'amenitas_id.*.required' => 'Amenitas wajib diisi!',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }


        if (!isset($session_gallery)) {
            return back()->withErrors(['image_gallery' => 'Gallery wajib diisi!'])->withInput();
        }

        $gallery = collect([]);

        foreach (json_decode($session_gallery) as $key => $value) {
            $gallery->push($value);
        }

        // Ekstra
        $extra_name = ['result' => $request->extra_name];
        $extra_price = ['result' => $request->extra_price];

        // Amenitas
        $amenitas_id = ['result' => $request->amenitas_id];

        $gallery_kamar = ['result' => $gallery];

        // Save to Master Kamar database
        Masterkamar::create([
            'user_id' => auth()->user()->id,
            'nama_kamar' => $request->room_name,
            'kode_kamar' => $request->room_code,
            'jenis_kamar' => $request->room_category,
            'kapasitas_minimum' => $request->min_capacity,
            'kapasitas_maksimum' => $request->max_capacity,
            'tipe_harga' => $request->price_type,
            'harga_kamar' => $request->room_price,
            'foto_kamar' => json_encode($gallery_kamar, true),
            'stok' => $request->room_stock,
            'refundable' => $request->refundable,
            'refundable_amount' => $request->refundable_amount,
            'nama_ekstra' => json_encode($extra_name, true),
            'harga_ekstra' => json_encode($extra_price, true),
            'id_amenitas' => json_encode($amenitas_id, true),
        ]);

        $request->session()->forget('kamar.xstay.gallery');

        return redirect()->route('kamarxstay');
    }

    public function copyRoom(Request $request, $id)
    {
        $room = Masterkamar::findOrFail($id);
        $new_room = $room->replicate();

        $new_room->created_at = \Carbon\Carbon::now();
        $new_room->save();

        return redirect()->route('kamarxstay');
    }

    public function viewEditRoom(Request $request, $id)
    {
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $room = Masterkamar::find($id);

        if (auth()->user()->id != $room->user_id) {
            return redirect()->route('kamarxstay');
        }

        $extra_name = isset($room->nama_extra) ? json_decode($room->nama_extra, true)['result'] : [];
        $extra_price = isset($room->harga_extra) ? json_decode($room->harga_extra, true)['result'] : [];
        $gallery_kamar = isset($room->foto_kamar) ? json_decode($room->foto_kamar, true)['result'] : [];
        $amenitas = isset($room->id_amenitas) ? json_decode($room->id_amenitas, true)['result'] : [];
        $amenitas_array = collect([]);
        $extras_array = collect([]);

        $request->session()->put('kamar.xstay.old_image', $gallery_kamar);
        if ($amenitas) {
            for ($i = 0; $i < count($amenitas); $i++) {
                $data_amenitas = Attributes::where('id', $amenitas[$id])->first();

                $amenitas_array->push([
                    'amenitas_id' => $data_amenitas->id,
                    'amenitas_name' => $data_amenitas->text,
                    'amenitas_img' => Storage::url($data_amenitas->image),
                ]);
            }
        }

        for ($i = 0; $i < count($extra_name); $i++) {
            $extras_array->push([
                'extra_name' => $extra_name[$i],
                'extra_price' => $extra_price[$i],
            ]);
        }

        $attributes = Attributes::get();

        return view('BE.seller.xstay.editkamar', compact('xstay', 'room', 'attributes', 'extra_name', 'extra_price', 'id', 'gallery_kamar', 'amenitas', 'amenitas_array', 'extras_array'));
    }

    public function editRoom(Request $request, $id)
    {
    }

    public function deleteRoom($id)
    {
        $user_id = auth()->user()->id;
        $room = Masterkamar::findOrFail($id);

        $xstay = product::with('productdetail')->where(['user_id' => $user_id, 'type' => 'xstay'])->where('kamar_id', $room->id);

        dd($xstay);
    }

    public function addRoomGallery(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'images_gallery' => 'required_without_all:saved_gallery',
            'images_gallery.*' => 'mimes:jpeg,png,jpg|max:2048'
        ], [
            'images_gallery.required_without_all' => 'Foto Gallery wajib diisi!',
            'images_gallery.*.mimes' => "Foto harus memiliki ekstensi (jpg, jpeg, atau png)",
            'images_gallery.*.max' => 'Foto maksimum berukuran 2MB',
        ]);

        if ($validator->fails()) {
            return array(
                'status' => 0,
                'message' => $validator->messages()->first()
            );
        }

        $sessionImg =  $request->session()->get('kamar.xstay.gallery');

        $fields = collect([]);
        $gallery = '';

        if ($request->hasFile('images_gallery')) {
            $images = $request->file('images_gallery');

            foreach ($images as $key => $image) {
                // dump($image);
                $file_name = time() . 'GalleryProductXstay' . $key . '.' . $image->getClientOriginalExtension();
                $tujuan = 'Seller/Product/Xstay/Kamar/';

                $lebar_foto = Image::make($image)->width();
                $lebar_foto = $lebar_foto * 50 / 100;

                Image::make($image)->resize($lebar_foto, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($tujuan . $file_name);

                $gallery = $tujuan . $file_name;

                $fields->push(
                    $gallery
                );

                // delete old image
                if (isset($sessionImg)) {
                    foreach (json_decode($sessionImg) as $key => $val) {
                        if (file_exists($val)) {
                            unlink($val);
                        }
                    }
                }
            }
        }

        $request->session()->put('kamar.xstay.gallery', $fields);
        // dd($request->session()->get('kamar.xstay.gallery'));
        return array(
            'status' => 1,
            'message' => 'Foto Gallery berhasil diperbarui.',
        );
    }
}
