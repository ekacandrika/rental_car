<?php

namespace App\Http\Controllers\Seller\Rental;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Paket;
use App\Models\MasterChoice;
use App\Models\Pilihan;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\detailWisata;
use App\Models\Mobildetail;
use App\Models\MerekMobil;
use App\Models\MerekBus;
use App\Models\JenisMobil;
use App\Models\Kategori;
use App\Models\DetailKendaraan;
use Illuminate\Http\Request;

class EditListingRentalController extends Controller
{
    public function showAddDetail(Request $request, $id)
    {
        $user_id = Auth::user()->id;
        $jenismobil = JenisMobil::where('user_id',$user_id)->get();

        $layoutbus = DB::table('layout_kursi')->where('user_id', $user_id)
                    ->join('users', 'layout_kursi.user_id', '=', 'users.id')
                    ->get();
                    
        $jenisbus = DB::table('jenis_bus')->where('user_id', $user_id)
                    ->join('users', 'jenis_bus.user_id', '=', 'users.id')
                    ->get();

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();
        $pulau     = DB::select('select * from pulau');    
        $provinsi  = Province::all();
        $kabupaten = Regency::all();
        $kecamatan = District::all();
        $kelurahan = Village::where('id', $detail->productdetail->village_id)->first();
        $objek     = detailWisata::all();
        
        $locations = $this-> getTagLocation();
        
        $kendaraan = DetailKendaraan::where('id', $detail->productdetail->id_detail_kendaraan)->first();
        // dd($detail->productdetail);
        
        $kendaraan['nama_kendaraan']   = $kendaraan->nama_kendaraan;
        $kendaraan['kapasitas_kursi']  = $kendaraan->kapasitas_kursi;
        $kendaraan['kapasitas_koper']  = $kendaraan->kapasitas_koper;
        $kendaraan['harga_akomodasi']  = $kendaraan->harga_akomodasi;
        $kendaraan['pilihan_tambahan'] = $kendaraan->pilihan_tambahan;
        
        $listing = $detail->product_code;
        $add_listing['product_name'] = $detail->product_name;
        $add_listing['province_id'] = $detail->productdetail->province_id;
        $add_listing['regency_id'] = $detail->productdetail->regency_id;
        $add_listing['district_id'] = $detail->productdetail->district_id;
        $add_listing['village_id'] = $detail->productdetail->village_id;
        
        // add tag location
        $add_listing['tag_location_1'] = $detail->productdetail->tag_location_1;
        $add_listing['tag_location_2'] = $detail->productdetail->tag_location_2;
        $add_listing['tag_location_3'] = $detail->productdetail->tag_location_3;
        $add_listing['tag_location_4'] = $detail->productdetail->tag_location_4;
        $add_listing['new_tag_location'] = $detail->productdetail->new_tag_location;

        $add_listing['deskripsi'] = $detail->productdetail->deskripsi;
        $add_listing['komplemen'] = $detail->productdetail->catatan;
        $add_listing['gallery']   = $detail->productdetail->gallery;

        // add kapasitas_kursi dan kapasitas kpoer
        $add_listing['kapasitas_kursi']  = $kendaraan['kapasitas_kursi'];
        $add_listing['kapasitas_koper']  = $kendaraan['kapasitas_koper'];
        $add_listing['nama_kendaraan']   = $kendaraan['nama_kendaraan'];
        $add_listing['id_merek_mobil']   = $kendaraan->id_merek_mobil;
        $add_listing['id_jenis_mobil']   = $kendaraan->id_jenis_mobil;
        $add_listing['status']           = $kendaraan->status;
        $add_listing['pilihan_tambahan'] = $kendaraan->pilihan_tambahan;
        $add_listing['pilihan_tambahan'] = $kendaraan->pilihan_tambahan;
        $add_listing['harga_akomodasi']  = $kendaraan['harga_akomodasi'];


        $rental['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $rental['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $rental['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = true;

        $dynamic_tag = [];
        
        $i =0;
        $x =0;
        $z =0;
        $y =0;

        foreach($pulau as $p){
            
            $prov = Province::where('island_id',$p->id)->get();
            
            $dynamic_tag['pulau'][$i]['id_pulau'] = $p->id;
            $dynamic_tag['pulau'][$i]['nama_pulau'] =$p->island_name;
            
            foreach($prov as $pv){
                
                $dynamic_tag['pulau'][$i]['provinsi'][$x]['id_provinsi']=$pv->id;
                $dynamic_tag['pulau'][$i]['provinsi'][$x]['nama_provinsi']=$pv->name;
                
                $kab = Regency::where('province_id',$pv->id)->get();
                
                foreach($kab as $k){

                    $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['id_kabupaten']=$k->id;
                    $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['nama_kabupaten']=$k->name;


                    $wisata =detailWisata::where('kabupaten_id',$k->id)->get();
                    foreach($wisata as $w){
                       
                        $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['wisata'][$y]['id_wisata']=$w->id;
                        $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['wisata'][$y]['nama_wisata']=$w->namaWisata;
                       
                        $y++;
                    }

                    $z++;

                }    
                
                $x++;
            }

            $i++;
        }

        $dynamic_tag = collect($dynamic_tag);

        return view('BE.seller.rental.add-listing', compact('pulau','provinsi', 'kabupaten', 'kecamatan', 'kelurahan', 'objek', 'jenismobil','kendaraan','jenisbus','layoutbus','detail', 'listing', 'add_listing', 'rental', 'id', 'isedit', 'locations', 'dynamic_tag'));
    }

    public function editAddListing(Request $request, $id)
    {
        // dd($request->all());
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $kendaraan = DetailKendaraan::where('id', $detail->productdetail->id_detail_kendaraan)->first();

        // dd(count(json_decode($detail->productdetail->gallery)));
        // update image
        // $gallery='';
        // $fields =collect([]);
        
        // if($request->hasFile('gallery')){
        //     $image = $request->file('gallery');
        
        //     foreach($image as $key => $value){
        //         $foto   = time().'GalleryProductRental'.$value->getClientOriginalName();
        //         $tujuan = 'Seller/Product/Rental/Gallery/';
        //         $value->move($tujuan,$foto);
        //         $gallery= $tujuan.$foto;

        //         $fields->push([
        //             'gallery'=>$gallery
        //         ]);
        //     }
        
        //     // delete old image
        //     if(isset($detail->productdetail->gallery) && count(json_decode($detail->productdetail->gallery)) > 1){
                
        //         foreach(json_decode($detail->productdetail->gallery )as $key => $v){
        //             if(file_exists($v->gallery)){
        //                 unlink($gallery);
        //             }
        //         }
        //     }
        // }

        $new_tag_location = [];
        $new_tag_location['results']=$request->new_tag_location;

        $kendaraan->update([
            'nama_kendaraan' => $request->nama_kendaraan,
            'kapasitas_kursi' => $request->kapasitas_kursi,
            'kapasitas_koper' => $request->kapasitas_koper,
            'harga_akomodasi' => $request->harga_akomodasi,
            'pilihan_tambahan' => $request->pilihan_tambahan,
            'id_jenis_mobil'=>$request->jenismobil,
            'id_merek_mobil'=>$request->merekmobil
        ]);

        $product_detail = $detail->productdetail()->update([
            'tag_location_1' => $request->tag_location_1,
            'tag_location_2' => $request->tag_location_2,
            'tag_location_3' => $request->tag_location_3,
            'tag_location_4' => $request->tag_location_4,
            'tag_location_4' => $request->tag_location_4,
            'new_tag_location' => json_encode($new_tag_location),
            'deskripsi' => $request->deskripsi,
            'catatan' => $request->catatan,
            'komplemen' => $request->catatan,
            'pilihan_tambahan' => $request->pilihan_tambahan,
            // 'gallery'=>json_encode($fields),
            // 'izin_ekstra' => 
        ]);

        $detail->update([
            'product_name' => $request->product_name,
        ]);

        return redirect()->route('rental.viewPilihanEkstraEdit', $id);
    }

    public function showEditPilihanEkstra(Request $request, $id)
    {
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();
        
        $pilihan = Pilihan::where('id', $detail->productdetail->pilihan_id)->first();

        $master_pilihan = MasterChoice::where('product_type','transfer and rental')->get();

        $pilihan_ekstra['ekstra_radio_button'] = $detail->productdetail->izin_ekstra;

        $fields = collect([]);
        $judul_pilihan = json_decode($pilihan->judul_pilihan);
        $deskripsi_pilihan = json_decode($pilihan->deskripsi_pilihan);
        $harga_pilihan = json_decode($pilihan->harga_pilihan);
        $kewajiban_pilihan = json_decode($pilihan->kewajiban_pilihan);
        $nama_pilihan = json_decode($pilihan->nama_pilihan);
        // dd($pilihan);
        if($judul_pilihan){
            foreach ($judul_pilihan as $key => $value) {
                $fields->push([
                    'judul_pilihan' => isset($judul_pilihan[$key]) ? $judul_pilihan[$key] : null,
                    'deskripsi_pilihan' => isset($deskripsi_pilihan[$key]) ? $deskripsi_pilihan[$key] : null,
                    'harga_pilihan' => isset($harga_pilihan[$key]) ? $harga_pilihan[$key] : null,
                    'kewajiban_pilihan' => isset( $kewajiban_pilihan[$key]) ? $kewajiban_pilihan[$key] : null,
                    'nama_pilihan' => isset($nama_pilihan[$key]) ? $nama_pilihan[$key]:null,
                ]);
            }
        }

        $pilihan_ekstra['pilihan_ekstra'] = $fields;

        $rental['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $rental['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $rental['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = true;

        return view('BE.seller.rental.pilihan-addlisting', compact('pilihan_ekstra', 'rental', 'isedit', 'id','master_pilihan'));
    }
    public function editPilihanEkstra(Request $request, $id)
    {
        // dd($request->ekstra);
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();
        
        $pilihan = Pilihan::where('id', $detail->productdetail->pilihan_id)->first();

        $detailproduct = ProductDetail::findOrFail($detail->productdetail->id);
        $detailproduct->izin_ekstra=$request->ekstra;
        $detailproduct->save();
        
        $judul_pilihan = collect([]);
        $deskripsi_pilihan = collect([]);
        $harga_pilihan = collect([]);   
        $kewajiban_pilihan = collect([]);    
        $nama_pilihan = collect([]);    

        if (isset($request->fields)) {
            if (count($request->fields) !== 0) {
                foreach ($request->fields as $key => $value) {
                    $judul_pilihan->push($value['nama_pilihan']);
                    // $deskripsi_pilihan->push($value['deskripsi_pilihan']);
                    $harga_pilihan->push($value['harga_pilihan']);
                    $kewajiban_pilihan->push($value['kewajiban_pilihan']);
                    $nama_pilihan->push($value['nama_pilihan']);
                }
            }
        }

        $pilihan_detail = $pilihan->update([
            'judul_pilihan' => null,
            'deskripsi_pilihan' => null,
            'harga_pilihan' => json_encode($harga_pilihan),
            'kewajiban_pilihan' => json_encode($kewajiban_pilihan),
            'nama_pilihan' => json_encode($nama_pilihan)
        ]);

        // dd($detailproduct);
        
        // return redirect()->back();
        // return redirect()->route('rental.viewFaq');
        return redirect()->route('dashboardmyListing');
    }

    public function rentalEditGallery(Request $request, $id){
        
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $kendaraan = DetailKendaraan::where('id', $detail->productdetail->id_detail_kendaraan)->first();

        // dd($request->all());
        $session_gallery = $request->session()->get('add_listing.gallery');

        $gallery = '';
        $fields = collect([]);

        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {

                $new_foto = time() . 'GalleryProductRental' . $value->getClientOriginalName();
                $tujuan_upload = 'Seller/Product/Rental/Gallery/';
                $value->move($tujuan_upload, $new_foto);
                $gallery = $tujuan_upload . $new_foto;

                // 
                $fields->push([
                    'gallery' => $gallery,
                ]);
            }
        }

        //delete old image
        if (isset($session_gallery)) {
            foreach (json_decode($session_gallery) as $key => $value) {
                // dump($value->gallery);
                if (file_exists($value->gallery)) {
                    unlink($value->gallery);
                }
            }
        }

        // $request->session()->put('rental.add_listing.gallery', $fields);
        $product_detail = $detail->productdetail()->update([
            'gallery'=>json_encode($fields),
            // 'izin_ekstra' => 
        ]);

        return array(
            'status' => 1,
            'message' => 'Foto Gallery berhasil diperbarui.',
        );
    }

    public function getTagLocation(){
        $data = [];

        $provinces = Province::all();
        $i=0;
        $x=0;
        $y=0;
        
        //foreach
        foreach($provinces as $province){
            $data[$i]['provinces_id'] =$province->id;
            $data[$i]['provinces_name']=$province->name;
           
            $regencies = Regency::where('province_id',$province->id)->get();

            foreach($regencies as $regency){
                $data[$i]['regency'][$x]['regencies_id'] =$regency->id;
                $data[$i]['regency'][$x]['regencies_name']=$regency->name;
                $x++;
                    
                $districts = District::where('regency_id',$regency->id)->groupBy('name')->get();

                foreach($districts as $district){
                    $data[$i]['district'][$y]['districts_id'] =$district->id;
                    $data[$i]['district'][$y]['districts_name']=$district->name;

                    $y++;
                }
            }

            $i++;
        }

        return $data;
    }
}
