<?php

namespace App\Http\Controllers\Seller\Rental;


use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Paket;
use App\Models\Pilihan;
use App\Models\MasterChoice;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\Mobildetail;
use App\Models\MerekMobil;
use App\Models\MerekBus;
use App\Models\JenisMobil;
use App\Models\Kategori;
use App\Models\DetailKendaraan;
use App\Models\detailWisata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ListingRentalController extends Controller
{
    public function getmerekmobil(Request $request)
    {
        $id_jenis_mobil = $request->id_jenis_mobil;

        $merekmobils = Merekmobil::where('jenis_id', $id_jenis_mobil)->get();
        
        // echo json_encode($merekmobils->toArray());
        // die;

        foreach ($merekmobils as $merekmobil) {
            echo "<option value='$merekmobil->id'>$merekmobil->merek_mobil</option>";
        }
    }
    public function getmerekbus(Request $request)
    {
        $id_jenis_bus = $request->id_jenis_bus;

        $merekbus = Merekbus::where('id_jenis', $id_jenis_bus)->get();

        foreach ($merekbus as $mb) {
            echo "<option value='$mb->id_jenis'>$mb->merek_bus</option>";
        }
    }

    public function index(Request $request)
    {
        $pulau     = DB::select('select * from pulau');
        $provinsi  = Province::all();
        $kabupaten = Regency::all();
        $kecamatan = '';
        $kelurahan = '';
        $objek     = detailWisata::all();

        $dynamic_tag = [];
        
        $i =0;
        $x =0;
        $z =0;
        $y =0;
        
        foreach($pulau as $p){
            
            $prov = Province::where('island_id',$p->id)->get();
            
            $dynamic_tag['pulau'][$i]['id_pulau'] = $p->id;
            $dynamic_tag['pulau'][$i]['nama_pulau'] =$p->island_name;
            
            foreach($prov as $pv){
                
                $dynamic_tag['pulau'][$i]['provinsi'][$x]['id_provinsi']=$pv->id;
                $dynamic_tag['pulau'][$i]['provinsi'][$x]['nama_provinsi']=$pv->name;
                
                $kab = Regency::where('province_id',$pv->id)->get();
                
                foreach($kab as $k){

                    $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['id_kabupaten']=$k->id;
                    $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['nama_kabupaten']=$k->name;


                    $wisata =detailWisata::where('kabupaten_id',$k->id)->get();
                    foreach($wisata as $w){
                       
                        $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['wisata'][$y]['id_wisata']=$w->id;
                        $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['wisata'][$y]['nama_wisata']=$w->namaWisata;
                       
                        $y++;
                    }

                    $z++;

                }    
                
                $x++;
            }

            $i++;
        }

        $dynamic_tag = collect($dynamic_tag);
        $locations = $this-> getTagLocation();
        // dd(collect($dynamic_tag));
       /*  if ($request->session()->has('rental.add_listing')) {
            $kabupaten = Regency::where('id', $request->session()->get('rental.add_listing.regency_id'))->first();
            $kecamatan = District::where('id', $request->session()->get('rental.add_listing.district_id'))->first();
            $kelurahan = Village::where('id', $request->session()->get('rental.add_listing.village_id'))->first();
        } */

        $user_id = Auth::user()->id;
        if ($request->session()->missing('rental.product_code')) {

            $product_recent = Product::where('type', 'rental')->orderBy('id', 'desc')->first();
            $code = isset($product_recent) ? $product_recent->product_code : 1;

            // dd($code);

            if ($code != 1) {
                $code = explode("RT", $code);
                $code = substr($code[1], 0, -8);
                $code++;
            }

            $product = Product::create([
                'user_id' => $user_id,
                'type' => 'rental',
            ]);

            $request->session()->put('rental.add_listing.product_id', $product->id);
            $product_id = Product::where('id', $product->id)->first();

            $product_code = $product->id . 'RT' . $code . date('dmY');
            $product->slug = "";

            $product_id->update([
                'product_code' => $product_code,
            ]);

            $request->session()->put('rental.product_code', $product_code);
        }

        $jenismobil = JenisMobil::where('user_id',$user_id)->groupBy('jenis_kendaraan')->get();
        // dd($jenismobil);
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $listing = $request->session()->get('rental.product_code');
        $add_listing = $request->session()->has('rental.add_listing') ? $request->session()->get('rental.add_listing') : '';
        $rental = $request->session()->has('rental') ? $request->session()->get('rental') : '';
        $isedit =false;
        
        return view('BE.seller.rental.add-listing', compact(
            'pulau',
            'provinsi',
            'kabupaten',
            'kecamatan',
            'kelurahan',
            'objek',
            'jenismobil',
            'rental',
            'add_listing',
            'listing',
            'hotel',
            'xstay',
            'locations',
            'isedit',
            'dynamic_tag'
        ));
    }

    public function addListing(Request $request)
    {
        // $pesan=[
        //     'required'=>':attribute harus ditambahkan'
        // ];

        // $this->validate($request,[
        //     'details'=>'required'
        // ],$pesan);
        
        // $gallery = '';
        // $fields = collect([]);

        // if ($request->hasFile('gallery')) {
        //     $image = $request->file('gallery');

        //     foreach ($image as $key => $value) {

        //         $new_foto = time() . 'GalleryProductRental' . $value->getClientOriginalName();
        //         $tujuan_upload = 'Seller/Product/Rental/Gallery/';
        //         $value->move($tujuan_upload, $new_foto);
        //         $gallery = $tujuan_upload . $new_foto;

        //         $fields->push([
        //             'gallery' => $gallery,
        //         ]);
        //     }

        //     //delete old image
        //     if ($request->session()->has('rental.add_listing.gallery')) {
        //         foreach ($request->session()->get('rental.add_listing.gallery') as $key => $value) {
        //             if (file_exists($value['gallery'])) {
        //                 unlink($value['gallery']);
        //             }
        //         }
        //     }
        // }

        // dd($request->all());

        $new_tag =[];
        $new_tag['results']=$request->new_tag_location;

        $request->session()->put('rental.add_listing.nama_kendaraan', $request->nama_kendaraan);
        $request->session()->put('rental.add_listing.id_jenis_mobil', $request->jenismobil);
        $request->session()->put('rental.add_listing.id_merek_mobil', $request->merekmobil);

        $request->session()->put('rental.add_listing.tag_location_1', $request->tag_location_1);
        $request->session()->put('rental.add_listing.tag_location_2', $request->tag_location_2);
        $request->session()->put('rental.add_listing.tag_location_3', $request->tag_location_3);
        $request->session()->put('rental.add_listing.tag_location_4', $request->tag_location_4);
        $request->session()->put('rental.add_listing.new_tag_location', json_encode($new_tag));

        $request->session()->put('rental.add_listing.district_id', $request->kecamatan);
        $request->session()->put('rental.add_listing.village_id', $request->kelurahan);

        $request->session()->put('rental.add_listing.kapasitas_kursi', $request->kapasitas_kursi);
        $request->session()->put('rental.add_listing.kapasitas_koper', $request->kapasitas_koper);
        $request->session()->put('rental.add_listing.harga_akomodasi', $request->harga_akomodasi);
        $request->session()->put('rental.add_listing.status', $request->status);
        // $request->session()->put('rental.add_listing.gallery', $fields);
        $request->session()->put('rental.add_listing.deskripsi', $request->deskripsi);
        $request->session()->put('rental.add_listing.komplemen', $request->catatan);
        $request->session()->put('rental.add_listing.pilihan_tambahan', $request->pilihan_tambahan);

        // dd($request->session()->get('rental.add_listing'));
        return redirect()->route('rental.viewPilihan');
    }

    public function rentalAddGallery(Request $request){
        
        // dd($request->all());
        $session_gallery = $request->session()->get('add_listing.gallery');

        $gallery = '';
        $fields = collect([]);

        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {

                $new_foto = time() . 'GalleryProductRental' . $value->getClientOriginalName();
                $tujuan_upload = 'Seller/Product/Rental/Gallery/';
                $value->move($tujuan_upload, $new_foto);
                $gallery = $tujuan_upload . $new_foto;

                // 
                $fields->push([
                    'gallery' => $gallery,
                ]);
            }
        }

        //delete old image
        if (isset($session_gallery)) {
            foreach (json_decode($session_gallery) as $key => $value) {
                // dump($value->gallery);
                if (file_exists($value->gallery)) {
                    unlink($value->gallery);
                }
            }
        }

        $request->session()->put('rental.add_listing.gallery', $fields);
        
        return array(
            'status' => 1,
            'message' => 'Foto Gallery berhasil diperbarui.',
        );
    }

    public function viewPilihan(Request $request)
    {

        $pilihan_ekstra = '';
        $master_pilihan = MasterChoice::where('product_type','transfer and rental')->get();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->orWhere('type', 'xstay')->get();
        // $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        // dd($hotel);
        if ($request->session()->has('rental.pilihan_ekstra')) {
            $pilihan_ekstra = $request->session()->get('rental.pilihan_ekstra');
        } else {
            $request->session()->put('rental.pilihan_ekstra', '');
            $pilihan_ekstra = '';
        }

        $isedit=false;

        return view('BE.seller.rental.pilihan-addlisting', compact('pilihan_ekstra', 'hotel', 'master_pilihan','isedit'));
    }

    public function addPilihan(Request $request)
    {
        // dd($request->all());

        $request->session()->put('rental.pilihan_ekstra.ekstra', $request->ekstra_radio_button);

        $fields = collect([]);

        if ($request->fields) {
            foreach ($request->fields as $key => $value) {
                $fields->push([
                    'judul_pilihan' => isset($value['nama_pilihan']) ? $value['nama_pilihan']:null,
                    'deskripsi_pilihan' => null,
                    'harga_pilihan' => $value['harga_pilihan'],
                    'kewajiban_pilihan' =>isset($value['kewajiban_pilihan']) ? $value['kewajiban_pilihan']:null,
                    'nama_pilihan' =>isset($value['nama_pilihan']) ? $value['nama_pilihan']:null
                ]);
            }
        }
        // dd($fields);
        $request->session()->put('rental.pilihan_ekstra.pilihan', $fields);

        $user_id = Auth::user()->id;
        $kategori = Kategori::where('nama_kategori', 'rental')->first();
        $gallery = $request->session()->get('rental.add_listing.gallery');

        $product_id = Product::where('product_code', $request->session()->get('rental.product_code'))->first();
        $product_id->slug = "";
        $product_id->update([
            'product_name' => $request->session()->get('rental.add_listing.nama_kendaraan'),
        ]);

        $judul_pilihan = collect([]);
        $deskripsi_pilihan = collect([]);
        $harga_pilihan = collect([]);
        $kewajiban_pilihan = collect([]);
        $nama_pilihan = collect([]);

        foreach ($request->session()->get('rental.pilihan_ekstra.pilihan') as $key => $value) {
            $judul_pilihan->push($value['judul_pilihan']);
            $deskripsi_pilihan->push($value['deskripsi_pilihan']);
            $harga_pilihan->push($value['harga_pilihan']);
            $kewajiban_pilihan->push($value['kewajiban_pilihan']);
            $nama_pilihan->push(isset($value['nama_pilihan'])  ? $value['nama_pilihan'] : null);
        }

        $pilihan = Pilihan::create([
            'judul_pilihan' => json_encode($judul_pilihan),
            'deskripsi_pilihan' => json_encode($deskripsi_pilihan),
            'harga_pilihan' => json_encode($harga_pilihan),
            'kewajiban_pilihan' => json_encode($kewajiban_pilihan),
            'nama_pilihan' => json_encode($nama_pilihan),
        ]);

        $detailkendaraan = DetailKendaraan::create([
            'id_jenis_bus' => $request->session()->get('rental.add_listing.id_jenis_bus'),
            'id_jenis_mobil' => $request->session()->get('rental.add_listing.id_jenis_mobil'),
            'id_merek_mobil' => $request->session()->get('rental.add_listing.id_merek_mobil'),
            'nama_kendaraan' => $request->session()->get('rental.add_listing.nama_kendaraan'),
            'kapasitas_kursi' => $request->session()->get('rental.add_listing.kapasitas_kursi'),
            'kapasitas_koper' => $request->session()->get('rental.add_listing.kapasitas_koper'),
            'harga_akomodasi' => $request->session()->get('rental.add_listing.harga_akomodasi'),
            'status' => $request->session()->get('rental.add_listing.status'),
            'pilihan_tambahan' => $request->session()->get('rental.add_listing.pilihan_tambahan'),
        ]);

        $productdetail = Productdetail::create([
            'product_id' => $request->session()->get('rental.add_listing.product_id'),
            'tag_location_1' => $request->session()->get('rental.add_listing.tag_location_1'),
            'tag_location_2' => $request->session()->get('rental.add_listing.tag_location_2'),
            'tag_location_3' => $request->session()->get('rental.add_listing.tag_location_3'),
            'tag_location_4' => $request->session()->get('rental.add_listing.tag_location_4'),
            'new_tag_location' => $request->session()->get('rental.add_listing.new_tag_location'),
            'district_id' => $request->session()->get('rental.add_listing.district_id'),
            'village_id' => $request->session()->get('rental.add_listing.village_id'),
            'pilihan_id' => $pilihan->id,
            'id_detail_kendaraan' => $detailkendaraan->id,
            'kategori_id' => $kategori->id,
            'deskripsi' => $request->session()->get('rental.add_listing.deskripsi'),
            'catatan' => $request->session()->get('rental.add_listing.komplemen'),
            'komplemen' => $request->session()->get('rental.add_listing.komplemen'),
            'pilihan_tambahan' => $request->session()->get('rental.add_listing.pilihan_tambahan'),
            'gallery' => json_encode($gallery), 
            'izin_ekstra' => $request->session()->get('rental.pilihan_ekstra.ekstra'),
            'base_url' => url('/') . '/rental/' . $product_id->slug,
        ]);

        $request->session()->forget('rental');

        return redirect()->route('dashboardmyListing');
    }

    public function viewFaq(Request $request)
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'rental'
        ])->has('productdetail')->with('productdetail')->get();

        return view('BE.seller.rental.faq-rental', compact('data', 'xstay', 'faqs'));
    }

    public function viewFaqCode(Request $request)
    {
        if ($request->product_rental == null) {
            return redirect()->route('rental.viewFaq');
        }
        
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $product = Product::where('id', $request->product_rental)->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'rental'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('id', $request->product_rental)->with('productdetail')->first();

        // return view('BE.seller.hotel.faq', compact('data', 'xstay', 'faqs', 'faq_content'));
        return redirect()->route('rental.viewFaq.edit', $product->product_code);
    }

    public function viewFaqEdit($code)
    {
        if ($code == null) {
            return redirect()->route('rental.viewFaq');
        }

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'rental'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('product_code', $code)->where('user_id', auth()->user()->id)->with('productdetail')->first();

        return view('BE.seller.rental.faq-rental', compact('faqs', 'faq_content', 'data', 'xstay'));
    }

    public function viewFaqPost(Request $request, $id)
    {

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'rental'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('id', $id)->with('productdetail')->first();
        $faq_update = Productdetail::where('product_id', $id)->first();

        $faq_update->update([
            'faq' => $request->faq,
        ]);

        return redirect()->route('rental.viewFaq');
    }

    public function getTagLocation(){
        $data = [];

        $provinces = Province::all();
        $i=0;
        $x=0;
        $y=0;
        
        //foreach
        foreach($provinces as $province){
            $data[$i]['provinces_id'] =$province->id;
            $data[$i]['provinces_name']=$province->name;
           
            $regencies = Regency::where('province_id',$province->id)->get();

            foreach($regencies as $regency){
                $data[$i]['regency'][$x]['regencies_id'] =$regency->id;
                $data[$i]['regency'][$x]['regencies_name']=$regency->name;
                $x++;
                    
                $districts = District::where('regency_id',$regency->id)->groupBy('name')->get();

                foreach($districts as $district){
                    $data[$i]['district'][$y]['districts_id'] =$district->id;
                    $data[$i]['district'][$y]['districts_name']=$district->name;

                    $y++;
                }
            }

            $i++;
        }

        return $data;
    }
}
