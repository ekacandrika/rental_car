<?php

namespace App\Http\Controllers\Seller\Transfer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Paket;
use App\Models\Pilihan;
use App\Models\MasterChoice;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\Mobildetail;
use App\Models\BusDetail;
use App\Models\MerekMobil;
use App\Models\DetailKendaraan;
use App\Models\DetailRute;
use App\Models\JenisMobil;
use App\Models\LayoutBus;
use App\Models\MerekBus;
use App\Models\JenisBus;
use App\Models\Masterroute;
use App\Models\Kategori;
use App\Models\MasterDropPick;
use Illuminate\Http\Request;

class EditListingTransferController extends Controller
{
    public function showAddDetailUmum(Request $request, $id)
    {
        $user_id = Auth::user()->id;
        $jenismobil = JenisMobil::where('user_id',$user_id)->get();

        $layoutbus = DB::table('layout_kursi')->where('user_id', $user_id)
                    ->join('users', 'layout_kursi.user_id', '=', 'users.id')
                    ->get();
                    
        $jenisbus = DB::table('jenis_bus')->where('user_id', $user_id)
                    ->join('users', 'jenis_bus.user_id', '=', 'users.id')
                    ->get();

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $kendaraan = DetailKendaraan::where('id', $detail->productdetail->id_detail_kendaraan)->first();
        $kendaraan['nama_kendaraan'] = $kendaraan->nama_kendaraan;
        $kendaraan['kapasitas_kursi'] = $kendaraan->kapasitas_kursi;
        $kendaraan['kapasitas_koper'] = $kendaraan->kapasitas_koper;
        $kendaraan['pilihan_tambahan'] = $kendaraan->pilihan_tambahan;
        $kendaraan['layout_bus_id'] = $kendaraan->layout_bus_id;
        
        $listing = $detail->product_code;
        // dd($kendaraan);
        $add_listing['id_jenis_bus'] = $kendaraan->id_jenis_bus;
        $add_listing['id_jenis_mobil'] = $kendaraan->id_jenis_mobil;
        $add_listing['gallery'] = $detail->productdetail->gallery;
        $add_listing['product_name'] = $detail->product_name;
        $add_listing['deskripsi'] = $detail->productdetail->deskripsi;
        $add_listing['catatan'] = $detail->productdetail->catatan;
        $add_listing['pilihan_tambahan'] = $kendaraan['pilihan_tambahan'];
        
        $transfer['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $transfer['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $transfer['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = true;

        if($detail->productdetail->tipe_tur=='Transfer Umum'){
            return view('BE.seller.transfer.Edit.edit-listing-umum', compact('jenismobil','kendaraan','jenisbus','layoutbus','detail', 'listing', 'add_listing', 'transfer', 'id', 'isedit'));
        }

        return view('BE.seller.transfer.Edit.edit-listing-schedule', compact('jenismobil','kendaraan','jenisbus','layoutbus','detail', 'listing', 'add_listing', 'transfer', 'id', 'isedit'));

    }
    
    // Edit Private

    // Edit Private End


    public function editAddDetailUmum(Request $request, $id)
    {
        // dd($request->all());
        $gallery='';
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $kendaraan = DetailKendaraan::where('id', $detail->productdetail->id_detail_kendaraan)->first();

        $kendaraan->update([
            'nama_kendaraan' => $request->nama_kendaraan,
            'kapasitas_kursi' => $request->kapasitas_kursi,
            'kapasitas_koper' => $request->kapasitas_koper,
            'pilihan_tambahan' => $request->pilihan_tambahan,
            'id_jenis_bus' => $request->jenisbus,
            'id_merek_mobil' => $request->merekbus,
            'layout_bus_id'=> $request->layoutbus
        ]);

        $gambar = collect([]);

        if($request->hasFile('gallery')){
            $imgs = $request->file('gallery');
            foreach($imgs as $key =>$img){
                $foto   = time().'GalleryProductTrasfer'.$img->getClientOriginalName();
                $tujuan = 'Seller/Product/Gallery/';

                $img->move($tujuan,$foto);
                $gallery=$tujuan.$foto;

                $gambar->push([
                    'gallery'=>$gallery
                ]);
            }
        }

        if($request->gallery){
            if(isset($detail->productdetail->gallery) && count(json_decode($detail->productdetail->gallery)) > 1){
                    
                foreach(json_decode($detail->productdetail->gallery) as $key => $v){
                    if(file_exists($v->gallery)){
                        unlink($v->gallery);
                    }
                }
            }
        }

        $product_detail = $detail->productdetail()->update([
            'deskripsi' => $request->deskripsi,
            'catatan' => $request->catatan,
            'tipe_tur'=>$request->status,
            'gallery'=>json_encode($gambar)
        ]);

        $detail->update([
            'product_name' => $request->product_name,
        ]);

        $request->session()->put('transfer.add_listing.type', $request->status);

        return redirect()->route('transfer.viewRuteSchedule.edit', $id);
    }

    // public function 

    public function editAddDetailPrivate(Request $request, $id){
        // dd($request->all());
        $gallery='';
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $kendaraan = DetailKendaraan::where('id', $detail->productdetail->id_detail_kendaraan)->first();

        $kendaraan->update([
            'nama_kendaraan' => $request->nama_kendaraan,
            'kapasitas_kursi' => $request->kapasitas_kursi,
            'kapasitas_koper' => $request->kapasitas_koper,
            'id_jenis_mobil' => $request->jenismobil,
            'id_merek_mobil' => $request->merekmobil,
            'pilihan_tambahan' => $request->pilihan_tambahan,
        ]);

        $gambar = collect([]);

        if($request->hasFile('gallery')){
            $imgs = $request->file('gallery');
            foreach($imgs as $key =>$img){
                $foto   = time().'GalleryProductTrasfer'.$img->getClientOriginalName();
                $tujuan = 'Seller/Product/Gallery/';

                $img->move($tujuan,$foto);
                $gallery=$tujuan.$foto;

                $gambar->push([
                    'gallery'=>$gallery
                ]);
            }
        }

        if($request->gallery){
            if(isset($detail->productdetail->gallery) && count(json_decode($detail->productdetail->gallery)) > 1){
                    
                foreach(json_decode($detail->productdetail->gallery)as $key => $v){
                    if(file_exists($v->gallery)){
                        unlink($v->gallery);
                    }
                }
            }
        }

        $product_detail = $detail->productdetail()->update([
            'deskripsi' => $request->deskripsi,
            'catatan' => $request->catatan,
            'tipe_tur'=>$request->status,
            'gallery'=>json_encode($gambar)
        ]);

        $detail->update([
            'product_name' => $request->product_name,
        ]);

        $request->session()->put('transfer.add_listing.type', $request->status);

        return redirect()->route('transfer.viewRute.edit',$id);
    }

    public function showEditScheduleRute($id){
        
        $user_id = Auth::user()->id;
        $master_drop_pick = MasterDropPick::all();
        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        // $detail_rute = DetailRute::where('id',$detail->productdetail->drop_pick_detail)->first();
        $detail_rute = isset($detail->productdetail->drop_pick_detail) ? json_decode($detail->productdetail->drop_pick_detail, true)['result'] : [];
        
        // dd($detail_rute);
        $drop_pick = collect([]);

        foreach ($detail_rute as $key => $rute) {
            # code...
            $drop_pick->push([
                'judul_bus'=>$rute['judul_bus'],
                'id_pickup'=>$rute['id_pickup'],
                'latitude_pickup'=>$rute['latitude_pickup'],
                'longitude_pickup'=>$rute['longitude_pickup'],
                'tempat_pickup'=>$rute['tempat_pickup'],
                'id_drop'=>$rute['id_drop'],
                'latitude_drop'=>$rute['latitude_drop'],
                'longitude_drop'=>$rute['longitude_drop'],
                'tempat_drop'=>$rute['tempat_drop'],
                'harga'=>$rute['harga']
            ]);
        }
        // $fields = collect([]);
        // // dd($detail->productdetail);
 
        // $judul_rute = $detail_rute['judul_rute'];
        // $id_naik = $detail_rute['id_naik'];
        // $naik_long = $detail_rute['naik_long'];
        // $naik_long = $detail_rute['naik_long'];
        // $naik_lat = $detail_rute['naik_lat'];
        // $id_turun = $detail_rute['id_turun'];
        // $turun_long = $detail_rute['turun_long'];
        // $turun_lat = $detail_rute['turun_lat'];
        // $harga = $detail_rute['harga'];

        // $rute['judul_rute'] = $judul_rute;
        // $rute['id_naik'] = $id_naik;
        // $rute['naik_long'] = $naik_long;
        // $rute['naik_lat'] = $naik_lat;
        // $rute['id_turun'] = $id_turun;
        // $rute['turun_long'] = $turun_long;
        // $rute['turun_lat'] = $turun_lat;
        // $rute['harga'] = $harga;

        $transfer['add_listing']=$detail->productdetail->rute_id;
        $transfer['rute']=$drop_pick;
        $transfer['rute_id']=$detail->productdetail->rute_id;
        $transfer['batas']=$detail->productdetail->batas_pembayaran;
        $transfer['pilihan_ekstra']=isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = true; 
 
        return view('BE.seller.transfer.private.rute-schedule', compact('rute','transfer','isedit','id','master_drop_pick'));
        
    }

    public function editRuteSchedule(Request $request, $id){
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $rutes = $request->rute;
        $drop_pick = collect([]);
        


        foreach ($rutes as $key => $rute) {
            # code...
            $pickup = MasterDropPick::where('id',$rute['id_naik'])->first();
            $drop = MasterDropPick::where('id',$rute['id_turun'])->first();

            // dump($pickup);

            $drop_pick->push([
                'judul_bus'=>$rute['judul_bus'],
                'id_pickup'=>$rute['id_naik'],
                'latitude_pickup'=>$pickup->latitude,
                'longitude_pickup'=>$pickup->longitude,
                'tempat_pickup'=>$pickup->judulDropPick,
                'id_drop'=>$rute['id_turun'],
                'latitude_drop'=>$drop->latitude,
                'longitude_drop'=>$drop->longitude,
                'tempat_drop'=>$drop->judulDropPick,
                'harga'=>$rute['harga']
            ]);
        }   

        $drop_pick_detail['result'] = $drop_pick;

        $detail->productdetail->update([
            'drop_pick_detail'=>json_encode($drop_pick_detail)
        ]);

        // $detail_rute = DetailRute::where('id',$detail->productdetail->rute_id)->first();        
        // $detail_rute->update([
        //     'judul_rute' => json_encode($request->judul_rute),
        //     'naik_long' => $request->naik_long,
        //     'naik_lat' => $request->naik_lat,
        //     'turun_long' => $request->turun_long,
        //     'turun_lat' => $request->turun_lat,
        //     'harga' => $request->harga,
        // ]);

        $transfer['add_listing']=$detail->productdetail->rute_id;
        $transfer['rute']=$detail->productdetail->rute_id;
        $transfer['batas']=$detail->productdetail->batas_bayar;
        $transfer['pilihan_ekstra']=isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = true;

        return redirect()->route('transfer.viewBatasEdit', $id);
        
    }
    
    public function showEditRute($id){
       
        $user_id = Auth::user()->id;
        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $detail_rute = DetailRute::join('masterroutes as b','b.id','=','detail_rute.id_rute')->where('detail_rute.id_rute',$detail->productdetail->rute_id)->first();
        
        $data =json_decode($detail_rute);

        // dd($data);
        $transfer['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $transfer['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $transfer['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
       
        $isedit = true;
        return view('BE.seller.transfer.edit.rute-listing', compact('data','transfer','isedit','id'));

    }

    public function editRute(Request $request, $id){
        $user_id = Auth::user()->id;
        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $detail_rute = DetailRute::where('id',$detail->productdetail->rute_id)->first();
        
        $detail_rute->update([
            'id_rute'=>$request->id_rute,
            'harga'=>$request->harga
        ]);

        // edit regency_id
        $detail->productdetail->update([
            'regency_id'=>$request->regency_id
        ]);
        

        $isedit = true;

        return redirect()->route('transfer.viewBatasEdit', $id);
    }

    public function showEditBatas(Request $request, $id)
    {
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $fields = collect([]);
        $kebijakan_pembatalan_sebelumnya = json_decode($detail->productdetail->kebijakan_pembatalan_sebelumnya);
        $kebijakan_pembatalan_sesudah = json_decode($detail->productdetail->kebijakan_pembatalan_sesudah);
        $kebijakan_pembatalan_potongan = json_decode($detail->productdetail->kebijakan_pembatalan_potongan);
        // dd($detail->productdetail);
        foreach ($kebijakan_pembatalan_sebelumnya as $key => $value) {
            $fields->push([
                'kebijakan_pembatalan_sebelumnya' => $kebijakan_pembatalan_sebelumnya[$key],
                'kebijakan_pembatalan_sesudah' => $kebijakan_pembatalan_sesudah[$key],
                'kebijakan_pembatalan_potongan' => $kebijakan_pembatalan_potongan[$key],
            ]);
        }

        $batas['batas_pembayaran'] = $detail->productdetail->batas_pembayaran;
        $batas['pembatalan'] = $fields;

        $transfer['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $transfer['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $transfer['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = true;
        
        $type = $request->session()->get('transfer.add_listing.type');

        return view('BE.seller.transfer.private.pembayaran-addlisting', compact('batas', 'transfer', 'isedit', 'id','type'));
    }

    public function editBatas(Request $request, $id)
    {
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $kebijakan_pembatalan_sebelumnya = collect([]);
        $kebijakan_pembatalan_sesudah = collect([]);
        $kebijakan_pembatalan_potongan = collect([]);    

        if (isset($request->fields)) {
            if (count($request->fields) !== 0) {
                foreach ($request->fields as $key => $value) {
                    $kebijakan_pembatalan_sebelumnya->push($value['kebijakan_pembatalan_sebelumnya']);
                    $kebijakan_pembatalan_sesudah->push($value['kebijakan_pembatalan_sesudah']);
                    $kebijakan_pembatalan_potongan->push($value['kebijakan_pembatalan_potongan']);
                }
            }
        }

        $product_detail = $detail->productdetail()->update([
            'batas_pembayaran' => $request->batas_pembayaran,
            'kebijakan_pembatalan_sebelumnya' => json_encode($kebijakan_pembatalan_sebelumnya),
            'kebijakan_pembatalan_sesudah' => json_encode($kebijakan_pembatalan_sesudah),
            'kebijakan_pembatalan_potongan' => json_encode($kebijakan_pembatalan_potongan)
        ]);

        return redirect()->route('transfer.viewPilihanEdit', $id);
    }

    public function showEditPilihanEkstra(Request $request, $id)
    {
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $master_pilihan = MasterChoice::where('product_type','transfer and rental')->get();   
        $pilihan = Pilihan::where('id', $detail->productdetail->pilihan_id)->first();
        // dd($pilihan);
        $pilihan_ekstra['ekstra_radio_button'] = $detail->productdetail->izin_ekstra;

        $fields = collect([]);
        $judul_pilihan = json_decode($pilihan->judul_pilihan);
        $deskripsi_pilihan = json_decode($pilihan->deskripsi_pilihan);
        $harga_pilihan = json_decode($pilihan->harga_pilihan);
        $kewajiban_pilihan = json_decode($pilihan->kewajiban_pilihan);
        $nama_pilihan = json_decode($pilihan->nama_pilihan);

        foreach ($judul_pilihan as $key => $value) {
            $fields->push([
                'judul_pilihan' => $judul_pilihan[$key],
                'deskripsi_pilihan' => $deskripsi_pilihan[$key],
                'harga_pilihan' => $harga_pilihan[$key],
                'kewajiban_pilihan' => $kewajiban_pilihan[$key],
                'nama_pilihan'=>isset($nama_pilihan[$key]) ? $nama_pilihan[$key]:null
            ]);
        }

        $pilihan_ekstra['pilihan_ekstra'] = $fields;

        $transfer['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $transfer['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $transfer['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = true;
        $type = $request->session()->get('transfer.add_listing.type');

        return view('BE.seller.transfer.private.pilihan-addlisting', compact('pilihan_ekstra', 'transfer', 'isedit', 'id','master_pilihan','type'));
    }

    public function editPilihanEkstra(Request $request, $id)
    {
        // dd($request->all());
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();
        $pilihan = Pilihan::where('id', $detail->productdetail->pilihan_id)->first();
        // dd($pilihan);
        $detail->productdetail()->update([
            'izin_ekstra' => $request->ekstra,
        ]);

        $judul_pilihan = collect([]);
        $deskripsi_pilihan = collect([]);
        $harga_pilihan = collect([]);   
        $kewajiban_pilihan = collect([]);    
        $nama_pilihan = collect([]);    

        if (isset($request->fields)) {
            if (count($request->fields) !== 0) {
                foreach ($request->fields as $key => $value) {
                    // dd($value);
                    $judul_pilihan->push($value['nama_pilihan']);
                    // $deskripsi_pilihan->push($value['deskripsi_pilihan']);
                    $harga_pilihan->push($value['harga_pilihan']);
                    $kewajiban_pilihan->push($value['kewajiban_pilihan']);
                    $nama_pilihan->push($value['nama_pilihan']);
                }
            }
        }

        $pilihan_detail = $pilihan->update([
            'judul_pilihan' => json_encode($judul_pilihan),
            'deskripsi_pilihan' => null,
            'harga_pilihan' => json_encode($harga_pilihan),
            'kewajiban_pilihan' => json_encode($kewajiban_pilihan),
            'nama_pilihan' => json_encode($nama_pilihan)
        ]);
       


        return redirect()->route('dashboardmyListing');  
        // return redirect()->route('transfer.viewFaq');
    }

}
