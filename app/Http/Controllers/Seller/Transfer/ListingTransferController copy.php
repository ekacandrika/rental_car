<?php

namespace App\Http\Controllers\Seller\Transfer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Paket;
use App\Models\Pilihan;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\Mobildetail;
use App\Models\BusDetail;
use App\Models\MerekMobil;
use App\Models\DetailKendaraan;
use App\Models\DetailRute;
use App\Models\JenisMobil;
use App\Models\LayoutBus;
use App\Models\MerekBus;
use App\Models\JenisBus;
use App\Models\Masterroute;
use Illuminate\Http\Request;

class ListingTransferController extends Controller
{
    public function getmerekmobil(Request $request)
    {
        $id_jenis_mobil = $request->id_jenis_mobil;

        $merekmobils = Merekmobil::where('jenis_id', $id_jenis_mobil)->get();
        
        foreach ($merekmobils as $merekmobil) {
            echo "<option value='$merekmobil->id'>$merekmobil->merek_mobil</option>";
        }
    }

    public function index(Request $request)
    {
        $jenismobil = JenisMobil::all();

        $user_id = Auth::user()->id;

        $layoutbus = DB::table('layout_kursi')->where('user_id', $user_id)
                    ->join('users', 'layout_kursi.user_id', '=', 'users.id')
                    ->get();
                    
        $jenisbus = DB::table('jenis_bus')->where('user_id', $user_id)
                    ->join('users', 'jenis_bus.user_id', '=', 'users.id')
                    ->get();

        $add_listing = $request->session()->has('product.add_listing') ? $request->session()->get('product.add_listing') : '';

        return view('BE.seller.transfer.private.add-listing', compact('jenismobil', 'add_listing', 'layoutbus', 'jenisbus'));
    }

    public function addListingPrivate(Request $request){
        $user_id = Auth::user()->id;
        $gallery = '';
        $fields = collect([]);

        if ($request->hasFile('gallery')) {
            $image = $request->file('gallery');

            foreach ($image as $key => $value) {
                
                $new_foto = time() . 'GalleryProductTransferPrivate' . $value->getClientOriginalName();
                $tujuan_upload = 'Seller/Product/Transfer/Gallery/';
                $value->move($tujuan_upload, $new_foto);
                $gallery = $tujuan_upload . $new_foto;

                $fields->push([
                    'gallery' => $gallery,
                ]);
            }

            //delete old image
            if($request->session()->has('product.add_listing.gallery')){
                foreach ($request->session()->get('product.add_listing.gallery') as $key => $value) {
                    if (file_exists($value['gallery'])) {
                        unlink($value['gallery']);
                    }
                }
            }
        }

        $request->session()->forget('product');

        $request->session()->put('product.add_listing.nama_kendaraan', $request->nama_kendaraan);
        $request->session()->put('product.add_listing.id_jenis_mobil', $request->jenismobil);
        $request->session()->put('product.add_listing.kapasitas_kursi', $request->kapasitas_kursi);
        $request->session()->put('product.add_listing.kapasitas_koper', $request->kapasitas_koper);
        $request->session()->put('product.add_listing.status', $request->status);
        $request->session()->put('product.add_listing.gallery', $fields);
        $request->session()->put('product.add_listing.deskripsi', $request->deskripsi);
        $request->session()->put('product.add_listing.catatan', $request->catatan);

        $product = $request->session()->has('product.add_listing') ? $request->session()->get('product.add_listing') : '';

        return redirect()->route('transfer.viewRute');
    }

    public function addListingUmum(Request $request){
        $user_id = Auth::user()->id;
        $gallery = '';
        $fields = collect([]);

        if ($request->hasFile('gallery')) {
            $image = $request->file('gallery');

            foreach ($image as $key => $value) {
                
                $new_foto = time() . 'GalleryProductTransferUmum' . $value->getClientOriginalName();
                $tujuan_upload = 'Seller/Product/Transfer/Gallery/';
                $value->move($tujuan_upload, $new_foto);
                $gallery = $tujuan_upload . $new_foto;

                $fields->push([
                    'gallery' => $gallery,
                ]);
            }

            //delete old image
            if($request->session()->has('product.add_listing.gallery')){
                foreach ($request->session()->get('product.add_listing.gallery') as $key => $value) {
                    if (file_exists($value['gallery'])) {
                        unlink($value['gallery']);
                    }
                }
            }
        }

        $request->session()->forget('product');

        $request->session()->put('product.add_listing.nama_kendaraan', $request->nama_kendaraan);
        $request->session()->put('product.add_listing.id_jenis_bus', $request->jenisbus);
        $request->session()->put('product.add_listing.kapasitas_kursi', $request->kapasitas_kursi);
        $request->session()->put('product.add_listing.kapasitas_koper', $request->kapasitas_koper);
        $request->session()->put('product.add_listing.status', $request->status);
        $request->session()->put('product.add_listing.gallery', $fields);
        $request->session()->put('product.add_listing.deskripsi', $request->deskripsi);
        $request->session()->put('product.add_listing.catatan', $request->catatan);

        $product = $request->session()->has('product.add_listing') ? $request->session()->get('product.add_listing') : '';

        return redirect()->route('transfer.viewRute');
    }
    
    public function viewRute(Request $request){
        $rute = $request->session()->has('product.rute') ? $request->session->get('product.rute') : [];

        return view('BE.seller.transfer.private.rute-addlisting', compact('rute'));
    }
    
    public function viewRutee(Request $request, $id){
        $rute = $request->session()->has('product.rute') ? $request->session->get('product.rute') : [];

        $kecamatan = District::all();
        $data = DB::table('masterroutes')->where('id', $id)->get();
        $from = District::first();
        $to = District::first();

        return view('BE.seller.transfer.private.rute-listing', compact('rute', 'kecamatan', 'from', 'to', 'data'));
    }

    public function addRute(Request $request){
        $request->session()->put('product.rute.id_rute', $request->id_rute);
        $request->session()->put('product.rute.harga', $request->harga);
        
        $rute = $request->session()->has('product.rute') ? $request->session()->get('product.rute') : '';

        return redirect()->route('transfer.viewBatas');
    }
    
    public function viewBatas(Request $request){
        $batas = $request->session()->has('product.batas') ? $request->session->get('product.batas') : [];

        return view('BE.seller.transfer.private.pembayaran-addlisting', compact('batas'));
    }
    public function addBatas(Request $request){
        
        $request->session()->put('product.batas.batas_pembayaran', $request->batas_pembayaran);

        $fields = collect([]);

        foreach ($request->fields as $key => $value) {
            $fields->push([
                'kebijakan_pembatalan_sebelumnya' => $value['kebijakan_pembatalan_sebelumnya'],
                'kebijakan_pembatalan_sesudah' => $value['kebijakan_pembatalan_sesudah'],
                'kebijakan_pembatalan_potongan' => $value['kebijakan_pembatalan_potongan']
            ]);
        }

        $request->session()->put('product.batas.pembatalan', $fields);

        $batas = $request->session()->has('product.batas') ? $request->session()->get('product.batas') : '';

        
        return redirect()->route('transfer.viewPilihan');
    }

    public function viewPilihan(Request $request){
        $pilihan_ekstra = $request->session()->has('product.pilihan_ekstra') ? $request->session()->get('product.pilihan_ekstra') : [];

        return view('BE.seller.transfer.private.pilihan-addlisting', compact('pilihan_ekstra'));
    }

    public function addPilihan(Request $request){

        $fields = collect([]);

        foreach ($request->fields as $key => $value) {
            $fields->push([
                'judul_pilihan' => $value['judul_pilihan'],
                'deskripsi_pilihan' => $value['deskripsi_pilihan'],
                'harga_pilihan' => $value['harga_pilihan']
            ]);
        }

        $request->session()->put('product.pilihan_ekstra.pilihan', $fields);

        $user_id = Auth::user()->id;
        $gallery = $request->session()->get('product.add_listing.gallery');
        $kebijakan_pembatalan_sebelumnya = collect([]);
        $kebijakan_pembatalan_sesudah = collect([]);
        $kebijakan_pembatalan_potongan = collect([]);

        foreach($request->session()->get('product.batas.pembatalan') as $key => $value){
            $kebijakan_pembatalan_sebelumnya->push($value['kebijakan_pembatalan_sebelumnya']);
            $kebijakan_pembatalan_sesudah->push($value['kebijakan_pembatalan_sesudah']);
            $kebijakan_pembatalan_potongan->push($value['kebijakan_pembatalan_potongan']);
        }
        
        $product = Product::create([
            'user_id' => $user_id,
            'product_code' => 'AC' . mt_rand(1,99) . date('dmY'),
            'product_name' =>  'Transfer Mobil ' . $request->session()->get('product.add_listing.nama_mobil'),
            'type' => 'Transfer',
            'slug' => 'transfer-slug-testing',
        ]);

        $judul_pilihan = collect([]);
        $deskripsi_pilihan = collect([]);
        $harga_pilihan = collect([]);

        foreach ($request->session()->get('product.pilihan_ekstra.pilihan') as $key => $value) {
            $judul_pilihan->push($value['judul_pilihan']);
            $deskripsi_pilihan->push($value['deskripsi_pilihan']);
            $harga_pilihan->push($value['harga_pilihan']);
        }

        $pilihan = Pilihan::create([
            'judul_pilihan' => json_encode($judul_pilihan),
            'deskripsi_pilihan' => json_encode($deskripsi_pilihan),
            'harga_pilihan' => json_encode($harga_pilihan),
        ]);

        $detailkendaraan = DetailKendaraan::create([
            'id_jenis_bus' => $request->session()->get('product.add_listing.id_jenis_bus'),
            'id_jenis_mobil' => $request->session()->get('product.add_listing.id_jenis_mobil'),
            'nama_kendaraan' => $request->session()->get('product.add_listing.nama_kendaraan'),
            'kapasitas_kursi' => $request->session()->get('product.add_listing.kapasitas_kursi'),
            'kapasitas_koper' => $request->session()->get('product.add_listing.kapasitas_koper'),
            'status' => $request->session()->get('product.add_listing.status'),
        ]);

        $detailrute = DetailRute::create([
            'id_rute' => $request->session()->get('product.rute.id_rute'),
            'harga' => $request->session()->get('product.rute.harga'),
        ]);

        $productdetail = Productdetail::create([
            'product_id' => $product->id,
            'rute_id' => $detailrute->id,
            'id_detail_kendaraan' => $detailkendaraan->id,
            'batas_pembayaran' => $request->session()->get('product.batas.batas_pembayaran'),
            'kebijakan_pembatalan_sebelumnya' => $kebijakan_pembatalan_sebelumnya,
            'kebijakan_pembatalan_sesudah' => $kebijakan_pembatalan_sesudah,
            'kebijakan_pembatalan_potongan' => $kebijakan_pembatalan_potongan,
            'deskripsi' => $request->session()->get('product.add_listing.deskripsi'),
            'catatan' => $request->session()->get('product.add_listing.catatan'),
            'gallery' => json_encode($gallery),
        ]);

        $request->session()->forget('product');

        return redirect()->route('transfer.addListingUmum');
    }

    // Master Mobil

    public function viewMasterMobil(Request $request){ 
        $data = DB::table('merek_mobil')
        ->join('jenis_mobil', 'merek_mobil.jenis_id', '=', 'jenis_mobil.id')
        ->paginate(10);

        $user_id = Auth::user()->id;
        $bus = DB::table('merek_bus')
        ->join('jenis_bus', 'merek_bus.id_jenis', '=', 'jenis_bus.id_jenis')
        ->join('users', 'merek_bus.user_id', '=', 'users.id')
        ->where('merek_bus.user_id', $user_id)
        ->paginate(10);

        return view('BE.seller.transfer.master-mobil', compact('data', 'bus'));
    }

    public function viewAddJenisMobil(Request $request){
        $master_mobil = $request->session()->has('mobil.master_mobil') ? $request->session()->get('mobil.master_mobil') : '';
        return view('BE.seller.transfer.merek-mobil', compact('master_mobil'));
    }

    public function addJenisMobil(Request $request){
        $request->session()->put('mobil.master_mobil.jenis_kendaraan', $request->jenis_kendaraan);

        $jenismobil = JenisMobil::create([
            'jenis_kendaraan' => $request->session()->get('mobil.master_mobil.jenis_kendaraan')
        ]);

        return redirect()->route('masterMobil.viewAddMerekMobil');
    }

    public function viewAddMerekMobil(Request $request){
        $jenismobil = JenisMobil::all();
        $jenis_mobil = $request->session()->has('mobil.jenis_mobil') ? $request->session()->get('mobil.jenis_mobil') : '';
        return view('BE.seller.transfer.jenis-mobil', compact('jenismobil', 'jenis_mobil'));
    }

    public function addMerekMobil(Request $request){
        $request->session()->put('mobil.master_mobil.jenis_id', $request->jenismobil);
        $request->session()->put('mobil.master_mobil.merek_mobil', $request->merek_mobil);
        $request->session()->put('mobil.master_mobil.plat_mobil', $request->plat_mobil);

        $merekmobil = MerekMobil::create([
            'jenis_id' => $request->session()->get('mobil.master_mobil.jenis_id'),
            'merek_mobil' => $request->session()->get('mobil.master_mobil.merek_mobil'),
            'plat_mobil' => $request->session()->get('mobil.master_mobil.plat_mobil')
        ]);

        $request->session()->forget('mobil');

        return redirect()->route('masterMobil');
    }
    
    public function edit($id){
        $merekmobil = DB::table('merek_mobil')->where('id_merek', $id)->get();
        $jenismobil = JenisMobil::all();
        return view('BE.seller.transfer.edit-merek-mobil', ['merekmobil'=> $merekmobil], compact('jenismobil'));
    }

    public function update(Request $request){
        DB::table('merek_mobil')->where('id_merek', $request->id_merek)->update([
            'jenis_id' => $request->jenismobil,
            'merek_mobil' => $request->merek_mobil,
            'plat_mobil' => $request->plat_mobil
        ]);

        return redirect()->route('masterMobil');
    }

    public function hapus($id){
        DB::table('merek_mobil')->where('id_merek', $id)->delete();

        return redirect()->route('masterMobil');
    }

    // Master Bus

    public function viewAddJenisBus(Request $request){
        $master_bus = $request->session()->has('mobil.master_bus') ? $request->session()->get('mobil.master_bus') : '';
        return view('BE.seller.transfer.jenis-bus', compact('master_bus'));
    }

    public function addJenisBus(Request $request){
        $user_id = Auth::user()->id;
        $request->session()->put('mobil.master_bus.jenis_kendaraan', $request->jenis_kendaraan);

        $jenisbus = JenisBus::create([
            'user_id' => $user_id,
            'jenis_kendaraan' => $request->session()->get('mobil.master_bus.jenis_kendaraan')
        ]);

        return redirect()->route('masterMobil.viewAddMerekBus');
    }

    public function viewAddMerekBus(Request $request){
        $user_id = Auth::user()->id;
        $jenisbus = DB::table('jenis_bus')->where('user_id', $user_id)
                    ->join('users', 'jenis_bus.user_id', '=', 'users.id')
                    ->get();

        $jenis_bus = $request->session()->has('mobil.jenis_bus') ? $request->session()->get('mobil.jenis_bus') : '';
        return view('BE.seller.transfer.merek-bus', compact('jenisbus', 'jenis_bus'));
    }

    public function addMerekBus(Request $request){
        $user_id = Auth::user()->id;
        $request->session()->put('mobil.master_bus.id_jenis', $request->jenisbus);
        $request->session()->put('mobil.master_bus.merek_bus', $request->merek_bus);
        $request->session()->put('mobil.master_bus.plat_bus', $request->plat_bus);

        $merekbus = merekbus::create([
            'user_id' => $user_id,
            'id_jenis' => $request->session()->get('mobil.master_bus.id_jenis'),
            'merek_bus' => $request->session()->get('mobil.master_bus.merek_bus'),
            'plat_bus' => $request->session()->get('mobil.master_bus.plat_bus')
        ]);

        $request->session()->forget('mobil');

        return redirect()->route('masterMobil');
    }

    // Master Layout Bus

    public function viewMasterBus(Request $request){
        $data = DB::table('layout_kursi')
        ->join('users', 'layout_kursi.user_id', '=', 'users.id')
        ->paginate(10);

        return view('BE.seller.transfer.master-bus', ['data' => $data]);
    }

    public function viewAddLayoutBus(Request $request){
        $layout_bus = $request->session()->has('layout.layout_bus') ? $request->session()->get('layout.layout_bus') : '';
        return view('BE.seller.transfer.layout-bus', compact('layout_bus'));
    }

    public function addLayoutBus(Request $request){
        $user_id = Auth::user()->id;

        $request->session()->put('layout.layout_bus.jumlah_kursi', $request->jumlah_kursi);

        $request->session()->put('layout.layout_bus.layout_kursi', $request->layout_kursi);
        $request->session()->put('layout.layout_bus.lantai_bus', $request->lantai_bus);

        $request->session()->put('layout.layout_bus.kursi_1', $request->kursi_1);
        $request->session()->put('layout.layout_bus.kursi_2', $request->kursi_2);
        $request->session()->put('layout.layout_bus.kursi_3', $request->kursi_3);
        $request->session()->put('layout.layout_bus.kursi_4', $request->kursi_4);
        $request->session()->put('layout.layout_bus.kursi_5', $request->kursi_5);
        $request->session()->put('layout.layout_bus.kursi_6', $request->kursi_6);
        $request->session()->put('layout.layout_bus.kursi_7', $request->kursi_7);
        $request->session()->put('layout.layout_bus.kursi_8', $request->kursi_8);
        $request->session()->put('layout.layout_bus.kursi_9', $request->kursi_9);
        $request->session()->put('layout.layout_bus.kursi_10', $request->kursi_10);
        $request->session()->put('layout.layout_bus.kursi_11', $request->kursi_11);
        $request->session()->put('layout.layout_bus.kursi_12', $request->kursi_12);
        $request->session()->put('layout.layout_bus.kursi_13', $request->kursi_13);
        $request->session()->put('layout.layout_bus.kursi_14', $request->kursi_14);
        $request->session()->put('layout.layout_bus.kursi_15', $request->kursi_15);
        $request->session()->put('layout.layout_bus.kursi_16', $request->kursi_16);
        $request->session()->put('layout.layout_bus.kursi_17', $request->kursi_17);
        $request->session()->put('layout.layout_bus.kursi_18', $request->kursi_18);
        $request->session()->put('layout.layout_bus.kursi_19', $request->kursi_19);
        $request->session()->put('layout.layout_bus.kursi_20', $request->kursi_20);
        $request->session()->put('layout.layout_bus.kursi_21', $request->kursi_21);
        $request->session()->put('layout.layout_bus.kursi_22', $request->kursi_22);
        $request->session()->put('layout.layout_bus.kursi_23', $request->kursi_23);
        $request->session()->put('layout.layout_bus.kursi_24', $request->kursi_24);
        $request->session()->put('layout.layout_bus.kursi_25', $request->kursi_25);
        $request->session()->put('layout.layout_bus.kursi_26', $request->kursi_26);
        $request->session()->put('layout.layout_bus.kursi_27', $request->kursi_27);
        $request->session()->put('layout.layout_bus.kursi_28', $request->kursi_28);
        $request->session()->put('layout.layout_bus.kursi_29', $request->kursi_29);
        $request->session()->put('layout.layout_bus.kursi_30', $request->kursi_30);
        $request->session()->put('layout.layout_bus.kursi_31', $request->kursi_31);
        $request->session()->put('layout.layout_bus.kursi_32', $request->kursi_32);
        $request->session()->put('layout.layout_bus.kursi_33', $request->kursi_33);
        $request->session()->put('layout.layout_bus.kursi_34', $request->kursi_34);
        $request->session()->put('layout.layout_bus.kursi_35', $request->kursi_35);
        $request->session()->put('layout.layout_bus.kursi_36', $request->kursi_36);
        $request->session()->put('layout.layout_bus.kursi_37', $request->kursi_37);
        $request->session()->put('layout.layout_bus.kursi_38', $request->kursi_38);
        $request->session()->put('layout.layout_bus.kursi_39', $request->kursi_39);        
        $request->session()->put('layout.layout_bus.kursi_40', $request->kursi_40);
        $request->session()->put('layout.layout_bus.kursi_41', $request->kursi_41);
        $request->session()->put('layout.layout_bus.kursi_42', $request->kursi_42);
        $request->session()->put('layout.layout_bus.kursi_43', $request->kursi_43);
        $request->session()->put('layout.layout_bus.kursi_44', $request->kursi_44);
        $request->session()->put('layout.layout_bus.kursi_45', $request->kursi_45);
        $request->session()->put('layout.layout_bus.kursi_46', $request->kursi_46);
        $request->session()->put('layout.layout_bus.kursi_47', $request->kursi_47);
        $request->session()->put('layout.layout_bus.kursi_48', $request->kursi_48);
        $request->session()->put('layout.layout_bus.kursi_49', $request->kursi_49);        
        $request->session()->put('layout.layout_bus.kursi_50', $request->kursi_50);
        $request->session()->put('layout.layout_bus.kursi_51', $request->kursi_51);
        $request->session()->put('layout.layout_bus.kursi_52', $request->kursi_52);
        $request->session()->put('layout.layout_bus.kursi_53', $request->kursi_53);
        $request->session()->put('layout.layout_bus.kursi_54', $request->kursi_54);
        $request->session()->put('layout.layout_bus.kursi_55', $request->kursi_55);
        $request->session()->put('layout.layout_bus.kursi_56', $request->kursi_56);
        $request->session()->put('layout.layout_bus.kursi_57', $request->kursi_57);
        $request->session()->put('layout.layout_bus.kursi_58', $request->kursi_58);
        $request->session()->put('layout.layout_bus.kursi_59', $request->kursi_59);

        $request->session()->put('layout.layout_bus.harga_1', $request->harga_1);
        $request->session()->put('layout.layout_bus.harga_2', $request->harga_2);
        $request->session()->put('layout.layout_bus.harga_3', $request->harga_3);
        $request->session()->put('layout.layout_bus.harga_4', $request->harga_4);
        $request->session()->put('layout.layout_bus.harga_5', $request->harga_5);
        $request->session()->put('layout.layout_bus.harga_6', $request->harga_6);
        $request->session()->put('layout.layout_bus.harga_7', $request->harga_7);
        $request->session()->put('layout.layout_bus.harga_8', $request->harga_8);
        $request->session()->put('layout.layout_bus.harga_9', $request->harga_9);
        $request->session()->put('layout.layout_bus.harga_10', $request->harga_10);
        $request->session()->put('layout.layout_bus.harga_11', $request->harga_11);
        $request->session()->put('layout.layout_bus.harga_12', $request->harga_12);
        $request->session()->put('layout.layout_bus.harga_13', $request->harga_13);
        $request->session()->put('layout.layout_bus.harga_14', $request->harga_14);
        $request->session()->put('layout.layout_bus.harga_15', $request->harga_15);
        $request->session()->put('layout.layout_bus.harga_16', $request->harga_16);
        $request->session()->put('layout.layout_bus.harga_17', $request->harga_17);
        $request->session()->put('layout.layout_bus.harga_18', $request->harga_18);
        $request->session()->put('layout.layout_bus.harga_19', $request->harga_19);
        $request->session()->put('layout.layout_bus.harga_20', $request->harga_20);
        $request->session()->put('layout.layout_bus.harga_21', $request->harga_21);
        $request->session()->put('layout.layout_bus.harga_22', $request->harga_22);
        $request->session()->put('layout.layout_bus.harga_23', $request->harga_23);
        $request->session()->put('layout.layout_bus.harga_24', $request->harga_24);
        $request->session()->put('layout.layout_bus.harga_25', $request->harga_25);
        $request->session()->put('layout.layout_bus.harga_26', $request->harga_26);
        $request->session()->put('layout.layout_bus.harga_27', $request->harga_27);
        $request->session()->put('layout.layout_bus.harga_28', $request->harga_28);
        $request->session()->put('layout.layout_bus.harga_29', $request->harga_29);
        $request->session()->put('layout.layout_bus.harga_30', $request->harga_30);
        $request->session()->put('layout.layout_bus.harga_31', $request->harga_31);
        $request->session()->put('layout.layout_bus.harga_32', $request->harga_32);
        $request->session()->put('layout.layout_bus.harga_33', $request->harga_33);
        $request->session()->put('layout.layout_bus.harga_34', $request->harga_34);
        $request->session()->put('layout.layout_bus.harga_35', $request->harga_35);
        $request->session()->put('layout.layout_bus.harga_36', $request->harga_36);
        $request->session()->put('layout.layout_bus.harga_37', $request->harga_37);
        $request->session()->put('layout.layout_bus.harga_38', $request->harga_38);
        $request->session()->put('layout.layout_bus.harga_39', $request->harga_39);        
        $request->session()->put('layout.layout_bus.harga_40', $request->harga_40);
        $request->session()->put('layout.layout_bus.harga_41', $request->harga_41);
        $request->session()->put('layout.layout_bus.harga_42', $request->harga_42);
        $request->session()->put('layout.layout_bus.harga_43', $request->harga_43);
        $request->session()->put('layout.layout_bus.harga_44', $request->harga_44);
        $request->session()->put('layout.layout_bus.harga_45', $request->harga_45);
        $request->session()->put('layout.layout_bus.harga_46', $request->harga_46);
        $request->session()->put('layout.layout_bus.harga_47', $request->harga_47);
        $request->session()->put('layout.layout_bus.harga_48', $request->harga_48);
        $request->session()->put('layout.layout_bus.harga_49', $request->harga_49);        
        $request->session()->put('layout.layout_bus.harga_50', $request->harga_50);
        $request->session()->put('layout.layout_bus.harga_51', $request->harga_51);
        $request->session()->put('layout.layout_bus.harga_52', $request->harga_52);
        $request->session()->put('layout.layout_bus.harga_53', $request->harga_53);
        $request->session()->put('layout.layout_bus.harga_54', $request->harga_54);
        $request->session()->put('layout.layout_bus.harga_55', $request->harga_55);
        $request->session()->put('layout.layout_bus.harga_56', $request->harga_56);
        $request->session()->put('layout.layout_bus.harga_57', $request->harga_57);
        $request->session()->put('layout.layout_bus.harga_58', $request->harga_58);
        $request->session()->put('layout.layout_bus.harga_59', $request->harga_59);

        $layoutbus = LayoutBus::create([
            'user_id' => $user_id,
            'jumlah_kursi' => $request->session()->get('layout.layout_bus.jumlah_kursi'),
            'layout_kursi' => $request->session()->get('layout.layout_bus.layout_kursi'),
            'lantai_bus' => $request->session()->get('layout.layout_bus.lantai_bus'),
            'kursi_1' => $request->session()->get('layout.layout_bus.kursi_1'),
            'kursi_2' => $request->session()->get('layout.layout_bus.kursi_2'),
            'kursi_3' => $request->session()->get('layout.layout_bus.kursi_3'),
            'kursi_4' => $request->session()->get('layout.layout_bus.kursi_4'),
            'kursi_5' => $request->session()->get('layout.layout_bus.kursi_5'),
            'kursi_6' => $request->session()->get('layout.layout_bus.kursi_6'),
            'kursi_7' => $request->session()->get('layout.layout_bus.kursi_7'),
            'kursi_8' => $request->session()->get('layout.layout_bus.kursi_8'),
            'kursi_9' => $request->session()->get('layout.layout_bus.kursi_9'),
            'kursi_10' => $request->session()->get('layout.layout_bus.kursi_10'),
            'kursi_11' => $request->session()->get('layout.layout_bus.kursi_11'),
            'kursi_12' => $request->session()->get('layout.layout_bus.kursi_12'),
            'kursi_13' => $request->session()->get('layout.layout_bus.kursi_13'),
            'kursi_14' => $request->session()->get('layout.layout_bus.kursi_14'),
            'kursi_15' => $request->session()->get('layout.layout_bus.kursi_15'),
            'kursi_16' => $request->session()->get('layout.layout_bus.kursi_16'),
            'kursi_17' => $request->session()->get('layout.layout_bus.kursi_17'),
            'kursi_18' => $request->session()->get('layout.layout_bus.kursi_18'),
            'kursi_19' => $request->session()->get('layout.layout_bus.kursi_19'),
            'kursi_20' => $request->session()->get('layout.layout_bus.kursi_20'),
            'kursi_21' => $request->session()->get('layout.layout_bus.kursi_21'),
            'kursi_22' => $request->session()->get('layout.layout_bus.kursi_22'),
            'kursi_23' => $request->session()->get('layout.layout_bus.kursi_23'),
            'kursi_24' => $request->session()->get('layout.layout_bus.kursi_24'),
            'kursi_25' => $request->session()->get('layout.layout_bus.kursi_25'),
            'kursi_26' => $request->session()->get('layout.layout_bus.kursi_26'),
            'kursi_27' => $request->session()->get('layout.layout_bus.kursi_27'),
            'kursi_28' => $request->session()->get('layout.layout_bus.kursi_28'),
            'kursi_29' => $request->session()->get('layout.layout_bus.kursi_29'),
            'kursi_30' => $request->session()->get('layout.layout_bus.kursi_30'),
            'kursi_31' => $request->session()->get('layout.layout_bus.kursi_31'),
            'kursi_32' => $request->session()->get('layout.layout_bus.kursi_32'),
            'kursi_33' => $request->session()->get('layout.layout_bus.kursi_33'),
            'kursi_34' => $request->session()->get('layout.layout_bus.kursi_34'),
            'kursi_35' => $request->session()->get('layout.layout_bus.kursi_35'),
            'kursi_36' => $request->session()->get('layout.layout_bus.kursi_36'),
            'kursi_37' => $request->session()->get('layout.layout_bus.kursi_37'),
            'kursi_38' => $request->session()->get('layout.layout_bus.kursi_38'),
            'kursi_39' => $request->session()->get('layout.layout_bus.kursi_39'),
            'kursi_40' => $request->session()->get('layout.layout_bus.kursi_40'),
            'kursi_41' => $request->session()->get('layout.layout_bus.kursi_41'),
            'kursi_42' => $request->session()->get('layout.layout_bus.kursi_42'),
            'kursi_43' => $request->session()->get('layout.layout_bus.kursi_43'),
            'kursi_44' => $request->session()->get('layout.layout_bus.kursi_44'),
            'kursi_45' => $request->session()->get('layout.layout_bus.kursi_45'),
            'kursi_46' => $request->session()->get('layout.layout_bus.kursi_46'),
            'kursi_47' => $request->session()->get('layout.layout_bus.kursi_47'),
            'kursi_48' => $request->session()->get('layout.layout_bus.kursi_48'),
            'kursi_49' => $request->session()->get('layout.layout_bus.kursi_49'),
            'kursi_50' => $request->session()->get('layout.layout_bus.kursi_50'),
            'kursi_51' => $request->session()->get('layout.layout_bus.kursi_51'),
            'kursi_52' => $request->session()->get('layout.layout_bus.kursi_52'),
            'kursi_53' => $request->session()->get('layout.layout_bus.kursi_53'),
            'kursi_54' => $request->session()->get('layout.layout_bus.kursi_54'),
            'kursi_55' => $request->session()->get('layout.layout_bus.kursi_55'),
            'kursi_56' => $request->session()->get('layout.layout_bus.kursi_56'),
            'kursi_57' => $request->session()->get('layout.layout_bus.kursi_57'),
            'kursi_58' => $request->session()->get('layout.layout_bus.kursi_58'),
            'kursi_59' => $request->session()->get('layout.layout_bus.kursi_59'),
            
            'harga_1' => $request->session()->get('layout.layout_bus.harga_1'),
            'harga_2' => $request->session()->get('layout.layout_bus.harga_2'),
            'harga_3' => $request->session()->get('layout.layout_bus.harga_3'),
            'harga_4' => $request->session()->get('layout.layout_bus.harga_4'),
            'harga_5' => $request->session()->get('layout.layout_bus.harga_5'),
            'harga_6' => $request->session()->get('layout.layout_bus.harga_6'),
            'harga_7' => $request->session()->get('layout.layout_bus.harga_7'),
            'harga_8' => $request->session()->get('layout.layout_bus.harga_8'),
            'harga_9' => $request->session()->get('layout.layout_bus.harga_9'),
            'harga_10' => $request->session()->get('layout.layout_bus.harga_10'),
            'harga_11' => $request->session()->get('layout.layout_bus.harga_11'),
            'harga_12' => $request->session()->get('layout.layout_bus.harga_12'),
            'harga_13' => $request->session()->get('layout.layout_bus.harga_13'),
            'harga_14' => $request->session()->get('layout.layout_bus.harga_14'),
            'harga_15' => $request->session()->get('layout.layout_bus.harga_15'),
            'harga_16' => $request->session()->get('layout.layout_bus.harga_16'),
            'harga_17' => $request->session()->get('layout.layout_bus.harga_17'),
            'harga_18' => $request->session()->get('layout.layout_bus.harga_18'),
            'harga_19' => $request->session()->get('layout.layout_bus.harga_19'),
            'harga_20' => $request->session()->get('layout.layout_bus.harga_20'),
            'harga_21' => $request->session()->get('layout.layout_bus.harga_21'),
            'harga_22' => $request->session()->get('layout.layout_bus.harga_22'),
            'harga_23' => $request->session()->get('layout.layout_bus.harga_23'),
            'harga_24' => $request->session()->get('layout.layout_bus.harga_24'),
            'harga_25' => $request->session()->get('layout.layout_bus.harga_25'),
            'harga_26' => $request->session()->get('layout.layout_bus.harga_26'),
            'harga_27' => $request->session()->get('layout.layout_bus.harga_27'),
            'harga_28' => $request->session()->get('layout.layout_bus.harga_28'),
            'harga_29' => $request->session()->get('layout.layout_bus.harga_29'),
            'harga_30' => $request->session()->get('layout.layout_bus.harga_30'),
            'harga_31' => $request->session()->get('layout.layout_bus.harga_31'),
            'harga_32' => $request->session()->get('layout.layout_bus.harga_32'),
            'harga_33' => $request->session()->get('layout.layout_bus.harga_33'),
            'harga_34' => $request->session()->get('layout.layout_bus.harga_34'),
            'harga_35' => $request->session()->get('layout.layout_bus.harga_35'),
            'harga_36' => $request->session()->get('layout.layout_bus.harga_36'),
            'harga_37' => $request->session()->get('layout.layout_bus.harga_37'),
            'harga_38' => $request->session()->get('layout.layout_bus.harga_38'),
            'harga_39' => $request->session()->get('layout.layout_bus.harga_39'),            
            'harga_40' => $request->session()->get('layout.layout_bus.harga_40'),
            'harga_41' => $request->session()->get('layout.layout_bus.harga_41'),
            'harga_42' => $request->session()->get('layout.layout_bus.harga_42'),
            'harga_43' => $request->session()->get('layout.layout_bus.harga_43'),
            'harga_44' => $request->session()->get('layout.layout_bus.harga_44'),
            'harga_45' => $request->session()->get('layout.layout_bus.harga_45'),
            'harga_46' => $request->session()->get('layout.layout_bus.harga_46'),
            'harga_47' => $request->session()->get('layout.layout_bus.harga_47'),
            'harga_48' => $request->session()->get('layout.layout_bus.harga_48'),
            'harga_49' => $request->session()->get('layout.layout_bus.harga_49'),            
            'harga_50' => $request->session()->get('layout.layout_bus.harga_50'),
            'harga_51' => $request->session()->get('layout.layout_bus.harga_51'),
            'harga_52' => $request->session()->get('layout.layout_bus.harga_52'),
            'harga_53' => $request->session()->get('layout.layout_bus.harga_53'),
            'harga_54' => $request->session()->get('layout.layout_bus.harga_54'),
            'harga_55' => $request->session()->get('layout.layout_bus.harga_55'),
            'harga_56' => $request->session()->get('layout.layout_bus.harga_56'),
            'harga_57' => $request->session()->get('layout.layout_bus.harga_57'),
            'harga_58' => $request->session()->get('layout.layout_bus.harga_58'),
            'harga_59' => $request->session()->get('layout.layout_bus.harga_59')
        ]);

        $request->session()->forget('layout');

        return redirect()->route('masterBus');
    }

    public function addLayoutBusM39(Request $request){
        $user_id = Auth::user()->id;

        $request->session()->put('layout.layout_bus.jumlah_kursi', $request->jumlah_kursi);

        $request->session()->put('layout.layout_bus.layout_kursi', $request->layout_kursi);
        $request->session()->put('layout.layout_bus.lantai_bus', $request->lantai_bus);

        $request->session()->put('layout.layout_bus.kursi_1', $request->kursi_1);
        $request->session()->put('layout.layout_bus.kursi_2', $request->kursi_2);
        $request->session()->put('layout.layout_bus.kursi_3', $request->kursi_3);
        $request->session()->put('layout.layout_bus.kursi_4', $request->kursi_4);
        $request->session()->put('layout.layout_bus.kursi_5', $request->kursi_5);
        $request->session()->put('layout.layout_bus.kursi_6', $request->kursi_6);
        $request->session()->put('layout.layout_bus.kursi_7', $request->kursi_7);
        $request->session()->put('layout.layout_bus.kursi_8', $request->kursi_8);
        $request->session()->put('layout.layout_bus.kursi_9', $request->kursi_9);
        $request->session()->put('layout.layout_bus.kursi_10', $request->kursi_10);
        $request->session()->put('layout.layout_bus.kursi_11', $request->kursi_11);
        $request->session()->put('layout.layout_bus.kursi_12', $request->kursi_12);
        $request->session()->put('layout.layout_bus.kursi_13', $request->kursi_13);
        $request->session()->put('layout.layout_bus.kursi_14', $request->kursi_14);
        $request->session()->put('layout.layout_bus.kursi_15', $request->kursi_15);
        $request->session()->put('layout.layout_bus.kursi_16', $request->kursi_16);
        $request->session()->put('layout.layout_bus.kursi_17', $request->kursi_17);
        $request->session()->put('layout.layout_bus.kursi_18', $request->kursi_18);
        $request->session()->put('layout.layout_bus.kursi_19', $request->kursi_19);
        $request->session()->put('layout.layout_bus.kursi_20', $request->kursi_20);
        $request->session()->put('layout.layout_bus.kursi_21', $request->kursi_21);
        $request->session()->put('layout.layout_bus.kursi_22', $request->kursi_22);
        $request->session()->put('layout.layout_bus.kursi_23', $request->kursi_23);
        $request->session()->put('layout.layout_bus.kursi_24', $request->kursi_24);
        $request->session()->put('layout.layout_bus.kursi_25', $request->kursi_25);
        $request->session()->put('layout.layout_bus.kursi_26', $request->kursi_26);
        $request->session()->put('layout.layout_bus.kursi_27', $request->kursi_27);
        $request->session()->put('layout.layout_bus.kursi_28', $request->kursi_28);
        $request->session()->put('layout.layout_bus.kursi_29', $request->kursi_29);
        $request->session()->put('layout.layout_bus.kursi_30', $request->kursi_30);
        $request->session()->put('layout.layout_bus.kursi_31', $request->kursi_31);
        $request->session()->put('layout.layout_bus.kursi_32', $request->kursi_32);
        $request->session()->put('layout.layout_bus.kursi_33', $request->kursi_33);
        $request->session()->put('layout.layout_bus.kursi_34', $request->kursi_34);
        $request->session()->put('layout.layout_bus.kursi_35', $request->kursi_35);
        $request->session()->put('layout.layout_bus.kursi_36', $request->kursi_36);
        $request->session()->put('layout.layout_bus.kursi_37', $request->kursi_37);
        $request->session()->put('layout.layout_bus.kursi_38', $request->kursi_38);
        $request->session()->put('layout.layout_bus.kursi_39', $request->kursi_39);        
        $request->session()->put('layout.layout_bus.kursi_40', $request->kursi_40);
        $request->session()->put('layout.layout_bus.kursi_41', $request->kursi_41);
        $request->session()->put('layout.layout_bus.kursi_42', $request->kursi_42);
        $request->session()->put('layout.layout_bus.kursi_43', $request->kursi_43);
        $request->session()->put('layout.layout_bus.kursi_44', $request->kursi_44);
        $request->session()->put('layout.layout_bus.kursi_45', $request->kursi_45);
        $request->session()->put('layout.layout_bus.kursi_46', $request->kursi_46);
        $request->session()->put('layout.layout_bus.kursi_47', $request->kursi_47);
        $request->session()->put('layout.layout_bus.kursi_48', $request->kursi_48);
        $request->session()->put('layout.layout_bus.kursi_49', $request->kursi_49);        
        $request->session()->put('layout.layout_bus.kursi_50', $request->kursi_50);
        $request->session()->put('layout.layout_bus.kursi_51', $request->kursi_51);
        $request->session()->put('layout.layout_bus.kursi_52', $request->kursi_52);
        $request->session()->put('layout.layout_bus.kursi_53', $request->kursi_53);
        $request->session()->put('layout.layout_bus.kursi_54', $request->kursi_54);
        $request->session()->put('layout.layout_bus.kursi_55', $request->kursi_55);
        $request->session()->put('layout.layout_bus.kursi_56', $request->kursi_56);
        $request->session()->put('layout.layout_bus.kursi_57', $request->kursi_57);
        $request->session()->put('layout.layout_bus.kursi_58', $request->kursi_58);
        $request->session()->put('layout.layout_bus.kursi_59', $request->kursi_59);

        $request->session()->put('layout.layout_bus.harga_1', $request->harga_1);
        $request->session()->put('layout.layout_bus.harga_2', $request->harga_2);
        $request->session()->put('layout.layout_bus.harga_3', $request->harga_3);
        $request->session()->put('layout.layout_bus.harga_4', $request->harga_4);
        $request->session()->put('layout.layout_bus.harga_5', $request->harga_5);
        $request->session()->put('layout.layout_bus.harga_6', $request->harga_6);
        $request->session()->put('layout.layout_bus.harga_7', $request->harga_7);
        $request->session()->put('layout.layout_bus.harga_8', $request->harga_8);
        $request->session()->put('layout.layout_bus.harga_9', $request->harga_9);
        $request->session()->put('layout.layout_bus.harga_10', $request->harga_10);
        $request->session()->put('layout.layout_bus.harga_11', $request->harga_11);
        $request->session()->put('layout.layout_bus.harga_12', $request->harga_12);
        $request->session()->put('layout.layout_bus.harga_13', $request->harga_13);
        $request->session()->put('layout.layout_bus.harga_14', $request->harga_14);
        $request->session()->put('layout.layout_bus.harga_15', $request->harga_15);
        $request->session()->put('layout.layout_bus.harga_16', $request->harga_16);
        $request->session()->put('layout.layout_bus.harga_17', $request->harga_17);
        $request->session()->put('layout.layout_bus.harga_18', $request->harga_18);
        $request->session()->put('layout.layout_bus.harga_19', $request->harga_19);
        $request->session()->put('layout.layout_bus.harga_20', $request->harga_20);
        $request->session()->put('layout.layout_bus.harga_21', $request->harga_21);
        $request->session()->put('layout.layout_bus.harga_22', $request->harga_22);
        $request->session()->put('layout.layout_bus.harga_23', $request->harga_23);
        $request->session()->put('layout.layout_bus.harga_24', $request->harga_24);
        $request->session()->put('layout.layout_bus.harga_25', $request->harga_25);
        $request->session()->put('layout.layout_bus.harga_26', $request->harga_26);
        $request->session()->put('layout.layout_bus.harga_27', $request->harga_27);
        $request->session()->put('layout.layout_bus.harga_28', $request->harga_28);
        $request->session()->put('layout.layout_bus.harga_29', $request->harga_29);
        $request->session()->put('layout.layout_bus.harga_30', $request->harga_30);
        $request->session()->put('layout.layout_bus.harga_31', $request->harga_31);
        $request->session()->put('layout.layout_bus.harga_32', $request->harga_32);
        $request->session()->put('layout.layout_bus.harga_33', $request->harga_33);
        $request->session()->put('layout.layout_bus.harga_34', $request->harga_34);
        $request->session()->put('layout.layout_bus.harga_35', $request->harga_35);
        $request->session()->put('layout.layout_bus.harga_36', $request->harga_36);
        $request->session()->put('layout.layout_bus.harga_37', $request->harga_37);
        $request->session()->put('layout.layout_bus.harga_38', $request->harga_38);
        $request->session()->put('layout.layout_bus.harga_39', $request->harga_39);        
        $request->session()->put('layout.layout_bus.harga_40', $request->harga_40);
        $request->session()->put('layout.layout_bus.harga_41', $request->harga_41);
        $request->session()->put('layout.layout_bus.harga_42', $request->harga_42);
        $request->session()->put('layout.layout_bus.harga_43', $request->harga_43);
        $request->session()->put('layout.layout_bus.harga_44', $request->harga_44);
        $request->session()->put('layout.layout_bus.harga_45', $request->harga_45);
        $request->session()->put('layout.layout_bus.harga_46', $request->harga_46);
        $request->session()->put('layout.layout_bus.harga_47', $request->harga_47);
        $request->session()->put('layout.layout_bus.harga_48', $request->harga_48);
        $request->session()->put('layout.layout_bus.harga_49', $request->harga_49);        
        $request->session()->put('layout.layout_bus.harga_50', $request->harga_50);
        $request->session()->put('layout.layout_bus.harga_51', $request->harga_51);
        $request->session()->put('layout.layout_bus.harga_52', $request->harga_52);
        $request->session()->put('layout.layout_bus.harga_53', $request->harga_53);
        $request->session()->put('layout.layout_bus.harga_54', $request->harga_54);
        $request->session()->put('layout.layout_bus.harga_55', $request->harga_55);
        $request->session()->put('layout.layout_bus.harga_56', $request->harga_56);
        $request->session()->put('layout.layout_bus.harga_57', $request->harga_57);
        $request->session()->put('layout.layout_bus.harga_58', $request->harga_58);
        $request->session()->put('layout.layout_bus.harga_59', $request->harga_59);

        $layoutbus = LayoutBus::create([
            'user_id' => $user_id,
            'jumlah_kursi' => $request->session()->get('layout.layout_bus.jumlah_kursi'),
            'layout_kursi' => $request->session()->get('layout.layout_bus.layout_kursi'),
            'lantai_bus' => $request->session()->get('layout.layout_bus.lantai_bus'),
            'kursi_1' => $request->session()->get('layout.layout_bus.kursi_1'),
            'kursi_2' => $request->session()->get('layout.layout_bus.kursi_2'),
            'kursi_3' => $request->session()->get('layout.layout_bus.kursi_3'),
            'kursi_4' => $request->session()->get('layout.layout_bus.kursi_4'),
            'kursi_5' => $request->session()->get('layout.layout_bus.kursi_5'),
            'kursi_6' => $request->session()->get('layout.layout_bus.kursi_6'),
            'kursi_7' => $request->session()->get('layout.layout_bus.kursi_7'),
            'kursi_8' => $request->session()->get('layout.layout_bus.kursi_8'),
            'kursi_9' => $request->session()->get('layout.layout_bus.kursi_9'),
            'kursi_10' => $request->session()->get('layout.layout_bus.kursi_10'),
            'kursi_11' => $request->session()->get('layout.layout_bus.kursi_11'),
            'kursi_12' => $request->session()->get('layout.layout_bus.kursi_12'),
            'kursi_13' => $request->session()->get('layout.layout_bus.kursi_13'),
            'kursi_14' => $request->session()->get('layout.layout_bus.kursi_14'),
            'kursi_15' => $request->session()->get('layout.layout_bus.kursi_15'),
            'kursi_16' => $request->session()->get('layout.layout_bus.kursi_16'),
            'kursi_17' => $request->session()->get('layout.layout_bus.kursi_17'),
            'kursi_18' => $request->session()->get('layout.layout_bus.kursi_18'),
            'kursi_19' => $request->session()->get('layout.layout_bus.kursi_19'),
            'kursi_20' => $request->session()->get('layout.layout_bus.kursi_20'),
            'kursi_21' => $request->session()->get('layout.layout_bus.kursi_21'),
            'kursi_22' => $request->session()->get('layout.layout_bus.kursi_22'),
            'kursi_23' => $request->session()->get('layout.layout_bus.kursi_23'),
            'kursi_24' => $request->session()->get('layout.layout_bus.kursi_24'),
            'kursi_25' => $request->session()->get('layout.layout_bus.kursi_25'),
            'kursi_26' => $request->session()->get('layout.layout_bus.kursi_26'),
            'kursi_27' => $request->session()->get('layout.layout_bus.kursi_27'),
            'kursi_28' => $request->session()->get('layout.layout_bus.kursi_28'),
            'kursi_29' => $request->session()->get('layout.layout_bus.kursi_29'),
            'kursi_30' => $request->session()->get('layout.layout_bus.kursi_30'),
            'kursi_31' => $request->session()->get('layout.layout_bus.kursi_31'),
            'kursi_32' => $request->session()->get('layout.layout_bus.kursi_32'),
            'kursi_33' => $request->session()->get('layout.layout_bus.kursi_33'),
            'kursi_34' => $request->session()->get('layout.layout_bus.kursi_34'),
            'kursi_35' => $request->session()->get('layout.layout_bus.kursi_35'),
            'kursi_36' => $request->session()->get('layout.layout_bus.kursi_36'),
            'kursi_37' => $request->session()->get('layout.layout_bus.kursi_37'),
            'kursi_38' => $request->session()->get('layout.layout_bus.kursi_38'),
            'kursi_39' => $request->session()->get('layout.layout_bus.kursi_39'),
            'kursi_40' => $request->session()->get('layout.layout_bus.kursi_40'),
            'kursi_41' => $request->session()->get('layout.layout_bus.kursi_41'),
            'kursi_42' => $request->session()->get('layout.layout_bus.kursi_42'),
            'kursi_43' => $request->session()->get('layout.layout_bus.kursi_43'),
            'kursi_44' => $request->session()->get('layout.layout_bus.kursi_44'),
            'kursi_45' => $request->session()->get('layout.layout_bus.kursi_45'),
            'kursi_46' => $request->session()->get('layout.layout_bus.kursi_46'),
            'kursi_47' => $request->session()->get('layout.layout_bus.kursi_47'),
            'kursi_48' => $request->session()->get('layout.layout_bus.kursi_48'),
            'kursi_49' => $request->session()->get('layout.layout_bus.kursi_49'),
            'kursi_50' => $request->session()->get('layout.layout_bus.kursi_50'),
            'kursi_51' => $request->session()->get('layout.layout_bus.kursi_51'),
            'kursi_52' => $request->session()->get('layout.layout_bus.kursi_52'),
            'kursi_53' => $request->session()->get('layout.layout_bus.kursi_53'),
            'kursi_54' => $request->session()->get('layout.layout_bus.kursi_54'),
            'kursi_55' => $request->session()->get('layout.layout_bus.kursi_55'),
            'kursi_56' => $request->session()->get('layout.layout_bus.kursi_56'),
            'kursi_57' => $request->session()->get('layout.layout_bus.kursi_57'),
            'kursi_58' => $request->session()->get('layout.layout_bus.kursi_58'),
            'kursi_59' => $request->session()->get('layout.layout_bus.kursi_59'),
            
            'harga_1' => $request->session()->get('layout.layout_bus.harga_1'),
            'harga_2' => $request->session()->get('layout.layout_bus.harga_2'),
            'harga_3' => $request->session()->get('layout.layout_bus.harga_3'),
            'harga_4' => $request->session()->get('layout.layout_bus.harga_4'),
            'harga_5' => $request->session()->get('layout.layout_bus.harga_5'),
            'harga_6' => $request->session()->get('layout.layout_bus.harga_6'),
            'harga_7' => $request->session()->get('layout.layout_bus.harga_7'),
            'harga_8' => $request->session()->get('layout.layout_bus.harga_8'),
            'harga_9' => $request->session()->get('layout.layout_bus.harga_9'),
            'harga_10' => $request->session()->get('layout.layout_bus.harga_10'),
            'harga_11' => $request->session()->get('layout.layout_bus.harga_11'),
            'harga_12' => $request->session()->get('layout.layout_bus.harga_12'),
            'harga_13' => $request->session()->get('layout.layout_bus.harga_13'),
            'harga_14' => $request->session()->get('layout.layout_bus.harga_14'),
            'harga_15' => $request->session()->get('layout.layout_bus.harga_15'),
            'harga_16' => $request->session()->get('layout.layout_bus.harga_16'),
            'harga_17' => $request->session()->get('layout.layout_bus.harga_17'),
            'harga_18' => $request->session()->get('layout.layout_bus.harga_18'),
            'harga_19' => $request->session()->get('layout.layout_bus.harga_19'),
            'harga_20' => $request->session()->get('layout.layout_bus.harga_20'),
            'harga_21' => $request->session()->get('layout.layout_bus.harga_21'),
            'harga_22' => $request->session()->get('layout.layout_bus.harga_22'),
            'harga_23' => $request->session()->get('layout.layout_bus.harga_23'),
            'harga_24' => $request->session()->get('layout.layout_bus.harga_24'),
            'harga_25' => $request->session()->get('layout.layout_bus.harga_25'),
            'harga_26' => $request->session()->get('layout.layout_bus.harga_26'),
            'harga_27' => $request->session()->get('layout.layout_bus.harga_27'),
            'harga_28' => $request->session()->get('layout.layout_bus.harga_28'),
            'harga_29' => $request->session()->get('layout.layout_bus.harga_29'),
            'harga_30' => $request->session()->get('layout.layout_bus.harga_30'),
            'harga_31' => $request->session()->get('layout.layout_bus.harga_31'),
            'harga_32' => $request->session()->get('layout.layout_bus.harga_32'),
            'harga_33' => $request->session()->get('layout.layout_bus.harga_33'),
            'harga_34' => $request->session()->get('layout.layout_bus.harga_34'),
            'harga_35' => $request->session()->get('layout.layout_bus.harga_35'),
            'harga_36' => $request->session()->get('layout.layout_bus.harga_36'),
            'harga_37' => $request->session()->get('layout.layout_bus.harga_37'),
            'harga_38' => $request->session()->get('layout.layout_bus.harga_38'),
            'harga_39' => $request->session()->get('layout.layout_bus.harga_39'),            
            'harga_40' => $request->session()->get('layout.layout_bus.harga_40'),
            'harga_41' => $request->session()->get('layout.layout_bus.harga_41'),
            'harga_42' => $request->session()->get('layout.layout_bus.harga_42'),
            'harga_43' => $request->session()->get('layout.layout_bus.harga_43'),
            'harga_44' => $request->session()->get('layout.layout_bus.harga_44'),
            'harga_45' => $request->session()->get('layout.layout_bus.harga_45'),
            'harga_46' => $request->session()->get('layout.layout_bus.harga_46'),
            'harga_47' => $request->session()->get('layout.layout_bus.harga_47'),
            'harga_48' => $request->session()->get('layout.layout_bus.harga_48'),
            'harga_49' => $request->session()->get('layout.layout_bus.harga_49'),            
            'harga_50' => $request->session()->get('layout.layout_bus.harga_50'),
            'harga_51' => $request->session()->get('layout.layout_bus.harga_51'),
            'harga_52' => $request->session()->get('layout.layout_bus.harga_52'),
            'harga_53' => $request->session()->get('layout.layout_bus.harga_53'),
            'harga_54' => $request->session()->get('layout.layout_bus.harga_54'),
            'harga_55' => $request->session()->get('layout.layout_bus.harga_55'),
            'harga_56' => $request->session()->get('layout.layout_bus.harga_56'),
            'harga_57' => $request->session()->get('layout.layout_bus.harga_57'),
            'harga_58' => $request->session()->get('layout.layout_bus.harga_58'),
            'harga_59' => $request->session()->get('layout.layout_bus.harga_59')
        ]);

        $request->session()->forget('layout');

        return redirect()->route('masterBus');
    }
    
    public function editBus($id){
        $data = DB::table('layout_kursi')->where('id_layout', $id)->get();
        $layout = LayoutBus::all();
        return view('BE.seller.transfer.edit-layout-bus', ['data' => $data], compact('layout'));
    }

    public function updateBus(Request $request){
        DB::table('layout_kursi')->where('id_layout', $request->id_layout)->update([
            'kursi_1' => $request->kursi_1,
            'harga_1' => $request->harga_1
        ]);

        return redirect()->route('masterBus');
    }

    // View Rute Kamtuu

    public function viewRuteKamtuu(){
        $kecamatan = District::all();
        $data = Masterroute::all();
        $from = District::first();
        $to = District::first();

        return view('BE.seller.transfer.rute-by-kamtuu', compact('kecamatan', 'from', 'to', 'data'));
    }
}
