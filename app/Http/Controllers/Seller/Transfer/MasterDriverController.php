<?php

namespace App\Http\Controllers\Seller\Transfer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\MasterDriver;
use App\Models\user;
use App\Models\Hotel;

class MasterDriverController extends Controller
{
    // Master Driver
    public function viewMasterDriver(Request $request){
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;
        $data = DB::table('master_driver')
        ->join('users', 'master_driver.id_seller', '=', 'users.id')
        ->where('master_driver.id_seller', $user_id)
        ->paginate(10);

        return view('BE.seller.transfer.Master.Driver.masterDriver', compact('data', 'hotel', 'xstay'));
    }

    public function viewAddDriver(Request $request){

        $add_driver = '';
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        if ($request->session()->has('driver.add_driver')) {
            $add_driver = $request->session()->get('driver.add_driver');
        } else {
            $request->session()->put('driver.add_driver', '');
            $add_driver = '';
        }
        $master_driver = $request->session()->has('driver.add_driver') ? $request->session()->get('driver.add_driver') : '';

        return view('BE.seller.transfer.Master.Driver.addDriver', compact('add_driver', 'master_driver', 'hotel', 'xstay'));
    }

    public function addDriver(Request $request){
        $user_id = Auth::user()->id;

        $request->session()->put('driver.add_driver.nama_driver', $request->nama_driver);
        $request->session()->put('driver.add_driver.no_telp', $request->no_telp);
        $request->session()->put('driver.add_driver.alamat', $request->alamat);

        $driver = MasterDriver::create([
            'id_seller' => $user_id,
            'nama_driver' => $request->session()->get('driver.add_driver.nama_driver'),
            'no_telp' => $request->session()->get('driver.add_driver.no_telp'),
            'alamat' => $request->session()->get('driver.add_driver.alamat')
        ]);

        return redirect()->route('masterDriver');
    }

    // Edit Driver
    public function editDriver(Request $request, $id)
    {
        $user_id = Auth::user()->id;
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $add_driver = user::where([
            'id_driver' => $id])
            ->join('master_driver', 'users.id', '=', 'master_driver.id_seller')
            ->has('masterdriver')->with('masterdriver')->first();

        $isedit = 'true';

        $driver['nama_driver'] = $add_driver->nama_driver;
        $driver['no_telp'] = $add_driver->no_telp;
        $driver['alamat'] = $add_driver->alamat;
        
        return view('BE.seller.transfer.Master.Driver.addDriver', compact('isedit', 'add_driver', 'driver', 'id', 'hotel', 'xstay'));
    }

    public function updateDriver(Request $request, $id)
    {
        $add_driver = user::where([
            'id_driver' => $id])
            ->join('master_driver', 'users.id', '=', 'master_driver.id_seller')
            ->has('masterdriver')->with('masterdriver')->first();

            $master = DB::table('master_driver')->where('id_driver', $id)->update([
                'nama_driver' => $request->nama_driver,
                'no_telp' => $request->no_telp,
                'alamat' => $request->alamat,
            ]);

        return redirect()->route('masterDriver');
    }

    public function hapusDriver($id){
        DB::table('master_driver')->where('id_Driver', $id)->delete();

        return redirect()->route('masterDriver');
    }
}
