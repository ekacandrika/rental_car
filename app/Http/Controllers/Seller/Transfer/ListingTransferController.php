<?php

namespace App\Http\Controllers\Seller\Transfer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Paket;
use App\Models\Pilihan;
use App\Models\MasterChoice;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\Mobildetail;
use App\Models\BusDetail;
use App\Models\MerekMobil;
use App\Models\DetailKendaraan;
use App\Models\DetailRute;
use App\Models\JenisMobil;
use App\Models\LayoutBus;
use App\Models\MerekBus;
use App\Models\JenisBus;
use App\Models\Masterroute;
use App\Models\MasterDriver;
use App\Models\Kategori;
use App\Models\message;
use App\Models\MasterDropPick;
use Illuminate\Http\Request;

class ListingTransferController extends Controller
{
    public function getmerekmobil(Request $request)
    {
        $id_jenis_mobil = $request->id_jenis_mobil;

        $merekmobils = Merekmobil::where('jenis_id', $id_jenis_mobil)->get();

        foreach ($merekmobils as $merekmobil) {
            echo "<option value='$merekmobil->id'>$merekmobil->merek_mobil</option>";
        }
    }

    public function index(Request $request)
    {
        $user_id = Auth::user()->id;

        $jenismobil = JenisMobil::where('user_id', $user_id)->get();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();


        $layoutbus = DB::table('layout_kursi')->where('user_id', $user_id)
            ->join('users', 'layout_kursi.user_id', '=', 'users.id')
            ->get();
        // dd($layoutbus);
        $jenisbus = DB::table('jenis_bus')->where('user_id', $user_id)
            ->join('users', 'jenis_bus.user_id', '=', 'users.id')
            ->get();

        if ($request->session()->missing('transfer.product_code')) {
            $user_id = Auth::user()->id;

            $product_recent = Product::where('type', 'transfer')->orderBy('id', 'desc')->first();
            $code = isset($product_recent) ? $product_recent->product_code : 1;

            if ($code != 1) {
                $code = explode("TR", $code);
                $code = substr($code[1], 0, -8);
                $code++;
            }

            $product = Product::create([
                'user_id' => $user_id,
                'type' => 'transfer',
            ]);

            $request->session()->put('transfer.add_listing.product_id', $product->id);

            $product_id = Product::where('id', $product->id)->first();

            $product_code = $product->id . 'TR' . $code . date('dmY');
            $product->slug = "";

            $product_id->update([
                'product_code' => $product_code,
            ]);
            $request->session()->put('transfer.product_code', $product_code, 'isedit');
        }

        $isedit = false;

        $listing = $request->session()->get('transfer.product_code');
        $add_listing = $request->session()->has('transfer.add_listing') ? $request->session()->get('transfer.add_listing') : '';
        $transfer = $request->session()->has('transfer') ? $request->session()->get('transfer') : '';

        return view('BE.seller.transfer.private.add-listing', compact('jenismobil', 'add_listing', 'layoutbus', 'jenisbus', 'listing', 'transfer', 'hotel', 'xstay', 'isedit'));
    }

    public function addListingPrivate(Request $request)
    {
        /* $gallery = '';
        $fields = collect([]);

        if ($request->hasFile('gallery')) {
            $image = $request->file('gallery');

            foreach ($image as $key => $value) {

                $new_foto = time() . 'GalleryProductTransferPrivate' . $value->getClientOriginalName();
                $tujuan_upload = 'Seller/Product/Transfer/Gallery/';
                $value->move($tujuan_upload, $new_foto);
                $gallery = $tujuan_upload . $new_foto;

                $fields->push([
                    'gallery' => $gallery,
                ]);
            }

            //delete old image
            if ($request->session()->has('transfer.add_listing.gallery')) {
                foreach ($request->session()->get('transfer.add_listing.gallery') as $key => $value) {
                    if (file_exists($value['gallery'])) {
                        unlink($value['gallery']);
                    }
                }
            }
        } */

        $request->session()->put('transfer.add_listing.type', $request->status);
        $request->session()->put('transfer.add_listing.nama_kendaraan', $request->nama_kendaraan);
        $request->session()->put('transfer.add_listing.id_jenis_mobil', $request->jenismobil);
        $request->session()->put('transfer.add_listing.id_merek_mobil', $request->merekmobil);
        $request->session()->put('transfer.add_listing.kapasitas_kursi', $request->kapasitas_kursi);
        $request->session()->put('transfer.add_listing.kapasitas_koper', $request->kapasitas_koper);
        $request->session()->put('transfer.add_listing.status', $request->status);
        // $request->session()->put('transfer.add_listing.gallery', $fields);
        $request->session()->put('transfer.add_listing.deskripsi', $request->deskripsi);
        $request->session()->put('transfer.add_listing.catatan', $request->catatan);
        $request->session()->put('transfer.add_listing.pilihan_tambahan', $request->pilihan_tambahan);
        $request->session()->put('transfer.add_listing.product_name', $request->product_name);

        return redirect()->route('transfer.viewRute');
    }

    public function addTransferScheduleGallery(Request $request)
    {

        // dd($request->all());
        $gallery = '';
        $fields = collect([]);

        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {

                $new_foto = time() . 'GalleryProductTransferUmum' . $value->getClientOriginalName();
                $tujuan_upload = 'Seller/Product/Transfer/Gallery/';
                $value->move($tujuan_upload, $new_foto);
                $gallery = $tujuan_upload . $new_foto;

                $fields->push([
                    'gallery' => $gallery,
                ]);
            }
        }
        // dd($fields);
        //delete old image
        if ($request->session()->has('transfer.add_listing.gallery')) {
            foreach ($request->session()->get('transfer.add_listing.gallery') as $key => $value) {
                dump($value);
                if (file_exists($value['gallery'])) {
                    unlink($value['gallery']);
                }
            }
        }

        $request->session()->put('transfer.add_listing.gallery', $fields);

        return [
            'status' => 1,
            'message' => 'Galeri berhasil diperbarui'
        ];
    }

    public function addListingUmum(Request $request)
    {
        // merekbus
        $request->session()->put('transfer.add_listing.type', $request->status);
        $request->session()->put('transfer.add_listing.nama_kendaraan', $request->nama_kendaraan);
        $request->session()->put('transfer.add_listing.id_jenis_bus', $request->jenisbus);
        $request->session()->put('transfer.add_listing.id_merek_mobil', $request->merekbus);
        $request->session()->put('transfer.add_listing.kapasitas_kursi', $request->kapasitas_kursi);
        $request->session()->put('transfer.add_listing.kapasitas_koper', $request->kapasitas_koper);
        $request->session()->put('transfer.add_listing.status', $request->status);
        $request->session()->put('transfer.add_listing.deskripsi', $request->deskripsi);
        $request->session()->put('transfer.add_listing.catatan', $request->catatan);
        $request->session()->put('transfer.add_listing.pilihan_tambahan', $request->pilihan_tambahan);
        $request->session()->put('transfer.add_listing.product_name', $request->product_name);
        $request->session()->put('transfer.add_listing.layoutbus', $request->layoutbus);

        return redirect()->route('transfer.viewRuteSchedule');
    }

    public function addTransferRegulerGallery(Request $request)
    {

        // dd($request->all());
        $gallery = '';
        $fields = collect([]);

        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {

                $new_foto = time() . 'GalleryProductTransferUmum' . $value->getClientOriginalName();
                $tujuan_upload = 'Seller/Product/Transfer/Gallery/';
                $value->move($tujuan_upload, $new_foto);
                $gallery = $tujuan_upload . $new_foto;

                $fields->push([
                    'gallery' => $gallery,
                ]);
            }
        }
        // dd($fields);
        //delete old image
        if ($request->session()->has('transfer.add_listing.gallery')) {
            foreach ($request->session()->get('transfer.add_listing.gallery') as $key => $value) {
                dump($value);
                if (file_exists($value['gallery'])) {
                    unlink($value['gallery']);
                }
            }
        }

        $request->session()->put('transfer.add_listing.gallery', $fields);

        return [
            'status' => 1,
            'message' => 'Galeri berhasil diperbarui'
        ];
    }

    public function viewRute(Request $request)
    {
        $rute = $request->session()->has('transfer.rute') ? $request->session()->get('transfer.rute') : [];
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $transfer = $request->session()->has('transfer') ? $request->session()->get('transfer') : '';

        $isedit = false;
        return view('BE.seller.transfer.private.rute-addlisting', compact('rute', 'hotel', 'xstay', 'transfer', 'isedit'));
    }

    public function viewRutee(Request $request, $id)
    {
        // dd($request->all());
        $rute = $request->session()->has('transfer.rute') ? $request->session()->get('transfer.rute') : [];

        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $kecamatan = District::all();
        $data = DB::table('masterroutes')->where('id', $id)->get();
        $from = District::first();
        $to = District::first();

        $transfer = $request->session()->has('transfer') ? $request->session()->get('transfer') : '';

        $isedit = false;
        return view('BE.seller.transfer.private.rute-listing', compact('rute', 'kecamatan', 'from', 'to', 'data', 'hotel', 'xstay', 'transfer', 'isedit'));
    }

    public function addRute(Request $request)
    {
        // dd($request->all());
        $request->session()->put('transfer.rute.id_rute', $request->id_rute);
        $request->session()->put('transfer.rute.harga', $request->harga);
        $request->session()->put('transfer.rute.regency_id');

        $rute = $request->session()->has('transfer.rute') ? $request->session()->get('transfer.rute') : '';
        return redirect()->route('transfer.viewBatas');
    }

    public function addRutee(Request $request)
    {
        $request->session()->put('transfer.rute.id_rute', $request->id_rute);
        $request->session()->put('transfer.rute.harga', $request->harga);

        $rute = $request->session()->has('transfer.rute') ? $request->session()->get('transfer.rute') : '';

        return redirect()->route('transfer.viewRutee');
    }

    public function viewRuteSchedule(Request $request)
    {

        $master_drop_pick = MasterDropPick::all();

        $rute = '';
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        if ($request->session()->has('transfer.rute')) {
            $rute = $request->session()->get('transfer.rute');
        } else {
            $rute = collect([]);
            $request->session()->put('transfer.rute', null);
        }

        $isedit = false;
        $transfer = $request->session()->has('transfer') ? $request->session()->get('transfer') : '';

        // dd($transfer);
        return view('BE.seller.transfer.private.rute-schedule', compact('rute', 'hotel', 'xstay', 'transfer', 'isedit','master_drop_pick'));
    }

    public function addRuteSchedule(Request $request)
    {
        // dd($request->all());
        $drop_pick = collect([]);

        $rutes = $request->rute;

        foreach ($rutes as $key => $rute) {
            # code...
            $pickup = MasterDropPick::where('id',$rute['id_naik'])->first();
            $drop = MasterDropPick::where('id',$rute['id_turun'])->first();

            // dump($pickup);

            $drop_pick->push([
                'judul_bus'=>$rute['judul_bus'],
                'id_pickup'=>$rute['id_naik'],
                'latitude_pickup'=>$pickup->latitude,
                'longitude_pickup'=>$pickup->longitude,
                'tempat_pickup'=>$pickup->judulDropPick,
                'id_drop'=>$rute['id_turun'],
                'latitude_drop'=>$drop->latitude,
                'longitude_drop'=>$drop->longitude,
                'tempat_drop'=>$drop->judulDropPick,
                'harga'=>$rute['harga']
            ]);
        }
        
        $request->session()->put('transfer.rute', $drop_pick);
        $rute = $request->session()->has('transfer.rute') ? $request->session()->get('transfer.rute') : null;

        return redirect()->route('transfer.viewAvailability');
    }

    public function viewTransferAvailablity(Request $request)
    {

        $rute = '';
        $master_drop_pick = MasterDropPick::all();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        if ($request->session()->has('transfer.rute')) {
            $rute = $request->session()->get('transfer.rute');
        } else {
            $rute = collect([]);
            $request->session()->put('transfer.rute', null);
        }

        $isedit = false;
        $transfer = $request->session()->has('transfer') ? $request->session()->get('transfer') : '';
        return view('BE.seller.transfer.private.rute-schedule', compact('rute', 'hotel', 'xstay', 'transfer', 'isedit', 'master_drop_pick'));
    }

    public function addTransferAvailablity(Request $request)
    {

        $available = collect([]);
        // dd($available);

        foreach ($request->details as $key => $value) {
            $available->push([
                'tgl_awal' => $value['tgl_awal'],
                'tgl_akhir' => $value['tgl_akhir'],
                'diskon' => $value['diskon'],
                'rute' => $request->session()->get('transfer.rute')
            ]);
        }

        // dd($available);

        $request->session()->put('transfer.available', $available);


        return redirect()->route('transfer.viewBatas');
    }

    public function viewBatas(Request $request)
    {

        // dd($request->session());
        $type = $request->session()->get('transfer.add_listing.status');
        // dd($type);
        $batas = '';
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        if ($request->session()->has('transfer.batas')) {
            $batas = $request->session()->get('transfer.batas');
        } else {
            $request->session()->put('transfer.batas', '');
            $batas = '';
        }

        $transfer = $request->session()->has('transfer') ? $request->session()->get('transfer') : '';
        $isedit = false;
        return view('BE.seller.transfer.private.pembayaran-addlisting', compact('batas', 'transfer', 'hotel', 'xstay', 'type', 'isedit'));
    }

    public function addBatas(Request $request)
    {
        // dd($request->session());

        $pesan = [
            'required' => 'Kebijakan pembatalan harus ditambahkan'
        ];

        $this->validate($request, [
            'fields' => 'required'
        ], $pesan);

        $request->session()->put('transfer.batas.batas_pembayaran', $request->batas_pembayaran);

        $fields = collect([]);

        if (isset($request->fields)) {
            foreach ($request->fields as $key => $value) {
                $fields->push([
                    'kebijakan_pembatalan_sebelumnya' => $value['kebijakan_pembatalan_sebelumnya'],
                    'kebijakan_pembatalan_sesudah' => $value['kebijakan_pembatalan_sesudah'],
                    'kebijakan_pembatalan_potongan' => $value['kebijakan_pembatalan_potongan']
                ]);
            }
        }

        $request->session()->put('transfer.batas.pembatalan', $fields);

        $batas = $request->session()->has('transfer.batas') ? $request->session()->get('transfer.batas') : '';

        return redirect()->route('transfer.viewPilihan');
    }

    public function viewPilihan(Request $request)
    {

        $user_id = Auth::user()->id;
        $type = $request->session()->get('transfer.add_listing.status');

        $master_pilihan = MasterChoice::where('product_type', 'transfer and rental')->get();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', ['hotel'])->orWhere('type', 'xstay')->get();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        // dd($hotel);
        $masterdriver = DB::table('master_driver')->where('id_seller', $user_id)
            ->join('users', 'master_driver.id_seller', '=', 'users.id')
            ->get();

        $pilihan_ekstra = '';

        if ($request->session()->has('transfer.pilihan_ekstra')) {
            $pilihan_ekstra = $request->session()->get('transfer.pilihan_ekstra');
        } else {
            $request->session()->put('transfer.pilihan_ekstra', '');
            $pilihan_ekstra = '';
        }
        $isedit = false;
        return view('BE.seller.transfer.private.pilihan-addlisting', compact('pilihan_ekstra', 'masterdriver', 'hotel', 'xstay', 'master_pilihan', 'type', 'isedit'));
    }

    public function addPilihan(Request $request)
    {
        dd($request->session());
        // fields
        if ($request->session()->get('transfer.add_listing.type') == 'Transfer Pivate') {
            $confrim = 'by_seller';
        }else{
            $confrim = 'instant';
        }

        // $pesan = [
        //     'required' => 'Harap pilihan dan ekstra ditambahkan'
        // ];

        // $this->validate($request, [
        //     'fields' => 'required'
        // ], $pesan);

        $request->session()->put('transfer.pilihan_ekstra.ekstra', $request->ekstra_radio_button);

        $fields = collect([]);

        // $request->session()->put('transfer.product_code', '13tr413022023');
        // dd($request->session()->get('transfer'));
        $product = Product::where('product_code', $request->session()->get('transfer.product_code'))->first();

        if (isset($product->slug)) {
            $product->slug = "";
        }

        $product->update([
            'product_name' => $request->session()->get('transfer.add_listing.nama_kendaraan'),
        ]);

        if ($request->fields) {
            foreach ($request->fields as $key => $value) {
                $fields->push([
                    'judul_pilihan' => isset($value['nama_pilihan']) ? $value['nama_pilihan'] : null,
                    'deskripsi_pilihan' => null,
                    'harga_pilihan' => $value['harga_pilihan'],
                    'kewajiban_pilihan' => isset($value['kewajiban_pilihan']) ? $value['kewajiban_pilihan'] : null,
                    'nama_pilihan' => isset($value['nama_pilihan']) ? $value['nama_pilihan'] : null,
                ]);
            }
        }

        $request->session()->put('transfer.masterdriver.driver', $request->masterdriver);
        $request->session()->put('transfer.pilihan_ekstra.pilihan', $fields);

        $user_id = Auth::user()->id;
        $kategori = Kategori::where('nama_kategori', 'transfer')->first();
        $gallery = $request->session()->get('transfer.add_listing.gallery');
        $kebijakan_pembatalan_sebelumnya = collect([]);
        $kebijakan_pembatalan_sesudah = collect([]);
        $kebijakan_pembatalan_potongan = collect([]);

        foreach ($request->session()->get('transfer.batas.pembatalan') as $key => $value) {
            $kebijakan_pembatalan_sebelumnya->push($value['kebijakan_pembatalan_sebelumnya']);
            $kebijakan_pembatalan_sesudah->push($value['kebijakan_pembatalan_sesudah']);
            $kebijakan_pembatalan_potongan->push($value['kebijakan_pembatalan_potongan']);
        }

        $judul_pilihan = collect([]);
        $deskripsi_pilihan = collect([]);
        $harga_pilihan = collect([]);
        $kewajiban_pilihan = collect([]);
        $nama_pilihan = collect([]);

        foreach ($request->session()->get('transfer.pilihan_ekstra.pilihan') as $key => $value) {
            $judul_pilihan->push($value['judul_pilihan']);
            $deskripsi_pilihan->push($value['deskripsi_pilihan']);
            $harga_pilihan->push($value['harga_pilihan']);
            $kewajiban_pilihan->push($value['kewajiban_pilihan']);
            $nama_pilihan->push(isset($value['nama_pilihan']) ? $value['nama_pilihan'] : null);
        }

        $pilihan = Pilihan::create([
            'judul_pilihan' => json_encode($judul_pilihan),
            'deskripsi_pilihan' => json_encode($deskripsi_pilihan),
            'harga_pilihan' => json_encode($harga_pilihan),
            'kewajiban_pilihan' => json_encode($kewajiban_pilihan),
            'nama_pilihan' => json_encode($nama_pilihan),
            'driver' => $request->session()->get('transfer.masterdriver.driver'),
        ]);

        $detailkendaraan = DetailKendaraan::create([
            'id_jenis_bus' => $request->session()->get('transfer.add_listing.id_jenis_bus'),
            'id_jenis_mobil' => $request->session()->get('transfer.add_listing.id_jenis_mobil'),
            'id_merek_mobil' => $request->session()->get('transfer.add_listing.id_merek_mobil'),
            'layout_bus_id' => $request->session()->get('transfer.add_listing.layoutbus'),
            'nama_kendaraan' => $request->session()->get('transfer.add_listing.nama_kendaraan'),
            'kapasitas_kursi' => $request->session()->get('transfer.add_listing.kapasitas_kursi'),
            'kapasitas_koper' => $request->session()->get('transfer.add_listing.kapasitas_koper'),
            'harga_akomodasi' => $request->session()->get('transfer.rute.harga'),
            'status' => $request->session()->get('transfer.add_listing.status'),
            'pilihan_tambahan' => $request->session()->get('transfer.add_listing.pilihan_tambahan'),
        ]);

        // get droppick data
        $pickup = MasterDropPick::where('id',$request->session()->get('transfer.rute.id_naik'))->first();
        $drop = MasterDropPick::where('id',$request->session()->get('transfer.rute.id_turun'))->first();

        $drop_pick_detail['result'] =  $request->session()->get('transfer.rute');
        // [
        //     'judul_rute' => $request->session()->get('transfer.rute.judul_bus'),
        //     'id_naik' => $request->session()->get('transfer.rute.id_naik'),
        //     'judul_naik' => $pickup->judulDropPick,
        //     'naik_long' => $pickup->latitude,
        //     'naik_lat' => $pickup->longitude,
        //     'id_turun' => $request->session()->get('transfer.rute.id_turun'),
        //     'judul_turun' => $drop->judulDropPick,
        //     'turun_long' => $drop->latitude,
        //     'turun_lat' => $drop->longitude,
        //     'harga' => $request->session()->get('transfer.rute.harga'),
        // ];

        // dd(json_encode($drop_pick_detail));

        // $detailrute = DetailRute::create([
        //     'id_rute' => $request->session()->get('transfer.rute.id_rute'),
        //     'judul_rute' => json_encode($request->session()->get('transfer.rute.judul_rute')),
        //     'judul_naik' => $pickup->judulDropPick,
        //     'naik_long' => $pickup->latitude,
        //     'naik_lat' => $pickup->longitude,
        //     'judul_turun' => $drop->judulDropPick,
        //     'turun_long' => $drop->latitude,
        //     'turun_lat' => $drop->longitude,
        //     'harga' => $request->session()->get('transfer.rute.harga'),
        // ]);

        $productdetail = Productdetail::create([
            'product_id' => $request->session()->get('transfer.add_listing.product_id'),
            'rute_id' => $request->session()->has('transfer.rute.id_rute') ? $request->session()->get('transfer.rute.id_rute'):null,
            'kategori_id' => $kategori->id,
            'regency_id' => $request->session()->has('transfer.rute.regency_id') ? $request->session()->get('transfer.rute.regency_id'):null,
            'pilihan_id' => $pilihan->id,
            'id_detail_kendaraan' => $detailkendaraan->id,
            'confirmation' => $confrim,
            'batas_pembayaran' => $request->session()->get('transfer.batas.batas_pembayaran'),
            'kebijakan_pembatalan_sebelumnya' => $kebijakan_pembatalan_sebelumnya,
            'kebijakan_pembatalan_sesudah' => $kebijakan_pembatalan_sesudah,
            'kebijakan_pembatalan_potongan' => $kebijakan_pembatalan_potongan,
            'deskripsi' => $request->session()->get('transfer.add_listing.deskripsi'),
            'catatan' => $request->session()->get('transfer.add_listing.catatan'),
            'tipe_tur' => $request->session()->get('transfer.add_listing.type'),
            'gallery' => json_encode($gallery),
            'izin_ekstra' => $request->ekstra,
            'base_url' => url('/') . '/transfer/' . $product->slug,
            'drop_pick_detail'=>json_encode($drop_pick_detail)
        ]);

        $request->session()->forget('transfer');
        $request->session()->flash('url', url()->to('/') . '/transfer/' . $product->slug);
        return redirect()->route('dashboardmyListing');
    }

    // Master Mobil

    public function viewMasterMobil(Request $request)
    {
        $user_id = Auth::user()->id;
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $data = MerekMobil::with('jenismobil')->where('merek_mobil.user_id', $user_id)->paginate(10);

        // dd($data);


        $bus = DB::table('merek_bus')
            ->join('jenis_bus', 'merek_bus.id_jenis', '=', 'jenis_bus.id_jenis')
            ->join('users', 'merek_bus.user_id', '=', 'users.id')
            ->where('merek_bus.user_id', $user_id)
            ->paginate(10);

        return view('BE.seller.transfer.master-mobil', compact('data', 'bus', 'hotel', 'xstay'));
    }

    public function viewAddJenisMobil(Request $request)
    {
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $master_mobil = $request->session()->has('mobil.master_mobil') ? $request->session()->get('mobil.master_mobil') : '';
        return view('BE.seller.transfer.merek-mobil', compact('master_mobil', 'hotel', 'xstay'));
    }

    public function addJenisMobil(Request $request)
    {
        $request->session()->put('mobil.master_mobil.jenis_kendaraan', $request->jenis_kendaraan);
        
        $jenismobil = JenisMobil::create([
            'user_id' => auth()->user()->id,
            'jenis_kendaraan' => $request->session()->get('mobil.master_mobil.jenis_kendaraan')
        ]);
        
        // dd($jenismobil);

        return redirect()->route('masterMobil.viewAddMerekMobil');
    }

    public function viewAddMerekMobil(Request $request)
    {
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $jenismobil = JenisMobil::where('user_id',auth()->user()->id)->groupBy('jenis_kendaraan')->get();
        $jenis_mobil = $request->session()->has('mobil.jenis_mobil') ? $request->session()->get('mobil.jenis_mobil') : '';
        return view('BE.seller.transfer.jenis-mobil', compact('jenismobil', 'jenis_mobil', 'hotel', 'xstay'));
    }

    public function addMerekMobil(Request $request)
    {
        // dd($request->all());
        $request->session()->put('mobil.master_mobil.jenis_id', $request->jenismobil);
        $request->session()->put('mobil.master_mobil.merek_mobil', $request->merek_mobil);
        $request->session()->put('mobil.master_mobil.plat_mobil', $request->plat_mobil);

        $user_id = auth()->user()->id;
        $merekmobil = MerekMobil::create([
            'user_id' => Auth::user()->id,
            'jenis_id' => $request->session()->get('mobil.master_mobil.jenis_id'),
            'merek_mobil' => $request->session()->get('mobil.master_mobil.merek_mobil'),
            'plat_mobil' => $request->session()->get('mobil.master_mobil.plat_mobil')
        ]);

        return redirect()->route('masterMobil');
    }

    public function edit($id)
    {
        $merekmobil = DB::table('merek_mobil')->where('id', $id)->get();
        $jenismobil = JenisMobil::groupBy('jenis_kendaraan')->get();
        // dd($jenismobil);
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        return view('BE.seller.transfer.edit-merek-mobil', ['merekmobil' => $merekmobil], compact('jenismobil', 'hotel', 'xstay'));
    }

    public function update(Request $request)
    {
        DB::table('merek_mobil')->where('id', $request->id_merek)->update([
            'jenis_id' => $request->jenismobil,
            'merek_mobil' => $request->merek_mobil,
            'plat_mobil' => $request->plat_mobil
        ]);

        return redirect()->route('masterMobil');
    }

    public function hapus($id)
    {
        DB::table('merek_mobil')->where('id', $id)->delete();

        return redirect()->route('masterMobil');
    }

    // Master Bus

    public function viewAddJenisBus(Request $request)
    {
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $master_bus = $request->session()->has('mobil.master_bus') ? $request->session()->get('mobil.master_bus') : '';
        return view('BE.seller.transfer.jenis-bus', compact('master_bus', 'hotel', 'xstay'));
    }

    public function addJenisBus(Request $request)
    {

        $pesan = [
            'required' => ':attribute tidak boleh kosong'
        ];

        $this->validate($request, [
            'jenis_kendaraan' => 'required',
        ], $pesan);

        $user_id = Auth::user()->id;
        $request->session()->put('mobil.master_bus.jenis_kendaraan', $request->jenis_kendaraan);

        $jenisbus = JenisBus::create([
            'user_id' => $user_id,
            'jenis_kendaraan' => $request->session()->get('mobil.master_bus.jenis_kendaraan')
        ]);

        return redirect()->route('masterMobil.viewAddMerekBus');
    }

    public function viewAddMerekBus(Request $request)
    {
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $user_id = Auth::user()->id;
        $jenisbus = DB::table('jenis_bus')->where('user_id', $user_id)
            ->join('users', 'jenis_bus.user_id', '=', 'users.id')
            ->get();

        $jenis_bus = $request->session()->has('mobil.jenis_bus') ? $request->session()->get('mobil.jenis_bus') : '';
        return view('BE.seller.transfer.merek-bus', compact('jenisbus', 'jenis_bus', 'hotel', 'xstay'));
    }

    public function addMerekBus(Request $request)
    {
        $user_id = Auth::user()->id;
        $request->session()->put('mobil.master_bus.id_jenis', $request->jenisbus);
        $request->session()->put('mobil.master_bus.merek_bus', $request->merek_bus);
        $request->session()->put('mobil.master_bus.plat_bus', $request->plat_bus);

        $merekbus = merekbus::create([
            'user_id' => $user_id,
            'id_jenis' => $request->session()->get('mobil.master_bus.id_jenis'),
            'merek_bus' => $request->session()->get('mobil.master_bus.merek_bus'),
            'plat_bus' => $request->session()->get('mobil.master_bus.plat_bus')
        ]);

        $request->session()->forget('mobil');

        return redirect()->route('masterMobil');
    }

    // Master Layout Bus

    public function viewMasterBus(Request $request)
    {
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $data = DB::table('layout_kursi')
            ->join('users', 'layout_kursi.user_id', '=', 'users.id')
            ->paginate(10);

        return view('BE.seller.transfer.master-bus', ['data' => $data], compact('hotel', 'xstay'));
    }

    public function viewAddLayoutBus(Request $request)
    {
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $layout_bus = $request->session()->has('layout.layout_bus') ? $request->session()->get('layout.layout_bus') : '';
        $isedit = false;
        return view('BE.seller.transfer.layout-bus', compact('layout_bus', 'hotel', 'xstay', 'isedit'));
    }

    public function viewEditLayoutBus(Request $request, $id)
    {
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $layout_bus = LayoutBus::where('id_layout', $id)->first();
        
        // dd($layout_bus);

        if ($layout_bus->jumlah_kursi == 31) {
            // $layout_m31 = LayoutBus::where('id_layout',$id)->where('jumlah_kursi',$layout_bus->jumlah_kursi)->get();
            $layout_m31 = $layout_bus;
        } else {
            $layout_m31 = null;
        }

        if ($layout_bus->jumlah_kursi == 39) {
            // $layout_m39 = LayoutBus::where('id_layout',$id)->where('jumlah_kursi',$layout_bus->jumlah_kursi)->get();
            $layout_m39 = $layout_bus;
        } else {
            $layout_m39 = null;
        }
        // $layout_m31 =
        $layouts = json_decode($layout_bus->layout_kursi, true)['result'];
        $layout = collect([]);

        foreach($layouts as $key => $value){

            $layout->push([
                'chair_number'=>$value['kursi'],
                'price_chair'=>isset($value['harga']) ? $value['harga'] : null,
                'disable_chair'=>isset($value['disable']) ? $value['disable'] : null,
            ]);
        }
        // dd($layout);

        $isedit = true;
        // return view('BE.seller.transfer.layout-bus-edit', compact('layout_bus', 'layout_m31', 'layout_m39', 'hotel', 'xstay', 'isedit', 'id'));
        return view('BE.seller.transfer.new-layout-bus-edit', compact('layout_bus', 'layout_m31', 'layout_m39', 'layout', 'hotel', 'xstay', 'isedit', 'id'));
    }

    public function editLayoutBus(Request $request, $id)
    {

        // dd($request->all());

        $user_id = Auth::user()->id;
        $layout_bus = LayoutBus::where('id_layout', $id)->first();

        $request->session()->put('layout.layout_bus.jumlah_kursi', $request->jumlah_kursi);

        $request->session()->put('layout.layout_bus.layout_kursi', $request->layout_kursi);
        $request->session()->put('layout.layout_bus.lantai_bus', $request->lantai_bus);

        $request->session()->put('layout.layout_bus.kursi_1', $request->kursi_1);
        $request->session()->put('layout.layout_bus.kursi_2', $request->kursi_2);
        $request->session()->put('layout.layout_bus.kursi_3', $request->kursi_3);
        $request->session()->put('layout.layout_bus.kursi_4', $request->kursi_4);
        $request->session()->put('layout.layout_bus.kursi_5', $request->kursi_5);
        $request->session()->put('layout.layout_bus.kursi_6', $request->kursi_6);
        $request->session()->put('layout.layout_bus.kursi_7', $request->kursi_7);
        $request->session()->put('layout.layout_bus.kursi_8', $request->kursi_8);
        $request->session()->put('layout.layout_bus.kursi_9', $request->kursi_9);
        $request->session()->put('layout.layout_bus.kursi_10', $request->kursi_10);
        $request->session()->put('layout.layout_bus.kursi_11', $request->kursi_11);
        $request->session()->put('layout.layout_bus.kursi_12', $request->kursi_12);
        $request->session()->put('layout.layout_bus.kursi_13', $request->kursi_13);
        $request->session()->put('layout.layout_bus.kursi_14', $request->kursi_14);
        $request->session()->put('layout.layout_bus.kursi_15', $request->kursi_15);
        $request->session()->put('layout.layout_bus.kursi_16', $request->kursi_16);
        $request->session()->put('layout.layout_bus.kursi_17', $request->kursi_17);
        $request->session()->put('layout.layout_bus.kursi_18', $request->kursi_18);
        $request->session()->put('layout.layout_bus.kursi_19', $request->kursi_19);
        $request->session()->put('layout.layout_bus.kursi_20', $request->kursi_20);
        $request->session()->put('layout.layout_bus.kursi_21', $request->kursi_21);
        $request->session()->put('layout.layout_bus.kursi_22', $request->kursi_22);
        $request->session()->put('layout.layout_bus.kursi_23', $request->kursi_23);
        $request->session()->put('layout.layout_bus.kursi_24', $request->kursi_24);
        $request->session()->put('layout.layout_bus.kursi_25', $request->kursi_25);
        $request->session()->put('layout.layout_bus.kursi_26', $request->kursi_26);
        $request->session()->put('layout.layout_bus.kursi_27', $request->kursi_27);
        $request->session()->put('layout.layout_bus.kursi_28', $request->kursi_28);
        $request->session()->put('layout.layout_bus.kursi_29', $request->kursi_29);
        $request->session()->put('layout.layout_bus.kursi_30', $request->kursi_30);
        $request->session()->put('layout.layout_bus.kursi_31', $request->kursi_31);
        $request->session()->put('layout.layout_bus.kursi_32', $request->kursi_32);
        $request->session()->put('layout.layout_bus.kursi_33', $request->kursi_33);
        $request->session()->put('layout.layout_bus.kursi_34', $request->kursi_34);
        $request->session()->put('layout.layout_bus.kursi_35', $request->kursi_35);
        $request->session()->put('layout.layout_bus.kursi_36', $request->kursi_36);
        $request->session()->put('layout.layout_bus.kursi_37', $request->kursi_37);
        $request->session()->put('layout.layout_bus.kursi_38', $request->kursi_38);
        $request->session()->put('layout.layout_bus.kursi_39', $request->kursi_39);
        $request->session()->put('layout.layout_bus.kursi_40', $request->kursi_40);
        $request->session()->put('layout.layout_bus.kursi_41', $request->kursi_41);
        $request->session()->put('layout.layout_bus.kursi_42', $request->kursi_42);
        $request->session()->put('layout.layout_bus.kursi_43', $request->kursi_43);
        $request->session()->put('layout.layout_bus.kursi_44', $request->kursi_44);
        $request->session()->put('layout.layout_bus.kursi_45', $request->kursi_45);
        $request->session()->put('layout.layout_bus.kursi_46', $request->kursi_46);
        $request->session()->put('layout.layout_bus.kursi_47', $request->kursi_47);
        $request->session()->put('layout.layout_bus.kursi_48', $request->kursi_48);
        $request->session()->put('layout.layout_bus.kursi_49', $request->kursi_49);
        $request->session()->put('layout.layout_bus.kursi_50', $request->kursi_50);
        $request->session()->put('layout.layout_bus.kursi_51', $request->kursi_51);
        $request->session()->put('layout.layout_bus.kursi_52', $request->kursi_52);
        $request->session()->put('layout.layout_bus.kursi_53', $request->kursi_53);
        $request->session()->put('layout.layout_bus.kursi_54', $request->kursi_54);
        $request->session()->put('layout.layout_bus.kursi_55', $request->kursi_55);
        $request->session()->put('layout.layout_bus.kursi_56', $request->kursi_56);
        $request->session()->put('layout.layout_bus.kursi_57', $request->kursi_57);
        $request->session()->put('layout.layout_bus.kursi_58', $request->kursi_58);
        $request->session()->put('layout.layout_bus.kursi_59', $request->kursi_59);

        $request->session()->put('layout.layout_bus.harga_1', $request->harga_1);
        $request->session()->put('layout.layout_bus.harga_2', $request->harga_2);
        $request->session()->put('layout.layout_bus.harga_3', $request->harga_3);
        $request->session()->put('layout.layout_bus.harga_4', $request->harga_4);
        $request->session()->put('layout.layout_bus.harga_5', $request->harga_5);
        $request->session()->put('layout.layout_bus.harga_6', $request->harga_6);
        $request->session()->put('layout.layout_bus.harga_7', $request->harga_7);
        $request->session()->put('layout.layout_bus.harga_8', $request->harga_8);
        $request->session()->put('layout.layout_bus.harga_9', $request->harga_9);
        $request->session()->put('layout.layout_bus.harga_10', $request->harga_10);
        $request->session()->put('layout.layout_bus.harga_11', $request->harga_11);
        $request->session()->put('layout.layout_bus.harga_12', $request->harga_12);
        $request->session()->put('layout.layout_bus.harga_13', $request->harga_13);
        $request->session()->put('layout.layout_bus.harga_14', $request->harga_14);
        $request->session()->put('layout.layout_bus.harga_15', $request->harga_15);
        $request->session()->put('layout.layout_bus.harga_16', $request->harga_16);
        $request->session()->put('layout.layout_bus.harga_17', $request->harga_17);
        $request->session()->put('layout.layout_bus.harga_18', $request->harga_18);
        $request->session()->put('layout.layout_bus.harga_19', $request->harga_19);
        $request->session()->put('layout.layout_bus.harga_20', $request->harga_20);
        $request->session()->put('layout.layout_bus.harga_21', $request->harga_21);
        $request->session()->put('layout.layout_bus.harga_22', $request->harga_22);
        $request->session()->put('layout.layout_bus.harga_23', $request->harga_23);
        $request->session()->put('layout.layout_bus.harga_24', $request->harga_24);
        $request->session()->put('layout.layout_bus.harga_25', $request->harga_25);
        $request->session()->put('layout.layout_bus.harga_26', $request->harga_26);
        $request->session()->put('layout.layout_bus.harga_27', $request->harga_27);
        $request->session()->put('layout.layout_bus.harga_28', $request->harga_28);
        $request->session()->put('layout.layout_bus.harga_29', $request->harga_29);
        $request->session()->put('layout.layout_bus.harga_30', $request->harga_30);
        $request->session()->put('layout.layout_bus.harga_31', $request->harga_31);
        $request->session()->put('layout.layout_bus.harga_32', $request->harga_32);
        $request->session()->put('layout.layout_bus.harga_33', $request->harga_33);
        $request->session()->put('layout.layout_bus.harga_34', $request->harga_34);
        $request->session()->put('layout.layout_bus.harga_35', $request->harga_35);
        $request->session()->put('layout.layout_bus.harga_36', $request->harga_36);
        $request->session()->put('layout.layout_bus.harga_37', $request->harga_37);
        $request->session()->put('layout.layout_bus.harga_38', $request->harga_38);
        $request->session()->put('layout.layout_bus.harga_39', $request->harga_39);
        $request->session()->put('layout.layout_bus.harga_40', $request->harga_40);
        $request->session()->put('layout.layout_bus.harga_41', $request->harga_41);
        $request->session()->put('layout.layout_bus.harga_42', $request->harga_42);
        $request->session()->put('layout.layout_bus.harga_43', $request->harga_43);
        $request->session()->put('layout.layout_bus.harga_44', $request->harga_44);
        $request->session()->put('layout.layout_bus.harga_45', $request->harga_45);
        $request->session()->put('layout.layout_bus.harga_46', $request->harga_46);
        $request->session()->put('layout.layout_bus.harga_47', $request->harga_47);
        $request->session()->put('layout.layout_bus.harga_48', $request->harga_48);
        $request->session()->put('layout.layout_bus.harga_49', $request->harga_49);
        $request->session()->put('layout.layout_bus.harga_50', $request->harga_50);
        $request->session()->put('layout.layout_bus.harga_51', $request->harga_51);
        $request->session()->put('layout.layout_bus.harga_52', $request->harga_52);
        $request->session()->put('layout.layout_bus.harga_53', $request->harga_53);
        $request->session()->put('layout.layout_bus.harga_54', $request->harga_54);
        $request->session()->put('layout.layout_bus.harga_55', $request->harga_55);
        $request->session()->put('layout.layout_bus.harga_56', $request->harga_56);
        $request->session()->put('layout.layout_bus.harga_57', $request->harga_57);
        $request->session()->put('layout.layout_bus.harga_58', $request->harga_58);
        $request->session()->put('layout.layout_bus.harga_59', $request->harga_59);

        $layout_bus::where('id_layout', $id)->update([
            'user_id' => $user_id,
            'jumlah_kursi' => $request->session()->get('layout.layout_bus.jumlah_kursi'),
            'layout_kursi' => $request->session()->get('layout.layout_bus.layout_kursi'),
            'lantai_bus' => $request->session()->get('layout.layout_bus.lantai_bus'),
            'kursi_1' => $request->session()->get('layout.layout_bus.kursi_1'),
            'kursi_2' => $request->session()->get('layout.layout_bus.kursi_2'),
            'kursi_3' => $request->session()->get('layout.layout_bus.kursi_3'),
            'kursi_4' => $request->session()->get('layout.layout_bus.kursi_4'),
            'kursi_5' => $request->session()->get('layout.layout_bus.kursi_5'),
            'kursi_6' => $request->session()->get('layout.layout_bus.kursi_6'),
            'kursi_7' => $request->session()->get('layout.layout_bus.kursi_7'),
            'kursi_8' => $request->session()->get('layout.layout_bus.kursi_8'),
            'kursi_9' => $request->session()->get('layout.layout_bus.kursi_9'),
            'kursi_10' => $request->session()->get('layout.layout_bus.kursi_10'),
            'kursi_11' => $request->session()->get('layout.layout_bus.kursi_11'),
            'kursi_12' => $request->session()->get('layout.layout_bus.kursi_12'),
            'kursi_13' => $request->session()->get('layout.layout_bus.kursi_13'),
            'kursi_14' => $request->session()->get('layout.layout_bus.kursi_14'),
            'kursi_15' => $request->session()->get('layout.layout_bus.kursi_15'),
            'kursi_16' => $request->session()->get('layout.layout_bus.kursi_16'),
            'kursi_17' => $request->session()->get('layout.layout_bus.kursi_17'),
            'kursi_18' => $request->session()->get('layout.layout_bus.kursi_18'),
            'kursi_19' => $request->session()->get('layout.layout_bus.kursi_19'),
            'kursi_20' => $request->session()->get('layout.layout_bus.kursi_20'),
            'kursi_21' => $request->session()->get('layout.layout_bus.kursi_21'),
            'kursi_22' => $request->session()->get('layout.layout_bus.kursi_22'),
            'kursi_23' => $request->session()->get('layout.layout_bus.kursi_23'),
            'kursi_24' => $request->session()->get('layout.layout_bus.kursi_24'),
            'kursi_25' => $request->session()->get('layout.layout_bus.kursi_25'),
            'kursi_26' => $request->session()->get('layout.layout_bus.kursi_26'),
            'kursi_27' => $request->session()->get('layout.layout_bus.kursi_27'),
            'kursi_28' => $request->session()->get('layout.layout_bus.kursi_28'),
            'kursi_29' => $request->session()->get('layout.layout_bus.kursi_29'),
            'kursi_30' => $request->session()->get('layout.layout_bus.kursi_30'),
            'kursi_31' => $request->session()->get('layout.layout_bus.kursi_31'),
            'kursi_32' => $request->session()->get('layout.layout_bus.kursi_32'),
            'kursi_33' => $request->session()->get('layout.layout_bus.kursi_33'),
            'kursi_34' => $request->session()->get('layout.layout_bus.kursi_34'),
            'kursi_35' => $request->session()->get('layout.layout_bus.kursi_35'),
            'kursi_36' => $request->session()->get('layout.layout_bus.kursi_36'),
            'kursi_37' => $request->session()->get('layout.layout_bus.kursi_37'),
            'kursi_38' => $request->session()->get('layout.layout_bus.kursi_38'),
            'kursi_39' => $request->session()->get('layout.layout_bus.kursi_39'),
            'kursi_40' => $request->session()->get('layout.layout_bus.kursi_40'),
            'kursi_41' => $request->session()->get('layout.layout_bus.kursi_41'),
            'kursi_42' => $request->session()->get('layout.layout_bus.kursi_42'),
            'kursi_43' => $request->session()->get('layout.layout_bus.kursi_43'),
            'kursi_44' => $request->session()->get('layout.layout_bus.kursi_44'),
            'kursi_45' => $request->session()->get('layout.layout_bus.kursi_45'),
            'kursi_46' => $request->session()->get('layout.layout_bus.kursi_46'),
            'kursi_47' => $request->session()->get('layout.layout_bus.kursi_47'),
            'kursi_48' => $request->session()->get('layout.layout_bus.kursi_48'),
            'kursi_49' => $request->session()->get('layout.layout_bus.kursi_49'),
            'kursi_50' => $request->session()->get('layout.layout_bus.kursi_50'),
            'kursi_51' => $request->session()->get('layout.layout_bus.kursi_51'),
            'kursi_52' => $request->session()->get('layout.layout_bus.kursi_52'),
            'kursi_53' => $request->session()->get('layout.layout_bus.kursi_53'),
            'kursi_54' => $request->session()->get('layout.layout_bus.kursi_54'),
            'kursi_55' => $request->session()->get('layout.layout_bus.kursi_55'),
            'kursi_56' => $request->session()->get('layout.layout_bus.kursi_56'),
            'kursi_57' => $request->session()->get('layout.layout_bus.kursi_57'),
            'kursi_58' => $request->session()->get('layout.layout_bus.kursi_58'),
            'kursi_59' => $request->session()->get('layout.layout_bus.kursi_59'),

            'harga_1' => $request->session()->get('layout.layout_bus.harga_1'),
            'harga_2' => $request->session()->get('layout.layout_bus.harga_2'),
            'harga_3' => $request->session()->get('layout.layout_bus.harga_3'),
            'harga_4' => $request->session()->get('layout.layout_bus.harga_4'),
            'harga_5' => $request->session()->get('layout.layout_bus.harga_5'),
            'harga_6' => $request->session()->get('layout.layout_bus.harga_6'),
            'harga_7' => $request->session()->get('layout.layout_bus.harga_7'),
            'harga_8' => $request->session()->get('layout.layout_bus.harga_8'),
            'harga_9' => $request->session()->get('layout.layout_bus.harga_9'),
            'harga_10' => $request->session()->get('layout.layout_bus.harga_10'),
            'harga_11' => $request->session()->get('layout.layout_bus.harga_11'),
            'harga_12' => $request->session()->get('layout.layout_bus.harga_12'),
            'harga_13' => $request->session()->get('layout.layout_bus.harga_13'),
            'harga_14' => $request->session()->get('layout.layout_bus.harga_14'),
            'harga_15' => $request->session()->get('layout.layout_bus.harga_15'),
            'harga_16' => $request->session()->get('layout.layout_bus.harga_16'),
            'harga_17' => $request->session()->get('layout.layout_bus.harga_17'),
            'harga_18' => $request->session()->get('layout.layout_bus.harga_18'),
            'harga_19' => $request->session()->get('layout.layout_bus.harga_19'),
            'harga_20' => $request->session()->get('layout.layout_bus.harga_20'),
            'harga_21' => $request->session()->get('layout.layout_bus.harga_21'),
            'harga_22' => $request->session()->get('layout.layout_bus.harga_22'),
            'harga_23' => $request->session()->get('layout.layout_bus.harga_23'),
            'harga_24' => $request->session()->get('layout.layout_bus.harga_24'),
            'harga_25' => $request->session()->get('layout.layout_bus.harga_25'),
            'harga_26' => $request->session()->get('layout.layout_bus.harga_26'),
            'harga_27' => $request->session()->get('layout.layout_bus.harga_27'),
            'harga_28' => $request->session()->get('layout.layout_bus.harga_28'),
            'harga_29' => $request->session()->get('layout.layout_bus.harga_29'),
            'harga_30' => $request->session()->get('layout.layout_bus.harga_30'),
            'harga_31' => $request->session()->get('layout.layout_bus.harga_31'),
            'harga_32' => $request->session()->get('layout.layout_bus.harga_32'),
            'harga_33' => $request->session()->get('layout.layout_bus.harga_33'),
            'harga_34' => $request->session()->get('layout.layout_bus.harga_34'),
            'harga_35' => $request->session()->get('layout.layout_bus.harga_35'),
            'harga_36' => $request->session()->get('layout.layout_bus.harga_36'),
            'harga_37' => $request->session()->get('layout.layout_bus.harga_37'),
            'harga_38' => $request->session()->get('layout.layout_bus.harga_38'),
            'harga_39' => $request->session()->get('layout.layout_bus.harga_39'),
            'harga_40' => $request->session()->get('layout.layout_bus.harga_40'),
            'harga_41' => $request->session()->get('layout.layout_bus.harga_41'),
            'harga_42' => $request->session()->get('layout.layout_bus.harga_42'),
            'harga_43' => $request->session()->get('layout.layout_bus.harga_43'),
            'harga_44' => $request->session()->get('layout.layout_bus.harga_44'),
            'harga_45' => $request->session()->get('layout.layout_bus.harga_45'),
            'harga_46' => $request->session()->get('layout.layout_bus.harga_46'),
            'harga_47' => $request->session()->get('layout.layout_bus.harga_47'),
            'harga_48' => $request->session()->get('layout.layout_bus.harga_48'),
            'harga_49' => $request->session()->get('layout.layout_bus.harga_49'),
            'harga_50' => $request->session()->get('layout.layout_bus.harga_50'),
            'harga_51' => $request->session()->get('layout.layout_bus.harga_51'),
            'harga_52' => $request->session()->get('layout.layout_bus.harga_52'),
            'harga_53' => $request->session()->get('layout.layout_bus.harga_53'),
            'harga_54' => $request->session()->get('layout.layout_bus.harga_54'),
            'harga_55' => $request->session()->get('layout.layout_bus.harga_55'),
            'harga_56' => $request->session()->get('layout.layout_bus.harga_56'),
            'harga_57' => $request->session()->get('layout.layout_bus.harga_57'),
            'harga_58' => $request->session()->get('layout.layout_bus.harga_58'),
            'harga_59' => $request->session()->get('layout.layout_bus.harga_59')
        ]);

        $request->session()->forget('layout');

        return redirect()->route('masterBus');
    }

    public function editLayoutBusM39(Request $request, $id)
    {
        $user_id = Auth::user()->id;
        $layout_bus = LayoutBus::where('id_layout', $id)->first();

        $request->session()->put('layout.layout_bus.jumlah_kursi', $request->jumlah_kursi);

        $request->session()->put('layout.layout_bus.layout_kursi', $request->layout_kursi);
        $request->session()->put('layout.layout_bus.lantai_bus', $request->lantai_bus);

        $request->session()->put('layout.layout_bus.kursi_1', $request->kursi_1);
        $request->session()->put('layout.layout_bus.kursi_2', $request->kursi_2);
        $request->session()->put('layout.layout_bus.kursi_3', $request->kursi_3);
        $request->session()->put('layout.layout_bus.kursi_4', $request->kursi_4);
        $request->session()->put('layout.layout_bus.kursi_5', $request->kursi_5);
        $request->session()->put('layout.layout_bus.kursi_6', $request->kursi_6);
        $request->session()->put('layout.layout_bus.kursi_7', $request->kursi_7);
        $request->session()->put('layout.layout_bus.kursi_8', $request->kursi_8);
        $request->session()->put('layout.layout_bus.kursi_9', $request->kursi_9);
        $request->session()->put('layout.layout_bus.kursi_10', $request->kursi_10);
        $request->session()->put('layout.layout_bus.kursi_11', $request->kursi_11);
        $request->session()->put('layout.layout_bus.kursi_12', $request->kursi_12);
        $request->session()->put('layout.layout_bus.kursi_13', $request->kursi_13);
        $request->session()->put('layout.layout_bus.kursi_14', $request->kursi_14);
        $request->session()->put('layout.layout_bus.kursi_15', $request->kursi_15);
        $request->session()->put('layout.layout_bus.kursi_16', $request->kursi_16);
        $request->session()->put('layout.layout_bus.kursi_17', $request->kursi_17);
        $request->session()->put('layout.layout_bus.kursi_18', $request->kursi_18);
        $request->session()->put('layout.layout_bus.kursi_19', $request->kursi_19);
        $request->session()->put('layout.layout_bus.kursi_20', $request->kursi_20);
        $request->session()->put('layout.layout_bus.kursi_21', $request->kursi_21);
        $request->session()->put('layout.layout_bus.kursi_22', $request->kursi_22);
        $request->session()->put('layout.layout_bus.kursi_23', $request->kursi_23);
        $request->session()->put('layout.layout_bus.kursi_24', $request->kursi_24);
        $request->session()->put('layout.layout_bus.kursi_25', $request->kursi_25);
        $request->session()->put('layout.layout_bus.kursi_26', $request->kursi_26);
        $request->session()->put('layout.layout_bus.kursi_27', $request->kursi_27);
        $request->session()->put('layout.layout_bus.kursi_28', $request->kursi_28);
        $request->session()->put('layout.layout_bus.kursi_29', $request->kursi_29);
        $request->session()->put('layout.layout_bus.kursi_30', $request->kursi_30);
        $request->session()->put('layout.layout_bus.kursi_31', $request->kursi_31);
        $request->session()->put('layout.layout_bus.kursi_32', $request->kursi_32);
        $request->session()->put('layout.layout_bus.kursi_33', $request->kursi_33);
        $request->session()->put('layout.layout_bus.kursi_34', $request->kursi_34);
        $request->session()->put('layout.layout_bus.kursi_35', $request->kursi_35);
        $request->session()->put('layout.layout_bus.kursi_36', $request->kursi_36);
        $request->session()->put('layout.layout_bus.kursi_37', $request->kursi_37);
        $request->session()->put('layout.layout_bus.kursi_38', $request->kursi_38);
        $request->session()->put('layout.layout_bus.kursi_39', $request->kursi_39);
        $request->session()->put('layout.layout_bus.kursi_40', $request->kursi_40);
        $request->session()->put('layout.layout_bus.kursi_41', $request->kursi_41);
        $request->session()->put('layout.layout_bus.kursi_42', $request->kursi_42);
        $request->session()->put('layout.layout_bus.kursi_43', $request->kursi_43);
        $request->session()->put('layout.layout_bus.kursi_44', $request->kursi_44);
        $request->session()->put('layout.layout_bus.kursi_45', $request->kursi_45);
        $request->session()->put('layout.layout_bus.kursi_46', $request->kursi_46);
        $request->session()->put('layout.layout_bus.kursi_47', $request->kursi_47);
        $request->session()->put('layout.layout_bus.kursi_48', $request->kursi_48);
        $request->session()->put('layout.layout_bus.kursi_49', $request->kursi_49);
        $request->session()->put('layout.layout_bus.kursi_50', $request->kursi_50);
        $request->session()->put('layout.layout_bus.kursi_51', $request->kursi_51);
        $request->session()->put('layout.layout_bus.kursi_52', $request->kursi_52);
        $request->session()->put('layout.layout_bus.kursi_53', $request->kursi_53);
        $request->session()->put('layout.layout_bus.kursi_54', $request->kursi_54);
        $request->session()->put('layout.layout_bus.kursi_55', $request->kursi_55);
        $request->session()->put('layout.layout_bus.kursi_56', $request->kursi_56);
        $request->session()->put('layout.layout_bus.kursi_57', $request->kursi_57);
        $request->session()->put('layout.layout_bus.kursi_58', $request->kursi_58);
        $request->session()->put('layout.layout_bus.kursi_59', $request->kursi_59);

        $request->session()->put('layout.layout_bus.harga_1', $request->harga_1);
        $request->session()->put('layout.layout_bus.harga_2', $request->harga_2);
        $request->session()->put('layout.layout_bus.harga_3', $request->harga_3);
        $request->session()->put('layout.layout_bus.harga_4', $request->harga_4);
        $request->session()->put('layout.layout_bus.harga_5', $request->harga_5);
        $request->session()->put('layout.layout_bus.harga_6', $request->harga_6);
        $request->session()->put('layout.layout_bus.harga_7', $request->harga_7);
        $request->session()->put('layout.layout_bus.harga_8', $request->harga_8);
        $request->session()->put('layout.layout_bus.harga_9', $request->harga_9);
        $request->session()->put('layout.layout_bus.harga_10', $request->harga_10);
        $request->session()->put('layout.layout_bus.harga_11', $request->harga_11);
        $request->session()->put('layout.layout_bus.harga_12', $request->harga_12);
        $request->session()->put('layout.layout_bus.harga_13', $request->harga_13);
        $request->session()->put('layout.layout_bus.harga_14', $request->harga_14);
        $request->session()->put('layout.layout_bus.harga_15', $request->harga_15);
        $request->session()->put('layout.layout_bus.harga_16', $request->harga_16);
        $request->session()->put('layout.layout_bus.harga_17', $request->harga_17);
        $request->session()->put('layout.layout_bus.harga_18', $request->harga_18);
        $request->session()->put('layout.layout_bus.harga_19', $request->harga_19);
        $request->session()->put('layout.layout_bus.harga_20', $request->harga_20);
        $request->session()->put('layout.layout_bus.harga_21', $request->harga_21);
        $request->session()->put('layout.layout_bus.harga_22', $request->harga_22);
        $request->session()->put('layout.layout_bus.harga_23', $request->harga_23);
        $request->session()->put('layout.layout_bus.harga_24', $request->harga_24);
        $request->session()->put('layout.layout_bus.harga_25', $request->harga_25);
        $request->session()->put('layout.layout_bus.harga_26', $request->harga_26);
        $request->session()->put('layout.layout_bus.harga_27', $request->harga_27);
        $request->session()->put('layout.layout_bus.harga_28', $request->harga_28);
        $request->session()->put('layout.layout_bus.harga_29', $request->harga_29);
        $request->session()->put('layout.layout_bus.harga_30', $request->harga_30);
        $request->session()->put('layout.layout_bus.harga_31', $request->harga_31);
        $request->session()->put('layout.layout_bus.harga_32', $request->harga_32);
        $request->session()->put('layout.layout_bus.harga_33', $request->harga_33);
        $request->session()->put('layout.layout_bus.harga_34', $request->harga_34);
        $request->session()->put('layout.layout_bus.harga_35', $request->harga_35);
        $request->session()->put('layout.layout_bus.harga_36', $request->harga_36);
        $request->session()->put('layout.layout_bus.harga_37', $request->harga_37);
        $request->session()->put('layout.layout_bus.harga_38', $request->harga_38);
        $request->session()->put('layout.layout_bus.harga_39', $request->harga_39);
        $request->session()->put('layout.layout_bus.harga_40', $request->harga_40);
        $request->session()->put('layout.layout_bus.harga_41', $request->harga_41);
        $request->session()->put('layout.layout_bus.harga_42', $request->harga_42);
        $request->session()->put('layout.layout_bus.harga_43', $request->harga_43);
        $request->session()->put('layout.layout_bus.harga_44', $request->harga_44);
        $request->session()->put('layout.layout_bus.harga_45', $request->harga_45);
        $request->session()->put('layout.layout_bus.harga_46', $request->harga_46);
        $request->session()->put('layout.layout_bus.harga_47', $request->harga_47);
        $request->session()->put('layout.layout_bus.harga_48', $request->harga_48);
        $request->session()->put('layout.layout_bus.harga_49', $request->harga_49);
        $request->session()->put('layout.layout_bus.harga_50', $request->harga_50);
        $request->session()->put('layout.layout_bus.harga_51', $request->harga_51);
        $request->session()->put('layout.layout_bus.harga_52', $request->harga_52);
        $request->session()->put('layout.layout_bus.harga_53', $request->harga_53);
        $request->session()->put('layout.layout_bus.harga_54', $request->harga_54);
        $request->session()->put('layout.layout_bus.harga_55', $request->harga_55);
        $request->session()->put('layout.layout_bus.harga_56', $request->harga_56);
        $request->session()->put('layout.layout_bus.harga_57', $request->harga_57);
        $request->session()->put('layout.layout_bus.harga_58', $request->harga_58);
        $request->session()->put('layout.layout_bus.harga_59', $request->harga_59);

        $layout_bus::where('id_layout', $id)->update([
            'user_id' => $user_id,
            'jumlah_kursi' => $request->session()->get('layout.layout_bus.jumlah_kursi'),
            'layout_kursi' => $request->session()->get('layout.layout_bus.layout_kursi'),
            'lantai_bus' => $request->session()->get('layout.layout_bus.lantai_bus'),
            'kursi_1' => $request->session()->get('layout.layout_bus.kursi_1'),
            'kursi_2' => $request->session()->get('layout.layout_bus.kursi_2'),
            'kursi_3' => $request->session()->get('layout.layout_bus.kursi_3'),
            'kursi_4' => $request->session()->get('layout.layout_bus.kursi_4'),
            'kursi_5' => $request->session()->get('layout.layout_bus.kursi_5'),
            'kursi_6' => $request->session()->get('layout.layout_bus.kursi_6'),
            'kursi_7' => $request->session()->get('layout.layout_bus.kursi_7'),
            'kursi_8' => $request->session()->get('layout.layout_bus.kursi_8'),
            'kursi_9' => $request->session()->get('layout.layout_bus.kursi_9'),
            'kursi_10' => $request->session()->get('layout.layout_bus.kursi_10'),
            'kursi_11' => $request->session()->get('layout.layout_bus.kursi_11'),
            'kursi_12' => $request->session()->get('layout.layout_bus.kursi_12'),
            'kursi_13' => $request->session()->get('layout.layout_bus.kursi_13'),
            'kursi_14' => $request->session()->get('layout.layout_bus.kursi_14'),
            'kursi_15' => $request->session()->get('layout.layout_bus.kursi_15'),
            'kursi_16' => $request->session()->get('layout.layout_bus.kursi_16'),
            'kursi_17' => $request->session()->get('layout.layout_bus.kursi_17'),
            'kursi_18' => $request->session()->get('layout.layout_bus.kursi_18'),
            'kursi_19' => $request->session()->get('layout.layout_bus.kursi_19'),
            'kursi_20' => $request->session()->get('layout.layout_bus.kursi_20'),
            'kursi_21' => $request->session()->get('layout.layout_bus.kursi_21'),
            'kursi_22' => $request->session()->get('layout.layout_bus.kursi_22'),
            'kursi_23' => $request->session()->get('layout.layout_bus.kursi_23'),
            'kursi_24' => $request->session()->get('layout.layout_bus.kursi_24'),
            'kursi_25' => $request->session()->get('layout.layout_bus.kursi_25'),
            'kursi_26' => $request->session()->get('layout.layout_bus.kursi_26'),
            'kursi_27' => $request->session()->get('layout.layout_bus.kursi_27'),
            'kursi_28' => $request->session()->get('layout.layout_bus.kursi_28'),
            'kursi_29' => $request->session()->get('layout.layout_bus.kursi_29'),
            'kursi_30' => $request->session()->get('layout.layout_bus.kursi_30'),
            'kursi_31' => $request->session()->get('layout.layout_bus.kursi_31'),
            'kursi_32' => $request->session()->get('layout.layout_bus.kursi_32'),
            'kursi_33' => $request->session()->get('layout.layout_bus.kursi_33'),
            'kursi_34' => $request->session()->get('layout.layout_bus.kursi_34'),
            'kursi_35' => $request->session()->get('layout.layout_bus.kursi_35'),
            'kursi_36' => $request->session()->get('layout.layout_bus.kursi_36'),
            'kursi_37' => $request->session()->get('layout.layout_bus.kursi_37'),
            'kursi_38' => $request->session()->get('layout.layout_bus.kursi_38'),
            'kursi_39' => $request->session()->get('layout.layout_bus.kursi_39'),
            'kursi_40' => $request->session()->get('layout.layout_bus.kursi_40'),
            'kursi_41' => $request->session()->get('layout.layout_bus.kursi_41'),
            'kursi_42' => $request->session()->get('layout.layout_bus.kursi_42'),
            'kursi_43' => $request->session()->get('layout.layout_bus.kursi_43'),
            'kursi_44' => $request->session()->get('layout.layout_bus.kursi_44'),
            'kursi_45' => $request->session()->get('layout.layout_bus.kursi_45'),
            'kursi_46' => $request->session()->get('layout.layout_bus.kursi_46'),
            'kursi_47' => $request->session()->get('layout.layout_bus.kursi_47'),
            'kursi_48' => $request->session()->get('layout.layout_bus.kursi_48'),
            'kursi_49' => $request->session()->get('layout.layout_bus.kursi_49'),
            'kursi_50' => $request->session()->get('layout.layout_bus.kursi_50'),
            'kursi_51' => $request->session()->get('layout.layout_bus.kursi_51'),
            'kursi_52' => $request->session()->get('layout.layout_bus.kursi_52'),
            'kursi_53' => $request->session()->get('layout.layout_bus.kursi_53'),
            'kursi_54' => $request->session()->get('layout.layout_bus.kursi_54'),
            'kursi_55' => $request->session()->get('layout.layout_bus.kursi_55'),
            'kursi_56' => $request->session()->get('layout.layout_bus.kursi_56'),
            'kursi_57' => $request->session()->get('layout.layout_bus.kursi_57'),
            'kursi_58' => $request->session()->get('layout.layout_bus.kursi_58'),
            'kursi_59' => $request->session()->get('layout.layout_bus.kursi_59'),

            'harga_1' => $request->session()->get('layout.layout_bus.harga_1'),
            'harga_2' => $request->session()->get('layout.layout_bus.harga_2'),
            'harga_3' => $request->session()->get('layout.layout_bus.harga_3'),
            'harga_4' => $request->session()->get('layout.layout_bus.harga_4'),
            'harga_5' => $request->session()->get('layout.layout_bus.harga_5'),
            'harga_6' => $request->session()->get('layout.layout_bus.harga_6'),
            'harga_7' => $request->session()->get('layout.layout_bus.harga_7'),
            'harga_8' => $request->session()->get('layout.layout_bus.harga_8'),
            'harga_9' => $request->session()->get('layout.layout_bus.harga_9'),
            'harga_10' => $request->session()->get('layout.layout_bus.harga_10'),
            'harga_11' => $request->session()->get('layout.layout_bus.harga_11'),
            'harga_12' => $request->session()->get('layout.layout_bus.harga_12'),
            'harga_13' => $request->session()->get('layout.layout_bus.harga_13'),
            'harga_14' => $request->session()->get('layout.layout_bus.harga_14'),
            'harga_15' => $request->session()->get('layout.layout_bus.harga_15'),
            'harga_16' => $request->session()->get('layout.layout_bus.harga_16'),
            'harga_17' => $request->session()->get('layout.layout_bus.harga_17'),
            'harga_18' => $request->session()->get('layout.layout_bus.harga_18'),
            'harga_19' => $request->session()->get('layout.layout_bus.harga_19'),
            'harga_20' => $request->session()->get('layout.layout_bus.harga_20'),
            'harga_21' => $request->session()->get('layout.layout_bus.harga_21'),
            'harga_22' => $request->session()->get('layout.layout_bus.harga_22'),
            'harga_23' => $request->session()->get('layout.layout_bus.harga_23'),
            'harga_24' => $request->session()->get('layout.layout_bus.harga_24'),
            'harga_25' => $request->session()->get('layout.layout_bus.harga_25'),
            'harga_26' => $request->session()->get('layout.layout_bus.harga_26'),
            'harga_27' => $request->session()->get('layout.layout_bus.harga_27'),
            'harga_28' => $request->session()->get('layout.layout_bus.harga_28'),
            'harga_29' => $request->session()->get('layout.layout_bus.harga_29'),
            'harga_30' => $request->session()->get('layout.layout_bus.harga_30'),
            'harga_31' => $request->session()->get('layout.layout_bus.harga_31'),
            'harga_32' => $request->session()->get('layout.layout_bus.harga_32'),
            'harga_33' => $request->session()->get('layout.layout_bus.harga_33'),
            'harga_34' => $request->session()->get('layout.layout_bus.harga_34'),
            'harga_35' => $request->session()->get('layout.layout_bus.harga_35'),
            'harga_36' => $request->session()->get('layout.layout_bus.harga_36'),
            'harga_37' => $request->session()->get('layout.layout_bus.harga_37'),
            'harga_38' => $request->session()->get('layout.layout_bus.harga_38'),
            'harga_39' => $request->session()->get('layout.layout_bus.harga_39'),
            'harga_40' => $request->session()->get('layout.layout_bus.harga_40'),
            'harga_41' => $request->session()->get('layout.layout_bus.harga_41'),
            'harga_42' => $request->session()->get('layout.layout_bus.harga_42'),
            'harga_43' => $request->session()->get('layout.layout_bus.harga_43'),
            'harga_44' => $request->session()->get('layout.layout_bus.harga_44'),
            'harga_45' => $request->session()->get('layout.layout_bus.harga_45'),
            'harga_46' => $request->session()->get('layout.layout_bus.harga_46'),
            'harga_47' => $request->session()->get('layout.layout_bus.harga_47'),
            'harga_48' => $request->session()->get('layout.layout_bus.harga_48'),
            'harga_49' => $request->session()->get('layout.layout_bus.harga_49'),
            'harga_50' => $request->session()->get('layout.layout_bus.harga_50'),
            'harga_51' => $request->session()->get('layout.layout_bus.harga_51'),
            'harga_52' => $request->session()->get('layout.layout_bus.harga_52'),
            'harga_53' => $request->session()->get('layout.layout_bus.harga_53'),
            'harga_54' => $request->session()->get('layout.layout_bus.harga_54'),
            'harga_55' => $request->session()->get('layout.layout_bus.harga_55'),
            'harga_56' => $request->session()->get('layout.layout_bus.harga_56'),
            'harga_57' => $request->session()->get('layout.layout_bus.harga_57'),
            'harga_58' => $request->session()->get('layout.layout_bus.harga_58'),
            'harga_59' => $request->session()->get('layout.layout_bus.harga_59')
        ]);

        $request->session()->forget('layout');

        return redirect()->route('masterBus');
    }

    public function deleteLayoutBus($id)
    {
        $layout_bus = LayoutBus::where('id_layout', $id)->first();

        $layout_bus::where('id_layout', $id)->delete();

        return redirect()->route('masterBus');
    }

    public function addLayoutBus(Request $request)
    {
        // dd($request->all());
        $user_id = Auth::user()->id;

        $request->session()->put('layout.layout_bus.jumlah_kursi', $request->jumlah_kursi);

        $request->session()->put('layout.layout_bus.layout_kursi', $request->layout_kursi);
        $request->session()->put('layout.layout_bus.lantai_bus', $request->lantai_bus);

        $request->session()->put('layout.layout_bus.kursi_1', $request->kursi_1);
        $request->session()->put('layout.layout_bus.kursi_2', $request->kursi_2);
        $request->session()->put('layout.layout_bus.kursi_3', $request->kursi_3);
        $request->session()->put('layout.layout_bus.kursi_4', $request->kursi_4);
        $request->session()->put('layout.layout_bus.kursi_5', $request->kursi_5);
        $request->session()->put('layout.layout_bus.kursi_6', $request->kursi_6);
        $request->session()->put('layout.layout_bus.kursi_7', $request->kursi_7);
        $request->session()->put('layout.layout_bus.kursi_8', $request->kursi_8);
        $request->session()->put('layout.layout_bus.kursi_9', $request->kursi_9);
        $request->session()->put('layout.layout_bus.kursi_10', $request->kursi_10);
        $request->session()->put('layout.layout_bus.kursi_11', $request->kursi_11);
        $request->session()->put('layout.layout_bus.kursi_12', $request->kursi_12);
        $request->session()->put('layout.layout_bus.kursi_13', $request->kursi_13);
        $request->session()->put('layout.layout_bus.kursi_14', $request->kursi_14);
        $request->session()->put('layout.layout_bus.kursi_15', $request->kursi_15);
        $request->session()->put('layout.layout_bus.kursi_16', $request->kursi_16);
        $request->session()->put('layout.layout_bus.kursi_17', $request->kursi_17);
        $request->session()->put('layout.layout_bus.kursi_18', $request->kursi_18);
        $request->session()->put('layout.layout_bus.kursi_19', $request->kursi_19);
        $request->session()->put('layout.layout_bus.kursi_20', $request->kursi_20);
        $request->session()->put('layout.layout_bus.kursi_21', $request->kursi_21);
        $request->session()->put('layout.layout_bus.kursi_22', $request->kursi_22);
        $request->session()->put('layout.layout_bus.kursi_23', $request->kursi_23);
        $request->session()->put('layout.layout_bus.kursi_24', $request->kursi_24);
        $request->session()->put('layout.layout_bus.kursi_25', $request->kursi_25);
        $request->session()->put('layout.layout_bus.kursi_26', $request->kursi_26);
        $request->session()->put('layout.layout_bus.kursi_27', $request->kursi_27);
        $request->session()->put('layout.layout_bus.kursi_28', $request->kursi_28);
        $request->session()->put('layout.layout_bus.kursi_29', $request->kursi_29);
        $request->session()->put('layout.layout_bus.kursi_30', $request->kursi_30);
        $request->session()->put('layout.layout_bus.kursi_31', $request->kursi_31);
        $request->session()->put('layout.layout_bus.kursi_32', $request->kursi_32);
        $request->session()->put('layout.layout_bus.kursi_33', $request->kursi_33);
        $request->session()->put('layout.layout_bus.kursi_34', $request->kursi_34);
        $request->session()->put('layout.layout_bus.kursi_35', $request->kursi_35);
        $request->session()->put('layout.layout_bus.kursi_36', $request->kursi_36);
        $request->session()->put('layout.layout_bus.kursi_37', $request->kursi_37);
        $request->session()->put('layout.layout_bus.kursi_38', $request->kursi_38);
        $request->session()->put('layout.layout_bus.kursi_39', $request->kursi_39);
        $request->session()->put('layout.layout_bus.kursi_40', $request->kursi_40);
        $request->session()->put('layout.layout_bus.kursi_41', $request->kursi_41);
        $request->session()->put('layout.layout_bus.kursi_42', $request->kursi_42);
        $request->session()->put('layout.layout_bus.kursi_43', $request->kursi_43);
        $request->session()->put('layout.layout_bus.kursi_44', $request->kursi_44);
        $request->session()->put('layout.layout_bus.kursi_45', $request->kursi_45);
        $request->session()->put('layout.layout_bus.kursi_46', $request->kursi_46);
        $request->session()->put('layout.layout_bus.kursi_47', $request->kursi_47);
        $request->session()->put('layout.layout_bus.kursi_48', $request->kursi_48);
        $request->session()->put('layout.layout_bus.kursi_49', $request->kursi_49);
        $request->session()->put('layout.layout_bus.kursi_50', $request->kursi_50);
        $request->session()->put('layout.layout_bus.kursi_51', $request->kursi_51);
        $request->session()->put('layout.layout_bus.kursi_52', $request->kursi_52);
        $request->session()->put('layout.layout_bus.kursi_53', $request->kursi_53);
        $request->session()->put('layout.layout_bus.kursi_54', $request->kursi_54);
        $request->session()->put('layout.layout_bus.kursi_55', $request->kursi_55);
        $request->session()->put('layout.layout_bus.kursi_56', $request->kursi_56);
        $request->session()->put('layout.layout_bus.kursi_57', $request->kursi_57);
        $request->session()->put('layout.layout_bus.kursi_58', $request->kursi_58);
        $request->session()->put('layout.layout_bus.kursi_59', $request->kursi_59);

        $request->session()->put('layout.layout_bus.harga_1', $request->harga_1);
        $request->session()->put('layout.layout_bus.harga_2', $request->harga_2);
        $request->session()->put('layout.layout_bus.harga_3', $request->harga_3);
        $request->session()->put('layout.layout_bus.harga_4', $request->harga_4);
        $request->session()->put('layout.layout_bus.harga_5', $request->harga_5);
        $request->session()->put('layout.layout_bus.harga_6', $request->harga_6);
        $request->session()->put('layout.layout_bus.harga_7', $request->harga_7);
        $request->session()->put('layout.layout_bus.harga_8', $request->harga_8);
        $request->session()->put('layout.layout_bus.harga_9', $request->harga_9);
        $request->session()->put('layout.layout_bus.harga_10', $request->harga_10);
        $request->session()->put('layout.layout_bus.harga_11', $request->harga_11);
        $request->session()->put('layout.layout_bus.harga_12', $request->harga_12);
        $request->session()->put('layout.layout_bus.harga_13', $request->harga_13);
        $request->session()->put('layout.layout_bus.harga_14', $request->harga_14);
        $request->session()->put('layout.layout_bus.harga_15', $request->harga_15);
        $request->session()->put('layout.layout_bus.harga_16', $request->harga_16);
        $request->session()->put('layout.layout_bus.harga_17', $request->harga_17);
        $request->session()->put('layout.layout_bus.harga_18', $request->harga_18);
        $request->session()->put('layout.layout_bus.harga_19', $request->harga_19);
        $request->session()->put('layout.layout_bus.harga_20', $request->harga_20);
        $request->session()->put('layout.layout_bus.harga_21', $request->harga_21);
        $request->session()->put('layout.layout_bus.harga_22', $request->harga_22);
        $request->session()->put('layout.layout_bus.harga_23', $request->harga_23);
        $request->session()->put('layout.layout_bus.harga_24', $request->harga_24);
        $request->session()->put('layout.layout_bus.harga_25', $request->harga_25);
        $request->session()->put('layout.layout_bus.harga_26', $request->harga_26);
        $request->session()->put('layout.layout_bus.harga_27', $request->harga_27);
        $request->session()->put('layout.layout_bus.harga_28', $request->harga_28);
        $request->session()->put('layout.layout_bus.harga_29', $request->harga_29);
        $request->session()->put('layout.layout_bus.harga_30', $request->harga_30);
        $request->session()->put('layout.layout_bus.harga_31', $request->harga_31);
        $request->session()->put('layout.layout_bus.harga_32', $request->harga_32);
        $request->session()->put('layout.layout_bus.harga_33', $request->harga_33);
        $request->session()->put('layout.layout_bus.harga_34', $request->harga_34);
        $request->session()->put('layout.layout_bus.harga_35', $request->harga_35);
        $request->session()->put('layout.layout_bus.harga_36', $request->harga_36);
        $request->session()->put('layout.layout_bus.harga_37', $request->harga_37);
        $request->session()->put('layout.layout_bus.harga_38', $request->harga_38);
        $request->session()->put('layout.layout_bus.harga_39', $request->harga_39);
        $request->session()->put('layout.layout_bus.harga_40', $request->harga_40);
        $request->session()->put('layout.layout_bus.harga_41', $request->harga_41);
        $request->session()->put('layout.layout_bus.harga_42', $request->harga_42);
        $request->session()->put('layout.layout_bus.harga_43', $request->harga_43);
        $request->session()->put('layout.layout_bus.harga_44', $request->harga_44);
        $request->session()->put('layout.layout_bus.harga_45', $request->harga_45);
        $request->session()->put('layout.layout_bus.harga_46', $request->harga_46);
        $request->session()->put('layout.layout_bus.harga_47', $request->harga_47);
        $request->session()->put('layout.layout_bus.harga_48', $request->harga_48);
        $request->session()->put('layout.layout_bus.harga_49', $request->harga_49);
        $request->session()->put('layout.layout_bus.harga_50', $request->harga_50);
        $request->session()->put('layout.layout_bus.harga_51', $request->harga_51);
        $request->session()->put('layout.layout_bus.harga_52', $request->harga_52);
        $request->session()->put('layout.layout_bus.harga_53', $request->harga_53);
        $request->session()->put('layout.layout_bus.harga_54', $request->harga_54);
        $request->session()->put('layout.layout_bus.harga_55', $request->harga_55);
        $request->session()->put('layout.layout_bus.harga_56', $request->harga_56);
        $request->session()->put('layout.layout_bus.harga_57', $request->harga_57);
        $request->session()->put('layout.layout_bus.harga_58', $request->harga_58);
        $request->session()->put('layout.layout_bus.harga_59', $request->harga_59);

        $layoutbus = LayoutBus::create([
            'user_id' => $user_id,
            'jumlah_kursi' => $request->session()->get('layout.layout_bus.jumlah_kursi'),
            'layout_kursi' => $request->session()->get('layout.layout_bus.layout_kursi'),
            'lantai_bus' => $request->session()->get('layout.layout_bus.lantai_bus'),
            'kursi_1' => $request->session()->get('layout.layout_bus.kursi_1'),
            'kursi_2' => $request->session()->get('layout.layout_bus.kursi_2'),
            'kursi_3' => $request->session()->get('layout.layout_bus.kursi_3'),
            'kursi_4' => $request->session()->get('layout.layout_bus.kursi_4'),
            'kursi_5' => $request->session()->get('layout.layout_bus.kursi_5'),
            'kursi_6' => $request->session()->get('layout.layout_bus.kursi_6'),
            'kursi_7' => $request->session()->get('layout.layout_bus.kursi_7'),
            'kursi_8' => $request->session()->get('layout.layout_bus.kursi_8'),
            'kursi_9' => $request->session()->get('layout.layout_bus.kursi_9'),
            'kursi_10' => $request->session()->get('layout.layout_bus.kursi_10'),
            'kursi_11' => $request->session()->get('layout.layout_bus.kursi_11'),
            'kursi_12' => $request->session()->get('layout.layout_bus.kursi_12'),
            'kursi_13' => $request->session()->get('layout.layout_bus.kursi_13'),
            'kursi_14' => $request->session()->get('layout.layout_bus.kursi_14'),
            'kursi_15' => $request->session()->get('layout.layout_bus.kursi_15'),
            'kursi_16' => $request->session()->get('layout.layout_bus.kursi_16'),
            'kursi_17' => $request->session()->get('layout.layout_bus.kursi_17'),
            'kursi_18' => $request->session()->get('layout.layout_bus.kursi_18'),
            'kursi_19' => $request->session()->get('layout.layout_bus.kursi_19'),
            'kursi_20' => $request->session()->get('layout.layout_bus.kursi_20'),
            'kursi_21' => $request->session()->get('layout.layout_bus.kursi_21'),
            'kursi_22' => $request->session()->get('layout.layout_bus.kursi_22'),
            'kursi_23' => $request->session()->get('layout.layout_bus.kursi_23'),
            'kursi_24' => $request->session()->get('layout.layout_bus.kursi_24'),
            'kursi_25' => $request->session()->get('layout.layout_bus.kursi_25'),
            'kursi_26' => $request->session()->get('layout.layout_bus.kursi_26'),
            'kursi_27' => $request->session()->get('layout.layout_bus.kursi_27'),
            'kursi_28' => $request->session()->get('layout.layout_bus.kursi_28'),
            'kursi_29' => $request->session()->get('layout.layout_bus.kursi_29'),
            'kursi_30' => $request->session()->get('layout.layout_bus.kursi_30'),
            'kursi_31' => $request->session()->get('layout.layout_bus.kursi_31'),
            'kursi_32' => $request->session()->get('layout.layout_bus.kursi_32'),
            'kursi_33' => $request->session()->get('layout.layout_bus.kursi_33'),
            'kursi_34' => $request->session()->get('layout.layout_bus.kursi_34'),
            'kursi_35' => $request->session()->get('layout.layout_bus.kursi_35'),
            'kursi_36' => $request->session()->get('layout.layout_bus.kursi_36'),
            'kursi_37' => $request->session()->get('layout.layout_bus.kursi_37'),
            'kursi_38' => $request->session()->get('layout.layout_bus.kursi_38'),
            'kursi_39' => $request->session()->get('layout.layout_bus.kursi_39'),
            'kursi_40' => $request->session()->get('layout.layout_bus.kursi_40'),
            'kursi_41' => $request->session()->get('layout.layout_bus.kursi_41'),
            'kursi_42' => $request->session()->get('layout.layout_bus.kursi_42'),
            'kursi_43' => $request->session()->get('layout.layout_bus.kursi_43'),
            'kursi_44' => $request->session()->get('layout.layout_bus.kursi_44'),
            'kursi_45' => $request->session()->get('layout.layout_bus.kursi_45'),
            'kursi_46' => $request->session()->get('layout.layout_bus.kursi_46'),
            'kursi_47' => $request->session()->get('layout.layout_bus.kursi_47'),
            'kursi_48' => $request->session()->get('layout.layout_bus.kursi_48'),
            'kursi_49' => $request->session()->get('layout.layout_bus.kursi_49'),
            'kursi_50' => $request->session()->get('layout.layout_bus.kursi_50'),
            'kursi_51' => $request->session()->get('layout.layout_bus.kursi_51'),
            'kursi_52' => $request->session()->get('layout.layout_bus.kursi_52'),
            'kursi_53' => $request->session()->get('layout.layout_bus.kursi_53'),
            'kursi_54' => $request->session()->get('layout.layout_bus.kursi_54'),
            'kursi_55' => $request->session()->get('layout.layout_bus.kursi_55'),
            'kursi_56' => $request->session()->get('layout.layout_bus.kursi_56'),
            'kursi_57' => $request->session()->get('layout.layout_bus.kursi_57'),
            'kursi_58' => $request->session()->get('layout.layout_bus.kursi_58'),
            'kursi_59' => $request->session()->get('layout.layout_bus.kursi_59'),

            'harga_1' => $request->session()->get('layout.layout_bus.harga_1'),
            'harga_2' => $request->session()->get('layout.layout_bus.harga_2'),
            'harga_3' => $request->session()->get('layout.layout_bus.harga_3'),
            'harga_4' => $request->session()->get('layout.layout_bus.harga_4'),
            'harga_5' => $request->session()->get('layout.layout_bus.harga_5'),
            'harga_6' => $request->session()->get('layout.layout_bus.harga_6'),
            'harga_7' => $request->session()->get('layout.layout_bus.harga_7'),
            'harga_8' => $request->session()->get('layout.layout_bus.harga_8'),
            'harga_9' => $request->session()->get('layout.layout_bus.harga_9'),
            'harga_10' => $request->session()->get('layout.layout_bus.harga_10'),
            'harga_11' => $request->session()->get('layout.layout_bus.harga_11'),
            'harga_12' => $request->session()->get('layout.layout_bus.harga_12'),
            'harga_13' => $request->session()->get('layout.layout_bus.harga_13'),
            'harga_14' => $request->session()->get('layout.layout_bus.harga_14'),
            'harga_15' => $request->session()->get('layout.layout_bus.harga_15'),
            'harga_16' => $request->session()->get('layout.layout_bus.harga_16'),
            'harga_17' => $request->session()->get('layout.layout_bus.harga_17'),
            'harga_18' => $request->session()->get('layout.layout_bus.harga_18'),
            'harga_19' => $request->session()->get('layout.layout_bus.harga_19'),
            'harga_20' => $request->session()->get('layout.layout_bus.harga_20'),
            'harga_21' => $request->session()->get('layout.layout_bus.harga_21'),
            'harga_22' => $request->session()->get('layout.layout_bus.harga_22'),
            'harga_23' => $request->session()->get('layout.layout_bus.harga_23'),
            'harga_24' => $request->session()->get('layout.layout_bus.harga_24'),
            'harga_25' => $request->session()->get('layout.layout_bus.harga_25'),
            'harga_26' => $request->session()->get('layout.layout_bus.harga_26'),
            'harga_27' => $request->session()->get('layout.layout_bus.harga_27'),
            'harga_28' => $request->session()->get('layout.layout_bus.harga_28'),
            'harga_29' => $request->session()->get('layout.layout_bus.harga_29'),
            'harga_30' => $request->session()->get('layout.layout_bus.harga_30'),
            'harga_31' => $request->session()->get('layout.layout_bus.harga_31'),
            'harga_32' => $request->session()->get('layout.layout_bus.harga_32'),
            'harga_33' => $request->session()->get('layout.layout_bus.harga_33'),
            'harga_34' => $request->session()->get('layout.layout_bus.harga_34'),
            'harga_35' => $request->session()->get('layout.layout_bus.harga_35'),
            'harga_36' => $request->session()->get('layout.layout_bus.harga_36'),
            'harga_37' => $request->session()->get('layout.layout_bus.harga_37'),
            'harga_38' => $request->session()->get('layout.layout_bus.harga_38'),
            'harga_39' => $request->session()->get('layout.layout_bus.harga_39'),
            'harga_40' => $request->session()->get('layout.layout_bus.harga_40'),
            'harga_41' => $request->session()->get('layout.layout_bus.harga_41'),
            'harga_42' => $request->session()->get('layout.layout_bus.harga_42'),
            'harga_43' => $request->session()->get('layout.layout_bus.harga_43'),
            'harga_44' => $request->session()->get('layout.layout_bus.harga_44'),
            'harga_45' => $request->session()->get('layout.layout_bus.harga_45'),
            'harga_46' => $request->session()->get('layout.layout_bus.harga_46'),
            'harga_47' => $request->session()->get('layout.layout_bus.harga_47'),
            'harga_48' => $request->session()->get('layout.layout_bus.harga_48'),
            'harga_49' => $request->session()->get('layout.layout_bus.harga_49'),
            'harga_50' => $request->session()->get('layout.layout_bus.harga_50'),
            'harga_51' => $request->session()->get('layout.layout_bus.harga_51'),
            'harga_52' => $request->session()->get('layout.layout_bus.harga_52'),
            'harga_53' => $request->session()->get('layout.layout_bus.harga_53'),
            'harga_54' => $request->session()->get('layout.layout_bus.harga_54'),
            'harga_55' => $request->session()->get('layout.layout_bus.harga_55'),
            'harga_56' => $request->session()->get('layout.layout_bus.harga_56'),
            'harga_57' => $request->session()->get('layout.layout_bus.harga_57'),
            'harga_58' => $request->session()->get('layout.layout_bus.harga_58'),
            'harga_59' => $request->session()->get('layout.layout_bus.harga_59')
        ]);

        $request->session()->forget('layout');

        return redirect()->route('masterBus');
    }

    public function addLayoutBusM39(Request $request)
    {
        $user_id = Auth::user()->id;

        $request->session()->put('layout.layout_bus.jumlah_kursi', $request->jumlah_kursi);

        $request->session()->put('layout.layout_bus.layout_kursi', $request->layout_kursi);
        $request->session()->put('layout.layout_bus.lantai_bus', $request->lantai_bus);

        $request->session()->put('layout.layout_bus.kursi_1', $request->kursi_1);
        $request->session()->put('layout.layout_bus.kursi_2', $request->kursi_2);
        $request->session()->put('layout.layout_bus.kursi_3', $request->kursi_3);
        $request->session()->put('layout.layout_bus.kursi_4', $request->kursi_4);
        $request->session()->put('layout.layout_bus.kursi_5', $request->kursi_5);
        $request->session()->put('layout.layout_bus.kursi_6', $request->kursi_6);
        $request->session()->put('layout.layout_bus.kursi_7', $request->kursi_7);
        $request->session()->put('layout.layout_bus.kursi_8', $request->kursi_8);
        $request->session()->put('layout.layout_bus.kursi_9', $request->kursi_9);
        $request->session()->put('layout.layout_bus.kursi_10', $request->kursi_10);
        $request->session()->put('layout.layout_bus.kursi_11', $request->kursi_11);
        $request->session()->put('layout.layout_bus.kursi_12', $request->kursi_12);
        $request->session()->put('layout.layout_bus.kursi_13', $request->kursi_13);
        $request->session()->put('layout.layout_bus.kursi_14', $request->kursi_14);
        $request->session()->put('layout.layout_bus.kursi_15', $request->kursi_15);
        $request->session()->put('layout.layout_bus.kursi_16', $request->kursi_16);
        $request->session()->put('layout.layout_bus.kursi_17', $request->kursi_17);
        $request->session()->put('layout.layout_bus.kursi_18', $request->kursi_18);
        $request->session()->put('layout.layout_bus.kursi_19', $request->kursi_19);
        $request->session()->put('layout.layout_bus.kursi_20', $request->kursi_20);
        $request->session()->put('layout.layout_bus.kursi_21', $request->kursi_21);
        $request->session()->put('layout.layout_bus.kursi_22', $request->kursi_22);
        $request->session()->put('layout.layout_bus.kursi_23', $request->kursi_23);
        $request->session()->put('layout.layout_bus.kursi_24', $request->kursi_24);
        $request->session()->put('layout.layout_bus.kursi_25', $request->kursi_25);
        $request->session()->put('layout.layout_bus.kursi_26', $request->kursi_26);
        $request->session()->put('layout.layout_bus.kursi_27', $request->kursi_27);
        $request->session()->put('layout.layout_bus.kursi_28', $request->kursi_28);
        $request->session()->put('layout.layout_bus.kursi_29', $request->kursi_29);
        $request->session()->put('layout.layout_bus.kursi_30', $request->kursi_30);
        $request->session()->put('layout.layout_bus.kursi_31', $request->kursi_31);
        $request->session()->put('layout.layout_bus.kursi_32', $request->kursi_32);
        $request->session()->put('layout.layout_bus.kursi_33', $request->kursi_33);
        $request->session()->put('layout.layout_bus.kursi_34', $request->kursi_34);
        $request->session()->put('layout.layout_bus.kursi_35', $request->kursi_35);
        $request->session()->put('layout.layout_bus.kursi_36', $request->kursi_36);
        $request->session()->put('layout.layout_bus.kursi_37', $request->kursi_37);
        $request->session()->put('layout.layout_bus.kursi_38', $request->kursi_38);
        $request->session()->put('layout.layout_bus.kursi_39', $request->kursi_39);
        $request->session()->put('layout.layout_bus.kursi_40', $request->kursi_40);
        $request->session()->put('layout.layout_bus.kursi_41', $request->kursi_41);
        $request->session()->put('layout.layout_bus.kursi_42', $request->kursi_42);
        $request->session()->put('layout.layout_bus.kursi_43', $request->kursi_43);
        $request->session()->put('layout.layout_bus.kursi_44', $request->kursi_44);
        $request->session()->put('layout.layout_bus.kursi_45', $request->kursi_45);
        $request->session()->put('layout.layout_bus.kursi_46', $request->kursi_46);
        $request->session()->put('layout.layout_bus.kursi_47', $request->kursi_47);
        $request->session()->put('layout.layout_bus.kursi_48', $request->kursi_48);
        $request->session()->put('layout.layout_bus.kursi_49', $request->kursi_49);
        $request->session()->put('layout.layout_bus.kursi_50', $request->kursi_50);
        $request->session()->put('layout.layout_bus.kursi_51', $request->kursi_51);
        $request->session()->put('layout.layout_bus.kursi_52', $request->kursi_52);
        $request->session()->put('layout.layout_bus.kursi_53', $request->kursi_53);
        $request->session()->put('layout.layout_bus.kursi_54', $request->kursi_54);
        $request->session()->put('layout.layout_bus.kursi_55', $request->kursi_55);
        $request->session()->put('layout.layout_bus.kursi_56', $request->kursi_56);
        $request->session()->put('layout.layout_bus.kursi_57', $request->kursi_57);
        $request->session()->put('layout.layout_bus.kursi_58', $request->kursi_58);
        $request->session()->put('layout.layout_bus.kursi_59', $request->kursi_59);

        $request->session()->put('layout.layout_bus.harga_1', $request->harga_1);
        $request->session()->put('layout.layout_bus.harga_2', $request->harga_2);
        $request->session()->put('layout.layout_bus.harga_3', $request->harga_3);
        $request->session()->put('layout.layout_bus.harga_4', $request->harga_4);
        $request->session()->put('layout.layout_bus.harga_5', $request->harga_5);
        $request->session()->put('layout.layout_bus.harga_6', $request->harga_6);
        $request->session()->put('layout.layout_bus.harga_7', $request->harga_7);
        $request->session()->put('layout.layout_bus.harga_8', $request->harga_8);
        $request->session()->put('layout.layout_bus.harga_9', $request->harga_9);
        $request->session()->put('layout.layout_bus.harga_10', $request->harga_10);
        $request->session()->put('layout.layout_bus.harga_11', $request->harga_11);
        $request->session()->put('layout.layout_bus.harga_12', $request->harga_12);
        $request->session()->put('layout.layout_bus.harga_13', $request->harga_13);
        $request->session()->put('layout.layout_bus.harga_14', $request->harga_14);
        $request->session()->put('layout.layout_bus.harga_15', $request->harga_15);
        $request->session()->put('layout.layout_bus.harga_16', $request->harga_16);
        $request->session()->put('layout.layout_bus.harga_17', $request->harga_17);
        $request->session()->put('layout.layout_bus.harga_18', $request->harga_18);
        $request->session()->put('layout.layout_bus.harga_19', $request->harga_19);
        $request->session()->put('layout.layout_bus.harga_20', $request->harga_20);
        $request->session()->put('layout.layout_bus.harga_21', $request->harga_21);
        $request->session()->put('layout.layout_bus.harga_22', $request->harga_22);
        $request->session()->put('layout.layout_bus.harga_23', $request->harga_23);
        $request->session()->put('layout.layout_bus.harga_24', $request->harga_24);
        $request->session()->put('layout.layout_bus.harga_25', $request->harga_25);
        $request->session()->put('layout.layout_bus.harga_26', $request->harga_26);
        $request->session()->put('layout.layout_bus.harga_27', $request->harga_27);
        $request->session()->put('layout.layout_bus.harga_28', $request->harga_28);
        $request->session()->put('layout.layout_bus.harga_29', $request->harga_29);
        $request->session()->put('layout.layout_bus.harga_30', $request->harga_30);
        $request->session()->put('layout.layout_bus.harga_31', $request->harga_31);
        $request->session()->put('layout.layout_bus.harga_32', $request->harga_32);
        $request->session()->put('layout.layout_bus.harga_33', $request->harga_33);
        $request->session()->put('layout.layout_bus.harga_34', $request->harga_34);
        $request->session()->put('layout.layout_bus.harga_35', $request->harga_35);
        $request->session()->put('layout.layout_bus.harga_36', $request->harga_36);
        $request->session()->put('layout.layout_bus.harga_37', $request->harga_37);
        $request->session()->put('layout.layout_bus.harga_38', $request->harga_38);
        $request->session()->put('layout.layout_bus.harga_39', $request->harga_39);
        $request->session()->put('layout.layout_bus.harga_40', $request->harga_40);
        $request->session()->put('layout.layout_bus.harga_41', $request->harga_41);
        $request->session()->put('layout.layout_bus.harga_42', $request->harga_42);
        $request->session()->put('layout.layout_bus.harga_43', $request->harga_43);
        $request->session()->put('layout.layout_bus.harga_44', $request->harga_44);
        $request->session()->put('layout.layout_bus.harga_45', $request->harga_45);
        $request->session()->put('layout.layout_bus.harga_46', $request->harga_46);
        $request->session()->put('layout.layout_bus.harga_47', $request->harga_47);
        $request->session()->put('layout.layout_bus.harga_48', $request->harga_48);
        $request->session()->put('layout.layout_bus.harga_49', $request->harga_49);
        $request->session()->put('layout.layout_bus.harga_50', $request->harga_50);
        $request->session()->put('layout.layout_bus.harga_51', $request->harga_51);
        $request->session()->put('layout.layout_bus.harga_52', $request->harga_52);
        $request->session()->put('layout.layout_bus.harga_53', $request->harga_53);
        $request->session()->put('layout.layout_bus.harga_54', $request->harga_54);
        $request->session()->put('layout.layout_bus.harga_55', $request->harga_55);
        $request->session()->put('layout.layout_bus.harga_56', $request->harga_56);
        $request->session()->put('layout.layout_bus.harga_57', $request->harga_57);
        $request->session()->put('layout.layout_bus.harga_58', $request->harga_58);
        $request->session()->put('layout.layout_bus.harga_59', $request->harga_59);

        $layoutbus = LayoutBus::create([
            'user_id' => $user_id,
            'jumlah_kursi' => $request->session()->get('layout.layout_bus.jumlah_kursi'),
            'layout_kursi' => $request->session()->get('layout.layout_bus.layout_kursi'),
            'lantai_bus' => $request->session()->get('layout.layout_bus.lantai_bus'),
            'kursi_1' => $request->session()->get('layout.layout_bus.kursi_1'),
            'kursi_2' => $request->session()->get('layout.layout_bus.kursi_2'),
            'kursi_3' => $request->session()->get('layout.layout_bus.kursi_3'),
            'kursi_4' => $request->session()->get('layout.layout_bus.kursi_4'),
            'kursi_5' => $request->session()->get('layout.layout_bus.kursi_5'),
            'kursi_6' => $request->session()->get('layout.layout_bus.kursi_6'),
            'kursi_7' => $request->session()->get('layout.layout_bus.kursi_7'),
            'kursi_8' => $request->session()->get('layout.layout_bus.kursi_8'),
            'kursi_9' => $request->session()->get('layout.layout_bus.kursi_9'),
            'kursi_10' => $request->session()->get('layout.layout_bus.kursi_10'),
            'kursi_11' => $request->session()->get('layout.layout_bus.kursi_11'),
            'kursi_12' => $request->session()->get('layout.layout_bus.kursi_12'),
            'kursi_13' => $request->session()->get('layout.layout_bus.kursi_13'),
            'kursi_14' => $request->session()->get('layout.layout_bus.kursi_14'),
            'kursi_15' => $request->session()->get('layout.layout_bus.kursi_15'),
            'kursi_16' => $request->session()->get('layout.layout_bus.kursi_16'),
            'kursi_17' => $request->session()->get('layout.layout_bus.kursi_17'),
            'kursi_18' => $request->session()->get('layout.layout_bus.kursi_18'),
            'kursi_19' => $request->session()->get('layout.layout_bus.kursi_19'),
            'kursi_20' => $request->session()->get('layout.layout_bus.kursi_20'),
            'kursi_21' => $request->session()->get('layout.layout_bus.kursi_21'),
            'kursi_22' => $request->session()->get('layout.layout_bus.kursi_22'),
            'kursi_23' => $request->session()->get('layout.layout_bus.kursi_23'),
            'kursi_24' => $request->session()->get('layout.layout_bus.kursi_24'),
            'kursi_25' => $request->session()->get('layout.layout_bus.kursi_25'),
            'kursi_26' => $request->session()->get('layout.layout_bus.kursi_26'),
            'kursi_27' => $request->session()->get('layout.layout_bus.kursi_27'),
            'kursi_28' => $request->session()->get('layout.layout_bus.kursi_28'),
            'kursi_29' => $request->session()->get('layout.layout_bus.kursi_29'),
            'kursi_30' => $request->session()->get('layout.layout_bus.kursi_30'),
            'kursi_31' => $request->session()->get('layout.layout_bus.kursi_31'),
            'kursi_32' => $request->session()->get('layout.layout_bus.kursi_32'),
            'kursi_33' => $request->session()->get('layout.layout_bus.kursi_33'),
            'kursi_34' => $request->session()->get('layout.layout_bus.kursi_34'),
            'kursi_35' => $request->session()->get('layout.layout_bus.kursi_35'),
            'kursi_36' => $request->session()->get('layout.layout_bus.kursi_36'),
            'kursi_37' => $request->session()->get('layout.layout_bus.kursi_37'),
            'kursi_38' => $request->session()->get('layout.layout_bus.kursi_38'),
            'kursi_39' => $request->session()->get('layout.layout_bus.kursi_39'),
            'kursi_40' => $request->session()->get('layout.layout_bus.kursi_40'),
            'kursi_41' => $request->session()->get('layout.layout_bus.kursi_41'),
            'kursi_42' => $request->session()->get('layout.layout_bus.kursi_42'),
            'kursi_43' => $request->session()->get('layout.layout_bus.kursi_43'),
            'kursi_44' => $request->session()->get('layout.layout_bus.kursi_44'),
            'kursi_45' => $request->session()->get('layout.layout_bus.kursi_45'),
            'kursi_46' => $request->session()->get('layout.layout_bus.kursi_46'),
            'kursi_47' => $request->session()->get('layout.layout_bus.kursi_47'),
            'kursi_48' => $request->session()->get('layout.layout_bus.kursi_48'),
            'kursi_49' => $request->session()->get('layout.layout_bus.kursi_49'),
            'kursi_50' => $request->session()->get('layout.layout_bus.kursi_50'),
            'kursi_51' => $request->session()->get('layout.layout_bus.kursi_51'),
            'kursi_52' => $request->session()->get('layout.layout_bus.kursi_52'),
            'kursi_53' => $request->session()->get('layout.layout_bus.kursi_53'),
            'kursi_54' => $request->session()->get('layout.layout_bus.kursi_54'),
            'kursi_55' => $request->session()->get('layout.layout_bus.kursi_55'),
            'kursi_56' => $request->session()->get('layout.layout_bus.kursi_56'),
            'kursi_57' => $request->session()->get('layout.layout_bus.kursi_57'),
            'kursi_58' => $request->session()->get('layout.layout_bus.kursi_58'),
            'kursi_59' => $request->session()->get('layout.layout_bus.kursi_59'),

            'harga_1' => $request->session()->get('layout.layout_bus.harga_1'),
            'harga_2' => $request->session()->get('layout.layout_bus.harga_2'),
            'harga_3' => $request->session()->get('layout.layout_bus.harga_3'),
            'harga_4' => $request->session()->get('layout.layout_bus.harga_4'),
            'harga_5' => $request->session()->get('layout.layout_bus.harga_5'),
            'harga_6' => $request->session()->get('layout.layout_bus.harga_6'),
            'harga_7' => $request->session()->get('layout.layout_bus.harga_7'),
            'harga_8' => $request->session()->get('layout.layout_bus.harga_8'),
            'harga_9' => $request->session()->get('layout.layout_bus.harga_9'),
            'harga_10' => $request->session()->get('layout.layout_bus.harga_10'),
            'harga_11' => $request->session()->get('layout.layout_bus.harga_11'),
            'harga_12' => $request->session()->get('layout.layout_bus.harga_12'),
            'harga_13' => $request->session()->get('layout.layout_bus.harga_13'),
            'harga_14' => $request->session()->get('layout.layout_bus.harga_14'),
            'harga_15' => $request->session()->get('layout.layout_bus.harga_15'),
            'harga_16' => $request->session()->get('layout.layout_bus.harga_16'),
            'harga_17' => $request->session()->get('layout.layout_bus.harga_17'),
            'harga_18' => $request->session()->get('layout.layout_bus.harga_18'),
            'harga_19' => $request->session()->get('layout.layout_bus.harga_19'),
            'harga_20' => $request->session()->get('layout.layout_bus.harga_20'),
            'harga_21' => $request->session()->get('layout.layout_bus.harga_21'),
            'harga_22' => $request->session()->get('layout.layout_bus.harga_22'),
            'harga_23' => $request->session()->get('layout.layout_bus.harga_23'),
            'harga_24' => $request->session()->get('layout.layout_bus.harga_24'),
            'harga_25' => $request->session()->get('layout.layout_bus.harga_25'),
            'harga_26' => $request->session()->get('layout.layout_bus.harga_26'),
            'harga_27' => $request->session()->get('layout.layout_bus.harga_27'),
            'harga_28' => $request->session()->get('layout.layout_bus.harga_28'),
            'harga_29' => $request->session()->get('layout.layout_bus.harga_29'),
            'harga_30' => $request->session()->get('layout.layout_bus.harga_30'),
            'harga_31' => $request->session()->get('layout.layout_bus.harga_31'),
            'harga_32' => $request->session()->get('layout.layout_bus.harga_32'),
            'harga_33' => $request->session()->get('layout.layout_bus.harga_33'),
            'harga_34' => $request->session()->get('layout.layout_bus.harga_34'),
            'harga_35' => $request->session()->get('layout.layout_bus.harga_35'),
            'harga_36' => $request->session()->get('layout.layout_bus.harga_36'),
            'harga_37' => $request->session()->get('layout.layout_bus.harga_37'),
            'harga_38' => $request->session()->get('layout.layout_bus.harga_38'),
            'harga_39' => $request->session()->get('layout.layout_bus.harga_39'),
            'harga_40' => $request->session()->get('layout.layout_bus.harga_40'),
            'harga_41' => $request->session()->get('layout.layout_bus.harga_41'),
            'harga_42' => $request->session()->get('layout.layout_bus.harga_42'),
            'harga_43' => $request->session()->get('layout.layout_bus.harga_43'),
            'harga_44' => $request->session()->get('layout.layout_bus.harga_44'),
            'harga_45' => $request->session()->get('layout.layout_bus.harga_45'),
            'harga_46' => $request->session()->get('layout.layout_bus.harga_46'),
            'harga_47' => $request->session()->get('layout.layout_bus.harga_47'),
            'harga_48' => $request->session()->get('layout.layout_bus.harga_48'),
            'harga_49' => $request->session()->get('layout.layout_bus.harga_49'),
            'harga_50' => $request->session()->get('layout.layout_bus.harga_50'),
            'harga_51' => $request->session()->get('layout.layout_bus.harga_51'),
            'harga_52' => $request->session()->get('layout.layout_bus.harga_52'),
            'harga_53' => $request->session()->get('layout.layout_bus.harga_53'),
            'harga_54' => $request->session()->get('layout.layout_bus.harga_54'),
            'harga_55' => $request->session()->get('layout.layout_bus.harga_55'),
            'harga_56' => $request->session()->get('layout.layout_bus.harga_56'),
            'harga_57' => $request->session()->get('layout.layout_bus.harga_57'),
            'harga_58' => $request->session()->get('layout.layout_bus.harga_58'),
            'harga_59' => $request->session()->get('layout.layout_bus.harga_59')
        ]);

        $request->session()->forget('layout');

        return redirect()->route('masterBus');
    }

    public function editBus($id)
    {
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $data = DB::table('merek_bus')->where('id_merek', $id)->get();
        $jenisbus = JenisBus::groupBy('jenis_kendaraan')->get();
        return view('BE.seller.transfer.edit-merek-bus', ['data' => $data], compact('jenisbus', 'hotel', 'xstay'));
    }

    public function updateBus(Request $request)
    {
        DB::table('merek_bus')->where('id_merek', $request->id_merek)->update([
            'id_jenis' => $request->jenisbus,
            'merek_bus' => $request->merek_bus,
            'plat_bus' => $request->plat_bus
        ]);

        return redirect()->route('masterMobil');
    }

    public function hapusBus($id)
    {
        DB::table('merek_bus')->where('id_merek', $id)->delete();

        return redirect()->route('masterMobil');
    }

    // View Rute Kamtuu

    public function viewRuteKamtuu()
    {
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $kecamatan = District::all();
        $data = Masterroute::all();
        $from = District::first();
        $to = District::first();

        return view('BE.seller.transfer.rute-by-kamtuu', compact('kecamatan', 'from', 'to', 'data', 'hotel', 'xstay'));
    }

    public function viewFaq(Request $request)
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'transfer'
        ])->has('productdetail')->with('productdetail')->get();

        return view('BE.seller.transfer.faq-transfer', compact('data', 'xstay', 'faqs'));
    }

    public function viewFaqCode(Request $request)
    {
        if ($request->product_transfer == null) {
            return redirect()->route('transfer.viewFaq');
        }

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $product = Product::where('id', $request->product_transfer)->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'transfer'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('id', $request->product_transfer)->with('productdetail')->first();

        // return view('BE.seller.hotel.faq', compact('data', 'xstay', 'faqs', 'faq_content'));
        return redirect()->route('transfer.viewFaq.edit', $product->product_code);
    }

    public function viewFaqEdit($code)
    {
        if ($code == null) {
            return redirect()->route('transfer.viewFaq');
        }

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'transfer'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('product_code', $code)->where('user_id', auth()->user()->id)->with('productdetail')->first();

        return view('BE.seller.transfer.faq-transfer', compact('faqs', 'faq_content', 'data', 'xstay'));
    }

    public function viewFaqPost(Request $request, $id)
    {

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'transfer'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('id', $id)->with('productdetail')->first();
        $faq_update = Productdetail::where('product_id', $id)->first();

        $faq_update->update([
            'faq' => $request->faq,
        ]);

        return redirect()->route('transfer.viewFaq');
    }

    public function addNewLayoutBus(Request $request){
        // dd($request->all());
        //start validate
        $pesan = [
            'jenis_kendaraan' => 'required',
        ];

        $this->validate($request,[
            'kolom_kursi'=>'required',
            'baris_kursi'=>'required',
            'judul_layout'=>'required',
        ],$pesan);

        // $arr_layout = collect([]);

        // foreach($request->layout as $key => $l){
        //     dump($l);
        // }
        // die;
        $layout['result'] = $request->layout;  
        
        // $data =[
        //     'user_id'=> Auth::user()->id,
        //     'kolom_kursi'=>isset($request->kolom_kursi) ? $request->kolom_kursi : 0,
        //     'baris_kursi'=>isset($request->baris_kursi) ? $request->baris_kursi : 0,
        //     'judul_layout'=>$request->judul_layout,
        //     'jumlah_kursi'=>$request->jumlah_kursi,
        //     'layout_kursi'=>json_encode($layout)
        // ];

        // dd($data);
        // 
        $bus_layout = new LayoutBus;
        // $bus_layout::create();
        
        $bus_layout->user_id = Auth::user()->id;
        $bus_layout->kolom_kursi  = $request->kolom_kursi;
        $bus_layout->baris_kursi  = $request->baris_kursi;
        $bus_layout->judul_layout = $request->judul_layout;
        $bus_layout->jumlah_kursi = $request->jumlah_kursi;
        $bus_layout->harga_Kursi = $request->harga_Kursi;
        $bus_layout->layout_kursi = json_encode($layout);


        $bus_layout->save();

        return redirect()->route('masterBus');
    }

    public function editNewLayoutBus($id, Request $request){
        // dd($request->all());
        $layout_bus =  LayoutBus::where('id_layout',$id);

        $layout['result'] = $request->layout; 
        
        $layout_bus->update([
            'kolom_kursi' => isset($request->kolom_kursi) ? $request->kolom_kursi : 0,
            'baris_kursi' => isset($request->baris_kursi) ? $request->baris_kursi : 0,
            'judul_layout' => $request->judul_layout,
            'jumlah_kursi' => $request->jumlah_kursi,
            'harga_kursi' => $request->harga_kursi,
            'layout_kursi' => json_encode($layout)
        ]);

        return redirect()->route('masterBus');
    }

    public function viewNewEditLayoutBus(Request $request, $id)
    {
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $layout_bus = LayoutBus::where('id_layout', $id)->first();
        
        
        $layouts = json_decode($layout_bus->layout_kursi, true)['result'];
        // $layouts = json_decode($layout_bus->layout_kursi, true);
        $layout = collect([]);

        foreach($layouts as $key => $value){

            $layout->push([
                'chair_number'=> intval($value['kursi']),
                'price_chair'=>isset($value['harga']) ? intval($value['harga']) : null,
                'disable_chair'=>isset($value['avaiblity']) ? ($value['avaiblity'] == 'on' ? true: false) : null,
                'user_id'=>null
            ]);
        }
        // dd($layout);
        $isedit = true;
        
        // return view('BE.seller.transfer.layout-bus-edit', compact('layout_bus', 'layout_m31', 'layout_m39', 'hotel', 'xstay', 'isedit', 'id'));
        return view('BE.seller.transfer.new-layout-bus-edit', compact('layout_bus', 'layout', 'hotel', 'xstay', 'isedit', 'id'));
    }

}
