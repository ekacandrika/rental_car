<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Masterkamar;
use App\Models\Price;
use App\Models\Regency;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Attributes;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function info_dasar()
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $code = 'HTL' . mt_rand(1, 99) . date('dmY');
        $regency = Regency::all();
        $attributes = Attributes::get();

        return view('BE.seller.hotel.informasidasar', compact('regency', 'code', 'data', 'xstay', 'attributes'));
    }

    public function harga()
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        return view('BE.seller.hotel.harga', compact('data', 'xstay'));
    }

    public function batas_bayar()
    {
        return view('BE.seller.hotel.batasbayar');
    }

    public function maps_foto()
    {
        return view('BE.seller.hotel.mapsfoto');
    }

    public function pilihan_ekstra()
    {
        return view('BE.seller.hotel.pilihanekstra');
    }

    public function faq()
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        
        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'hotel'
        ])->has('productdetail')->with('productdetail')->get();

        return view('BE.seller.hotel.faq', compact('data', 'xstay', 'faqs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_name' => 'required',
            'confirmation' => 'required',
            'regency_id' => 'required',
            'alamat' => 'required',
            'fasilitas_id' => 'required',
            'fasilitas_id.*' => 'required',
            'amenitas_id' => 'required',
            'amenitas_id.*' => 'required', 
            // 'icon_fasilitas' => 'required',
            // 'icon_fasilitas.*' => 'mimes:jpeg,png,jpg,ico|max:1048',
            // 'deskripsi_fasilitas' => 'required',
            // 'deskripsi_fasilitas.*' => 'required',
            // 'icon_amenitas' => 'required',
            // 'icon_amenitas.*' => 'mimes:jpeg,png,jpg,ico|max:1048',
            // 'judul_amenitas' => 'required',
            // 'judul_amenitas.*' => 'required',
            // 'isi_amenitas' => 'required',
            // 'isi_amenitas.*' => 'required',
            'deskripsi' => 'required',
            // 'catatan' => 'required',
        ],[
            'product_name.required' => 'Judul wajib diisi!',
            'confirmation.required' => 'Konfirmasi paket wajib diisi!',
            'regency_id.required' => 'Tag lokasi wajib diisi!',
            'alamat.required' => 'Alamat wajib diisi!',
            'fasilitas_id.required' => 'Fasilitas wajib diisi!',
            'fasilitas_id.*.required' => 'Fasilitas wajib diisi!',
            'amenitas_id.required' => 'Amenitas wajib diisi!',
            'amenitas_id.*.required' => 'Amenitas wajib diisi!', 
            // 'icon_fasilitas.required' => 'Icon fasilitas wajib diisi!',
            // 'icon_fasilitas.*.required' => 'Icon fasilitas wajib diisi!',
            // 'icon_fasilitas.*.mimes' => 'Icon fasilitas harus memiliki ekstensi (jpg, jpeg, png, atau ico)',
            // 'icon_fasilitas.*.max' => 'Icon fasilitas maksimum berukuran 1MB',
            // 'deskripsi_fasilitas.required' => 'Deskripsi wajib diisi!',
            // 'deskripsi_fasilitas.*.required' => 'Deskripsi wajib diisi!',
            // 'icon_amenitas.required' => 'Icon amenitas wajib diisi!',
            // 'icon_amenitas.*.required' => 'Icon amenitas wajib diisi!',
            // 'icon_amenitas.*.mimes' => 'Icon amenitas harus memiliki ekstensi (jpg, jpeg, png, atau ico)',
            // 'icon_amenitas.*.max' => 'Icon amenitas maksimum berukuran 1MB',
            // 'judul_amenitas.required' => 'Judul amenitas wajib diisi!',
            // 'judul_amenitas.*.required' => 'Judul amenitas wajib diisi!',
            // 'isi_amenitas.required' => 'Isi amenitas wajib diisi!',
            // 'isi_amenitas.*.required' => 'Isi amenitas wajib diisi!',
            'deskripsi.required' => 'Deskripsi wajib diisi!',
            // 'catatan.required' => 'Catatan wajib diisi!',
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        // $images_fasilitas = [];
        // foreach ($request->icon_fasilitas as $value) {
        //     $new_icon_fasilitas = time() . 'iconfasilitas' . $value->getClientOriginalName();
        //     $tujuan_uploud_icon_fasilitas = 'IconFasilitas/';
        //     $value->move($tujuan_uploud_icon_fasilitas, $new_icon_fasilitas);

        //     array_push($images_fasilitas, $tujuan_uploud_icon_fasilitas . $new_icon_fasilitas);
        // }

        // $images_amenitas = [];
        // foreach ($request->icon_amenitas as $result) {
        //     $new_icon_amenitas = time() . 'iconamenitas' . $result->getClientOriginalName();
        //     $tujuan_uploud_icon_amenitas = 'IconAmenitas/';
        //     $result->move($tujuan_uploud_icon_amenitas, $new_icon_amenitas);

        //     array_push($images_amenitas, $tujuan_uploud_icon_amenitas . $new_icon_amenitas);
        // }

        // Fasilitas
        $fasilitas_id = ['result' => $request->fasilitas_id];


        // Amenitas
        $amenitas_id = ['result' => $request->amenitas_id];

        // save to Product database
        $product = new Hotel;
        $product->user_id = auth()->user()->id;
        $product->product_code = $request->product_code;
        $product->product_name = $request->product_name;
        $product->type = "hotel";
        $product->save();

        $slug = Hotel::where('slug', $product->slug)->first();

        $slug->update([
            "product_link" => url('hotel') . '/' . $slug->slug
        ]);

        // save to Product Detail database
        $product_detail = new Productdetail;
        $product_detail->product_id = $product->id;
        $product_detail->regency_id = $request->regency_id;
        $product_detail->alamat = $request->alamat;
        $product_detail->confirmation = $request->confirmation;
        // $product_detail->icon_fasilitas = json_encode($icon_fasilitas, true);
        // $product_detail->deskripsi_fasilitas = json_encode($deskripsi, true);
        // $product_detail->icon_amenitas = json_encode($icon_amenitas, true);
        // $product_detail->judul_amenitas = json_encode($judul_amenitas, true);
        // $product_detail->isi_amenitas = json_encode($isi_amenitas, true);
        $product_detail->id_fasilitas = json_encode($fasilitas_id, true);
        $product_detail->id_amenitas = json_encode($amenitas_id, true);
        $product_detail->deskripsi = $request->deskripsi;
        $product_detail->catatan = $request->catatan;
        $product_detail->base_url = url('/') . '/hotel/' . $product->slug;
        $product_detail->save();


        return redirect()->route('hotel.infokamar.edit', $product->product_code);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Hotel::with('productdetail')->where('product_code', $id)->first();
        $regency = Regency::all();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $product_detail = Productdetail::where('product_id', $data->id)->first();
        $json = json_decode($product_detail, true);

        // Fasilitas
        $array = json_decode($json['icon_fasilitas'], true);
        $array1 = json_decode($json['deskripsi_fasilitas'], true);

        $result['result'] = isset($array) ? $array['result'] : [];
        $val['result'] = isset($array1) ? $array1['result'] : [];

        // Amenitas
        $array2 = json_decode($json['icon_amenitas'], true);
        $array3 = json_decode($json['judul_amenitas'], true);
        $array4 = json_decode($json['isi_amenitas'], true);

        // dd($array, $array1, $array2, $array3, $array4);
        $val1['result'] = isset($array2) ? $array2['result'] : [];
        $val2['result'] = isset($array3) ? $array3['result'] : [];
        $val3['result'] = isset($array4) ? $array4['result'] : [];

        $amenities = isset($product_detail->id_amenitas) ? json_decode($product_detail->id_amenitas, true)['result'] : [];
        $amenitas_array = collect([]);
        $fasilities = isset($product_detail->id_fasilitas) ? json_decode($product_detail->id_fasilitas, true)['result'] : [];
        $fasilitas_array = collect([]);

        for ($i = 0; $i < count($amenities); $i++) {
            $id_amenitas = Attributes::where('id', $amenities[$i])->first();
            $amenitas_array->push([
                'amenitas_id' => $id_amenitas->id,
                'amenitas_name' => $id_amenitas->text,
                'amenitas_img' => Storage::url($id_amenitas->image)
            ]);
        }

        for ($i = 0; $i < count($fasilities); $i++) {
            $id_fasilitas = Attributes::where('id', $fasilities[$i])->first();
            $fasilitas_array->push([
                'fasilitas_id' => $id_fasilitas->id,
                'fasilitas_name' => $id_fasilitas->text,
                'fasilitas_img' => Storage::url($id_fasilitas->image)
            ]);
        }

        $attributes = Attributes::get();

        return view('BE.seller.hotel.informasidasar_dup', compact('data', 'regency', 'hotel', 'xstay', 'result', 
        'val', 'val1', 'val2', 'val3', 'amenities', 'amenitas_array', 'fasilities', 'fasilitas_array', 'attributes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'product_name' => 'required',
            'confirmation' => 'required',
            'regency_id' => 'required',
            'alamat' => 'required',
            'fasilitas_id' => 'required',
            'fasilitas_id.*' => 'required',
            'amenitas_id' => 'required',
            'amenitas_id.*' => 'required',
            'deskripsi' => 'required',
        ],[
            'product_name.required' => 'Judul wajib diisi!',
            'confirmation.required' => 'Konfirmasi paket wajib diisi!',
            'regency_id.required' => 'Tag lokasi wajib diisi!',
            'alamat.required' => 'Alamat wajib diisi!',
            'fasilitas_id.required' => 'Fasilitas wajib diisi!',
            'fasilitas_id.*.required' => 'Fasilitas wajib diisi!',
            'amenitas_id.required' => 'Amenitas wajib diisi!',
            'amenitas_id.*.required' => 'Amenitas wajib diisi!',
            'deskripsi.required' => 'Deskripsi wajib diisi!',
        ]);

        
        if ($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        // dd($request->all());

        $data = Hotel::where('id', $id)->first();
        $data2 = Productdetail::where('product_id', $id)->first();

        $json = json_decode($data2, true);

        // Amenitas
        $amenitas_id = ['result' => $request->amenitas_id];

        // Fasilitas
        $fasilitas_id = ['result' => $request->fasilitas_id];

        //Update post with new image
        $data->update([
            'product_name' => $request->product_name,
        ]);

        $data2->update([
            'product_id'            => $data->id,
            'regency_id'            => $request->regency_id,
            'alamat'                => $request->alamat,
            'id_amenitas'           => $amenitas_id,
            'id_fasilitas'          => $fasilitas_id,
            'confirmation'          => $request->confirmation,
            'deskripsi'             => $request->deskripsi,
            'catatan'               => $request->catatan,
        ]);

        return redirect()->route('hotel.infokamar.edit', $data->product_code);
    }

    // Informasi Kamar
    public function info_kamar()
    {
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $kamar = Masterkamar::where('user_id', Auth::user()->id)->get();

        return view('BE.seller.hotel.informasikamar', compact('hotel', 'xstay', 'kamar'));
    }

    public function info_kamar_edit($id)
    {
        $data = Hotel::with('productdetail')->where('product_code', $id)->first();
        // dd($data);
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $data2 = Productdetail::where('product_id', $data->id)->first();
        $masterKamar = Masterkamar::where('id', $data2->kamar_id)->first();
        $kamar = Masterkamar::where('user_id', Auth::user()->id)->get();

        $rooms = isset($data2->id_kamar) ? json_decode($data2->id_kamar, true)['result'] : [];
        $rooms_array = collect([]);

        for ($i = 0; $i < count($rooms); $i++) {
            $id_room = Masterkamar::where('id', $rooms[$i])->first();
            $rooms_array->push([
                'room_id' => $id_room->id,
                'room_name' => $id_room->nama_kamar,
                'room_code' => $id_room->kode_kamar,
                'room_cat' => $id_room->jenis_kamar,
                'room_price' => $id_room->harga_kamar,
                'room_stok' => $id_room->stok,
            ]);
        }

        if ($masterKamar == null) {
            return view('BE.seller.hotel.informasikamar', compact('data', 'data2', 'hotel', 'xstay', 'masterKamar', 'kamar', 'rooms_array'));
        } else {
            $json = json_decode($masterKamar, true);

            $array = json_decode($json['foto_kamar'], true);
            $array1 = json_decode($json['fasilitas'], true);

            $result['result'] = $array['result'];
            $val['result'] = $array1['result'];

            return view('BE.seller.hotel.informasikamar_edit', compact('data', 'data2', 'hotel', 'xstay', 'result', 'val', 
            'masterKamar', 'array', 'array1', 'kamar', 'rooms_array'));
        }
    }

    public function info_kamar_update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'room_id' => 'required',
        ],[
            'room_id.required' => 'Informasi kamar wajib diisi!',
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        $data = Hotel::where('id', $id)->first();
        $product_detail = Productdetail::where('product_id', $id)->first();

        // Kamar
        $id_kamar = ['result' => $request->room_id];

        $product_detail->update([
            'id_kamar' => $id_kamar,
        ]);

        return redirect()->route('hotelharga.edit', $data->product_code);
    }

    // Harga
    public function harga_edit(Request $request, $id)
    {
        $data = Hotel::with('productdetail')->where('product_code', $id)->first();
        $data2 = Productdetail::with('harga', 'diskon', 'masterkamar')->where('product_id', $data->id)->first();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $harga = Price::where('id', $data2->harga_id)->first();
        $diskon = Discount::where('id', $data2->discount_id)->first();

        $rooms = isset($data2->id_kamar) ? json_decode($data2->id_kamar, true)['result'] : [];
        $rooms_array = collect([]);

        for ($i = 0; $i < count($rooms); $i++) {
            $id_room = Masterkamar::where('id', $rooms[$i])->first();
            $rooms_array->push([
                'room_id' => $id_room->id,
                'room_name' => $id_room->nama_kamar,
                'room_code' => $id_room->kode_kamar,
                'room_cat' => $id_room->jenis_kamar,
                'room_price' => $id_room->harga_kamar,
            ]);
        }

        // dd($diskon['tgl_start']);
        // dd($data2->masterkamar);
        if ($diskon == null) {
            return view('BE.seller.hotel.harga', compact('data', 'data2', 'hotel', 'xstay', 'rooms_array'));
        } else if ($diskon['tgl_start'] == null) {
            // dd('masuk');
            $json = json_decode($diskon, true);

            $array = json_decode($json['max_orang'], true);
            $array1 = json_decode($json['min_orang'], true);
            $array2 = json_decode($json['diskon_orang'], true);
            
            return view('BE.seller.hotel.harga_dup', compact('data', 'data2', 'hotel', 'xstay', 'array', 'array1', 'array2', 'rooms_array'));
        } else {
            $json = json_decode($diskon, true);

            $array = json_decode($json['max_orang'], true);
            $array1 = json_decode($json['min_orang'], true);
            $array2 = json_decode($json['diskon_orang'], true);

            $details = collect([]);
            // dd($json);
            if($json['tgl_start']){
                // foreach (json_decode($json['tgl_start']) as $key => $value) {
                    $details->push([
                        'tgl_start' => $json['tgl_start'],
                        'tgl_end' => $json['tgl_end'],
                        'discount' => $json['discount']
                    ]);
                // }
            }

            return view('BE.seller.hotel.harga_edit', compact('data', 'data2', 'hotel', 'xstay', 'array', 'array1', 'array2', 'details', 'rooms_array'));
        }
    }

    public function harga_update_residen(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'dewasa_residen' => 'required|integer|gte:0',
            'anak_residen' => 'required|integer|gte:0',
            'balita_residen' => 'required|integer|gte:0',
            'dewasa_non_residen' => 'sometimes|nullable|integer|gte:0',
            'anak_non_residen' => 'sometimes|nullable|integer|gte:0',
            'balita_non_residen' => 'sometimes|nullable|integer|gte:0'
        ],[
            'dewasa_residen.required' => 'Harga Dewasa Residen wajib diisi!',
            'anak_residen.required' => 'Harga Anak Residen wajib diisi!',
            'balita_residen.required' => 'Harga Balita Residen wajib diisi!',
            'dewasa_residen.integer' => 'Harga Dewasa Residen harus berupa bilangan bulat!',
            'anak_residen.integer' => 'Harga Anak Residen harus berupa bilangan bulat!',
            'balita_residen.integer' => 'Harga Balita Residen harus berupa bilangan bulat!',
            'dewasa_non_residen.integer' => 'Harga Dewasa non Residen harus berupa bilangan bulat!',
            'anak_non_residen.integer' => 'Harga Anak non Residen harus berupa bilangan bulat!',
            'balita_non_residen.integer' => 'Harga Balita non Residen harus berupa bilangan bulat!',
            'dewasa_residen.gte' => 'Harga Dewasa Residen minimal 0',
            'anak_residen.gte' => 'Harga Anak Residen minimal 0',
            'balita_residen.gte' => 'Harga Balita Residen minimal 0',
            'dewasa_non_residen.gte' => 'Harga Dewasa non Residen minimal 0',
            'anak_non_residen.gte' => 'Harga Anak non Residen minimal 0',
            'balita_non_residen.gte' => 'Harga Balita non Residen minimal 0',
        ]);

        $data = Hotel::where('id', $id)->first();

        if ($validator->fails())
        {
            return redirect()->route('hotelharga.edit', $data->product_code)->withErrors($validator, 'harga');
        }
        
        $data2 = Productdetail::where('product_id', $id)->first();
        $harga = Price::where('id', $data2->harga_id)->first();
        // dd($id, $data2);
        $dewasa_residen = $request->dewasa_residen;
        $anak_residen = $request->anak_residen;
        $balita_residen = $request->balita_residen;
        $dewasa_non_residen = isset($request->dewasa_non_residen) ? $request->dewasa_non_residen : $request->dewasa_residen;
        $anak_non_residen = isset($request->anak_non_residen) ? $request->anak_non_residen : $request->anak_residen;
        $balita_non_residen = isset($request->balita_non_residen) ? $request->balita_non_residen : $request->balita_residen;

        if ($harga == null) {
            $price = new Price;
            $price->dewasa_residen = $dewasa_residen;
            $price->anak_residen = $anak_residen;
            $price->balita_residen = $balita_residen;
            $price->dewasa_non_residen = $dewasa_non_residen;
            $price->anak_non_residen = $anak_non_residen;
            $price->balita_non_residen = $balita_non_residen;
            $price->save();

            $data2->update([
                'harga_id' => $price->id,
            ]);
        } else {
            $harga->update([
                'dewasa_residen' => $dewasa_residen,
                'anak_residen' => $anak_residen,
                'balita_residen' => $balita_residen,
                'dewasa_non_residen' => $dewasa_non_residen,
                'anak_non_residen' => $anak_non_residen,
                'balita_non_residen' => $balita_non_residen,
            ]);
        }

        return redirect()->route('hotelharga.edit', $data->product_code);
    }

    public function diskon_grup(Request $request, $id)
    {
        $data = Hotel::where('id', $id)->first();
        // dd($request);
        // $validator = Validator::make($request->all(), [
        //     'min_orang' => 'required',
        //     'max_orang' => 'required',
        //     'diskon_orang' => 'required',
        //     'min_orang.*' => 'required|integer|lte:max_orang.*',
        //     'max_orang.*' => 'required|integer|gte:min_orang.*',
        //     'diskon_orang.*' => 'required|present|numeric|gt:0',
        // ],[
        //     'min_orang.required' => 'Minimal jumlah orang wajib diisi!',
        //     'max_orang.required' => 'Maksimal jumlah orang wajib diisi!',
        //     'diskon_orang.required' => 'Diskon wajib diisi!',
        //     'min_orang.*.integer' => 'Minimal jumlah orang harus bilangan bulat!',
        //     'max_orang.*.integer' => 'Maksimal jumlah orang harus bilangan bulat!',
        //     'diskon_orang.*.numeric' => 'Diskon harus berupa angka! (Desimal gunakan titik)',
        //     'min_orang.*.lte' => 'Minimal orang harus lebih kecil dari maksimal orang!',
        //     'max_orang.*.gte' => 'Maksimal orang harus lebih besar dari minimal orang!',
        //     'diskon_orang.*.gt' => 'Diskon harus lebih besar dari 0!'
        // ]);

        // if ($validator->fails())
        // {
        //     return redirect()->route('hotelharga.edit', $data->product_code)->withErrors($validator, 'diskon_orang');
        // }

        // $data = Hotel::where('id', $id)->first();
        $data2 = Productdetail::where('product_id', $id)->first();
        $diskon = Discount::where('id', $data2->discount_id)->first();

        $arr_min = [
            'min_orang' => isset($request->min_orang) ? $request->min_orang : []
        ];
        $arr_max = [
            'max_orang' => isset($request->max_orang) ? $request->max_orang : []
        ];
        $arr_discount = [
            'diskon_orang' => isset($request->diskon_orang) ? $request->diskon_orang : []
        ];

        if ($diskon == null) {
            $discount = new Discount;
            $discount->min_orang = json_encode($arr_min, true);
            $discount->max_orang = json_encode($arr_max, true);
            $discount->diskon_orang = json_encode($arr_discount, true);
            $discount->save();

            $data2->update([
                'discount_id' => $discount->id,
            ]);
        } else {
            $diskon->update([
                'min_orang' => json_encode($arr_min, true),
                'max_orang' => json_encode($arr_max, true),
                'diskon_orang' => json_encode($arr_discount, true),
            ]);
        }

        return redirect()->route('hotelharga.edit', $data->product_code);
    }

    public function diskon_date(Request $request, $id)
    {

        // dd($request);

        $validator = Validator::make($request->all(), [
            'details' => 'required',
            'details.*' => 'required',
            'details.*.tgl_start' => 'required|date|before_or_equal:details.*.tgl_end',
            'details.*.tgl_end' => 'required|date|after_or_equal:details.*.tgl_start',
            'details.*.discount' => 'required|numeric',
        ],[
            'details' => 'Data wajib diisi!',
            'details.*' => 'Data wajib diisi!',
            'details.*.tgl_start.required' => 'Dari tanggal wajib diisi!',
            'details.*.tgl_end.required' => 'Sampai tanggal wajib diisi!',
            'details.*.discount.required' => 'Diskon atau penambahan biaya wajib diisi!',
            'details.*.tgl_start.date' => "Pastikan (Dari tanggal) terisi dengan benar",
            'details.*.tgl_end.date' => 'Pastikan (Sampai tanggal) terisi dengan benar',
            'details.*.discount.numeric' => 'Diskon atau penambahan biaya harus berupa angka!',
            'details.*.tgl_start.before_or_equal' => 'Pastikan tanggal mulai harus lebih kecil dari tanggal selesai!',
            'details.*.tgl_end.after_or_equal' => 'Pastikan Tanggal selesai harus lebih besar dari tanggal mulai!',
        ]);

        if ($validator->fails())
        {
            return redirect()->route('hotelharga.edit', $id)->withErrors($validator, 'diskon_ketersediaan');
        }
        
        $data = Hotel::where('id', $id)->first();
        $data2 = Productdetail::where('product_id', $id)->first();
        $disc = Discount::where('id', $data2->discount_id)->first();

        $tgl_start = collect([]);
        $tgl_end = collect([]);
        $discount = collect([]);

        if (isset($request->details)) {
            foreach ($request->details as $key => $value) {
                $tgl_start->push($value['tgl_start']);
                $tgl_end->push($value['tgl_end']);
                $discount->push($value['discount']);
            }
        }
        
        if ($disc == null) {
            $arr_min = [
                'min_orang' => isset($request->min_orang) ? $request->min_orang : []
            ];
            $arr_max = [
                'max_orang' => isset($request->max_orang) ? $request->max_orang : []
            ];
            $arr_discount = [
                'diskon_orang' => isset($request->diskon_orang) ? $request->diskon_orang : []
            ];

            $diskon = new Discount;

            $diskon->min_orang = json_encode($arr_min, true);
            $diskon->max_orang = json_encode($arr_max, true);
            $diskon->diskon_orang = json_encode($arr_discount, true);

            $diskon->tgl_start = $tgl_start;
            $diskon->tgl_end = $tgl_end;
            $diskon->discount = $discount;
            $diskon->save();

            $data2->update([
                'discount_id' => $diskon->id,
            ]);
        } else {
            $disc->update([
                'tgl_start' => $tgl_start,
                'tgl_end' => $tgl_end,
                'discount' => $discount,
            ]);
        }

        // $disc->update([
        //     'tgl_start' => $tgl_start,
        //     'tgl_end' => $tgl_end,
        //     'discount' => $discount,
        // ]);

        return redirect()->route('hotelbatasbayar.edit', $data->product_code);
    }

    // Batas Bayar
    public function batas_bayar_edit($id)
    {
        $data = Hotel::with('productdetail')->where('product_code', $id)->first();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $data2 = Productdetail::where('product_id', $data->id)->first();

        if ($data2->kebijakan_pembatalan_sebelumnya == null && $data2->kebijakan_pembatalan_sesudah == null && $data2->kebijakan_pembatalan_potongan == null) {
            return view('BE.seller.hotel.batasbayar', compact('data', 'hotel', 'xstay', 'data2'));
        } else {
            // $json = json_decode($data2, true);

            // $array = json_decode($json['kebijakan_pembatalan_sebelumnya'], true);
            // $array1 = json_decode($json['kebijakan_pembatalan_sesudah'], true);
            // $array2 = json_decode($json['kebijakan_pembatalan_potongan'], true);

            $json = json_decode($data2);

            // dd(json_decode($data2)->kebijakan_pembatalan_sebelumnya);
            // dd(json_decode(json_decode($data2)->kebijakan_pembatalan_sebelumnya)->sebelumnya);
            // $value['sebelumnya'] = $array['sebelumnya'];
            // $value['sesudah'] = $array1['sesudah'];
            // $value['potongan'] = $array2['potongan'];

            $value['sebelumnya'] = json_decode($json->kebijakan_pembatalan_sebelumnya)->sebelumnya;
            $value['sesudah'] = json_decode($json->kebijakan_pembatalan_sesudah)->sesudah;
            $value['potongan'] = json_decode($json->kebijakan_pembatalan_potongan)->potongan;

            // dd($value['sebelumnya']);

            // if(isset($value['sebelumnya'])){
            //     dd('masuk', $value['sebelumnya']);
            // }

            return view('BE.seller.hotel.batasbayar_dup', compact('data', 'hotel', 'xstay', 'value'));
        }
    }

    public function batas_bayar_update(Request $request, $id)
    {
        $data = Hotel::where('id', $id)->first();
        $data2 = Productdetail::where('product_id', $id)->first();

        $validator = Validator::make($request->all(), [
            'batas_pembayaran' => 'required|integer',
            'kebijakan_pembatalan_sebelumnya' => 'required',
            'kebijakan_pembatalan_sesudah' => 'required',
            'kebijakan_pembatalan_potongan' => 'required',
            'kebijakan_pembatalan_sebelumnya.*' => 'required|integer|lte:batas_pembayaran|gte:kebijakan_pembatalan_sesudah.*',
            'kebijakan_pembatalan_sesudah.*' => 'required|integer|lte:batas_pembayaran|lte:kebijakan_pembatalan_sebelumnya.*',
            'kebijakan_pembatalan_potongan.*' => 'required|integer',
        ],[
            'batas_pembayaran.required' => 'Batas pembayaran wajib diisi!',
            'batas_pembayaran.integer' => 'Pastikan batas pembayaran berupa bilangan bulat',
            // 'batas_pembayaran.gte' => 'Batas pembayaran harus lebih besar daripada batas waktu pembatalan',
            'kebijakan_pembatalan_sebelumnya.required' => 'Data wajib diisi!',
            'kebijakan_pembatalan_sesudah.required' => 'Data wajib diisi!',
            'kebijakan_pembatalan_potongan.required' => 'Potongan wajib diisi!',
            'kebijakan_pembatalan_sebelumnya.*.required' => 'Data wajib diisi!',
            'kebijakan_pembatalan_sesudah.*.required' => 'Data wajib diisi!',
            'kebijakan_pembatalan_potongan.*.required' => 'Potongan wajib diisi!',
            'kebijakan_pembatalan_sebelumnya.*.integer' => "Pastikan jumlah hari kebijakan pembatalan berupa bilangan bulat",
            'kebijakan_pembatalan_sesudah.*.integer' => 'Pastikan jumlah hari kebijakan pembatalan berupa bilangan bulat',
            'kebijakan_pembatalan_potongan.*.integer' => 'Pastikan potongan berupa bilangan bulat',
            'kebijakan_pembatalan_sebelumnya.*.lte' => 'Batas pembayaran harus lebih besar daripada batas waktu pembatalan',
            'kebijakan_pembatalan_sebelumnya.*.gte' => 'Nilai (hari sebelumnya sampai) harus lebih besar daripada nilai (hari sebelumnya)',
            'kebijakan_pembatalan_sesudah.*.lte' => 'Batas pembayaran harus lebih besar daripada batas waktu pembatalan',
            'kebijakan_pembatalan_sesudah.*.lte' => 'Nilai (hari sebelumnya sampai) harus lebih besar daripada nilai (hari sebelumnya)',
        ]);

        if ($validator->fails())
        {
            // return redirect()->route('hotelbatasbayar.edit', $data->product_code)->withErrors($validator, 'batas_waktu');
            return back()->withErrors($validator, 'batas_waktu');

        }

        $arr = [
            'sebelumnya' => $request->kebijakan_pembatalan_sebelumnya
        ];
        $arr1 = [
            'sesudah' => $request->kebijakan_pembatalan_sesudah
        ];
        $arr2 = [
            'potongan' => $request->kebijakan_pembatalan_potongan
        ];

        $data2->update([
            'batas_pembayaran' => $request->batas_pembayaran,
            'kebijakan_pembatalan_sebelumnya' => json_encode($arr),
            'kebijakan_pembatalan_sesudah' => json_encode($arr1),
            'kebijakan_pembatalan_potongan' => json_encode($arr2)
        ]);

        return redirect()->route('hotelmapsfoto.edit', $data->product_code);
    }

    // Maps Foto
    public function maps_foto_edit($id)
    {
        $data = Hotel::with('productdetail')->where('product_code', $id)->first();
        $detail = Productdetail::where('product_id', $data->id)->first();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        if ($detail->link_maps == null && $detail->foto_maps1 == null && $detail->foto_maps2 == null) {
            return view('BE.seller.hotel.mapsfoto', compact('data', 'hotel', 'xstay', 'detail'));
        } else {
            return view('BE.seller.hotel.mapsfoto_dup', compact('data', 'hotel', 'xstay', 'detail'));
        }
    }

    public function maps_foto_update(Request $request, $id)
    {
        $data = Hotel::where('id', $id)->first();
        $data2 = Productdetail::where('product_id', $id)->first();

        $validator = Validator::make($request->all(), [
            'link_maps' => 'required',
            'foto_maps_2' => 'required_without_all:saved_thumbnail|image|mimes:jpeg,png,jpg|max:2048',
        ],[
            'link_maps.required' => 'Link peta google maps wajib diisi!',
            'foto_maps_2.required_without_all' => 'Foto Thumbnail wajib diisi!',
            'foto_maps_2.mimes' => 'Foto harus memiliki ekstensi (jpg, jpeg, atau png)',
            'foto_maps_2.max' => 'Foto maksimum berukuran 2MB',
        ]);

        if ($validator->fails() && !isset($data2->foto_maps_1))
        {   $request->session()->flash('message', 'Foto Gallery harap diisi!');
            return redirect()->route('hotelmapsfoto.edit', $data->product_code)->withErrors($validator);
        }

        if (!isset($data2->foto_maps_1))
        {   $request->session()->flash('message', 'Foto Gallery harap diisi!');
            return redirect()->route('hotelmapsfoto.edit', $data->product_code);
        }
        
        if ($validator->fails())
        {
            return redirect()->route('hotelmapsfoto.edit', $data->product_code)->withErrors($validator);
        }

        $foto_maps_2 = isset($data2->foto_maps_2) ? $data2->foto_maps_2 : '';

        $fields = collect([]);

        if ($request->hasFile('foto_maps_2')) {

            $image1 = $request->file('foto_maps_2');
            $new_foto1 = time() . 'fotomaps_2'  . '.' . $image1->getClientOriginalExtension();
            $tujuan_uploud1 = 'FotoMaps_2/';
            $image1->move($tujuan_uploud1, $new_foto1);
            $foto_maps_2 = $tujuan_uploud1 . $new_foto1;
            if (file_exists($data2->foto_maps_2)) {
                unlink($data2->foto_maps_2);
            }
        }

        //Update post with new image
         $data2->update([
            'foto_maps_2'    => $foto_maps_2,
            'link_maps'       => $request->link_maps,
        ]);

        return redirect()->route('hotelpilihanekstra.edit', $data->product_code);
    }

    public function maps_photo_gallery(Request $request, $id)
    {
        // Validate request
        $validator = Validator::make($request->all(), [
            // Validate when saved gallery not exist
            'images_gallery' => 'required_without_all:saved_gallery',  
            'images_gallery.*' => 'mimes:jpeg,png,jpg|max:2048'
        ],[
            'images_gallery.required_without_all' => 'Foto Gallery wajib diisi!',
            'images_gallery.*.mimes' => "Foto harus memiliki ekstensi (jpg, jpeg, atau png)",
            'images_gallery.*.max' => 'Foto maksimum berukuran 2MB',
        ]);

        // Return when validate fails on one or more rule(s)
        if ($validator->fails())
        {
            return array(
                'status' => 0,
                'message' => $validator->messages()->first(),
            );
        }

        // Execute these code when validate success
        $data2 = Productdetail::where('product_id', $id)->first();
        $session_gallery = $data2->foto_maps_1;
        $fields = collect([]);

        // Save all new images gallery
        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {
                $new_foto = time() . 'fotomaps_1'. $key . '.' . $value->getClientOriginalExtension();
                $tujuan_upload = 'FotoMaps_1/';

                $value->move($tujuan_upload, $new_foto);
                $images_gallery = $tujuan_upload . $new_foto;

                $fields->push(
                    $images_gallery,
                );
            }
        }

        // Delete all existing saved gallery
        if (isset($session_gallery) && !$request->has('saved_gallery')) {
            foreach (json_decode($session_gallery) as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }
        }

        // // Delete some saved gallery iamges
        if (isset($session_gallery) && $request->has('saved_gallery')) {

            $differences = array_diff(json_decode($session_gallery), $request->saved_gallery);

            foreach ($differences as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }

            foreach ($request->saved_gallery as $key => $value) {
                $fields->push(
                    $value,
                );
            }
        }

        // $product_detail = $detail->productdetail()->update([
        //     'gallery' => json_encode($fields),
        // ]);

        // $request->session()->put('activity.peta.images_gallery', $fields);

        //update post with new image
        $data2->update([
            'foto_maps_1'   => json_encode($fields),
        ]);

        return array(
            'status' => 1,
            'message' => 'Foto Gallery berhasil diperbarui.'
        );
    }

    // Pilihan Ekstra
    public function pilihan_ekstra_edit($id)
    {
        $data = Hotel::with('productdetail')->where('product_code', $id)->first();
        $product_detail = Productdetail::where('product_id', $data->id)->first();
        $hotel = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        if ($product_detail->izin_ekstra == null) {
            return view('BE.seller.hotel.pilihanekstra', compact('data', 'hotel', 'xstay', 'product_detail'));
        } else {
            return view('BE.seller.hotel.pilihanekstra_dup', compact('data', 'hotel', 'xstay', 'product_detail'));
        }
    }

    public function pilihan_ekstra_update(Request $request, $id)
    {
        $data2 = Productdetail::where('product_id', $id)->first();
        $data = Hotel::where('id', $id)->first();

        $validator = Validator::make($request->all(), [
            'izin_ekstra' => 'required',
            'tnc' => 'accepted|required'
        ],[
            'izin_ekstra.required' => 'Ekstra wajib diisi!',
            'tnc.accepted' => 'Setujui syarat dan ketentuan untuk melanjutkan.',
            'tnc.required' => 'Setujui syarat dan ketentuan untuk melanjutkan.',
        ]);

        if ($validator->fails())
        {
            // dd($validator);
            return back()->withErrors($validator, 'pilihan_ekstra');
        }

        $data2->update([
            'izin_ekstra' => $request->izin_ekstra,
        ]);

        $request->session()->flash('url', url()->to('/') . '/hotel/' . $data->slug);
        // dd($request->session());

        return redirect()->route('dashboardmyListing');
    }

    // FAQ

    public function faq_code(Request $request)
    {
        if ($request->product_hotel == null) {
            return redirect()->route('hotelfaq');
        }
        
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $product = Product::where('id', $request->product_hotel)->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'hotel'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('id', $request->product_hotel)->with('productdetail')->first();

        // return view('BE.seller.hotel.faq', compact('data', 'xstay', 'faqs', 'faq_content'));
        return redirect()->route('hotelfaq.edit', $product->product_code);
    }

    public function faq_edit($code)
    {
        if ($code == null) {
            return redirect()->route('hotelfaq');
        }

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'hotel'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('product_code', $code)->where('user_id', auth()->user()->id)->with('productdetail')->first();

        return view('BE.seller.hotel.faq', compact('data', 'xstay', 'faqs', 'faq_content'));
    }

    public function faq_update(Request $request, $id)
    {
        $data = Hotel::where('id', $id)->first();
        $data2 = Productdetail::where('product_id', $id)->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'hotel'
        ])->has('productdetail')->with('productdetail')->get();

        $data2->update([
            'faq' => $request->faq
        ]);

        return redirect()->route('hotelfaq');
    }

    public function view_master_kamar(){
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $rooms = Masterkamar::where('user_id', auth()->user()->id)->paginate(5);
        return view('BE.seller.hotel.master-kamar', compact('data', 'xstay', 'rooms'));
    }

    public function view_add_kamar(){
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $attributes = Attributes::get();

        return view('BE.seller.hotel.tambah-kamar', compact('data', 'xstay', 'attributes'));
    }

    public function add_kamar(Request $request){
        $session_gallery = $request->session()->has('kamar.hotel.gallery') ? $request->session()->get('kamar.hotel.gallery') : null;
        $validator = Validator::make($request->all(), [
            'product_type' => 'required',
            'room_name' => 'required',
            'room_code' => 'required',
            'room_category' => 'required',
            'min_capacity' => 'required|lte:max_capacity',
            'max_capacity' => 'required|gte:min_capacity',
            'price_type' => 'required',
            'room_price' => 'required|numeric|min:0',
            'room_stock' => 'required|numeric|min:0',
            'refundable' => 'required',
            'refundable_amount' => 'required',
            'refundable_amount.*' => 'required|numeric|min:0|max:100',
            'refundable_price' => 'required',
            'refundable_price.*' => 'required|numeric|min:0',
            'extra_name' => 'required',
            'extra_name.*' => 'required',
            'extra_price' => 'required|min:0',
            'extra_price.*' => 'required|numeric|min:0',
            'amenitas_id' => 'required',
            'amenitas_id.*' => 'required'
        ],[
            'product_type.required' => 'Jenis produk wajib diisi!',
            'room_name.required' => 'Nama kamar wajib diisi!',
            'room_code.required' => 'Kode kamar wajib diisi!',
            'room_category.required' => 'Jenis Kamar wajib diisi!',
            'min_capacity.required' => 'Min kapasitas wajib diisi!',
            'min_capacity.lte' => 'Min kapasitas harus lebih kecil dari maks kapasitas',
            'max_capacity.required' => 'Maks kapasitas Kamar wajib diisi!',
            'max_capacity.gte' => 'Maks kapasitas harus lebih kecil dari min kapasitas',
            'price_type.required' => 'Jenis harga kamar wajib diisi!',
            'room_price.required' => 'Harga kamar diisi!',
            'room_price.numeric' => 'Harga kamar harus berupa angka!',
            'room_price.min' => 'Harga kamar minimal 0',
            'room_stock.required' => 'Stok kamar diisi!',
            'room_stock.numeric' => 'Stok kamar harus berupa angka!',
            'room_stock.min' => 'Stok kamar minimal 0',
            'refundable.required' => 'Refundable wajib diisi!',
            'refundable_amount.required' => 'Persen refund wajib diisi!',
            'refundable_amount.*' => 'Persen refund wajib diisi!',
            'refundable_amount.*.numeric' => 'Persen refund harus berupa angka!',
            'refundable_amount.*.min' => 'Persen refund minimal 0',
            'refundable_amount.*.max' => 'Persen refund maksimal 100',
            'refundable_price.required' => 'Harga refund wajib diisi!',
            'refundable_price.*' => 'Harga refund wajib diisi!',
            'refundable_price.*.numeric' => 'Harga refund harus berupa angka!',
            'refundable_price.*.min' => 'Harga refund minimal 0',
            'extra_name' => 'Nama ekstra wajib diisi!',
            'extra_name.*' => 'Nama ekstra wajib diisi!',
            'extra_price.required' => 'Harga ekstra diisi!',
            'extra_price.numeric' => 'Harga ekstra harus berupa angka!',
            'extra_price.min' => 'Harga ekstra minimal 0',
            'extra_price.*.required' => 'Harga ekstra wajib diisi!',
            'extra_price.*.min' => 'Harga ekstra minimal 0',
            'amenitas_id.required' => 'Amenitas wajib diisi!',
            'amenitas_id.*.required' => 'Amenitas wajib diisi!',
            // 'amenitas_icon.required' => 'Icon amenitas wajib diisi!',
            // 'amenitas_icon.*.required' => 'Icon amenitas wajib diisi!',
            // 'amenitas_icon.*.mimes' => 'Icon amenitas harus memiliki ekstensi (jpg, jpeg, png, atau ico)',
            // 'amenitas_icon.*.min' => 'Icon amenitas maksimum berukuran 1MB',
            // 'amenitas_name.required' => 'Nama amenitas wajib diisi!',
            // 'amenitas_name.*.required' => 'Nama amenitas wajib diisi!',
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        if(!isset($session_gallery)){
            return back()->withErrors(['image_gallery' => 'Gallery wajib diisi!'])->withInput();
        }

        // Ekstra
        $extra_name = ['result' => $request->extra_name];
        $extra_price = ['result' => $request->extra_price];

        // Amenitas
        $amenitas_id = ['result' => $request->amenitas_id];

        // Gallery
        $gallery = collect([]);

        foreach (json_decode($session_gallery) as $key => $value) {
                
            $url = url('/') . '/';
            $file_name = explode($url, $value, 2);
            $gallery->push($file_name[1]);
            
        }

        $gallery_kamar = ['result' => $gallery];

        $foto_kamar = json_encode($gallery_kamar, true);
        $nama_ekstra = json_encode($extra_name, true);
        $harga_ekstra = json_encode($extra_price, true);
        $id_amenitas = json_encode($amenitas_id, true);

        // Save to Master Kamar database
        $masterkamar = new Masterkamar;
        $masterkamar->user_id = auth()->user()->id;
        $masterkamar->jenis_produk = $request->product_type;
        $masterkamar->nama_kamar = $request->room_name;
        $masterkamar->kode_kamar = $request->room_code;
        $masterkamar->jenis_kamar = $request->room_category;
        $masterkamar->kapasitas_minimum = $request->min_capacity;
        $masterkamar->kapasitas_maksimum = $request->max_capacity;
        $masterkamar->tipe_harga = $request->price_type;
        $masterkamar->harga_kamar = $request->room_price;
        $masterkamar->foto_kamar = $foto_kamar;
        $masterkamar->stok = $request->room_stock;
        $masterkamar->refundable = $request->refundable;
        $masterkamar->refundable_amount = $request->refundable_amount;
        $masterkamar->refundable_price = $request->refundable_price;
        $masterkamar->nama_ekstra = $nama_ekstra;
        $masterkamar->harga_ekstra = $harga_ekstra;
        $masterkamar->id_amenitas = $id_amenitas;
        $masterkamar->save();

        // $product->user_id = auth()->user()->id;
        // $product->product_code = $request->product_code;
        // $product->product_name = $request->product_name;
        // $product->type = "hotel";
        // $product->save();

        // Masterkamar::create([
        //     'user_id' => auth()->user()->id,
        //     'jenis_produk' => $request->product_type,
        //     'nama_kamar' => $request->room_name,
        //     'kode_kamar' => $request->room_code,
        //     'jenis_kamar' => $request->room_category,
        //     'kapasitas_minimum' => $request->min_capacity,
        //     'kapasitas_maksimum' => $request->max_capacity,
        //     'tipe_harga' => $request->price_type,
        //     'harga_kamar' => $request->room_price,
        //     'foto_kamar' => $foto_kamar,
        //     'stok' => $request->room_stock,
        //     'refundable' => $request->refundable,
        //     'refundable_amount' => $request->refundable_amount,
        //     'refundable_price' => $request->refundable_price,
        //     'nama_ekstra' => $nama_ekstra,
        //     'harga_ekstra' => $harga_ekstra,
        //     'id_amenitas' => $id_amenitas,
        // ]);

        $request->session()->forget('kamar.hotel.gallery');

        return redirect()->route('kamarhotel');
    }

    public function clone_kamar($id){

        $kamar = Masterkamar::find($id);
        $newKamar = $kamar->replicate();

        $newKamar->created_at = Carbon::now();
        $newKamar->save();

        return redirect()->route('kamarhotel');
    }

    public function view_edit_kamar(Request $request, $id){
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $kamar = Masterkamar::find($id);

        if(auth()->user()->id != $kamar->user_id){
            return redirect()->route('kamarhotel');
        }

        $extra_name = isset($kamar->nama_ekstra) ? json_decode($kamar->nama_ekstra, true)['result'] : [];
        $extra_price = isset($kamar->harga_ekstra) ? json_decode($kamar->harga_ekstra, true)['result'] : [];
        $gallery_kamar = isset($kamar->foto_kamar) ? json_decode($kamar->foto_kamar, true)['result'] : [];
        $amenities = isset($kamar->id_amenitas) ? json_decode($kamar->id_amenitas, true)['result'] : [];
        $amenitas_array = collect([]);
        $extras_array = collect([]);
        $request->session()->put('kamar.hotel.old_image', $gallery_kamar);

        for ($i = 0; $i < count($amenities); $i++) {
            $id_amenitas = Attributes::where('id', $amenities[$i])->first();
            $amenitas_array->push([
                'amenitas_id' => $id_amenitas->id,
                'amenitas_name' => $id_amenitas->text,
                'amenitas_img' => Storage::url($id_amenitas->image)
            ]);
        }

        for ($j = 0; $j < count($extra_name); $j++) {
            $extras_array->push([
                'extra_name' => $extra_name[$j],
                'extra_price' => $extra_price[$j],
            ]);
        }

        $attributes = Attributes::get();

        return view('BE.seller.hotel.edit-kamar', compact('data', 'xstay', 'kamar', 
        'extra_name', 'extra_price', 'id', 'gallery_kamar', 'attributes', 'amenities',
        'amenitas_array', 'extras_array'));
    }

    public function edit_kamar(Request $request, $id){
        $kamar = Masterkamar::findOrFail($id);

        if(auth()->user()->id != $kamar->user_id){
            return redirect()->route('kamarhotel');
        }

        $session_gallery = $request->session()->get('kamar.hotel.gallery');
        $new_image = $request->session()->get('kamar.hotel.new_image');
        $old_image = $request->session()->get('kamar.hotel.old_image');

        $validator = Validator::make($request->all(), [
            'room_name' => 'required',
            'room_code' => 'required',
            'room_category' => 'required',
            'min_capacity' => 'required|lte:max_capacity',
            'max_capacity' => 'required|gte:min_capacity',
            'price_type' => 'required',
            'room_price' => 'required|numeric|min:0',
            'room_stock' => 'required|numeric|min:0',
            'refundable' => 'required',
            'refundable_amount' => 'required',
            'refundable_amount.*' => 'required|numeric|min:0|max:100',
            'refundable_price' => 'required',
            'refundable_price.*' => 'required|numeric|min:0',
            'extra_name' => 'required',
            'extra_name.*' => 'required',
            'extra_price' => 'required|min:0',
            'extra_price.*' => 'required|numeric|min:0',
            'amenitas_id' => 'required',
            'amenitas_id.*' => 'required'
        ],[
            'room_name.required' => 'Nama kamar wajib diisi!',
            'room_code.required' => 'Kode kamar wajib diisi!',
            'room_category.required' => 'Jenis Kamar wajib diisi!',
            'min_capacity.required' => 'Min kapasitas wajib diisi!',
            'min_capacity.lte' => 'Min kapasitas harus lebih kecil dari maks kapasitas',
            'max_capacity.required' => 'Maks kapasitas Kamar wajib diisi!',
            'max_capacity.gte' => 'Maks kapasitas harus lebih kecil dari min kapasitas',
            'price_type.required' => 'Jenis harga kamar wajib diisi!',
            'room_price.required' => 'Harga kamar diisi!',
            'room_price.numeric' => 'Harga kamar harus berupa angka!',
            'room_price.min' => 'Harga kamar minimal 0',
            'room_stock.required' => 'Stok kamar diisi!',
            'room_stock.numeric' => 'Stok kamar harus berupa angka!',
            'room_stock.min' => 'Stok kamar minimal 0',
            'refundable.required' => 'Refundable wajib diisi!',
            'refundable_amount.required' => 'Persen refund wajib diisi!',
            'refundable_amount.*' => 'Persen refund wajib diisi!',
            'refundable_amount.*.numeric' => 'Persen refund harus berupa angka!',
            'refundable_amount.*.min' => 'Persen refund minimal 0',
            'refundable_amount.*.max' => 'Persen refund maksimal 100',
            'refundable_price.required' => 'Harga refund wajib diisi!',
            'refundable_price.*' => 'Harga refund wajib diisi!',
            'refundable_price.*.numeric' => 'Harga refund harus berupa angka!',
            'refundable_price.*.min' => 'Harga refund minimal 0',
            'extra_name' => 'Nama ekstra wajib diisi!',
            'extra_name.*' => 'Nama ekstra wajib diisi!',
            'extra_price.required' => 'Harga ekstra diisi!',
            'extra_price.numeric' => 'Harga ekstra harus berupa angka!',
            'extra_price.min' => 'Harga ekstra minimal 0',
            'extra_price.*.required' => 'Harga ekstra wajib diisi!',
            'extra_price.*.min' => 'Harga ekstra minimal 0',
            'amenitas_id.required' => 'Amenitas wajib diisi!',
            'amenitas_id.*.required' => 'Amenitas wajib diisi!',
            // 'amenitas_icon.required' => 'Icon amenitas wajib diisi!',
            // 'amenitas_icon.*.required' => 'Icon amenitas wajib diisi!',
            // 'amenitas_icon.*.mimes' => 'Icon amenitas harus memiliki ekstensi (jpg, jpeg, png, atau ico)',
            // 'amenitas_icon.*.min' => 'Icon amenitas maksimum berukuran 1MB',
            // 'amenitas_name.required' => 'Nama amenitas wajib diisi!',
            // 'amenitas_name.*.required' => 'Nama amenitas wajib diisi!',
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        if(!isset($session_gallery) && !isset($old_image)){
            return back()->withErrors(['image_gallery'=>'Gallery wajib diisi!'])->withInput();
        }

        // Ekstra
        $extra_name = ['result' => $request->extra_name];
        $extra_price = ['result' => $request->extra_price];

        // Amenitas
        $amenitas_id = ['result' => $request->amenitas_id];

        // Gallery kamar
        // Default if gallery kamar doesn't change
        $gallery_kamar = $kamar->foto_kamar;

        // If gallery has change
        if(isset($session_gallery)){
            $gallery_kamar = ['result' => $session_gallery];
        }

        // Delete some saved gallery iamges
        $old_image = $request->session()->get('kamar.hotel.old_image');
        if (isset($kamar->foto_kamar) && isset($old_image) && count($old_image) > 0 && !is_array($old_image)) {
            $gallery = json_decode($kamar->foto_kamar, true)['result'];
            $differences = array_diff($gallery, $old_image->toArray());

            foreach ($differences as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }
        }
        
        $kamar->update([
            'user_id' => auth()->user()->id,
            'jenis_produk' => $request->product_type,
            'nama_kamar' => $request->room_name,
            'kode_kamar' => $request->room_code,
            'jenis_kamar' => $request->room_category,
            'kapasitas_minimum' => $request->min_capacity,
            'kapasitas_maksimum' => $request->max_capacity,
            'tipe_harga' => $request->price_type,
            'harga_kamar' => $request->room_price,
            'foto_kamar' => is_array($old_image) ? $gallery_kamar : json_encode($gallery_kamar, true),
            'stok' => $request->room_stock,
            'refundable' => $request->refundable,
            'refundable_amount' => $request->refundable_amount,
            'refundable_price' => $request->refundable_price,
            'nama_ekstra' => json_encode($extra_name, true),
            'harga_ekstra' => json_encode($extra_price, true),
            'id_amenitas' => json_encode($amenitas_id, true),
        ]);

        $request->session()->forget('kamar.hotel.new_image');
        $request->session()->forget('kamar.hotel.old_image');
        $request->session()->forget('kamar.hotel.gallery');

        return redirect()->route('kamarhotel');
    }

    public function delete_kamar($id){

        // Jangan lupa cek di productdetail, product, detailpesanan, serta booking order sebelum hapus
        Masterkamar::where('id', $id)->findOrFail($id)->delete();

        return redirect()->route('kamarhotel');
    }

    public function gallery_kamar(Request $request)
    {
        // Validate request
        $validator = Validator::make($request->all(), [
            // Validate when saved gallery does not exist
            'images_gallery' => 'required_without_all:saved_gallery',  
            'images_gallery.*' => 'mimes:jpeg,png,jpg|max:2048'
        ],[
            'images_gallery.required_without_all' => 'Foto Gallery wajib diisi!',
            'images_gallery.*.mimes' => "Foto harus memiliki ekstensi (jpg, jpeg, atau png)",
            'images_gallery.*.max' => 'Foto maksimum berukuran 2MB',
        ]);

        // Return when validate fails on one or more rule(s)
        if ($validator->fails())
        {
            return array(
                'status' => 0,
                'message' => $validator->messages()->first(),
            );
        }

        $session_gallery = $request->session()->get('kamar.hotel.gallery');
        
        $fields = collect([]);

        // Save all new images gallery
        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {
                // dd($value);
                $new_foto = time() . 'GalleryProductHotelKamar'. $key . '.' . $value->getClientOriginalExtension();
                $tujuan_upload = 'Seller/Product/Hotel/Kamar/';

                $lebar_foto = Image::make($value)->width();
                $lebar_foto = $lebar_foto * 50 / 100;

                Image::make($value)->resize($lebar_foto, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($tujuan_upload . $new_foto);

                $gallery_kamar = $tujuan_upload . $new_foto;

                $fields->push(
                    url('/').'/'.$gallery_kamar,
                );
                
            }
        }

        // Delete all existing saved gallery
        if (isset($session_gallery)) {
            foreach (json_decode($session_gallery) as $key => $value) {
                
                $url = url('/') . '/';
                $file_name = explode($url, $value, 2);

                if (file_exists($file_name[1])) {
                    unlink($file_name[1]);
                }
            }
        }

        $request->session()->put('kamar.hotel.gallery', $fields);

        return array(
            'status' => 1,
            'message' => 'Foto Gallery berhasil diperbarui.',
        );
    }

    public function gallery_kamar_edit(Request $request, $id)
    {
        // Validate request
        $validator = Validator::make($request->all(), [
            // Validate when saved gallery not exist
            'images_gallery' => 'required_without_all:saved_gallery',  
            'images_gallery.*' => 'mimes:jpeg,png,jpg|max:2048'
        ],[
            'images_gallery.required_without_all' => 'Foto Gallery wajib diisi!',
            'images_gallery.*.mimes' => "Foto harus memiliki ekstensi (jpg, jpeg, atau png)",
            'images_gallery.*.max' => 'Foto maksimum berukuran 2MB',
        ]);

        // Return when validate fails on one or more rule(s)
        if ($validator->fails())
        {
            return array(
                'status' => 0,
                'message' => $validator->messages()->first(),
            );
        }

        // Execute these code when validate success
        $user_id = Auth::user()->id;

        $detail = Masterkamar::where([
            'user_id' => $user_id,
            'id' => $id
        ])->first();

        $session_new_image = $request->session()->get('kamar.hotel.new_image');

        $new_image = collect([]);
        $fields = collect([]);

        // Save all new images gallery
        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {
                $new_foto = time() . 'GalleryProductHotelKamar'. $key . '.' . $value->getClientOriginalExtension();
                $tujuan_upload = 'Seller/Product/Hotel/Kamar/';

                $lebar_foto = Image::make($value)->width();
                $lebar_foto = $lebar_foto * 50 / 100;

                Image::make($value)->resize($lebar_foto, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($tujuan_upload . $new_foto);

                $images_gallery = $tujuan_upload . $new_foto;

                $new_image->push(
                    $images_gallery,
                );

                // Delete all previous new image that stored on Session
                if (isset($session_new_image)) {
                    foreach (json_decode($session_new_image) as $key => $value) {
                        
                        if (file_exists($value)) {
                            unlink($value);
                        }
                    }
                }
            }
        }

        // Pindah saat simpan edit
        // // Delete all existing saved gallery
        // if (isset($detail->foto_kamar) && !$request->has('saved_gallery')) {
        //     foreach (json_decode($detail->foto_kamar) as $key => $value) {
        //         if (file_exists($value[0])) {
        //             unlink($value[0]);
        //         }
        //     }
        // }

        if($request->has('saved_gallery')) {
            foreach ($request->saved_gallery as $key => $value) {
                $fields->push(
                    $value,
                );
            }
        }

        // Delete some saved gallery iamges
        // if (isset($detail->foto_kamar) && $request->has('saved_gallery')) {
        //     $gallery = json_decode($detail->foto_kamar, true)['result'];
        //     $differences = array_diff($gallery, $request->saved_gallery);

        //     // foreach ($differences as $key => $value) {
        //     //     if (file_exists($value)) {
        //     //         unlink($value);
        //     //     }
        //     // }

        //     // foreach ($request->saved_gallery as $key => $value) {
        //     //     $fields->push(
        //     //         $value,
        //     //     );
        //     // }
        // }

        $request->session()->put('kamar.hotel.new_image', $new_image);
        $request->session()->put('kamar.hotel.old_image', $fields);
        $image = $fields->merge($new_image);
        $request->session()->put('kamar.hotel.gallery', $image);

        return array(
            'status' => 1,
            'message' => 'Foto Gallery berhasil diperbarui.',
            'data' => $image
        );
    }
}
