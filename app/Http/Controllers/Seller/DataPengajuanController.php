<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Models\Pengajuan;
use App\Models\DataPengajuan;
use Illuminate\Http\Request;

class DataPengajuanController extends Controller
{
    public function store(Request $request, $id)
    {
        $data = $request->all();
        $data['status'] = 'Request';
        DataPengajuan::create($data);

        $data_pengajuan = Pengajuan::findOrFail($id);
        $data_pengajuan->update([
            'status' => 'Proses'
        ]);

        return redirect()->route('dashboardseller');
    }
}
