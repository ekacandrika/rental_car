<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Models\Pengajuan;
use Illuminate\Http\Request;

class PengajuanController extends Controller
{
    public function viewUploadDiscovery($id)
    {
        $data = Pengajuan::findOrFail($id);
        return view('BE.seller.uploadDiscovery', compact('data'));
    }

    public function updateDiscovery(Request $request, $id)
    {
        $data = Pengajuan::findOrFail($id);
        // dd($data);
        if ($request->hasFile('kegiatan')) {
            $kegiatan = $request->kegiatan;
            $new = time() . 'Discovery' . $kegiatan->getClientOriginalName();
            $tujuan_uploud = 'Discovery/';
            $kegiatan->move($tujuan_uploud, $new);

            //delete old image
            if (file_exists(($data->kegiatan))) {
                unlink(($data->kegiatan));
            }

            //update post with new image
            $data->update([
                'kegiatan' => $tujuan_uploud . $new
            ]);
        }

        return redirect()->route('dashboardseller');
    }

    public function viewUploadTandaTerima($id)
    {
        $data = Pengajuan::findOrFail($id);
        return view('BE.seller.uploadTandaTerima', compact('data'));
    }

    public function updateTandaTerima(Request $request, $id)
    {
        $data = Pengajuan::findOrFail($id);
        // dd($data);
        if ($request->hasFile('tanda_terima')) {
            $tanda_terima = $request->tanda_terima;
            $new = time() . 'TandaTerima' . $tanda_terima->getClientOriginalName();
            $tujuan_uploud = 'TandaTerima/';
            $tanda_terima->move($tujuan_uploud, $new);

            //delete old image
            if (file_exists(($data->tanda_terima))) {
                unlink(($data->tanda_terima));
            }

            //update post with new image
            $data->update([
                'tanda_terima' => $tujuan_uploud . $new
            ]);
        }

        return redirect()->route('dashboardseller');
    }
}
