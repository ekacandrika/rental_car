<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;

use App\Models\reviewPost;
use App\Models\Hotel;
use App\Models\BookingOrder;
// use App\Models\BranchStore;
use App\Models\Pengajuan;
use App\Models\PengajuanPembatalan;
use App\Models\Product;
use App\Models\Productdetail;
use App\Models\User;
use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use App\Models\Userdetail;
use App\Models\Village;
use App\Models\Detailpesanan;
use App\Models\Kelolatoko;
use App\Models\BannerToko;
use App\Models\Allbanner;
use App\Models\BranchStore;
use App\Models\LikeProduct;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SellerController extends Controller
{
    public function index()
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        return view('BE.seller.hotel.dashboard-kelolatoko', compact('data', 'xstay'));
    }

    public function viewLaporanSeller()
    {
        $data = BookingOrder::with('productdetail')->where('tgl_checkout', '!=', 'null')->where('toko_id', auth()->user()->id)->get();
        $data_pengajuan = Pengajuan::where('toko_id', auth()->user()->id)->get();
        
        $penjualan_perbulan = BookingOrder::with('productdetail')->whereDate('created_at',date('Y-m-d'))->where('toko_id',auth()->user()->id)->get();
        $penjualan_pertahun = BookingOrder::with('productdetail')->whereYear('created_at',date('Y'))->where('toko_id',auth()->user()->id)->get();
        $penjualan_sejak_bergabung =  BookingOrder::with('productdetail')->whereBetween('created_at',[auth()->user()->updated_at, now()])->where('toko_id',auth()->user()->id)->get();

        $data_listing = Product::with('productdetail')->has('productdetail')->where('user_id',auth()->user()->id)->get();
        $tagihan_belum_terbayar = BookingOrder::with('productdetail')->whereNull('tgl_checkout')->where('toko_id', auth()->user()->id)->get();
       
        $pendapatan_perbulan = Pengajuan::where('toko_id', auth()->user()->id)->whereDate('created_at',date('Y-m-d'))->get();
        $pendapatan_pertahun = Pengajuan::where('toko_id', auth()->user()->id)->whereYear('created_at',date('Y'))->get();
        $pendapatan_sejak_bergabung = Pengajuan::where('toko_id', auth()->user()->id)->whereBetween('created_at',[auth()->user()->updated_at, now()])->get();

        $bulan_penjualan=[];
        $bulan_pendapatan=[];
        $penjualan=[];
        $pendapatan=[];
        $total_pendapatan = 0;
        $pendapan_perseller=[];
        $peringkat='';

        foreach($data as $key => $value){
            $bulan = $value->created_at->format('F');
            if(!in_array($bulan,$bulan_penjualan)){
                array_push($bulan_penjualan,$bulan);
                $penjualan[$bulan]=0;
            }

            $penjualan[$bulan] +=$value->total_price;

        }

        foreach($data_pengajuan as $key => $pengajuan){
            $bulan = $pengajuan->created_at->format('F');
           
            if(!in_array($bulan,$bulan_pendapatan)){
                array_push($bulan_pendapatan,$bulan);
                $pendapatan[$bulan]=0;
            }
            
            $pendapatan[$bulan] +=$pengajuan->komisi;
        }
        
        $users = User::where('role','seller')->get();

        foreach($users as $key => $user){
            $data_pendapatan = Pengajuan::select(DB::raw('sum(komisi) as komisi'))->where('toko_id', $user->id)->get();
            
            foreach($data_pendapatan as $key => $v){
                $total_pendapatan=$v->komisi;
                
                if(!in_array($total_pendapatan, $pendapan_perseller)){
                    array_push($pendapan_perseller, $total_pendapatan);
                    // krsort($pendapan_perseller);
                }
            }
        }

        for($i=0; $i < count($pendapan_perseller);$i++){
            $low = $i;

            for($j=$i+1;$j < count($pendapan_perseller);$j++){
                if($pendapan_perseller[$j] > $pendapan_perseller[$low]){
                    $low=$j;
                }
            }

           if($pendapan_perseller[$i] < $pendapan_perseller[$low]){
                $tmp=$pendapan_perseller[$low];
                $pendapan_perseller[$low] = $pendapan_perseller[$i];
                $pendapan_perseller[$i]=$tmp;
           }
        }

        $peringkat = array_keys($pendapan_perseller,$data_pengajuan->sum('komisi'));
       
        return view('BE.seller.dashboard-seller', compact('data','penjualan','pendapatan','data_pengajuan','bulan_penjualan','bulan_pendapatan','penjualan_perbulan',
        'penjualan_pertahun','penjualan_sejak_bergabung','tagihan_belum_terbayar','data_listing'
        ,'pendapatan_perbulan','pendapatan_pertahun','pendapatan_sejak_bergabung','peringkat','pendapan_perseller'));
    }

    public function dashboard_seller()
    {
        $data_pengajuan = Pengajuan::where('toko_id', auth()->user()->id)->get();
        
        $turs = Pengajuan::whereYear('created_at', date('Y'))->where('status', '-')->where('type', 'tour')->where('toko_id', auth()->user()->id)->get();
        $hotels = Pengajuan::whereYear('created_at', date('Y'))->where('status', '-')->where('type', 'hotel')->where('toko_id', auth()->user()->id)->get();
        $xstays = Pengajuan::whereYear('created_at', date('Y'))->where('status', '-')->where('type', 'xstay')->where('toko_id', auth()->user()->id)->get();
        $activities = Pengajuan::whereYear('created_at', date('Y'))->where('status', '-')->where('type', 'activity')->where('toko_id', auth()->user()->id)->get();
        $rentals = Pengajuan::whereYear('created_at', date('Y'))->where('status', '-')->where('type', 'Rental')->where('toko_id', auth()->user()->id)->get();
        $transfers = Pengajuan::whereYear('created_at', date('Y'))->where('status', '-')->where('type', 'Transfer')->where('toko_id', auth()->user()->id)->get();
        $months_tur = [];
        $months_hotel = [];
        $months_xstay = [];
        $months_activity = [];
        $months_rental = [];
        $months_transfer = [];
        $earnings_tur = [];
        $earnings_hotel = [];
        $earnings_xstay = [];
        $earnings_activity = [];
        $earnings_rental = [];
        $earnings_transfer = [];
        $total_tur = 0;
        $total_hotel = 0;
        $total_xstay = 0;
        $total_activity = 0;
        $total_rental = 0;
        $total_transfer = 0;

        // Tur
        foreach ($turs as $tur) {
            $month_tur = $tur->created_at->format('F');

            if (!in_array($month_tur, $months_tur)) {
                array_push($months_tur, $month_tur);
                $earnings_tur[$month_tur] = 0;
            }

            $earnings_tur[$month_tur] += $tur->total_price;
            $total_tur += $tur->total_price;
        }

        // Hotel
        foreach ($hotels as $hotel) {
            $month_hotel = $hotel->created_at->format('F');

            if (!in_array($month_hotel, $months_hotel)) {
                array_push($months_hotel, $month_hotel);
                $earnings_hotel[$month_hotel] = 0;
            }

            $earnings_hotel[$month_hotel] += $hotel->total_price;
            $total_hotel += $hotel->total_price;
        }

        // Xstay
        foreach ($xstays as $xstay) {
            $month_xstay = $xstay->created_at->format('F');

            if (!in_array($month_xstay, $months_xstay)) {
                array_push($months_xstay, $month_xstay);
                $earnings_xstay[$month_xstay] = 0;
            }

            $earnings_xstay[$month_xstay] += $xstay->total_price;
            $total_xstay += $xstay->total_price;
        }

        foreach ($activities as $activity) {
            $month_activity = $activity->created_at->format('F');

            if (!in_array($month_activity, $months_activity)) {
                array_push($months_activity, $month_activity);
                $earnings_activity[$month_activity] = 0;
            }

            $earnings_activity[$month_activity] += $activity->total_price;
            $total_activity += $activity->total_price;
        }

        // Rental
        foreach ($rentals as $rental) {
            $month_rental = $rental->created_at->format('F');

            if (!in_array($month_rental, $months_rental)) {
                array_push($months_rental, $month_rental);
                $earnings_rental[$month_rental] = 0;
            }

            $earnings_rental[$month_rental] += $rental->total_price;
            $total_rental += $rental->total_price;
        }

        // Transfer
        foreach ($transfers as $transfer) {
            $month_transfer = $transfer->created_at->format('F');

            if (!in_array($month_transfer, $months_transfer)) {
                array_push($months_transfer, $month_transfer);
                $earnings_transfer[$month_transfer] = 0;
            }

            $earnings_transfer[$month_transfer] += $transfer->total_price;
            $total_transfer += $transfer->total_price;
        }

        $produks_tur_success = Pengajuan::where('type', 'tour')->where('status', 'Sudah Bayar')->where('toko_id', auth()->user()->id)->sum('total_price');
        $produks_tur_proses = Pengajuan::where('type', 'tour')->where('status', 'Proses' and 'status', '-')->where('toko_id', auth()->user()->id)->sum('total_price');
        $produks_hotel_success = Pengajuan::where('type', 'hotel')->where('status', 'Sudah Bayar')->where('toko_id', auth()->user()->id)->sum('total_price');
        $produks_hotel_proses = Pengajuan::where('type', 'hotel')->where('status', 'Proses' and 'status', '-')->where('toko_id', auth()->user()->id)->sum('total_price');
        $produks_xstay_success = Pengajuan::where('type', 'xstay')->where('status', 'Sudah Bayar')->where('toko_id', auth()->user()->id)->sum('total_price');
        $produks_xstay_proses = Pengajuan::where('type', 'xstay')->where('status', 'Proses' and 'status', '-')->where('toko_id', auth()->user()->id)->sum('total_price');
        $produks_activity_success = Pengajuan::where('type', 'activity')->where('status', 'Sudah Bayar')->where('toko_id', auth()->user()->id)->sum('total_price');
        $produks_activity_proses = Pengajuan::where('type', 'activity')->where('status', 'Proses' and 'status', '-')->where('toko_id', auth()->user()->id)->sum('total_price');
        $produks_rental_success = Pengajuan::where('type', 'Rental')->where('status', 'Sudah Bayar')->where('toko_id', auth()->user()->id)->sum('total_price');
        $produks_rental_proses = Pengajuan::where('type', 'Rental')->where('status', 'Proses' and 'status', '-')->where('toko_id', auth()->user()->id)->sum('total_price');
        $produks_transfer_success = Pengajuan::where('type', 'Transfer')->where('status', 'Sudah Bayar')->where('toko_id', auth()->user()->id)->sum('total_price');
        $produks_transfer_proses = Pengajuan::where('type', 'Transfer')->where('status', 'Proses' and 'status', '-')->where('toko_id', auth()->user()->id)->sum('total_price');
        // dd($months_hotel);
        return view('BE.seller.laporan-seller', compact(
            'months_hotel',
            'months_xstay',
            'months_activity',
            'months_rental',
            'months_transfer',
            'earnings_tur',
            'earnings_hotel',
            'earnings_xstay',
            'earnings_activity',
            'earnings_rental',
            'earnings_transfer',
            'total_tur',
            'total_hotel',
            'total_xstay',
            'total_activity',
            'total_rental',
            'total_transfer',
            'produks_tur_success',
            'produks_tur_proses',
            'produks_hotel_success',
            'produks_hotel_proses',
            'produks_xstay_success',
            'produks_xstay_proses',
            'produks_activity_success',
            'produks_activity_proses',
            'produks_rental_success',
            'produks_rental_proses',
            'produks_transfer_success',
            'produks_transfer_proses',
            'data_pengajuan'
        ));
    }

    public function myListing(Request $request)
    {
        // if($request->ajax()){
        // }
        $available = $request->available;
        $type = $request->type;
        $search = $request->search;

        if($available == 'draft'){
            $status_available = 'waiting approve';
        }else{
            $status_available = $available;
        }
        // dd($request->all());
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        // $data_listing = Product::where('user_id', auth()->user()->id)
        //     ->has('productdetail')
        //     ->with('productdetail', 'productdetail.DetailKendaraan', 'productdetail.DetailKendaraan.jenismobil', 'productdetail.DetailKendaraan.merekmobil')
        //     ->paginate(5);

        // $data_listing = Productdetail::with('diskon', 'product', 'detailkendaraan')->get();

        $data_listing = Productdetail::has('product')->with('product', 'DetailKendaraan', 'DetailKendaraan.jenismobil', 'DetailKendaraan.merekmobil')
            ->whereHas('product', function ($query) {
                $query->where('user_id', '=', auth()->user()->id);
                    
            }
        );

        if($available){
            $data_listing = $data_listing->where('available',$status_available)->get();
           
        }else if($type){
            $data_listing = $data_listing->whereHas('product', function ($query) use ($type) {
                
                $query->where('type', '=',$type);
                    
            })->get();
        }else if($search){

            $data_listing = $data_listing->whereHas('product', function ($query) use ($search) {
                
                $query->where('product_name', 'like','%'.$search.'%');
                    
            })->get();
        }
        else{
            $data_listing = $data_listing->get();
            // dd($data_listing);
        }
        // dd($data_listing);
        // die; 
        //              Product::join('product_detail','product.id','=','product_detail.product_id')
        //            ->join('detail_kendaraan','product_detail.detailmobil_id','=','detail_kendaraan.id')
        //            ->where('user_id', auth()->user()->id)->has('productdetail')->with('productdetail')->get();

        //        dd($data_listing);
        return view('BE.seller.my-listing', compact('data', 'xstay', 'data_listing','type','search','available'));
    }

    public function listPesanan()
    {
        $data = BookingOrder::where('toko_id', auth()->user()->id)->where('status', '1')->get();
        $data_permintaan = PengajuanPembatalan::where('toko_id', auth()->user()->id)->get();
        $count = Detailpesanan::where('toko_id', auth()->user()->id)->where('confirm_order', '=','waiting')->count();

        return view('BE.seller.list-pesanan', compact('data', 'data_permintaan', 'count'));
    }

    public function updateStatusPermintaanPembatalan($id)
    {
        $data = PengajuanPembatalan::findOrFail($id);

        
        $data->update([
            'status_pengajuan' => 'Dibatalkan Atas Permintaan Traveller'
        ]);

        return redirect()->back();
    }

    public function storeBatalkanPesanan(Request $request)
    {
        $pembatalanCode = mt_rand(1, 99) . date('dmY');

        $data = new PengajuanPembatalan;
        $data->kode_pembatalan = $pembatalanCode;
        $data->user_id = $request->user_id;
        $data->data_booking_id = $request->data_booking_id;
        $data->product_id = $request->product_id;
        $data->toko_id = $request->toko_id;
        $data->harga = $request->harga;
        $data->tgl_checkout = $request->tgl_checkout;
        $data->status_pembayaran = $request->status_pembayaran;
        $data->status_pengajuan = 'Dibatalkan Oleh Seller';
        $data->save();

        // update status from table booking to "2"
        $update_booking = BookingOrder::where('id', $request->data_booking_id)->first();
        $update_booking->update([
            'status' => '2'
        ]);

        return redirect()->back();
    }

    public function deleteListing($id){

        // $productdetail = Productdetail::where('product_id', $id)->first();
        Product::where('id', $id)->findOrFail($id)->delete();
        // Detailpesanan::where('id', $id)->findOrFail($id)->delete();
        // BookingOrder::where('booking_code', $request->kode_booking)->delete();

        return redirect()->route('dashboardmyListing');

    }

    public function createBranchStore(){
        $province = Province::all();
        $regency = Regency::all();
        $district = District::all();
        $village = Village::all();
        $isedit = false;
        return view('BE.seller.branch_store',compact('isedit','province', 'regency', 'district', 'village'));
    }

    public function storeBranchStore(Request $request){
        
        $branch_store = new BranchStore;

        $branch_store::create([
            'province_id' => $request->province_id,
            'regency_id' =>  $request->regency_id,
            'district_id' => $request->district_id,
            'village_id' =>  $request->village_id,
            'address' =>  $request->address,
            'post_code' =>  $request->post_code,
            'user_id'=>auth()->user()->id
        ]);

        return redirect()->route('seller.profilesetting', auth()->user()->role);
    }

    public function editBranchStore($id){

        $branch_store = BranchStore::findOrFail($id);
        // dd($branch_store->province_id);

        $province = Province::all();
        $regency = Regency::all();
        $district = District::all();
        $village = Village::all();
        
        $isedit = true;
        
        return view('BE.seller.branch_store',compact('branch_store','isedit','province', 'regency', 'district', 'village'));
    }

    public function updateBranchStore(Request $request, $id){

        $branch_store = BranchStore::findOrFail($id);
        $branch_store->update([
            'province' => $request->province_id,
            'regency' =>  $request->regency_id,
            'district' => $request->ristrict_id,
            'village' =>  $request->village_id,
            'address' =>  $request->address,
            'post_code' =>  $request->post_code,
        ]);

        return redirect()->route('seller.profilesetting', auth()->user()->role);
    }

    public function deleteBranchStore($id){
        $branch_store = BranchStore::findOrFail($id);

        $branch_store::destroy($id);

        return redirect()->route('seller.profilesetting', auth()->user()->role);

    }

    public function updateToko(Request $request, $id){
        
    }

    public function confirmListPesanan()
    {
        $count = Detailpesanan::where('toko_id', auth()->user()->id)->where('confirm_order', '=','waiting')
            ->orWhere('confirm_order', '=','reject')->get();

        return view('BE.seller.confirm-list-pesanan', compact('count'));
    }


    public function switchValue (Request $request, $id)
    {
        $radioButton = Detailpesanan::findOrFail($id);
        $radioButton->confirm_order = $request->confirm_order;
        $radioButton->save();
        
        return response()->json(['message' => 'Value updated successfully']);
    }

    public function detailToko($id=null){
        // dd(auth()->user());
        if(auth()->user()){
            if(auth()->user()->role=='seller'){
                $id = auth()->user()->id;
            }
        }

        $dekorasi = user::where('id', $id)->with('kelolatoko')->first();
        //dd($dekorasi);
        if($dekorasi){
            $toko = Kelolatoko::where('id_seller', $dekorasi->id)->first();
            $banner = Allbanner::where('user_id', $dekorasi->id)->first();
    
        }else{
            $toko = null;
            $banner = null;
    
        }
       
        if($banner){
            $banners = BannerToko::where('all_banner',$banner->id)->whereBetween('display_order',[1,6])->orderBy('display_order','asc')->inRandomOrder()->limit(6)->get()->toArray(); 

        }else{
            $banners = null;
        }
              
        // dd($banners);

        $lisensi = isset($dekorasi->lisensi) ? json_decode($dekorasi->lisensi, true)[0]:null;

        $i = 0; 
        $ls ='';
        
        if($lisensi){
            foreach($lisensi as $l){
                // dump(count($lisensi));
                $ls .= str_replace('"',"", str_replace('"',"",$l));
                if($i != count($lisensi)){
                    if($l!=null){
                        $ls .=", ";
                    }
                }
    
                $i++;
            }
        }


        $products    = Productdetail::with('product')->whereHas('product',function($q) use($id){
                        $q->where('user_id',$id)
                        ->orderBy('created_at','desc');
                    })->orderBy('created_at','desc')->limit(4)->get();
        
        $product_tur = Productdetail::with('product')->whereHas('product', function($q) use($id){
                            $q->where('type','tour')
                            ->where('user_id',$id);
                       })->inRandomOrder()->paginate(9);

        $product_transfer = Productdetail::with('product')->whereHas('product', function($q) use($id){
                                $q->where('type','transfer')->where('user_id',$id);
                            })->paginate(9);
        
        $product_rental  =   Productdetail::with('product')->whereHas('product', function($q) use($id){
                                $q->where('type','rental')->where('user_id',$id);
                            })->paginate(9);
  
        $product_activity = Productdetail::with('product')->whereHas('product',function($q) use($id){
                                $q->where('type','activity')->where('user_id',$id);
                            })->paginate(9);
        
        $product_hotel =    Productdetail::with('product')->whereHas('product',function($q) use($id){
                                $q->where('type','hotel')->where('user_id',$id);
                            })->paginate(9);
                          
        $product_xstay =   Productdetail::with('product')->whereHas('product',function($q) use($id){
                                $q->where('type','xstay')->where('user_id',$id);
                            })->paginate(9);

        $reviews  = reviewPost::join('product', 'product.id', '=', 'review_posts.product_id')
                   ->join('users', 'users.id', '=', 'review_posts.traveller_id')
                   ->whereNotNull('product_id')
                   ->where('product.user_id',$id)
                   ->orderBy('review_posts.created_at','desc')
                   ->limit(10)->get();

        $counts  = reviewPost::join('product', 'product.id', '=', 'review_posts.product_id')
        ->join('users', 'users.id', '=', 'review_posts.traveller_id')
        ->whereNotNull('product_id')
        ->where('product.user_id',$id)
        ->select('review_posts.star_rating')          
        ->limit(10)->get();           

        $var1 = $counts->sum('star_rating');
        $var2 = $counts->count();

        if($var1 == 0){
            $ratings = 0;
        }
        else{
            $ratings = $var1/$var2;
        }

        // check offical store
        $officialStore = BranchStore::where('user_id',$id)->get();
        $store_location = DB::table('user_detail')->where('user_id',$id)->first();

        $lokasi_cabang='';
        $x = 1;

        if($officialStore){
            foreach($officialStore as $key => $cabang){
                $lokasi_cabang .= $cabang->address;
                
                if($x != count($officialStore)){
                    $lokasi_cabang .=", "; 
                }
                
                $x++;
            }
        }
        // dd($officialStore,$lokasi_cabang);

        $count_likes = LikeProduct::select('status')->with('product')->whereHas('product',function($q) use($id){
            $q->where('user_id',$id);
        })->where('status',1);
        
        $likes1 = $count_likes->sum('status');
        $likes2 = $count_likes->count();

        if($likes1 == 0){
            $likes = 0;
        }else{
            $likes = round($likes1/$likes2);
        }
        
        return view('Produk.viewDetailToko',compact('dekorasi','toko', 
                    'banners', 'ls', 'products', 'product_tur','product_transfer','product_rental',
                    'product_activity','product_hotel','product_xstay','reviews','officialStore',
                    'store_location','ratings','likes','id','lokasi_cabang'));
    }

    public function filterProduct(Request $request){

    }

    public function orderComfrimBySeller(Request $request){
        
        $cari = $request->cari;
        
        if($cari){
            $data = BookingOrder::where('toko_id', auth()->user()->id)
            ->with(['users','productdetail'])
            ->where('status', '1')
            ->where('booking_code','like',"%".$cari."%")
            /* ->whereHas('productdetail',function($q) use($cari){
                $q->join('product','product.id','=','product_detail.product_id')
                ->orWhere('product_name','like','%'.$cari.'%');
            }) 
            */
            ->get();
        }else{
            $data = BookingOrder::where('toko_id', auth()->user()->id)
            ->where('status', '1')
            ->with(['users','productdetail'])
            ->get();
        }

        $list_datas = [];
        $i = 0;
        foreach($data as $key => $d){
            // Carbon\Carbon::parse($item->activity_date)->translatedFormat('d F Y')
            $list_datas[$i]["id"] = $d->id;
            $list_datas[$i]["toko_id"] = $d->toko_id;
            $list_datas[$i]["customer_id"] = $d->customer_id;
            $list_datas[$i]["booking_code"] = $d->booking_code;
            $list_datas[$i]["agent_id"]= $d->agent_id;
            $list_datas[$i]["product_detail_id"] =$d->product_detail_id;
            $list_datas[$i]["paket_id"]= $d->paket_id;
            $list_datas[$i]["paket_index"]= $d->paket_index;
            $list_datas[$i]["paket_index"]= $d->booking_code;
            $list_datas[$i]["activity_date"]= Carbon::parse($d->activity_date)->translatedFormat('d F Y');
            $list_datas[$i]["kamar_id"]= $d->kamar_id;
            $list_datas[$i]["check_in_date"]= $d->check_in_date;
            $list_datas[$i]["check_out_date"]= $d->check_out_date;
            $list_datas[$i]["refundable"]= $d->refundable;
            $list_datas[$i]["refundable_amount"]= $d->refundable_amount;
            $list_datas[$i]["refundable_price"]= $d->refundable_price;
            $list_datas[$i]["pilihan_id"]= $d->pilihan_id;
            $list_datas[$i]["index_pilihan"]= $d->index_pilihan;
            $list_datas[$i]["jumlah_pilihan"]= $d->jumlah_pilihan;
            $list_datas[$i]["extras_id"]= $d->extras_id;
            $list_datas[$i]["extra_name"]= $d->extra_name;
            $list_datas[$i]["catatan_umum_extra"]= $d->catatan_umum_extra;
            $list_datas[$i]["jumlah_extra"]= $d->jumlah_extra;
            $list_datas[$i]["catatan_per_extra"]= $d->catatan_per_extra;
            $list_datas[$i]["harga_ekstra"]= $d->harga_ekstra;
            $list_datas[$i]["harga_asli"]= $d->harga_asli;
            $list_datas[$i]["total_price"]= $d->total_price;
            $list_datas[$i]["catatan"]= $d->catatan;
            $list_datas[$i]["Ekstra_note"]= $d->Ekstra_note;
            $list_datas[$i]["peserta_dewasa"]= $d->peserta_dewasa;
            $list_datas[$i]["peserta_anak_anak"]= $d->peserta_anak_anak;
            $list_datas[$i]["peserta_balita"]= $d->peserta_balita;
            $list_datas[$i]["ekstra_sarapan"]= $d->ekstra_sarapan;
            $list_datas[$i]["tgl_checkout"]= Carbon::parse($d->tgl_checkout)->translatedFormat('d F Y');
            $list_datas[$i]["tgl_checkout_orig"]= Carbon::parse($d->tgl_checkout)->translatedFormat('Y-m-d');
            $list_datas[$i]["type"]= $d->type;
            $list_datas[$i]["status"]= $d->status;
            $list_datas[$i]["status_pembayaran"]= $d->status_pembayaran;
            $list_datas[$i]["kategori_peserta"]= $d->kategori_peserta;
            $list_datas[$i]["first_name"]= $d->first_name;
            $list_datas[$i]["last_name"]= $d->last_name;
            $list_datas[$i]["date_of_birth"]= $d->date_of_birth;
            $list_datas[$i]["jk"]= $d->jk;
            $list_datas[$i]["status_residen"]= $d->status_residen;
            $list_datas[$i]["citizenship"]= $d->citizenship;
            $list_datas[$i]["phone_number_order"]= $d->phone_number_order;
            $list_datas[$i]["email_order"]= $d->email_order;
            $list_datas[$i]["ID_card_photo_order"]= $d->ID_card_photo_order;
            $list_datas[$i]["created_at"]= $d->created_at;
            $list_datas[$i]["updated_at"]= $d->updated_at;
            $list_datas[$i]["users"]=$d->users;
            $list_datas[$i]["productdetail"]=$d->productdetail;
            $list_datas[$i]["productdetail"]["thumbnail"] = url($d->productdetail->thumbnail);
            
            $product = Product::where('id',$d->productdetail->id)->first();
            $list_datas[$i]["product"]=$product;
            
            $i++;
        }
        // return false;
        return response()->json([
            'success'=>true,
            'datas'=>$list_datas
        ]);
    }

    public function orderRejectSeller(Request $request){
        
        $cari =$request->cari;

        if($cari){

            $data_permintaan = PengajuanPembatalan::where('toko_id', auth()->user()->id)
            ->where('kode_pengajuan','like',"%".$cari."%")
            ->orWhere('kode_pembatalan','like',"%".$cari."%")
            ->get();

        }else{

            $data_permintaan = PengajuanPembatalan::where('toko_id', auth()->user()->id)
            ->get();

        }

        $list_reject = collect([]);
        
        foreach($data_permintaan as $key => $permintaan){

            //get user traveller
            $user = \App\Models\Usertraveller::where('user_id',$permintaan->user_id)->first();

            //get productdetail
            $detail = Productdetail::with('product')->where('product_id',$permintaan->product_id)->first();
            
            //get detail mobil
            if(isset($detail->detailmobil_id)){
                $detail_mobil = \App\Models\Mobildetail::where('id',$detail->detailmobil_id)->first();
                            
            }else{
                $detail_mobil = null;
            }

            $list_reject->push([
                'id'=>$permintaan->id,
                'user_id'=>$permintaan->user_id,
                'product_id'=>$permintaan->product_id,
                'data_booking_id'=>$permintaan->data_booking_id,
                'kode_pengajuan'=>$permintaan->kode_pengajuan,
                'kode_pembatalan'=>$permintaan->kode_pembatalan,
                'harga'=>$permintaan->harga,
                'tgl_checkout'=>Carbon::parse($permintaan->tgl_checkout)->translatedFormat('d F Y'),
                'tgl_pengajuan'=>Carbon::parse($permintaan->tgl_pengajuan)->translatedFormat('d F Y'),
                'activity_date'=>Carbon::parse($permintaan->activity_date)->translatedFormat('d F Y'),
                'tgl_checkout_orig'=>$permintaan->tgl_checkout,
                'tgl_pengajuan_orig'=>$permintaan->tgl_pengajuan,
                'activity_date_orig'=>$permintaan->activity_date,
                'status_pengajuan'=>$permintaan->status_pengajuan,
                'users'=>$user,
                'detail'=>[
                    'type'=>isset($detail->product) ? $detail->product->type:null,
                    'product_name'=>isset($detail->product) ? $detail->product->product_name:null,
                    'product_id'=> isset($detail->product_id) ? $detail->product_id:null,
                    'detailmobil_id'=>isset($detail->detailmobil_id) ? $detail->detailmobil_id:null,
                    'thumbnail'=>isset($detail->thumbnail) ? url($detail->thumbnail):null,
                    'kapasitas_kursi'=>$detail_mobil != null ? $detail_mobil->kapasitas_kursi:0,
                    'kapasitas_koper'=>$detail_mobil != null ? $detail_mobil->kapasitas_koper:0,
                ]
            ]);
        }

        return response()->json([
            'success'=>true,
            'datas'=>$list_reject
        ]);
    }
}
