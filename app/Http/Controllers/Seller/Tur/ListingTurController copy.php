<?php

namespace App\Http\Controllers\Seller\Tur;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Paket;
use App\Models\Pilihan;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\Itenerary;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;


class ListingTurController extends Controller
{
    public function index(Request $request)
    {        
        $provinsi = Province::all();
        $add_listing = $request->session()->has('product.add_listing') ? $request->session()->get('product.add_listing') : '';

        return view('BE.seller.tur.privateTrip.informasidasar', compact('provinsi', 'add_listing'));
    }
    public function addListingPrivate(Request $request)
    {
        $user_id = Auth::user()->id;
        
        $request->session()->forget('product');
        
        $request->session()->put('product.add_listing.product_name', $request->product_name);
        $request->session()->put('product.add_listing.confirmation_radio_button', $request->confirmation_radio_button);
        
        $request->session()->put('product.add_listing.province_id', $request->provinsi);
        $request->session()->put('product.add_listing.regency_id', $request->kabupaten);
        $request->session()->put('product.add_listing.district_id',$request->kecamatan);
        $request->session()->put('product.add_listing.village_id',$request->kelurahan);
            
        $request->session()->put('product.add_listing.tipe_tur',$request->tipe_tur);

        $request->session()->put('product.add_listing.min_orang',$request->min_orang);
        $request->session()->put('product.add_listing.max_orang',$request->max_orang);   

        $request->session()->put('product.add_listing.jml_hari',$request->jml_hari);
        $request->session()->put('product.add_listing.jml_malam',$request->jml_malam);    
            
        $request->session()->put('product.add_listing.makanan',$request->makanan);
        $request->session()->put('product.add_listing.bahasa',$request->bahasa);
        $request->session()->put('product.add_listing.tingkat_petualangan', $request->tingkat_petualangan);
            
        $request->session()->put('product.add_listing.deskripsi',$request->deskripsi);
        $request->session()->put('product.add_listing.paket_termasuk',$request->paket_termasuk);
        $request->session()->put('product.add_listing.paket_tidak_termasuk', $request->paket_tidak_termasuk);
        $request->session()->put('product.add_listing.catatan',$request->catatan);

        return redirect()->route('tur.viewItenenary');
    }
    public function addListingOpen(Request $request)
    {
        $user_id = Auth::user()->id;

        $fields = collect([]);
        
        foreach ($request->fields as $key => $value) {
            $fields->push([
                'nama_paket' => $value['nama_paket'],
                'jam_paket' => $value['jam_paket'],
                'max_peserta' => $value['max_peserta'],
                'min_peserta' => $value['min_peserta']
            ]);
        }

            $request->session()->forget('product');

            $request->session()->put('product.add_listing.product_name', $request->product_name);
            $request->session()->put('product.add_listing.confirmation_radio_button', $request->confirmation_radio_button);

            $request->session()->put('product.add_listing.province_id', $request->provinsi);
            $request->session()->put('product.add_listing.regency_id', $request->kabupaten);
            $request->session()->put('product.add_listing.district_id', $request->kecamatan);
            $request->session()->put('product.add_listing.village_id', $request->kelurahan);
            
            $request->session()->put('product.add_listing.tipe_tur', $request->tipe_tur);

            $request->session()->put('product.add_listing.paket', $fields);

            $request->session()->put('product.add_listing.min_orang', $request->min_orang);
            $request->session()->put('product.add_listing.max_orang', $request->max_orang);   

            $request->session()->put('product.add_listing.jml_hari', $request->jml_hari);
            $request->session()->put('product.add_listing.jml_malam', $request->jml_malam);    
            
            $request->session()->put('product.add_listing.makanan', $request->makanan);
            $request->session()->put('product.add_listing.bahasa', $request->bahasa);
            $request->session()->put('product.add_listing.tingkat_petualangan', $request->tingkat_petualangan);
            
            $request->session()->put('product.add_listing.deskripsi', $request->deskripsi);
            $request->session()->put('product.add_listing.paket_termasuk', $request->paket_termasuk);
            $request->session()->put('product.add_listing.paket_tidak_termasuk', $request->paket_tidak_termasuk);
            $request->session()->put('product.add_listing.catatan', $request->catatan);

        return redirect()->route('tur.viewItenenary');
    }

    public function viewItenenary(Request $request)
    {
        $itenerary = $request->session()->has('product.itenerary') ? $request->session()->get('product.itenerary') : [];

        return view('BE.seller.tur.privateTrip.itinerary', compact('itenerary'));
    }

    public function addItenenary(Request $request)
    {
        $gallery_itenenary = '';
        $details = collect([]);
        
        if ($request->hasFile('gallery_itenenary')) {
            $image = $request->file('gallery_itenenary');

            foreach ($image as $key => $value) {
                $new_foto = time() . 'GalleryProductTurItenerary' . $value->getClientOriginalName();
                $tujuan_upload = 'Seller/Product/Tur/Gallery/';
                $value->move($tujuan_upload, $new_foto);
                $gallery_itenenary = $tujuan_upload . $new_foto;
            }

            if($request->session()->has('product.itenenary.gallery_itenenary')){
                foreach ($request->session()->get('product.itenenary.gallery_itenenary') as $key => $value) {
                    if (file_exists($value['gallery_itenenary'])) {
                        unlink($value['gallery_itenenary']);
                    }
                }
            }
        }

        foreach ($request->details as $key => $value){
            $details->push([                    
                'judul_itenenary' => $value['judul_itenenary'],
                'deskripsi_itenenary' => $value['deskripsi_itenenary'],
                'gallery_itenenary' => $gallery_itenenary,
                'tautan_video' => $value['tautan_video']
            ]);
        }

        // dd($details);
        $request->session()->put('product.itenenary.additenenary', $details);

        return redirect()->route('tur.viewHarga');
    }

    public function viewHarga(Request $request)
    {
        $harga = $request->session()->has('product.harga') ? $request->session()->get('product.harga') : [];

        return view('BE.seller.tur.privateTrip.harga', compact('harga'));
    }

    public function addHarga(Request $request)
    {
        $request->session()->put('product.harga.dewasa_residen', $request->dewasa_residen);
        $request->session()->put('product.harga.anak_residen', $request->anak_residen);
        $request->session()->put('product.harga.balita_residen', $request->balita_residen);
        $request->session()->put('product.harga.dewasa_non_residen', $request->dewasa_non_residen == null ? $request->dewasa_residen : $request->dewasa_non_residen);
        $request->session()->put('product.harga.anak_non_residen', $request->anak_non_residen == null ? $request->anak_residen : $request->anak_non_residen);
        $request->session()->put('product.harga.balita_non_residen', $request->balita_non_residen == null ? $request->balita_residen : $request->balita_non_residen);
        
        $harga = $request->session()->has('product.harga') ? $request->session()->get('product.harga') : '';

        return view('BE.seller.tur.privateTrip.harga', compact('harga'));
    }

    public function addDiscountGroup(Request $request)
    {
        $fields = collect([]);

        foreach ($request->fields as $key => $value) {
            $fields->push([
                'min_orang' => $value['min_orang'],
                'max_orang' => $value['max_orang'],
                'diskon_orang' => $value['diskon_orang']
            ]);
        }
        
        $request->session()->put('product.harga.discount_group', $fields);
        $harga = $request->session()->has('product.harga') ? $request->session()->get('product.harga') : '';

        return view('BE.seller.tur.privateTrip.harga', compact('harga'));
    }

    public function addAvailability(Request $request)
    {
        // dd($request->details);
        $details = collect([]);
        

        if (isset($request->details)) {
            foreach ($request->details as $key => $value) {
                $details->push([
                    'tgl_start' => $value['tgl_start'],
                    'tgl_end' => $value['tgl_end'],
                    'discount' => $value['discount']
                ]);
            }
        }
        
        $request->session()->put('product.harga.availability', $details);
        
        return redirect()->route('tur.viewBatas');
    }

    public function viewBatas(Request $request)
    {
        $batas = $request->session()->has('product.batas') ? $request->session()->get('product.batas') : '';

        return view('BE.seller.tur.privateTrip.batasbayar', compact('batas'));
    }

    public function addBatas(Request $request)
    {
        $request->session()->put('product.batas.batas_pembayaran', $request->batas_pembayaran);

        $fields = collect([]);

        foreach ($request->fields as $key => $value) {
            $fields->push([
                'kebijakan_pembatalan_sebelumnya' => $value['kebijakan_pembatalan_sebelumnya'],
                'kebijakan_pembatalan_sesudah' => $value['kebijakan_pembatalan_sesudah'],
                'kebijakan_pembatalan_potongan' => $value['kebijakan_pembatalan_potongan']
            ]);
        }

        $request->session()->put('product.batas.pembatalan', $fields);

        $batas = $request->session()->has('product.batas') ? $request->session()->get('product.batas') : '';

        
        return redirect()->route('tur.viewPeta');
    }

    public function viewPeta(Request $request)
    {
        $peta = $request->session()->has('product.peta') ? $request->session()->get('product.peta') : '';

        return view('BE.seller.tur.privateTrip.mapsfoto', compact('peta'));
    }

    public function addPetaPhoto(Request $request)
    {
        $gallery = '';
        $thumbnail = '';
        $fields = collect([]);

        if ($request->hasFile('gallery')) {
            $image = $request->file('gallery');

            foreach ($image as $key => $value) {
                
                $new_foto = time() . 'GalleryProductTurGallery' . $value->getClientOriginalName();
                $tujuan_upload = 'Seller/Product/Tur/Gallery/';
                $value->move($tujuan_upload, $new_foto);
                $gallery = $tujuan_upload . $new_foto;

                $fields->push([
                    'gallery' => $gallery,
                ]);
            }

            if($request->session()->has('product.peta.gallery')){
                foreach ($request->session()->get('product.peta.gallery') as $key => $value) {
                    if (file_exists($value['gallery'])) {
                        unlink($value['gallery']);
                    }
                }
            }
        }

        if ($request->hasFile('thumbnail')) {
            $image = $request->file('thumbnail');
        
            $new_foto = time() . 'GalleryProductTurThumbnail' . $value->getClientOriginalName();
            $tujuan_upload = 'Seller/Product/Tur/Thumbnail/';
            $image->move($tujuan_upload, $new_foto);
            $thumbnail = $tujuan_upload . $new_foto;

            if (file_exists($request->session()->get('product.peta.thumbnail'))) {
                unlink($request->session()->get('product.peta.thumbnail'));
            }
        }

        $request->session()->put('product.peta.maps', $request->maps);
        $request->session()->put('product.peta.gallery', $fields);
        $request->session()->put('product.peta.thumbnail', $thumbnail);
        $peta = $request->session()->has('product.peta') ? $request->session()->get('product.peta') : '';

        return redirect()->route('tur.viewPilihanEkstra');
    }

    public function viewPilihanEkstra(Request $request)
    {
        $pilihan_ekstra = $request->session()->has('product.pilihan_ekstra') ? $request->session()->get('product.pilihan_ekstra') : '';

        return view('BE.seller.tur.privateTrip.pilihanekstra', compact('pilihan_ekstra'));
    }

    public function addPilihanEkstra(Request $request)
    {
        $request->session()->put('product.pilihan_ekstra.ekstra', $request->ekstra_radio_button);

        $fields = collect([]);

        foreach ($request->fields as $key => $value) {
            $fields->push([
                'judul_pilihan' => $value['judul_pilihan'],
                'deskripsi_pilihan' => $value['deskripsi_pilihan'],
                'harga_pilihan' => $value['harga_pilihan'],
                'kewajiban_pilihan' => $value['kewajiban_pilihan']
            ]);
        }

        $request->session()->put('product.pilihan_ekstra.pilihan', $fields);

        $user_id = Auth::user()->id;
        $gallery = $request->session()->get('product.peta.gallery');
        $kebijakan_pembatalan_sebelumnya = collect([]);
        $kebijakan_pembatalan_sesudah = collect([]);
        $kebijakan_pembatalan_potongan = collect([]);

        foreach($request->session()->get('product.batas.pembatalan') as $key => $value){
            $kebijakan_pembatalan_sebelumnya->push($value['kebijakan_pembatalan_sebelumnya']);
            $kebijakan_pembatalan_sesudah->push($value['kebijakan_pembatalan_sesudah']);
            $kebijakan_pembatalan_potongan->push($value['kebijakan_pembatalan_potongan']);
        }

        $product = Product::create([
            'user_id' => $user_id,
            'product_code' => 'AC' . mt_rand(1,99) . date('dmY'),
            'product_name' => $request->session()->get('product.add_listing.product_name'),
            'type' => 'Tur',
        ]);

        $nama_paket = collect([]);
        $jam_paket = collect([]);
        $max_peserta = collect([]);
        $min_peserta = collect([]);
        $paket_id = collect([]);

        foreach ($request->session()->get('product.add_listing.paket') as $key => $value) {
            $nama_paket->push($value['nama_paket']);
            $jam_paket->push($value['jam_paket']);
            $max_peserta->push($value['max_peserta']);
            $min_peserta->push($value['min_peserta']);
        }

        $paket = Paket::create([
            'nama_paket' => json_encode($nama_paket),
            'jam_paket' => json_encode($jam_paket),
            'max_peserta' => json_encode($max_peserta),
            'min_peserta' => json_encode($min_peserta),
        ]);

        $price = Price::create([
            'dewasa_residen' => $request->session()->get('product.harga.dewasa_residen'),
            'anak_residen' => $request->session()->get('product.harga.anak_residen'),
            'balita_residen' => $request->session()->get('product.harga.balita_residen'),
            'dewasa_non_residen' => $request->session()->get('product.harga.dewasa_non_residen'),
            'anak_non_residen' => $request->session()->get('product.harga.anak_non_residen'),
            'balita_non_residen' => $request->session()->get('product.harga.balita_non_residen'),
        ]);

        $max_orang = collect([]);
        $min_orang = collect([]);
        $diskon_orang = collect([]);
        $tgl_start = collect([]);
        $tgl_end = collect([]);
        $discount = collect([]);

        if (!empty($request->session()->get('product.harga.discount_group'))) {
            foreach ($request->session()->get('product.harga.discount_group') as $key => $value) {
                $max_orang->push($value['max_orang']);
                $min_orang->push($value['min_orang']);
                $diskon_orang->push($value['diskon_orang']);
            }
        }
        

        if (count($request->session()->get('product.harga.availability')) !== 0) {
            foreach ($request->session()->get('product.harga.availability') as $key => $value) {
                $tgl_start->push($value['tgl_start']);
                $tgl_end->push($value['tgl_end']);
                $discount->push($value['discount']);
            }
        }

        $discount_product = Discount::create([
            'max_orang' => json_encode($max_orang),
            'min_orang' => json_encode($min_orang),
            'diskon_orang' => json_encode($diskon_orang),
            'tgl_start' => json_encode($tgl_start),
            'tgl_end' => json_encode($tgl_end),
            'discount' => json_encode($discount),
        ]);

        $judul_pilihan = collect([]);
        $deskripsi_pilihan = collect([]);
        $harga_pilihan = collect([]);
        $kewajiban_pilihan = collect([]);

        foreach ($request->session()->get('product.pilihan_ekstra.pilihan') as $key => $value) {
            $judul_pilihan->push($value['judul_pilihan']);
            $deskripsi_pilihan->push($value['deskripsi_pilihan']);
            $harga_pilihan->push($value['harga_pilihan']);
            $kewajiban_pilihan->push($value['kewajiban_pilihan']);
        }

        $pilihan = Pilihan::create([
            'judul_pilihan' => json_encode($judul_pilihan),
            'deskripsi_pilihan' => json_encode($deskripsi_pilihan),
            'harga_pilihan' => json_encode($harga_pilihan),
            'kewajiban_pilihan' => json_encode($kewajiban_pilihan),
        ]);

        $gallery_itenenary = collect([]);
        $judul_itenenary = collect([]);
        $deskripsi_itenenary = collect([]);
        $tautan_video = collect([]);

        foreach ($request->session()->get('product.itenenary.additenenary') as $key => $value) {
            $judul_itenenary->push($value['judul_itenenary']);
            $deskripsi_itenenary->push($value['deskripsi_itenenary']);
            $gallery_itenenary->push($value['gallery_itenenary']);
            $tautan_video->push($value['tautan_video']);
        }

        $itenenary = Itenerary::create([
            'judul_itenenary' => json_encode($judul_itenenary),
            'deskripsi_itenenary' => json_encode($deskripsi_itenenary),
            'gallery_itenenary' => json_encode($gallery_itenenary),
            'tautan_video' => json_encode($tautan_video),
        ]);

        // dd($itenenary);

        $productdetail = Productdetail::create([
            'product_id' => $product->id,
            'province_id' => $request->session()->get('product.add_listing.province_id'),
            'regency_id' => $request->session()->get('product.add_listing.regency_id'),
            'district_id' => $request->session()->get('product.add_listing.district_id'),
            'village_id' => $request->session()->get('product.add_listing.village_id'),
            'discount_id' => $discount_product->id,
            'paket_id' => $paket->id,
            'harga_id' => $price->id,
            'pilihan_id' => $pilihan->id,
            'itenenary_id' => $itenenary->id,
            'confirmation' => $request->session()->get('product.add_listing.confirmation_radio_button'),
            'jml_hari' => $request->session()->get('product.add_listing.jml_hari'),
            'jml_malam' => $request->session()->get('product.add_listing.jml_malam'),
            'tipe_tur' => $request->session()->get('product.add_listing.tipe_tur'),
            'makanan' => $request->session()->get('product.add_listing.makanan'),
            'bahasa' => $request->session()->get('product.add_listing.bahasa'),
            'tingkat_petualangan' => $request->session()->get('product.add_listing.tingkat_petualangan'),
            'deskripsi' => $request->session()->get('product.add_listing.deskripsi'),
            'paket_termasuk' => $request->session()->get('product.add_listing.paket_termasuk'),
            'paket_tidak_termasuk' => $request->session()->get('product.add_listing.paket_tidak_termasuk'),
            'catatan' => $request->session()->get('product.add_listing.catatan'),
            'batas_pembayaran' => $request->session()->get('product.batas.batas_pembayaran'),
            'kebijakan_pembatalan_sebelumnya' => $kebijakan_pembatalan_sebelumnya,
            'kebijakan_pembatalan_sesudah' => $kebijakan_pembatalan_sesudah,
            'kebijakan_pembatalan_potongan' => $kebijakan_pembatalan_potongan,
            'link_maps' => $request->session()->get('product.peta.maps'),
            'izin_ekstra' => $request->session()->get('product.pilihan_ekstra.ekstra'),
            'gallery' => json_encode($gallery),
            'thumbnail' => $request->session()->get('product.peta.image_thumbnail'),
        ]);
        $request->session()->forget('product');

        return redirect()->route('tur');
    }
    
}
