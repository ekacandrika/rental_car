<?php

namespace App\Http\Controllers\Seller\Tur;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Paket;
use App\Models\MasterChoice;
use App\Models\Pilihan;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\Itenerary;
use App\Models\Kategori;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\detailObjek;
use App\Models\detailWisata;
use Illuminate\Support\Facades\DB;


class ListingTurController extends Controller
{
    public function index(Request $request)
    {
        // dd($request->session()->get('tur'));
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $province = Province::all();
        $kota     = Regency::all();
        $objek    = detailWisata::all();

        $island = DB::select('select * from pulau');
        // dd($objek);

        if ($request->session()->has('tur.add_listing')) {
            $kabupaten = Regency::where('id', $request->session()->get('tur.add_listing.regency_id'))->first();
            $kecamatan = District::where('id', $request->session()->get('tur.add_listing.district_id'))->first();
            $kelurahan = Village::where('id', $request->session()->get('tur.add_listing.village_id'))->first();
        }

        // dd($objek);

        if ($request->session()->missing('tur.product_code')) {
            $user_id = Auth::user()->id;

            $product_recent = Product::where('type', 'tour')->orderBy('id', 'desc')->first();
            $code = isset($product_recent) ? $product_recent->product_code : 1;


            if ($code != 1) {
                $code = explode("TO", $code);
                $code = substr($code[1], 0, -8);
                $code++;
            }

            $product = Product::create([
                'user_id' => $user_id,
                'type' => 'tour',
            ]);

            $request->session()->put('tur.add_listing.product_id', $product->id);
            $product_id = Product::where('id', $product->id)->first();

            $product_code = $product->id . 'TO' . $code . date('dmY');
            $product->slug = "";

            $product_id->update([
                'product_code' => $product_code,
            ]);

            $request->session()->put('tur.product_code', $product_code);
        }

        $dynamic_tag = [];
        
        $i =0;
        $x =0;
        $z =0;
        $y =0;
        
        foreach($island as $p){
            
            $prov = Province::where('island_id',$p->id)->get();
            
            $dynamic_tag['pulau'][$i]['id_pulau'] = $p->id;
            $dynamic_tag['pulau'][$i]['nama_pulau'] =$p->island_name;
            
            foreach($prov as $pv){
                
                $dynamic_tag['pulau'][$i]['provinsi'][$x]['id_provinsi']=$pv->id;
                $dynamic_tag['pulau'][$i]['provinsi'][$x]['nama_provinsi']=$pv->name;
                
                $kab = Regency::where('province_id',$pv->id)->get();
                
                foreach($kab as $k){

                    $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['id_kabupaten']=$k->id;
                    $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['nama_kabupaten']=$k->name;


                    $wisata =detailWisata::where('kabupaten_id',$k->id)->get();
                    foreach($wisata as $w){
                       
                        $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['wisata'][$y]['id_wisata']=$w->id;
                        $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['wisata'][$y]['nama_wisata']=$w->namaWisata;
                       
                        $y++;
                    }

                    $z++;

                }    
                
                $x++;
            }

            $i++;
        }

        $dynamic_tag = collect($dynamic_tag);
        // dd($dynamic_tag);
        $isedit = false;

        $listing = $request->session()->get('tur.product_code');
        $add_listing = $request->session()->has('tur.add_listing') ? $request->session()->get('tur.add_listing') : '';
        $tur = $request->session()->has('tur') ? $request->session()->get('tur') : '';

        return view('BE.seller.tur.privateTrip.informasidasar', compact('island','province','kota', 'objek', 'add_listing', 'tur', 'listing', 'data', 'xstay','isedit','dynamic_tag'));
    }
    public function addListingPrivate(Request $request)
    {
        // dd($request->all());

        $new_tag = [];
        $new_tag['results'] = $request->new_tag_location;

        $request->session()->put('tur.add_listing.product_name', $request->product_name);
        $request->session()->put('tur.add_listing.confirmation_radio_button', $request->confirmation_radio_button);

        $request->session()->put('tur.add_listing.province_id', $request->provinsi);
        $request->session()->put('tur.add_listing.tag_location_1', $request->tag_location_1);
        $request->session()->put('tur.add_listing.tag_location_2', $request->tag_location_2);
        $request->session()->put('tur.add_listing.regency_id', $request->kabupaten);
        $request->session()->put('tur.add_listing.tag_location_3', $request->tag_location_3);
        $request->session()->put('tur.add_listing.tag_location_4', $request->tag_location_4);
        $request->session()->put('tur.add_listing.new_tag_location', json_encode($new_tag));
        $request->session()->put('tur.add_listing.district_id', $request->kecamatan);
        $request->session()->put('tur.add_listing.village_id', $request->kelurahan);

        $request->session()->put('tur.add_listing.tipe_tur', $request->status);

        $request->session()->put('tur.add_listing.min_orang', $request->min_orang);
        $request->session()->put('tur.add_listing.max_orang', $request->max_orang);

        $request->session()->put('tur.add_listing.jml_hari', $request->jml_hari);
        $request->session()->put('tur.add_listing.jml_malam', $request->jml_malam);

        $request->session()->put('tur.add_listing.makanan', $request->makanan);
        $request->session()->put('tur.add_listing.bahasa', $request->bahasa);
        $request->session()->put('tur.add_listing.tingkat_petualangan', $request->tingkat_petualangan);
        $request->session()->put('tur.add_listing.nama_kategori', $request->nama_kategori);

        $request->session()->put('tur.add_listing.deskripsi', $request->deskripsi);
        $request->session()->put('tur.add_listing.paket_termasuk', $request->paket_termasuk);
        $request->session()->put('tur.add_listing.paket_tidak_termasuk', $request->paket_tidak_termasuk);
        $request->session()->put('tur.add_listing.catatan', $request->catatan);

        return redirect()->route('tur.viewItenenary');
    }
    public function addListingOpen(Request $request)
    {
        // dd($request->all());
        $fields = collect([]);
        
        foreach ($request->fields as $key => $value) {
            $fields->push([
                'nama_paket' => $value['nama_paket'],
                'jam_paket' => $value['jam_paket'],
                'max_peserta' => $value['max_peserta'],
                'min_peserta' => $value['min_peserta']
            ]);
        }

        $new_tag = [];
        $new_tag['results'] = $request->new_tag_location;

        $request->session()->put('tur.add_listing.product_name', $request->product_name);
        $request->session()->put('tur.add_listing.confirmation_radio_button', $request->confirmation_radio_button);

        $request->session()->put('tur.add_listing.province_id', $request->provinsi);
        $request->session()->put('tur.add_listing.tag_location_1', $request->tag_location_1);
        $request->session()->put('tur.add_listing.tag_location_2', $request->tag_location_2);
        $request->session()->put('tur.add_listing.regency_id', $request->kabupaten);
        $request->session()->put('tur.add_listing.tag_location_3', $request->tag_location_3);
        $request->session()->put('tur.add_listing.tag_location_4', $request->tag_location_4);
        $request->session()->put('tur.add_listing.new_tag_location', json_encode($new_tag));
        $request->session()->put('tur.add_listing.district_id', $request->kecamatan);
        $request->session()->put('tur.add_listing.village_id', $request->kelurahan);

        $request->session()->put('tur.add_listing.tipe_tur', $request->status);

        $request->session()->put('tur.add_listing.paket', $fields);

        $request->session()->put('tur.add_listing.min_orang', $request->min_orang);
        $request->session()->put('tur.add_listing.max_orang', $request->max_orang);
        $request->session()->put('tur.add_listing.durasi', $request->durasi);
        $request->session()->put('tur.add_listing.nama_kategori', $request->nama_kategori);

        $request->session()->put('tur.add_listing.jml_hari', $request->jml_hari);
        $request->session()->put('tur.add_listing.jml_malam', $request->jml_malam);

        $request->session()->put('tur.add_listing.makanan', $request->makanan);
        $request->session()->put('tur.add_listing.bahasa', $request->bahasa);
        $request->session()->put('tur.add_listing.tingkat_petualangan', $request->tingkat_petualangan);

        $request->session()->put('tur.add_listing.deskripsi', $request->deskripsi);
        $request->session()->put('tur.add_listing.paket_termasuk', $request->paket_termasuk);
        $request->session()->put('tur.add_listing.paket_tidak_termasuk', $request->paket_tidak_termasuk);
        $request->session()->put('tur.add_listing.catatan', $request->catatan);

        return redirect()->route('tur.viewItenenary');
    }

    public function viewItenenary(Request $request)
    {
        $itenerary = '';

        if ($request->session()->has('tur.itenenary')) {
            $itenerary = $request->session()->get('tur.itenenary');
        } else {
            $request->session()->put('tur.itenenary', '');
            $itenerary = '';
        }

        $tur = $request->session()->has('tur') ? $request->session()->get('tur') : '';

        // dd($request->session()->get('tur'));
        $isedit = false;

        return view('BE.seller.tur.privateTrip.itinerary', compact('itenerary', 'tur','isedit'));
    }

    public function addItenenary(Request $request)
    {
        // dd($request->all());
        $pesan=[
            'required'=>':attribute harus ditambahkan'
        ];

        $this->validate($request,[
            'details'=>'required'
        ],$pesan);

        $gallery_itenenary = '';
        $gallery_itenenary = $request->session()->has('tur.itenerary.gallery_itenenary') ? $request->session()->get('tur.itenerary.gallery_itenenary') : [];
        $details = collect([]);

        // dd($gallery_itenenary);

        // if ($request->hasFile('gallery_itenenary')) {
        //     $image = $request->file('gallery_itenenary');

        //     foreach ($image as $key => $value) {
        //         $new_foto = time() . 'GalleryProductTurItenerary' . $value->getClientOriginalName();
        //         $tujuan_upload = 'Seller/Product/Tur/Gallery/';
        //         $value->move($tujuan_upload, $new_foto);
        //         $gallery_itenenary = $tujuan_upload . $new_foto;
        //     }

        //     if($request->session()->has('tur.itenenary.gallery_itenenary')){
        //         foreach ($request->session()->get('tur.itenenary.gallery_itenenary') as $key => $value) {
        //             if (file_exists($value['gallery_itenenary'])) {
        //                 unlink($value['gallery_itenenary']);
        //             }
        //         }
        //     }
        // }

        // foreach ($request->details as $key => $value){
        //     $details->push([                    
        //         'judul_itenenary' => $value['judul_itenenary'],
        //         'deskripsi_itenenary' => $value['deskripsi_itenenary'],
        //         'gallery_itenenary' => $gallery_itenenary,
        //         'tautan_video' => isset($value['tautan_video']) ? $value['tautan_video'] : 's'
        //     ]);
        // }

        foreach ($request->details as $key => $value) {
            // $gallery_itenenary = '';

            // if (isset($value['gallery_itenenary'])) {
            //     $image = $value['gallery_itenenary'];
            //     $new_foto = time() . 'GalleryProductTurItenerary' . $image->getClientOriginalName();
            //     $tujuan_upload = 'Seller/Product/Tur/Gallery/';
            //     $image->move($tujuan_upload, $new_foto);
            //     $gallery_itenenary = $tujuan_upload . $new_foto;
            // }

            $details->push([
                'judul_itenenary' => $value['judul_itenenary'],
                'deskripsi_itenenary' => $value['deskripsi_itenenary'],
                'radio_itinenary' => isset($value['radio_itinenary']) ? $value['radio_itinenary'] : '',
                'images' => $gallery_itenenary[$key] ?? '',
                'tautan_video' => isset($value['tautan_video']) ? $value['tautan_video'] : ''
            ]);
        }

        // dd($details);

        // if ($request->session()->has('product.itenenary.additenenary')) {
        //     foreach ($request->session()->get('product.itenenary.additenenary') as $key => $value) {
        //         if (file_exists($value['gallery_itenenary']) && $value['gallery_itenenary'] != '') {
        //             unlink($value['gallery_itenenary']);
        //         }
        //     }
        // }


        // dd($details);
        $request->session()->put('tur.itenenary.additenenary', $details);
        // dd($request->session()->get('tur.itenenary.additenenary'));
        $isedit = 'false';
        return redirect()->route('tur.viewHarga');
    }

    public function addIteneraryGallery(Request $request)
    {
        $session_gallery = $request->session()->get('tur.itenenary.gallery_itenenary');

        $fields = collect([]);

        // Save all new images gallery
        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {
                // dd($value);
                $new_foto = time() . 'GalleryProductTurItenerary'. $key . '.' . $value->getClientOriginalExtension();
                $tujuan_upload = 'Seller/Product/Tur/Gallery/';

                $lebar_foto = Image::make($value)->width();
                $lebar_foto = $lebar_foto * 50 / 100;

                Image::make($value)->resize($lebar_foto, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($tujuan_upload . $new_foto);

                $gallery_itenenary = $tujuan_upload . $new_foto;

                $fields[$request->index_gallery[$key]] = url('/').'/'.$gallery_itenenary;
            }
        }

        // Delete all existing saved gallery
        if (isset($session_gallery)) {
            foreach (json_decode($session_gallery) as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }
        }

        $request->session()->put('tur.itenerary.gallery_itenenary', $fields);

        return array(
            'status' => 1,
            'message' => 'Foto Gallery berhasil diperbarui.'
        );
    }

    public function viewHarga(Request $request)
    {
        $type= $request->session()->get('tur.add_listing.tipe_tur');
        $packet=$request->session()->get('tur.add_listing.paket');
        
        $harga =  '';
        $harga_paket ='';

        if ($request->session()->has('tur.harga')) {
            $harga = $request->session()->get('tur.harga');
        } else {
            $request->session()->put('tur.harga', []);
            $harga = [];
        }

        if ($request->session()->has('tur.paket')) {
            $harga_paket = $request->session()->get('tur.paket');
        } else {
            $request->session()->put('tur.paket', []);
            $harga_paket = [];
        }

        $isedit=false;
        $tur = $request->session()->has('tur') ? $request->session()->get('tur') : '';

        return view('BE.seller.tur.privateTrip.harga', compact('tur', 'harga','type','packet','isedit'));
    }

    public function addHarga(Request $request)
    {
        // dd($request->all());
        
        /* 
        [
            'nama_paket'=>[
                'desawa_residen'=>0,
            ]
        ]
        */

        $data = [];
        
        
        $type= $request->session()->get('tur.add_listing.tipe_tur');
        $packet=$request->session()->get('tur.add_listing.paket');
        $paketHargaJson = '';
        if($type=='Open Trip'){
            // $nama_paket ='grebek_subuh';
            // dd($packet);
            foreach($packet as $paket){
                $nama_paket = str_replace(' ','_',$paket['nama_paket']);
                $harga = $request->$nama_paket;
                $data['harga'][$nama_paket]['dewasa_residen']=$harga['dewasa_residen'];
                $data['harga'][$nama_paket]['anak_residen']=$harga['anak_residen'];
                $data['harga'][$nama_paket]['balita_residen']=$harga['balita_residen'];
                $data['harga'][$nama_paket]['dewasa_non_residen']=$harga['dewasa_non_residen'] == null ? $harga['dewasa_residen'] : $harga['dewasa_non_residen'];
                $data['harga'][$nama_paket]['anak_non_residen']=$harga['anak_non_residen'] == null ? $harga['anak_residen'] : $harga['anak_non_residen'];
                $data['harga'][$nama_paket]['balita_non_residen']=$harga['balita_non_residen'] == null ? $harga['balita_residen'] : $harga['balita_non_residen'];

            }

            $request->session()->put('tur.harga.paket', $data);

            $paketHargaJson = json_encode($data);
        }else{
            $request->session()->put('tur.harga.dewasa_residen', $request->dewasa_residen);
            $request->session()->put('tur.harga.anak_residen', $request->anak_residen);
            $request->session()->put('tur.harga.balita_residen', $request->balita_residen != null ? $request->balita_residen : 0);
            $request->session()->put('tur.harga.dewasa_non_residen', $request->dewasa_non_residen == null ? $request->dewasa_residen : $request->dewasa_non_residen);
            $request->session()->put('tur.harga.anak_non_residen', $request->anak_non_residen == null ? $request->anak_residen : $request->anak_non_residen);
            $request->session()->put('tur.harga.balita_non_residen', $request->balita_non_residen == null ? $request->balita_residen : $request->balita_non_residen);
            
            $paketHargaJson =null;
        }
        
        // dd($data);
        $harga = $request->session()->has('tur.harga') ? $request->session()->get('tur.harga') : '';
        // $harga_paket = $request->session()->has('tur.harga') ? $request->session()->get('tur.harga') : '';
        $tur = $request->session()->has('tur') ? $request->session()->get('tur') : '';
        $isedit=false;
        
        return view('BE.seller.tur.privateTrip.harga', compact('harga','type','packet','paketHargaJson','tur','isedit'));
    }

    public function addDiscountGroup(Request $request)
    {

        $pesan=[
            'required'=>'Diskon harus ditambahkan'
        ];

        $type= $request->session()->get('tur.add_listing.tipe_tur');
        $packet=$request->session()->get('tur.add_listing.paket');
        

        $this->validate($request,[
            'fields'=>'required'
        ],$pesan);

        $gallery_itenenary = '';

        $fields = collect([]);

        foreach ($request->fields as $key => $value) {
            $fields->push([
                'min_orang' => $value['min_orang'],
                'max_orang' => $value['max_orang'],
                'diskon_orang' => $value['diskon_orang']
            ]);
        }

        $request->session()->put('tur.harga.discount_group', $fields);
        $harga = $request->session()->has('tur.harga') ? $request->session()->get('tur.harga') : '';
        $tur = $request->session()->has('tur') ? $request->session()->get('tur') : '';
        $isedit=false;

        return view('BE.seller.tur.privateTrip.harga', compact('harga','type','packet','tur','isedit'));
    }

    public function addAvailability(Request $request)
    {
        // dd($request->details[0]['paket'][0]);
        // dd($harga_paket['harga']);
        $details = collect([]);
        $harga_paket =$request->session()->get('tur.harga.paket');
        $packet=$request->session()->get('tur.add_listing.paket');
        if (isset($request->details)) {
            foreach ($request->details as $key => $value) {
                $details->push([
                    'tgl_start' => $value['tgl_start'],
                    'tgl_end' => $value['tgl_end'],
                    'discount' => $value['discount'],
                    'paket'=>$packet,
                    'harga_paket'=>$harga_paket['harga'] ?? ''
                ]);
            }
        }

        // dd($details);

        $request->session()->put('tur.harga.availability', $details);
        $tur = $request->session()->has('tur') ? $request->session()->get('tur') : '';
        // $isedit=false;
        return redirect()->route('tur.viewBatas','tur');
    }

    public function viewBatas(Request $request)
    {
        $batas = '';

        if ($request->session()->has('tur.batas')) {
            $batas = $request->session()->get('tur.batas');
        } else {
            $request->session()->put('tur.batas', '');
            $batas = '';
        }

        $tur = $request->session()->has('tur') ? $request->session()->get('tur') : '';
        $isedit=false;

        return view('BE.seller.tur.privateTrip.batasbayar', compact('batas', 'tur','isedit'));
    }

    public function addBatas(Request $request)
    {
        $pesan=[
            'required'=>'Kebijakan Pembatalan harus ditambahkan'
        ];

        $this->validate($request,[
            'fields'=>'required'
        ],$pesan);

        $request->session()->put('tur.batas.batas_pembayaran', $request->batas_pembayaran);

        $fields = collect([]);

        foreach ($request->fields as $key => $value) {
            $fields->push([
                'kebijakan_pembatalan_sebelumnya' => $value['kebijakan_pembatalan_sebelumnya'],
                'kebijakan_pembatalan_sesudah' => $value['kebijakan_pembatalan_sesudah'],
                'kebijakan_pembatalan_potongan' => $value['kebijakan_pembatalan_potongan']
            ]);
        }

        $request->session()->put('tur.batas.pembatalan', $fields);

        $batas = $request->session()->has('tur.batas') ? $request->session()->get('tur.batas') : '';


        return redirect()->route('tur.viewPeta');
    }

    public function viewPeta(Request $request)
    {
        $peta = '';

        if ($request->session()->has('tur.peta')) {
            $peta = $request->session()->get('tur.peta');
        } else {
            $request->session()->put('tur.peta', '');
            $peta = '';
        }

        $tur = $request->session()->has('tur') ? $request->session()->get('tur') : '';
        $isedit=false;
        return view('BE.seller.tur.privateTrip.mapsfoto', compact('peta', 'tur','isedit'));
    }

    public function addPetaPhoto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'maps' => 'required',
            'image_thumbnail' => 'required_without_all:saved_thumbnail|image|mimes:jpeg,png,jpg|max:2048',
        ],[
            'maps.required' => 'Link peta google maps wajib diisi!',
            'image_thumbnail.required_without_all' => 'Foto Thumbnail wajib diisi!',
            'image_thumbnail.mimes' => 'Foto harus memiliki ekstensi (jpg, jpeg, atau png)',
            'image_thumbnail.max' => 'Foto maksimum berukuran 2MB',
        ]);

        if ($validator->fails())
        {
            return redirect()->route('tur.viewPeta')->withErrors($validator);
        }

        $image_thumbnail = $request->session()->has('tur.peta.image_thumbnail') ? $request->session()->get('tur.peta.image_thumbnail') : '';

        // $fields = collect([]);

        // if ($request->session()->has('tur.peta.images_gallery') && !$request->hasFile('images_gallery')) {
        //     $image = $request->session()->get('tur.peta.images_gallery');
        //     foreach ($image as $key => $value) {
        //         $fields->push(
        //             $value,
        //         );
        //     }
        // }


        // if ($request->hasFile('images_gallery')) {
        //     $image = $request->file('images_gallery');

        //     foreach ($image as $key => $value) {

        //         $new_foto = time() . 'GalleryProductTur' . $value->getClientOriginalName();
        //         $tujuan_upload = 'Seller/Product/Tur/Gallery/';

        //         $lebar_foto = Image::make($value)->width();
        //         $lebar_foto = $lebar_foto * 50 / 100;

        //         Image::make($value)->resize($lebar_foto, null, function ($constraint) {
        //             $constraint->aspectRatio();
        //         })->save($tujuan_upload . $new_foto);

        //         // $value->move($tujuan_upload, $new_foto);
        //         $images_gallery = $tujuan_upload . $new_foto;

        //         $fields->push(
        //             $images_gallery,
        //         );
        //     }

        //     //delete old image
        //     if ($request->session()->has('tur.peta.images_gallery')) {
        //         foreach ($request->session()->get('tur.peta.images_gallery') as $key => $value) {
        //             if (file_exists($value[0])) {
        //                 unlink($value[0]);
        //             }
        //         }
        //     }
        // }

        if ($request->hasFile('image_thumbnail')) {
            $image = $request->file('image_thumbnail');

            $new_foto = time() . 'GalleryProductTur.' . $image->getClientOriginalExtension();
            $tujuan_upload = 'Seller/Product/Tur/Thumbnail/';

            $lebar_foto = Image::make($image)->width();
            $lebar_foto = $lebar_foto * 50 / 100;

            Image::make($image)->resize($lebar_foto, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($tujuan_upload . $new_foto);

            // $image->move($tujuan_upload, $new_foto);
            $image_thumbnail = $tujuan_upload . $new_foto;

            //delete old image
            if (file_exists($request->session()->get('tur.peta.image_thumbnail'))) {
                unlink($request->session()->get('tur.peta.image_thumbnail'));
            }
        }

        $request->session()->put('tur.peta.maps', $request->maps);
        // $request->session()->put('tur.peta.images_gallery', $fields);
        $request->session()->put('tur.peta.image_thumbnail', $image_thumbnail);

        return redirect()->route('tur.viewPilihanEkstra');
    }

    public function addPetaPhotoGallery(Request $request)
    {
        // Validate request
        $validator = Validator::make($request->all(), [
            // Validate when saved gallery not exist
            'images_gallery' => 'required_without_all:saved_gallery',  
            'images_gallery.*' => 'mimes:jpeg,png,jpg|max:2048'
        ],[
            'images_gallery.required_without_all' => 'Foto Gallery wajib diisi!',
            'images_gallery.*.mimes' => "Foto harus memiliki ekstensi (jpg, jpeg, atau png)",
            'images_gallery.*.max' => 'Foto maksimum berukuran 2MB',
        ]);

        // Return when validate fails on one or more rule(s)
        if ($validator->fails())
        {
            return array(
                'status' => 0,
                'message' => $validator->messages()->first(),
            );
        }

        // Execute these code when validate success
        $session_gallery = $request->session()->get('tur.peta.images_gallery');

        $fields = collect([]);

        // Save all new images gallery
        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {
                $new_foto = time() . 'GalleryProductTur'. $key . '.' . $value->getClientOriginalExtension();
                $tujuan_upload = 'Seller/Product/Tur/Gallery/';

                $lebar_foto = Image::make($value)->width();
                $lebar_foto = $lebar_foto * 50 / 100;

                Image::make($value)->resize($lebar_foto, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($tujuan_upload . $new_foto);

                $images_gallery = $tujuan_upload . $new_foto;

                $fields->push(
                    $images_gallery,
                );
            }
        }

        // Delete all existing saved gallery
        if (isset($session_gallery) && !$request->has('saved_gallery')) {
            foreach (json_decode($session_gallery) as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }
        }

        // Delete some saved gallery iamges
        if (isset($session_gallery) && $request->has('saved_gallery')) {

            $differences = array_diff(json_decode($session_gallery), $request->saved_gallery);

            foreach ($differences as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }

            foreach ($request->saved_gallery as $key => $value) {
                $fields->push(
                    $value,
                );
            }
        }

        // $product_detail = $detail->productdetail()->update([
        //     'gallery' => json_encode($fields),
        // ]);

        $request->session()->put('tur.peta.images_gallery', $fields);

        return array(
            'status' => 1,
            'message' => 'Foto Gallery berhasil diperbarui.'
        );
    }

    public function viewPilihanEkstra(Request $request)
    {
        $pilihan_ekstra = '';
        
        $masterPilihan = MasterChoice::where('product_type','tour')->get();
        $hotels  = Hotel::where('type','Hotel')->get();

        if ($request->session()->has('tur.pilihan_ekstra')) {
            $pilihan_ekstra = $request->session()->get('tur.pilihan_ekstra');
        } else {
            $request->session()->put('tur.pilihan_ekstra', '');
            $pilihan_ekstra = '';
        }
        $isedit=false;
        return view('BE.seller.tur.privateTrip.pilihanekstra', compact('pilihan_ekstra','masterPilihan','hotels','isedit'));
    }

    public function addPilihanEkstra(Request $request)
    {
        // dd($request->fields);
        // if(!$request->fields){
        //     return redirect()->back()->with('error', 'Harap mengisi/menambahkan pilihan'); 
        // }
        // die;
        $request->session()->put('tur.pilihan_ekstra.ekstra', $request->ekstra_radio_button);

        $fields = collect([]);

        if($request->fields){
            foreach ($request->fields as $key => $value) {
                $fields->push([
                    'judul_pilihan' => isset($value['nama_asuransi']) ? $value['nama_asuransi']:$value['nama_hotel'],
                    'deskripsi_pilihan' => isset($value['nama_asuransi']) ? '' : $value['deskripsi_pilihan'],
                    'harga_pilihan' => $value['harga_pilihan'],
                    'kewajiban_pilihan' => $value['kewajiban_pilihan'],
                    'nama_pilihan'=>$value['nama_pilihan']
                ]);
            }
        }

        // dd($fields);

        $request->session()->put('tur.pilihan_ekstra.pilihan', $fields);

        $user_id = Auth::user()->id;
        $kategori = Kategori::where('nama_kategori', 'tour')->first();
        $gallery = $request->session()->get('tur.peta.images_gallery');
        $kebijakan_pembatalan_sebelumnya = collect([]);
        $kebijakan_pembatalan_sesudah = collect([]);
        $kebijakan_pembatalan_potongan = collect([]);

        $product_id = Product::where('product_code', $request->session()->get('tur.product_code'))->first();
        $product_id->slug = "";
        $product_id->update([
            'product_name' => $request->session()->get('tur.add_listing.product_name'),
        ]);

        $nama_katagori = $request->session()->get('tur.add_listing.nama_kategori');
        $tagCategories = explode(",",$nama_katagori);
        // dd($tagCategories);
        // dd($product_id);
        $product_id->tag($tagCategories);

        foreach ($request->session()->get('tur.batas.pembatalan') as $key => $value) {
            $kebijakan_pembatalan_sebelumnya->push($value['kebijakan_pembatalan_sebelumnya']);
            $kebijakan_pembatalan_sesudah->push($value['kebijakan_pembatalan_sesudah']);
            $kebijakan_pembatalan_potongan->push($value['kebijakan_pembatalan_potongan']);
        }

        $nama_paket = collect([]);
        $jam_paket = collect([]);
        $max_peserta = collect([]);
        $min_peserta = collect([]);
        $paket_id = collect([]);
        $paket = null;

        if ($request->session()->has('tur.add_listing.paket')) {
            foreach ($request->session()->get('tur.add_listing.paket') as $key => $value) {
                $nama_paket->push($value['nama_paket']);
                $jam_paket->push($value['jam_paket']);
                $max_peserta->push($value['max_peserta']);
                $min_peserta->push($value['min_peserta']);
            }
        }

        if ($request->session()->get('tur.add_listing.tipe_tur') == 'Tur Private') {
            $paket = Paket::create([
                'nama_paket' => 'null',
                'jam_paket' => 'null',
                'max_peserta' => $request->session()->get('tur.add_listing.max_orang'),
                'min_peserta' => $request->session()->get('tur.add_listing.min_orang'),
            ]);
        }else{
            // dd($nama_paket);
            $paket = Paket::create([
                'nama_paket' => json_encode($nama_paket),
                'jam_paket' => json_encode($jam_paket),
                'max_peserta' => json_encode($max_peserta),
                'min_peserta' => json_encode($min_peserta),
            ]);

            // echo $paket->id;
        }

        // die;
        if($request->session()->get('tur.add_listing.tipe_tur')=='Open Trip'){
            $listPaket = $request->session()->get('tur.add_listing.paket');
            foreach($listPaket as $list){
                $nama_paket = str_replace(' ','_',$list['nama_paket']);
                $harga_paket = $request->session()->get('tur.harga.paket');
                foreach($harga_paket as $harga){
                    $price = Price::create([
                        'dewasa_residen' =>$harga[$nama_paket]['dewasa_residen'],
                        'anak_residen' =>$harga[$nama_paket]['anak_residen'],
                        'balita_residen' =>$harga[$nama_paket]['balita_residen'],
                        'dewasa_non_residen' =>$harga[$nama_paket]['dewasa_non_residen'],
                        'anak_non_residen' =>$harga[$nama_paket]['anak_non_residen'],
                        'balita_non_residen' =>$harga[$nama_paket]['balita_non_residen'],
                        'paket_id'=>$paket->id
                    ]);
                }
            }
        }else{
            $price = Price::create([
                'dewasa_residen' => $request->session()->get('tur.harga.dewasa_residen'),
                'anak_residen' => $request->session()->get('tur.harga.anak_residen'),
                'balita_residen' => $request->session()->get('tur.harga.balita_residen'),
                'dewasa_non_residen' => $request->session()->get('tur.harga.dewasa_non_residen'),
                'anak_non_residen' => $request->session()->get('tur.harga.anak_non_residen'),
                'balita_non_residen' => $request->session()->get('tur.harga.balita_non_residen'),
            ]);
        }

        $max_orang = collect([]);
        $min_orang = collect([]);
        $diskon_orang = collect([]);
        $tgl_start = collect([]);
        $tgl_end = collect([]);
        $discount = collect([]);

        if (!empty($request->session()->get('tur.harga.discount_group'))) {
            foreach ($request->session()->get('tur.harga.discount_group') as $key => $value) {
                $max_orang->push($value['max_orang']);
                $min_orang->push($value['min_orang']);
                $diskon_orang->push($value['diskon_orang']);
            }
        }


        if (count($request->session()->get('tur.harga.availability')) !== 0) {
            foreach ($request->session()->get('tur.harga.availability') as $key => $value) {
                $tgl_start->push($value['tgl_start']);
                $tgl_end->push($value['tgl_end']);
                $discount->push($value['discount']);
            }
        }

        $discount_product = Discount::create([
            'max_orang' => json_encode($max_orang),
            'min_orang' => json_encode($min_orang),
            'diskon_orang' => json_encode($diskon_orang),
            'tgl_start' => json_encode($tgl_start),
            'tgl_end' => json_encode($tgl_end),
            'discount' => json_encode($discount),
        ]);

        $judul_pilihan = collect([]);
        $deskripsi_pilihan = collect([]);
        $harga_pilihan = collect([]);
        $kewajiban_pilihan = collect([]);
        $nama_pilihan = collect([]);

        foreach ($request->session()->get('tur.pilihan_ekstra.pilihan') as $key => $value) {
            $judul_pilihan->push($value['judul_pilihan']);
            $deskripsi_pilihan->push($value['deskripsi_pilihan']);
            $harga_pilihan->push($value['harga_pilihan']);
            $kewajiban_pilihan->push($value['kewajiban_pilihan']);
            $nama_pilihan->push($value['nama$nama_pilihan']);
        }

        $pilihan = Pilihan::create([
            'judul_pilihan' => json_encode($judul_pilihan),
            'deskripsi_pilihan' => json_encode($deskripsi_pilihan),
            'harga_pilihan' => json_encode($harga_pilihan),
            'kewajiban_pilihan' => json_encode($kewajiban_pilihan),
            'nama_pilihan' => json_encode($nama_pilihan),
        ]);

        $gallery_itenenary = collect([]);
        $judul_itenenary = collect([]);
        $deskripsi_itenenary = collect([]);
        $tautan_video = collect([]);

        // $url =url('/').'/';
        // dd($request->session()->get('tur.itenerary'));
        foreach ($request->session()->get('tur.itenenary.additenenary') as $key => $value) {
            $judul_itenenary->push($value['judul_itenenary']);
            $deskripsi_itenenary->push($value['deskripsi_itenenary']);
            if ($value['images'] != '') {
                $split = explode(url('/').'/',$value['images']); 
                $gallery_itenenary->push($split[1]);
            }

            if ($value['images'] == '') {
                $gallery_itenenary->push('');
            }
            
            $tautan_video->push($value['tautan_video']);
        }
        // dd($deskripsi_itenenary);
        $itenenary = Itenerary::create([
            'judul_itenenary' => json_encode($judul_itenenary),
            'deskripsi_itenenary' => json_encode($deskripsi_itenenary),
            'gallery_itenenary' => json_encode($gallery_itenenary),
            'tautan_video' => json_encode($tautan_video),
        ]);

        // dd($itenenary);
        // $paket->id;
        // die;
        $productdetail = Productdetail::create([
            'product_id' => $request->session()->get('tur.add_listing.product_id'),
            'tag_location_1' => $request->session()->get('tur.add_listing.tag_location_1'),
            'tag_location_2' => $request->session()->get('tur.add_listing.tag_location_2') ,
            'province_id' => $request->session()->get('tur.add_listing.province_id') !=null ? $request->session()->get('tur.add_listing.province_id') :null,
            'regency_id' => $request->session()->get('tur.add_listing.regency_id') !=null ? $request->session()->get('tur.add_listing.regency_id') :null,
            'tag_location_3' => $request->session()->get('tur.add_listing.tag_location_3'),
            'tag_location_4' => $request->session()->get('tur.add_listing.tag_location_4'),
            'new_tag_location' => $request->session()->get('tur.add_listing.new_tag_location'),
            'district_id' => $request->session()->get('tur.add_listing.district_id') !=null? $request->session()->get('tur.add_listing.district_id') :null,
            'village_id' => $request->session()->get('tur.add_listing.village_id') !=null ? $request->session()->get('tur.add_listing.village_id'):null,
            'discount_id' => $discount_product->id,
            'paket_id' => $paket->id,
            'kategori_id' => $kategori->id,
            'harga_id' => $request->session()->get('tur.add_listing.tipe_tur') =='Open Trip' ? $paket->id :$price->id,
            'pilihan_id' => $pilihan->id,
            'itenenary_id' => $itenenary->id,
            'confirmation' => $request->session()->get('tur.add_listing.confirmation_radio_button'),
            'jml_hari' => $request->session()->get('tur.add_listing.jml_hari'),
            'jml_malam' => $request->session()->get('tur.add_listing.jml_malam'),
            'tipe_tur' => $request->session()->get('tur.add_listing.tipe_tur'),
            'makanan' => $request->session()->get('tur.add_listing.makanan'),
            'bahasa' => $request->session()->get('tur.add_listing.bahasa'),
            'tingkat_petualangan' => $request->session()->get('tur.add_listing.tingkat_petualangan'),
            'deskripsi' => $request->session()->get('tur.add_listing.deskripsi'),
            'paket_termasuk' => $request->session()->get('tur.add_listing.paket_termasuk'),
            'paket_tidak_termasuk' => $request->session()->get('tur.add_listing.paket_tidak_termasuk'),
            'catatan' => $request->session()->get('tur.add_listing.catatan'),
            'batas_pembayaran' => $request->session()->get('tur.batas.batas_pembayaran'),
            'kebijakan_pembatalan_sebelumnya' => $kebijakan_pembatalan_sebelumnya,
            'kebijakan_pembatalan_sesudah' => $kebijakan_pembatalan_sesudah,
            'kebijakan_pembatalan_potongan' => $kebijakan_pembatalan_potongan,
            'link_maps' => $request->session()->get('tur.peta.maps'),
            'izin_ekstra' => $request->session()->get('tur.pilihan_ekstra.ekstra'),
            'gallery' => json_encode($gallery),
            'thumbnail' => $request->session()->get('tur.peta.image_thumbnail'),
            'base_url' => url('/') . '/tur/' . $product_id->slug,
        ]);
        $request->session()->forget('tur');
        $request->session()->flash('url', url()->to('/') . '/tur/' . $product_id->slug);
        
        return redirect()->route('dashboardmyListing');
    }

    public function viewFaq(Request $request)
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'tour'
        ])->has('productdetail')->with('productdetail')->get();

        return view('BE.seller.tur.faq', compact('data', 'xstay', 'faqs'));
    }

    public function viewFaqCode(Request $request)
    {
        if ($request->product_tur == null) {
            return redirect()->route('tur.viewFaq');
        }
        
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $product = Product::where('id', $request->product_tur)->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'tour'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('id', $request->product_tur)->with('productdetail')->first();

        // return view('BE.seller.hotel.faq', compact('data', 'xstay', 'faqs', 'faq_content'));
        return redirect()->route('tur.viewFaq.edit', $product->product_code);
    }

    public function viewFaqEdit($code)
    {
        if ($code == null) {
            return redirect()->route('tur.viewFaq');
        }

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'tour'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('product_code', $code)->where('user_id', auth()->user()->id)->with('productdetail')->first();

        return view('BE.seller.tur.faq', compact('faqs', 'faq_content', 'data', 'xstay'));
    }

    public function viewFaqPost(Request $request, $id)
    {

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'tour'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('id', $id)->with('productdetail')->first();
        $faq_update = Productdetail::where('product_id', $id)->first();

        $faq_update->update([
            'faq' => $request->faq,
        ]);

        return redirect()->route('dashboardmyListing');
    }

    public function getTagLocation(){
        $data = [];

        $provinces = Province::all();
        $i=0;
        $x=0;
        $y=0;
        //foreach
        foreach($provinces as $province){
            // $data->push(
            //     [
            //         'provinces_id' =>$province->id,
            //         'provinces_name'=>$province->name
            //     ]
            // );
            $data[$i]['provinces_id'] =$province->id;
            $data[$i]['provinces_name']=$province->name;
           
            $regencies = Regency::where('province_id',$province->id)->get();

            foreach($regencies as $regency){
                // $data->push(
                //     [
                //         'regencies_id' =>$regency->id,
                //         'regencies_name'=>$regency->name
                //     ]
                // );
                $data[$i]['regency'][$x]['regencies_id'] =$regency->id;
                $data[$i]['regency'][$x]['regencies_name']=$regency->name;
                $x++;
                    
                $districts = District::where('regency_id',$regency->id)->groupBy('name')->get();

                foreach($districts as $district){
                    $data[$i]['district'][$y]['districts_id'] =$district->id;
                    $data[$i]['district'][$y]['districts_name']=$district->name;

                    $y++;
                }
            }

            $i++;
        }

        return $data;
    }

}
