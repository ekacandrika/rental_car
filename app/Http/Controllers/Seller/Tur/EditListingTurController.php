<?php

namespace App\Http\Controllers\Seller\Tur;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Paket;
use App\Models\Pilihan;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\Itenerary;
use App\Models\Kategori;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\MasterChoice;
use Illuminate\Support\Facades\DB;
use App\Models\detailWisata;

class EditListingTurController extends Controller
{
    public function showAddDetail(Request $request, $id)
    {
     
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with(['tagged'])->first();
        
        $productdetail = ProductDetail::where('product_id',$id)->first();
        
        $paket = Paket::where('id', $productdetail->paket_id)->first();
        $min_orang = null;
        $max_orang = null;
        
        $island = DB::select('select * from pulau');
        // dd($objek);

        $province =  Province::all();;
        // if ($request->session()->has('tur.add_listing')) {
            $kabupaten = Regency::where('id', $request->session()->get('tur.add_listing.regency_id'))->first();
            $kecamatan = District::where('id', $request->session()->get('tur.add_listing.district_id'))->first();
            $kelurahan = Village::where('id', $request->session()->get('tur.add_listing.village_id'))->first();
        // }else{
            $kota     = Regency::all();
            $objek    = detailWisata::all();
        // }

        // dd($provinsi);
        $listing = $detail->product_code;

        $fields = collect([]);
        $nama_paket = json_decode($paket['nama_paket']);
        $jam_paket = json_decode($paket['jam_paket']);
        $max_peserta = json_decode($paket['max_peserta']);
        $min_peserta = json_decode($paket['min_peserta']);

        if(isset($nama_paket)){
           foreach ($nama_paket as $key => $value) {
               $fields->push([
                   'nama_paket' => $nama_paket[$key],
                   'jam_paket' => $jam_paket[$key],
                   'max_peserta' => $max_peserta[$key],
                   'min_peserta' => $min_peserta[$key]
               ]);
           }
        }else{
            $min_orang = $min_peserta;
            $max_orang = $max_peserta;
        } 

        $tagged ="";

        if(isset($detail->tagged)){
            $countTagged = $detail->tagged->count();
            foreach($detail->tags as $key =>$value){
                // dump($value->name);
                $tagged .= $value->name;
                if($countTagged >1){
                    $tagged .=", ";
                }
            } 
        }

        // dd($tagged);

        $add_listing['product_id'] = $detail->id;
        $add_listing['product_name'] = $detail->product_name;
        $add_listing['confirmation'] = $productdetail->confirmation;
        $add_listing['confirmation_radio_button'] = $productdetail->confirmation;

        $add_listing['tag_location_1'] = $productdetail->tag_location_1;
        $add_listing['tag_location_2'] = $productdetail->tag_location_2;
        $add_listing['tag_location_3'] = $productdetail->tag_location_3;
        $add_listing['tag_location_4'] = $productdetail->tag_location_4;
        $add_listing['new_tag_location'] = isset($productdetail->new_tag_location) ? json_decode($productdetail->new_tag_location, true):null;
        $add_listing['tipe_tur'] = $productdetail->tipe_tur;
        $type = $productdetail->tipe_tur;

        $add_listing['paket'] = $fields;
        
        $add_listing['jml_hari'] = $productdetail->jml_hari;
        $add_listing['jml_malam'] = $productdetail->jml_malam;
        $add_listing['min_orang'] = $min_orang;
        $add_listing['max_orang'] = $max_orang;
        $add_listing['makanan'] = $productdetail->makanan;
        $add_listing['bahasa'] = $productdetail->bahasa;
        $add_listing['tingkat_petualangan'] = $productdetail->tingkat_petualangan;
        $add_listing['durasi'] = $productdetail->durasi;

        $add_listing['deskripsi'] = $productdetail->deskripsi;
        $add_listing['paket_termasuk'] = $productdetail->paket_termasuk;
        $add_listing['paket_tidak_termasuk'] = $productdetail->paket_tidak_termasuk;
        $add_listing['catatan'] = $productdetail->catatan;
        $add_listing['nama_kategori']=$tagged;

        $tur['itenenary'] = isset($productdetail->itenenary_id) ? $productdetail->itenenary_id : ''; 
        $tur['harga'] = isset($productdetail->harga_id) ? $productdetail->harga_id : ''; 
        $tur['batas'] = isset($productdetail->batas_pembayaran) ? $productdetail->batas_pembayaran : '';
        $tur['peta'] = isset($productdetail->link_maps) ? $productdetail->link_maps : '';
        $tur['pilihan_ekstra'] = isset($productdetail->pilihan_id) ? $productdetail->pilihan_id : '';
        
        $dynamic_tag = [];
        
        $i =0;
        $x =0;
        $z =0;
        $y =0;
        
        foreach($island as $p){
            
            $prov = Province::where('island_id',$p->id)->get();
            
            $dynamic_tag['pulau'][$i]['id_pulau'] = $p->id;
            $dynamic_tag['pulau'][$i]['nama_pulau'] =$p->island_name;
            
            foreach($prov as $pv){
                
                $dynamic_tag['pulau'][$i]['provinsi'][$x]['id_provinsi']=$pv->id;
                $dynamic_tag['pulau'][$i]['provinsi'][$x]['nama_provinsi']=$pv->name;
                
                $kab = Regency::where('province_id',$pv->id)->get();
                
                foreach($kab as $k){

                    $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['id_kabupaten']=$k->id;
                    $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['nama_kabupaten']=$k->name;


                    $wisata =detailWisata::where('kabupaten_id',$k->id)->get();
                    foreach($wisata as $w){
                       
                        $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['wisata'][$y]['id_wisata']=$w->id;
                        $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['wisata'][$y]['nama_wisata']=$w->namaWisata;
                       
                        $y++;
                    }

                    $z++;

                }    
                
                $x++;
            }

            $i++;
        }

        $dynamic_tag = collect($dynamic_tag);

        $isedit = true;
        // dd($add_listing['new_tag_location']);
        return view('BE.seller.tur.privateTrip.informasidasar', compact('province', 'island', 'kabupaten', 'kecamatan', 'kota', 'objek', 'kelurahan', 'detail', 'paket', 'data', 'xstay', 'listing', 'add_listing', 'tur', 'id', 'isedit', 'dynamic_tag'));
    }
    
    public function editAddDetailPrivate(Request $request, $id)
    {
    
        // dd($request->all());
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $paket = Paket::where('id', $detail->productdetail->paket_id)->first();

        $nama_paket = collect([]);
        $jam_paket = collect([]);
        $max_peserta = collect([]);
        $min_peserta = collect([]);

        if($request->status=='Open Trip'){
            foreach ($request->fields as $key => $value) {
                $nama_paket->push($value['nama_paket']);
                $jam_paket->push($value['jam_paket']);
                $max_peserta->push($value['max_peserta']);
                $min_peserta->push($value['min_peserta']);
            }
    
            $paket->update([
                'nama_paket' => json_encode($nama_paket),
                'jam_paket' => json_encode($jam_paket),
                'max_peserta' => json_encode($max_peserta),
                'min_peserta' => json_encode($min_peserta),
            ]);

        }else{
            $paket->update([
                'nama_paket' => isset($request->nama_paket) ? $request->nama_paket:null,
                'jam_paket' => isset($request->jam_paket) ? $request->jam_paket:null,
                'max_peserta' => $request->max_orang,
                'min_peserta' => $request->min_orang,
            ]); 
        }


        $tagCatagories = explode(',',$request->nama_kategori);
        $taged = $tagCatagories;

        $dynamic_tag_location = [];
        $dynamic_tag_location['results'] = $request->new_tag_location;

        $product_detail = $detail->productdetail()->update([
            'tag_location_1' => $request->tag_location_1,
            'tag_location_2' => $request->tag_location_2,
            'tag_location_3' => $request->tag_location_3,
            'tag_location_4' => $request->tag_location_4,
            'new_tag_location'=>json_encode($dynamic_tag_location),
            'confirmation' => $request->confirmation_radio_button,
            'durasi' => $request->durasi,
            'makanan' => $request->makanan,
            'bahasa' => $request->bahasa,
            'tingkat_petualangan' => $request->tingkat_petualangan,
            'deskripsi' => $request->deskripsi,
            'paket_termasuk' => $request->paket_termasuk,
            'paket_tidak_termasuk' => $request->paket_tidak_termasuk,
            'catatan' => $request->catatan,
        ]);

        
        $detail->update([
            'product_name' => $request->product_name,
        ]);

        if($request->nama_kategori){
            $detail->retag($tagCatagories);
        }else{
            $detail->untag();
        }

        return redirect()->route('tur.viewHargaEdit', $id);
    }

    public function editAddDetailOpen(Request $request, $id)
    {
        // dd($request->all());
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $paket = Paket::where('id', $detail->productdetail->paket_id)->first();

        $nama_paket = collect([]);
        $jam_paket = collect([]);
        $max_peserta = collect([]);
        $min_peserta = collect([]);

        foreach ($request->fields as $key => $value) {
            $nama_paket->push($value['nama_paket']);
            $jam_paket->push($value['jam_paket']);
            $max_peserta->push($value['max_peserta']);
            $min_peserta->push($value['min_peserta']);
        }

        $paket->update([
            'nama_paket' => json_encode($nama_paket),
            'jam_paket' => json_encode($jam_paket),
            'max_peserta' => json_encode($max_peserta),
            'min_peserta' => json_encode($min_peserta),
        ]);

        $tagCatagories = explode(',',$request->nama_kategori);
        $taged = $tagCatagories;

        $product_detail = $detail->productdetail()->update([
            'tag_location_1' => $request->tag_location_1,
            'tag_location_2' => $request->tag_location_2,
            'tag_location_3' => $request->tag_location_3,
            'tag_location_4' => $request->tag_location_4,
            'confirmation' => $request->confirmation_radio_button,
            'durasi' => $request->durasi,
            'makanan' => $request->makanan,
            'bahasa' => $request->bahasa,
            'tingkat_petualangan' => $request->tingkat_petualangan,
            'deskripsi' => $request->deskripsi,
            'paket_termasuk' => $request->paket_termasuk,
            'paket_tidak_termasuk' => $request->paket_tidak_termasuk,
            'catatan' => $request->catatan,
        ]);

        $detail->update([
            'product_name' => $request->product_name,
        ]);

        if($request->nama_kategori){
            $detail->retag($tagCatagories);
        }else{
            $detail->untag();
        }

        return redirect()->route('tur.viewItenenarydit', $id);
    }

    public function showEditItenenary(Request $request, $id)
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();
        
        $itene = Itenerary::where('id', $detail->productdetail->itenenary_id)->first();

        $itenenary = collect([]);

        $judul_itenenary = json_decode($itene['judul_itenenary']);
        $deskripsi_itenenary = json_decode($itene['deskripsi_itenenary']);
        $gallery_itenenary = json_decode($itene['gallery_itenenary']);
        $tautan_video = json_decode($itene['tautan_video']);

        foreach ($judul_itenenary as $key => $value) {
            $itenenary->push([
                'judul_itenenary' => $judul_itenenary[$key],
                'deskripsi_itenenary' => $deskripsi_itenenary[$key],
                'gallery_itenenary' => $gallery_itenenary[$key],
                'tautan_video' => $tautan_video[$key],
                'radio_itinenary'=> isset($gallery_itenenary[$key]) ? 'images':'videos'
            ]);
        }
        // dd($itenenary);
        $it['itenenary'] = $itenenary;

        $tur['itenenary'] = isset($detail->productdetail->itenenary_id) ? $detail->productdetail->itenenary_id : ''; 
        $tur['harga'] = isset($detail->productdetail->harga_id) ? $detail->productdetail->harga_id : ''; 
        $tur['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $tur['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $tur['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = true;

        return view('BE.seller.tur.privateTrip.itinerary', compact('it','data', 'xstay', 'tur', 'id', 'isedit'));
    }

    public function editAddDetailItenenary(Request $request, $id){
        // dd($request->all());
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();
    
        $itene = Itenerary::where('id', $detail->productdetail->itenenary_id)->first();

        $judul = json_decode($itene['judul_itenenary']);
        $deskripsi= json_decode($itene['deskripsi_itenenary']);
        $gallery = json_decode($itene['gallery_itenenary']);
        $video = json_decode($itene['tautan_video']);

        $judul_itenenary =collect([]);
        $deskripsi_itenenary =collect([]);
        $gallery_itenenary =collect([]);
        $tautan_video =collect([]);

        foreach($request->details as $key => $detail){

            // echo $gallery[$key];
            
            if(isset($detail['gallery_itenenary'])){
                $foto = time().'GalleryProductTurItenerary'.$key.'.'.$detail['gallery_itenenary']->getClientOriginalExtension();
                $tujuan_upload = 'Seller/Product/Tur/Gallery/';
    
                $lebar = Image::make($detail['gallery_itenenary'])->width();
                $lebar = $lebar * 50/100;

                // constraint
                Image::make($detail['gallery_itenenary'])->resize($lebar, null, function($constraint){
                    $constraint->aspectRatio();
                })->save($tujuan_upload.$foto);
            }
            
            $judul_itenenary->push($detail['judul_itenenary']);
            $deskripsi_itenenary->push($detail['deskripsi_itenenary']);
            $gallery_itenenary->push(isset($detail['gallery_itenenary']) ? $tujuan_upload.$foto :$gallery[$key]);
            $tautan_video->push(isset($detail['tautan_video']) ? $detail['tautan_video']:$video[$key]);
        }
    //    dd($gallery_itenenary); 
        $itene->update([
            'judul_itenenary'=>json_encode($judul_itenenary),
            'deskripsi_itenenary'=>json_encode($deskripsi_itenenary),
            'gallery_itenenary'=>json_encode($gallery_itenenary),
            'tautan_video'=>json_encode($tautan_video),
        ]);

        return redirect()->route('tur.viewHargaEdit', $id);
    }

    public function showEditHarga(Request $request, $id)
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();
    
        $diskon = Discount::where('id', $detail->productdetail->discount_id)->first();
            
        $type = $detail->productdetail->tipe_tur;
        $paket = Paket::where('id',$detail->productdetail->paket_id)->first();
       
        if($type=='Open Trip'){
            $harga = Price::where('paket_id', $detail->productdetail->paket_id)->get();
        }else{
            $harga = Price::where('id', $detail->productdetail->harga_id)->first();
        }

        $data   = [];
        $packet = collect([]);
        
        $nama_paket = json_decode($paket['nama_paket']);
        $jam_paket = json_decode($paket['jam_paket']);
        $max_peserta = json_decode($paket['max_peserta']);
        $min_peserta = json_decode($paket['min_peserta']);
        
        $discount_group = collect([]);
        $availability = collect([]);

        $min_orang = json_decode($diskon['min_orang']);
        $max_orang = json_decode($diskon['max_orang']);
        $diskon_orang = json_decode($diskon['diskon_orang']);

        $tgl_start = json_decode($diskon['tgl_start']);
        $tgl_end = json_decode($diskon['tgl_end']);
        $discount = json_decode($diskon['discount']);

        foreach ($max_orang as $key => $value) {
            $discount_group->push([
                'min_orang' => $min_orang[$key],
                'max_orang' => $max_orang[$key],
                'diskon_orang' => $diskon_orang[$key]
            ]);
        }

        foreach ($tgl_start as $key => $value) {
            $availability->push([
                'tgl_start' => $tgl_start[$key],
                'tgl_end' => $tgl_end[$key],
                'discount' => $discount[$key]
            ]);
        }

        if($type=='Open Trip'){

            foreach($nama_paket as $key => $value){
                $nama = str_replace(' ','_',$value);
                
                $packet->push([
                    'nama_paket'=>$value,
                    'jam_paket'=>$jam_paket[$key],
                    'max_peserta'=>$max_peserta[$key],
                    'min_peserta'=>$min_peserta[$key],
                ]);
                // $packet[]['nama_paket']=$nama_paket[$key];

                $data['harga'][$nama]['dewasa_residen'] = isset($harga[$key]['dewasa_residen']) ? $harga[$key]['dewasa_residen']:0;
                $data['harga'][$nama]['anak_residen'] = isset($harga[$key]['anak_residen']) ? $harga[$key]['anak_residen']:0;
                $data['harga'][$nama]['balita_residen'] = isset($harga[$key]['balita_residen']) ? $harga[$key]['balita_residen']:0;
                $data['harga'][$nama]['dewasa_non_residen'] = isset($harga[$key]['dewasa_non_residen']) ? $harga[$key]['dewasa_non_residen']:0;
                $data['harga'][$nama]['anak_non_residen'] = isset($harga[$key]['anak_non_residen']) ? $harga[$key]['anak_non_residen']:0;
                $data['harga'][$nama]['balita_non_residen'] = isset($harga[$key]['balita_non_residen']) ? $harga[$key]['balita_non_residen']:0;
            }

            $harga['paket']=$data;
            $details = collect([]);

            foreach($availability as $key => $v){
                // dump($packet[$key]);
                $details->push([
                    'tgl_start'=>$v['tgl_start'],
                    'tgl_end' => $v['tgl_end'],
                    'discount' => $v['discount'],
                    'paket'=>$packet,
                    'harga_paket'=>$data['harga'] ?? ''
                ]);
            }

            // dd($details);
            $harga['availability'] = $details;

        }else{
            $harga['dewasa_residen'] = $harga->dewasa_residen;
            $harga['anak_residen'] = $harga->anak_residen;
            $harga['balita_residen'] = $harga->balita_residen;
            $harga['dewasa_non_residen'] = $harga->dewasa_non_residen;
            $harga['anak_non_residen'] = $harga->anak_non_residen;
            $harga['balita_non_residen'] = $harga->balita_non_residen;
            $harga['availability'] = $availability;
        }
        
        $harga['discount_group'] = $discount_group;

        $tur['harga'] = isset($detail->productdetail->harga_id) ? $detail->productdetail->harga_id : ''; 
        $tur['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $tur['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $tur['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = true;

        return view('BE.seller.tur.privateTrip.harga', compact('harga', 'data', 'xstay', 'tur', 'id', 'isedit','type','packet'));
    }
    
    public function editHarga(Request $request, $id)
    {
        $data =[];
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])
        ->has('productdetail')->with('productdetail')->first();
        
        $paket = Paket::where('id',$detail->productdetail->paket_id)->first();        
        $nama_paket = json_decode($paket['nama_paket']);
        $type =$detail->productdetail->tipe_tur;
        
        if($type=='Open Trip'){
            $harga = Price::where('paket_id', $detail->productdetail->paket_id)->get();

            foreach($nama_paket as $key => $value){
                $nama  = str_replace(' ','_',$value);
                $price = $request->$nama;
                
                $data['dewasa_residen'] = $price['dewasa_residen'];
                $data['anak_residen'] = $price['anak_residen'];
                $data['balita_residen'] = $price['balita_residen'];
                $data['dewasa_non_residen'] = $price['dewasa_non_residen'] == 0 ? $price['dewasa_residen'] : $price['dewasa_non_residen'];
                $data['anak_non_residen'] = $price['anak_non_residen'] == 0 ? $price['anak_residen'] : $price['anak_non_residen'];
                $data['balita_non_residen'] = $price['balita_non_residen'] == 0 ? $price['balita_residen'] : $price['balita_non_residen'];

                if(isset($harga[$key])){
                    $harga_id = $harga[$key]->id;
                    Price::where('id',$harga_id)->update($data);
                }else{
                    $data['paket_id'] = $detail->productdetail->paket_id;
                    Price::create($data);
                }

            }
            // dd($harga);
            // die;
        }else{
            $harga = Price::where('id', $detail->productdetail->harga_id)->first();

            $harga->update([
                'dewasa_residen' => $request->dewasa_residen,
                'anak_residen' => $request->anak_residen,
                'balita_residen' => $request->balita_residen,
                'dewasa_non_residen' => $request->dewasa_non_residen == null ? $request->dewasa_residen : $request->dewasa_non_residen,
                'anak_non_residen' => $request->anak_non_residen == null ? $request->anak_residen : $request->anak_non_residen,
                'balita_non_residen' => $request->balita_non_residen == null ? $request->balita_residen : $request->balita_non_residen
            ]); 
        }

        // echo $id;die;
        
        return redirect()->route('tur.viewHargaEdit', $id);
    }

    public function editDiscountGroup(Request $request, $id)
    {
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();
        
        $diskon = Discount::where('id', $detail->productdetail->discount_id)->first();

        $max_orang = collect([]);
        $min_orang = collect([]);
        $diskon_orang = collect([]);

        if (!empty($request->fields)) {
            foreach ($request->fields as $key => $value) {
                $max_orang->push($value['max_orang']);
                $min_orang->push($value['min_orang']);
                $diskon_orang->push($value['diskon_orang']);
            }
        }        

        $diskon->update([
            'max_orang' => json_encode($max_orang),
            'min_orang' => json_encode($min_orang),
            'diskon_orang' => json_encode($diskon_orang),
        ]);

        return redirect()->route('tur.viewHargaEdit', $id);
    }

    public function editAvailability(Request $request, $id)
    {
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();
        
        $diskon = Discount::where('id', $detail->productdetail->discount_id)->first();

        $tgl_start = collect([]);
        $tgl_end = collect([]);
        $discount = collect([]);    

        if (isset($request->details)) {
            if (count($request->details) !== 0) {
                foreach ($request->details as $key => $value) {
                    $tgl_start->push($value['tgl_start']);
                    $tgl_end->push($value['tgl_end']);
                    $discount->push($value['discount']);
                }
            }
        }
                
        $diskon->update([
            'tgl_start' => json_encode($tgl_start),
            'tgl_end' => json_encode($tgl_end),
            'discount' => json_encode($discount),
        ]);
                
        return redirect()->route('tur.viewBatasEdit', $id);
    }

    public function showEditBatas(Request $request, $id)
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $fields = collect([]);
        $kebijakan_pembatalan_sebelumnya = json_decode($detail->productdetail->kebijakan_pembatalan_sebelumnya);
        $kebijakan_pembatalan_sesudah = json_decode($detail->productdetail->kebijakan_pembatalan_sesudah);
        $kebijakan_pembatalan_potongan = json_decode($detail->productdetail->kebijakan_pembatalan_potongan);

        foreach ($kebijakan_pembatalan_sebelumnya as $key => $value) {
            $fields->push([
                'kebijakan_pembatalan_sebelumnya' => $kebijakan_pembatalan_sebelumnya[$key],
                'kebijakan_pembatalan_sesudah' => $kebijakan_pembatalan_sesudah[$key],
                'kebijakan_pembatalan_potongan' => $kebijakan_pembatalan_potongan[$key],
            ]);
        }

        $batas['batas_pembayaran'] = $detail->productdetail->batas_pembayaran;
        $batas['pembatalan'] = $fields;

        $tur['itenerary'] = isset($detail->productdetail->itenenary_id) ? $detail->productdetail->itenenary_id : ''; 
        $tur['harga'] = isset($detail->productdetail->harga_id) ? $detail->productdetail->harga_id : ''; 
        $tur['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $tur['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $tur['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = 'true';

        return view('BE.seller.tur.privateTrip.batasbayar', compact('batas', 'data', 'xstay', 'tur', 'isedit', 'id'));
    }

    public function editBatas(Request $request, $id)
    {
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $kebijakan_pembatalan_sebelumnya = collect([]);
        $kebijakan_pembatalan_sesudah = collect([]);
        $kebijakan_pembatalan_potongan = collect([]);    

        if (isset($request->fields)) {
            if (count($request->fields) !== 0) {
                foreach ($request->fields as $key => $value) {
                    $kebijakan_pembatalan_sebelumnya->push($value['kebijakan_pembatalan_sebelumnya']);
                    $kebijakan_pembatalan_sesudah->push($value['kebijakan_pembatalan_sesudah']);
                    $kebijakan_pembatalan_potongan->push($value['kebijakan_pembatalan_potongan']);
                }
            }
        }

        $product_detail = $detail->productdetail()->update([
            'batas_pembayaran' => $request->batas_pembayaran,
            'kebijakan_pembatalan_sebelumnya' => json_encode($kebijakan_pembatalan_sebelumnya),
            'kebijakan_pembatalan_sesudah' => json_encode($kebijakan_pembatalan_sesudah),
            'kebijakan_pembatalan_potongan' => json_encode($kebijakan_pembatalan_potongan)
        ]);

        return redirect()->route('tur.viewPetaEdit', $id);
    }

    public function showEditPeta(Request $request, $id)
    {

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $images_gallery = collect([]);
        $gallery = json_decode($detail->productdetail->gallery);
        
        if($gallery){
            foreach ($gallery as $key => $value) {
                $images_gallery->push(
                    $gallery[$key]
                );
            }
        }


        $peta['maps'] = $detail->productdetail->link_maps;
        $peta['images_gallery'] = $images_gallery;
        $peta['image_thumbnail'] = $detail->productdetail->thumbnail;

        $tur['itenenary'] = isset($detail->productdetail->itenenary_id) ? $detail->productdetail->itenenary_id : ''; 
        $tur['harga'] = isset($detail->productdetail->harga_id) ? $detail->productdetail->harga_id : ''; 
        $tur['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $tur['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $tur['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = true;

        return view('BE.seller.tur.privateTrip.mapsfoto', compact('peta', 'data', 'xstay', 'id', 'tur', 'isedit'));
    }

    public function editPetaPhoto(Request $request, $id)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'maps' => 'required',
            'image_thumbnail' => 'required_without_all:saved_thumbnail|image|mimes:jpeg,png,jpg|max:2048',
        ],[
            'maps.required' => 'Link peta google maps wajib diisi!',
            'image_thumbnail.required_without_all' => 'Foto Thumbnail wajib diisi!',
            'image_thumbnail.mimes' => 'Foto harus memiliki ekstensi (jpg, jpeg, atau png)',
            'image_thumbnail.max' => 'Foto maksimum berukuran 2MB',
        ]);

        if ($validator->fails())
        {
            return redirect()->route('tur.viewPetaEdit', $id)->withErrors($validator);
        }

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();

        $image_thumbnail = isset($detail->productdetail->thumbnail) ? $detail->productdetail->thumbnail : '';

        if ($request->hasFile('image_thumbnail')) {
            $image = $request->file('image_thumbnail');
        
            $new_foto = time() . 'GalleryProductTur.' . $image->getClientOriginalExtension();
            $tujuan_upload = 'Seller/Product/Tur/Thumbnail/';
            
            $lebar_foto = Image::make($image)->width();
            $lebar_foto= $lebar_foto * 50 / 100;

            Image::make($image)->resize($lebar_foto, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($tujuan_upload . $new_foto);

            $image_thumbnail = $tujuan_upload . $new_foto;

            //delete old image
            if (file_exists($detail->productdetail->thumbnail)) {
                unlink($detail->productdetail->thumbnail);
            }
        }

        $product_detail = $detail->productdetail()->update([
            'link_maps' => $request->maps,
            'thumbnail' => $image_thumbnail,
        ]);

        return redirect()->route('tur.viewPilihanEkstraEdit', $id);
    }

    public function editPetaPhotoGallery(Request $request, $id)
    {
        // Validate request
        $validator = Validator::make($request->all(), [
            // Validate when saved gallery not exist
            'images_gallery' => 'required_without_all:saved_gallery',  
            'images_gallery.*' => 'mimes:jpeg,png,jpg|max:2048'
        ],[
            'images_gallery.required_without_all' => 'Foto Gallery wajib diisi!',
            'images_gallery.*.mimes' => "Foto harus memiliki ekstensi (jpg, jpeg, atau png)",
            'images_gallery.*.max' => 'Foto maksimum berukuran 2MB',
        ]);

        // Return when validate fails on one or more rule(s)
        if ($validator->fails())
        {
            return array(
                'status' => 0,
                'message' => $validator->messages()->first(),
            );
        }

        // Execute these code when validate success
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id
        ])->has('productdetail')->with('productdetail')->first();

        $fields = collect([]);

        // Save all new images gallery
        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {
                $new_foto = time() . 'GalleryProductTur'. $key . '.' . $value->getClientOriginalExtension();
                $tujuan_upload = 'Seller/Product/Tur/Gallery/';

                $lebar_foto = Image::make($value)->width();
                $lebar_foto = $lebar_foto * 50 / 100;

                Image::make($value)->resize($lebar_foto, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($tujuan_upload . $new_foto);

                $images_gallery = $tujuan_upload . $new_foto;

                $fields->push(
                    $images_gallery,
                );
            }
        }

        // Delete all existing saved gallery
        if (isset($detail->productdetail->gallery) && !$request->has('saved_gallery')) {
            foreach (json_decode($detail->productdetail->gallery) as $key => $value) {
                if (file_exists($value[0])) {
                    unlink($value[0]);
                }
            }
        }

        // Delete some saved gallery iamges
        if (isset($detail->productdetail->gallery) && $request->has('saved_gallery')) {

            $differences = array_diff(json_decode($detail->productdetail->gallery), $request->saved_gallery);

            foreach ($differences as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }

            foreach ($request->saved_gallery as $key => $value) {
                $fields->push(
                    $value,
                );
            }
        }

        $product_detail = $detail->productdetail()->update([
            'gallery' => json_encode($fields),
        ]);

        return array(
            'status' => 1,
            'message' => 'Foto Gallery berhasil diperbarui.'
        );
    }

    public function showEditPilihanEkstra(Request $request, $id)
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();
        
        $pilihan = Pilihan::where('id', $detail->productdetail->pilihan_id)->first();
        $masterPilihan = MasterChoice::where("product_type",'tour')->get();
        $pilihan_ekstra['ekstra_radio_button'] = $detail->productdetail->izin_ekstra;

        // dd($pilihan);

        $fields = collect([]);
        $judul_pilihan = json_decode($pilihan->judul_pilihan);
        $deskripsi_pilihan = json_decode($pilihan->deskripsi_pilihan);
        $harga_pilihan = json_decode($pilihan->harga_pilihan);
        $kewajiban_pilihan = json_decode($pilihan->kewajiban_pilihan);
        $nama_pilihan = json_decode($pilihan->nama_pilihan);

        // dd($pilihan->nama_pilihan);

        foreach ($judul_pilihan as $key => $value) {
            $fields->push([
                'judul_pilihan' => $judul_pilihan[$key],
                'deskripsi_pilihan' => $deskripsi_pilihan[$key],
                'harga_pilihan' => $harga_pilihan[$key],
                'kewajiban_pilihan' => $kewajiban_pilihan[$key],
                'name'=>isset($nama_pilihan[$key]) ?$nama_pilihan[$key]:null
            ]);
        }

        $pilihan_ekstra['pilihan_ekstra'] = $fields;

        $tur['harga'] = isset($detail->productdetail->harga_id) ? $detail->productdetail->harga_id : ''; 
        $tur['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $tur['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $tur['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = true;

        return view('BE.seller.tur.privateTrip.pilihanekstra', compact('pilihan_ekstra', 'data', 'xstay', 'tur', 'isedit', 'id','masterPilihan'));
    }

    public function editPilihanEkstra(Request $request, $id)
    {
        // dd($request->fields);
        if(!$request->fields){
            return redirect()->back()->with('error', 'Harap mengisi pilihan'); 
        }
      
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id])->has('productdetail')->with('productdetail')->first();
        
        $pilihan = Pilihan::where('id', $detail->productdetail->pilihan_id)->first();

        $detail->productdetail()->update([
            'izin_ekstra' => $request->ekstra_radio_button,
        ]);

        $judul_pilihan = collect([]);
        $deskripsi_pilihan = collect([]);
        $harga_pilihan = collect([]);   
        $kewajiban_pilihan = collect([]);    
        $nama_pilihan = collect([]);    


        if (isset($request->fields)) {
            if (count($request->fields) !== 0) {
                foreach ($request->fields as $key => $value) {
                    $judul_pilihan->push(isset($value['nama_asuransi']) ? $value['nama_asuransi']:$value['nama_hotel']);
                    $deskripsi_pilihan->push($value['deskripsi_pilihan']);
                    $harga_pilihan->push($value['harga_pilihan']);
                    $kewajiban_pilihan->push($value['kewajiban_pilihan']);
                    $nama_pilihan->push($value['nama_pilihan']);
                }
            }
        }

        $pilihan_detail = $pilihan->update([
            'judul_pilihan' => json_encode($judul_pilihan),
            'deskripsi_pilihan' => json_encode($deskripsi_pilihan),
            'harga_pilihan' => json_encode($harga_pilihan),
            'kewajiban_pilihan' => json_encode($kewajiban_pilihan),
            'nama_pilihan'=>json_encode($nama_pilihan)
        ]);
        
        // dd()

        return redirect()->route('tur.viewFaq');
    }

    public function getTagLocation(){
        $data = [];

        $provinces = Province::all();
        $i=0;
        $x=0;
        $y=0;
        //foreach
        foreach($provinces as $province){
            // $data->push(
            //     [
            //         'provinces_id' =>$province->id,
            //         'provinces_name'=>$province->name
            //     ]
            // );
            $data[$i]['provinces_id'] =$province->id;
            $data[$i]['provinces_name']=$province->name;
           
            $regencies = Regency::where('province_id',$province->id)->get();

            foreach($regencies as $regency){
                // $data->push(
                //     [
                //         'regencies_id' =>$regency->id,
                //         'regencies_name'=>$regency->name
                //     ]
                // );
                $data[$i]['regency'][$x]['regencies_id'] =$regency->id;
                $data[$i]['regency'][$x]['regencies_name']=$regency->name;
                $x++;
                    
                $districts = District::where('regency_id',$regency->id)->groupBy('name')->get();

                foreach($districts as $district){
                    $data[$i]['district'][$y]['districts_id'] =$district->id;
                    $data[$i]['district'][$y]['districts_name']=$district->name;

                    $y++;
                }
            }

            $i++;
        }

        return $data;
    }
}
