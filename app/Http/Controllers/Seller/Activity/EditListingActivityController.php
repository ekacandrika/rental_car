<?php

namespace App\Http\Controllers\Seller\Activity;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Paket;
use App\Models\Kategori;
use App\Models\Pilihan;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\detailWisata;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EditListingActivityController extends Controller
{
    //
    public function showAddDetail(Request $request, $id)
    {

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id
        ])->has('productdetail')->with('productdetail')->first();

        $paket = Paket::where('id', $detail->productdetail->paket_id)->first();

        $pulau     = DB::select("select * from pulau");
        $provinsi  = Province::all();
        $kabupaten = Regency::all();
        $objek     = detailWisata::all();$kecamatan = District::where('id', $detail->productdetail->district_id)->first();
        $kelurahan = Village::where('id', $detail->productdetail->village_id)->first();

        $listing = $detail->product_code;

        $fields = collect([]);
        $nama_paket = json_decode($paket['nama_paket']);
        $jam_paket = json_decode($paket['jam_paket']);
        $max_peserta = json_decode($paket['max_peserta']);
        $min_peserta = json_decode($paket['min_peserta']);

        $dynamic_tag = [];
        
        $i =0;
        $x =0;
        $z =0;
        $y =0;

        foreach($pulau as $p){
            
            $prov = Province::where('island_id',$p->id)->get();
            
            $dynamic_tag['pulau'][$i]['id_pulau'] = $p->id;
            $dynamic_tag['pulau'][$i]['nama_pulau'] =$p->island_name;
            
            foreach($prov as $pv){
                
                $dynamic_tag['pulau'][$i]['provinsi'][$x]['id_provinsi']=$pv->id;
                $dynamic_tag['pulau'][$i]['provinsi'][$x]['nama_provinsi']=$pv->name;
                
                $kab = Regency::where('province_id',$pv->id)->get();
                
                foreach($kab as $k){

                    $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['id_kabupaten']=$k->id;
                    $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['nama_kabupaten']=$k->name;


                    $wisata =detailWisata::where('kabupaten_id',$k->id)->get();
                    foreach($wisata as $w){
                       
                        $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['wisata'][$y]['id_wisata']=$w->id;
                        $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['wisata'][$y]['nama_wisata']=$w->namaWisata;
                       
                        $y++;
                    }

                    $z++;

                }    
                
                $x++;
            }

            $i++;
        }

        $dynamic_tag = collect($dynamic_tag);

        foreach ($nama_paket as $key => $value) {
            $fields->push([
                'nama_paket' => $nama_paket[$key],
                'jam_paket' => $jam_paket[$key],
                'max_peserta' => $max_peserta[$key],
                'min_peserta' => $min_peserta[$key]
            ]);
        }

        $add_listing['product_name'] = $detail->product_name;
        $add_listing['confirmation'] = $detail->productdetail->confirmation;

        $add_listing['pulau_id'] = $detail->productdetail->tag_location_1;
        $add_listing['provinsi_id'] = $detail->productdetail->tag_location_2;
        $add_listing['kabupaten_id'] = $detail->productdetail->tag_location_3;
        $add_listing['objek_id'] = $detail->productdetail->tag_location_4;
        $add_listing['new_tag_location'] = $detail->productdetail->new_tag_location;

        $add_listing['paket'] = $fields;

        $add_listing['ringkasan'] = $detail->productdetail->ringkasan;
        $add_listing['deskripsi_wisata'] = $detail->productdetail->deskripsi;
        $add_listing['paket_termasuk'] = $detail->productdetail->paket_termasuk;
        $add_listing['paket_tidak_termasuk'] = $detail->productdetail->paket_tidak_termasuk;
        $add_listing['catatan'] = $detail->productdetail->catatan;

        $activity['harga'] = isset($detail->productdetail->harga_id) ? $detail->productdetail->harga_id : '';
        $activity['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $activity['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $activity['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = 'true';

        return view('BE.seller.activity.add-listing', compact('pulau', 'provinsi', 'kabupaten', 'kecamatan', 'kelurahan', 'objek', 'detail', 'paket', 'data', 'xstay', 'listing', 'add_listing', 'activity', 'id', 'isedit', 'dynamic_tag'));
    }

    public function editAddDetail(Request $request, $id)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'product_name' => 'required',
            'confirmation_radio_button' => 'required',
            'pulau' => 'required',
            'provinsi' => 'required',
            'kabupaten' => 'required',
            'objek' => 'required',
            'fields.*.nama_paket' => 'required',
            'fields.*.jam_paket' => 'required',
            'fields.*.min_peserta' => 'required|numeric|lte:fields.*.max_peserta',
            'fields.*.max_peserta' => 'required|numeric|gte:fields.*.min_peserta',
            'ringkasan' => 'required',
            'deskripsi_wisata' => 'required',
            'paket_termasuk' => 'required',
            'paket_tidak_termasuk' => 'required'
        ],[
            'product_name.required' => 'Judul wajib diisi!',
            'confirmation_radio_button.required' => 'Konfirmasi paket wajib diisi!',
            'pulau.required' => 'Pulau wajib diisi!',
            'provinsi.required' => 'Provinsi wajib diisi!',
            'kabupaten.required' => 'Kabupaten wajib diisi!',
            'objek.required' => 'Objek Wisata wajib diisi!',
            'fields.*.nama_paket.required' => 'Nama Paket wajib diisi!',
            'fields.*.jam_paket.required' => 'Jam Paket wajib diisi!',
            'fields.*.min_peserta.required' => 'Peserta Min wajib diisi!',
            'fields.*.max_peserta.required' => 'Peserta Max wajib diisi!',
            'fields.*.min_peserta.numeric' => 'Peserta Min harus berupa angka!',
            'fields.*.max_peserta.numeric' => 'Peserta Max harus berupa angka!',
            'fields.*.min_peserta.lte' => 'Peserta Min harus lebih kecil dari peserta max!',
            'fields.*.max_peserta.gte' => 'Peserta Max harus lebih besar dari peserta min!',
            'ringkasan.required' => 'Ringkasan wajib diisi!',
            'deskripsi_wisata.required' => 'Deskripsi Activity wajib diisi!',
            'paket_termasuk.required' => 'Paket Termasuk wajib diisi!',
            'paket_tidak_termasuk.required' => 'Paket Tidak Termasuk wajib diisi!',
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id
        ])->has('productdetail')->with('productdetail')->first();

        $paket = Paket::where('id', $detail->productdetail->paket_id)->first();

        $nama_paket = collect([]);
        $jam_paket = collect([]);
        $max_peserta = collect([]);
        $min_peserta = collect([]);

        foreach ($request->fields as $key => $value) {
            $nama_paket->push($value['nama_paket']);
            $jam_paket->push($value['jam_paket']);
            $max_peserta->push($value['max_peserta']);
            $min_peserta->push($value['min_peserta']);
        }

        $paket->update([
            'nama_paket' => json_encode($nama_paket),
            'jam_paket' => json_encode($jam_paket),
            'max_peserta' => json_encode($max_peserta),
            'min_peserta' => json_encode($min_peserta),
        ]);

        $new_tag_location = [];
        $new_tag_location['results'] = $request->new_tag_location;

        $product_detail = $detail->productdetail()->update([
            'tag_location_1' => $request->pulau,
            'tag_location_2' => $request->provinsi,
            'tag_location_3' => $request->kabupaten,
            'tag_location_4' => $request->objek,
            'new_tag_location' => json_encode($new_tag_location),
            'confirmation' => $request->confirmation_radio_button,
            'ringkasan' => $request->ringkasan,
            'deskripsi' => $request->deskripsi_wisata,
            'paket_termasuk' => $request->paket_termasuk,
            'paket_tidak_termasuk' => $request->paket_tidak_termasuk,
            'catatan' => $request->catatan,
        ]);

        $detail->update([
            'product_name' => $request->product_name,
        ]);

        return redirect()->route('activity.viewHargaEdit', $id);
    }

    public function showEditHarga(Request $request, $id)
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id
        ])->has('productdetail')->with('productdetail')->first();

        $harga = Price::where('id', $detail->productdetail->harga_id)->first();
        $diskon = Discount::where('id', $detail->productdetail->discount_id)->first();

        $discount_group = collect([]);
        $availability = collect([]);

        $min_orang = json_decode($diskon['min_orang']);
        $max_orang = json_decode($diskon['max_orang']);
        $diskon_orang = json_decode($diskon['diskon_orang']);

        $tgl_start = json_decode($diskon['tgl_start']);
        $tgl_end = json_decode($diskon['tgl_end']);
        $discount = json_decode($diskon['discount']);

        foreach ($max_orang as $key => $value) {
            $discount_group->push([
                'min_orang' => $min_orang[$key],
                'max_orang' => $max_orang[$key],
                'diskon_orang' => $diskon_orang[$key]
            ]);
        }

        foreach ($tgl_start as $key => $value) {
            $availability->push([
                'tgl_start' => $tgl_start[$key],
                'tgl_end' => $tgl_end[$key],
                'discount' => $discount[$key]
            ]);
        }

        $harga['dewasa_residen'] = $harga->dewasa_residen;
        $harga['anak_residen'] = $harga->anak_residen;
        $harga['balita_residen'] = $harga->balita_residen;
        $harga['dewasa_non_residen'] = $harga->dewasa_non_residen;
        $harga['anak_non_residen'] = $harga->anak_non_residen;
        $harga['balita_non_residen'] = $harga->balita_non_residen;
        $harga['discount_group'] = $discount_group;
        $harga['availability'] = $availability;

        $activity['harga'] = isset($detail->productdetail->harga_id) ? $detail->productdetail->harga_id : '';
        $activity['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $activity['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $activity['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = 'true';

        return view('BE.seller.activity.harga-listing', compact('harga', 'data', 'xstay', 'activity', 'id', 'isedit'));
    }

    public function editHarga(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'dewasa_residen' => 'required|numeric|gte:0',
            'anak_residen' => 'required|numeric|gte:0',
            'balita_residen' => 'required|numeric|gte:0',
            'dewasa_non_residen' => 'sometimes|nullable|numeric|gte:0',
            'anak_non_residen' => 'sometimes|nullable|numeric|gte:0',
            'balita_non_residen' => 'sometimes|nullable|numeric|gte:0'
        ],[
            'dewasa_residen.required' => 'Harga Dewasa Residen wajib diisi!',
            'anak_residen.required' => 'Harga Anak Residen wajib diisi!',
            'balita_residen.required' => 'Harga Balita Residen wajib diisi!',
            'dewasa_residen.numeric' => 'Harga Dewasa Residen harus berupa angka!',
            'anak_residen.numeric' => 'Harga Anak Residen harus berupa angka!',
            'balita_residen.numeric' => 'Harga Balita Residen harus berupa angka!',
            'dewasa_non_residen.numeric' => 'Harga Dewasa non Residen harus berupa angka!',
            'anak_non_residen.numeric' => 'Harga Anak non Residen harus berupa angka!',
            'balita_non_residen.numeric' => 'Harga Balita non Residen harus berupa angka!',
            'dewasa_residen.gte' => 'Harga Dewasa Residen minimal 0',
            'anak_residen.gte' => 'Harga Anak Residen minimal 0',
            'balita_residen.gte' => 'Harga Balita Residen minimal 0',
            'dewasa_non_residen.gte' => 'Harga Dewasa non Residen minimal 0',
            'anak_non_residen.gte' => 'Harga Anak non Residen minimal 0',
            'balita_non_residen.gte' => 'Harga Balita non Residen minimal 0',
        ]);

        if ($validator->fails())
        {
            return redirect()->route('activity.viewHargaEdit', $id)->withErrors($validator, 'harga');
        }

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id
        ])->has('productdetail')->with('productdetail')->first();

        $harga = Price::where('id', $detail->productdetail->harga_id)->first();
        $harga->update([
            'dewasa_residen' => $request->dewasa_residen,
            'anak_residen' => $request->anak_residen,
            'balita_residen' => $request->balita_residen,
            'dewasa_non_residen' => $request->dewasa_non_residen == null ? $request->dewasa_residen : $request->dewasa_non_residen,
            'anak_non_residen' => $request->anak_non_residen == null ? $request->anak_residen : $request->anak_non_residen,
            'balita_non_residen' => $request->balita_non_residen == null ? $request->balita_residen : $request->balita_non_residen
        ]);

        return redirect()->route('activity.viewHargaEdit', $id);
    }

    public function editDiscountGroup(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'fields.*.min_orang' => 'required|integer|lte:fields.*.max_orang',
            'fields.*.max_orang' => 'required|integer|gte:fields.*.min_orang',
            'fields.*.diskon_orang' => 'required|numeric|gt:0',
        ],[
            'fields.*.min_orang.required' => 'Minimal jumlah orang wajib diisi!',
            'fields.*.max_orang.required' => 'Maksimal jumlah orang wajib diisi!',
            'fields.*.diskon_orang.required' => 'Diskon wajib diisi!',
            'fields.*.min_orang.integer' => 'Minimal jumlah orang harus bilangan bulat!',
            'fields.*.max_orang.integer' => 'Maksimal jumlah orang harus bilangan bulat!',
            'fields.*.diskon_orang.numeric' => 'Diskon harus berupa angka! (Desimal gunakan titik)',
            'fields.*.min_orang.lte' => 'Minimal orang harus lebih besar dari maksimal orang!',
            'fields.*.max_orang.gte' => 'Maksimal orang harus lebih kecil dari minimal orang!',
            'fields.*.diskon_orang.gt' => 'Diskon harus lebih besar dari 0!'
        ]);

        if ($validator->fails())
        {
            return redirect()->route('activity.viewHargaEdit', $id)->withErrors($validator, 'diskon_orang');
        }

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id
        ])->has('productdetail')->with('productdetail')->first();

        $diskon = Discount::where('id', $detail->productdetail->discount_id)->first();

        $max_orang = collect([]);
        $min_orang = collect([]);
        $diskon_orang = collect([]);

        if (!empty($request->fields)) {
            foreach ($request->fields as $key => $value) {
                $max_orang->push($value['max_orang']);
                $min_orang->push($value['min_orang']);
                $diskon_orang->push($value['diskon_orang']);
            }
        }

        $diskon->update([
            'max_orang' => json_encode($max_orang),
            'min_orang' => json_encode($min_orang),
            'diskon_orang' => json_encode($diskon_orang),
        ]);

        return redirect()->route('activity.viewHargaEdit', $id);
    }

    public function editAvailability(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'details.*.tgl_start' => 'required|date|before_or_equal:details.*.tgl_end',
            'details.*.tgl_end' => 'required|date|after_or_equal:details.*.tgl_start',
            'details.*.discount' => 'required|numeric',
        ],[
            'details.*.tgl_start.required' => 'Dari tanggal wajib diisi!',
            'details.*.tgl_end.required' => 'Sampai tanggal wajib diisi!',
            'details.*.discount.required' => 'Diskon atau penambahan biaya wajib diisi!',
            'details.*.tgl_start.date' => "Pastikan (Dari tanggal) terisi dengan benar",
            'details.*.tgl_end.date' => 'Pastikan (Sampai tanggal) terisi dengan benar',
            'details.*.discount.numeric' => 'Diskon atau penambahan biaya harus berupa angka!',
            'details.*.tgl_start.before_or_equal' => 'Pastikan tanggal mulai harus lebih kecil dari tanggal selesai!',
            'details.*.tgl_end.after_or_equal' => 'Pastikan Tanggal selesai harus lebih besar dari tanggal mulai!',
        ]);

        if ($validator->fails())
        {
            return redirect()->route('activity.viewHargaEdit')->withErrors($validator, 'diskon_ketersediaan');
        }

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id
        ])->has('productdetail')->with('productdetail')->first();

        $diskon = Discount::where('id', $detail->productdetail->discount_id)->first();

        $tgl_start = collect([]);
        $tgl_end = collect([]);
        $discount = collect([]);

        if (isset($request->details)) {
            if (count($request->details) !== 0) {
                foreach ($request->details as $key => $value) {
                    $tgl_start->push($value['tgl_start']);
                    $tgl_end->push($value['tgl_end']);
                    $discount->push($value['discount']);
                }
            }
        }

        $diskon->update([
            'tgl_start' => json_encode($tgl_start),
            'tgl_end' => json_encode($tgl_end),
            'discount' => json_encode($discount),
        ]);

        return redirect()->route('activity.viewBatasEdit', $id);
    }

    public function showEditBatas(Request $request, $id)
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id
        ])->has('productdetail')->with('productdetail')->first();

        $fields = collect([]);
        $kebijakan_pembatalan_sebelumnya = json_decode($detail->productdetail->kebijakan_pembatalan_sebelumnya);
        $kebijakan_pembatalan_sesudah = json_decode($detail->productdetail->kebijakan_pembatalan_sesudah);
        $kebijakan_pembatalan_potongan = json_decode($detail->productdetail->kebijakan_pembatalan_potongan);

        foreach ($kebijakan_pembatalan_sebelumnya as $key => $value) {
            $fields->push([
                'kebijakan_pembatalan_sebelumnya' => $kebijakan_pembatalan_sebelumnya[$key],
                'kebijakan_pembatalan_sesudah' => $kebijakan_pembatalan_sesudah[$key],
                'kebijakan_pembatalan_potongan' => $kebijakan_pembatalan_potongan[$key],
            ]);
        }

        $batas['batas_pembayaran'] = $detail->productdetail->batas_pembayaran;
        $batas['pembatalan'] = $fields;

        $activity['harga'] = isset($detail->productdetail->harga_id) ? $detail->productdetail->harga_id : '';
        $activity['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $activity['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $activity['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = 'true';

        return view('BE.seller.activity.batas-listing', compact('batas', 'data', 'xstay', 'activity', 'isedit', 'id'));
    }

    public function editBatas(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'batas_pembayaran' => 'required|integer',
            'fields.*.kebijakan_pembatalan_sebelumnya' => 'required|integer|lt:batas_pembayaran|gte:fields.*.kebijakan_pembatalan_sesudah',
            'fields.*.kebijakan_pembatalan_sesudah' => 'required|integer|lt:batas_pembayaran|lte:fields.*.kebijakan_pembatalan_sebelumnya',
            'fields.*.kebijakan_pembatalan_potongan' => 'required|integer',
        ],[
            'batas_pembayaran.required' => 'Batas pembayaran wajib diisi!',
            'batas_pembayaran.integer' => 'Pastikan batas pembayaran berupa bilangan bulat',
            // 'batas_pembayaran.gte' => 'Batas pembayaran harus lebih besar daripada batas waktu pembatalan',
            'fields.*.kebijakan_pembatalan_sebelumnya.required' => 'Data wajib diisi!',
            'fields.*.kebijakan_pembatalan_sesudah.required' => 'Data wajib diisi!',
            'fields.*.kebijakan_pembatalan_potongan.required' => 'Potongan wajib diisi!',
            'fields.*.kebijakan_pembatalan_sebelumnya.integer' => "Pastikan jumlah hari kebijakan pembatalan berupa bilangan bulat",
            'fields.*.kebijakan_pembatalan_sesudah.integer' => 'Pastikan jumlah hari kebijakan pembatalan berupa bilangan bulat',
            'fields.*.kebijakan_pembatalan_potongan.integer' => 'Pastikan potongan berupa bilangan bulat',
            'fields.*.kebijakan_pembatalan_sebelumnya.lt' => 'Batas pembayaran harus lebih besar daripada batas waktu pembatalan',
            'fields.*.kebijakan_pembatalan_sebelumnya.gte' => 'Nilai (hari sebelumnya sampai) harus lebih besar daripada nilai (hari sebelumnya)',
            'fields.*.kebijakan_pembatalan_sesudah.lt' => 'Batas pembayaran harus lebih besar daripada batas waktu pembatalan',
            'fields.*.kebijakan_pembatalan_sesudah.lte' => 'Nilai (hari sebelumnya sampai) harus lebih besar daripada nilai (hari sebelumnya)',
        ]);

        if ($validator->fails())
        {
            return redirect()->route('activity.viewBatasEdit', $id)->withErrors($validator, 'batas_waktu');
        }

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id
        ])->has('productdetail')->with('productdetail')->first();

        $kebijakan_pembatalan_sebelumnya = collect([]);
        $kebijakan_pembatalan_sesudah = collect([]);
        $kebijakan_pembatalan_potongan = collect([]);

        if (isset($request->fields)) {
            if (count($request->fields) !== 0) {
                foreach ($request->fields as $key => $value) {
                    $kebijakan_pembatalan_sebelumnya->push($value['kebijakan_pembatalan_sebelumnya']);
                    $kebijakan_pembatalan_sesudah->push($value['kebijakan_pembatalan_sesudah']);
                    $kebijakan_pembatalan_potongan->push($value['kebijakan_pembatalan_potongan']);
                }
            }
        }

        $product_detail = $detail->productdetail()->update([
            'batas_pembayaran' => $request->batas_pembayaran,
            'kebijakan_pembatalan_sebelumnya' => json_encode($kebijakan_pembatalan_sebelumnya),
            'kebijakan_pembatalan_sesudah' => json_encode($kebijakan_pembatalan_sesudah),
            'kebijakan_pembatalan_potongan' => json_encode($kebijakan_pembatalan_potongan)
        ]);

        return redirect()->route('activity.viewPetaEdit', $id);
    }

    public function showEditPeta(Request $request, $id)
    {

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id
        ])->has('productdetail')->with('productdetail')->first();

        $images_gallery = collect([]);
        $gallery = json_decode($detail->productdetail->gallery);

        foreach ($gallery as $key => $value) {
            $images_gallery->push(
                $gallery[$key]
            );
        }

        $peta['maps'] = $detail->productdetail->link_maps;
        $peta['images_gallery'] = $images_gallery;
        $peta['image_thumbnail'] = $detail->productdetail->thumbnail;

        $activity['harga'] = isset($detail->productdetail->harga_id) ? $detail->productdetail->harga_id : '';
        $activity['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $activity['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $activity['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = 'true';

        return view('BE.seller.activity.peta-listing', compact('peta', 'data', 'xstay', 'id', 'activity', 'isedit'));
    }

    public function editPetaPhoto(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'maps' => 'required',
            'image_thumbnail' => 'required_without_all:saved_thumbnail|image|mimes:jpeg,png,jpg|max:2048',
        ],[
            'maps.required' => 'Link peta google maps wajib diisi!',
            'image_thumbnail.required_without_all' => 'Foto Thumbnail wajib diisi!',
            'image_thumbnail.mimes' => 'Foto harus memiliki ekstensi (jpg, jpeg, atau png)',
            'image_thumbnail.max' => 'Foto maksimum berukuran 2MB',
        ]);

        if ($validator->fails())
        {
            return redirect()->route('activity.viewPetaEdit', $id)->withErrors($validator);
        }
        
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id
        ])->has('productdetail')->with('productdetail')->first();

        $image_thumbnail = isset($detail->productdetail->thumbnail) ? $detail->productdetail->thumbnail : '';

        if ($request->hasFile('image_thumbnail')) {
            $image = $request->file('image_thumbnail');

            $new_foto = time() . 'GalleryProductActivity.' . $image->getClientOriginalExtension();
            $tujuan_upload = 'Seller/Product/Activity/Thumbnail/';

            $lebar_foto = Image::make($image)->width();
            $lebar_foto = $lebar_foto * 50 / 100;

            Image::make($image)->resize($lebar_foto, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($tujuan_upload . $new_foto);

            $image_thumbnail = $tujuan_upload . $new_foto;

            //delete old image
            if (file_exists($detail->productdetail->thumbnail)) {
                unlink($detail->productdetail->thumbnail);
            }
        }

        $product_detail = $detail->productdetail()->update([
            'link_maps' => $request->maps,
            'thumbnail' => $image_thumbnail,
        ]);

        return redirect()->route('activity.viewPilihanEkstraEdit', $id);
    }

    public function editPetaPhotoGallery(Request $request, $id)
    {
        // Validate request
        $validator = Validator::make($request->all(), [
            // Validate when saved gallery not exist
            'images_gallery' => 'required_without_all:saved_gallery',  
            'images_gallery.*' => 'mimes:jpeg,png,jpg|max:2048'
        ],[
            'images_gallery.required_without_all' => 'Foto Gallery wajib diisi!',
            'images_gallery.*.mimes' => "Foto harus memiliki ekstensi (jpg, jpeg, atau png)",
            'images_gallery.*.max' => 'Foto maksimum berukuran 2MB',
        ]);

        // Return when validate fails on one or more rule(s)
        if ($validator->fails())
        {
            return array(
                'status' => 0,
                'message' => $validator->messages()->first(),
            );
        }

        // Execute these code when validate success
        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id
        ])->has('productdetail')->with('productdetail')->first();

        $fields = collect([]);

        // Save all new images gallery
        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {
                $new_foto = time() . 'GalleryProductActivity'. $key . '.' . $value->getClientOriginalExtension();
                $tujuan_upload = 'Seller/Product/Activity/Gallery/';

                $lebar_foto = Image::make($value)->width();
                $lebar_foto = $lebar_foto * 50 / 100;

                Image::make($value)->resize($lebar_foto, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($tujuan_upload . $new_foto);

                $images_gallery = $tujuan_upload . $new_foto;

                $fields->push(
                    $images_gallery,
                );
            }
        }

        // Delete all existing saved gallery
        if (isset($detail->productdetail->gallery) && !$request->has('saved_gallery')) {
            foreach (json_decode($detail->productdetail->gallery) as $key => $value) {
                if (file_exists($value[0])) {
                    unlink($value[0]);
                }
            }
        }

        // Delete some saved gallery iamges
        if (isset($detail->productdetail->gallery) && $request->has('saved_gallery')) {

            $differences = array_diff(json_decode($detail->productdetail->gallery), $request->saved_gallery);

            foreach ($differences as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }

            foreach ($request->saved_gallery as $key => $value) {
                $fields->push(
                    $value,
                );
            }
        }

        $product_detail = $detail->productdetail()->update([
            'gallery' => json_encode($fields),
        ]);

        return array(
            'status' => 1,
            'message' => 'Foto Gallery berhasil diperbarui.'
        );
    }

    public function showEditPilihanEkstra(Request $request, $id)
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id
        ])->has('productdetail')->with('productdetail')->first();

        $pilihan = Pilihan::where('id', $detail->productdetail->pilihan_id)->first();

        $pilihan_ekstra['ekstra_radio_button'] = $detail->productdetail->izin_ekstra;

        $fields = collect([]);
        $judul_pilihan = json_decode($pilihan->judul_pilihan);
        $deskripsi_pilihan = json_decode($pilihan->deskripsi_pilihan);
        $harga_pilihan = json_decode($pilihan->harga_pilihan);
        $kewajiban_pilihan = json_decode($pilihan->kewajiban_pilihan);

        foreach ($judul_pilihan as $key => $value) {
            $fields->push([
                'judul_pilihan' => $judul_pilihan[$key],
                'deskripsi_pilihan' => $deskripsi_pilihan[$key],
                'harga_pilihan' => $harga_pilihan[$key],
                'kewajiban_pilihan' => $kewajiban_pilihan[$key],
            ]);
        }

        $pilihan_ekstra['pilihan_ekstra'] = $fields;

        $activity['harga'] = isset($detail->productdetail->harga_id) ? $detail->productdetail->harga_id : '';
        $activity['batas'] = isset($detail->productdetail->batas_pembayaran) ? $detail->productdetail->batas_pembayaran : '';
        $activity['peta'] = isset($detail->productdetail->link_maps) ? $detail->productdetail->link_maps : '';
        $activity['pilihan_ekstra'] = isset($detail->productdetail->pilihan_id) ? $detail->productdetail->pilihan_id : '';
        $isedit = 'true';

        return view('BE.seller.activity.pilihan-listing', compact('pilihan_ekstra', 'data', 'xstay', 'activity', 'isedit', 'id'));
    }

    public function editPilihanEkstra(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'fields.*.judul_pilihan' => 'required',
            'fields.*.deskripsi_pilihan' => 'required',
            'fields.*.harga_pilihan' => 'required|integer|gte:0',
            'fields.*.kewajiban_pilihan' => 'required',
            'ekstra_radio_button' => 'required',
            'tnc' => 'accepted|required'
        ],[
            'fields.*.judul_pilihan.required' => 'Judul pilihan wajib diisi!',
            'fields.*.deskripsi_pilihan.required' => 'Deskripsi pilihan wajib diisi!',
            'fields.*.harga_pilihan.required' => 'Harga pilihan wajib diisi!',
            'fields.*.kewajiban_pilihan.required' => "Kewajiban pilihan wajib diisi!",
            'fields.*.harga_pilihan.integer' => 'Kewajiban pilihan harus berupa bilangan bulat.',
            'fields.*.harga_pilihan.gte' => 'Kewajiban pilihan harus lebih besar atau sama dengan 0.',
            'ekstra_radio_button.required' => 'Ekstra wajib diisi!',
            'tnc.accepted' => 'Setujui syarat dan ketentuan untuk melanjutkan.',
            'tnc.required' => 'Setujui syarat dan ketentuan untuk melanjutkan.',
        ]);

        if ($validator->fails())
        {
            return redirect()->route('activity.viewPilihanEkstraEdit', $id)->withErrors($validator, 'pilihan_ekstra');
        }

        $user_id = Auth::user()->id;

        $detail = Product::where([
            'user_id' => $user_id,
            'id' => $id
        ])->has('productdetail')->with('productdetail')->first();

        $pilihan = Pilihan::where('id', $detail->productdetail->pilihan_id)->first();

        $detail->productdetail()->update([
            'izin_ekstra' => $request->ekstra_radio_button,
        ]);

        $judul_pilihan = collect([]);
        $deskripsi_pilihan = collect([]);
        $harga_pilihan = collect([]);
        $kewajiban_pilihan = collect([]);

        if (isset($request->fields)) {
            if (count($request->fields) !== 0) {
                foreach ($request->fields as $key => $value) {
                    $judul_pilihan->push($value['judul_pilihan']);
                    $deskripsi_pilihan->push($value['deskripsi_pilihan']);
                    $harga_pilihan->push($value['harga_pilihan']);
                    $kewajiban_pilihan->push($value['kewajiban_pilihan']);
                }
            }
        }

        $pilihan_detail = $pilihan->update([
            'judul_pilihan' => json_encode($judul_pilihan),
            'deskripsi_pilihan' => json_encode($deskripsi_pilihan),
            'harga_pilihan' => json_encode($harga_pilihan),
            'kewajiban_pilihan' => json_encode($kewajiban_pilihan)
        ]);

        // return redirect()->route('activity.viewFaq');
        return redirect()->route('dashboardmyListing');
    }
}
