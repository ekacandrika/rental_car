<?php

namespace App\Http\Controllers\Seller\Activity;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Paket;
use App\Models\Kategori;
use App\Models\Pilihan;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\detailWisata;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AddListingActivityController extends Controller
{
    public function index(Request $request)
    {

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $pulau     = DB::select("select * from pulau");
        $provinsi  = Province::all();
        $kabupaten = Regency::all();
        $objek     = detailWisata::all();
        $kecamatan = '';
        $kelurahan = '';

        // dd($request->session()->get('activity.add_listing'));

        if ($request->session()->has('activity.add_listing')) {
            $kabupaten = Regency::where('id', $request->session()->get('activity.add_listing.kabupaen_id'))->first();
            $kecamatan = District::where('id', $request->session()->get('activity.add_listing.district_id'))->first();
            $kelurahan = Village::where('id', $request->session()->get('activity.add_listing.village_id'))->first();

            // dd($kabupaten);

            if ($kabupaten == null) {
                $kabupaten = Regency::all();
            }
        }

        if ($request->session()->missing('activity.product_code')) {
            $user_id = Auth::user()->id;

            $product_recent = Product::where('type', 'activity')->orderBy('id', 'desc')->first();
            $code = isset($product_recent) ? $product_recent->product_code : 1;

            // dd($code);

            if ($code != 1) {
                $code = explode("AC", $code);
                $code = substr($code[1], 0, -8);
                $code++;
            }

            $product = Product::create([
                'user_id' => $user_id,
                'type' => 'activity',
            ]);

            $request->session()->put('activity.add_listing.product_id', $product->id);
            $product_id = Product::where('id', $product->id)->first();

            $product_code = $product->id . 'AC' . $code . date('dmY');
            $product->slug = "";

            $product_id->update([
                'product_code' => $product_code,
            ]);

            $request->session()->put('activity.product_code', $product_code);
        }
        $dynamic_tag = [];

        $i =0;
        $x =0;
        $z =0;
        $y =0;
        
        foreach($pulau as $p){
            
            $prov = Province::where('island_id',$p->id)->get();
            
            $dynamic_tag['pulau'][$i]['id_pulau'] = $p->id;
            $dynamic_tag['pulau'][$i]['nama_pulau'] =$p->island_name;
            
            foreach($prov as $pv){
                
                $dynamic_tag['pulau'][$i]['provinsi'][$x]['id_provinsi']=$pv->id;
                $dynamic_tag['pulau'][$i]['provinsi'][$x]['nama_provinsi']=$pv->name;
                
                $kab = Regency::where('province_id',$pv->id)->get();
                
                foreach($kab as $k){

                    $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['id_kabupaten']=$k->id;
                    $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['nama_kabupaten']=$k->name;


                    $wisata =detailWisata::where('kabupaten_id',$k->id)->get();
                    foreach($wisata as $w){
                       
                        $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['wisata'][$y]['id_wisata']=$w->id;
                        $dynamic_tag['pulau'][$i]['provinsi'][$x]['kabupaten'][$z]['wisata'][$y]['nama_wisata']=$w->namaWisata;
                       
                        $y++;
                    }

                    $z++;

                }    
                
                $x++;
            }

            $i++;
        }

        $dynamic_tag = collect($dynamic_tag);    

        $listing = $request->session()->get('activity.product_code');
        $add_listing = $request->session()->has('activity.add_listing') ? $request->session()->get('activity.add_listing') : '';
        $activity = $request->session()->has('activity') ? $request->session()->get('activity') : '';

        return view('BE.seller.activity.add-listing', compact('pulau', 'provinsi', 'kabupaten', 'kecamatan', 'kelurahan', 'objek', 'add_listing', 'activity', 'data', 'xstay', 'listing','dynamic_tag'));
    }

    public function addListing(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_name' => 'required',
            'confirmation_radio_button' => 'required',
            'pulau' => 'required',
            'provinsi' => 'required',
            'kabupaten' => 'required',
            'objek' => 'required',
            'fields.*.nama_paket' => 'required',
            'fields.*.jam_paket' => 'required',
            'fields.*.min_peserta' => 'required|numeric|lte:fields.*.max_peserta',
            'fields.*.max_peserta' => 'required|numeric|gte:fields.*.min_peserta',
            'ringkasan' => 'required',
            'deskripsi_wisata' => 'required',
            'paket_termasuk' => 'required',
            'paket_tidak_termasuk' => 'required'
        ], [
            'product_name.required' => 'Judul wajib diisi!',
            'confirmation_radio_button.required' => 'Konfirmasi paket wajib diisi!',
            'pulau.required' => 'pulau wajib diisi!',
            'provinsi.required' => 'provinsi wajib diisi!',
            'kabupaten.required' => 'kabupaten wajib diisi!',
            'objek.required' => 'objek wajib diisi!',
            'fields.*.nama_paket.required' => 'Nama Paket wajib diisi!',
            'fields.*.jam_paket.required' => 'Jam Paket wajib diisi!',
            'fields.*.min_peserta.required' => 'Peserta Min wajib diisi!',
            'fields.*.max_peserta.required' => 'Peserta Max wajib diisi!',
            'fields.*.min_peserta.numeric' => 'Peserta Min harus berupa angka!',
            'fields.*.max_peserta.numeric' => 'Peserta Max harus berupa angka!',
            'fields.*.min_peserta.lte' => 'Peserta Min harus lebih kecil dari peserta max!',
            'fields.*.max_peserta.gte' => 'Peserta Max harus lebih besar dari peserta min!',
            'ringkasan.required' => 'Ringkasan wajib diisi!',
            'deskripsi_wisata.required' => 'Deskripsi Activity wajib diisi!',
            'paket_termasuk.required' => 'Paket Termasuk wajib diisi!',
            'paket_tidak_termasuk.required' => 'Paket Tidak Termasuk wajib diisi!',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $fields = collect([]);

        foreach ($request->fields as $key => $value) {
            $fields->push([
                'nama_paket' => $value['nama_paket'],
                'jam_paket' => $value['jam_paket'],
                'max_peserta' => $value['max_peserta'],
                'min_peserta' => $value['min_peserta']
            ]);
        }

        // dd($request->all());
        // $new_tag = [];
        $new_tag['results'] = $request->new_tag_location;

        $request->session()->put('activity.add_listing.product_name', $request->product_name);
        $request->session()->put('activity.add_listing.confirmation', $request->confirmation_radio_button);

        $request->session()->put('activity.add_listing.pulau_id', $request->pulau);
        $request->session()->put('activity.add_listing.provinsi_id', $request->provinsi);
        $request->session()->put('activity.add_listing.kabupaten_id', $request->kabupaten);
        $request->session()->put('activity.add_listing.objek_id', $request->objek);

        $request->session()->put('activity.add_listing.paket', $fields);

        $request->session()->put('activity.add_listing.ringkasan', $request->ringkasan);
        $request->session()->put('activity.add_listing.deskripsi_wisata', $request->deskripsi_wisata);
        $request->session()->put('activity.add_listing.paket_termasuk', $request->paket_termasuk);
        $request->session()->put('activity.add_listing.paket_tidak_termasuk', $request->paket_tidak_termasuk);
        $request->session()->put('activity.add_listing.catatan', $request->catatan);
        
        $request->session()->put('activity.add_listing.new_tag_location', json_encode($new_tag));

        // $user_id = Auth::user()->id;

        // $product_recent = Product::where('type', 'activity')->orderBy('id', 'desc')->first();

        // $code = isset($product_recent) ? $product_recent->product_code : 1;

        // if ($code != 1) {
        //     // explode(" ", $pizza);
        //     $code = substr($code, 3, -8);
        //     $code++;
        // }

        // dd($product_code);
        // $product = Product::create([
        //     'user_id' => $user_id,
        //     // 'product_code' => 'AC' . mt_rand(1, 99) . date('dmY'),
        //     'product_name' => $request->session()->get('activity.add_listing.product_name'),
        //     'type' => 'activity',
        // ]);

        // $request->session()->put('activity.add_listing.product_id', $product->id);

        // $product_id = Product::where('id', $product->id)->first();

        // $product_code = $product->id . 'AC' . $code . date('dmY');
        // $product->slug = "";

        // $product_id->update([
        //     'product_code' => $product_code,
        // ]);

        return redirect()->route('activity.viewHarga');
    }

    public function viewHarga(Request $request)
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $harga =  '';

        if ($request->session()->has('activity.harga')) {
            $harga = $request->session()->get('activity.harga');
        } else {
            $request->session()->put('activity.harga', []);
            $harga = [];
        }

        $activity = $request->session()->has('activity') ? $request->session()->get('activity') : '';

        return view('BE.seller.activity.harga-listing', compact('harga', 'data', 'xstay', 'activity'));
    }

    public function addHarga(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'dewasa_residen' => 'required|numeric|gte:0',
            'anak_residen' => 'required|numeric|gte:0',
            'balita_residen' => 'required|numeric|gte:0',
            'dewasa_non_residen' => 'sometimes|nullable|numeric|gte:0',
            'anak_non_residen' => 'sometimes|nullable|numeric|gte:0',
            'balita_non_residen' => 'sometimes|nullable|numeric|gte:0'
        ], [
            'dewasa_residen.required' => 'Harga Dewasa Residen wajib diisi!',
            'anak_residen.required' => 'Harga Anak Residen wajib diisi!',
            'balita_residen.required' => 'Harga Balita Residen wajib diisi!',
            'dewasa_residen.numeric' => 'Harga Dewasa Residen harus berupa angka!',
            'anak_residen.numeric' => 'Harga Anak Residen harus berupa angka!',
            'balita_residen.numeric' => 'Harga Balita Residen harus berupa angka!',
            'dewasa_non_residen.numeric' => 'Harga Dewasa non Residen harus berupa angka!',
            'anak_non_residen.numeric' => 'Harga Anak non Residen harus berupa angka!',
            'balita_non_residen.numeric' => 'Harga Balita non Residen harus berupa angka!',
            'dewasa_residen.gte' => 'Harga Dewasa Residen minimal 0',
            'anak_residen.gte' => 'Harga Anak Residen minimal 0',
            'balita_residen.gte' => 'Harga Balita Residen minimal 0',
            'dewasa_non_residen.gte' => 'Harga Dewasa non Residen minimal 0',
            'anak_non_residen.gte' => 'Harga Anak non Residen minimal 0',
            'balita_non_residen.gte' => 'Harga Balita non Residen minimal 0',
        ]);

        if ($validator->fails()) {
            return redirect()->route('activity.viewHarga')->withErrors($validator, 'harga');
        }

        $request->session()->put('activity.harga.dewasa_residen', $request->dewasa_residen);
        $request->session()->put('activity.harga.anak_residen', $request->anak_residen);
        $request->session()->put('activity.harga.balita_residen', $request->balita_residen);
        $request->session()->put('activity.harga.dewasa_non_residen', $request->dewasa_non_residen == null ? $request->dewasa_residen : $request->dewasa_non_residen);
        $request->session()->put('activity.harga.anak_non_residen', $request->anak_non_residen == null ? $request->anak_residen : $request->anak_non_residen);
        $request->session()->put('activity.harga.balita_non_residen', $request->balita_non_residen == null ? $request->balita_residen : $request->balita_non_residen);

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $harga = $request->session()->has('activity.harga') ? $request->session()->get('activity.harga') : '';

        return redirect()->route('activity.viewHarga')->with(['harga' => $harga, 'data' => $data, 'xstay' => $xstay, 'success_harga' => 'Harga berhasil diperbarui']);
    }

    public function addDiscountGroup(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'fields' => 'required',
            'fields.*.min_orang' => 'required|integer|lte:fields.*.max_orang',
            'fields.*.max_orang' => 'required|integer|gte:fields.*.min_orang',
            'fields.*.diskon_orang' => 'required|present|numeric|gt:0',
        ], [
            'fields.required' => 'Jumlah diskon orang wajib diisi',
            'fields.*.min_orang.required' => 'Minimal jumlah orang wajib diisi!',
            'fields.*.max_orang.required' => 'Maksimal jumlah orang wajib diisi!',
            'fields.*.diskon_orang.required' => 'Diskon wajib diisi!',
            'fields.*.min_orang.integer' => 'Minimal jumlah orang harus bilangan bulat!',
            'fields.*.max_orang.integer' => 'Maksimal jumlah orang harus bilangan bulat!',
            'fields.*.diskon_orang.numeric' => 'Diskon harus berupa angka! (Desimal gunakan titik)',
            'fields.*.min_orang.lte' => 'Minimal orang harus lebih besar dari maksimal orang!',
            'fields.*.max_orang.gte' => 'Maksimal orang harus lebih kecil dari minimal orang!',
            'fields.*.diskon_orang.gt' => 'Diskon harus lebih besar dari 0!'
        ]);

        if ($validator->fails()) {
            return redirect()->route('activity.viewHarga')->withErrors($validator, 'diskon_orang');
        }

        $fields = collect([]);

        foreach ($request->fields as $key => $value) {
            $fields->push([
                'min_orang' => $value['min_orang'],
                'max_orang' => $value['max_orang'],
                'diskon_orang' => $value['diskon_orang']
            ]);
        }
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $request->session()->put('activity.harga.discount_group', $fields);
        $harga = $request->session()->has('activity.harga') ? $request->session()->get('activity.harga') : '';

        return redirect()->route('activity.viewHarga')->with(['harga' => $harga, 'data' => $data, 'xstay' => $xstay, 'success_diskon_orang' => 'Diskon grup berhasil diperbarui']);
    }

    public function addAvailability(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'details.*.tgl_start' => 'required|date|before_or_equal:details.*.tgl_end',
            'details.*.tgl_end' => 'required|date|after_or_equal:details.*.tgl_start',
            'details.*.discount' => 'required|numeric',
        ], [
            'details.*.tgl_start.required' => 'Dari tanggal wajib diisi!',
            'details.*.tgl_end.required' => 'Sampai tanggal wajib diisi!',
            'details.*.discount.required' => 'Diskon atau penambahan biaya wajib diisi!',
            'details.*.tgl_start.date' => "Pastikan (Dari tanggal) terisi dengan benar",
            'details.*.tgl_end.date' => 'Pastikan (Sampai tanggal) terisi dengan benar',
            'details.*.discount.numeric' => 'Diskon atau penambahan biaya harus berupa angka!',
            'details.*.tgl_start.before_or_equal' => 'Pastikan tanggal mulai harus lebih kecil dari tanggal selesai!',
            'details.*.tgl_end.after_or_equal' => 'Pastikan Tanggal selesai harus lebih besar dari tanggal mulai!',
        ]);

        if ($validator->fails()) {
            return redirect()->route('activity.viewHarga')->withErrors($validator, 'diskon_ketersediaan');
        }

        $details = collect([]);

        if (isset($request->details)) {
            foreach ($request->details as $key => $value) {
                $details->push([
                    'tgl_start' => $value['tgl_start'],
                    'tgl_end' => $value['tgl_end'],
                    'discount' => $value['discount']
                ]);
            }
        }

        $request->session()->put('activity.harga.availability', $details);

        return redirect()->route('activity.viewBatas');
    }

    public function viewBatas(Request $request)
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $batas = '';

        if ($request->session()->has('activity.batas')) {
            $batas = $request->session()->get('activity.batas');
        } else {
            $request->session()->put('activity.batas', '');
            $batas = '';
        }

        $activity = $request->session()->has('activity') ? $request->session()->get('activity') : '';

        return view('BE.seller.activity.batas-listing', compact('batas', 'data', 'xstay', 'activity'));
    }

    public function addBatasWaktuPembayaranDanPembatalan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'batas_pembayaran' => 'required|integer',
            'fields' => 'required',
            'fields.*.kebijakan_pembatalan_sebelumnya' => 'required|integer|lt:batas_pembayaran|gte:fields.*.kebijakan_pembatalan_sesudah',
            'fields.*.kebijakan_pembatalan_sesudah' => 'required|integer|lt:batas_pembayaran|lte:fields.*.kebijakan_pembatalan_sebelumnya',
            'fields.*.kebijakan_pembatalan_potongan' => 'required|integer',
        ], [
            'batas_pembayaran.required' => 'Batas pembayaran wajib diisi!',
            'batas_pembayaran.integer' => 'Pastikan batas pembayaran berupa bilangan bulat',
            'fields.required' => 'Data kebijakan pembatalan wajib diisi!',
            // 'batas_pembayaran.gte' => 'Batas pembayaran harus lebih besar daripada batas waktu pembatalan',
            'fields.*.kebijakan_pembatalan_sebelumnya.required' => 'Data kebijakan pembatalan wajib diisi!',
            'fields.*.kebijakan_pembatalan_sesudah.required' => 'Data kebijakan pembatalan wajib diisi!',
            'fields.*.kebijakan_pembatalan_potongan.required' => 'Potongan wajib diisi!',
            'fields.*.kebijakan_pembatalan_sebelumnya.integer' => "Pastikan jumlah hari kebijakan pembatalan berupa bilangan bulat",
            'fields.*.kebijakan_pembatalan_sesudah.integer' => 'Pastikan jumlah hari kebijakan pembatalan berupa bilangan bulat',
            'fields.*.kebijakan_pembatalan_potongan.integer' => 'Pastikan potongan berupa bilangan bulat',
            'fields.*.kebijakan_pembatalan_sebelumnya.lt' => 'Batas pembayaran harus lebih besar daripada batas waktu pembatalan',
            'fields.*.kebijakan_pembatalan_sebelumnya.gte' => 'Nilai (hari sebelumnya sampai) harus lebih besar daripada nilai (hari sebelumnya)',
            'fields.*.kebijakan_pembatalan_sesudah.lt' => 'Batas pembayaran harus lebih besar daripada batas waktu pembatalan',
            'fields.*.kebijakan_pembatalan_sesudah.lte' => 'Nilai (hari sebelumnya sampai) harus lebih besar daripada nilai (hari sebelumnya)',
        ]);

        if ($validator->fails()) {
            return redirect()->route('activity.viewBatas')->withErrors($validator, 'batas_waktu');
        }

        $request->session()->put('activity.batas.batas_pembayaran', $request->batas_pembayaran);

        $fields = collect([]);

        if (isset($request->fields)) {
            foreach ($request->fields as $key => $value) {
                $fields->push([
                    'kebijakan_pembatalan_sebelumnya' => $value['kebijakan_pembatalan_sebelumnya'],
                    'kebijakan_pembatalan_sesudah' => $value['kebijakan_pembatalan_sesudah'],
                    'kebijakan_pembatalan_potongan' => $value['kebijakan_pembatalan_potongan']
                ]);
            }
        }

        $request->session()->put('activity.batas.pembatalan', $fields);

        $batas = $request->session()->has('activity.batas') ? $request->session()->get('activity.batas') : '';

        return redirect()->route('activity.viewPeta');
    }

    public function viewPeta(Request $request)
    {

        // if ($request->session()->get('activity.batas') == '') {
        //     return redirect()->route('activity');
        // }

        // dd($request->session());
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $peta = '';

        if ($request->session()->has('activity.peta')) {
            $peta = $request->session()->get('activity.peta');
        } else {
            $request->session()->put('activity.peta', '');
            $peta = '';
        }

        $activity = $request->session()->has('activity') ? $request->session()->get('activity') : '';

        return view('BE.seller.activity.peta-listing', compact('peta', 'data', 'xstay', 'activity'));
    }

    public function addPetaPhoto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'maps' => 'required',
            'image_thumbnail' => 'required_without_all:saved_thumbnail|image|mimes:jpeg,png,jpg|max:2048',
        ], [
            'maps.required' => 'Link peta google maps wajib diisi!',
            'image_thumbnail.required_without_all' => 'Foto Thumbnail wajib diisi!',
            'image_thumbnail.mimes' => 'Foto harus memiliki ekstensi (jpg, jpeg, atau png)',
            'image_thumbnail.max' => 'Foto maksimum berukuran 2MB',
        ]);

        if ($validator->fails()) {
            return redirect()->route('activity.viewPeta')->withErrors($validator);
        }

        // $images_gallery = $request->session()->has('activity.peta.image_gallery') ? $request->session()->get('activity.peta.image_gallery') : '';
        $image_thumbnail = $request->session()->has('activity.peta.image_thumbnail') ? $request->session()->get('activity.peta.image_thumbnail') : '';

        // $fields = collect([]);

        // if ($request->session()->has('activity.peta.images_gallery') && !$request->hasFile('images_gallery')) {
        //     $image = $request->session()->get('activity.peta.images_gallery');
        //     foreach ($image as $key => $value) {
        //         $fields->push(
        //             $value,
        //         );
        //     }
        // }


        // if ($request->hasFile('images_gallery')) {
        //     $image = $request->file('images_gallery');

        //     foreach ($image as $key => $value) {

        //         $new_foto = time() . 'GalleryProductActivity' . $value->getClientOriginalName();
        //         $tujuan_upload = 'Seller/Product/Activity/Gallery/';

        //         $lebar_foto = Image::make($value)->width();
        //         $lebar_foto = $lebar_foto * 50 / 100;

        //         Image::make($value)->resize($lebar_foto, null, function ($constraint) {
        //             $constraint->aspectRatio();
        //         })->save($tujuan_upload . $new_foto);

        //         // $value->move($tujuan_upload, $new_foto);
        //         $images_gallery = $tujuan_upload . $new_foto;

        //         $fields->push(
        //             $images_gallery,
        //         );
        //     }

        //     //delete old image
        //     if ($request->session()->has('activity.peta.images_gallery')) {
        //         foreach ($request->session()->get('activity.peta.images_gallery') as $key => $value) {
        //             if (file_exists($value[0])) {
        //                 unlink($value[0]);
        //             }
        //         }
        //     }
        // }

        if ($request->hasFile('image_thumbnail')) {
            $image = $request->file('image_thumbnail');

            $new_foto = time() . 'GalleryProductActivity.' . $image->getClientOriginalExtension();
            $tujuan_upload = 'Seller/Product/Activity/Thumbnail/';

            $lebar_foto = Image::make($image)->width();
            $lebar_foto = $lebar_foto * 50 / 100;

            Image::make($image)->resize($lebar_foto, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($tujuan_upload . $new_foto);

            // $image->move($tujuan_upload, $new_foto);
            $image_thumbnail = $tujuan_upload . $new_foto;

            //delete old image
            if (file_exists($request->session()->get('activity.peta.image_thumbnail'))) {
                unlink($request->session()->get('activity.peta.image_thumbnail'));
            }
        }

        $request->session()->put('activity.peta.maps', $request->maps);
        // $request->session()->put('activity.peta.images_gallery', $fields);
        $request->session()->put('activity.peta.image_thumbnail', $image_thumbnail);

        // $peta = $request->session()->has('activity.peta') ? $request->session()->get('activity.peta') : '';
        // dd($request->all());

        return redirect()->route('activity.viewPilihanEkstra');
    }

    public function addPetaPhotoGallery(Request $request)
    {
        // Validate request
        $validator = Validator::make($request->all(), [
            // Validate when saved gallery not exist
            'images_gallery' => 'required_without_all:saved_gallery',
            'images_gallery.*' => 'mimes:jpeg,png,jpg|max:2048'
        ], [
            'images_gallery.required_without_all' => 'Foto Gallery wajib diisi!',
            'images_gallery.*.mimes' => "Foto harus memiliki ekstensi (jpg, jpeg, atau png)",
            'images_gallery.*.max' => 'Foto maksimum berukuran 2MB',
        ]);

        // Return when validate fails on one or more rule(s)
        if ($validator->fails()) {
            return array(
                'status' => 0,
                'message' => $validator->messages()->first(),
            );
        }

        // Execute these code when validate success
        $session_gallery = $request->session()->get('activity.peta.images_gallery');

        $fields = collect([]);

        // Save all new images gallery
        if ($request->hasFile('images_gallery')) {
            $image = $request->file('images_gallery');

            foreach ($image as $key => $value) {
                $new_foto = time() . 'GalleryProductActivity' . $key . '.' . $value->getClientOriginalExtension();
                $tujuan_upload = 'Seller/Product/Activity/Gallery/';

                $lebar_foto = Image::make($value)->width();
                $lebar_foto = $lebar_foto * 50 / 100;

                Image::make($value)->resize($lebar_foto, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($tujuan_upload . $new_foto);

                $images_gallery = $tujuan_upload . $new_foto;

                $fields->push(
                    $images_gallery,
                );
            }
        }

        // Delete all existing saved gallery
        if (isset($session_gallery) && !$request->has('saved_gallery')) {
            foreach (json_decode($session_gallery) as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }
        }

        // Delete some saved gallery iamges
        if (isset($session_gallery) && $request->has('saved_gallery')) {

            $differences = array_diff(json_decode($session_gallery), $request->saved_gallery);

            foreach ($differences as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                }
            }

            foreach ($request->saved_gallery as $key => $value) {
                $fields->push(
                    $value,
                );
            }
        }

        // $product_detail = $detail->productdetail()->update([
        //     'gallery' => json_encode($fields),
        // ]);

        $request->session()->put('activity.peta.images_gallery', $fields);

        return array(
            'status' => 1,
            'message' => 'Foto Gallery berhasil diperbarui.',
            'data' => $fields
        );
    }

    public function viewPilihanEkstra(Request $request)
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $pilihan_ekstra = '';

        if ($request->session()->has('activity.pilihan_ekstra')) {
            $pilihan_ekstra = $request->session()->get('activity.pilihan_ekstra');
        } else {
            $request->session()->put('activity.pilihan_ekstra', '');
            $pilihan_ekstra = '';
        }

        return view('BE.seller.activity.pilihan-listing', compact('pilihan_ekstra', 'data', 'xstay'));
    }

    public function addPilihanEkstra(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fields.*.judul_pilihan' => 'required',
            'fields.*.deskripsi_pilihan' => 'required',
            'fields.*.harga_pilihan' => 'required|integer|gte:0',
            'fields.*.kewajiban_pilihan' => 'required',
            'ekstra_radio_button' => 'required',
            'tnc' => 'accepted|required'
        ], [
            'fields.*.judul_pilihan.required' => 'Judul pilihan wajib diisi!',
            'fields.*.deskripsi_pilihan.required' => 'Deskripsi pilihan wajib diisi!',
            'fields.*.harga_pilihan.required' => 'Harga pilihan wajib diisi!',
            'fields.*.kewajiban_pilihan.required' => "Kewajiban pilihan wajib diisi!",
            'fields.*.harga_pilihan.integer' => 'Kewajiban pilihan harus berupa bilangan bulat.',
            'fields.*.harga_pilihan.gte' => 'Kewajiban pilihan harus lebih besar atau sama dengan 0.',
            'ekstra_radio_button.required' => 'Ekstra wajib diisi!',
            'tnc.accepted' => 'Setujui syarat dan ketentuan untuk melanjutkan.',
            'tnc.required' => 'Setujui syarat dan ketentuan untuk melanjutkan.',
        ]);

        if ($validator->fails()) {
            return redirect()->route('activity.viewPilihanEkstra')->withErrors($validator, 'pilihan_ekstra');
        }

        $request->session()->put('activity.pilihan_ekstra.ekstra', $request->ekstra_radio_button);

        $fields = collect([]);

        if ($request->fields) {
            foreach ($request->fields as $key => $value) {
                $fields->push([
                    'judul_pilihan' => $value['judul_pilihan'],
                    'deskripsi_pilihan' => $value['deskripsi_pilihan'],
                    'harga_pilihan' => $value['harga_pilihan'],
                    'kewajiban_pilihan' => $value['kewajiban_pilihan']
                ]);
            }
        }

        $request->session()->put('activity.pilihan_ekstra.pilihan', $fields);

        // Storing to Database
        $user_id = Auth::user()->id;
        $kategori = Kategori::where('nama_kategori', 'activity')->first();
        $gallery = $request->session()->get('activity.peta.images_gallery');
        $kebijakan_pembatalan_sebelumnya = collect([]);
        $kebijakan_pembatalan_sesudah = collect([]);
        $kebijakan_pembatalan_potongan = collect([]);

        $product_id = Product::where('product_code', $request->session()->get('activity.product_code'))->first();
        $product_id->slug = "";
        $product_id->update([
            'product_name' => $request->session()->get('activity.add_listing.product_name'),
        ]);


        foreach ($request->session()->get('activity.batas.pembatalan') as $key => $value) {
            $kebijakan_pembatalan_sebelumnya->push($value['kebijakan_pembatalan_sebelumnya']);
            $kebijakan_pembatalan_sesudah->push($value['kebijakan_pembatalan_sesudah']);
            $kebijakan_pembatalan_potongan->push($value['kebijakan_pembatalan_potongan']);
        }

        // $product = Product::create([
        //     'user_id' => $user_id,
        //     'product_code' => 'AC' . mt_rand(1, 99) . date('dmY'),
        //     'product_name' => $request->session()->get('activity.add_listing.product_name'),
        //     'type' => 'activity',
        // ]);

        $nama_paket = collect([]);
        $jam_paket = collect([]);
        $max_peserta = collect([]);
        $min_peserta = collect([]);
        $paket_id = collect([]);

        foreach ($request->session()->get('activity.add_listing.paket') as $key => $value) {
            $nama_paket->push($value['nama_paket']);
            $jam_paket->push($value['jam_paket']);
            $max_peserta->push($value['max_peserta']);
            $min_peserta->push($value['min_peserta']);
        }

        $paket = Paket::create([
            'nama_paket' => json_encode($nama_paket),
            'jam_paket' => json_encode($jam_paket),
            'max_peserta' => json_encode($max_peserta),
            'min_peserta' => json_encode($min_peserta),
        ]);

        $price = Price::create([
            'dewasa_residen' => $request->session()->get('activity.harga.dewasa_residen'),
            'anak_residen' => $request->session()->get('activity.harga.anak_residen'),
            'balita_residen' => $request->session()->get('activity.harga.balita_residen'),
            'dewasa_non_residen' => $request->session()->get('activity.harga.dewasa_non_residen'),
            'anak_non_residen' => $request->session()->get('activity.harga.anak_non_residen'),
            'balita_non_residen' => $request->session()->get('activity.harga.balita_non_residen'),
        ]);

        $max_orang = collect([]);
        $min_orang = collect([]);
        $diskon_orang = collect([]);
        $tgl_start = collect([]);
        $tgl_end = collect([]);
        $discount = collect([]);

        if (!empty($request->session()->get('activity.harga.discount_group'))) {
            foreach ($request->session()->get('activity.harga.discount_group') as $key => $value) {
                $max_orang->push($value['max_orang']);
                $min_orang->push($value['min_orang']);
                $diskon_orang->push($value['diskon_orang']);
            }
        }


        if (count($request->session()->get('activity.harga.availability')) !== 0) {
            foreach ($request->session()->get('activity.harga.availability') as $key => $value) {
                $tgl_start->push($value['tgl_start']);
                $tgl_end->push($value['tgl_end']);
                $discount->push($value['discount']);
            }
        }

        $discount_product = Discount::create([
            'max_orang' => json_encode($max_orang),
            'min_orang' => json_encode($min_orang),
            'diskon_orang' => json_encode($diskon_orang),
            'tgl_start' => json_encode($tgl_start),
            'tgl_end' => json_encode($tgl_end),
            'discount' => json_encode($discount),
        ]);

        $judul_pilihan = collect([]);
        $deskripsi_pilihan = collect([]);
        $harga_pilihan = collect([]);
        $kewajiban_pilihan = collect([]);

        foreach ($request->session()->get('activity.pilihan_ekstra.pilihan') as $key => $value) {
            $judul_pilihan->push($value['judul_pilihan']);
            $deskripsi_pilihan->push($value['deskripsi_pilihan']);
            $harga_pilihan->push($value['harga_pilihan']);
            $kewajiban_pilihan->push($value['kewajiban_pilihan']);
        }

        $pilihan = Pilihan::create([
            'judul_pilihan' => json_encode($judul_pilihan),
            'deskripsi_pilihan' => json_encode($deskripsi_pilihan),
            'harga_pilihan' => json_encode($harga_pilihan),
            'kewajiban_pilihan' => json_encode($kewajiban_pilihan),
        ]);

        $productdetail = Productdetail::create([
            'product_id' => $request->session()->get('activity.add_listing.product_id'),
            'tag_location_1' => $request->session()->get('activity.add_listing.pulau_id'),
            'tag_location_2' => $request->session()->get('activity.add_listing.provinsi_id'),
            'tag_location_3' => $request->session()->get('activity.add_listing.kabupaten_id'),
            'tag_location_4' => $request->session()->get('activity.add_listing.objek_id'),
            'new_tag_location' => $request->session()->get('activity.add_listing.new_tag_location'),
            'discount_id' => $discount_product->id,
            'paket_id' => $paket->id,
            'kategori_id' => $kategori->id,
            'harga_id' => $price->id,
            'pilihan_id' => $pilihan->id,
            'confirmation' => $request->session()->get('activity.add_listing.confirmation'),
            'ringkasan' => $request->session()->get('activity.add_listing.ringkasan'),
            'deskripsi' => $request->session()->get('activity.add_listing.deskripsi_wisata'),
            'paket_termasuk' => $request->session()->get('activity.add_listing.paket_termasuk'),
            'paket_tidak_termasuk' => $request->session()->get('activity.add_listing.paket_tidak_termasuk'),
            'catatan' => $request->session()->get('activity.add_listing.catatan'),
            'batas_pembayaran' => $request->session()->get('activity.batas.batas_pembayaran'),
            'kebijakan_pembatalan_sebelumnya' => $kebijakan_pembatalan_sebelumnya,
            'kebijakan_pembatalan_sesudah' => $kebijakan_pembatalan_sesudah,
            'kebijakan_pembatalan_potongan' => $kebijakan_pembatalan_potongan,
            'link_maps' => $request->session()->get('activity.peta.maps'),
            'izin_ekstra' => $request->session()->get('activity.pilihan_ekstra.ekstra'),
            'gallery' => json_encode($gallery),
            'thumbnail' => $request->session()->get('activity.peta.image_thumbnail'),
            'base_url' => url('/') . '/activity/' . $product_id->slug,
            'available' => 'waiting approve',
        ]);

        $request->session()->forget('activity');

        return redirect()->route('activity.viewFaq');
    }

    public function viewFaq(Request $request)
    {
        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'activity'
        ])->has('productdetail')->with('productdetail')->get();
        // dd($activity->productdetail);

        // $activity_detail = $activity->productdetail;




        // $pilihan_ekstra = $request->session()->has('activity.pilihan_ekstra') ? $request->session()->get('activity.pilihan_ekstra') : '';

        return view('BE.seller.activity.faq-listing', compact('data', 'xstay', 'faqs'));
    }

    public function viewFaqCode(Request $request)
    {
        if ($request->product_activity == null) {
            return redirect()->route('activity.viewFaq');
        }

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();
        $product = Product::where('id', $request->product_activity)->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'activity'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('id', $request->product_activity)->with('productdetail')->first();

        // return view('BE.seller.hotel.faq', compact('data', 'xstay', 'faqs', 'faq_content'));
        return redirect()->route('activity.viewFaq.edit', $product->product_code);
    }

    public function viewFaqEdit($code)
    {
        if ($code == null) {
            return redirect()->route('activity.viewFaq');
        }
        // dd($request->product_activity);

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'activity'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('product_code', $code)->where('user_id', auth()->user()->id)->with('productdetail')->first();

        return view('BE.seller.activity.faq-listing', compact('faqs', 'faq_content', 'data', 'xstay'));
    }

    public function viewFaqPost(Request $request, $id)
    {

        // dd($id);

        $data = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('user_id', auth()->user()->id)->where('type', 'xstay')->first();

        $user_id = Auth::user()->id;

        $faqs = Product::where([
            'user_id' => $user_id,
            'type' => 'activity'
        ])->has('productdetail')->with('productdetail')->get();
        $faq_content = Product::where('id', $id)->with('productdetail')->first();
        $faq_update = Productdetail::where('product_id', $id)->first();

        $faq_update->update([
            'faq' => $request->faq,
        ]);

        // return view('BE.seller.activity.faq-listing', compact('faqs', 'faq_content', 'data', 'xstay'));
        return redirect()->route('dashboardmyListing');
    }
}
