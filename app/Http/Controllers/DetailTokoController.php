<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Kelolatoko;
use App\Models\BannerToko;
use App\Models\Allbanner;
use App\Models\TemporaryFile;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class DetailTokoController extends Controller
{
    public function index()
    {
        $user_id = Auth::user()->id;
        $dekorasi = user::where('id', $user_id)->with('kelolatoko')->first();
        $toko = Kelolatoko::where('id_seller', $dekorasi->id)->first();
        $banner = Allbanner::where('user_id', $dekorasi->id)->first();
        $allbanner = '';
        $bannerone = BannerToko::where('display_order', 1)->get();
        $bannertwo = BannerToko::where('display_order', 2)->get();
        $bannerthree = BannerToko::where('display_order', 3)->get();
        $bannerfour = BannerToko::where('display_order', 4)->get();
        $bannerfive = BannerToko::where('display_order', 5)->get();

        if ($allbanner == null ) {
            $allbanner = Allbanner::where('user_id', $dekorasi->id)->first();
        } else{
            $allbanner = '';
        }

        return view('Produk.viewDetailToko', compact('dekorasi', 'toko', 'allbanner','banner', 'bannerone', 'bannertwo', 'bannerthree', 'bannerfour', 'bannerfive'));
    }
}
