<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\District;
use App\Models\Hotel;
use App\Models\Masterroute;
use App\Models\detailWisata;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class MasterrouteController extends Controller
{
    private function hotel()
    {
        $hotel = Hotel::with('productdetail')->where('type', 'hotel')->first();
        $xstay = Hotel::with('productdetail')->where('type', 'xstay')->first();

        return [$hotel, $xstay];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        list($hotel, $xstay) = $this->hotel();
        $kecamatan = District::all();
        // $data = Masterroute::all();
        
        $from = District::first();
        $to = District::first();
        
        $from_point = detailWisata::all();
        $to_point = detailWisata::all();

        $collect = collect();
        $route_result = $this->getLoadData();
        $collect->push($route_result);
        
        // dd($collect[0]);
        return view('BE.seller.master-route', compact('kecamatan', 'hotel', 'xstay', 'from', 'to', 'from_point', 'to_point', 'route_result', 'collect'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $rute = new Masterroute;
        
        $detail['result'] = [];
        
        $i=0;

        foreach($request->district_id_from as $key => $from){
            
            $detail['result'][$i]['from'] = $request->district_id_from[$key];
            // $detail['result'][$i]['depature'] = $request->depature_id[$key];
            $detail['result'][$i]['to'] = $request->district_id_to[$key];
            // $detail['result'][$i]['destination'] = $request->desination_id[$key];
        
            $i++;
        }

        $rute::create([
            'judul_rute'=> $request->title,
            'rute'=>json_encode($detail)
        ]); 
        
        return redirect()->route('masterroute.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kecamatan = District::all();
        $data = Masterroute::findOrFail($id);
        
        $from = District::first();
        $to = District::first();
        
        $from_point = detailWisata::all();
        $to_point = detailWisata::all();

        $rute = $this->getLoadData($id)[0];
        
        return view('BE.seller.master-route-edit', compact('kecamatan', 'from', 'to', 'from_point', 'to_point', 'data','id','rute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Masterroute::findOrFail($id);

        $detail['result'] = [];
        
        $i=0;

        foreach($request->from as $key => $f){
            
            $detail['result'][$i]['from'] = $request->from[$key];
            // $detail['result'][$i]['depature'] = $request->depature[$key];
            $detail['result'][$i]['to'] = $request->to[$key];
            // $detail['result'][$i]['destination'] = $request->desination[$key];
        
            $i++;
        }

        $data->update([
            'judul_rute'=> $request->title,
            'rute'=>json_encode($detail)
        ]);
        
        return redirect()->route('masterroute.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rute = Masterroute::findOrFail($id);

        $rute->destroy($id);
        return redirect()->route('masterroute.index');
    }

    public function getRoute_old(Request $request){
        
        if($request->search){
            $results = Masterroute::where('judul_rute','like','%'.$request->search.'%')
            ->orWhere('judul_rute','like','%'.ucfirst($request->search).'%')
            ->orWhere('judul_rute','like','%'.ucwords($request->search).'%')
            ->orWhere('judul_rute','like','%'.strtolower($request->search).'%')
            ->orWhere('judul_rute','like','%'.strtoupper($request->search).'%')
            ->groupBy('judul_rute')->get();
        }else{
            $results = Masterroute::groupBy('judul_rute')->get();
        }

        $route_result = [];

        $i = 0;
        $x = 0;
        
        $collect = collect();

        foreach($results as $result){

            $route_result[$i]['title_route'] =  $result->title_rute; 

            $lists = Masterroute::select('masterroutes_new.id','from.name as from','to.name as to','depature.namaWisata as depature','destination.namaWisata as destination')
            ->join('districts as from','from.id','=','masterroutes_new.to_id')
            ->join('districts as to','to.id','=','masterroutes_new.to_id')
            // ->join('detail_wisatas as depature','depature.id','=','masterroutes_new.obj_from_id')
            // ->join('detail_wisatas as destination','destination.id','=','masterroutes_new.obj_to_id')
            ->where('title_rute',$result->title_rute)
            ->get();

            foreach ($lists as $key => $list) {
                # code...
                $route_result[$i]['rute'][$x]['id']=$list['id'];
                $route_result[$i]['rute'][$x]['from']=$list['from'];
                // $route_result[$i]['rute'][$x]['depature']=$list['depature'];
                $route_result[$i]['rute'][$x]['to']=$list['to'];
                // $route_result[$i]['rute'][$x]['destination']=$list['destination'];
                $x++;
            }

            $i++;
        }

        return response()->json([
            'status'=>true,
            'data'=>$route_result
        ],200);
    }

    /**
     * method search master roue
     * @param Request $request
     * @return json response 
     */
    public function getRoute(Request $request){

        if($request->search){
            $results = Masterroute::where('judul_rute','like','%'.$request->search.'%')
            ->orWhere('judul_rute','like','%'.ucfirst($request->search).'%')
            ->orWhere('judul_rute','like','%'.ucwords($request->search).'%')
            ->orWhere('judul_rute','like','%'.strtolower($request->search).'%')
            ->orWhere('judul_rute','like','%'.strtoupper($request->search).'%')
            ->get();
        }else{
            $results = Masterroute::all();
        }

        $i = 0; $j= 0;
        
        foreach($results as $result){

            $route_result[$i]['title_route'] =  $result->judul_rute; 
            $route_result[$i]['id'] =  $result->id; 

            // get from disctrict
            $details = json_decode($result->rute, true)['result'];

            foreach($details as $key => $detail){
                // get district
                $from = District::where('id',$detail['from'])->first();
                $to = District::where('id',$detail['to'])->first();
                
                //get detail wisata 
                $depature = detailWisata::where('id',$detail['depature'])->first(); 
                $destination = detailWisata::where('id',$detail['destination'])->first(); 
                
                $rute[$i]['routes'][$key]['from_id'] = isset($from->id) ? $from->id:null;
                $rute[$i]['routes'][$key]['from'] = isset($from->name) ? $from->name:null;
                // $rute[$i]['routes'][$key]['depature_id'] = $depature->id;
                // $rute[$i]['routes'][$key]['depature'] = $depature->namaWisata;
                $rute[$i]['routes'][$key]['to_id'] = isset($to->id) ? $to->id:null;
                $rute[$i]['routes'][$key]['to'] = isset($to->name) ? $to->name:null;
                // $rute[$i]['routes'][$key]['destination_id'] = $destination->id;
                // $rute[$i]['routes'][$key]['destination'] = $destination->namaWisata;
                
                $j++;
            }

            $i++;
        }

        $total = count($route_result);
        $per_page = 10;
        $current_page = $request->input('page') ?? 1;
        $start_point = ($current_page * $per_page) - $per_page;

        $route_result = array_slice($route_result,$start_point, $per_page, true);
        $route_result = New Paginator($route_result, $total, $per_page, $current_page,[
            'page'=>$request->url(),
            'query'=>$request->query()
        ]);

        if(isset($route_result)){
            return response()->json([
                'status'=>true,
                'data'=>$route_result
            ],200);
        }else{
            return response()->json([
                'status'=>true,
                'data'=>[]
            ],400);
        }
        
    }
    
    /**
     * method to get data from detail array
     * @param int $index 
     * @param int $key 
     * @param int $id
     */ 
    public function editDetail($index, $key, $id){
        // dd($key,$id);
        $data = $this->getLoadData($id);
       
        $title_rute = $data[0]['title_route'];
        $row   = $data[0]['routes'][$key]; 
        
        $from = District::all();
        $to = District::all();
        
        $from_point = detailWisata::all();
        $to_point = detailWisata::all();
        
        $from_id = $row['from_id'];
        // $depature_id = $row['depature_id'];

        $to_id = $row['to_id'];
        // $destination_id = $row['destination_id'];

        return view('BE.seller.master-route-detail-edit',compact('from', 'to', 'data', 'from_point', 'to_point', 'from_id', 'to_id', 'title_rute', 'key','id'));
    }

    /** 
    *  update detail 
    *  @param Request $request
    */ 
    public function updateDetail(Request $request, $key,$id){
        
        $data = Masterroute::findOrFail($id);
        
        $array = $data->toArray()['rute'];
        $rute  = json_decode($array, true)['result']; 
        
        $rute[$key]['from'] = $request->from;
        // $rute[$key]['depature'] = $request->depature;
        $rute[$key]['to'] = $request->to;
        // $rute[$key]['destination'] = $request->desination;

        $detail['result'] = $rute;
        
        $new_array =[
            'rute'=>json_encode($detail),
        ];

        $data->update($new_array);
        
        return redirect()->route('masterroute.index');
    }

    /**
    * method to clone detail or child and update detail route
    * @param int $id, Request $request
    */
    public function clone(Request $request, $id){
        $row  = Masterroute::findOrFail($id);        
       
        $row_new = [
            'from'=>$request->from,
            // 'depature'=>$request->depature,
            'to'=>$request->to,
            // 'destination'=>$request->destination,
        ];

        $row_array = collect(json_decode($row->rute,true)['result']);
        $row_array->push($row_new);

        $detail['result'] = $row_array;
        
        $row->update([
            'rute'=>json_encode($detail),
            'updated_at'=>Carbon::now()
        ]);

        return redirect()->route('masterroute.index');
    }

    /**
    *  method to delete detai
    *  @param int $key
    *  @param int $id
    */
    public function deleteDetail($index, $key, $id){
        // get data for ready delete
        $data = Masterroute::findOrFail($id);
        
        $array = $data->toArray()['rute'];
        $rute  = json_decode($array, true)['result'];

        // delete proses
        if($rute[$key]){
            unset($rute[$key]);
        }

        $detail['result']= $rute;

        $data->update([
            'rute'=>json_encode($detail)
        ]);

        return redirect()->route('masterroute.index');
    }

    /**
    * method to clone rute parent 
    * @param int $id  
    */
    public function parentClone($id){
        $rute = Masterroute::findOrFail($id);
        
        $newRoute = $rute->replicate();
        $newRoute->created_at = Carbon::now();

        $newRoute->save();

        return redirect()->route('masterroute.index');
    }

    public function datas(Request $request){
        
        $json = $this->getLoadData();
        $total = count($json);
        $per_page = 10;
        $current_page = $request->input('page') ?? 1;
        $start_point = ($current_page * $per_page) - $per_page;

        $json = array_slice($json,$start_point, $per_page, true);
        $json = New Paginator($json, $total, $per_page, $current_page,[
            'page'=>$request->url(),
            'query'=>$request->query()
        ]);
        
        return response()->json([
            'success'=>true,
            'datas'=>$json
        ],200);
    }
    /**
    * method to get data array
    * @param int $id 
    * @return array data
    */
    private function getLoadData($id = null, $limit=null,$offset=null){
       
        if($id){
           $datas = Masterroute::where('id',$id)->get();
        }else{
            // offset($offset)->limit(10)-
            if($limit){
                $datas = Masterroute::offset($offset)->limit($limit)->get();
            }else{
                $datas = Masterroute::get();
            }
        }

        $rute = [];
        $i=0;
     
        // dd($datas);

        foreach($datas as $key => $data){
            $rute[$i]['id'] = $data->id;
            $rute[$i]['title_route'] = $data->judul_rute;

            // get detail
            $details = json_decode($data->rute, true)['result'];

            foreach($details as $key => $detail){
                // get district
                $from = District::where('id',$detail['from'])->first();
                $to = District::where('id',$detail['to'])->first();
                
                //get detail wisata 
                // $depature = detailWisata::where('id',$detail['depature'])->first(); 
                // $destination = detailWisata::where('id',$detail['destination'])->first(); 
                
                $rute[$i]['routes'][$key]['from_id'] = isset($from->id) ? $from->id:null;
                $rute[$i]['routes'][$key]['from'] = isset($from->name) ? $from->name:null;
                // $rute[$i]['routes'][$key]['depature_id'] = $depature->id;
                // $rute[$i]['routes'][$key]['depature'] = $depature->namaWisata;
                $rute[$i]['routes'][$key]['to_id'] = isset($to->id) ? $to->id:null;
                $rute[$i]['routes'][$key]['to'] = isset($to->name) ? $to->name:null;
                // $rute[$i]['routes'][$key]['destination_id'] = $destination->id;
                // $rute[$i]['routes'][$key]['destination'] = $destination->namaWisata;
                
            }
                    
            $i++;
        }
        
        return $rute;
    }

    
}
