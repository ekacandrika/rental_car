<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BookingOrder;
use App\Models\DataPengajuan;
use App\Models\Pengajuan;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function viewLaporan()
    {
        //== Grafik Pendapatan ==//
        $hotels = DataPengajuan::whereYear('created_at', date('Y'))->where('status', 'Terverifikasi')->where('type', 'hotel')->get();
        $xstays = DataPengajuan::whereYear('created_at', date('Y'))->where('status', 'Terverifikasi')->where('type', 'xstay')->get();
        $rentals = DataPengajuan::whereYear('created_at', date('Y'))->where('status', 'Terverifikasi')->where('type', 'Rental')->get();
        $transfers = DataPengajuan::whereYear('created_at', date('Y'))->where('status', 'Terverifikasi')->where('type', 'Transfer')->get();
        $activities = DataPengajuan::whereYear('created_at', date('Y'))->where('status', 'Terverifikasi')->where('type', 'activity')->get();
        $tours = DataPengajuan::whereYear('created_at', date('Y'))->where('status', 'Terverifikasi')->where('type', 'tour')->get();
        $months_hotel = [];
        $months_xstay = [];
        $months_rental = [];
        $months_transfer = [];
        $months_activity = [];
        $months_tour = [];
        $earnings_hotel = [];
        $earnings_xstay = [];
        $earnings_rental = [];
        $earnings_transfer = [];
        $earnings_activity = [];
        $earnings_tour = [];
        $total_hotel = 0;
        $total_xstay = 0;
        $total_rental = 0;
        $total_transfer = 0;
        $total_activity = 0;
        $total_tour = 0;

        // Hotel
        foreach ($hotels as $hotel) {
            $month_hotel = $hotel->created_at->format('F');

            if (!in_array($month_hotel, $months_hotel)) {
                array_push($months_hotel, $month_hotel);
                $earnings_hotel[$month_hotel] = 0;
            }

            $earnings_hotel[$month_hotel] += $hotel->komisi;
            $total_hotel += $hotel->komisi;
        }

        // Xstay
        foreach ($xstays as $xstay) {
            $month_xstay = $xstay->created_at->format('F');

            if (!in_array($month_xstay, $months_xstay)) {
                array_push($months_xstay, $month_xstay);
                $earnings_xstay[$month_xstay] = 0;
            }

            $earnings_xstay[$month_xstay] += $xstay->komisi;
            $total_xstay += $xstay->komisi;
        }

        // Rental
        foreach ($rentals as $rental) {
            $month_rental = $rental->created_at->format('F');

            if (!in_array($month_rental, $months_rental)) {
                array_push($months_rental, $month_rental);
                $earnings_rental[$month_rental] = 0;
            }

            $earnings_rental[$month_rental] += $rental->komisi;
            $total_rental += $rental->komisi;
        }

        // Transfer
        foreach ($transfers as $transfer) {
            $month_transfer = $transfer->created_at->format('F');

            if (!in_array($month_transfer, $months_transfer)) {
                array_push($months_transfer, $month_transfer);
                $earnings_transfer[$month_transfer] = 0;
            }

            $earnings_transfer[$month_transfer] += $transfer->komisi;
            $total_transfer += $transfer->komisi;
        }

        // Activity
        foreach ($activities as $activity) {
            $month_activity = $activity->created_at->format('F');

            if (!in_array($month_activity, $months_activity)) {
                array_push($months_activity, $month_activity);
                $earnings_activity[$month_activity] = 0;
            }

            $earnings_activity[$month_activity] += $activity->komisi;
            $total_activity += $activity->komisi;
        }

        // Tour
        foreach ($tours as $tour) {
            $month_tour = $tour->created_at->format('F');

            if (!in_array($month_tour, $months_tour)) {
                array_push($months_tour, $month_tour);
                $earnings_tour[$month_tour] = 0;
            }

            $earnings_tour[$month_tour] += $tour->komisi;
            $total_tour += $tour->komisi;
        }
        //==========================================//

        //== Grafik Penjualan Produk ==//
        // Hotel
        $bulanHotel = array();
        $jumlahHotel = array();

        for ($i = 1; $i <= 12; $i++) {
            $bulanHotel[$i] = date("F", strtotime(date('Y') . "-" . $i . "-01"));
            $jumlahHotel[$i] = DB::table('booking')
                ->whereYear('created_at', date('Y'))
                ->whereMonth('created_at', $i)
                ->where('type', 'hotel')
                ->where('tgl_checkout', '!=', null)
                ->count();
        }

        // Xstay
        $bulanXstay = array();
        $jumlahXstay = array();

        for ($i = 1; $i <= 12; $i++) {
            $bulanXstay[$i] = date("F", strtotime(date('Y') . "-" . $i . "-01"));
            $jumlahXstay[$i] = DB::table('booking')
                ->whereYear('created_at', date('Y'))
                ->whereMonth('created_at', $i)
                ->where('type', 'xstay')
                ->where('tgl_checkout', '!=', null)
                ->count();
        }

        // Rental
        $bulanRental = array();
        $jumlahRental = array();

        for ($i = 1; $i <= 12; $i++) {
            $bulanRental[$i] = date("F", strtotime(date('Y') . "-" . $i . "-01"));
            $jumlahRental[$i] = DB::table('booking')
                ->whereYear('created_at', date('Y'))
                ->whereMonth('created_at', $i)
                ->where('type', 'Rental')
                ->where('tgl_checkout', '!=', null)
                ->count();
        }

        // Transfer
        $bulanTransfer = array();
        $jumlahTransfer = array();

        for ($i = 1; $i <= 12; $i++) {
            $bulanTransfer[$i] = date("F", strtotime(date('Y') . "-" . $i . "-01"));
            $jumlahTransfer[$i] = DB::table('booking')
                ->whereYear('created_at', date('Y'))
                ->whereMonth('created_at', $i)
                ->where('type', 'Transfer')
                ->where('tgl_checkout', '!=', null)
                ->count();
        }

        // Activity
        $bulanActivity = array();
        $jumlahActivity = array();

        for ($i = 1; $i <= 12; $i++) {
            $bulanActivity[$i] = date("F", strtotime(date('Y') . "-" . $i . "-01"));
            $jumlahActivity[$i] = DB::table('booking')
                ->whereYear('created_at', date('Y'))
                ->whereMonth('created_at', $i)
                ->where('type', 'activity')
                ->where('tgl_checkout', '!=', null)
                ->count();
        }

        // Tour
        $bulanTour = array();
        $jumlahTour = array();

        for ($i = 1; $i <= 12; $i++) {
            $bulanTour[$i] = date("F", strtotime(date('Y') . "-" . $i . "-01"));
            $jumlahTour[$i] = DB::table('booking')
                ->whereYear('created_at', date('Y'))
                ->whereMonth('created_at', $i)
                ->where('type', 'tour')
                ->where('tgl_checkout', '!=', null)
                ->count();
        }
        //===============================================//

        //== Grafik Seller Penjualan Produk Terbanyak ==//
        $produkTerjual = BookingOrder::whereYear('created_at', date('Y'))
            ->where('tgl_checkout', '!=', null)
            ->get();
        $user = User::all();
        $groupedData = $produkTerjual->groupBy('toko_id');
        $data = [];

        foreach ($groupedData as $key => $value) {
            $userData = $user->where('id', $key)->first();
            $data[] = [
                'namaUser' => $userData->first_name,
                'jumlahTerjual' => $value->count()
            ];
        }
        //============================================//

        return view('BE.admin.laporan-admin', compact(
            'months_hotel',
            'months_xstay',
            'months_rental',
            'months_transfer',
            'months_activity',
            'months_tour',
            'earnings_hotel',
            'earnings_xstay',
            'earnings_rental',
            'earnings_transfer',
            'earnings_activity',
            'earnings_tour',
            'total_hotel',
            'total_xstay',
            'total_rental',
            'total_transfer',
            'total_activity',
            'total_tour',
            'bulanHotel',
            'bulanXstay',
            'bulanRental',
            'bulanTransfer',
            'bulanActivity',
            'bulanTour',
            'jumlahHotel',
            'jumlahXstay',
            'jumlahRental',
            'jumlahTransfer',
            'jumlahActivity',
            'jumlahTour',
            'data',
        ));
    }

    public function viewPengajuanPenarikan()
    {
        $data = DataPengajuan::where('status', 'Request')->get();
        $success = DataPengajuan::where('status', 'Terverifikasi')->get();

        return view('BE.admin.list-pengajuan', compact('data', 'success'));
    }

    public function verifikasiPengajuan(Request $request, $id)
    {
        $data = DataPengajuan::findOrFail($id);
        $data_pengajuan = Pengajuan::where('id', $request->pengajuan_id)->first();

        $data->update([
            'status' => 'Terverifikasi'
        ]);

        $data_pengajuan->update([
            'status' => 'Sudah Bayar'
        ]);

        return redirect()->back();
    }
}
