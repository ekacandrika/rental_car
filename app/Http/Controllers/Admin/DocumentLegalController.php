<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterDocumentLegal;

class DocumentLegalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('BE.admin.master-document-legal');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dokument = New MasterDocumentLegal();

        $validate = $request->validate([
            'document_title'=>'required',
        ],[
            'document_title.required'=>'Judul/Nama Dokumen tidak bole kosong'
        ]);

        $dokument::create($request->all());

        return redirect()->route('legal.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dokumen = MasterDocumentLegal::findOrFail($id);
        return view('BE.admin.master-document-legal-edit',compact('dokumen','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dokumen = MasterDocumentLegal::findOrFail($id);
        return view('BE.admin.master-document-legal-edit',compact('dokumen','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // ddd('test',$request->all());
        $dokumen = MasterDocumentLegal::findOrFail($id);

        $validate = $request->validate([
            'document_title'=>'required',
        ],[
            'document_title.required'=>'Judul/Nama Dokumen tidak bole kosong'
        ]);

        $dokumen->update($request->all());

        return redirect()->route('legal.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dokumen = MasterDocumentLegal::findOrFail($id);
        $dokumen->delete();
        return redirect()->route('legal.index');   
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datas(Request $request)
    {
        $cari = $request->cari;    
        $doc = New MasterDocumentLegal();
    
        if($cari){
            $dokumen = $doc->where('document_title','like','%'.$cari.'%')->paginate(3);
        }else{
            $dokumen = $doc->paginate(3);
        }

        if($dokumen){
            $status='success';
        }else{
            $status='error';
        }

        // dd($doc);

        return response()->json([
            'success'=>true,
            'datas'=>$dokumen,
            'status'=>$status
        ]);
    }
}
