<?php

namespace App\Http\Controllers;

use App\Models\pajakAdmin;
use App\Models\pajakLocal;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Product;

class PajakAdminController extends Controller
{
    public function index(PajakAdmin $pajakAdmin)
    {
        $pajakAdmin = PajakAdmin::first();
        $user = User::get();
        $produk = Product::select('type')->distinct()->get();
        $pajakLocals = PajakLocal::join('users', 'pajak_locals.user_id', '=', 'users.id')
            ->join('pajak_admins', 'pajak_locals.pajak_id', '=', 'pajak_admins.id')
            ->select(
                'users.first_name',
                'users.role',
                'pajak_locals.*',
                'pajak_admins.markup'
            )->get();
        //        dd($pajakLocals);
        return view('BE.admin.Pajak.index', compact('pajakAdmin', 'user', 'pajakLocals', 'produk'));
    }

    public function pajakUpdate(PajakAdmin $pajakAdmin, Request $request)
    {
        $this->validate(
            $request,
            [
                'komisi' => 'required',
                'markup' => 'required',
                'komisiAgent' => 'required',
                'komisiCorporate' => 'required',
                'user_id' => 'required',
            ],
            [
                'komisi.required' => 'Komisi Belum Diisi',
                'markup.required' => 'Markup Belum Diisi',
                'komisiAgent.required' => 'Komisi Agent Belum Diisi',
                'komisiCorporate.required' => 'Komisi Corporate Belum Diisi',
            ]
        );

        $pajakAdmin = PajakAdmin::first();
        $pajakAdmin->komisi = $request->komisi;
        $pajakAdmin->markup = $request->markup;
        $pajakAdmin->komisiAgent = $request->komisiAgent;
        $pajakAdmin->komisiCorporate = $request->komisiCorporate;
        $pajakAdmin->user_id = $request->user_id;
        //        $pajakAdmin->total_pajak = $markup - $komisi;

        $pajakAdmin->update();
        return back()->with('success', 'Selamat Anda Berhasil Mengubah Pajak Global');
    }

    public function pajakLocalStore(Request $request)
    {
        // dd($request->all());
        $this->validate(
            $request,
            [
                'markupLocal' => 'required',
                'komisiLocal' => 'required',
                'komisiLocalAgent' => 'required',
                'komisiLocalCorporate' => 'required',
                'statusMarkupLocal' => 'required',
                'statusKomisiLocal' => 'required',
                'statusKomisiLocalAgent' => 'required',
                'statusKomisiLocalCorporate' => 'required',
                'user_id' => 'required',
                'produk_type' => 'required',
            ],
            [
                'komisiLocal.required' => 'Komisi Local Belum Diisi',
                'markupLocal.required' => 'Markup Local Belum Diisi',
                'komisiLocalAgent.required' => 'Komisi Agent Belum Diisi',
                'komisiLocalCorporate.required' => 'Komisi Corporate Belum Diisi',
                'komisiCorporate.required' => 'Komisi Corporate Belum Diisi',
                'user_id.required' => 'Seller Belum Dipilih',
                'produk_type.required' => 'Produk Belum Dipilih',
            ]
        );
        //        $pajakAdmin = pajakAdmin::first();


        $pajakLocal = new PajakLocal();
        $pajakLocal->markupLocal = $request->markupLocal;
        $pajakLocal->komisiLocalAgent = $request->komisiLocalAgent;
        $pajakLocal->komisiLocalCorporate = $request->komisiLocalCorporate;
        $pajakLocal->komisiLocal = $request->komisiLocal;
        $pajakLocal->statusMarkupLocal = $request->statusMarkupLocal;
        $pajakLocal->statusKomisiLocalAgent = $request->statusKomisiLocalAgent;
        $pajakLocal->statusKomisiLocalCorporate = $request->statusKomisiLocalCorporate;
        $pajakLocal->statusKomisiLocal = $request->statusKomisiLocal;
        $pajakLocal->user_id = $request->user_id;
        $pajakLocal->produk_type = $request->produk_type;
        $pajakLocal->pajak_id = 1;
        //        dd($pajakLocal);
        $pajakLocal->save();
        return back()->with('success', 'Selamat Anda Berhasil Menambah Pajak Lokal');
    }

    public function pajakLocalUpdate(Request $request, $id)
    {

        $this->validate(
            $request,
            [
                'komisiLocalAgent' => 'required',
                'markupLocal' => 'required',
                'komisiLocalCorporate' => 'required',
                'komisiLocal' => 'required',
                'user_id' => 'required',
                'produk_type' => 'required',
            ],
            [
                'komisiLocal.required' => 'Komisi Local Belum Diisi',
                'markupLocal.required' => 'Markup Local Belum Diisi',
                'komisiLocalAgent.required' => 'Komisi Agent Belum Diisi',
                'komisiLocalCorporate.required' => 'Komisi Corporate Belum Diisi',
                'komisiCorporate.required' => 'Komisi Corporate Belum Diisi',
                'user_id.required' => 'Seller Belum Dipilih',
                'produk_type.required' => 'Produk Belum Dipilih',
            ]
        );

        $pajakLocal = PajakLocal::find($id);

        $pajakLocal->komisiLocalAgent = $request->komisiLocalAgent;
        $pajakLocal->markupLocal = $request->markupLocal;
        $pajakLocal->komisiLocalCorporate = $request->komisiLocalCorporate;
        $pajakLocal->komisiLocal = $request->komisiLocal;
        $pajakLocal->statusMarkupLocal = $request->statusMarkupLocal;
        $pajakLocal->statusKomisiLocalAgent = $request->statusKomisiLocalAgent;
        $pajakLocal->statusKomisiLocalCorporate = $request->statusKomisiLocalCorporate;
        $pajakLocal->statusKomisiLocal = $request->statusKomisiLocal;
        $pajakLocal->user_id = $request->user_id;
        $pajakLocal->produk_type = $request->produk_type;
        $pajakLocal->pajak_id = 1;
        //        dd($pajakLocal);
        $pajakLocal->update();
        return redirect()->route('pajak')->with('success', 'Selamat Anda Berhasil Mengubah Pajak Lokal');
    }

    public function pajakLocalEdit($id)
    {

        $pajakAdmin = PajakAdmin::first();
        $user = User::get();
        $produk = Product::select('type')->distinct()->get();
        $pajakLocals = PajakLocal::findOrFail($id)->join('users', 'pajak_locals.user_id', '=', 'users.id')
            ->join('pajak_admins', 'pajak_locals.pajak_id', '=', 'pajak_admins.id')
            ->select(
                'users.first_name',
                'pajak_locals.*',
                'pajak_admins.markup'
            )->first();

        //        dd($pajakLocals);


        return view('BE.admin.Pajak.edit', compact('pajakAdmin', 'user', 'pajakLocals', 'produk'));
    }

    public function pajakDestroy($id)
    {
        $pajakLoc = PajakLocal::findOrFail($id);
        $pajakLoc->delete();
        return redirect()->route('pajak')
            ->with('success', 'Pajak Local Berhasil Dihapus');
    }
}
