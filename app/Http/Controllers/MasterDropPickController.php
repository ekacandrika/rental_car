<?php

namespace App\Http\Controllers;

use App\Models\MasterDropPick;
use Illuminate\Http\Request;

class MasterDropPickController extends Controller
{
   public function viewMasterDropPick()
   {


       $data = MasterDropPick::all()->sortByDesc('updated_at');
       $latestItem = MasterDropPick::all()->sortByDesc('updated_at')->first();
       $latestItemId = $latestItem && $latestItem->created_at >= \Carbon\Carbon::now()->subDays(7) ? $latestItem->id : null;
       session(['latestItemId' => $latestItemId]);


       return view('BE.seller.transfer.Master.DropPick.index', compact('data'));
   }


   public function addMasterDropPick()
   {


       return view('BE.seller.transfer.Master.DropPick.add');
   }


   public function storeMasterDropPick(Request $request)
   {
       $validatedData = $request->validate([
           'judulDropPick' => 'required|max:255',
           'latitude' => 'required|max:255',
           'longitude' => 'required|max:255',
       ], [
           'judulDropPick' => 'Isi Judul Drop Off - Pick Up',
           'latitude' => 'Isi Latitude',
           'longitude' => 'Isi Longitude',
       ]);


//        dd($validatedData);


       MasterDropPick::create($validatedData);


       return redirect()->route('masterDropPick')->with('success', 'Tempat Drop off - Pick Up Berhasil Ditambahkan');
   }
   public function editMasterDropPick($id)
   {
       $item  = MasterDropPick::findOrFail($id);


       return view('BE.seller.transfer.master.DropPick.edit', compact('item'));
   }


   public function updateMasterDropPick(Request $request, $id)
   {
       $validatedData = $request->validate([
           'judulDropPick' => 'required|max:255',
           'latitude' => 'required|max:255',
           'longitude' => 'required|max:255',
       ], [
           'judulDropPick' => 'Isi Judul Drop Off - Pick Up',
           'latitude' => 'Isi Latitude',
           'longitude' => 'Isi Longitude',
       ]);


       $dropPick = MasterDropPick::findOrFail($id);
       $dropPick->update($validatedData);




       return redirect()->route('masterDropPick')->with('success', 'Tempat Drop off - Pick Up Berhasil Diubah');
   }


   public function deleteMasterDropPick($id)
   {
       $data = MasterDropPick::findOrFail($id);
       $data->delete();
       return redirect()->route('masterDropPick')
           ->with('success', 'Tempat Drop Off - Pick Up berhasil dihapus');
   }
}
