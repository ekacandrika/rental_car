<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Models\BookingOrder;
use App\Models\Detailpesanan;
use App\Models\Pengajuan;
use App\Models\PajakAdmin;
use App\Models\PajakLocal;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Xendit\Xendit;
use App\Models\Order;
use App\Models\Receipt;

class PaymentController extends Controller
{
    // private $token = 'xnd_development_iEcSNUmRAtJVbdXDxaB7ZB4NgpKMOxzKlN3KkjQw9wTv9XVIafq1qIOQi59ULE';
    // xnd_public_production_9WwbW75C9ArUZ1yNr2mNC84uzIwt4OYyGjmhHzd4sC04eRPQqRH7gWU0Qnpetu
    // xnd_production_yik8wBp3gXUW8798k5SkuANdBem1fHtvf10R0KyFIMKh8F3aLRjyf1JxjHKSiBQ
    private $token = 'xnd_development_2xG4JLMpgPupacwEq1326YCW1vmURUtgsBmGxHfbPrMqzEZkYusAgxcrapGVARb';


    public function createVA(Request $request)
    {
        $booking_idx = explode(",", $request->booking_id);
        // dd($booking_idx);
        $payment = json_decode(request()->cookie('payment'), true);
        Xendit::setApiKey($this->token);

        $orderCode = 'CO' . mt_rand(1, 99) . date('dmY');

        $params = [
            "external_id" => $orderCode,
            "bank_code" => 'MANDIRI',
            "name" => $request->name,
            "expected_amount" => $request->harga,
            "is_closed" => true,
            "expiration_date" => Carbon::now()->addDay(1)->toISOString(),
            "is_single_use" => true
        ];

        $idx_booking = explode(",", $request->booking_code_arr);
        $booking_order_idx = collect([]);
        for ($i = 0; $i < count($idx_booking); $i++) {
            // Update status in table booking from "0 => In Cart" to "1 => Checkout"
            $booking = BookingOrder::where('customer_id', $request->customer_id)->where('status', '0')->where('booking_code', $idx_booking[$i])->first();
            $data = Detailpesanan::where('customer_id', $request->customer_id)->where('booking_code', $idx_booking[$i])->first();
            $booking_order_idx->push($booking->id);
            $booking->update([
                'status' => '1',
                'tgl_checkout' => date('Y-m-d'),
                'status_pembayaran' => 'PENDING',
            ]);
            $data->update([
                'status' => '1'
            ]);
        }

        // // Update status in table booking from "0 => In Cart" to "1 => Checkout"
        // $booking = BookingOrder::where('customer_id', $request->customer_id)->where('status', '0')->get();
        // $data = Detailpesanan::where('customer_id', $request->customer_id)->get();
        // foreach ($booking as  $x) {
        //     $x->update([
        //         'status' => '1',
        //         'tgl_checkout' => date('Y-m-d'),
        //         'status_pembayaran' => 'PENDING',
        //     ]);
        // }
        // foreach ($data as  $xa) {
        //     $xa->update([
        //         'status' => '1'
        //     ]);
        // }

        $data_booking = Detailpesanan::with('productdetail')->where('status', '1')->get();
        $pajak_global = PajakAdmin::first();
        $pajak_global_result = intval($pajak_global->komisi) / 100;
        foreach ($data_booking as $foo) {
            $pajak_local = PajakLocal::where('user_id', $foo->toko_id)->first();
            $pengajuan = new Pengajuan;
            $pengajuan->product_id = $foo->productdetail->product_id;
            $pengajuan->toko_id = $foo->toko_id;
            $pengajuan->agent_id = $foo->agent_id;
            $pengajuan->booking_code = $foo->booking_code;
            $pengajuan->tgl_checkout = date('Y-m-d');
            $pengajuan->total_price = $foo->harga_asli;
            if ($pajak_local == null) {
                $hasil = intval($foo->harga_asli) * $pajak_global_result;
                $pengajuan->komisi = intval($foo->harga_asli) - $hasil;
            } else {
                if ($pajak_local->komisiLocal == null) {
                    $hasil = intval($foo->harga_asli) * $pajak_global_result;
                    $pengajuan->komisi = intval($foo->harga_asli) - $hasil;
                } else {
                    if ($pajak_local->statusKomisiLocal == 'percent') {
                        $val = intval($pajak_local->komisiLocal) / 100;
                        $ab = intval($foo->harga_asli) * $val;
                        $pengajuan->komisi = intval($foo->harga_asli) - $ab;
                    } else {
                        $pengajuan->komisi = intval($foo->harga_asli) - $pajak_local->komisiLocal;
                    }
                }
            }
            $pengajuan->type = $foo->type;
            $pengajuan->status = "-";
            $pengajuan->save();
        }

        // Delete all data in table detailpesan when status "1 => Checkout"
        $data_detailPesanan = Detailpesanan::where('status', '1')->get();
        foreach ($data_detailPesanan as  $ac) {
            Detailpesanan::where('id', $ac->id)->delete();
        }

        // Save Order To Database
        // $booking_idx = explode(",", $request->booking_id);
        for ($i = 0; $i < count($booking_order_idx); $i++) {
            Order::create([
                "external_id" => $orderCode,
                "payment_channel" => 'Virtual Account',
                "booking_id" => (int)$booking_order_idx[$i],
                "total_price" => $request->harga,
                "order_datetime" => Carbon::now(),
                "status" => 'PENDING',
            ]);
        }

        $createVA = \Xendit\VirtualAccounts::create($params);
        $json = json_encode($createVA, true);
        $decode = json_decode($json, true);

        $payment = [
            'booking_id' => $booking_order_idx,
            'harga' => $request->harga,
            'nama_customer' => $request->nama_customer,
            'external_id' => $decode['external_id'],
        ];
        $cookie = cookie('payment', json_encode($payment), 2880);
        return redirect()->route('order.createInvoice')->cookie($cookie);
    }

    public function createInvoice(Request $request)
    {
        $payment = json_decode(request()->cookie('payment'), true);
        Xendit::setApiKey($this->token);

        $params = [
            'external_id' => $payment['external_id'],
            'payer_email' => 'test_invoice@xendit.co',
            'description' => 'Kamtuu Booking Payment',
            'amount' => $payment['harga']
        ];

        $createInvoice = \Xendit\Invoice::create($params);
        $json = json_encode($createInvoice, true);
        $decode = json_decode($json, true);

        // Redirect to payment tab
        $url = $decode['invoice_url'];
        return redirect()->away($url);
    }
    public function callbackPayment()
    {

        Xendit::setApiKey($this->token);

        $xenditXCallbackToken = 'FCTvb5C9TssvRLVziFOmPQHHLYarCngKuzDfIhcgdUt7YRbJ';

        $reqHeaders = getallheaders();
        $xIncomingCallbackTokenHeader = isset($reqHeaders['x-callback-token']) ? $reqHeaders['x-callback-token'] : "";

        /* 
        {
                "id": "579c8d61f23fa4ca35e52da4",
                "external_id": "invoice_123124123",
                "user_id": "5781d19b2e2385880609791c",
                "is_high": true,
                "payment_method": "BANK_TRANSFER",
                "status": "PAID",
                "merchant_name": "Xendit",
                "amount": 50000,
                "paid_amount": 50000,
                "bank_code": "PERMATA",
                "paid_at": "2016-10-12T08:15:03.404Z",
                "payer_email": "wildan@xendit.co",
                "description": "This is a description",
                "adjusted_received_amount": 47500,
                "fees_paid_amount": 0,
                "updated": "2016-10-10T08:15:03.404Z",
                "created": "2016-10-10T08:15:03.404Z",
                "currency": "IDR",
                "payment_channel": "PERMATA",
                "payment_destination": "888888888888"
        }
        */

        /* 
          "external_id": "dana-ewallet",
            "amount": 1001,
            "business_id": "5850eteStin791bd40096364",
            "payment_status": "PAID",
            "transaction_date": "2019-04-07T01:35:46.658Z",
            "callback_authentication_token": "sample-token-id=="
        */

        if ($xIncomingCallbackTokenHeader === $xenditXCallbackToken) {

            $input = file_get_contents("php://input");
            $arrInput = json_decode($input, true);

            $external_id = $arrInput['external_id'];
            $status = $arrInput['status'];

            $payment = Order::where('external_id', $external_id)->exists();
            $orders = Order::where('external_id', $external_id)->get();

            if ($payment) {
                if ($status == 'PAID') {

                    $update = Order::where('external_id', $external_id)->update([
                        'status' => 'PAID',
                    ]);


                    foreach ($orders as $key => $order) {

                        BookingOrder::where('id', $order->booking_id)->update([
                            'status' => 2,
                            'status_pembayaran' => 'PAID'
                        ]);
                    }

                    return 'PEMBAYARAN BERHASIL!';
                } else {

                    $update = Order::where('external_id', $external_id)->update([
                        'status' => 'UNPAID',
                    ]);

                    foreach ($orders as $key => $order) {

                        BookingOrder::where('id', $order->booking_id)->update([
                            'status' => 0,
                            'status_pembayaran' => '-'
                        ]);
                    }

                    return 'PEMBAYARAN GAGAL!';
                }


                // insert to table receipt
                Receipt::create([
                    'external_id' => $external_id,
                    'merchant_name' => $arrInput['merchant_name'],
                    'status' => $arrInput['status'],
                    'payment_channel' => $arrInput['payment_channel'],
                ]);
            } else {
                return response()->json([
                    'message' => 'DATA TIDAK DITEMUKAN!'
                ]);
            }
        } else {
            http_response_code(403);
        }
    }

    public function callbackPaymentFVA()
    {

        Xendit::setApiKey($this->token);

        $xenditXCallbackToken = 'FCTvb5C9TssvRLVziFOmPQHHLYarCngKuzDfIhcgdUt7YRbJ';

        $reqHeaders = getallheaders();
        $xIncomingCallbackTokenHeader = isset($reqHeaders['x-callback-token']) ? $reqHeaders['x-callback-token'] : "";

        /* 
        {
                "id": "579c8d61f23fa4ca35e52da4",
                "external_id": "invoice_123124123",
                "user_id": "5781d19b2e2385880609791c",
                "is_high": true,
                "payment_method": "BANK_TRANSFER",
                "status": "PAID",
                "merchant_name": "Xendit",
                "amount": 50000,
                "paid_amount": 50000,
                "bank_code": "PERMATA",
                "paid_at": "2016-10-12T08:15:03.404Z",
                "payer_email": "wildan@xendit.co",
                "description": "This is a description",
                "adjusted_received_amount": 47500,
                "fees_paid_amount": 0,
                "updated": "2016-10-10T08:15:03.404Z",
                "created": "2016-10-10T08:15:03.404Z",
                "currency": "IDR",
                "payment_channel": "PERMATA",
                "payment_destination": "888888888888"
        }
        */

        /* 
          "external_id": "dana-ewallet",
            "amount": 1001,
            "business_id": "5850eteStin791bd40096364",
            "payment_status": "PAID",
            "transaction_date": "2019-04-07T01:35:46.658Z",
            "callback_authentication_token": "sample-token-id=="
        */

        if ($xIncomingCallbackTokenHeader === $xenditXCallbackToken) {

            $input = file_get_contents("php://input");
            $arrInput = json_decode($input, true);

            $external_id = $arrInput['external_id'];
            $status = $arrInput['status'];

            $payment = Order::where('external_id', $external_id)->exists();
            $orders = Order::where('external_id', $external_id)->get();

            if ($payment) {

                $update = Order::where('external_id', $external_id)->update([
                    'status' => 'PAID',
                ]);

                foreach ($orders as $key => $order) {

                    BookingOrder::where('id', $order->booking_id)->update([
                        'status' => 2,
                        'status_pembayaran' => 'PAID'
                    ]);
                }

                return 'PEMBAYARAN BERHASIL!';

                Receipt::create([
                    'external_id' => $external_id,
                    'merchant_name' => $arrInput['merchant_name'],
                    'status' => $arrInput['status'],
                    'payment_channel' => $arrInput['payment_channel'],
                ]);
            } else {
                return response()->json([
                    'message' => 'DATA TIDAK DITEMUKAN!'
                ]);
            }
        } else {
            http_response_code(403);
        }
    }
}
