<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Hotel;
use App\Models\Paket;
use App\Models\Price;
use App\Models\Pilihan;
use App\Models\Product;
use App\Models\Regency;
use App\Models\Village;
use App\Models\Discount;
use App\Models\District;
use App\Models\Province;
use App\Models\Itenerary;
use App\Models\Kelolatoko;
use App\Models\reviewPost;
use App\Models\detailObjek;
use App\Models\Likeproduct;
use App\Models\Masterpoint;
use App\Models\detailWisata;
use Illuminate\Http\Request;
use App\Models\Productdetail;
use App\Models\settingGeneral;
use App\Models\settingHomepages;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class homepagesController extends Controller
{

    function getPromoproduct(String $type)
    {
        return product::where('type', 'activity')
            ->has('productdetail')
            ->with('productdetail.diskon', 'productdetail.harga', 'productdetail.masterkamar', 'productdetail.detailkendaraan', 'productdetail.regency', 'user')
            ->whereHas('productdetail.diskon')
            ->limit(10)
            ->latest()
            ->get()->filter(function ($item) {
                $tgl_start = json_decode($item->productdetail->diskon['tgl_start']);
                $tgl_end = json_decode($item->productdetail->diskon['tgl_end']);

                foreach ($tgl_start as $key => $start) {
                    if (Carbon::now()->between(Carbon::parse($start)->toDateTimeString(), Carbon::parse($tgl_end[$key])->toDateTimeString())) {
                        return $item;
                    }
                }
            });
    }

    function getproduct(String $type)
    {
        if ($type == 'hotel' || $type == 'xstay') {
            return product::where('type', $type)
            ->has('productdetail.masterkamar')
            ->with('productdetail.diskon', 'productdetail.harga', 'productdetail.masterkamar', 'productdetail.detailkendaraan', 'productdetail.regency', 'user')
            ->limit(5)
            ->inRandomOrder()
            ->get();
        }

        return product::where('type', $type)
            ->has('productdetail')
            ->with('tagged', 'productdetail.diskon', 'productdetail.harga', 'productdetail.masterkamar', 'productdetail.detailkendaraan', 'productdetail.regency', 'user')
            ->limit(5)
            ->inRandomOrder()
            ->get();

        // if ($type == 'hotel' || $type == 'xstay') {
        //     return product::where('type', $type)
        //     ->has('productdetail.masterkamar')
        //     ->whereHas('productdetail', function($query){
        //         $query->where('available', 'publish');
        //     })
        //     ->with('productdetail.diskon', 'productdetail.harga', 'productdetail.masterkamar', 'productdetail.detailkendaraan', 'productdetail.regency', 'user')
        //     ->limit(5)
        //     ->inRandomOrder()
        //     ->get();
        // }

        // return product::where('type', $type)
        //     ->whereHas('productdetail', function($query){
        //         $query->where('available', 'publish');
        //     })
        //     ->with('productdetail.diskon', 'productdetail.harga', 'productdetail.masterkamar', 'productdetail.detailkendaraan', 'productdetail.regency', 'user')
        //     ->limit(5)
        //     ->inRandomOrder()
        //     ->get();
    }

    public function getTagLocation(){
        $data = [];

        $provinces = Province::all();
        $i=0;
        $x=0;
        $y=0;
        //foreach
        foreach($provinces as $province){
            // $data->push(
            //     [
            //         'provinces_id' =>$province->id,
            //         'provinces_name'=>$province->name
            //     ]
            // );
            $data[$i]['provinces_id'] =$province->id;
            $data[$i]['provinces_name']=$province->name;
           
            $regencies = Regency::where('province_id',$province->id)->get();

            foreach($regencies as $regency){
                // $data->push(
                //     [
                //         'regencies_id' =>$regency->id,
                //         'regencies_name'=>$regency->name
                //     ]
                // );
                $data[$i]['regency'][$x]['regencies_id'] =$regency->id;
                $data[$i]['regency'][$x]['regencies_name']=$regency->name;
                $x++;
                    
                $districts = District::where('regency_id',$regency->id)->groupBy('name')->get();

                foreach($districts as $district){
                    $data[$i]['district'][$y]['districts_id'] =$district->id;
                    $data[$i]['district'][$y]['districts_name']=$district->name;

                    $y++;
                }
            }

            $i++;
        }

        return $data;
    }

    public function getSelectFromToTour(){

        $turFromTo = Province::with('regencies')->get();

        return $turFromTo;
    }

    public function index()
    {

        $location = $this->getTagLocation();
        $provinsi = Province::all();
        $regencyTur = Regency::all();
        $kecamatan = District::all();
        $today = Carbon::today()->format('m/d/Y');
        $home_image_slider = settingHomepages::where('section', 'Home')->first();
        $activity = product::has('productdetail')->with('productdetail')->where('type', 'activity')->get();
        $turs = product::has('productdetail')->with('productdetail')->where('type', 'tour')->get();

        //get master point/pin location
        $titik_lokasi = Masterpoint::select("titik_route.id as id","regencies.id as regency_id","titik_route.name_titik as name")
                            ->join('districts','districts.id','=','titik_route.district_id')
                            ->join('regencies','regencies.id','=','districts.regency_id')
                            ->get();

        $collect_pin = collect([]);
        
        foreach($titik_lokasi as $key => $value){
            // dump($value->district);

            $collect_pin->push([
                'nama_titik'=>$value->name,
                'regency_id'=>$value->regency_id
            ]);
        }

          $activity = product::where('type', 'activity')
            ->has('productdetail')
            ->with('productdetail.harga', 'productdetail.regency', 'user', 'productdetail.diskon', 'productdetail')
            ->whereHas('productdetail.diskon')
            ->limit(10)
            ->latest()
            ->get()->filter(function ($item) {
                $tgl_start = json_decode($item->productdetail->diskon['tgl_start']);
                $tgl_end = json_decode($item->productdetail->diskon['tgl_end']);

                foreach ($tgl_start as $key => $start) {
                    if (Carbon::now()->between(Carbon::parse($start)->toDateTimeString(), Carbon::parse($tgl_end[$key])->toDateTimeString())) {
                        return $item;
                    }
                }
            });

        $tur = product::where('type', 'tour')
            ->has('productdetail')
            ->with('tagged', 'productdetail.harga', 'productdetail.regency', 'user', 'productdetail.diskon')
            ->whereHas('productdetail.diskon')
            ->limit(10)
            ->latest()
            ->get()->filter(function ($item) {
                $tgl_start = json_decode($item->productdetail->diskon['tgl_start']);
                $tgl_end = json_decode($item->productdetail->diskon['tgl_end']);

                foreach ($tgl_start as $key => $start) {
                    if (Carbon::now()->between(Carbon::parse($start)->toDateTimeString(), Carbon::parse($tgl_end[$key])->toDateTimeString())) {
                        return $item;
                    }
                }
            });

        $transfer = $this->getproduct('Transfer');;
        $rental = $this->getproduct('rental');

        $hotel = $this->getProductDetail('hotel','non api');
        $xstay = $this->getProductDetail('activity','non api');

        $activity_populer = $this->getproduct('activity');
        $tur_populer = $this->getproduct('tour');
        $transfer_populer = $this->getproduct('Transfer');
        $rental_populer = $this->getproduct('rental');
        $hotel_populer = $this->getproduct('hotel');
        $xstay_populer = $this->getproduct('xstay');


        $sellers = User::where('role', 'seller')->whereNotNull('email_verified_at')
            ->with(['product' => function ($query) {
                $query->select('user_id', 'type', DB::raw('count(*) as total'))->groupBy('product.user_id', 'product.type');
            }])->limit(20)->latest()->get();


        // $settingGeneral = settingGeneral::select('regency_order')->find(99)->toArray();
        // $settingGeneral = json_decode($settingGeneral['regency_order']);

        $settingGeneral = [];
        $settingGeneral = [];

        $top_destination = [];

        foreach ($settingGeneral as $data) {
            $name = Regency::where('id', $data)->first();
            $top_destination[] = [
                $name->name,
            ];
        }

        $kontens = [];
        $kamtuuSets = [];
        $travellerSets = [];
        $sellerSets = [];
        $agenSets = [];
        $storeSets = [];
        $corporateSets = [];
        $layananSets = [];
        $kerjasamaSets = []; 

        /* $kontens = settingHomepages::where('section', '=', 'Konten_1')->get();
        $kamtuuSets = settingHomepages::where('section', '=', 'Kamtuu')->get();
        $travellerSets = settingHomepages::where('section', '=', 'Traveller')->get();
        $sellerSets = settingHomepages::where('section', '=', 'Seller')->get();
        $agenSets = settingHomepages::where('section', '=', 'Agen')->get();
        $storeSets = settingHomepages::where('section', '=', 'OFStore')->get();
        $corporateSets = settingHomepages::where('section', '=', 'Corporate')->get();
        $layananSets = settingHomepages::where('section', '=', 'Layanan')->get();
        $kerjasamaSets = settingHomepages::where('section', '=', 'Kerjasama')->get(); */

        //add list product by regenci by eka
        $turRegions = [];
        $hotelRegions = [];
        $xstayRegions = [];
        $rentalRegions = [];
        $transferRegions = [];
        $activityRegions = [];

        /*
        $hotelRegions = $this->productListRegency('hotel');
        $xstayRegions = $this->productListRegency('xstay');
        $rentalRegions = $this->productListRegency('rental');
        $transferRegions = $this->productListRegency('transfer');
        $activityRegions = $this->productListRegency('activity'); 
        */

        $search_from_to_tour = $this->getSelectFromToTour();
        return view('Homepage.index', compact(
            'kontens',
            'kamtuuSets',
            'travellerSets',
            'sellerSets',
            'agenSets',
            'storeSets',
            'corporateSets',
            'layananSets',
            'kerjasamaSets',
            'activity',
            'activity',
            'provinsi',
            'sellers',
            'tur',
            'turs',
            'transfer',
            'rental',
            'hotel',
            'xstay',
            'home_image_slider',
            'regencyTur',
            'activity_populer',
            'tur_populer',
            'transfer_populer',
            'rental_populer',
            'hotel_populer',
            'xstay_populer',
            'top_destination',
            'turRegions','transferRegions','rentalRegions','activityRegions','hotelRegions','xstayRegions',
            'kecamatan',
            'location',
            'search_from_to_tour',
            'collect_pin'
        ));
    }

    public function indexNew()
    {
        $home_image_slider = settingHomepages::where('section', 'Home')->first();
       
        //get master point/pin location
        $titik_lokasi = Masterpoint::select("titik_route.id as id","titik_route.district_id as district_id","titik_route.name_titik as name")
                        ->get();

        $collect_pin = collect([]);
        
        foreach($titik_lokasi as $key => $value){
           $collect_pin->push([
                'nama_titik'=>$value->name,
                'district_id'=>$value->district_id
            ]);
        }
       
        $activity = $this->getProductDetail('activity','non api');
        $tur = $this->getProductDetail('tour','non api');
        $transfer = $this->getProductDetail('transfer','non api');
        $rental = $this->getProductDetail('rental','non api');
        $hotel = $this->getProductDetail('hotel','non api');
        $xstay = $this->getProductDetail('xstay','non api');

        $activity_populer = $this->getProductDetail('activity','non api');
        $tur_populer = $this->getProductDetail('tour','non api');
        $transfer_populer = $this->getProductDetail('transfer','non api');
        $rental_populer = $this->getProductDetail('rental','non api');
        $hotel_populer = $this->getProductDetail('hotel','non api');
        $xstay_populer = $this->getProductDetail('xstay','non api');


        $sellers = User::where('role', 'seller')->whereNotNull('email_verified_at')
            ->with(['product' => function ($query) {
                $query->select('user_id', 'type', DB::raw('count(*) as total'))->groupBy('product.user_id', 'product.type');
            }])->limit(20)->latest()->get();


        $settingGeneral = settingGeneral::select('regency_order')->find(99)->toArray();
        $settingGeneral = json_decode($settingGeneral['regency_order']);

        $settingGeneral = [];
        $settingGeneral = [];

        $top_destination = [];

        foreach ($settingGeneral as $data) {
            $name = Regency::where('id', $data)->first();
            $top_destination[] = [
                $name->name,
            ];
        }

        $kamtuuSets = settingHomepages::where('section', '=', 'Kamtuu')->get();
        $travellerSets = settingHomepages::where('section', '=', 'Traveller')->get();
        $sellerSets = settingHomepages::where('section', '=', 'Seller')->get();
        $agenSets = settingHomepages::where('section', '=', 'Agen')->get();
        $storeSets = settingHomepages::where('section', '=', 'OFStore')->get();
        $corporateSets = settingHomepages::where('section', '=', 'Corporate')->get();
        $layananSets = settingHomepages::where('section', '=', 'Layanan')->get();
        $kerjasamaSets = settingHomepages::where('section', '=', 'Kerjasama')->get();
        // dd($activity_populer);
        $search_from_to_tour = $this->getSelectFromToTour();
        return view('homepage.new-index',
            compact(
            'kamtuuSets',
            'travellerSets',
            'sellerSets',
            'agenSets',
            'storeSets',
            'corporateSets',
            'layananSets',
            'kerjasamaSets',
            'activity',
            'sellers',
            'tur',
            'transfer',
            'rental',
            'hotel',
            'xstay',
            'home_image_slider',
            'activity_populer',
            'tur_populer',
            'transfer_populer',
            'rental_populer',
            'hotel_populer',
            'xstay_populer',
            'top_destination',
            'search_from_to_tour',
            'collect_pin')
        );
    }

    public function getTopDest(){
        
        $settingGeneral = settingGeneral::select('regency_order')->find(99)->toArray();
        $settingGeneral = json_decode($settingGeneral['regency_order']);

        $settingGeneral = [];
        $settingGeneral = [];

        $top_destination = [];

        foreach ($settingGeneral as $data) {
            $name = Regency::where('id', $data)->first();
            $top_destination[] = [
                $name->name,
            ];
        }

        return view('Homepage.top-desti',compact('top_destination'));
    }

    public function getOfficialStore(){
        
        $settingGeneral = settingGeneral::select('regency_order')->find(99)->toArray();
        $settingGeneral = json_decode($settingGeneral['regency_order']);

        $settingGeneral = [];
        $settingGeneral = [];

        $top_destination = [];

        foreach ($settingGeneral as $data) {
            $name = Regency::where('id', $data)->first();
            $top_destination[] = [
                $name->name,
            ];
        }

        return view('Homepage.top-desti',compact('top_destination'));
    }

    public function getTransfer(Request $request)
    {
        $nama_produk = $request->produk_promo;
        $transfer = product::where('type', $nama_produk)->with('productdetail.harga')->limit(5)->latest()->get();

        // $kabupatens = Regency::where('province_id', $id_provinsi)->get();
        // dd($kabupatens);

        foreach ($transfer as $tf) {
            // echo "<option value='$kabupaten->id'>$kabupaten->name</option>";
            echo "<div class='flex justify-center'>
                    <x-destindonesia.card-produk :prod='$tf'>
                    </x-destindonesia.card-produk>
                </div>
            ";
        }
    }

    public function submitLike(Request $request)
    {
        // $like = new Likeproduct;

        $cekLike = Likeproduct::where('product_id', $request->product_id)->where('user_id', auth::id());

        // return response()->json($request->all());
        // dd($request->all());

        if (!auth::id()) {
            return response()->json([
                'status' => false,
                'message' => 'Anda harus login atau registrai terlebih dahulu'
            ], 200);
        }

        $data = [
            'product_id' => $request->product_id,
            'user_id' => auth::id(),
        ];

        $response = [];

        if ($cekLike->count() > 0) {

            $row = $cekLike->first();
            $response['success'] = true;

            if ($row->status === 1) {
                $data['status'] = 0;
                $response['message'] = 'Anda tidak menyukai produk ini';
            } else {
                $data['status'] = 1;
                $response['message'] = 'Anda menyukai produk ini';
            }

            Likeproduct::where('product_id', $request->product_id)->where('user_id', auth::id())->delete();

            $like = Likeproduct::create($data);

            $response['data'] = $like;
            return response()->json($response, 200);
        } else {

            $data['status'] = 1;

            $like = Likeproduct::create($data);

            return response()->json([
                'success' => true,
                'message' => 'Anda telah menyukai produk ini',
                'data' => $like
            ], 200);
        }
    }

    /** 
     * method untuk mengambil daftar produk
     * edit by eka
     */
    public function productListRegency($type, Request $request)
    {
        $kotaKabupaten = product::select(DB::raw('distinct lower(c.name) as name,product.type'))
            ->join('product_detail as b', 'b.product_id', '=', 'product.id')
            ->join('regencies as c', 'c.id', '=', 'b.tag_location_3')
            ->where('product.type', $type)
            ->get()->toArray();
        if(!$kotaKabupaten){
            $kotaKabupaten = product::select(DB::raw('distinct lower(c.name) as name,product.type'))
            ->join('product_detail as b', 'b.product_id', '=', 'product.id')
            ->join('regencies as c', 'c.id', '=', 'b.regency_id')
            ->where('product.type', $type)
            ->get()->toArray();
        }

        if($request->mode=='api'){
            return response()->json([
                'success'=>true,
                'data'=>$kotaKabupaten
            ]);
        }else{
            return $kotaKabupaten;
        }
    }
    

    public function productByNameRegency()
    {
    }

    public function getMasterTitik(){
        //get master point/pin location
        $titik_lokasi = Masterpoint::select("titik_route.id as id","regencies.id as regencies_id","titik_route.name_titik as name")
                            ->join('districts','districts.id','=','titik_route.district_id')
                            ->join('regencies','regencies.id','=','districts.regency_id')
                            ->get();
        
        $list = collect([]);

        foreach($titik_lokasi as $key => $value){
            echo "<option value='$value->regencies_id'>$value->name</option>";
            // $list->push([
            //     'id'=>$value->id,
            //     'name'=>$value->name,
            // ]);
        }

        // return response()->json([
        //     'success'=>200,
        //     'data'=>$list
        // ]);
    }

    public function getproductDetail($type, $mode=null){
        
        if($type=='hotel' || $type=='xstay'){
            $limit =  5;
        }else{
            $limit =  10;
        }

        $product = Product::where('type', $type)
        ->has('productdetail')
        ->with('tagged', 'productdetail.harga','productdetail.masterkamar', 'productdetail.detailkendaraan','productdetail.regency', 'user', 'productdetail.diskon')
        ->whereHas('productdetail.diskon')
        ->limit($limit)
        ->latest()
        ->get();
        // ->filter(function ($item) {
        //     $tgl_start = json_decode($item->productdetail->diskon['tgl_start']);
        //     $tgl_end = json_decode($item->productdetail->diskon['tgl_end']);

        //     foreach ($tgl_start as $key => $start) {
        //         if (Carbon::now()->between(Carbon::parse($start)->toDateTimeString(), Carbon::parse($tgl_end[$key])->toDateTimeString())) {
        //             return $item;
        //         }
        //     }
        // });
    

        $list_product = collect([]);

        foreach($product as $prod){

            // get tag lokasi
            $tag1 = DB::table("pulau")->where('id',$prod->productdetail->tag_location_1)->first();
            $tag2 = Province::where('id', $prod->productdetail->tag_location_2)->first();
            $tag3 = District::where('id',$prod->productdetail->tag_location_3)->first();
            $tag4 = detailWisata::where('id', $prod->productdetail->tag_location_4)->first();
            
            $name1 = null;
            $url1  ="#";

            $name2 = null;
            $url2  ="#";

            $name3 = null;
            $url3 ="#";

            $name4 = null;
            $url4  ="#";
            
            $tag_collect_1 = collect([]);
            $tag_collect_2 = collect([]);
            $tag_collect_3 = collect([]);
            $tag_collect_4 = collect([]);
            $regency_collect = collect([]);

            $regency = $prod->productdetail->regency;

            if($regency){
                $destPulau = detailObjek::where('provinsi_id',$regency->id)->first();
                $name3 = $regency->name;
                
                if($destPulau){
                   $url3 = route('kabupatenShow',$destPulau->slug);
                }

                $regency_collect->push([
                    'name'=>$name3,
                    'url'=>$url3
                ]);
            }

            if($tag1){
                // get pulau
                $destPulau = detailObjek::where('pulau_id',$tag1->id)->first();
                $name1 = $tag1->island_name;
                
                if($destPulau){
                   $url1 = route('pulauShow',$destPulau->slug);
                }

                $tag_collect_1->push([
                    'name'=>$name1,
                    'url'=>$url1
                ]);
            }

            if($tag2){
                // get provinsi
                $destPulau = detailObjek::where('provinsi_id',$tag2->id)->first();
                $name2 = $tag2->name;
                
                if($destPulau){
                   $url2 = route('provinsiShow',$destPulau->slug);
                }
                
                $tag_collect_2->push([
                    'name'=>$name2,
                    'url'=>$url2
                ]);
            }

            if($tag3){
                // get kabupaten
                $destPulau = detailObjek::where('provinsi_id',$tag3->id)->first();
                $name3 = $tag3->name;
                
                if($destPulau){
                   $url3 = route('kabupatenShow',$destPulau->slug);
                }

                $tag_collect_3->push([
                    'name'=>$name3,
                    'url'=>$url3
                ]);
            }

            if($tag4){
                // get wisata
                $name4 = $tag4->namaWisata;
                
                if($destPulau){
                   $url4 = route('wisata',$destPulau->tag4);
                }

                $tag_collect_4->push([
                    'name'=>$name4,
                    'url'=>$url4
                ]);
            }
            
            //harga
            if($type=='rental' || $type=='transfer'){
                $harga = number_format($prod->productdetail->harga->dewasa_residen ?? $prod->productdetail->detailkendaraan->harga_akomodasi);
            }elseif($type=='hotel' || $type=='xstay'){
                $harga = isset($prod->productdetail->masterkamar->harga_kamar) ? number_format($prod->productdetail->masterkamar->harga_kamar):number_format(0);
            }elseif($type=='activity'){
                $harga = $harga = isset($prod->productdetail->harga->dewasa_residen) ? number_format($prod->productdetail->harga->dewasa_residen) : number_format(0);
            }else{
                $price = [];
                
                if ($prod->productdetail->tipe_tur == 'Open Trip') {
                $price = Price::where('paket_id',$prod->productdetail->paket_id)->orderBy("dewasa_residen")->get();

                    if (!isset($price[0])) {
                        $harga = Price::where('id', $prod->productdetail->harga_id)->get();
                    }
                }

                if($prod->productdetail->tipe_tur == 'Tur Private'){
                    $harga = isset($prod->productdetail->harga->dewasa_residen) ? number_format($prod->productdetail->harga->dewasa_residen) : number_format(0);
                }

                if($prod->productdetail->tipe_tur == 'Open Trip'){
                    $harga = isset($harga[0]->dewasa_residen) ? number_format($harga[0]->dewasa_residen) :number_format(0);
                }
            }

            $rating = reviewPost::where('product_id', $prod->id)->sum('star_rating');
            $review_count = reviewPost::where('product_id', $prod->id)->get();
           
            $rating = (count($review_count) == 0 ? 0 : round($rating/count($review_count),2))." ".round(count($review_count),2)." review";

            $thumbnail=null;
            if(isset($prod->productdetail->thumbnail)){
            $thumbnail = $prod->productdetail->thumbnail;
            }elseif(isset(json_decode($prod->productdetail->gallery)[0]->gallery)){
            $thumbnail = json_decode($prod->productdetail->gallery)[0]->gallery;
            }else{
            $thumbnail = 'https://via.placeholder.com/450x450';
            }

            $info_toko = Kelolatoko::where('id_seller',$prod->user->id)->first();

            $list_product->push([
                'product'=>[
                    'id'=>$prod->id,
                    'user_id'=>$prod->user_id,
                    'product_code'=>$prod->product_code,
                    'product_name'=>$prod->product_name,
                    'slug'=>$prod->slug,
                    'type'=>$prod->type
                ],
                'tagged'=>$prod->tagged,
                'tag1'=>$tag_collect_1,
                'tag2'=>$tag_collect_2,
                'tag3'=>$tag_collect_3,
                'tag4'=>$tag_collect_4,
                'regency'=>$regency_collect,
                'harga'=>$harga,
                'rating'=>$rating,
                'thumbnail'=>$thumbnail,
                'id_seller'=>$info_toko->id_seller,
                'logo_toko'=>isset($info_toko->logo_toko) ? $info_toko->logo_toko:'https://via.placeholder.com/50x50',
            ]);
        }
        if($mode == 'non api'){
            return $list_product;
        }else{
            return response()->json([
                'data'=>$list_product
            ],200);
        }
    }

    public function getContent(){

        $konten = settingHomepages::where('section', '=', 'Konten_1')->get();

        $konten_list = collect([]);

        foreach($konten as $k){
            $konten_list->push([
                'img'=>\Storage::url($k->image_section),
                'judul'=>$k->title_section,
                'detail'=>$k->detail_section,
            ]);
        }

        return response()->json([
            'success'=>200,
            'data'=>$konten_list
        ],200);
    }
}
