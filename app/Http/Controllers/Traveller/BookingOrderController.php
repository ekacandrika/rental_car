<?php

namespace App\Http\Controllers\Traveller;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Kupon;
use App\Models\Order;
use App\Models\Product;
use App\Models\PajakAdmin;
use App\Models\PajakLocal;
use App\Models\BookingOrder;
use Illuminate\Http\Request;
use App\Models\Detailpesanan;
use App\Models\Productdetail;
use App\Models\Usertraveller;
use App\Mail\AdminNotifyMail;
use App\Mail\SellerNotifyMail;
use App\Mail\TravellerNotifyMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class BookingOrderController extends Controller
{
    public function booking_view(Request $request)
    {
        if (auth()->user() == null) {
            return redirect()->route('LoginTraveller');
        }
        
        $cari = $request->search;

        $traveller = Usertraveller::where('user_id', auth()->user()->id)->first();
        
        if($cari){
            $data = Detailpesanan::where('customer_id', $traveller->id)->where('status', '0')
            ->with('productdetail')
            ->whereHas('productdetail',function($q) use($cari){
                $q->join('product','product.id','=','product_detail.product_id')
                ->where('product.product_name','like','%'.$cari.'%');
            })->orderBy('confirm_order', 'asc')->get();
        }else{
            $data = Detailpesanan::where('customer_id', $traveller->id)->where('status', '0')->orderBy('confirm_order', 'asc')->get();
        }
        // dd($data[0]->productdetail);
        $getBookingId = BookingOrder::where('customer_id', $traveller->id)->first();

        return view('BE.traveller.booking.my-cart', compact('data', 'getBookingId', 'traveller','cari'));
    }

    public function viewDetailOrderProduct(Request $request, $id)
    {
        if (auth()->user() == null) {
            return redirect()->route('LoginTraveller');
        }

        $data = Detailpesanan::where('booking_code', $id)->where('status', '0')->first();
        $detail_produk = Productdetail::where('id', $data->product_detail_id)->first();
        $produk = Product::where('id', $detail_produk->product_id)->first();
        $user = User::where('id', $data->toko_id)->first();
        $user_id = Auth::user()->id;
        $profile = Usertraveller::where('user_id', $user_id)->first();

        return view('BE.traveller.detail-order', compact('data', 'detail_produk', 'produk', 'user', 'profile'));
    }

    public function viewDetailOrderProductLetter($order, $booking)
    {
        if (auth()->user() == null) {
            return redirect()->route('LoginTraveller');
        }
        $order = Order::select('external_id','booking_id')
                ->where('external_id',$order)
                ->with('bookings')
                ->first();
//            dd($order);


        $data = BookingOrder::where('booking_code', $booking)->where('status', '1')->first();
//        dd($booking);
        $detail_produk = Productdetail::where('id', $data->product_detail_id)->first();
        $produk = Product::where('id', $detail_produk->product_id)->first();
        $user = User::where('id', $data->toko_id)->first();
        $user_id = Auth::user()->id;
        $profile = Usertraveller::where('user_id', $user_id)->first();


        return view('BE.traveller.detail-order-letter', compact('data', 'detail_produk', 'produk', 'user', 'profile','order'));
    }

    public function cekKupon(Request $request)
    {
        $traveller = Usertraveller::where('user_id', auth()->user()->id)->first();
        $data = Detailpesanan::where('customer_id', $traveller->id)->where('status', '0')->get();
        $getBookingId = BookingOrder::where('customer_id', $traveller->id)->first();
        $data_listing = Product::with('productdetail')->where('type', 'Transfer')->first();
        $kupon  = Kupon::where('kodeKupon', $request->kode_kupon)->first();
        $json = json_decode($kupon->user_id, true);
        $user = User::where('id', auth()->user()->id)->first();

        if (in_array($user->id, $json)) {
            // Jika kupon expired
            if (Carbon::now()->format('Y-m-d') > $kupon->expiredDate) {
                return redirect()->back()->with('error', 'Kupon sudah expired!');

                // Jika kupon belum expired
            } else {
                $diskon = $kupon->diskonKupon;
                return view('BE.traveller.booking.my-cart', compact('data', 'getBookingId', 'data_listing', 'traveller', 'diskon'));
            }
        } else {
            return redirect()->back();
        }
    }

    public function booking_store_hotel(Request $request)
    {
        // dd(request()->cookie('detail'), $request->all());
        $data = $request->all();
        // dd($data);
        $phone_number_order = collect([]);
        $ID_card_photo_order = collect([]);

        foreach ($data['phone_number'] as $key => $value) {
            if ($data['nationality'][$key] == 'indonesia') {
                $phone_number_order->push('62' . $value);
            } else {
                $phone_number_order->push($value);
            }
        }

        if ($request->hasFile('kitas')) {
            $image = $request->file('kitas');

            foreach ($image as $key => $value) {

                $new_foto = time() . 'KitasPesertaOrder' . $value->getClientOriginalName();
                $tujuan_upload = 'Order/Peserta/Kitas/';

                $value->move($tujuan_upload, $new_foto);
                $images_gallery = $tujuan_upload . $new_foto;

                $ID_card_photo_order->push(
                    $images_gallery,
                );
            }
        }

        // dd($request);
        // Cek apakah data sudah ada di cart atau belum
        $traveller = Usertraveller::where('user_id', auth()->user()->id)->first();
        // $checker = Detailpesanan::where('product_detail_id', $request->product_detail_id)->where('customer_id', $traveller->id)->exists();
        $checker = false;
        $pajak_global = PajakAdmin::first();
        $pajak_global_result = intval($pajak_global->markup) / 100;
        $pajak_local = PajakLocal::where('produk_type', 'hotel')->first();

        $request->session()->forget('order');

        if ($checker) {
            // Jika data sudah ada (true)
            return redirect()->route('hotel.index')->withCookie(Cookie::forget('detail'));

            // Jika data belum ada (false)
        } else {
            $detail_pesanan = new Detailpesanan;
            $detail_pesanan->toko_id = $request->toko_id;
            $detail_pesanan->customer_id = isset($request->customer_id) ? $request->customer_id : null;
            $detail_pesanan->agent_id = isset($request->agent_id) ? $request->agent_id : null;
            $detail_pesanan->product_detail_id = $request->product_detail_id;
            $detail_pesanan->harga_asli = $request->total_price;
            $detail_pesanan->harga_ekstra = $request->harga_ekstra;
            $detail_pesanan->extra_name = $request->nama_ekstra;
            $detail_pesanan->jumlah_extra = $request->jumlah_ekstra;
            $detail_pesanan->type = $request->type;
            $detail_pesanan->refundable = $request->refundable;
            $detail_pesanan->refundable_amount = $request->refundable_amount;
            $detail_pesanan->refundable_price = $request->refundable_price;
            $detail_pesanan->confirm_order = $request->confirm_order;

            if ($request->role == 'agent') {
                $detail_pesanan->total_price = $request->total_price;
            } else {
                if ($pajak_local == null) {
                    $hasil = intval($request->total_price) * $pajak_global_result;
                    $detail_pesanan->total_price = $request->total_price + $hasil;
                } else {
                    if ($pajak_local->statusMarkupLocal == 'percent') {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $detail_pesanan->total_price = $request->total_price + $hasil;
                        } else {
                            $val = intval($pajak_local->markupLocal) / 100;
                            $ab = intval($request->total_price) * $val;
                            $detail_pesanan->total_price = $request->total_price + $ab;
                        }
                    } else {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $detail_pesanan->total_price = $request->total_price + $hasil;
                        } else {
                            $detail_pesanan->total_price = $request->total_price + $pajak_local->markupLocal;
                        }
                    }
                }
            }
            $detail_pesanan->booking_code = $request->booking_code;
            $detail_pesanan->activity_date = isset($request->activity_date) ? $request->activity_date : null;
            $detail_pesanan->catatan = isset($request->catatan) ? $request->catatan : null;
            $detail_pesanan->Ekstra_note = isset($request->Ekstra_note) ? $request->Ekstra_note : null;
            $detail_pesanan->kamar_id = $request->kamar_id;
            $detail_pesanan->check_in_date = $request->check_in_date;
            $detail_pesanan->check_out_date = $request->check_out_date;
            $detail_pesanan->peserta_dewasa = $request->peserta_dewasa;
            $detail_pesanan->peserta_anak_anak = $request->peserta_anak_anak;
            $detail_pesanan->peserta_balita = $request->peserta_balita;
            $detail_pesanan->ekstra_sarapan = isset($request->ekstra_sarapan) ? $request->ekstra_sarapan : null;
            $detail_pesanan->status = '0';

            $detail_pesanan->harga_product = $request->harga_product;
            $detail_pesanan->diskon_per_tanggal = $request->diskon_per_tanggal;
            $detail_pesanan->surcharge_per_tanggal = $request->surcharge_per_tanggal;
            $detail_pesanan->harga_per_tanggal = $request->harga_per_tanggal;
            $detail_pesanan->diskon_grup = $request->diskon_grup;
            $detail_pesanan->harga_akhir = $request->harga_akhir;
            $detail_pesanan->harga_pilihan = $request->harga_pilihan;
            $detail_pesanan->jumlah_kamar = $request->jumlah_kamar;
            $detail_pesanan->jumlah_malam = $request->jumlah_malam;

            $detail_pesanan->kategori_peserta = json_encode($request->kategori_peserta);
            $detail_pesanan->first_name = json_encode($data['first_name']);
            $detail_pesanan->last_name = json_encode($data['last_name']);
            $detail_pesanan->date_of_birth = json_encode($data['birth_date']);
            $detail_pesanan->jk = json_encode($data['jenis_kelamin']);
            $detail_pesanan->status_residen = json_encode($data['residen_status']);
            $detail_pesanan->citizenship = json_encode($data['nationality']);
            $detail_pesanan->phone_number_order = json_encode($phone_number_order);
            $detail_pesanan->email_order = json_encode($data['email']);
            $detail_pesanan->ID_card_photo_order = json_encode($ID_card_photo_order);

            $detail_pesanan->save();

            $booking = new BookingOrder;
            $booking->toko_id = $request->toko_id;
            $booking->customer_id = isset($request->customer_id) ? $request->customer_id : null;
            $booking->agent_id = isset($request->agent_id) ? $request->agent_id : null;
            $booking->product_detail_id = $request->product_detail_id;
            $booking->harga_asli = $request->total_price;
            $booking->harga_ekstra = $request->harga_ekstra;
            $booking->extra_name = $request->nama_ekstra;
            $booking->jumlah_extra = $request->jumlah_ekstra;
            $booking->type = $request->type;
            $booking->refundable = $request->refundable;
            $booking->refundable_amount = $request->refundable_amount;
            $booking->refundable_price = $request->refundable_price;

            if ($request->role == 'agent') {
                $booking->total_price = $request->total_price;
            } else {
                if ($pajak_local == null) {
                    $hasil = intval($request->total_price) * $pajak_global_result;
                    $booking->total_price = $request->total_price + $hasil;
                } else {
                    if ($pajak_local->statusMarkupLocal == 'percent') {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $booking->total_price = $request->total_price + $hasil;
                        } else {
                            $val = intval($pajak_local->markupLocal) / 100;
                            $ab = intval($request->total_price) * $val;
                            $booking->total_price = $request->total_price + $ab;
                        }
                    } else {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $booking->total_price = $request->total_price + $hasil;
                        } else {
                            $booking->total_price = $request->total_price + $pajak_local->markupLocal;
                        }
                    }
                }
            }
            $booking->booking_code = $request->booking_code;
            $booking->activity_date = isset($request->activity_date) ? $request->activity_date : null;
            $booking->catatan = isset($request->catatan) ? $request->catatan : null;
            $booking->Ekstra_note = isset($request->Ekstra_note) ? $request->Ekstra_note : null;
            $booking->kamar_id = $request->kamar_id;
            $booking->check_in_date = $request->check_in_date;
            $booking->check_out_date = $request->check_out_date;
            $booking->peserta_dewasa = $request->peserta_dewasa;
            $booking->peserta_anak_anak = $request->peserta_anak_anak;
            $booking->peserta_balita = $request->peserta_balita;
            $booking->ekstra_sarapan = isset($request->ekstra_sarapan) ? $request->ekstra_sarapan : null;
            $booking->status = '0';
            $booking->status_pembayaran = '-';

            $booking->harga_product = $request->harga_product;
            $booking->diskon_per_tanggal = $request->diskon_per_tanggal;
            $booking->surcharge_per_tanggal = $request->surcharge_per_tanggal;
            $booking->harga_per_tanggal = $request->harga_per_tanggal;
            $booking->diskon_grup = $request->diskon_grup;
            $booking->harga_akhir = $request->harga_akhir;
            $booking->harga_pilihan = $request->harga_pilihan;
            $booking->jumlah_kamar = $request->jumlah_kamar;
            $booking->jumlah_malam = $request->jumlah_malam;

            $booking->kategori_peserta = json_encode($request->kategori_peserta);
            $booking->first_name = json_encode($data['first_name']);
            $booking->last_name = json_encode($data['last_name']);
            $booking->date_of_birth = json_encode($data['birth_date']);
            $booking->jk = json_encode($data['jenis_kelamin']);
            $booking->status_residen = json_encode($data['residen_status']);
            $booking->citizenship = json_encode($data['nationality']);
            $booking->phone_number_order = json_encode($phone_number_order);
            $booking->email_order = json_encode($data['email']);
            $booking->ID_card_photo_order = json_encode($ID_card_photo_order);

            $booking->save();
        }

        $this->send_email_notif($request->session()->get('order.product_detail_id'));
        return redirect()->route('hotel.index')->withCookie(Cookie::forget('detail'));
    }

    public function booking_store_xstay(Request $request)
    {
        // dd(request()->cookie('detail'), $request->all());
        $data = $request->all();
        // dd($data);
        $phone_number_order = collect([]);
        $ID_card_photo_order = collect([]);

        foreach ($data['phone_number'] as $key => $value) {
            if ($data['nationality'][$key] == 'indonesia') {
                $phone_number_order->push('62' . $value);
            } else {
                $phone_number_order->push($value);
            }
        }

        if ($request->hasFile('kitas')) {
            $image = $request->file('kitas');

            foreach ($image as $key => $value) {

                $new_foto = time() . 'KitasPesertaOrder' . $value->getClientOriginalName();
                $tujuan_upload = 'Order/Peserta/Kitas/';

                $value->move($tujuan_upload, $new_foto);
                $images_gallery = $tujuan_upload . $new_foto;

                $ID_card_photo_order->push(
                    $images_gallery,
                );
            }
        }

        // dd($request);
        // Cek apakah data sudah ada di cart atau belum
        $traveller = Usertraveller::where('user_id', auth()->user()->id)->first();
        // $checker = Detailpesanan::where('product_detail_id', $request->product_detail_id)->where('customer_id', $traveller->id)->exists();
        $checker = false;
        $pajak_global = PajakAdmin::first();
        $pajak_global_result = intval($pajak_global->markup) / 100;
        $pajak_local = PajakLocal::where('produk_type', 'xstay')->first();

        $request->session()->forget('order');

        if ($checker) {
            // Jika data sudah ada (true)
            return redirect()->route('hotel.index')->withCookie(Cookie::forget('detail_xstay'));

            // Jika data belum ada (false)
        } else {
            $detail_pesanan = new Detailpesanan;
            $detail_pesanan->toko_id = $request->toko_id;
            $detail_pesanan->customer_id = isset($request->customer_id) ? $request->customer_id : null;
            $detail_pesanan->agent_id = isset($request->agent_id) ? $request->agent_id : null;
            $detail_pesanan->product_detail_id = $request->product_detail_id;
            $detail_pesanan->harga_asli = $request->total_price;
            $detail_pesanan->harga_ekstra = $request->harga_ekstra;
            $detail_pesanan->extra_name = $request->nama_ekstra;
            $detail_pesanan->jumlah_extra = $request->jumlah_ekstra;
            $detail_pesanan->type = $request->type;
            $detail_pesanan->refundable = $request->refundable;
            $detail_pesanan->refundable_amount = $request->refundable_amount;
            $detail_pesanan->refundable_price = $request->refundable_price;
            $detail_pesanan->confirm_order = $request->confirm_order;

            if ($request->role == 'agent') {
                $detail_pesanan->total_price = $request->total_price;
            } else {
                if ($pajak_local == null) {
                    $hasil = intval($request->total_price) * $pajak_global_result;
                    $detail_pesanan->total_price = $request->total_price + $hasil;
                } else {
                    if ($pajak_local->statusMarkupLocal == 'percent') {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $detail_pesanan->total_price = $request->total_price + $hasil;
                        } else {
                            $val = intval($pajak_local->markupLocal) / 100;
                            $ab = intval($request->total_price) * $val;
                            $detail_pesanan->total_price = $request->total_price + $ab;
                        }
                    } else {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $detail_pesanan->total_price = $request->total_price + $hasil;
                        } else {
                            $detail_pesanan->total_price = $request->total_price + $pajak_local->markupLocal;
                        }
                    }
                }
            }
            $detail_pesanan->booking_code = $request->booking_code;
            $detail_pesanan->activity_date = isset($request->activity_date) ? $request->activity_date : null;
            $detail_pesanan->catatan = isset($request->catatan) ? $request->catatan : null;
            $detail_pesanan->Ekstra_note = isset($request->Ekstra_note) ? $request->Ekstra_note : null;
            $detail_pesanan->kamar_id = $request->kamar_id;
            $detail_pesanan->check_in_date = $request->check_in_date;
            $detail_pesanan->check_out_date = $request->check_out_date;
            $detail_pesanan->peserta_dewasa = $request->peserta_dewasa;
            $detail_pesanan->peserta_anak_anak = $request->peserta_anak_anak;
            $detail_pesanan->peserta_balita = $request->peserta_balita;
            $detail_pesanan->ekstra_sarapan = isset($request->ekstra_sarapan) ? $request->ekstra_sarapan : null;
            $detail_pesanan->status = '0';

            $detail_pesanan->harga_product = $request->harga_product;
            $detail_pesanan->diskon_per_tanggal = $request->diskon_per_tanggal;
            $detail_pesanan->surcharge_per_tanggal = $request->surcharge_per_tanggal;
            $detail_pesanan->harga_per_tanggal = $request->harga_per_tanggal;
            $detail_pesanan->diskon_grup = $request->diskon_grup;
            $detail_pesanan->harga_akhir = $request->harga_akhir;
            $detail_pesanan->harga_pilihan = $request->harga_pilihan;
            $detail_pesanan->jumlah_kamar = $request->jumlah_kamar;
            $detail_pesanan->jumlah_malam = $request->jumlah_malam;

            $detail_pesanan->kategori_peserta = json_encode($request->kategori_peserta);
            $detail_pesanan->first_name = json_encode($data['first_name']);
            $detail_pesanan->last_name = json_encode($data['last_name']);
            $detail_pesanan->date_of_birth = json_encode($data['birth_date']);
            $detail_pesanan->jk = json_encode($data['jenis_kelamin']);
            $detail_pesanan->status_residen = json_encode($data['residen_status']);
            $detail_pesanan->citizenship = json_encode($data['nationality']);
            $detail_pesanan->phone_number_order = json_encode($phone_number_order);
            $detail_pesanan->email_order = json_encode($data['email']);
            $detail_pesanan->ID_card_photo_order = json_encode($ID_card_photo_order);

            $detail_pesanan->save();

            $booking = new BookingOrder;
            $booking->toko_id = $request->toko_id;
            $booking->customer_id = isset($request->customer_id) ? $request->customer_id : null;
            $booking->agent_id = isset($request->agent_id) ? $request->agent_id : null;
            $booking->product_detail_id = $request->product_detail_id;
            $booking->harga_asli = $request->total_price;
            $booking->harga_ekstra = $request->harga_ekstra;
            $booking->extra_name = $request->nama_ekstra;
            $booking->jumlah_extra = $request->jumlah_ekstra;
            $booking->type = $request->type;
            $booking->refundable = $request->refundable;
            $booking->refundable_amount = $request->refundable_amount;
            $booking->refundable_price = $request->refundable_price;

            if ($request->role == 'agent') {
                $booking->total_price = $request->total_price;
            } else {
                if ($pajak_local == null) {
                    $hasil = intval($request->total_price) * $pajak_global_result;
                    $booking->total_price = $request->total_price + $hasil;
                } else {
                    if ($pajak_local->statusMarkupLocal == 'percent') {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $booking->total_price = $request->total_price + $hasil;
                        } else {
                            $val = intval($pajak_local->markupLocal) / 100;
                            $ab = intval($request->total_price) * $val;
                            $booking->total_price = $request->total_price + $ab;
                        }
                    } else {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $booking->total_price = $request->total_price + $hasil;
                        } else {
                            $booking->total_price = $request->total_price + $pajak_local->markupLocal;
                        }
                    }
                }
            }
            $booking->booking_code = $request->booking_code;
            $booking->activity_date = isset($request->activity_date) ? $request->activity_date : null;
            $booking->catatan = isset($request->catatan) ? $request->catatan : null;
            $booking->Ekstra_note = isset($request->Ekstra_note) ? $request->Ekstra_note : null;
            $booking->kamar_id = $request->kamar_id;
            $booking->check_in_date = $request->check_in_date;
            $booking->check_out_date = $request->check_out_date;
            $booking->peserta_dewasa = $request->peserta_dewasa;
            $booking->peserta_anak_anak = $request->peserta_anak_anak;
            $booking->peserta_balita = $request->peserta_balita;
            $booking->ekstra_sarapan = isset($request->ekstra_sarapan) ? $request->ekstra_sarapan : null;
            $booking->status = '0';
            $booking->status_pembayaran = '-';

            $booking->harga_product = $request->harga_product;
            $booking->diskon_per_tanggal = $request->diskon_per_tanggal;
            $booking->surcharge_per_tanggal = $request->surcharge_per_tanggal;
            $booking->harga_per_tanggal = $request->harga_per_tanggal;
            $booking->diskon_grup = $request->diskon_grup;
            $booking->harga_akhir = $request->harga_akhir;
            $booking->harga_pilihan = $request->harga_pilihan;
            $booking->jumlah_kamar = $request->jumlah_kamar;
            $booking->jumlah_malam = $request->jumlah_malam;

            $booking->kategori_peserta = json_encode($request->kategori_peserta);
            $booking->first_name = json_encode($data['first_name']);
            $booking->last_name = json_encode($data['last_name']);
            $booking->date_of_birth = json_encode($data['birth_date']);
            $booking->jk = json_encode($data['jenis_kelamin']);
            $booking->status_residen = json_encode($data['residen_status']);
            $booking->citizenship = json_encode($data['nationality']);
            $booking->phone_number_order = json_encode($phone_number_order);
            $booking->email_order = json_encode($data['email']);
            $booking->ID_card_photo_order = json_encode($ID_card_photo_order);

            $booking->save();
        }

        $this->send_email_notif($request->session()->get('order.product_detail_id'));
        return redirect()->route('xstay.index')->withCookie(Cookie::forget('detail_xstay'));
    }

    public function booking_store_activity(Request $request)
    {
        $data = $request->all();
        // dd($data);
        // dd($data, $request->session()->get('order.activity_date'));
        $phone_number_order = collect([]);
        $ID_card_photo_order = collect([]);

        foreach ($data['phone_number'] as $key => $value) {
            if ($data['nationality'][$key] == 'indonesia') {
                $phone_number_order->push('62' . $value);
            } else {
                $phone_number_order->push($value);
            }
        }

        if ($request->hasFile('kitas')) {
            $image = $request->file('kitas');

            foreach ($image as $key => $value) {

                $new_foto = time() . 'KitasPesertaOrder' . $value->getClientOriginalName();
                $tujuan_upload = 'Order/Peserta/Kitas/';

                $value->move($tujuan_upload, $new_foto);
                $images_gallery = $tujuan_upload . $new_foto;

                $ID_card_photo_order->push(
                    $images_gallery,
                );
            }
        }

        $index_pilihan = collect([]);
        $jumlah_pilihan = collect([]);

        $extra_id = collect([]);
        $catatan_per_ekstra = collect([]);
        $jumlah_ekstra = collect([]);
        $order_pilihan = $request->session()->get('order.pilihan');
        $order_extra = $request->session()->get('order.extra');

        if (isset($order_pilihan) && !empty($order_pilihan)) {
            foreach ($request->session()->get('order.pilihan') as $key => $value) {
                if (isset($value['index'])) {
                    $index_pilihan->push($value['index']);
                    $jumlah_pilihan->push($value['jumlah']);
                }
            }
        }

        if (isset($order_extra) && !empty($order_extra)) {
            foreach ($request->session()->get('order.extra') as $key => $value) {
                $index = '';
                if (isset($value['id_extra'])) {
                    $index = $value['id_extra'];
                }

                $extra_id->push($index);
                $jumlah_ekstra->push($value['jumlah_extra']);
                $catatan_per_ekstra->push($value['note_extra']);
            }
        }

        // dd($request->session()->get('order'));
        // Cek apakah data sudah ada di cart atau belum
        $traveller = Usertraveller::where('user_id', auth()->user()->id)->first();
        // $checker = Detailpesanan::where('product_detail_id', $request->session()->get('order.product_detail_id'))->where('customer_id', $traveller->id)->exists();
        $checker = false;
        $pajak_global = PajakAdmin::first();
        $pajak_global_result = intval($pajak_global->markup) / 100;
        $pajak_local = PajakLocal::where('produk_type', 'activity')->first();

        if ($checker) {
            // Jika data sudah ada (true)
            $order = $request->session()->forget('order');
            return redirect()->route('activity.index')->withCookie(Cookie::forget('detail'));

            // Jika data belum ada (false)
        } else {
            $detail_pesanan = new Detailpesanan;

            $detail_pesanan->toko_id = $request->session()->get('order.toko_id');
            $detail_pesanan->customer_id = \Auth::user()->role == 'traveller' ? $traveller->id : null;
            $detail_pesanan->agent_id = \Auth::user()->role == 'agent' ? \Auth::user()->id : null;
            $detail_pesanan->product_detail_id = $request->session()->get('order.product_detail_id');

            $detail_pesanan->harga_asli = $request->total_price;
            $detail_pesanan->harga_ekstra = $request->harga_ekstra;
            $detail_pesanan->type = $request->session()->get('order.type');
            $detail_pesanan->confirm_order = $request->confirm_order;

            if (\Auth::user()->role == 'agent') {
                $detail_pesanan->total_price = $request->total_price;
            } else {
                if ($pajak_local == null) {
                    $hasil = intval($request->total_price) * $pajak_global_result;
                    $detail_pesanan->total_price = ceil($request->total_price + $hasil);
                } else {
                    if ($pajak_local->statusMarkupLocal == 'percent') {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $detail_pesanan->total_price = ceil($request->total_price + $hasil);
                        } else {
                            $val = intval($pajak_local->markupLocal) / 100;
                            $ab = intval($request->total_price) * $val;
                            $detail_pesanan->total_price = ceil($request->total_price + $ab);
                        }
                    } else {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $detail_pesanan->total_price = ceil($request->total_price + $hasil);
                        } else {
                            $detail_pesanan->total_price = ceil($request->total_price + $pajak_local->markupLocal);
                        }
                    }
                }
            }
            $detail_pesanan->booking_code = $request->session()->get('order.booking_code');

            $detail_pesanan->activity_date = $request->session()->get('order.activity_date') != null ? $request->session()->get('order.activity_date') : date("m/d/Y", strtotime('today'));;
            $detail_pesanan->catatan = isset($request->catatan) ? $request->catatan : null;
            $detail_pesanan->Ekstra_note = isset($request->Ekstra_note) ? $request->Ekstra_note : null;
            $detail_pesanan->check_in_date = $request->check_in_date;
            $detail_pesanan->check_out_date = $request->check_out_date;
            $detail_pesanan->peserta_dewasa = $request->peserta_dewasa;
            $detail_pesanan->peserta_anak_anak = $request->peserta_anak;
            $detail_pesanan->peserta_balita = $request->peserta_balita;
            $detail_pesanan->ekstra_sarapan = isset($request->ekstra_sarapan) ? $request->ekstra_sarapan : null;
            $detail_pesanan->status = '0';

            $detail_pesanan->paket_id = $request->session()->get('order.paket_id');
            $detail_pesanan->paket_index = $request->session()->get('order.paket.index');

            $detail_pesanan->pilihan_id = $request->session()->get('order.pilihan_id');
            $detail_pesanan->index_pilihan = count($index_pilihan) == 0 ? null : json_encode($index_pilihan);
            $detail_pesanan->jumlah_pilihan = count($jumlah_pilihan) == 0 ? null : json_encode($jumlah_pilihan);

            $detail_pesanan->extras_id = count($extra_id) == 0 ? null : json_encode($extra_id);
            $detail_pesanan->catatan_umum_extra = $request->session()->get('order.catatan_extra') == '' ? null : $request->session()->get('order.catatan_extra');
            $detail_pesanan->catatan_per_extra = count($catatan_per_ekstra) == 0 ? null : json_encode($catatan_per_ekstra);
            $detail_pesanan->jumlah_extra = count($jumlah_ekstra) == 0 ? null : json_encode($jumlah_ekstra);

            $detail_pesanan->harga_residen_dewasa = $request->harga_residen_dewasa;
            $detail_pesanan->harga_residen_anak = $request->harga_residen_anak;
            $detail_pesanan->harga_residen_balita = $request->harga_residen_balita;
            $detail_pesanan->harga_non_residen_dewasa = $request->harga_non_residen_dewasa;
            $detail_pesanan->harga_non_residen_anak = $request->harga_non_residen_anak;
            $detail_pesanan->harga_non_residen_balita = $request->harga_non_residen_balita;
            $detail_pesanan->harga_product = $request->harga_product;
            $detail_pesanan->diskon_per_tanggal = $request->diskon_per_tanggal;
            $detail_pesanan->surcharge_per_tanggal = $request->surcharge_per_tanggal;
            $detail_pesanan->harga_per_tanggal = $request->harga_per_tanggal;
            $detail_pesanan->diskon_grup = $request->diskon_grup;
            $detail_pesanan->harga_akhir = $request->harga_akhir;
            $detail_pesanan->harga_pilihan = $request->harga_pilihan;
            $detail_pesanan->nama_paket = $request->nama_paket;
            $detail_pesanan->waktu_paket = $request->waktu_paket;

            $detail_pesanan->kategori_peserta = json_encode($request->kategori_peserta);
            $detail_pesanan->first_name = json_encode($data['first_name']);
            $detail_pesanan->last_name = json_encode($data['last_name']);
            $detail_pesanan->date_of_birth = json_encode($data['birth_date']);
            $detail_pesanan->jk = json_encode($data['jenis_kelamin']);
            $detail_pesanan->status_residen = json_encode($data['residen_status']);
            $detail_pesanan->citizenship = json_encode($data['nationality']);
            $detail_pesanan->phone_number_order = json_encode($phone_number_order);
            $detail_pesanan->email_order = json_encode($data['email']);
            $detail_pesanan->ID_card_photo_order = json_encode($ID_card_photo_order);

            $detail_pesanan->save();

            $booking = new BookingOrder;

            $booking->toko_id = $request->session()->get('order.toko_id');
            $booking->customer_id = \Auth::user()->role == 'traveller' ? $traveller->id : null;
            $booking->agent_id = \Auth::user()->role == 'agent' ? \Auth::user()->id : null;
            $booking->product_detail_id = $request->session()->get('order.product_detail_id');

            $booking->harga_asli = $request->total_price;
            $booking->harga_ekstra = $request->harga_ekstra;
            $booking->type = $request->session()->get('order.type');

            if (\Auth::user()->role == 'agent') {
                $booking->total_price = $request->total_price;
            } else {
                if ($pajak_local == null) {
                    $hasil = intval($request->total_price) * $pajak_global_result;
                    $booking->total_price = ceil($request->total_price + $hasil);
                } else {
                    if ($pajak_local->statusMarkupLocal == 'percent') {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $booking->total_price = ceil($request->total_price + $hasil);
                        } else {
                            $val = intval($pajak_local->markupLocal) / 100;
                            $ab = intval($request->total_price) * $val;
                            $booking->total_price = ceil($request->total_price + $ab);
                        }
                    } else {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $booking->total_price = ceil($request->total_price + $hasil);
                        } else {
                            $booking->total_price = ceil($request->total_price + $pajak_local->markupLocal);
                        }
                    }
                }
            }

            $booking->booking_code = $request->session()->get('order.booking_code');

            $booking->activity_date = $request->session()->get('order.activity_date') != null ? $request->session()->get('order.activity_date') : date("m/d/Y", strtotime('today'));;
            $booking->catatan = isset($request->catatan) ? $request->catatan : null;
            $booking->Ekstra_note = isset($request->Ekstra_note) ? $request->Ekstra_note : null;
            $booking->check_in_date = $request->check_in_date;
            $booking->check_out_date = $request->check_out_date;
            $booking->peserta_dewasa = $request->peserta_dewasa;
            $booking->peserta_anak_anak = $request->peserta_anak;
            $booking->peserta_balita = $request->peserta_balita;
            $booking->ekstra_sarapan = isset($request->ekstra_sarapan) ? $request->ekstra_sarapan : null;
            $booking->status = '0';
            $booking->status_pembayaran = '-';

            $booking->paket_id = $request->session()->get('order.paket_id');
            $booking->paket_index = $request->session()->get('order.paket.index');

            $booking->pilihan_id = $request->session()->get('order.pilihan_id');
            $booking->index_pilihan = count($index_pilihan) == 0 ? null : json_encode($index_pilihan);
            $booking->jumlah_pilihan = count($jumlah_pilihan) == 0 ? null : json_encode($jumlah_pilihan);

            $booking->extras_id = count($extra_id) == 0 ? null : json_encode($extra_id);
            $booking->catatan_umum_extra = $request->session()->get('order.catatan_extra') == '' ? null : $request->session()->get('order.catatan_extra');
            $booking->catatan_per_extra = count($catatan_per_ekstra) == 0 ? null : json_encode($catatan_per_ekstra);
            $booking->jumlah_extra = count($jumlah_ekstra) == 0 ? null : json_encode($jumlah_ekstra);

            $booking->harga_residen_dewasa = $request->harga_residen_dewasa;
            $booking->harga_residen_anak = $request->harga_residen_anak;
            $booking->harga_residen_balita = $request->harga_residen_balita;
            $booking->harga_non_residen_dewasa = $request->harga_non_residen_dewasa;
            $booking->harga_non_residen_anak = $request->harga_non_residen_anak;
            $booking->harga_non_residen_balita = $request->harga_non_residen_balita;
            $booking->harga_product = $request->harga_product;
            $booking->diskon_per_tanggal = $request->diskon_per_tanggal;
            $booking->surcharge_per_tanggal = $request->surcharge_per_tanggal;
            $booking->harga_per_tanggal = $request->harga_per_tanggal;
            $booking->diskon_grup = $request->diskon_grup;
            $booking->harga_akhir = $request->harga_akhir;
            $booking->harga_pilihan = $request->harga_pilihan;
            $booking->nama_paket = $request->nama_paket;
            $booking->waktu_paket = $request->waktu_paket;

            $booking->kategori_peserta = json_encode($request->kategori_peserta);
            $booking->first_name = json_encode($data['first_name']);
            $booking->last_name = json_encode($data['last_name']);
            $booking->date_of_birth = json_encode($data['birth_date']);
            $booking->jk = json_encode($data['jenis_kelamin']);
            $booking->status_residen = json_encode($data['residen_status']);
            $booking->citizenship = json_encode($data['nationality']);
            $booking->phone_number_order = json_encode($phone_number_order);
            $booking->email_order = json_encode($data['email']);
            $booking->ID_card_photo_order = json_encode($ID_card_photo_order);

            $booking->save();
        }

        $this->send_email_notif($request->session()->get('order.product_detail_id'));
        $order = $request->session()->forget('order');

        return redirect()->route('activity.index')->withCookie(Cookie::forget('detail'));
    }

    public function booking_store_tour(Request $request)
    {
        if($request->session()->get('order') == null || Auth::user() == null){
            return redirect()->back();
        }

        $data = $request->all();
        // dd($data);
        $phone_number_order = collect([]);
        $ID_card_photo_order = collect([]);

        foreach ($data['phone_number'] as $key => $value) {
            if ($data['nationality'][$key] == 'indonesia') {
                $phone_number_order->push('62' . $value);
            } else {
                $phone_number_order->push($value);
            }
        }

        if ($request->hasFile('kitas')) {
            $image = $request->file('kitas');

            foreach ($image as $key => $value) {

                $new_foto = time() . 'KitasPesertaOrder' . $value->getClientOriginalName();
                $tujuan_upload = 'Order/Peserta/Kitas/';

                $value->move($tujuan_upload, $new_foto);
                $images_gallery = $tujuan_upload . $new_foto;

                $ID_card_photo_order->push(
                    $images_gallery,
                );
            }
        }

        $index_pilihan = collect([]);
        $jumlah_pilihan = collect([]);

        $extra_id = collect([]);
        $catatan_per_ekstra = collect([]);
        $jumlah_ekstra = collect([]);

        foreach ($request->session()->get('order.pilihan') as $key => $value) {
            if (isset($value['index'])) {
                $index_pilihan->push($value['index']);
                $jumlah_pilihan->push($value['jumlah']);
            }
        }

        foreach ($request->session()->get('order.extra') as $key => $value) {
            $index = '';
            if (isset($value['id_extra'])) {
                $index = $value['id_extra'];
            }

            $extra_id->push($index);
            $jumlah_ekstra->push($value['jumlah_extra']);
            $catatan_per_ekstra->push($value['note_extra']);
        }

        $index_pilihan = collect([]);
        $jumlah_pilihan = collect([]);

        $extra_id = collect([]);
        $catatan_per_ekstra = collect([]);
        $jumlah_ekstra = collect([]);

        foreach ($request->session()->get('order.pilihan') as $key => $value) {
            if (isset($value['index'])) {
                $index_pilihan->push($value['index']);
                $jumlah_pilihan->push($value['jumlah']);
            }
        }

        foreach ($request->session()->get('order.extra') as $key => $value) {
            $index = '';
            if (isset($value['id_extra'])) {
                $index = $value['id_extra'];
            }

            $extra_id->push($index);
            $jumlah_ekstra->push($value['jumlah_extra']);
            $catatan_per_ekstra->push($value['note_extra']);
        }

        // dd($request->session()->get('order'));
        // Cek apakah data sudah ada di cart atau belum
        $traveller = Usertraveller::where('user_id', auth()->user()->id)->first();
        $checker = Detailpesanan::where('product_detail_id', $request->session()->get('order.product_detail_id'))->where('customer_id', $traveller->id)->exists();
        $pajak_global = PajakAdmin::first();
        $pajak_global_result = intval($pajak_global->markup) / 100;
        $pajak_local = PajakLocal::where('produk_type', 'tour')->first();

        if ($checker) {
            // Jika data sudah ada (true)
            $order = $request->session()->forget('order');
            return redirect()->route('tur.index')->withCookie(Cookie::forget('detail'));

            // Jika data belum ada (false)
        } else {
            $detail_pesanan = new Detailpesanan;

            $detail_pesanan->toko_id = $request->session()->get('order.toko_id');
            $detail_pesanan->customer_id = \Auth::user()->role == 'traveller' ? $traveller->id : null;
            $detail_pesanan->agent_id = \Auth::user()->role == 'agent' ? \Auth::user()->id : null;
            $detail_pesanan->product_detail_id = $request->session()->get('order.product_detail_id');

            $detail_pesanan->harga_asli = $request->total_price;
            $detail_pesanan->harga_ekstra = $request->harga_ekstra;
            $detail_pesanan->type = $request->session()->get('order.type');

            if (\Auth::user()->role == 'agent') {
                $detail_pesanan->total_price = $request->total_price;
            } else {
                if ($pajak_local == null) {
                    $hasil = intval($request->total_price) * $pajak_global_result;
                    $detail_pesanan->total_price = ceil($request->total_price + $hasil);
                } else {
                    if ($pajak_local->statusMarkupLocal == 'percent') {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $detail_pesanan->total_price = ceil($request->total_price + $hasil);
                        } else {
                            $val = intval($pajak_local->markupLocal) / 100;
                            $ab = intval($request->total_price) * $val;
                            $detail_pesanan->total_price = ceil($request->total_price + $ab);
                        }
                    } else {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $detail_pesanan->total_price = ceil($request->total_price + $hasil);
                        } else {
                            $detail_pesanan->total_price = ceil($request->total_price + $pajak_local->markupLocal);
                        }
                    }
                }
            }
            $detail_pesanan->booking_code = $request->session()->get('order.booking_code');

            $detail_pesanan->activity_date = $request->session()->get('order.activity_date') != null ? $request->session()->get('order.activity_date') : null;
            $detail_pesanan->catatan = isset($request->catatan) ? $request->catatan : null;
            $detail_pesanan->Ekstra_note = isset($request->Ekstra_note) ? $request->Ekstra_note : null;
            $detail_pesanan->check_in_date = $request->check_in_date;
            $detail_pesanan->check_out_date = $request->check_out_date;
            $detail_pesanan->peserta_dewasa = $request->peserta_dewasa;
            $detail_pesanan->peserta_anak_anak = $request->peserta_anak_anak;
            $detail_pesanan->peserta_balita = $request->peserta_balita;
            $detail_pesanan->ekstra_sarapan = isset($request->ekstra_sarapan) ? $request->ekstra_sarapan : null;
            $detail_pesanan->status = '0';

            $detail_pesanan->paket_id = $request->session()->get('order.paket_id');
            $detail_pesanan->paket_index = $request->session()->get('order.paket.index');

            $detail_pesanan->pilihan_id = $request->session()->get('order.pilihan_id');
            $detail_pesanan->index_pilihan = count($index_pilihan) == 0 ? null : json_encode($index_pilihan);
            $detail_pesanan->jumlah_pilihan = count($jumlah_pilihan) == 0 ? null : json_encode($jumlah_pilihan);

            $detail_pesanan->extras_id = count($extra_id) == 0 ? null : json_encode($extra_id);
            $detail_pesanan->catatan_umum_extra = $request->session()->get('order.catatan_extra') == '' ? null : $request->session()->get('order.catatan_extra');
            $detail_pesanan->catatan_per_extra = count($catatan_per_ekstra) == 0 ? null : json_encode($catatan_per_ekstra);
            $detail_pesanan->jumlah_extra = count($jumlah_ekstra) == 0 ? null : json_encode($jumlah_ekstra);

            $detail_pesanan->kategori_peserta = json_encode($request->kategori_peserta);
            $detail_pesanan->first_name = json_encode($data['first_name']);
            $detail_pesanan->last_name = json_encode($data['last_name']);
            $detail_pesanan->date_of_birth = json_encode($data['birth_date']);
            $detail_pesanan->jk = json_encode($data['jenis_kelamin']);
            $detail_pesanan->status_residen = json_encode($data['residen_status']);
            $detail_pesanan->citizenship = json_encode($data['nationality']);
            $detail_pesanan->phone_number_order = json_encode($phone_number_order);
            $detail_pesanan->email_order = json_encode($data['email']);
            $detail_pesanan->ID_card_photo_order = json_encode($ID_card_photo_order);

            $detail_pesanan->save();

            $booking = new BookingOrder;

            $booking->toko_id = $request->session()->get('order.toko_id');
            $booking->customer_id = \Auth::user()->role == 'traveller' ? $traveller->id : null;
            $booking->agent_id = \Auth::user()->role == 'agent' ? \Auth::user()->id : null;
            $booking->product_detail_id = $request->session()->get('order.product_detail_id');

            $booking->harga_asli = $request->total_price;
            $booking->harga_ekstra = $request->harga_ekstra;
            $booking->type = $request->session()->get('order.type');

            if (\Auth::user()->role == 'agent') {
                $booking->total_price = $request->total_price;
            } else {
                if ($pajak_local == null) {
                    $hasil = intval($request->total_price) * $pajak_global_result;
                    $booking->total_price = ceil($request->total_price + $hasil);
                } else {
                    if ($pajak_local->statusMarkupLocal == 'percent') {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $booking->total_price = ceil($request->total_price + $hasil);
                        } else {
                            $val = intval($pajak_local->markupLocal) / 100;
                            $ab = intval($request->total_price) * $val;
                            $booking->total_price = ceil($request->total_price + $ab);
                        }
                    } else {
                        if ($pajak_local->markupLocal == null) {
                            $hasil = intval($request->total_price) * $pajak_global_result;
                            $booking->total_price = ceil($request->total_price + $hasil);
                        } else {
                            $booking->total_price = ceil($request->total_price + $pajak_local->markupLocal);
                        }
                    }
                }
            }

            $booking->booking_code = $request->session()->get('order.booking_code');

            $booking->activity_date = $request->session()->get('order.activity_date') != null ? $request->session()->get('order.activity_date') : null;
            $booking->catatan = isset($request->catatan) ? $request->catatan : null;
            $booking->Ekstra_note = isset($request->Ekstra_note) ? $request->Ekstra_note : null;
            $booking->check_in_date = $request->check_in_date;
            $booking->check_out_date = $request->check_out_date;
            $booking->peserta_dewasa = $request->peserta_dewasa;
            $booking->peserta_anak_anak = $request->peserta_anak_anak;
            $booking->peserta_balita = $request->peserta_balita;
            $booking->ekstra_sarapan = isset($request->ekstra_sarapan) ? $request->ekstra_sarapan : null;
            $booking->status = '0';
            $booking->status_pembayaran = '-';

            $booking->paket_id = $request->session()->get('order.paket_id');
            $booking->paket_index = $request->session()->get('order.paket.index');

            $booking->pilihan_id = $request->session()->get('order.pilihan_id');
            $booking->index_pilihan = count($index_pilihan) == 0 ? null : json_encode($index_pilihan);
            $booking->jumlah_pilihan = count($jumlah_pilihan) == 0 ? null : json_encode($jumlah_pilihan);

            $booking->extras_id = count($extra_id) == 0 ? null : json_encode($extra_id);
            $booking->catatan_umum_extra = $request->session()->get('order.catatan_extra') == '' ? null : $request->session()->get('order.catatan_extra');
            $booking->catatan_per_extra = count($catatan_per_ekstra) == 0 ? null : json_encode($catatan_per_ekstra);
            $booking->jumlah_extra = count($jumlah_ekstra) == 0 ? null : json_encode($jumlah_ekstra);

            $booking->kategori_peserta = json_encode($request->kategori_peserta);
            $booking->first_name = json_encode($data['first_name']);
            $booking->last_name = json_encode($data['last_name']);
            $booking->date_of_birth = json_encode($data['birth_date']);
            $booking->jk = json_encode($data['jenis_kelamin']);
            $booking->status_residen = json_encode($data['residen_status']);
            $booking->citizenship = json_encode($data['nationality']);
            $booking->phone_number_order = json_encode($phone_number_order);
            $booking->email_order = json_encode($data['email']);
            $booking->ID_card_photo_order = json_encode($ID_card_photo_order);

            $booking->save();
        }

        $this->send_email_notif($request->session()->get('order.product_detail_id'));
        $order = $request->session()->forget('order');

        return redirect()->route('tur.index')->withCookie(Cookie::forget('detail'));
    }

    public function booking_store_transfer(Request $request)
    {
        // Cek apakah data sudah ada di cart atau belum
        // dd($request->all());
        $traveller = Usertraveller::where('user_id', auth()->user()->id)->first();
        $checker = Detailpesanan::where('product_detail_id', $request->product_detail_id)->where('customer_id', $traveller->id)->exists();
        $pajak_global = PajakAdmin::first();
        $pajak_global_result = intval($pajak_global->markup) / 100;
        $pajak_local = PajakLocal::where('produk_type', 'Transfer')->first();

        $request->session()->forget('order');

        // if ($checker) {
        //     // Jika data sudah ada (true)
        //     return redirect()->route('hotel.index')->withCookie(Cookie::forget('detail'));

        //     // Jika data belum ada (false)
        // } else {
        $detail_pesanan = new Detailpesanan;
        $detail_pesanan->toko_id = $request->toko_id;
        $detail_pesanan->customer_id = isset($request->customer_id) ? $request->customer_id : null;
        $detail_pesanan->agent_id = isset($request->agent_id) ? $request->agent_id : null;
        $detail_pesanan->product_detail_id = $request->product_detail_id;
        $detail_pesanan->harga_asli = $request->total_price;
        $detail_pesanan->flight_info = isset($request->flight_info) ? $request->flight_info : null;
        $detail_pesanan->airport_pickup = isset($request->airport_pickup) ? $request->airport_pickup : null;
        $detail_pesanan->pickup_address = isset($request->pickup_address) ? $request->pickup_address : null;
        $detail_pesanan->dropoff_traveller = isset($request->dropoff_traveller) ? $request->dropoff_traveller : null;
        $detail_pesanan->pickup_time = isset($request->time_pickup) ? $request->time_pickup : null;
        $detail_pesanan->type = $request->type;
        if ($request->role == 'agent') {
            $detail_pesanan->total_price = $request->total_price;
        } else {
            if ($pajak_local == null) {
                $hasil = intval($request->total_price) * $pajak_global_result;
                $detail_pesanan->total_price = $request->total_price + $hasil;
            } else {
                if ($pajak_local->statusMarkupLocal == 'percent') {
                    if ($pajak_local->markupLocal == null) {
                        $hasil = intval($request->total_price) * $pajak_global_result;
                        $detail_pesanan->total_price = $request->total_price + $hasil;
                    } else {
                        $val = intval($pajak_local->markupLocal) / 100;
                        $ab = intval($request->total_price) * $val;
                        $detail_pesanan->total_price = $request->total_price + $ab;
                    }
                } else {
                    if ($pajak_local->markupLocal == null) {
                        $hasil = intval($request->total_price) * $pajak_global_result;
                        $detail_pesanan->total_price = $request->total_price + $hasil;
                    } else {
                        $detail_pesanan->total_price = $request->total_price + $pajak_local->markupLocal;
                    }
                }
            }
        }
        $detail_pesanan->booking_code = $request->booking_code;
        $detail_pesanan->activity_date = isset($request->activity_date) ? $request->activity_date : null;
        $detail_pesanan->catatan = isset($request->catatan) ? $request->catatan : null;
        $detail_pesanan->Ekstra_note = isset($request->Ekstra_note) ? $request->Ekstra_note : null;
        $detail_pesanan->check_in_date = $request->start_date;
        $detail_pesanan->check_out_date = isset($request->check_out_date) ? $request->check_out_date : null;
        $detail_pesanan->peserta_dewasa = isset($request->peserta_dewasa) ? $request->peserta_dewasa : null;
        $detail_pesanan->peserta_anak_anak = isset($request->peserta_anak_anak) ? $request->peserta_anak_anak : null;
        $detail_pesanan->peserta_balita = isset($request->peserta_balita) ? $request->peserta_balita : null;
        $detail_pesanan->ekstra_sarapan = isset($request->ekstra_sarapan) ? $request->ekstra_sarapan : null;
        $detail_pesanan->status = '0';
        $detail_pesanan->save();

        $booking = new BookingOrder;
        $booking->toko_id = $request->toko_id;
        $booking->customer_id = isset($request->customer_id) ? $request->customer_id : null;
        $booking->agent_id = isset($request->agent_id) ? $request->agent_id : null;
        $booking->product_detail_id = $request->product_detail_id;
        $booking->harga_asli = $request->total_price;
        $booking->harga_ekstra = $request->harga_ekstra;
        $booking->flight_info = isset($request->flight_info) ? $request->flight_info : null;
        $booking->airport_pickup = isset($request->airport_pickup) ? $request->airport_pickup : null;
        $booking->pickup_address = isset($request->pickup_address) ? $request->pickup_address : null;
        $booking->dropoff_traveller = isset($request->dropoff_traveller) ? $request->dropoff_traveller : null;
        $booking->pickup_time = isset($request->time_pickup) ? $request->time_pickup : null;
        $booking->type = $request->type;
        if ($request->role == 'agent') {
            $booking->total_price = $request->total_price;
        } else {
            if ($pajak_local == null) {
                $hasil = intval($request->total_price) * $pajak_global_result;
                $booking->total_price = $request->total_price + $hasil;
            } else {
                if ($pajak_local->statusMarkupLocal == 'percent') {
                    if ($pajak_local->markupLocal == null) {
                        $hasil = intval($request->total_price) * $pajak_global_result;
                        $booking->total_price = $request->total_price + $hasil;
                    } else {
                        $val = intval($pajak_local->markupLocal) / 100;
                        $ab = intval($request->total_price) * $val;
                        $booking->total_price = $request->total_price + $ab;
                    }
                } else {
                    if ($pajak_local->markupLocal == null) {
                        $hasil = intval($request->total_price) * $pajak_global_result;
                        $booking->total_price = $request->total_price + $hasil;
                    } else {
                        $booking->total_price = $request->total_price + $pajak_local->markupLocal;
                    }
                }
            }
        }
        $booking->booking_code = $request->booking_code;
        $booking->activity_date = isset($request->activity_date) ? $request->activity_date : null;
        $booking->catatan = isset($request->catatan) ? $request->catatan : null;
        $booking->Ekstra_note = isset($request->Ekstra_note) ? $request->Ekstra_note : null;
        $booking->check_in_date = $request->start_date;
        $booking->check_out_date = isset($request->check_out_date) ? $request->check_out_date : null;
        $booking->peserta_dewasa = isset($request->peserta_dewasa) ? $request->peserta_dewasa : null;
        $booking->peserta_anak_anak = isset($request->peserta_anak_anak) ? $request->peserta_anak_anak : null;
        $booking->peserta_balita = isset($request->peserta_balita) ? $request->peserta_balita : null;
        $booking->ekstra_sarapan = isset($request->ekstra_sarapan) ? $request->ekstra_sarapan : null;
        $booking->status = '0';
        $booking->status_pembayaran = '-';
        $booking->save();
        // }

        $this->send_email_notif($request->session()->get('order.product_detail_id'));
        return redirect()->route('transfer.index')->withCookie(Cookie::forget('detail'));
    }

    public function booking_store_rental(Request $request)
    {
        // Cek apakah data sudah ada di cart atau belum
        $traveller = Usertraveller::where('user_id', auth()->user()->id)->first();
        $checker = Detailpesanan::where('product_detail_id', $request->product_detail_id)->where('customer_id', $traveller->id)->exists();
        $pajak_global = PajakAdmin::first();
        $pajak_global_result = intval($pajak_global->markup) / 100;
        $pajak_local = PajakLocal::where('produk_type', 'Rental')->first();

        $request->session()->forget('order');

        // if ($checker) {
        //     // Jika data sudah ada (true)
        //     return redirect()->route('rental.index')->withCookie(Cookie::forget('detail'));

        //     // Jika data belum ada (false)
        // } else {
        $detail_pesanan = new Detailpesanan;
        $detail_pesanan->toko_id = $request->toko_id;
        $detail_pesanan->customer_id = isset($request->customer_id) ? $request->customer_id : null;
        $detail_pesanan->agent_id = isset($request->agent_id) ? $request->agent_id : null;
        $detail_pesanan->product_detail_id = $request->product_detail_id;
        $detail_pesanan->harga_asli = $request->total_price;
        $detail_pesanan->harga_ekstra = $request->harga_ekstra;
        $detail_pesanan->type = $request->type;
        if ($request->role == 'agent') {
            $detail_pesanan->total_price = $request->total_price;
        } else {
            if ($pajak_local == null) {
                $hasil = intval($request->total_price) * $pajak_global_result;
                $detail_pesanan->total_price = $request->total_price + $hasil;
            } else {
                if ($pajak_local->statusMarkupLocal == 'percent') {
                    if ($pajak_local->markupLocal == null) {
                        $hasil = intval($request->total_price) * $pajak_global_result;
                        $detail_pesanan->total_price = $request->total_price + $hasil;
                    } else {
                        $val = intval($pajak_local->markupLocal) / 100;
                        $ab = intval($request->total_price) * $val;
                        $detail_pesanan->total_price = $request->total_price + $ab;
                    }
                } else {
                    if ($pajak_local->markupLocal == null) {
                        $hasil = intval($request->total_price) * $pajak_global_result;
                        $detail_pesanan->total_price = $request->total_price + $hasil;
                    } else {
                        $detail_pesanan->total_price = $request->total_price + $pajak_local->markupLocal;
                    }
                }
            }
        }
        $detail_pesanan->booking_code = $request->booking_code;
        $detail_pesanan->activity_date = isset($request->activity_date) ? $request->activity_date : null;
        $detail_pesanan->catatan = isset($request->catatan) ? $request->catatan : null;
        $detail_pesanan->Ekstra_note = isset($request->Ekstra_note) ? $request->Ekstra_note : null;
        $detail_pesanan->check_in_date = isset($request->check_in_date) ? $request->check_in_date : null;
        $detail_pesanan->check_out_date = isset($request->check_out_date) ? $request->check_out_date : null;
        $detail_pesanan->peserta_dewasa = isset($request->peserta_dewasa) ? $request->peserta_dewasa : null;
        $detail_pesanan->peserta_anak_anak = isset($request->peserta_anak_anak) ? $request->peserta_anak_anak : null;
        $detail_pesanan->peserta_balita = isset($request->peserta_balita) ? $request->peserta_balita : null;
        $detail_pesanan->ekstra_sarapan = isset($request->ekstra_sarapan) ? $request->ekstra_sarapan : null;
        $detail_pesanan->status = '0';
        $detail_pesanan->save();

        $booking = new BookingOrder;
        $booking->toko_id = $request->toko_id;
        $booking->customer_id = isset($request->customer_id) ? $request->customer_id : null;
        $booking->agent_id = isset($request->agent_id) ? $request->agent_id : null;
        $booking->product_detail_id = $request->product_detail_id;
        $booking->harga_asli = $request->total_price;
        $booking->harga_ekstra = $request->harga_ekstra;
        $booking->type = $request->type;
        if ($request->role == 'agent') {
            $booking->total_price = $request->total_price;
        } else {
            if ($pajak_local == null) {
                $hasil = intval($request->total_price) * $pajak_global_result;
                $booking->total_price = $request->total_price + $hasil;
            } else {
                if ($pajak_local->statusMarkupLocal == 'percent') {
                    if ($pajak_local->markupLocal == null) {
                        $hasil = intval($request->total_price) * $pajak_global_result;
                        $booking->total_price = $request->total_price + $hasil;
                    } else {
                        $val = intval($pajak_local->markupLocal) / 100;
                        $ab = intval($request->total_price) * $val;
                        $booking->total_price = $request->total_price + $ab;
                    }
                } else {
                    if ($pajak_local->markupLocal == null) {
                        $hasil = intval($request->total_price) * $pajak_global_result;
                        $booking->total_price = $request->total_price + $hasil;
                    } else {
                        $booking->total_price = $request->total_price + $pajak_local->markupLocal;
                    }
                }
            }
        }
        $booking->booking_code = $request->booking_code;
        $booking->activity_date = isset($request->activity_date) ? $request->activity_date : null;
        $booking->catatan = isset($request->catatan) ? $request->catatan : null;
        $booking->Ekstra_note = isset($request->Ekstra_note) ? $request->Ekstra_note : null;
        $booking->check_in_date = isset($request->check_in_date) ? $request->check_in_date : null;
        $booking->check_out_date = isset($request->check_out_date) ? $request->check_out_date : null;
        $booking->peserta_dewasa = isset($request->peserta_dewasa) ? $request->peserta_dewasa : null;
        $booking->peserta_anak_anak = isset($request->peserta_anak_anak) ? $request->peserta_anak_anak : null;
        $booking->peserta_balita = isset($request->peserta_balita) ? $request->peserta_balita : null;
        $booking->ekstra_sarapan = isset($request->ekstra_sarapan) ? $request->ekstra_sarapan : null;
        $booking->status = '0';
        $booking->status_pembayaran = '-';
        $booking->save();
        // }booking_store_transfer

        $this->send_email_notif($request->session()->get('order.product_detail_id'));
        return redirect()->route('rental.index')->withCookie(Cookie::forget('detail'));
    }

    public function destroy(Request $request, $id)
    {
        Detailpesanan::where('id', $id)->findOrFail($id)->delete();
        BookingOrder::where('booking_code', $request->kode_booking)->delete();

        return redirect()->route('cart');
    }

    public function update_status_confirm(Request $request, $id)
    {
        $data_detailPesanan = Detailpesanan::where('id', $id)->first();
        $data_detailPesanan->update([
            'confirm_order' => 'waiting',
        ]);

        return redirect()->route('cart');
    }

    public function send_email_notif($product_detail_id){
        // send to admin
        $admin_email = 'kamtuuofficial@gmail.com';
        Mail::mailer('smtp2')->to($admin_email)->send(new AdminNotifyMail('There is one incoming transaction from a traveler user'));

        //send to seller
        $detail = Productdetail::where('id',$product_detail_id)->with('product')->first();
        $user_seller = User::where('id',$detail->product->user_id)->first();
        $seller_email = $user_seller->email;
        Mail::mailer('smtp2')->to($seller_email)->send(new SellerNotifyMail('ekacandrika','There is one incoming transaction from a traveler user, please check your inbox','Product Transaction Notification'));

        //send to traveller
        $traveller_email = auth()->user()->email;
        Mail::mailer('smtp2')->to($traveller_email)->send(new TravellerNotifyMail('ekacandrika','Successful transaction, please check your inbox','Product Transaction Notification'));
        
    }

}
