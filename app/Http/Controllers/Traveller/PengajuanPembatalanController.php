<?php

namespace App\Http\Controllers\Traveller;

use App\Http\Controllers\Controller;
use App\Models\BookingOrder;
use App\Models\PengajuanPembatalan;
use Illuminate\Http\Request;

class PengajuanPembatalanController extends Controller
{
    public function viewListPengajuan()
    {
        $data = PengajuanPembatalan::where('user_id', auth()->user()->id)->get();

        return view('BE.traveller.list-pengajuan_pembatalan', compact('data'));
    }

    public function storePengajuanPembatalan(Request $request)
    {
        $pengajuanCode = mt_rand(1, 99) . date('dmY');

        $data = new PengajuanPembatalan;
        $data->kode_pengajuan = $pengajuanCode;
        $data->user_id = $request->user_id;
        $data->data_booking_id = $request->data_booking_id;
        $data->product_id = $request->product_id;
        $data->toko_id = $request->toko_id;
        $data->harga = $request->harga;
        $data->tgl_pengajuan = date('Y-m-d');
        $data->tgl_checkout = $request->tgl_checkout;
        $data->status_pembayaran = $request->status_pembayaran;
        $data->status_pengajuan = 'Proses';
        $data->save();

        // update status from table booking to "2"
        $update_booking = BookingOrder::where('id', $request->data_booking_id)->first();
        // dd($request->all());
        // dd($update_booking);
        $update_booking->update([
            'status' => '2'
        ]);

        return redirect()->route('viewListPengajuan');
    }
}
