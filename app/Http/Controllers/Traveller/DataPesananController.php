<?php

namespace App\Http\Controllers\Traveller;

use App\Http\Controllers\Controller;
use App\Models\BookingOrder;
use App\Models\Order;
use App\Models\Usertraveller;
use Illuminate\Http\Request;

class DataPesananController extends Controller
{
    public function viewDataPesanan()
    {
        $traveller = Usertraveller::where('user_id', auth()->user()->id)->first();
        $data = Order::with(['bookings' => function ($query1){
                $query1->with('productdetail');
                $query1->with(['users' => function ($query2){
                    $query2->where('user_id', auth()->user()->id);
                }]);
                }])
            ->get();
//        dd($data);
//        $order = Order::where('customer_id', $traveller->id)->where('status', '1')->get();

        return view('BE.traveller.daftar-pesanan', compact('data',));
    }
}
