<?php

namespace App\Http\Controllers\Traveller;

use App\Http\Controllers\Controller;
use App\Models\Userdetail;
use App\Models\Usertraveller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class InfoBookingController extends Controller
{
    //
    public function index()
    {
        $user_id = Auth::user()->id;
        $profile = Usertraveller::where('user_id', $user_id)->first();
        return view('BE.traveller.info-booking', compact('profile'));
    }

    public function updateDataBooking(Request $request)
    {
        $user_id = Auth::user()->id;
        $usertraveller = Usertraveller::where('user_id', $user_id)->first();

        $validator = Validator::make($request->all(), [
            'first_name_booking' => 'required',
            'email_booking' => 'required|email',
            'phone_number_booking' => 'required|min:5|max:13',
        ],[
            'first_name_booking.required' => 'Nama depan tidak boleh kosong!',
            'email_booking.required' => 'Email tidak boleh kosong!',
            'email_booking.email' => 'Pastikan email yang Anda masukkan sudah benar',
            'phone_number_booking.required' => 'Email tidak boleh kosong!',
            'phone_number_booking.min' => 'Nomor telepon minimal :min karakter',
            'phone_number_booking.max' => 'Nomor telepon maksimal :max karakter',
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator, 'data_booking')>withInput();
        }

        $usertraveller->update([
            'first_name_booking' => $request->first_name_booking,
            'last_name_booking' => $request->last_name_booking,
            'email_booking' => $request->email_booking,
            'phone_number_booking' => $request->phone_number_booking,
            'citizenship' => $request->citizenship,
            'status_residen' => $request->status_residen,
            'akun_booking_status' => isset($request->akun_booking_status),
        ]);

        return redirect()->route('infobooking')->with(['success_data_booking'=>'Data berhasil diperbarui']);
    }

    public function updateKitasBooking(Request $request)
    {
        $user_id = Auth::user()->id;
        $usertraveller = Usertraveller::where('user_id', $user_id)->first();
        $userdetail= Userdetail::where('user_id', $user_id)->first();
 
        $validator = Validator::make($request->all(), [
            'nik' => 'required_with:ID_card_photo|min:16|max:16',
            'passport_number' => 'required_with:passport_photo',
            'passport_photo' => 'image|mimes:jpeg,png,jpg|max:2048',
            'ID_card_photo' => 'image|mimes:jpeg,png,jpg|max:2048',
            'passport_active_period' => 'required_with:passport_photo'
        ],[
            'nik.required_with' => 'NIK wajib diisi!',
            'nik.min' => 'NIK harus berisikan :min karakter',
            'nik.max' => 'NIK harus berisikan :max karakter',
            'passport_number.required_with' => 'Nomor Passport wajib diisi!',
            'passport_photo.mimes' => 'Foto harus memiliki ekstensi (jpg, jpeg, atau png)',
            'passport_photo.max' => 'Foto maksimum berukuran 2MB',
            'passport_active_period.required_with' => 'Tanggal berlaku passport wajib diisi!',
            'ID_card_photo.mimes' => 'Foto harus memiliki ekstensi (jpg, jpeg, atau png)',
            'ID_card_photo.max' => 'Foto maksimum berukuran 2MB',
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator, 'kitas_booking');
        }
        
        $passport_photo = '';
        $ID_card_photo = '';
        
        if ($request->hasFile('passport_photo')) {
            $image = $request->file('passport_photo');
            $new_foto = time() . 'FotoPassport.' . $image->getClientOriginalExtension();
            $tujuan_upload = 'public/user/fotopassport/';
            $image->move('storage/user/fotopassport/', $new_foto);
            $passport_photo = $tujuan_upload . $new_foto;

            //delete old image
            if (Storage::exists($usertraveller->passport_photo)) {
                Storage::delete($usertraveller->passport_photo);
            }
        }

        if ($request->hasFile('ID_card_photo')) {
            $image = $request->file('ID_card_photo');
            // $new_foto = time() . 'FotoKTP' . $image->getClientOriginalName();
            // $tujuan_upload = 'User/FotoKTP/';
            // $image->move($tujuan_upload, $new_foto);
            // $ID_card_photo = $tujuan_upload . $new_foto;
            $new_foto = time() . 'FotoKTP.' . $image->getClientOriginalExtension();
            $tujuan_upload = 'public/user/fotoktp/';
            $image->move('storage/user/fotoktp/', $new_foto);
            $ID_card_photo = $tujuan_upload . $new_foto;

            //delete old image
            // if (file_exists(($usertraveller->ID_card_photo))) {
            //     unlink(($usertraveller->ID_card_photo));
            // }

            if (Storage::exists($usertraveller->ID_card_photo)) {
                Storage::delete($usertraveller->ID_card_photo);
            }
        }

        $usertraveller->update([
            'nik' => $request->nik,
            'passport_number' => $request->passport_number,
            'passport_active_period' => $request->passport_active_period,
            'passport_photo' => $passport_photo == null? $usertraveller->passport_photo : $passport_photo,
            'ID_card_photo' => $ID_card_photo == null? $usertraveller->ID_card_photo : $ID_card_photo
        ]);

        $userdetail->update([
            'identity_card_number' => $request->nik,
            'ID_card_type' => 'KITAS',
            'ID_card_photo' => $ID_card_photo == null? $usertraveller->ID_card_photo : $ID_card_photo
        ]);

        return redirect()->route('infobooking')->with(['success_kitas_booking'=>'Kitas berhasil diperbarui']);
    }
}
