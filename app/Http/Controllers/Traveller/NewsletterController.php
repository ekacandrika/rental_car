<?php

namespace App\Http\Controllers\Traveller;

use App\Http\Controllers\Controller;
use App\Models\Usertraveller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    //
    public function index()
    {
        $user_id = Auth::user()->id;
        $profile = Usertraveller::where('user_id', $user_id)->first();

        return view('BE.traveller.newsletter', compact('profile'));
    }

    /**
     * Update Newsletter
     */
    public function updateNewsletter(Request $request)
    {
        $user_id = Auth::user()->id;
        $traveller = Usertraveller::where('user_id', $user_id)->first();

        $traveller->update([
            'newslatter_status' => $request->newslatter_status,
        ]);

    }
}
