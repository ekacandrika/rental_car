<?php

namespace App\Http\Controllers\Traveller;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Userdetail;
use App\Models\Usertraveller;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\LikeProduct;
use App\Models\Productdetail;
use App\Models\BookingOrder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Validator;

class ProfileController extends Controller
{
    //

    public function index()
    {
        $user_id = Auth::user()->id;
        $profile = Usertraveller::where('user_id', $user_id)->first();
        $profile_email = [Auth::user()->email];
        $profile_phone_number = [Auth::user()->no_tlp];
        $data = '';
        $provinsi = Province::all();

        if ($profile == null) {
            return view('BE.traveller.profil', compact('profile', 'provinsi', 'data', 'profile_email', 'profile_phone_number'));
        }

        if ($profile->second_email != null) {
            $profile_email = [
                Auth::user()->email, $profile->second_email
            ];
        }

        if ($profile->second_phone_number != null) {
            $profile_phone_number = [
                Auth::user()->no_tlp, $profile->second_phone_number
            ];
        }

        if ($profile->province_id != null) {
            $kabupaten = Regency::where('id', $profile->regency_id)->first();
            $kecamatan = District::where('id', $profile->district_id)->first();
            $kelurahan = Village::where('id', $profile->village_id)->first();

            $data = [
                'kabupaten' => $kabupaten,
                'kecamatan' => $kecamatan,
                'kelurahan' => $kelurahan
            ];
        }

        return view('BE.traveller.profil', compact('profile', 'provinsi', 'data', 'profile_email', 'profile_phone_number'));
    }

    /**
     * Update Profile Data Diri
     */
    public function updateDataDiri(Request $request)
    {
        $user_id = Auth::user()->id;
        $usertraveller = Usertraveller::where('user_id', $user_id)->first();
        $userdetail = Userdetail::where('user_id', $user_id)->first();
        $user = User::where('id', $user_id)->first();

        if ($usertraveller == null) {
            $usertraveller = Usertraveller::create([
                'user_id' => $user_id,
                'akun_booking_status' => 0,
                'newslatter_status' => 1
            ]);
        }

        if ($userdetail == null) {
            $userdetail = Userdetail::create([
                'user_id' => $user_id,
            ]);
        }
        
        // Validation
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'profile_photo_path' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'date_of_birth' =>'required|date'
        ],[
            'first_name.required' => 'Nama depan wajib diisi!',
            'profile_photo_path.mimes' => 'Foto harus memiliki ekstensi (jpg, jpeg, atau png)',
            'profile_photo_path.max' => 'Foto maksimum berukuran 2MB',
            'date_of_birth.required' => 'Tanggal lahir wajib diisi!',
            'date_of_birth.date' => 'Pastikan tanggal yang Anda masukkan sudah benar.',
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator, 'data_diri');
        }

        $image = $request->file('profile_photo_path');

        // Upload New Image
        if ($request->hasFile('profile_photo_path')) {
            $new_foto = time() . 'FotoProfileTraveller' . $image->getClientOriginalName();
            $tujuan_upload = 'User/FotoProfileTraveller/';
            $image->move($tujuan_upload, $new_foto);
        }

        //Delete Old Image
        if (file_exists(($user->profile_photo_path))) {
            unlink(($user->profile_photo_path));
        }

        // Update User Traveller Table
        $usertraveller->update([
            'last_name' => $request->last_name,
            'date_of_birth' => $request->date_of_birth,
            'gender' => $request->jk
        ]);

        // Update User Detail Table
        $userdetail->update([
            'last_name' => $request->last_name,
            'date_of_birth' => $request->date_of_birth,
        ]);

        // Update User Table
        $user->update([
            'first_name' => $request->first_name,
            'jk' => $request->jk,
            'profile_photo_path' => $image == null ? $user->profile_photo_path : $tujuan_upload . $new_foto,
        ]);

        return redirect()->route('profile')->with(['success_data_diri'=>'Data diri berhasil diperbarui']);
    }

    /**
     * Update Profile Data Alamat
     */
    public function updateDataAlamat(Request $request)
    {
        $user_id = Auth::user()->id;
        $usertraveller = Usertraveller::where('user_id', $user_id)->first();
        $userdetail = Userdetail::where('user_id', $user_id)->first();

        $validator = Validator::make($request->all(), [
            'address' => 'required',
            'provinsi' => 'required',
            'kabupaten' =>'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'postal_code' => 'required'
        ],[
            'address.required' => 'Alamat wajib diisi!',
            'provinsi.required' => 'Provinsi wajib diisi!',
            'kabupaten.required' => 'Kota/kabupaten wajib diisi!',
            'kecamatan.required' => 'Kecamatan wajib diisi!',
            'kelurahan.required' => 'Kelurahan wajib diisi!',
            'postal_code.required' => 'Kode Pos wajib diisi!',
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator, 'alamat');
        }

        // Update User Traveller Table
        $usertraveller->update([
            'province_id'     => $request->provinsi,
            'regency_id'     => $request->kabupaten,
            'district_id'   => $request->kecamatan,
            'village_id' => $request->kelurahan,
            'address' => $request->address,
            'postal_code' => $request->postal_code,
        ]);

        // Update User Detail Table
        $userdetail->update([
            'province_id'     => $request->provinsi,
            'regency_id'     => $request->kabupaten,
            'district_id'   => $request->kecamatan,
            'village_id' => $request->kelurahan,
            'address' => $request->address,
            'postal_code' => $request->postal_code,
        ]);

        return redirect()->route('profile')->with(['success_alamat'=>'Alamat berhasil diperbarui']);
    }

    /**
     * Add secondary email
     */
    public function updateEmail(Request $request, $email)
    {
        $user_id = Auth::user()->id;
        $usertraveller = Usertraveller::where('user_id', $user_id)->first();
        $userdetail = Userdetail::where('user_id', $user_id)->first();

        // Validation
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email|unique:user_traveller,second_email|unique:user_detail,second_email',
        ],[
            'email.required' => 'Email wajib diisi!',
            'email.email' => 'Pastikan email yang Anda masukkan sudah benar',
            'email.unique' => 'Email yang Anda masukkan sudah terdaftar.',
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator, 'add_secondary_email');
        }

        // Update User Traveller Table
        $usertraveller->update([
            'second_email' => $request->email,
        ]);

        // Update User Detail Table
        $userdetail->update([
            'second_email' => $request->email,
        ]);

        return redirect()->route('profile')->with(['success_add_secondary_email'=>'Secondary email berhasil ditambahkan']);
    }

    /**
     * Delete selected email
     */
    public function deleteEmail(Request $request, $email)
    {
        $user_id = Auth::user()->id;
        $usertraveller = Usertraveller::where('user_id', $user_id)->first();
        $userdetail = Userdetail::where('user_id', $user_id)->first();
        $user = User::where('id', $user_id)->first();

        // Delete Email Utama, update secondary email
        if ($email == $user->email) {
            $user->update([
                'email' => $usertraveller->second_email,
            ]);

            $usertraveller->update([
                'second_email' => null,
            ]);

            $userdetail->update([
                'second_email' => null,
            ]);
        }

        // Delete Secondary Email
        if ($email == $usertraveller->second_email) {
            $usertraveller->update([
                'second_email' => null,
            ]);

            $userdetail->update([
                'second_email' => null,
            ]);
        }

        return redirect()->route('profile')->with(['success_delete_email'=>'Email berhasil dihapus']);
    }

    /**
     * Change Primary Email with selected email
     */
    public function updatePrimaryEmail(Request $request, $email)
    {
        $user_id = Auth::user()->id;
        $usertraveller = Usertraveller::where('user_id', $user_id)->first();
        $userdetail = Userdetail::where('user_id', $user_id)->first();
        $user = User::where('id', $user_id)->first();

        $usertraveller->update([
            'second_email' => $user->email,
        ]);

        $userdetail->update([
            'second_email' => $user->email,
        ]);

        $user->update([
            'email' => $email,
        ]);

        return redirect()->route('profile')->with(['success_update_primary_email'=>'Primary email berhasil diperbarui']);
    }

    /**
     * Add secondary phone number
     */
    public function updatePhoneNumber(Request $request, $no_telp)
    {
        // Validation
        $validator = Validator::make($request->all(), [
            'no_telp' => 'required|unique:users,no_tlp|unique:user_traveller,second_phone_number|unique:user_detail,second_phone_number|min:5|max:13',
        ],[
            'no_telp.required' => 'No telepon wajib diisi!',
            'no_telp.unique' => 'No telepon yang Anda masukkan sudah terdaftar.',
            'no_telp.min' => 'Nomor telepon minimal :min karakter',
            'no_telp.max' => 'Nomor telepon maksimal :max karakter',
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator, 'add_secondary_phone');
        }

        $user_id = Auth::user()->id;
        $usertraveller = Usertraveller::where('user_id', $user_id)->first();
        $userdetail = Userdetail::where('user_id', $user_id)->first();

        $usertraveller->update([
            'second_phone_number' => $request->no_telp,
        ]);

        $userdetail->update([
            'second_phone_number' => $request->no_telp,
        ]);

        return redirect()->route('profile')->with(['success_add_secondary_phone'=>'No telepon berhasil ditambahkan']);
    }

    /**
     * Delete selected phone number
     */
    public function deletePhoneNumber(Request $request, $no_telp)
    {
        $user_id = Auth::user()->id;
        $usertraveller = Usertraveller::where('user_id', $user_id)->first();
        $userdetail = Userdetail::where('user_id', $user_id)->first();
        $user = User::where('id', $user_id)->first();

        if ($no_telp == $user->no_tlp) {
            $user->update([
                'no_tlp' => $usertraveller->second_phone_number,
            ]);

            $usertraveller->update([
                'second_phone_number' => null,
            ]);

            $userdetail->update([
                'second_phone_number' => null,
            ]);
        }

        if ($no_telp == $usertraveller->second_phone_number) {
            $usertraveller->update([
                'second_phone_number' => null,
            ]);

            $userdetail->update([
                'second_phone_number' => null,
            ]);
        }

        return redirect()->route('profile')->with(['success_delete_phone'=>'No telepon berhasil dihapus']);
    }

    /**
     * Change Primary Phone Number with selected phone number
     */
    public function updatePrimaryPhoneNumber(Request $request, $no_telp)
    {
        $user_id = Auth::user()->id;
        $usertraveller = Usertraveller::where('user_id', $user_id)->first();
        $userdetail = Userdetail::where('user_id', $user_id)->first();
        $user = User::where('id', $user_id)->first();

        $usertraveller->update([
            'second_phone_number' => $user->no_tlp,
        ]);

        $userdetail->update([
            'second_phone_number' => $user->no_tlp,
        ]);

        $user->update([
            'no_tlp' => $no_telp,
        ]);

        return redirect()->route('profile')->with(['success_update_primary_phone'=>'No telepon utama berhasil diperbarui']);
    }

    public function getLikes(Request $request)
    {
        $user_id = Auth::user()->id;
        $datas = [];
        $likes = LikeProduct::where('user_id', $user_id)->with('product', 'product.productdetail')->with('user')->get();


        $i = 0;
        $x = 0;

        // echo "<pre>";

        foreach ($likes as $like) {

            $datas[$i]['id'] = $like->id;
            $datas[$i]['user_id'] = $like->user_id;
            $datas[$i]['product_id'] = $like->product_id;
            $datas[$i]['product_name'] = $like->product->product_name;
            $datas[$i]['type'] = $like->product->type;
            $datas[$i]['profile_photo_path'] = $like->user->profile_photo_path ?? '';
            $datas[$i]['first_name'] = $like->user->first_name ?? 'Seller';
            $datas[$i]['thumbnail'] = $like->product->productdetail->thumbnail;

            $details = ProductDetail::where('product_id', $like->id)->get();
            foreach ($details as $detail) {
                // dump($detail->toArray());
                $datas[$i]['detail'][$x]['alamat'] = $detail['alamat'];
                $datas[$i]['detail'][$x]['harga']['dewasa_residen'] = isset($detail['harga']['dewasa_residen']) ? $detail['harga']['dewasa_residen'] : 0;
                $datas[$i]['detail'][$x]['harga']['dewasa_non_residen'] = isset($detail['harga']['dewasa_non_residen']) ? $detail['harga']['dewasa_non_residen'] : 0;
                $datas[$i]['detail'][$x]['harga']['anak_residen'] = isset($detail['harga']['anak_residen']) ? $detail['harga']['anak_residen'] : 0;
                $datas[$i]['detail'][$x]['harga']['anak_non_residen'] = isset($detail['harga']['anak_non_residen']) ? $detail['harga']['anak_non_residen'] : 0;

                $x++;
            }

            $i++;

            // print_r($like);
        }
        // dump($likes);
        // dump($datas);
        return view('BE.traveller.favorit', compact('datas'));
    }

    public function travellerDashboard() {

        $date_created = Auth::user()->created_at;
        if ($date_created != null) {
            $date_joined = date("d F Y", strtotime(Auth::user()->created_at));
        } else {
            $date_joined = "2023";
        }

        $traveller = Usertraveller::where('user_id', Auth::user()->id)->first();
        $orders = BookingOrder::where('customer_id', $traveller->id)
        ->select(DB::raw('count(*) as type_count, type'))
        ->where('status', '1')
        ->groupBy('type')->get();

        return view('BE.traveller.dashboard-traveller', compact('date_joined', 'orders'));
    }
}
