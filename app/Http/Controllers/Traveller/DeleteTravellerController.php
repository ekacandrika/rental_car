<?php

namespace App\Http\Controllers\Traveller;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Usertraveller;
use Illuminate\Http\Request;

class DeleteTravellerController extends Controller
{
    //
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_id = \Auth::user()->id;
        $usertraveller = Usertraveller::where('user_id', $user_id)->first();
        $user = User::where('id', $user_id)->first();

        if (file_exists(($usertraveller->passport_photo))) {
            unlink(($usertraveller->passport_photo));
        }

        if (file_exists(($usertraveller->ID_card_photo))) {
            unlink(($usertraveller->ID_card_photo));
        }

        if (file_exists(($user->profile_photo_path))) {
            unlink(($user->profile_photo_path));
        }

        User::findOrFail($id)->delete();

        return redirect()->route('welcome');
    }
}
