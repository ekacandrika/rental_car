<?php

namespace App\Http\Controllers\Produk;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Mobildetail;
use App\Models\DetailKendaraan;
use App\Models\DetailRute;
use App\Models\JenisMobil;
use App\Models\Kategori;
use App\Models\Pilihan;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\Usertraveller;
use App\Models\reviewPost;
use App\Models\LikeProduct;
use App\Models\BookingOrder;
use App\Models\Kelolatoko;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RentalProdukController extends Controller
{
    //
    public function index()
    {

        $provinsi = Province::all();
        $rental = Product::with('productdetail')->where('type', 'Rental')->get();

        return view('Produk.Rental.index', compact('provinsi', 'rental'));
    }

    public function getLocation($tur_detail, $id){
        $province = Province::where('id', $id)->first();
        $regency = Regency::where('id', $id)->first();
        $district = District::where('id', $id)->first();

        // Check if Location is Province
        if ($province) {
            return $province->name;
        }

        // Check if Location is Province
        if ($regency) {
            return $regency->name;
        }

        // Check if Location is Province
        if ($district) {
            return $district->name;
        }

        return null;
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\newsletter $newsletter
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $rental = Product::where('slug', $slug)->with('productdetail')->first();
        $rental_detail = $rental->productdetail;
        if($rental->productdetail['province_id']){
            $provinsi = Province::where('id', $rental->productdetail['province_id'])->first();

        }else{
            $provinsi = Province::where('id', $rental->productdetail['province_id'])->first();
        }
        
        $mobil_detail = DetailKendaraan::where('id', $rental_detail['id_detail_kendaraan'])->with(['jenismobil', 'merekmobil'])->first();
        // $jenis_mobil = JenisMobil::where('id', $mobil_detail->merekmobil['jenis_id'])->first();
        $harga = Price::where('id', $rental->productdetail['harga_id'])->first();
        $diskon = Discount::where('id', $rental->productdetail['discount_id'])->first();
        $pilihan = Pilihan::where('id', $rental->productdetail['pilihan_id'])->first();

        $tag_location_1 = $this->getLocation($rental_detail, $rental_detail['tag_location_1']);
        $tag_location_2 = $this->getLocation($rental_detail, $rental_detail['tag_location_2']);
        $tag_location_3 = $this->getLocation($rental_detail, $rental_detail['tag_location_3']);
        $tag_location_4 = $this->getLocation($rental_detail, $rental_detail['tag_location_4']);

        $info_toko = KelolaToko::where('id_seller',$rental->user->id)->first();

        // dd($mobil_detail);
        $share = \Share::page('https://www.kamtuu-staging.cerise.id/rental/' . $slug)->facebook()->whatsapp();

        $galleries = \collect([]);

        if (count(json_decode($rental_detail['gallery'])) > 1) {
            $galleries[0] = (json_decode($rental_detail['gallery']))[0];
            $galleries[1] = (json_decode($rental_detail['gallery']))[1];
            $galleries[2] = isset((json_decode($rental_detail['gallery']))[2]) ? (json_decode($rental_detail['gallery']))[2] : (json_decode($rental_detail['gallery']))[0];

            if (isset((json_decode($rental_detail['gallery']))[3]) && isset((json_decode($rental_detail['gallery']))[4])) {
                $galleries[3] = (json_decode($rental_detail['gallery']))[3];
                $galleries[4] = (json_decode($rental_detail['gallery']))[4];
            } elseif (isset((json_decode($rental_detail['gallery']))[3])) {
                $galleries[3] = (json_decode($rental_detail['gallery']))[3];
                $galleries[4] = $galleries[0];
            } elseif ($galleries[2] != $galleries[0]) {
                $galleries[3] = $galleries[0];
                $galleries[4] = $galleries[1];
            } else {
                $galleries[3] = $galleries[1];
                $galleries[4] = $galleries[0];
            }
        }

        $reviews = reviewPost::leftJoin('product', 'product.id', '=', 'review_posts.product_id')->where('review_posts.status','active')->where('slug', $slug)->get();
        $counts = reviewPost::leftJoin('product', 'product.id', '=', 'review_posts.product_id')->where('review_posts.status','active')->where('slug', $slug)
            ->select('review_posts.star_rating')->get();
        $var1 = $counts->sum('star_rating');
        $var2 = $counts->count();

        $booking_count = BookingOrder::where('product_detail_id', $rental_detail->id)->get()->count();
        $like_count = LikeProduct::where('product_id', $rental->id)->get()->count();

        $like = 0;
        if (Auth::user()) {
            $isLike = LikeProduct::where('product_id', $rental->id)->where('user_id', Auth::user()->id)->first();
            $like = isset($isLike) ? $isLike->status : 0;
        }

        $ratings = $var1 == 0 ? 0 : $var1 / $var2;

        // dd($pilihan);

        return view('Produk.Rental.detail', compact('rental', 'rental_detail', 'provinsi', 
        'mobil_detail', 'harga', 'diskon', 'pilihan', 'share', 'galleries', 'reviews',
        'counts', 'var1', 'var2', 'booking_count', 'like_count', 'like', 'ratings',
        'tag_location_1', 'tag_location_2', 'tag_location_3', 'tag_location_4','info_toko'));
    }

    public function proses(Request $request)
    {
        $detail = json_decode($request->cookie('detail'), true);
        $detail = [
            'product_name' => $request->product_name,
            'total' => $request->total,
            'type' => $request->type,
            'toko_id' => $request->toko_id,
            'product_detail_id' => $request->product_detail_id,
            'pilihan_id' => $request->pilihan_id,
            'check_in_date' => Carbon::parse($request->check_in_date)->translatedFormat('Y-m-d'),
            'check_out_date' => Carbon::parse($request->check_out_date)->translatedFormat('Y-m-d'),
        ];
        // dd($detail);
        $cookie = cookie('detail', json_encode($detail), 2880);

        return redirect()->route('rental.detailPesanan')->withCookie($cookie);
    }

    public function detailPesanan(Request $request)
    {
        $detail = json_decode(request()->cookie('detail'), true);
        $code = mt_rand(1, 99) . date('dmY');
        $user_id = Auth::user()->id;
        $profile = Usertraveller::where('user_id', $user_id)->first();
        $agent_id = User::where('role', 'agent')->where('id', $user_id)->first();
        $seller = User::where('id', $detail['toko_id'])->first();

        $detail_produk =Productdetail::where('id',$detail['product_detail_id'])->first();
        $pilihan = Pilihan::Where('id',$detail['pilihan_id'])->first();
        // dd($pilihan);

        $nama_pilihan  = json_decode($pilihan->nama_pilihan);
        $harga_pilihan = json_decode($pilihan->harga_pilihan);
        $kewajiban_pilihan = json_decode($pilihan->kewajiban_pilihan);

        // $judul_pilihan = json_decode($pilihan->judul_pilihan);
        // $deskripsi_pilihan = json_decode($pilihan->deskripsi_pilihan);

        $fields = collect([]);

        if($nama_pilihan){
            foreach($nama_pilihan as $key => $value){
                $fields->push([
                    'nama_pilihan'=>$nama_pilihan[$key],
                    'harga_pilihan'=>$harga_pilihan[$key],
                    'kewajiban_pilihan'=>$kewajiban_pilihan[$key],
                ]);
            }
        }

        $tag_location_1 ="";
        $tag_location_2 ="";
        $tag_location_3 ="";
        $tag_location_4 ="";

        // get location
        $provinsi = Province::all();
        foreach($provinsi as $key => $v1){
            // dump($v1->id);
            // dump($detail_produk->tag_location_1);
            if($detail_produk->tag_location_1 == intval($v1->id)){
                $tag_location_1 = $v1->name;
            }elseif($detail_produk->tag_location_2 == intval($v1->id)){
                $tag_location_2 = $v1->name;
            }elseif($detail_produk->tag_location_3 == intval($v1->id)){
                $tag_location_3 = $v1->name;
            }elseif($detail_produk->tag_location_4 == intval($v1->id)){
                $tag_location_4 = $v1->name;
            }
        }
        // die;
        $regency = Regency::all();
        foreach($regency as $key => $v2){

            if($detail_produk->tag_location_1 == intval($v2->id)){
                $tag_location_1 = $v2->name;
            }elseif($detail_produk->tag_location_2 == intval($v2->id)){
                $tag_location_2 = $v2->name;
            }elseif($detail_produk->tag_location_3 == intval($v2->id)){
                $tag_location_3 = $v2->name;
            }elseif($detail_produk->tag_location_4 == intval($v2->id)){
                $tag_location_4 = $v2->name;
            }
        }
        // die;
        $district = District::all();
        foreach($district as $key => $v3){
            
            if($detail_produk->tag_location_1 == intval($v3->id)){
                $tag_location_1 = $v3->name;
            }elseif($detail_produk->tag_location_2 == intval($v3->id)){
                $tag_location_2 = $v3->name;
            }elseif($detail_produk->tag_location_3 == intval($v3->id)){
                $tag_location_3 = $v3->name;
            }elseif($detail_produk->tag_location_4 == intval($v3->id)){
                $tag_location_4 = $v3->name;
            }
        }

        $village = Village::all();
        foreach($village as $key => $v4){
            
            if($detail_produk->tag_location_1 == intval($v4->id)){
                $tag_location_1 = $v4->name;
            }elseif($detail_produk->tag_location_2 == intval($v4->id)){
                $tag_location_2 = $v4->name;
            }elseif($detail_produk->tag_location_3 == intval($v4->id)){
                $tag_location_3 = $v4->name;
            }elseif($detail_produk->tag_location_4 == intval($v4->id)){
                $tag_location_4 = $v4->name;
            }
        }

        // dd($detail_produk);

        return view('Produk.Rental.form-pemesanan', compact('profile', 'detail', 'code', 'agent_id', 'seller','fields','tag_location_1','tag_location_2','tag_location_3','tag_location_4'));
    }

    // public function getPro

    public function search(Request $request)
    {
        $lokasi = $request->search;
        $durasi = $request->durasi;
        $tgl_mulai = $request->tgl_mulai;
        $waktu_mulai = $request->waktu_mulai;
        $tgl_selesai = $request->tgl_selesai;
        $waktu_selesai = $request->waktu_selesai;
        $lepas_kunci = $request->lepas_kunci;
        $metode_transfer = $request->metode_transfer;

        if ($lokasi && $durasi && $tgl_mulai && $waktu_mulai && $tgl_selesai && $waktu_selesai && $lepas_kunci != null && $metode_transfer == null) {
            $regencyTo = Regency::all();
            $rental = Product::with('productdetail')->where('type', 'rental')->get();
            $data_detail = Productdetail::with('product')->whereHas('product', function ($query) {
                $query->where('type', '=', 'Rental');
            })
                ->where('regency_id', 'LIKE', "%" . $lokasi . "%")
                ->get();

            return view('Produk.Rental.search', compact('rental', 'data_detail', 'lokasi', 'durasi', 'tgl_mulai', 'waktu_mulai', 'tgl_selesai', 'waktu_selesai', 'lepas_kunci', 'metode_transfer'));
        } elseif ($lokasi && $durasi && $tgl_mulai && $waktu_mulai && $tgl_selesai && $waktu_selesai && $lepas_kunci == null && $metode_transfer != null) {
            $regencyTo = Regency::all();
            $rental = Product::with('productdetail')->where('type', 'rental')->get();
            $data_detail = Productdetail::with('product')->whereHas('product', function ($query) {
                $query->where('type', '=', 'Rental');
            })
                ->where('regency_id', 'LIKE', "%" . $lokasi . "%")
                ->get();

            return view('Produk.Rental.search', compact('rental', 'data_detail', 'lokasi', 'durasi', 'tgl_mulai', 'waktu_mulai', 'tgl_selesai', 'waktu_selesai', 'lepas_kunci', 'metode_transfer'));
        } else {
            return redirect()->back();
        }
    }

    public function searchByHarga(Request $request)
    {
        $lokasi = $request->search;
        $durasi = $request->durasi;
        $tgl_mulai = $request->tgl_mulai;
        $waktu_mulai = $request->waktu_mulai;
        $tgl_selesai = $request->tgl_selesai;
        $waktu_selesai = $request->waktu_selesai;
        $lepas_kunci = $request->lepas_kunci;
        $min_price = $request->min_price;
        $max_price = $request->max_price;

        if ($min_price && $max_price && $lokasi && $durasi && $tgl_mulai && $waktu_mulai && $tgl_selesai && $waktu_selesai && $lepas_kunci) {
            $regencyTo = Regency::all();
            $rental = Product::with('productdetail')->where('type', 'rental')->get();
            $detail_rental = Productdetail::with('product', 'detailkendaraan')
                ->whereHas('product', function ($query) {
                    $query->where('type', '=', 'Rental');
                })
                ->where('regency_id', 'LIKE', "%" . $lokasi . "%")
                ->get();

            return view('Produk.Rental.searchByHarga', compact('rental', 'detail_rental', 'lokasi', 'durasi', 'tgl_mulai', 'waktu_mulai', 'tgl_selesai', 'waktu_selesai', 'lepas_kunci', 'min_price', 'max_price'));
        } elseif ($min_price && $max_price && $lokasi && $durasi && $tgl_mulai && $waktu_mulai && $tgl_selesai && $waktu_selesai && $lepas_kunci) {
            $regencyTo = Regency::all();
            $rental = Product::with('productdetail')->where('type', 'rental')->get();
            $detail_rental = Productdetail::with('product', 'detailkendaraan')
                ->whereHas('product', function ($query) {
                    $query->where('type', '=', 'Rental');
                })
                ->where('regency_id', 'LIKE', "%" . $lokasi . "%")
                ->get();

            return view('Produk.Rental.searchByHarga', compact('rental', 'detail_rental', 'lokasi', 'durasi', 'tgl_mulai', 'waktu_mulai', 'tgl_selesai', 'waktu_selesai', 'lepas_kunci', 'min_price', 'max_price'));
        } else {
            return redirect()->back();
        }
    }

    public function rentalIndex(Request $request)
    {
        // $regencyTo  = Regency::all();
        $regency_name = $request->query('name');
        $regencyFrom = DB::table('product as a')
            ->select(DB::raw('c.*'))
            ->join('product_detail as b', 'b.product_id', '=', 'a.id')
            ->join('regencies as c', 'c.id', '=', 'b.regency_id')
            ->where('name', $regency_name)
            ->orWhere('name', str()->upper($regency_name))
            ->distinct()
            ->get();

        $rental =  DB::table('product as a')
            ->select('b.*', 'a.*', 'c.*', 'd.*')
            ->join('product_detail as b', 'b.product_id', '=', 'a.id')
            ->join('regencies as c', 'c.id', '=', 'b.regency_id')
            ->join('price as d', 'd.id', '=', 'b.harga_id', 'left')
            ->where('type', 'rental')
            ->where('name', $regency_name)
            // ->orWhere('name',str()->upper($regency_name))
            ->paginate(5);
        // dd($rental);
        return view('Produk.Rental.new-index', compact('rental', 'regencyFrom'));
    }
}
