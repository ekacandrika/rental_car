<?php

namespace App\Http\Controllers\Produk;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Usertraveller;
use App\Models\Productdetail;
use App\Models\DetailPemesanan;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Paket;
use App\Models\Itenerary;
use App\Models\Kategori;
use App\Models\Pilihan;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\Kelolatoko;
use App\Models\reviewPost;
use App\Models\LikeProduct;
use App\Models\BookingOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Thumbnail;

class TurProdukController extends Controller
{
    //
    public function index(Request $request)
    {
        // dd($request->all());
        $regencyTo = Regency::all();
        $tur = Product::with('productdetail')->where('type', 'tour')->paginate(4);

        return view('Produk.Tur.index', compact('regencyTo', 'tur'));
    }

    public function getLocation($tur_detail, $id){
        $province = Province::where('id', $id)->first();
        $regency = Regency::where('id', $id)->first();
        $district = District::where('id', $id)->first();

        // Check if Location is Province
        if ($province) {
            return $province->name;
        }

        // Check if Location is Province
        if ($regency) {
            return $regency->name;
        }

        // Check if Location is Province
        if ($district) {
            return $district->name;
        }

        return null;
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\newsletter $newsletter
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $tur = Product::where('slug', $slug)->with('productdetail', 'user', 'tagged')->first();
        
        $info_toko = null;
        
        if($tur){
            $info_toko = KelolaToko::where('id_seller',$tur->user->id)->first();
        }
        
        $tur_detail = $tur->productdetail;
        $provinsi = Province::where('id', $tur->productdetail['province_id'])->first();
        $data = Province::with('regencies', 'regencies.districts')->where('id', $tur_detail['tag_location_1'])->first();
        $provinsi_1 = Province::where('id', $tur->productdetail['province_id_1'])->first();
        $provinsi_2 = Province::where('id', $tur->productdetail['province_id_2'])->first();

        $tag_location_1 = $this->getLocation($tur_detail, $tur_detail['tag_location_1']);
        $tag_location_2 = $this->getLocation($tur_detail, $tur_detail['tag_location_2']);
        $tag_location_3 = $this->getLocation($tur_detail, $tur_detail['tag_location_3']);
        $tag_location_4 = $this->getLocation($tur_detail, $tur_detail['tag_location_4']);

        $regency = Regency::where('id', $tur->productdetail['regency_id'])->first();
        $pakets = Paket::where('id', $tur->productdetail['paket_id'])->first();
        $itinerary = Itenerary::where('id', $tur->productdetail['itenenary_id'])->first();
        $harga = [];

        if ($tur_detail->tipe_tur == 'Open Trip') {
            $harga = Price::where('paket_id', $tur->productdetail['paket_id'])->get();

            if (!isset($harga[0])) {
                $harga = Price::where('id', $tur->productdetail['harga_id'])->first();
            }
        }

        if ($tur_detail->tipe_tur == 'Tur Private') {
            $harga = Price::where('id', $tur->productdetail['harga_id'])->first();
        }

        
        $diskon = Discount::where('id', $tur->productdetail['discount_id'])->first();
        $pilihan = Pilihan::where('id', $tur->productdetail['pilihan_id'])->first();
        $share = \Share::page('https://www.kamtuu-staging.cerise.id/tur/' . $slug)->facebook()->whatsapp();

        $galleries = \collect([]);
        
        if(json_decode($tur_detail['gallery'])!=null){
            if (count(json_decode($tur_detail['gallery'])) > 1) {
                $galleries[0] = (json_decode($tur_detail['gallery']))[0];
                $galleries[1] = (json_decode($tur_detail['gallery']))[1];
                $galleries[2] = isset((json_decode($tur_detail['gallery']))[2]) ? (json_decode($tur_detail['gallery']))[2] : (json_decode($tur_detail['gallery']))[0];
    
                if (isset((json_decode($tur_detail['gallery']))[3]) && isset((json_decode($tur_detail['gallery']))[4])) {
                    $galleries[3] = (json_decode($tur_detail['gallery']))[3];
                    $galleries[4] = (json_decode($tur_detail['gallery']))[4];
                } elseif (isset((json_decode($tur_detail['gallery']))[3])) {
                    $galleries[3] = (json_decode($tur_detail['gallery']))[3];
                    $galleries[4] = $galleries[0];
                } elseif ($galleries[2] != $galleries[0]) {
                    $galleries[3] = $galleries[0];
                    $galleries[4] = $galleries[1];
                } else {
                    $galleries[3] = $galleries[1];
                    $galleries[4] = $galleries[0];
                }
            }
        }

        $card_tur = Product::where('type', 'tour')->has('productdetail')->with('tagged', 'productdetail.diskon', 'productdetail.harga', 'productdetail.masterkamar', 'productdetail.detailkendaraan', 'productdetail.regency', 'user')->limit(5)->inRandomOrder()->get();

        $reviews = reviewPost::leftJoin('product', 'product.id', '=', 'review_posts.product_id')->Join('users', 'users.id', '=', 'review_posts.traveller_id')->where('status','active')->where('slug', $slug)->get();
        $counts = reviewPost::leftJoin('product', 'product.id', '=', 'review_posts.product_id')->Join('users', 'users.id', '=', 'review_posts.traveller_id')->where('status','active')->where('slug', $slug)
            ->select('review_posts.star_rating')->get();
        $var1 = $counts->sum('star_rating');
        $var2 = $counts->count();
        // dd($reviews);

        $booking_count = BookingOrder::where('product_detail_id', $tur_detail->id)->get()->count();
        $like_count = LikeProduct::where('product_id', $tur->id)->get()->count();

        $like = 0;
        if (Auth::user()) {
            $isLike = LikeProduct::where('product_id', $tur->id)->where('user_id', Auth::user()->id)->first();
            $like = isset($isLike) ? $isLike->status : 0;
        }

        $ratings = $var1 == 0 ? 0 : $var1 / $var2;

        if ($tur_detail->tipe_tur == 'Tur Private') {
            return view('Produk.Tur.detail-tur-private', compact(
                'tur', 'tur_detail', 'itinerary', 'provinsi', 'provinsi_1', 'provinsi_2', 'pakets',
                'harga', 'diskon', 'pilihan', 'share', 'galleries', 'card_tur', 'reviews',
                'counts', 'var1', 'var2', 'ratings', 'like', 'regency', 'booking_count',
                'like_count', 'tag_location_1', 'tag_location_2', 'tag_location_3', 'tag_location_4',
                'info_toko'
            ));
        }

        if ($tur_detail->tipe_tur == 'Open Trip') {
            $pakets_max = max(json_decode($pakets['max_peserta']));
            $pakets_min = min(json_decode($pakets['min_peserta']));
            
            return view('Produk.Tur.detail', compact(
                'tur', 'tur_detail', 'itinerary', 'provinsi', 'provinsi_1', 'provinsi_2', 'pakets',
                'harga', 'diskon', 'pilihan', 'share', 'galleries', 'card_tur', 'reviews',
                'counts', 'var1', 'var2', 'ratings', 'like', 'regency', 'booking_count',
                'like_count', 'tag_location_1', 'tag_location_2', 'tag_location_3', 'tag_location_4', 
                'pakets_max', 'pakets_min','info_toko'
            ));
        }
    }

    public function search(Request $request)
    {
        // dd($request->all());
        $search = $request->search;
        $tur_to = $request->tur_to;
        $konfirmasi_by = $request->confirmation;
        $kategori = $request->kategori;
        $tgl = $request->tgl;
        
        $regencyFrom1 = null;
        $regencyFrom2 = null;
        $regencyFrom3 = null;
        $regencyFrom4 = null;
        
        if ($kategori == 'Open Trip' && $search && $tur_to && $konfirmasi_by == null) {
            $regencyTo = Regency::all();

            $districtFrom = District::where('id',$search)->with('regency')->first();
            $districtTo = District::where('id',$tur_to)->with('regency')->first();

            $from = $districtFrom->regency->id;
            $to = $districtTo->regency->id;
            
            // dd($regencyTo);

            $tur = Product::with('productdetail')->where('type', 'tour')->get();
            
            $data_tur = Productdetail::with('product')
                ->where('tag_location_3', 'LIKE', "%" . $from . "%")
                ->orWhere('tag_location_3', 'LIKE', "%" . $to . "%")
                ->where('tipe_tur', 'LIKE', "%" . $kategori . "%")
                ->get();

                foreach($data_tur as $key =>$data){
                    $regencyFrom1 = $this->getLocation($data, $data->tag_location_1);
                    $regencyFrom2 = $this->getLocation($data, $data->tag_location_2);
                    $regencyFrom3 = $this->getLocation($data, $data->tag_location_3);
                    $regencyFrom4 = $this->getLocation($data, $data->tag_location_4);
                }
            // dd($data_tur);
            return view('Produk.Tur.search', compact('tur', 'data_tur', 'search', 'konfirmasi_by', 'kategori', 'regencyTo', 'tgl', 'tur_to','regencyFrom1','regencyFrom2','regencyFrom3','regencyFrom4'));
        } elseif ($kategori == 'Tur Private' && $search && $tur_to && $konfirmasi_by == null) {
            $regencyTo = Regency::all();
            // $this->getLocation($tur_detail, $tur_detail['tag_location_1']);
            $tur = Product::with('productdetail')->where('type', 'tour')->get();
            $data_tur = Productdetail::with('product')->where('regency_id', 'LIKE', "%" . $search . "%")
            ->where('tipe_tur', 'LIKE', "%" . $kategori . "%")
            ->get();

            foreach($data_tur as $key =>$data){
                $regencyFrom1 = $this->getLocation($data, $data->tag_location_1);
                $regencyFrom2 = $this->getLocation($data, $data->tag_location_2);
                $regencyFrom3 = $this->getLocation($data, $data->tag_location_3);
                $regencyFrom4 = $this->getLocation($data, $data->tag_location_4);
            }

            return view('Produk.Tur.search', compact('tur', 'data_tur', 'search', 'konfirmasi_by', 'kategori', 'regencyTo', 'tgl', 'tur_to','regencyFrom1','regencyFrom2','regencyFrom3','regencyFrom4'));
        } elseif ($search && $tur_to && $konfirmasi_by != null && $kategori == 'Open Trip') {
            $regencyTo = Regency::all();
            $tur = Product::with('productdetail')->where('type', 'tour')->get();
            $data_tur = Productdetail::with('product')->where('regency_id', 'LIKE', "%" . $search . "%")
                ->where('confirmation', 'LIKE', "%" . $konfirmasi_by . "%")
                ->where('tipe_tur', 'LIKE', "%" . $kategori . "%")
                ->get();
                foreach($data_tur as $key =>$data){
                    $regencyFrom1 = $this->getLocation($data, $data->tag_location_1);
                    $regencyFrom2 = $this->getLocation($data, $data->tag_location_2);
                    $regencyFrom3 = $this->getLocation($data, $data->tag_location_3);
                    $regencyFrom4 = $this->getLocation($data, $data->tag_location_4);
                }
            // die;
            return view('Produk.Tur.search.searchFull', compact('tur', 'data_tur', 'search', 'konfirmasi_by', 'kategori', 'regencyTo', 'tgl', 'tur_to','regencyFrom1','regencyFrom2','regencyFrom3','regencyFrom4'));
        } elseif ($search && $tur_to && $konfirmasi_by != null && $kategori == 'Tur Private') {
            $regencyTo = Regency::all();
            $tur = Product::with('productdetail')->where('type', 'tour')->get();
            $data_tur = Productdetail::with('product')->where('regency_id', 'LIKE', "%" . $search . "%")
                ->where('confirmation', 'LIKE', "%" . $konfirmasi_by . "%")
                ->where('tipe_tur', 'LIKE', "%" . $kategori . "%")
                ->get();

                foreach($data_tur as $key =>$data){
                    $regencyFrom1 = $this->getLocation($data, $data->tag_location_1);
                    $regencyFrom2 = $this->getLocation($data, $data->tag_location_2);
                    $regencyFrom3 = $this->getLocation($data, $data->tag_location_3);
                    $regencyFrom4 = $this->getLocation($data, $data->tag_location_4);
                }

            return view('Produk.Tur.search.searchFull', compact('tur', 'data_tur', 'search', 'konfirmasi_by', 'kategori', 'regencyTo', 'tgl', 'tur_to','regencyFrom1','regencyFrom2','regencyFrom3','regencyFrom4'));
        } else {
            return redirect()->back();
        }
    }

    public function searchByHarga(Request $request)
    {
        $search = $request->search;
        $tur_to = $request->tur_to;
        $konfirmasi_by = $request->confirmation;
        $kategori = $request->kategori;
        $tgl = $request->tgl;
        $min_price = $request->min_price;
        $max_price = $request->max_price;

        if ($kategori == 'Open Trip' && $search && $tur_to && $konfirmasi_by == null && $min_price && $max_price) {
            $regencyTo = Regency::all();
            $tur = Product::with('productdetail')->where('type', 'tour')->get();
            $data_tur = Productdetail::with('product')->where('regency_id', 'LIKE', "%" . $search . "%")
                ->where('tipe_tur', 'LIKE', "%" . $kategori . "%")
                ->get();

            return view('Produk.Tur.searchByHarga', compact('tur', 'data_tur', 'search', 'konfirmasi_by', 'kategori', 'regencyTo', 'tgl', 'tur_to', 'min_price', 'max_price'));
        } elseif ($kategori == 'Tur Private' && $search && $tur_to && $konfirmasi_by == null && $min_price && $max_price) {
            $regencyTo = Regency::all();
            $tur = Product::with('productdetail')->where('type', 'tour')->get();
            $data_tur = Productdetail::with('product')->where('regency_id', 'LIKE', "%" . $search . "%")
                ->where('tipe_tur', 'LIKE', "%" . $kategori . "%")
                ->get();

            return view('Produk.Tur.searchByHarga', compact('tur', 'data_tur', 'search', 'konfirmasi_by', 'kategori', 'regencyTo', 'tgl', 'tur_to', 'min_price', 'max_price'));
        } elseif ($search && $tur_to && $konfirmasi_by != null && $kategori == 'Open Trip' && $min_price && $max_price) {
            $regencyTo = Regency::all();
            $tur = Product::with('productdetail')->where('type', 'tour')->get();
            $data_tur = Productdetail::with('product')->where('regency_id', 'LIKE', "%" . $search . "%")
                ->where('confirmation', 'LIKE', "%" . $konfirmasi_by . "%")
                ->where('tipe_tur', 'LIKE', "%" . $kategori . "%")
                ->get();

            return view('Produk.Tur.searchByHarga', compact('tur', 'data_tur', 'search', 'konfirmasi_by', 'kategori', 'regencyTo', 'tgl', 'tur_to', 'min_price', 'max_price'));
        } elseif ($search && $tur_to && $konfirmasi_by != null && $kategori == 'Tur Private' && $min_price && $max_price) {
            $regencyTo = Regency::all();
            $tur = Product::with('productdetail')->where('type', 'tour')->get();
            $data_tur = Productdetail::with('product')->where('regency_id', 'LIKE', "%" . $search . "%")
                ->where('confirmation', 'LIKE', "%" . $konfirmasi_by . "%")
                ->where('tipe_tur', 'LIKE', "%" . $kategori . "%")
                ->get();

            return view('Produk.Tur.searchByHarga', compact('tur', 'data_tur', 'search', 'konfirmasi_by', 'kategori', 'regencyTo', 'tgl', 'tur_to', 'min_price', 'max_price'));
        } else {
            return redirect()->back();
        }
    }

    public function turOrder(Request $request, $slug)
    {
        $data = $request->all();
        // dd($data);
        $request->session()->forget('order');

        $code = mt_rand(1, 99) . date('dmY');
        $request->session()->put('order.booking_code', $code);
        $request->session()->put('order.product_name', $data['product_name']);
        $request->session()->put('order.product_id', $data['product_id']);

        $request->session()->put('order.toko_id', $data['toko_id']);
        $request->session()->put('order.product_detail_id', $data['product_detail_id']);
        $request->session()->put('order.type', $data['type']);

        $request->session()->put('order.harga_id', isset($data['harga_id']) ? $data['harga_id'] : '');
        // dd($request->session()->get('order.harga_id'));
        
        $request->session()->put('order.paket_id', isset($data['paket_id']) ? $data['paket_id'] : '');
        $request->session()->put('order.pilihan_id', $data['pilihan_id']);
        $request->session()->put('order.tur_date', $data['tur_date']);

        $request->session()->put('order.paket.index', isset($data['paket']['index']) ? $data['paket']['index'] : '');
        $request->session()->put('order.paket.nama_paket', isset($data['paket']['nama_paket']) ?  $data['paket']['nama_paket'] : '');
        $request->session()->put('order.paket.waktu', isset($data['paket']['waktu']) ? $data['paket']['waktu'] : '');
        $request->session()->put('order.pilihan', isset($data['pilihan'][0]['harga']) ? $data['pilihan'] : []);
        $request->session()->put('order.catatan_extra', isset($data['catatan_ekstra']) ? $data['catatan_ekstra'] : '');
        $request->session()->put('order.extra', isset($data['extra']) ? $data['extra'] : []);
        $request->session()->put('order.dewasa_count', (int) $data['dewasa_count']);
        $request->session()->put('order.anak_count', (int) $data['anak_count']);
        $request->session()->put('order.balita_count', (int) $data['balita_count']);

        // return redirect()->route('tur.viewToOrder', [$slug]);
        // return view('Produk.Tur.detail-pemesanan', compact('slug', 'data'));
        // return view('Produk.Tur.detail-pemesanan', compact('slug', 'data', 'seller'));
        if (Auth::user() != null) {
            return redirect()->route('tur.viewToOrder', [$slug]);
        } else {
            return redirect()->route('LoginTraveller');
        }
    }

    public function viewToOrder(Request $request, $slug)
    {
        // dd($request->all());
        $order = $request->session()->get('order');
        $seller = Product::where('slug', $slug)->with('productdetail', 'user', 'productdetail.diskon')->first();
        
        if($order == null || Auth::user() == null){
            return redirect()->back();
        }

        if($seller->type=='tour'){
            if($seller->productdetail->tipe_tur==='Open Trip'){
                $harga = Price::where('id', $request->session()->get('order.harga_id'))->first();
            }else{
                $harga = Price::where('id', $seller->productdetail->harga_id)->first();
            }
        }else{
            $harga = Price::where('id', $seller->productdetail->harga_id)->first();
        }
        
        // dd($request->session()->get('order.harga_id'));        
        // dd($harga);
        $data_booking = Usertraveller::where('user_id', Auth::user()->id)->first();
        $data_user = Auth::user();
        // dd($order);
        $get_total_people = $request->session()->get('order.dewasa_count');
        $get_date = $request->session()->get('order.tur_date');
        $formated_date = Carbon::parse($get_date)->format('d F Y');
        $diskon_tanggal = 0;
        $diskon_group = 0;

        $total_pilihan = 0;
        $total_extra = 0;

        // dd($order['pilihan']);
        if (isset($order['pilihan'])) {
            foreach ($order['pilihan'] as $key => $value) {
                $total_pilihan = $total_pilihan + ($value['jumlah'] * $value['harga']);
            }
        }

        if (isset($order['extra'])) {
            foreach ($order['extra'] as $key => $value) {
                $total_extra = $total_extra + ($value['jumlah_extra'] * $value['harga_extra']);
            }
        }

        foreach (json_decode($seller->productdetail->diskon->tgl_start) as $key => $value) {
            $tgl_start = Carbon::parse($value)->toDateTimeString();
            $tgl_end = Carbon::parse(json_decode($seller->productdetail->diskon->tgl_end)[$key])->toDateTimeString();
            $check = Carbon::parse($get_date)->between($tgl_start, $tgl_end);

            if ($check) {
                $diskon_tanggal = json_decode($seller->productdetail->diskon->discount)[$key];
                break;
            }
        }

        foreach (json_decode($seller->productdetail->diskon->min_orang) as $key => $value) {

            $max_orang = json_decode($seller->productdetail->diskon->max_orang)[$key];
            $diskon_orang = json_decode($seller->productdetail->diskon->diskon_orang)[$key];

            if ($get_total_people >= $value && $get_total_people <= $max_orang) {
                $diskon_group = $diskon_group < $diskon_orang ? $diskon_orang : $diskon_group;
            }
        }

        return view('Produk.Tur.detail-pemesanan', compact(
            'slug',
            'order',
            'seller',
            'harga',
            'diskon_tanggal',
            'diskon_group',
            'total_pilihan',
            'total_extra',
            'data_booking',
            'formated_date',
            'data_user'
        ));
    }

    public function addToOrder(Request $request)
    {
        $data = $request->all();
        // dd($data);

        $phone_number_order = collect([]);
        $ID_card_photo_order = collect([]);

        foreach ($data['phone_number'] as $key => $value) {
            if ($data['nationality'][$key] == 'indonesia') {
                $phone_number_order->push('62' . $value);
            } else {
                $phone_number_order->push($value);
            }
        }

        if ($request->hasFile('kitas')) {
            // dd('true');
            $image = $request->file('kitas');

            foreach ($image as $key => $value) {

                $new_foto = time() . 'KitasPesertaOrder' . $value->getClientOriginalName();
                $tujuan_upload = 'Order/Peserta/Kitas/';

                // $lebar_foto = Image::make($value)->width();
                // $lebar_foto= $lebar_foto * 50 / 100;

                // Image::make($value)->resize($lebar_foto, null, function ($constraint) {
                //     $constraint->aspectRatio();
                // })->save($tujuan_upload . $new_foto);

                $value->move($tujuan_upload, $new_foto);
                $images_gallery = $tujuan_upload . $new_foto;

                $ID_card_photo_order->push(
                    $images_gallery,
                );
            }
        }
        // dd($ID_card_photo_order);

        $index_pilihan = collect([]);
        $jumlah_pilihan = collect([]);

        $extra_id = collect([]);
        $catatan_per_ekstra = collect([]);
        $jumlah_ekstra = collect([]);

        foreach ($request->session()->get('order.pilihan') as $key => $value) {
            if (isset($value['index'])) {
                $index_pilihan->push($value['index']);
                $jumlah_pilihan->push($value['jumlah']);
            }
        }

        foreach ($request->session()->get('order.extra') as $key => $value) {
            // dd($value['id_extra']);
            $index = '';
            if (isset($value['id_extra'])) {
                $index = $value['id_extra'];
            }
            // $index = $value;
            // dd($index);
            $extra_id->push($index);
            $jumlah_ekstra->push($value['jumlah_extra']);
            $catatan_per_ekstra->push($value['note_extra']);
        }

        $detailpemesanan = DetailPemesanan::create([
            'user_id' => Auth::user()->id,
            'product_id' => $request->session()->get('order.product_id'),
            // 'total_harga' => $data['total_harga'],

            'paket_id' => $request->session()->get('order.paket_id'),
            'paket_index' => $request->session()->get('order.paket.index'),

            // 'order_code' => $request->session()->get('order.order_code'),
            'tanggal_kegiatan' => $request->session()->get('order.activity_date'),

            'pilihan_id' => $request->session()->get('order.pilihan_id'),
            'index_pilihan' => json_encode($index_pilihan),
            'jumlah_pilihan' => json_encode($jumlah_pilihan),

            'extras_id' => json_encode($extra_id),
            'catatan_umum_extra' => $request->session()->get('order.catatan_extra'),
            'catatan_per_extra' => json_encode($catatan_per_ekstra),
            'jumlah_extra' => json_encode($jumlah_ekstra),

            'kategori_peserta' => json_encode($request->kategori_peserta),
            'first_name' => json_encode($data['first_name']),
            'last_name' => json_encode($data['last_name']),
            'date_of_birth' => json_encode($data['birth_date']),
            'jk' => json_encode($data['jenis_kelamin']),
            'status_residen' => json_encode($data['residen_status']),
            'citizenship' => json_encode($data['nationality']),
            // 'country_code_order' => json_encode($data['country_code']),
            'phone_number_order' => json_encode($phone_number_order),
            'email_order' => json_encode($data['email']),
            'ID_card_photo_order' => json_encode($ID_card_photo_order),
        ]);

        $order = $request->session()->forget('order');
    }

    public function turIndex(Request $request)
    {
        $name = $request->query('name');
        $regencyTo  = Regency::all();

        $regencyFrom = DB::table('product as a')
            ->select(DB::raw('c.*'))
            ->join('product_detail as b', 'b.product_id', '=', 'a.id')
            ->join('regencies as c', 'c.id', '=', 'b.regency_id')
            ->where('name', $name)
            ->orWhere('name', str()->upper($name))
            ->distinct()
            ->get();

        $tur =  DB::table('product as a')
            ->select('b.*', 'a.*', 'c.*', 'd.*')
            ->join('product_detail as b', 'b.product_id', '=', 'a.id')
            ->join('regencies as c', 'c.id', '=', 'b.regency_id')
            ->join('price as d', 'd.id', '=', 'b.harga_id', 'left')
            ->where('name', 'KOTA YOGYAKARTA')
            ->where('type', 'tur')
            // ->orWhere('name',str()->upper($regency_name))
            ->paginate(5);

        // dd($tur);
        return view('Produk.Tur.new-index', compact('regencyTo', 'tur', 'regencyFrom'));
    }

    // 
    public function reviewProductStore(Request $request)
    {   

        if(auth()->user()->role!='traveller'){
            return false;
        }

        $gallery=[];
        $imgs = collect([]);
        
        if($request->images){
            foreach($request->images as $key => $img){
                $new_file = time().rand(1,99).$key.'.'.$img->getClientOriginalExtension();
               
                $img->move(storage_path('app/public/gallery_post'),$new_file);
                $imgs->push($new_file);
            }
        }

        $gallery['result']=$imgs;
        // die;
        $review = new reviewPost();
        $review->product_id = $request->product_id;
        $review->comments = $request->comment;
        $review->star_rating = $request->rating;
        $review->gallery_post = json_encode($gallery);
        $review->traveller_id = auth()->user()->id;
        $review->status ='deactive';

        $review->save();
        return redirect()->back()->with('flash_msg_success', 'Your review has been submitted Successfully,');
    }

    public function reviewPostGallery(Request $request){
        dd($request->all());
    }
}
