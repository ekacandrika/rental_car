<?php

namespace App\Http\Controllers\Produk;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Mobildetail;
use App\Models\DetailKendaraan;
use App\Models\DetailRute;
use App\Models\JenisMobil;
use App\Models\Kategori;
use App\Models\Pilihan;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\Usertraveller;
use App\Models\reviewPost;
use App\Models\LikeProduct;
use App\Models\BookingOrder;
use App\Models\LayoutBus;
use App\Models\Masterroute;
use App\Models\Kelolatoko;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class TransferProdukController extends Controller
{
    //
    public function index()
    {

        $regencyTo = Regency::all();
        $transfer = Product::with('productdetail')->where('type', 'Transfer')->get();
        // dd($transfer);

        return view('Produk.Transfer.index', compact('regencyTo', 'transfer'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\newsletter $newsletter
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $transfer = Product::where('slug', $slug)->with('productdetail')->first();
        $transfer_detail = $transfer->productdetail;
        $provinsi = Province::where('id', $transfer->productdetail['province_id'])->first();
        $mobil_detail = DetailKendaraan::where('id', $transfer->productdetail['id_detail_kendaraan'])->with('merekmobil', 'jenismobil', 'jenisbus', 'merekbus')->first();
        // $jenis_mobil = JenisMobil::where('id', $mobil_detail->merekmobil['jenis_id'])->first();
        $harga = Price::where('id', $transfer->productdetail['harga_id'])->first();
        $diskon = Discount::where('id', $transfer->productdetail['discount_id'])->first();
        $pilihan = Pilihan::where('id', $transfer->productdetail['pilihan_id'])->first();
        $rute = DetailRute::where('id', $transfer->productdetail['rute_id'])->first();
        $master_route = Masterroute::where('id', $transfer->productdetail->rute_id)->first();

        $info_toko = KelolaToko::where('id_seller',$transfer->user->id)->first();

        // dd($diskon);
        $tgl_start = isset($diskon) ? $diskon->tgl_start :null;
        $tgl_end = isset($diskon) ? $diskon->tgl_end :null;
        $kebijakan_sebelumnya = json_decode($transfer_detail['kebijakan_pembatalan_sebelumnya']);

        $share = \Share::page('https://www.kamtuu-staging.cerise.id/transfer/' . $slug)->facebook()->whatsapp();

        $galleries = \collect([]);

        if (count(json_decode($transfer_detail['gallery'])) > 1) {
            $galleries[0] = (json_decode($transfer_detail['gallery']))[0];
            $galleries[1] = (json_decode($transfer_detail['gallery']))[1];
            $galleries[2] = isset((json_decode($transfer_detail['gallery']))[2]) ? (json_decode($transfer_detail['gallery']))[2] : (json_decode($transfer_detail['gallery']))[0];

            if (isset((json_decode($transfer_detail['gallery']))[3]) && isset((json_decode($transfer_detail['gallery']))[4])) {
                $galleries[3] = (json_decode($transfer_detail['gallery']))[3];
                $galleries[4] = (json_decode($transfer_detail['gallery']))[4];
            } elseif (isset((json_decode($transfer_detail['gallery']))[3])) {
                $galleries[3] = (json_decode($transfer_detail['gallery']))[3];
                $galleries[4] = $galleries[0];
            } elseif ($galleries[2] != $galleries[0]) {
                $galleries[3] = $galleries[0];
                $galleries[4] = $galleries[1];
            } else {
                $galleries[3] = $galleries[1];
                $galleries[4] = $galleries[0];
            }
        }

        $detail_rute = isset($transfer_detail->drop_pick_detail) ? json_decode($transfer_detail->drop_pick_detail, true)['result'] : [];


        if ($detail_rute) {
            // dd($detail_rute);
            // $nama_bus =
            $rute = collect([]);

            foreach ($detail_rute as $value) {
                $rute->push([
                    'judul_bus' => isset($value['judul_bus']) ? $value['judul_bus'] :null,
                    'id_pickup' => isset($value['id_pickup']) ? $value['id_pickup']:null,
                    'latitude_pickup' => isset($value['latitude_pickup']) ? $value['latitude_pickup']:null,
                    'longitude_pickup' => isset($value['longitude_pickup']) ?$value['longitude_pickup']:null,
                    'tempat_pickup' => isset($value['tempat_pickup']) ? $value['tempat_pickup']:null,
                    'id_drop' => isset($value['id_drop']) ?$value['id_drop']:null,
                    'latitude_drop' => isset($value['latitude_drop']) ?$value['latitude_drop']:null,
                    'longitude_drop' => isset($value['longitude_drop']) ? $value['longitude_drop']:null,
                    'tempat_drop' => isset($value['tempat_drop']) ? $value['tempat_drop']:null,
                    'harga' => isset($value['harga']) ? $value['harga']:null
                ]);
            }

            // dd($transfer_detail);
        }

        // $card_activity = Product::where('type', 'activity')->has('productdetail')->with('tagged', 'productdetail.diskon', 'productdetail.harga', 'productdetail.masterkamar', 'productdetail.detailkendaraan', 'productdetail.regency', 'user')->limit(5)->inRandomOrder()->get();

        $reviews = reviewPost::leftJoin('product', 'product.id', '=', 'review_posts.product_id')->Join('users', 'users.id', '=', 'review_posts.traveller_id')->where('review_posts.status','active')->where('slug', $slug)->get();
        $counts = reviewPost::leftJoin('product', 'product.id', '=', 'review_posts.product_id')->Join('users', 'users.id', '=', 'review_posts.traveller_id')->where('review_posts.status','active')->where('slug', $slug)
            ->select('review_posts.star_rating')->get();
        $var1 = $counts->sum('star_rating');
        $var2 = $counts->count();

        $booking_count = BookingOrder::where('product_detail_id', $transfer_detail->id)->get()->count();
        $like_count = LikeProduct::where('product_id', $transfer->id)->get()->count();

        $like = 0;
        if (Auth::user()) {
            $isLike = LikeProduct::where('product_id', $transfer->id)->where('user_id', Auth::user()->id)->first();
            $like = isset($isLike) ? $isLike->status : 0;
        }

        $ratings = $var1 == 0 ? 0 : $var1 / $var2;

        // $rute = isset($master_route->rute) ? json_decode($master_route->rute, true)['result'] : null;
        // $from = isset($rute) ? District::where('id',$rute['from'])->first()->name :'Bandara Internasional YIA';
        // $to = isset($rute) ? District::where('id',$rute['to'])->first()->name : 'Hotel Yogyakarta';

        return view('Produk.Transfer.detail', compact(
            'transfer',
            'transfer_detail',
            'provinsi',
            'mobil_detail',
            'harga',
            'diskon',
            'pilihan',
            'share',
            'galleries',
            'kebijakan_sebelumnya',
            'rute',
            'reviews',
            'counts',
            'var1',
            'var2',
            'booking_count',
            'like_count',
            'like',
            'ratings',
            'detail_rute',
            'tgl_start',
            'tgl_end',
            'info_toko'
        ));
    }

    public function proses(Request $request)
    {
        // dd($request->all());
        $detail = json_decode($request->cookie('detail'), true);
        $detail = [
            'product_name' => $request->product_name,
            'total' => $request->total,
            'type' => $request->type,
            'toko_id' => $request->toko_id,
            'pilihan_id' => $request->pilihan_id,
            'product_detail_id' => $request->product_detail_id,
            'harga_kursi'=>$request->harga_kursi_total,
            'start_date' => Carbon::parse($request->check_in_date)->translatedFormat('Y-m-d'),
        ];
        // dd($detail);
        $cookie = cookie('detail', json_encode($detail), 2880);

        if ($request->status == 'Transfer Umum') {
            // update layout kursi dulu
            $this->update_kursi($request->layout, $request->layout_bus_id);
            $data = $request->all();
            $request->session()->put('order.catatan_extra', isset($data['catatan_ekstra']) ? $data['catatan_ekstra'] : '');
            $request->session()->put('order.extra', isset($data['extra']) ? $data['extra'] : []);
            $request->session()->put('order.chair', isset($data['kursi']) ? $data['kursi'] : []);
            $request->session()->put('order.route', isset($data['rute']) ? $data['rute'] : []);
            return redirect()->route('transfer.detailPesanan.umum')->withCookie($cookie);
        } elseif ($request->status == 'Transfer Private') {
            $data = $request->all();
            $request->session()->put('order.catatan_extra', isset($data['catatan_ekstra']) ? $data['catatan_ekstra'] : '');
            $request->session()->put('order.extra', isset($data['extra']) ? $data['extra'] : []);
            return redirect()->route('transfer.detailPesanan.private')->withCookie($cookie);
        }
    }

    public function update_kursi($layout, $id)
    {

        $layout_bus = LayoutBus::where('id_layout', $id)->first();
        $layout_kursi['result'] = $layout;
        // dd($layout_kursi);
        $layout_bus::where('id_layout', $id)->update([
            'layout_kursi' => json_encode($layout_kursi)
        ]);
    }

    public function detailPesananUmum(Request $request)
    {
        
        $order = $request->session()->get('order');
        $detail = json_decode(request()->cookie('detail'), true);
        $code = mt_rand(1, 99) . date('dmY');
        $user_id = Auth::user()->id;
        $profile = Usertraveller::where('user_id', $user_id)->first();
        $agent_id = User::where('role', 'agent')->where('id', $user_id)->first();
        $seller = User::where('id', $detail['toko_id'])->first();
        $harga_kursi = $detail['harga_kursi'];
        $pilihan = Pilihan::Where('id', $detail['pilihan_id'])->first();
        // dd($detail);

        $nama_pilihan  = json_decode($pilihan->nama_pilihan);
        $harga_pilihan = json_decode($pilihan->harga_pilihan);

        $fields = collect([]);

        if ($nama_pilihan) {
            foreach ($nama_pilihan as $key => $value) {
                $fields->push([
                    'nama_pilihan' => $nama_pilihan[$key],
                    'harga_pilihan' => $harga_pilihan[$key],
                ]);
            }
        }


        $product_detail =  Productdetail::where('id', $detail['product_detail_id'])->first();
        $detail_rute = isset($product_detail->drop_pick_detail) ? json_decode($product_detail->drop_pick_detail, true)['result'] : [];

        return view('Produk.Transfer.form-pemesanan', compact('profile', 'detail', 'code', 'agent_id', 'order', 'seller', 'fields','harga_kursi'));
    }

    public function detailPesananPrivate(Request $request)
    {
        $order = $request->session()->get('order');
        $detail = json_decode(request()->cookie('detail'), true);
        $code = mt_rand(1, 99) . date('dmY');
        $user_id = Auth::user()->id;
        $profile = Usertraveller::where('user_id', $user_id)->first();
        $agent_id = User::where('role', 'agent')->where('id', $user_id)->first();
        $seller = User::where('id', $detail['toko_id'])->first();

        $pilihan = Pilihan::Where('id', $detail['pilihan_id'])->first();

        $nama_pilihan  = json_decode($pilihan->nama_pilihan);
        $harga_pilihan = json_decode($pilihan->harga_pilihan);

        $fields = collect([]);

        if ($nama_pilihan) {
            foreach ($nama_pilihan as $key => $value) {
                $fields->push([
                    'nama_pilihan' => $nama_pilihan[$key],
                    'harga_pilihan' => $harga_pilihan[$key],
                ]);
            }
        }

        $product_detail =  Productdetail::where('id', $detail['product_detail_id'])->first();
        $master_route = Masterroute::where('id', $product_detail->rute_id)->first();

        $thumbnail = $product_detail->thumbnail;
        $rute = isset($master_route->rute) ? json_decode($master_route->rute, true)['result'] : null;
        $from = isset($rute) ? District::where('id', $rute['from'])->first()->name : 'Bandara Internasional YIA';
        $to = isset($rute) ? District::where('id', $rute['to'])->first()->name : 'Hotel Yogyakarta';

        return view('Produk.Transfer.form-private', compact('profile', 'detail', 'code', 'agent_id', 'order', 'seller', 'fields', 'from', 'to'));
    }

    public function search(Request $request)
    {
        $dari = $request->search;
        $ke = $request->to;
        $tgl = $request->tgl;
        $waktu_ambil = $request->waktu_ambil;
        $metode_transfer = $request->metode_transfer;

        if ($dari != null && $ke != null && $tgl != null && $waktu_ambil != null && $tgl != null && $metode_transfer == 'Transfer Umum') {
            $regencyTo = $this->getSelectFromToTour();
            $transfer = Product::with('productdetail')->where('type', 'transfer')->get();
            $detail_transfer = Productdetail::with('product', 'detailkendaraan')
                ->whereHas('product', function ($query) {
                    $query->where('type', '=', 'transfer');
                })
                ->whereHas('detailkendaraan', function ($query) {
                    $query->where('status', '=', 'Transfer Umum');
                })
                ->get();

            return view('Produk.Transfer.search', compact('transfer', 'detail_transfer', 'regencyTo', 'dari', 'ke', 'tgl', 'waktu_ambil', 'metode_transfer'));
        } elseif ($dari != null && $ke != null && $tgl != null && $waktu_ambil != null && $tgl != null && $metode_transfer == 'Transfer Private') {
            $regencyTo = $this->getSelectFromToTour();
            $transfer = Product::with('productdetail')->where('type', 'transfer')->get();
            $detail_transfer_private = Productdetail::with('product', 'detailkendaraan')
                ->whereHas('product', function ($query) {
                    $query->where('type', '=', 'transfer');
                })
                ->whereHas('detailkendaraan', function ($query) {
                    $query->where('status', '=', 'Transfer Private');
                })
                ->get();

            return view('Produk.Transfer.search', compact('transfer', 'detail_transfer_private', 'regencyTo', 'dari', 'ke', 'tgl', 'waktu_ambil', 'metode_transfer'));
        } else {
            return redirect()->back();
        }
    }

    public function filterByHargaTransfer(Request $request)
    {
        $dari = $request->search;
        $ke = $request->to;
        $tgl = $request->tgl;
        $waktu_ambil = $request->waktu_ambil;
        $metode_transfer = $request->metode_transfer;
        $min_price = $request->min_price;
        $max_price = $request->max_price;

        if ($dari != null && $ke != null && $tgl != null && $waktu_ambil != null && $tgl != null && $metode_transfer == 'Transfer Umum' && $min_price && $max_price) {
            $regencyTo = $this->getSelectFromToTour();
            $transfer = Product::with('productdetail')->where('type', 'transfer')->get();
            $detail_transfer = Productdetail::with('product', 'detailkendaraan')
                ->whereHas('product', function ($query) {
                    $query->where('type', '=', 'transfer');
                })
                ->whereHas('detailkendaraan', function ($query) {
                    $query->where('status', '=', 'Transfer Umum');
                })
                ->get();

            return view('Produk.Transfer.searchByHarga', compact('transfer', 'detail_transfer', 'regencyTo', 'dari', 'ke', 'tgl', 'waktu_ambil', 'metode_transfer', 'min_price', 'max_price'));
        } elseif ($dari != null && $ke != null && $tgl != null && $waktu_ambil != null && $tgl != null && $metode_transfer == 'Transfer Private' && $min_price && $max_price) {
            $regencyTo = $this->getSelectFromToTour();
            $transfer = Product::with('productdetail')->where('type', 'transfer')->get();
            $detail_transfer_private = Productdetail::with('product', 'detailkendaraan')
                ->whereHas('product', function ($query) {
                    $query->where('type', '=', 'transfer');
                })
                ->whereHas('detailkendaraan', function ($query) {
                    $query->where('status', '=', 'Transfer Private');
                })
                ->get();

            return view('Produk.Transfer.searchByHarga', compact('transfer', 'detail_transfer_private', 'regencyTo', 'dari', 'ke', 'tgl', 'waktu_ambil', 'metode_transfer', 'min_price', 'max_price'));
        } else {
            return redirect()->back();
        }
    }

    public function transferIndex(Request $request)
    {
        $regency_name = $request->query('name');

        $regencyTo  = Regency::all();

        $regencyFrom = DB::table('product as a')
            ->select(DB::raw('c.*'))
            ->join('product_detail as b', 'b.product_id', '=', 'a.id')
            ->join('regencies as c', 'c.id', '=', 'b.regency_id')
            ->where('name', $regency_name)
            ->orWhere('name', str()->upper($regency_name))
            ->distinct()
            ->get();

        $transfer =  DB::table('product as a')
            ->select('b.*', 'a.*', 'c.*', 'd.*')
            ->join('product_detail as b', 'b.product_id', '=', 'a.id')
            ->join('regencies as c', 'c.id', '=', 'b.regency_id')
            ->join('price as d', 'd.id', '=', 'b.harga_id', 'left')
            ->where('type', 'transfer')
            ->where('name', $regency_name)
            // ->orWhere('name',str()->upper($regency_name))
            ->paginate(5);

        return view('Produk.Transfer.new-index', compact('regencyTo', 'regencyFrom', 'transfer'));
    }

    public function getSelectFromToTour(){

        $turFromTo = Province::with('regencies')->get();

        return $turFromTo;
    }
}
