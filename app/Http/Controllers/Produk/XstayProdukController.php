<?php

namespace App\Http\Controllers\Produk;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Productdetail;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Paket;
// use App\Models\Itinenary;
use App\Models\Kategori;
use App\Models\Pilihan;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\Usertraveller;
use App\Models\reviewPost;
use App\Models\LikeProduct;
use App\Models\BookingOrder;
use App\Models\Attributes;
use App\Models\Masterkamar;
use Attribute;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use App\Models\PajakAdmin;
use App\Models\PajakLocal;
use App\Models\Detailpesanan;

class XstayProdukController extends Controller
{
    public function index()
    {
        $xstay = Product::with('productdetail')->has('productdetail')->where('type', 'xstay')->get();

        return view('Produk.Xstay.index', compact('xstay'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\newsletter $newsletter
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $slug)
    {
        $xstay = Product::where('slug', $slug)->with('productdetail')->first();
        $xstay_detail = $xstay->productdetail;
        $regency = Regency::where('id', $xstay->productdetail['regency_id'])->first();
        $pakets = Paket::where('id', $xstay->productdetail['paket_id'])->first();
        // $itinerary = Itinenary::where('id', $xstay->productdetail['itinenary'])->first();
        $harga = Price::where('id', $xstay->productdetail['harga_id'])->first();
        $diskon = Discount::where('id', $xstay->productdetail['discount_id'])->first();
        $pilihan = Pilihan::where('id', $xstay->productdetail['pilihan_id'])->first();
        // dd($diskon);

        $share = \Share::page('https://www.kamtuu-staging.cerise.id/xstay/' . $slug)->facebook()->whatsapp();
        $sebelumnya = json_decode($xstay_detail->kebijakan_pembatalan_sebelumnya)->sebelumnya;
        $sesudahnya = json_decode($xstay_detail->kebijakan_pembatalan_sesudah)->sesudah;
        $potongan = json_decode($xstay_detail->kebijakan_pembatalan_potongan)->potongan;
        $galleries = \collect([]);

        // Kamar
        $rooms = isset($xstay_detail->id_kamar) ? json_decode($xstay_detail->id_kamar, true)['result'] : [];
        $rooms_array = collect([]);
        $amenitas_room_array = collect([]);
        // $amenitas_room_array = [];

        $checkin = $request->query('checkIn') ?? date("m/d/Y", strtotime('today'));
        $checkout = $request->query('checkOut') ?? date("m/d/Y", strtotime('tomorrow'));
        $total_kmr = $request->query('total_kamar');
        $kmr = $request->query('kmr');
        $query_room = collect([]);
        $total_pengunjung = 0;
        $total_dewasa = 0;
        $total_anak = 0;
        $umur_anak = $request->query('anak');
        $checkin_format = isset($checkin) ? date("D, M j", strtotime($checkin)) : '';
        $nights = 0;
        
        // Get interval (nights)
        if (isset($checkin) && isset($checkout)) {
            $checkin1 = new DateTime($checkin);
            $checkout1 = new DateTime($checkout);
            $interval = $checkin1->diff($checkout1);
            $nights = $interval->format('%a');
        }
        
        if($total_kmr > 0 ){
            for($index_anak = 0; $index_anak < $total_kmr; $index_anak++){
                $dewasa = explode('_',$request->query('kamar')[$index_anak])[0];
                $anak   = explode('_',$request->query('kamar')[$index_anak])[1];
                $total = explode('_',$request->query('kamar')[$index_anak])[2];

                $total_pengunjung = $total_pengunjung + $total;
                $total_dewasa = $total_dewasa + $dewasa;
                $total_anak = $total_anak + $anak;

                $query_room->push([
                    'dewasa'=>intval($dewasa),
                    'anak'=>intval($anak)
                ]);
            }
        }

        // If total kamar IS NULL
        if ($total_kmr == null) {
            $total_kmr = 1;
            $total_pengunjung = 1;
            $total_dewasa = 1;
            $query_room->push([
                'dewasa' => 1,
                'anak' => 0,
            ]);
        }

        // dd($rooms);
        if($checkin != null && $checkout !=null && $total_kmr != null){
            for($x = 0; $x < count($rooms);$x++){
                // dd($diskon);
                if(empty(json_decode($diskon->tgl_start)) || json_decode($diskon->tgl_start)==null || empty(json_decode($diskon->tgl_end)) || json_decode($diskon->tgl_end)==null){
                    break;
                }
                
                $id_room = Masterkamar::where('id',$rooms[$x])->first();
                $reserved_room = Detailpesanan::where('kamar_id', $rooms[$x])->get();

                $room_available_stok = $id_room->stok - $reserved_room->count();

                if($id_room->stok < $total_kmr || $room_available_stok <= 0){
                    continue;
                }
                
                // filter amount of traveller to selected room
                for($search_index = 0; $search_index < $total_kmr;$search_index ++){
                    if($request->kmr == null){
                        $total_person =  1;

                        break;
                    }
                    // dd(count(json_decode($diskon->tgl_start)));
                    // dump($request->kamar[$search_index]);
                    $total_person = explode('_',$request->kamar[$search_index])[2];

                    if(!($total_person >= $id_room->kapasitas_minimum && $total_person <= $id_room->kapasitas_maksimum)){
                        continue 2;
                    }
                }

                // dd($diskon->tgl_start);
                // Check if checkin and checkout date are available
                for($available=0; $available < count(json_decode($diskon->tgl_start)); $available++){
                    
                    $start = date('Y-m-d',strtotime(json_decode($diskon->tgl_start)[$available]));
                    // dump('test');
                    // dump($start);
                    $end = date('Y-m-d',strtotime(json_decode($diskon->tgl_end)[$available]));
                    $in = date('Y-m-d',strtotime($checkin));
                    $out = date('Y-m-d',strtotime($checkout));

                    if(($in >= $start) && ($in < $end) && ($out > $start) && ($out <= $end)) {
                        break;
                    }
                    
                    // dd(count(json_decode($diskon->tgl_start)));
                    // dd($available);
                    // echo count(json_decode($diskon->tgl_start)) - 1;
                    /* if($available == (count(json_decode($diskon->tgl_start)) - 1)) {
                        break 2;
                    } */
                }

                // foreach
                $id_amenitas = isset($id_room->id_amenitas) ? json_decode($id_room->id_amenitas, true)['result'] : [];
                
                for($index=0; $index < count($id_amenitas);$index++){
                    $amenitas_detail = Attributes::where('id',$id_amenitas[$index])->first();
    
                    $amenitas_room_array->push([
                        'amenitas_id'=>$amenitas_detail->id,
                        'amenitas_name'=>$amenitas_detail->text,
                        'amenitas_img'=>Storage::url($amenitas_detail->image),
                    ]);
                }
    
                $id_room->amenitas = $amenitas_room_array;
                $id_room->room_available_stok = $room_available_stok;
                
                $rooms_array->push(
                    $id_room->toArray()
                );
            }
        }
        // dd($rooms_array);
        // for($i=0; $i < count($rooms); $i++){
            
        // }
        // $lowest_price = $harga->dewasa_residen <= $harga->dewasa_non_residen ? $harga->dewasa_residen : $harga->dewasa_non_residen;
        $lowest_price = 0;

        if (count(json_decode($xstay_detail['foto_maps_1'])) > 1 && isset($xstay_detail['foto_maps_1'])) {
            $galleries[0] = (json_decode($xstay_detail['foto_maps_1']))[0];
            $galleries[1] = (json_decode($xstay_detail['foto_maps_1']))[1];
            $galleries[2] = isset((json_decode($xstay_detail['foto_maps_1']))[2]) ? (json_decode($xstay_detail['foto_maps_1']))[2] : (json_decode($xstay_detail['foto_maps_1']))[0];

            if (isset((json_decode($xstay_detail['foto_maps_1']))[3]) && isset((json_decode($xstay_detail['foto_maps_1']))[4])) {
                $galleries[3] = (json_decode($xstay_detail['foto_maps_1']))[3];
                $galleries[4] = (json_decode($xstay_detail['foto_maps_1']))[4];
            } elseif (isset((json_decode($xstay_detail['foto_maps_1']))[3])) {
                $galleries[3] = (json_decode($xstay_detail['foto_maps_1']))[3];
                $galleries[4] = $galleries[0];
            } elseif ($galleries[2] != $galleries[0]) {
                $galleries[3] = $galleries[0];
                $galleries[4] = $galleries[1];
            } else {
                $galleries[3] = $galleries[1];
                $galleries[4] = $galleries[0];
            }
        }

        $amenitas = isset($xstay_detail->id_amenitas) ? json_decode($xstay_detail->id_amenitas, true)['result'] : [];
        $amenitas_array = collect([]);

        for($i=0; $i < count($amenitas); $i++){
            $amenitas_detail = Attributes::where('id',$amenitas[$i])->first();

            $amenitas_array->push([
                'amenitas_id'=>$amenitas_detail->id,
                'amenitas_name'=>$amenitas_detail->text,
                'amenitas_img'=>Storage::url($amenitas_detail->image),
            ]);
        }

        $fasilitas = isset($xstay_detail->id_fasilitas) ? json_decode($xstay_detail->id_fasilitas, true)['result'] : [];
        $fasilitas_array = collect([]);

        for($i=0; $i < count($amenitas); $i++){
            $fasilitas_detail = Attributes::where('id',$fasilitas[$i])->first();

            $fasilitas_array->push([
                'fasilitas_id'=>$fasilitas_detail->id,
                'fasilitas_name'=>$fasilitas_detail->text,
                'fasilitas_img'=>Storage::url($fasilitas_detail->image),
            ]);
        }

        // Pajak
        $pajak = 0;
        $cek_pajak_local = PajakLocal::where('user_id', $xstay->user_id)->where('produk_type', 'xstay')->first();

        // Check if pajak local ISSET
        if (isset($cek_pajak_local)) {
            $pajak = $cek_pajak_local->markupLocal;
        }

        // Check if pajak admin ISSET AND pajak local IS NOT SET
        if (!isset($cek_pajak_local)) {
            $cek_pajak_Global = PajakAdmin::where('user_id', 1)->first();

            if (isset($cek_pajak_Global)) {
                $pajak = $cek_pajak_Global->markup;
            }
        }
        
        // $card_activity = Product::where('type', 'activity')->has('productdetail')->with('tagged', 'productdetail.diskon', 'productdetail.harga', 'productdetail.masterkamar', 'productdetail.detailkendaraan', 'productdetail.regency', 'user')->limit(5)->inRandomOrder()->get();

        $reviews = reviewPost::leftJoin('product', 'product.id', '=', 'review_posts.product_id')->join('users', 'users.id', '=', 'review_posts.traveller_id')->where('review_posts.status','active')->where('review_posts.status','active')->where('slug', $slug)->get();
        $counts = reviewPost::leftJoin('product', 'product.id', '=', 'review_posts.product_id')->join('users', 'users.id', '=', 'review_posts.traveller_id')->where('review_posts.status','active')->where('slug', $slug)
            ->select('review_posts.star_rating')->get();
        $var1 = $counts->sum('star_rating');
        $var2 = $counts->count();

        $booking_count = BookingOrder::where('product_detail_id', $xstay_detail->id)->get()->count();
        $like_count = LikeProduct::where('product_id', $xstay->id)->get()->count();

        $like = 0;
        if (Auth::user()) {
            $isLike = LikeProduct::where('product_id', $xstay->id)->where('user_id', Auth::user()->id)->first();
            $like = isset($isLike) ? $isLike->status : 0;
        }

        $ratings = $var1 == 0 ? 0 : $var1 / $var2;

        // dd(json_decode($xstay_detail->icon_fasilitas)->result);

        return view('Produk.Xstay.detail', compact('xstay', 'xstay_detail', 'regency', 
        'pakets', 'harga', 'diskon', 'pilihan', 'share', 'galleries', 'sebelumnya', 
        'sesudahnya', 'potongan', 'lowest_price', 'reviews', 'counts', 'var1', 'var2', 'booking_count',
        'like_count', 'like', 'ratings','amenitas_array','fasilitas_array','query_room','total_pengunjung','amenitas_room_array','rooms_array','umur_anak','slug','checkin','checkout','checkin_format', 'total_kmr', 'nights', 'total_dewasa', 'total_anak', 'pajak'));
    }

    public function proses(Request $request)
    {
        $room = Masterkamar::where('id', $request->selected_room_id)->first();

        $refundable = $room->refundable[$request->index];
        $refundable_amount = $room->refundable_amount[$request->index];
        $refundable_price = $room->refundable_price[$request->index];
        $foto_kamar = json_decode($request->selected_room)->foto_kamar;
        $room_discount_by_date = 0;
        $diskon = Discount::where('id', $request->diskon_id)->first();
        $diskon_by_date = 0;
        $diskon_by_group = 0;
        
        // dd(!empty(json_decode($diskon->min_orang)->min_orang), !empty(json_decode($diskon->max_orang)->max_orang), !empty(json_decode($diskon->diskon_orang)->diskon_orang));
        if (isset($diskon->tgl_start) && isset($diskon->tgl_end) && isset($diskon->discount)) {
            for ($available_index = 0; $available_index < count(json_decode($diskon->tgl_start)); $available_index++) {
                $start = date('Y-m-d', strtotime(json_decode($diskon->tgl_start)[$available_index]));
                $end = date('Y-m-d', strtotime(json_decode($diskon->tgl_end)[$available_index]));
                $in = date('Y-m-d', strtotime($request->check_in_date));
                $out = date('Y-m-d', strtotime($request->check_out_date));
                
                // If checkin and checkout date IS BETWEEN start and end date, break from current loop
                if(($in >= $start) && ($in < $end) && ($out > $start) && ($out <= $end)) {
                    $diskon_by_date = json_decode($diskon->discount)[$available_index];
                    break;
                }
            }
        }

        if (!empty(json_decode($diskon->min_orang)->min_orang) && !empty(json_decode($diskon->max_orang)->max_orang) && !empty(json_decode($diskon->diskon_orang)->diskon_orang)) {
            foreach (json_decode($diskon->min_orang)->min_orang as $key => $value) {
    
                $max_orang = json_decode($diskon->max_orang)->max_orang[$key];
                $diskon_orang = json_decode($diskon->diskon_orang)->diskon_orang[$key];
    
                if ($request->total_pengunjung >= $value && $request->total_pengunjung <= $max_orang) {
                    $diskon_by_group = $diskon_by_group < $diskon_orang ? $diskon_orang : $diskon_by_group;
                }
            }
        }

        $detail_xstay = json_decode($request->cookie('detail_xstay'), true);
        $detail_xstay = [
            'product_name' => $request->product_name,
            'total' => $request->total,
            'type' => $request->type,
            'toko_id' => $request->toko_id,
            'product_detail_id' => $request->product_detail_id,

            'check_in_date' => Carbon::parse($request->check_in_date)->translatedFormat('Y-m-d'),
            'check_out_date' => Carbon::parse($request->check_out_date)->translatedFormat('Y-m-d'),
            'room_name' => $request->selected_room_name,
            'room_id' => $request->selected_room_id,
            'room_total' => $request->total_kmr,
            'refundable' => $refundable,
            'refundable_amount' => $refundable_amount,
            'refundable_price' => $refundable_price,
            'reviews' => $request->reviews,
            'ratings' => $request->ratings,
            'foto_kamar' => json_decode($foto_kamar, true)['result'][0] ?? '',
            'rooms' => $request->room,
            'nama_ekstra' => $request->hotelExtraName ?? 'Tidak Ada Ekstra',
            'harga_ekstra' => $request->hotelExtra ?? 0,
            'total_pengunjung' => $request->total_pengunjung ?? 0,
            'total_harga_kamar' => $request->total_harga_kamar ?? 0,
            'diskon_by_date' => $diskon_by_date,
            'diskon_by_group' => $diskon_by_group,
            'total_harga_ekstra' => $request->total_harga_ekstra,
            'total_tax' => $request->total_tax,
            'total_harga_kamar_refund' => $request->total ?? 0,
            'total_dewasa' => $request->total_dewasa ?? 0,
            'total_anak' => $request->total_anak ?? 0,
            'pajak' => $request->pajak ?? 0,
            
            // 'peserta_dewasa' => $request->dewasa,
            // 'peserta_anak_anak' => $request->anak_anak,
            // 'peserta_balita' => $request->balita,
            'ekstra_sarapan' => $request->ekstra_sarapan,
            'diskon' => $request->diskon,
        ];
        // dd($detail);

        $data = $request->all();
        $request->session()->put('order.catatan_extra', isset($data['catatan_ekstra']) ? $data['catatan_ekstra'] : '');
        $request->session()->put('order.extra', isset($data['extra']) ? $data['extra'] : []);
        $cookie = cookie('detail_xstay', json_encode($detail_xstay), 2880);

        return redirect()->route('xstay.detailPesanan')->withCookie($cookie);
    }

    public function detailPesanan(Request $request)
    {
        if(request()->cookie('detail_xstay') == null){
            return redirect()->back();
        }
        $order = $request->session()->get('order');
        $detail = json_decode(request()->cookie('detail_xstay'), true);
        $code = mt_rand(1, 99) . date('dmY');
        $user_id = Auth::user()->id;
        $profile = Usertraveller::where('user_id', $user_id)->first();
        $agent_id = User::where('role', 'agent')->where('id', $user_id)->first();

        return view('Produk.Xstay.detail-pemesanan', compact('profile', 'detail', 'code', 'agent_id', 'order'));
    }

    public function search(Request $request)
    {
        $data = $request->search;

        if ($data) {
            $xstay = Product::with('productdetail')->where('type', 'xstay')->get();
            $data_detail = Productdetail::with('product')->whereHas('product', function ($query) {
                $query->where('type', '=', 'xstay');
            })
                ->where('regency_id', 'LIKE', "%" . $data . "%")
                ->get();

            return view('Produk.Xstay.search', compact('data_detail', 'xstay', 'data'));
        } else {
            return redirect()->back();
        }
    }

    public function searchByPrice(Request $request)
    {
        $min_price = $request->min_price;
        $max_price = $request->max_price;
        $data = $request->search;

        if ($min_price && $max_price && $data) {
            $xstay = Product::with('productdetail')->where('type', 'xstay')->get();
            $data_detail = Productdetail::with(['product', 'masterkamar' => function ($query) use ($min_price, $max_price) {
                $query->whereBetween('harga_kamar', [$min_price, $max_price]);
            }])
                ->whereHas('product', function ($query) {
                    $query->where('type', '=', 'xstay');
                })
                ->where('regency_id', 'LIKE', "%" . $data . "%")
                ->get();

            return view('Produk.Xstay.searchByPrice', compact('data_detail', 'xstay', 'min_price', 'max_price', 'data'));
        } else {
            return redirect()->back();
        }
    }

    public function xstayIndex(Request $request){
        $regency_name = $request->query('name');
        $regencyTo  = Regency::all();

        $regencyFrom = DB::table('product as a')
            ->select(DB::raw('c.*'))
            ->join('product_detail as b', 'b.product_id', '=', 'a.id')
            ->join('regencies as c', 'c.id', '=', 'b.regency_id')
            ->where('name', $regency_name)
            ->orWhere('name', str()->upper($regency_name))
            ->distinct()
            ->get();

        $xstay =  DB::table('product as a')
            ->select('b.*', 'a.*', 'c.*', 'd.*')
            ->join('product_detail as b', 'b.product_id', '=', 'a.id')
            ->join('regencies as c', 'c.id', '=', 'b.regency_id')
            ->join('price as d', 'd.id', '=', 'b.harga_id', 'left')
            ->where('name', $regency_name)
            ->where('type', 'xstay')
            // ->orWhere('name',str()->upper($regency_name))
            ->paginate(5);

        return view('Produk.Xstay.new-index', compact('xstay', 'regencyFrom'));
    }
}
