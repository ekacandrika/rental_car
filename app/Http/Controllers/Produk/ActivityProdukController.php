<?php

namespace App\Http\Controllers\Produk;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Usertraveller;
use App\Models\Userdetail;
use App\Models\Productdetail;
use App\Models\DetailPemesanan;
use App\Models\Detailpesanan;
use App\Models\Product;
use App\Models\Discount;
use App\Models\Hotel;
use App\Models\Price;
use App\Models\Paket;
use App\Models\Extra;
use App\Models\Kategori;
use App\Models\Pilihan;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use App\Models\reviewPost;
use App\Models\LikeProduct;
use App\Models\BookingOrder;
use App\Models\Kelolatoko;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ActivityProdukController extends Controller
{

    public function index()
    {

        $provinsi = Province::all();
        $activity = Product::with('productdetail')->where('type', 'activity')->get();

        return view('Produk.Activity.index', compact('activity'));
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $activity_name = $request->activity_name;


        if ($search != null && $activity_name == null) {
            $activity = Product::with('productdetail')->where('type', 'activity')->get();
            $regencyTo = $this->getSelectFromToTour();
            $data_detail = Productdetail::with('product')
                ->whereHas('product', function ($query) {
                    $query->where('type', '=', 'activity');
                })
                ->where('regency_id', 'LIKE', "%" . $search . "%")
                ->get();

            return view('Produk.Activity.search', compact('activity', 'data_detail', 'search', 'regencyTo', 'activity_name'));
        } elseif ($activity_name != null && $search == null) {
            $activity = Product::with('productdetail')->where('type', 'activity')->get();
            $regencyTo = $this->getSelectFromToTour();
            $data_activity = Product::with('productdetail')->where('product_name', 'LIKE', "%" . $activity_name . "%")
                ->where('type', 'activity')
                ->get();

            return view('Produk.Activity.search', compact('activity', 'data_activity', 'activity_name', 'search', 'regencyTo',));
        } elseif ($search != null && $activity_name != null) {
            $activity = Product::with('productdetail')->where('type', 'activity')->get();
            $regencyTo = $this->getSelectFromToTour();
            $data_activity = Product::with('productdetail')
                ->whereHas('productdetail', function ($query) use ($search) {
                    $query->where('regency_id', 'LIKE', "%" . $search . "%");
                })
                ->where('product_name', 'LIKE', "%" . $activity_name . "%")
                ->where('type', 'activity')
                ->get();

            return view('Produk.Activity.search.search-full', compact('activity', 'data_activity', 'search', 'regencyTo', 'activity_name'));
        } else {
            return redirect()->back();
        }
    }

    public function searchByHarga(Request $request)
    {
        $search = $request->search;
        $activity_name = $request->activity_name;
        $min_price = $request->min_price;
        $max_price = $request->max_price;
        $regencyTo = $this->getSelectFromToTour();

        if ($search != null && $activity_name == null && $min_price && $max_price) {
            $activity = Product::with('productdetail')->where('type', 'activity')->get();
            $data_detail = Productdetail::with('product')
                ->whereHas('product', function ($query) {
                    $query->where('type', '=', 'activity');
                })
                ->where('regency_id', 'LIKE', "%" . $search . "%")
                ->get();

            return view('Produk.Activity.searchByHarga', compact('activity', 'data_detail', 'search', 'regencyTo', 'activity_name', 'min_price', 'max_price'));
        } elseif ($activity_name != null && $search == null && $min_price && $max_price) {
            $activity = Product::with('productdetail')->where('type', 'activity')->get();
            $data_activity = Product::with('productdetail')->where('product_name', 'LIKE', "%" . $activity_name . "%")
                ->where('type', 'activity')
                ->get();

            return view('Produk.Activity.searchByHarga', compact('activity', 'data_activity', 'activity_name', 'search', 'regencyTo', 'min_price', 'max_price'));
        } elseif ($search != null && $activity_name != null && $min_price && $max_price) {
            $activity = Product::with('productdetail')->where('type', 'activity')->get();
            $data_activity = Product::with('productdetail')
                ->whereHas('productdetail', function ($query) use ($search) {
                    $query->where('regency_id', 'LIKE', "%" . $search . "%");
                })
                ->where('product_name', 'LIKE', "%" . $activity_name . "%")
                ->where('type', 'activity')
                ->get();

            return view('Produk.Activity.searchByHarga', compact('activity', 'data_activity', 'search', 'regencyTo', 'activity_name', 'min_price', 'max_price'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\newsletter $newsletter
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $activity = Product::where('slug', $slug)->with('productdetail')->first();
        $activity_detail = $activity->productdetail;
        $regency = Regency::where('id', $activity->productdetail['tag_location_3'])->first();
        $provinsi = Province::where('id', $activity->productdetail['tag_location_2'])->first();
        $pakets = Paket::where('id', $activity->productdetail['paket_id'])->first();
        $harga = Price::where('id', $activity->productdetail['harga_id'])->first();
        $diskon = Discount::where('id', $activity->productdetail['discount_id'])->first();
        $pilihan = Pilihan::where('id', $activity->productdetail['pilihan_id'])->first();
        $share = \Share::page('https://www.kamtuu-staging.cerise.id/activity/' . $slug)->facebook()->whatsapp();

        $galleries = \collect([]);

        $info_toko = KelolaToko::where('id_seller',$activity->user->id)->first();

        if (count(json_decode($activity_detail['gallery'])) > 1) {
            $galleries[0] = (json_decode($activity_detail['gallery']))[0];
            $galleries[1] = (json_decode($activity_detail['gallery']))[1];
            $galleries[2] = isset((json_decode($activity_detail['gallery']))[2]) ? (json_decode($activity_detail['gallery']))[2] : (json_decode($activity_detail['gallery']))[0];

            if (isset((json_decode($activity_detail['gallery']))[3]) && isset((json_decode($activity_detail['gallery']))[4])) {
                $galleries[3] = (json_decode($activity_detail['gallery']))[3];
                $galleries[4] = (json_decode($activity_detail['gallery']))[4];
            } elseif (isset((json_decode($activity_detail['gallery']))[3])) {
                $galleries[3] = (json_decode($activity_detail['gallery']))[3];
                $galleries[4] = $galleries[0];
            } elseif ($galleries[2] != $galleries[0]) {
                $galleries[3] = $galleries[0];
                $galleries[4] = $galleries[1];
            } else {
                $galleries[3] = $galleries[1];
                $galleries[4] = $galleries[0];
            }
        }

        $card_activity = Product::where('type', 'activity')->has('productdetail')->with('tagged', 'productdetail.diskon', 'productdetail.harga', 'productdetail.masterkamar', 'productdetail.detailkendaraan', 'productdetail.regency', 'user')->limit(5)->inRandomOrder()->get();

        $reviews = reviewPost::leftJoin('product', 'product.id', '=', 'review_posts.product_id')->join('users', 'users.id', '=', 'review_posts.traveller_id')->where('review_posts.status','active')->where('review_posts.status','active')->where('slug', $slug)->get();
        $counts = reviewPost::leftJoin('product', 'product.id', '=', 'review_posts.product_id')->join('users', 'users.id', '=', 'review_posts.traveller_id')->where('review_posts.status','active')->where('slug', $slug)
            ->select('review_posts.star_rating')->get();
        $var1 = $counts->sum('star_rating');
        $var2 = $counts->count();

        $booking_count = BookingOrder::where('product_detail_id', $activity_detail->id)->get()->count();
        $like_count = LikeProduct::where('product_id', $activity->id)->get()->count();

        $like = 0;
        if (Auth::user()) {
            $isLike = LikeProduct::where('product_id', $activity->id)->where('user_id', Auth::user()->id)->first();
            $like = isset($isLike) ? $isLike->status : 0;
        }

        $ratings = $var1 == 0 ? 0 : $var1 / $var2;

        return view('Produk.Activity.detail', compact(
            'activity',
            'activity_detail',
            'provinsi',
            'pakets',
            'harga',
            'diskon',
            'pilihan',
            'share',
            'galleries',
            'card_activity',
            'reviews',
            'counts',
            'var1',
            'var2',
            'booking_count',
            'like_count',
            'like',
            'ratings',
            'regency',
            'info_toko'
        ));
    }

    public function activityOrder(Request $request, $slug)
    {
        $data = $request->all();

        $request->session()->forget('order');

        $code = mt_rand(1, 99) . date('dmY');
        $request->session()->put('order.booking_code', $code);
        $request->session()->put('order.product_name', $data['product_name']);
        $request->session()->put('order.product_id', $data['product_id']);

        $request->session()->put('order.toko_id', $data['toko_id']);
        $request->session()->put('order.product_detail_id', $data['product_detail_id']);
        $request->session()->put('order.type', $data['type']);

        $request->session()->put('order.paket_id', $data['paket_id']);

        $request->session()->put('order.pilihan_id', $data['pilihan_id']);
        $request->session()->put('order.activity_date', $data['activity_date']);

        $request->session()->put('order.paket.index', $data['paket']['index']);
        $request->session()->put('order.paket.nama_paket', $data['paket']['nama_paket']);
        $request->session()->put('order.paket.waktu', $data['paket']['waktu']);

        $request->session()->put('order.pilihan', $data['pilihan'] ?? []);
        $request->session()->put('order.catatan_extra', isset($data['catatan_ekstra']) ? $data['catatan_ekstra'] : '');
        $request->session()->put('order.extra', isset($data['extra']) ? $data['extra'] : []);
        $request->session()->put('order.dewasa_count', (int) $data['dewasa_count']);
        $request->session()->put('order.anak_count', (int) $data['anak_count']);
        $request->session()->put('order.balita_count', (int) $data['balita_count']);

        // return redirect()->route('activity.viewToOrder', [$slug]);
        // return view('Produk.Activity.detail-pemesanan', compact('slug', 'data'));
        if (Auth::user() != null) {
            return redirect()->route('activity.viewToOrder', [$slug]);
        } else {
            return redirect()->route('LoginTraveller');
        }
    }


    public function viewToOrder(Request $request, $slug)
    {

        $order = $request->session()->get('order');

        if ($order == null || Auth::user() == null) {
            return redirect()->back();
        }

        $seller = Product::where('slug', $slug)->with('productdetail', 'user', 'productdetail.diskon')->first();
        $harga = Price::where('id', $seller->productdetail->harga_id)->first();
        $data_booking = Usertraveller::where('user_id', Auth::user()->id)->first();

        // if(!isset($data_booking)){
        //     $data_booking = Userdetail::where('user_id', Auth::user()->id)->first();
        // }

        $get_total_people = $request->session()->get('order.dewasa_count');
        $get_date = $request->session()->get('order.activity_date');
        $formated_date = Carbon::parse($get_date)->format('d F Y');
        $diskon_tanggal = 0;
        $diskon_group = 0;

        $total_pilihan = 0;
        $total_extra = 0;

        if (!isset($order['pilihan'])) {
            foreach ($order['pilihan'] as $key => $value) {
                $total_pilihan = $total_pilihan + ($value['jumlah'] * $value['harga']);
            }
        }

        if (!isset($order['extra'])) {
            foreach ($order['extra'] as $key => $value) {
                $total_extra = $total_extra + ($value['jumlah_extra'] * $value['harga_extra']);
            }
        }

        foreach (json_decode($seller->productdetail->diskon->tgl_start) as $key => $value) {
            $tgl_start = Carbon::parse($value)->toDateTimeString();
            $tgl_end = Carbon::parse(json_decode($seller->productdetail->diskon->tgl_end)[$key])->toDateTimeString();
            $check = Carbon::parse($get_date)->between($tgl_start, $tgl_end);

            if ($check) {
                $diskon_tanggal = json_decode($seller->productdetail->diskon->discount)[$key];
                break;
            }
        }

        foreach (json_decode($seller->productdetail->diskon->min_orang) as $key => $value) {

            $max_orang = json_decode($seller->productdetail->diskon->max_orang)[$key];
            $diskon_orang = json_decode($seller->productdetail->diskon->diskon_orang)[$key];

            if ($get_total_people >= $value && $get_total_people <= $max_orang) {
                $diskon_group = $diskon_group < $diskon_orang ? $diskon_orang : $diskon_group;
            }
        }

        return view('Produk.Activity.detail-pemesanan', compact(
            'slug',
            'order',
            'seller',
            'harga',
            'diskon_tanggal',
            'diskon_group',
            'total_pilihan',
            'total_extra',
            'data_booking',
            'formated_date'
        ));
    }

    public function addToOrder(Request $request)
    {
        $data = $request->all();
        // dd($request->session(), $data);
        // dd(Auth::user()->role);

        $phone_number_order = collect([]);
        $ID_card_photo_order = collect([]);

        foreach ($data['phone_number'] as $key => $value) {
            if ($data['nationality'][$key] == 'indonesia') {
                $phone_number_order->push('62' . $value);
            } else {
                $phone_number_order->push($value);
            }
        }

        if ($request->hasFile('kitas')) {
            $image = $request->file('kitas');

            foreach ($image as $key => $value) {

                $new_foto = time() . 'KitasPesertaOrder' . $value->getClientOriginalName();
                $tujuan_upload = 'Order/Peserta/Kitas/';

                $value->move($tujuan_upload, $new_foto);
                $images_gallery = $tujuan_upload . $new_foto;

                $ID_card_photo_order->push(
                    $images_gallery,
                );
            }
        }

        $index_pilihan = collect([]);
        $jumlah_pilihan = collect([]);

        $extra_id = collect([]);
        $catatan_per_ekstra = collect([]);
        $jumlah_ekstra = collect([]);

        foreach ($request->session()->get('order.pilihan') as $key => $value) {
            if (isset($value['index'])) {
                $index_pilihan->push($value['index']);
                $jumlah_pilihan->push($value['jumlah']);
            }
        }

        foreach ($request->session()->get('order.extra') as $key => $value) {
            // dd($value['id_extra']);
            $index = '';
            if (isset($value['id_extra'])) {
                $index = $value['id_extra'];
            }
            // $index = $value;
            // dd($index);
            $extra_id->push($index);
            $jumlah_ekstra->push($value['jumlah_extra']);
            $catatan_per_ekstra->push($value['note_extra']);
        }

        $detailpemesanan = Detailpesanan::create([
            'customer_id' => Auth::user()->id,
            'product_id' => $request->session()->get('order.product_id'),
            // 'total_harga' => $data['total_price'],

            'paket_id' => $request->session()->get('order.paket_id'),
            'paket_index' => $request->session()->get('order.paket.index'),

            // 'order_code' => $request->session()->get('order.order_code'),
            'tanggal_kegiatan' => $request->session()->get('order.activity_date'),

            'pilihan_id' => $request->session()->get('order.pilihan_id'),
            'index_pilihan' => json_encode($index_pilihan),
            'jumlah_pilihan' => json_encode($jumlah_pilihan),

            'extras_id' => json_encode($extra_id),
            'catatan_umum_extra' => $request->session()->get('order.catatan_extra'),
            'catatan_per_extra' => json_encode($catatan_per_ekstra),
            'jumlah_extra' => json_encode($jumlah_ekstra),

            'kategori_peserta' => json_encode($request->kategori_peserta),
            'first_name' => json_encode($data['first_name']),
            'last_name' => json_encode($data['last_name']),
            'date_of_birth' => json_encode($data['birth_date']),
            'jk' => json_encode($data['jenis_kelamin']),
            'status_residen' => json_encode($data['residen_status']),
            'citizenship' => json_encode($data['nationality']),
            // 'country_code_order' => json_encode($data['country_code']),
            'phone_number_order' => json_encode($phone_number_order),
            'email_order' => json_encode($data['email']),
            'ID_card_photo_order' => json_encode($ID_card_photo_order),
        ]);

        $order = $request->session()->forget('order');
    }

    public function activityIndex(Request $request)
    {
        $regency_name = $request->query('name');

        $regencyTo  = Regency::all();

        $regencyFrom = DB::table('product as a')
            ->select(DB::raw('c.*'))
            ->join('product_detail as b', 'b.product_id', '=', 'a.id')
            ->join('regencies as c', 'c.id', '=', 'b.regency_id')
            ->where('name', $regency_name)
            ->orWhere('name', str()->upper($regency_name))
            ->distinct()
            ->get();

        $activity =  DB::table('product as a')
            ->select('b.*', 'a.*', 'c.*', 'd.*')
            ->join('product_detail as b', 'b.product_id', '=', 'a.id')
            ->join('regencies as c', 'c.id', '=', 'b.regency_id')
            ->join('price as d', 'd.id', '=', 'b.harga_id', 'left')
            ->where('name', $regency_name)
            ->where('type', 'activity')
            // ->orWhere('name',str()->upper($regency_name))
            ->paginate(5);
        // return view('Hotel.new-index', compact('hotel'));
        return view('Produk.Activity.new-index', compact('activity', 'regencyFrom'));
    }

    public function getSelectFromToTour()
    {

        $turFromTo = Province::with('regencies')->get();

        return $turFromTo;
    }
}
