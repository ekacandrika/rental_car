<?php

namespace App\Http\Controllers;

use Svg\Tag\Rect;
use App\Models\User;
use App\Models\Userdetail;
use App\Models\Usertraveller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class SosialiteController extends Controller
{
    public function redirect(Request $request, $provider){
        $request->session()->put('role', $request->query('role'));
        return Socialite::driver($provider)->redirect();
    }

    public function handleCallback(Request $request, $provider){
        try {
            $user = Socialite::driver($provider)->stateless()->user();

        } catch (\Exception $e) {
            return redirect()->back();
        }
        
        $role =  $request->session()->get('role');
        $userLogin = $this->userHandle($role, $user, $provider);

        Auth()->login($userLogin, true);
        
        if($role=='traveller'){
            session()->forget(['role']);
            return redirect()->to('/dashboard/traveller');
        }elseif($role=='seller'){
            session()->forget(['role']);
            return redirect()->route('dashboardseller');
        }elseif($role=='agent'){
            session()->forget(['role']);
            return redirect()->route('dashboardagent');
        }else{
            session()->forget(['role']);
            return redirect()->to('/dashboard/admin/kamtuu');
        }
    }

    public function userHandle($role, $account, $provider){
        
        $id = $account->getId();
        $email = $account->getEmail();
        $name = $account->getName();
       
        $user = User::where('email',$email)->first();

        if(!$user){
            $user = User::create([
                'first_name'=>$name,
                'email'=>$email,
                'jenis_registrasi'=>null,
                'role'=>$role,
                'lisensi'=>null,
                'email_verified_at'=>carbon::now(),
                'no_tlp'=>$this->genRandomNoTlp()
            ]);
        }

        $user_id = $user->id;

        $user_detail = Userdetail::where('user_id',$user_id)->first();
        $user_name = explode(" ",$name); 

        if(!$user_detail){
            Userdetail::create([
                'user_id'=>$user_id,
                'last_name'=>end($user_name)
            ]);

            $this->sendEmailNotify();
        }

        if($role == 'traveller'){
            $user_traveller = Usertraveller::where('user_id',$user_id)->first();

            if(!$user_traveller){
                Usertraveller::create([
                    'user_id'=>$user_id,
                    'last_name'=>end($user_name),
                ]);
            }
        }

        return $user;
    }

    private function sendEmailNotify(){
        
        Mail::send('BE.admin.email-notif',['text'=>null],function($message){
            $message->to('ekacandrika@gmail.com');
            $message->subject('User register notification');
        });
    }

    private function genRandomNoTlp(){
        $requiredLength = 7;
        $highestDigit = 8;

        $sequence = '';
        $numberPrefixes = '0812';

        for ($i = 0; $i < $requiredLength; ++$i) {
            $sequence .= mt_rand(0, $highestDigit);
        }
        
        return $numberPrefixes.$sequence;
        
    }
}
