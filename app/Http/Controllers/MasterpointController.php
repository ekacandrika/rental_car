<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use App\Models\Masterpoint;
use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use App\Models\Village;

class MasterpointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinsi = Province::all();

        return view('BE.admin.master-titik',compact('provinsi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $point = new Masterpoint;
        foreach($request->district_id as $key => $district){
            $point::create([
                'district_id'=>$request->district_id[$key]['value'],
                'name_titik'=>$request->point_name[$key]['value'],
                'created_at'=>Carbon::now()
            ]);
        }

        return response()->json([
            'success'=>true,
            'message'=>'Titik rute berhasil ditambahkan'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = Masterpoint::findOrFail($id);

        $district_data = District::where('id',$row->district_id)->first();
        $regency_data = Regency::where('id',$district_data->regency_id)->first();
        $provinsi_data = Province::where('id',$regency_data->province_id)->first();

        $districts = District::All();
        $regencies = Regency::All();
        $provinces = Province::All();

        return view('BE.admin.master-titik-edit',compact('provinces','regencies','districts', 'provinsi_data','regency_data','district_data','row','id'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $row = Masterpoint::findOrFail($id);

        $row->update([
            'district_id'=>$request->district_id,
            'name_titik'=>$request->name_titik
        ]);

        return redirect()->route('master-titik.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $row = Masterpoint::findOrFail($id);

        $row->destroy($id);
        return redirect()->route('master-titik.index');
    }
        /**
     * Clone the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clone($id)
    {
        $row = Masterpoint::findOrFail($id);

        $row_new = $row->replicate();
        $row_new->created_at = Carbon::now();

        $row_new->save();

        return redirect()->route('master-titik.index');
    }

    /**
    * api get Kota/Kabupaten
    * 
    * @param int id provisi 
    * @return \Illuminate\Http\Response
    */
    public function getKabupaten($id){
        $kabupaten = Regency::where('province_id',$id)->get();

        $option = '<option>-Pilih Kota/Kabupaten-</option>';
        foreach ($kabupaten as $key => $value) {
            # code...
            $option .='<option value="'.$value->id.'">'.$value->name.'</option>';
        }

        echo $option;
    }

    /**
    * api get Kota/Kabupaten
    * 
    * @param int id provisi 
    * @return \Illuminate\Http\Response
    */
    public function getKecamatan($id){
        $kecamatan = District::where('regency_id',$id)->get();

        $option = '<option>-Pilih Kecamatan-</option>';
        foreach ($kecamatan as $key => $value) {
            # code...
            $option .='<option value="'.$value->id.'" data-name="'.$value->name.'">'.$value->name.'</option>';
        }

        echo $option;
    }

    public function getKelurahan($id){
        $kecamatan = District::where('regency_id',$id)->get();

        $option = '<option>-Pilih Kecamatan-</option>';
        foreach ($kecamatan as $key => $value) {
            # code...
            $option .='<option value="'.$value->id.'" data-name="'.$value->name.'">'.$value->name.'</option>';
        }

        echo $option;
    }

    public function getDesa($id){
        $kecamatan = Village::where('district_id',$id)->get();

        $option = '<option>-Pilih Kelurahan-</option>';
        foreach ($kecamatan as $key => $value) {
            # code...
            $option .='<option value="'.$value->id.'" data-name="'.$value->name.'">'.$value->name.'</option>';
        }

        echo $option;
    }

    public function search(Request $request){

        $nama_titik = $request->search;

        $datas = Masterpoint::select('titik_route.id','titik_route.name_titik','districts.name')
        ->join('districts','districts.id','=','titik_route.district_id')
        ->where('titik_route.name_titik','like','%'.str()->upper($nama_titik).'%')
        ->orWhere('titik_route.name_titik','like','%'.str()->ucfirst($nama_titik).'%')
        ->orWhere('titik_route.name_titik','like','%'.str()->title($nama_titik).'%')
        ->orWhere('titik_route.name_titik','like','%'.str()->lower($nama_titik).'%')
        ->orWhere('districts.name','like','%'.str()->upper($nama_titik).'%')
        ->orWhere('districts.name','like','%'.str()->ucfirst($nama_titik).'%')
        ->orWhere('districts.name','like','%'.str()->title($nama_titik).'%')
        ->orWhere('districts.name','like','%'.str()->lower($nama_titik).'%')
        ->get(); 

        // dd($datas);
        /* $datas = District::with('masterpoint')
        ->whereHas('masterpoint',function($query) use ($nama_titik){
            $query->whereNotNull('district_id');
            // ->orWhere('name_titik','like','%'.str()->upper($nama_titik).'%')
            // ->orWhere('name_titik','like','%'.str()->ucfirst($nama_titik).'%')
            // ->orWhere('name_titik','like','%'.str()->title($nama_titik).'%')
            // ->orWhere('name_titik','like','%'.str()->lower($nama_titik).'%')
            ;
        })
        ->where('name','like','%'.str()->upper($nama_titik).'%')
        ->where('name','like','%'.str()->ucfirst($nama_titik).'%')
        ->where('name','like','%'.str()->title($nama_titik).'%')
        ->where('name','like','%'.str()->lower($nama_titik).'%')
        // ->limit(15)
        ->get(); */
        
        $data_rows = collect([]);

        foreach($datas as $key =>$data){

            $data_rows->push([
                'id'=>$data->id,
                'name'=>$data->name,
                'name_titik'=>$data->name_titik,
            ]);

        }

        $per_page = 10;
        $total = count($data_rows);
        $current_page = $request->input('page') ?? 1;
        $start_point = ($current_page * $per_page) - $per_page;

        $data_rows = $data_rows->toArray();
        $data_rows = array_slice($data_rows, $start_point, $per_page, true);
        $data_rows = New Paginator($data_rows, $total, $per_page, $current_page,[
            'page'=>$request->url(),
            'page'=>$request->query(),
        ]);

        return response()->json([
            'success'=>true,
            'datas'=>$data_rows
        ]);
    }

    public function datas(Request $request){
        $datas = Masterpoint::with('district')->get();
        $data_rows = collect([]);

        foreach($datas as $key =>$data){

            $data_rows->push([
                'id'=>$data->id,
                'name_titik'=>$data->name_titik,
                'name'=>$data->district->name,
            ]);

        }

        $per_page = 10;
        $total = count($data_rows);
        $current_page = $request->input('page') ?? 1;
        $start_point = ($current_page * $per_page) - $per_page;

        $data_rows = $data_rows->toArray();
        $data_rows = array_slice($data_rows, $start_point, $per_page, true);
        $data_rows = New Paginator($data_rows, $total, $per_page, $current_page,[
            'page'=>$request->url(),
            'page'=>$request->query(),
        ]);

        return response()->json([
            'success'=>true,
            'datas'=>$data_rows
        ]);
    }

    public function multiDelete(Request $request){
        
        foreach($request->deleted_id as $key => $value){

            $id = $value['id'];
            $row = Masterpoint::findOrFail($id);

            // dump($row);
            $row->destroy($id);
        }

        return response()->json([
            'success'=>true,
            'status'=>"success",
            'message'=>'Data berhasil dihapus'
        ]);
    }
}
