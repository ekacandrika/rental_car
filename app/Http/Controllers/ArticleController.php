<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Models\newsletter;
use Illuminate\Support\Facades\Validator;
use Thumbnail;

class ArticleController extends Controller
{
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */

        public function index()
        {
                $newsletters = newsletter::with(['tagged'])->get();

                $sects = ['iklan', 'sponsor', 'feature'];
                //        dd($sects);
                return view('BE.admin.Destindonesia.Article.index', compact('sects', 'newsletters'));
        }

        public function newsletter()
        {
                $newsletters = newsletter::get();

                //        dd($newsletters);
                return view('Destindonesia.Article.index', compact('newsletters'));
        }

        public function newsletterCategory($slug)
        {
                $newsletters = newsletter::withAnyTag([$slug])->paginate(5);
                return view('Destindonesia.Article.category', compact('newsletters'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
                //
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\StorenewsletterRequest $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
                // dd($request->all());

                // $this->validate($request, [
                //         'title' => 'required',
                //         'subtitle' => 'required',
                //         'description' => 'required',
                //         'thumbnail' => 'required',
                //         'image_primary' => 'required',
                //         'article_section' => 'required',
                // ]);
                $validate = Validator::make($request->all(),[
                       'title' => 'required',
                       'subtitle' => 'required',
                       'description' => 'required',
                       'thumbnail' => 'required',
                       'image_primary' => 'required',
                       'article_section' => 'required',
                       'tags' => 'required' 
                ],[
                        'title.required'=>'Judul newslater tidak boleh kosong',
                        'subtitle.required'=>'Sub judul newslater tidak boleh kosong',
                        'description.required'=>'Deskripsi newslater tidak boleh kosong',
                        'thumbnail.required'=>'Gamabar thumbnail newslater tidak boleh kosong',
                        'image_primary.required'=>'Gambar utama newslater tidak boleh kosong',
                        'article_section.required'=>'Jenis artikel tidak boleh kosong',
                        'tags.required'=>'Tags artikel tidak boleh kosong',
                ]);

                if($validate->fails()){
                        return redirect()->route('newsletter.index')->withErrors($validate)->withInput();
                }

                //        Thumbnail
                $thumbnail = $request->file('thumbnail');
                $input['thumbnail'] = time() . '.' . $thumbnail->extension();
                $destinationPath = ('public/' . '/' . $input['thumbnail']);
                $img = Thumbnail::make($thumbnail->path());
                $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                })->save(storage_path('app/public/' . '/' . $input['thumbnail']));

                $description = $request->description;
                $nameFile = time() . $request->image_primary->getClientOriginalName();
                $path = $request->file('image_primary')->storeAs('public/images', $nameFile);

                $dom = new \DomDocument();
                $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $imageFile = $dom->getElementsByTagName('imageFile');

                foreach ($imageFile as $item => $image) {
                        $data = $img->getAttribute('src');
                        list($type, $data) = explode(';', $data);
                        list(, $data) = explode(',', $data);
                        $imgeData = base64_decode($data);
                        $image_name = "/upload/" . time() . $item . '.png';
                        $path = public_path() . $image_name;
                        file_put_contents($path, $imgeData);
                        $image->removeAttribute('src');
                        $image->setAttribute('src', $image_name);
                }

                $description = $dom->saveHTML();
                $fileUpload = new newsletter;
                $fileUpload->title = $request->title;
                $fileUpload->subtitle = $request->subtitle;
                $fileUpload->article_section = $request->article_section;
                $fileUpload->description = $description;
                $fileUpload->image_primary = $path;
                $fileUpload->thumbnail = $destinationPath;
                $tags = explode(",", $request->tags);
                //        $select2['select2'] = implode(',', $request->select2);
                //        dd($select2);
                $fileUpload->save();
                $fileUpload->tag($tags);

                return redirect()->route('newsletter.index');
        }

        /**
         * Display the specified resource.
         *
         * @param \App\Models\newsletter $newsletter
         * @return \Illuminate\Http\Response
         */
        public function show($slug)
        {
                $newsletters = newsletter::where('slug', $slug)->with('tagged')->first();
                //        $tagslug = newsletter::with('tagged');
                //            dd($tagslug);
                // Share button 1
                $shareButtons1 = \Share::page(
                        'https://www.kamtuu-staging.cerise.id/destindonesia/newsletter/' . $slug
                )
                        ->facebook()
                        ->twitter()
                        ->linkedin()
                        ->telegram()
                        ->whatsapp()
                        ->reddit();
                //        dd($shareButtons1);

                //        dd($newsletters);
                return view('Destindonesia.Article.post', compact('newsletters', 'shareButtons1'));
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param \App\Models\newsletter $newsletter
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
                $newsletters = newsletter::with(['tagged'])->find($id);
                $tags = implode(",", $newsletters['tag_names']);


                //        dd($tags);
                $sects = ['iklan', 'sponsor', 'feature'];
                return view('BE.admin.Destindonesia.Article.edit', compact('newsletters', 'sects', 'tags'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \App\Http\Requests\UpdatenewsletterRequest $request
         * @param \App\Models\newsletter $newsletter
         * @return \Illuminate\Http\Response
         */
        public function update(Newsletter $newsletter, Request $request, $id)
        {

                $validate = Validator::make($request->all(),[
                        'title' => 'required',
                        'subtitle' => 'required',
                        'description' => 'required',
                        'article_section' => 'required',
                        'tags' => 'required' 
                 ],[
                        'title.required'=>'Judul newslater tidak boleh kosong',
                        'subtitle.required'=>'Sub judul newslater tidak boleh kosong',
                        'description.required'=>'Deskripsi newslater tidak boleh kosong',
                        'article_section.required'=>'Jenis artikel tidak boleh kosong',
                        'tags.required'=>'Tags artikel tidak boleh kosong',
                 ]);
 
                 if($validate->fails()){
                        return redirect()->route('newsletter.edit',['id'=>$id])->withErrors($validate)->withInput();
                 }

                $newsletter = newsletter::findOrFail($id);

                $description = $request->description;
                $dom = new \DomDocument();
                $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $imageFile = $dom->getElementsByTagName('imageFile');
                foreach ($imageFile as $item => $image) {
                        $data = $image->getAttribute('src');
                        list($type, $data) = explode(';', $data);
                        list(, $data) = explode(',', $data);
                        $imgeData = base64_decode($data);
                        $image_name = "/upload/" . time() . $item . '.png';
                        $path = public_path() . $image_name;
                        file_put_contents($path, $imgeData);
                        $image->removeAttribute('src');
                        $image->setAttribute('src', $image_name);
                }
                $description = $dom->saveHTML();
                $newsletter->title = $request->title;
                $newsletter->subtitle = $request->subtitle;
                $newsletter->article_section = $request->article_section;
                $newsletter->description = $description;
                $tags = explode(",", $request->tags);
                //        $tags = array($request->tags);
                $tagged = $tags;


                //        Thumbnail
                if ($request->hasfile('thumbnail')) {
                        $thumbnail = $request->file('thumbnail');
                        $input['thumbnail'] = time() . '.' . $thumbnail->extension();
                        $destinationPath = ('public/thumbnail' . '/' . $input['thumbnail']);
                        if (File::exists($destinationPath)) {
                                File::delete($destinationPath);
                        }
                        $img = Thumbnail::make($thumbnail->path());
                        $img->resize(100, 100, function ($constraint) {
                                $constraint->aspectRatio();
                        })->save(storage_path('app/public/thumbnail' . '/' . $input['thumbnail']));
                        $newsletter->thumbnail = $destinationPath;
                }

                //        image
                if ($request->hasfile('image_primary')) {
                        $destination = 'storage/' . $newsletter->image_primary;
                        if (File::exists($destination)) {
                                File::delete($destination);
                        }
                        $file = $request->file('image_primary');
                        $extention = $file->getClientOriginalExtension();
                        $filename = 'img/' . time() . '.' . $extention;
                        $file->move('storage/img/', $filename);
                        $newsletter->image_primary = $filename;
                }

                //        dd($tagged);
                $newsletter->update();
                //        $newsletter->tag($tags);
                if ($request->tags) {
                        $newsletter->retag($tags);
                } else {
                        $newsletter->untag();
                }
                //        $newsletter->untag($tags);
                //        $newsletter->retag($tags);
                return back();
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param \App\Models\newsletter $newsletter
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
                $newsletter = newsletter::findOrFail($id);
                \Storage::delete($newsletter->image_primary);
                \Storage::delete($newsletter->thumbnail);
                $newsletter->delete();
                return redirect()->route('newsletter.index')
                        ->with('success', 'Alternative has been deleted successfully');
        }
}
