<?php

namespace App\Http\Controllers\Corporate;

use App\Http\Controllers\Controller;
use App\Models\EditAkunCorporate;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class EditAkunController extends Controller
{
    //

    public function index()
    {
        $corporate = EditAkunCorporate::get();
        return view('BE.corporate.edit-account', compact('corporate'));
    }

    public function updateData(Request $request, EditAkunCorporate $corporate, $id, $status)
    {
        $corporate = EditAkunCorporate::find($id);
        
        if ($status == 1) {
            $corporate->update([
                'nama_depan' => $request->nama_depan,
                'nama_belakang' => $request->nama_belakang,
                'alamat_email' => $request->alamat_email,
                'no_telepon' => $request->no_telepon,
            ]);
        }

        if ($status == 2) {
            $corporate->update([
                'negara' => $request->negara,
                'nama_dagang' => $request->nama_dagang,
                'nama_perusahaan' => $request->nama_perusahaan,
                'no_telp' => $request->no_telp,
                'alamat' => $request->alamat,
                'kota' => $request->kota,
                'kode_pos' => $request->kode_pos,
                'website' => $request->website,
            ]);
        }

        return redirect()->route('corporate');

    }
}
