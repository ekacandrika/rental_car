<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Regency;
use App\Models\Village;
use App\Models\District;
use App\Models\Province;
use App\Models\Userdetail;
use App\Models\BranchStore;
use Illuminate\Http\Request;
use App\Models\MasterDocumentLegal;
use App\Models\MasterDocumentValue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class UserdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($role)
    {
        // dd($role);
        $province = Province::all();
        $regency = Regency::all();
        $district = District::all();
        $village = Village::all();
       
        $docs = MasterDocumentLegal::get();
        $legal_doc = collect([]);
        $doc_value = collect([]);

        foreach($docs as $key => $doc){
            $legal_doc->push([
                'title'=>$doc->document_title,
                'is_mandatory'=>$doc->is_mandatory =='yes' ? 'required':null,
                'name'=>str()->replace(' ','_',(str()->lower($doc->document_title)))
            ]);

            $value = MasterDocumentValue::where('user_id',auth()->user()->id)->with('user','masterdocumentlegal')
                    ->where('doc_legal_id',$doc->id)->first();
            // dd($value->masterdocumentlegal);
            if($value){
                $doc_value->push([
                    'id'=>$value->id,
                    'value'=>$value->value,
                    'doc_name'=>$value->masterdocumentlegal->document_title,
                    'is_mandatory'=>$doc->is_mandatory =='yes' ? 'required':null,
                    'name'=>str()->replace(' ','_',(str()->lower($doc->document_title)))
                ]);
            }
        }
        
        // dd($doc_value);
        // dd('test');
        $userdetail = Userdetail::where('user_id', auth()->user()->id)->first();
        $branch_stores = BranchStore::with('province')->with('regency')->with('district')->with('village')->where('user_id',auth()->user()->id)->get();
        // dd(json_decode(auth()->user()->lisensi));
        $personal_tur ='';
        $personal_hotel ='';
        $bussiness_tour_hotel ='';
        
        $personal_rental ='';
        $personal_transfer ='';
        $bussiness_transfer_rental ='';
        
        $personal_activity ='';
        $personal_xstay ='';
        $bussiness_activity_xstay ='';

        if(is_array(json_decode(auth()->user()->lisensi))){
            $lisensi = json_decode(auth()->user()->lisensi);
            // dump($lisensi[0]);
            foreach($lisensi as $key => $v){
                $personal_tur = $v->personal_tur;
                $personal_hotel = $v->personal_hotel;
                $bussiness_tour_hotel = $v->bussiness_tour_hotel;
                $personal_transfer = $v->personal_transfer;
                $personal_rental = $v->personal_rental;
                $bussiness_transfer_rental = $v->bussiness_transfer_rental;
                $personal_activity = $v->personal_activity;
                $personal_xstay = $v->personal_xstay;
                $bussiness_activity_xstay = $v->bussiness_activity_xstay;
            }
        }else{
            
        }

        // echo $bussiness_transfer_rental;

        return view('BE.seller.profil', compact('province', 'regency', 'district', 'village', 'userdetail','personal_tur',
        'personal_hotel','bussiness_tour_hotel','personal_rental','personal_transfer','bussiness_transfer_rental','personal_activity'
        ,'personal_xstay','bussiness_activity_xstay','branch_stores','legal_doc','doc_value'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_data_diri(Request $request, $id)
    {
        $user = User::where('id', $id)->first();
        $userdetail = Userdetail::where('user_id', $id)->first();

        //check if image is uploaded
        if ($request->hasFile('profile_photo_path')) {

            //upload new image
            $image = $request->file('profile_photo_path');
            $new_foto = time() . 'FotoProfileSeller' . $image->getClientOriginalName();
            $tujuan_uploud = 'FotoProfileSeller/';
            $image->move($tujuan_uploud, $new_foto);

            //delete old image
            if (file_exists(($user->profile_photo_path))) {
                unlink(($user->profile_photo_path));
            }

            //update post with new image
            $user->update([
                'profile_photo_path'    => $tujuan_uploud . $new_foto,
                'first_name'            => $request->first_name,
                'jk'                    => $request->jk
            ]);

            // $userdetail->update([
            //     'last_name'     => $request->last_name,
            //     'date_of_birth' => $request->date_of_birth,
            // ]);
        } else {
            //update post without image
            $user->update([
                'first_name'    => $request->first_name,
                'jk'            => $request->jk
            ]);

            // $userdetail->update([
            //     'last_name'     => $request->last_name,
            //     'date_of_birth' => $request->date_of_birth,
            // ]);
        }

        return redirect()->route('seller.profilesetting', auth()->user()->role);
    }

    public function update_kategori_seller(Request $request, $id)
    {
        $data = $request->all();

        User::findOrFail($id)->update($data);

        return redirect()->route('seller.profilesetting', auth()->user()->role);
    }

    public function update_lisensi_personal(Request $request, $id)
    {
        
        $lisensi = collect([]);
        // dump($request->all());
        $lisensi->push([
                'personal_tur'=>isset($request->lisensi['personal_tur'])?$request->lisensi['personal_tur']:null,
                'personal_hotel'=>isset($request->lisensi['personal_hotel'])?$request->lisensi['personal_hotel']:null,
                'bussiness_tour_hotel'=>isset($request->lisensi['bussiness_tour_hotel'])?$request->lisensi['bussiness_tour_hotel']:null,
                'personal_transfer'=>isset($request->lisensi['personal_transfer'])?$request->lisensi['personal_transfer']:null,
                'personal_rental'=>isset($request->lisensi['personal_rental'])?$request->lisensi['personal_rental']:null,
                'bussiness_transfer_rental'=>isset($request->lisensi['bussiness_transfer_rental'])?$request->lisensi['bussiness_transfer_rental']:null,
                'personal_activity'=>isset($request->lisensi['personal_activity'])?$request->lisensi['personal_activity']:null,
                'personal_xstay'=>isset($request->lisensi['personal_xstay'])?$request->lisensi['personal_xstay']:null,
                'bussiness_activity_xstay'=>isset($request->lisensi['bussiness_activity_xstay']) ? $request->lisensi['bussiness_activity_xstay']:null,
        ]);
        
        // dd($lisensi);
        $data = json_encode($lisensi);

        User::findOrFail($id)->update([
            'lisensi'=>$data
        ]);

        return redirect()->route('seller.profilesetting', auth()->user()->role);
    }

    public function update_alamat(Request $request, $id)
    {
        $userdetail = Userdetail::where('user_id', $id)->first();

        $userdetail->update([
            'address'       => $request->address,
            'province_id'   => $request->province_id,
            'regency_id'    => $request->regency_id,
            'district_id'   => $request->district_id,
            'village_id'    => $request->village_id,
            'postal_code'   => $request->postal_code,
        ]);

        return redirect()->route('seller.profilesetting', auth()->user()->role);
    }

    public function update_identitas(Request $request, $id)
    {
        $userdetail = Userdetail::where('user_id', $id)->first();

        //check if image is uploaded
        if ($request->hasFile('ID_card_photo')) {

            //upload new image
            $image = $request->file('ID_card_photo');
            $new_foto = time() . 'FotoIdentitasSeller' . $image->getClientOriginalName();
            $tujuan_uploud = 'FotoIdentitasSeller/';
            $image->move($tujuan_uploud, $new_foto);

            //delete old image
            if (file_exists(($userdetail->ID_card_photo))) {
                unlink(($userdetail->ID_card_photo));
            }

            //update post with new image
            $userdetail->update([
                'ID_card_photo'         => $tujuan_uploud . $new_foto,
                'ID_card_type'          => $request->ID_card_type,
                'identity_card_number'  => $request->identity_card_number
            ]);
        } else {
            //update post without image
            $userdetail->update([
                'ID_card_type'          => $request->ID_card_type,
                'identity_card_number'  => $request->identity_card_number
            ]);
        }

        return redirect()->route('seller.profilesetting', auth()->user()->role);
    }

    public function update_second_email(Request $request, $id)
    {
        $data = Userdetail::where('user_id', $id)->first();

        $data->update([
            'second_email' => $request->second_email
        ]);

        return redirect()->back();
    }

    public function update_second_no_hp(Request $request, $id)
    {
        $data = Userdetail::where('user_id', $id)->first();

        $data->update([
            'second_email' => $request->second_email
        ]);

        return redirect()->back();
    }
    
    public function update_jenis_toko(Request $request, $id){
        $userdetail = Userdetail::where('user_id',$id)->first();
        // dd($userdetail);

        if($userdetail){
            $userdetail->update([
                'store_type'=>$request->jenis_toko,
                'updated_at'=>\Carbon\Carbon::now()
            ]);
        }else{
            Userdetail::create([
                'user_id'=>$id,
                'store_type'=>$request->jenis_toko,
                'created_at'=>\Carbon\Carbon::now()
            ]);
        }

        return redirect()->route('seller.profilesetting', auth()->user()->role);
    }
   
    public function update_dokumen(Request $request, $id){
        // dd($request->all());
        $userdetail = Userdetail::where('user_id',$id)->first();
        $master_document = MasterDocumentLegal::get();
        $input = $request->all();

        foreach($master_document as $in){
            $name_document = str()->replace(' ','_',(str()->lower($in->document_title)));
            
            if($request->hasFile($name_document)){

                $file = $request->file($name_document);
                $name_file = time().$file->getClientOriginalName();

                $path ='Seller/';
                $full_path = $path.$name_file;

                $file->move($path,$name_file);

                // check data
                $doc_value = MasterDocumentValue::where('user_id',$in->id)->first();
                if($doc_value){
                    if(file_exists($doc_value->value)){
                        unlink($doc_value->value);
                    }

                    $doc_value->update([
                      'value'=>  $full_path,
                      'updated_at'=>Carbon::now()
                    ]);

                }else{
                    MasterDocumentValue::create([
                        'user_id'=>$id,
                        'doc_legal_id'=>$in->id,
                        'value'=>$full_path,
                        'created_at'=>Carbon::now()
                    ]);
                }
            }
        }
        /*    if($request->hasFile('doc_akta')){
            $deed_doc = $request->file('doc_akta');

            $deed = time().$deed_doc->getClientOriginalName();
            $path = 'Seller/';

            $deed_doc->move($path,$deed);

            $userdetail->update([
                'doc_akta'=>$path.$deed
            ]);
        }

        if($request->hasFile('doc_pengesahan')){
            $legal_doc = $request->file('doc_pengesahan');

            $legal = time().$legal_doc->getClientOriginalName();
            $path  = 'Seller/';

            $legal_doc->move($path,$legal);

            $userdetail->update([
                'doc_legal'=>$path.$legal
            ]);
        }

        if($request->hasFile('doc_nib')){
            $nib_doc =$request->file('doc_nib');

            $nib = $nib_doc->getClientOriginalName();
            $path= 'Seller/';

            $nib_doc->move($path,$nib);

            $userdetail->update([
                'doc_nib'=>$path.$nib
            ]);
        }

        if($request->hasFile('doc_ktp_direktur')){
            $id_card_doc = $request->file('doc_ktp_direktur');

            $id_card = time().$id_card_doc->getClientOriginalName();
            $path  = 'Seller/';

            $id_card_doc->move($path,$id_card);

            $userdetail->update([
                'doc_directur_id_card'=>$path.$id_card
            ]);
        }

        if($request->hasFile('id_card_photo')){
            $id_card =$request->file('id_card_photo');

            $id_card_photo = $id_card->getClientOriginalName();
            $path= 'Seller/';

            $id_card->move($path,$id_card_photo);

            $userdetail->update([
                'ID_card_photo'=>$path.$id_card_photo
            ]);
        } */
    
        return redirect()->route('seller.profilesetting', auth()->user()->role);
    }
    
    public function update_penanggung_jawab(Request $request, $id){
       
        $user = User::where('id',$id)->first();
        $userdetail =Userdetail::where('user_id',$id)->first();
        // dd($request->all());
        $this->validate($request,[
            'first_name'=>'required',
            'email'=>'required',
            // 'jabatan'=>'required',
            'no_tlp'=>'required',
        ],[
            'first_name.required'=>'Nama tidak boleh kosong',
            'email.required'=>'Email tidak boleh kosong',
            // 'jabatan.required'=>'Jabatan tidak boleh kosong',
            'no_tlp.required'=>'No telp tidak boleh kosong',
        ]);

        if($request->hasFile('profile_photo_path')){

            $validate =  Validator::make($request->all(),[
                'profile_photo_path.*'=>'mimes:jpeg,png,jpg|max:2048',
            ],[
               'profile_photo_path.*.mimes'=>'Foto harus memilik ekstensi (jpg, jpeg, atau png)',
               'profile_photo_path.*.max'=>'Foto maksimum berukuran 2MB' 
            ]);

            if($validate->fails()){
                return redirect()->route('seller.profilesetting', auth()->user()->role)->withErrors($validate)->withInput();
            }

            $image= $request->file('profile_photo_path');

            $photo = $image->getClientOriginalName();
            $path = 'Seller/';

            $image->move($path,$photo);

            // delete old photo profile
            if(file_exists($user->profile_photo_path)){
                unlink($user->profile_photo_path);
            }

            $user->update([
                'first_name'=>$request->first_name,
                'email'=>$request->email,
                'no_tlp'=>$request->no_tlp,
                'profile_photo_path'=>$path.$photo
            ]);
        }else{
            $user->update([
                'first_name'=>$request->first_name,
                'email'=>$request->email,
                'no_tlp'=>$request->no_tlp,
                // 'whatsapp_no'=>$request->whatsapp != null ? $request->whatsapp_no : $request->no_telp,
            ]); 
        }

        $id_card_photo = '';

        if($request->hasFile('id_card_photo')){
            $validate =  Validator::make($request->all(),[
                'id_card_photo.*'=>'mimes:jpeg,png,jpg|max:2048',
            ],[
               'id_card_photo.*.mimes'=>'Foto harus memilik ekstensi (jpg, jpeg, atau png)',
               'id_card_photo.*.max'=>'Foto maksimum berukuran 2MB' 
            ]);

            if($validate->fails()){
                return redirect()->route('seller.profilesetting', auth()->user()->role)->withErrors($validate)->withInput();
            }

            $image= $request->file('id_card_photo');

            $photo = $image->getClientOriginalName();
            $path = 'Seller/';

            // delete old photo profile
            if(file_exists($userdetail->ID_card_photo)){
                unlink($userdetail->ID_card_photo);
            }
             
            $image->move($path,$photo);
            $id_card_photo = $path.$photo;
        }

        if($userdetail){
            $userdetail->update([
                'ID_card_type'=>$request->id_card_type,
                'job_title'=> $request->jabatan,
                'identity_card_number'=>$request->identity_card_number,
                'ID_card_photo'=>$id_card_photo,
            ]);
        }else{
            Userdetail::create([
                'ID_card_type'=>$request->id_card_type,
                'job_title'=> $request->jabatan,
                'identity_card_number'=>$request->identity_card_number,
                'user_id'=>$id,
                'ID_card_photo'=>$id_card_photo,
            ]);
        }
        

        return redirect()->route('seller.profilesetting', auth()->user()->role);
    }
    
    public function update_data_perusahaan(Request $request, $id){
       
        $userdetail = Userdetail::where('user_id',$id)->first();

        // dd($id,$request->all(),$userdetail);

        $this->validate($request,[
            'nama_perusahaan'=>'required',
            'alamat'=>'required',
            'tgl_pendirian'=>'required',
            'province_id'=>'required',
            'regency_id'=>'required',
            'district_id'=>'required',
            'village_id'=>'required',
            'kode_pos'=>'required',
            'nama_direktur'=>'required',
        ],[
            'nama_perusahaan.required'=>'Nama perusahaan tidak boleh kosong',
            'alamat.required'=>'Alamat tidak boleh kosong',
            'tgl_pendirian.required'=>'Tanggal pendirian tidak boleh kosong',
            'province_id.required'=>'Provinsi tidak boleh kosong',
            'regency_id.required'=>'Kabupaten/Kota tidak boleh kosong',
            'district_id.required'=>'Kecamatan tidak boleh kosong',
            'village_id.required'=>'Kelurahan/Desa tidak boleh kosong',
            'kode_pos.required'=>'Kode pos tidak boleh kosong',
            'nama_direktur.required'=>'Nama direktur tidak boleh kosong',
        ]);

        $userdetail->update([
            'company_name'=>$request->nama_perusahaan,
            'address'=>$request->alamat,
            'date_of_establishment'=>\Carbon\Carbon::createFromFormat('d/m/Y',$request->tgl_pendirian)->format('Y-m-d'),
            'province_id'=>$request->province_id,
            'regency_id'=>$request->regency_id,
            'district_id'=>$request->district_id,
            'village_id'=>$request->village_id,
            'postal_code'=>$request->kode_pos,
            'directur_name'=>$request->nama_direktur,
        ]);

        return redirect()->route('seller.profilesetting', auth()->user()->role);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();

        return redirect()->route('welcome');
    }
}
