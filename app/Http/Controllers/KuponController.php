<?php

namespace App\Http\Controllers;

use App\Models\Kupon;
use App\Models\Usertraveller;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class KuponController extends Controller
{
    public function index()
    {
        $n = 12;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        $kupon = Kupon::where([])->paginate(2);

//        dd($kupon);

        return view('BE.admin.Kupon.index', compact('randomString', 'kupon'));
    }

    public function travellerKupon()
    {
        $user = \Auth::user()->id;
        $ids = Kupon::select('user_id')->get();
        $idcoll = collect($ids)->toArray();
        $cek = in_array($user, $idcoll);
        $kupons = Kupon::select('kodeKupon')->get();

        $traveller = Usertraveller::where('user_id','=',$user)->select('kupon_code')->first();

        if ($traveller == null){
            $kuponTraveller = [];
            return view('BE.traveller.kupon.claim-kupon',compact('kuponTraveller'));
        }

        $collTraveller = collect($traveller->kupon_code)->toArray();

        $kuponTraveller = Kupon::select('*')->whereIn('kodeKupon',$collTraveller)->get();
//        get data jika ada yang sama di dalam array pada kode kupon
//        dd($kuponTraveller);

        return view('BE.traveller.kupon.claim-kupon',compact('kuponTraveller'));
    }



    public function travellerKuponClaim(Kupon $kupon, Request $request)
    {

        $user = \Auth::user()->id;
        $kupon = Kupon::where('kodeKupon', '=', $request->kodeKupon)->first();
        $kuponUser = Kupon::where('kodeKupon', '=', $request->kodeKupon)->select('user_id')->first();
        $kodeTraveller = Usertraveller::where('user_id','=',$user)->select('kupon_code')->first();
        $kuponColl = collect($kodeTraveller->kupon_code);
        if ($kuponUser == null){
            return redirect()->route('traveller.kupon')->with('warning', 'Kode Kupon Tidak Sesuai');
        }
        $coll = collect($kuponUser->user_id);
        $cekFirst = $coll;
        $cek3 = $cekFirst->toArray();
//        cek array tanpa inputan + hanya di database
        $coll[] = $request->user_id;


//        foreach ($kupon as $item) {
//            $data = json_decode($request->user_id);
//            dd($kuponUser);
//            $data = $item->user_id;
//            foreach ($data as $value) {
        $cek = strval($user);

        $cek2 = $coll->toArray();
//        cek array yang ditambah oleh inputan

//        dd($cek, $cek2, $cek3);

        if ((in_array($cek, $cek2)) && ($kupon->user_id == 0) && ($kupon->jumlahKupon > 0) && ($kupon->kodeKupon == $request->kodeKupon)) {
            $cekStock = $kupon->jumlahKupon - 1;
            Kupon::where('kodeKupon', '=', $request->kodeKupon)->update([
                'user_id' => $coll,
                'jumlahKupon' => $cekStock]);
            $kuponColl[] = $request->kodeKupon;
            Usertraveller::where('user_id','=',$user)->update(['kupon_code' => $kuponColl]);
//            echo "add new";

            return redirect()->route('traveller.kupon')->with('success', 'Selamat Anda Berhasil Mendapatkan Kupon');
        } elseif ((!in_array($cek, $cek3)) && ($kupon->jumlahKupon > 0) && ($kupon->kodeKupon == $request->kodeKupon)) {
            $cekStock = $kupon->jumlahKupon - 1;
            Kupon::where('kodeKupon', '=', $request->kodeKupon)->update([
                'user_id' => $coll,
                'jumlahKupon' => $cekStock]);
            $kuponColl[] = $request->kodeKupon;
            Usertraveller::where('user_id','=',$user)->update([
                'kupon_code' => $kuponColl]);
//            echo "Add New Coupon other id";

            return redirect()->route('traveller.kupon')->with('success', 'Selamat Anda Berhasil Mendapatkan Kupon');
        }elseif ((in_array($cek, $cek2)) && ($kupon->kodeKupon == $request->kodeKupon)) {
            echo "Have Coupon";


            return redirect()->route('traveller.kupon')->with('danger', 'Anda Sudah Memiliki Kupon');
        } else {


            return redirect()->route('traveller.kupon')->with('warning', 'Kode Kupon Tidak Ada');
        }
//            }
//        }

//

//        $kupon->update($data);
//        foreach isi array user + tambahkan user
    }


    public
    function kuponStore(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            "judulKupon" => 'required',
            "kodeKupon" => "required",
            "diskonKupon" => 'required',
            "jumlahKupon" => 'required',
            "expiredDate" => 'required',
        ],[
            "judulKupon.required" =>'Judul kupon tidak boleh kosong',
            "kodeKupon.required" =>'Kode kupon tidak boleh kosong',
            "diskonKupon.required" =>'Diskon tidak boleh kosong',
            "jumlahKupon.required" =>'Jumlah tidak boleh kosong',
            "expiredDate.required" =>'tanggal kadaluarsa tidak boleh kosong',
        ]);

        $data = new Kupon();
        $data->kodeKupon = $request->kodeKupon;
        $data->judulKupon = $request->judulKupon;
        $data->diskonKupon = $request->diskonKupon;
        $data->jumlahKupon = $request->jumlahKupon;
        $data->expiredDate = Carbon::parse($request->expiredDate);
//        dd($data);

        $data->save();
        return back();

    }
}
