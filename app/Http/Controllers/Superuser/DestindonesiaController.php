<?php

namespace App\Http\Controllers\Superuser;

use App\Http\Controllers\Controller;
use App\Models\Destindonesia;
use App\Models\Province;
use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use Illuminate\Http\Request;

class DestindonesiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = Destindonesia::all();
        // return view('Backend.Destindonesia.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Province::all();
        return view('BE.formdestinasi.form-destinasi', compact('provinces'));
    }

    public function getkabupaten(Request $request)
    {
        $id_provinsi = $request->id_provinsi;
        $id_kabupaten = isset($request->id_kabupaten) ? $request->id_kabupaten :null;

        $kabupatens = Regency::where('province_id', $id_provinsi)->get();
        // dd($kabupatens);

        foreach ($kabupatens as $kabupaten) {
            $seleted = $kabupaten->id==$id_kabupaten ? 'selected':'';
            echo "<option value='$kabupaten->id' $seleted >$kabupaten->name</option>";
        }
    }

    public function getkecamatan(Request $request)
    {
        $id_kabupaten = $request->id_kabupaten;
        $id_kecamatan = isset($request->id_kecamatan) ? $request->id_kecamatan :null;

        $kecamatans = District::where('regency_id', $id_kabupaten)->get();

        foreach ($kecamatans as $kecamatan) {
            $seleted = $kecamatan->id==$id_kecamatan ? 'selected':'';
           
            echo "<option value='$kecamatan->id' $seleted >$kecamatan->name</option>";
        }
    }

    public function getdesa(Request $request)
    {
        $id_kecamatan = $request->id_kecamatan;
        $id_kelurahan = isset($request->id_kelurahan) ? $request->id_kelurahan:null;

        $desas = Village::where('district_id', $id_kecamatan)->get();

        foreach ($desas as $desa) {
            $seleted = $desa->id==$id_kelurahan?'selected':'';
            echo "<option value='$desa->id' $seleted>$desa->name</option>";
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'provinsi_id' => 'required',
            'kota_id' => 'required',
            'kecamatan_id' => 'required',
            'objekwisata' => 'required',
        ]);

        $data = $request->all();
        // dd($data);

        Destindonesia::create($data);

        return redirect()->route('destindonesia.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
