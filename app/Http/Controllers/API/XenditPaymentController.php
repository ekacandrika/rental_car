<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Xendit\Xendit;
use App\Models\Order;

class XenditPaymentController extends Controller
{
    private $token = 'xnd_public_production_9WwbW75C9ArUZ1yNr2mNC84uzIwt4OYyGjmhHzd4sC04eRPQqRH7gWU0Qnpetu';

    public function callbackVA(Request $request)
    {
        Xendit::setApiKey($this->token);

        // This will be your Callback Verification Token you can obtain from the dashboard.
        // Make sure to keep this confidential and not to reveal to anyone.
        // This token will be used to verify the origin of request validity is really from Xendit
        $xenditXCallbackToken = 'FCTvb5C9TssvRLVziFOmPQHHLYarCngKuzDfIhcgdUt7YRbJ';

        // This section is to get the callback Token from the header request, 
        // which will then later to be compared with our xendit callback verification token
        $reqHeaders = getallheaders();
        $xIncomingCallbackTokenHeader = isset($reqHeaders['x-callback-token']) ? $reqHeaders['x-callback-token'] : "";

        // In order to ensure the request is coming from xendit
        // You must compare the incoming token is equal with your xendit callback verification token
        // This is to ensure the request is coming from Xendit and not from any other third party.
        if ($xIncomingCallbackTokenHeader === $xenditXCallbackToken) {
            // Incoming Request is verified coming from Xendit
            // You can then perform your checking and do the necessary, 
            // such as update your invoice records

            // This line is to obtain all request input in a raw text json format
            $rawRequestInput = file_get_contents("php://input");
            // This line is to format the raw input into associative array
            $arrRequestInput = json_decode($rawRequestInput, true);
            // print_r($arrRequestInput);

            $external_id = $arrRequestInput['external_id'];
            $status = $arrRequestInput['status'];

            // You can then retrieve the information from the object array and use it for your application requirement checking
            $payment = Order::where('external_id', $external_id)->exists();
            if ($payment) {
                if ($status == 'ACTIVE') {
                    $update = Order::where('external_id', $external_id)->update([
                        'status' => 'SUCCESS'
                    ]);
                    if ($update > 0) {
                        return 'PEMBAYARAN BERHASIL!';
                    }
                    return 'PEMBAYARAN GAGAL!';
                }
            } else {
                return response()->json([
                    'message' => 'DATA TIDAK DITEMUKAN!'
                ]);
            }
        } else {
            // Request is not from xendit, reject and throw http status forbidden
            http_response_code(403);
        }
        // var_dump($json);
        // $external_id = $request->external_id;
        // $status = $request->status;
    }
}
