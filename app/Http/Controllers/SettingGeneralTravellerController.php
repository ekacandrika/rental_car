<?php

namespace App\Http\Controllers;

use App\Models\BookingOrder;
use App\Models\DetailPemesanan;
use App\Models\Order;
use App\Models\Productdetail;
use Conner\Tests\Tagging\Book;
use Dompdf\Dompdf;
use Dompdf\Options;
use PDF;
use Illuminate\Support\Facades\View;
use function Symfony\Component\String\b;
use App\Models\Attributes;
use Illuminate\Support\Facades\Storage;
use App\Models\Masterkamar;
use App\Models\DetailKendaraan;
use App\Models\detailObjek;
use App\Models\MerekMobil;
use App\Models\Discount;
use App\Models\Masterroute;
use App\Models\District;
use App\Models\Village;

class SettingGeneralTravellerController extends Controller
{
    public function getInvoice($order, $booking)
    {
        $bookings = BookingOrder::where('booking_code', $booking)
            ->with(['productdetail' => function ($query) {
                $query->with('product');
            }])
            ->with(['users' => function ($query2) {
                $query2->where('user_id', auth()->user()->id);
            }])
            ->first();

        $data = Order::where('external_id', $order)
            ->with(['bookings' => function ($q) {
                $q->with(['productdetail' => function ($query) {
                    $query->with('product');
                }]);
                $q->with(['users' => function ($query2) {
                    $query2->where('user_id', auth()->user()->id);
                }]);
            }])
            ->get();

        //        dd($data);
        return View::make('Dokumen.invoice_pdf', compact('data', 'bookings', 'booking', 'order'));
    }

    public function getVoucher($id_booking)
    {
        $booking = BookingOrder::where('booking_code', $id_booking)
            ->with(['productdetail' => function ($query) {
                $query->with('product');
            }])
            ->with(['users' => function ($query2) {
            }])->first();

        //  dump($booking);          
        return View::make('Dokumen.voucher_pdf', compact('booking', 'id_booking'));
    }

    public function downloadVoucher($id_booking)
    {
        $booking = BookingOrder::where('booking_code', $id_booking)
            ->with(['productdetail' => function ($query) {
                $query->with('product');
            }])
            ->with(['users' => function ($query2) {
            }])->first();
        $html   = view('Dokumen.voucher_pdf_download', compact('booking', 'id_booking'))->render();
        $option = new Options();
        $option->set('isHtml5ParserEnabled', true);
        $dompdf = new Dompdf($option);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream('voucher.pdf', ['Attachment' => false]);
    }

    public function downloadInvoice($order, $booking)
    {
        $bookings = BookingOrder::where('booking_code', $booking)
            ->with(['productdetail' => function ($query) {
                $query->with('product');
            }])
            ->with(['users' => function ($query2) {
                $query2->where('user_id', auth()->user()->id);
            }])
            ->first();

        $data = Order::where('external_id', $order)
            ->with(['bookings' => function ($q) {
                $q->with(['productdetail' => function ($query) {
                    $query->with('product');
                }]);
                $q->with(['users' => function ($query2) {
                    $query2->where('user_id', auth()->user()->id);
                }]);
            }])
            ->get();

        $dompdf = new Dompdf();
        $dompdf->loadHtml(View::make('Dokumen.invoice_pdf_download', compact('bookings', 'booking', 'data'))->render());
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream('invoice_pdf.pdf', ['Attachment' => false]);
    }

    public function getReceipt($order, $booking)
    {
        $bookings = BookingOrder::where('booking_code', $booking)
            ->with(['productdetail' => function ($query) {
                $query->with('product');
            }])
            ->with(['users' => function ($query2) {
                $query2->where('user_id', auth()->user()->id);
            }])
            ->first();

        $orders = Order::where('external_id', $order)
            ->with(['bookings' => function ($q) {
                $q->with(['productdetail' => function ($query) {
                    $query->with('product');
                }]);
                $q->with(['users' => function ($query2) {
                    $query2->where('user_id', auth()->user()->id);
                }]);
            }])
            ->get();

        return View::make('Dokumen.receipt_pdf', compact('bookings', 'orders', 'booking', 'order'));
    }

    public function downloadReceipt($order, $booking)
    {
        $bookings = BookingOrder::where('booking_code', $booking)
            ->with(['productdetail' => function ($query) {
                $query->with('product');
            }])
            ->with(['users' => function ($query2) {
                $query2->where('user_id', auth()->user()->id);
            }])
            ->first();

        $orders = Order::where('external_id', $order)
            ->with(['bookings' => function ($q) {
                $q->with(['productdetail' => function ($query) {
                    $query->with('product');
                }]);
                $q->with(['users' => function ($query2) {
                    $query2->where('user_id', auth()->user()->id);
                }]);
            }])
            ->get();

        $dompdf = new Dompdf();
        $dompdf->loadHtml(View::make('Dokumen.receipt_pdf_download', compact('orders', 'bookings', 'order', 'booking'))->render());
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream('invoice.pdf', ['Attachment' => true]);
    }

    public function test($booking)
    {
        $bookings = BookingOrder::where('booking_code', $booking)
            ->with(['productdetail' => function ($query) {
                $query->with('product');
            }])
            ->with(['users' => function ($query2) {
                $query2->where('user_id', auth()->user()->id);
            }])
            ->first();

        $fasilitas = '';
        $amenitas = '';
        $jenis_kamar = '';
        $kode_kamar = '';
        $dewasa = isset($bookings->peserta_dewasa) ? $bookings->peserta_dewasa . ' dewasa' : '';
        $anak = isset($bookings->peserta_anak) ? $bookings->peserta_anak . ' anak' : '';
        $balita = isset($bookings->peserta_balita) ? $bookings->peserta_balita . ' balita' : '';

        if (isset($bookings->first_name) && isset($booking->last_name)) {
            $first_name = isset($bookings->first_name) ? json_decode($bookings->first_name, true)[0] : $bookings->users->last_name;
            $last_name = isset($bookings->last_name) ? json_decode($bookings->last_name, true)[0] : $bookings->users->last_name;
       
        } else {
           
            $first_name = isset($bookings->first_name) ? json_decode($bookings->first_name, true)[0] : $bookings->users->last_name;
            $last_name = isset($bookings->last_name) ? json_decode($bookings->last_name, true)[0] : $bookings->users->last_name;
        }

        $jumlah_peserta = isset($bookings->kategori_peserta) ? json_decode($bookings->kategori_peserta, true) : [];
        $jumlah_dewasa = 0;
        $jumlah_anak = 0;
        $jumlah_balita = 0;

        $nama_mobil = "";
        $type_mobil = "";
        $merk_mobil = "";
        $nopol      = "";
        $kapasitas_mobil = "";

        $tour = null;

        $rute_id = '';
        $from = '';
        $to = '';
        $rutes = '';
        $detail_rute = '';

        $activity_name = "";
        $nama_lokasi ='';

        foreach ($jumlah_peserta as $peserta) {

            if ($peserta == 'dewasa') {
                $jumlah_dewasa += 1;
            }

            if ($peserta == 'anak') {
                $jumlah_anak += 1;
            }

            if ($peserta == 'anak') {
                $jumlah_anak += 1;
            }
        }

        if ($bookings->type === 'hotel' || $bookings->type === 'xstay') {
            $i = 0;
            $x = 0;
            $hotel_and_xstay = $bookings->productdetail;
            $id_fasilitas = isset($hotel_and_xstay->id_fasilitas) ? $hotel_and_xstay->id_fasilitas : [];
            $id_amenitas = isset($hotel_and_xstay->id_amenitas) ? $hotel_and_xstay->id_amenitas : [];


            foreach (json_decode($id_fasilitas, true)['result'] as $fasilitas_key) {

                $data_fasiltas = Attributes::where('id', $fasilitas_key)->first();

                $fasilitas_img = str_replace('public/icons/attributes/', 'storage/app/public/icons/attributes/', $data_fasiltas->image);
                $fasilitas_base64 = 'data:image/svg+xml;base64,' . base64_encode(file_get_contents(base_path($fasilitas_img)));

                $fasilitas .= "<img src=" . $fasilitas_base64 . " alt=" . $data_fasiltas->text . " width='20px' height='20px'/>";

                if (count(json_decode($id_fasilitas, true)['result']) != $i) {
                    $fasilitas .= ", ";
                }

                $i++;
            }

            foreach (json_decode($id_amenitas, true)['result'] as $amenitas_key) {

                $data_amenitas = Attributes::where('id', $amenitas_key)->first();

                $fasilitas_img = str_replace('public/icons/attributes/', 'storage/app/public/icons/attributes/', $data_amenitas->image);
                $amenitas_base64 = 'data:image/svg+xml;base64,' . base64_encode(file_get_contents(base_path($fasilitas_img)));

                $amenitas .= "<img src=" . $amenitas_base64 . " alt=" . $data_amenitas->text . " width='20px' height='20px'/>";

                if (count(json_decode($id_amenitas, true)['result']) != $x) {
                    $amenitas .= ", ";
                }

                $x++;
            }

            $kamar =  Masterkamar::where('id', $bookings->kamar_id)->first();

            $jenis_kamar = isset($kamar->nama_kamar) ? $kamar->nama_kamar : $kamar->jenis_kamar;
            $kode_kamar = $kamar->kode_kamar;

            // echo $amenitas;

        }

        if ($bookings->type === 'rental') {

            $rental = $bookings->productdetail;

            $detail_mobil = DetailKendaraan::where("id", $rental->id_detail_kendaraan)->first();
            $jenis_mobil  = MerekMobil::where("id", $detail_mobil->id_jenis_mobil)->first();
            // dd($detail_mobil,$jenis_mobil);          
            $nama_mobil = $detail_mobil->nama_kendaraan;
            $type_mobil = $jenis_mobil->merek_mobil;
            $kapasitas_mobil = $detail_mobil->kapasitas_kursi . ' kursi';
            $nopol = $jenis_mobil->plat_mobil;
        }

        if ($bookings->type === 'tour') {
            $tour = $bookings->productdetail;
            $peserta = Discount::where('id', $tour->discount_id)->first();

            $min_orang = isset($peserta->min_orang) ? count(json_decode($peserta->min_orang, true)) : 0;
            $max_orang = isset($peserta->max_orang) ? count(json_decode($peserta->max_orang, true)) : 0;
        }

        if ($bookings->type == 'transfer') {
            $transfer = $bookings->productdetail;

            $rute_id = $transfer->rute_id;

            if ($transfer->rute_id) {
                $rute = Masterroute::where('id', $transfer->rute_id)->first();

                $detail_rute = json_decode($rute->rute, true)['result'];
                $from = District::where('id', $detail_rute['from']);
                $to = District::where('id', $detail_rute['to']);
            } else {
                $rutes = json_decode($transfer->drop_pick_detail, true)['result'];
            }
        }

        if ($bookings->type == 'activity') {
            $activity =  $bookings->productdetail;
            $activity_name = $activity->product->name_product;

            if($activity->tag_location_4){
                $lokasi = Village::where('id',$activity->tag_location_4)->first();

                if(!$lokasi->name){
                    $lokasi = detailObjek::where('id',$activity->tag_location)->first();

                    $nama_lokasi = $lokasi->title;

                }

            }else{
                $lokasi = Village::where('id',$activity->village_id)->first();
                $nama_lokasi = $lokasi->name;
            }
            
            // dd($activity);
        }

        // die;
        $pdf = PDF::loadView('Dokumen.test_pdf', compact(
            'bookings',
            'booking',
            'fasilitas',
            'amenitas',
            'dewasa',
            'anak',
            'balita',
            'jenis_kamar',
            'kode_kamar',
            'jumlah_dewasa',
            'jumlah_anak',
            'jumlah_balita',
            'first_name',
            'last_name',
            'type_mobil',
            'kapasitas_mobil',
            'nopol',
            'nama_mobil',
            'tour',
            'rute_id',
            'from',
            'to',
            'detail_rute',
            'rutes',
            'activity_name',
            'nama_lokasi'
        ));
        return $pdf->stream('kamtuu-voucher.pdf');
    }
}
