<?php

namespace App\View\Components\produk;

use Illuminate\View\Component;

class CardPopulerHomeNew extends Component
{
    public $type;
    public $act;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($type, $act)
    {
        //
        $this->type = $type;
        $this->act = $act;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.produk.card-populer-home-new');
    }
}
