<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CardDetailToko extends Component
{
    public $act;
    public $type;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($act, $type)
    {
        $this->act  = $act;
        $this->type = $type;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.card-detail-toko');
    }
}
