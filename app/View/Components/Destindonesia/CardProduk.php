<?php

namespace App\View\Components\Destindonesia;

use Illuminate\View\Component;

class CardProduk extends Component
{
    public $act;
    public $type;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($act, $type)
    {
        $this->act = $act;
        $this->type = $type;
    }
    
    public function lazyData(){
        return $this->act;
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.destindonesia.card-produk');
    }
}
