<?php

namespace App\View\Components\xstay;

use Illuminate\View\Component;

class app-xstay extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.xstay.app-xstay');
    }
}
