<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;

class RegisterNotifyMail extends Mailable
{
    use Queueable, SerializesModels;
    
    public $name;
    public $message;
    // public $t;
    public $fromEmail;
    public $fromName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the message envelope.
     *
     * @return \Illuminate\Mail\Mailables\Envelope
     */
    public function envelope()
    {
        return new Envelope(
            subject: 'User register notification',
        );
    }

    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        $user = auth()->user();
        $token = $this->urlHashing($user);
        
        return new Content(
            view: 'auth.email-verify-custom',
            with:[
                'id'=>$user->id,
                'token'=>$token 
            ]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments()
    {
        return [];
    }

    private function urlHashing($notifiable){
        return URL::temporarySignedRoute(
            'email.verifed',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire',60)),
            [
                'id'=>$notifiable->id,
                'hash'=>sha1($notifiable->getEmailForVerification()),
            ]
        );
    }
}
