<?php

namespace App\Actions\Fortify;

use App\Models\User;
use App\Models\Userdetail;
use App\Models\Usertraveller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'first_name' => ['required', 'string', 'max:255'],
            'jenis_registrasi' => ['nullable', 'string', 'max:255'],
            'role' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users', 'unique:user_traveller,second_email', 'unique:user_detail,second_email'],
            'no_tlp' => ['required', 'string', 'max:255'],
            'jk' => ['required', 'string', 'max:255'],
            'lisensi' => ['nullable', 'string', 'max:255'],
            'password' => $this->passwordRules(),
            'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['accepted', 'required'] : '',
        ],[
            'first_name.required' => 'Nama wajib diisi!',
            'email.email' => 'Pastikan email yang Anda masukkan sudah benar',
            'email.unique' => 'Email yang Anda masukkan sudah terdaftar.',
            'no_tlp.required' => 'Nomor telepon wajib diisi!',
            'password.confirmed' => 'Pastikan password yang Anda masukan sudah sesuai.'
        ])->validate();

        $registrasi = User::create([
            'first_name' => $input['first_name'],
            'jenis_registrasi' => isset($input['jenis_registrasi']) ? $input['jenis_registrasi'] : null,
            'role' => $input['role'],
            'email' => $input['email'],
            'no_tlp' => $input['no_tlp'],
            'lisensi' => isset($input['lisensi']) ? $input['lisensi'] : null,
            'jk' => $input['jk'],
            'password' => Hash::make($input['password']),
        ]);

        $userdetail = Userdetail::create([
            'user_id' => $registrasi->id,
        ]);

        if ($input['role'] == 'traveller') {
            $usertraveller = Usertraveller::create([
                'user_id' => $registrasi->id,
                'akun_booking_status' => 0,
                'newslatter_status' => 1
            ]);
        }
        if ($input['role'] == 'agent') {
            $usertraveller = Usertraveller::create([
                'user_id' => $registrasi->id,
                'akun_booking_status' => 0,
                'newslatter_status' => 1
            ]);
        }

        return $registrasi;
        return $userdetail;
        return $usertraveller;
    }
}
