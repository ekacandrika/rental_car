<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use App\Models\Province;

class BranchStore extends Model
{
    use HasFactory;

    protected $hidden=[
        'id'
    ];

    protected $fillable=[
        'province_id',
        'regency_id',
        'district_id',
        'village_id',
        'post_code',
        'address',
        'user_id',
    ];

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function village()
    {
        return $this->belongsTo(Village::class);
    }
}
