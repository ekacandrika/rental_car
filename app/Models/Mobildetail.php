<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mobildetail extends Model
{
    use HasFactory;

    protected $table = 'detail_mobil';

    protected $hidden = [
        'merek_id'
    ];

    protected $fillable = [
        'merek_id',
        'nama_mobil',
        'kapasitas_kursi',
        'kapasitas_koper',
        'harga_akomodasi',
        'status'
    ];
    public function merekmobil()
    {
        return $this->belongsTo(MerekMobil::class, 'merek_id');
    }
}
