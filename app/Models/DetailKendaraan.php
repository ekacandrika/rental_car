<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailKendaraan extends Model
{
    use HasFactory;

    protected $table = 'detail_kendaraan';

    protected $hidden = [
        'id_jenis_bus',
        'id_jenis_mobil'
    ];
    
    protected $fillable = [
        'id_jenis_bus',
        'id_jenis_mobil',
        'id_merek_mobil',
        'nama_kendaraan',
        'kapasitas_kursi',
        'kapasitas_koper',
        'harga_akomodasi',
        'status',
        'pilihan_tambahan'
    ];
    public function jenismobil()
    {
        return $this->belongsTo(JenisMobil::class, 'id_jenis_mobil');
    }
    public function jenisbus()
    {
        return $this->belongsTo(JenisBus::class, 'id_jenis_bus', 'id_jenis');
    }
    public function merekmobil()
    {
        return $this->belongsTo(MerekMobil::class, 'id_merek_mobil');
    }
    public function merekbus()
    {
        return $this->belongsTo(MerekBus::class);
    }
}
