<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailRute extends Model
{
    use HasFactory;

    protected $table = 'detail_rute';

    protected $fillable = [
        'id_rute',
        'judul_rute',
        'naik_long',
        'naik_lat',
        'turun_long',
        'turun_lat',
        'harga'
    ];
}
