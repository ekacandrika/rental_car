<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use RalphJSmit\Laravel\SEO\Support\HasSEO;
use Cviebrock\EloquentSluggable\Sluggable;

class newsletter extends Model
{
    use HasFactory;
    use HasSEO;
    use Sluggable;
    use \Conner\Tagging\Taggable;

    protected $fillable = ['title', 'subtitle', 'image_primary', 'article_section', 'thumbnail', 'decsription'];

    public function getDynamicSEOData(): SEOData
    {
        // Override only the properties you want:
        return new SEOData(
            title: $this->title,
        );
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function taggable()
    {
        return $this->morphTo();
    }

    /**
     * Get instance of tag linked to the tagged value
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
//    public function tag()
//    {
//        $model = TaggingUtility::tagModelString();
//        return $this->belongsTo($model, 'tag_slug', 'slug');
//    }
}
