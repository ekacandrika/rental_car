<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PajakLocal extends Model
{
    use HasFactory;

    protected $table = 'pajak_locals';
    protected $guarded = ['id'];
}
