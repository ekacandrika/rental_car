<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MerekMobil extends Model
{
    use HasFactory;
    protected $table = 'merek_mobil';
    protected $hidden = [
        'jenis_id'
    ];

    public function jenismobil()
    {
        return $this->belongsTo(JenisMobil::class, 'jenis_id');
    }

    protected $fillable = [
        'id',
        'user_id',
        'jenis_id',
        'merek_mobil',
        'plat_mobil'
    ];
}
