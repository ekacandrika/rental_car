<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Masterkamar extends Model
{
    use HasFactory;

    protected $table = 'master_kamars';

    protected $guarded = [];

    protected $casts = [
        'refundable' => 'array',
        'refundable_amount' => 'array',
        'refundable_price' => 'array',
    ];
}
