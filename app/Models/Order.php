<?php

namespace App\Models;

use Conner\Tests\Tagging\Book;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'order';
    protected $guarded = [];

    public function bookings()
    {
        return $this->belongsTo(BookingOrder::class, 'booking_id');
    }
}
