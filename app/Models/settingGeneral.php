<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class settingGeneral extends Model
{
    use HasFactory;
    protected $fillable = ['section','title_section','isi_section'];
    protected $casts = [
        'title_section' => 'array',
        'isi_section' => 'array',
    ];
//    protected function data(): Attribute
//
//    {
//
//        return Attribute::make(
//            get: fn ($value) => json_decode($value, true),
//            set: fn ($value) => json_encode($value),
//        );
//    }
}
