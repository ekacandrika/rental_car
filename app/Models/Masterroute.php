<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Masterroute extends Model
{
    use HasFactory;

    protected $table = 'masterroutes';

    protected $guarded = ['id'];

    public function kecamatan()
    {
        return $this->belongsTo(District::class);
    }
}
