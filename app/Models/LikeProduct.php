<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use Product;
// use User;

class LikeProduct extends Model
{
    use HasFactory;

    protected $table = 'like_product';

    protected $fillable=[
        'product_id','user_id','status'
    ];

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
