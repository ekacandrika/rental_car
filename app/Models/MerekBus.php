<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MerekBus extends Model
{
    use HasFactory;
    protected $table = 'merek_bus';
    protected $hidden = [
        'user_id',
        'id_jenis'
    ];

    public function jenisbus()
    {
        return $this->hasMany(JenisBus::class);
    }

    protected $fillable = [
        'user_id',
        'id_jenis',
        'merek_bus',
        'plat_bus'
    ];
}
