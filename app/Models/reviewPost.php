<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class reviewPost extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function detailObjek(){
        return $this->belongsTo('detailObjek');
    }
}
