<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class formulirProdukHotel extends Model
{
    use HasFactory;
    protected $fillable = ['checkIn'
    ,'checkOut'
    ,'lokasi'
    ,'jenisHotel'
    ,'fasilitas'
    ,'kebutuhan'
    ] ;
    protected $casts = [
        'jenisMakan' => 'array',
        'jumlahOrang' => 'array',
        'jumlahOrangPerkamar' => 'array',
        'jenisKamar' => 'array'
    ] ;
}
