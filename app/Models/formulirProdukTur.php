<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class formulirProdukTur extends Model
{
    use HasFactory;

    protected $fillable = ['tanggalMulai',
        'waktuMulai',
        'lokasi',
        'dewasa',
        'anak',
        'durasi',
        'balita',
        'transportasi',
        'jumlahPeserta',
        'makanan',
        'namaObjek',
    ];
    protected $casts = ['jenisKendaraan' => 'array',
        'deskripsiCustom' => 'array'];
}
