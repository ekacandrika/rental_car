<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class settingHomepages extends Model
{
    use HasFactory;
    protected $table = 'setting_homepages';
    protected $fillable = [
        'title_section',
        'image_section',
        'detail_section',
        'status_section',
        'section'
    ];
}
