<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Allbanner extends Model
{
    use HasFactory;
    protected $table = 'all_banner';
    protected $fillable = [
        'user_id', 
        'banner_code',
        'type',
    ];
}
