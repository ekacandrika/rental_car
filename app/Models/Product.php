<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    use HasFactory;
    use Sluggable;
    use \Conner\Tagging\Taggable;


    protected $table = 'product';
    protected $guarded = [];

    public function productdetail()
    {
        return $this->hasOne(Productdetail::class, 'product_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function type(){
        return 'type';
    }
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['product_code', 'product_name']
            ]
        ];
    }

    public function taggable()
    {
        return $this->morphTo();
    }
}
