<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisMobil extends Model
{
    use HasFactory;
    protected $table = 'jenis_mobil';

    public function merekmobil()
    {
        return $this->hasMany(Merekmobil::class);
    }

    public function mobildetail()
    {
        return $this->hasMany(Mobildetail::class);
    }

    protected $fillable = [
        'user_id',
        'jenis_kendaraan'
    ];
}
