<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Productdetail extends Model
{
    use HasFactory;

    use Searchable {
        Searchable::search as parentSearch;
     }     

    protected $table = 'product_detail';

    protected $guarded = [];

    public function harga()
    {
        return $this->belongsTo(Price::class);
    }

    public function diskon()
    {
        return $this->belongsTo(Discount::class, 'discount_id');
    }

    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount_id');
    }

    public function mobildetail()
    {
        return $this->belongsTo(Mobildetail::class);
    }

    public function detailkendaraan()
    {
        return $this->belongsTo(DetailKendaraan::class, 'id_detail_kendaraan');
    }

    public function masterkamar()
    {
        return $this->belongsTo(Masterkamar::class, 'kamar_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class, 'regency_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function toSearchableArray()
    {
    // no need to return values as the database engine only uses the array keys
        return [
            'product.product_name' => '',
            'product.type' => '',
        ];
    }

    public static function search($query = '', $callback = null)
    {
        return static::parentSearch($query, $callback)->query(function ($builder) {
            $builder->join('product', 'product_detail.product_id', '=', 'product.id')->with('product'
                    , 'product.user', 'DetailKendaraan', 'DetailKendaraan.jenismobil', 'DetailKendaraan.merekmobil'
                )
                ->select('product_detail.*');
        });
    }


}
