<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengajuanPembatalan extends Model
{
    use HasFactory;

    protected $table = 'pengajuan_pembatalans';
    protected $guarded = ['id'];
}
