<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detailpesanan extends Model
{
    use HasFactory;

    protected $table = 'detailpesanans';

    protected $guarded = ['id'];

    public function productdetail()
    {
        return $this->belongsTo(Productdetail::class, 'product_detail_id');
    }
}
