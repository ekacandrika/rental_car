<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisBus extends Model
{
    use HasFactory;
    protected $table = 'jenis_bus';
    
    // public function merekbus()
    // {
    //     return $this->hasMany(MerekBus::class);
    // }
    
    // public function busdetail()
    // {
    //     return $this->hasMany(BusDetail::class);
    // }

    protected $fillable = [
        'user_id',
        'jenis_kendaraan'
    ];
}
