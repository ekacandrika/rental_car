<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LayoutBus extends Model
{
    use HasFactory;

    protected $table = 'layout_kursi';
    
    public function user()
    {
        return $this->hasMany(User::class);
    }
    protected $hidden = [
        'id_layout',
        'user_id'
    ];

    protected $fillable = [
        'user_id',
        'jumlah_kursi',
        'layout_kursi',
        'lantai_bus',
        'kursi_1', 'kursi_2', 'kursi_3', 'kursi_4', 'kursi_5', 'kursi_6',
        'kursi_7', 'kursi_8', 'kursi_9', 'kursi_10', 'kursi_11', 'kursi_12',
        'kursi_13', 'kursi_14', 'kursi_15', 'kursi_16', 'kursi_17', 'kursi_18',
        'kursi_19', 'kursi_20', 'kursi_21', 'kursi_22', 'kursi_23', 'kursi_24',
        'kursi_25', 'kursi_26', 'kursi_27', 'kursi_28', 'kursi_29', 'kursi_30',
        'kursi_31', 'kursi_32', 'kursi_33', 'kursi_34', 'kursi_35', 'kursi_36',
        'kursi_37', 'kursi_38', 'kursi_39', 'kursi_40',
        'kursi_41', 'kursi_42', 'kursi_43', 'kursi_44', 'kursi_45', 'kursi_46',
        'kursi_47', 'kursi_48', 'kursi_49', 'kursi_50',
        'kursi_51', 'kursi_52', 'kursi_53', 'kursi_54', 'kursi_55', 'kursi_56',
        'kursi_57', 'kursi_58', 'kursi_59', 

        'harga_1', 'harga_2', 'harga_3', 'harga_4', 'harga_5', 'harga_6',
        'harga_7', 'harga_8', 'harga_9', 'harga_10', 'harga_11', 'harga_12',
        'harga_13', 'harga_14', 'harga_15', 'harga_16', 'harga_17', 'harga_18',
        'harga_19', 'harga_20', 'harga_21', 'harga_22', 'harga_23', 'harga_24',
        'harga_25', 'harga_26', 'harga_27', 'harga_28', 'harga_29', 'harga_30',
        'harga_31', 'harga_32', 'harga_33', 'harga_34', 'harga_35', 'harga_36',
        'harga_37', 'harga_38', 'harga_39', 'kursi_40',
        'kursi_41', 'kursi_42', 'kursi_43', 'kursi_44', 'kursi_45', 'kursi_46',
        'kursi_47', 'kursi_48', 'kursi_49', 'kursi_50',
        'kursi_51', 'kursi_52', 'kursi_53', 'kursi_54', 'kursi_55', 'kursi_56',
        'kursi_57', 'kursi_58', 'kursi_59',
        'kolom_kursi','baris_kursi','judul_layout','harga_kursi'
    ];
}
