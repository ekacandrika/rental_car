<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class formulirProdukTransfer extends Model
{
    use HasFactory;

    protected $fillable = ['tanggalPergi'
    ,'jamPergi'
    ,'transferDari'
    ,'kotaDari'
    ,'provinsiDari'
    ,'embedMapsDari'
    ,'transferKe'
    ,'kotaKe'
    ,'provinsiKe'
    ,'embedMapsKe'
    ,'coupon_question'
    ,'tanggalPulang'
    ,'jamPulang'
    ,'transport'
    ,'dewasa'
    ,'anak'
    ,'balita'
    ,'barangBesar'
    ,'barangKecil'
    ,'memo'
    ,'kamtuu'] ;
}
