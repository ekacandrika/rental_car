<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Hotel extends Model
{
    use HasFactory;
    use Sluggable;

    protected $table = 'product';

    protected $guarded = [];

    public function productdetail()
    {
        return $this->hasOne(Productdetail::class, 'product_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['product_name', 'product_code']
            ]
        ];
    }
}
