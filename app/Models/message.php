<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class message extends Model
{
    use HasFactory;

    protected $casts = ['created_at' => 'datetime'];

    public function detailMessage()
    {
        return $this->belongsTo(detailMessage::class, 'detail_message_id', 'id');
    }
}
