<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterDocumentValue extends Model
{
    use HasFactory;

    protected $table ='master_document_value';

    protected $guarded = ['id'];

    public function masterdocumentlegal(){
        return $this->belongsTo(MasterDocumentLegal::class,'doc_legal_id');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
