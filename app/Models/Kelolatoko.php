<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelolatoko extends Model
{
    use HasFactory;
    protected $table = 'kelolatokos';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */

    protected $fillable = [
        'id_seller',
        'nama_toko',
        'logo_toko',
        'banner_toko',
        'nama_banner',
        'deskripsi',
        'deskripsi_tambahan',
        'tentang_toko',
        'official_store',
        'lokasi_toko',
        'tautan_banner',
        'banner_one', 
        'display_order_one',
        'banner_two', 
        'display_order_two',
        'banner_three', 
        'display_order_three',
        'banner_four', 
        'display_order_four',
        'banner_five', 
        'display_order_five'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */

    protected $appends = [
        'logo_toko',
    ];

    public function user(){
        return $this->belongsTo(User::class,'id_seller');
    }
}
