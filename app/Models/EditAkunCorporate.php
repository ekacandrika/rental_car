<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EditAkunCorporate extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'nama_depan',
        'nama_belakang',
        'alamat_email',
        'no_telepon',
        'negara',
        'nama_dagang',
        'nama_perusahaan',
        'no_telp',
        'alamat',
        'kota',
        'kode_pos',
        'website'
    ];
}
