<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingOrder extends Model
{
    use HasFactory;

    protected $table = 'booking';

    protected $guarded = ['id'];

    public function productdetail()
    {
        return $this->belongsTo(Productdetail::class, 'product_detail_id');
    }

    public function users()
    {
        return $this->belongsTo(Usertraveller::class, 'customer_id');
    }

    public function orders()
    {
        return $this->belongsTo(Order::class);
    }
}
