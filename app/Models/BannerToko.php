<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerToko extends Model
{
    use HasFactory;
    protected $table = 'banner_toko';
    protected $fillable = [
        'all_banner',
        'banner', 
        'link_banner',
        'display_order',
    ];

    public function allbanner()
    {
        return $this->hasMany(Allbanner::class, 'all_banner');
    }
}
