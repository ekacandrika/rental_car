<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusDetail extends Model
{
    use HasFactory;

    protected $table = 'detail_bus';

    protected $hidden = [
        'id_jenis'
    ];
    
    protected $fillable = [
        'id_jenis',
        'nama_bus',
        'kapasitas_kursi',
        'kapasitas_koper',
        'status'
    ];
    public function jenisbus()
    {
        return $this->belongsTo(JenisBus::class);
    }
}
