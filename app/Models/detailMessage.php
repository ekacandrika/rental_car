<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class detailMessage extends Model
{
    use HasFactory;

    protected $fillable = ['user1', 'user2', 'text', 'receiverFrom', 'sendTo'];


    protected $casts = ['created_at' => 'datetime'];

    public function item()
    {
        return $this->hasMany(message::class, 'item_id', 'id');
    }
    public function messages()
    {
        return $this->hasMany(message::class, 'detail_message_id');
    }
    public function users()
    {
        return $this->belongsTo(User::class, 'user');
    }
    public function produks()
    {
        return $this->belongsTo(Product::class, 'produk_id');
    }

}
