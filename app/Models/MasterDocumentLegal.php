<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterDocumentLegal extends Model
{
    use HasFactory;

    protected $table='master_document_legal';

    protected $guarded = ['id'];

    public function masterdocumentvalue(){
        return $this->hasOne(MasterDocumentValue::class,'id');
    }
}
