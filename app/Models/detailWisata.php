<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use RalphJSmit\Laravel\SEO\Support\HasSEO;
use Cviebrock\EloquentSluggable\Sluggable;

class detailWisata extends Model
{
    use HasFactory;
    use HasSEO;
    use Sluggable;
    use \Conner\Tagging\Taggable;

    protected $fillable = [
        'title',
        'nama_wisata',
        'header',
        'kategori',
        'hargaTiket',
        'hari',
        'jamOperasional',
        'transportasi',
        'deskripsi',
        'embed_maps',
        'thumbnail',
        'gallery_pulau',
        'slug'
    ];

    public function getDynamicSEOData(): SEOData
    {
        // Override only the properties you want:
        return new SEOData(
            title: $this->title,
        );
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function taggable()
    {
        return $this->morphTo();
    }
}
