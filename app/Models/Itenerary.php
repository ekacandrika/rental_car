<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Itenerary extends Model
{
    use HasFactory;

    protected $table = 'itenenary';

    protected $fillable = [
        'paket_id',
        // product_id
        'judul_itenenary',
        'deskripsi_itenenary',
        'gallery_itenenary',
        'tautan_video'
    ];
}
