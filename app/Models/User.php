<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'first_name',
        'jenis_registrasi',
        'role',
        'email',
        'no_tlp',
        'lisensi',
        'jk',
        'password',
        'profile_photo_path',
        'whatsapp_no',
        'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    public function layoutbus()
    {
        return $this->belongsTo(LayoutBus::class);
    }

    public function masterdriver()
    {
        return $this->hasMany(MasterDriver::class, 'id_seller');
    }

    public function kelolatoko()
    {
        return $this->hasMany(Kelolatoko::class, 'id_seller');
    }

    public function allbanner()
    {
        return $this->hasMany(Allbanner::class, 'user_id');
    }

    public function product()
    {
        return $this->hasMany(Product::class);
    }

    public function masterdocumentvalue(){
        return $this->hasMany(MasterDocumentValue::class);
    }
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];
}
