<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterDriver extends Model
{
    use HasFactory;
    protected $table = 'master_driver';
    protected $guarded = [];

    protected $fillable = [
        'id_seller',
        'nama_driver',
        'no_telp',
        'alamat'
    ];
}
