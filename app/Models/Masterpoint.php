<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Masterpoint extends Model
{
    use HasFactory;

    protected $table = 'titik_route';
    
    protected $guarded = ['id'];

    public $timestamps = false;

    public function district(){
        return $this->belongsTo(District::class);
    }

}
