<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usertraveller extends Model
{
    use HasFactory;

    protected $table = 'user_traveller';
    protected $guarded = [];
    protected $casts = ['kupon_code' => 'array'];

    public function booking()
    {
        return $this->hasMany(BookingOrder::class);
    }
}
