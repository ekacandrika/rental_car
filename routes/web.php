<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KuponController;
use Database\Seeders\SettingGeneralSeeder;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\homepagesController;
use App\Http\Controllers\SosialiteController;
use App\Http\Controllers\DetailTokoController;
use App\Http\Controllers\PajakAdminController;
use App\Http\Controllers\UserdetailController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Agent\AgentController;
use App\Http\Controllers\MasterpointController;
use App\Http\Controllers\Agent\CouponController;
use App\Http\Controllers\Seller\HotelController;
use App\Http\Controllers\Seller\XstayController;
use App\Http\Controllers\Seller\SellerController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\FormulirProdukController;
use App\Http\Controllers\MasterDropPickController;
use App\Http\Controllers\SettingGeneralController;
use App\Http\Controllers\Payment\PaymentController;
use App\Http\Controllers\Agent\AgentOrderController;
use App\Http\Controllers\Produk\TurProdukController;
use App\Http\Controllers\Seller\PengajuanController;
use App\Http\Controllers\SettingHomepagesController;
use App\Http\Controllers\Admin\MasterrouteController;
use App\Http\Controllers\DestindonesiaHomeController;
use App\Http\Controllers\EmailVerificationController;
use App\Http\Controllers\Seller\KelolatokoController;
use App\Http\Controllers\Traveller\ProfileController;
use App\Http\Controllers\Produk\HotelProdukController;
use App\Http\Controllers\Produk\XstayProdukController;
use App\Http\Controllers\Admin\DocumentLegalController;
use App\Http\Controllers\Produk\RentalProdukController;
use App\Http\Controllers\Seller\DataPengajuanController;
use App\Http\Controllers\Traveller\NewsletterController;
use App\Http\Controllers\Produk\ActivityProdukController;
use App\Http\Controllers\Produk\TransferProdukController;
use App\Http\Controllers\Seller\Tur\ListingTurController;
use App\Http\Controllers\Traveller\DataPesananController;
use App\Http\Controllers\Traveller\InfoBookingController;
use App\Http\Controllers\Traveller\BookingOrderController;
use App\Http\Controllers\SettingGeneralTravellerController;
use App\Http\Controllers\Superuser\DestindonesiaController;
use App\Http\Controllers\Seller\Tur\EditListingTurController;
use App\Http\Controllers\Traveller\DeleteTravellerController;
use App\Http\Controllers\Seller\Rental\ListingRentalController;
use App\Http\Controllers\Traveller\TravellerPasswordController;
use App\Http\Controllers\Seller\Transfer\MasterDriverController;
use App\Http\Controllers\Traveller\PengajuanPembatalanController;
use App\Http\Controllers\Seller\Rental\EditListingRentalController;
use App\Http\Controllers\Seller\Transfer\ListingTransferController;
use App\Http\Controllers\Seller\Activity\AddListingActivityController;
use App\Http\Controllers\Seller\Activity\EditListingActivityController;
use App\Http\Controllers\Seller\Transfer\EditListingTransferController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Destindonesia
Route::get('/destindonesia', [DestindonesiaHomeController::class, 'show'])->name('destindonesia.index');
Route::get('/destindonesia/pulaus/{id}', [DestindonesiaHomeController::class, 'showById'])->name('destindonesia.pulau');
Route::prefix('destindonesia')->group(function () {
    // Route::get('/', function () {
    //     return view('Destindonesia.index');
    // });

    // Route::get('/kota', function () {
    //     return view('Destindonesia.kota-kabupaten');
    // });
    // });
    Route::get('/list', [DestindonesiaHomeController::class, 'listDesti'])->name('destindonesia.list');
    Route::get('/{id}', [DestindonesiaHomeController::class, 'provDestindonesia'])->name('destindonesia.prov');

    Route::get('/kabupaten/{slug}', [SettingGeneralController::class, 'kabupatenShow'])->name('kabupatenShow');

    // Route::get('/provinsi', function () {
    //     return view('Destindonesia.provinsi');
    // });
    Route::get('/provinsi/{slug}', [SettingGeneralController::class, 'provinsiShow'])->name('provinsiShow');
    Route::get('/provinsi/{slug}/all', [SettingGeneralController::class, 'allProvinsiShow'])->name('provinsiShow.all');

    Route::get('/pulau/{slug}', [SettingGeneralController::class, 'pulauShow'])->name('pulauShow');
    Route::get('/pulau/{slug}/all', [SettingGeneralController::class, 'allPulauShow'])->name('pulauShow.all');
    Route::post('/pulau/add-review', [SettingGeneralController::class, 'reviewObjekStore'])->name('reviewObjek.store');

    // Route::get('/pulau', function () {
    //     return view('Destindonesia.pulau');
    // });

    // Route::get('/wisata', function () {
    //     return view('Destindonesia.wisata');
    // });

    Route::get('wisata/{slug}', [SettingGeneralController::class, 'wisataShow'])->name('wisataShow');
    Route::get('wisata/{slug}/all', [SettingGeneralController::class, 'allWisataShow'])->name('wisataShow.all');

    Route::get('/newsletter/{slug}', [ArticleController::class, 'show'])->name('newsletter.show');
    Route::get('/newsletters', [ArticleController::class, 'newsletter'])->name('newsletter.destindonesia');
    Route::get('/newsletter/category/{tag_slug}', [ArticleController::class, 'newsletterCategory'])->name('newsletter.category');

    Route::get('/article', function () {
        return view('Destindonesia.Article.post');
    });

    Route::get('/article/profile', function () {
        return view('Destindonesia.Article.profile-writer');
    });

    Route::get('/map', function () {
        return view('Destindonesia.map');
    });

    // Route::get('/list',[DestindonesiaHomeController::class,'listDesti'])->name('destindonesia.listDesti');
});

// Homepage
// Route::get('/new', [homepagesController::class, 'index'])->name('welcome');
Route::get('/', [homepagesController::class, 'indexNew'])->name('welcome-new');
Route::get('/home/new', [homepagesController::class, 'index']);

Route::get('/home', [homepagesController::class, 'indexNew']);
Route::get('/home/list-formulir', function () {
    return view('Homepage.formulir-list');
})->name('list-formulir');
Route::get('/home/list-kategori', function () {
    return view('Homepage.list-kategori');
});
Route::get('/kamtuu/{id}', [SettingHomepagesController::class, 'show'])->name('section.show');
Route::post('/getPromo', [homepagesController::class, 'getTransfer'])->name('getPromo');
// Route::get('/likes',[ProfileController ::class,'getLikes'])->name('likes');
Route::post('/like', [homepagesController::class, 'submitLike'])->name('submit.like');

// Auth::routes(['verify' => true]);
Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');


Route::group(['middleware' => ['auth', 'CekRole:super user kamtuu']], function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});

Route::group(['middleware' => ['auth', 'CekRole:user kamtuu produk']], function () {
    Route::get('/dashboard/user/produk', function () {
        return view('BE.user.dashboard-user');
    })->name('dashboarduser');
});

Route::group(['middleware' => ['auth', 'CekRole:user kamtuu handle pembayaran']], function () {
    Route::get('/dashboard/user/handlepembayaran', function () {
        return view('dashboarduserbayar');
    })->name('dashboardbayar');
});

Route::group(['middleware' => ['auth', 'CekRole:agent']], function () {
    Route::get('/agent/laporan', [AgentController::class, 'viewLaporanAgent'])->name('viewLaporanAgent');
    Route::get('/agent/myOrder', [AgentController::class, 'viewMyOrder'])->name('viewMyOrder');
    Route::get('/agent/myAccount', [AgentController::class, 'viewMyAccount'])->name('viewMyAccount');
    Route::get('/agent/detailOrder/{id}', [AgentController::class, 'viewDetailOrder'])->name('viewDetailOrder');

    // Update Profile
    Route::put('/agent/updateAccountManager/{id}', [AgentController::class, 'updateAccountManager'])->name('updateAccountManager');
    Route::put('/agent/updateAccountAgent/{id}', [AgentController::class, 'updateAccountAgent'])->name('updateAccountAgent');


    Route::get('/dashboard/agent', function () {
        return view('BE.agent.dashboard-agent');
    })->name('dashboardagent')->middleware(['auth', 'verified']);
    Route::get('/dashboard-agent/my-account', function () {
        return view('BE.agent.my-account-agent');
    });
    Route::get('/dashboard-agent/my-orders', function () {
        return view('BE.agent.my-order-agent');
    })->name('my-order.index');
    Route::get('/dashboard-agent/detail-order', function () {
        return view('BE.agent.detail-order');
    });

    Route::get('/agent/laporan', [AgentController::class, 'viewLaporanAgent'])->name('viewLaporanAgent');
    Route::get('/agent/myOrder', [AgentController::class, 'viewMyOrder'])->name('viewMyOrder');
    Route::get('/agent/myAccount', [AgentController::class, 'viewMyAccount'])->name('viewMyAccount');
    Route::get('/agent/detailOrder/{id}', [AgentController::class, 'viewDetailOrder'])->name('viewDetailOrder');

    // Update Profile
    Route::put('/agent/updateAccountManager/{id}', [AgentController::class, 'updateAccountManager'])->name('updateAccountManager');
    Route::put('/agent/updateAccountAgent/{id}', [AgentController::class, 'updateAccountAgent'])->name('updateAccountAgent');

    // Route::get('/dashboard-agent/kupon-agent',[CouponController::class,'index'])->name('coupon.index');
    // Route::post('/dashboard-agent/kupon-agent/add',[CouponController::class,'store'])->name('coupon.store');
    // Route::get('/dashboard-agent/kupon-agent/{id}', [CouponController::class,'edit'])->name('coupon.edit');
    // Route::put('/dashboard-agent/kupon-agent/{id}', [CouponController::class,'update'])->name('coupon.update');
    // Route::delete('/dashboard-agent/kupon-agent/{id}', [CouponController::class,'delete'])->name('coupon.delete');
    // Route::get('/dashboard-agent/kupon-agent/print/{id?}', [CouponController::class,'pdf'])->name('coupon.print');
    // Route::get('/dashboard-agent/kupon-agent/download/{id?}', [CouponController::class,'download'])->name('coupon.download');
    // Route::get('/dashboard-agent/kupon-agent/emailing/{id?}', [CouponController::class,'emailing'])->name('coupon.emaling');
    // Route::get('/dashboard-agent/kupon-agent/show/{id?}', [CouponController::class,'show'])->name('coupon.show');

    Route::get('/dashboard-agent/kupon-agent', [AgentOrderController::class, 'index'])->name('coupon.index');
    Route::post('/dashboard-agent/kupon-agent/add', [AgentOrderController::class, 'store'])->name('coupon.store');
    Route::get('/dashboard-agent/kupon-agent/{id}', [AgentOrderController::class, 'edit'])->name('coupon.edit');
    Route::put('/dashboard-agent/kupon-agent/{id}', [AgentOrderController::class, 'update'])->name('coupon.update');
    Route::delete('/dashboard-agent/kupon-agent/{id}', [AgentOrderController::class, 'delete'])->name('coupon.delete');
    Route::get('/dashboard-agent/kupon-agent/print/{id?}', [AgentOrderController::class, 'pdf'])->name('coupon.print');
    Route::get('/dashboard-agent/kupon-agent/download/{id?}', [AgentOrderController::class, 'download'])->name('coupon.download');
    Route::get('/dashboard-agent/kupon-agent/emailing/{id?}', [AgentOrderController::class, 'emailing'])->name('coupon.emaling');
    Route::get('/dashboard-agent/kupon-agent/show/{id?}', [AgentOrderController::class, 'show'])->name('coupon.show');

    Route::get('/dashboard-agent/password', function () {
        return view('BE.agent.password-agent');
    });
    Route::get('/dashboard-agent/add-user-agent',[AgentController::class,'addUserAgent']);
});

// Dashboard Seller
Route::group(['middleware' => ['auth', 'CekRole:seller']], function () {
    Route::get('/dashboard/seller', [SellerController::class, 'viewLaporanSeller'])->name('dashboardseller')->middleware(['auth', 'verified']);
    Route::get('/listpesanan/seller', [SellerController::class, 'listPesanan'])->name('listPesanan');
    Route::get('/laporan/seller', [SellerController::class, 'dashboard_seller'])->name('viewLaporanSeller');
    Route::get('/konfirmasi/list-pesanan', [SellerController::class, 'confirmListPesanan'])->name('confirmListPesanan');
    Route::post('/radio-button/{id}/update-status', [SellerController::class, 'switchValue'])->name('switch.value');

    // List Pesanan Seller
    Route::put('/setujuiPermintaan/{id}', [SellerController::class, 'updateStatusPermintaanPembatalan'])->name('updateStatusPermintaanPembatalan');
    Route::post('/batalkanPesanan', [SellerController::class, 'storeBatalkanPesanan'])->name('storeBatalkanPesanan');

    // Pengajuan Penarikan
    Route::get('/UploadDiscovery/{id}', [PengajuanController::class, 'viewUploadDiscovery'])->name('viewUploadDiscovery');
    Route::get('/UploadTandaTerima/{id}', [PengajuanController::class, 'viewUploadTandaTerima'])->name('viewUploadTandaTerima');
    Route::put('/updateDiscovery/{id}', [PengajuanController::class, 'updateDiscovery'])->name('updateDiscovery');
    Route::put('/updateTandaTerima/{id}', [PengajuanController::class, 'updateTandaTerima'])->name('updateTandaTerima');
    Route::post('/pengajuanpenarikan/{id}', [DataPengajuanController::class, 'store'])->name('store.datapengajuan');

    // Kelola Toko
    Route::get('/dashboard/kelola-toko', [KelolatokoController::class, 'index'])->name('kelolatoko');
    Route::get('/dashboard/kelola-toko/dekor-toko', [KelolatokoController::class, 'dekorasiToko'])->name('kelolatoko.DekorasiToko');
    Route::put('/dashboard/kelola-toko/dekor-toko', [KelolatokoController::class, 'addDekorasiToko'])->name('kelolatoko.addDekorasiToko');
    // Route::get('kelola-toko/dekor-toko/edit/{id}',[KelolatokoController::class, 'showDekorasiToko'])->name('kelolatoko.editDekorasiToko');  
    Route::get('kelola-toko/dekor-toko/edit/{id}', [KelolatokoController::class, 'showDekorasiToko'])->name('kelolatoko.viewEditDekorasiToko');
    Route::put('kelola-toko/dekor-toko/update/{id}', [KelolatokoController::class, 'editDekorasiToko'])->name('kelolatoko.editDekorasiToko');
    Route::delete('/hapus/banner/{id}',[KelolaTokoController::class,'deleteBanner'])->name('delete.banner');
    // Kelola Toko
    Route::get('/dashboard/kelolaToko', [SellerController::class, 'index'])->name('dashboardkelolatoko');
    Route::get('/dashboard/myListing', [SellerController::class, 'myListing'])->name('dashboardmyListing');
    Route::delete('/delete/listing/{id}', [SellerController::class, 'deleteListing'])->name('deleteListing');
    Route::get('/dahsboard/mylisting/update/status', [KelolatokoController::class, 'updateStatus'])->name('updateStatus');
    Route::put('/update/status/{id}', [KelolatokoController::class, 'updateStatus'])->name('updateStatus');
    Route::delete('/delete/listing/{id}', [SellerController::class, 'deleteListing'])->name('deleteListing');
    Route::post('/update/toko/{id}', [SellerController::class, 'updateToko'])->name('seller.update-toko');

    // buat cabang
    Route::get('/create/branch', [SellerController::class, 'createBranchStore'])->name('seller.create-branch');
    Route::post('/store/branch', [SellerController::class, 'storeBranchStore'])->name('seller.store-branch');
    Route::get('/edit/branch/{id}', [SellerController::class, 'editBranchStore'])->name('seller.edit-branch');
    Route::put('/update/branch/{id}', [SellerController::class, 'updateBranchStore'])->name('seller.update-branch');
    Route::delete('/delete/branch/{id}', [SellerController::class, 'deleteBranchStore'])->name('seller.delete-branch');


    Route::get('/dashboard/kelola-toko/banner-toko', [KelolatokoController::class, 'viewBannerToko'])->name('kelolatoko.bannerToko');
    Route::post('/dashboard/kelola-toko/banner-toko', [KelolatokoController::class, 'addBannerToko'])->name('kelolatoko.addBannerToko');
    Route::post('/dashboard/kelola-toko/banner-toko/upload', [KelolatokoController::class, 'tmpUpload'])->name('kelolatoko.tmpUpload');
    Route::post('/dashboard/kelola-toko/reorder-banner', [KelolatokoController::class, 'reorder'])->name('kelolatoko.reorder');

    //Detail Toko
    // Route::get('/detail-toko', [DetailTokoController::class,'show'])->name('transfer.show');

    Route::prefix('detail-toko')->group(function () {
        // detailToko
        Route::get('/', [SellerController::class,'detailToko'])->name('detail.toko');
        
        Route::get('/formulir', function () {
            return view('Produk.Transfer.formulir');
        });
        Route::get('/search', function () {
            return view('Produk.Transfer.search');
        });
        // Route::get('/detail', function () {
        //     return view('Produk.Transfer.detail');
        // });
        Route::get('/pemesanan-bandara', function () {
            return view('Produk.Transfer.form-private');
        });
        Route::get('/pemesanan', function () {
            return view('Produk.Transfer.form-pemesanan');
        });
        Route::get('/detail-jadwal', function () {
            return view('Produk.Transfer.detail-schedule');
        });
    });

    // Profile Traveller
    Route::get('/dashboard/profile/setting/{role}', [UserdetailController::class, 'edit'])->name('seller.profilesetting');
    Route::put('/dashboard/profile/updatedatadiri/{id}', [UserdetailController::class, 'update_data_diri'])->name('seller.update.data.diri');
    Route::put('/dashboard/profile/updatekategoriseller/{id}', [UserdetailController::class, 'update_kategori_seller'])->name('seller.update.kategori.seller');
    Route::put('/dashboard/profile/updatelisensipersonal/{id}', [UserdetailController::class, 'update_lisensi_personal'])->name('seller.update.lisensi.personal');
    Route::put('/dashboard/profile/updatealamat/{id}', [UserdetailController::class, 'update_alamat'])->name('seller.update.alamat');
    Route::put('/dashboard/profile/updateidentitas/{id}', [UserdetailController::class, 'update_identitas'])->name('seller.update.identitas');
    Route::put('/dashboard/profile/updatesecondemail/{id}', [UserdetailController::class, 'update_second_email'])->name('seller.update.second.email');
    Route::put('/dashboard/profile/updatesecondnohp/{id}', [UserdetailController::class, 'update_second_no_hp'])->name('seller.update_second_no_hp');
    Route::delete('/dashboard/profile/deleteaccount/{id}', [UserdetailController::class, 'destroy'])->name('seller.destroy.account');
    Route::get('/dashboard/reset-password', [ForgotPasswordController::class, 'create'])->name('seller.resetpassword');
    Route::post('/forget-password', [ForgotPasswordController::class, 'store'])->name('forget.password.post');
    Route::get('/reset-password/{token}', [ForgotPasswordController::class, 'edit'])->name('reset.password.get');
    Route::post('/reset-password', [ForgotPasswordController::class, 'update'])->name('reset.password.post');
    Route::get('/dashboard/newsletter', function () {
        return view('BE.seller.newsletter');
    })->name('seller.newsletter');
    Route::get('/dashboard/hapus-akun', function () {
        return view('BE.seller.hapus-akun');
    })->name('seller.hapus.akun');

    //company profile
    Route::put('/dashboard/profile/updatejenistoko/{id}', [UserdetailController::class, 'update_jenis_toko'])->name('seller.update.jenis.toko');
    Route::put('/dashboard/profile/update/data-perusahaan/{id}', [UserdetailController::class, 'update_data_perusahaan'])->name('seller.update.data.perusahaan');
    Route::put('/dashboard/profile/update/data-penanggungjawab/{id}', [UserdetailController::class, 'update_penanggung_jawab'])->name('seller.update.data.penanggungjawab');
    Route::put('/dashboard/profile/update/data-dokumen/{id}', [UserdetailController::class, 'update_dokumen'])->name('seller.update.data.dokumen');

    // Produk Hotel
    Route::get('/hotel/informasidasar', [HotelController::class, 'info_dasar'])->name('hotelinfodasar');
    Route::post('/hotel/store', [HotelController::class, 'store'])->name('hotel.store');
    Route::get('/hotel/informasidasar/{id}', [HotelController::class, 'edit'])->name('hotel.edit');
    Route::put('/hotel/informasidasar/update/{id}', [HotelController::class, 'update'])->name('hotel.update');

    Route::get('hotel/informasikamar', [HotelController::class, 'info_kamar'])->name('hotelinfokamar');
    Route::get('hotel/informasikamar/{id}', [HotelController::class, 'info_kamar_edit'])->name('hotel.infokamar.edit');
    Route::put('hotel/informasikamarupdate/{id}', [HotelController::class, 'info_kamar_update'])->name('hotel.infokamar.update');

    Route::get('/hotel/harga', [HotelController::class, 'harga'])->name('hotelharga');
    Route::get('/hotel/harga/{id}', [HotelController::class, 'harga_edit'])->name('hotelharga.edit');
    Route::put('/hotel/harga/diskon-date/{id}', [HotelController::class, 'diskon_date'])->name('hoteldiskondate.update');
    Route::put('/hotel/harga/diskon-grup/{id}', [HotelController::class, 'diskon_grup'])->name('hoteldiskongrup.update');
    Route::put('/hotel/harga/updateResiden/{id}', [HotelController::class, 'harga_update_residen'])->name('hotelharga.update.residen');

    Route::get('/hotel/batasbayar', [HotelController::class, 'batas_bayar'])->name('hotelbatasbayar');
    Route::get('/hotel/batasbayar/{id}', [HotelController::class, 'batas_bayar_edit'])->name('hotelbatasbayar.edit');
    Route::put('/hotel/batasbayar/update/{id}', [HotelController::class, 'batas_bayar_update'])->name('hotelbatasbayar.update');

    Route::get('/hotel/mapsfoto', [HotelController::class, 'maps_foto'])->name('hotelmapsfoto');
    Route::get('/hotel/mapsfoto/{code}', [HotelController::class, 'maps_foto_edit'])->name('hotelmapsfoto.edit');
    Route::put('/hotel/mapsfoto/update/{id}', [HotelController::class, 'maps_foto_update'])->name('hotelmapsfoto.update');
    Route::post('/hotel/mapsfoto/update/gallery/{id}', [HotelController::class, 'maps_photo_gallery'])->name('hotelmapsfotogallery.update');

    Route::get('/hotel/pilihanekstra', [HotelController::class, 'pilihan_ekstra'])->name('hotelpilihanekstra');
    Route::get('/hotel/pilihanekstra/{code}', [HotelController::class, 'pilihan_ekstra_edit'])->name('hotelpilihanekstra.edit');
    Route::put('/hotel/pilihanekstra/update/{id}', [HotelController::class, 'pilihan_ekstra_update'])->name('hotelpilihanekstra.update');

    Route::get('/hotel/faq', [HotelController::class, 'faq'])->name('hotelfaq');
    Route::post('/hotel/faq', [HotelController::class, 'faq_code'])->name('hotelfaq.code');
    Route::get('/hotel/faq/{code}', [HotelController::class, 'faq_edit'])->name('hotelfaq.edit');
    Route::put('/hotel/faq/update/{id}', [HotelController::class, 'faq_update'])->name('hotelfaq.update');

    // Route::get('/hotel/kamar', function () {
    //     return view('BE.seller.hotel.master-kamar');
    // })->name('kamarhotel');
    Route::get('/hotel/kamar', [HotelController::class, 'view_master_kamar'])->name('kamarhotel');
    Route::get('/hotel/kamar/tambah', [HotelController::class, 'view_add_kamar'])->name('kamarhotel.view.tambah');
    Route::post('/hotel/kamar/tambah', [HotelController::class, 'add_kamar'])->name('kamarhotel.tambah');
    Route::post('/hotel/kamar/gallery', [HotelController::class, 'gallery_kamar'])->name('kamarhotel.gallery');
    Route::post('/hotel/kamar/gallery/edit/{id}', [HotelController::class, 'gallery_kamar_edit'])->name('kamarhotel.gallery.edit');
    Route::post('/hotel/kamar/clone/{id}', [HotelController::class, 'clone_kamar'])->name('kamarhotel.clone');
    Route::get('/hotel/kamar/edit/{id}', [HotelController::class, 'view_edit_kamar'])->name('kamarhotel.view.edit');
    Route::put('/hotel/kamar/edit/{id}', [HotelController::class, 'edit_kamar'])->name('kamarhotel.edit');
    Route::delete('/hotel/kamar/delete/{id}', [HotelController::class, 'delete_kamar'])->name('kamarhotel.delete');

    // master kamar
    Route::get('/xstay/kamar', [XstayController::class, 'viewRoom'])->name('kamarxstay');
    Route::get('/xstay/kamar/tambah', [XstayController::class, 'viewAddRoom'])->name('kamarxstay.view.tambah');
    Route::post('/xstay/kamar/tambah', [XstayController::class, 'addRoom'])->name('kamarxstay.tambah');
    Route::post('/xstay/kamar/copy/{id}', [XstayController::class, 'copyRoom'])->name('kamarxstay.copy');
    Route::get('/xstay/kamar/edit/{id}', [XstayController::class, 'viewEditRoom'])->name('kamarxstay.view.edit');
    Route::put('/xstay/kamar/edit/{id}', [XstayController::class, 'editRoom'])->name('kamarxstay.update');
    Route::delete('/xstay/kamar/hapus/{id}', [XstayController::class, 'deleteRoom'])->name('kamarxstay.delete');
    Route::post('/xstay/kamar/galeri', [XstayController::class, 'addRoomGallery'])->name('kamarxstay.gallery');
    Route::post('/xstay/kamar/galeri/{id}', [XstayController::class, 'editRoomGallery'])->name('kamarxstay.edit.gallery');

    // Produk Xstay
    Route::get('xstay/informasidasar', [XstayController::class, 'index'])->name('infodasar');
    Route::get('xstay/informasidasar/{id}', [XstayController::class, 'edit'])->name('infodasar.edit');
    Route::post('xstay/informasidasar', [XstayController::class, 'store'])->name('infodasar.store');
    Route::put('xstay/informasidasar/{id}', [XstayController::class, 'update'])->name('infodasar.update');

    Route::get('xstay/informasikamar', [XstayController::class, 'info_kamar'])->name('xstayinfokamar');
    Route::get('xstay/informasikamar/{id}', [XstayController::class, 'info_kamar_edit'])->name('xstay.infokamar.edit');
    Route::put('xstay/informasikamarupdate/{id}', [XstayController::class, 'info_kamar_update'])->name('xstay.infokamar.update');

    Route::get('xstay/harga', [XstayController::class, 'harga'])->name('harga');
    Route::get('xstay/harga/{id}', [XstayController::class, 'harga_edit'])->name('harga.edit');
    Route::put('xstay/harga/diskon-date/{id}', [XstayController::class, 'harga_update_diskon_date'])->name('harga.diskonDate.update');
    Route::put('xstay/harga/diskon-grup/{id}', [XstayController::class, 'harga_update_diskon_grup'])->name('harga.Grup.update');
    Route::put('xstay/harga/updateResiden/{id}', [XstayController::class, 'hargaResiden_update'])->name('hargaResiden.update');

    Route::get('xstay/bayar', [XstayController::class, 'bayar'])->name('bayar');
    Route::get('xstay/bayar/{id}', [XstayController::class, 'bayar_edit'])->name('bayar.edit');
    Route::put('xstay/bayar/update/{id}', [XstayController::class, 'bayar_update'])->name('bayar.update');

    Route::get('xstay/maps', [XstayController::class, 'maps'])->name('maps');
    Route::get('xstay/maps/{id}', [XstayController::class, 'maps_edit'])->name('maps.edit');
    Route::put('xstay/maps/update/{id}', [XstayController::class, 'maps_update'])->name('maps.update');
    Route::post('xstay/maps/update/gallery/{id}', [XstayController::class, 'maps_photo_gallery'])->name('xstaymapsfotogallery.update');


    Route::get('xstay/ekstra', [XstayController::class, 'ekstra'])->name('ekstra');
    Route::get('xstay/ekstra/{id}', [XstayController::class, 'ekstra_edit'])->name('ekstra.edit');
    Route::put('xstay/ekstra/update/{id}', [XstayController::class, 'ekstra_update'])->name('ekstra.update');

    Route::get('xstay/faq', [XstayController::class, 'faq'])->name('faq.index');
    Route::post('xstay/faq', [XstayController::class, 'faq_code'])->name('faq.code');
    Route::get('xstay/faq/{code}', [XstayController::class, 'faq_edit'])->name('faq.edit');
    Route::put('xstay/faq/update/{id}', [XstayController::class, 'faq_update'])->name('faq.update');

    Route::prefix('dashboard')->group(function () {

        Route::get('/seller/company', function () {
            return view('BE.seller.company-profil');
        });

        // Route::get('/seller/dekorasi/toko', function () {
        //     return view('BE.seller.dekorasi-toko');
        // });
        Route::get('/seller/dekorasi/toko', [KelolatokoController::class, 'index'])->name('dekorasi-toko');

        //  Produk Transfer
        Route::prefix('/seller/transfer')->group(function () {
            Route::get('/add-informasi', [ListingTransferController::class, 'index'])->name('transfer');
            Route::put('/add-informasi', [ListingTransferController::class, 'addListingUmum'])->name('transfer.addListingUmum');
            Route::put('/add-listing', [ListingTransferController::class, 'addListingPrivate'])->name('transfer.addListingPrivate');

            Route::get('/add-rute', [ListingTransferController::class, 'viewRute'])->name('transfer.viewRute');
            Route::post('/add-rute', [ListingTransferController::class, 'addRute'])->name('transfer.addRute');

            Route::get('/rute-kamtuu', [ListingTransferController::class, 'viewRuteKamtuu'])->name('ruteKamtuu');
            Route::get('/add-rute/{id}', [ListingTransferController::class, 'viewRutee'])->name('ruteKamtuu.pilih');

            Route::get('/rute-schedule', [ListingTransferController::class, 'viewRuteSchedule'])->name('transfer.viewRuteSchedule');
            Route::post('/rute-schedule', [ListingTransferController::class, 'addRuteSchedule'])->name('transfer.addRuteSchedule');
            Route::get('/transfer-availablity', [ListingTransferController::class, 'viewTransferAvailablity'])->name('transfer.viewAvailability');
            Route::put('/transfer-availablity', [ListingTransferController::class, 'addTransferAvailablity'])->name('transfer.addAvailability');
            // transfer.addAvailability
            // transfer.addAvailability


            Route::get('/add-pilihan', [ListingTransferController::class, 'viewPilihan'])->name('transfer.viewPilihan');
            Route::put('/add-pilihan', [ListingTransferController::class, 'addPilihan'])->name('transfer.addPilihan');

            // Route::get('/add-harga', [ListingTransferController::class, 'viewHarga'])->name('tur.viewHarga');
            // Route::put('/add-harga', [ListingTransferController::class, 'addHarga'])->name('tur.addHarga');
            // Route::put('/add-harga/addDiscountGroup', [ListingTransferController::class, 'addDiscountGroup'])->name('tur.addDiscountGroup');
            // Route::put('/add-harga/addAvailability', [ListingTransferController::class, 'addAvailability'])->name('tur.addAvailability');

            Route::get('/add-batas', [ListingTransferController::class, 'viewBatas'])->name('transfer.viewBatas');
            Route::put('/add-batas', [ListingTransferController::class, 'addBatas'])->name('transfer.addBatas');

            // Edit Listing Transfer
            Route::get('/add-informasi/umum/edit/{id}', [EditListingTransferController::class, 'showAddDetailUmum'])->name('transfer.viewAddListingEdit');
            Route::put('/add-informasi/edit/{id}', [EditListingTransferController::class, 'editAddDetailUmum'])->name('transfer.addListingUmum.edit');
            Route::put('/add-listing/edit/{id}', [EditListingTransferController::class, 'editAddDetailPrivate'])->name('transfer.addListingPrivate.edit');

            Route::get('/rute-schedule/{id}', [EditListingTransferController::class, 'showEditScheduleRute'])->name('transfer.viewRuteSchedule.edit');
            Route::put('/rute-schedule/{id}', [EditListingTransferController::class, 'editRuteSchedule'])->name('transfer.editRuteSchedule');

            Route::get('/rute/{id}', [EditListingTransferController::class, 'showEditRute'])->name('transfer.viewRute.edit');
            Route::put('/rute/{id}', [EditListingTransferController::class, 'editRute'])->name('transfer.editRutee');

            Route::get('/add-pilihan/edit/{id}', [EditListingTransferController::class, 'showEditPilihanEkstra'])->name('transfer.viewPilihanEdit');
            Route::put('/add-pilihan/edit/{id}', [EditListingTransferController::class, 'editPilihanEkstra'])->name('transfer.addPilihanEkstra.edit');

            Route::get('/add-batas/edit/{id}', [EditListingTransferController::class, 'showEditBatas'])->name('transfer.viewBatasEdit');
            Route::put('/add-batas/edit/{id}', [EditListingTransferController::class, 'editBatas'])->name('transfer.addBatas.edit');

            // video_embed
            Route::get('/edit/transfer-availablity/{id}', [EditListingTransferController::class, 'viewEditTransferAvailablity'])->name('transfer.viewEditAvailability');
            Route::put('/edit/transfer-availablity/{id}', [EditListingTransferController::class, 'editTransferAvailablity'])->name('transfer.editAvailability');

            Route::get('/faq', [ListingTransferController::class, 'viewFaq'])->name('transfer.viewFaq');
            Route::post('/faq', [ListingTransferController::class, 'viewFaqCode'])->name('transfer.viewFaq.code');
            Route::get('/faq/{code}', [ListingTransferController::class, 'viewFaqEdit'])->name('transfer.viewFaq.edit');
            Route::post('/faq/{id}', [ListingTransferController::class, 'viewFaqPost'])->name('transfer.viewFaq.update');
        });

        // Master Kendaraan
        Route::prefix('seller/transfer/master-kendaraan')->group(function () {
            Route::get('/all', [ListingTransferController::class, 'viewMasterMobil'])->name('masterMobil');
            Route::get('/edit/{id}', [ListingTransferController::class, 'edit']);
            Route::post('/update', [ListingTransferController::class, 'update'])->name('masterMobil.update');
            Route::get('/hapus/{id}', [ListingTransferController::class, 'hapus']);

            Route::get('/edit-bus/{id}', [ListingTransferController::class, 'editBus']);
            Route::post('/update-bus', [ListingTransferController::class, 'updateBus'])->name('masterMobil.updateBus');
            Route::get('/hapus-bus/{id}', [ListingTransferController::class, 'hapusBus']);

            Route::get('/jenis-mobil', [ListingTransferController::class, 'viewAddJenisMobil'])->name('masterMobil.viewAddJenisMobil');
            Route::post('/jenis-mobil', [ListingTransferController::class, 'addJenisMobil'])->name('masterMobil.addJenisMobil');
            Route::get('/merek-mobil', [ListingTransferController::class, 'viewAddMerekMobil'])->name('masterMobil.viewAddMerekMobil');
            Route::post('/merek-mobil', [ListingTransferController::class, 'addMerekMobil'])->name('masterMobil.addMerekMobil');
            Route::get('/jenis-bus', [ListingTransferController::class, 'viewAddJenisBus'])->name('masterMobil.viewAddJenisBus');
            Route::post('/jenis-bus', [ListingTransferController::class, 'addJenisBus'])->name('masterMobil.addJenisBus');
            Route::get('/merek-bus', [ListingTransferController::class, 'viewAddMerekBus'])->name('masterMobil.viewAddMerekBus');
            Route::post('/merek-bus', [ListingTransferController::class, 'addMerekBus'])->name('masterMobil.addMerekBus');
        });

        // Master Bus
        Route::prefix('seller/transfer/master-bus')->group(function () {
            Route::get('/all', [ListingTransferController::class, 'viewMasterBus'])->name('masterBus');
            Route::get('/edit/{id}', [ListingTransferController::class, 'editBus']);
            Route::post('/update', [ListingTransferController::class, 'updateBus'])->name('masterBus.updateBus');
            Route::get('/hapus/{id}', [ListingTransferController::class, 'hapusBus']);

            Route::get('/layout-bus', [ListingTransferController::class, 'viewAddLayoutBus'])->name('layout');
            Route::put('/layout-bus', [ListingTransferController::class, 'addLayoutBus'])->name('layout.addLayoutBus');
            Route::get('/layout-bus/{id}', [ListingTransferController::class, 'viewEditLayoutBus'])->name('bus.viewLayout.edit');
            Route::put('/layout', [ListingTransferController::class, 'addLayoutBusM39'])->name('layout.addLayoutBusM39');
            Route::put('/layout-bus/edit/{id}', [ListingTransferController::class, 'EditLayoutBus'])->name('bus.editLayout');
            Route::put('/layout-bus/M39/edit/{id}', [ListingTransferController::class, 'editLayoutBusM39'])->name('bus.editLayout.M39');
            Route::delete('/layout-bus/delete/{id}', [ListingTransferController::class, 'deleteLayoutBus'])->name('bus.Layout.delete');
            Route::post('/layout-bus', [ListingTransferController::class, 'addNewLayoutBus'])->name('layout.addNewLayoutBus');
            Route::put('/layout-bus/edit/{id}', [ListingTransferController::class, 'editNewLayoutBus'])->name('layout.editNewLayoutBus');
            Route::get('/layout-bus/{id}', [ListingTransferController::class, 'viewNewEditLayoutBus'])->name('bus.viewNewEditLayoutBus.edit');


            Route::post('/add/transfer-reguler/gallery/{id}', [ListingTransferController::class, 'addTransferRegulerGallery'])->name('transfer.editRegulerGallery');
            Route::post('/edit/transfer-reguler/gallery', [ListingTransferController::class, 'addTransferRegulerGallery'])->name('transfer.addRegulerGallery');
            Route::post('/add/transfer-schedule/gallery', [ListingTransferController::class, 'addTransferScheduleGallery'])->name('transfer.addScheduleGallery');
            Route::post('/edit/transfer-schedule/gallery/{id}', [ListingTransferController::class, 'addTransferScheduleGallery'])->name('transfer.editScheduleGallery');
        });

        // Master Driver
        Route::prefix('seller/transfer/master-driver')->group(function () {
            Route::get('/all', [MasterDriverController::class, 'viewMasterDriver'])->name('masterDriver');
            Route::get('/edit/{id}', [MasterDriverController::class, 'editDriver'])->name('masterDriver.addDriverEdit');
            Route::post('/edit/{id}', [MasterDriverController::class, 'updateDriver'])->name('masterDriver.addDriver.edit');
            Route::get('/hapus/{id}', [MasterDriverController::class, 'hapusDriver']);

            Route::get('/add', [MasterDriverController::class, 'viewAddDriver'])->name('masterDriver.viewAddDriver');
            Route::post('/add', [MasterDriverController::class, 'addDriver'])->name('masterDriver.addDriver');
        });

        //Transfer Schedule
        Route::get('/seller/transfer/schedule/add-detail', function () {
            return view('BE.seller.transfer.schedule.add-listing');
        });
        Route::prefix('/seller/transfer')->group(function () {
            Route::get('schedule/informasi', function () {
                return view('BE.seller.transfer.schedule.add-listing');
            });
            Route::get('schedule/pilihan', function () {
                return view('BE.seller.transfer.schedule.pilihan-addlisting');
            });
            Route::get('schedule/rute', function () {
                return view('BE.seller.transfer.schedule.rute-addlisting');
            });
            Route::get('schedule/pembayaran', function () {
                return view('BE.seller.transfer.schedule.pembayaran-addlisting');
            });
        });

        //Transfer Private
        Route::get('/seller/transfer/private/add-detail', function () {
            return view('BE.seller.transfer.private.add-listing');
        });
        Route::prefix('/seller/transfer/private')->group(function () {
            Route::get('informasi', function () {
                return view('BE.seller.transfer.private.add-listing');
            });
            Route::get('pilihan', function () {
                return view('BE.seller.transfer.private.pilihan-addlisting');
            });
            Route::get('rute', function () {
                return view('BE.seller.transfer.private.rute-addlisting');
            });
            Route::get('pembayaran', function () {
                return view('BE.seller.transfer.private.pembayaran-addlisting');
            });
        });
        // Route::get('/seller/transfer/faq', function () {
        //     return view('BE.seller.transfer.faq-transfer');
        // });

        //BE Rental
        Route::prefix('/seller/rental')->group(function () {
            Route::get('/add-informasi', [ListingRentalController::class, 'index'])->name('rental');
            Route::post('/add-informasi', [ListingRentalController::class, 'addListing'])->name('rental.addListing');

            Route::get('/batas', [ListingRentalController::class, 'viewBatas'])->name('rental.viewBatas');
            Route::put('/batas', [ListingRentalController::class, 'addBatas'])->name('rental.addBatas');

            Route::get('/add-pilihan', [ListingRentalController::class, 'viewPilihan'])->name('rental.viewPilihan');
            Route::put('/add-pilihan', [ListingRentalController::class, 'addPilihan'])->name('rental.addPilihan');

            Route::post('/add-rental-gallery', [ListingRentalController::class, 'rentalAddGallery'])->name('rental.addGallery');
            // Edit Rental
            Route::get('/add-informasi/edit/{id}', [EditListingRentalController::class, 'showAddDetail'])->name('rental.viewAddListingEdit');
            Route::post('/add-informasi/edit/{id}', [EditListingRentalController::class, 'editAddListing'])->name('rental.addListing.edit');

            Route::get('/pilihan/edit/{id}', [EditListingRentalController::class, 'showEditPilihanEkstra'])->name('rental.viewPilihanEkstraEdit');
            Route::put('/pilihan/edit/{id}', [EditListingRentalController::class, 'editPilihanEkstra'])->name('rental.addPilihanEkstra.edit');

            Route::post('/edit-rental-gallery/{id}', [EditListingRentalController::class, 'rentalEditGallery'])->name('rental.editGallery');

            Route::get('/faq', [ListingRentalController::class, 'viewFaq'])->name('rental.viewFaq');
            Route::post('/faq', [ListingRentalController::class, 'viewFaqCode'])->name('rental.viewFaq.code');
            Route::get('/faq/{code}', [ListingRentalController::class, 'viewFaqEdit'])->name('rental.viewFaq.edit');
            Route::post('/faq/{id}', [ListingRentalController::class, 'viewFaqPost'])->name('rental.viewFaq.update');
        });

        //BE Activity
        Route::prefix('/seller/activity')->group(function () {
            // Add Listing
            Route::get('/add-detail', [AddListingActivityController::class, 'index'])->name('activity');
            Route::put('/add-detail', [AddListingActivityController::class, 'addListing'])->name('activity.addListing');

            Route::get('/harga', [AddListingActivityController::class, 'viewHarga'])->name('activity.viewHarga');
            Route::put('/harga', [AddListingActivityController::class, 'addHarga'])->name('activity.addHarga');
            Route::put('/harga/addDiscountGroup', [AddListingActivityController::class, 'addDiscountGroup'])->name('activity.addDiscountGroup');
            Route::put('/harga/addAvailability', [AddListingActivityController::class, 'addAvailability'])->name('activity.addAvailability');

            Route::get('/batas', [AddListingActivityController::class, 'viewBatas'])->name('activity.viewBatas');
            Route::put('/batas', [AddListingActivityController::class, 'addBatasWaktuPembayaranDanPembatalan'])->name('activity.addBatasWaktuPembayaranDanPembatalan');

            Route::get('/peta', [AddListingActivityController::class, 'viewPeta'])->name('activity.viewPeta');
            Route::put('/peta', [AddListingActivityController::class, 'addPetaPhoto'])->name('activity.addPetaPhoto');
            Route::post('/peta/gallery', [AddListingActivityController::class, 'addPetaPhotoGallery'])->name('activity.addPetaPhotoGallery');

            Route::get('/pilihan', [AddListingActivityController::class, 'viewPilihanEkstra'])->name('activity.viewPilihanEkstra');
            Route::put('/pilihan', [AddListingActivityController::class, 'addPilihanEkstra'])->name('activity.addPilihanEkstra');

            // Edit Listing
            Route::get('/add-detail/edit/{id}', [EditListingActivityController::class, 'showAddDetail'])->name('activity.viewAddListingEdit');
            Route::put('/add-detail/edit/{id}', [EditListingActivityController::class, 'editAddDetail'])->name('activity.addListing.edit');

            Route::get('/harga/edit/{id}', [EditListingActivityController::class, 'showEditHarga'])->name('activity.viewHargaEdit');
            Route::put('/harga/edit/{id}', [EditListingActivityController::class, 'editHarga'])->name('activity.addHarga.edit');
            Route::put('/harga-addDiscountGroup/edit/{id}', [EditListingActivityController::class, 'editDiscountGroup'])->name('activity.addDiscountGroup.edit');
            Route::put('/harga-addAvailability/edit/{id}', [EditListingActivityController::class, 'editAvailability'])->name('activity.addAvailability.edit');

            Route::get('/batas/edit/{id}', [EditListingActivityController::class, 'showEditBatas'])->name('activity.viewBatasEdit');
            Route::put('/batas/edit/{id}', [EditListingActivityController::class, 'editBatas'])->name('activity.addBatasWaktuPembayaranDanPembatalan.edit');

            Route::get('/peta/edit/{id}', [EditListingActivityController::class, 'showEditPeta'])->name('activity.viewPetaEdit');
            Route::put('/peta/edit/{id}', [EditListingActivityController::class, 'editPetaPhoto'])->name('activity.addPetaPhoto.edit');
            Route::post('/peta/edit/gallery/{id}', [EditListingActivityController::class, 'editPetaPhotoGallery'])->name('activity.addPetaPhotoGallery.edit');

            Route::get('/pilihan/edit/{id}', [EditListingActivityController::class, 'showEditPilihanEkstra'])->name('activity.viewPilihanEkstraEdit');
            Route::put('/pilihan/edit/{id}', [EditListingActivityController::class, 'editPilihanEkstra'])->name('activity.addPilihanEkstra.edit');

            Route::get('/faq', [AddListingActivityController::class, 'viewFaq'])->name('activity.viewFaq');
            Route::post('/faq', [AddListingActivityController::class, 'viewFaqCode'])->name('activity.viewFaq.code');
            Route::get('/faq/{code}', [AddListingActivityController::class, 'viewFaqEdit'])->name('activity.viewFaq.edit');
            Route::post('/faq/{id}', [AddListingActivityController::class, 'viewFaqPost'])->name('activity.viewFaq.update');
        });
    });

    // Dashboard
    Route::prefix('dashboard')->group(function () {

        // Route::get('seller/tur/faq', function () {
        //     return view('BE.seller.tur.faq');
        // });

        Route::prefix('/seller/tur')->group(function () {
            Route::get('/add-informasi', [ListingTurController::class, 'index'])->name('tur');
            Route::post('/add-informasi', [ListingTurController::class, 'addListingPrivate'])->name('tur.addListingPrivate');
            Route::post('/add-listing', [ListingTurController::class, 'addListingOpen'])->name('tur.addListingOpen');

            Route::get('/add-itenenary', [ListingTurController::class, 'viewItenenary'])->name('tur.viewItenenary');
            Route::put('/add-itenenary', [ListingTurController::class, 'addItenenary'])->name('tur.addItenenary');
            Route::post('/add-itenerary/gallery', [ListingTurController::class, 'addIteneraryGallery'])->name('tur.addIteneraryGallery');

            Route::get('/add-harga', [ListingTurController::class, 'viewHarga'])->name('tur.viewHarga');
            Route::put('/add-harga', [ListingTurController::class, 'addHarga'])->name('tur.addHarga');
            Route::put('/add-harga/addDiscountGroup', [ListingTurController::class, 'addDiscountGroup'])->name('tur.addDiscountGroup');
            Route::put('/add-harga/addAvailability', [ListingTurController::class, 'addAvailability'])->name('tur.addAvailability');

            Route::get('/add-batas', [ListingTurController::class, 'viewBatas'])->name('tur.viewBatas');
            Route::put('/add-batas', [ListingTurController::class, 'addBatas'])->name('tur.addBatas');

            Route::get('/add-peta', [ListingTurController::class, 'viewPeta'])->name('tur.viewPeta');
            Route::put('/add-peta', [ListingTurController::class, 'addPetaPhoto'])->name('tur.addPetaPhoto');
            Route::post('/add-peta/gallery', [ListingTurController::class, 'addPetaPhotoGallery'])->name('tur.addPetaPhotoGallery');

            Route::get('/add-pilihan', [ListingTurController::class, 'viewPilihanEkstra'])->name('tur.viewPilihanEkstra');
            Route::put('/add-pilihan', [ListingTurController::class, 'addPilihanEkstra'])->name('tur.addPilihanEkstra');

            // Edit Listing Tur
            Route::get('/add-informasi/edit/{id}', [EditListingTurController::class, 'showAddDetail'])->name('tur.viewAddListingEdit');
            Route::put('/add-informasi/edit/{id}', [EditListingTurController::class, 'editAddDetailPrivate'])->name('tur.addListingPrivate.edit');
            Route::put('/add-listing/edit/{id}', [EditListingTurController::class, 'editAddDetailOpen'])->name('tur.addListingOpen.edit');

            Route::get('/add-itenenary/edit/{id}', [EditListingTurController::class, 'showEditItenenary'])->name('tur.viewItenenarydit');
            Route::put('/add-itenenary/edit/{id}', [EditListingTurController::class, 'editAddDetailItenenary'])->name('tur.addItenenary.edit');

            Route::get('/add-harga/edit/{id}', [EditListingTurController::class, 'showEditHarga'])->name('tur.viewHargaEdit');
            Route::put('/add-harga/edit/{id}', [EditListingTurController::class, 'editHarga'])->name('tur.addHarga.edit');
            Route::put('/add-harga/addDiscountGroup/edit/{id}', [EditListingTurController::class, 'editDiscountGroup'])->name('tur.addDiscountGroup.edit');
            Route::put('/add-harga/addAvailability/edit/{id}', [EditListingTurController::class, 'editAvailability'])->name('tur.addAvailability.edit');

            Route::get('/add-batas/edit/{id}', [EditListingTurController::class, 'showEditBatas'])->name('tur.viewBatasEdit');
            Route::put('/add-batas/edit/{id}', [EditListingTurController::class, 'editBatas'])->name('tur.addBatas.edit');

            Route::get('/add-peta/edit/{id}', [EditListingTurController::class, 'showEditPeta'])->name('tur.viewPetaEdit');
            Route::put('/add-peta/edit/{id}', [EditListingTurController::class, 'editPetaPhoto'])->name('tur.addPetaPhoto.edit');
            Route::post('/add-peta/edit/gallery/{id}', [EditListingTurController::class, 'editPetaPhotoGallery'])->name('tur.addPetaPhotoGallery.edit');

            Route::get('/add-pilihan/edit/{id}', [EditListingTurController::class, 'showEditPilihanEkstra'])->name('tur.viewPilihanEkstraEdit');
            Route::put('/add-pilihan/edit/{id}', [EditListingTurController::class, 'editPilihanEkstra'])->name('tur.addPilihanEkstra.edit');

            Route::get('/faq', [ListingTurController::class, 'viewFaq'])->name('tur.viewFaq');
            Route::post('/faq', [ListingTurController::class, 'viewFaqCode'])->name('tur.viewFaq.code');
            Route::get('/faq/{code}', [ListingTurController::class, 'viewFaqEdit'])->name('tur.viewFaq.edit');
            Route::post('/faq/{id}', [ListingTurController::class, 'viewFaqPost'])->name('tur.viewFaq.update');
        });
        //Tur private
        // Route::prefix('seller/tur/private')->group(function () {
        //     Route::get('informasi', function () {
        //         return view('BE.seller.tur.privateTrip.informasidasar');
        //     });

        //     Route::get('itinerary', function () {
        //         return view('BE.seller.tur.privateTrip.itinerary');
        //     });

        //     Route::get('harga', function () {
        //         return view('BE.seller.tur.privateTrip.harga');
        //     });

        //     Route::get('bayar', function () {
        //         return view('BE.seller.tur.privateTrip.batasbayar');
        //     });

        //     Route::get('maps', function () {
        //         return view('BE.seller.tur.privateTrip.mapsfoto');
        //     });

        //     Route::get('pilihan', function () {
        //         return view('BE.seller.tur.privateTrip.pilihanekstra');
        //     });
        // });


        //Tur Open
        // Route::prefix('seller/tur/open')->group(function () {
        //     Route::get('informasi', function () {
        //         return view('BE.seller.tur.openTrip.informasidasar');
        //     });

        //     Route::get('itinerary', function () {
        //         return view('BE.seller.tur.openTrip.itinerary');
        //     });

        //     Route::get('harga', function () {
        //         return view('BE.seller.tur.openTrip.harga');
        //     });

        //     Route::get('bayar', function () {
        //         return view('BE.seller.tur.openTrip.batasbayar');
        //     });

        //     Route::get('maps', function () {
        //         return view('BE.seller.tur.openTrip.mapsfoto');
        //     });

        //     Route::get('pilihan', function () {
        //         return view('BE.seller.tur.openTrip.pilihanekstra');
        //     });
        // });

        // // Dashboard Hotel
        // Route::prefix('seller/hotel')->group(function () {
        //     Route::get('informasi', function () {
        //         return view('BE.seller.hotel.informasidasar');
        //     });

        //     Route::get('harga', function () {
        //         return view('BE.seller.hotel.harga');
        //     });

        //     Route::get('bayar', function () {
        //         return view('BE.seller.hotel.batasbayar');
        //     });

        //     Route::get('maps', function () {
        //         return view('BE.seller.hotel.mapsfoto');
        //     });

        //     Route::get('ekstra', function () {
        //         return view('BE.seller.hotel.pilihanekstra');
        //     });

        //     Route::get('faq', function () {
        //         return view('BE.seller.hotel.faq');
        //     });
        // });

        // // Dashboard XStay
        // Route::prefix('seller/xstay')->group(function () {
        //     Route::get('informasi', function () {
        //         return view('BE.seller.xstay.informasidasar');
        //     });

        //     Route::get('harga', function () {
        //         return view('BE.seller.xstay.harga');
        //     });

        //     Route::get('bayar', function () {
        //         return view('BE.seller.xstay.batasbayar');
        //     });

        //     Route::get('maps', function () {
        //         return view('BE.seller.xstay.mapsfoto');
        //     });

        //     Route::get('ekstra', function () {
        //         return view('BE.seller.xstay.pilihanekstra');
        //     });

        //     Route::get('faq', function () {
        //         return view('BE.seller.xstay.faq');
        //     });
        // });

        // Route::get('/seller/tur/faq', function () {
        //     return view('BE.seller.tur.faq-tur');
        // });

        // Route::get('/travel', function () {
        //     return view('BE.traveller.dashboard-traveller');
        // });

        // Route::get('/agent', function () {
        //    return view('BE.agent.dashboard-agent');
        // });
    });
});

Route::group(['middleware' => ['auth', 'CekRole:traveller, agent, corporate']], function () {
    // Route::get('/dashboard/traveller', function () {
    //     return view('BE.traveller.dashboard-traveller');
    // });

    Route::get('/dashboard/traveller', [ProfileController::class, 'travellerDashboard'])->middleware(['auth', 'verified']);

    //    Chat
    Route::get('/inbox', [MessageController::class, 'indexTraveller'])->name('inboxTraveller');
    Route::get('/inbox/{receiver}/{user_id}', [MessageController::class, 'inboxTraveller'])->name('inboxChatTraveller');
    Route::post('/inbox/traveller/store/{receiver}', [MessageController::class, 'inboxTravellerStore'])->name('inboxTravellerStore');
    Route::get('/inbox/traveller/admin/{received}/{sender}', [MessageController::class, 'inboxTravellerAdmin'])->name('inboxTravellerAdmin');
    Route::post('/inbox/traveller/admin/{received}', [MessageController::class, 'inboxTravellerAdminStore'])->name('inboxTravellerAdminStore');

    Route::get('/claim/code/kupon', [KuponController::class, 'travellerKupon'])->name('traveller.kupon');
    Route::put('/claim/code/kupon/add', [KuponController::class, 'travellerKuponClaim'])->name('traveller.kupon.claim');
    Route::get('/list/kupon', [KuponController::class, 'travellerKupon'])->name('traveller.kupon.list');

    // // Get Province, Regency, District, Village Data
    // Route::post('/getkabupaten', [ProfileController::class, 'getkabupaten'])->name('profile.getkabupaten');
    // Route::post('/getkecamatan', [ProfileController::class, 'getkecamatan'])->name('profile.getkecamatan');
    // Route::post('/getkelurahan', [ProfileController::class, 'getkelurahan'])->name('profile.getkelurahan');

    // Traveller Profile
    Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
    Route::put('/profile/update/data-diri', [ProfileController::class, 'updateDataDiri'])->name('profile.updateDataDiri');
    Route::put('/profile/update/data-alamat', [ProfileController::class, 'updateDataAlamat'])->name('profile.updateDataAlamat');
    Route::put('/profile/update/{email}/update-email', [ProfileController::class, 'updateEmail'])->name('profile.updateEmail');
    Route::put('/profile/update/{email}/delete-email', [ProfileController::class, 'deleteEmail'])->name('profile.deleteEmail');
    Route::put('/profile/update/{email}/primary-email', [ProfileController::class, 'updatePrimaryEmail'])->name('profile.updatePrimaryEmail');
    Route::put('/profile/update/{no_telp}/update-phone-number', [ProfileController::class, 'updatePhoneNumber'])->name('profile.updatePhoneNumber');
    Route::put('/profile/update/{no_telp}/delete-phone-number', [ProfileController::class, 'deletePhoneNumber'])->name('profile.deletePhoneNumber');
    Route::put('/profile/update/{no_telp}/primary-phone-number', [ProfileController::class, 'updatePrimaryPhoneNumber'])->name('profile.updatePrimaryPhoneNumber');

    // Traveller Info Booking
    Route::get('/info-booking', [InfoBookingController::class, 'index'])->name('infobooking');
    Route::put('/info-booking/update/data-booking', [InfoBookingController::class, 'updateDataBooking'])->name('infobooking.updateDataBooking');
    Route::put('/info-booking/update/kitas-booking', [InfoBookingController::class, 'updateKitasBooking'])->name('infobooking.updateKitasBooking');

    // Traveller Newsletter
    Route::get('/newsletter', [NewsletterController::class, 'index'])->name('newsletter');
    Route::put('/newsletter/update', [NewsletterController::class, 'updateNewsletter'])->name('newsletter.updateNewsletter');

    // Traveller Hapus Akun
    Route::get('/hapus-akun', function () {
        return view('BE.traveller.hapus-akun');
    })->name('hapusakun');

    // Traveller Ubah Password
    // Route::get('/ubah-password', [TravellerPasswordController::class, 'index'])->name('ubahpassword');
    // Route::put('/ubah-password/update', [TravellerPasswordController::class, 'updatePassword'])->name('ubahpassword.updatePassword');

    Route::delete('/profile/traveller/deleteaccount/{id}', [DeleteTravellerController::class, 'destroy'])->name('traveller.destroy.account');
    Route::get('/profile/traveller/reset-password', [TravellerPasswordController::class, 'create'])->name('traveller.resetpassword');
    Route::post('/forget-password', [TravellerPasswordController::class, 'store'])->name('forget.password.post');
    Route::get('/reset-password/{token}', [TravellerPasswordController::class, 'edit'])->name('reset.password.get');
    Route::post('/reset-password', [TravellerPasswordController::class, 'update'])->name('reset.password.post');

    Route::get('/dataPesanan', [DataPesananController::class, 'viewDataPesanan'])->name('viewDataPesanan');
    Route::get('/listPembatalan', [PengajuanPembatalanController::class, 'viewListPengajuan'])->name('viewListPengajuan');
    Route::post('/pengajuanPembatalanPesanan', [PengajuanPembatalanController::class, 'storePengajuanPembatalan'])->name('storePengajuanPembatalan');
    // Route::get('/favorit', function () {
    //     return view('BE.traveller.favorit');
    // })->name('favorit');
    Route::get('/favorit', [ProfileController::class, 'getLikes'])->name('favorit');
    Route::get('/faq', function () {
        return view('BE.traveller.faq');
    })->name('faq');
    Route::get('/pesan', function () {
        return view('BE.traveller.pesan');
    })->name('pesan');
    Route::get('/detail-pesan', function () {
        return view('BE.traveller.detail-pesan');
    })->name('detailpesan');
    Route::get('/dukungan', function () {
        return view('BE.traveller.support');
    })->name('support');
});

// Booking Order
Route::get('cart', [BookingOrderController::class, 'booking_view'])->name('cart');
Route::post('booking-store/activity', [BookingOrderController::class, 'booking_store_activity'])->name('booking.store.activity');
Route::post('booking-store/tour', [BookingOrderController::class, 'booking_store_tour'])->name('booking.store.tour');
Route::post('booking-store/hotel', [BookingOrderController::class, 'booking_store_hotel'])->name('booking.store.hotel');
Route::post('booking-store/xstay', [BookingOrderController::class, 'booking_store_xstay'])->name('booking.store.xstay');
Route::post('booking-store/transfer', [BookingOrderController::class, 'booking_store_transfer'])->name('booking.store.transfer');
Route::post('booking-store/rental', [BookingOrderController::class, 'booking_store_rental'])->name('booking.store.rental');
Route::delete('deleteItem/{id}', [BookingOrderController::class, 'destroy'])->name('booking.destroy');
Route::put('updateStatus/{id}', [BookingOrderController::class, 'update_status_confirm'])->name('booking.update.status_confirm');

Route::resource('/master/legal',DocumentLegalController::class);
Route::get('/legal/datas',[DocumentLegalController::class,'datas'])->name('document.datas');

// Detail Order In Cart
Route::get('detailOrderProduct/{id}', [BookingOrderController::class, 'viewDetailOrderProduct'])->name('viewDetailOrderProduct');
Route::get('detailOrderProduct/{order}/{booking}/letter', [BookingOrderController::class, 'viewDetailOrderProductLetter'])->name('viewDetailOrderProductLetter');



// Invoice Order
Route::get('/invoice/{order}/{booking}', [SettingGeneralTravellerController::class, 'getInvoice'])->name('getInvoice');
Route::get('/invoice/download/{order}/{booking}', [SettingGeneralTravellerController::class, 'downloadInvoice'])->name('downloadInvoice');

// Receipt Order
Route::get('/receipt/{order}/{booking}', [SettingGeneralTravellerController::class, 'getReceipt'])->name('getReceipt');
Route::get('/receipt/download/{order}/{booking}', [SettingGeneralTravellerController::class, 'downloadReceipt'])->name('downloadReceipt');
Route::get('/voucher/{id_booking}', [SettingGeneralTravellerController::class, 'getVoucher'])->name('voucher');
Route::get('/voucher/download/{id_booking}', [SettingGeneralTravellerController::class, 'downloadVoucher'])->name('downloadVoucher');
Route::get('/download/voucher/{booking}', [SettingGeneralTravellerController::class, 'test'])->name('donwload-pdf');

// Cek Kupon
Route::post('/useKupon', [BookingOrderController::class, 'cekKupon'])->name('cekKupon');

// Payment Gateway XENDIT
Route::post('xendit/va/invoice', [PaymentController::class, 'createVA'])->name('order.store');
Route::get('xendit/va/payment-link', [PaymentController::class, 'createInvoice'])->name('order.createInvoice');
Route::post('xendit/payment/invoice', [PaymentController::class, 'callbackPayment'])->name('invoice.payment');
Route::post('xendit/va/payment-callback', [PaymentController::class, 'callbackPaymentFVA'])->name('invoice.payment');

Route::prefix('dashboard')->group(function () {
    Route::get('destinasi', [DestindonesiaController::class, 'create']);
    Route::post('/getkabupaten', [DestindonesiaController::class, 'getkabupaten'])->name('getkabupaten');
    Route::post('/getkecamatan', [DestindonesiaController::class, 'getkecamatan'])->name('getkecamatan');
    Route::post('/getdesa', [DestindonesiaController::class, 'getdesa'])->name('getdesa');
    Route::post('/getmerekmobil', [ListingRentalController::class, 'getmerekmobil'])->name('getmerekmobil');
    Route::post('/getmerekmobil2', [ListingRentalController::class, 'getmerekmobil2'])->name('getmerekmobil2');
    Route::post('/getmerekbus', [ListingRentalController::class, 'getmerekbus'])->name('getmerekbus');
});
Route::group(['middleware' => ['auth', 'CekRole:corporate']], function () {
    Route::get('/dashboard/corporate', function () {
        return view('BE.corporate.dashboard-corporate');
    })->name('dashboardcorporate');
    // Route::get('/dashboard-corporate', function () {
    //     return view('BE.corporate.dashboard-corporate');
    // })->name('dashboardagent');
    Route::get('/dashboard-corporate/my-account', function () {
        return view('BE.corporate.my-account-corporate');
    });
    Route::get('/dashboard-corporate/my-orders', function () {
        return view('BE.corporate.my-order-corporate');
    });
    Route::get('/dashboard-corporate/detail-order', function () {
        return view('BE.corporate.detail-order');
    });
    Route::get('/dashboard-corporate/password', function () {
        return view('BE.corporate.password-corporate');
    });
    // Route::get('/dashboard-corporate/add-user-corporate', function () {
    //     return view('BE.corporate.user-corporate');
    // });

});

Route::group(['middleware' => ['auth', 'CekRole:visitor']], function () {
    Route::get('/dashboard/visitor', function () {
        return view('BE.visitor.dashboard-visitor');
    })->name('dashboardvisitor');
});

// Admin
Route::group(['middleware' => ['auth', 'CekRole:admin']], function () {
    Route::get('laporan/admin', [AdminController::class, 'viewLaporan'])->name('viewLaporan');
    Route::get('pengajuanPenarikan', [AdminController::class, 'viewPengajuanPenarikan'])->name('viewPengajuanPenarikan');
    Route::put('verifikasiPengajuan/{id}', [AdminController::class, 'verifikasiPengajuan'])->name('verifikasiPengajuan');

    Route::get('dashboard/admin/kamtuu', function () {
        return view('BE.admin.dashboard-admin');
    });
    Route::get('dashboard/admin/kamtuu/master-user', function () {
        return view('BE.admin.master-user');
    });

    Route::get('dashboard/admin/kamtuu/master-place', function () {
        return view('BE.admin.master-place');
    });

    Route::get('dashboard/admin/kamtuu/master-rute', function () {
        return view('BE.admin.master-rute');
    });

    Route::get('dashboard/admin/kamtuu/master-tag', function () {
        return view('BE.admin.master-tag');
    });

    //    inbox in admin account
    Route::get('/dashboard/admin/kamtuu/messages', [MessageController::class, 'indexMessagesAdmin'])->name('indexMessagesAdmin');
    Route::get('/dashboard/admin/kamtuu/messages/inbox/{sender}', [MessageController::class, 'indexMessagesAdminInbox'])->name('indexMessagesAdminInbox');
    Route::post('/dashboard/admin/kamtuu/messages/store/{sender}', [MessageController::class, 'indexMessagesAdminStore'])->name('indexMessagesAdminStore');

    // master pilihan
    route::get('dashboard/admin/master/pilihan', [SettingGeneralController::class, 'masterChoiceIndex'])->name('choice.index');
    route::post('dashboard/admin/master/pilihan', [SettingGeneralController::class, 'masterChoiceStore'])->name('choice.store');
    route::get('dashboard/admin/master/pilihan/{id}/edit', [SettingGeneralController::class, 'masterChoiceEdit'])->name('choice.edit');
    route::put('dashboard/admin/master/pilihan/{id}/update', [SettingGeneralController::class, 'masterChoiceUpdate'])->name('choice.update');
    route::delete('dashboard/admin/master/pilihan/{id}/delete', [SettingGeneralController::class, 'masterChoiceDelete'])->name('choice.delete');

    // confrimation seller account
    route::get('dashboard/admin/confrimation/seller/account', [SettingGeneralController::class, 'confrimationSellerAccountIndex'])->name('confrim.seller-account.index');
    route::get('dashboard/admin/confrimation/seller/detail/{id}', [SettingGeneralController::class, 'confrimationSellerDetail'])->name('confrim.seller-account.detail');
    route::put('dashboard/admin/confrimation/seller/update/{id}', [SettingGeneralController::class, 'updateStatusSeller'])->name('confrim.seller-account.update');

    //review moderator
    route::get('dashboard/admin/ulasan',[SettingGeneralController::class,'reviewModerator'])->name('review.moderator');
    route::get('dashboard/admin/review/datas',[SettingGeneralController::class,'reviewsDatas'])->name('review.datas');
    route::get('dashboard/admin/review/search',[SettingGeneralController::class,'reviewSearch'])->name('review.search');
    route::put('dashboard/admin/ulasan/{id}',[SettingGeneralController::class,'reviewModeratorUpdate'])->name('review.moderator.update');
    route::delete('dashboard/admin/ulasan/{id}',[SettingGeneralController::class,'reviewModeratorDelete'])->name('review.moderator.delete');
    route::delete('dashboard/admin/delete/ulasan',[SettingGeneralController::class,'reviewMultipeDelete'])->name('review.multiple.delete');
});

// Koper

Route::get('/koper', function () {
    return view('Produk.Koper.koper');
});

//  Produk Hotel
Route::get('/hotel', [HotelProdukController::class, 'index'])->name('hotel.index');
// Route::get('/hotel/regional/{any}', [HotelProdukController::class,'hotelIndex'])->name('hotel.index');
Route::get('/hotel/{slug}', [HotelProdukController::class, 'show'])->name('hotel.show');
Route::post('/prosespesanhotel', [HotelProdukController::class, 'proses'])->name('hotel.prosespesan');
Route::get('/detailpesananhotel', [HotelProdukController::class, 'detailPesanan'])->name('hotel.detailPesanan');
Route::get('/formulir', [FormulirProdukController::class, 'formulirHotel'])->name('formulirHotel');
Route::post('/formulir/send', [FormulirProdukController::class, 'formulirHotelStore'])->name('formulirHotel.store');
Route::get('/list/hotel', [HotelProdukController::class, 'hotelIndex'])->name('list.hotel');

// Produk Hotel Search
Route::post('/hotel/search', [HotelProdukController::class, 'search'])->name('hotel.search');
// Route::get('/hotel/{slug}', [HotelProdukController::class, 'searchRoom'])->name('hotel.search.room');
Route::post('/hotel/filterByPrice', [HotelProdukController::class, 'filterByPrice'])->name('hotel.filterByPrice');

Route::prefix('/test')->group(function () {
    Route::get('/formulir', function () {
        return view('Produk.Hotel.formulir');
    });
    Route::get('/index', function () {
        return view('Produk.Hotel.index');
    });
    Route::get('/listhotel', function () {
        return view('Produk.Hotel.search');
    })->name('listhotel');
    Route::get('/detail', function () {
        return view('Produk.Hotel.detail');
    });
    Route::get('/detail/pemesanan', function () {
        return view('Produk.Hotel.detail-pemesanan');
    });
});
//  Produk Xstay
// Route::get('/xstay/regional/{any}', [XstayProdukController::class, 'xstayIndex'])->name('xstay.index');
Route::get('/xstay', [XstayProdukController::class, 'index'])->name('xstay.index');
Route::get('/xstayDetail/{slug}', [XstayProdukController::class, 'show'])->name('xstay.show');
Route::post('/prosespesanxstay', [XstayProdukController::class, 'proses'])->name('xstay.prosespesan');
Route::get('/detailpesananxstay', [XstayProdukController::class, 'detailPesanan'])->name('xstay.detailPesanan');
Route::get('list/xstay', [XstayProdukController::class, 'xstayIndex'])->name('list.xstay');

// Produk Xstay Search
Route::post('/xstay/search', [XstayProdukController::class, 'search'])->name('xstay.search');
Route::post('/xstay/searchByPrice', [XstayProdukController::class, 'searchByPrice'])->name('xstay.searchByPrice');

// Route::prefix('/testxstay')->group(function () {
//     Route::get('/xstay', function () {
//         return view('Produk.Xstay.index');
//     });
//     Route::get('/test/search', function () {
//         return view('Produk.Xstay.search');
//     });
//     Route::get('/xstay-detail', function () {
//         return view('Produk.Xstay.detail');
//     });
//     Route::get('/xstay-detail/detail-pemesanan', function () {
//         return view('Produk.Xstay.detail-pemesanan');
//     });
// });

//  Produk Activity
Route::get('/activity', [ActivityProdukController::class, 'index'])->name('activity.index');
// Route::get('/activity/regional/{any}', [ActivityProdukController::class,'activityIndex'])->name('activity.index');
Route::get('/activity/{slug}', [ActivityProdukController::class, 'show'])->name('activity.show');
Route::post('/activity/{slug}', [ActivityProdukController::class, 'activityOrder'])->name('activity.activityOrder');
Route::post('/activity-search', [ActivityProdukController::class, 'search'])->name('activity.search');
Route::post('/activity-searchByHarga', [ActivityProdukController::class, 'searchByHarga'])->name('activity.searchByHarga');
Route::get('/activity/{slug}/order', [ActivityProdukController::class, 'viewToOrder'])->name('activity.viewToOrder');
Route::post('/activity/{slug}/order', [ActivityProdukController::class, 'addToOrder'])->name('activity.addToOrder');
Route::get('/activity-detail', function () {
    return view('Produk.Activity.detail');
});
Route::get('/activity-detail-pemesanan', function () {
    return view('Produk.Activity.detail-pemesanan');
});
Route::get('/list/activity', [ActivityProdukController::class, 'activityIndex'])->name('list.activity');


// Transfer
Route::get('/transfer', [TransferProdukController::class, 'index'])->name('transfer.index');
// Route::get('/transfer/regional/{any}', [TransferProdukController::class, 'transferIndex'])->name('transfer.index');
Route::get('/transfer/{slug}', [TransferProdukController::class, 'show'])->name('transfer.show');
Route::post('/prosespesantransfer', [TransferProdukController::class, 'proses'])->name('transfer.prosespesan');
Route::get('/detailpesananprivate', [TransferProdukController::class, 'detailPesananPrivate'])->name('transfer.detailPesanan.private');
Route::get('/detailpesananumum', [TransferProdukController::class, 'detailPesananUmum'])->name('transfer.detailPesanan.umum');
Route::get('/list/transfer', [TransferProdukController::class, 'transferIndex'])->name('list.transfer');

// Transfer search
Route::post('/transfer/search', [TransferProdukController::class, 'search'])->name('transfer.search');
Route::post('/transfer/filterByHargaTransfer', [TransferProdukController::class, 'filterByHargaTransfer'])->name('transfer.filterByHargaTransfer');
Route::get('/formulir', [FormulirProdukController::class, 'formulirTransfer'])->name('formulirTransfer');
Route::post('/formulir/send', [FormulirProdukController::class, 'formulirTransferStore'])->name('formulirTransfer.store');

Route::prefix('testtransfer')->group(function () {
    Route::get('/', function () {
        return view('Produk.Transfer.index');
    });
    // Route::get('/formulir', function () {
    //     return view('Produk.Transfer.formulir');
    // });
    Route::get('/search', function () {
        return view('Produk.Transfer.search');
    });
    // Route::get('/detail', function () {
    //     return view('Produk.Transfer.detail');
    // });
    Route::get('/pemesanan-bandara', function () {
        return view('Produk.Transfer.form-private');
    });
    Route::get('/pemesanan', function () {
        return view('Produk.Transfer.form-pemesanan');
    });
    Route::get('/detail-jadwal', function () {
        return view('Produk.Transfer.detail-schedule');
    });
});

// Rental
Route::get('/rental', [RentalProdukController::class, 'index'])->name('rental.index');
// Route::get('/rental/regional/{any}', [RentalProdukController::class, 'rentalIndex'])->name('rental.index');
Route::get('/rental/{slug}', [RentalProdukController::class, 'show'])->name('rental.show');
Route::post('/prosespesanrental', [RentalProdukController::class, 'proses'])->name('rental.prosespesan');
Route::get('/detailpesananrental', [RentalProdukController::class, 'detailPesanan'])->name('rental.detailPesanan');
Route::get('list/rental', [RentalProdukController::class, 'rentalIndex'])->name('list.rental');

// Rental search
Route::post('/rental/search', [RentalProdukController::class, 'search'])->name('rental.search');
Route::post('/rental/searchByHarga', [RentalProdukController::class, 'searchByHarga'])->name('rental.searchByHarga');

Route::prefix('testrental')->group(function () {
    // Route::get('/', function () {
    //     return view('Produk.Rental.index');
    // });
    // Route::get('/detail', function () {
    //     return view('Produk.Rental.detail');
    // });
    Route::get('/pemesanan', function () {
        return view('Produk.Rental.form-pemesanan');
    });
    Route::get('/search', function () {
        return view('Produk.Rental.search');
    });
});

// Tur
Route::get('/tur', [TurProdukController::class, 'index'])->name('tur.index');
Route::post('/tur/search', [TurProdukController::class, 'search'])->name('tur.search');
Route::post('/tur/searchByHarga', [TurProdukController::class, 'searchByHarga'])->name('tur.searchByHarga');
Route::get('/tur/{slug}', [TurProdukController::class, 'show'])->name('tur.show');
Route::post('/tur/{slug}', [TurProdukController::class, 'turOrder'])->name('tur.turOrder');
// Route::get('/tur/regional/{any}', [TurProdukController::class,'turIndex'])->name('tur.index');
Route::get('/tur/{slug}/order', [TurProdukController::class, 'viewToOrder'])->name('tur.viewToOrder');
Route::post('/tur/{slug}/order', [TurProdukController::class, 'addToOrder'])->name('tur.addToOrder');
Route::get('formulir', [FormulirProdukController::class, 'formulirTur'])->name('formulirTour');
Route::post('formulir/send', [FormulirProdukController::class, 'formulirTurStore'])->name('formulirTour.store');
Route::post('/tur/review/add-review', [TurProdukController::class, 'reviewProductStore'])->name('reviewProduct.store');
Route::get('/list/tur/', [TurProdukController::class, 'turIndex'])->name('list.tur');


// Route::get('/tur-formulir', function () {
//     return view('Produk.Tur.formulir');
// });

// Route::get('/tur-detail-pemesanan', function () {
//     return view('Produk.Tur.detail-pemesanan-open-trip');
// });
// Route::get('/tur-detail-private-pemesanan', function () {
//     return view('Produk.Tur.detail-pemesanan');
// });

// Route::prefix('tur')->group(function () {
//     // Route::get('/', function () {
//     //     return view('Produk.Tur.index');
//     // });

//     // Route::get('/formulir', function () {
//     //     return view('Produk.Tur.formulir');
//     // });

//     // Route::get('/search', function () {
//     //     return view('Produk.Tur.search');
//     // });

//     // Route::get('/detail', function () {
//     //     return view('Produk.Tur.detail');
//     // });

//     // Route::get('/detail-private', function () {
//     //     return view('Produk.Tur.detail-tur-private');
//     // });

//     // Route::get('/detail-pemesanan', function () {
//     //     return view('Produk.Tur.detail-pemesanan-open-trip');
//     // });

//     // Route::get('/detail-private-pemesanan', function () {
//     //     return view('Produk.Tur.detail-pemesanan');
//     // });
// });

//Login Landing Page Seller
Route::get('/Login-Seller', function () {
    return view('Homepage.User.login-seller');
})->name('LoginSeller');

//Register Landing Page Seller
Route::get('/Register-Seller', function () {
    return view('Homepage.User.Register-Seller');
})->name('RegisterSeller');

//Landing Page Seller
Route::get('/Seller', function () {
    return view('Homepage.Landing Page.landing-page-seller');
})->name('LP-Seller');

//Login Landing Page Agen
Route::get('/Login-Agen', function () {
    return view('Homepage.User.login-agen');
})->name('LoginAgen');

//Register Landing Page Agen
Route::get('/Register-Agen', function () {
    return view('Homepage.User.register-agen');
})->name('RegisterAgen');

//Landing Page Agen
Route::get('/Agen', function () {
    return view('Homepage.Landing Page.landing-page-agen');
})->name('LP-Agen');


//Login Landing Page Traveller
Route::get('/Login-Traveller', function () {
    return view('Homepage.User.login-traveller');
})->name('LoginTraveller');

//Register Landing Page Traveller
Route::get('/Register-Traveller', function () {
    return view('Homepage.User.register-traveller');
})->name('RegisterTraveller');

//Inbox Seller
Route::get('inbox/seller/', [MessageController::class, 'indexSeller'])->name('inbox.seller');
Route::get('inbox/seller/{sender}/{receiver}', [MessageController::class, 'inboxSeller'])->name('chat.seller');
Route::post('/chat/seller/send', [MessageController::class, 'inboxSellerStore'])->name('inbox.seller.store');


// Admin
Route::group(['middleware' => ['auth', 'CekRole:admin']], function () {



    Route::prefix('dashboard')->group(function () {
        Route::get('/admin/kamtuu', function () {
            return view('BE.admin.dashboard-admin');
        })->name('admin.dashboard');
        Route::get('/admin/kamtuu/master-user', function () {
            return view('BE.admin.master-user');
        });
        Route::get('/admin/kamtuu/master-place', function () {
            return view('BE.admin.master-place');
        });
        Route::get('/admin/kamtuu/master-tag', function () {
            return view('BE.admin.master-tag');
        });
        //CRUD settings
        //        Route::get('/setting/sections', [SettingHomepagesController::class, 'store'])->name('store.section');
        Route::get('/admin/kamtuu/section', [SettingHomepagesController::class, 'index'])->name('section.index');
        Route::put('/admin/kamtuu/section/{id}/update', [SettingHomepagesController::class, 'update'])->name('section.update');
        Route::get('/admin/kamtuu/section/{id}', [SettingHomepagesController::class, 'edit'])->name('section.edit');
        Route::post('/admin/kamtuu/section/create', [SettingHomepagesController::class, 'store'])->name('section.store');
        Route::post('/admin/kamtuu/section/create-home-gallery', [SettingHomepagesController::class, 'storeImageSlider'])->name('section.homeGallery');
        Route::delete('/admin/kamtuu/section/{id}', [SettingHomepagesController::class, 'destroy'])->name('section.delete');

        //    newsletter
        Route::get('/admin/kamtuu/newsletter', [ArticleController::class, 'index'])->name('newsletter.index');
        Route::post('/admin/kamtuu/newsletter/create', [ArticleController::class, 'store'])->name('newsletter.store');
        Route::put('/admin/kamtuu/newsletter/{id}/update', [ArticleController::class, 'update'])->name('newsletter.update');
        Route::get('/admin/kamtuu/newsletter/{id}', [ArticleController::class, 'edit'])->name('newsletter.edit');
        //    Route::get('/destindonesia/article/profile', [ArticleController::class,'profileWriter'])->name('newsletter.profile');
        Route::delete('/admin/kamtuu/newsletter/delete/{id}', [ArticleController::class, 'destroy'])->name('newsletter.destroy');

        //    Slide Image
        Route::get('/admin/kamtuu/slider-homepage', [SettingHomepagesController::class, 'addSliderImage'])->name('slider-image.index');
        Route::post('slider-homepage-store', [SettingHomepagesController::class, 'imageSliderCrop'])->name('slider-image.store');

        //    Master Route
        Route::resource('/admin/kamtuu/masterroute', MasterrouteController::class);
        Route::post('/admin/kamtuu/masterroute/{id}/clone',[MasterrouteController::class,'clone'])->name('masterroute.clone');
        Route::post('/admin/kamtuu/masterroute/{id}/clone/parent',[MasterrouteController::class,'parentClone'])->name('masterroute.clone.parent');
        Route::get('/admin/kamtuu/masterroute/{index}/{key}/{id}/edit/detail',[MasterrouteController::class,'editDetail'])->name('masterroute.edit.detail');
        Route::put('/admin/kamtuu/masterroute/{key}/{id}/update/detail',[MasterrouteController::class,'updateDetail'])->name('masterroute.update.detail');
        Route::delete('/admin/kamtuu/masterroute/{index}/{key}/{id}/delete/detail',[MasterrouteController::class,'deleteDetail'])->name('masterroute.delete.detail');
        Route::get('/admin/kamtuu/datas',[MasterrouteController::class,'datas'])->name('masterroute.json.datas');
        //    Destindonesia Objek
        Route::get('/admin/kamtuu/place', [SettingGeneralController::class, 'select2Pulau'])->name('select2Pulau');
        Route::get('/admin/kamtuu/place/objek', [SettingGeneralController::class, 'objekDestindonesia'])->name('objek');
        Route::get('/admin/kamtuu/place/objek/edit/{id}', [SettingGeneralController::class, 'objekEdit'])->name('objek.edit');
        Route::post('/admin/kamtuu/place/objek/edit/update/{id}', [SettingGeneralController::class, 'objekUpdate'])->name('objek.update');
        Route::post('/admin/kamtuu/place/objek/tambah-objek', [SettingGeneralController::class, 'objekStore'])->name('objek.store');
        Route::post('/admin/kamtuu/place/objek/gallery/edit/{id}', [SettingGeneralController::class, 'gallery_objek_edit'])->name('objek.gallery.edit');
        Route::delete('/admin/kamtuu/place/objek/delete-objek/{id}', [SettingGeneralController::class, 'deleteObjek'])->name('objek.delete');


        Route::get('/admin/kamtuu/place/wisata', [SettingGeneralController::class, 'wisataDestindonesia'])->name('wisata');
        Route::get('/admin/kamtuu/place/wisata/edit/{id}', [SettingGeneralController::class, 'wisataEdit'])->name('wisata.edit');
        Route::post('/admin/kamtuu/place/wisata/update/{id}', [SettingGeneralController::class, 'wisataUpdate'])->name('wisata.update');
        Route::post('/admin/kamtuu/place/wisata/tambah-wisata', [SettingGeneralController::class, 'wisataStore'])->name('wisata.store');
        Route::delete('/admin/kamtuu/place/wisata/delete-wisata/{id}', [SettingGeneralController::class, 'wisataDelete'])->name('wisata.delete');

        //updateGalleryWisata
        Route::post('/admin/kamtuu/place/wisata/update/galery/{id}', [SettingGeneralController::class, 'updateGalleryWisata'])->name('wisata.galeri.edit');
        //    FAQs
        Route::get('/admin/kamtuu/faqs', [SettingGeneralController::class, 'addDynamics'])->name('faqs.index');
        Route::post('/admin/kamtuu/faqs/store', [SettingGeneralController::class, 'storeDynamics'])->name('faqs.store');

        //    Formulir List Admin
        Route::get('/admin/kamtuu/formulir/list', [FormulirProdukController::class, 'formulirList'])->name('formulirList');
        Route::get('/admin/kamtuu/formulir/list/tur/{id}', [FormulirProdukController::class, 'formulirTurShow'])->name('formulirTur.show');
        Route::get('/admin/kamtuu/formulir/list/hotel/{id}', [FormulirProdukController::class, 'formulirHotelShow'])->name('formulirHotel.show');
        Route::get('/admin/kamtuu/formulir/list/transfer/{id}', [FormulirProdukController::class, 'formulirTransferShow'])->name('formulirTransfer.show');

        Route::get('/admin/kamtuu/inbox', [MessageController::class, 'index'])->name('inbox');
        Route::post('/admin/kamtuu/inbox/send', [MessageController::class, 'inboxStore'])->name('inbox.store');

        // Master Extra
        Route::get('/admin/kamtuu/extra', [SettingGeneralController::class, 'viewExtra'])->name('extra.index');
        Route::post('/admin/kamtuu/extra', [SettingGeneralController::class, 'addExtra'])->name('extra.store');
        Route::delete('/admin/kamtuu/extra/{id}/delete', [SettingGeneralController::class, 'destroyExtra'])->name('extra.destroy');
        Route::get('/admin/kamtuu/extra/{id}', [SettingGeneralController::class, 'editExtra'])->name('extra.edit');
        Route::put('/admin/kamtuu/extra/{id}/update', [SettingGeneralController::class, 'updateExtra'])->name('extra.update');

        Route::get('/admin/kamtuu/user/index', [SettingGeneralController::class, 'showUser'])->name('user.admin-add');
        Route::get('/admin/kamtuu/user', [SettingGeneralController::class, 'createUser'])->name('user.admin-create');
        Route::post('/admin/kamtuu/user', [SettingGeneralController::class, 'saveUser'])->name('user.admin-store');
        Route::get('/admin/kamtuu/user/{id}', [SettingGeneralController::class, 'editUser'])->name('user.admin-edit');
        Route::put('/admin/kamtuu/user/{id}', [SettingGeneralController::class, 'updateUser'])->name('user.admin-update');
        Route::delete('/admin/kamtuu/user/{id}', [SettingGeneralController::class, 'deleteUser'])->name('user.admin-delete');

        //    Pajak Admin
        Route::get('/admin/kamtuu/pajak', [PajakAdminController::class, 'index'])->name('pajak');
        Route::put('/admin/kamtuu/pajak/update', [PajakAdminController::class, 'pajakUpdate'])->name('pajak.update');

        //        Pajak Local
        Route::post('/admin/kamtuu/pajak/add', [PajakAdminController::class, 'pajakLocalStore'])->name('pajak.local.store');
        Route::delete('/admin/kamtuu/pajak/delete/{id}', [PajakAdminController::class, 'pajakDestroy'])->name('pajak.local.destroy');
        Route::get('/admin/kamtuu/pajak/{id}', [PajakAdminController::class, 'pajakLocalEdit'])->name('pajak.local.edit');
        Route::put('/admin/kamtuu/pajak/local/update/{id}', [PajakAdminController::class, 'pajakLocalUpdate'])->name('pajak.local.update');

        //    Kupon
        Route::get('/admin/kamtuu/kupon', [KuponController::class, 'index'])->name('kupon');
        Route::post('/admin/kamtuu/kupon/add', [KuponController::class, 'kuponStore'])->name('kupon.store');
        //        Route::put('/admin/kamtuu/kupon/{id}', [KuponController::class, 'kuponEdit'])->name('kupon.edit');
        //        Route::get('/admin/kamtuu/kupon/update', [KuponController::class, 'kuponUpdate'])->name('kupon.update');
        //        Route::delete('/admin/kamtuu/kupon/delete/{id}', [KuponController::class, 'kuponDestroy'])->name('kupon.destroy');

        Route::get('/admin/kamtuu/setting/order-regency', [SettingGeneralController::class, 'orderRegency'])->name('order.regency');
        Route::post('/admin/kamtuu/setting/order-regency/store', [SettingGeneralController::class, 'orderRegencyStore'])->name('regencyOrder.store');
        //      acc  mylisting
        Route::get('/admin/kamtuu/acc-listing', [SettingGeneralController::class, 'accListing'])->name('accListing');
        Route::put('/admin/kamtuu/listing/confirm/{id}', [SettingGeneralController::class, 'accListingUpdate'])->name('accListingUpdate');
        Route::post('/admin/kamtuu/produk/search', [SettingGeneralController::class, 'searchListing'])->name('listing.search');
    });
});
// Route::get('/destindonesia/newsletter/{slug}',[ArticleController::class,'show'])->name('newsletter.show');

// Master Route
// Route::resource('masterroute', MasterrouteController::class);
Route::get('/test/barcode', [TurProdukController::class, 'testBarcode'])->name('test.qrCode');
// Route::get('/downlod/barcode',[TurProdukController::class,'downloadBarcode'])->name('download.qrCode');
// Route::get('/transfer/{slug}', [TransferProdukController::class,'show'])->name('xstay.show');

// download barcode
Route::get('/download/barcode', function (Request $request) {

    $img = base64_decode($request->query('base64'));
    $extFile  = '.png';
    $nameFile = $request->query('slug') . $extFile;

    if (!file_exists($nameFile)) {
        file_put_contents('storage/' . $nameFile, $img);
    }

    return response()->download('storage/' . $nameFile);
})->name('download.qrCode');

//Inbox Produk
Route::get('/chat/{user_id}/{type}', [MessageController::class, 'index'])->name('inbox');
Route::post('/chat/send/{user_id}/{id}', [MessageController::class, 'inboxStore'])->name('inbox.store');
Route::get('/product/name/list/{type}', [homepagesController::class, 'productListRegency']);
Route::get('/tur/tag/location', [ListingTurController::class, 'getTagLocation']);

//Master Drop Pick
Route::prefix('seller/transfer/master-drop-pick')->group(function () {
    Route::get('/all', [MasterDropPickController::class, 'viewMasterDropPick'])->name('masterDropPick');
    Route::get('/add', [MasterDropPickController::class, 'addMasterDropPick'])->name('addMasterDropPick');
    Route::post('/store', [MasterDropPickController::class, 'storeMasterDropPick'])->name('storeMasterDropPick');
    Route::get('/edit/{id}', [MasterDropPickController::class, 'editMasterDropPick'])->name('editMasterDropPick');
    Route::put('/update/{id}', [MasterDropPickController::class, 'updateMasterDropPick'])->name('updateMasterDropPick');
    Route::delete('/delete/{id}', [MasterDropPickController::class, 'deleteMasterDropPick'])->name('deleteMasterDropPick');
});

Route::post('/destindonesia/want-to-go', [SettingGeneralController::class, 'wontToGo'])->name('ingin-kesana-submit');
Route::post('/destindonesia/been-there', [SettingGeneralController::class, 'beenThere'])->name('pernah-kesana-submit');
// Route::post('/destindonesia/wisata/category',[SettingGeneralController::class, 'objectByCategory'])->name('object.by-category');
Route::get('/destindonesia/objek/wisata',[SettingGeneralController::class, 'objectByCategory'])->name('object.by-category');
Route::get('/destindonesia/objek/allwisata',[SettingGeneralController::class, 'allObjectByCategory'])->name('all-object.by-category');
Route::get('/master/route/results',[MasterrouteController::class,'getRoute'])->name('master.route.result');
// Route::get('request/not-found',function(){

// })
// route master titik
Route::resource('/admin/kamtuu/route/master-titik',MasterpointController::class);
Route::get('/admin/kamtuu/route/cari-titik',[MasterpointController::class,'search'])->name('master.titik.cari');
Route::get('/admin/kamtuu/route/data-titik',[MasterpointController::class,'datas'])->name('master.titik.datas');
Route::get('/admin/kamtuu/route/regencies/{id}',[MasterpointController::class,'getKabupaten'])->name('master.titik.regencies');
Route::get('/admin/kamtuu/route/districts/{id}',[MasterpointController::class,'getKecamatan'])->name('master.titik.district');
Route::get('/admin/kamtuu/route/village/{id}',[MasterpointController::class,'getDesa'])->name('master.titik.village');
Route::post('/admin/kamtuu/route/master-titik/{id}/clone',[MasterpointController::class,'clone'])->name('master.titik.clone');
Route::delete('/admin/kamtuu/route/multiple-delete',[MasterpointController::class,'multiDelete'])->name('master.titik.multi-delete');

// route destindonesia card
Route::get('/destindonesi/filter/card/list/{id}',[DestindonesiaHomeController::class,'filterByRegency'])->name('filter.dest.regency');
Route::get('/destindonesi/filter/card-cat/list/{id}',[DestindonesiaHomeController::class,'filterByListCategory'])->name('filter.dest.category');

Route::post('/review/post-gallery',[TurProdukController::class,'reviewPostGallery'])->name('gallery-post');
Route::get('/detail-toko/{id}', [SellerController::class,'detailToko'])->name('toko.show');

//articel destindonesi search
Route::get('/search/destindonesia',[DestindonesiaHomeController::class,'searchDestindo'])->name('search.destindo');
Route::get('/search/result/destindo',[DestindonesiaHomeController::class,'seacrhResult'])->name('search.result');
Route::post('/send/verify-email',[EmailVerificationController::class,'sendEmailVerify'])->middleware('auth')->name('verify.request');
Route::get('/verify-email/{id}/{hash}',[EmailVerificationController::class,'emailVerify'])->middleware(['auth','signed'])->name('email.verifed');
Route::get('/auth/{provider}/',[SosialiteController::class,'redirect'])->name('auth.social.redirect');
Route::get('/auth/{provider}/callback',[SosialiteController::class,'handleCallback']);

Route::get('/master/pencarian-titik',[homepagesController::class,'getMasterTitik'])->name('master.titik');
Route::get('/product/ListRegency/{type}',[homepagesController::class,'productListRegency'])->name('list.regency');
Route::get('/product/detail/{type}/{mode?}',[homepagesController::class,'getProductDetail'])->name('product.detail');
Route::get('/home/konten',[homepagesController::class,'getContent'])->name('home.content');
