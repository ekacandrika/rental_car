CREATE DATABASE  IF NOT EXISTS `rental_car` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `rental_car`;
-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: rental_car
-- ------------------------------------------------------
-- Server version	8.0.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product_detail`
--

DROP TABLE IF EXISTS `product_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_detail` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint unsigned NOT NULL,
  `province_id` bigint unsigned DEFAULT NULL,
  `province_id_1` bigint unsigned DEFAULT NULL,
  `province_id_2` bigint unsigned DEFAULT NULL,
  `regency_id` bigint unsigned DEFAULT NULL,
  `regency_id_1` bigint unsigned DEFAULT NULL,
  `regency_id_2` bigint unsigned DEFAULT NULL,
  `district_id` bigint unsigned DEFAULT NULL,
  `village_id` bigint unsigned DEFAULT NULL,
  `rute_id` bigint unsigned DEFAULT NULL,
  `kamar_id` bigint unsigned DEFAULT NULL,
  `discount_id` bigint unsigned DEFAULT NULL,
  `paket_id` bigint unsigned DEFAULT NULL,
  `extra_id` bigint unsigned DEFAULT NULL,
  `kategori_id` bigint unsigned DEFAULT NULL,
  `itenenary_id` bigint unsigned DEFAULT NULL,
  `harga_id` bigint unsigned DEFAULT NULL,
  `pilihan_id` bigint unsigned DEFAULT NULL,
  `mobil_id` bigint unsigned DEFAULT NULL,
  `detailmobil_id` bigint unsigned DEFAULT NULL,
  `id_detail_kendaraan` bigint unsigned DEFAULT NULL,
  `transfer_id` bigint unsigned DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jml_hari` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jml_malam` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipe_tur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `durasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_fasilitas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi_fasilitas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_amenitas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `judul_amenitas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isi_amenitas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `makanan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahasa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tingkat_petualangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ringkasan` text COLLATE utf8mb4_unicode_ci,
  `deskripsi` text COLLATE utf8mb4_unicode_ci,
  `paket_termasuk` text COLLATE utf8mb4_unicode_ci,
  `paket_tidak_termasuk` text COLLATE utf8mb4_unicode_ci,
  `catatan` text COLLATE utf8mb4_unicode_ci,
  `komplemen` text COLLATE utf8mb4_unicode_ci,
  `pilihan_tambahan` text COLLATE utf8mb4_unicode_ci,
  `batas_pembayaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kebijakan_pembatalan_sebelumnya` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kebijakan_pembatalan_sesudah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kebijakan_pembatalan_potongan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_maps` text COLLATE utf8mb4_unicode_ci,
  `foto_maps_1` text COLLATE utf8mb4_unicode_ci,
  `foto_maps_2` text COLLATE utf8mb4_unicode_ci,
  `izin_ekstra` enum('Bolehkan','Tidak Usah') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery` text COLLATE utf8mb4_unicode_ci,
  `thumbnail` text COLLATE utf8mb4_unicode_ci,
  `faq` text COLLATE utf8mb4_unicode_ci,
  `base_url` text COLLATE utf8mb4_unicode_ci,
  `available` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_detail_product_id_foreign` (`product_id`),
  CONSTRAINT `product_detail_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_detail`
--

LOCK TABLES `product_detail` WRITE;
/*!40000 ALTER TABLE `product_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_detail` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-13 16:44:39
