import "./bootstrap";
import "../assets/css/custom.css";
import "../assets/css/jquery-ui.css";
import "../assets/css/timepicker.css";
import "../assets/css/gijgo.css";
import "../assets/css/glightbox.min.css";
import "../assets/css/errors.css";
import "../assets/css/chat-box.css";

import "../assets/js/jquery-1.12.4.min.js";
import "../assets/js/jquery-ui.min.js";
import "../assets/js/timepicker.js";
import "../assets/js/gijgo.js";
import "../assets/js/glightbox.min.js";
// import "../assets/js/chart.js";
// import '../assets/js/gijgo.min.js';
// import "../assets/js/iconpicker.js";

import "../assets/fonts/gijgo-material.ttf";
import "../assets/fonts/summernote.eot";
import "../assets/fonts/summernote.ttf";
import "../assets/fonts/summernote.woff";
import "../assets/fonts/summernote.woff2";

import Alpine from "alpinejs";

// IconPicker
import "vanilla-icon-picker/dist/themes/default.min.css"; // 'default' theme
import IconPicker from "vanilla-icon-picker";

window.Alpine = Alpine;
window.IconPicker = IconPicker;

Alpine.start();
