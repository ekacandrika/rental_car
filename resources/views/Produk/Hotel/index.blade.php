<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kamtuu Hotel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap" />
    <link rel="stylesheet" href="https://unpkg.com/flowbite@1.5.3/dist/flowbite.min.css" />
    <script src="https://cdn.tailwindcss.com"></script>
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        /* Hide scrollbar for Chrome, Safari and Opera */
        .no-scrollbar::-webkit-scrollbar {
            display: none;
        }

        /* Hide scrollbar for IE, Edge and Firefox */
        .no-scrollbar {
            -ms-overflow-style: none;
            /* IE and Edge */
            scrollbar-width: none;
            /* Firefox */
        }

        .swiper-pagination-bullet {
            background: rgb(173, 151, 151);
            opacity: 1;
        }

        .swiper-pagination-bullet-active {
            background: white;
        }
    </style>
    <style>
        .slide-container {
            max-width: 1120px;
            width: 100%;
            padding: 40px 0;
        }

        .slide-content {
            margin: 0 40px;
            overflow: hidden;
            border-radius: 25px;
        }

        .card {
            border-radius: 25px;
            background-color: #FFF;
        }

        .image-content,
        .card-content {
            display: flex;
            flex-direction: column;
            align-items: center;
            padding: 10px 14px;
        }

        .image-content {
            position: relative;
            row-gap: 5px;
            padding: 25px 0;
        }

        .overlay {
            position: absolute;
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
            background-color: #4070F4;
            border-radius: 25px 25px 0 25px;
        }

        .overlay::before,
        .overlay::after {
            content: '';
            position: absolute;
            right: 0;
            bottom: -40px;
            height: 40px;
            width: 40px;
            background-color: #4070F4;
        }

        .overlay::after {
            border-radius: 0 25px 0 0;
            background-color: #FFF;
        }

        .card-image {
            position: relative;
            height: 150px;
            width: 150px;
            border-radius: 50%;
            background: #FFF;
            padding: 3px;
        }

        .card-image .card-img {
            height: 100%;
            width: 100%;
            object-fit: cover;
            border-radius: 50%;
            border: 4px solid #4070F4;
        }

        .name {
            font-size: 18px;
            font-weight: 500;
            color: #333;
        }

        .description {
            font-size: 14px;
            color: #707070;
            text-align: center;
        }

        .button {
            border: none;
            font-size: 16px;
            color: #FFF;
            padding: 8px 16px;
            background-color: #4070F4;
            border-radius: 6px;
            margin: 14px;
            cursor: pointer;
            transition: all 0.3s ease;
        }

        .button:hover {
            background: #265DF2;
        }

        .swiper-navBtn {
            color: #6E93f7;
            transition: color 0.3s ease;
        }

        .swiper-navBtn:hover {
            color: #4070F4;
        }

        .swiper-navBtn::before,
        .swiper-navBtn::after {
            font-size: 35px;
        }

        .swiper-button-next {
            right: 0;
        }

        .swiper-button-prev {
            left: 0;
        }

        .swiper-pagination-bullet {
            background-color: #6E93f7;
            opacity: 1;
        }

        .swiper-pagination-bullet-active {
            background-color: #4070F4;
        }

        @media screen and (max-width: 768px) {
            .slide-content {
                margin: 0 10px;
            }

            .swiper-navBtn {
                display: none;
            }
        }
    </style>
    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border-b border-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Carousel --}}
    <div class="container mx-auto my-5 sm:rounded-lg swiper carousel-banner">
        <div
            class="swiper-wrapper w-[380px] sm:w-[768px] h-[192px] md:w-[1024px] md:h-[256px] lg:w-[1200px] lg:h-[300px] xl:w-[1536px] xl:h-[384px]">
            <a href="#" class="swiper-slide">
                <img class="w-full h-full px-5 sm:px-0 object-center object-cover sm:rounded-lg"
                    src="https://images.unsplash.com/photo-1501785888041-af3ef285b470?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-1">
            </a>
            <a href="#" class="swiper-slide">
                <img class="w-full h-full px-5 sm:px-0 object-center object-cover rounded-lg"
                    src="https://images.unsplash.com/photo-1433838552652-f9a46b332c40?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-2">
            </a>
            <a href="#" class="swiper-slide">
                <img class="w-full h-full px-5 sm:px-0 object-center object-cover rounded-lg"
                    src="https://images.unsplash.com/photo-1518548419970-58e3b4079ab2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-3">
            </a>
            <a href="#" class="swiper-slide">
                <img class="w-full h-full px-5 sm:px-0 object-center object-cover rounded-lg"
                    src="https://images.unsplash.com/photo-1555899434-94d1368aa7af?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-4">
            </a>
        </div>
        <div class=" pl-3 text-left swiper-pagination"></div>
    </div>


    {{-- Search Bar --}}
    {{-- <div class="container mx-auto  my-5">
        <p class="text-4xl font-extrabold text-center">Reserved for Search Bar</p>
    </div> --}}
    <x-hotel.app-hotel :queryroom="$query_room" :totalpengunjung="$total_pengunjung" :umuranak="$umur_anak">
        <select name="search" id="" style="background-image: none;" required
            class="ml-11 px-4 py-2 border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
            <option value="">Destinasi</option>
            @foreach ($hotel as $value)
            @php
            $regency = App\Models\Regency::where('id', $value->productdetail->regency_id)->get();
            @endphp
            @foreach ($regency as $item)
            <option style="font-size: 10px" value="{{ $item->id }}">{{ $item->name }}</option>
            @endforeach
            @endforeach
        </select>
    </x-hotel.app-hotel>

    {{-- Kamtuu About #1 --}}
    <div class="my-auto p-4 grid justify-items-center">
        <p class="text-center text-3xl xl:text-4xl font-semibold">Cara mudah cari hotel di Indonesia, cari di Kamtuu!
        </p>
    </div>
    <div class="container mx-auto my-5 p-5 rounded-lg block md:flex">
        {{-- Desktop --}}
        <table class="table-auto text-center xl:text-justify">
            <thead>
                <tr>
                    <th><img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/icons/people_1.png') }}"
                            alt="people_1">
                    </th>
                    <th><img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/icons/people_1.png') }}"
                            alt="people_1">
                    </th>
                    <th><img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/icons/people_1.png') }}"
                            alt="people_1">
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="align-middle xl:align-bottom px-4">
                        <p class="text-lg lg:text-xl text-center xl:text-center font-semibold">Banyak
                            Pilihan
                        </p>
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <p class="text-lg lg:text-xl text-center xl:text-center font-semibold">Banyak
                            Pilihan</p>
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <p class="text-lg lg:text-xl text-center xl:text-center font-semibold">Banyak
                            Pilihan</p>
                    </td>
                </tr>
                <tr>
                    <td class="align-top px-4">
                        <p class="text-sm lg:text-base text-center xl:text-center font-medium my-2">Lorem Ipsum is
                            simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </td>
                    <td class="align-top px-4">
                        <p class="text-sm lg:text-base text-center xl:text-center font-medium my-2">Lorem Ipsum is
                            simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </td>
                    <td class="align-top px-4">
                        <p class="text-sm lg:text-base text-center xl:text-center font-medium my-2">Lorem Ipsum is
                            simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="my-auto p-4 grid justify-items-center">
        <p class="text-center text-3xl xl:text-4xl font-semibold">Cara Memesan
        </p>
    </div>
    <div class="container mx-auto my-5 p-5 rounded-lg block md:flex">
        {{-- Desktop --}}
        <table class="table-auto text-center xl:text-justify">
            <thead>
                <tr>
                    <th><img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/icons/Group_109.png') }}"
                            alt="Group_109">
                    </th>
                    <th></th>
                    <th><img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/icons/Group_109.png') }}"
                            alt="Group_109">
                    </th>
                    <th></th>
                    <th><img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/icons/Group_109.png') }}"
                            alt="Group_109">
                    </th>
                    <th></th>
                    <th><img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/icons/Group_109.png') }}"
                            alt="Group_109">
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="align-middle xl:align-bottom px-4">
                        <p class="text-lg lg:text-xl text-center xl:text-center font-semibold">Banyak
                            Pilihan
                        </p>
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/icons/arrow-right-solid-1.svg') }}"
                            alt="Group_109">
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <p class="text-lg lg:text-xl text-center xl:text-center font-semibold">Banyak
                            Pilihan</p>
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/icons/arrow-right-solid-1.svg') }}"
                            alt="Group_109">
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <p class="text-lg lg:text-xl text-center xl:text-center font-semibold">Banyak
                            Pilihan</p>
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/icons/arrow-right-solid-1.svg') }}"
                            alt="Group_109">
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <p class="text-lg lg:text-xl text-center xl:text-center font-semibold">Banyak
                            Pilihan</p>
                    </td>
                </tr>
                <tr>
                    <td class="align-top px-4">
                        <p class="text-sm lg:text-base text-center xl:text-center font-medium my-2">Lorem Ipsum is
                            simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </td>
                    <td class="align-top px-4"></td>
                    <td class="align-top px-4">
                        <p class="text-sm lg:text-base text-center xl:text-center font-medium my-2">Lorem Ipsum is
                            simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </td>
                    <td class="align-top px-4"></td>
                    <td class="align-top px-4">
                        <p class="text-sm lg:text-base text-center xl:text-center font-medium my-2">Lorem Ipsum is
                            simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </td>
                    <td class="align-top px-4"></td>
                    <td class="align-top px-4">
                        <p class="text-sm lg:text-base text-center xl:text-center font-medium my-2">Lorem Ipsum is
                            simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <hr class="my-10 border-[#F2F2F2]">

    {{-- Official Stores --}}
    <div class="my-auto p-4 grid justify-items-center">
        <p class="text-center text-3xl xl:text-4xl font-semibold">
            Butuh pesan hotel untuk grup besar?<br>Hubungi kami!
        </p>
        <div class="flex justify-center">
            <a href="/hotel/formulir">
                <x-destindonesia.button-primary text="Pesan Hotel Untuk Grup">
                    @slot('button')
                    Pesan Hotel Untuk Grup
                    @endslot
                </x-destindonesia.button-primary>
            </a>
        </div>
    </div>

    <hr class="my-10 border-[#F2F2F2]">

    {{-- Review --}}
    <div class="container mx-auto my-5">
        <p class="text-center text-2xl sm:text-3xl font-semibold my-3 mx-3 sm:mx-0">
            Apa kata mereka?
        </p>

        <div class="swiper review-swipper">
            <div class="mx-auto py-5 swiper-wrapper">
                <div class="flex justify-center swiper-slide">
                    <div
                        class="bg-gray-200 grid grid-rows-2 sm:grid-rows-none sm:grid-cols-2 justify-items-center content-center py-3 mx-3 w-[320px] sm:w-full max-w-[520px] sm:h-[240px] rounded-lg shadow-lg">
                        <div class="py-3 rounded-lg">
                            <img class="w-full h-full object-cover object-center rounded-lg"
                                src="{{ asset('storage/img/foto-profile..png') }}" alt="profile-picture">
                        </div>
                        <div class="text-center sm:text-left py-3 px-7 sm:pr-7">
                            <p class="text-xl font-semibold">John Deepy</p>
                            <div class="flex justify-center sm:justify-start my-2">
                                <img src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="20px"
                                    height="20px">
                                <p class="mx-2 font-semibold text-base">
                                    4.7/<span class="text-sm">5</span>
                                </p>
                            </div>
                            <p class="text-sm">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                do
                                eiusmod
                                tempor incididunt
                                ut labore et dolore magna aliqua. Volutpat odio facilisis mauris sit.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </div>

    {{-- FAQ --}}
    <div class="container mx-auto mt-5 mb-10">
        <p class="text-center text-2xl sm:text-3xl font-semibold my-3 mx-3 sm:mx-0">
            Sering Ditanyakan (FAQ)
        </p>

        <div x-data="{ tabs: ['Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'] }">
            <ul class="text-sm font-medium text-justify sm:text-left">
                <template x-for="(tab, index) in tabs" :key="index">
                    <li class="my-2 mx-3 border-b border-gray-200" x-data="{ open: false }">
                        <div class="flex" :class=" { 'pt-5 pb-2': open, 'py-5': !open }">
                            <button class="text-lg sm:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open" x-text="tab"></button>
                            <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        <p class="py-5 font-normal text-[#4F4F4F] text-base sm:text-xl" x-show="open" x-transition>
                            Lorem
                            Ipsum is
                            simply
                            dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to make a type
                            specimen book. </p>
                    </li>
                </template>
            </ul>
        </div>
    </div>


    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script src="https://unpkg.com/flowbite@1.5.3/dist/flowbite.js"></script>
    <script>
        var swiper = new Swiper(".slide-content", {
            slidesPerView: 1,
            spaceBetween: 25,
            loop: true,
            centerSlide: 'true',
            fade: 'true',
            grabCursor: 'true',
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
                dynamicBullets: true,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },

            breakpoints: {
                0: {
                    slidesPerView: 1,
                },
                520: {
                    slidesPerView: 1,
                },
                950: {
                    slidesPerView: 1,
                },
            },
        });
    </script>
    <script>
        var bannerSwiper = new Swiper(".carousel-banner", {
            loop: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
                enabled: false,
            },
            autoplay: {
                delay: 3000,
                pauseOnMouseEnter: true,
                disableOnInteraction: false,
            },
            breakpoints: {
                640: {
                    pagination: {
                        enabled: true
                    }
                }
            }
        });
    </script>

</body>

</html>