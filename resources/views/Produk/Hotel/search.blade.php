<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kamtuu Hotel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap" />

    {{-- select 2 --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
     

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        /* Hide scrollbar for Chrome, Safari and Opera */
        .no-scrollbar::-webkit-scrollbar {
            display: none;
        }

        /* Hide scrollbar for IE, Edge and Firefox */
        .no-scrollbar {
            -ms-overflow-style: none;
            /* IE and Edge */
            scrollbar-width: none;
            /* Firefox */
        }

        .swiper-pagination-bullet {
            background: rgb(173, 151, 151);
            opacity: 1;
        }

        .swiper-pagination-bullet-active {
            background: white;
        }

        .leftcolumn {
            float: left;
            width: 25%;
            box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
        }

        /* Right column */
        .rightcolumn {
            float: left;
            width: 72%;
            margin-left: 20px;
            box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
        }

        .card {
            background-color: white;
            padding: 20px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        @media screen and (max-width: 800px) {

            .leftcolumn,
            .rightcolumn {
                width: 100%;
                padding: 0;
            }
        }
        .select2-container .select2-selection--single {
            height: 100%;
            /* padding-left: 0.75rem; */
            /* padding-right: 0.75rem; */
            padding-top: 0.5rem;
            padding-bottom: 0rem;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 100%;
        }
        .select2-selection__arrow {
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            /* padding-right: 0.75rem; */
        }
        .select2-container--default .select2-selection--single{
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            /* border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px; */
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #444;
            line-height: 35px;
        }
    </style>
    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border-b border-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Carousel --}}
    <div class="container mx-auto my-5 sm:rounded-lg swiper carousel-banner">
        <div
            class="swiper-wrapper w-[380px] sm:w-[768px] h-[192px] md:w-[1024px] md:h-[256px] lg:w-[1200px] lg:h-[300px] xl:w-[1536px] xl:h-[384px]">
            <a href="#" class="swiper-slide">
                <img class="w-full h-full px-5 sm:px-0 object-center object-cover sm:rounded-lg"
                    src="https://images.unsplash.com/photo-1501785888041-af3ef285b470?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-1">
            </a>
            <a href="#" class="swiper-slide">
                <img class="w-full h-full px-5 sm:px-0 object-center object-cover rounded-lg"
                    src="https://images.unsplash.com/photo-1433838552652-f9a46b332c40?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-2">
            </a>
            <a href="#" class="swiper-slide">
                <img class="w-full h-full px-5 sm:px-0 object-center object-cover rounded-lg"
                    src="https://images.unsplash.com/photo-1518548419970-58e3b4079ab2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-3">
            </a>
            <a href="#" class="swiper-slide">
                <img class="w-full h-full px-5 sm:px-0 object-center object-cover rounded-lg"
                    src="https://images.unsplash.com/photo-1555899434-94d1368aa7af?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-4">
            </a>
        </div>
        <div class=" pl-3 text-left swiper-pagination"></div>
    </div>


    {{-- Search Bar --}}
    {{-- <div class="container mx-auto  my-5">
        <p class="text-4xl font-extrabold text-center">Reserved for Search Bar</p>
    </div> --}}
    <x-hotel.app-hotel :queryroom="$query_room" :totalpengunjung="$total_pengunjung" :umuranak="$umur_anak">
        <select name="search" id="searchhotel" style="background-image: none;"
            class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
            <option value="">Destinasi</option>
            @foreach ($hotel as $value)
            @php
            $regency = App\Models\Regency::where('id', $value->productdetail->regency_id)->get();
            @endphp
            @foreach ($regency as $item)
            <option style="font-size: 10px" value="{{ $item->id }}">{{ $item->name }}</option>
            @endforeach
            @endforeach
        </select>
    </x-hotel.app-hotel>

    {{-- Kamtuu About #1 --}}
    <div class="container mx-auto my-5 p-5 rounded-lg block md:flex">
        <p class="text-left text-3xl xl:text-3xl">Filter
        </p>
    </div>
    <div class="container mx-auto my-5 p-5">
        {{-- Desktop --}}
        <div class="row">
            <div class="leftcolumn">
                <div class="card">
                    <h2 class="pb-2">Harga</h2>
                    <form action="{{ route('hotel.filterByPrice') }}" method="POST">
                        @csrf
                        <input type="text" name="min_price" placeholder="Harga Minimum"
                            class="placeholder:text-slate-400 mb-3 block bg-[#F2F2F2] w-full border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-[#9E3D64] focus:ring-[#9E3D64] focus:ring-1 sm:text-sm">
                        <input type="text" name="max_price" placeholder="Harga Maksimum"
                            class="placeholder:text-slate-400 mb-3 block bg-[#F2F2F2] w-full border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-[#9E3D64] focus:ring-[#9E3D64] focus:ring-1 sm:text-sm">
                        <input type="hidden" name="search" value="{{ $data }}">
                        <hr class="my-5 border-[#4F4F4F]">
                        <button
                            class="px-2 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white ">
                            Terapkan
                        </button>
                    </form>
                    {{-- <h2 class="pb-2">Lokasi</h2>
                    <div class="flex items-center mb-4">
                        <input checked id="default-checkbox" type="checkbox" value=""
                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                        <label for="default-checkbox"
                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Lokasi 1</label>
                    </div>
                    <div class="flex items-center">
                        <input id="checked-checkbox" type="checkbox" value=""
                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                        <label for="checked-checkbox"
                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Lokasi 2</label>
                    </div> --}}
                </div>
            </div>
            <div class="flex justify-between items-center mb-4">
                <h5 class="text-xl leading-none text-gray-900 dark:text-white ml-5">Menampilkan
                    {{ $data_detail->count() }} Hotel
                </h5>
                <div class="text-sm font-medium text-blue-600 hover:underline dark:text-blue-500 mr-5">
                    <form>
                        <fieldset>
                            <div
                                class="relative border border-[#4F4F4F] text-gray-500 bg-white shadow-lg focus:border-2 focus:border-[#4F4F4F]">
                                <select class="appearance-none w-full py-2 px-2 bg-white" name="produk-dropdown"
                                    id="produk-dropdown">
                                    <option value="Harga(Termahal-Termurah)">Harga (Termahal-Termurah)</option>
                                </select>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="rightcolumn">
                {{-- <div class=" w-full lg:max-w-full lg:flex mb-5">
                    <div class="h-48 lg:h-auto lg:w-48 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden"
                        style="background-image: url('https://images.unsplash.com/photo-1544644181-1484b3fdfc62?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fGJhbGl8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60')"
                        title="Mountain">
                        <div class="grid place-content-start ml-3" style="margin-top: 100%">
                            <span class="bg-[#FFFFFF] py-[2px] px-[5px] rounded-lg text-gray font-bold">Iklan</span>
                        </div>
                    </div>
                    <div
                        class="border-r border-b border-l border-gray-400 lg:border-l-0 lg:border-t lg:border-gray-400 bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal">
                        <div class="mb-8">
                            <div class="text-gray-900 font-bold text-xl mb-2">Nama Hotel</div>
                            <p class="text-gray-700 text-base">Lokasi</p>
                            <div class="flex justify items-center">
                                <img src="{{ asset('storage') }}/img/icon/person-swimming-solid.svg" alt=""
                                    style="max-width: 3%">
                                &nbsp;
                                <p class="text-gray-700 text-base">
                                    Kolam Renang
                                </p>
                                <img src="{{ asset('storage') }}/img/icon/hot-tub-person-solid.svg" alt=""
                                    style="max-width: 3%" class="ml-5">
                                &nbsp;
                                <div class="text-sm">
                                    <p class="text-gray-700 text-base">
                                        Bathtub Air Panas
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="flex items-center">
                            <div class="grid grid-cols-1 gap-6 sm:grid-cols-3">
                                <div class="col-span-2">
                                    <p class="mb-3 font-light text-dark-500 dark:text-dark-400">Refundable</p>
                                    <p class="mb-3 font-light text-dark-500 dark:text-dark-400"><strong>Rating</strong>
                                        4.7 (12 Ulasan)</p>
                                </div>
                                <p class="mb-3 font-light text-dark-500 dark:text-dark-400" style="margin-left: 82px">
                                    <span class="bg-[#FFB800] py-[2px] px-[5px] rounded-lg text-white font-bold">Sisa 5
                                        Kamar!</span> <br> <strong class="text-gray-900 font-bold text-xl">IDR
                                        100.000</strong> <br> total Rp 150.000
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" w-full lg:max-w-full lg:flex mb-5">
                    <div class="h-48 lg:h-auto lg:w-48 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden"
                        style="background-image: url('https://images.unsplash.com/photo-1544644181-1484b3fdfc62?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fGJhbGl8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60')"
                        title="Mountain">
                    </div>
                    <div
                        class="border-r border-b border-l border-gray-400 lg:border-l-0 lg:border-t lg:border-gray-400 bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal">
                        <div class="mb-8">
                            <div class="text-gray-900 font-bold text-xl mb-2">Nama Hotel</div>
                            <p class="text-gray-700 text-base">Lokasi</p>
                            <div class="flex justify items-center">
                                <img src="{{ asset('storage') }}/img/icon/person-swimming-solid.svg" alt=""
                                    style="max-width: 3%">
                                &nbsp;
                                <p class="text-gray-700 text-base">
                                    Kolam Renang
                                </p>
                                <img src="{{ asset('storage') }}/img/icon/hot-tub-person-solid.svg" alt=""
                                    style="max-width: 3%" class="ml-5">
                                &nbsp;
                                <div class="text-sm">
                                    <p class="text-gray-700 text-base">
                                        Bathtub Air Panas
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="flex items-center">
                            <div class="grid grid-cols-1 gap-6 sm:grid-cols-3">
                                <div class="col-span-2">
                                    <p class="mb-3 font-light text-dark-500 dark:text-dark-400">Refundable</p>
                                    <p class="mb-3 font-light text-dark-500 dark:text-dark-400"><strong>Rating</strong>
                                        4.7 (12 Ulasan)</p>
                                </div>
                                <p class="mb-3 font-light text-dark-500 dark:text-dark-400" style="margin-left: 82px">
                                    <span class="bg-[#D50006] py-[2px] px-[5px] rounded-lg text-white font-bold">Sisa 5
                                        Kamar!</span> <br> <strong class="text-gray-900 font-bold text-xl">IDR
                                        100.000</strong> <br> total Rp 150.000
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" w-full lg:max-w-full lg:flex mb-5">
                    <div class="h-48 lg:h-auto lg:w-48 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden"
                        style="background-image: url('https://images.unsplash.com/photo-1544644181-1484b3fdfc62?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fGJhbGl8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60')"
                        title="Mountain">
                    </div>
                    <div
                        class="border-r border-b border-l border-gray-400 lg:border-l-0 lg:border-t lg:border-gray-400 bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal">
                        <div class="mb-8">
                            <div class="text-gray-900 font-bold text-xl mb-2">Nama Hotel</div>
                            <p class="text-gray-700 text-base">Lokasi</p>
                            <div class="flex justify items-center">
                                <img src="{{ asset('storage') }}/img/icon/person-swimming-solid.svg" alt=""
                                    style="max-width: 3%">
                                &nbsp;
                                <p class="text-gray-700 text-base">
                                    Kolam Renang
                                </p>
                                <img src="{{ asset('storage') }}/img/icon/hot-tub-person-solid.svg" alt=""
                                    style="max-width: 3%" class="ml-5">
                                &nbsp;
                                <div class="text-sm">
                                    <p class="text-gray-700 text-base">
                                        Bathtub Air Panas
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="flex items-center">
                            <div class="grid grid-cols-1 gap-6 sm:grid-cols-3">
                                <div class="col-span-2">
                                    <p class="mb-3 font-light text-dark-500 dark:text-dark-400">Refundable</p>
                                    <p class="mb-3 font-light text-dark-500 dark:text-dark-400"><strong>Rating</strong>
                                        4.7 (12 Ulasan)</p>
                                </div>
                                <p class="mb-3 font-light text-dark-500 dark:text-dark-400" style="margin-left: 82px">
                                    <span class="bg-[#D50006] py-[2px] px-[5px] rounded-lg text-white font-bold">Sisa 5
                                        Kamar!</span> <br> <strong class="text-gray-900 font-bold text-xl">IDR
                                        100.000</strong> <br> total Rp 150.000
                                </p>
                            </div>
                        </div>
                    </div>
                </div> --}}
                @foreach ($data_detail as $item)
                @php
                $regency = App\Models\Regency::where('id', $item->regency_id)->first();
                $kamar = App\Models\Masterkamar::where('id', $item->kamar_id)->first();
                // dd($item);
                @endphp
                <a href="{{ route('hotel.show', $item->product->slug) }}">
                    <div class=" w-full lg:max-w-full lg:flex my-1">
                        <div class="h-48 lg:h-auto lg:w-48 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden"
                            style="background-image: url('https://images.unsplash.com/photo-1544644181-1484b3fdfc62?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fGJhbGl8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60')"
                            title="Mountain">
                        </div>

                        <div
                            class="border-r border-b border-l border-gray-400 lg:border-l-0 lg:border-t lg:border-gray-400 bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal">
                            <div class="mb-8">
                                <div class="text-gray-900 font-bold text-xl mb-2">
                                    {{ $item->product->product_name }}
                                </div>
                                <p class="text-gray-700 text-base">{{ $regency->name }}</p>
                                <div class="flex justify items-center">
                                    @foreach (json_decode($kamar['foto_kamar'])->result as $key => $foto_kamar)
                                    <img src="{{ asset($foto_kamar) }}" alt="" style="max-width: 3%">
                                    &nbsp;
                                    <p class="text-gray-700 text-base">
                                        {{ json_decode($kamar['fasilitas'])->result[$key] }}
                                    </p>
                                    &nbsp;&nbsp;
                                    @endforeach
                                    {{-- <img src="{{ asset('storage') }}/img/icon/hot-tub-person-solid.svg" alt=""
                                        style="max-width: 3%" class="ml-5">
                                    &nbsp;
                                    <div class="text-sm">
                                        <p class="text-gray-700 text-base">
                                            Bathtub Air Panas
                                        </p>
                                    </div> --}}
                                </div>
                                <div class="flex items-center">
                                    <div class="grid grid-cols-1 gap-6 sm:grid-cols-3">
                                        <div class="col-span-2">
                                            <p class="mb-3 font-light text-dark-500 dark:text-dark-400">Refundable</p>
                                            <p class="mb-3 font-light text-dark-500 dark:text-dark-400">
                                                <strong>Rating</strong>
                                                4.7 (12 Ulasan)
                                            </p>
                                        </div>
                                        <p class="mb-3 font-light text-dark-500 dark:text-dark-400"
                                            style="margin-left: 82px">
                                            <span
                                                class="bg-[#D50006] py-[3px] px-[3px] rounded-lg text-white font-bold">Sisa
                                                {{ $kamar->stok }}
                                                Kamar!</span> <br> <strong class="text-gray-900 font-bold text-lg">IDR
                                                {{ number_format($kamar->harga_kamar) }}</strong>{{-- <br> total Rp
                                            150.000 --}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>

    <script>
        var bannerSwiper = new Swiper(".carousel-banner", {
            loop: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
                enabled: false,
            },
            autoplay: {
                delay: 3000,
                pauseOnMouseEnter: true,
                disableOnInteraction: false,
            },
            breakpoints: {
                640: {
                    pagination: {
                        enabled: true
                    }
                }
            }
        });

        $("#searchhotel").select2();
    </script>

</body>

</html>