<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Destindonesia') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles

    <style>
    </style>

</head>

<body class="bg-white font-Inter">

<header>
    <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
</header>

<div class="max-w-7xl bg-[#F2F2F2] mx-auto rounded-md m-5">
    <div class="grid p-5 justify-items-center">
        <h1 class="font-semibold text-[26px]">Formulir Custom Hotel</h1>
    </div>

    <div class="p-10">

        <div>
            <p class="text-kamtuu-second text-[18px]">ID : CUS01TO0001</p>
        </div>

        <div class="grid max-w-6xl p-5 mx-auto my-5 text-center">
            <h1 class="font-semibold text-[24px]">Butuh kamar hotel dalam jumlah besar? Beritahukan kami.
                Tuliskan kebutuhan Anda di kotak berikut.</h1>
        </div>
        <form method="POST" action="{{ route('formulirHotel.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="grid lg:grid-cols-[33%_33%_33%]">
                <div>
                    <p class="text-[18px] font-semibold">Tanggal Check-in</p>

                    <div class="grid py-2 grid-cols-[80%_20%]">
                        <div class="p-2 bg-white border border-black rounded-xl">
                            <input name="checkIn" id="datepickerFormulirHotelCheckin" placeholder=" "/>
                        </div>
                        <div class="md:hidden lg:hidden">
                            <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                 class="w-[15px] h-[15px] mt-[1rem] -ml-[30px] mr-1" alt="polygon 3" title="Kamtuu">
                        </div>
                    </div>
                </div>
                <div>
                    <p class="text-[18px] font-semibold">Tanggal Check-out</p>

                    <div class="grid py-2 grid-cols-[80%_20%]">
                        <div class="p-2 bg-white border border-black rounded-xl">
                            <input name="checkOut" id="datepickerFormulirHotelCheckout" placeholder=" "/>
                        </div>
                        <div class="md:hidden lg:hidden">
                            <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                 class="w-[15px] h-[15px] mt-[1rem] -ml-[30px] mr-1" alt="polygon 3" title="Kamtuu">
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid lg:grid-cols-[33%_33%_33%]">
                <div>
                    <p class="text-[18px] font-semibold">Lokasi Hotel</p>
                    <div class="grid py-2">
                        <div class="inline-flex">
                            <input class="bg-white border border-black rounded-xl" type="text" name="lokasi" id="">
                        </div>
                    </div>
                </div>

                <div>
                    <p class="text-[18px] font-semibold">Hotel Bintang</p>

                    <div class="grid py-2 grid-cols-[80%_20%]">
                        <select name="jenisHotel" id=""
                                class="p-2 bg-white border border-black focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7 rounded-xl">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                        <div class="md:hidden lg:hidden">
                            {{--                            <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"--}}
                            {{--                                 class="w-[15px] h-[15px] mt-[1rem] -ml-[30px] mr-1" alt="polygon 3" title="Kamtuu">--}}
                        </div>
                    </div>
                </div>
            </div>

            <div class="my-5">

                <div class="font-semibold text-[18px]">
                    <p>Jumlah Traveller</p>
                </div>


                <table class="table table-bordered" id="dynamicAddRemove">
                    <tr>
                        <th>Jumlah Orang :</th>
                        <th>Jumlah Orang Perkamar :</th>
                    </tr>
                    <tr>
                        <td><input class="rounded-full my-2 mx-2" type="text"
                                   name="addMoreInputFields1[0][jumlahOrang]"
                                   placeholder="Enter Title" class="form-control"/>
                        </td>
                        <td><input class="rounded-full my-2" type="text"
                                   name="addMoreInputFields2[0][jumlahOrangPerkamar]"
                                   placeholder="Enter Isi" class="form-control"/>
                        </td>
                        <td class="px-4">
                            <select name="addMoreInputFields3[][jenisKamar]"
                                    class="w-[4rem] h-[17.5px] md:w-full md:h-[3rem] lg:w-full lg:h-[2rem] border border-gray-500 ring-gray-500 mt-[5px] lg:mt-0 text-[8px]"
                            >
                                <option value="smoking">Smoking</option>
                                <option value="no smoking">No Smoking</option>
                            </select>
                        </td>
                        <td>
                            <button type="button" name="add" id="dynamic-ar"
                                    class="p-2 bg-kamtuu-second text-white rounded-lg">Tambah
                            </button>
                        </td>
                    </tr>

                </table>


                <div class="grid grid-cols-3 px-4 text-[8px] lg:text-base md:text-base">
                    <div class="flex items-center">
                        <input id="option1" type="checkbox" name="jenisMakan[]"
                               class="w-4 h-4  border-gray-400 text-kamtuu-second form-checkbox" value="Makan Pagi"/>
                        <label for="option1" class="ml-3 font-medium text-gray-700">Makan Pagi</label>
                    </div>
                    <div class="flex items-center">
                        <input id="option1" type="checkbox" name="jenisMakan[]" value="Makan Siang"
                               class="w-4 h-4 border-gray-400 text-kamtuu-second form-checkbox"/>
                        <label for="option1"  class="ml-3 font-medium text-gray-700">Makan Siang</label>
                    </div>
                    <div class="flex items-center">
                        <input id="option1" name="jenisMakan[]" type="checkbox" value="Makan Malam"
                               class="w-4 h-4 border-gray-400 text-kamtuu-second form-checkbox"/>
                        <label for="option1" class="ml-3 font-medium text-gray-700">Makan Malam</label>
                    </div>
                </div>

                {{--                <div class="row" x-data="handler()">--}}
                {{--                    <div class="btn btn-primary">--}}
                {{--                        <button type="button" class="p-3 my-3 text-white rounded-full btn btn-info bg-kamtuu-second"--}}
                {{--                                @click="addNewField()">Tambah +--}}
                {{--                        </button>--}}
                {{--                    </div>--}}
                {{--                    <template x-for="(field, index) in fields" :key="index">--}}
                {{--                        <div class="max-w-4xl p-2 my-5 bg-gray-200 rounded-lg md:max-w-2xl lg:max-w-6xl">--}}
                {{--                            <table class="table-fixed w-[25rem] lg:w-[95rem]">--}}
                {{--                                <thead>--}}
                {{--                                <th class="text-[8px] md:text-[18px] lg:text-[18px]">Jumlah Orang</th>--}}
                {{--                                <th class="text-[8px] md:text-[18px] lg:text-[18px]">Jumlah Orang per Kamar</th>--}}
                {{--                                <th class="text-[8px] md:text-[18px] lg:text-[18px]">Jenis Kamar</th>--}}
                {{--                                </thead>--}}
                {{--                                <tbody>--}}
                {{--                                <tr>--}}
                {{--                                    <td class="px-4"><input--}}
                {{--                                            class="w-[4rem] h-[1rem] md:w-full md:h-[3rem] lg:w-full lg:h-[2rem]"--}}
                {{--                                            x-model="field.txt1" type="text" name="traveller[][jumlahOrang]"></td>--}}
                {{--                                    <td class="px-4"><input--}}
                {{--                                            class="w-[4rem] h-[1rem] md:w-full md:h-[3rem] lg:w-full lg:h-[2rem]"--}}
                {{--                                            x-model="field.txt2" type="text" name="traveller[][jumlahOrangPerkamar]"></td>--}}
                {{--                                    <td class="px-4">--}}
                {{--                                        <select name="traveller[][jenisKamar]"--}}
                {{--                                                class="w-[4rem] h-[17.5px] md:w-full md:h-[3rem] lg:w-full lg:h-[2rem] border border-gray-500 ring-gray-500 mt-[5px] lg:mt-0 text-[8px]"--}}
                {{--                                                x-model="field.txt3">--}}
                {{--                                            <option value="smoking">Smoking</option>--}}
                {{--                                            <option value="no smoking">No Smoking</option>--}}
                {{--                                        </select>--}}
                {{--                                    </td>--}}
                {{--                                    <td>--}}
                {{--                                        <button type="button" class="btn btn-danger btn-small"--}}
                {{--                                                @click="removeField(index)">--}}
                {{--                                            <img src="{{ asset('storage/icons/delete-dynamic-data.png') }}"--}}
                {{--                                                 class="h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">--}}
                {{--                                        </button>--}}
                {{--                                    </td>--}}
                {{--                                </tr>--}}
                {{--                                </tbody>--}}
                {{--                            </table>--}}
                {{--                            <div class="grid grid-cols-3 px-4 text-[8px] lg:text-base md:text-base">--}}
                {{--                                <div class="flex items-center">--}}
                {{--                                    <input id="option1" type="checkbox" name="traveller[][jenisMakan]"--}}
                {{--                                           class="w-4 h-4  border-gray-400 text-kamtuu-second form-checkbox" value="Makan Pagi"/>--}}
                {{--                                    <label for="option1" class="ml-3 font-medium text-gray-700">Makan Pagi</label>--}}
                {{--                                </div>--}}
                {{--                                <div class="flex items-center">--}}
                {{--                                    <input id="option1" type="checkbox" name="traveller[][jenisMakan]" value="Makan Siang"--}}
                {{--                                           class="w-4 h-4 border-gray-400 text-kamtuu-second form-checkbox"/>--}}
                {{--                                    <label for="option1"  class="ml-3 font-medium text-gray-700">Makan Siang</label>--}}
                {{--                                </div>--}}
                {{--                                <div class="flex items-center">--}}
                {{--                                    <input id="option1" name="traveller[][jenisMakan]" type="checkbox" value="Makan Malam"--}}
                {{--                                           class="w-4 h-4 border-gray-400 text-kamtuu-second form-checkbox"/>--}}
                {{--                                    <label for="option1" class="ml-3 font-medium text-gray-700">Makan Malam</label>--}}
                {{--                                </div>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </template>--}}
                {{--                </div>--}}
                <div class="py-5">
                    <div class="py-5 font-semibold">
                        <p>Tuliskan fasilitas hotel yang Anda inginkan</p>
                        <textarea class="w-[15rem] lg:w-[35rem]" name="fasilitas" id="" cols="50" rows="10"
                                  placeholder="Tuliskan hal-hal lain yang perlu kami ketahui."></textarea>
                    </div>

                    <div class="font-semibold">
                        <p>Tuliskan hal-hal lain yang perlu kami ketahui sehubungan dengan hotel yang Anda inginkan</p>
                        <textarea class="w-[15rem] lg:w-[35rem]" name="kebutuhan" id="" cols="50" rows="10"
                                  placeholder="Tuliskan hal-hal lain yang perlu kami ketahui."></textarea>
                    </div>
                </div>

                <x-destindonesia.button-primary>
                    @slot('button')
                        Simpan
                    @endslot
                </x-destindonesia.button-primary>
            </div>
        </form>

    </div>

</div>

<footer>
    <x-destindonesia.footer></x-destindonesia.footer>
</footer>

<script type="text/javascript">
    var i = 0;
    $("#dynamic-ar").click(function () {
        ++i;
        $("#dynamicAddRemove").append('<tr>' +
            '<td><input class="rounded-full my-2" type="text" name="addMoreInputFields1' +
            '[' + i + ']' +
            '[jumlahOrang]" placeholder="Jumlah Orang" class="form-control" /></td> <br>' +
            '<td><input class="rounded-full my-2 mx-2" type="text" name="addMoreInputFields2' +
            '[' + i + ']' +
            '[jumlahOrangPerkamar]" placeholder="Jumlah Orang Perkamar" class="form-control" /></td>' +
            '<td><select name="addMoreInputFields3' +
            '[' + i + ']' +
            '[jenisKamar]" class="w-[4rem] h-[17.5px] ' +
            'md:w-full md:h-[3rem] lg:w-full lg:h-[2rem] border border-gray-500 ring-gray-500 mt-[5px] lg:mt-0 ' +
            'text-[8px]" > <option value="smoking">Smoking</option> <option value="no smoking">No Smoking</option> </select></td>' +
            '<td><button type="button" class="rounded-lg bg-kamtuu-third text-white mx-2 p-2">Delete</button></td>' +
            '</tr>'
        );
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });
</script>

<script>
    $('#datepickerFormulirHotelCheckin').datepicker();
</script>

<script>
    $('#datepickerFormulirHotelCheckout').datepicker();
</script>

<script>
    var i = 0;

    function handler() {
        return {
            fields: [],
            addNewField() {
                this.fields.push({
                    txt1: '',
                    txt2: '',
                    txt3: '',
                });
            },
            removeField(index) {
                this.fields.splice(index, 1);
            }
        }
    }
</script>

<script>
    function valueChanged() {
        if ($('.coupon_question').is(":checked"))
            $(".answer").show();
        else
            $(".answer").hide();
    };
</script>

</body>

</html>
