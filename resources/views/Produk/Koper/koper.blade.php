@extends('Destindonesia.layouts.app')

<header>
    <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
</header>

<div class="m-3">
    <h4 class="text-lg font-semibold">
        Koper
    </h4>

    <div class="grid lg:grid-cols-[80%_20%]">
        {{-- Column Left --}}
        <div>
            <div class="grid grid-cols-2">
                <div class="grid justify-items-start">
                    <div class="inline-flex p-2">
                        <input class="mt-[6px] checked:bg-kamtuu-second" type="checkbox" name="" id="">
                        <p class="px-2">Pilih Semua</p>
                    </div>
                </div>
                <div class="grid justify-end">
                    <button class="text-[#D50006]">Hapus Semua</button>
                </div>
            </div>

        {{-- Produk --}}
            <div class="py-5 m-3">
                <x-produk.produk-koper>
                    @slot('kategori')
                    Tur
                    @endslot

                    @slot('judul_produk')
                    Tour Bali
                    @endslot

                    @slot('nama_toko')
                    Kamtuu
                    @endslot

                    @slot('tanggal_produk')
                    19 Oktober 2022
                    @endslot

                    @slot('rincian_1')
                    Dewasa 1
                    @endslot

                    @slot('rincian_2')
                    Anak-Anak 1
                    @endslot

                    @slot('id_booking')
                    ID:190318138992310
                    @endslot

                    @slot('harga')
                    IDR 800.000
                    @endslot
                </x-produk.produk-koper>
            </div>

            <div class="py-5 m-3">
                <x-produk.produk-koper>
                    @slot('kategori')
                    Tur
                    @endslot

                    @slot('judul_produk')
                    Tour Bali
                    @endslot

                    @slot('nama_toko')
                    Kamtuu
                    @endslot

                    @slot('tanggal_produk')
                    19 Oktober 2022
                    @endslot

                    @slot('rincian_1')
                    Dewasa 1
                    @endslot

                    @slot('rincian_2')
                    Anak-Anak 1
                    @endslot

                    @slot('id_booking')
                    ID:190318138992310
                    @endslot

                    @slot('harga')
                    IDR 800.000
                    @endslot
                </x-produk.produk-koper>
            </div>
        </div>

        {{-- Column Right --}}
        <div class="mt-9">
            <div class="bg-[#F2F2F2] m-5 rounded-lg ">
                <div class="p-2">
                    <p>Ringkasan Pesanan</p>
                    <div class="grid grid-cols-2 text-[#828282]">
                        <div class="grid justify-items-start text-[14px]">Total (8 Item)</div>
                        <div class="grid justify-items-end text-[14px]">IDR 800,000</div>
                    </div>
                    <div class="relative py-4">
                        <div class="absolute inset-0 flex items-center">
                          <div class="w-full border-b border-black"></div>
                        </div>
                    </div>
                    <div class="grid grid-cols-2">
                        <div class="grid justify-items-start text-[14px]">Total Bayar</div>
                        <div class="grid justify-items-end text-[14px] text-kamtuu-second">IDR 800,000</div>
                    </div>
                    <div class="grid justify-items-center">
                        <x-destindonesia.button-primary>@slot('button') Pesan Sekarang @endslot</x-destindonesia.button-primary>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>




<footer>
    <x-destindonesia.footer></x-destindonesia.footer>
</footer>
