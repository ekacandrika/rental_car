<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tur Detail') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles

    <style>
        .tab button.active {
            background-color: #FFF6FA;
            color: #9E3D64;
            border-bottom: 1px solid #9E3D64;
        }
    </style>
</head>

<body class="bg-white font-Inter">
    <header class="border border-b-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Gallery Lightbox --}}
    <div class="container mt-5 mb-2.5 lg:my-5 mx-auto">
        <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff" class="swiper mySwiper2 ">
            <div class="swiper-wrapper">
                @foreach (json_decode($rental_detail['gallery']) as $gallery)
                <div class="swiper-slide">
                    <img src="{{ asset($gallery->gallery) }}"
                        class="object-cover object-center w-full h-[316px] sm:h-[319px] md:h-[383px] lg:h-[511px] xl:h-[639px] 2xl:h-[767px]" />
                </div>
                @endforeach
            </div>
            <div class="swiper-button-next hidden"></div>
            <div class="swiper-button-prev hidden"></div>
        </div>

        @if (count(json_decode($rental_detail['gallery'])) > 1)
        <div class="relative">
            <div thumbsSlider="" class="swiper mySwiper pt-1">
                <div class="swiper-wrapper space-x-1 sm:space-x-2 justify-center">
                    @foreach (json_decode($rental_detail['gallery']) as $gallery)
                    <div class="swiper-slide cursor-pointer h-max-[97px]">
                        <img src="{{ asset($gallery->gallery) }}"
                            class="object-cover object-center h-[75px] sm:h-[97px] md:h-[120px] lg:h-[167px] xl:h-[214px] 2xl:h-[261px] w-full" />
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="flex md:absolute md:bottom-0 md:right-0">
                <div>
                    <button
                        class="relative w-full md:w-fit md:-translate-x-5 md:-translate-y-5 px-1 md:px-3 py-1 lg:px-5 lg:py-2 text-sm sm:text-base font-semibold border-2 border-gray-300 text-black duration-300 bg-gray-300 rounded-sm hover:bg-gray-100">
                        <span class="md:pr-[11px]">
                           {{$like_count}} Favorit 
                        </span>
                        <span>
                            <svg class="absolute md:top-[9px] md:right-0 w-6 h-6 text-white hover:fill-red-500 md:pr-[7px]" id="16" onclick="submitLike2(16)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z"></path>
                            </svg>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        @endif
    </div>

    <div>
        {{-- Tabs --}}
        <div class="container mx-auto my-2.5 lg:my-5 sticky bg-white top-0 z-20" x-data="{
            active: 0,
            tabs: ['Ringkasan', 'Lokasi', 'Kebijakan', 'Ulasan']
        }">
            <div class="flex sm:block px-2.5">
                <ul
                    class="flex justify-start overflow-x-auto text-sm font-medium text-center sm:text-left text-gray-500 dark:text-gray-400 border-b border-[#BDBDBD]">
                    <template x-for="(tab, index) in tabs" :key="index">
                        <li class="pb-2 mt-2" :class="{ 'border-b-2 border-[#9E3D64]': active == index }"
                            x-on:click="active = index">
                            <a :href="`#` + tab.toLowerCase()">
                                <button
                                    class="inline-block py-2 px-6 lg:px-9 text-sm lg:text-xl rounded-lg duration-200"
                                    :class="{
                                        'text-[#9E3D64] bg-white font-bold': active ==
                                            index,
                                        'text-black font-normal hover:text-[#9E3D64]': active != index
                                    }" x-text="tab"></button>
                            </a>
                        </li>
                    </template>
                </ul>
            </div>
        </div>

        {{-- Content --}}
        <div class="container mx-auto">
            <div class="grid grid-cols-6 lg:gap-5">
                {{-- Left --}}
                <div class="col-span-6 lg:col-span-4 bg-white p-2.5 lg:p-5">
                    {{-- Ringkasan --}}
                    <div id="ringkasan">
                        <p class="text-3xl pb-2 font-semibold text-[#333333] whitespace-normal break-words">
                            {{ $rental['product_name'] }}
                        </p>
                        <div class="flex pb-2 items-center">
                            <img class="mr-2" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <p class="text-xl font-semibold text-[#333333]">{{ $ratings }} <span class="font-normal">({{
                                    $var2 }} ulasan)</span></p>
                        </div>
                        <div
                            class="grid grid-cols-2 lg:grid-cols-2 w-80 lg:w-96 py-2.5 lg:pt-5 space-y-2 sm:space-y-0 lg:gap-10">
                            <div class="space-y-2 mt-2 lg:mt-0">
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/car-solid 1.svg') }}" alt="user">
                                    {{ $mobil_detail->merekmobil->merek_mobil }}
                                </p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/car-solid 1.svg') }}" alt="user-group">
                                    {{ $mobil_detail->jenismobil->jenis_kendaraan }}
                                </p>
                            </div>
                            <div class="space-y-2">
                                <p class="flex font-sm font-medium"><img class="mr-2" width="20px"
                                        src="{{ asset('storage/icons/seats.svg') }}" alt="user">
                                    {{ $mobil_detail['kapasitas_kursi'] }} Seats
                                </p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="20px"
                                        src="{{ asset('storage/icons/koper.svg') }}" alt="user-group">
                                    {{ $mobil_detail['kapasitas_koper'] }} Koper
                                </p>
                            </div>
                        </div>
                        <div class="text-justify my-2.5 lg:my-10">
                            <p class="text-xl lg:text-3xl mb-2.5 font-semibold">Deskripsi</p>
                            {!! $rental_detail['deskripsi'] !!}
                        </div>
                        {{-- Paket Termasuk --}}
                        <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2] hidden"
                            x-data="{ open: false }">
                            <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-3': !open }">
                                <button
                                    class="text-lg sm:text-xl md:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                    x-on:click="open = !open">Paket Termasuk</button>
                                <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                                    x-on:click="open = !open"
                                    src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}" alt="chevron-down"
                                    width="26px" height="15px">
                            </div>
                            <div class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                                x-show="open" x-transition>
                                {!! $rental_detail['paket_termasuk'] !!}
                            </div>
                        </div>

                        {{-- Paket Tidak Termasuk --}}
                        <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2] hidden"
                            x-data="{ open: false }">
                            <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-3': !open }">
                                <button
                                    class="text-lg sm:text-xl md:text-2xl  text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                    x-on:click="open = !open">Paket Tidak Termasuk</button>
                                <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                                    x-on:click="open = !open"
                                    src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}" alt="chevron-down"
                                    width="26px" height="15px">
                            </div>
                            <div class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                                x-show="open" x-transition>
                                {!! $rental_detail['paket_tidak_termasuk'] !!}
                            </div>
                        </div>
                    </div>

                    <div id="kebijakan">
                        <div class="mb-4 border-b border-gray-200">
                            <ul class="flex flex-wrap -mb-px text-xs lg:text-base font-medium text-center tab gap-1"
                                id="myTab" data-tabs-toggle="#myTabContent" role="tablist">
                                <li class="mr-2" role="presentation">
                                    <button
                                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent text-gray-500 border-gray-100 tablinks active h-[66px] lg:h-auto"
                                        id="komplemen-tab" data-tabs-target="#komplemen" type="button" role="tab"
                                        aria-controls="komplemen" aria-selected="true"
                                        onclick="openCity(event, 'komplemen')">Komplemen</button>
                                </li>
                                <li class="mr-2 w-20 lg:w-auto" role="presentation">
                                    <button
                                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent text-gray-500 border-gray-100 tablinks"
                                        id="pilihan-tab" data-tabs-target="#pilihan" type="button" role="tab"
                                        aria-controls="pilihan" aria-selected="false"
                                        onclick="openCity(event, 'pilihan')">Pilihan Tambahan</button>
                                </li>
                                <li class="mr-2 w-20 lg:w-auto hidden" role="presentation">
                                    <button
                                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent text-gray-500 border-gray-100 tablinks"
                                        id="kebijakan-tab" data-tabs-target="#kebijakan" type="button" role="tab"
                                        aria-controls="kebijakan" aria-selected="false"
                                        onclick="openCity(event, 'kebijakans')">Kebijakan Pembatalan</button>
                                </li>
                                <li class="w-[64px] lg:w-auto" role="presentation">
                                    <button
                                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent text-gray-500 border-gray-100 tablinks h-[66px] lg:h-auto"
                                        id="faq-tab" data-tabs-target="#faq" type="button" role="tab"
                                        aria-controls="faq" aria-selected="false"
                                        onclick="openCity(event, 'faq')">FAQ</button>
                                </li>
                            </ul>
                        </div>
                        <div id="myTabContent">
                            <div class="rounded-lg dark:bg-gray-800 tabcontent" id="komplemen" role="tabpanel"
                                aria-labelledby="komplemen-tab">
                                <div class="space-y-2 px-3 lg:py-2 font-medium text-gray-500 text-sm lg:text-lg md:text-base"
                                    x-show="open" x-transition>
                                    @if (isset($rental_detail['komplemen']))
                                    {!! $rental_detail['komplemen'] !!}
                                    @else
                                    <p class="text-base sm:text-lg md:text-xl">Tidak ada Komplemen.</p>
                                    @endif
                                </div>
                            </div>
                            <div class="hidden rounded-lg dark:bg-gray-800 tabcontent" id="pilihan" role="tabpanel"
                                aria-labelledby="pilihan-tab">
                                <div class="space-y-2 px-3 lg:py-2 font-medium text-gray-500 text-sm lg:text-lg md:text-base"
                                    x-show="open" x-transition>
                                    @if (isset($rental_detail['pilihan_tambahan']))
                                    {!! $rental_detail['pilihan_tambahan'] !!}
                                    @else
                                    <p class="text-base sm:text-lg md:text-xl">Tidak ada Pilihan Tambahan.</p>
                                    @endif
                                </div>
                            </div>
                            <div class="hidden rounded-lg dark:bg-gray-800 tabcontent" id="kebijakans" role="tabpanel"
                                aria-labelledby="kebijakan-tab">
                                <div class="space-y-2 px-3 lg:py-2 font-medium text-gray-500 text-sm lg:text-lg md:text-base"
                                    x-show="open" x-transition>
                                    @if (isset($rental_detail['kebijakan_pembatalan_sebelumnya']))
                                    @foreach (json_decode($rental_detail['kebijakan_pembatalan_sebelumnya']) as $k =>
                                    $kp)
                                    <p class="py-3">Pembatalan <strong>{{ $kp }} hari</strong>
                                        sampai
                                        <strong>{{ json_decode($rental_detail['kebijakan_pembatalan_sesudah'])[$k] }}
                                            hari</strong>
                                        sebelumnya potongan
                                        <strong>{{ json_decode($rental_detail['kebijakan_pembatalan_potongan'])[$k]
                                            }}%.</strong>
                                    </p>
                                    @endforeach
                                    @else
                                    <p class="text-base sm:text-lg md:text-xl">Tidak ada kebijakan pembatalan.</p>
                                    @endif
                                </div>
                            </div>
                            <div class="hidden rounded-lg dark:bg-gray-800 tabcontent" id="faq" role="tabpanel"
                                aria-labelledby="faq-tab">
                                <div class="space-y-2 px-3 lg:py-2 font-medium text-gray-500 text-sm lg:text-lg md:text-base"
                                    x-show="open" x-transition>
                                    @if (isset($rental_detail['faq']))
                                    {!! $rental_detail['faq'] !!}
                                    @else
                                    <p class="text-base sm:text-lg md:text-xl">Tidak ada FAQ.</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Right --}}
                <div class="col-span-6 lg:col-span-2 space-y-5 p-2.5 lg:p-5">
                    {{-- Detail --}}
                    <div class="lg:block rounded-md shadow-lg p-5 mb-5 bg-white border border-gray-200 space-y-3">
                        {{-- <p class="text-base font-semibold mb-2.5">Jumat, 12 Agustus 2022 • 09.00</p> --}}
                        <div class="">
                            <p class="text-sm font-normal">Lokasi</p>
                            <p class="text-base font-semibold">{{ isset($regency->name) ? $regency->name .',' : ''}} {{
                                isset($provinsi->name) ? $provinsi->name :null}}
                                {{
                                isset($tag_location_1) ? $tag_location_1 : '' }} {{ isset($tag_location_2) ? ' - '.
                                $tag_location_2 : '' }} {{
                                isset($tag_location_3) ? ' - '. $tag_location_3 : '' }}
                                {{ isset($tag_location_4) ? ' - '. $tag_location_4 : '' }}</p>
                        </div>
                        <div class="">
                            <p class="text-sm font-normal">Rental Mulai</p>
                            <p class="text-base font-semibold">{{ isset($regency->name) ? $regency->name .',' : ''}} {{
                                isset($provinsi->name) ? $provinsi->name :null}}
                                {{
                                isset($tag_location_1) ? $tag_location_1 : '' }} {{ isset($tag_location_2) ? ' - '.
                                $tag_location_2 : '' }} {{
                                isset($tag_location_3) ? ' - '. $tag_location_3 : '' }}
                                {{ isset($tag_location_4) ? ' - '. $tag_location_4 : '' }}</p>
                        </div>
                        <div class="">
                            <p class="text-sm font-normal">Rental Selesai</p>
                            <p class="text-base font-semibold">{{ isset($regency->name) ? $regency->name .',' : ''}} {{
                                isset($provinsi->name) ? $provinsi->name :null}}
                                {{
                                isset($tag_location_1) ? $tag_location_1 : '' }} {{ isset($tag_location_2) ? ' - '.
                                $tag_location_2 : '' }} {{
                                isset($tag_location_3) ? ' - '. $tag_location_3 : '' }}
                                {{ isset($tag_location_4) ? ' - '. $tag_location_4 : '' }}</p>
                        </div>
                    </div>
                    {{-- Price --}}
                    @if (auth()->user() == null)
                    <div class="p-5 grid justify-items-center">
                        <a href="{{ route('LoginTraveller') }}"
                            class="px-8 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">
                            Login</a>
                    </div>
                    @else
                    <form class="space-y-2" action="{{ route('rental.prosespesan') }}" method="POST">
                        @csrf
                        <div class="relative block order-last sm:order-none my-3 sm:my-0">
                            <label for="activity-date-picker" class="form-label inline-block mb-2 text-gray-700">Tanggal
                                Mulai</label>
                            <div
                                class="placeholder:text-[#BDBDBD] block bg-white w-1/4 md:w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                <input class="w-full" placeholder="Pilih tanggal" type="text" id="check_in_date"
                                    name="check_in_date" required />
                            </div>
                        </div>
                        <div class="relative block order-last sm:order-none my-3 sm:my-0">
                            <label for="activity-date-picker" class="form-label inline-block mb-2 text-gray-700">Tanggal
                                Selesai</label>
                            <div
                                class="placeholder:text-[#BDBDBD] block bg-white w-1/4 md:w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                <input class="w-full" placeholder="Pilih tanggal" type="text" id="check_out_date"
                                    name="check_out_date" required />
                            </div>
                        </div>
                        <div class="lg:block rounded-md shadow-lg p-5 mb-5 bg-white border border-gray-200">
                            <p class="lg:text-2xl xl:text-3xl text-center font-bold text-[#23AEC1]">IDR
                                {{ number_format($mobil_detail['harga_akomodasi']) }} </p>
                            <div class="flex justify-center">
                                <input type="hidden" name="total" value="{{ $mobil_detail['harga_akomodasi'] }}">
                                <input type="hidden" name="product_detail_id" value="{{ $rental_detail['id'] }}">
                                <input type="hidden" value="{{ $rental_detail['pilihan_id'] }}" name="pilihan_id">
                                <input type="hidden" value="{{ $rental['product_name'] }}" name="product_name">
                                <input type="hidden" value="{{ $rental['user_id'] }}" name="toko_id">
                                <input type="hidden" value="{{ $rental['type'] }}" name="type">
                                <x-destindonesia.button-primary text="Pesan Sekarang">
                                    @slot('button')
                                    Pesan Sekarang
                                    @endslot
                                </x-destindonesia.button-primary>
                            </div>
                        </div>
                    </form>
                    @endif

                    {{-- Store --}}
                    <div class="lg:rounded-md lg:shadow-lg p-5 my-5 bg-white border-y-2 lg:border border-gray-200">
                        <a href={{route('toko.show',$rental->user->id)}}>
                            <p class="lg:text-2xl xl:text-3xl font-bold text-center text-[#333333]">Kunjungi Toko</p>
                        </a>
                        <div class="flex justify-center py-5">
                            <a href={{route('toko.show',$rental->user->id)}}>
                                <img src="{{ isset($info_toko->logo_toko) ? asset($info_toko->logo_toko) : asset('storage/img/tur-detail-1.png') }}"
                                    class="bg-clip-content lg:w-[96px] lg:h-[96px] xl:w-[100px] xl:h-[100px] shadow-xl bg-white rounded-full"
                                    alt="store-profile">
                            </a>    
                        </div>
                        <p class="pt-3 text-xl font-semibold text-center">{{ isset($info_toko->nama_toko) ? $info_toko->nama_toko :'Nama Toko'}}</p>
                        <p class="text-lg font-normal text-center pb-2">Bergabung sejak {{
                            isset($rental->user->created_at)
                            ? date('Y',strtotime($rental->user->created_at)) : '2021'}}</p>
                        <div class="flex justify-center">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                        </div>
                        <div class="flex justify-center">
                            <x-destindonesia.button-primary text="Kirim Pesan">
                                @slot('button')
                                Kirim Pesan
                                @endslot
                            </x-destindonesia.button-primary>
                        </div>
                    </div>

                    {{-- QR Code --}}
                    <div>
                        <p class="text-2xl font-semibold text-center">Kode QR Produk</p>
                        <div class="flex justify-center mt-5">
                            {{-- data:image/png;base64,".DNS2D::getBarcodePNG($rental_detail->base_url ??
                            url('/').'/tur/'.$rental->slug,'QRCODE')}}" --}}
                            <img src="{{" data:image/png;base64,".DNS2D::getBarcodePNG($rental_detail->base_url ??
                            url('/').'/rental/'.$rental->slug,'QRCODE')}}" alt="QR-code">
                        </div>
                        <a class="flex justify-center"
                            href="{{route('download.qrCode',['slug'=>$rental->slug,'base64'=>DNS2D::getBarcodePNG($rental_detail->base_url ?? url('/').'/rental/'.$rental->slug,'QRCODE')])}}"
                            download="QR-code-produk">
                            <x-destindonesia.button-primary text="Unduh Kode QR">
                                @slot('button')
                                Unduh Kode QR
                                @endslot
                            </x-destindonesia.button-primary>
                        </a>
                    </div>
                </div>
            </div>
            <div class="order-1 lg:order-none col-span-6 hidden">
                <hr class="border border-[#BDBDBD] my-5">

                {{-- Lokasi --}}
                <div id="lokasi" class="my-5">
                    <p class="text-3xl py-3 font-semibold text-[#333333]">Lokasi Anda</p>
                    <p class="text-sm font-normal pb-3">{{ isset($regency->name) ? $regency->name .',' : ''}} {{
                        isset($provinsi->name) ? $provinsi->name :null}}
                        {{
                        isset($tag_location_1) ? $tag_location_1 : '' }} {{ isset($tag_location_2) ? ' - '.
                        $tag_location_2 : '' }} {{
                        isset($tag_location_3) ? ' - '. $tag_location_3 : '' }}
                        {{ isset($tag_location_4) ? ' - '. $tag_location_4 : '' }}</p>
                    <div>
                        {!! $rental_detail['link_maps'] !!}
                    </div>
                </div>

                {{-- Perlu Diketahui --}}
                <p class="text-3xl py-3 font-semibold text-[#333333]">Perlu Diketahui</p>
                {{-- Catatan --}}
                <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                    x-data="{ open: false }">
                    <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-3': !open }">
                        <button
                            class="text-lg sm:text-xl md:text-2xl  text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                            x-on:click="open = !open">Catatan</button>
                        <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                            x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            alt="chevron-down" width="26px" height="15px">
                    </div>
                    <div class="py-5 px-3 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl" x-show="open"
                        x-transition>
                        @if (isset($rental_detail['catatan']))
                        {!! $rental_detail['catatan'] !!}
                        @else
                        <p class="text-base sm:text-lg md:text-xl">Tidak ada Catatan.</p>
                        @endif
                    </div>
                </div>

                {{-- Kebijakan Pembatalan --}}
                <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                    x-data="{ open: false }">
                    <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-3': !open }">
                        <button
                            class="text-lg sm:text-xl md:text-2xl  text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                            x-on:click="open = !open">Kebijakan Pembatalan</button>
                        <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                            x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            alt="chevron-down" width="26px" height="15px">
                    </div>
                    <div class="py-5 px-3 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl" x-show="open"
                        x-transition>

                        @if (isset($rental_detail['kebijakan_pembatalan_sebelumnya']))
                        @foreach (json_decode($rental_detail['kebijakan_pembatalan_sebelumnya']) as $key => $kps)
                        <p class="py-3">Pembatalan <strong>{{ $kps }} hari</strong> sampai
                            <strong>{{ json_decode($rental_detail['kebijakan_pembatalan_sesudah'])[$key] }}
                                hari</strong>
                            sebelumnya potongan
                            <strong>{{ json_decode($rental_detail['kebijakan_pembatalan_potongan'])[$key] }}%.</strong>
                        </p>
                        @endforeach
                        {{-- {!! $activity_detail['kebijakan_pembatalan_sebelumnya'] !!} --}}
                        @else
                        <p class="text-base sm:text-lg md:text-xl">Tidak ada kebijakan pembatalan.</p>
                        @endif
                    </div>
                </div>

                {{-- Sering Ditanyakan (FAQ) --}}
                <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                    x-data="{ open: false }">
                    <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-3': !open }">
                        <button
                            class="text-lg sm:text-xl md:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                            x-on:click="open = !open">Sering Ditanyakan (FAQ)</button>
                        <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                            x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            alt="chevron-down" width="26px" height="15px">
                    </div>
                    <div class="py-5 px-3 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl" x-show="open"
                        x-transition>
                        @if (isset($rental_detail['faq']))
                        {!! $rental_detail['faq'] !!}
                        @else
                        <p class="text-base sm:text-lg md:text-xl">Tidak ada FAQ.</p>
                        @endif
                    </div>
                </div>

                {{-- Ulasan --}}
                <div id="ulasan" class="my-5">
                    <x-destindonesia.ulasan></x-destindonesia.ulasan>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>

    <script>
        $(document).ready(function() {
            var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            $('#check_in_date').datepicker({
                minDate: today
            });
        })
        $(document).ready(function() {
            var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            $('#check_out_date').datepicker({
                minDate: today
            });
        })

        $(document).ready(function(){
            
            function submitValidate(e, msg){
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'error',
                        title: msg
                    })
                e.preventDefault()
            }

            $('#form-tur-private').on("submit",function(e){
                let role="{{auth()->user() != null ? auth()->user()->role:null}}";
                if(role){
                    if(role!='traveller'){
                        submitValidate(e, 'Silahkan login terlebih dahulu')
                    }

                    submitValidate(e, 'Silahkan login terlebih dahulu')
                }
                

                if($("#comments").val()){
                    submitValidate(e, 'Isi komentar tidak kosong')
                }
                //e.preventDefault();
            })
        })

        function price() {
            return {
                dewasa_count: 0,
                anak_count: 0,
                balita_count: 0,
                sumTotal() {
                    this.$watch('dewasa_count; anak_count; balita_count', () => {
                        Alpine.store("detail").total_price = this.dewasa_count * Alpine.store("detail")
                            .dewasa_price + this.anak_count * Alpine.store("detail").anak_price + this
                            .balita_count * Alpine.store("detail").balita_price
                    })
                },
            }
        }
    </script>

    <script>
        var swiper = new Swiper(".mySwiper", {
            spaceBetween: 2,
            slidesPerView: 5,
            freeMode: true,
            watchSlidesProgress: true,
        });
        var swiper2 = new Swiper(".mySwiper2", {
            spaceBetween: 2,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            thumbs: {
                swiper: swiper,
            },
        });
    </script>
    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
</body>

</html>