<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tur Detail') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    <style>
        input[type='number']::-webkit-outer-spin-button,
        input[type='number']::-webkit-inner-spin-button,
        input[type='number'] {
            -webkit-appearance: none;
            margin: 0;
            -moz-appearance: textfield !important;
        }
    </style>
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border border-b-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    <div class="container mx-auto my-5">
        <div class="grid grid-cols-6 lg:gap-5">
            {{-- Mobile Information --}}
            <div class="col-span-6 p-3">
                <div class="block lg:hidden border border-[#23AEC1] rounded-md p-5">
                    <div class="flex">
                        <img class="rounded-md object-cover object-center shadow-md mr-5 w-20 h-20"
                            src="{{ asset('storage/img/car-detail-2.png') }}" alt="tur-img">
                        <div>
                            <p class="text-xl font-semibold">Nama Mobil</p>
                            <p class="text-base font-normal">Nama Seller</p>
                            <p class="text-base font-normal">Lokasi Yogyakarta</p>
                        </div>
                    </div>
                    <div class="mt-5">
                        <p>Rental Mulai</p>
                        <p><b> Jumat, 12 Agustus 2022 • 09.00</b></p>
                    </div>
                    <div class="mt-2.5">
                        <p>Rental Selesai</p>
                        <p><b>Sabtu, 13 Agustus 2022 • 09.00</b></p>
                    </div>
                    <div class="mt-2.5">
                        <p>Durasi</p>
                        <p><b>1 Hari • Lepas Kunci</b></p>
                    </div>
                </div>
            </div>
            {{-- Left Side --}}
            <div class="col-span-6 lg:col-span-4 bg-white p-3">

                {{-- Peserta 1 --}}
                <form action="{{ route('booking.store.rental') }}" method="POST">
                    @csrf
                    <div class="rounded-md shadow-sm border border-gray-200 py-5 px-2.5 lg:px-10 mb-5"
                        x-data="{ tur_form: { first_name: '', last_name: '', birth_date: '', jenis_kelamin: 'male', residen: false, non_residen: false, nationality: '', country_code: '+62', phone_number: '', email: '', residen_status: 'residen', kitas: '', same_as_user: false } }">
                        <div class="block sm:flex sm:justify-between my-2">
                            <p class="text-2xl font-semibold">Detail Pemesan</p>
                            <p class="text-sm font-medium">ID Booking: {{ $code }}</p>
                        </div>
                        <div class="my-5 flex items-center">
                            <input class="rounded-sm mr-2" type="checkbox" id="identity"
                                x-model="tur_form.same_as_user" x-init="$watch('tur_form.same_as_user', (isChecked) => {
                                    if (isChecked) {
                                        tur_form.first_name = '{{ auth()->user()->first_name }}';
                                        tur_form.last_name = '{{ isset($profile->last_name) ? $profile->last_name : null}}';
                                        tur_form.birth_date = '{{ isset($profile->date_of_birth) ? Carbon\Carbon::parse($profile->date_of_birth)->translatedFormat('d/m/Y') : null}}';
                                        tur_form.jenis_kelamin = '{{ isset($profile->gender) ? $profile->gender : null }}';
                                        tur_form.residen = true;
                                        tur_form.non_residen = false;
                                        tur_form.nationality = '{{ isset($profile->citizenship) ? $profile->citizenship : null }}';
                                        tur_form.country_code = '+62';
                                        tur_form.phone_number = '{{ isset($profile->phone_number_booking) ? $profile->phone_number_booking : null}}';
                                        tur_form.email = '{{ isset($profile->email_booking) ? $profile->email_booking : null }}';
                                        tur_form.residen_status = '';
                                    } else {
                                        tur_form.first_name = '';
                                        tur_form.last_name = '';
                                        tur_form.birth_date = '';
                                        tur_form.jenis_kelamin = 'male';
                                        tur_form.residen = false;
                                        tur_form.non_residen = false;
                                        tur_form.nationality = '';
                                        tur_form.country_code = '+62';
                                        tur_form.phone_number = '';
                                        tur_form.email = '';
                                        tur_form.residen_status = '';
                                        tur_form.kitas = '';
                                    }
                                })">
                            <label for="identity" class="text-sm sm:text-base text-black">Sama seperti
                                pemilik akun</label>
                        </div>

                        {{-- Name --}}
                        <div class="grid grid-rows-1 my-2">
                            <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                                <div class="relative block">
                                    <label for="first_name"
                                        class="form-label inline-block mb-2 text-gray-700 text-base font-medium">Nama
                                        Depan
                                        & Tengah</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="Nama depan" type="text" name="first_name" id="first_name"
                                        x-model="tur_form.first_name" required />
                                </div>
                                <div class="relative block">
                                    <label for="last_name"
                                        class="form-label inline-block mb-2 text-gray-700 text-base font-medium">Nama
                                        Belakang</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="Nama belakang" type="text" name="last_name" id="last_name"
                                        x-model="tur_form.last_name" required />
                                </div>
                            </div>
                        </div>

                        {{-- Identity --}}
                        <div class="grid grid-rows-1 my-2">
                            <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                                <div class="relative block">
                                    <label for="birth_date" class="form-label inline-block mb-2 text-gray-700">Tanggal
                                        Lahir</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="09/08/2022" type="text" name="birth_date" id="birth_date"
                                        x-model="tur_form.birth_date" required />
                                </div>

                                <div class="relative block">
                                    <label for="jenis_kelamin" class="form-label inline-block mb-2 text-gray-700">Jenis
                                        Kelamin</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                            name="jenis_kelamin" id="jenis_kelamin" x-model="tur_form.jenis_kelamin"
                                            required>
                                            <option value="">Pilih</option>
                                            <option value="Laki-laki">Pria</option>
                                            <option value="Perempuan">Wanita</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sm:grid sm:grid-cols-2 gap-5">
                            <div>

                                {{-- Residen - Non Residen --}}
                                <div class="sm:flex my-2 space-y-2 sm:space-y-0">
                                    <div class="flex items-center mr-5">
                                        <input class="rounded-sm form-checkbox mr-2" type="checkbox" name="residen"
                                            id="residen" x-model="tur_form.residen">
                                        <label for="residen" class="text-black">Residen</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input class="rounded-sm form-checkbox mr-2" type="checkbox"
                                            name="non-residen" id="non-residen" x-model="tur_form.non_residen">
                                        <label for="non_residen" class="text-black">Non Residen</label>
                                    </div>
                                </div>

                                {{-- Nationality --}}
                                <div class="relative block my-2">
                                    <label for="nationality"
                                        class="form-label inline-block mb-2 text-gray-700">Kebangsaan</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                            name="nationality" id="nationality" x-model="tur_form.nationality"
                                            required>
                                            <option value="">Pilih</option>
                                            <option value="indonesia">Indonesia</option>
                                            <option value="japan">Jepang</option>
                                        </select>
                                    </div>
                                </div>

                                {{-- Phone Number --}}
                                <div class="relative block my-2">
                                    <label for=" phone-number" class="form-label inline-block mb-2 text-gray-700">No.
                                        HP/WhatsApp</label>
                                    <div class="flex">
                                        <div
                                            class="relative border border-[#828282] rounded-l-md bg-white shadow-sm sm:text-sm">
                                            <select
                                                class="appearance-none form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-l-md bg-white"
                                                name="country_code" id="country_code" x-model="tur_form.country_code"
                                                required>
                                                <option selected value="+62">+62</option>
                                                <option value="+60">+60</option>
                                            </select>
                                        </div>
                                        <input
                                            class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-r-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                            placeholder="81234567890" type="text" name="phone_number"
                                            id="phone_number" x-model="tur_form.phone_number" required />
                                    </div>
                                </div>

                                {{-- Email --}}
                                <div class="relative block my-2">
                                    <label for=" email"
                                        class="form-label inline-block mb-2 text-gray-700">Email</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="emailku@gmail.com" type="text" name="email" id="email"
                                        x-model="tur_form.email" required />
                                </div>

                                {{-- Residen --}}
                                <div class="relative block my-2">
                                    <label for=" residen-status"
                                        class="form-label inline-block mb-2 text-gray-700">Status
                                        Residen</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                            name="residen_status" id="residen_status"
                                            x-model="tur_form.residen_status" required>
                                            <option value="">Pilih</option>
                                            <option value="residen">Residen</option>
                                            <option value="non-residen">Non Residen</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <p class="my-2">KTP/KITAS</p>
                                <div x-data="displayImage()">
                                    <div class="mb-2">
                                        <template x-if="imageUrl">
                                            <img :src="imageUrl"
                                                class="object-contain rounded border border-gray-300 w-full h-56">
                                        </template>

                                        <template x-if="!imageUrl">
                                            <div
                                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-full h-56">
                                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                    width="20px" height="20px">
                                            </div>
                                        </template>

                                        <input class="mt-2" type="file" accept="image/*" @change="selectedFile"
                                            x-model="tur_form.kitas" required>
                                    </div>
                                </div>
                                <p class="text-xs font-normal">Catatan: Beberapa seller mungkin menerapkan kebijakan
                                    harga
                                    berbeda antara residen dan
                                    non-residen</p>
                            </div>
                        </div>
                    </div>

                    {{-- Pilihan --}}
                    <div class="py-2.5 px-5 lg:py-5 lg:px-10 mb-5 rounded-md border-y-2 border-[#E0E0E0] bg-[#F7F7F7]">
                        <div>
                            @if(count($fields) > 0)
                            <div class="grid grid-cols-2 items-center">
                                <p class="text-base lg:text-2xl font-semibold grid justify-items-start text-[#333333]">
                                    Pilihan
                                </p>
                            </div>
                                @foreach ($fields as $item)
                                    <div class="flex lg:grid lg:grid-cols-2">
                                        <div class="mt-0 lg:mt-5 w-[222px]" x-data="choiceMandatory()">
                                            @if($item['kewajiban_pilihan']==='wajib')
                                            <input type="checkbox" class="appearance-none checked:bg-[#27BED3] w-3 h-3 ..." name="pilihan[]" value="{{$item['harga_pilihan']}}" checked/>
                                            @else
                                            <input type="checkbox" class="appearance-none checked:bg-[#27BED3] w-3 h-3 ..." name="pilihan[]" value="{{$item['harga_pilihan']}}"/>
                                            @endif
                                            <span class="font-medium text-[#333333] ml-1 text-[10px] lg:text-base">{{$item['nama_pilihan']}}</span>
                                        </div>
                                        <p class="mt-2 lg:mt-5 font-semibold text-[#27BED3] ml-4 lg:ml-10 text-[10px] lg:text-base">{{number_format($item['harga_pilihan'])}}</p>                                    
                                    </div>
                                @endforeach
                            @endif
                            <div class="grid grid-cols-2 items-center">
                                <p class="text-base lg:text-2xl font-semibold grid justify-items-start text-[#333333]">
                                    Pilihan tidak tersedia
                                </p>
                            </div>

                            {{-- <div class="flex lg:grid lg:grid-cols-2">
                                <div class="mt-0 lg:mt-5 w-[222px]">
                                    <input type="checkbox" class="appearance-none checked:bg-[#27BED3] w-3 h-3 ..." />
                                    <span class="font-medium text-[#333333] ml-1 text-[10px] lg:text-base">Sopir
                                        Berbahasa
                                        Inggris</span>
                                </div>
                                <p
                                    class="mt-2 lg:mt-5 font-semibold text-[#27BED3] ml-4 lg:ml-10 text-[10px] lg:text-base">
                                    Rp. 1,500,000</p>
                            </div> --}}
                        </div>
                    </div>
                    <p class="mt-2.5 lg:mt-5 text-sm lg:text-base">Sebelum mengkonfirmasi pesanan Anda, harap untuk
                        memastikan kembali bahwa data yang anda masukkan sudah akurat.</p>

                    <input type="hidden" name="customer_id" value="{{ isset($profile->id) ? $profile->id : null}}">
                    <input type="hidden" name="product_detail_id" value="{{ $detail['product_detail_id'] }}">
                    <input type="hidden" name="total_price" id="total_price" value="{{ $detail['total'] }}">
                    <input type="hidden" name="harga_pilihan" value="" id="harga_pilihan">
                    <input type="hidden" name="booking_code" value="{{ $code }}">
                    <input type="hidden" name="toko_id" value="{{ $detail['toko_id'] }}">
                    <input type="hidden" name="type" value="{{ $detail['type'] }}">
                    <input type="hidden" name="check_in_date" value="{{ $detail['check_in_date'] }}">
                    <input type="hidden" name="check_out_date" value="{{ $detail['check_out_date'] }}">
                    @if (auth()->user()->role == 'agent')
                        <input type="hidden" name="agent_id" value="{{ $agent_id->id }}">
                        <input type="hidden" name="role" value="{{ $agent_id->role }}">
                    @endif
                    <button type="submit"
                        class="text-white bg-[#27BED3] w-full hover:bg-[#1C97A8] focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-0 lg:mb-5 lg:mb-10 mt-5 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Simpan
                        di Koper</button>
                </form>
            </div>


            {{-- Right Side --}}
            <div class="col-span-6 lg:col-span-2 space-y-5 p-3">

                {{-- Information --}}
                <div class="hidden lg:block border border-[#23AEC1] rounded-md shadow-lg p-5">
                    <div class="flex">
                        <img class="rounded-md object-cover object-center shadow-md mr-5 w-20 h-20"
                            src="{{ asset('storage/img/car-detail-2.png') }}" alt="tur-img">
                        <div>
                            <p class="text-xl font-semibold">{{ $detail['product_name'] }}</p>
                            <p class="text-base font-normal">{{ $seller->first_name }}</p>
                            <p class="text-base font-normal capitalize">LOKASI {{$tag_location_1 !=' ' ? $tag_location_1.' - ' : null}}
                                {{$tag_location_2 !=' ' ? $tag_location_2.' - ' : null}}
                                {{$tag_location_3 !=' ' ? $tag_location_3.' - ' : null}}
                                {{$tag_location_4 !=' ' ? $tag_location_4 : null}}</p>
                        </div>
                    </div>
                    <div class="mt-5">
                        @php
                            $dateStart = DateTime::createFromFormat('Y-m-d', $detail['check_in_date']);
                            $dateEnd = DateTime::createFromFormat('Y-m-d', $detail['check_out_date']);
                        @endphp
                        <p>Rental Mulai</p>
                        <p><b> {{ $dateStart->format('l') }}, {{ $dateStart->format('d F Y') }} • 09.00</b></p>
                    </div>
                    <div class="mt-2.5">
                        <p>Rental Selesai</p>
                        <p><b>{{ $dateEnd->format('l') }}, {{ $dateEnd->format('d F Y') }} • 09.00</b></p>
                    </div>
                    <div class="mt-2.5">
                        @php
                            $timestamp1 = strtotime($detail['check_in_date']);
                            $timestamp2 = strtotime($detail['check_out_date']);
                            $diff = $timestamp2 - $timestamp1;
                            $days = floor($diff / (60 * 60 * 24));
                        @endphp
                        <p>Durasi</p>
                        <p><b>{{ $days }} Hari • Lepas Kunci</b></p>
                    </div>
                </div>

                {{-- Kupon --}}
                {{-- <div>KUPON</div> --}}

                {{-- Biaya --}}
                <div class="border border-[#4F4F4F] rounded-md shadow-lg p-5">
                    <p class="text-xl font-semibold mb-2">Biaya</p>
                    <div class="space-y-1 my-2">
                        <div class="flex justify-between text-base font-semibold">
                            <p class="text-[#333333]">Harga</p>
                            <p class="text-[#23AEC1]">Rp. {{ number_format($detail['total']) }}</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Diskon per tanggal</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Surcharge per tanggal</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                    </div>

                    <hr class="border border-[#4F4F4F] my-5" />

                    <div class="space-y-1 my-2">
                        <div class="flex justify-between text-base font-semibold">
                            <p class="text-[#333333]">Harga per Tanggal</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Diskon Grup</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Harga Bersih</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Kupon Toko</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                    </div>

                    <hr class="border border-[#707070] my-5" />

                    <div class="space-y-1 my-2">
                        <div class="flex justify-between text-base font-semibold">
                            <p class="text-[#333333]">Harga Akhir</p>
                            <p class="text-[#23AEC1]">Rp.  {{ number_format($detail['total']) }}</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Pilihan</p>
                            <p class="text-[#23AEC1]" id="add_price_amount">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Extra</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Biaya Lain-lain</p>
                            <p class="text-[#23AEC1]">Rp. </p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Biaya Pemesanan</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Pajak</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Promo</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Kupon</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                    </div>

                    <hr class="border border-[#707070] my-5" />

                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-lg font-semibold">Total Bayar</p>
                        <p class="text-lg font-semibold text-[#23AEC1]" id="total_bayar">Rp.
                            {{ number_format($detail['total']) }}</p>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                    this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                    if (!event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },
            }
        }

        $('input[name="pilihan[]"]').on('change',function(){
            let checked = $(this).is(":checked");
            let total_amount = '{{$detail['total']}}';
            let new_total_amount = 0;
            var harga = 0;

            if(checked){
                $('input[name="pilihan[]"]:checked').each(function(i, obj){
                    console.log(obj.value);
                    harga += parseInt(obj.value);
                })

                new_total_amount = parseInt(total_amount) + harga;

                console.log('harga pilihan: ', harga);

                setHarga(new_total_amount, harga)
            }


            if(!checked){
                
                harga = parseInt($("#harga_pilihan").val());
                total_amount = parseInt($("#total_price").val());
                
                harga -= $(this).val();
                new_total_amount = total_amount - $(this).val();

                console.log('total harga: ', total_amount);
                console.log('harga pilihan: ', harga);

                console.log(harga)
                console.log(new_total_amount)
                setHarga(new_total_amount, harga)

            }
        });

        function setHarga(new_total_amount, harga){
            $("#harga_pilihan").val(harga)
            $("#total_price").val(new_total_amount);
            $("#add_price_amount").html(`Rp. ${harga.toLocaleString('en-US',{ minimumFractionDigits: 0 })}`)
            $("#total_bayar").html(`Rp. ${new_total_amount.toLocaleString('en-US',{ minimumFractionDigits: 0 })}`)
        }

        function choiceMandatory(){
            let is_choice = $('input[name="pilihan[]"]').is(":checked");
            let choice = $('input[name="pilihan[]"]:checked');
            let total_amount = '{{$detail['total']}}';
            let new_total_amount = 0;
            let choice_price = 0;

            if(is_choice){
                choice.prop('disabled',true);
                choice.each(function(i, obj){
                    choice_price += parseInt(obj.value);
                });
    
                new_total_amount = parseInt(total_amount) + choice_price;

                console.log(choice_price)
                console.log(new_total_amount)
                setHarga(new_total_amount, choice_price)
            }
        }
    </script>
</body>

</html>
