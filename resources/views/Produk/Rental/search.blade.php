<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Search Tur') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    {{-- select 2 --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #sedan:checked+#sedan {
            display: block;
        }

        #filter-toggle:checked+#filter {
            display: block;
        }
        .select2-container .select2-selection--single {
            height: 100%;
            /* padding-left: 0.75rem; */
            /* padding-right: 0.75rem; */
            padding-top: 0.5rem;
            padding-bottom: 0rem;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 100%;
        }
        .select2-selection__arrow {
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            /* padding-right: 0.75rem; */
        }
        .select2-container--default .select2-selection--single{
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            /* border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px; */
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #444;
            line-height: 35px;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border border-b-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Hero --}}
    <div class="relative bg-slate-400">
        <img class="w-full opacity-70 object-cover" src="{{ asset('storage/img/Header.png') }}" alt="banner-tur">
    </div>

    <div class="grid text-center my-5 hidden lg:block">
        <p>Search</p>
    </div>
    <div class="container my-5 mx-2.5 w-auto">
        <div class="flex lg:grid-cols-none block lg:hidden ">
            <div class="text-sm font-medium text-blue-600 hover:underline dark:text-blue-500 rounded-lg">
                <form>
                    <fieldset>
                        <div class="relative border border-[#4F4F4F] text-gray-500 bg-white shadow-lg rounded-lg">
                            <select class="appearance-none w-full py-2 pl-5 pr-10 bg-white text-sm"
                                name="produk-dropdown" id="produk-dropdown">
                                <option value="Harga(Termahal-Termurah)">Harga (Termahal-Termurah)</option>
                                <option value="Harga(Termurah-Termahal)">Harga (Termurah-Termahal)</option>
                            </select>
                        </div>
                    </fieldset>
                </form>
            </div>

            <div class="grid justify-items-end ml-20">
                <label class="items-center w-10 px-2.5 py-2 text-white rounded-full cursor-pointer hover:text-white"
                    for="filter-toggle">
                    <img class="w-full opacity-70 object-cover" src="{{ asset('storage/icons/filter-solid.svg') }}"
                        alt="filter-toogle">
                </label>
            </div>
        </div>

        <input class="hidden" type="checkbox" id="filter-toggle" />

        <div class="hidden shadow-lg divide-y divide-y-reverse mt-2.5 rounded-lg" id="filter">

            <div class="p-2.5">
                <p class="my-2 font-semibold text-xs">Layanan</p>
                <div class="flex items-center mb-1">
                    <input id="default-checkbox" type="checkbox" value=""
                        class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                    <label for="default-checkbox"
                        class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Layanan 1</label>
                </div>
                <div class="flex items-center">
                    <input checked="" id="checked-checkbox" type="checkbox" value=""
                        class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                    <label for="checked-checkbox"
                        class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Layanan 2</label>
                </div>
            </div>

            <div class="p-2.5">
                <p class="mb-2 font-semibold text-xs">Harga</p>
                <input type="text" name="min_price" placeholder="Harga Minimum"
                    class="placeholder:text-slate-400 mb-3 block bg-[#F2F2F2] w-full border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-[#9E3D64] focus:ring-[#9E3D64] focus:ring-1 sm:text-sm">
                <input type="text" name="max_price" placeholder="Harga Maksimum"
                    class="placeholder:text-slate-400 mb-3 block bg-[#F2F2F2] w-full border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-[#9E3D64] focus:ring-[#9E3D64] focus:ring-1 sm:text-sm">
                {{-- <input type="hidden" name="search" value="{{ $data }}"> --}}
            </div>

            <div class="px-2.5 pb-2.5">
                <p class="my-2 font-semibold text-xs">Kategori</p>
                <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                    <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                        <button
                            class="text-xs sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                            x-on:click="open = !open">Sedan</button>
                        <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                            x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            alt="chevron-down" width="15px" height="15px">
                    </div>
                    <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                        x-show="open" x-transition>
                        <div class="flex items-center">
                            <input id="default-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="default-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 1</label>
                        </div>
                        <div class="flex items-center">
                            <input checked="" id="checked-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="checked-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 2</label>
                        </div>
                    </ul>
                </div>

                <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                    <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                        <button
                            class="text-xs sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                            x-on:click="open = !open">Mini</button>
                        <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                            x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            alt="chevron-down" width="15px" height="15px">
                    </div>

                    <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                        x-show="open" x-transition>
                        <div class="flex items-center">
                            <input id="default-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="default-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 1</label>
                        </div>
                        <div class="flex items-center">
                            <input checked="" id="checked-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="checked-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 2</label>
                        </div>
                    </ul>
                </div>

                <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                    <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                        <button
                            class="text-xs sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                            x-on:click="open = !open">MVP</button>
                        <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                            x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            alt="chevron-down" width="15px" height="15px">
                    </div>

                    <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                        x-show="open" x-transition>
                        <div class="flex items-center">
                            <input id="default-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="default-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 1</label>
                        </div>
                        <div class="flex items-center">
                            <input checked="" id="checked-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="checked-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 2</label>
                        </div>
                    </ul>
                </div>

                <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                    <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                        <button
                            class="text-xs sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                            x-on:click="open = !open">Luxury</button>
                        <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                            x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            alt="chevron-down" width="15px" height="15px">
                    </div>

                    <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                        x-show="open" x-transition>
                        <div class="flex items-center">
                            <input id="default-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="default-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 1</label>
                        </div>
                        <div class="flex items-center">
                            <input checked="" id="checked-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="checked-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 2</label>
                        </div>
                    </ul>
                </div>

                <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                    <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                        <button
                            class="text-xs sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                            x-on:click="open = !open">Mini Bus</button>
                        <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                            x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            alt="chevron-down" width="15px" height="15px">
                    </div>

                    <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                        x-show="open" x-transition>
                        <div class="flex items-center">
                            <input id="default-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="default-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 1</label>
                        </div>
                        <div class="flex items-center">
                            <input checked="" id="checked-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="checked-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 2</label>
                        </div>
                    </ul>
                </div>
            </div>

        </div>

    </div>

    <div class="container my-2.5 lg:my-10 lg:mx-auto">
        <div class="grid grid-cols-5 lg:grid-cols-10 gap-2 mx-2.5">
            <div class="col-start-1 col-end-3">
                <p class="text-sm lg:text-2xl font-semibold">Jenis Kendaraan</p>
            </div>
            <button type="submit"
                class="items-center px-px py-px lg:py-2.5 text-xs lg:text-sm font-medium text-center text-white bg-[#23AEC1] rounded-full">Mobil</button>
            <button type="submit"
                class="items-center px-px py-px lg:py-2.5 text-xs lg:text-sm font-medium text-center text-[#333333] bg-[#F2F2F2] rounded-full">Motor</button>
            <button type="submit"
                class="items-center px-px py-px lg:py-2.5 text-xs lg:text-sm font-medium text-center text-[#333333] bg-[#F2F2F2] rounded-full">Boat</button>
        </div>

        <div class="lg:hidden block flex justify-between items-center m-2.5">
            <h5 class="text-sm lg:text-xl leading-none text-gray-900 lg:ml-5">Menampilkan 5 Mobil <span
                    class="font-semibold italic">"Wilayah Yogyakarta pada Tanggal 21 Juli 2022"</span></h5>
        </div>
    </div>

    <div class="container mx-auto">
        <div class="grid grid-cols-10 mx-2.5 gap-2.5 lg:gap-5">
            {{-- Filter --}}
            <div class="hidden lg:block col-start-1 col-end-3">
                <p class="text-sm lg:text-2xl font-semibold">Filter</p>
                <div class="shadow-lg divide-y divide-y-reverse mt-5 rounded-lg">
                    <form action="{{ route('rental.searchByHarga') }}" method="POST">
                        @csrf
                        <input type="hidden" name="search" value="{{ $lokasi }}">
                        <input type="hidden" name="durasi" value="{{ $durasi }}">
                        <input type="hidden" name="tgl_mulai" value="{{ $tgl_mulai }}">
                        <input type="hidden" name="waktu_mulai" value="{{ $waktu_mulai }}">
                        <input type="hidden" name="tgl_selesai" value="{{ $tgl_selesai }}">
                        <input type="hidden" name="waktu_selesai" value="{{ $waktu_selesai }}">
                        <input type="hidden" name="lepas_kunci" value="{{ $lepas_kunci }}">
                        {{-- <div class="p-5">
                            <p class="my-2 font-semibold">Layanan</p>
                            <div class="flex items-center mb-4">
                                <input id="default-checkbox" type="checkbox" value=""
                                    class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="default-checkbox"
                                    class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Layanan 1</label>
                            </div>
                            <div class="flex items-center">
                                <input checked="" id="checked-checkbox" type="checkbox" value=""
                                    class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="checked-checkbox"
                                    class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Layanan 2</label>
                            </div>
                        </div> --}}
                        <div class="p-5">
                            <p class="my-2 font-semibold">Harga</p>
                            <input type="text" name="min_price" placeholder="Harga Minimum"
                                class="placeholder:text-slate-400 mb-3 block bg-[#F2F2F2] w-full border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-[#9E3D64] focus:ring-[#9E3D64] focus:ring-1 sm:text-sm">
                            <input type="text" name="max_price" placeholder="Harga Maksimum"
                                class="placeholder:text-slate-400 mb-3 block bg-[#F2F2F2] w-full border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-[#9E3D64] focus:ring-[#9E3D64] focus:ring-1 sm:text-sm">
                        </div>
                        <div class="p-5">
                            <p class="my-2 font-semibold">Kategori</p>
                            <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                                <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                                    <button
                                        class="text-sm sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                        x-on:click="open = !open">Sedan</button>
                                    <img class="float-right duration-200 cursor-pointer"
                                        :class="{ 'rotate-180': open }" x-on:click="open = !open"
                                        src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                        alt="chevron-down" width="15px" height="15px">
                                </div>
                                <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                                    x-show="open" x-transition>
                                    <div class="flex items-center">
                                        <input id="default-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="default-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            1</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input checked="" id="checked-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="checked-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            2</label>
                                    </div>
                                </ul>
                            </div>

                            <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                                <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                                    <button
                                        class="text-sm sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                        x-on:click="open = !open">Mini</button>
                                    <img class="float-right duration-200 cursor-pointer"
                                        :class="{ 'rotate-180': open }" x-on:click="open = !open"
                                        src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                        alt="chevron-down" width="15px" height="15px">
                                </div>
                                <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                                    x-show="open" x-transition>
                                    <div class="flex items-center">
                                        <input id="default-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="default-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            1</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input checked="" id="checked-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="checked-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            2</label>
                                    </div>
                                </ul>
                            </div>

                            <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                                <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                                    <button
                                        class="text-sm sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                        x-on:click="open = !open">MVP</button>
                                    <img class="float-right duration-200 cursor-pointer"
                                        :class="{ 'rotate-180': open }" x-on:click="open = !open"
                                        src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                        alt="chevron-down" width="15px" height="15px">
                                </div>
                                <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                                    x-show="open" x-transition>
                                    <div class="flex items-center">
                                        <input id="default-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="default-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            1</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input checked="" id="checked-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="checked-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            2</label>
                                    </div>
                                </ul>
                            </div>

                            <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                                <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                                    <button
                                        class="text-sm sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                        x-on:click="open = !open">Luxury</button>
                                    <img class="float-right duration-200 cursor-pointer"
                                        :class="{ 'rotate-180': open }" x-on:click="open = !open"
                                        src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                        alt="chevron-down" width="15px" height="15px">
                                </div>
                                <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                                    x-show="open" x-transition>
                                    <div class="flex items-center">
                                        <input id="default-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="default-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            1</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input checked="" id="checked-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="checked-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            2</label>
                                    </div>
                                </ul>
                            </div>

                            <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                                <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                                    <button
                                        class="text-sm sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                        x-on:click="open = !open">Mini Bus</button>
                                    <img class="float-right duration-200 cursor-pointer"
                                        :class="{ 'rotate-180': open }" x-on:click="open = !open"
                                        src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                        alt="chevron-down" width="15px" height="15px">
                                </div>
                                <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                                    x-show="open" x-transition>
                                    <div class="flex items-center">
                                        <input id="default-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="default-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            1</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input checked="" id="checked-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="checked-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            2</label>
                                    </div>
                                </ul>
                            </div>
                        </div>
                        <hr>
                        <div class="p-5">
                            <button
                                class="px-2 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white ">
                                Terapkan
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-span-10 lg:col-span-8">
                <div class="flex lg:grid lg:grid-cols-4 justify-between items-center mb-4 hidden lg:block">
                    <h5 class="col-start-1 col-span-3 text-sm lg:text-xl leading-none text-gray-900 lg:ml-5">
                        Menampilkan 5 Mobil <span class="font-semibold italic">"Wilayah Yogyakarta pada Tanggal
                            {{ Carbon\Carbon::parse($tgl_mulai)->translatedFormat('d F Y') }}"</span></h5>
                    <div class="col-span-1 text-sm font-medium text-blue-600 hover:underline dark:text-blue-500">
                        <form>
                            <fieldset>
                                <div
                                    class="relative border border-[#4F4F4F] text-gray-500 bg-white shadow-lg focus:border-2 focus:border-[#4F4F4F] rounded-lg">
                                    <select class="appearance-none w-full py-2 pl-5 pr-10 bg-white"
                                        name="produk-dropdown" id="produk-dropdown">
                                        <option value="Harga(Termahal-Termurah)">Harga (Termahal-Termurah)</option>
                                        <option value="Harga(Termurah-Termahal)">Harga (Termurah-Termahal)</option>
                                    </select>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>

                <div class="grid grid-cols-10 lg:grid-cols-1 gap-5">
                    @foreach ($data_detail as $value)
                        @php
                            $data_mobil = App\Models\DetailKendaraan::where('id', $value->id_detail_kendaraan)->first();
                        @endphp
                        <a href="{{ route('rental.show', $value->product->slug) }}"
                            class="lg:grid col-span-5 lg:grid-cols-10 grid-rows-3 lg:grid-rows-none grid-flow-col border-2 border-zinc-50 rounded-lg hover:shadow-xl duration-200 py-2.5 ">
                            <div class="row-span-1 col-span-5 lg:col-start-1 lg:col-span-2 ">
                                <img class="rounded-t-lg object-cover object-center lg:p-5 h-32 lg:h-max w-full"
                                    src="{{ asset('storage/img/img-car.png') }}" alt="transfer-1">
                            </div>
                            <div class="row-span-1 lg:col-start-3 lg:col-end-7">
                                <div class="p-2.5 lg:py-5">
                                    <p class="text-sm lg:text-lg font-semibold">{{ $data_mobil->nama_kendaraan }}
                                    </p>
                                    <div class="flex my-px gap-1 lg:gap-2">
                                        <div
                                            class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                            Sedan</div>
                                    </div>
                                    <div class="my-1 lg:my-2 flex">
                                        <img class="mr-1 w-3 h-3 lg:w-3.5 lg:h-3.5"
                                            src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-[8px] lg:text-xs">4.7 <span
                                                class="font-semibold text-[#4F4F4F]">(12 Ulasan)</span></p>
                                    </div>
                                    <div class="flex mt-3.5 lg:mt-10 lg:pt-2">
                                        <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                            src="{{ asset('storage/icons/seats.svg') }}" alt="rating"
                                            width="17px" height="17px">
                                        <p class="font-bold text-[8px] lg:text-sm mr-2">
                                            {{ $data_mobil->kapasitas_kursi }} Seats</p>
                                        <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                            src="{{ asset('storage/icons/koper.svg') }}" alt="rating"
                                            width="17px" height="17px">
                                        <p class="font-bold  text-[8px] lg:text-sm mr-2">
                                            {{ $data_mobil->kapasitas_koper }} Koper</p>
                                    </div>
                                    <p class="font-semibold text-[8px] lg:text-sm mt-1">Layanan 1 • Layanan 1
                                    </p>
                                </div>
                            </div>
                            <div class="row-span-1 lg:col-start-7 lg:col-span-8 text-left lg:text-center px-2.5">
                                <div class="pb-2.5 lg:py-12">
                                    <p class="font-bold text-xs lg:text-2xl text-[#23AEC1]">IDR
                                        {{ number_format($data_mobil->harga_akomodasi) }}</p>
                                    <button type="submit"
                                        class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-lg font-semibold text-center text-white bg-[#FFB800] rounded lg:rounded-lg">Pesan</button>
                                </div>
                            </div>
                        </a>
                    @endforeach
                    {{-- <a href="#"
                        class="lg:grid col-span-5 lg:grid-cols-10 grid-rows-3 lg:grid-rows-none grid-flow-col border-2 border-zinc-50 rounded-lg hover:shadow-xl duration-200 py-2.5 ">
                        <div class="row-span-1 col-span-5 lg:col-start-1 lg:col-span-2 ">
                            <img class="rounded-t-lg object-cover object-center lg:p-5 h-32 lg:h-max w-full"
                                src="{{ asset('storage/img/img-car.png') }}" alt="transfer-1">
                        </div>
                        <div class="row-span-1 lg:col-start-3 lg:col-end-7">
                            <div class="p-2.5 lg:py-5">
                                <p class="text-sm lg:text-lg font-semibold">Nama Mobil</p>
                                <div class="flex my-px gap-1 lg:gap-2">
                                    <div
                                        class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                        Sedan</div>
                                </div>
                                <div class="my-1 lg:my-2 flex">
                                    <img class="mr-1 w-3 h-3 lg:w-3.5 lg:h-3.5"
                                        src="{{ asset('storage/icons/Star 1.png') }}" alt="rating" width="15px"
                                        height="15px">
                                    <p class="font-bold text-[8px] lg:text-xs">4.7 <span
                                            class="font-semibold text-[#4F4F4F]">(12 Ulasan)</span></p>
                                </div>
                                <div class="flex mt-3.5 lg:mt-10 lg:pt-2">
                                    <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                        src="{{ asset('storage/icons/seats.svg') }}" alt="rating" width="17px"
                                        height="17px">
                                    <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                                    <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                        src="{{ asset('storage/icons/koper.svg') }}" alt="rating" width="17px"
                                        height="17px">
                                    <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                                </div>
                                <p class="font-semibold text-[8px] lg:text-sm mt-1">Layanan 1 • Layanan 1 </p>
                            </div>
                        </div>
                        <div class="row-span-1 lg:col-start-7 lg:col-span-8 text-left lg:text-center px-2.5">
                            <div class="pb-2.5 lg:py-12">
                                <p class="font-bold text-xs lg:text-2xl text-[#23AEC1]">IDR 100,000</p>
                                <button type="submit"
                                    class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-lg font-semibold text-center text-white bg-[#FFB800] rounded lg:rounded-lg">Pesan</button>
                            </div>
                        </div>
                    </a>

                    <a href="#"
                        class="lg:grid col-span-5 lg:grid-cols-10 grid-rows-3 lg:grid-rows-none grid-flow-col border-2 border-zinc-50 rounded-lg hover:shadow-xl duration-200 mb-5 py-2.5 ">
                        <div class="row-span-1 col-span-5 lg:col-start-1 lg:col-span-2 ">
                            <img class="rounded-t-lg object-cover object-center lg:p-5 h-32 lg:h-auto w-full"
                                src="{{ asset('storage/img/img-car.png') }}" alt="transfer-1">
                        </div>
                        <div class="row-span-1 lg:col-start-3 lg:col-end-7">
                            <div class="p-2.5 lg:py-5">
                                <p class="text-sm lg:text-lg font-semibold">Nama Mobil</p>
                                <div class="flex my-px gap-1 lg:gap-2">
                                    <div
                                        class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                        Sedan</div>
                                </div>
                                <div class="my-1 lg:my-2 flex">
                                    <img class="mr-1 w-3 h-3 lg:w-3.5 lg:h-3.5"
                                        src="{{ asset('storage/icons/Star 1.png') }}" alt="rating" width="15px"
                                        height="15px">
                                    <p class="font-bold text-[8px] lg:text-xs">4.7 <span
                                            class="font-semibold text-[#4F4F4F]">(12 Ulasan)</span></p>
                                </div>
                                <div class="flex mt-3.5 lg:mt-10 lg:pt-2">
                                    <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                        src="{{ asset('storage/icons/seats.svg') }}" alt="rating" width="17px"
                                        height="17px">
                                    <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                                    <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                        src="{{ asset('storage/icons/koper.svg') }}" alt="rating" width="17px"
                                        height="17px">
                                    <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                                </div>
                                <p class="font-semibold text-[8px] lg:text-sm mt-1">Layanan 1 • Layanan 1 </p>
                            </div>
                        </div>
                        <div class="row-span-1 lg:col-start-7 lg:col-span-8 text-left lg:text-center px-2.5">
                            <div class="pb-2.5 lg:py-12">
                                <p class="font-bold text-xs lg:text-2xl text-[#23AEC1]">IDR 100,000</p>
                                <button type="submit"
                                    class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-lg font-semibold text-center text-white bg-[#FFB800] rounded lg:rounded-lg">Pesan</button>
                            </div>
                        </div>
                    </a> --}}
                </div>

            </div>
        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        var turAboutSwiper = new Swiper(".tur-about-swiper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
                enabled: true,
            },
            breakpoints: {
                640: {
                    slidesPerView: 3,
                    navigation: {
                        enabled: false
                    },
                }
            }
        });

        var reviewSwipper = new Swiper(".review-swipper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    </script>
</body>

</html>
