<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Search Tur') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Hero --}}
    <div class="relative bg-slate-400">
        <img class="w-full max-h-[317px] opacity-70 object-cover"
            src="https://images.unsplash.com/photo-1615278166719-c411455d594d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzZ8fHRvdXJ8ZW58MHx8MHx8&auto=format&fit=crop&w=1920&q=60"
            alt="banner-tur">
    </div>

    <div class="container mx-auto my-5 text-center">
        <x-produk.cari-tur>
            <x-slot name="turFrom">
                <select name="search" id="" style="background-image: none;"
                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                    <option value="">Dari</option>
                    @foreach ($tur as $value)
                    @php
                    $regency = App\Models\Regency::where('id', $value->productdetail->regency_id)->get();
                    @endphp
                    @foreach ($regency as $item)
                    <option style="font-size: 10px" value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                    @endforeach
                </select>
            </x-slot>
            <x-slot name="turTo">
                <select name="tur_to" id="" style="background-image: none;"
                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                    <option value="">Ke</option>
                    @foreach ($regencyTo as $foo)
                    <option style="font-size: 10px" value="{{ $foo->id }}">{{ $foo->name }}</option>
                    @endforeach
                </select>
            </x-slot>
        </x-produk.cari-tur>
    </div>

    {{-- Content --}}
    <div class="container px-5 sm:px-0 mx-auto my-5">
        <div class="grid grid-cols-12 lg:gap-10">
            <div class="col-span-12 lg:col-span-4">
                <p class="text-2xl font-medium">Filter</p>
                <div class="p-5 shadow-lg">
                    <p class="my-3">Harga</p>
                    <form action="{{ route('tur.searchByHarga') }}" method="POST">
                        @csrf
                        <input type="hidden" name="search" value="{{ $search }}">
                        <input type="hidden" name="tur_to" value="{{ $tur_to }}">
                        <input type="hidden" name="confirmation" value="{{ $konfirmasi_by }}">
                        <input type="hidden" name="kategori" value="{{ $kategori }}">
                        <input type="hidden" name="tgl" value="{{ $tgl }}">
                        <input type="text" name="min_price" placeholder="Harga Minimum"
                            class="placeholder:text-slate-400 mb-3 block bg-[#F2F2F2] w-full border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-[#9E3D64] focus:ring-[#9E3D64] focus:ring-1 sm:text-sm">
                        <input type="text" name="max_price" placeholder="Harga Maksimum"
                            class="placeholder:text-slate-400 mb-3 block bg-[#F2F2F2] w-full border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-[#9E3D64] focus:ring-[#9E3D64] focus:ring-1 sm:text-sm">
                        <button
                            class="px-2 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white ">
                            Terapkan
                        </button>
                    </form>
                </div>
            </div>
            <div class="col-span-12 lg:col-span-8">
                @if ($search != null && $konfirmasi_by == null && $kategori != null)
                <p class="my-5">Menampilkan 100+ Hasil <span class="font-semibold italic">“Destinasi
                        Yogyakarta
                        dari
                        Jakarta pada Tanggal {{ Carbon\Carbon::parse($tgl)->translatedFormat('d F Y') }}
                        bersifat {{ $kategori }}”</span></p>
                @endif
                @if ($konfirmasi_by != null && $search == null && $kategori != null)
                <p class="my-5">Menampilkan 100+ Hasil <span class="font-semibold italic">“Destinasi
                        Yogyakarta
                        dari
                        Jakarta pada Tanggal {{ Carbon\Carbon::parse($tgl)->translatedFormat('d F Y') }}
                        bersifat {{ $kategori }}”</span></p>
                @endif
                @if ($kategori != null && $search == null && $konfirmasi_by == null)
                <p class="my-5">Menampilkan 100+ Hasil <span class="font-semibold italic">“Destinasi
                        Yogyakarta
                        dari
                        Jakarta pada Tanggal {{ Carbon\Carbon::parse($tgl)->translatedFormat('d F Y') }}
                        bersifat {{ $kategori }}”</span></p>
                @endif
                <div class="grid lg:grid-cols-3 md:grid-cols-3 gap-5">
                    @if ($search != null && $konfirmasi_by == null && $kategori == 'Open Trip' && $tur_to != null && $tgl != null)
                    @foreach ($data_tur as $item)
                    @php
                    $regency = App\Models\Regency::where('id', $item->regency_id)->first();
                    $price = App\Models\Price::where('id', $item->harga_id)->first();
                                $dewasa_residen = $price->dewasa_residen;
                                $dewasa_non_residen = $price->dewasa_non_residen;
                                $harga_min = min($dewasa_residen, $dewasa_non_residen);
                    @endphp
                    @if ($harga_min >= $min_price && $harga_min <= $max_price)
                    <a href="{{ route('tur.show', $item->product->slug) }}"
                        class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                        <img class="rounded-t-lg object-cover object-center"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="tur-1">
                        <div class="p-3">
                            <p class="text-base font-medium">{{ $regency->name }}</p>
                            <p class="text-lg font-semibold">{{ $item->product->product_name }}</p>
                            <div class="mb-3 flex">
                                <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                    width="15px" height="15px">
                                <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12 Ulasan)</span>
                                </p>
                            </div>
                            <p class="text-xl font-semibold text-[#23AEC1]">IDR {{number_format($harga_min)}}</p>
                        </div>
                    </a>
                    @endif
                    {{-- <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                        <img class="rounded-t-lg object-cover object-center"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="tur-1">
                        <div class="p-3">
                            <p class="text-base font-medium">Lokasi</p>
                            <p class="text-lg font-semibold">Nama Tur</p>
                            <div class="mb-3 flex">
                                <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                    width="15px" height="15px">
                                <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12 Ulasan)</span>
                                </p>
                            </div>
                            <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                        </div>
                    </a>
                    <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                        <img class="rounded-t-lg object-cover object-center"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="tur-1">
                        <div class="p-3">
                            <p class="text-base font-medium">Lokasi</p>
                            <p class="text-lg font-semibold">Nama Tur</p>
                            <div class="mb-3 flex">
                                <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                    width="15px" height="15px">
                                <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12
                                        Ulasan)</span>
                                </p>
                            </div>
                            <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                        </div>
                    </a>
                    <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                        <img class="rounded-t-lg object-cover object-center"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="tur-1">
                        <div class="p-3">
                            <p class="text-base font-medium">Lokasi</p>
                            <p class="text-lg font-semibold">Nama Tur</p>
                            <div class="mb-3 flex">
                                <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                    width="15px" height="15px">
                                <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12
                                        Ulasan)</span>
                                </p>
                            </div>
                            <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                        </div>
                    </a>
                    <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                        <img class="rounded-t-lg object-cover object-center"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="tur-1">
                        <div class="p-3">
                            <p class="text-base font-medium">Lokasi</p>
                            <p class="text-lg font-semibold">Nama Tur</p>
                            <div class="mb-3 flex">
                                <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                    width="15px" height="15px">
                                <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12
                                        Ulasan)</span>
                                </p>
                            </div>
                            <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                        </div>
                    </a> --}}
                    @endforeach
                    @endif
                    @if ($konfirmasi_by == null && $search != null && $kategori == 'Tur Private' && $tur_to != null && $tgl != null)
                        @foreach ($data_tur as $item)
                            @php
                                $regency = App\Models\Regency::where('id', $item->regency_id)->first();
                                $price = App\Models\Price::where('id', $item->harga_id)->first();
                                $dewasa_residen = $price->dewasa_residen;
                                $dewasa_non_residen = $price->dewasa_non_residen;
                                $harga_min = min($dewasa_residen, $dewasa_non_residen);
                            @endphp
                            @if ($harga_min >= $min_price && $harga_min <= $max_price)
                            <a href="{{ route('tur.show', $item->product->slug) }}" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">{{ $regency->name }}</p>
                                    <p class="text-lg font-semibold">{{ $item->product->product_name }}</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}"
                                            alt="rating" width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12
                                                Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR {{number_format($harga_min)}}</p>
                                </div>
                            </a>
                            @endif
                            {{-- <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">Lokasi</p>
                                    <p class="text-lg font-semibold">Nama Tur</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12 Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                                </div>
                            </a>
                            <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">Lokasi</p>
                                    <p class="text-lg font-semibold">Nama Tur</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12
                                                Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                                </div>
                            </a>
                            <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">Lokasi</p>
                                    <p class="text-lg font-semibold">Nama Tur</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12
                                                Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                                </div>
                            </a>
                            <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">Lokasi</p>
                                    <p class="text-lg font-semibold">Nama Tur</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12
                                                Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                                </div>
                            </a> --}}
                    @endforeach
                    @endif

                    @if ($search != null && $konfirmasi_by != null && $kategori == 'Open Trip' && $tur_to != null && $tgl != null)
                    @foreach ($data_tur as $item)
                    @php
                    $regency = App\Models\Regency::where('id', $item->regency_id)->first();
                    $price = App\Models\Price::where('id', $item->harga_id)->first();
                                $dewasa_residen = $price->dewasa_residen;
                                $dewasa_non_residen = $price->dewasa_non_residen;
                                $harga_min = min($dewasa_residen, $dewasa_non_residen);
                    @endphp
                    @if ($harga_min >= $min_price && $harga_min <= $max_price)
                    <a href="{{ route('tur.show', $item->product->slug) }}"
                        class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                        <img class="rounded-t-lg object-cover object-center"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="tur-1">
                        <div class="p-3">
                            <p class="text-base font-medium">{{ $regency->name }}</p>
                            <p class="text-lg font-semibold">{{ $item->product->product_name }}</p>
                            <div class="mb-3 flex">
                                <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                    width="15px" height="15px">
                                <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12 Ulasan)</span>
                                </p>
                            </div>
                            <p class="text-xl font-semibold text-[#23AEC1]">IDR {{number_format($harga_min)}}</p>
                        </div>
                    </a>
                    @endif
                    {{-- <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                        <img class="rounded-t-lg object-cover object-center"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="tur-1">
                        <div class="p-3">
                            <p class="text-base font-medium">Lokasi</p>
                            <p class="text-lg font-semibold">Nama Tur</p>
                            <div class="mb-3 flex">
                                <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                    width="15px" height="15px">
                                <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12 Ulasan)</span>
                                </p>
                            </div>
                            <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                        </div>
                    </a>
                    <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                        <img class="rounded-t-lg object-cover object-center"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="tur-1">
                        <div class="p-3">
                            <p class="text-base font-medium">Lokasi</p>
                            <p class="text-lg font-semibold">Nama Tur</p>
                            <div class="mb-3 flex">
                                <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                    width="15px" height="15px">
                                <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12
                                        Ulasan)</span>
                                </p>
                            </div>
                            <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                        </div>
                    </a>
                    <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                        <img class="rounded-t-lg object-cover object-center"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="tur-1">
                        <div class="p-3">
                            <p class="text-base font-medium">Lokasi</p>
                            <p class="text-lg font-semibold">Nama Tur</p>
                            <div class="mb-3 flex">
                                <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                    width="15px" height="15px">
                                <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12
                                        Ulasan)</span>
                                </p>
                            </div>
                            <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                        </div>
                    </a>
                    <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                        <img class="rounded-t-lg object-cover object-center"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="tur-1">
                        <div class="p-3">
                            <p class="text-base font-medium">Lokasi</p>
                            <p class="text-lg font-semibold">Nama Tur</p>
                            <div class="mb-3 flex">
                                <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                    width="15px" height="15px">
                                <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12
                                        Ulasan)</span>
                                </p>
                            </div>
                            <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                        </div>
                    </a> --}}
                    @endforeach
                    @endif
                    @if ($konfirmasi_by != null && $search != null && $kategori == 'Tur Private' && $tur_to != null && $tgl != null)
                        @foreach ($data_tur as $item)
                            @php
                                $regency = App\Models\Regency::where('id', $item->regency_id)->first();
                                $price = App\Models\Price::where('id', $item->harga_id)->first();
                                $dewasa_residen = $price->dewasa_residen;
                                $dewasa_non_residen = $price->dewasa_non_residen;
                                $harga_min = min($dewasa_residen, $dewasa_non_residen);
                            @endphp
                            @if ($harga_min >= $min_price && $harga_min <= $max_price)
                            <a href="{{ route('tur.show', $item->product->slug) }}" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">{{ $regency->name }}</p>
                                    <p class="text-lg font-semibold">{{ $item->product->product_name }}</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}"
                                            alt="rating" width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12
                                                Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR {{number_format($harga_min)}}</p>
                                </div>
                            </a>
                            @endif
                            {{-- <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">Lokasi</p>
                                    <p class="text-lg font-semibold">Nama Tur</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12 Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                                </div>
                            </a>
                            <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">Lokasi</p>
                                    <p class="text-lg font-semibold">Nama Tur</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12
                                                Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                                </div>
                            </a>
                            <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">Lokasi</p>
                                    <p class="text-lg font-semibold">Nama Tur</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12
                                                Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                                </div>
                            </a>
                            <a href="/tur/detail" class="rounded-lg shadow-lg hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">Lokasi</p>
                                    <p class="text-lg font-semibold">Nama Tur</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12
                                                Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                                </div>
                            </a> --}}
                    @endforeach
                    @endif
                </div>
                {{-- <div class="flex">
                    <div class="flex flex-col">
                        <div class="flex flex-row">
                            <a href="#" class="rounded-lg shadow-lg flex-1 hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">Lokasi</p>
                                    <p class="text-lg font-semibold">Nama Tur</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12 Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                                </div>
                            </a>
                            <a href="#" class="rounded-lg shadow-lg flex-1 hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">Lokasi</p>
                                    <p class="text-lg font-semibold">Nama Tur</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12 Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                                </div>
                            </a>
                            <a href="#" class="rounded-lg shadow-lg flex-1 hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">Lokasi</p>
                                    <p class="text-lg font-semibold">Nama Tur</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12 Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                                </div>
                            </a>
                        </div>
                        <div class="flex flex-row">
                            <a href="#" class="rounded-lg shadow-lg flex-1 hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">Lokasi</p>
                                    <p class="text-lg font-semibold">Nama Tur</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12 Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                                </div>
                            </a>
                            <a href="#" class="rounded-lg shadow-lg flex-1 hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">Lokasi</p>
                                    <p class="text-lg font-semibold">Nama Tur</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12 Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                                </div>
                            </a>
                            <a href="#" class="rounded-lg shadow-lg flex-1 hover:shadow-xl duration-200">
                                <img class="rounded-t-lg object-cover object-center"
                                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                    alt="tur-1">
                                <div class="p-3">
                                    <p class="text-base font-medium">Lokasi</p>
                                    <p class="text-lg font-semibold">Nama Tur</p>
                                    <div class="mb-3 flex">
                                        <img class="mr-3" src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                            width="15px" height="15px">
                                        <p class="font-bold text-xs">4.7 <span class="text-[#828282]">(12 Ulasan)</span>
                                        </p>
                                    </div>
                                    <p class="text-xl font-semibold text-[#23AEC1]">IDR 100,000</p>
                                </div>
                            </a>
                        </div>
                    </div>

                </div> --}}
            </div>

        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        var turAboutSwiper = new Swiper(".tur-about-swiper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
                enabled: true,
            },
            breakpoints: {
                640: {
                    slidesPerView: 3,
                    navigation: {
                        enabled: false
                    },
                }
            }
        });

        var reviewSwipper = new Swiper(".review-swipper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    </script>
</body>

</html>