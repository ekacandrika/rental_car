<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tur Detail') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <script defer src="https://cdn.jsdelivr.net/npm/@alpinejs/focus@3.x.x/dist/cdn.min.js"></script>
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    {{-- sweet alert --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.min.css" rel="stylesheet">

    <!-- Styles -->
    <style>
        input[type='number']::-webkit-outer-spin-button,
        input[type='number']::-webkit-inner-spin-button,
        input[type='number'] {
            -webkit-appearance: none;
            margin: 0;
            -moz-appearance: textfield !important;
        }

        #social-links ul {
            padding-left: 0;
        }

        #social-links ul li {
            display: inline-block;
        }

        #social-links ul li a {
            padding: 6px;
            /* border: 1px solid #ccc; */
            border-radius: 5px;
            margin: 1px;
            font-size: 25px;
        }

        #social-links .fa-facebook {
            color: #0d6efd;
        }

        #social-links .fa-twitter {
            color: deepskyblue;
        }

        #social-links .fa-linkedin {
            color: #0e76a8;
        }

        #social-links .fa-whatsapp {
            color: #25D366
        }

        #social-links .fa-reddit {
            color: #FF4500;
            ;
        }

        #social-links .fa-telegram {
            color: #0088cc;
        }

        .rate {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rate:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rate:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rate:not(:checked)>label:before {
            content: '★ ';
        }

        .rate>input:checked~label {
            color: #ffc700;
        }

        .rate:not(:checked)>label:hover,
        .rate:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rate>input:checked+label:hover,
        .rate>input:checked+label:hover~label,
        .rate>input:checked~label:hover,
        .rate>input:checked~label:hover~label,
        .rate>label:hover~input:checked~label {
            color: #c59b08;
        }

        .star-rating-complete {
            color: #c59b08;
        }

        .rating-container .form-control:hover,
        .rating-container .form-control:focus {
            background: #fff;
            border: 1px solid #ced4da;
        }

        .rating-container textarea:focus,
        .rating-container input:focus {
            color: #000;
        }

        .rated {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rated:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ffc700;
        }

        .rated:not(:checked)>label:before {
            content: '★ ';
        }

        .rated>input:checked~label {
            color: #ffc700;
        }

        .rated:not(:checked)>label:hover,
        .rated:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rated>input:checked+label:hover,
        .rated>input:checked+label:hover~label,
        .rated>input:checked~label:hover,
        .rated>input:checked~label:hover~label,
        .rated>label:hover~input:checked~label {
            color: #c59b08;
        }

        .maps-embed {
            padding-bottom: 60%
        }

        .maps-embed iframe {
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
            position: absolute;
        }
    </style>
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Gallery Lightbox --}}
    <div class="container mx-auto my-5">
        @if(json_decode($tur_detail['gallery'])!=null)
            @if (count(json_decode($tur_detail['gallery'])) == 1)
            <div class="relative">
                <div class="mb-2 md:mb-0">
                    <img class="object-cover object-center max-h-[426px] md:h-[287px] lg:h-[383px] xl:h-[479px] 2xl:h-[575px] w-full md:max-h-full"
                        src="{{ asset(json_decode($tur_detail['gallery'])[0]) }}" alt="detail-1">
                </div>
            </div>
            @endif
            @if (count(json_decode($tur_detail['gallery'])) > 1)
            <div class="relative md:grid md:grid-cols-2 grid-flow-col gap-2">
                <div class="mb-2 md:mb-0">
                    <img class="object-cover object-center max-h-[426px] md:h-[287px] lg:h-[383px] xl:h-[479px] 2xl:h-[575px] w-full md:max-h-full"
                        src="{{ asset($galleries[0]) }}" alt="detail-1">
                </div>
                <div>
                    <div class="grid grid-cols-2 md:grid-cols-1 md:grid-rows-2 gap-2">
                        <div class="grid grid-cols-2 gap-2">
                            <div>
                                <img class="object-cover object-center h-full md:h-[139px] lg:h-[187px] xl:h-[235px] 2xl:h-[283px] w-full md:max-h-full"
                                    src="{{ asset($galleries[1]) }}" alt="detail-2">
                            </div>
                            <div>
                                <img class="object-cover object-center h-full md:h-[139px] lg:h-[187px] xl:h-[235px] 2xl:h-[283px] w-full md:max-h-full"
                                    src="{{ asset($galleries[2]) }}" alt="detail-3">
                            </div>
                        </div>
                        <div class="grid grid-cols-2 gap-2">
                            <div>
                                <img class="object-cover object-center h-full md:h-[139px] lg:h-[187px] xl:h-[235px] 2xl:h-[283px] w-full md:max-h-full"
                                    src="{{ asset($galleries[3]) }}" alt="detail-4">
                            </div>
                            <div>
                                <img class="object-cover object-center h-full md:h-[139px] lg:h-[187px] xl:h-[235px] 2xl:h-[283px] w-full md:max-h-full"
                                    src="{{ asset($galleries[4]) }}" alt="detail-5">
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div x-data="{ open: false }" @keydown.escape="open = false">
                    <button @click="open = true"
                        class="w-full md:w-fit md:absolute md:bottom-0 md:right-0 md:-translate-x-5 md:-translate-y-5 px-1 md:px-3 py-1 lg:px-5 lg:py-2 text-sm sm:text-base font-semibold border-2 border-gray-300 text-black duration-300 bg-gray-300 rounded-sm hover:bg-gray-100">
                        Semua foto
                    </button>
                    <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                        style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                        <div
                            class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                            <button
                                class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                            </button>
                        </div>
                        <div class="h-full w-full flex items-center justify-center overflow-hidden"
                            x-data="{active: 0, slides: {{ $tur_detail['gallery'] }}}">
                            <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                    <button type="button"
                                        class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === 0 ? slides.length - 1 : active - 1">
                                        <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>

                            @foreach (json_decode($tur_detail['gallery']) as $index=>$gallery)
                            <div class="h-full w-full flex items-center justify-center absolute">
                                <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                    x-show="active === {{ $index }}" x-transition:enter="transition ease-out duration-150"
                                    x-transition:enter-start="opacity-0 transform scale-90"
                                    x-transition:enter-end="opacity-100 transform scale-100"
                                    x-transition:leave="transition ease-in duration-150"
                                    x-transition:leave-start="opacity-100 transform scale-100"
                                    x-transition:leave-end="opacity-0 transform scale-90">

                                    <img src="{{ asset($gallery) }}"
                                        class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                </div>
                                <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                    x-show="active === {{ $index }}">
                                    <span class="w-12 text-right" x-text="{{ $index }} + 1"></span>
                                    <span class="w-4 text-center">/</span>
                                    <span class="w-12 text-left"
                                        x-text="{{ count(json_decode($tur_detail['gallery'])) }}"></span>
                                </div>
                            </div>
                            @endforeach
                            <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-start w-12 md:ml-16">
                                    <button type="button"
                                        class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === slides.length - 1 ? 0 : active + 1">
                                        <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="flex md:absolute md:bottom-0 md:right-0 gap-2">
                    <div x-data="{ open: false }" @keydown.escape="open = false">
                        <button @click="open = true"
                            class="w-full md:w-fit md:-translate-x-5 md:-translate-y-5 px-1 md:px-3 py-1 lg:px-5 lg:py-2 text-sm sm:text-base font-semibold border-2 border-gray-300 text-black duration-300 bg-gray-300 rounded-sm hover:bg-gray-100">
                            Semua foto
                        </button>
                        <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                            style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                            <div
                                class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                                <button
                                    class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                    style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                    <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                                </button>
                            </div>
                            <div class="h-full w-full flex items-center justify-center overflow-hidden"
                                x-data="{active: 0, slides: {{ $tur_detail['gallery'] }}}">
                                <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                    <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                        <button type="button"
                                            class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                            style="background-color: rgba(230, 230, 230, 0.4);"
                                            @click="active = active === 0 ? slides.length - 1 : active - 1">
                                            <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                        </button>
                                    </div>
                                </div>
        
                                @foreach (json_decode($tur_detail['gallery']) as $index=>$gallery)
                                <div class="h-full w-full flex items-center justify-center absolute">
                                    <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                        x-show="active === {{ $index }}" x-transition:enter="transition ease-out duration-150"
                                        x-transition:enter-start="opacity-0 transform scale-90"
                                        x-transition:enter-end="opacity-100 transform scale-100"
                                        x-transition:leave="transition ease-in duration-150"
                                        x-transition:leave-start="opacity-100 transform scale-100"
                                        x-transition:leave-end="opacity-0 transform scale-90">
        
                                        <img src="{{ asset($gallery) }}"
                                            class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                    </div>
                                    <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                        x-show="active === {{ $index }}">
                                        <span class="w-12 text-right" x-text="{{ $index }} + 1"></span>
                                        <span class="w-4 text-center">/</span>
                                        <span class="w-12 text-left"
                                            x-text="{{ count(json_decode($tur_detail['gallery'])) }}"></span>
                                    </div>
                                </div>
                                @endforeach
        
                                {{-- <template x-for="(slide, index) in slides" :key="index"
                                    x-data="{url: window.location.host, protocol: window.location.protocol}">
                                    <div class="h-full w-full flex items-center justify-center absolute">
                                        <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                            x-show="active === index" x-transition:enter="transition ease-out duration-150"
                                            x-transition:enter-start="opacity-0 transform scale-90"
                                            x-transition:enter-end="opacity-100 transform scale-100"
                                            x-transition:leave="transition ease-in duration-150"
                                            x-transition:leave-start="opacity-100 transform scale-100"
                                            x-transition:leave-end="opacity-0 transform scale-90">
        
                                            <img :src="`${protocol}//${url}/${slide.gallery}`"
                                                class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                        </div>
                                        <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                            x-show="active === index">
                                            <span class="w-12 text-right" x-text="index + 1"></span>
                                            <span class="w-4 text-center">/</span>
                                            <span class="w-12 text-left" x-text="slides.length"></span>
                                        </div>
                                    </div>
                                </template> --}}
                                <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                    <div class="flex items-center justify-start w-12 md:ml-16">
                                        <button type="button"
                                            class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                            style="background-color: rgba(230, 230, 230, 0.4);"
                                            @click="active = active === slides.length - 1 ? 0 : active + 1">
                                            <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <button
                            class="relative w-full md:w-fit md:-translate-x-5 md:-translate-y-5 px-1 md:px-3 py-1 lg:px-5 lg:py-2 text-sm sm:text-base font-semibold border-2 border-gray-300 text-black duration-300 bg-gray-300 rounded-sm hover:bg-gray-100">
                            <span class="md:pr-[11px]">
                            {{$like_count}} Favorit 
                            </span>
                            <span>
                                <svg class="absolute md:top-[9px] md:right-0 w-6 h-6 text-white hover:fill-red-500 md:pr-[7px]" id="16" onclick="submitLike2(16)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                    <path d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z"></path>
                                </svg>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            @endif
        @endif
    </div>

    <div>
        {{-- Tabs --}}
        <div class="container mx-auto my-5 sticky bg-white top-0 z-20" x-data="{ active:0,
        tabs:['Ringkasan', 'Itinerary', 'Paket', 'Lokasi', 'Ulasan']}">
            <div class="flex sm:block">
                <ul
                    class="flex justify-start overflow-x-auto text-sm font-medium text-center sm:text-left text-gray-500 dark:text-gray-400 border-b border-[#BDBDBD]">
                    <template x-for="(tab, index) in tabs" :key="index">
                        <li class="pb-2 sm:mt-2" :class="{'border-b-2 border-[#9E3D64]': active == index}"
                            x-on:click="active = index">
                            <a :href="`#`+tab.toLowerCase()">
                                <button
                                    class="inline-block py-1 sm:py-2 px-5 sm:px-9 text-lg sm:text-xl rounded-lg duration-200"
                                    :class="{'text-[#9E3D64] bg-white font-bold': active == index, 'text-black font-normal hover:text-[#9E3D64]': active != index}"
                                    x-text="tab"></button>
                            </a>
                        </li>
                    </template>
                </ul>
            </div>
        </div>

        {{-- Content --}}
        <div class="container mx-auto">
            <div class="grid grid-cols-5 gap-5">
                {{-- Left Side --}}
                <div class="col-span-5 lg:col-span-4 bg-white p-3">
                    {{-- Ringkasan --}}
                    <div id="ringkasan">
                        <p class="text-3xl py-3 font-semibold text-[#333333] whitespace-normal break-words">{{
                            $tur['product_name'] }}
                        </p>
                        <p class="text-xl py-1 font-semibold text-[#333333] whitespace-normal break-words">{{
                            $provinsi->name ?? (isset($provinsi->name) ? $provinsi->name:null) }}
                            {{ (isset($provinsi_2->name) ? ' - '. $provinsi_2->name:null) }}
                            {{isset($tag_location_1) ? $tag_location_1 : '' }} {{ isset($tag_location_2) ? ' - '.
                            $tag_location_2 : '' }} {{
                            isset($tag_location_3) ? ' - '. $tag_location_3 : '' }}
                            {{ isset($tag_location_4) ? ' - '. $tag_location_4 : '' }}</p>
                        <div class="flex py-2 items-center">
                            <img class="mx-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <p class="text-xl font-semibold text-[#333333]">{{ $ratings }} <span class="font-normal">({{
                                    $var2 }} Ulasan)</span></p>
                        </div>
                        <div class="flex flex-wrap my-2 py-1 text-sm">
                            <p class="mt-2 mr-1"><span class="font-semibold">{{ $booking_count }}</span> kali dipesan •
                            </p>
                            <p class="mt-2 mr-1"><span class="font-semibold">{{ $like_count }}</span> orang menyukai ini
                                •</p>
                            <div class="mt-2 flex">
                                <p>Share</p>
                                {!! $share !!}
                            </div>
                        </div>
                        <div class="flex flex-wrap my-2">
                            <a href="#"
                                class="rounded-full flex py-1 px-5 mt-2 mr-3 bg-[#F2F2F2] hover:shadow-md duration-200">
                                <img class="mr-2" src="{{ asset('storage/icons/bookmark-solid 1.svg') }}" alt="type"
                                    width="12px">Tour
                            </a>
                            @if (isset($provinsi))
                            <a href="#"
                                class="rounded-full flex py-1 px-5 mt-2 mr-3 bg-[#F2F2F2] hover:shadow-md duration-200">
                                <img class="mr-2" src="{{ asset('storage/icons/location-dot-solid 1.svg') }}"
                                    alt="location" width="12px">{{ $provinsi->name ?? '' }}
                            </a>
                            @endif
                            @if (isset($regency))
                            <a href="#"
                                class="rounded-full flex py-1 px-5 mt-2 mr-3 bg-[#F2F2F2] hover:shadow-md duration-200">
                                <img class="mr-2" src="{{ asset('storage/icons/location-dot-solid 1.svg') }}"
                                    alt="location" width="12px">{{ $regency->name ?? '' }}
                            </a>
                            @endif
                            @if (isset($tag_location_1))
                            <a href="#"
                                class="rounded-full flex py-1 px-5 mt-2 mr-3 bg-[#F2F2F2] hover:shadow-md duration-200">
                                <img class="mr-2" src="{{ asset('storage/icons/location-dot-solid 1.svg') }}"
                                    alt="location" width="12px">{{ $tag_location_1 ?? '' }}
                            </a>
                            @endif
                            @if (isset($tag_location_2))
                            <a href="#"
                                class="rounded-full flex py-1 px-5 mt-2 mr-3 bg-[#F2F2F2] hover:shadow-md duration-200">
                                <img class="mr-2" src="{{ asset('storage/icons/location-dot-solid 1.svg') }}"
                                    alt="location" width="12px">{{ $tag_location_2 ?? '' }}
                            </a>
                            @endif
                            @if (isset($tag_location_3))
                            <a href="#"
                                class="rounded-full flex py-1 px-5 mt-2 mr-3 bg-[#F2F2F2] hover:shadow-md duration-200">
                                <img class="mr-2" src="{{ asset('storage/icons/location-dot-solid 1.svg') }}"
                                    alt="location" width="12px">{{ $tag_location_3 ?? '' }}
                            </a>
                            @endif
                            @if (isset($tag_location_4))
                            <a href="#"
                                class="rounded-full flex py-1 px-5 mt-2 mr-3 bg-[#F2F2F2] hover:shadow-md duration-200">
                                <img class="mr-2" src="{{ asset('storage/icons/location-dot-solid 1.svg') }}"
                                    alt="location" width="12px">{{ $tag_location_4 ?? '' }}
                            </a>
                            @endif
                            {{-- <a href="#"
                                class="rounded-full flex py-1 px-5 mt-2 bg-[#F2F2F2] hover:shadow-md duration-200">
                                <img class="mr-2" src="{{ asset('storage/icons/tag-solid 1.svg') }}" alt="category"
                                    width="12px">Kategori
                            </a> --}}

                        </div>
                        <div class="flex flex-wrap my-2">
                            @forelse ($tur->tagged as $tag)
                            <a href="#"
                                class="rounded-full flex py-1 px-5 mt-2 mr-3 bg-[#F2F2F2] hover:shadow-md duration-200">
                                <img class="mr-2" src="{{ asset('storage/icons/tag-solid 1.svg') }}" alt="category"
                                    width="12px">{{ $tag->tag_name }}
                            </a>
                            @empty

                            @endforelse
                        </div>
                        <div
                            class="border-y border-gray-200 sm:border-y-0 grid sm:grid-cols-2 md:w-3/4 py-3 space-y-2 sm:space-y-0">
                            <div class="space-y-2">
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/user-solid.svg') }}" alt="user">Tipe Tur: <span
                                        class="px-1">{{
                                        $tur_detail['tipe_tur'] }}</span>
                                </p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/user-group-solid.svg') }}" alt="user-group">Peserta
                                    Min {{ $pakets->min_peserta }} Max {{ $pakets->max_peserta }}</p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/location-dot-solid 1.svg') }}"
                                        alt="location">Berangkat dari: <span class="px-1">{{ $provinsi->name ??
                                        $tag_location_1 ?? $tag_location_2 ?? $tag_location_3 ?? $tag_location_4 ?? ''
                                        }}</span>
                                </p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/hourglass-solid.svg') }}" alt="hourglass">Durasi:
                                    <span class="px-1">{{ $tur_detail['jml_hari'] }} Hari {{ $tur_detail['jml_malam'] }}
                                        Malam</span>
                                </p>
                            </div>
                            <div class="space-y-2">
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/utensils-solid.svg') }}" alt="utensils">Makanan:
                                    <span class="px-1">{{ $tur_detail['makanan'] }}</span>
                                </p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/earth-asia-solid.svg') }}" alt="user">Bahasa:
                                    <span class="px-1">{{ $tur_detail['bahasa'] }}</span>
                                </p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/check-to-slot-solid.svg') }}" alt="slot">Konfirmasi
                                    <span class="px-1">{{ $tur_detail['confirmation'] == 'by_seller' ? 'By Seller' :
                                        $tur_detail['confirmation'] }}</span>
                                </p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/person-hiking-solid.svg') }}" alt="user">Tingkat
                                    Petualangan: <span class="px-1">{{ $tur_detail['tingkat_petualangan'] }}</span></p>
                            </div>
                        </div>
                        <div class="text-justify my-5">
                            <p class="text-3xl my-3 font-semibold">Deskripsi Wisata</p>
                            {!! $tur_detail['deskripsi'] !!}
                        </div>
                    </div>

                    {{-- Itinerary --}}
                    <div id="itinerary" class="py-3">
                        <p class="text-3xl py-3 font-semibold text-[#333333]">Itinerary</p>
                        <div x-data="tabs()" x-init="$nextTick(() => addColor()) ">
                            <ul class="text-sm font-medium text-justify sm:text-left">
                                @foreach (json_decode($itinerary->judul_itenenary) as $index=>$item)
                                <li class="my-2 px-3 rounded-md bg-[#F2F2F2]"
                                    :style="`border-left-width: 8px; border-color:`+tab[{{ $index }}]"
                                    x-data="{ open:false }">
                                    <div class="grid grid-cols-12" :class="{'pt-3 pb-2' : open, 'py-3' : !open}"
                                        x-on:click="open = !open">
                                        <div class="col-span-11 md:grid grid-cols-11">
                                            <button
                                                class="text-base sm:text-lg text-[#4F4F4F] font-semibold col-span-10 w-full text-left rounded-lg whitespace-normal break-words">{{
                                                $item }}</button>
                                        </div>
                                        <div class="w-full">
                                            <img class="float-right duration-200 cursor-pointer"
                                                :class="{'rotate-180' : open}" x-on:click="open = !open"
                                                src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                                alt="chevron-down" width="26px" height="15px">
                                        </div>
                                    </div>
                                    <div class="py-5 md:pl-14 lg:pl-16 xl:pl-20 2xl:pl-24 md:pr-10 lg:pr-12 font-normal text-[#4F4F4F] text-base lg:text-lg xl:text-xl"
                                        x-show="open" x-transition>
                                        <p>{{ json_decode($itinerary->deskripsi_itenenary)[$index] }}</p>
                                        @if ( json_decode($itinerary->gallery_itenenary)[$index] != "")
                                        <img class="my-3 object-cover object-center sm:max-w-md rounded-md"
                                            src="{{ asset(json_decode($itinerary->gallery_itenenary)[$index]) }}"
                                            alt="itinerary-img-{{ $index }}">
                                        @endif

                                        @if ( json_decode($itinerary->tautan_video)[$index] != "")
                                        @php
                                        $video_id = explode("https://www.youtube.com/watch?v=",
                                        json_decode($itinerary->tautan_video)[$index]);
                                        @endphp
                                        {!! json_decode($itinerary->tautan_video)[$index] !!}
                                        {{-- <iframe src="https://www.youtube.com/embed/{{ $video_id[0] }}"
                                            frameborder="0" allowfullscreen></iframe> --}}
                                        @endif
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    {{-- Gallery --}}
                    <div class="my-5 swiper gallery-tur-swiper">
                        <div class="mx-auto py-5 swiper-wrapper">
                            @if (count($galleries) > 0)
                            @foreach ($galleries as $index=>$gallery)
                            <div class="flex justify-center swiper-slide">
                                <div class="w-full h-[321px]">
                                    <img class="w-full h-full rounded-md object-cover object-center"
                                        src="{{ asset($gallery) }}" alt="gallery-tur-{{ $index }}">
                                </div>
                            </div>
                            @endforeach
                            @endif
                            @if(json_decode($tur_detail['gallery'])!=null)
                            @if(count(json_decode($tur_detail['gallery'])) == 1)
                            <div class="flex justify-center swiper-slide">
                                <div class="w-full h-[321px]">
                                    <img class="w-full h-full rounded-md object-cover object-center"
                                        src="{{ asset(json_decode($tur_detail['gallery'])[0]) }}"
                                        alt="gallery-tur-{{ $index }}">
                                </div>
                            </div>
                            @endif
                            @endif
                        </div>
                        <div class="swiper-button-next rounded-full bg-white hover:border border-gray-300 hover:translate-x-1 hover:shadow-md duration-200 p-7"
                            style="--swiper-navigation-color: #9E3D64; --swiper-pagination-color: #9E3D64">
                        </div>
                        <div class="swiper-button-prev rounded-full bg-white hover:border border-gray-300 hover:-translate-x-1 hover:shadow-md duration-200 p-7"
                            style="--swiper-navigation-color: #9E3D64; --swiper-pagination-color: #9E3D64">
                        </div>
                    </div>

                    {{-- Paket Termasuk --}}
                    <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                        x-data="{ open:false }">
                        <div class="flex justify-between" :class=" {'pt-3 pb-2' : open, 'py-3' : !open}">
                            <button
                                class="text-lg sm:text-xl md:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open">Paket Termasuk</button>
                            <img class="float-right duration-200 cursor-pointer" :class="{'rotate-180' : open}"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                            x-show="open" x-transition>
                            {!! $tur_detail['paket_termasuk'] !!}
                        </ul>
                    </div>

                    {{-- Paket Tidak Termasuk --}}
                    <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                        x-data="{ open:false }">
                        <div class="flex justify-between" :class=" {'pt-3 pb-2' : open, 'py-3' : !open}">
                            <button
                                class="text-lg sm:text-xl md:text-2xl  text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open">Paket Tidak Termasuk</button>
                            <img class="float-right duration-200 cursor-pointer" :class="{'rotate-180' : open}"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                            x-show="open" x-transition>
                            {!! $tur_detail['paket_tidak_termasuk'] !!}
                        </ul>
                    </div>
                </div>

                {{-- Right Side --}}
                <div class="col-span-5 lg:col-span-1 order-last lg:order-none" x-data="price()">
                    {{-- Price --}}
                    <form action="{{ route('tur.turOrder', $tur->slug) }}" method="POST"
                        class="block sm:rounded-md shadow-lg p-3 mb-3 bg-white border border-gray-200">
                        @csrf

                        <input type="hidden" name="product_name" value="{{ $tur['product_name'] }}">
                        <input type="hidden" name="product_id" value="{{ $tur['id'] }}">
                        <input type="hidden" name="paket_id" value="{{ $tur_detail['paket_id'] }}">
                        <input type="hidden" name="pilihan_id" value="{{ $tur_detail['pilihan_id'] }}">
                        <input type="hidden" name="toko_id" value="{{ $tur['user_id'] }}">
                        <input type="hidden" name="product_detail_id" value="{{ $tur_detail['id'] }}">
                        <input type="hidden" name="type" value="{{ $tur['type'] }}">
                        <div x-data="{bookmarked: '{{ $like }}'}" class="flex justify-between text-[#333333]">
                            <p class="text-base font-medium">Mulai</p>
                            <button type="button"
                                x-on:click="submitLike2({{$tur->id}}); bookmarked = bookmarked === '0' ? '1' : '0'"
                                class="flex text-sm">
                                <img class="mr-1" x-show="bookmarked === '0'"
                                    src="{{ asset('storage/icons/bookmark-regular 1.svg') }}" alt="bookmark"
                                    width="14px">
                                <img class="mr-1" x-show="bookmarked === '1'"
                                    src="{{ asset('storage/icons/bookmark-solid 1.svg') }}" alt="bookmark" width="14px">
                                Favorit
                            </button>
                        </div>
                        <p class="text-2xl xl:text-3xl text-center py-2 font-bold text-[#23AEC1]">IDR
                            <span x-text="new Intl.NumberFormat().format($store.detail.total_price)"></span>
                        </p>
                        <div class="relative block order-last my-3 sm:my-0">
                            <label for="tur-date-picker" class="form-label inline-block mb-2 text-gray-700">Tanggal
                                Tur</label>
                            <div
                                class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                <input placeholder="Pilih tanggal" type="text" id="tur_date" name="tur_date"
                                    :class="'!m-0 sm:!w-full'" required readonly>
                            </div>
                        </div>
                        {{-- Tambahan --}}
                        @if (count(json_decode($pilihan['judul_pilihan'])) > 0 || $tur_detail['izin_ekstra'] ==
                        'Bolehkan')
                        <p class="my-3">Tambahan</p>
                        @endif
                        <div class="my-3 space-y-3">
                            @if (count(json_decode($pilihan['judul_pilihan'])) > 0)
                            <div>
                                @livewire('tur.modal-pilihan', ["pilihans" => $pilihan])
                            </div>
                            @endif
                            @if ($tur_detail['izin_ekstra'] == 'Bolehkan')
                            <div>
                                @livewire('tur.modal-ekstra')
                            </div>
                            @endif
                        </div>

                        {{-- Jumlah Peserta --}}
                        <div class="my-5" x-data="price()" x-init="sumTotal()">
                            <p class="my-2">Jumlah</p>
                            <div class="flex lg:block 2xl:flex justify-between my-3 lg:my-5">
                                <div>
                                    <p class="text-sm font-medium">Dewasa</p>
                                    <p class="text-xs font-normal text-[#828282]">Di atas 12 tahun</p>
                                </div>
                                <div class="lg:my-3 2xl:my-0 flex lg:justify-center 2xl:justify-end" x-data="{ 
                                        increment() { this.dewasa_count >= {{ $pakets->max_peserta }} ? {{ $pakets->max_peserta }} : this.dewasa_count++ }, 
                                        decrement() { this.dewasa_count <= {{ $pakets->min_peserta }} ? {{ $pakets->min_peserta }} : this.dewasa_count--; 
                                                        this.anak_count = this.anak_count >= this.dewasa_count ? this.dewasa_count : this.anak_count
                                                        this.balita_count = this.balita_count >= this.dewasa_count ? this.dewasa_count : this.balita_count  } 
                                            }">
                                    <button type="button" class="text-3xl font-semibold mx-3"
                                        x-on:click="decrement()">-</button>
                                    <input
                                        class="block w-[50px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        type="number" min="{{ $pakets->min_peserta }}" max="{{ $pakets->max_peserta }}"
                                        step="1" value="1" x-model="dewasa_count" name="dewasa_count">
                                    <button type="button" class="text-3xl font-semibold mx-3"
                                        x-on:click="increment()">+</button>
                                </div>
                            </div>
                            <div class="flex lg:block 2xl:flex justify-between my-3 lg:my-5">
                                <div>
                                    <p class="text-sm font-medium">Anak-anak</p>
                                    <p class="text-xs font-normal text-[#828282]">5 - 12 tahun</p>
                                </div>
                                <div class="lg:my-3 2xl:my-0 flex lg:justify-center 2xl:justify-end"
                                    x-data="{ increment() { this.anak_count >= this.dewasa_count ? this.anak_count : this.anak_count++ }, decrement() { this.anak_count === 0 ? 0 : this.anak_count-- } }">
                                    <button type="button" class="text-3xl font-semibold mx-3"
                                        x-on:click="decrement()">-</button>
                                    <input
                                        class="block w-[50px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        type="number" min="0" :max="dewasa_count" step="1" value="1"
                                        x-model="anak_count" name="anak_count">
                                    <button type="button" class="text-3xl font-semibold mx-3"
                                        x-on:click="increment()">+</button>
                                </div>
                            </div>
                            <div class="flex lg:block 2xl:flex justify-between my-3 lg:my-5">
                                <div>
                                    <p class="text-sm font-medium">Balita</p>
                                    <p class="text-xs font-normal text-[#828282]">Di bawah 5 tahun</p>
                                </div>
                                <div class="lg:my-3 2xl:my-0 flex lg:justify-center 2xl:justify-end"
                                    x-data="{ increment() { this.balita_count >= this.dewasa_count ? this.balita_count : this.balita_count++ }, decrement() { this.balita_count === 0 ? 0 : this.balita_count-- } }">
                                    <button type="button" class="text-3xl font-semibold mx-3"
                                        x-on:click="decrement()">-</button>
                                    <input
                                        class="block w-[50px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        type="number" min="0" :max="dewasa_count" step="1" value="1"
                                        x-model="balita_count" name="balita_count">
                                    <button type="button" class="text-3xl font-semibold mx-3"
                                        x-on:click="increment()">+</button>
                                </div>
                            </div>
                        </div>
                        <div class="flex justify-center">
                            <a href="/tur/detail-private/pemesanan">
                                <x-destindonesia.button-primary text="Pesan Sekarang">@slot('button') Pesan Sekarang
                                    @endslot</x-destindonesia.button-primary>
                            </a>
                        </div>
                    </form>

                    {{-- Store --}}
                    <div class="lg:rounded-md lg:shadow-lg p-3 my-5 bg-white border-y-2 lg:border border-gray-200">
                        {{-- @dump($tur->user) --}}
                        <a href={{route('toko.show',$tur->user->id)}}>
                            <p class="lg:text-2xl xl:text-3xl font-bold text-center text-[#333333]">Kunjungi Toko</p>
                        </a>
                         <div class="flex justify-center py-3">
                            <a href={{route('toko.show',$tur->user->id)}}>
                                <img src="{{ isset($info_toko->logo_toko) ? asset($info_toko->logo_toko) : asset('storage/img/tur-detail-1.png') }}"
                                class="bg-clip-content lg:w-[96px] lg:h-[96px] xl:w-[100px] xl:h-[100px] shadow-xl bg-white rounded-full"
                                alt="store-profile">
                            </a>
                        </div>
                        <p class="pt-3 text-xl font-semibold text-center">{{ isset($info_toko->nama_toko) ? $info_toko->nama_toko : 'Nama Toko'}}</p>
                       <p class="text-lg font-normal text-center pb-2">Bergabung sejak {{ isset($tur->user->created_at)
                            ? date('Y',strtotime($tur->user->created_at)) : '2021'}}</p>
                        <div class="flex justify-center">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                        </div>
                        <div class="flex justify-center">
                            @if(Auth::user())
                            <x-destindonesia.button-primary text="Kirim Pesan">@slot('button') <a
                                    href="{{route('inbox', [$tur->user_id, $tur->type])}}">Kirim Pesan</a> @endslot
                            </x-destindonesia.button-primary>
                            @else
                            <x-destindonesia.button-primary text="Kirim Pesan">@slot('button') <a href="#">Kirim
                                    Pesan</a> @endslot
                            </x-destindonesia.button-primary>
                            @endif
                        </div>
                    </div>

                    {{-- QR Code --}}
                    <div>
                        <p class="text-2xl font-semibold text-center">Kode QR Produk</p>
                        <div class="flex justify-center mt-5">
                            {{-- {{}} --}}
                            <img src="{{"
                                data:image/png;base64,".DNS2D::getBarcodePNG($tur_detail->base_url,'QRCODE')}}"
                            alt="QR-code">
                        </div>
                        <a class="flex justify-center"
                            href="{{route('download.qrCode',['slug'=>$tur->slug,'base64'=>DNS2D::getBarcodePNG($tur_detail->base_url,'QRCODE')])}}"
                            download="QR-code-produk">
                            <x-destindonesia.button-primary text="Unduh Kode QR">@slot('button') Unduh Kode QR @endslot
                            </x-destindonesia.button-primary>
                        </a>
                    </div>



                </div>

                {{-- Content --}}
                <div class="order-1 lg:order-none col-span-5 p-3">
                    <hr class="border border-[#BDBDBD] my-5">

                    {{-- Lokasi --}}
                    <div id="lokasi" class="my-5">
                        <p class="text-3xl py-3 font-semibold text-[#333333]">Lokasi</p>
                        <p class="text-sm font-normal pb-3">
                            {{ isset($regency->name) ? $regency->name .',' : ''}} {{
                            isset($provinsi->name) ? $provinsi->name :null}}
                            {{
                            isset($tag_location_1) ? $tag_location_1 : '' }} {{ isset($tag_location_2) ? ' - '.
                            $tag_location_2 : '' }} {{
                            isset($tag_location_3) ? ' - '. $tag_location_3 : '' }}
                            {{ isset($tag_location_4) ? ' - '. $tag_location_4 : '' }}</p>
                        <div class="overflow-hidden relative maps-embed">
                            {!! $tur_detail['link_maps'] !!}
                        </div>
                    </div>

                    {{--Perlu Diketahui --}}
                    <p class="text-3xl py-3 font-semibold text-[#333333]">Perlu Diketahui</p>
                    {{-- Catatan --}}
                    <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                        x-data="{ open:false }">
                        <div class="flex justify-between" :class=" {'pt-3 pb-2' : open, 'py-3' : !open}">
                            <button
                                class="text-lg sm:text-xl md:text-2xl  text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open">Catatan</button>
                            <img class="float-right duration-200 cursor-pointer" :class="{'rotate-180' : open}"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        <div class="py-5 px-3 text-[#4F4F4F] text-base sm:text-lg md:text-xl" x-show="open"
                            x-transition>
                            @if (isset($tur_detail['catatan']))
                            {!! $tur_detail['catatan'] !!}
                            @else
                            <p class="text-base sm:text-lg md:text-xl">Tidak ada Catatan.</p>
                            @endif
                        </div>
                    </div>

                    {{-- Kebijakan Pembatalan --}}
                    <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                        x-data="{ open:false }">
                        <div class="flex justify-between" :class=" {'pt-3 pb-2' : open, 'py-3' : !open}">
                            <button
                                class="text-lg sm:text-xl md:text-2xl  text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open">Kebijakan Pembatalan</button>
                            <img class="float-right duration-200 cursor-pointer" :class="{'rotate-180' : open}"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        <div class="py-5 px-3 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl" x-show="open"
                            x-transition>

                            @if (isset($tur_detail['kebijakan_pembatalan_sebelumnya']))
                            @foreach (json_decode($tur_detail['kebijakan_pembatalan_sebelumnya']) as $key=>$kps)
                            <p class="py-3">Pembatalan <strong>{{$kps}} hari</strong> sampai
                                <strong>{{json_decode($tur_detail['kebijakan_pembatalan_sesudah'])[$key]}}
                                    hari</strong>
                                sebelumnya potongan
                                <strong>{{json_decode($tur_detail['kebijakan_pembatalan_potongan'])[$key]}}%.</strong>
                            </p>
                            @endforeach
                            {{-- {!! $activity_detail['kebijakan_pembatalan_sebelumnya'] !!} --}}
                            @else
                            <p class="text-base sm:text-lg md:text-xl">Tidak ada kebijakan pembatalan.</p>
                            @endif
                        </div>
                    </div>

                    {{-- Sering Ditanyakan (FAQ) --}}
                    <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                        x-data="{ open:false }">
                        <div class="flex justify-between" :class=" {'pt-3 pb-2' : open, 'py-3' : !open}">
                            <button
                                class="text-lg sm:text-xl md:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open">Sering Ditanyakan (FAQ)</button>
                            <img class="float-right duration-200 cursor-pointer" :class="{'rotate-180' : open}"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        <div class="py-5 px-3 text-[#4F4F4F] text-base sm:text-lg md:text-xl" x-show="open"
                            x-transition>
                            @if (isset($tur_detail['faq']))
                            {!! $tur_detail['faq'] !!}
                            @else
                            <p class="text-base sm:text-lg md:text-xl">Tidak ada FAQ.</p>
                            @endif
                        </div>
                    </div>

                    {{-- Ulasan --}}
                    <div id="ulasan" class="my-5">
                        <div class="container mx-auto">
                            @if(isset($reviews->star_rating))
                            <div class="container">
                                <div class="row">
                                    <div class="col mt-4">
                                        <p class="font-weight-bold ">Review</p>
                                        <div class="form-group row">
                                            <input type="hidden" name="detailObjek_id" value="{{ $reviews->id }}">
                                            <div class="col">
                                                <div class="rated">
                                                    @for($i=1; $i<=$reviews->star_rating; $i++)
                                                        <input type="radio" id="star{{$i}}" class="rate" name="rating"
                                                            value="5" />
                                                        <label class="star-rating-complete" title="text">{{$i}}
                                                            stars</label>
                                                        @endfor
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row mt-4">
                                            <div class="col">
                                                <p>{{ $reviews->comments }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="container">
                                <div class="row">
                                </div>
                            </div>
                            <div>
                                <div class="container max-w-6xl p-4 ">

                                    <div class="flex mb-4">
                                        <div class="col mt-4 w-full">
                                            <form class="py-2 px-4 w-full" action="{{route('reviewProduct.store')}}"
                                                style="box-shadow: 0 0 10px 0 #ddd;" method="POST" autocomplete="off" enctype="multipart/form-data" id="form-tur-private">
                                                @csrf
                                                <p class="font-weight-bold ">Ulasan</p>
                                                <div class="form-group row">
                                                    <input type="hidden" name="product_id" value="{{ $tur->id }}">
                                                    <div class="col">
                                                        <div class="rate">
                                                            <input type="radio" id="star5" class="rate" name="rating"
                                                                value="5" />
                                                            <label for="star5" title="text">5 stars</label>
                                                            <input type="radio" checked id="star4" class="rate"
                                                                name="rating" value="4" />
                                                            <label for="star4" title="text">4 stars</label>
                                                            <input type="radio" id="star3" class="rate" name="rating"
                                                                value="3" />
                                                            <label for="star3" title="text">3 stars</label>
                                                            <input type="radio" id="star2" class="rate" name="rating"
                                                                value="2">
                                                            <label for="star2" title="text">2 stars</label>
                                                            <input type="radio" id="star1" class="rate" name="rating"
                                                                value="1" />
                                                            <label for="star1" title="text">1 star</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row mt-4" x-data="imageUpload">
                                                    <div class="col">
                                                    {{-- 01040900471 --}}
                                                        <textarea
                                                            class=" form-control w-full flex-auto block p-4 font-medium border border-transparent rounded-lg outline-none focus:border-[#9E3D64] focus:text-green-500"
                                                            name="comment" rows="6 " placeholder="Berikan Ulasan Anda"
                                                            maxlength="200" id="comments">
                                                        </textarea>
                                                        {{-- <input class="pt-4" type="file" accept="image/*" @change="selectedFile" name="images[]" --}}
                                                        <input type="file" name="images[]" class="form-control w-full flex-auto block border border-transparent outline-none focus:border-[#9E3D64] focus:text-green-500" accept="image/*" @change="selectedFile" multiple>
                                                        <template x-if="imgDetail.length >= 1">
                                                            <div class="flex justify-start items-center mt-2">
                                                                <template x-for="(detail,index) in imgDetail" :key="index">
                                                                    <div class="flex justify-center items-center">
                                                                        <img :src="detail"
                                                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                                            :alt="'upload'+index">
                                                                        <button type="button"
                                                                            class="absolute mx-2 translate-x-12 -translate-y-14">
                                                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                                                alt="" width="25px" @click="removeImage(index)">
                                                                        </button>
                                                                    </div>
                                                                </template>
                                                            </div>
                                                        </template>
                                                    </div>
                                                </div>
                                                <div class="mt-3 text-right">
                                                    <button
                                                        class="py-2 px-3 rounded-lg bg-kamtuu-second text-white">Submit
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-2 p-5">
                                        <div class="flex">
                                            <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                class="lg:w-[48px] lg:h-[48px] mt-[0.2rem] mr-1 inline-flex"
                                                alt="rating" title="Rating">
                                            <p class="flex"><span
                                                    class="lg:text-[50px] font-semibold ">{{$ratings}}</span> <span
                                                    class="lg:text-[20px] lg:pt-9"> /5.0 dari {{$var2}} ulasan</span>
                                            </p>
                                        </div>
                                    </div>

                                    {{-- ulasan desktop --}}
                                    @foreach($reviews as $review)
                                    <div class="hidden lg:block md:block">
                                        {{-- <x-produk.card-reviews :review="$review"></x-produk.card-reviews> --}}
                                        <div class="grid grid-row-5 rounded-lg shadow-lg justify-items-start mb-4">
                                            {{-- <div class="grid grid-row-5 rounded-lg shadow-lg justify-items-start py-5"> --}}
                                                <div class="grid grid:row-2 grid-cols-10 gap-2 pl-3 mt-7">
                                                    @if(isset($review->profile_photo_path))
                                                    <img class="w-20 h-20 rounded-full bg-gray-400" scr="{{isset($review->profile_photo_path) ? asset($review->profile_photo_path):null}}">
                                                    @else
                                                    <div class="w-20 h-20 rounded-full bg-gray-400 relative">
                                                        @php
                                                            $name = $review->first_name;
                                                            $exp_name = explode(' ',$name);
                                                            $inisial ="";

                                                            $inisial = substr($exp_name[0],0,1);

                                                            if(count($exp_name) > 1){
                                                                $inisial .=substr(end($exp_name),0,1);
                                                            }
                                                        @endphp
                                                        @if(count($exp_name) > 1)
                                                        <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 13px;">{{$inisial}}</p>
                                                        @else
                                                        <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 26px;">{{$inisial}}</p>
                                                        @endif
                                                    </div>
                                                    @endif
                                                    <div class="col-span-2 my-auto">
                                                        <p class="font-semibold text-gray-900">{{$review->first_name}}<p/>
                                                        {{-- Carbon::parse($p->created_at)->diffForHumans(); --}}
                                                        <p class="font-light text-gray-600 text-xs">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}<p/>
                                                    </div>
                                                </div>
                                                <div class="grid grid-cols-5 pl-3 mb-2">
                                                    <div class="col-span-5">
                                                        <img class="w-[20px] h-[20] mt-[0.3rem] mr-1  ml-3 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                                        @for ($i = 0; $i < ($review->star_rating-1); $i++)
                                                            <img class="w-[20px] h-[20] mt-[0.3rem] mr-1 -ml-2 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                                        @endfor
                                                    </div>
                                                </div>
                                                <div class="grid grid-cols-5 space-y-5 text-justify pl-3 pr-5">
                                                    <div class="col-span-5 pl-3">
                                                    <p id="text-ulasan">{{$review->comments}}</p>
                                                    </div>
                                                </div>
                                                <div class="mt-3 pl-3">
                                                    <button type="button" id="show-btn-ulasan" class="px-3 py-2 text-black text-sm underline">Tampilkan</button>
                                                    <button type="button" id="hide-btn-ulasan" class="px-3 py-2 text-black text-sm underline hidden">Sembunyikan</button>
                                                </div>
                                                @if($review->gallery_post)
                                                <div class="flex justify-start justify-items-center pl-4 pb-4">
                                                    @php
                                                        $post_gallery = json_decode($review->gallery_post, true);
                                                        //dump($post_gallery);
                                                    @endphp
                                                    <img class="w-50 h-40 bg-slate-400 rounded-lg" src="{{$post_gallery['result'] != null ? Storage::url('gallery_post/'.$post_gallery['result'][0]):'https://via.placeholder.com/540x540'}}">
                                                    <div class="relative" x-data="{open:false}" @keydown.escape="open = false">
                                                        <button @click="open=true" type="button" id="hide-btn-foto-ulasan" 
                                                            class="absolute px-3 py-2 text-black text-sm underline" style="width:169px;top:110px">
                                                            Tampilkan semua foto
                                                        </button>
                                                        <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                                                                    style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                                                                    <div
                                                                        class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                                                                        <button
                                                                            class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                                                            style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                                                            <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                                                                        </button>
                                                                    </div>
                                                                    <div class="h-full w-full flex items-center justify-center overflow-hidden"
                                                                        x-data="{active: 0, slides: {{ ($review->gallery_post) }} }">
                                                                        <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                                                            <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                                                                <button type="button"
                                                                                    class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                                                    style="background-color: rgba(230, 230, 230, 0.4);"
                                                                                    @click="active = active === 0 ? slides.length - 1 : active - 1">
                                                                                    <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                        @if ($post_gallery)
                                                                            @foreach ($post_gallery['result'] as $index=>$gallery)
                                                                                <div class="h-full w-full flex items-center justify-center absolute">
                                                                                    <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                                                                        x-show="active === {{ $index }}"
                                                                                        x-transition:enter="transition ease-out duration-150"
                                                                                        x-transition:enter-start="opacity-0 transform scale-90"
                                                                                        x-transition:enter-end="opacity-100 transform scale-100"
                                                                                        x-transition:leave="transition ease-in duration-150"
                                                                                        x-transition:leave-start="opacity-100 transform scale-100"
                                                                                        x-transition:leave-end="opacity-0 transform scale-90">

                                                                                        <img src="{{ Storage::url('gallery_post/'.$gallery) }}"
                                                                                            class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                                                                    </div>
                                                                                    <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                                                                        x-show="active === {{ $index }}">
                                                                                        <span class="w-12 text-right" x-text="{{ $index }} + 1"></span>
                                                                                        <span class="w-4 text-center">/</span>
                                                                                        <span class="w-12 text-left"
                                                                                            x-text="{{ count($post_gallery['result']) }}"></span>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        @endif
                                                                        
                                                                        <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                                                            <div class="flex items-center justify-start w-12 md:ml-16">
                                                                                <button type="button"
                                                                                    class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                                                    style="background-color: rgba(230, 230, 230, 0.4);"
                                                                                    @click="active = active === slides.length - 1 ? 0 : active + 1">
                                                                                    <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                    </div>
                                                </div>
                                                @endif
                                            {{-- </div> --}}
                                            {{-- <div class="grid p-5 justify-items-center">
                                                <p class="text-[18px] font-bold">Jhonny Wilson</p>
                                                <p class="text-[18px] -mt-2">{{$review->created_at->format('j F, Y')}}
                                                </p>
                                                <div class="flex p-2">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex pt-3"><span
                                                            class="text-[16px] font-semibold ">{{$review->star_rating}}.0</span>
                                                        <span class="text-[16px]"> /5.0</span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div
                                                class="grid justify-start justify-items-center lg:-ml-[15rem] xl:-ml-[23rem] p-5 md:-ml-[10rem]">
                                                <p class="text-[18px]">{{ $review->comments }}</p>
                                            </div> --}}
                                        </div>
                                    </div>

                                    {{-- ulasan mobile --}}
                                    <div class="block lg:hidden md:hidden">
                                        <div
                                            class="grid grid-cols-1 divide-y rounded-lg shadow-xl justify-items-center border border-gray-300">
                                            <div class="grid p-5 justify-items-center">
                                                <p class="text-sm md:text-[18px] font-bold my-3">{{$review->frist_name}}</p>
                                                <p class="text-sm md:text-[18px] -mt-2">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}</p>
                                                <div class="flex p-2">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex pt-3"><span
                                                            class="text-[16px] font-semibold ">{{$review->star_rating}}</span>
                                                        <span class="text-[16px]"> /5.0</span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="grid justify-start justify-items-center p-5">
                                                <p class="text-sm md:text-[18px]"> {{ $review->comments }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                    {{-- Anda Mungkin Suka --}}
                    <div>
                        <div class="sm:flex justify-between">
                            <p class="text-2xl sm:text-3xl font-bold text-[#D50006]">Anda Mungkin Suka</p>
                            <a href="{{ route('tur.index') }}" class="my-2 text-xl font-bold text-[#9E3D64]">Lihat
                                semua</a>
                        </div>
                        <div class="my-2 sm:my-5 swiper carousel-tur-swiper">
                            <div class="mx-auto py-5 swiper-wrapper">
                                @if (json_decode($card_tur) != null)
                                @foreach (json_decode($card_tur) as $card)
                                <div class="flex justify-center swiper-slide">
                                    <x-produk.card-populer-home :act="$card" :type="'tur'" />
                                </div>
                                @endforeach
                                @else
                                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                                    Produk tidak ditemukan
                                </div>
                                @endif
                            </div>
                            <div class="-mx-2 swiper-button-next rounded-full bg-white hover:border hover:-translate-x-1 hover:shadow-md duration-200 p-7 border border-[#9E3D64]"
                                style="--swiper-navigation-color: #9E3D64; --swiper-pagination-color: #9E3D64"></div>
                            <div class="-mx-2 swiper-button-prev rounded-full bg-white hover:border hover:-translate-x-1 hover:shadow-md duration-200 p-7 border border-[#9E3D64]"
                                style="--swiper-navigation-color: #9E3D64; --swiper-pagination-color: #9E3D64"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    @livewireScripts
    <script>
        @if(!empty($diskon['tgl_start'][0]) && !empty($diskon['tgl_end'][0]))
            var tgl_start = JSON.parse("{{ $diskon['tgl_start'] }}".replace(/&quot;/g,'"'));
            var tgl_end = JSON.parse("{{ $diskon['tgl_end'] }}".replace(/&quot;/g,'"'));
            var disabledDates = [];
            var jml_tgl = tgl_start.length;
            for(var i = 0; i < jml_tgl; i++) {
                var start = new Date(tgl_start[i]);
                var end = new Date(tgl_end[i]);
                var date = new Date(start);
                while (date <= end) {
                    disabledDates.push(date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2));
                    date.setDate(date.getDate() + 1);
                }
            }
            today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            $('#tur_date').datepicker({
                minDate: today,
                iconsLibrary: 'fontawesome',
                disableDates: function (date) {
                    var formattedDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                    return ($.inArray(formattedDate, disabledDates) != -1);
                }
            });
        @else
            $(document).ready(function() {
                today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                $('#tur_date').datepicker({
                    minDate: today
                });
            })
        @endif
        $(document).ready(function(){
            
            function submitValidate(e, msg){
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'error',
                        title: msg
                    })
                e.preventDefault()
            }

            $('#form-tur-private').on("submit",function(e){
                let role="{{auth()->user() != null ? auth()->user()->role:null}}";
                if(!role){
                    submitValidate(e, 'Silahkan login terlebih dahulu')
                }
                
                if(role!='traveller'){
                        submitValidate(e, 'Silahkan login terlebih dahulu')
                    }

                if(!$("#comments").val()){
                    submitValidate(e, 'Isi komentar tidak kosong')
                }
                //e.preventDefault();
            })
        });
        
        // $(document).ready(function() {
        //     today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        //     $('#tur_date').datepicker({
        //         minDate: today
        //     });
        // })
        
        document.addEventListener("alpine:init", () => {
            Alpine.store("detail", {
                    total_price: 0,
                    dewasa_price: {!! ($harga['dewasa_residen'] <= $harga['dewasa_non_residen']) ? $harga['dewasa_residen'] : $harga['dewasa_non_residen'] !!},
                    anak_price: {!! ($harga['anak_residen'] <= $harga['anak_non_residen']) ? $harga['anak_residen'] : $harga['anak_non_residen'] !!},
                    balita_price: {!! ($harga['balita_residen'] <= $harga['balita_non_residen']) ? $harga['balita_residen'] : $harga['balita_non_residen'] !!},
                    total_count: 0
            });
        });
  

        function price() {
            return {
                dewasa_count: 0,
                anak_count: 0,
                balita_count: 0,
                sumTotal() {
                    this.$watch('dewasa_count; anak_count; balita_count', () => {
                        Alpine.store("detail").total_price = this.dewasa_count * Alpine.store("detail").dewasa_price + this.anak_count * Alpine.store("detail").anak_price + this.balita_count * Alpine.store("detail").balita_price
                        Alpine.store("detail").total_count = this.dewasa_count + this.anak_count + this.balita_count
                    })
                },
            }
        }

        function tabs() {
            return {
                sum_itinerary: {{ count(json_decode($itinerary->judul_itenenary)) }},
                tab: [],
                addColor() {
                    let i = 0
                    while(i < this.sum_itinerary){
                        this.tab.push('#'+Math.floor(Math.random()*16777215).toString(16))
                        i++
                    }
                }
            }
        }

        function submitLike2(id){
            $.ajax({
                url:'/like',
                method:'POST',
                dataType:'json',
                cache:false,
                data:{
                    '_token':$("meta[name='csrf-token']").attr("content"),
                    'product_id':id
                },
                success(resp){
                    if(resp.success){
                        if(resp.data.status==1){
                            $("."+id).addClass("fill-red-500"); 
                            $("#"+id).addClass("hover:fill-black"); 
                        }else{
                            $("."+id).addClass("hover:fill-red-500"); 
                            $("."+id).removeClass("fill-red-500"); 
                            $("."+id).addClass("text-white"); 
                        }
                    }else{
                        // console.log(resp.message)
                        // $(".dialog").dialog();
                        alert(resp.message);
                    }
                },
                failure(resp){
                    console.log('error')
                }
            });
        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function imageUpload(){
            return{
                imageUrl:'',
                imgDetail:[],
                selectedFile(event){
                    this.fileToUrl(event)
                },
                fileToUrl(event){
                    if (!event.target.files.length) return
                    let file = event.target.files
                    
                    for (let i = 0; i < file.length; i++) {

                        let reader = new FileReader();
                        let srcImg = ''
                        this.imgDetail = []

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.imgDetail = [...this.imgDetail, srcImg]
                        };
                    }

                    //this.fileUpload();
                },
                removeImage(index){
                    this.imgDetail.splice(index, 1)
                },
                fileUpload(){
                    let fd = new FormData()
                    
                    console.log(file)

                    for(const file of this.imgDetail){
                        console.log(file)
                        /*
                        if(typeof file === 'object'){
                            fd.append('gallery_post[]',file, file.name)
                        }
                        if(typeof file === 'string'){
                            fd.append('seved_post_gallery',file)
                        }
                        */
                    }

                    /*
                    $.ajaxSetup({
                        headers:{
                            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        method:'POST',
                        url:"{{route('gallery-post')}}",
                        data:fd,
                        processData:false,
                        contentType:false,
                        beforeSend:function(){

                        },
                        success:function(msg){
                            console.log(msg)
                        },
                        error:function(data){

                        }
                    })
                    */
                }
            }
        }

        var galleryTurSwiper = new Swiper(".gallery-tur-swiper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
        },});

        var carouselTurSwiper = new Swiper(".carousel-tur-swiper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            breakpoints: {
                768: {
                    slidesPerView: 3,
                },
                1280: {
                    slidesPerView: 5,
                },
            }
        });
        window.addEventListener('content-updated', event => {
        window.mySwiper = new Swiper(".mySwiper", {
            loop: true,
            spaceBetween: 10,
            slidesPerView: 5,
            freeMode: true,
            watchSlidesProgress: true,
            watchSlidesVisibility: true,
            // slideToClickedSlide: true,
        });

        window.mySwiper2 = new Swiper(".mySwiper2", {
            loop: true,
            spaceBetween: 10,
            loopedSlides: 5,
            navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev"
            },
            thumbs: {
            swiper: mySwiper
            }
        });

        // mySwiper.controller.control = swiper2;
        // swiper2.controller.control = mySwiper;
    })

    </script>
</body>

</html>