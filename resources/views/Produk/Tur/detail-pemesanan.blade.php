<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tur Detail') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    <style>
        input[type='number']::-webkit-outer-spin-button,
        input[type='number']::-webkit-inner-spin-button,
        input[type='number'] {
            -webkit-appearance: none;
            margin: 0;
            -moz-appearance: textfield !important;
        }
    </style>
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border border-b-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    <div class="container mx-auto my-5">
        <div class="grid grid-cols-6 lg:gap-5">
            {{-- Mobile Information --}}
            <div class="col-span-6 p-3">
                <div class="block lg:hidden border border-gray-200 rounded-md shadow-lg p-5">
                    <div class="flex">
                        <img class="rounded-md object-cover object-center shadow-md mr-5 w-20 h-20"
                            src="{{ isset($seller->productdetail->thumbnail) ? asset($seller->productdetail->thumbnail) : asset('storage/img/tur-detail-5.jpg') }}"
                            alt="tur-img">
                        <div>
                            <p class="text-base md:text-lg lg:text-xl font-semibold">{{ $order['product_name'] }}</p>
                            <p class="text-sm md:text-base font-normal">{{ $seller->user->first_name }}</p>
                        </div>
                    </div>
                    <div class="mt-5 text-sm sm:text-base">
                        <p>{{ $formated_date }}</p>
                        <p>{{ $seller->productdetail->tipe_tur }}</p>
                        <p>{{ $order['dewasa_count'] }} Dewasa</p>
                        @if ( $order['anak_count'] > 0 )
                        <p>{{ $order['anak_count'] }} Anak</p>
                        @endif
                        @if ( $order['balita_count'] > 0 )
                        <p>{{ $order['balita_count'] }} Balita</p>
                        @endif
                    </div>
                </div>
            </div>
            {{-- Left Side --}}
            <form action="{{ route('booking.store.tour') }}" method="POST" class="col-span-6 lg:col-span-4 bg-white p-3"
                enctype="multipart/form-data" x-data="price()">
                @csrf

                {{-- Peserta 1 --}}
                <div class="rounded-md shadow-sm border bg-slate-100 border-gray-200 py-5 px-10 mb-5"
                    x-data="{ tur_form: { first_name:'', last_name:'', birth_date:'', jenis_kelamin:'Laki-Laki', residen:false, non_residen:false, nationality:'indonesia', country_code:'62', phone_number:'', email:'', residen_status:'residen', kitas:'', same_as_user:false } }">
                    <div class="block sm:flex sm:justify-between my-2">
                        <p class="text-2xl font-semibold">Detail Peserta</p>
                        <p class="text-sm font-medium">ID Booking: 12NJAFB731</p>
                    </div>
                    <div class="block sm:flex sm:justify-between my-5 sm:my-2">
                        <p class="text-base font-semibold">Peserta 1: <span class="text-xs font-medium">{{
                                $order['product_name'] }}</span>
                        </p>
                        <button type="button" class="text-[#D50006] text-sm font-medium"
                            x-on:click="tur_form.same_as_user = false; tur_form.first_name=''; tur_form.last_name=''; tur_form.birth_date=''; tur_form.jenis_kelamin='Laki-Laki'; tur_form.residen=false; tur_form.non_residen=false; tur_form.nationality=''; tur_form.country_code='+62'; tur_form.phone_number=''; tur_form.email=''; tur_form.residen_status='residen'; tur_form.kitas=''">Hapus
                            Semua</button>
                    </div>
                    <div class="my-5 flex items-center">
                        @php
                        // dd($data_booking);
                        if ($data_booking['akun_booking_status'] == 1) {
                        $newDate = Carbon\Carbon::createFromFormat('Y-m-d',
                        $data_booking['date_of_birth'])->format('m/d/Y');
                        }
                        @endphp
                        <input class="rounded-sm mr-2" type="checkbox" name="identity" id="identity"
                            x-model="tur_form.same_as_user" x-init="$watch('tur_form.same_as_user', (isChecked) => {
                                if(isChecked && {{ isset($data_booking) }}){
                                    tur_form.first_name='{{ $data_booking['first_name_booking'] ?? $data_user->first_name }}'; 
                                    tur_form.last_name='{{ $data_booking['last_name_booking'] ?? $data_booking['last_name'] }}'; 
                                    tur_form.birth_date='{{ $data_booking['akun_booking_status'] == 1 ? $newDate : '' }}'; 
                                    tur_form.jenis_kelamin='{{ $data_booking['gender'] }}'; 
                                    tur_form.residen=true; 
                                    tur_form.non_residen=false; 
                                    tur_form.nationality='{{ $data_booking['citizenship'] }}'; 
                                    tur_form.country_code='+60'; 
                                    tur_form.phone_number='{{ $data_booking['phone_number_booking'] ?? $data_user->no_tlp }}'; 
                                    tur_form.email='{{ $data_booking['email_booking'] ?? $data_user->email }}'; 
                                    tur_form.residen_status='{{ $data_booking['status_residen'] }}'
                                } else {
                                    tur_form.first_name=''; 
                                    tur_form.last_name=''; 
                                    tur_form.birth_date=''; 
                                    tur_form.jenis_kelamin='Laki-Laki'; 
                                    tur_form.residen=false; 
                                    tur_form.non_residen=false; 
                                    tur_form.nationality='indonesia'; 
                                    tur_form.country_code='+62'; 
                                    tur_form.phone_number=''; 
                                    tur_form.email=''; 
                                    tur_form.residen_status='residen'; 
                                    tur_form.kitas=''
                                }
                            })">
                        <label for="identity" class="text-sm sm:text-base text-black">Sama seperti
                            pemilik akun</label>
                    </div>

                    {{-- Name --}}
                    <div class="grid grid-rows-1 my-2">
                        <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                            <div class="relative block">
                                <label for="first_name"
                                    class="form-label inline-block mb-2 text-gray-700 text-base font-medium">Nama Depan
                                    & Tengah</label>
                                <input
                                    class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                    placeholder="Nama depan" type="text" name="first_name[]" id="first_name"
                                    x-model="tur_form.first_name" />
                                <input type="hidden" name="kategori_peserta[]" value="dewasa">
                            </div>
                            <div class="relative block">
                                <label for="last_name"
                                    class="form-label inline-block mb-2 text-gray-700 text-base font-medium">Nama
                                    Belakang</label>
                                <input
                                    class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                    placeholder="Nama belakang" type="text" name="last_name[]" id="last_name"
                                    x-model="tur_form.last_name" />
                            </div>
                        </div>
                    </div>

                    {{-- Identity --}}
                    <div class="grid grid-rows-1 my-2">
                        <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                            <div class="relative block">
                                <label for="birth_date" class="form-label inline-block mb-2 text-gray-700">Tanggal
                                    Lahir</label>
                                <div
                                    class="placeholder:text-[#BDBDBD] block bg-white w-full md:w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                    <input :class="'!m-0 sm:!w-full'" type="text" name="birth_date" []
                                        id="birth_date_dewasa0" x-model="tur_form.birth_date" />
                                </div>

                            </div>

                            <div class="relative block">
                                <label for="jenis_kelamin" class="form-label inline-block mb-2 text-gray-700">Jenis
                                    Kelamin</label>
                                <div class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                    <select
                                        class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                        name="jenis_kelamin[]" id="jenis_kelamin" x-model="tur_form.jenis_kelamin">
                                        <option selected value="Laki-Laki">Pria</option>
                                        <option value="Perempuan">Wanita</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="sm:grid sm:grid-cols-2 gap-5">
                        <div>

                            {{-- Residen - Non Residen --}}
                            {{-- <div class="sm:flex my-2 space-y-2 sm:space-y-0">
                                <div class="flex items-center mr-5">
                                    <input class="rounded-sm form-checkbox mr-2" type="checkbox" name="residen"
                                        id="residen" x-model="tur_form.residen">
                                    <label for="residen" class="text-black">Residen</label>
                                </div>
                                <div class="flex items-center">
                                    <input class="rounded-sm form-checkbox mr-2" type="checkbox" name="non-residen"
                                        id="non-residen" x-model="tur_form.non_residen">
                                    <label for="non_residen" class="text-black">Non Residen</label>
                                </div>
                            </div> --}}

                            {{-- Nationality --}}
                            <div class="relative block my-2">
                                <label for="nationality"
                                    class="form-label inline-block mb-2 text-gray-700">Kebangsaan</label>
                                <div class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                    <select
                                        class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                        name="nationality[]" id="nationality" x-model="tur_form.nationality">
                                        <option selected value="indonesia">Indonesia</option>
                                        <option value="other">Other</option>
                                    </select>
                                </div>
                            </div>

                            {{-- Phone Number --}}
                            <div class="relative block my-2">
                                <label for=" phone-number" class="form-label inline-block mb-2 text-gray-700">No.
                                    HP/WhatsApp</label>
                                <div class="flex">
                                    <template x-if="tur_form.nationality == 'indonesia'">
                                        <input
                                            class="border-[#828282] rounded-l-md shadow-sm sm:text-sm w-[50px] focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-l-md bg-white"
                                            type="text" readonly value="62" />
                                    </template>
                                    <input :class="tur_form.nationality == 'indonesia' ? 'rounded-r-md' : 'rounded-md'"
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="81234567890" type="text" name="phone_number[]" id="phone_number"
                                        x-model="tur_form.phone_number" />
                                </div>
                                <template x-if="tur_form.nationality == 'other'">
                                    <p class="py-2 text-xs text-kamtuu-primary">Please write phone number with your
                                        country
                                        code.</p>
                                </template>
                            </div>

                            {{-- Email --}}
                            <div class="relative block my-2">
                                <label for=" email" class="form-label inline-block mb-2 text-gray-700">Email</label>
                                <input
                                    class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                    placeholder="emailku@gmail.com" type="text" name="email[]" id="email"
                                    x-model="tur_form.email" />
                            </div>

                            {{-- Residen --}}
                            <div class="relative block my-2">
                                <label for=" residen-status" class="form-label inline-block mb-2 text-gray-700">Status
                                    Residen</label>
                                <div class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                    <select @change="dewasaCount($event.target.value)"
                                        class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                        name="residen_status[]" id="residen_status" x-model="tur_form.residen_status">
                                        <option value="residen">Residen</option>
                                        <option value="non-residen">Non Residen</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p class="my-2">KTP/KITAS</p>
                            <div x-data="displayImage()">
                                <div class="mb-2">
                                    <template x-if="imageUrl">
                                        <img :src="imageUrl"
                                            class="object-contain rounded border border-gray-300 w-full h-56">
                                    </template>

                                    <template x-if="!imageUrl">
                                        <div
                                            class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-full h-56">
                                            <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                width="20px" height="20px">
                                        </div>
                                    </template>

                                    <input class="mt-2" type="file" accept="image/*" @change="selectedFile"
                                        x-model="tur_form.kitas" name="kitas[]">
                                </div>
                            </div>
                            <p class="text-xs font-normal">Catatan: Beberapa seller mungkin menerapkan kebijakan harga
                                berbeda antara residen dan
                                non-residen</p>
                        </div>
                    </div>
                </div>

                {{-- Loop Peserta Dewasa --}}
                @if ($order['dewasa_count'] > 1)
                @for ($i = 1; $i < $order['dewasa_count']; $i++) <div>
                    <div class="rounded-md shadow-sm border bg-slate-100 border-gray-200 py-5 px-10 mb-5"
                        x-data="{ tur_form: { first_name:'', last_name:'', birth_date:'', jenis_kelamin:'Laki-Laki', residen:false, non_residen:false, nationality:'indonesia', country_code:'62', phone_number:'', email:'', residen_status:'residen', kitas:'', same_as_user:false } }">
                        <div class="block sm:flex sm:justify-between my-5 sm:my-2">
                            <p class="text-base font-semibold">Peserta {{$i+1}} (Dewasa)
                            </p>
                            <button type="button" class="text-[#D50006] text-sm font-medium"
                                x-on:click="tur_form.first_name=''; tur_form.last_name=''; tur_form.birth_date=''; tur_form.jenis_kelamin='Laki-Laki'; tur_form.residen=false; tur_form.non_residen=false; tur_form.nationality='indonesia'; tur_form.country_code='+62'; tur_form.phone_number=''; tur_form.email=''; tur_form.residen_status='residen'; tur_form.kitas=''">Hapus
                                Semua
                            </button>
                        </div>

                        {{-- Name --}}
                        <div class="grid grid-rows-1 my-2">
                            <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                                <div class="relative block">
                                    <label for="first_name"
                                        class="form-label inline-block mb-2 text-gray-700 text-base font-medium">Nama
                                        Depan
                                        & Tengah</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="Nama depan" type="text" name="first_name[]" id="first_name"
                                        x-model="tur_form.first_name" />
                                    <input type="hidden" name="kategori_peserta[]" value="dewasa">
                                </div>
                                <div class="relative block">
                                    <label for="last_name"
                                        class="form-label inline-block mb-2 text-gray-700 text-base font-medium">Nama
                                        Belakang</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="Nama belakang" type="text" name="last_name[]" id="last_name"
                                        x-model="tur_form.last_name" />
                                </div>
                            </div>
                        </div>

                        {{-- Identity --}}
                        <div class="grid grid-rows-1 my-2">
                            <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                                <div class="relative block">
                                    <label for="birth_date" class="form-label inline-block mb-2 text-gray-700">Tanggal
                                        Lahir</label>
                                    <div
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full md:w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                        <input :class="'!m-0 sm:!w-full'" type="text" name="birth_date[]"
                                            id="birth_date_dewasa{{$i}}" x-model="tur_form.birth_date" />
                                    </div>
                                </div>

                                <div class="relative block">
                                    <label for="jenis_kelamin" class="form-label inline-block mb-2 text-gray-700">Jenis
                                        Kelamin</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                            name="jenis_kelamin[]" id="jenis_kelamin" x-model="tur_form.jenis_kelamin">
                                            <option selected value="Laki-Laki">Pria</option>
                                            <option value="Perempuan">Wanita</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sm:grid sm:grid-cols-2 gap-5">
                            <div>
                                {{-- Residen - Non Residen --}}
                                {{-- <div class="sm:flex my-2 space-y-2 sm:space-y-0">
                                    <div class="flex items-center mr-5">
                                        <input class="rounded-sm form-checkbox mr-2" type="checkbox" name="residen"
                                            id="residen" x-model="tur_form.residen">
                                        <label for="residen" class="text-black">Residen</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input class="rounded-sm form-checkbox mr-2" type="checkbox" name="non-residen"
                                            id="non-residen" x-model="tur_form.non_residen">
                                        <label for="non_residen" class="text-black">Non Residen</label>
                                    </div>
                                </div> --}}

                                {{-- Nationality --}}
                                <div class="relative block my-2">
                                    <label for="nationality"
                                        class="form-label inline-block mb-2 text-gray-700">Kebangsaan</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                            name="nationality[]" id="nationality" x-model="tur_form.nationality">
                                            <option selected value="indonesia">Indonesia</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                </div>

                                {{-- Phone Number --}}
                                <div class="relative block my-2">
                                    <label for=" phone-number" class="form-label inline-block mb-2 text-gray-700">No.
                                        HP/WhatsApp</label>
                                    <div class="flex">
                                        <template x-if="tur_form.nationality == 'indonesia'">
                                            <input
                                                class="border-[#828282] rounded-l-md shadow-sm sm:text-sm w-[50px] focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-l-md bg-white"
                                                type="text" readonly value="62" />
                                        </template>
                                        <input
                                            :class="tur_form.nationality == 'indonesia' ? 'rounded-r-md' : 'rounded-md'"
                                            class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                            placeholder="81234567890" type="text" name="phone_number[]"
                                            id="phone_number" x-model="tur_form.phone_number" />
                                    </div>
                                    <template x-if="tur_form.nationality == 'other'">
                                        <p class="py-2 text-xs text-kamtuu-primary">Please write phone number with your
                                            country
                                            code.</p>
                                    </template>
                                </div>

                                {{-- Email --}}
                                <div class="relative block my-2">
                                    <label for=" email" class="form-label inline-block mb-2 text-gray-700">Email</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="emailku@gmail.com" type="text" name="email[]" id="email"
                                        x-model="tur_form.email" />
                                </div>

                                {{-- Residen --}}
                                <div class="relative block my-2">
                                    <label for=" residen-status"
                                        class="form-label inline-block mb-2 text-gray-700">Status
                                        Residen</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select @change="dewasaCount($event.target.value)"
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                            name="residen_status[]" id="residen_status"
                                            x-model="tur_form.residen_status">
                                            <option value="residen">Residen</option>
                                            <option value="non-residen">Non Residen</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <p class="my-2">KTP/KITAS</p>
                                <div x-data="displayImage()">
                                    <div class="mb-2">
                                        <template x-if="imageUrl">
                                            <img :src="imageUrl"
                                                class="object-contain rounded border border-gray-300 w-full h-56">
                                        </template>

                                        <template x-if="!imageUrl">
                                            <div
                                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-full h-56">
                                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                    width="20px" height="20px">
                                            </div>
                                        </template>

                                        <input class="mt-2" type="file" accept="image/*" @change="selectedFile"
                                            x-model="tur_form.kitas" name="kitas[]">
                                    </div>
                                </div>
                                <p class="text-xs font-normal">Catatan: Beberapa seller mungkin menerapkan kebijakan
                                    harga
                                    berbeda antara residen dan
                                    non-residen</p>
                            </div>
                        </div>
                    </div>
        </div>
        @endfor
        @endif

        @if ($order['anak_count'] > 0)
        @for ($i = 0; $i < $order['anak_count']; $i++) <div>
            <div class="rounded-md shadow-sm border bg-slate-100 border-gray-200 py-5 px-10 mb-5"
                x-data="{ tur_form: { first_name:'', last_name:'', birth_date:'', jenis_kelamin:'Laki-Laki', residen:false, non_residen:false, nationality:'indonesia', country_code:'', phone_number:'', email:'', residen_status:'residen', kitas:'', same_as_user:false } }">
                <div class="block sm:flex sm:justify-between my-5 sm:my-2">
                    <p class="text-base font-semibold">Peserta {{$i+1}} (Anak)
                    </p>
                    <button type="button" class="text-[#D50006] text-sm font-medium"
                        x-on:click="tur_form.first_name=''; tur_form.last_name=''; tur_form.birth_date=''; tur_form.jenis_kelamin='Laki-Laki'; tur_form.residen=false; tur_form.non_residen=false; tur_form.nationality='indonesia'; tur_form.country_code='+62'; tur_form.phone_number=''; tur_form.email=''; tur_form.residen_status='residen'; tur_form.kitas=''">Hapus
                        Semua
                    </button>
                </div>

                {{-- Name --}}
                <div class="grid grid-rows-1 my-2">
                    <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                        <div class="relative block">
                            <label for="first_name"
                                class="form-label inline-block mb-2 text-gray-700 text-base font-medium">Nama
                                Depan
                                & Tengah</label>
                            <input
                                class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                placeholder="Nama depan" type="text" name="first_name[]" id="first_name"
                                x-model="tur_form.first_name" />
                            <input type="hidden" name="kategori_peserta[]" value="anak">
                        </div>
                        <div class="relative block">
                            <label for="last_name"
                                class="form-label inline-block mb-2 text-gray-700 text-base font-medium">Nama
                                Belakang</label>
                            <input
                                class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                placeholder="Nama belakang" type="text" name="last_name[]" id="last_name"
                                x-model="tur_form.last_name" />
                        </div>
                    </div>
                </div>

                {{-- Identity --}}
                <div class="grid grid-rows-1 my-2">
                    <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                        <div class="relative block">
                            <label for="birth_date" class="form-label inline-block mb-2 text-gray-700">Tanggal
                                Lahir</label>
                            <div
                                class="placeholder:text-[#BDBDBD] block bg-white w-full md:w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                <input :class="'!m-0 sm:!w-full'" type="text" name="birth_date[]"
                                    id="birth_date_anak{{$i}}" x-model="tur_form.birth_date" />
                            </div>
                        </div>

                        <div class="relative block">
                            <label for="jenis_kelamin" class="form-label inline-block mb-2 text-gray-700">Jenis
                                Kelamin</label>
                            <div class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                <select
                                    class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                    name="jenis_kelamin[]" id="jenis_kelamin" x-model="tur_form.jenis_kelamin">
                                    <option selected value="Laki-Laki">Pria</option>
                                    <option value="Perempuan">Wanita</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="sm:grid sm:grid-cols-2 gap-5">
                    <div>
                        {{-- Residen - Non Residen --}}
                        {{-- <div class="sm:flex my-2 space-y-2 sm:space-y-0">
                            <div class="flex items-center mr-5">
                                <input class="rounded-sm form-checkbox mr-2" type="checkbox" name="residen" id="residen"
                                    x-model="tur_form.residen">
                                <label for="residen" class="text-black">Residen</label>
                            </div>
                            <div class="flex items-center">
                                <input class="rounded-sm form-checkbox mr-2" type="checkbox" name="non-residen"
                                    id="non-residen" x-model="tur_form.non_residen">
                                <label for="non_residen" class="text-black">Non Residen</label>
                            </div>
                        </div> --}}

                        {{-- Nationality --}}
                        <div class="relative block my-2">
                            <label for="nationality"
                                class="form-label inline-block mb-2 text-gray-700">Kebangsaan</label>
                            <div class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                <select
                                    class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                    name="nationality[]" id="nationality" x-model="tur_form.nationality">
                                    <option selected value="indonesia">Indonesia</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                        </div>

                        {{-- Phone Number --}}
                        <div class="relative block my-2">
                            <label for=" phone-number" class="form-label inline-block mb-2 text-gray-700">No.
                                HP/WhatsApp</label>
                            <div class="flex">
                                <template x-if="tur_form.nationality == 'indonesia'">
                                    <input
                                        class="border-[#828282] rounded-l-md shadow-sm sm:text-sm w-[50px] focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-l-md bg-white"
                                        type="text" readonly value="62" />
                                </template>
                                <input :class="tur_form.nationality == 'indonesia' ? 'rounded-r-md' : 'rounded-md'"
                                    class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                    placeholder="81234567890" type="text" name="phone_number[]" id="phone_number"
                                    x-model="tur_form.phone_number" />
                            </div>
                            <template x-if="tur_form.nationality == 'other'">
                                <p class="py-2 text-xs text-kamtuu-primary">Please write phone number with your
                                    country
                                    code.</p>
                            </template>
                        </div>

                        {{-- Email --}}
                        <div class="relative block my-2">
                            <label for=" email" class="form-label inline-block mb-2 text-gray-700">Email</label>
                            <input
                                class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                placeholder="emailku@gmail.com" type="text" name="email[]" id="email"
                                x-model="tur_form.email" />
                        </div>

                        {{-- Residen --}}
                        <div class="relative block my-2">
                            <label for=" residen-status" class="form-label inline-block mb-2 text-gray-700">Status
                                Residen</label>
                            <div class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                <select @change="anakCount($event.target.value)"
                                    class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                    name="residen_status[]" id="residen_status" x-model="tur_form.residen_status">
                                    <option value="residen">Residen</option>
                                    <option value="non-residen">Non Residen</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div>
                        <p class="my-2">KTP/KITAS</p>
                        <div x-data="displayImage()">
                            <div class="mb-2">
                                <template x-if="imageUrl">
                                    <img :src="imageUrl"
                                        class="object-contain rounded border border-gray-300 w-full h-56">
                                </template>

                                <template x-if="!imageUrl">
                                    <div
                                        class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-full h-56">
                                        <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                            width="20px" height="20px">
                                    </div>
                                </template>

                                <input class="mt-2" type="file" accept="image/*" @change="selectedFile"
                                    x-model="tur_form.kitas" name="kitas[]">
                            </div>
                        </div>
                        <p class="text-xs font-normal">Catatan: Beberapa seller mungkin menerapkan kebijakan
                            harga
                            berbeda antara residen dan
                            non-residen</p>
                    </div>
                </div>
            </div>
    </div>
    @endfor
    @endif

    @if ($order['balita_count'] > 0)
    @for ($i = 0; $i < $order['balita_count']; $i++) <div>
        <div class="rounded-md shadow-sm border bg-slate-100 border-gray-200 py-5 px-10 mb-5"
            x-data="{ tur_form: { first_name:'', last_name:'', birth_date:'', jenis_kelamin:'Laki-Laki', residen:false, non_residen:false, nationality:'indonesia', country_code:'+62', phone_number:'', email:'', residen_status:'residen', kitas:'', same_as_user:false } }">
            <div class="block sm:flex sm:justify-between my-5 sm:my-2">
                <p class="text-base font-semibold">Peserta {{$i+1}} (Balita)
                </p>
                <button type="button" class="text-[#D50006] text-sm font-medium"
                    x-on:click="tur_form.first_name=''; tur_form.last_name=''; tur_form.birth_date=''; tur_form.jenis_kelamin='Laki-Laki'; tur_form.residen=false; tur_form.non_residen=false; tur_form.nationality='indonesia'; tur_form.country_code='+62'; tur_form.phone_number=''; tur_form.email=''; tur_form.residen_status='residen'; tur_form.kitas=''">Hapus
                    Semua
                </button>
            </div>

            {{-- Name --}}
            <div class="grid grid-rows-1 my-2">
                <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                    <div class="relative block">
                        <label for="first_name"
                            class="form-label inline-block mb-2 text-gray-700 text-base font-medium">Nama
                            Depan
                            & Tengah</label>
                        <input
                            class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                            placeholder="Nama depan" type="text" name="first_name[]" id="first_name"
                            x-model="tur_form.first_name" />
                        <input type="hidden" name="kategori_peserta[]" value="balita">
                    </div>
                    <div class="relative block">
                        <label for="last_name"
                            class="form-label inline-block mb-2 text-gray-700 text-base font-medium">Nama
                            Belakang</label>
                        <input
                            class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                            placeholder="Nama belakang" type="text" name="last_name[]" id="last_name"
                            x-model="tur_form.last_name" />
                    </div>
                </div>
            </div>

            {{-- Identity --}}
            <div class="grid grid-rows-1 my-2">
                <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                    <div class="relative block">
                        <label for="birth_date" class="form-label inline-block mb-2 text-gray-700">Tanggal
                            Lahir</label>
                        <div
                            class="placeholder:text-[#BDBDBD] block bg-white w-full md:w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                            <input :class="'!m-0 sm:!w-full'" type="text" name="birth_date[]"
                                id="birth_date_balita{{$i}}" x-model="tur_form.birth_date" />
                        </div>
                    </div>

                    <div class="relative block">
                        <label for="jenis_kelamin" class="form-label inline-block mb-2 text-gray-700">Jenis
                            Kelamin</label>
                        <div class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                            <select
                                class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                name="jenis_kelamin[]" id="jenis_kelamin" x-model="tur_form.jenis_kelamin">
                                <option selected value="Laki-Laki">Pria</option>
                                <option value="Perempuan">Wanita</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="sm:grid sm:grid-cols-2 gap-5">
                <div>
                    {{-- Residen - Non Residen --}}
                    {{-- <div class="sm:flex my-2 space-y-2 sm:space-y-0">
                        <div class="flex items-center mr-5">
                            <input class="rounded-sm form-checkbox mr-2" type="checkbox" name="residen" id="residen"
                                x-model="tur_form.residen">
                            <label for="residen" class="text-black">Residen</label>
                        </div>
                        <div class="flex items-center">
                            <input class="rounded-sm form-checkbox mr-2" type="checkbox" name="non-residen"
                                id="non-residen" x-model="tur_form.non_residen">
                            <label for="non_residen" class="text-black">Non Residen</label>
                        </div>
                    </div> --}}

                    {{-- Nationality --}}
                    <div class="relative block my-2">
                        <label for="nationality" class="form-label inline-block mb-2 text-gray-700">Kebangsaan</label>
                        <div class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                            <select
                                class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                name="nationality[]" id="nationality" x-model="tur_form.nationality">
                                <option selected value="indonesia">Indonesia</option>
                                <option value="other">Other</option>
                            </select>
                        </div>
                    </div>

                    {{-- Phone Number --}}
                    <div class="relative block my-2">
                        <label for=" phone-number" class="form-label inline-block mb-2 text-gray-700">No.
                            HP/WhatsApp</label>
                        <div class="flex">
                            <template x-if="tur_form.nationality == 'indonesia'">
                                <input
                                    class="border-[#828282] rounded-l-md shadow-sm sm:text-sm w-[50px] focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-l-md bg-white"
                                    type="text" readonly value="62" />
                            </template>
                            <input :class="tur_form.nationality == 'indonesia' ? 'rounded-r-md' : 'rounded-md'"
                                class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                placeholder="81234567890" type="text" name="phone_number[]" id="phone_number"
                                x-model="tur_form.phone_number" />
                        </div>
                        <template x-if="tur_form.nationality == 'other'">
                            <p class="py-2 text-xs text-kamtuu-primary">Please write phone number with your
                                country
                                code.</p>
                        </template>
                    </div>

                    {{-- Email --}}
                    <div class="relative block my-2">
                        <label for=" email" class="form-label inline-block mb-2 text-gray-700">Email</label>
                        <input
                            class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                            placeholder="emailku@gmail.com" type="text" name="email[]" id="email"
                            x-model="tur_form.email" />
                    </div>

                    {{-- Residen --}}
                    <div class="relative block my-2">
                        <label for=" residen-status" class="form-label inline-block mb-2 text-gray-700">Status
                            Residen</label>
                        <div class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                            <select @change="balitaCount($event.target.value)"
                                class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                name="residen_status[]" id="residen_status" x-model="tur_form.residen_status">
                                <option value="residen">Residen</option>
                                <option value="non-residen">Non Residen</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div>
                    <p class="my-2">KTP/KITAS</p>
                    <div x-data="displayImage()">
                        <div class="mb-2">
                            <template x-if="imageUrl">
                                <img :src="imageUrl" class="object-contain rounded border border-gray-300 w-full h-56">
                            </template>

                            <template x-if="!imageUrl">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-full h-56">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>

                            <input class="mt-2" type="file" accept="image/*" @change="selectedFile"
                                x-model="tur_form.kitas" name="kitas[]">
                        </div>
                    </div>
                    <p class="text-xs font-normal">Catatan: Beberapa seller mungkin menerapkan kebijakan
                        harga
                        berbeda antara residen dan
                        non-residen</p>
                </div>
            </div>
        </div>
        </div>
        @endfor
        @endif

        <input type="hidden" x-model="$store.order.final_price" name="total_price" />
        <div>
            <p class="font-semibold my-5">Sebelum mengonfirmasi pesanan Anda, harap untuk memastikan kembali
                bahwa data
                yang
                anda masukkan sudah akurat.</p>
        </div>
        <div class="w-full grid justify-content-center">
            {{-- Button --}}
            <button type="submit"
                class="my-5 w-full bg-[#23AEC1] text-white text-base font-semibold py-2 rounded-md hover:bg-[#3ac0d2] duration-200">
                Simpan
                di Koper
            </button>
        </div>
        </form>


        {{-- Right Side --}}
        <div class="col-span-6 lg:col-span-2 space-y-5 p-3">

            {{-- Information --}}
            <div class="hidden lg:block border border-[#23AEC1] rounded-md shadow-lg p-5">
                <div class="flex">
                    <img class="rounded-md object-cover object-center shadow-md mr-5 w-20 h-20"
                        src="{{ isset($seller->productdetail->thumbnail) ? asset($seller->productdetail->thumbnail) : asset('storage/img/tur-detail-5.jpg') }}"
                        alt="tur-img">
                    <div>
                        <p class="text-xl font-semibold">{{ $order['product_name'] }}</p>
                        <p class="text-base font-normal">{{ $seller->user->first_name }}</p>
                    </div>
                </div>
                <div class="mt-5">
                    <p>{{ $formated_date }}</p>
                    <p>{{ $seller->productdetail->tipe_tur }}</p>
                    <p>{{ $order['dewasa_count'] }} Dewasa</p>
                    @if ( $order['anak_count'] > 0 )
                    <p>{{ $order['anak_count'] }} Anak</p>
                    @endif
                    @if ( $order['balita_count'] > 0 )
                    <p>{{ $order['balita_count'] }} Balita</p>
                    @endif
                </div>
            </div>

            {{-- Kupon --}}
            <div>KUPON</div>

            {{-- Biaya --}}
            <div class="border border-[#23AEC1] rounded-md shadow-lg p-5" x-data="price()">
                <p class="text-xl font-semibold mb-2">Biaya</p>
                <div class="space-y-1 my-2">
                    <p class="text-base font-semibold">Harga Residen</p>
                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Dewasa</p>
                        <p class="text-[#23AEC1]">Rp. <span x-text="$store.order.dewasa_residen_price"></span></p>
                    </div>
                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Anak-anak</p>
                        <p class="text-[#23AEC1]">Rp. <span x-text="$store.order.anak_residen_price"></span></p>
                    </div>
                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Balita</p>
                        <p class="text-[#23AEC1]">Rp. <span x-text="$store.order.balita_residen_price"></span></p>
                    </div>
                </div>

                <div class="space-y-1 my-2">
                    <p class="text-base font-semibold">Harga Non-Residen</p>
                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Dewasa</p>
                        <p class="text-[#23AEC1]">Rp. <span x-text="$store.order.dewasa_non_residen_price"></span></p>
                    </div>
                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Anak-anak</p>
                        <p class="text-[#23AEC1]">Rp. <span x-text="$store.order.anak_non_residen_price"></span></p>
                    </div>
                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Balita</p>
                        <p class="text-[#23AEC1]">Rp. <span x-text="$store.order.balita_non_residen_price"></span></p>
                    </div>
                </div>

                <hr class="border border-[#707070] my-5" />

                <div class="space-y-1 my-2">
                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-base font-semibold">Harga Tur</p>
                        <p class="text-[#23AEC1] text-base font-semibold">Rp. <span
                                x-text="$store.order.product_price"></span></p>
                    </div>

                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Diskon per tanggal</p>
                        @if ( $diskon_tanggal < 0 ) <p class="text-[#23AEC1]">- Rp. <span
                                x-text="$store.order.product_price * {{ ((int)$diskon_tanggal)/100*(-1)}}"></span></p>
                            @endif
                            @if ( $diskon_tanggal >= 0 )
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                            @endif
                    </div>
                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Surcharge per tanggal</p>
                        @if ( $diskon_tanggal > 0 ) <p class="text-[#23AEC1]">- Rp. <span
                                x-text="Math.ceil($store.order.product_price * {{ ((int)$diskon_tanggal)/100 }})"></span>
                        </p>
                        @endif
                        @if ( $diskon_tanggal <= 0) <p class="text-[#23AEC1]"> Rp. 0</p>
                            @endif
                    </div>
                </div>

                <hr class="border border-[#707070] my-5" />

                <div class="space-y-1 my-2">
                    <div class="flex justify-between">
                        <p class="text-base font-semibold">Harga per Tanggal</p>
                        <p class="text-[#23AEC1] text-base font-semibold">Rp. <span
                                x-text="$store.order.by_date_price"></span></p>
                    </div>

                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Diskon Grup</p>
                        <p class="text-[#23AEC1]">- Rp. <span
                                x-text="Math.ceil($store.order.by_date_price * {{ ((int)$diskon_group)/100 }})"></span>
                        </p>
                    </div>
                    {{-- <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Harga Bersih</p>
                        <p class="text-[#23AEC1]">- Rp. 0</p>
                    </div>
                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Kupon Toko</p>
                        <p class="text-[#23AEC1]">- Rp. 150,000</p>
                    </div> --}}
                </div>

                <hr class="border border-[#707070] my-5" />

                <div class="space-y-1 my-2">
                    <div class="flex justify-between">
                        <p class="text-base font-semibold">Harga Akhir</p>
                        <p class="text-base font-semibold text-[#23AEC1]">Rp. <span
                                x-text="$store.order.by_group_price"></span></p>
                    </div>

                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Pilihan</p>
                        <p class="text-[#23AEC1]">Rp. <span x-text="$store.order.pilihan"></span></p>
                    </div>
                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Extra</p>
                        <p class="text-[#23AEC1]">Rp. <span x-text="$store.order.extra"></span></p>
                    </div>
                    {{-- <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Biaya Lain-lain</p>
                        <p class="text-[#23AEC1]">Rp. 0</p>
                    </div>
                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Biaya Pemesanan</p>
                        <p class="text-[#23AEC1]">Rp. 10,000</p>
                    </div>
                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Pajak</p>
                        <p class="text-[#23AEC1]">Rp. 0</p>
                    </div>
                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Promo</p>
                        <p class="text-[#23AEC1]">- Rp. 0</p>
                    </div>
                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-[#828282]">Kupon</p>
                        <p class="text-[#23AEC1]">- Rp. 160,000</p>
                    </div> --}}
                </div>

                <hr class="border border-[#707070] my-5" />

                <div class="flex justify-between text-sm font-normal">
                    <p class="text-lg font-semibold">Total Bayar</p>
                    <p class="text-lg font-semibold text-[#23AEC1]">Rp. <span x-text="$store.order.final_price"></span>
                    </p>
                </div>
            </div>

            {{-- Button --}}
            {{-- <button
                class="w-full bg-[#23AEC1] text-white text-base font-semibold py-2 rounded-md hover:bg-[#3ac0d2] duration-200">Simpan
                di Koper</button> --}}
        </div>

        </div>
        </div>

        <footer>
            <x-destindonesia.footer></x-destindonesia.footer>
        </footer>

        @livewireScripts
        <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
        <script>
            for (let i = 0; i < {{$order['dewasa_count']}}; i++) {
                $(`#birth_date_dewasa${i}`).datepicker();
            }
            
            for (let i = 0; i < {{$order['anak_count']}}; i++) {
                $(`#birth_date_anak${i}`).datepicker();
            }

            for (let i = 0; i < {{$order['balita_count']}}; i++) {
                $(`#birth_date_balita${i}`).datepicker();
            }

            // $('#birth_date').datepicker();
            
            function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                if (! event.target.files.length) return

                let file = event.target.files[0],
                    reader = new FileReader()

                reader.readAsDataURL(file)
                reader.onload = e => callback(e.target.result)
                },
            }
        }
        </script>
        <script>
            document.addEventListener("alpine:init", () => {
            Alpine.store("order", {
                    product_price: '-',
                    by_date_price: 0,
                    by_group_price: 0,
                    final_price: 0,
                    dewasa_residen_price: 0,
                    anak_residen_price: 0,
                    balita_residen_price: 0,
                    dewasa_non_residen_price: 0,
                    anak_non_residen_price: 0,
                    balita_non_residen_price: 0,
                    pilihan: 0,
                    extra:0,
            });
        });

    function price() {
        return {
            dewasa_residen_count: {{ $order['dewasa_count'] > 0 ? $order['dewasa_count'] : 0 }},
            anak_residen_count: {{ $order['anak_count'] > 0 ? $order['anak_count'] : 0 }},
            balita_residen_count: {{ $order['balita_count'] > 0 ? $order['balita_count'] : 0 }},
            dewasa_non_residen_count: 0,
            anak_non_residen_count: 0,
            balita_non_residen_count: 0,
            init() {
                Alpine.nextTick(() => { 
                    Alpine.store("order").dewasa_residen_price = this.dewasa_residen_count * {!! $harga['dewasa_residen'] !!}
                    Alpine.store("order").anak_residen_price = this.anak_residen_count * {!! $harga['anak_residen'] !!}
                    Alpine.store("order").balita_residen_price = this.balita_residen_count * {!! $harga['balita_residen'] !!}
                    Alpine.store("order").dewasa_non_residen_price = this.dewasa_non_residen_count * {!! $harga['dewasa_non_residen'] !!}
                    Alpine.store("order").anak_non_residen_price = this.anak_non_residen_count * {!! $harga['anak_non_residen'] !!}
                    Alpine.store("order").balita_non_residen_price = this.balita_non_residen_count * {!! $harga['balita_non_residen'] !!}
                    Alpine.store("order").product_price = Alpine.store("order").dewasa_residen_price + Alpine.store("order").anak_residen_price + Alpine.store("order").balita_residen_price + Alpine.store("order").dewasa_non_residen_price + Alpine.store("order").anak_non_residen_price + Alpine.store("order").balita_non_residen_price 
                    Alpine.store("order").by_date_price = Alpine.store("order").product_price + Math.ceil((Alpine.store("order").product_price * {!! (int)$diskon_tanggal !!}/100 ))
                    Alpine.store("order").by_group_price = Alpine.store("order").by_date_price - Math.ceil((Alpine.store("order").by_date_price * {!! (int)$diskon_group !!}/100 ))
                    Alpine.store("order").pilihan = {!! (int)$total_pilihan !!}
                    Alpine.store("order").extra = {!! (int)$total_extra !!}
                    Alpine.store("order").final_price = Alpine.store("order").by_group_price + Alpine.store("order").pilihan + Alpine.store("order").extra
                })
                this.$watch('dewasa_residen_count; anak_residen_count; balita_residen_count; dewasa_non_residen_count; anak_non_residen_count; balita_non_residen_count', () => {
                    console.log('masuk')
                    Alpine.store("order").dewasa_residen_price = this.dewasa_residen_count * {!! $harga['dewasa_residen'] !!}
                    Alpine.store("order").anak_residen_price = this.anak_residen_count * {!! $harga['anak_residen'] !!}
                    Alpine.store("order").balita_residen_price = this.balita_residen_count * {!! $harga['balita_residen'] !!}
                    Alpine.store("order").dewasa_non_residen_price = this.dewasa_non_residen_count * {!! $harga['dewasa_non_residen'] !!}
                    Alpine.store("order").anak_non_residen_price = this.anak_non_residen_count * {!! $harga['anak_non_residen'] !!}
                    Alpine.store("order").balita_non_residen_price = this.balita_non_residen_count * {!! $harga['balita_non_residen'] !!}
                    Alpine.store("order").product_price = Alpine.store("order").dewasa_residen_price + Alpine.store("order").anak_residen_price + Alpine.store("order").balita_residen_price + Alpine.store("order").dewasa_non_residen_price + Alpine.store("order").anak_non_residen_price + Alpine.store("order").balita_non_residen_price
                    Alpine.store("order").by_date_price = Alpine.store("order").product_price + Math.ceil((Alpine.store("order").product_price * {!! (int)$diskon_tanggal !!}/100 ))
                    Alpine.store("order").by_group_price = Alpine.store("order").by_date_price - Math.ceil((Alpine.store("order").by_date_price * {!! (int)$diskon_group !!}/100 ))
                    Alpine.store("order").final_price = Alpine.store("order").by_group_price + Alpine.store("order").pilihan + Alpine.store("order").extra
                })
            },
            dewasaCount(status) {
                console.log(status)
                if (status === "residen") {
                    this.dewasa_residen_count++
                    this.dewasa_non_residen_count = this.dewasa_non_residen_count <= 0 ? 0 : this.dewasa_non_residen_count-1
                } 

                if (status === "non-residen") {
                    this.dewasa_non_residen_count++
                    this.dewasa_residen_count = this.dewasa_residen_count <= 0 ? 0 : this.dewasa_residen_count-1
                }

                console.log(this.dewasa_residen_count)
                console.log(Alpine.store("order").dewasa_residen_price)
                
            },
            anakCount(status) {
                if (status === "residen") {
                    this.anak_residen_count++
                    this.anak_non_residen_count = this.anak_non_residen_count <= 0 ? 0 : this.anak_non_residen_count-1
                } 

                if (status === "non-residen") {
                    this.anak_non_residen_count++
                    this.anak_residen_count = this.anak_residen_count <= 0 ? 0 : this.anak_residen_count-1
                }                    
            },
            balitaCount(status) {
                if (status === "residen") {
                    this.balita_residen_count++
                    this.balita_non_residen_count = this.balita_non_residen_count <= 0 ? 0 : this.balita_non_residen_count-1
                } 

                if (status === "non-residen") {
                    this.balita_non_residen_count++
                    this.balita_residen_count = this.balita_residen_count <= 0 ? 0 : this.balita_residen_count-1
                }                    
            },
        }
    }
        </script>
</body>

</html>