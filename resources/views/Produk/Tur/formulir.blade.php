<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Destindonesia') }}</title>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    {{-- <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script> --}}
    {{-- <script src="{{ resource_path('assets/js/gijgo.min.js') }}" type="text/javascript"></script>
    <script src="{{ resource_path('assets/js/summernote.js') }}" type="text/javascript"></script>
    <link rel="stylesheet" href="{{ resource_path('assets/css/summernote.css') }}" /> --}}


    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js" defer></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js" defer></script>
    {{-- @vite(['resources/assets/js/gijgo.min.js']) --}}
    {{-- @vite(['resources/assets/css/summernote-lite.min.css', 'resources/assets/js/summernote-lite.min.js']) --}}

    <!-- Styles -->
    @livewireStyles

    <style>
        .note-editable {
            background-color: white;
            /* color: black; */
        }

        .note-toolbar {
            background-color: white;
        }

        .note-editable ul {
            list-style: disc !important;
            list-style-position: inside !important;
        }

        .note-editable ol {
            list-style: decimal !important;
            list-style-position: inside !important;
        }
    </style>

</head>

<body class="bg-white font-Inter">

    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    <div class="max-w-7xl bg-[#F2F2F2] mx-auto rounded-md m-5">
        <div class="grid p-5 justify-items-center">
            <h1 class="font-semibold text-[26px]">Formulir Custom Tour</h1>
        </div>

        <div class="p-5 sm:p-10">

            {{-- <div id="summernote" class="click2edit"></div> --}}


            <div>
                <p class="text-kamtuu-second text-[18px]">ID : CUS01TO0001</p>
            </div>

            <div class="grid max-w-6xl p-5 mx-auto text-justify md:text-center">
                <h1 class="font-semibold text-[20px] sm:text-[24px]">Anda tidak menemukan paket tur yang sesuai? Anda
                    butuh pengaturan
                    khusus?
                    Beritahu kami. Tuliskan kebutuhan Anda di formulir berikut.</h1>
            </div>

            <form method="POST" action="{{ route('formulirTour.store') }}" enctype="multipart/form-data">
                @csrf

                <div class="grid lg:grid-cols-[33%_33%_33%]">
                    <div>
                        <p class="text-[18px] font-semibold">Tanggal Mulai</p>

                        <div class="lg:grid py-2 lg:grid-cols-[80%_20%] w-2/4 lg:w-full">
                            <div class="p-2 bg-white border border-black rounded-xl">
                                <input name="tanggalMulai" id="datepickerFormulirTur" placeholder=" " readonly />
                            </div>
                            {{-- <div class="md:hidden lg:hidden">
                            <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                        </div> --}}
                        </div>
                    </div>
                    <div>
                        <p class="text-[18px] font-semibold">Mulai</p>

                        <div class="lg:grid py-2 lg:grid-cols-[80%_20%] w-2/4 lg:w-full">
                            <div class="p-2 bg-white border border-black rounded-xl">
                                <input name="waktuMulai" id="timepickerFormulirTur" placeholder=" " />
                            </div>
                        </div>
                    </div>
                    <div>
                        <p class="text-[18px] font-semibold">Durasi</p>
                        <div class="grid py-2">
                            <div class="inline-flex">
                                <input class="w-2/4 lg:w-full bg-white border border-black rounded-xl" type="number"
                                    name="durasi" id="">
                                <p class="px-2 py-2">Hari</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="grid grid-rows-2 py-5">
                    <div>
                        <p class="text-[18px] font-semibold">Lokasi Mulai Tur:</p>
                    </div>
                    <div>
                        <select class="py-1 px-2 w-2/4 h-10 bg-white border border-black rounded-lg" name="lokasi"
                            id="">
                            @foreach ($lokasis as $lokasi)
                                <option value="{{ $lokasi->namaWisata }}">{{ $lokasi->namaWisata }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="grid lg:grid-cols-[20%_20%_60%] py-5 grid-rows-7">
                    <div class="col-span-3">
                        <p class="text-[18px] font-semibold">Jumlah Peserta:</p>
                    </div>

                    <div class="col-span-2 row-span-2">
                        <p>Dewasa</p>
                        <p class="text-[#828282]">Diatas 12 tahun</p>
                    </div>
                    <div>
                        <input name="dewasa" class="numberstyle" type="number" min="1" step="1"
                            value="1">
                    </div>

                    <div class="col-span-2 row-span-2">
                        <p>Anak</p>
                        <p class="text-[#828282]">5 sampai 12 tahun</p>
                    </div>
                    <div>
                        <input name="anak" class="numberstyle" type="number" min="1" step="1"
                            value="1">
                    </div>

                    <div class="col-span-2 row-span-2">
                        <p>Balita</p>
                        <p class="text-[#828282]">Dibawah 5 tahun</p>
                    </div>
                    <div>
                        <input name="balita" class="numberstyle" type="number" min="1" step="1"
                            value="1">
                    </div>
                </div>

                <div class="grid lg:grid-cols-3">
                    <div class="grid col-span-3">
                        <p class="font-semibold text-[18px]">Jenis Akomodasi</p>
                    </div>
                    <div class="grid lg:grid-cols-[25%_15%_60%] col-span-3">
                        <div>
                            <select class="w-3/4 h-8 rounded-xl px-2" name="transportasi" id="">
                                <option value="Mobil">Mobil</option>
                            </select>
                        </div>

                        <div>
                            <p class="w-3/4">Jumlah Peserta</p>
                        </div>

                        <div>
                            <input name="jumlahPeserta" class="numberstyle" type="number" min="1" step="1"
                                value="1">
                        </div>
                    </div>
                </div>

                <div class="grid lg:grid-cols-3">
                    <div class="grid col-span-3">
                        <p class="font-semibold text-[18px]">Makanan</p>
                    </div>
                    <div class="grid grid-cols-[25%_15%_60%] col-span-3">
                        <div>
                            <select class="w-full sm:w-3/4 h-8 rounded-xl px-2" name="makanan" id="">
                                <option value="Halal">Halal</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="py-5">
                    <p class="text-[18px] font-semibold">Centang jenis transportasi yang menjadi preferensi Anda:</p>
                </div>

                <div class="grid lg:grid-cols-[10%_10%_80%]">
                    <div class="flex">
                        <input class="checked:bg-kamtuu-second" type="checkbox" name="jenisKendaraan[]"
                            value="Private" id="">
                        <p class="px-3 -mt-1">Private</p>
                    </div>
                    <div class="flex">
                        <input class="checked:bg-kamtuu-second" type="checkbox" name="jenisKendaraan[]"
                            value="Umum" id="">
                        <p class="px-3 -mt-1">Umum</p>
                    </div>
                    <div class="flex">
                        <input class="checked:bg-kamtuu-second" type="checkbox" name="jenisKendaraan[]"
                            value="Kombinasi" id="">
                        <p class="px-3 -mt-1">Boleh Kombinasi Private & Umum</p>
                    </div>
                </div>

                <div class="py-5">
                    <p class="py-3 text-[20px] sm:text-[24px] text-justify sm:text-left font-semibold">Bila Anda
                        mengetahui,
                        tuliskan nama objek wisata
                        yang ingin
                        dimasukkan ke dalam paket tur Anda. Bila
                        tidak tahu, centeng “Serahkan ke Kamtuu” pada bagian bawah kotak.</p>

                    <textarea class="rounded-md w-full lg:w-[35rem]" name="namaObjek" id="kamtuu-input" cols="50" rows="10"
                        placeholder="Tuliskan hal-hal lain yang perlu kami ketahui untuk mengaturkan transfer Anda."></textarea>

                    <div class="flex m-2">
                        <input class="checked:bg-kamtuu-second kamtuu-checkbox" type="checkbox" name="Kamtuu"
                            value="yes" id="">
                        <p class="px-3 -mt-1">Serahkan ke Kamtuu</p>
                    </div>
                </div>

                <div class="pt-5 pb-3">
                    <p class="text-[20px] sm:text-[24px] text-justify sm:text-left font-semibold">Bila Anda Punya
                        rancangan
                        program
                        tur sendiri,
                        silahkan masukkan di
                        bawah.</p>
                </div>


                <table class="table table-bordered w-full" id="dynamicAddRemove">
                    <tr>
                        <th>Deskripsi Tur :</th>
                    </tr>
                    <tr>
                        <td>
                            <textarea class="rounded-lg w-[65rem] my-2 mx-2" type="text" name="addMoreInputFields1[0][deskripsiCustom]"
                                placeholder="Enter Title" class="form-control"></textarea>
                        </td>
                        <td>
                            <button type="button" name="add" id="dynamic-ar"
                                class="p-2 bg-kamtuu-second text-white rounded-lg">Tambah
                            </button>
                        </td>
                    </tr>

                </table>

                {{--            <div> --}}
                {{--                <div class="row" x-data="handler()"> --}}
                {{--                    <div class="btn btn-primary"> --}}
                {{--                        <button type="button" --}}
                {{--                                class="px-3 py-1 my-3 text-white rounded-full btn btn-info bg-kamtuu-second" --}}
                {{--                                @click="addNewField()">Tambah + --}}
                {{--                        </button> --}}
                {{--                    </div> --}}
                {{--                    <template x-if="fields.length < 1"> --}}
                {{--                        <h1>Klik tombol tambah di atas untuk menambahkan.</h1> --}}
                {{--                    </template> --}}
                {{--                    <template x-if="fields.length >= 1" class="col"> --}}
                {{--                        <table --}}
                {{--                            class="table table-bordered py-3 px-2 md:px-5 align-items-center table-sm rounded-md border-separate bg-white"> --}}
                {{--                            <thead class="thead-light border-b border-b-slate-200"> --}}
                {{--                            <tr class="hidden md:table-row"> --}}
                {{--                                <th class="px-3">Hari</th> --}}
                {{--                                <th></th> --}}
                {{--                                <th>Hapus</th> --}}
                {{--                            </tr> --}}
                {{--                            </thead> --}}
                {{--                            <tbody class="wrapper"> --}}
                {{--                            <template x-for="(field, index) in fields" :key="index"> --}}
                {{--                                <tr> --}}
                {{--                                    <td class="hidden px-2 py-5 md:flex items-center justify-center" --}}
                {{--                                        x-text="index + 1"> --}}
                {{--                                    </td> --}}
                {{--                                    --}}{{-- <td><input x-model="field.txt1" type="text" name="txt1[]" --}}
                {{--                                            class="w-[10rem] form-control"></td> --}}
                {{--                                    --}}{{-- Run Function summernote in .wrapper .summernote or .click2edit --}}
                {{--                                    <td class="px-2 pb-5 md:py-5"> --}}
                {{--                                        --}}{{-- <div x-model="field.txt1" class="w-[10rem] click2edit" id="summernote"> --}}
                {{--                                        </div> --}}

                {{--                                        --}}{{-- Mobile --}}
                {{--                                        <div class="px-2 py-1 md:hidden flex justify-between"> --}}
                {{--                                            <div class="font-bold">Hari <span x-text="index + 1"></span> --}}
                {{--                                            </div> --}}
                {{--                                            <div> --}}
                {{--                                                <button type="button" class="btn btn-danger btn-small" --}}
                {{--                                                        @click="removeField(index)"> --}}
                {{--                                                    <img src="{{ asset('storage/icons/delete-carbage.png') }}" --}}
                {{--                                                         class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3" --}}
                {{--                                                         title="Kamtuu"> --}}
                {{--                                                </button> --}}
                {{--                                            </div> --}}
                {{--                                        </div> --}}

                {{--                                        --}}{{-- Desktop --}}
                {{--                                        <div class="w-[10rem] click2edit" id="summernote" --}}
                {{--                                             x-init="$nextTick(() => { initSummerNote() })"> --}}
                {{--                                        </div> --}}
                {{--                                    </td> --}}
                {{--                                    <td class="hidden px-2 py-5 md:flex items-center justify-center"> --}}
                {{--                                        <button type="button" class="px-5 btn btn-danger btn-small" --}}
                {{--                                                @click="removeField(index)"> --}}
                {{--                                            <img src="{{ asset('storage/icons/delete-carbage.png') }}" --}}
                {{--                                                 class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3" --}}
                {{--                                                 title="Kamtuu"> --}}
                {{--                                        </button> --}}
                {{--                                    </td> --}}
                {{--                                </tr> --}}

                {{--                            </template> --}}
                {{--                            </tbody> --}}
                {{--                            <tfoot> --}}
                {{--                            <tr> --}}
                {{--                                <td colspan="4" class="text-right"></td> --}}
                {{--                            </tr> --}}
                {{--                            </tfoot> --}}
                {{--                        </table> --}}
                {{--                    </template> --}}
                {{--                </div> --}}
                {{--            </div> --}}

                {{-- <button id="edit" class="btn btn-primary" onclick="edit()" type="button">Edit 1</button>
            <button id="save" class="btn btn-primary" onclick="save()" type="button">Save 2</button>
            <div class="click2edit">click2edit</div> --}}

                <x-destindonesia.button-primary>
                    @slot('button')
                        Simpan
                    @endslot
                </x-destindonesia.button-primary>
            </form>
        </div>

    </div>


    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>


    <script type="text/javascript">
        var i = 0;
        $("#dynamic-ar").click(function() {
            ++i;
            $("#dynamicAddRemove").append('<tr>' +
                '<td><textarea class="rounded-lg w-[65rem] my-2 mx-2" type="text" name="addMoreInputFields1' +
                '[' + i + ']' +
                '[deskripsiCustom]" placeholder="Tuliskan Deskripsi" class="form-control"></textarea></td>' +
                '<td><button type="button" class="p-2 rounded-lg mx-2 bg-kamtuu-third remove-input-field">Delete</button></td>' +
                '</tr>'
            );
        });
        $(document).on('click', '.remove-input-field', function() {
            $(this).parents('tr').remove();
        });
    </script>

    <script>
        //     window.summernote = require('summernote');

        //     var summernote = new summernote('#summernote', {
        //     placeholder: 'Hello stand alone ui',
        //     tabsize: 2,
        //     height: 120,
        //     toolbar: [
        //         ['style', ['style']],
        //         ['font', ['bold', 'underline', 'clear']],
        //         ['color', ['color']],
        //         ['para', ['ul', 'ol', 'paragraph']],
        //         ['table', ['table']],
        //         ['insert', ['link', 'picture', 'video']],
        //         ['view', ['fullscreen', 'codeview', 'help']]
        //     ]
        // });
        const initSummerNote = () => {
            $('.click2edit').summernote({
                placeholder: 'Hello stand alone ui',
                tabsize: 2,
                height: 120,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']]
                ]
            });
        }

        // $(document).ready(function() {
        //     $('#summernote').summernote({
        //     placeholder: 'Hello stand alone ui',
        //     tabsize: 2,
        //     height: 120,
        //     toolbar: [
        //         ['style', ['style']],
        //         ['font', ['bold', 'underline', 'clear']],
        //         ['color', ['color']],
        //         ['para', ['ul', 'ol', 'paragraph']],
        //         ['table', ['table']],
        //         ['insert', ['link', 'picture', 'video']],
        //         ['view', ['fullscreen', 'codeview', 'help']]
        //     ]
        // });
        // });

        //     var edit = function() {
        //   $('.click2edit').summernote({focus: true});
        // };

        // var save = function() {
        //   var markup = $('.click2edit').summernote('code');
        //   $('.click2edit').summernote('destroy');
        // };
    </script>

    <script>
        let allCheckBox = document.querySelectorAll('.kamtuu-checkbox')

        allCheckBox.forEach((checkbox) => {
            checkbox.addEventListener('change', (event) => {
                if (event.target.checked) {
                    console.log('masuk')
                    document.getElementById('kamtuu-input').readOnly = true;
                } else {
                    document.getElementById('kamtuu-input').readOnly = false;
                }
            })
        })

        // let checkbox = document.querySelector('.kamtuu-checkbox').checked;
        // console.log(checkbox);
        // if (checkbox) {
        //     document.getElementById('kamtuu-input').readOnly = true;
        // }
    </script>

    <script>
        $(document).ready(function() {
            $('#datepickerFormulirTur').datepicker();
        })
    </script>

    <script>
        $(document).ready(function() {
            $('#timepickerFormulirTur').timepicker();
        })
    </script>

    <script>
        (function($) {

            $.fn.numberstyle = function(options) {

                /*
                 * Default settings
                 */
                var settings = $.extend({
                    value: 0,
                    step: undefined,
                    min: undefined,
                    max: undefined
                }, options);

                /*
                 * Init every element
                 */
                return this.each(function(i) {

                    /*
                     * Base options
                     */
                    var input = $(this);

                    /*
                     * Add new DOM
                     */
                    var container = document.createElement('div'),
                        btnAdd = document.createElement('div'),
                        btnRem = document.createElement('div'),
                        min = (settings.min) ? settings.min : input.attr('min'),
                        max = (settings.max) ? settings.max : input.attr('max'),
                        value = (settings.value) ? settings.value : parseFloat(input.val());
                    container.className = 'numberstyle-qty';
                    btnAdd.className = (max && value >= max) ? 'qty-btn qty-add disabled' :
                        'qty-btn qty-add';
                    btnAdd.innerHTML = '+';
                    btnRem.className = (min && value <= min) ? 'qty-btn qty-rem disabled' :
                        'qty-btn qty-rem';
                    btnRem.innerHTML = '-';
                    input.wrap(container);
                    input.closest('.numberstyle-qty').prepend(btnRem).append(btnAdd);

                    /*
                     * Attach events
                     */
                    // use .off() to prevent triggering twice
                    $(document).off('click', '.qty-btn').on('click', '.qty-btn', function(e) {

                        var input = $(this).siblings('input'),
                            sibBtn = $(this).siblings('.qty-btn'),
                            step = (settings.step) ? parseFloat(settings.step) : parseFloat(input
                                .attr('step')),
                            min = (settings.min) ? settings.min : (input.attr('min')) ? input.attr(
                                'min') : undefined,
                            max = (settings.max) ? settings.max : (input.attr('max')) ? input.attr(
                                'max') : undefined,
                            oldValue = parseFloat(input.val()),
                            newVal;

                        //Add value
                        if ($(this).hasClass('qty-add')) {

                            newVal = (oldValue >= max) ? oldValue : oldValue + step,
                                newVal = (newVal > max) ? max : newVal;

                            if (newVal == max) {
                                $(this).addClass('disabled');
                            }
                            sibBtn.removeClass('disabled');

                            //Remove value
                        } else {

                            newVal = (oldValue <= min) ? oldValue : oldValue - step,
                                newVal = (newVal < min) ? min : newVal;

                            if (newVal == min) {
                                $(this).addClass('disabled');
                            }
                            sibBtn.removeClass('disabled');

                        }

                        //Update value
                        input.val(newVal).trigger('change');

                    });

                    input.on('change', function() {

                        const val = parseFloat(input.val()),
                            min = (settings.min) ? settings.min : (input.attr('min')) ? input.attr(
                                'min') : undefined,
                            max = (settings.max) ? settings.max : (input.attr('max')) ? input.attr(
                                'max') : undefined;

                        if (val > max) {
                            input.val(max);
                        }

                        if (val < min) {
                            input.val(min);
                        }
                    });

                });
            };

            $('.numberstyle').numberstyle();

        }(jQuery));
    </script>

    <script>
        function handler() {
            return {
                fields: [],
                index: 0,
                addNewField() {
                    this.index++;
                    // $('.click2edit').summernote({focus: true});
                    // var tableHtml = ('<tr><td>'+this.index+'</td> <td> <div class="w-[10rem] click2edit" id="summernote"></div></td><td><button type="button" class="px-5 btn btn-danger btn-small" onClick="(index) => this.fields.splice(index, 1)"><img src="{{ asset('storage/icons/delete-carbage.png') }}"class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3"title="Kamtuu"></button></td></tr>')
                    this.fields.push({
                        txt1: ''
                    });
                    var markupStr = []
                    for (let i = 0; i < this.fields.length; i++) {
                        markupStr[i] = $('.click2edit').eq(i).summernote('code');
                        console.log(`${markupStr[i]}`, markupStr)

                    }
                    //    var markupStr = $('.summernote').eq(1).summernote('code');
                    //    var edit = function() {
                    // $('.wrapper').append('<div class="w-[10rem] click2edit" id="summernote"></div>')
                    // $('.wrapper').append(tableHtml);
                    // $('.click2edit').summernote({focus: true});
                    // };
                    //    $('.summernote').summernote({focus: true});
                    //    var markupStr = $('.click2edit').eq(this.count).summernote('code');
                    //    console.log(this.count)
                    //     $(document).ready(function() {
                    //     $('.summernote').summernote({
                    //     placeholder: 'Hello stand alone ui',
                    //     tabsize: 2,
                    //     height: 120,
                    //     toolbar: [
                    //         ['style', ['style']],
                    //         ['font', ['bold', 'underline', 'clear']],
                    //         ['color', ['color']],
                    //         ['para', ['ul', 'ol', 'paragraph']],
                    //         ['table', ['table']],
                    //         ['insert', ['link', 'picture', 'video']],
                    //         ['view', ['fullscreen', 'codeview', 'help']]
                    //     ]
                    // });
                    // });
                    console.log(this.fields)
                },
                removeField(index) {
                    this.fields.splice(index, 1);
                }
            }
        }
    </script>

</body>

</html>
