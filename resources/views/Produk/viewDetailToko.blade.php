<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tur') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border border-b-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>
    {{-- @dump($banners) --}}
    {{-- Hero --}}
    <div class="relative bg-slate-400">
        <img class="w-full opacity-70 object-cover" src="{{ isset($toko->banner_toko) ? asset($toko->banner_toko):'https://via.placeholder/50x50' }}" alt="banner-tur">
    </div>

    <div class="container mx-auto my-5 border-b">
    {{-- grid gap-5 mx-10 my-10 grid:row-2 lg:grid-cols-5 xl:mr-24 --}}
        <div class="grid gap-2 mx-10 my-10 grid:row-2 lg:grid-cols-12 xl:mr-24 ml-2">
            <div class="flex justify-center lg:justify-end border-1 border-gray-600">
                <img alt="Logo Toko" src="{{isset($toko->logo_toko) ? asset($toko->logo_toko) : asset('storage/img/tur-detail-1.png') }}" class="mx-auto object-cover rounded-full h-24 w-24 bg-white p-1 shadow-sm border-1 border-gray-500"/>
            </div>
            <div class="lg:col-span-8">
                <p>{{isset($toko->nama_toko) ? $toko->nama_toko:'Nama Toko'}}</p>
                <div class="flex flex-wrap gap-2 text-sm font-semibold">
                    <p class="flex space-x-2">
                        {{-- class="w-2 h-2 overflow-hidden" --}}
                        <img src="{{asset('icons/envelope-regular.svg')}}" class="w-8 h-8 overflow-hidden" width="10"><span style="margin-top:7px">Balasan pesan &plusmn; 10 menit</span>
                    </p>
                    <p class="flex space-x-2 spa">
                        <img src="{{asset('icons/location-dot-solid.svg')}}" class="w-8 h-8 text-black" width="10"><span style="margin-top:7px">{{isset($toko) ? $toko->lokasi_toko:'Alamat toko tidak tersedia'}}</span>
                    </p>
                    <p class="flex space-x-2">
                        <img src="{{asset('icons/Star 1.png')}}" class="w-8 h-8 text-black" width="10"><span style="margin-top:7px">{{$ratings}}</span>
                    </p>
                    <p class="flex space-x-2">
                        <img src="{{asset('icons/thumbs-up-regular.svg')}}" class="w-8 h-8 text-black" width="10"><span style="margin-top:7px">{{$likes}}</span>
                    </p>
                    <p class="flex space-x-2" style="margin-top:7px">
                        ID Seller: <span>{{$id}}</span>
                    </p>
                </div>
                <div>
                    <p class="flex space-x-2 py-2">
                        Cabang Resmi&nbsp;:&nbsp;<span>{{$lokasi_cabang}}</span>
                    </p>
                </div>
                <div class="flex justify-start gap-5 py-2">
                    <button class="px-8 py-4 rounded-lg text-white bg-blue-300 text-md" style="background:#93C5FD;">Kirim Pesan</button>
                    {{-- <button class="px-8 py-4 rounded-lg text-blue-300 text-md bg-transparent border border-blue-300">Info Toko</button> --}}
                </div>
                {{-- <img class="w-10" src="{{asset('icons/envelope-regular.svg')}}"> --}}
            </div>
        </div>
    </div>
    
    <div>
        <div class="container sticky top-0 z-20 mx-auto my-5 bg-white" x-data="{ active:0,
        tabs:['Beranda', 'Layanan', 'Ulasan','Info Toko']}">
            <div class="flex sm:block">
                <ul
                    class="flex justify-start overflow-x-auto text-sm font-medium text-center sm:text-left text-gray-500 dark:text-gray-400 border-b border-[#BDBDBD]">
                    <template x-for="(tab, index) in tabs" :key="index">
                        <li class="pb-2 sm:mt-2" :class="{'border-b-2 border-[#9E3D64]': active == index}"
                            x-on:click="active = index">
                            <a :href="`#`+tab.toLowerCase()">
                                <button
                                    class="inline-block px-5 py-1 text-lg duration-200 rounded-lg sm:py-2 sm:px-9 sm:text-xl"
                                    :class="{'text-[#9E3D64] bg-white font-bold': active == index, 'text-black font-normal hover:text-[#9E3D64]': active != index}"
                                    x-text="tab">
                                </button>
                            </a>
                        </li>
                    </template>
                </ul>
            </div>
        </div>
        <div class="container mx-auto">
            <div class="grid grid-cols-5 gap-5">
                <div class="col-span-5 p-3 bg-white lg:col-span-4">
                    <div id="beranda" class="pt-20">
                        <p>Mengapa Membeli layanan di toko kami?</p>

                        <div class="grid grid-cols-1 mt-2 mb-5">
                            @if (isset($banners[0]))   
                            <a href="{{isset($banners[0]['link_banner']) ? $banners[0]['link_banner']:'#'}}">
                            <img class="object-cover w-full h-full opacity-70" alt="" src="{{isset($banners) ? asset($banners[0]['banner']):''}}">
                            </a>
                            @endif
                        </div>
                        <div class="py-5 text-justify text-md lg:text-xl">
                            <p>
                                {!! isset($toko->tentang_toko) ?  $toko->tentang_toko :""!!}
                            </p>
                        </div>

                        <div class="grid grid-rows-2 mt-2 mb-5 gap-3 overflow-hidden">
                            @if (isset($banners[1]))
                            <a class="mb-2" href="{{isset($banners[1]['link_banner']) ? $banners[1]['link_banner'] :'#'}}">
                                <img class="object-cover overflow-hidden rounded-lg w-full h-full opacity-70" alt="" src="{{isset($banners[1]) ? asset($banners[1]['banner']) :''}}">
                            </a>
                            {{-- <br> --}}
                            @endif
                            @if (isset($banners[2]))
                            <a class="mt-2" href="{{isset($banners[2]['link_banner']) ? $banners[2]['link_banner'] :'#'}}">
                                <img class="object-cover overflow-hidden rounded-lg w-full h-full opacity-70" alt="" src="{{isset($banners[2]) ? asset($banners[2]['banner']) :''}}">
                            </a>
                            @endif
                            
                            {{-- <img class="object-cover overflow-hidden rounded-lg w-full h-full opacity-70" alt="" src={{isset($banner_three->banner) ? asset($banner_three->banner) :'https://source.unsplash.com/600x400?store'}}> --}}
                        </div>
                        <div class="grid grid-cols-3 mt-2 mb-5 gap-4 overflow-hidden">
                            @if (isset($banners[3]))
                            <a href="{{isset($banners[3]['link_banner']) ? $banners[3]['link_banner'] :'#'}}">
                                <img class="object-cover overflow-hidden rounded-lg w-80 h-80 opacity-70" alt="" src="{{isset($banners[3]) ? asset($banners[3]['banner']) :''}}">
                            </a>
                            @endif
                            @if (isset($banners[4]))
                            <a href="{{isset($banners[4]['link_banner']) ? $banners[4]['link_banner'] :'#'}}">
                                <img class="object-cover overflow-hidden rounded-lg w-80 h-80 opacity-70" alt="" src="{{isset($banners[4]) ? asset($banners[4]['banner']) :''}}">
                            </a>
                            @endif
                            @if (isset($banners[5]))
                            <a href="{{isset($banners[5]['link_banner']) ? $banners[5]['link_banner'] :'#'}}">
                                <img class="object-cover overflow-hidden rounded-lg w-80 h-80 opacity-70" alt="" src="{{isset($banners[5]) ? asset($banners[5]['banner']) :''}}">
                            </a>
                            @endif

                        </div>
                        <div>
                            <p class="font-bold text-md">Tebaru</p>
                            {{-- @dump($products->toArray()) --}}
                            <div class="hidden md:block lg:block">
                                <div class="grid grid-cols-4 gap-4">
                                    @foreach ($products as $product)
                                    <x-toko.card-detail-toko :act="$product" :type=null></x-toko.card-detail-toko>
                                    @endforeach
                                </div>
                            </div>
                            <div class="block md:hidden lg:hidden">
                                <div class="grid grid-cols-2 gap-4">
                                    @foreach ($products as $product)
                                        <x-toko.card-detail-toko :act="$product" :type=null></x-toko.card-detail-toko>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="layanan" class="pt-14">
                        <div class="container" x-data="{active:'Tur'}">
                            <div class="flex mx-5 sm:block sm:mx-0">
                                <ul class="flex justify-start overflow-x-auto text-sm font-medium text-gray-400 dark:text-gray-300">
                                    <li class="mx-2 mt-2">
                                        <button x-on:click="active='Tur'" aria-current="page" class="inline-block py-2 px-9 rounded-lg active"
                                        :class="active=='Tur' ? 'text-white bg-[#23AEC1]':'text-[#23AEC1] bg-slate-200 hoverLtext-white hover:bg-[#23AEC1]'">Tour</button>
                                    </li>
                                     <li class="mx-2 mt-2">
                                        <button x-on:click="active='Transfer'" aria-current="page" class="inline-block py-2 px-9 rounded-lg active"
                                        :class="active=='Transfer' ? 'text-white bg-[#23AEC1]':'text-[#23AEC1] bg-slate-200 hoverLtext-white hover:bg-[#23AEC1]'">Transfer</button>
                                    </li>
                                     <li class="mx-2 mt-2">
                                        <button x-on:click="active='Rental'" aria-current="page" class="inline-block py-2 px-9 rounded-lg active"
                                        :class="active=='Rental' ? 'text-white bg-[#23AEC1]':'text-[#23AEC1] bg-slate-200 hoverLtext-white hover:bg-[#23AEC1]'">Rental</button>
                                    </li>
                                     <li class="mx-2 mt-2">
                                        <button x-on:click="active='Hotel'" aria-current="page" class="inline-block py-2 px-9 rounded-lg active"
                                        :class="active=='Hotel' ? 'text-white bg-[#23AEC1]':'text-[#23AEC1] bg-slate-200 hoverLtext-white hover:bg-[#23AEC1]'">Hotel</button>
                                    </li>
                                    <li class="mx-2 mt-2">
                                        <button x-on:click="active='Activity'" aria-current="page" class="inline-block py-2 px-9 rounded-lg active"
                                        :class="active=='Activity' ? 'text-white bg-[#23AEC1]':'text-[#23AEC1] bg-slate-200 hoverLtext-white hover:bg-[#23AEC1]'">Activity</button>
                                    </li>
                                    <li class="mx-2 mt-2">
                                        <button x-on:click="active='Xstay'" aria-current="page" class="inline-block py-2 px-9 rounded-lg active"
                                        :class="active=='Xstay' ? 'text-white bg-[#23AEC1]':'text-[#23AEC1] bg-slate-200 hoverLtext-white hover:bg-[#23AEC1]'">Xstay</button>
                                    </li>
                                </ul>
                            </div>
                            <div class="flex justify-items-start justify-start mt-3">
                                <div class="hidden md:block lg:block">
                                    <div x-show="active=='Tur'">
                                        {{-- @dump(count($product_tur)) --}}
                                        @if(count($product_tur) > 0)
                                        <div class="grid grid-cols-4 gap-3 justify-items-start content-start justify-self-start" id="default-view">
                                            @foreach ($product_tur as $key => $product)
                                                <x-toko.card-detail-toko :act="$product" :type=null></x-toko.card-detail-toko>
                                            @endforeach
                                        </div>
                                        {{$product_tur->links()}}
                                        @else
                                            <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                                                Product tidak ditemukan
                                            </div>
                                        @endif
                                    </div>  
                                    <div x-show="active=='Transfer'">
                                        @if(count($product_transfer) > 0)
                                        <div class="grid grid-cols-4 gap-3 justify-items-start content-start justify-self-start" id="default-view">
                                            @foreach ($product_transfer as $product)
                                                <x-toko.card-detail-toko :act="$product" :type=null></x-toko.card-detail-toko>
                                            @endforeach
                                        </div>
                                        {{$product_transfer->links()}}
                                        @else
                                            <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                                                Product tidak ditemukan
                                            </div>
                                        @endif
                                    </div>  
                                    <div x-show="active=='Rental'">
                                        @if(count($product_rental) > 0)
                                        <div class="grid grid-cols-4 gap-3 justify-items-start content-start justify-self-start" id="default-view">
                                            @foreach ($product_rental as $product)
                                                <x-toko.card-detail-toko :act="$product" :type=null></x-toko.card-detail-toko>
                                            @endforeach 
                                        </div>
                                        {{$product_rental->links()}}
                                        @else
                                            <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                                                Product tidak ditemukan
                                            </div>
                                        @endif
                                    </div>  
                                    <div x-show="active=='Hotel'">
                                        @if(count($product_hotel) > 0)
                                        <div class="grid grid-cols-4 gap-3 justify-items-start content-start justify-self-start" id="default-view">
                                            @foreach ($product_hotel as $product)
                                                <x-toko.card-detail-toko :act="$product" :type=null></x-toko.card-detail-toko>
                                            @endforeach
                                        </div>
                                        {{$product_hotel->links()}}
                                        @else
                                            <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                                                Product tidak ditemukan
                                            </div>
                                        @endif
                                    </div>  
                                    <div x-show="active=='Activity'">
                                        @if(count($product_activity) > 0)
                                        <div class="grid grid-cols-4 gap-3 justify-items-start content-start justify-self-start" id="default-view">
                                            @foreach ($product_activity as $product)
                                                <x-toko.card-detail-toko :act="$product" :type=null></x-toko.card-detail-toko>
                                            @endforeach
                                        </div>
                                        {{$product_activity->links()}}
                                        @else
                                            <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                                                Product tidak ditemukan
                                            </div>
                                        @endif
                                    </div>
                                    <div x-show="active=='Xstay'">
                                        @if(count($product_xstay) > 0)
                                        <div class="grid grid-cols-4 gap-3 justify-items-start content-start justify-self-start" id="default-view">
                                            @foreach ($product_xstay as $product)
                                                <x-toko.card-detail-toko :act="$product" :type=null></x-toko.card-detail-toko>
                                            @endforeach
                                        </div>
                                        {{$product_xstay->links()}}
                                        @else
                                            <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                                                Product tidak ditemukan
                                            </div>
                                        @endif
                                    </div>                                    
                                </div>
                                <div class="block md:hidden lg:hidden">       
                                    <div class="grid grid-cols-2 justify-items-center md:hidden lg:hidden">
                                        @foreach ($product_tur as $product)
                                            <x-toko.card-detail-toko :act="$product" :type=null></x-toko.card-detail-toko>
                                        @endforeach
                                    </div>
                                    {{$product_tur->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="ulasan" class="pt-20">
                        <div class="container">
                            @foreach ($reviews as $review)
                            <div class="grid grid-row-5 rounded-lg shadow-lg justify-items-start py-5">
                                <div class="grid grid:row-2 grid-cols-10 gap-2 pl-3">
                                                    @if(isset($review->profile_photo_path))
                                                    <img class="w-20 h-20 rounded-full bg-gray-400" scr="{{isset($review->profile_photo_path) ? asset($review->profile_photo_path):null}}">
                                                    @else
                                                    <div class="w-20 h-20 rounded-full bg-gray-400 relative">
                                                        @php
                                                            $name = $review->first_name;
                                                            $exp_name = explode(' ',$name);
                                                            $inisial ="";

                                                            $inisial = substr($exp_name[0],0,1);

                                                            if(count($exp_name) > 1){
                                                                $inisial .=substr(end($exp_name),0,1);
                                                            }
                                                        @endphp
                                                        @if(count($exp_name) > 1)
                                                        <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 13px;">{{$inisial}}</p>
                                                        @else
                                                        <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 26px;">{{$inisial}}</p>
                                                        @endif
                                                    </div>
                                                    @endif
                                                    <div class="col-span-2 my-auto">
                                                        <p class="font-semibold text-gray-900">{{$review->first_name}}<p/>
                                                        {{-- Carbon::parse($p->created_at)->diffForHumans(); --}}
                                                        <p class="font-light text-gray-600 text-xs">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}<p/>
                                                    </div>
                                                </div>
                                <div class="grid grid-cols-5 pl-3 mb-2">
                                    <div class="col-span-5">
                                        <img class="w-[20px] h-[20] mt-[0.3rem] mr-1  ml-3 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                        @for ($i = 0; $i < ($review->star_rating-1); $i++)
                                            <img class="w-[20px] h-[20] mt-[0.3rem] mr-1 -ml-2 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                        @endfor
                                    </div>
                                </div>
                                <div class="grid grid-cols-5 space-y-5 text-justify pl-3 pr-5">
                                    <div class="col-span-5 pl-3">
                                    <p id="text-ulasan">{{$review->comments}}</p>
                                    </div>
                                </div>
                                <div class="mt-3 pl-3">
                                    <button type="button" id="show-btn-ulasan" class="px-3 py-2 text-black text-sm underline hidden">Tampilkan</button>
                                    <button type="button" id="hide-btn-ulasan" class="px-3 py-2 text-black text-sm underline hidden">Sembunyikan</button>
                                </div>
                                @if($review->gallery_post)
                                <div class="flex justify-start justify-items-center pl-4">
                                    @php
                                        $post_gallery = json_decode($review->gallery_post, true);
                                    @endphp
                                    @if(count($post_gallery['result']) > 0)
                                    <img class="w-50 h-40 bg-slate-400 rounded-lg" src="{{$post_gallery['result'] != null ? Storage::url('gallery_post/'.$post_gallery['result'][0]):'https://via.placeholder.com/540x540'}}">
                                    <div class="relative" x-data="{open:false}" @keydown.escape="open = false">
                                        <button @click="open=true" type="button" id="hide-btn-foto-ulasan" 
                                            class="absolute px-3 py-2 text-black text-sm underline" style="width:169px;top:110px">
                                            Tampilkan semua foto
                                        </button>
                                        <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                                                    style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                                                    <div
                                                        class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                                                        <button
                                                            class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                                            style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                                            <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                                                        </button>
                                                    </div>
                                                    <div class="h-full w-full flex items-center justify-center overflow-hidden"
                                                        x-data="{active: 0, slides: {{ ($review->gallery_post) }} }">
                                                        <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                                            <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                                                <button type="button"
                                                                    class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                                    style="background-color: rgba(230, 230, 230, 0.4);"
                                                                    @click="active = active === 0 ? slides.length - 1 : active - 1">
                                                                    <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                                                </button>
                                                            </div>
                                                        </div>
                                                        @if ($post_gallery)
                                                            @foreach ($post_gallery['result'] as $index=>$gallery)
                                                                <div class="h-full w-full flex items-center justify-center absolute">
                                                                    <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                                                        x-show="active === {{ $index }}"
                                                                        x-transition:enter="transition ease-out duration-150"
                                                                        x-transition:enter-start="opacity-0 transform scale-90"
                                                                        x-transition:enter-end="opacity-100 transform scale-100"
                                                                        x-transition:leave="transition ease-in duration-150"
                                                                        x-transition:leave-start="opacity-100 transform scale-100"
                                                                        x-transition:leave-end="opacity-0 transform scale-90">

                                                                        <img src="{{ Storage::url('gallery_post/'.$gallery) }}"
                                                                            class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                                                    </div>
                                                                    <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                                                        x-show="active === {{ $index }}">
                                                                        <span class="w-12 text-right" x-text="{{ $index }} + 1"></span>
                                                                        <span class="w-4 text-center">/</span>
                                                                        <span class="w-12 text-left"
                                                                            x-text="{{ count($post_gallery['result']) }}"></span>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                        
                                                        <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                                            <div class="flex items-center justify-start w-12 md:ml-16">
                                                                <button type="button"
                                                                    class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                                    style="background-color: rgba(230, 230, 230, 0.4);"
                                                                    @click="active = active === slides.length - 1 ? 0 : active + 1">
                                                                    <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                @endif
                            </div>  
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mx-auto my-5" id="info toko">
        <div class="w-full h-full border border-gray-50 rounded-lg shadow-md p-5">
            <label class="font-semibold">Deskripsi Toko</label>
            <p>{{isset($toko->deskripsi) ? $toko->deskripsi:''}}</p>
            <label class="font-semibold">Bergabung Sejak</label>
            <p>{{isset($dekorasi) ? Carbon\Carbon::parse($dekorasi->created_at)->toFormattedDateString():Carbon\Carbon::parse(now())->toFormattedDateString()}}</p>
            <label class="font-semibold">Kategori Seller</label>
            <p>{{$dekorasi->jenis_registrasi}}</p>
            <label class="font-semibold">Lesensi Seller</label>
            <p>{{$ls}}</p>
            <div class="mt-3">
                <label class="font-semibold">Deskripsi Tambahan</label>
                <p>
                {{isset($toko->deskripsi_tambahan) ? $toko->deskripsi_tambahan:''}}
                </p>
            </div>
        </div>
    </div>
    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        var turAboutSwiper = new Swiper(".tur-about-swiper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
                enabled: true,
            },
            breakpoints: {
            640: {
                slidesPerView: 3,
                navigation: {
                    enabled: false
                },
            }
        }
    });

    var reviewSwipper = new Swiper(".review-swipper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
    });
    /*
    $.ajax({
        type:'GET',
        url:'/get/layanan',
        id:{
            type
        },
        function:success(resp){

        },
        function:error(data){

        }
    })
    */

    </script>
</body>

</html>
