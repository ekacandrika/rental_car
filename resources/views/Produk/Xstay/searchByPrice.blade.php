<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Search Tur') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Hero --}}
    <div class="relative bg-slate-400">
        <img class="w-full max-h-[317px] opacity-70 object-cover"
            src="https://images.unsplash.com/photo-1615278166719-c411455d594d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzZ8fHRvdXJ8ZW58MHx8MHx8&auto=format&fit=crop&w=1920&q=60"
            alt="banner-tur">
    </div>

    <div class="container mx-auto my-5 text-center">
        <x-produk.cari-xstay>
            <select name="search" id="" style="background-image: none;"
                class="border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7 mt-2">
                <option value="">Dari</option>
                @foreach ($xstay as $value)
                    @php
                        $regency = App\Models\Regency::where('id', $value->productdetail->regency_id)->get();
                    @endphp
                    @foreach ($regency as $item)
                        <option style="font-size: 10px" value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                @endforeach
            </select>
        </x-produk.cari-xstay>
    </div>

    {{-- Content --}}
    <div class="container mx-auto my-5">
        <div class="grid grid-cols-1 lg:grid-cols-12 md:grid-cols-12 gap-10">
            <div class="col-span-4">
                <div class="m-5 p-5 shadow-lg">
                    <p class="text-2xl font-medium">Filter</p>
                    <p class="my-3">Harga</p>
                    <form action="{{ route('xstay.searchByPrice') }}" method="POST">
                        @csrf
                        <input type="text" name="min_price" placeholder="Harga Minimum"
                            class="placeholder:text-slate-400 mb-3 block bg-[#F2F2F2] w-full border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-[#9E3D64] focus:ring-[#9E3D64] focus:ring-1 sm:text-sm">
                        <input type="text" name="max_price" placeholder="Harga Maksimum"
                            class="placeholder:text-slate-400 mb-3 block bg-[#F2F2F2] w-full border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-[#9E3D64] focus:ring-[#9E3D64] focus:ring-1 sm:text-sm">
                        <input type="hidden" name="search" value="{{ $data }}">
                        <button
                            class="px-2 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white ">
                            Terapkan
                        </button>
                    </form>
                </div>
            </div>
            <div class="col-span-8">
                <div class="grid grid-cols-2 p-5">
                    <div>
                        <p class="my-5">Menampilkan {{ $data_detail->count() }} XStay</p>
                    </div>
                    <div class="mt-4">
                        <select class="py-1 pr-4 rounded-lg border border-black ring-black w-full" name=""
                            id="">
                            <option value="">(Harga Termurah - Termahal)</option>
                        </select>
                    </div>
                </div>
                <div class="grid grid-cols-1 py-5 mr-2">
                    @foreach ($data_detail as $item)
                        @php
                            $regency = App\Models\Regency::where('id', $item->regency_id)->first();
                            $kamar = $item->masterkamar;
                        @endphp
                        @if ($kamar && $kamar->harga_kamar >= $min_price && $kamar->harga_kamar <= $max_price)
                            <a href="{{ route('xstay.show', $item->product->slug) }}">
                                <div
                                    class="grid grid-cols-1 md:grid-cols-[20%_50%_30%] lg:md:grid-cols-[15%_55%_30%] bg-white shadow-xl p-2 rounded-xl my-5 border border-[#E0E0E0]">
                                    <div class="grid content-center">
                                        <img class="w-full h-[190px]md:w-[110px] lg:w-[110px] rounded-lg object-cover object-center"
                                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                            alt="tur-1">
                                    </div>
                                    <div class="px-3 grid text-center md:text-start lg:text-start py-3 md:py-0 lg:py-0">
                                        <p class="font-semibold text-[18px]">{{ $item->product->product_name }}</p>
                                        <p class="text-[12px] mt-5">{{ $regency->name }}</p>
                                        <p class="text-[12px] font-semibold">Rating 4.7 <span class="font-normal">(12
                                                Ulasan)</span>
                                        </p>
                                    </div>
                                    <div class="grid text-center md:text-end lg:text-end">
                                        <p class="text-kamtuu-second text-[20px] font-semibold">IDR
                                            {{ number_format($kamar->harga_kamar) }}</p>
                                        <p>Refundable</p>
                                        <p class="text-[#D50006] text-[12px]">Sisa {{ $kamar->stok }} Kamar</p>
                                    </div>
                                </div>
                            </a>
                        @endif
                    @endforeach
                    {{-- <div
                        class="grid grid-cols-1 md:grid-cols-[20%_50%_30%] lg:md:grid-cols-[15%_55%_30%] bg-white shadow-xl p-2 rounded-xl my-5 border border-[#E0E0E0]">
                        <div class="grid content-center">
                            <img class="w-full h-[190px]md:w-[110px] lg:w-[110px] rounded-lg object-cover object-center"
                                src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                alt="tur-1">
                        </div>
                        <div class="px-3 grid text-center md:text-start lg:text-start py-3 md:py-0 lg:py-0">
                            <p class="font-semibold text-[18px]">Xstay Staycation</p>
                            <p class="text-[12px] mt-5">Yogyakarta</p>
                            <p class="text-[12px] font-semibold">Rating 4.7 <span class="font-normal">(12 Ulasan)</span>
                            </p>
                        </div>
                        <div class="grid text-center md:text-end lg:text-end">
                            <p class="text-kamtuu-second text-[20px] font-semibold">IDR 100.000</p>
                            <p>Refundable</p>
                            <p class="text-[#D50006] text-[12px]">Sisa 2 Kamar</p>
                        </div>
                    </div>
                    <div
                        class="grid grid-cols-1 md:grid-cols-[20%_50%_30%] lg:md:grid-cols-[15%_55%_30%] bg-white shadow-xl p-2 rounded-xl my-5 border border-[#E0E0E0]">
                        <div class="grid content-center">
                            <img class="w-full h-[190px]md:w-[110px] lg:w-[110px] rounded-lg object-cover object-center"
                                src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                alt="tur-1">
                        </div>
                        <div class="px-3 grid text-center md:text-start lg:text-start py-3 md:py-0 lg:py-0">
                            <p class="font-semibold text-[18px]">Xstay Staycation</p>
                            <p class="text-[12px] mt-5">Yogyakarta</p>
                            <p class="text-[12px] font-semibold">Rating 4.7 <span class="font-normal">(12 Ulasan)</span>
                            </p>
                        </div>
                        <div class="grid text-center md:text-end lg:text-end">
                            <p class="text-kamtuu-second text-[20px] font-semibold">IDR 100.000</p>
                            <p>Not Refundable</p>
                            <p class="text-[#D50006] text-[12px]">Sisa 2 Kamar</p>
                        </div>
                    </div>
                    <div
                        class="grid grid-cols-1 md:grid-cols-[20%_50%_30%] lg:md:grid-cols-[15%_55%_30%] bg-white shadow-xl p-2 rounded-xl my-5 border border-[#E0E0E0]">
                        <div class="grid content-center">
                            <img class="w-full h-[190px]md:w-[110px] lg:w-[110px] rounded-lg object-cover object-center"
                                src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                alt="tur-1">
                        </div>
                        <div class="px-3 grid text-center md:text-start lg:text-start py-3 md:py-0 lg:py-0">
                            <p class="font-semibold text-[18px]">Xstay Staycation</p>
                            <p class="text-[12px] mt-5">Yogyakarta</p>
                            <p class="text-[12px] font-semibold">Rating 4.7 <span class="font-normal">(12 Ulasan)</span>
                            </p>
                        </div>
                        <div class="grid text-center md:text-end lg:text-end">
                            <p class="text-kamtuu-second text-[20px] font-semibold">IDR 100.000</p>
                            <p>Not Refundable</p>
                            <p class="text-[#D50006] text-[12px]">Sisa 2 Kamar</p>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        var turAboutSwiper = new Swiper(".tur-about-swiper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
                enabled: true,
            },
            breakpoints: {
                640: {
                    slidesPerView: 3,
                    navigation: {
                        enabled: false
                    },
                }
            }
        });

        var reviewSwipper = new Swiper(".review-swipper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    </script>
</body>

</html>
