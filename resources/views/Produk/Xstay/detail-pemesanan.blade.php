<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tur Detail') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    <style>
        input[type='number']::-webkit-outer-spin-button,
        input[type='number']::-webkit-inner-spin-button,
        input[type='number'] {
            -webkit-appearance: none;
            margin: 0;
            -moz-appearance: textfield !important;
        }
    </style>
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border border-b-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    <div class="container mx-auto my-5">
        <div class="grid grid-cols-6 lg:gap-5">
            {{-- Mobile Information --}}
            <div class="col-span-6 p-3">
                <div class="block lg:hidden border border-gray-200 rounded-md shadow-lg p-5">
                    <div class="flex">
                        <img class="rounded-md object-cover object-center shadow-md mr-5 w-20 h-20"
                            src="{{ asset('storage/img/tur-detail-5.jpg') }}" alt="tur-img">
                        <div>
                            <p class="text-base md:text-lg lg:text-xl font-semibold">Labuan Bajo</p>
                            <p class="text-sm md:text-base font-normal">Nama Toko</p>
                        </div>
                    </div>
                    <div class="mt-5 text-sm sm:text-base">
                        <p>Rabu, 10 Agustus 2022</p>
                        <p>Trip Privat</p>
                        <p>2 Dewasa</p>
                        <p>1 Anak</p>
                    </div>
                </div>
            </div>
            {{-- Left Side --}}
            <div class="col-span-6 lg:col-span-4 bg-white p-3">

                {{-- Peserta 1 --}}
                <form action="{{ route('booking.store.xstay') }}" method="POST" x-data="xstayDetail()"
                    enctype="multipart/form-data">
                    @csrf
                    @foreach (json_decode($detail['rooms']) as $room_key => $room_value)
                    <div class="rounded-md shadow-sm border border-gray-200 py-5 px-10 mb-5"
                        x-data="{ tur_form: { first_name: '', last_name: '', birth_date: '', jenis_kelamin: 'male', residen: false, non_residen: false, nationality: 'indonesia', country_code: '+62', phone_number: '', email: '', residen_status: 'residen', kitas: '', same_as_user: false } }">
                        <div class="block sm:flex sm:justify-between my-2">
                            <p class="text-2xl font-semibold">Detail Tamu {{ $room_key + 1 }}</p>
                            @if ($room_key == 0)
                            <p class="text-sm font-medium">ID Booking: {{ $code }}</p>
                            @endif
                        </div>
                        <div class="block sm:flex sm:justify-between my-5 sm:my-2">
                            <p class="text-base font-semibold">{{ $detail['product_name'] }}: <span
                                    class="text-xs font-medium">{{ $room_value->dewasa }} Dewasa,
                                    {{ $room_value->anak > 0 ? $room_value->anak . ' Anak-anak, ' : '' }} {{
                                    $detail['room_name'] }}</span>
                            </p>
                            <button class="text-[#D50006] text-sm font-medium"
                                x-on:click="tur_form.same_as_user = false" type="button">Hapus
                                Semua
                            </button>
                        </div>
                        @if ($room_key == 0)
                        <div class="my-5 flex items-center">
                            <input class="rounded-sm mr-2" type="checkbox" id="identity" x-model="tur_form.same_as_user"
                                x-init="$watch('tur_form.same_as_user', (isChecked) => {
                                if (isChecked) {
                                    tur_form.first_name = '{{ auth()->user()->first_name }}';
                                    tur_form.last_name = '{{ $profile->last_name }}';
                                    tur_form.birth_date = '{{ Carbon\Carbon::parse($profile->date_of_birth)->translatedFormat('d/m/Y') }}';
                                    tur_form.jenis_kelamin = '{{ $profile->gender }}';
                                    tur_form.nationality = '{{ $profile->citizenship }}';
                                    tur_form.country_code = '+62';
                                    tur_form.phone_number = '{{ $profile->phone_number_booking }}';
                                    tur_form.email = '{{ $profile->email_booking }}';
                                    tur_form.residen = '{{ $profile->status_residen }}';
                                } else {
                                    tur_form.first_name = '';
                                    tur_form.last_name = '';
                                    tur_form.birth_date = '';
                                    tur_form.jenis_kelamin = 'male';
                                    tur_form.nationality = 'indonesia';
                                    tur_form.country_code = '+62';
                                    tur_form.phone_number = '';
                                    tur_form.email = '';
                                    tur_form.residen = '';
                                    tur_form.kitas = '';
                                }
                            })">
                            <label for="identity" class="text-sm sm:text-base text-black">Sama seperti
                                pemilik akun</label>
                        </div>
                        @endif

                        {{-- Name --}}
                        <div class="grid grid-rows-1 my-2">
                            <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                                <div class="relative block">
                                    <label for="first_name"
                                        class="form-label inline-block mb-2 text-gray-700 text-base font-medium">Nama
                                        Depan
                                        & Tengah</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="Nama depan" type="text" name="first_name[]" id="first_name"
                                        x-model="tur_form.first_name" required />
                                    <input type="hidden" name="kategori_peserta[]" value="dewasa">
                                </div>
                                <div class="relative block">
                                    <label for="last_name"
                                        class="form-label inline-block mb-2 text-gray-700 text-base font-medium">Nama
                                        Belakang</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="Nama belakang" type="text" name="last_name[]" id="last_name"
                                        x-model="tur_form.last_name" required />
                                </div>
                            </div>
                        </div>

                        {{-- Identity --}}
                        <div class="grid grid-rows-1 my-2">
                            <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                                <div class="relative block">
                                    <label for="birth_date" class="form-label inline-block mb-2 text-gray-700">Tanggal
                                        Lahir</label>
                                    <div
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                        <input placeholder="Pilih tanggal" type="text" name="birth_date[]"
                                            :id="'birth_date-'+{{ $room_key + 1 }}" x-data="{id:$id('birth_date')}"
                                            x-init="initDatePickerHandler(id)" x-model="tur_form.birth_date" required />
                                    </div>
                                </div>

                                <div class="relative block">
                                    <label for="jenis_kelamin" class="form-label inline-block mb-2 text-gray-700">Jenis
                                        Kelamin</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                            name="jenis_kelamin[]" id="jenis_kelamin" x-model="tur_form.jenis_kelamin"
                                            required>
                                            <option value="">Pilih</option>
                                            <option value="Laki-Laki">Pria</option>
                                            <option value="Perempuan">Wanita</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sm:grid sm:grid-cols-2 gap-5">
                            <div>

                                {{-- Residen - Non Residen --}}
                                {{-- <div class="sm:flex my-2 space-y-2 sm:space-y-0">
                                    <div class="flex items-center mr-5">
                                        <input class="rounded-sm form-checkbox mr-2" type="checkbox" name="residen"
                                            id="residen" x-model="tur_form.residen">
                                        <label for="residen" class="text-black">Residen</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input class="rounded-sm form-checkbox mr-2" type="checkbox" name="non-residen"
                                            id="non-residen" x-model="tur_form.non_residen">
                                        <label for="non_residen" class="text-black">Non Residen</label>
                                    </div>
                                </div> --}}

                                {{-- Nationality --}}
                                <div class="relative block my-2">
                                    <label for="nationality"
                                        class="form-label inline-block mb-2 text-gray-700">Kebangsaan</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                            name="nationality[]" id="nationality" x-model="tur_form.nationality"
                                            required>
                                            <option selected value="indonesia">Indonesia</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                </div>

                                {{-- Phone Number --}}
                                <div class="relative block my-2">
                                    <label for=" phone-number" class="form-label inline-block mb-2 text-gray-700">No.
                                        HP/WhatsApp</label>
                                    <div class="flex">
                                        <template x-if="tur_form.nationality == 'indonesia'">
                                            <input
                                                class="border-[#828282] rounded-l-md shadow-sm sm:text-sm w-[50px] focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-l-md bg-white"
                                                type="text" readonly value="62" />
                                        </template>
                                        <input
                                            :class="tur_form.nationality == 'indonesia' ? 'rounded-r-md' : 'rounded-md'"
                                            class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                            :placeholder="tur_form.nationality == 'other' ? '6081234567890' : '81234567890'"
                                            type="text" name="phone_number[]" id="phone_number"
                                            x-model="tur_form.phone_number" required />
                                    </div>
                                    <template x-if="tur_form.nationality == 'other'">
                                        <p class="py-2 text-xs text-kamtuu-primary">Please write phone number with your
                                            country
                                            code.</p>
                                    </template>
                                </div>

                                {{-- Email --}}
                                <div class="relative block my-2">
                                    <label for=" email" class="form-label inline-block mb-2 text-gray-700">Email</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="emailku@gmail.com" type="text" name="email[]" id="email"
                                        x-model="tur_form.email" required />
                                </div>

                                {{-- Residen --}}
                                <div class="relative block my-2">
                                    <label for=" residen-status"
                                        class="form-label inline-block mb-2 text-gray-700">Status
                                        Residen</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                            name="residen_status[]" id="residen_status" x-model="tur_form.residen"
                                            required>
                                            <option value="">Pilih</option>
                                            <option value="residen">Residen</option>
                                            <option value="non_residen">Non Residen</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <p class="my-2">KTP/KITAS</p>
                                <div x-data="displayImage()">
                                    <div class="mb-2">
                                        <template x-if="imageUrl">
                                            <img :src="imageUrl"
                                                class="object-contain rounded border border-gray-300 w-full h-56">
                                        </template>

                                        <template x-if="!imageUrl">
                                            <div
                                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-full h-56">
                                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                    width="20px" height="20px">
                                            </div>
                                        </template>

                                        <input class="mt-2" name="kitas[]" type="file" accept="image/*"
                                            @change="selectedFile" x-model="tur_form.kitas" required>
                                    </div>
                                </div>
                                <p class="text-xs font-normal">Catatan: Beberapa seller mungkin menerapkan kebijakan
                                    harga
                                    berbeda antara residen dan
                                    non-residen</p>
                            </div>
                        </div>
                    </div>
                    @endforeach


                    {{-- Peserta 2 --}}
                    <div class="rounded-md shadow-sm border border-gray-200 py-5 px-10 mb-5">
                        {{-- Tambahan --}}
                        <p class="my-3 text-2xl font-semibold">Ekstra</p>
                        <table class="w-full text-sm text-left">
                            <thead>
                                <tr>
                                    <th>Nama Ekstra</th>
                                    <th>Jumlah Ekstra</th>
                                    <th>Total Harga Ekstra</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- @if ($order['extra'] != null)
                                @php
                                $total = 0;
                                $total_extra = 0;
                                @endphp
                                @foreach ($order['extra'] as $key => $value)
                                @php
                                $total_harga = $total_extra + $value['jumlah_extra'] * $value['harga_extra'];
                                $data_ekstra = App\Models\Extra::where('id', $value['id_extra'])->first();
                                $total += $total_harga;
                                @endphp --}}
                                <tr>
                                    <td>
                                        <p>
                                            {{ $detail['nama_ekstra'] }}
                                        </p>
                                    </td>
                                    <td>
                                        <p>
                                            {{ $detail['total_pengunjung'] }}
                                        </p>
                                    </td>
                                    <td>
                                        <p>
                                            Rp. {{ number_format($detail['total_harga_ekstra']) }}
                                        </p>
                                    </td>
                                </tr>
                                {{-- @endforeach
                                @endif --}}
                            </tbody>
                        </table>
                    </div>

                    <div>
                        <p class="font-semibold">Sebelum mengonfirmasi pesanan Anda, harap untuk memastikan kembali
                            bahwa
                            data
                            yang
                            anda masukkan sudah akurat.</p>
                    </div>
                    <div class="w-full">

                        <input type="hidden" name="customer_id" value="{{ $profile->id }}">
                        <input type="hidden" name="harga_ekstra" value="{{ $detail['harga_ekstra'] ?? null }}">
                        <input type="hidden" name="nama_ekstra" value="{{ $detail['nama_ekstra'] ?? null}}">
                        <input type="hidden" name="jumlah_ekstra" value="{{ $detail['total_pengunjung'] ?? null}}">
                        <input type="hidden" name="product_detail_id" value="{{ $detail['product_detail_id'] }}">
                        <input type="hidden" name="total_price" :value="$store.order.final_price">
                        <input type="hidden" name="toko_id" value="{{ $detail['toko_id'] }}">
                        <input type="hidden" name="booking_code" value="{{ $code }}">
                        <input type="hidden" name="type" value="{{ $detail['type'] }}">
                        <input type="hidden" name="kamar_id" value="{{ $detail['room_id'] }}">
                        <input type="hidden" name="check_in_date" value="{{ $detail['check_in_date'] }}">
                        <input type="hidden" name="check_out_date" value="{{ $detail['check_out_date'] }}">
                        <input type="hidden" name="peserta_dewasa" value="{{ $detail['total_dewasa'] }}">
                        <input type="hidden" name="peserta_anak_anak" value="{{ $detail['total_anak'] }}">
                        <input type="hidden" name="refundable" value="{{ $detail['refundable'] }}">
                        <input type="hidden" name="refundable_amount" value="{{ $detail['refundable_amount'] }}">
                        <input type="hidden" name="refundable_price" value="{{ $detail['refundable_price'] }}">
                        {{-- <input type="hidden" name="peserta_balita" value="{{ $detail['peserta_balita'] }}"> --}}
                        {{-- @if ($detail['ekstra_sarapan'] != null)
                        <input type="hidden" name="ekstra_sarapan" value="{{ $detail['ekstra_sarapan'] }}">
                        @endif --}}
                        <input type="hidden" :value="$store.order.product_price" name="harga_product" />
                        <input type="hidden" :value="$store.order.diskon_by_date" name="diskon_per_tanggal" />
                        <input type="hidden" :value="$store.order.surcharge_by_date" name="surcharge_per_tanggal" />
                        <input type="hidden" :value="$store.order.price_by_date" name="harga_per_tanggal" />
                        <input type="hidden" name="diskon_grup" :value="$store.order.diskon_grup" />
                        <input type="hidden" :value="$store.order.price_by_group" name="harga_akhir" />
                        {{-- <input type="hidden" x-model="$store.order.pilihan" name="harga_pilihan" /> --}}
                        {{-- <input type="hidden" x-model="$store.order.final_price" name="total_price" /> --}}
                        <input type="hidden" value="{{ $detail['room_total'] }}" name="jumlah_kamar">
                        <input type="hidden" :value="$store.order.days" name="jumlah_malam">

                        @php
                        $detail_produk = App\Models\Productdetail::where('id', $detail['product_detail_id'])->first();
                        @endphp
                        <input type="hidden" value={{ $detail_produk->confirmation ?? 'by_seller' }}
                        name="confirm_order" />

                        @if (auth()->user()->role == 'agent')
                        <input type="hidden" name="agent_id" value="{{ $agent_id->id }}">
                        <input type="hidden" name="role" value="{{ $agent_id->role }}">
                        @endif
                        <button
                            class="w-full bg-[#23AEC1] text-white text-base font-semibold py-2 rounded-md hover:bg-[#3ac0d2] duration-200">
                            Simpan
                            di Koper
                        </button>
                    </div>
                </form>
            </div>


            {{-- Right Side --}}
            <div class="col-span-6 lg:col-span-2 space-y-5 p-3">

                {{-- Information --}}
                <div class="hidden lg:block border border-[#23AEC1] rounded-md shadow-lg p-5">
                    <div class="flex">
                        <img class="rounded-md object-cover object-center shadow-md mr-5 w-20 h-20"
                            src="{{ $detail['foto_kamar'] != '' ? asset( $detail['foto_kamar'] ) : asset('storage/img/tur-detail-5.jpg') }}"
                            alt="tur-img">
                        <div>
                            <p class="text-xl font-semibold">{{ $detail['product_name'] }}</p>
                            @php
                            $timestamp1 = strtotime($detail['check_in_date']);
                            $timestamp2 = strtotime($detail['check_out_date']);
                            $diff = $timestamp2 - $timestamp1;
                            $days = floor($diff / (60 * 60 * 24));
                            @endphp
                            <p class="text-base font-normal">{{ $days }} Malam</p>
                        </div>
                    </div>
                    <div class="grid grid-cols-2">
                        <div class="mt-5">
                            <p>Check-in</p>
                            <p>Check-out</p>
                        </div>
                        <div class="mt-5">
                            <p> {{ Carbon\Carbon::parse($detail['check_in_date'])->translatedFormat('d F Y') }} </p>
                            <p> {{ Carbon\Carbon::parse($detail['check_out_date'])->translatedFormat('d F Y') }} </p>
                        </div>
                    </div>
                </div>

                {{-- Kupon --}}
                {{-- <div>KUPON</div> --}}

                {{-- Biaya --}}
                <div class="border border-[#23AEC1] rounded-md shadow-lg p-5">
                    <p class="text-xl font-semibold mb-2">Rincian Biaya</p>
                    <div class="space-y-1 my-2">
                        <p class="text-base font-semibold">Harga</p>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">{{ $detail['room_total'] . ' kamar x ' . $days . ' malam'}}</p>
                            <p class="text-[#23AEC1]">Rp.
                                {{ number_format($detail['total_harga_kamar_refund']) }}
                            </p>
                        </div>
                    </div>
                    {{-- @if ($profile->status_residen == 'residen')
                    @php
                    $detail_product = App\Models\Productdetail::where('id', $detail['product_detail_id'])->first();
                    $Residen = App\Models\Price::where('id', $detail_product->harga_id)->first();
                    @endphp
                    <div class="space-y-1 my-2">
                        <p class="text-base font-semibold">Harga Residen</p>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Dewasa</p>
                            <p class="text-[#23AEC1]">Rp.
                                {{ number_format($Residen->dewasa_residen * $detail['peserta_dewasa']) }}
                            </p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Anak-anak</p>
                            <p class="text-[#23AEC1]">Rp.
                                {{ number_format($Residen->anak_residen * $detail['peserta_anak_anak']) }}
                            </p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Balita</p>
                            <p class="text-[#23AEC1]">Rp.
                                {{ number_format($Residen->balita_residen * $detail['peserta_balita']) }}
                            </p>
                        </div>
                    </div>
                    @endif
                    @if ($profile->status_residen == 'non-residen')
                    @php
                    $detail_product = App\Models\Productdetail::where('id', $detail['product_detail_id'])->first();
                    $nonResiden = App\Models\Price::where('id', $detail_product->harga_id)->first();
                    @endphp
                    <div class="space-y-1 my-2">
                        <p class="text-base font-semibold">Harga Non-Residen</p>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Dewasa</p>
                            <p class="text-[#23AEC1]">Rp.
                                {{ number_format($nonResiden->dewasa_non_residen * $detail['peserta_dewasa']) }}
                            </p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Anak-anak</p>
                            <p class="text-[#23AEC1]">Rp.
                                {{ number_format($nonResiden->anak_non_residen * $detail['peserta_anak_anak']) }}
                            </p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Balita</p>
                            <p class="text-[#23AEC1]">Rp.
                                {{ number_format($nonResiden->balita_non_residen * $detail['peserta_balita']) }}
                            </p>
                        </div>
                    </div>
                    @endif --}}

                    <hr class="border border-[#707070] my-5" />

                    <div class="space-y-1 my-2">
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-base font-semibold">Harga Xstay</p>
                            <p class="text-[#23AEC1] text-base font-semibold">Rp.
                                {{ number_format($detail['total_harga_kamar_refund']) }}</p>
                        </div>
                        @php
                        $diskon_tanggal = $detail['total_harga_kamar_refund'] * ($detail['diskon_by_date'] / 100);
                        $harga_diskon_tanggal = $detail['total_harga_kamar_refund'] + $diskon_tanggal;
                        @endphp

                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Diskon per tanggal</p>
                            <p class="text-[#23AEC1]">- Rp. {{ $detail['diskon_by_date'] < 0 ?
                                    number_format($diskon_tanggal) : 0 }} </p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Surcharge per tanggal</p>
                            <p class="text-[#23AEC1]">Rp. {{ $detail['diskon_by_date'] > 0 ?
                                number_format($diskon_tanggal) : 0 }}</p>
                            {{-- <p class="text-[#23AEC1]">
                                @if ($profile->status_residen == 'non-residen')
                                @php
                                $detail_product = App\Models\Productdetail::where('id',
                                $detail['product_detail_id'])->first();
                                $nonResiden = App\Models\Price::where('id', $detail_product->harga_id)->first();
                                $nonResidenDewasa = $nonResiden->dewasa_non_residen * $detail['peserta_dewasa'];
                                $nonResidenAnak = $nonResiden->anak_non_residen * $detail['peserta_anak_anak'];
                                $nonResidenBalita = $nonResiden->balita_non_residen * $detail['peserta_balita'];
                                $totalHargaNonResiden = $nonResidenDewasa + $nonResidenAnak + $nonResidenBalita;
                                $totalnonres = $detail['total'] + $totalHargaNonResiden;
                                $val = intval($detail['diskon']) / 100;
                                $ab = intval($totalnonres) * $val;
                                @endphp
                                @if (isset($total))
                                @php
                                $totalHargaNonResiden = $nonResidenDewasa + $nonResidenAnak + $nonResidenBalita;
                                $totalekstra = $detail['total'] + $total + $totalHargaNonResiden;
                                $val = intval($detail['diskon']) / 100;
                                $ab = intval($totalekstra) * $val;
                                @endphp
                                - Rp. {{number_format($ab)}}
                                @else
                                - Rp. {{number_format($ab)}}
                                @endif
                                @endif
                                @if ($profile->status_residen == 'residen')
                                @php
                                $detail_product = App\Models\Productdetail::where('id',
                                $detail['product_detail_id'])->first();
                                $Residen = App\Models\Price::where('id', $detail_product->harga_id)->first();
                                $ResidenDewasa = $Residen->dewasa_residen * $detail['peserta_dewasa'];
                                $ResidenAnak = $Residen->anak_residen * $detail['peserta_anak_anak'];
                                $ResidenBalita = $Residen->balita_residen * $detail['peserta_balita'];
                                $totalHargaResiden = $ResidenDewasa + $ResidenAnak + $ResidenBalita;
                                $totalres = $detail['total'] + $totalHargaResiden;
                                $val = intval($detail['diskon']) / 100;
                                $ab = intval($totalres) * $val;
                                @endphp
                                @if (isset($total))
                                @php
                                $totalHargaResiden = $ResidenDewasa + $ResidenAnak + $ResidenBalita;
                                $totalekstra = $detail['total'] + $total + $totalHargaResiden;
                                $val = intval($detail['diskon']) / 100;
                                $ab = intval($totalekstra) * $val;
                                @endphp
                                - Rp. {{number_format($ab)}}
                                @else
                                - Rp. {{number_format($ab)}}
                                @endif
                                @endif
                            </p> --}}
                        </div>
                    </div>

                    <hr class="border border-[#707070] my-5" />

                    <div class="space-y-1 my-2">
                        <div class="flex justify-between">
                            <p class="text-base font-semibold">Harga per Tanggal</p>
                            <p class="text-[#23AEC1] text-base font-semibold">Rp. {{
                                number_format($harga_diskon_tanggal) }}</p>
                        </div>
                        @php
                        $diskon_group = $harga_diskon_tanggal * ($detail['diskon_by_group'] / 100);
                        $harga_diskon_group = $harga_diskon_tanggal + $diskon_group;
                        @endphp

                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Diskon Grup</p>
                            <p class="text-[#23AEC1]">- Rp. {{ number_format($diskon_group) ?? 0 }}</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Harga Bersih</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Kupon Toko</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                    </div>

                    <hr class="border border-[#707070] my-5" />

                    @php
                    $harga_total_without_tax = $harga_diskon_group + $detail['total_harga_ekstra'];
                    $total_pajak = $harga_total_without_tax * ($detail['pajak']/100);
                    $harga_total = $harga_diskon_group + $detail['total_harga_ekstra'] + $total_pajak;
                    @endphp

                    <div class="space-y-1 my-2">
                        <div class="flex justify-between">
                            <p class="text-base font-semibold">Harga Akhir</p>
                            <p class="text-base font-semibold text-[#23AEC1]">
                                {{-- @if (isset($total))
                                Rp. {{ number_format($total) }}
                                @else
                                Rp. 0
                                @endif --}}
                                Rp. {{ number_format($harga_diskon_group) ?? 0 }}
                            </p>
                        </div>

                        {{-- <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Pilihan</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div> --}}
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Extra</p>
                            <p class="text-[#23AEC1]">
                                Rp. {{ number_format($detail['total_harga_ekstra']) ?? 0 }}
                            </p>
                            {{-- <p class="text-[#23AEC1]">
                                @if (isset($total))
                                Rp. {{ number_format($total) }}
                                @else
                                Rp. 0
                                @endif
                            </p> --}}
                        </div>
                        {{-- <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Biaya Lain-lain</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Biaya Pemesanan</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div> --}}
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Pajak</p>
                            <p class="text-[#23AEC1]">Rp. {{ number_format($total_pajak) ?? 0 }}</p>
                        </div>
                        {{-- <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Promo</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#828282]">Kupon</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div> --}}
                    </div>

                    <hr class="border border-[#707070] my-5" />

                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-lg font-semibold">Total Bayar</p>
                        <p class="text-lg font-semibold text-[#23AEC1]">
                            Rp. {{ number_format($harga_total) ?? 0 }}
                        </p>
                        {{-- @if ($profile->status_residen == 'non-residen')
                        @php
                        $detail_product = App\Models\Productdetail::where('id', $detail['product_detail_id'])->first();
                        $nonResiden = App\Models\Price::where('id', $detail_product->harga_id)->first();
                        $nonResidenDewasa = $nonResiden->dewasa_non_residen * $detail['peserta_dewasa'];
                        $nonResidenAnak = $nonResiden->anak_non_residen * $detail['peserta_anak_anak'];
                        $nonResidenBalita = $nonResiden->balita_non_residen * $detail['peserta_balita'];
                        $totalHargaNonResiden = $nonResidenDewasa + $nonResidenAnak + $nonResidenBalita;
                        $totalnonres = $detail['total'] + $totalHargaNonResiden;
                        $val = intval($detail['diskon']) / 100;
                        $ab = intval($totalnonres) * $val;
                        @endphp
                        <p class="text-lg font-semibold text-[#23AEC1]">
                            @if (isset($total))
                            @php
                            $totalHargaNonResiden = $nonResidenDewasa + $nonResidenAnak + $nonResidenBalita;
                            $totalekstra = $detail['total'] + $total + $totalHargaNonResiden;
                            $val = intval($detail['diskon']) / 100;
                            $ab = intval($totalekstra) * $val;
                            @endphp
                            Rp. {{ number_format($totalekstra - $ab) }}
                            @else
                            Rp. {{ number_format($totalnonres - $ab) }}
                            @endif
                        </p>
                        @endif
                        @if ($profile->status_residen == 'residen')
                        @php
                        $detail_product = App\Models\Productdetail::where('id', $detail['product_detail_id'])->first();
                        $Residen = App\Models\Price::where('id', $detail_product->harga_id)->first();
                        $ResidenDewasa = $Residen->dewasa_residen * $detail['peserta_dewasa'];
                        $ResidenAnak = $Residen->anak_residen * $detail['peserta_anak_anak'];
                        $ResidenBalita = $Residen->balita_residen * $detail['peserta_balita'];
                        $totalHargaResiden = $ResidenDewasa + $ResidenAnak + $ResidenBalita;
                        $totalres = $detail['total'] + $totalHargaResiden;
                        $val = intval($detail['diskon']) / 100;
                        $ab = intval($totalres) * $val;
                        @endphp
                        <p class="text-lg font-semibold text-[#23AEC1]">
                            @if (isset($total))
                            @php
                            $totalHargaResiden = $ResidenDewasa + $ResidenAnak + $ResidenBalita;
                            $totalekstra = $detail['total'] + $total + $totalHargaResiden;
                            $val = intval($detail['diskon']) / 100;
                            $ab = intval($totalekstra) * $val;
                            @endphp
                            Rp. {{ number_format($totalekstra - $ab) }}
                            @else
                            Rp. {{ number_format($totalres - $ab) }}
                            @endif
                        </p>
                        @endif --}}
                    </div>
                </div>


            </div>

        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                    this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                    if (!event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },
            }
        }

        function xstayDetail() {
            return {
                initDatePickerHandler(index) {
                    Alpine.nextTick((value) => { 
                            $('#'+index).datepicker({
                                iconsLibrary: 'fontawesome',
                            })
                        }
                    )
                },
            }
        }

        document.addEventListener("alpine:init", () => {
                Alpine.store("order", {
                        final_price: {{ $harga_total_without_tax ?? 0 }},
                        product_price: {{ $detail['total_harga_kamar_refund'] ?? 0 }},
                        diskon_by_date: {{ $detail['diskon_by_date'] < 0 ? $diskon_tanggal : 0 }},
                        surcharge_by_date: {{ $detail['diskon_by_date'] > 0 ? $diskon_tanggal : 0 }},
                        price_by_date: {{ $harga_diskon_tanggal ?? 0 }},
                        diskon_grup: {{ $diskon_group ?? 0 }},
                        price_by_group: {{ $harga_diskon_group }},
                        days: {{ $days }},
                });
            });
    </script>
</body>

</html>