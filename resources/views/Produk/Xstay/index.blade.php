<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tur') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Hero --}}
    <div class="relative bg-slate-400">
        <img class="w-full max-h-[317px] opacity-70 object-cover"
            src="https://images.unsplash.com/photo-1615278166719-c411455d594d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzZ8fHRvdXJ8ZW58MHx8MHx8&auto=format&fit=crop&w=1920&q=60"
            alt="banner-tur">
    </div>

    <div class="container mx-auto my-5 text-center">
        <x-produk.cari-xstay>
            <select name="search" id="" style="background-image: none;"
                class="border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7 mt-2">
                <option value="">Dari</option>
                @foreach ($xstay as $value)
                @php
                $regency = App\Models\Regency::where('id', $value->productdetail->regency_id)->get();
                @endphp
                @foreach ($regency as $item)
                <option style="font-size: 10px" value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
                @endforeach
            </select>
        </x-produk.cari-xstay>
    </div>

    {{-- Cari paket tur impianmu di Kamtuu --}}
    <div class="container mx-auto my-5">
        <p class="text-center text-2xl sm:text-3xl font-semibold my-3 mx-3 sm:mx-0">
            Pengalaman menginap paling seru, cari di Kamtuu sekarang!
        </p>
        <div class="swiper tur-about-swiper">
            <div class="mx-auto py-5 swiper-wrapper">
                <div class="flex justify-center swiper-slide">
                    <div
                        class="text-center grid justify-items-center py-10 px-5 mx-2 sm:hover:shadow-xl rounded-lg duration-200">
                        <img src="{{ asset('storage/icons/tur-lp-1.png') }}" alt="tur-lp-1">
                        <p class="text-base sm:text-lg font-semibold">Banyak Pilihan</p>
                        <p class="text-sm sm:text-base font-normal">Lorem Ipsum is simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </div>
                </div>
                <div class="flex justify-center swiper-slide">
                    <div
                        class="text-center grid justify-items-center py-10 px-5 mx-2 sm:hover:shadow-xl rounded-lg duration-200">
                        <img src="{{ asset('storage/icons/tur-lp-1.png') }}" alt="tur-lp-1">
                        <p class="text-base sm:text-lg font-semibold">Banyak Pilihan</p>
                        <p class="text-sm sm:text-base font-normal">Lorem Ipsum is simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </div>
                </div>
                <div class="flex justify-center swiper-slide">
                    <div
                        class="text-center grid justify-items-center py-10 px-5 mx-2 sm:hover:shadow-xl rounded-lg duration-200">
                        <img src="{{ asset('storage/icons/tur-lp-1.png') }}" alt="tur-lp-1">
                        <p class="text-base sm:text-lg font-semibold">Banyak Pilihan</p>
                        <p class="text-sm sm:text-base font-normal">Lorem Ipsum is simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </div>
                </div>
            </div>
            <div class="swiper-button-next"
                style="--swiper-navigation-color: #9E3D64; --swiper-pagination-color: #9E3D64"></div>
            <div class="swiper-button-prev"
                style="--swiper-navigation-color: #9E3D64; --swiper-pagination-color: #9E3D64"></div>
        </div>
        {{-- <div class="flex justify-center">
            <div
                class="text-center grid justify-items-center w-1/4 sm:w-1/2 lg:w-1/4 xl:w-1/6 py-10 px-5 mx-2 hover:shadow-xl rounded-lg duration-200">
                <img src="{{ asset('storage/img/tur-lp-1.png') }}" alt="tur-lp-1">
                <p class="text-base sm:text-lg font-semibold">Banyak Pilihan</p>
                <p class="text-sm sm:text-base font-normal">Lorem Ipsum is simply
                    dummy text of the printing
                    and typesetting industry.</p>
            </div>
            <div
                class="text-center grid justify-items-center w-1/4 sm:w-1/2 lg:w-1/4 xl:w-1/6 py-10 px-5 mx-2 hover:shadow-xl rounded-lg duration-200">
                <img src="{{ asset('storage/img/tur-lp-1.png') }}" alt="tur-lp-1">
                <p class="text-base sm:text-lg font-semibold">Banyak Pilihan</p>
                <p class="text-sm sm:text-base font-normal">Lorem Ipsum is simply
                    dummy text of the printing
                    and typesetting industry.</p>
            </div>
            <div
                class="text-center grid justify-items-center w-1/4 sm:w-1/2 lg:w-1/4 xl:w-1/6 py-10 px-5 mx-2 hover:shadow-xl rounded-lg duration-200">
                <img src="{{ asset('storage/img/tur-lp-1.png') }}" alt="tur-lp-1">
                <p class="text-base sm:text-lg font-semibold">Banyak Pilihan</p>
                <p class="text-sm sm:text-base font-normal">Lorem Ipsum is simply
                    dummy text of the printing
                    and typesetting industry.</p>
            </div>
        </div> --}}
    </div>

    {{-- Cara memesan --}}
    <div class="container mx-auto my-5">
        <p class="text-center text-2xl sm:text-3xl font-semibold my-3 mx-3 sm:mx-0">
            Cara Memesan
        </p>
        <div class="grid grid-cols-2 content-center justify-items-center md:flex justify-center overflow-x-auto">
            <div
                class="text-center grid justify-items-center w-3/4 md:w-1/2 lg:w-1/4 xl:w-1/6 py-10 px-5 mx-2 rounded-lg">
                <img class="max-w-[100px]" src="{{ asset('storage/icons/tur-xstay-2.png') }}" alt="tur-lp-2">
                <p class="text-base sm:text-lg font-semibold">Mulai Mencari</p>
                <p class="text-sm sm:text-base font-normal">Lorem Ipsum is simply
                    dummy text of the printing
                    and typesetting industry.</p>
            </div>
            <div class="hidden md:flex justify-center">
                <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" alt="arrow">
            </div>
            <div
                class="text-center grid justify-items-center w-3/4 md:w-1/2 lg:w-1/4 xl:w-1/6 py-10 px-5 mx-2 rounded-lg">
                <img class="max-w-[100px]" src="{{ asset('storage/icons/tur-xstay-2.png') }}" alt="tur-lp-2">
                <p class="text-base sm:text-lg font-semibold">Mulai Mencari</p>
                <p class="text-sm sm:text-base font-normal">Lorem Ipsum is simply
                    dummy text of the printing
                    and typesetting industry.</p>
            </div>
            <div class="hidden md:flex justify-center">
                <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" alt="arrow">
            </div>
            <div
                class="text-center grid justify-items-center w-3/4 md:w-1/2 lg:w-1/4 xl:w-1/6 py-10 px-5 mx-2 rounded-lg">
                <img class="max-w-[100px]" src="{{ asset('storage/icons/tur-xstay-2.png') }}" alt="tur-lp-2">
                <p class="text-base sm:text-lg font-semibold">Mulai Mencari</p>
                <p class="text-sm sm:text-base font-normal">Lorem Ipsum is simply
                    dummy text of the printing
                    and typesetting industry.</p>
            </div>
            <div class="hidden md:flex justify-center">
                <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" alt="arrow">
            </div>
            <div
                class="text-center grid justify-items-center w-3/4 md:w-1/2 lg:w-1/4 xl:w-1/6 py-10 px-5 mx-2 rounded-lg">
                <img class="max-w-[100px]" src="{{ asset('storage/icons/tur-xstay-2.png') }}" alt="tur-lp-2">
                <p class="text-base sm:text-lg font-semibold">Mulai Mencari</p>
                <p class="text-sm sm:text-base font-normal">Lorem Ipsum is simply
                    dummy text of the printing
                    and typesetting industry.</p>
            </div>
        </div>
    </div>

    <hr class="container mx-auto my-10 border-gray-200">

    {{-- <div class="container mx-auto my-5"> --}}
        {{-- <p class="text-center text-2xl sm:text-3xl font-semibold my-3 mx-3 sm:mx-0"> --}}
            {{-- Butuh penginapan sesuai dengan kebutuhan? <br> --}}
            {{-- Hubungi Kamtuu! --}}
            {{-- </p> --}}

        {{-- <div class="flex justify-center"> --}}
            {{-- <a href="/xstay/formulir"> --}}
                {{-- <x-destindonesia.button-primary>@slot('button') --}}
                    {{-- Minta Pengaturan Khusus --}}
                    {{-- @endslot</x-destindonesia.button-primary> --}}
                {{-- </a> --}}
            {{-- </div> --}}
        {{-- </div> --}}

    <hr class="container mx-auto my-10 border-gray-200">

    {{-- Review --}}
    <div class="container mx-auto lg:px-44 xl:px-64 2xl:px-96 my-5">
        <p class="text-center text-2xl sm:text-3xl font-semibold my-3 mx-3 sm:mx-0">
            Apa kata mereka?
        </p>

        <div class="swiper review-swipper">
            <div class="mx-auto py-5 swiper-wrapper">
                <div class="flex justify-center swiper-slide">
                    <div
                        class="bg-gray-200 grid grid-rows-2 sm:grid-rows-none sm:grid-cols-2 justify-items-center content-center py-3 mx-3 w-[320px] sm:w-full max-w-[520px] sm:h-[240px] rounded-lg shadow-lg">
                        <div class="py-3 rounded-lg">
                            <img class="w-full h-full object-cover object-center rounded-lg"
                                src="{{ asset('storage/icons/tur-lp-3.png') }}" alt="profile-picture">
                        </div>
                        <div class="text-center sm:text-left py-3 px-7 sm:pr-7">
                            <p class="text-xl font-semibold">John Deepy</p>
                            <div class="flex justify-center sm:justify-start my-2">
                                <img src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="20px"
                                    height="20px">
                                <p class="mx-2 font-semibold text-base">
                                    4.7/<span class="text-sm">5</span>
                                </p>
                            </div>
                            <p class="text-sm">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                do
                                eiusmod
                                tempor incididunt
                                ut labore et dolore magna aliqua. Volutpat odio facilisis mauris sit.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-button-next"
                style="--swiper-navigation-color: #9E3D64; --swiper-pagination-color: #9E3D64"></div>
            <div class="swiper-button-prev"
                style="--swiper-navigation-color: #9E3D64; --swiper-pagination-color: #9E3D64"></div>
        </div>
    </div>

    {{-- FAQ --}}
    <div class="container mx-auto mt-5 mb-10">
        <p class="text-center text-2xl sm:text-3xl font-semibold my-3 mx-3 sm:mx-0">
            Sering Ditanyakan (FAQ)
        </p>

        <div x-data="{ tabs: ['Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'] }">
            <ul class="text-sm font-medium text-justify sm:text-left">
                <template x-for="(tab, index) in tabs" :key="index">
                    <li class="my-2 mx-3 border-b border-gray-200" x-data="{ open: false }">
                        <div class="flex" :class=" { 'pt-5 pb-2': open, 'py-5': !open }">
                            <button class="text-lg sm:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open" x-text="tab"></button>
                            <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        <p class="py-5 font-normal text-[#4F4F4F] text-base sm:text-xl" x-show="open" x-transition>
                            Lorem
                            Ipsum is
                            simply
                            dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to make a type
                            specimen book. </p>
                    </li>
                </template>
            </ul>
        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        var turAboutSwiper = new Swiper(".tur-about-swiper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
                enabled: true,
            },
            breakpoints: {
                640: {
                    slidesPerView: 3,
                    navigation: {
                        enabled: false
                    },
                }
            }
        });

        var reviewSwipper = new Swiper(".review-swipper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    </script>
</body>

</html>