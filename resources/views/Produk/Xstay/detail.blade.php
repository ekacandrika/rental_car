<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tur Detail') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    
    {{-- sweet alert --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.min.css" rel="stylesheet">

    <!-- Styles -->
    <style>
        input[type='number']::-webkit-outer-spin-button,
        input[type='number']::-webkit-inner-spin-button,
        input[type='number'] {
            -webkit-appearance: none;
            margin: 0;
            -moz-appearance: textfield !important;
        }

        /* Hide scrollbar for Chrome, Safari and Opera */
        .no-scrollbar::-webkit-scrollbar {
            display: none;
        }

        /* Hide scrollbar for IE, Edge and Firefox */
        .no-scrollbar {
            -ms-overflow-style: none;
            /* IE and Edge */
            scrollbar-width: none;
            /* Firefox */
        }

        .swiper-pagination-bullet {
            background: rgb(173, 151, 151);
            opacity: 1;
        }

        .swiper-pagination-bullet-active {
            background: white;
        }

        .leftcolumn {
            float: left;
            width: 25%;
            box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
        }

        /* Right column */
        .rightcolumn {
            float: left;
            width: 72%;
            margin-left: 20px;
            box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
        }

        .card {
            background-color: white;
            padding: 20px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        @media screen and (max-width: 800px) {

            .leftcolumn,
            .rightcolumn {
                width: 100%;
                padding: 0;
            }
        }

        .kiri {
            float: left;
            display: block;
            width: 200px;
        }

        .kanan {
            float: right;
            display: block;
            width: 200px;
        }


        #social-links ul {
            padding-left: 0;
        }

        #social-links ul li {
            display: inline-block;
        }

        #social-links ul li a {
            padding: 6px;
            /* border: 1px solid #ccc; */
            border-radius: 5px;
            margin: 1px;
            font-size: 25px;
        }

        #social-links .fa-facebook {
            color: #0d6efd;
        }

        #social-links .fa-twitter {
            color: deepskyblue;
        }

        #social-links .fa-linkedin {
            color: #0e76a8;
        }

        #social-links .fa-whatsapp {
            color: #25D366
        }

        #social-links .fa-reddit {
            color: #FF4500;
            ;
        }

        #social-links .fa-telegram {
            color: #0088cc;
        }

        @media screen and (max-width: 350px) {
            .gj-unselectable {
                left: 0 !important;
            }
        }

        .rate {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rate:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rate:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rate:not(:checked)>label:before {
            content: '★ ';
        }

        .rate>input:checked~label {
            color: #ffc700;
        }

        .rate:not(:checked)>label:hover,
        .rate:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rate>input:checked+label:hover,
        .rate>input:checked+label:hover~label,
        .rate>input:checked~label:hover,
        .rate>input:checked~label:hover~label,
        .rate>label:hover~input:checked~label {
            color: #c59b08;
        }

        .star-rating-complete {
            color: #c59b08;
        }

        .rating-container .form-control:hover,
        .rating-container .form-control:focus {
            background: #fff;
            border: 1px solid #ced4da;
        }

        .rating-container textarea:focus,
        .rating-container input:focus {
            color: #000;
        }

        .rated {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rated:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ffc700;
        }

        .rated:not(:checked)>label:before {
            content: '★ ';
        }

        .rated>input:checked~label {
            color: #ffc700;
        }

        .rated:not(:checked)>label:hover,
        .rated:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rated>input:checked+label:hover,
        .rated>input:checked+label:hover~label,
        .rated>input:checked~label:hover,
        .rated>input:checked~label:hover~label,
        .rated>label:hover~input:checked~label {
            color: #c59b08;
        }

        .maps-embed {
            padding-bottom: 60%
        }

        .maps-embed iframe {
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
            position: absolute;
        }
    </style>
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Gallery Lightbox --}}
    <div class="container mx-auto my-5">

        @if (count(json_decode($xstay_detail['foto_maps_1'])) == 1)
        <div class="relative">
            <div class="mb-2 md:mb-0">
                <img class="object-cover object-center max-h-[426px] md:h-[287px] lg:h-[383px] xl:h-[479px] 2xl:h-[575px] w-full md:max-h-full"
                    src="{{ asset(json_decode($xstay_detail['foto_maps_1'])[0]) }}" alt="detail-1">
            </div>
        </div>
        @endif
        @if (count(json_decode($xstay_detail['foto_maps_1'])) > 1)
        <div class="relative md:grid md:grid-cols-2 grid-flow-col gap-2">
            <div class="mb-2 md:mb-0">
                <img class="object-cover object-center max-h-[426px] md:h-[287px] lg:h-[383px] xl:h-[479px] 2xl:h-[575px] w-full md:max-h-full"
                    src="{{ asset($galleries[0]) }}" alt="detail-1">
            </div>
            <div>
                <div class="grid grid-cols-2 md:grid-cols-1 md:grid-rows-2 gap-2">
                    <div class="grid grid-cols-2 gap-2">
                        <div>
                            <img class="object-cover object-center h-full md:h-[139px] lg:h-[187px] xl:h-[235px] 2xl:h-[283px] w-full md:max-h-full"
                                src="{{ asset($galleries[1]) }}" alt="detail-2">
                        </div>
                        <div>
                            <img class="object-cover object-center h-full md:h-[139px] lg:h-[187px] xl:h-[235px] 2xl:h-[283px] w-full md:max-h-full"
                                src="{{ asset($galleries[2]) }}" alt="detail-3">
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-2">
                        <div>
                            <img class="object-cover object-center h-full md:h-[139px] lg:h-[187px] xl:h-[235px] 2xl:h-[283px] w-full md:max-h-full"
                                src="{{ asset($galleries[3]) }}" alt="detail-4">
                        </div>
                        <div>
                            <img class="object-cover object-center h-full md:h-[139px] lg:h-[187px] xl:h-[235px] 2xl:h-[283px] w-full md:max-h-full"
                                src="{{ asset($galleries[4]) }}" alt="detail-5">
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex md:absolute md:bottom-0 md:right-0">
                <div x-data="{ open: false }" @keydown.escape="open = false">
                    <button @click="open = true"
                        class="w-full md:w-fit md:-translate-x-5 md:-translate-y-5 px-1 md:px-3 py-1 lg:px-5 lg:py-2 text-sm sm:text-base font-semibold border-2 border-gray-300 text-black duration-300 bg-gray-300 rounded-sm hover:bg-gray-100">
                        Semua foto
                    </button>
                    <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                        style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                        <div
                            class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                            <button
                                class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                            </button>
                        </div>
                        <div class="h-full w-full flex items-center justify-center overflow-hidden"
                            x-data="{ active: 0, slides: {{ $xstay_detail['foto_maps_1'] }} }">
                            <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                    <button type="button"
                                        class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === 0 ? slides.length - 1 : active - 1">
                                        <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>

                            <template x-for="(slide, index) in slides" :key="index"
                                x-data="{ url: window.location.host, protocol: window.location.protocol }">
                                <div class="h-full w-full flex items-center justify-center absolute">
                                    <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                        x-show="active === index" x-transition:enter="transition ease-out duration-150"
                                        x-transition:enter-start="opacity-0 transform scale-90"
                                        x-transition:enter-end="opacity-100 transform scale-100"
                                        x-transition:leave="transition ease-in duration-150"
                                        x-transition:leave-start="opacity-100 transform scale-100"
                                        x-transition:leave-end="opacity-0 transform scale-90">

                                        <img :src="`${protocol}//${url}/${slide}`"
                                            class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                    </div>
                                    <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                        x-show="active === index">
                                        <span class="w-12 text-right" x-text="index + 1"></span>
                                        <span class="w-4 text-center">/</span>
                                        <span class="w-12 text-left" x-text="slides.length"></span>
                                    </div>
                                </div>
                            </template>
                            <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-start w-12 md:ml-16">
                                    <button type="button"
                                        class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === slides.length - 1 ? 0 : active + 1">
                                        <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div x-data="{ open: false }" @keydown.escape="open = false">
                    <button @click="open = true"
                        class="relative w-full md:w-fit md:-translate-x-5 md:-translate-y-5 px-1 md:px-3 py-1 lg:px-5 lg:py-2 text-sm sm:text-base font-semibold border-2 border-gray-300 text-black duration-300 bg-gray-300 rounded-sm hover:bg-gray-100">
                        <span class="md:pr-[11px]">
                           {{$like_count}} Favorit 
                        </span>
                        <span>
                            <svg class="absolute md:top-[9px] md:right-0 w-6 h-6 text-white hover:fill-red-500 md:pr-[7px]" id="16" onclick="submitLike2(16)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z"></path>
                            </svg>
                        </span>
                    </button>
                </div>
                <div x-data="{ open: false }" @keydown.escape="open = false">
                    <button @click="open = true"
                        class="w-full md:w-fit ml-3 md:-translate-x-5 md:-translate-y-5 px-1 md:px-3 py-1 lg:px-5 lg:py-2 text-sm sm:text-base font-semibold border-2 border-gray-300 text-black duration-300 bg-gray-300 rounded-sm hover:bg-gray-100">
                        Lihat Video
                    </button>
                    <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                        x-show.transition="open" style="background-color: rgba(0,0,0,.7)">
                        <div
                            class="flex items-center justifty-start w-12 m-2 ml-6 mb-4 md:m-2 z-100 absolute right-1 top-1 transform">
                            <button
                                class="text-white item-center flex w-12 h-12 rounded-full justify-center focus:outline-none"
                                style="background-color: rgba(230, 230, 230, 0.4)" @click="open = false">
                                {{-- relative top-[10px] --}}
                                <img src="{{ asset('storage/icons/close-button.svg') }}"
                                    class="w-6 h-6 relative top-[10px]">
                            </button>
                        </div>
                        <div class="h-full w-full flex items-center justify-center overflow-hidden">
                            {{-- <div class="h-full w-full flex items-center justify-center absolute">
                                <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                    x-transition:enter="transition ease-out duration-150"
                                    x-transition:enter-start="opacity-0 transform scale-90"
                                    x-transition:enter-end="opacity-100 transform scale-100"
                                    x-transition:leave="transition ease-in duration-150"
                                    x-transition:leave-start="opacity-100 transform scale-100"
                                    x-transition:leave-end="opacity-0 transform scale-90">
                                </div> --}}
                                @php
                                $video_id = explode("https://www.youtube.com/watch?v=",$xstay_detail->video_embed);
                                @endphp

                                <iframe class="max-w-[1800px] max-h-[700]l"
                                    src="https://www.youtube.com/embed/{{isset($video_id[1]) ? $video_id[1]:null}}" frameborder="0"
                                    allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>

    <div>
        {{-- Tabs --}}
        <div class="container sticky top-0 z-20 mx-auto my-5 bg-white" x-data="{
            active: 0,
            tabs: ['Ringkasan', 'Lokasi', 'Video', 'Kebijakan', 'Ulasan']
        }">
            <div class="flex sm:block">
                <ul
                    class="flex justify-start overflow-x-auto text-sm font-medium text-center sm:text-left text-gray-500 dark:text-gray-400 border-b border-[#BDBDBD]">
                    <template x-for="(tab, index) in tabs" :key="index">
                        <li class="pb-2 mt-2" :class="{ 'border-b-2 border-[#9E3D64]': active == index }"
                            x-on:click="active = index">
                            <a :href="`#` + tab.toLowerCase()">
                                <button class="inline-block py-2 text-xl duration-200 rounded-lg px-9" :class="{
                                        'text-[#9E3D64] bg-white font-bold': active ==
                                            index,
                                        'text-black font-normal hover:text-[#9E3D64]': active != index
                                    }" x-text="tab"></button>
                            </a>
                        </li>
                    </template>
                </ul>
            </div>
        </div>

        {{-- Content --}}
        <div class="container mx-auto">
            <div class="grid grid-cols-4 gap-5">
                {{-- Left Side --}}
                <div class="col-span-4 p-3 bg-white lg:col-span-4">
                    {{-- Ringkasan --}}
                    <div id="ringkasan">
                        <p class="text-3xl py-3 font-semibold text-[#333333] whitespace-normal break-words">
                            {{ $xstay['product_name'] }}
                        </p>
                        <p class="text-xl py-1 font-semibold text-[#333333] whitespace-normal break-words">
                            {{ $xstay_detail->alamat }}</p>
                        <div class="flex items-center py-2">
                            <img class="mx-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <p class="text-xl font-semibold text-[#333333]">{{ $ratings }} <span class="font-normal">({{
                                    $var2 }} Ulasan)</span></p>
                        </div>
                        <div class="flex flex-wrap py-1 my-2 text-sm">
                            <p class="mt-2 mr-1"><span class="font-semibold">{{ $booking_count }}</span> kali dipesan •
                            </p>
                            <p class="mt-2 mr-1"><span class="font-semibold">{{ $like_count }}</span> orang menyukai ini
                                •</p>
                            <div class="flex mt-2">
                                <p>Share</p>
                                <div class="flex justify-end">
                                    {!! $share !!}
                                </div>
                            </div>
                        </div>
                        {{-- <div class="flex flex-wrap my-3"> --}}
                            {{-- <a href="#" --}} {{--
                                class="rounded-full flex py-1 px-5 mt-2 mr-3 bg-[#F2F2F2] hover:shadow-md duration-200">
                                --}}
                                {{-- <img class="mr-2" src="{{ asset('storage/icons/bookmark-solid 1.svg') }}"
                                    alt="type" --}} {{-- width="12px">Tour --}}
                                {{-- </a> --}}
                            {{-- <a href="#" --}} {{--
                                class="rounded-full flex py-1 px-5 mt-2 mr-3 bg-[#F2F2F2] hover:shadow-md duration-200">
                                --}}
                                {{-- <img class="mr-2" src="{{ asset('storage/icons/location-dot-solid 1.svg') }}" --}}
                                    {{-- alt="location" width="12px">Lokasi --}}
                                {{-- </a> --}}
                            {{-- <a href="#" --}} {{--
                                class="rounded-full flex py-1 px-5 mt-2 bg-[#F2F2F2] hover:shadow-md duration-200"> --}}
                                {{-- <img class="mr-2" src="{{ asset('storage/icons/tag-solid 1.svg') }}" alt="category"
                                    --}} {{-- width="12px">Kategori --}}
                                {{-- </a> --}}
                            {{-- </div> --}}
                        <p class="text-sm font-semibold md:text-base lg:text-4xl">Fasilitas Populer</p>
                        <div class="grid py-3 space-y-2 border-gray-200 border-y sm:border-y-0 sm:grid-cols-2 md:w-3/4">
                            <p class="flex font-medium font-sm"><img class="mr-2" width="18px"
                                    src="{{ asset('storage/icons/check-to-slot-solid.svg') }}" alt="slot">Konfirmasi
                                {{ $xstay_detail->confirmation }}</p>
                            @foreach ($fasilitas_array as $key => $icon_fasilitas)
                            <p class="flex font-medium font-sm"><img class="mr-2" width="18px" height="18px"
                                    src="{{ $icon_fasilitas['fasilitas_img'] }}" alt="icons-{{$key}}">{{
                                $icon_fasilitas['fasilitas_name'] }}
                            </p>
                            @endforeach
                        </div>
                        <div class="my-5 text-justify">
                            <p class="my-3 text-3xl font-semibold">Deskripsi xstay</p>
                            {!! $xstay_detail['deskripsi'] !!}
                        </div>
                    </div>

                    {{-- Amentias --}}
                    <div id="paket" class="pr-2 py-3 ">
                        <p class="text-3xl pr-3 font-semibold text-[#333333]">Amenitas Kamar</p>
                        <div class="grid grid-cols-1 lg:grid-cols-5 md:grid-cols-4">
                            @foreach ($amenitas_array as $key => $icon_amenitas)
                            <div class="grid grid-cols-1 my-2">
                                <div class="flex py-3">
                                    <img id="icons-amenitas" class="mr-2" src="{{ $icon_amenitas['amenitas_img'] }}"
                                        width="18px" height="18px" alt="icons-{{$key}}">
                                    <label class="pl-2 text-kamtuu-primary" for="icons-tidur">{{
                                        $icon_amenitas['amenitas_name'] }}</label>
                                </div>
                                {{-- <div class="pl-8">
                                    {{ json_decode($xstay_detail->isi_amenitas)->result[$key] }}
                                </div> --}}
                            </div>
                            @endforeach
                            {{-- Amenitas Kamar --}}
                            {{-- <div class="grid grid-cols-1 my-2">
                                <div class="flex">
                                    <img id="icons-tidur" class="px-1 h-[17.5px] w-[20px]"
                                        src="{{ asset('storage/icons/amenitas-tidur-kamtuu.png') }}" alt="icons-tidur">
                                    <label class="pl-2 text-kamtuu-primary" for="icons-tidur">Kamar Tidur</label>
                                </div>
                                <div class="pl-10 lg:-mt-10">
                                    <p>AC</p>
                                    <p>Sprei Linen</p>
                                    <p>Tirai Kedap Cahaya</p>
                                    <p>Tempat Tidur Bayi Gratis</p>
                                </div>
                            </div> --}}
                            {{-- Amenitas Makan --}}
                            {{-- <div class="grid grid-cols-1 my-2">
                                <div class="flex">
                                    <img id="icons-tidur" class="px-1 h-[17.5px] w-[20px]"
                                        src="{{ asset('storage/icons/amenitas-makan-kamtuu.png') }}" alt="icons-makan">
                                    <label class="pl-2 text-kamtuu-primary" for="icons-tidur">Makan dan Minum</label>
                                </div>
                                <div class="pl-7 lg:-mt-16">
                                    <p>Pembuat Kopi/Teh</p>
                                    <p>Air Minum Kemasan Gratis</p>
                                    <p>Kulkas</p>
                                </div>
                            </div> --}}
                            {{-- Amenitas Mandi --}}
                            {{-- <div class="grid my-2">
                                <div class="flex">
                                    <img id="icons-tidur" class="px-1 h-[17.5px] w-[20px]"
                                        src="{{ asset('storage/icons/amenitas-mandi-kamtuu.png') }}" alt="icons-makan">
                                    <label class="pl-2 text-kamtuu-primary" for="icons-tidur">Kamar Mandi</label>
                                </div>
                                <div class="pl-7">
                                    <p>Jubah Mandi</p>
                                    <p>Perlengkapan Mandi Gratis</p>
                                    <p>Pengering Rambut</p>
                                    <p>Kamar Mandi Pribadi</p>
                                    <p>Shower</p>
                                    <p>Handuk</p>
                                </div>
                            </div> --}}
                            {{-- Hiburan --}}
                            {{-- <div class="grid grid-cols-1 my-2">
                                <div class="inline-flex">
                                    <img id="icons-tidur" class="px-1 h-[17.5px] w-[20px]"
                                        src="{{ asset('storage/icons/amenitas-hiburan-kamtuu.png') }}"
                                        alt="icons-makan">
                                    <label class="pl-2 text-kamtuu-primary" for="icons-tidur">Hiburan</label>
                                </div>
                                <div class="pl-7 lg:-mt-20">
                                    <p>TV Led 40-inci Smart TV</p>
                                </div>
                            </div> --}}
                            {{-- Lain-lain --}}
                            {{-- <div class="grid grid-cols-1 my-2">
                                <div class="flex">
                                    <img id="icons-tidur" class="px-1 h-[17.5px] w-[20px]"
                                        src="{{ asset('storage/icons/amenitas-lain-kamtuu.png') }}" alt="icons-makan">
                                    <label class="pl-2 text-kamtuu-primary" for="icons-tidur">Kamar Mandi</label>
                                </div>
                                <div class="pl-7 ">
                                    <p>Akses Melalui Koridor Luar</p>
                                    <p>Tersedia Kamar Terhubung</p>
                                    <p>Meja Rias</p>
                                    <p>Kulkas Mini</p>
                                    <p>Sandal</p>
                                    <p>Kedap Suara</p>
                                    <p>Setrika/Meja Setrika</p>
                                </div>
                            </div> --}}
                        </div>
                    </div>

                    {{-- Video Embed --}}
                    <div class="pr-2 py-3" id="video">
                        <p class="text-3xl mb-2 pr-3 font-semibold text-[#333333]">Video</p>
                        {{-- begin embed youtube link --}}
                        @php
                        $video_id = explode("https://www.youtube.com/watch?v=",$xstay_detail->video_embed);
                        @endphp
                        {{-- {!! $xstay_detail->video_embed !!} --}}
                        <iframe class="max-w-[800px] max-h-[500px]" src="https://www.youtube.com/embed/{{isset($video_id[1]) ? $video_id[1]:null}}"
                            frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>

                {{-- Content --}}
                <div class="order-1 col-span-5 lg:order-none">
                    <p class="text-sm font-semibold md:text-base lg:text-4xl">Pilih Kamar Anda</p>
                    {{-- Search Kamar --}}
                    <x-xstay.app-xstay :queryroom="$query_room" :totalpengunjung="$total_pengunjung"
                        :umuranak="$umur_anak">
                        <x-slot name="slug">
                            {{ $slug }}
                        </x-slot>
                        <x-slot name="checkin">
                            {{ $checkin }}
                        </x-slot>
                        <x-slot name="checkout">
                            {{ $checkout }}
                        </x-slot>
                        {{-- <x-slot name="query_room">
                            {{ json_encode($query_room) }}
                        </x-slot> --}}
                    </x-xstay.app-xstay>


                    @if ($checkin == null || $checkout == null)
                    <div class="text-center font-bold text-lg py-10">
                        <p>Pilih tanggal Check In dan Check Out terlebih dahulu.</p>
                    </div>

                    @endif

                    @if ($checkin != null && $checkout != null && count($rooms_array) <= 0) <p
                        class="text-center font-bold text-lg py-10">Kamar yang Anda cari tidak tersedia.</p>
                        @endif

                        @if (count($rooms_array) > 0)
                        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
                            @foreach ($rooms_array as $room)
                            <div class="border border-[#BDBDBD] shadow-lg m-3">
                                <img class="w-[405px] h-[280px] md:w-[405px] lg:w-full object-cover object-center"
                                    src="{{ isset(json_decode($room['foto_kamar'], true)['result']) > 0 ? asset(json_decode($room['foto_kamar'], true)['result'][0]) : '' }}"
                                    alt="tur-1">
                                <div class="p-2">
                                    <p class="text-base font-semibold">{{ $room['nama_kamar'] }}</p>
                                </div>
                                @foreach ($room['amenitas'] as $key => $room_amenitas)
                                <div class="px-5 py-2 my-1">
                                    <p class="flex font-medium font-sm"><img class="mr-2" width="20px"
                                            src="{{ $room_amenitas['amenitas_img'] }}" alt="amenitas-{{$key}}">{{
                                        $room_amenitas['amenitas_name'] }}
                                </div>
                                @endforeach
                                <div class="grid grid-rows-1 p-5 my-1">
                                    {{-- @if ($xstay_detail['izin_ekstra'] != 'Tidak Usah') --}}
                                    @if (auth()->user() == null)
                                    <div class="p-5 grid justify-items-center">
                                        <a href="{{ route('LoginTraveller') }}"
                                            class="px-8 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">
                                            Login First</a>
                                    </div>
                                    @else
                                    <form action="{{ route('xstay.prosespesan') }}" method="POST"
                                        x-data="price({{$room['harga_kamar']}}, {{json_encode($room['refundable'])}}, {{json_encode($room['refundable_price'])}}, '{{$room['tipe_harga']}}')"
                                        x-init="sumTotal()">
                                        @csrf
                                        @foreach ($room['refundable'] as $key => $refundable)
                                        <div class="grid grid-cols-3" x-data="{ refundable : '{{ $refundable }}'}">
                                            <div class="col-span-2 inline-flex p-2 items-start">
                                                <input class="rounded-full text-kamtuu-second checked:bg-kamtuu-second"
                                                    type="radio" value="{{ $key }}" x-model="index">

                                                <template x-if="index === '{{ $key }}'">
                                                    <div>
                                                        <input
                                                            class="rounded-full text-kamtuu-second checked:bg-kamtuu-second"
                                                            type="hidden" name="hotelRefundablePrice"
                                                            id="hotelRefundable"
                                                            value="{{ $room['refundable_price'][$key] }}">
                                                        <input
                                                            class="rounded-full text-kamtuu-second checked:bg-kamtuu-second"
                                                            type="hidden" name="hotelRefundable" id="hotelRefundable"
                                                            value="{{ $refundable }}">
                                                    </div>
                                                </template>

                                                @if ($refundable == 'non')
                                                <label class="mx-2 mb-1 text-sm" for="hotelRefundable">Non
                                                    Refundable</label>
                                                @endif

                                                @if ($refundable == 'full')
                                                <div class="grid items-start">
                                                    <label class="mx-2 mb-1 text-sm" for="hotelRefundable">Refundable
                                                        Penuh</label>
                                                    @if ($checkin_format != '')
                                                    <div class="mx-2 text-xs">
                                                        <p class="text-[#828282]">Sebelum {{ $checkin_format }}</p>
                                                    </div>
                                                    @endif
                                                </div>
                                                @endif

                                                @if ($refundable == 'partial')
                                                <div class="grid items-start">
                                                    <label class="mx-2 mb-1 text-sm" for="hotelRefundable">Refundable
                                                        Sebagian <span>({{ $room['refundable_amount'][$key] ?? 0
                                                            }}%)</span>
                                                    </label>
                                                    @if ($checkin_format != '')
                                                    <div class="mx-2 text-xs">
                                                        <p class="text-[#828282]">Sebelum {{ $checkin_format }}</p>
                                                    </div>
                                                    @endif
                                                </div>
                                                @endif
                                                {{-- <p class="flex mx-2 font-medium font-sm">
                                                    <img class="mr-2" width="20px"
                                                        src="{{ asset('storage/icons/circle-info-solid (1) 1.svg') }}"
                                                        alt="user-group">
                                                </p> --}}
                                            </div>
                                            <div class="p-2">
                                                {{-- <textarea name="refundablePrice" class="hidden" id="" cols="30"
                                                    rows="10">{{ $refundable }}</textarea> --}}
                                                <p class="text-sm">+ Rp {{
                                                    number_format($room['refundable_price'][$key]) }}
                                                </p>
                                            </div>
                                        </div>
                                        @endforeach

                                        <hr class="border border-[#BDBDBD] my-5">
                                        <p class="font-bold">Lengkapi masa menginap Anda</p>
                                        <p class="my-3">Ekstra</p>
                                        {{-- <div class="my-3 space-y-3">
                                            <div>
                                                @livewire('tur.modal-pilihan', ['pilihans' => $pilihan])
                                            </div>
                                            <div>
                                                @livewire('tur.modal-ekstra')
                                            </div>
                                        </div> --}}
                                        <div>
                                            <div class="grid grid-cols-3">
                                                <div class="col-span-2 inline-flex p-2 items-start">
                                                    <input
                                                        class="rounded-full text-kamtuu-second checked:bg-kamtuu-second"
                                                        type="radio" name="hotelExtra" id="hotelExtra" value="0"
                                                        x-model="extra_price">
                                                    <label class="mx-2 mb-1 text-sm" for="hotelExtra">Tidak Ada
                                                        Ekstra</label>
                                                    <textarea name="hotelExtraName" class="hidden" id="" cols="30"
                                                        rows="10">Tidak Ada Extra</textarea>
                                                </div>
                                                <div class="p-2 grid items-start">
                                                    <p class="text-sm">+ Rp 0</p>
                                                </div>
                                            </div>
                                            @foreach (json_decode($room['nama_ekstra'], true)['result'] as $key =>
                                            $nama_ekstra)
                                            <div class="grid grid-cols-3"
                                                x-data="{ nama_ekstra : '{{ $nama_ekstra }}'}">
                                                <div class="col-span-2 inline-flex p-2">
                                                    <input
                                                        class="rounded-full text-kamtuu-second checked:bg-kamtuu-second"
                                                        type="radio" name="hotelExtra" id="hotelExtra"
                                                        value="{{ json_decode($room['harga_ekstra'], true)['result'][$key] }}"
                                                        x-model="extra_price">
                                                    <label class="mx-2 mb-1 text-sm" for="hotelExtra">{{ $nama_ekstra
                                                        }}</label>
                                                    <template
                                                        x-if="extra_price === '{{ json_decode($room['harga_ekstra'], true)['result'][$key] }}'">
                                                        <textarea name="hotelExtraName" class="hidden" id="" cols="30"
                                                            rows="10" x-model="nama_ekstra"></textarea>
                                                    </template>

                                                </div>
                                                <div class="p-2 grid items-start">
                                                    <p class="text-sm">+ Rp {{
                                                        number_format(json_decode($room['harga_ekstra'],
                                                        true)['result'][$key]) }}</p>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div>
                                            <div class="my-3">
                                                <p class="text-xs font-light">
                                                    Harga per malam per kamar
                                                </p>
                                                <p class="text-xl font-semibold"
                                                    x-text="'Rp ' + new Intl.NumberFormat().format(total_price)">
                                                </p>
                                            </div>

                                            <div class="grid grid-cols-2">
                                                <div>
                                                    <input type="hidden" value="{{ $room['harga_kamar'] }}"
                                                        id="harga_kamar">
                                                    <p class="text-sm text-gray-400"
                                                        x-text="'total Rp ' + new Intl.NumberFormat().format(total_price_with_tax)">
                                                    </p>
                                                </div>
                                                <div class="grid justify-items-end">
                                                    <p class="text-red-600">Sisa {{ $room['room_available_stok'] }}
                                                        Kamar</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid xl:grid-cols-2">
                                            <div class="flex mt-10">
                                                <div class="relative flex flex-row items-center group">
                                                    <a href="#">
                                                        <p class="text-kamtuu-second">Rincian Harga</p>
                                                    </a>
                                                    <img class="h-3 m-2"
                                                        src="{{ asset('storage/icons/right arrow.png') }}"
                                                        alt="user-group">
                                                    <div
                                                        class="absolute top-0 flex flex-col items-center hidden mt-6 group-hover:flex">
                                                        <div class="w-3 h-3 -mb-2 rotate-45 bg-white"></div>
                                                        <span
                                                            class="relative z-10 p-2 text-xs leading-none whitespace-no-wrap bg-white border-gray-400 rounded-md shadow-lg w-max-xl w-[25rem]">
                                                            <div class="grid grid-cols-2 m-5 text-start">
                                                                <div class="grid col-span-2">
                                                                    <p class="text-base font-semibold">Rincian Harga
                                                                    </p>
                                                                </div>
                                                                @php
                                                                $total_price = $total_kmr * $nights *
                                                                $room['harga_kamar']
                                                                @endphp
                                                                <div class="">
                                                                    <p>{{ $total_kmr }} Kamar x {{ $nights }} Malam </p>
                                                                    {{-- <p>Pajak dan biaya </p> --}}
                                                                    <p>Total</p>
                                                                </div>
                                                                <div class="">
                                                                    <p class=""
                                                                        x-text="'Rp ' + new Intl.NumberFormat().format(total_price)">
                                                                    </p>
                                                                    {{-- <p class=""
                                                                        x-text="'Rp ' + new Intl.NumberFormat().format(total_tax)">
                                                                    </p> --}}
                                                                    <p class=""
                                                                        x-text="'Rp ' + new Intl.NumberFormat().format(total_price)">
                                                                    </p>
                                                                </div>

                                                            </div>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid justify-items-end">
                                                <div class="p-5 grid justify-items-center">
                                                    @php
                                                    $diskons = json_decode($diskon['discount'], true)[0];
                                                    @endphp
                                                    <input type="hidden" :value="total_price" name="total">
                                                    <input type="hidden" value="{{ $xstay['product_name'] }}"
                                                        name="product_name">
                                                    {{-- <input type="hidden" value="2" name="dewasa">
                                                    <input type="hidden" value="0" name="anak">
                                                    <input type="hidden" value="0" name="balita"> --}}
                                                    <input type="hidden" value="{{ $xstay['user_id'] }}" name="toko_id">
                                                    <input type="hidden" value="{{ $xstay_detail['id'] }}"
                                                        name="product_detail_id">
                                                    <input type="hidden" value="{{ $xstay['type'] }}" name="type">
                                                    <input type="hidden" value="{{ $diskons }}" name="diskon">

                                                    <input type="hidden" value="{{ json_encode($query_room) }}"
                                                        name="room">
                                                    <input type="hidden" value="{{ $checkin }}" name="check_in_date">
                                                    <input type="hidden" value="{{ $checkout }}" name="check_out_date">
                                                    <input type="hidden" value="{{ $nights }}" name="nights">
                                                    <input type="hidden" value="{{ $total_kmr }}" name="total_kmr">
                                                    <input type="hidden" :value="index" name="index_refund">
                                                    <input type="hidden" value="{{ $room['id'] }}"
                                                        name="selected_room_id">
                                                    <input type="hidden" value="{{ $room['nama_kamar'] }}"
                                                        name="selected_room_name">
                                                    <input type="hidden" value="{{ json_encode($room) }}"
                                                        name="selected_room">
                                                    <input type="hidden" value="{{ $var2 }}" name="reviews">
                                                    <input type="hidden" value="{{ $ratings }}" name="ratings">
                                                    <input type="hidden" value="{{ $total_pengunjung }}"
                                                        name="total_pengunjung">
                                                    <input type="hidden" :value="total_harga_kamar"
                                                        name="total_harga_kamar">
                                                    <input type="hidden" value="{{ $xstay_detail['discount_id'] }}"
                                                        name="diskon_id">
                                                    <input type="hidden" :value="total_harga_ekstra"
                                                        name="total_harga_ekstra">
                                                    <input type="hidden" :value="total_tax" name="total_tax">
                                                    <input type="hidden" value="{{ $total_dewasa }}"
                                                        name="total_dewasa">
                                                    <input type="hidden" value="{{ $total_anak }}" name="total_anak">
                                                    <input type="hidden" :value="index" name="index_refund">
                                                    <input type="hidden" value="{{ $pajak ?? 0 }}" name="pajak">

                                                    @if (auth()->user()->role === 'traveller')
                                                    <button
                                                        class="px-8 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">Pesan</button>
                                                    @else
                                                    <p
                                                        class="px-8 py-2 md:text-base text-center text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">
                                                        Silakan login menggunakan akun traveller.</p>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    @endif
                                </div>
                            </div>
                            @endforeach

                            {{-- @endif --}}

                        </div>
                        @endif


                </div>

            </div>
        </div>

        <div class="container mx-auto">
            <hr class="border border-[#BDBDBD] my-5">

            {{-- Lokasi --}}
            <div id="lokasi" class="my-5">
                <p class="text-3xl py-3 font-semibold text-[#333333]">Lokasi Anda</p>
                <p class="pb-3 text-sm font-normal">{{ $regency->name }}</p>
                <div class="overflow-hidden relative maps-embed">
                    {!! $xstay_detail['link_maps'] !!}
                </div>
            </div>

            {{-- Perlu Diketahui --}}
            <p class="text-3xl py-3 font-semibold text-[#333333]">Perlu Diketahui</p>
            {{-- Catatan --}}
            @if (isset($xstay_detail['catatan']))
            <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                x-data="{ open: false }">
                <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-3': !open }">
                    <button
                        class="text-lg sm:text-xl md:text-2xl  text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                        x-on:click="open = !open">Catatan
                    </button>
                    <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                        x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                        alt="chevron-down" width="26px" height="15px">
                </div>
                <div class="py-5 px-3 text-[#4F4F4F]" x-show="open" x-transition>
                    {!! $xstay_detail['catatan'] !!}
                </div>
            </div>
            @endif


            {{-- Kebijakan Pembatalan --}}
            <div id="kebijakan">
                <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                    x-data="{ open: false }">
                    <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-3': !open }">
                        <button
                            class="text-lg sm:text-xl md:text-2xl  text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                            x-on:click="open = !open">Kebijakan Pembatalan
                        </button>
                        <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                            x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            alt="chevron-down" width="26px" height="15px">
                    </div>
                    {{-- @php
                    dd(json_decode($xstay_detail->kebijakan_pembatalan_sebelumnya)->sebelumnya);
                    @endphp --}}
                    <div class="py-5 px-3 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl" x-show="open"
                        x-transition>

                        @if (isset($xstay_detail->kebijakan_pembatalan_sebelumnya))
                        @foreach ($sebelumnya as $key => $kps)
                        <p class="py-3">Pembatalan <strong>{{ $kps }} hari</strong> sampai
                            <strong>{{ $sesudahnya[$key] }} hari</strong> sebelumnya potongan
                            <strong>{{ $potongan[$key] }}%.</strong>
                        </p>
                        @endforeach
                        @else
                        <p class="text-base sm:text-lg md:text-xl">Tidak ada kebijakan pembatalan.</p>
                        @endif
                    </div>
                </div>
            </div>

            {{-- Sering Ditanyakan (FAQ) --}}
            <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                x-data="{ open: false }">
                <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-3': !open }">
                    <button
                        class="text-lg sm:text-xl md:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                        x-on:click="open = !open">Sering Ditanyakan (FAQ)
                    </button>
                    <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                        x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                        alt="chevron-down" width="26px" height="15px">
                </div>
                <div class="py-5 px-3 text-[#4F4F4F] text-base sm:text-lg md:text-xl" x-show="open" x-transition>
                    @if (isset($xstay_detail['faq']))
                    {!! $xstay_detail['faq'] !!}
                    @else
                    <p class="text-base sm:text-lg md:text-xl">Tidak ada FAQ.</p>
                    @endif
                </div>
            </div>

            {{-- Ulasan --}}
            {{-- <div id="ulasan" class="my-5">
                <x-destindonesia.ulasan></x-destindonesia.ulasan>
            </div> --}}

            {{-- Ulasan --}}
            <div id="ulasan" class="my-5">
                <div class="container mx-auto">
                    @if(isset($reviews->star_rating))
                    <div class="container">
                        <div class="row">
                            <div class="col mt-4">
                                <p class="font-weight-bold ">Review</p>
                                <div class="form-group row">
                                    <input type="hidden" name="detailObjek_id" value="{{ $reviews->id }}">
                                    <div class="col">
                                        <div class="rated">
                                            @for($i=1; $i<=$reviews->star_rating; $i++)
                                                <input type="radio" id="star{{$i}}" class="rate" name="rating"
                                                    value="5" />
                                                <label class="star-rating-complete" title="text">{{$i}}
                                                    stars</label>
                                                @endfor
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mt-4">
                                    <div class="col">
                                        <p>{{ $reviews->comments }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="container">
                        <div class="row">
                        </div>
                    </div>
                    <div>
                        <div class="container max-w-6xl p-4 ">

                            <div class="flex mb-4">
                                <div class="col mt-4 w-full">
                                    <form class="py-2 px-4 w-full" action="{{route('reviewProduct.store')}}"
                                        style="box-shadow: 0 0 10px 0 #ddd;" method="POST" autocomplete="off" enctype="multipart/form-data" id="form-xstay-private">
                                        @csrf
                                        <p class="font-weight-bold ">Ulasan</p>
                                        <div class="form-group row">
                                            <input type="hidden" name="product_id" value="{{ $xstay->id }}">
                                            <div class="col">
                                                <div class="rate">
                                                    <input type="radio" id="star5" class="rate" name="rating"
                                                        value="5" />
                                                    <label for="star5" title="text">5 stars</label>
                                                    <input type="radio" checked id="star4" class="rate" name="rating"
                                                        value="4" />
                                                    <label for="star4" title="text">4 stars</label>
                                                    <input type="radio" id="star3" class="rate" name="rating"
                                                        value="3" />
                                                    <label for="star3" title="text">3 stars</label>
                                                    <input type="radio" id="star2" class="rate" name="rating" value="2">
                                                    <label for="star2" title="text">2 stars</label>
                                                    <input type="radio" id="star1" class="rate" name="rating"
                                                        value="1" />
                                                    <label for="star1" title="text">1 star</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row mt-4" x-data="imageUpload">
                                            <div class="col">
                                                <textarea
                                                    class=" form-control w-full flex-auto block p-4 font-medium border border-transparent rounded-lg outline-none focus:border-[#9E3D64] focus:text-green-500"
                                                    name="comment" rows="6 " placeholder="Berikan Ulasan Anda"
                                                    maxlength="200" id="comments"></textarea>
                                                <input type="file" name="images[]" class="form-control w-full flex-auto block border border-transparent outline-none focus:border-[#9E3D64] focus:text-green-500" accept="image/*" @change="selectedFile" multiple>
                                                        <template x-if="imgDetail.length >= 1">
                                                            <div class="flex justify-start items-center mt-2">
                                                                <template x-for="(detail,index) in imgDetail" :key="index">
                                                                    <div class="flex justify-center items-center">
                                                                        <img :src="detail"
                                                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                                            :alt="'upload'+index">
                                                                        <button type="button"
                                                                            class="absolute mx-2 translate-x-12 -translate-y-14">
                                                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                                                alt="" width="25px" @click="removeImage(index)">
                                                                        </button>
                                                                    </div>
                                                                </template>
                                                            </div>
                                                        </template>    
                                            </div>
                                        </div>
                                        <div class="mt-3 text-right">
                                            <button class="py-2 px-3 rounded-lg bg-kamtuu-second text-white">Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="grid grid-cols-2 p-5">
                                <div class="flex">
                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                        class="lg:w-[48px] lg:h-[48px] mt-[0.2rem] mr-1 inline-flex" alt="rating"
                                        title="Rating">
                                    <p class="flex"><span class="lg:text-[50px] font-semibold ">{{$ratings}}</span>
                                        <span class="lg:text-[20px] lg:pt-9"> /5.0 dari {{$var2}} ulasan</span>
                                    </p>
                                </div>
                            </div>

                            {{-- ulasan desktop --}}

                            @foreach($reviews as $review)
                            <div class="hidden lg:block md:block">
                                        {{-- <x-produk.card-reviews :review="$review"></x-produk.card-reviews> --}}
                                        <div class="grid grid-row-5 rounded-lg shadow-lg justify-items-start">
                                            {{-- <div class="grid grid-row-5 rounded-lg shadow-lg justify-items-start py-5"> --}}
                                                <div class="grid grid:row-2 grid-cols-10 gap-2 pl-3 mt-7">
                                                    @if(isset($review->profile_photo_path))
                                                    <img class="w-20 h-20 rounded-full bg-gray-400" scr="{{isset($review->profile_photo_path) ? asset($review->profile_photo_path):null}}">
                                                    @else
                                                    <div class="w-20 h-20 rounded-full bg-gray-400 relative">
                                                        @php
                                                            $name = $review->first_name;
                                                            $exp_name = explode(' ',$name);
                                                            $inisial ="";

                                                            $inisial = substr($exp_name[0],0,1);

                                                            if(count($exp_name) > 1){
                                                                $inisial .=substr(end($exp_name),0,1);
                                                            }
                                                        @endphp
                                                        @if(count($exp_name) > 1)
                                                        <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 13px;">{{$inisial}}</p>
                                                        @else
                                                        <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 26px;">{{$inisial}}</p>
                                                        @endif
                                                    </div>
                                                    @endif
                                                    <div class="col-span-2 my-auto">
                                                        <p class="font-semibold text-gray-900">{{$review->first_name}}<p/>
                                                        {{-- Carbon::parse($p->created_at)->diffForHumans(); --}}
                                                        <p class="font-light text-gray-600 text-xs">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}<p/>
                                                    </div>
                                                </div>
                                                <div class="grid grid-cols-5 pl-3 mb-2">
                                                    <div class="col-span-5">
                                                        <img class="w-[20px] h-[20] mt-[0.3rem] mr-1  ml-3 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                                        @for ($i = 0; $i < ($review->star_rating-1); $i++)
                                                            <img class="w-[20px] h-[20] mt-[0.3rem] mr-1 -ml-2 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                                        @endfor
                                                    </div>
                                                </div>
                                                <div class="grid grid-cols-5 space-y-5 text-justify pl-3 pr-5">
                                                    <div class="col-span-5 pl-3">
                                                    <p id="text-ulasan">{{$review->comments}}</p>
                                                    </div>
                                                </div>
                                                <div class="mt-3 pl-3">
                                                    <button type="button" id="show-btn-ulasan" class="px-3 py-2 text-black text-sm underline">Tampilkan</button>
                                                    <button type="button" id="hide-btn-ulasan" class="px-3 py-2 text-black text-sm underline hidden">Sembunyikan</button>
                                                </div>
                                                @if($review->gallery_post)
                                                <div class="flex justify-start justify-items-center pl-4 pb-4">
                                                    @php
                                                        $post_gallery = json_decode($review->gallery_post, true);
                                                        //dump($post_gallery);
                                                    @endphp
                                                    <img class="w-50 h-40 bg-slate-400 rounded-lg" src="{{$post_gallery['result'] != null ? Storage::url('gallery_post/'.$post_gallery['result'][0]):'https://via.placeholder.com/540x540'}}">
                                                    <div class="relative" x-data="{open:false}" @keydown.escape="open = false">
                                                        <button @click="open=true" type="button" id="hide-btn-foto-ulasan" 
                                                            class="absolute px-3 py-2 text-black text-sm underline" style="width:169px;top:110px">
                                                            Tampilkan semua foto
                                                        </button>
                                                        <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                                                                    style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                                                                    <div
                                                                        class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                                                                        <button
                                                                            class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                                                            style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                                                            <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                                                                        </button>
                                                                    </div>
                                                                    <div class="h-full w-full flex items-center justify-center overflow-hidden"
                                                                        x-data="{active: 0, slides: {{ ($review->gallery_post) }} }">
                                                                        <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                                                            <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                                                                <button type="button"
                                                                                    class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                                                    style="background-color: rgba(230, 230, 230, 0.4);"
                                                                                    @click="active = active === 0 ? slides.length - 1 : active - 1">
                                                                                    <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                        @if ($post_gallery)
                                                                            @foreach ($post_gallery['result'] as $index=>$gallery)
                                                                                <div class="h-full w-full flex items-center justify-center absolute">
                                                                                    <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                                                                        x-show="active === {{ $index }}"
                                                                                        x-transition:enter="transition ease-out duration-150"
                                                                                        x-transition:enter-start="opacity-0 transform scale-90"
                                                                                        x-transition:enter-end="opacity-100 transform scale-100"
                                                                                        x-transition:leave="transition ease-in duration-150"
                                                                                        x-transition:leave-start="opacity-100 transform scale-100"
                                                                                        x-transition:leave-end="opacity-0 transform scale-90">

                                                                                        <img src="{{ Storage::url('gallery_post/'.$gallery) }}"
                                                                                            class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                                                                    </div>
                                                                                    <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                                                                        x-show="active === {{ $index }}">
                                                                                        <span class="w-12 text-right" x-text="{{ $index }} + 1"></span>
                                                                                        <span class="w-4 text-center">/</span>
                                                                                        <span class="w-12 text-left"
                                                                                            x-text="{{ count($post_gallery['result']) }}"></span>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        @endif
                                                                        
                                                                        <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                                                            <div class="flex items-center justify-start w-12 md:ml-16">
                                                                                <button type="button"
                                                                                    class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                                                    style="background-color: rgba(230, 230, 230, 0.4);"
                                                                                    @click="active = active === slides.length - 1 ? 0 : active + 1">
                                                                                    <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                    </div>
                                                </div>
                                                @endif
                                            {{-- </div> --}}
                                            {{-- <div class="grid p-5 justify-items-center">
                                                <p class="text-[18px] font-bold">Jhonny Wilson</p>
                                                <p class="text-[18px] -mt-2">{{$review->created_at->format('j F, Y')}}
                                                </p>
                                                <div class="flex p-2">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex pt-3"><span
                                                            class="text-[16px] font-semibold ">{{$review->star_rating}}.0</span>
                                                        <span class="text-[16px]"> /5.0</span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div
                                                class="grid justify-start justify-items-center lg:-ml-[15rem] xl:-ml-[23rem] p-5 md:-ml-[10rem]">
                                                <p class="text-[18px]">{{ $review->comments }}</p>
                                            </div> --}}
                                        </div>
                                    </div>

                            {{-- ulasan mobile --}}
                            <div class="block lg:hidden md:hidden">
                                        <div
                                            class="grid grid-cols-1 divide-y rounded-lg shadow-xl justify-items-center border border-gray-300">
                                            <div class="grid p-5 justify-items-center">
                                                <p class="text-sm md:text-[18px] font-bold my-3">{{$review->frist_name}}</p>
                                                <p class="text-sm md:text-[18px] -mt-2">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}</p>
                                                <div class="flex p-2">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex pt-3"><span
                                                            class="text-[16px] font-semibold ">{{$review->star_rating}}</span>
                                                        <span class="text-[16px]"> /5.0</span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="grid justify-start justify-items-center p-5">
                                                <p class="text-sm md:text-[18px]"> {{ $review->comments }}</p>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                            @endforeach
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>


        {{-- <div> --}}
            {{-- <div class="flex justify-between"> --}}
                {{-- <p class="text-3xl font-bold text-[#D50006]">Anda Mungkin Suka</p> --}}
                {{-- <a href="#" class="text-xl font-bold text-[#9E3D64]">Lihat semua</a> --}}
                {{-- </div> --}}
            {{-- <div class="my-5 swiper carousel-tur-swiper"> --}}
                {{-- <div class="py-5 mx-auto swiper-wrapper"> --}}
                    {{-- <div class="flex justify-center swiper-slide"> --}}
                        {{-- <x-produk.card-populer></x-produk.card-populer> --}}
                        {{-- </div> --}}
                    {{-- <div class="flex justify-center swiper-slide"> --}}
                        {{-- <x-produk.card-populer></x-produk.card-populer> --}}
                        {{-- </div> --}}
                    {{-- <div class="flex justify-center swiper-slide"> --}}
                        {{-- <x-produk.card-populer></x-produk.card-populer> --}}
                        {{-- </div> --}}
                    {{-- <div class="flex justify-center swiper-slide"> --}}
                        {{-- <x-produk.card-populer></x-produk.card-populer> --}}
                        {{-- </div> --}}
                    {{-- <div class="flex justify-center swiper-slide"> --}}
                        {{-- <x-produk.card-populer></x-produk.card-populer> --}}
                        {{-- </div> --}}
                    {{-- </div> --}}
                {{-- <div class="swiper-button-next"></div> --}}
                {{-- <div class="swiper-button-prev"></div> --}}
                {{-- </div> --}}
            {{-- </div> --}}
    </div>

    {{-- <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff" class="swiper mySwiper2">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-1.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-2.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-3.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-4.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-5.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-6.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-7.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-8.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-9.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-10.jpg" />
            </div>
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
    <div thumbsSlider="" class="swiper mySwiper">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-1.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-2.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-3.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-4.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-5.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-6.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-7.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-8.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-9.jpg" />
            </div>
            <div class="swiper-slide">
                <img src="https://swiperjs.com/demos/images/nature-10.jpg" />
            </div>
        </div>
    </div> --}}


    </div>
    </div>
    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    {{-- <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script> --}}
    <script>
        @if(!empty($diskon['tgl_start'][0]) && !empty($diskon['tgl_end'][0]))
                var startDate = JSON.parse("{{ $diskon['tgl_start'] }}".replace(/&quot;/g,'"'));
                var endDate = JSON.parse("{{ $diskon['tgl_end'] }}".replace(/&quot;/g,'"'));
                var disabledDates = [];
                var jml_tgl = startDate.length;

                for(var i =0; i < jml_tgl;i++){
                    var start = new Date(startDate[i])
                    var end = new Date(endDate[i])
                    var date = new Date(start)

                    while(date <= end){
                        disabledDates.push(date.getFullYear()+'-'+('0' + (date.getMonth() + 1)).slice(-2)+'-'+('0'+date.getDate()).slice(-2))
                        date.setDate(date.getDate() + 1);
                    }

                }

            $('#checkin').datepicker({
                iconsLibrary: 'fontawesome',
                disableDates: function (date) {
                    var formattedDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                    return ($.inArray(formattedDate, disabledDates) != -1);
                }
            });

            $('#checkout').datepicker({
                iconsLibrary: 'fontawesome',
                disableDates: function (date) {
                    var formattedDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                    return ($.inArray(formattedDate, disabledDates) != -1);
                }
            });
        @else
            $(document).ready(function() {
                today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                $('#checkin').datepicker({
                    minDate: today
                });
                $('#checkout').datepicker({
                    minDate: today
                });
            })
        @endif
        
        $(document).ready(function() {
            var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            $('#check_in_date').datepicker({
                minDate: today
            });
        })
        $(document).ready(function() {
            var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            $('#check_out_date').datepicker({
                minDate: today
            });
        })

        function price(room_price, refund_info, refund_price, tipe_harga) {
            return {
                extra_price : 0,
                total_price : 0,
                refundable_array : [],
                index : 0,
                total_kmr : {{ $total_kmr }},
                nights : {{ $nights }},
                room_per_price: room_price,
                total_price_with_tax : 0,
                total_pengunjung: {{ $total_pengunjung }},
                total_harga_ekstra: 0,
                total_harga_kamar: 0,
                total_tax: 0,
                pajak: {{ $pajak ?? 0 }},
                sumTotal() {
                    Alpine.nextTick(() => { 
                        for (let i = 0; i < refund_info.length; i++) {
                            this.refundable_array.push({
                                refundable: refund_info[i],
                                refundable_price: refund_price[i]
                            })
                        }

                        if (tipe_harga === 'price_per_person') {
                            this.total_harga_kamar = this.total_pengunjung * this.nights * this.room_per_price
                        }

                        if (tipe_harga === 'price_per_room') {
                            this.total_harga_kamar = this.total_kmr * this.nights * this.room_per_price
                        }
                        
                        this.total_harga_ekstra = this.total_pengunjung * this.nights * this.extra_price
                        this.total_price = this.total_harga_kamar + this.total_harga_ekstra + parseInt(this.refundable_array[0].refundable_price)
                        this.total_tax = (this.pajak/100) * this.total_price
                        this.total_price_with_tax = this.total_tax + this.total_price
                    })

                    this.$watch('extra_price, index', () => {
                        if (tipe_harga === 'price_per_person') {
                            this.total_harga_kamar = this.total_pengunjung * this.nights * this.room_per_price
                        }

                        if (tipe_harga === 'price_per_room') {
                            this.total_harga_kamar = this.total_kmr * this.nights * this.room_per_price
                        }

                        this.total_harga_ekstra = this.total_pengunjung * this.nights * this.extra_price
                        this.total_price = this.total_harga_kamar + this.total_harga_ekstra + parseInt(this.refundable_array[this.index].refundable_price)
                        this.total_tax = (this.pajak/100) * this.total_price
                        this.total_price_with_tax = this.total_tax + this.total_price
                        
                    })
                }
            }
        }
    </script>

    <script>

        $(document).ready(function(){
            
            function submitValidate(e, msg){
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'error',
                        title: msg
                    })
                e.preventDefault()
            }

            $('#form-xstay-private').on("submit",function(e){
                let role="{{auth()->user() != null ? auth()->user()->role:null}}";
                if(!role){
                
                    submitValidate(e, 'Silahkan login terlebih dahulu')
                }

                if(role!='traveller'){
                        submitValidate(e, 'Silahkan login terlebih dahulu')
                }
                

                if($("#comments").val()){
                    submitValidate(e, 'Isi komentar tidak kosong')
                }
                //e.preventDefault();
            })
        })

        function imageUpload(){
            return{
                imageUrl:'',
                imgDetail:[],
                selectedFile(event){
                    this.fileToUrl(event)
                },
                fileToUrl(event){
                    if (!event.target.files.length) return
                    let file = event.target.files
                    
                    for (let i = 0; i < file.length; i++) {

                        let reader = new FileReader();
                        let srcImg = ''
                        this.imgDetail = []

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.imgDetail = [...this.imgDetail, srcImg]
                        };
                    }

                    //this.fileUpload();
                },
                removeImage(index){
                    this.imgDetail.splice(index, 1)
                },
                fileUpload(){
                    let fd = new FormData()
                    
                    console.log(file)

                    for(const file of this.imgDetail){
                        console.log(file)
                        /*
                        if(typeof file === 'object'){
                            fd.append('gallery_post[]',file, file.name)
                        }
                        if(typeof file === 'string'){
                            fd.append('seved_post_gallery',file)
                        }
                        */
                    }

                    /*
                    $.ajaxSetup({
                        headers:{
                            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        method:'POST',
                        url:"{{route('gallery-post')}}",
                        data:fd,
                        processData:false,
                        contentType:false,
                        beforeSend:function(){

                        },
                        success:function(msg){
                            console.log(msg)
                        },
                        error:function(data){

                        }
                    })
                    */
                }
            }
        }

        var bannerSwiper = new Swiper(".carousel-banner", {
            loop: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
                enabled: false,
            },
            autoplay: {
                delay: 3000,
                pauseOnMouseEnter: true,
                disableOnInteraction: false,
            },
            breakpoints: {
                640: {
                    pagination: {
                        enabled: true
                    }
                }
            }
        });

        // Get the modal by id
        var modal = document.getElementById("modal");

        // Get the modal image tag
        var modalImg = document.getElementById("modal-img");

        // this function is called when a small image is clicked
        function showModal(src) {
            modal.classList.remove('hidden');
            modalImg.src = src;
        }

        // this function is called when the close button is clicked
        function closeModal() {
            modal.classList.add('hidden');
        }
    </script>
</body>

</html>