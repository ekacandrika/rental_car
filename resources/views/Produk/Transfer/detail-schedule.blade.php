<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tur Detail') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles

    <style>
        .tab button.active {
            background-color: #FFF6FA;
            color: #9E3D64;
            border-bottom: 1px solid #9E3D64;
        }
    </style>
</head>

<body class="bg-white font-Inter">
    <header class="border border-b-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Gallery Lightbox --}}
    <div class="container mt-5 mb-2.5 lg:my-5 mx-auto">
        <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff" class="swiper mySwiper2 ">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="{{ asset('storage/img/car-detail-1.png') }}" class="object-cover object-center h-full" />
                </div>
                <div class="swiper-slide">
                    <img src="{{ asset('storage/img/car-detail-3-1.png') }}"
                        class="object-cover object-center h-full" />
                </div>
                <div class="swiper-slide">
                    <img src="{{ asset('storage/img/car-detail-4-1.png') }}"
                        class="object-cover object-center h-full" />
                </div>
                <div class="swiper-slide">
                    <img src="{{ asset('storage/img/car-detail-5-1.png') }}"
                        class="object-cover object-center h-full" />
                </div>
                <div class="swiper-slide">
                    <img src="{{ asset('storage/img/car-detail-6-1.png') }}"
                        class="object-cover object-center h-full" />
                </div>
            </div>
            <div class="swiper-button-next hidden"></div>
            <div class="swiper-button-prev hidden"></div>
        </div>
        <div thumbsSlider="" class="swiper mySwiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide cursor-pointer">
                    <img class="object-cover object-center h-full" src="{{ asset('storage/img/car-detail-2.png') }}" />
                </div>
                <div class="swiper-slide cursor-pointer">
                    <img class="object-cover object-center h-full" src="{{ asset('storage/img/car-detail-3.png') }}" />
                </div>
                <div class="swiper-slide cursor-pointer">
                    <img class="object-cover object-center h-full" src="{{ asset('storage/img/car-detail-4.png') }}" />
                </div>
                <div class="swiper-slide cursor-pointer">
                    <img class="object-cover object-center h-full" src="{{ asset('storage/img/car-detail-5.png') }}" />
                </div>
                <div class="swiper-slide cursor-pointer">
                    <img class="object-cover object-center h-full" src="{{ asset('storage/img/car-detail-6.png') }}" />
                </div>
            </div>
        </div>
    </div>

    <div>
        {{-- Tabs --}}
        <div class="container mx-auto my-2.5 lg:my-5 sticky bg-white top-0 z-20" x-data="{
            active: 0,
            tabs: ['Ringkasan', 'Kebijakan', 'Ulasan']
        }">
            <div class="flex sm:block px-2.5">
                <ul
                    class="flex justify-start overflow-x-auto text-sm font-medium text-center sm:text-left text-gray-500 dark:text-gray-400 border-b border-[#BDBDBD]">
                    <template x-for="(tab, index) in tabs" :key="index">
                        <li class="pb-2 mt-2" :class="{ 'border-b-2 border-[#9E3D64]': active == index }"
                            x-on:click="active = index">
                            <a :href="`#` + tab.toLowerCase()">
                                <button
                                    class="inline-block py-2 px-6 lg:px-9 text-sm lg:text-xl rounded-lg duration-200"
                                    :class="{ 'text-[#9E3D64] bg-white font-bold': active ==
                                        index, 'text-black font-normal hover:text-[#9E3D64]': active != index }"
                                    x-text="tab"></button>
                            </a>
                        </li>
                    </template>
                </ul>
            </div>
        </div>

        {{-- Content --}}
        <div class="container mx-auto">
            <div class="grid grid-cols-6 lg:gap-5">
                {{-- Left --}}
                <div class="col-span-6 lg:col-span-4 bg-white p-2.5 lg:p-5">
                    {{-- Ringkasan --}}
                    <div id="ringkasan">
                        <p class="text-3xl pb-2 font-semibold text-[#333333] whitespace-normal break-words">Nama Mobil
                        </p>
                        <div class="flex pb-2 items-center">
                            <img class="mr-2" src="{{ asset('storage/icons/Star 1.png') }}" alt="star"
                                width="30px" height="30px">
                            <p class="text-xl font-semibold text-[#333333]">4.7 <span class="font-normal">(12
                                    Ulasan)</span></p>
                        </div>
                        <div
                            class="grid grid-cols-2 lg:grid-cols-2 w-80 lg:w-96 py-2.5 lg:pt-5 space-y-2 sm:space-y-0 lg:gap-10">
                            <div class="space-y-2 mt-2 lg:mt-0">
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/car-solid 1.svg') }}" alt="user">
                                    Mercedes Benz
                                </p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/car-solid 1.svg') }}" alt="user-group">
                                    MVP
                                </p>
                            </div>
                            <div class="space-y-2">
                                <p class="flex font-sm font-medium"><img class="mr-2" width="20px"
                                        src="{{ asset('storage/icons/seats.svg') }}" alt="user">
                                    4 Seats
                                </p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="20px"
                                        src="{{ asset('storage/icons/koper.svg') }}" alt="user-group">
                                    2 Koper
                                </p>
                            </div>
                        </div>
                        <div class="text-justify my-2.5 lg:my-10">
                            <p class="text-xl lg:text-3xl mb-2.5 font-semibold">Deskripsi</p>
                            <p class="indent-8 text-xs lg:text-base"> Lorem ipsum dolor sit amet, consectetur
                                adipiscing
                                elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec
                                fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus.
                                Maecenas eget condimentum velit, sit amet feugiat lectus. Class aptent taciti sociosqu
                                ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent auctor purus
                                luctus enim egestas, ac scelerisque ante pulvinar. Donec ut rhoncus ex. Suspendisse ac
                                rhoncus nisl, eu tempor urna. Curabitur vel bibendum lorem. Morbi convallis convallis
                                diam sit amet lacinia. Aliquam in elementum tellus.</p>
                            <p class="indent-8 text-xs lg:text-base">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec
                                fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus.
                                Maecenas eget condimentum velit, sit amet feugiat lectus. Class aptent taciti sociosqu
                                ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent auctor purus
                                luctus enim egestas, ac scelerisque ante pulvinar. Donec ut rhoncus ex. Suspendisse ac
                                rhoncus nisl, eu tempor urna. Curabitur vel bibendum lorem. Morbi convallis convallis
                                diam sit amet lacinia. Aliquam in elementum tellus.</p>
                        </div>
                    </div>

                    <div>
                        <div>
                            <p class="text-xl lg:text-3xl font-semibold text-[#333333]">Jadwal</p>
                        </div>
                        <div class="bg-[#F2F2F2] my-2.5 p-2.5 lg:my-5 lg:p-5">

                            {{-- Pilih Bus --}}
                            <p class="text-lg lg:text-xl font-semibold mb-2.5 text-[#333333]">Pilih Bus</p>
                            <div class="grid grid-cols-none lg:grid-cols-2 gap-2.5 lg:gap-5 mb-2.5 lg:mb-5">
                                <a href="#" class="block p-2.5 lg:p-5 max-w-sm bg-[#23AEC1] rounded-lg">
                                    <div class="flex mx-2.5">
                                        <div class="text-white text-sm lg:text-base w-full">
                                            <p class="my-1"><b>BUS 001</b> - 06.00 WIB</p>
                                            <p class="my-1">Volvo B11R - Laksana SR2 Double Decker</p>
                                            <p class="my-1">25 Kursi <b>(Sisa 5 Kursi)</b></p>
                                            <p class="my-1">Mulai <b>Rp. 125,000</b></p>
                                        </div>
                                        <div class="w-6 h-6 lg:w-6 lg:h-6 ml-5 mt-1 ml-5 lg:ml-1">
                                            <img class="w-full" src="{{ asset('storage/icons/info-white.svg') }}"
                                                alt="info-hover">
                                        </div>
                                    </div>
                                </a>

                                <a href="#"
                                    class="block p-2.5 lg:p-5 max-w-sm bg-white rounded-lg border border-[#23AEC1] hover:bg-gray-100">
                                    <div class="flex mx-2.5">
                                        <div class="text-[#333333] text-sm lg:text-base w-full">
                                            <p class="my-1"><b>BUS 001</b> - 06.00 WIB</p>
                                            <p class="my-1">Volvo B11R - Laksana SR2 Double Decker</p>
                                            <p class="my-1">25 Kursi <b>(Sisa 5 Kursi)</b></p>
                                            <p class="my-1">Mulai <b>Rp. 125,000</b></p>
                                        </div>
                                        <div class="w-6 h-6 lg:w-6 lg:h-6 ml-5 lg:ml-1 mt-1">
                                            <img class="w-full"
                                                src="{{ asset('storage/icons/circle-info-solid (1) 1.svg') }}"
                                                alt="info-hover">
                                        </div>
                                    </div>
                                </a>

                                <a href="#"
                                    class="block p-2.5 lg:p-5 max-w-sm bg-white rounded-lg border border-[#23AEC1] hover:bg-gray-100">
                                    <div class="flex mx-2.5">
                                        <div class="text-[#333333] text-sm lg:text-base w-full">
                                            <p class="my-1"><b>BUS 001</b> - 06.00 WIB</p>
                                            <p class="my-1">Volvo B11R - Laksana SR2 Double Decker</p>
                                            <p class="my-1">25 Kursi <b>(Sisa 5 Kursi)</b></p>
                                            <p class="my-1">Mulai <b>Rp. 125,000</b></p>
                                        </div>
                                        <div class="w-6 h-6 lg:w-6 lg:h-6 ml-5 lg:ml-1 mt-1">
                                            <img class="w-full"
                                                src="{{ asset('storage/icons/circle-info-solid (1) 1.svg') }}"
                                                alt="info-hover">
                                        </div>
                                    </div>
                                </a>

                                <a href="#"
                                    class="block p-2.5 lg:p-5 max-w-sm bg-white rounded-lg border border-[#23AEC1] hover:bg-gray-100">
                                    <div class="flex mx-2.5">
                                        <div class="text-[#333333] text-sm lg:text-base w-full">
                                            <p class="my-1"><b>BUS 001</b> - 06.00 WIB</p>
                                            <p class="my-1">Volvo B11R - Laksana SR2 Double Decker</p>
                                            <p class="my-1">25 Kursi <b>(Sisa 5 Kursi)</b></p>
                                            <p class="my-1">Mulai <b>Rp. 125,000</b></p>
                                        </div>
                                        <div class="w-6 h-6 lg:w-6 lg:h-6 ml-5 lg:ml-1 mt-1">
                                            <img class="w-full"
                                                src="{{ asset('storage/icons/circle-info-solid (1) 1.svg') }}"
                                                alt="info-hover">
                                        </div>
                                    </div>
                                </a>
                            </div>

                            {{-- Lokasi Turun --}}
                            <p class="text-lg lg:text-xl font-semibold mb-2.5 text-[#333333]">Pilih Lokasi Naik dan
                                Turun</p>
                            <div class="grid grid-cols-none lg:grid-cols-2 gap-2.5 lg:gap-5 mb-2.5 lg:mb-5">
                                <a href="#"
                                    class="block p-1 lg:p-2.5 max-w-sm bg-[#23AEC1] rounded lg:rounded-lg">
                                    <div class="flex grid grid-cols-4 gap-5 mx-2.5">
                                        <div class="grid col-span-3 text-white text-sm lg:text-lg w-full">
                                            <p class="my-1">Naik 1 - Turun 1</p>
                                        </div>
                                        <div class="grid col-span-1 w-4 h-4 lg:w-5 lg:h-5 ml-5 mt-1 lg:ml-10">
                                            <img class="w-full" src="{{ asset('storage/icons/point-white.svg') }}"
                                                alt="info-hover">
                                        </div>
                                    </div>
                                </a>

                                <a href="#"
                                    class="block p-1 lg:p-2.5 max-w-sm bg-white rounded lg:rounded-lg border border-[#23AEC1] hover:bg-gray-100">
                                    <div class="flex grid grid-cols-4 gap-5 mx-2.5">
                                        <div class="grid col-span-3 text-[#333333] text-sm lg:text-lg w-full">
                                            <p class="my-1">Naik 1 - Turun 1</p>
                                        </div>
                                        <div class="grid col-span-1 w-4 h-4 lg:w-5 lg:h-5 ml-5 lg:ml-10 mt-1">
                                            <img class="w-full"
                                                src="{{ asset('storage/icons/location-dot-solid 1.svg') }}"
                                                alt="info-hover">
                                        </div>
                                    </div>
                                </a>

                                <a href="#"
                                    class="block p-1 lg:p-2.5 max-w-sm bg-white rounded lg:rounded-lg border border-[#23AEC1] hover:bg-gray-100">
                                    <div class="flex grid grid-cols-4 gap-5 mx-2.5">
                                        <div class="grid col-span-3 text-[#333333] text-sm lg:text-lg w-full">
                                            <p class="my-1">Naik 1 - Turun 1</p>
                                        </div>
                                        <div class="grid col-span-1 w-4 h-4 lg:w-5 lg:h-5 ml-5 lg:ml-10 mt-1">
                                            <img class="w-full"
                                                src="{{ asset('storage/icons/location-dot-solid 1.svg') }}"
                                                alt="info-hover">
                                        </div>
                                    </div>
                                </a>

                                <a href="#"
                                    class="block p-1 lg:p-2.5 max-w-sm bg-white rounded lg:rounded-lg border border-[#23AEC1] hover:bg-gray-100">
                                    <div class="flex grid grid-cols-4 gap-5 mx-2.5">
                                        <div class="grid col-span-3 text-[#333333] text-sm lg:text-lg w-full">
                                            <p class="my-1">Naik 1 - Turun 1</p>
                                        </div>
                                        <div class="grid col-span-1 w-4 h-4 lg:w-5 lg:h-5 ml-5 lg:ml-10 mt-1">
                                            <img class="w-full"
                                                src="{{ asset('storage/icons/location-dot-solid 1.svg') }}"
                                                alt="info-hover">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div id="kebijakan">
                        <div class="mb-4 border-b border-gray-200 dark:border-gray-700">
                            <ul class="flex flex-wrap -mb-px text-xs lg:text-base font-medium text-center tab gap-1"
                                id="myTab" data-tabs-toggle="#myTabContent" role="tablist">
                                <li class="mr-2" role="presentation">
                                    <button
                                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent text-gray-500 border-gray-100 tablinks active h-[66px] lg:h-auto"
                                        id="komplemen-tab" data-tabs-target="#komplemen" type="button"
                                        role="tab" aria-controls="komplemen" aria-selected="true"
                                        onclick="openCity(event, 'komplemen')">Komplemen</button>
                                </li>
                                <li class="mr-2 w-20 lg:w-auto" role="presentation">
                                    <button
                                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent text-gray-500 border-gray-100 tablinks"
                                        id="pilihan-tab" data-tabs-target="#pilihan" type="button" role="tab"
                                        aria-controls="pilihan" aria-selected="false"
                                        onclick="openCity(event, 'pilihan')">Pilihan Tambahan</button>
                                </li>
                                <li class="mr-2 w-20 lg:w-auto" role="presentation">
                                    <button
                                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent text-gray-500 border-gray-100 tablinks"
                                        id="kebijakan-tab" data-tabs-target="#kebijakan" type="button"
                                        role="tab" aria-controls="kebijakan" aria-selected="false"
                                        onclick="openCity(event, 'kebijakan')">Kebijakan Tambahan</button>
                                </li>
                                <li class="w-[64px] lg:w-auto" role="presentation">
                                    <button
                                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent text-gray-500 border-gray-100 tablinks h-[66px] lg:h-auto"
                                        id="faq-tab" data-tabs-target="#faq" type="button" role="tab"
                                        aria-controls="faq" aria-selected="false"
                                        onclick="openCity(event, 'faq')">FAQ</button>
                                </li>
                            </ul>
                        </div>
                        <div id="myTabContent">
                            <div class="rounded-lg dark:bg-gray-800 tabcontent" id="komplemen" role="tabpanel"
                                aria-labelledby="komplemen-tab">
                                <ul class="space-y-2 px-3 lg:py-2 font-medium text-gray-500 text-sm lg:text-lg md:text-base"
                                    x-show="open" x-transition>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                </ul>
                            </div>
                            <div class="hidden rounded-lg dark:bg-gray-800 tabcontent" id="pilihan" role="tabpanel"
                                aria-labelledby="pilihan-tab">
                                <ul class="space-y-2 px-3 lg:py-2 font-medium text-gray-500 text-sm lg:text-lg md:text-base"
                                    x-show="open" x-transition>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                </ul>
                            </div>
                            <div class="hidden rounded-lg dark:bg-gray-800 tabcontent" id="kebijakan" role="tabpanel"
                                aria-labelledby="kebijakan-tab">
                                <ul class="space-y-2 px-3 lg:py-2 font-medium text-gray-500 text-sm lg:text-lg md:text-base"
                                    x-show="open" x-transition>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                </ul>
                            </div>
                            <div class="hidden rounded-lg dark:bg-gray-800 tabcontent" id="faq" role="tabpanel"
                                aria-labelledby="faq-tab">
                                <ul class="space-y-2 px-3 lg:py-2 font-medium text-gray-500 text-sm lg:text-lg md:text-base"
                                    x-show="open" x-transition>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                    <li>Lorem Ipsum</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    {{-- Ulasan --}}
                    <div id="ulasan" class="mt-2 lg:mb-5 lg:mb-20">
                        <div>
                            <div class="container max-w-6xl">
                                <div>
                                    <p class="text-xl lg:text-3xl my-2.5 font-semibold">Ulasan</p>
                                </div>
                                <div class="flex mb-4 border border-gray-400 rounded-lg shadow-lg">
                                    <input type="text" placeholder="Masukan Ulasan Anda..."
                                        class="flex-auto block py-4 font-medium border border-transparent rounded-lg outline-none focus:border-[#9E3D64] focus:text-green-500" />
                                </div>

                                <div class="lg:grid lg:grid-cols-2 py-2.5 lg:py-5">
                                    <div class="flex">
                                        <img src="{{ asset('storage/icons/star-transfer.svg') }}"
                                            class="w-5 h-5 lg:w-[48px] lg:h-[48px] mt-[0.2rem] mr-1 inline-flex"
                                            alt="rating" title="Rating">
                                        <p class="flex"> <span class="lg:text-[50px] font-semibold ">4.0</span>
                                            <span class="lg:text-[20px] lg:pt-9"> /5.0 dari 12 ulasan</span></p>
                                    </div>
                                </div>

                                {{-- ulasan desktop --}}
                                <div class="hidden lg:block">
                                    <div class="pb-5">
                                        <div class="grid grid-cols-4 divide-x rounded-lg shadow-lg">
                                            <div class="p-5 col-span-1 grid justify-items-center">
                                                <p class="text-[18px] font-bold">Jhonny Wilson</p>
                                                <p class="text-[18px]">10 Agustus 2022</p>
                                                <div class="flex px-2">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex pt-3"> <span
                                                            class="text-[16px] font-semibold ">4.0</span> <span
                                                            class="text-[16px]"> /5.0</span></p>
                                                </div>
                                            </div>
                                            <div class="col-start-2 col-span-3 p-5">
                                                <p class="text-[18px]">Lorem ipsum dolor sit amet consectetur
                                                    adipisicing elit. Esse maiores adipisci mollitia explicabo.
                                                    Voluptates sapiente exercitationem sunt quo sint labore ipsam,
                                                    voluptatem consequatur optio ipsum iusto, iure doloremque in
                                                    mollitia!</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pb-5">
                                        <div class="grid grid-cols-4 divide-x rounded-lg shadow-lg">
                                            <div class="p-5 col-span-1 grid justify-items-center">
                                                <p class="text-[18px] font-bold">Jhonny Wilson</p>
                                                <p class="text-[18px]">10 Agustus 2022</p>
                                                <div class="flex px-2">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex pt-3"> <span
                                                            class="text-[16px] font-semibold ">4.0</span> <span
                                                            class="text-[16px]"> /5.0</span></p>
                                                </div>
                                            </div>
                                            <div class="col-start-2 col-span-3 p-5">
                                                <p class="text-[18px]">Lorem ipsum dolor sit amet consectetur
                                                    adipisicing elit. Esse maiores adipisci mollitia explicabo.
                                                    Voluptates sapiente exercitationem sunt quo sint labore ipsam,
                                                    voluptatem consequatur optio ipsum iusto, iure doloremque in
                                                    mollitia!</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pb-5">
                                        <div class="grid grid-cols-4 divide-x rounded-lg shadow-lg">
                                            <div class="p-5 col-span-1 grid justify-items-center">
                                                <p class="text-[18px] font-bold">Jhonny Wilson</p>
                                                <p class="text-[18px]">10 Agustus 2022</p>
                                                <div class="flex px-2">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex pt-3"> <span
                                                            class="text-[16px] font-semibold ">4.0</span> <span
                                                            class="text-[16px]"> /5.0</span></p>
                                                </div>
                                            </div>
                                            <div class="col-start-2 col-span-3 p-5">
                                                <p class="text-[18px]">Lorem ipsum dolor sit amet consectetur
                                                    adipisicing elit. Esse maiores adipisci mollitia explicabo.
                                                    Voluptates sapiente exercitationem sunt quo sint labore ipsam,
                                                    voluptatem consequatur optio ipsum iusto, iure doloremque in
                                                    mollitia!</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pb-5">
                                        <div class="grid grid-cols-4 divide-x rounded-lg shadow-lg">
                                            <div class="p-5 col-span-1 grid justify-items-center">
                                                <p class="text-[18px] font-bold">Jhonny Wilson</p>
                                                <p class="text-[18px]">10 Agustus 2022</p>
                                                <div class="flex px-2">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex pt-3"> <span
                                                            class="text-[16px] font-semibold ">4.0</span> <span
                                                            class="text-[16px]"> /5.0</span></p>
                                                </div>
                                            </div>
                                            <div class="col-start-2 col-span-3 p-5">
                                                <p class="text-[18px]">Lorem ipsum dolor sit amet consectetur
                                                    adipisicing elit. Esse maiores adipisci mollitia explicabo.
                                                    Voluptates sapiente exercitationem sunt quo sint labore ipsam,
                                                    voluptatem consequatur optio ipsum iusto, iure doloremque in
                                                    mollitia!</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- ulasan mobile --}}
                                <div class="block lg:hidden md:hidden">
                                    <div class="pb-5">
                                        <div class="grid grid-cols-4 divide-x rounded-lg shadow-lg">
                                            <div class="p-2.5 lg:p-5 col-span-1 grid justify-items-center">
                                                <p class="text-[10px] lg:text-[18px] font-bold">Jhonny Wilson</p>
                                                <p class="text-[8px] lg:text-[18px] p-1">10 Agustus 2022</p>
                                                <div class="flex lg:px-2">
                                                    <img src="{{ asset('storage/icons/star-transfer.svg') }}"
                                                        class="w-[14px] h-[14px] lg:w-[30px] lg:h-[30px] lg:mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex lg:pt-3"> <span
                                                            class="text-[10px] lg:text-[16px] font-semibold ">4.0</span>
                                                        <span class="text-[10px] lg:text-[16px]"> /5.0</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-start-2 col-span-3 p-2.5 lg:p-5">
                                                <p class="text-[8px] lg:text-[18px]">Lorem ipsum dolor sit amet
                                                    consectetur adipisicing elit. Esse maiores adipisci mollitia
                                                    explicabo. Voluptates sapiente exercitationem sunt quo sint labore
                                                    ipsam, voluptatem consequatur optio ipsum iusto, iure doloremque in
                                                    mollitia!</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Right --}}
                <div class="col-span-6 lg:col-span-2 space-y-5 p-2.5 lg:p-5">
                    {{-- Detail --}}
                    <div class="lg:block rounded-md shadow-lg p-5 mb-5 bg-white border border-gray-200">
                        <p class="text-xl font-semibold mb-2.5 text-[#9E3D64] text-center">Nama Bus</p>
                        <p class="text-base font-semibold mb-2.5">Jumat, 12 Agustus 2022 • 09.00</p>
                        <p class="text-base font-semibold mb-2.5">Dari</p>
                        <div class="flex">
                            <img src="{{ asset('storage/icons/bus-solid 1.svg') }}" alt="bus" width="20px"
                                height="20px">
                            <p class="ml-2.5 text-base font-semibold">Bandara Internasional YIA</p>
                        </div>
                        <img class="py-2" src="{{ asset('storage/icons/line-trans.svg') }}" alt="line">
                        <p class="text-base font-semibold mb-2.5">Ke</p>
                        <div class="flex">
                            <img src="{{ asset('storage/icons/location-dot-solid 1.svg') }}" alt="loc"
                                width="15px" height="15px">
                            <p class="ml-2.5 text-base font-semibold">Hotel Yogyakarta</p>
                        </div>
                    </div>
                    {{-- Price --}}
                    <div class="lg:block rounded-md shadow-lg p-5 mb-5 bg-white border border-gray-200">
                        <p class="text-xl lg:text-2xl xl:text-3xl text-center font-bold text-[#23AEC1]">IDR 500,000</p>
                        <div class="justify-center">
                            <a href="/transfer/pemesanan">
                                <x-destindonesia.button-primary text="Pesan Sekarang">
                                    @slot('button')
                                        Pesan Sekarang
                                    @endslot
                                </x-destindonesia.button-primary>
                            </a>
                        </div>
                    </div>

                    {{-- Store --}}
                    <div class="p-5 my-5 bg-white border-gray-200 lg:rounded-md lg:shadow-lg border-y-2 lg:border">
                        <a href={{route('toko.show',$transfer->user->id)}}>
                            <p class="lg:text-2xl xl:text-3xl font-bold text-center text-[#333333]">Kunjungi Toko</p>
                        </a>
                        <div class="flex justify-center py-5">
                            <a href={{route('toko.show',$transfer->user->id)}}>
                                <img src="{{ isset($info_toko->logo_toko) ? asset($info_toko->logo_toko) : asset('storage/img/tur-detail-1.png') }}"
                                    class="bg-clip-content lg:w-[96px] lg:h-[96px] xl:w-[100px] xl:h-[100px] shadow-xl bg-white rounded-full"
                                    alt="store-profile">
                            </a>    
                        </div>
                        <p class="pt-3 text-xl font-semibold text-center">{{ isset($info_toko->nama_toko) ? $info_toko->nama_toko :'Nama Toko'}}</p>
                        <p class="pb-2 text-lg font-normal text-center">Bergabung sejak {{
                            isset($transfer->user->created_at)
                            ? date('Y',strtotime($transfer->user->created_at)) : '2021'}}</p>
                        <div class="flex justify-center">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                        </div>
                        <div class="flex justify-center">
                            <x-destindonesia.button-primary text="Kirim Pesan">
                                @slot('button')
                                Kirim Pesan
                                @endslot
                            </x-destindonesia.button-primary>
                        </div>
                    </div>

                    {{-- QR Code --}}
                    <div>
                        <p class="text-xl lg:text-2xl font-semibold text-center">Kode QR Produk</p>
                        <div class="flex justify-center my-5">
                            <img src="{{ asset('storage/img/qr-code-transfer.png') }}" alt="QR-code">
                        </div>
                        <a class="justify-center" href="/storage/img/tur-detail-2.png" download="QR-code-produk">
                            <x-destindonesia.button-primary text="Unduh Kode QR">
                                @slot('button')
                                    Unduh Kode QR
                                @endslot
                            </x-destindonesia.button-primary>
                        </a>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        var swiper = new Swiper(".mySwiper", {
            spaceBetween: 2,
            slidesPerView: 5,
            freeMode: true,
            watchSlidesProgress: true,
        });
        var swiper2 = new Swiper(".mySwiper2", {
            spaceBetween: 2,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            thumbs: {
                swiper: swiper,
            },
        });
    </script>
    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
</body>

</html>
