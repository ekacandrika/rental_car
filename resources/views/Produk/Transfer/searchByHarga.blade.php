<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Search Tur') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

     {{-- select 2 --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
 

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #sedan:checked+#sedan {
            display: block;
        }

        #filter-toggle:checked+#filter {
            display: block;
        }
        .select2-container .select2-selection--single {
            height: 100%;
            /* padding-left: 0.75rem; */
            /* padding-right: 0.75rem; */
            padding-top: 0rem;
            padding-bottom: 0rem;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 100%;
        }
        .select2-selection__arrow {
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            /* padding-right: 0.75rem; */
        }
        .select2-container--default .select2-selection--single{
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            /* border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px; */
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {

            color: #444;
            line-height: 35px;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border border-b-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Hero --}}
    <div class="relative bg-slate-400">
        <img class="w-full opacity-70 object-cover" src="{{ asset('storage/img/Header.png') }}" alt="banner-tur">
    </div>
    <div class="container mx-auto my-5 text-center">
        <x-produk.cari-transfer>
            <x-slot name="transferfrom">
                <select name="search" id="transferfrombyprice" style="background-image: none;" required
                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                    <option value="">Dari</option>
                    @foreach($regencyTo as $lokasi)
                    <option value={{$lokasi->id}}>{{$lokasi->name}}</option>
                        @foreach($lokasi->regencies as $kota)
                            <option value={{$kota->id}}>{{$kota->name}}</option>   
                        @endforeach
                    @endforeach
                </select>
            </x-slot>
            <x-slot name="transferto">
                <select name="to" id="transfertobyprice" style="background-image: none;" required
                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                    <option value="">Ke</option>
                    @foreach($regencyTo as $lokasi)
                    <option value={{$lokasi->id}}>{{$lokasi->name}}</option>
                        @foreach($lokasi->regencies as $kota)
                            <option value={{$kota->id}}>{{$kota->name}}</option>   
                        @endforeach
                    @endforeach
                </select>
            </x-slot>
        </x-produk.cari-transfer>
    </div>
    <div class="container my-5 mx-2.5 w-auto">
        <div class="grid grid-cols-2 lg:grid-cols-none">
            <div class="grid justify-items-start text-left lg:text-center">
            </div>
            <div class="grid justify-items-end">
                <label
                    class="items-center block w-10 px-2.5 py-2 text-white rounded-full cursor-pointer lg:hidden hover:text-white"
                    for="filter-toggle">
                    <img class="w-full opacity-70 object-cover" src="{{ asset('storage/icons/filter-solid.svg') }}"
                        alt="filter-toogle">
                </label>
            </div>
        </div>

        <input class="hidden" type="checkbox" id="filter-toggle" />

        <div class="hidden shadow-lg divide-y divide-y-reverse mt-2.5 rounded-lg" id="filter">
            <div class="p-2.5">
                <p class="my-2 font-semibold text-xs">Layanan</p>
                <div class="flex items-center mb-1">
                    <input id="default-checkbox" type="checkbox" value=""
                        class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                    <label for="default-checkbox"
                        class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Layanan 1</label>
                </div>
                <div class="flex items-center">
                    <input checked="" id="checked-checkbox" type="checkbox" value=""
                        class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                    <label for="checked-checkbox"
                        class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Layanan 2</label>
                </div>
            </div>
            <div class="p-2.5">
                <p class="mb-2 font-semibold text-xs">Harga</p>
                <input type="text" placeholder="Harga Minimum"
                    class="placeholder:text-slate-400 mb-3 block bg-[#F2F2F2] w-full border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-[#9E3D64] focus:ring-[#9E3D64] focus:ring-1 text-xs">
                <input type="text" placeholder="Harga Maksimum"
                    class="placeholder:text-slate-400 block bg-[#F2F2F2] w-full border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-[#9E3D64] focus:ring-[#9E3D64] focus:ring-1 text-xs">
            </div>
            <div class="px-2.5 pb-2.5">
                <p class="my-2 font-semibold text-xs">Kategori</p>
                <div class="items-center">
                    <label class="flex items-center" for="sedan">
                        <p class="flex-1 mt-1 whitespace-nowrap font-inter text-xs font-medium">Sedan</p>
                        <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}" class="w-[12px] h-[12px] mt-1"
                            alt="user" title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="sedan" />

                    <div id="sedan" class="my-2 ml-2 hidden">
                        <div class="flex items-center">
                            <input id="default-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="default-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 1</label>
                        </div>
                        <div class="flex items-center mt-1">
                            <input checked="" id="checked-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="checked-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 2</label>
                        </div>
                    </div>
                </div>

                <div class="items-center">
                    <label class="flex items-center" for="mini">
                        <p class="flex-1 mt-1 whitespace-nowrap font-inter text-xs font-medium">Mini</p>
                        <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            class="w-[12px] h-[12px] mt-1" alt="user" title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="mini" />

                    <div id="mini" class="my-2 ml-2 hidden">
                        <div class="flex items-center">
                            <input id="default-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="default-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 1</label>
                        </div>
                        <div class="flex items-center">
                            <input checked="" id="checked-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="checked-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 2</label>
                        </div>
                    </div>
                </div>

                <div class="items-center">
                    <label class="flex items-center" for="mpv">
                        <p class="flex-1 mt-1 whitespace-nowrap font-inter text-xs font-medium">MVP</p>
                        <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            class="w-[12px] h-[12px] mt-1" alt="user" title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="mpv" />

                    <div id="mpv" class="my-2 ml-2 hidden">
                        <div class="flex items-center">
                            <input id="default-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="default-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 1</label>
                        </div>
                        <div class="flex items-center">
                            <input checked="" id="checked-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="checked-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 2</label>
                        </div>
                    </div>
                </div>

                <div class="items-center">
                    <label class="flex items-center" for="luxury">
                        <p class="flex-1 mt-1 whitespace-nowrap font-inter text-xs font-medium">Luxury</p>
                        <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            class="w-[12px] h-[12px] mt-1" alt="user" title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="luxury" />

                    <div id="luxury" class="my-2 ml-2 hidden">
                        <div class="flex items-center">
                            <input id="default-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="default-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 1</label>
                        </div>
                        <div class="flex items-center">
                            <input checked="" id="checked-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="checked-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 2</label>
                        </div>
                    </div>
                </div>

                <div class="items-center">
                    <label class="flex items-center" for="minibus">
                        <p class="flex-1 mt-1 whitespace-nowrap font-inter text-xs font-medium">Mini Bus</p>
                        <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            class="w-[12px] h-[12px] mt-1" alt="user" title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="minibus" />

                    <div id="minibus" class="my-2 ml-2 hidden">
                        <div class="flex items-center">
                            <input id="default-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="default-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 1</label>
                        </div>
                        <div class="flex items-center">
                            <input checked="" id="checked-checkbox" type="checkbox" value=""
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="checked-checkbox"
                                class="ml-2 text-xs font-medium text-gray-900 dark:text-gray-300">Mobil 2</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="container my-2.5 lg:my-10 lg:mx-auto">
        <div class="grid grid-cols-5 lg:grid-cols-10 gap-2 mx-2.5">
            <div class="col-start-1 col-end-3">
                <p class="text-sm lg:text-2xl font-semibold">Jenis Kendaraan</p>
            </div>
            <button type="submit"
                class="items-center px-px py-px lg:py-2.5 text-xs lg:text-sm font-medium text-center text-white bg-[#23AEC1] rounded-full">Mobil</button>
            <button type="submit"
                class="items-center px-px py-px lg:py-2.5 text-xs lg:text-sm font-medium text-center text-[#333333] bg-[#F2F2F2] rounded-full">Motor</button>
            <button type="submit"
                class="items-center px-px py-px lg:py-2.5 text-xs lg:text-sm font-medium text-center text-[#333333] bg-[#F2F2F2] rounded-full">Boat</button>
        </div>
    </div>

    <div class="container mx-auto">
        <div class="grid grid-cols-10 mx-2.5 gap-2.5 lg:gap-5">
            {{-- Filter --}}
            <div class="hidden lg:block col-start-1 col-end-3">
                <p class="text-sm lg:text-2xl font-semibold">Filter</p>
                <div class="shadow-lg divide-y divide-y-reverse mt-5 rounded-lg">
                    <form action="{{ route('transfer.filterByHargaTransfer') }}" method="POST">
                        @csrf
                        <input type="hidden" name="search" value="{{ $dari }}">
                        <input type="hidden" name="to" value="{{ $ke }}">
                        <input type="hidden" name="tgl" value="{{ $tgl }}">
                        <input type="hidden" name="waktu_ambil" value="{{ $waktu_ambil }}">
                        <input type="hidden" name="metode_transfer" value="{{ $metode_transfer }}">
                        {{-- <div class="p-5">
                            <p class="my-2 font-semibold">Layanan</p>
                            <div class="flex items-center mb-4">
                                <input id="default-checkbox" type="checkbox" value=""
                                    class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="default-checkbox"
                                    class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Layanan 1</label>
                            </div>
                            <div class="flex items-center">
                                <input checked="" id="checked-checkbox" type="checkbox" value=""
                                    class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="checked-checkbox"
                                    class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Layanan 2</label>
                            </div>
                        </div> --}}
                        <div class="p-5">
                            <p class="my-2 font-semibold">Harga</p>
                            <input type="text" name="min_price" placeholder="Harga Minimum"
                                class="placeholder:text-slate-400 mb-3 block bg-[#F2F2F2] w-full border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-[#9E3D64] focus:ring-[#9E3D64] focus:ring-1 sm:text-sm">
                            <input type="text" name="max_price" placeholder="Harga Maksimum"
                                class="placeholder:text-slate-400 mb-3 block bg-[#F2F2F2] w-full border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-[#9E3D64] focus:ring-[#9E3D64] focus:ring-1 sm:text-sm">
                        </div>
                        <div class="p-5">
                            <p class="my-2 font-semibold">Kategori</p>
                            <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                                <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                                    <button
                                        class="text-sm sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                        x-on:click="open = !open">Sedan</button>
                                    <img class="float-right duration-200 cursor-pointer"
                                        :class="{ 'rotate-180': open }" x-on:click="open = !open"
                                        src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                        alt="chevron-down" width="15px" height="15px">
                                </div>
                                <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                                    x-show="open" x-transition>
                                    <div class="flex items-center">
                                        <input id="default-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="default-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            1</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input checked="" id="checked-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="checked-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            2</label>
                                    </div>
                                </ul>
                            </div>

                            <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                                <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                                    <button
                                        class="text-sm sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                        x-on:click="open = !open">Mini</button>
                                    <img class="float-right duration-200 cursor-pointer"
                                        :class="{ 'rotate-180': open }" x-on:click="open = !open"
                                        src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                        alt="chevron-down" width="15px" height="15px">
                                </div>
                                <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                                    x-show="open" x-transition>
                                    <div class="flex items-center">
                                        <input id="default-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="default-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            1</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input checked="" id="checked-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="checked-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            2</label>
                                    </div>
                                </ul>
                            </div>

                            <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                                <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                                    <button
                                        class="text-sm sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                        x-on:click="open = !open">MVP</button>
                                    <img class="float-right duration-200 cursor-pointer"
                                        :class="{ 'rotate-180': open }" x-on:click="open = !open"
                                        src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                        alt="chevron-down" width="15px" height="15px">
                                </div>
                                <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                                    x-show="open" x-transition>
                                    <div class="flex items-center">
                                        <input id="default-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="default-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            1</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input checked="" id="checked-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="checked-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            2</label>
                                    </div>
                                </ul>
                            </div>

                            <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                                <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                                    <button
                                        class="text-sm sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                        x-on:click="open = !open">Luxury</button>
                                    <img class="float-right duration-200 cursor-pointer"
                                        :class="{ 'rotate-180': open }" x-on:click="open = !open"
                                        src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                        alt="chevron-down" width="15px" height="15px">
                                </div>
                                <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                                    x-show="open" x-transition>
                                    <div class="flex items-center">
                                        <input id="default-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="default-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            1</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input checked="" id="checked-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="checked-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            2</label>
                                    </div>
                                </ul>
                            </div>

                            <div class="text-sm font-medium text-justify sm:text-left" x-data="{ open: false }">
                                <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-px': !open }">
                                    <button
                                        class="text-sm sm:text-xl lg:text-sm text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                        x-on:click="open = !open">Mini Bus</button>
                                    <img class="float-right duration-200 cursor-pointer"
                                        :class="{ 'rotate-180': open }" x-on:click="open = !open"
                                        src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                        alt="chevron-down" width="15px" height="15px">
                                </div>
                                <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                                    x-show="open" x-transition>
                                    <div class="flex items-center">
                                        <input id="default-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="default-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            1</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input checked="" id="checked-checkbox" type="checkbox" value=""
                                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="checked-checkbox"
                                            class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Mobil
                                            2</label>
                                    </div>
                                </ul>
                            </div>
                        </div>
                        <hr>
                        <div class="p-5">
                            <button
                                class="px-2 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white ">
                                Terapkan
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-span-10 lg:col-span-8">
                <p class="my-2.5 lg:my-5 text-xs lg:text-base col-span-10 lg:col-span-1">Menampilkan 5 Mobil <span
                        class="font-semibold italic">"Bandara A ke Hotel A pada tanggal
                        {{ Carbon\Carbon::parse($tgl)->translatedFormat('d F Y') }} Jam
                        {{ Carbon\Carbon::parse($waktu_ambil)->translatedFormat('H:i') }}"</span></p>
                <div class="grid grid-cols-10 lg:grid-cols-1 gap-5">
                    @if (
                        $dari != null &&
                            $ke != null &&
                            $tgl != null &&
                            $waktu_ambil != null &&
                            $tgl != null &&
                            $metode_transfer == 'Transfer Umum')
                        @foreach ($detail_transfer as $val)
                            @php
                                $harga = App\Models\DetailRute::where('id', $val->rute_id)->first();
                            @endphp
                            @if(isset($harga->harga))
                                @if ($harga->harga >= $min_price && $harga->harga <= $max_price)
                                <a href="{{ route('transfer.show', $val->product->slug) }}"
                                    class="lg:grid col-span-5 lg:grid-cols-10 grid-rows-3 lg:grid-rows-none grid-flow-col border-2 border-zinc-50 rounded-lg hover:shadow-xl duration-200 py-2.5 ">
                                    <div class="row-span-1 col-span-5 lg:col-start-1 lg:col-span-2 ">
                                        <img class="rounded-t-lg object-cover object-center lg:p-5 h-32 lg:h-max w-full"
                                            src="{{ asset('storage/img/img-car.png') }}" alt="transfer-1">
                                    </div>
                                    <div class="row-span-1 lg:col-start-3 lg:col-end-7">
                                        <div class="p-2.5 lg:py-5">
                                            <p class="text-sm lg:text-lg font-semibold">
                                                {{ $val->detailkendaraan->nama_kendaraan }}
                                            </p>
                                            <div class="flex my-px gap-1 lg:gap-2">
                                                <div
                                                    class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                                    Sedan</div>
                                            </div>
                                            <div class="my-1 lg:my-2 flex">
                                                <img class="mr-1 w-3 h-3 lg:w-3.5 lg:h-3.5"
                                                    src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                                    width="15px" height="15px">
                                                <p class="font-bold text-[8px] lg:text-xs">4.7 <span
                                                        class="font-semibold text-[#4F4F4F]">(12 Ulasan)</span></p>
                                            </div>
                                            <div class="flex mt-3.5 lg:mt-10 lg:pt-2">
                                                <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                                    src="{{ asset('storage/icons/seats.svg') }}" alt="rating"
                                                    width="17px" height="17px">
                                                <p class="font-bold text-[8px] lg:text-sm mr-2">
                                                    {{ $val->detailkendaraan->kapasitas_kursi }} Seats</p>
                                                <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                                    src="{{ asset('storage/icons/koper.svg') }}" alt="rating"
                                                    width="17px" height="17px">
                                                <p class="font-bold  text-[8px] lg:text-sm mr-2">
                                                    {{ $val->detailkendaraan->kapasitas_koper }} Koper</p>
                                            </div>
                                            <p class="font-semibold text-[8px] lg:text-sm mt-1">Layanan 1 • Layanan 1
                                            </p>
                                        </div>
                                    </div>
                                    <div
                                        class="row-span-1 lg:col-start-7 lg:col-span-8 text-left lg:text-center px-2.5">
                                        <div class="pb-2.5 lg:py-12">
                                            <p class="font-bold text-xs lg:text-2xl text-[#23AEC1]">IDR
                                                {{ number_format($harga->harga) }}</p>
                                            <p class="font-medium text-[8px] lg:text-base text-white">Belum termasuk
                                                biaya tol</p>
                                            <button type="submit"
                                                class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-lg font-semibold text-center text-white bg-[#FFB800] rounded lg:rounded-lg">Pesan</button>
                                        </div>
                                    </div>
                                </a>
                                @endif
                            @endif
                        @endforeach
                    @endif

                    @if (
                        $dari != null &&
                            $ke != null &&
                            $tgl != null &&
                            $waktu_ambil != null &&
                            $tgl != null &&
                            $metode_transfer == 'Transfer Private')
                        @foreach ($detail_transfer_private as $fuu)
                            @php
                                $harga = App\Models\DetailRute::where('id', $fuu->rute_id)->first();
                            @endphp
                            @if ($harga->harga >= $min_price && $harga->harga <= $max_price)
                                <a href="{{ route('transfer.show', $fuu->product->slug) }}"
                                    class="lg:grid col-span-5 lg:grid-cols-10 grid-rows-3 lg:grid-rows-none grid-flow-col border-2 border-zinc-50 rounded-lg hover:shadow-xl duration-200 py-2.5 ">
                                    <div class="row-span-1 col-span-5 lg:col-start-1 lg:col-span-2 ">
                                        <img class="rounded-t-lg object-cover object-center lg:p-5 h-32 lg:h-max w-full"
                                            src="{{ asset('storage/img/img-car.png') }}" alt="transfer-1">
                                    </div>
                                    <div class="row-span-1 lg:col-start-3 lg:col-end-7">
                                        <div class="p-2.5 lg:py-5">
                                            <p class="text-sm lg:text-lg font-semibold">
                                                {{ $fuu->detailkendaraan->nama_kendaraan }}
                                            </p>
                                            <div class="flex my-px gap-1 lg:gap-2">
                                                <div
                                                    class="bg-[#9E3D64] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                                    Privat</div>
                                                <div
                                                    class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                                    Sedan</div>
                                            </div>
                                            <div class="my-1 lg:my-2 flex">
                                                <img class="mr-1 w-3 h-3 lg:w-3.5 lg:h-3.5"
                                                    src="{{ asset('storage/icons/Star 1.png') }}" alt="rating"
                                                    width="15px" height="15px">
                                                <p class="font-bold text-[8px] lg:text-xs">4.7 <span
                                                        class="font-semibold text-[#4F4F4F]">(12 Ulasan)</span></p>
                                            </div>
                                            <div class="flex mt-3.5 lg:mt-10 lg:pt-2">
                                                <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                                    src="{{ asset('storage/icons/seats.svg') }}" alt="rating"
                                                    width="17px" height="17px">
                                                <p class="font-bold text-[8px] lg:text-sm mr-2">
                                                    {{ $fuu->detailkendaraan->kapasitas_kursi }} Seats</p>
                                                <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                                    src="{{ asset('storage/icons/koper.svg') }}" alt="rating"
                                                    width="17px" height="17px">
                                                <p class="font-bold  text-[8px] lg:text-sm mr-2">
                                                    {{ $fuu->detailkendaraan->kapasitas_koper }} Koper</p>
                                            </div>
                                            <p class="font-semibold text-[8px] lg:text-sm mt-1">Layanan 1 • Layanan 1
                                            </p>
                                        </div>
                                    </div>
                                    <div
                                        class="row-span-1 lg:col-start-7 lg:col-span-8 text-left lg:text-center px-2.5">
                                        <div class="pb-2.5 lg:py-12">
                                            <p class="font-bold text-xs lg:text-2xl text-[#23AEC1]">IDR
                                                {{ number_format($harga->harga) }}</p>
                                            <p class="font-medium text-[8px] lg:text-base">Belum termasuk biaya tol</p>
                                            <button type="submit"
                                                class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-lg font-semibold text-center text-white bg-[#FFB800] rounded lg:rounded-lg">Pesan</button>
                                        </div>
                                    </div>
                                </a>
                            @endif
                        @endforeach
                    @endif

                    {{-- <a href="#"
                        class="lg:grid col-span-5 lg:grid-cols-10 grid-rows-3 lg:grid-rows-none grid-flow-col border-2 border-zinc-50 rounded-lg hover:shadow-xl duration-200 py-2.5 ">
                        <div class="row-span-1 col-span-5 lg:col-start-1 lg:col-span-2 ">
                            <img class="rounded-t-lg object-cover object-center lg:p-5 h-32 lg:h-max w-full"
                                src="{{ asset('storage/img/img-car.png') }}" alt="transfer-1">
                        </div>
                        <div class="row-span-1 lg:col-start-3 lg:col-end-7">
                            <div class="p-2.5 lg:py-5">
                                <p class="text-sm lg:text-lg font-semibold">Nama Mobil</p>
                                <div class="flex my-px gap-1 lg:gap-2">
                                    <div
                                        class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                        Sedan</div>
                                </div>
                                <div class="my-1 lg:my-2 flex">
                                    <img class="mr-1 w-3 h-3 lg:w-3.5 lg:h-3.5"
                                        src="{{ asset('storage/icons/Star 1.png') }}" alt="rating" width="15px"
                                        height="15px">
                                    <p class="font-bold text-[8px] lg:text-xs">4.7 <span
                                            class="font-semibold text-[#4F4F4F]">(12 Ulasan)</span></p>
                                </div>
                                <div class="flex mt-3.5 lg:mt-10 lg:pt-2">
                                    <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                        src="{{ asset('storage/icons/seats.svg') }}" alt="rating" width="17px"
                                        height="17px">
                                    <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                                    <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                        src="{{ asset('storage/icons/koper.svg') }}" alt="rating" width="17px"
                                        height="17px">
                                    <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                                </div>
                                <p class="font-semibold text-[8px] lg:text-sm mt-1">Layanan 1 • Layanan 1 </p>
                            </div>
                        </div>
                        <div class="row-span-1 lg:col-start-7 lg:col-span-8 text-left lg:text-center px-2.5">
                            <div class="pb-2.5 lg:py-12">
                                <p class="font-bold text-xs lg:text-2xl text-[#23AEC1]">IDR 100,000</p>
                                <p class="font-medium text-[8px] lg:text-base text-white">Belum termasuk biaya tol</p>
                                <button type="submit"
                                    class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-lg font-semibold text-center text-white bg-[#FFB800] rounded lg:rounded-lg">Pesan</button>
                            </div>
                        </div>
                    </a> --}}

                    {{-- <a href="#"
                        class="lg:grid col-span-5 lg:grid-cols-10 grid-rows-3 lg:grid-rows-none grid-flow-col border-2 border-zinc-50 rounded-lg hover:shadow-xl duration-200 mb-5 py-2.5 ">
                        <div class="row-span-1 col-span-5 lg:col-start-1 lg:col-span-2 ">
                            <img class="rounded-t-lg object-cover object-center lg:p-5 h-32 lg:h-max w-full"
                                src="{{ asset('storage/img/img-car.png') }}" alt="transfer-1">
                        </div>
                        <div class="row-span-1 lg:col-start-3 lg:col-end-7">
                            <div class="p-2.5 lg:py-5">
                                <p class="text-sm lg:text-lg font-semibold">Nama Mobil</p>
                                <div class="flex my-px gap-1 lg:gap-2">
                                    <div
                                        class="bg-[#9E3D64] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                        Privat</div>
                                    <div
                                        class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                        Sedan</div>
                                </div>
                                <div class="my-1 lg:my-2 flex">
                                    <img class="mr-1 w-3 h-3 lg:w-3.5 lg:h-3.5"
                                        src="{{ asset('storage/icons/Star 1.png') }}" alt="rating" width="15px"
                                        height="15px">
                                    <p class="font-bold text-[8px] lg:text-xs">4.7 <span
                                            class="font-semibold text-[#4F4F4F]">(12 Ulasan)</span></p>
                                </div>
                                <div class="flex mt-3.5 lg:mt-10 lg:pt-2">
                                    <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                        src="{{ asset('storage/icons/seats.svg') }}" alt="rating" width="17px"
                                        height="17px">
                                    <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                                    <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                        src="{{ asset('storage/icons/koper.svg') }}" alt="rating" width="17px"
                                        height="17px">
                                    <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                                </div>
                                <p class="font-semibold text-[8px] lg:text-sm mt-1">Layanan 1 • Layanan 1 </p>
                            </div>
                        </div>
                        <div class="row-span-1 lg:col-start-7 lg:col-span-8 text-left lg:text-center px-2.5">
                            <div class="pb-2.5 lg:py-12">
                                <p class="font-bold text-xs lg:text-2xl text-[#23AEC1]">IDR 100,000</p>
                                <p class="font-medium text-[8px] lg:text-base">Belum termasuk biaya tol</p>
                                <button type="submit"
                                    class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-lg font-semibold text-center text-white bg-[#FFB800] rounded lg:rounded-lg">Pesan</button>
                            </div>
                        </div>
                    </a> --}}
                </div>

            </div>
        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        var turAboutSwiper = new Swiper(".tur-about-swiper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
                enabled: true,
            },
            breakpoints: {
                640: {
                    slidesPerView: 3,
                    navigation: {
                        enabled: false
                    },
                }
            }
        });

        var reviewSwipper = new Swiper(".review-swipper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });

        $("#transferfrombyprice").select2()
        $("#transfertobyprice").select2()

    </script>
</body>

</html>
