<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tur Detail') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    
    {{-- sweet alert --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.min.css" rel="stylesheet">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles

    <style>
        .tab button.active {
            background-color: #FFF6FA;
            color: #9E3D64;
            border-bottom: 1px solid #9E3D64;
        }

        input[type='number']::-webkit-outer-spin-button,
        input[type='number']::-webkit-inner-spin-button,
        input[type='number'] {
            -webkit-appearance: none;
            margin: 0;
            -moz-appearance: textfield !important;
        }

        #social-links ul {
            padding-left: 0;
        }

        #social-links ul li {
            display: inline-block;
        }

        #social-links ul li a {
            padding: 6px;
            /* border: 1px solid #ccc; */
            border-radius: 5px;
            margin: 1px;
            font-size: 25px;
        }

        #social-links .fa-facebook {
            color: #0d6efd;
        }

        #social-links .fa-twitter {
            color: deepskyblue;
        }

        #social-links .fa-linkedin {
            color: #0e76a8;
        }

        #social-links .fa-whatsapp {
            color: #25D366
        }

        #social-links .fa-reddit {
            color: #FF4500;
            ;
        }

        #social-links .fa-telegram {
            color: #0088cc;
        }

        @media screen and (max-width: 350px) {
            .gj-unselectable {
                left: 0 !important;
            }
        }

        .rate {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rate:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rate:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rate:not(:checked)>label:before {
            content: '★ ';
        }

        .rate>input:checked~label {
            color: #ffc700;
        }

        .rate:not(:checked)>label:hover,
        .rate:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rate>input:checked+label:hover,
        .rate>input:checked+label:hover~label,
        .rate>input:checked~label:hover,
        .rate>input:checked~label:hover~label,
        .rate>label:hover~input:checked~label {
            color: #c59b08;
        }

        .star-rating-complete {
            color: #c59b08;
        }

        .rating-container .form-control:hover,
        .rating-container .form-control:focus {
            background: #fff;
            border: 1px solid #ced4da;
        }

        .rating-container textarea:focus,
        .rating-container input:focus {
            color: #000;
        }

        .rated {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rated:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ffc700;
        }

        .rated:not(:checked)>label:before {
            content: '★ ';
        }

        .rated>input:checked~label {
            color: #ffc700;
        }

        .rated:not(:checked)>label:hover,
        .rated:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rated>input:checked+label:hover,
        .rated>input:checked+label:hover~label,
        .rated>input:checked~label:hover,
        .rated>input:checked~label:hover~label,
        .rated>label:hover~input:checked~label {
            color: #c59b08;
        }

        .maps-embed {
            padding-bottom: 60%
        }

        .maps-embed iframe {
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
            position: absolute;
        }
        .disabled {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
        }
    </style>
</head>

<body class="bg-white font-Inter">
    <header class="border border-b-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Gallery Lightbox --}}
    <div class="container mt-5 mb-2.5 lg:my-5 mx-auto">
        <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff" class="swiper mySwiper2 ">
            <div class="swiper-wrapper">
                @foreach (json_decode($transfer_detail['gallery']) as $gallery)
                <div class="swiper-slide">
                    <img src="{{ asset($gallery->gallery) }}"
                        class="object-cover object-center w-full h-[316px] sm:h-[319px] md:h-[383px] lg:h-[511px] xl:h-[639px] 2xl:h-[767px]" />
                </div>
                @endforeach
            </div>
            <div class="hidden swiper-button-next"></div>
            <div class="hidden swiper-button-prev"></div>
        </div>

        @if (count(json_decode($transfer_detail['gallery'])) > 1)
        <div class="relative md:grid md:grid-cols-2 grid-flow-col gap-2">
            <div thumbsSlider="" class="pt-1 swiper mySwiper">
                <div class="justify-center space-x-1 swiper-wrapper sm:space-x-2">
                    @foreach (json_decode($transfer_detail['gallery']) as $gallery)
                    <div class="swiper-slide cursor-pointer h-max-[97px]">
                        <img src="{{ asset($gallery->gallery) }}"
                            class="object-cover object-center h-[75px] sm:h-[97px] md:h-[120px] lg:h-[167px] xl:h-[214px] 2xl:h-[261px] w-full" />
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="flex md:absolute md:bottom-0 md:right-0 gap-3">
                {{-- <div x-data="{ open: false }" @keydown.escape="open = false">
                    <button @click="open = true"
                        class="w-full md:w-fit md:-translate-x-5 md:-translate-y-5 px-1 md:px-3 py-1 lg:px-5 lg:py-2 text-sm sm:text-base font-semibold border-2 border-gray-300 text-black duration-300 bg-gray-300 rounded-sm hover:bg-gray-100">
                        Semua foto
                    </button>
                    <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                        style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                        <div
                            class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                            <button
                                class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                            </button>
                        </div>
                        <div class="h-full w-full flex items-center justify-center overflow-hidden"
                            x-data="{ active: 0, slides: {{ $transfer_detail['foto_maps_1'] }} }">
                            <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                    <button type="button"
                                        class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === 0 ? slides.length - 1 : active - 1">
                                        <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>

                            <template x-for="(slide, index) in slides" :key="index"
                                x-data="{ url: window.location.host, protocol: window.location.protocol }">
                                <div class="h-full w-full flex items-center justify-center absolute">
                                    <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                        x-show="active === index" x-transition:enter="transition ease-out duration-150"
                                        x-transition:enter-start="opacity-0 transform scale-90"
                                        x-transition:enter-end="opacity-100 transform scale-100"
                                        x-transition:leave="transition ease-in duration-150"
                                        x-transition:leave-start="opacity-100 transform scale-100"
                                        x-transition:leave-end="opacity-0 transform scale-90">

                                        <img :src="`${protocol}//${url}/${slide}`"
                                            class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                    </div>
                                    <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                        x-show="active === index">
                                        <span class="w-12 text-right" x-text="index + 1"></span>
                                        <span class="w-4 text-center">/</span>
                                        <span class="w-12 text-left" x-text="slides.length"></span>
                                    </div>
                                </div>
                            </template>
                            <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-start w-12 md:ml-16">
                                    <button type="button"
                                        class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === slides.length - 1 ? 0 : active + 1">
                                        <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div x-data="{ open: false }" @keydown.escape="open = false">
                    <button @click="open = true"
                        class="relative w-full md:w-fit md:-translate-x-5 md:-translate-y-5 px-1 md:px-3 py-1 lg:px-5 lg:py-2 text-sm sm:text-base font-semibold border-2 border-gray-300 text-black duration-300 bg-gray-300 rounded-sm hover:bg-gray-100">
                        <span class="md:pr-[11px]">
                           {{$like_count}} Favorit 
                        </span>
                        <span>
                            <svg class="absolute md:top-[9px] md:right-0 w-6 h-6 text-white hover:fill-red-500 md:pr-[7px]" id="16" onclick="submitLike2(16)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z"></path>
                            </svg>
                        </span>
                    </button>
                </div>
                {{-- <div x-data="{ open: false }" @keydown.escape="open = false">
                    <button @click="open = true"
                        class="w-full md:w-fit ml-3 md:-translate-x-5 md:-translate-y-5 px-1 md:px-3 py-1 lg:px-5 lg:py-2 text-sm sm:text-base font-semibold border-2 border-gray-300 text-black duration-300 bg-gray-300 rounded-sm hover:bg-gray-100">
                        Lihat Video
                    </button>
                    <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                        x-show.transition="open" style="background-color: rgba(0,0,0,.7)">
                        <div
                            class="flex items-center justifty-start w-12 m-2 ml-6 mb-4 md:m-2 z-100 absolute right-1 top-1 transform">
                            <button
                                class="text-white item-center flex w-12 h-12 rounded-full justify-center focus:outline-none"
                                style="background-color: rgba(230, 230, 230, 0.4)" @click="open = false">
                                <img src="{{ asset('storage/icons/close-button.svg') }}"
                                    class="w-6 h-6 relative top-[10px]">
                            </button>
                        </div>
                        <div class="h-full w-full flex items-center justify-center overflow-hidden">
                                @php
                                $video_id = explode("https://www.youtube.com/watch?v=",$xstay_detail->video_embed);
                                @endphp

                                <iframe class="max-w-[1800px] max-h-[700]l"
                                    src="https://www.youtube.com/embed/{{$video_id[1]}}" frameborder="0"
                                    allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
        
        @endif
    </div>

    <div>
        {{-- Tabs --}}
        <div class="container mx-auto my-2.5 lg:my-5 sticky bg-white top-0 z-20" x-data="{
            active: 0,
            tabs: ['Ringkasan', 'Kebijakan', 'Ulasan']
        }">
            <div class="flex sm:block px-2.5">
                <ul
                    class="flex justify-start overflow-x-auto text-sm font-medium text-center sm:text-left text-gray-500 dark:text-gray-400 border-b border-[#BDBDBD]">
                    <template x-for="(tab, index) in tabs" :key="index">
                        <li class="pb-2 mt-2" :class="{ 'border-b-2 border-[#9E3D64]': active == index }"
                            x-on:click="active = index">
                            <a :href="`#` + tab.toLowerCase()">
                                <button
                                    class="inline-block px-6 py-2 text-sm duration-200 rounded-lg lg:px-9 lg:text-xl"
                                    :class="{
                                        'text-[#9E3D64] bg-white font-bold': active ==
                                            index,
                                        'text-black font-normal hover:text-[#9E3D64]': active != index
                                    }" x-text="tab"></button>
                            </a>
                        </li>
                    </template>
                </ul>
            </div>
        </div>

        {{-- Content --}}
        <div class="container mx-auto">
            <div class="grid grid-cols-6 lg:gap-5">
                {{-- Left --}}
                <div class="col-span-6 lg:col-span-4 bg-white p-2.5 lg:p-5">
                    {{-- Ringkasan --}}
                    <div id="ringkasan">
                        <p class="text-3xl pb-2 font-semibold text-[#333333] whitespace-normal break-words">
                            {{ $transfer['product_name'] }}
                        </p>
                        <div class="flex items-center pb-2">
                            <img class="mr-2" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <p class="text-xl font-semibold text-[#333333]">{{ $ratings }} <span class="font-normal">({{
                                    $var2 }} Ulasan)</span></p>
                        </div>
                        <div
                            class="grid grid-cols-2 lg:grid-cols-2 w-80 lg:w-96 py-2.5 lg:pt-5 space-y-2 sm:space-y-0 lg:gap-10">
                            <div class="mt-2 space-y-2 lg:mt-0">
                                <p class="flex font-medium font-sm">
                                    {{-- <img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/car-solid 1.svg') }}" alt="user"> --}}
                                    {{-- Merek Mobil --}}
                                    {{-- {{ $transfer }} --}}
                                </p>
                                <p class="flex font-medium font-sm">
                                    {{-- <img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/car-solid 1.svg') }}" alt="user-group"> --}}
                                    {{-- Jenis Mobil --}}
                                    {{-- {{ dd($mobil_detail) }} --}}
                                    {{ $mobil_detail->jenismobil->jenis_kendaraan ?? '' }}
                                </p>
                            </div>
                            <div class="space-y-2">
                                <p class="flex font-medium font-sm"><img class="mr-2" width="20px"
                                        src="{{ asset('storage/icons/seats.svg') }}" alt="user">
                                    {{ $mobil_detail['kapasitas_kursi'] }} Seats
                                </p>
                                <p class="flex font-medium font-sm"><img class="mr-2" width="20px"
                                        src="{{ asset('storage/icons/koper.svg') }}" alt="user-group">
                                    {{ $mobil_detail['kapasitas_koper'] }} Koper
                                </p>
                            </div>
                        </div>
                        <div class="text-justify my-2.5 lg:my-10">
                            <p class="text-xl lg:text-3xl mb-2.5 font-semibold">Deskripsi</p>
                            {!! $transfer_detail['deskripsi'] !!}
                        </div>
                        @if($detail_rute)
                        <div class="text-justyfy my.5-2 lg:my-10">
                            <p class="text-xl lg:text-3xl mb-2.5 font-semibold">Jadwal</p>
                            <div class="py-3 px-2 border bg-[#F9F9F9] border-y-[#828282]" x-data="dropPick()">
                                <div class="my-3">
                                    <p class="mb-3">Pilih Bus</p>
                                    <div>
                                        <template x-for="(rute, index) in drop_pick">
                                            <button @click="busChoice(index)" type="button" :id="'btn-'+index"
                                            :class="$store.bus.current_index == index ? 'text-white bg-kamtuu-second':'text-[#333] bg-white border border-gray-300'"
                                            class="w-full px-3 py-2 m-2 ml-0 text-left duration-200 rounded-md sm:w-1/3">
                                                <template x-if="$store.rute.current_index==index">
                                                    <input type="hidden" name="rute[index]" :value="index">
                                                    <input type="hidden" name="rute[judul_bus]" :value="rute.judul_bus">
                                                    <input type="hidden" name="rute[harga]" :value="rute.harga">
                                                </template>
                                                <div class="flex space-x-2">
                                                    <div class="text-xs text-bold"><span x-text="rute.judul_bus"></span></div>
                                                    <div class="text-xs">
                                                        <label for="text" class="flex mb-2 text-sm font-bold text-[#333333]"
                                                        x-data="{ tooltip: false }" x-on:mouseover="tooltip = true"
                                                        x-on:mouseleave="tooltip = false">
                                                        <img class="mx-2 relative left-[100px]" width="15px"
                                                            src="{{ asset('storage/icons/circle-info-solid (1) 1.svg') }}" alt="">
                                                        <div x-show="tooltip"
                                                            class="text-sm text-white absolute bg-[#9E3D64] rounded-lg p-2 transform -translate-y-8 translate-x-8">
                                                            Pilih Bus
                                                        </div>
                                                    </div>
                                                </label>
                                                </div>
                                                <div class="flex space-x-2">
                                                    {{-- <div class="text-xs"><span x-text="detail_bus.merekmobil.merek_mobil"></span></div> --}}
                                                    <div class="text-xs"><span>-</span></div>
                                                    <div class="text-xs"><span x-text="nama_bus"></span></div>
                                                </div>
                                                <div class="flex space-x-2">
                                                    <div class="text-xs"><span x-text="detail_bus.jenisbus.jenis_kendaraan"></span></div>
                                                </div>
                                                <div class="flex space-x-2">
                                                    <div class="text-xs"><span>Harga Mulai</span></div>
                                                    <div class="text-xs"><span x-text="rute.harga_bus"></span></div>
                                                </div>
                                            </button>
                                        </template>
                                    </div>
                                    <p class="mb-3">Posisi Naik dan Turun</p>
                                    <div>
                                        <template x-for="(rute, index) in drop_pick">
                                            {{-- <input
                                                    class="hidden w-5 h-5 transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded appearance-none cursor-pointer peer checked:bg-blue-600 focus:outline-none checked:border-blue-600"
                                                    type="checkbox" name="rute[drop_pick]" :id="'drop_pick['+index+']'"
                                                    value=""> --}}
                                            <label type="checkbox" @click="ruteChoice(index)"  :data-index="index" :data-naik="rute.tempat_pickup" :data-turun="rute.tempat_drop"
                                            {{-- :class="$store.rute.current_index == index ? 'text-white bg-kamtuu-second':'text-[#333] bg-white border border-gray-300'" --}}
                                            class="w-full h-3 px-3 py-2 m-2 ml-0 text-left text-black duration-200 bg-white border border-gray-300 rounded-md cursor-pointer sm:w-1/3 sm:h-1/3 peer-click:bg-kamtuu-second peer-click:text-white" :id="'checked_rute['+index+']'">
                                                <template x-if="$store.rute.current_index==index">
                                                    <input type="hidden" name="rute[latitude_pickup]" :value="rute.latitude_pickup">
                                                    <input type="hidden" name="rute[longitude_pickup]" :value="rute.longitude_pickup">
                                                    <input type="hidden" name="rute[latitude_drop]" :value="rute.latitude_drop">
                                                    <input type="hidden" name="rute[longitude_drop]" :value="rute.longitude_drop">
                                                </template>
                                                <div class="flex space-x-2">
                                                    <div class="text-xs text-bold"><span x-text="rute.tempat_pickup"></span></div>
                                                    <div class="text-xs text-bold"><span>-</span></div>
                                                    <div class="text-xs text-bold"><span x-text="rute.tempat_drop"></span></div>
                                                    {{-- <div class="text-xs">
                                                        <label for="text" class="flex mb-2 text-sm font-bold text-[#333333]"
                                                        x-data="{ tooltip: false }" x-on:mouseover="tooltip = true"
                                                        x-on:mouseleave="tooltip = false">
                                                        <img class="mx-2 relative left-[100px]" width="15px"
                                                            src="{{ asset('storage/icons/circle-info-solid (1) 1.svg') }}" alt="">
                                                        <div x-show="tooltip"
                                                            class="text-sm text-white absolute bg-[#9E3D64] rounded-lg p-2 transform -translate-y-8 translate-x-8">
                                                            Pilih Bus
                                                        </div>
                                                    </div> --}}
                                                </label>
                                                {{-- </div>
                                                <div class="flex space-x-2">
                                                    <div class="text-xs"><span x-text="detail_bus.merekmobil.merek_mobil"></span></div>
                                                    <div class="text-xs"><span>-</span></div>
                                                    <div class="text-xs"><span x-text="nama_bus"></span></div>

                                                </div>
                                                <div class="flex space-x-2">
                                                    <div class="text-xs"><span x-text="detail_bus.jenisbus.jenis_kendaraan"></span></div>
                                                </div>
                                                <div class="flex space-x-2">
                                                    <div class="text-xs"><span>Harga Mulai</span></div>
                                                    <div class="text-xs"><span x-text="rute.harga_bus"></span></div>
                                                </div> --}}
                                            </button>
                                        </template>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>

                    <div id="kebijakan">
                        <div class="mb-4 border-b border-gray-200 dark:border-gray-700">
                            <ul class="flex flex-wrap gap-1 -mb-px text-xs font-medium text-center lg:text-base tab"
                                id="myTab" data-tabs-toggle="#myTabContent" role="tablist">
                                <li class="mr-2" role="presentation">
                                    {{-- 
                                    :class="{
                                        'text-[#9E3D64] bg-white font-bold': active ==
                                            index,
                                        'text-black font-normal hover:text-[#9E3D64]': active != index
                                    }" x-text="tab"    
                                    --}}
                                    <button
                                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent text-gray-500 border-gray-100 tablinks active h-[66px] lg:h-auto"
                                        id="komplemen-tab" data-tabs-target="#komplemen" type="button" role="tab"
                                        aria-controls="komplemen" aria-selected="true"
                                        onclick="openCity(event, 'komplemen')">Komplemen</button>
                                </li>
                                <li class="w-20 mr-2 lg:w-auto" role="presentation">
                                    <button
                                        class="inline-block p-4 text-gray-500 border-b-2 border-transparent border-gray-100 rounded-t-lg tablinks"
                                        id="pilihan-tab" data-tabs-target="#pilihan" type="button" role="tab"
                                        aria-controls="pilihan" aria-selected="false"
                                        onclick="openCity(event, 'pilihan')">Pilihan Tambahan</button>
                                </li>
                                <li class="w-20 mr-2 lg:w-auto" role="presentation">
                                    <button
                                        class="inline-block p-4 text-gray-500 border-b-2 border-transparent border-gray-100 rounded-t-lg tablinks"
                                        id="kebijakan-tab" data-tabs-target="#kebijakan" type="button" role="tab"
                                        aria-controls="kebijakan" aria-selected="false"
                                        onclick="openCity(event, 'kebijakans')">Kebijakan Tambahan</button>
                                </li>
                                <li class="w-[64px] lg:w-auto" role="presentation">
                                    <button
                                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent text-gray-500 border-gray-100 tablinks h-[66px] lg:h-auto"
                                        id="faq-tab" data-tabs-target="#faq" type="button" role="tab"
                                        aria-controls="faq" aria-selected="false"
                                        onclick="openCity(event, 'faq')">FAQ</button>
                                </li>
                            </ul>
                        </div>
                        <div id="myTabContent">
                            <div class="rounded-lg tabcontent" id="komplemen" role="tabpanel"
                                aria-labelledby="komplemen-tab">
                                @if (isset($transfer_detail['komplemen']))
                                {!! $transfer_detail['komplemen'] !!}
                                @else
                                {{-- text-xl font-semibold text-[#333333] --}}
                                <p class="text-base sm:text-lg md:text-xl">Tidak ada Komplemen.</p>
                                @endif
                            </div>
                            <div class="hidden rounded-lg tabcontent" id="pilihan" role="tabpanel"
                                aria-labelledby="pilihan-tab">
                                <div class="px-3 space-y-2 text-sm font-medium text-gray-500 lg:py-2 lg:text-lg md:text-base"
                                    x-show="open" x-transition>
                                    @if (isset($transfer_detail['pilihan_tambahan']))
                                    {!! $transfer_detail['pilihan_tambahan'] !!}
                                    @else
                                    <p class="text-base sm:text-lg md:text-xl">Tidak ada Pilihan Tambahan.</p>
                                    @endif
                                </div>
                            </div>
                            <div class="hidden rounded-lg tabcontent" id="kebijakans" role="tabpanel"
                                aria-labelledby="kebijakan-tab">
                                <div class="px-3 space-y-2 text-sm font-medium text-gray-500 lg:py-2 lg:text-lg md:text-base"
                                    x-show="open" x-transition>
                                    @if (isset($transfer_detail['kebijakan_pembatalan_sebelumnya']))
                                    @foreach ($kebijakan_sebelumnya as $k => $kp)
                                    <p class="py-3">Pembatalan <strong>{{ $kp }} hari</strong>
                                        sampai
                                        <strong>{{ json_decode($transfer_detail['kebijakan_pembatalan_sesudah'])[$k] }}
                                            hari</strong>
                                        sebelumnya potongan
                                        <strong>{{ json_decode($transfer_detail['kebijakan_pembatalan_potongan'])[$k]
                                            }}%.</strong>
                                    </p>
                                    @endforeach
                                    @else
                                    <p class="text-base sm:text-lg md:text-xl">Tidak ada kebijakan pembatalan.</p>
                                    @endif
                                </div>
                            </div>
                            <div class="hidden rounded-lg tabcontent" id="faq" role="tabpanel"
                                aria-labelledby="faq-tab">
                                <div class="px-3 space-y-2 text-sm font-medium text-gray-500 lg:py-2 lg:text-lg md:text-base"
                                    x-show="open" x-transition>
                                    @if (isset($transfer_detail['faq']))
                                    {!! $transfer_detail['faq'] !!}
                                    @else
                                    <p class="text-base sm:text-lg md:text-xl">Tidak ada FAQ.</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Ulasan --}}
                    <div id="ulasan" class="my-5">
                        <div class="container mx-auto">
                            @if(isset($reviews->star_rating))
                            <div class="container">
                                <div class="row">
                                    <div class="mt-4 col">
                                        <p class="font-weight-bold ">Review</p>
                                        <div class="form-group row">
                                            <input type="hidden" name="detailObjek_id" value="{{ $reviews->id }}">
                                            <div class="col">
                                                <div class="rated">
                                                    @for($i=1; $i<=$reviews->star_rating; $i++)
                                                        <input type="radio" id="star{{$i}}" class="rate" name="rating"
                                                            value="5" />
                                                        <label class="star-rating-complete" title="text">{{$i}}
                                                            stars</label>
                                                        @endfor
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mt-4 form-group row">
                                            <div class="col">
                                                <p>{{ $reviews->comments }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="container">
                                <div class="row">
                                </div>
                            </div>
                            <div>
                                <div class="container max-w-6xl p-4 ">

                                    <div class="flex mb-4">
                                        <div class="col mt-4 w-full">
                                            <form class="py-2 px-4 w-full" action="{{route('reviewProduct.store')}}"
                                                style="box-shadow: 0 0 10px 0 #ddd;" method="POST" autocomplete="off" enctype="multipart/form-data" id="form-transfer-private">
                                                @csrf
                                                <p class="font-weight-bold ">Ulasan</p>
                                                <div class="form-group row">
                                                    <input type="hidden" name="product_id" value="{{ $transfer->id }}">
                                                    <div class="col">
                                                        <div class="rate">
                                                            <input type="radio" id="star5" class="rate" name="rating"
                                                                value="5" />
                                                            <label for="star5" title="text">5 stars</label>
                                                            <input type="radio" checked id="star4" class="rate"
                                                                name="rating" value="4" />
                                                            <label for="star4" title="text">4 stars</label>
                                                            <input type="radio" id="star3" class="rate" name="rating"
                                                                value="3" />
                                                            <label for="star3" title="text">3 stars</label>
                                                            <input type="radio" id="star2" class="rate" name="rating"
                                                                value="2">
                                                            <label for="star2" title="text">2 stars</label>
                                                            <input type="radio" id="star1" class="rate" name="rating"
                                                                value="1" />
                                                            <label for="star1" title="text">1 star</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row mt-4" x-data="imageUpload">
                                                    <div class="col">
                                                    {{-- 01040900471 --}}
                                                        <textarea
                                                            class=" form-control w-full flex-auto block p-4 font-medium border border-transparent rounded-lg outline-none focus:border-[#9E3D64] focus:text-green-500"
                                                            name="comment" rows="6 " placeholder="Berikan Ulasan Anda"
                                                            maxlength="200" id="comments">
                                                        </textarea>
                                                        {{-- <input class="pt-4" type="file" accept="image/*" @change="selectedFile" name="images[]" --}}
                                                        <input type="file" name="images[]" class="form-control w-full flex-auto block border border-transparent outline-none focus:border-[#9E3D64] focus:text-green-500" accept="image/*" @change="selectedFile" multiple>
                                                        <template x-if="imgDetail.length >= 1">
                                                            <div class="flex justify-start items-center mt-2">
                                                                <template x-for="(detail,index) in imgDetail" :key="index">
                                                                    <div class="flex justify-center items-center">
                                                                        <img :src="detail"
                                                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                                            :alt="'upload'+index">
                                                                        <button type="button"
                                                                            class="absolute mx-2 translate-x-12 -translate-y-14">
                                                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                                                alt="" width="25px" @click="removeImage(index)">
                                                                        </button>
                                                                    </div>
                                                                </template>
                                                            </div>
                                                        </template>
                                                    </div>
                                                </div>
                                                <div class="mt-3 text-right">
                                                    <button
                                                        class="py-2 px-3 rounded-lg bg-kamtuu-second text-white">Submit
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-2 p-5">
                                        <div class="flex">
                                            <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                class="lg:w-[48px] lg:h-[48px] mt-[0.2rem] mr-1 inline-flex"
                                                alt="rating" title="Rating">
                                            <p class="flex"><span
                                                    class="lg:text-[50px] font-semibold ">{{$ratings}}</span> <span
                                                    class="lg:text-[20px] lg:pt-9"> /5.0 dari {{$var2}} ulasan</span>
                                            </p>
                                        </div>
                                    </div>

                                    {{-- ulasan desktop --}}

                                    @foreach($reviews as $review)
                                    <div class="hidden lg:block md:block">
                                        
                                        {{-- <x-produk.card-reviews :review="$review"></x-produk.card-reviews> --}}
                                        <div class="grid grid-row-5 rounded-lg shadow-lg justify-items-start">
                                            {{-- <div class="grid grid-row-5 rounded-lg shadow-lg justify-items-start py-5"> --}}
                                                <div class="grid grid:row-2 grid-cols-10 gap-2 pl-3 mt-7">
                                                    @if(isset($review->profile_photo_path))
                                                    <img class="w-20 h-20 rounded-full bg-gray-400" scr="{{isset($review->profile_photo_path) ? asset($review->profile_photo_path):null}}">
                                                    <div class="col-span-2 my-auto">
                                                    @else
                                                    <div class="w-20 h-20 rounded-full bg-gray-400 relative">
                                                        @php
                                                            $name = $review->first_name;
                                                            $exp_name = explode(' ',$name);
                                                            $inisial ="";

                                                            $inisial = substr($exp_name[0],0,1);

                                                            if(count($exp_name) > 1){
                                                                $inisial .=substr(end($exp_name),0,1);
                                                            }
                                                        @endphp
                                                        @if(count($exp_name) > 1)
                                                        <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 13px;">{{$inisial}}</p>
                                                        @else
                                                        <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 26px;">{{$inisial}}</p>
                                                        @endif
                                                    </div>
                                                    <div class="col-span-2 my-auto mx-3">
                                                    @endif
                                                        <p class="font-semibold text-gray-900">{{$review->first_name}}<p/>
                                                        {{-- Carbon::parse($p->created_at)->diffForHumans(); --}}
                                                        <p class="font-light text-gray-600 text-xs">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}<p/>
                                                    </div>
                                                </div>
                                                <div class="grid grid-cols-5 pl-3 mb-2">
                                                    <div class="col-span-5">
                                                        <img class="w-[20px] h-[20] mt-[0.3rem] mr-1  ml-3 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                                        @for ($i = 0; $i < ($review->star_rating-1); $i++)
                                                            <img class="w-[20px] h-[20] mt-[0.3rem] mr-1 -ml-2 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                                        @endfor
                                                    </div>
                                                </div>
                                                <div class="grid grid-cols-5 space-y-5 text-justify pl-3 pr-5">
                                                    <div class="col-span-5 pl-3">
                                                    <p id="text-ulasan">{{$review->comments}}</p>
                                                    </div>
                                                </div>
                                                <div class="mt-3 pl-3">
                                                    <button type="button" id="show-btn-ulasan" class="px-3 py-2 text-black text-sm underline">Tampilkan</button>
                                                    <button type="button" id="hide-btn-ulasan" class="px-3 py-2 text-black text-sm underline hidden">Sembunyikan</button>
                                                </div>
                                                @if($review->gallery_post)
                                                <div class="flex justify-start justify-items-center pl-4 pb-4">
                                                    @php
                                                        $post_gallery = json_decode($review->gallery_post, true);
                                                        //dump($post_gallery);
                                                    @endphp
                                                    <img class="w-50 h-40 bg-slate-400 rounded-lg" src="{{$post_gallery['result'] != null ? Storage::url('gallery_post/'.$post_gallery['result'][0]):'https://via.placeholder.com/540x540'}}">
                                                    <div class="relative" x-data="{open:false}" @keydown.escape="open = false">
                                                        <button @click="open=true" type="button" id="hide-btn-foto-ulasan" 
                                                            class="absolute px-3 py-2 text-black text-sm underline" style="width:169px;top:110px">
                                                            Tampilkan semua foto
                                                        </button>
                                                        <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                                                                    style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                                                                    <div
                                                                        class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                                                                        <button
                                                                            class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                                                            style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                                                            <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                                                                        </button>
                                                                    </div>
                                                                    <div class="h-full w-full flex items-center justify-center overflow-hidden"
                                                                        x-data="{active: 0, slides: {{ ($review->gallery_post) }} }">
                                                                        <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                                                            <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                                                                <button type="button"
                                                                                    class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                                                    style="background-color: rgba(230, 230, 230, 0.4);"
                                                                                    @click="active = active === 0 ? slides.length - 1 : active - 1">
                                                                                    <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                        @if ($post_gallery)
                                                                            @foreach ($post_gallery['result'] as $index=>$gallery)
                                                                                <div class="h-full w-full flex items-center justify-center absolute">
                                                                                    <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                                                                        x-show="active === {{ $index }}"
                                                                                        x-transition:enter="transition ease-out duration-150"
                                                                                        x-transition:enter-start="opacity-0 transform scale-90"
                                                                                        x-transition:enter-end="opacity-100 transform scale-100"
                                                                                        x-transition:leave="transition ease-in duration-150"
                                                                                        x-transition:leave-start="opacity-100 transform scale-100"
                                                                                        x-transition:leave-end="opacity-0 transform scale-90">

                                                                                        <img src="{{ Storage::url('gallery_post/'.$gallery) }}"
                                                                                            class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                                                                    </div>
                                                                                    <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                                                                        x-show="active === {{ $index }}">
                                                                                        <span class="w-12 text-right" x-text="{{ $index }} + 1"></span>
                                                                                        <span class="w-4 text-center">/</span>
                                                                                        <span class="w-12 text-left"
                                                                                            x-text="{{ count($post_gallery['result']) }}"></span>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        @endif
                                                                        
                                                                        <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                                                            <div class="flex items-center justify-start w-12 md:ml-16">
                                                                                <button type="button"
                                                                                    class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                                                    style="background-color: rgba(230, 230, 230, 0.4);"
                                                                                    @click="active = active === slides.length - 1 ? 0 : active + 1">
                                                                                    <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                    </div>
                                                </div>
                                                @endif
                                            {{-- </div> --}}
                                            {{-- <div class="grid p-5 justify-items-center">
                                                <p class="text-[18px] font-bold">Jhonny Wilson</p>
                                                <p class="text-[18px] -mt-2">{{$review->created_at->format('j F, Y')}}
                                                </p>
                                                <div class="flex p-2">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex pt-3"><span
                                                            class="text-[16px] font-semibold ">{{$review->star_rating}}.0</span>
                                                        <span class="text-[16px]"> /5.0</span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div
                                                class="grid justify-start justify-items-center lg:-ml-[15rem] xl:-ml-[23rem] p-5 md:-ml-[10rem]">
                                                <p class="text-[18px]">{{ $review->comments }}</p>
                                            </div> --}}
                                        </div>
                                    </div>

                                    {{-- ulasan mobile --}}
                                    <div class="block lg:hidden md:hidden">
                                        <div
                                            class="grid grid-cols-1 border border-gray-300 divide-y rounded-lg shadow-xl justify-items-center">
                                            <div class="grid p-5 justify-items-center">
                                                <p class="text-sm md:text-[18px] font-bold my-3">{{$review->frist_name}}</p>
                                                <p class="text-sm md:text-[18px] -mt-2">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}</p>

                                                <div class="flex p-2">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex pt-3"><span
                                                            class="text-[16px] font-semibold ">{{$review->star_rating}}</span>
                                                        <span class="text-[16px]"> /5.0</span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="grid justify-start p-5 justify-items-center">
                                                <p class="text-sm md:text-[18px]"> {{ $review->comments }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                {{-- Right --}}
                <div class="col-span-6 lg:col-span-2 space-y-5 p-2.5 lg:p-5">
                    {{-- Detail --}}
                    <div class="p-5 mb-5 bg-white border border-gray-200 rounded-md shadow-lg lg:block">
                        <p class="text-base font-semibold mb-2.5">Jumat, 12 Agustus 2022 • 09.00</p>
                        <p class="text-base font-semibold mb-2.5">Dari</p>
                        @php
                        // dump($transfer_detail);


                        if($transfer_detail->tipe_tur=='Transfer Umum'){

                            $from = $rute[0]['tempat_pickup'];
                            $to   = $rute[0]['tempat_drop'];

                        }elseif($transfer_detail->tipe_tur=='Transfer Private'){
                            if(isset($rute->result)){
                                $result = json_decode($master_route->rute, true)['result'];

                                $from =  App\Models\District::where('id',$result['from']);
                                $to   =  App\Models\District::where('id',$result['to']);
                            }else{
                                $from ='Bandara Internasional YIA';
                                $to = 'Hotel Yogyakarta';
                            }
                        }else{

                            $from ='Bandara Internasional YIA';
                            $to = 'Hotel Yogyakarta';
                        }


                        @endphp
                        <div class="flex">
                            <img src="{{ asset('storage/icons/bus-solid 1.svg') }}" alt="bus" width="20px"
                                height="20px">
                            <p class="ml-2.5 text-base font-semibold">{{$from}}</p>
                        </div>
                        <img class="py-2" src="{{ asset('storage/icons/line-trans.svg') }}" alt="line">
                        <p class="text-base font-semibold mb-2.5">Ke</p>
                        <div class="flex">
                            <img src="{{ asset('storage/icons/location-dot-solid 1.svg') }}" alt="loc" width="15px"
                                height="15px">
                            <p class="ml-2.5 text-base font-semibold">{{$to}}</p>
                        </div>
                    </div>
                    {{-- Price --}}
                    @if (auth()->user() == null)
                    <div class="grid p-5 justify-items-center">
                        <a href="{{ route('LoginTraveller') }}"
                            class="px-8 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">
                            Login</a>
                    </div>
                    @else
                    <form action="{{ route('transfer.prosespesan') }}" method="POST">
                        @csrf
                        <div class="relative order-last block my-3 sm:order-none sm:my-0">
                            <label for="activity-date-picker" class="inline-block mb-2 text-gray-700 form-label">Tanggal
                                Transfer</label>
                            <div
                                class="placeholder:text-[#BDBDBD] block bg-white w-1/4 md:w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                <input class="w-full" placeholder="Pilih tanggal" type="text" id="check_in_date"
                                    name="check_in_date" required />
                            </div>
                        </div>
                        @if($transfer_detail->tipe_tur=='Transfer Umum')
                        <input type="hidden" name="total" value="{{$rute[0]['harga']}}" x-data="">
                        @else
                            <input type="hidden" name="total" value="{{ isset($rute->harga) ? $rute->harga:0 }}">
                        @endif
                            <input type="hidden" name="layout_bus_id" value="{{ $mobil_detail->layout_bus_id }}">
                            <input type="hidden" name="product_detail_id" value="{{ $transfer_detail['id'] }}">
                            <input type="hidden" name="pilihan_id" value="{{ $transfer_detail['pilihan_id'] }}">
                            <input type="hidden" value="{{ $transfer['product_name'] }}" name="product_name">
                            <input type="hidden" name="status" value="{{ $mobil_detail->status }}">
                            <input type="hidden" value="{{ $transfer['user_id'] }}" name="toko_id">
                            <input type="hidden" value="{{ $transfer['type'] }}" name="type">

                            <p class="my-3">Ekstra</p>
                        <div class="my-3 space-y-3">
                            {{-- <div>
                                @livewire('tur.modal-pilihan', ['pilihans' => $pilihan])
                            </div> --}}
                            <div>
                                @livewire('tur.modal-ekstra')
                            </div>
                        </div>
                        @if($detail_rute)
                        {{-- <p class="my-3">Pilih tempat duduk</p>
                        <div class="my-3 space-y-3">
                            <div>
                                @livewire('transfer.modal-seat',['layout_id'=>$mobil_detail->layout_bus_id,'transfer'=>$transfer,'transfer_detail'=>$transfer_detail,'nama_bus'=>$transfer->product_name,'galleries'=>isset($galleries) ? $galleries[0]->gallery:null])
                            </div>
                        </div> --}}
                        @endif
                        <div class="p-5 mb-5 bg-white border border-gray-200 rounded-md shadow-lg lg:block">
                            <p class="lg:text-2xl xl:text-3xl text-center font-bold text-[#23AEC1]">IDR
                            @if($transfer_detail->tipe_tur=='Transfer Umum')
                                {{ number_format($rute[0]['harga']) }}
                            @else
                                {{isset($rute->harga) ? number_format($rute->harga) : 0 }}
                            @endif
                            </p>
                            <div class="flex justify-center">
                                {{-- <x-destindonesia.button-primary text="Pesan Sekarang">
                                    @slot('button')
                                    Pesan Sekarang
                                    @endslot
                                </x-destindonesia.button-primary> --}}
                            @if($transfer_detail->tipe_tur=='Transfer Umum')
                            <div>
                                {{ dd() }}
                                @livewire('transfer.modal-seat',['layout_id'=>$mobil_detail->layout_bus_id,'transfer'=>$transfer,'transfer_detail'=>$transfer_detail,'nama_bus'=>$transfer->product_name,'galleries'=>isset($galleries) ? $galleries[0]->gallery:null])
                            </div>
                            @else
                            <button type="submit"
                                class="my-3 px-8 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">
                                {{-- {{$button }} --}}
                                Pesan Sekarang
                            </button>
                            @endif
                            </div>
                        </div>
                    </form>
                    @endif

                    {{-- Store --}}
                    <div class="p-5 my-5 bg-white border-gray-200 lg:rounded-md lg:shadow-lg border-y-2 lg:border">
                        <a href={{route('toko.show',$transfer->user->id)}}>
                            <p class="lg:text-2xl xl:text-3xl font-bold text-center text-[#333333]">Kunjungi Toko</p>
                        </a>
                        <div class="flex justify-center py-5">
                            <a href={{route('toko.show',$transfer->user->id)}}>
                                <img src="{{ isset($info_toko->logo_toko) ? asset($info_toko->logo_toko) : asset('storage/img/tur-detail-1.png') }}"
                                    class="bg-clip-content lg:w-[96px] lg:h-[96px] xl:w-[100px] xl:h-[100px] shadow-xl bg-white rounded-full"
                                    alt="store-profile">
                            </a>    
                        </div>
                        <p class="pt-3 text-xl font-semibold text-center">{{ isset($info_toko->nama_toko) ? $info_toko->nama_toko :'Nama Toko'}}</p>
                        <p class="pb-2 text-lg font-normal text-center">Bergabung sejak {{
                            isset($transfer->user->created_at)
                            ? date('Y',strtotime($transfer->user->created_at)) : '2021'}}</p>
                        <div class="flex justify-center">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                        </div>
                        <div class="flex justify-center">
                            <x-destindonesia.button-primary text="Kirim Pesan">
                                @slot('button')
                                Kirim Pesan
                                @endslot
                            </x-destindonesia.button-primary>
                        </div>
                    </div>

                    {{-- QR Code --}}
                    <div>
                        <p class="text-2xl font-semibold text-center">Kode QR Produk</p>
                        <div class="flex justify-center mt-5">
                            <img src="{{ asset('storage/img/qr-code-transfer.png') }}" alt="QR-code">
                        </div>
                        <a class="flex justify-center" href="/storage/img/tur-detail-2.png" download="QR-code-produk">
                            <x-destindonesia.button-primary text="Unduh Kode QR">
                                @slot('button')
                                Unduh Kode QR
                                @endslot
                            </x-destindonesia.button-primary>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>

    <script>
        $(document).ready(function() {
            function disabledDate(){
                let start_date;
                let end_date;
                let diskon = '{!! isset($diskon)  ? $diskon : null !!}'
                if(diskon){
                    start_date = JSON.parse("{{$tgl_start}}".replace(/&quot;/g,'"'))
                    end_date   = JSON.parse("{{$tgl_end}}".replace(/&quot;/g,'"'))
                }else{
                    start_date = null;
                    end_date = null
                }
                let batas_pembayaran  = "{{$transfer_detail->batas_pembayaran}}"
                
                const disabled_date = []
                let jml_hari = start_date.length
                for(let i = 0; i < jml_hari; i++){
                    let start = new Date(start_date[i])
                    let end   = new Date(end_date[i])
                    let date  = new Date(start)

                    
                    if(date <= end){
                        for(let x =0; x < batas_pembayaran; x++){
                            let z = x;
                            let tgl;
                            let bulan;

                            tgl =  date.getDate() + x;
                            if(tgl > 31){
                                // console.log(z-1)
                                tgl = z-1;
                                bulan  = date.getMonth() + 1;
                                
                            }else{
                                tgl = date.getDate() + x;
                                bulan  = date.getMonth();
                            }
                            
                            disabled_date.push(date.getFullYear()+'-'+('0'+(bulan+1)).slice(-2)+'-'+('0'+(tgl)).slice(-2))
                        }
                    }
                }
                
                return disabled_date;
               
            }

            // console.log(disabledDate())

            var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            $('#check_in_date').datepicker({
                disableDates: function(date) {
                    var disabledDates = disabledDate();
                    var formattedDate = $.datepicker.formatDate('yy-mm-dd', date);
                    
                    if (disabledDates.indexOf(formattedDate) != -1) {
                        return false;
                    } else {
                        return true;
                    }
                },
                minDate: today
            });
        })

        document.addEventListener("alpine:init", () => {
            Alpine.store('rute',{
                init(){
                    this.chooseRute()
                },
                rute_choice:{!! isset($rute) ? $rute : '[]' !!},
                current_index:0,
                tempat_pickup:"",
                longitude_pickup:"",
                latitude_pickup:"",
                tempat_drop:"",
                longitude_drop:"",
                latitude_drop:"",
                array_rute:[],
                chooseRute(){
                    // alert()
                    console.log(this.rute_choice[this.current_index])
                    if(this.rute_choice.length===undefined){
                        $("#from-modal-seat").html(this.rute_choice[this.current_index]!=undefined ? this.rute_choice[this.current_index].tempat_pickup:null)
                        $("#to-modal-seat").html(this.rute_choice[this.current_index]!=undefined ? this.rute_choice[this.current_index].tempat_drop:null)
                    }

                    if(this.rute_choice.length > 0){

                        if(this.current_index == 0){
                            this.tempat_pickup = this.rute_choice[this.current_index].tempat_pickup
                            this.longitude_pickup = this.rute_choice[this.current_index].longitude_pickup
                            this.latitude_pickup = this.rute_choice[this.current_index].latitude_pickup

                            this.tempat_drop = this.rute_choice[this.current_index].tempat_drop
                            this.longitude_drop = this.rute_choice[this.current_index].longitude_drop
                            this.latitude_drop = this.rute_choice[this.current_index].latitude_drop
                        }else{
                            this.tempat_pickup = this.rute_choice[this.current_index].tempat_pickup
                            this.longitude_pickup = this.rute_choice[this.current_index].longitude_pickup
                            this.latitude_pickup = this.rute_choice[this.current_index].latitude_pickup

                            this.tempat_drop = this.rute_choice[this.current_index].tempat_drop
                            this.longitude_drop = this.rute_choice[this.current_index].longitude_drop
                            this.latitude_drop = this.rute_choice[this.current_index].latitude_drop
                        }
                    }

                }
            })
        });

        document.addEventListener("alpine:init", () => {
            Alpine.store("bus", {
                init(){
                  this.chooseBus()
                },
                current_index:0,
                nama_bus:'',
                bus_choice:{!! isset($rute) ? $rute : '[]' !!},
                chooseBus(){
                    if(this.bus_choice.length!=undefined){
                        if(this.current_index == 0){

                            this.nama_bus = this.bus_choice[this.current_index].judul_bus
                        }else{
                            this.nama_bus = this.bus_choice[this.current_index].judul_bus
                        }
                    }
                }
            })
        })

        document.addEventListener("alpine:init",()=>{
            Alpine.store("bus_chair",{
                init(){

                },
                ordered:[],
                total_paid:0,
                sequence:''
            })
        })

        function dropPick(){
            // console.log({!! isset($mobil_detail) ? $mobil_detail: '[]' !!})
            return{
                drop_pick :{!! isset($rute) ? $rute : '[]' !!},
                detail_bus:{!! isset($mobil_detail) ? $mobil_detail: '[]' !!},
                index:'',
                rute_naik:'',
                rute_naik:'',
                routes_array:[],
                nama_bus:"{!! $transfer->product_name !!}",
                busChoice(index){
                    Alpine.store("bus").current_index = index;
                    Alpine.store("bus").chooseBus()
                    return Alpine.store("bus").current_index;
                },
                ruteChoice(index){
                    let id = "checked_rute["+index+"]";

                    let data_id = parseInt(document.getElementById(id).dataset.index) + 1
                    var rute_naik = document.getElementById(id).dataset.naik
                    var rute_turun = document.getElementById(id).dataset.turun

                    this.index_rute = data_id
                    this.rute_naik  = rute_naik
                    this.rute_turun = rute_turun

                    this.checkExsist(this.index_rute, id)
                    Alpine.store("rute").current_index = index;
                    return Alpine.store("rute").current_index;
                },
                checkExsist(id, id_html){
                    if(this.routes_array.length > 0){
                        this.routes_array.find((el)=>{
                            if(el.id==id){
                                this.reduceRoute(id)
                                document.getElementById(id_html).classList.remove("bg-kamtuu-second")
                                document.getElementById(id_html).classList.remove("text-white")

                                document.getElementById(id_html).classList.add("bg-white")
                                document.getElementById(id_html).classList.add("text-black")
                            }else{
                                this.addRoute()
                                document.getElementById(id_html).classList.remove("bg-white")
                                document.getElementById(id_html).classList.remove("text-black")

                                document.getElementById(id_html).classList.add("bg-kamtuu-second")
                                document.getElementById(id_html).classList.add("text-white")
                            }
                        })
                    }else{
                        this.addRoute()
                        document.getElementById(id_html).classList.remove("bg-white")
                        document.getElementById(id_html).classList.remove("text-black")

                        document.getElementById(id_html).classList.add("bg-kamtuu-second")
                        document.getElementById(id_html).classList.add("text-white")
                    }
                },
                addRoute(){
                    this.routes_array.push({
                        id:this.index_rute,
                        naik:this.rute_naik,
                        turun:this.rute_turun
                    });

                    Alpine.store("rute").array_rute = this.routes_array;
                    return this.routes_array,Alpine.store("rute").array_rute=this.routes_array

                },
                reduceRoute(element){
                    const reduce = this.routes_array.filter((data)=> data.id!==element)
                    this.routes_array = reduce;
                    Alpine.store("rute").array_rute = this.routes_array;
                    return reduce, Alpine.store("rute").array_rute ;
                }
            }
        }
    </script>

    <script>
        var swiper = new Swiper(".mySwiper", {
            spaceBetween: 2,
            slidesPerView: 5,
            freeMode: true,
            watchSlidesProgress: true,
        });
        var swiper2 = new Swiper(".mySwiper2", {
            spaceBetween: 2,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            thumbs: {
                swiper: swiper,
            },
        });
    </script>
    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
    <script>

                $(document).ready(function(){
            
            function submitValidate(e, msg){
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'error',
                        title: msg
                    })
                e.preventDefault()
            }

            $('#form-transfer-private').on("submit",function(e){
                let role="{{auth()->user() != null ? auth()->user()->role:null}}";
                if(!role){
                    submitValidate(e, 'Silahkan login terlebih dahulu')
                }

                if(role!='traveller'){
                    submitValidate(e, 'Silahkan login terlebih dahulu')
                }

                

                if(!$("#comments").val()){
                    submitValidate(e, 'Isi komentar tidak kosong')
                }
                //e.preventDefault();
            })
        })    

        function imageUpload(){
            return{
                imageUrl:'',
                imgDetail:[],
                selectedFile(event){
                    this.fileToUrl(event)
                },
                fileToUrl(event){
                    if (!event.target.files.length) return
                    let file = event.target.files
                    
                    for (let i = 0; i < file.length; i++) {

                        let reader = new FileReader();
                        let srcImg = ''
                        this.imgDetail = []

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.imgDetail = [...this.imgDetail, srcImg]
                        };
                    }

                    //this.fileUpload();
                },
                removeImage(index){
                    this.imgDetail.splice(index, 1)
                },
                fileUpload(){
                    let fd = new FormData()
                    
                    console.log(file)

                    for(const file of this.imgDetail){
                        console.log(file)
                        /*
                        if(typeof file === 'object'){
                            fd.append('gallery_post[]',file, file.name)
                        }
                        if(typeof file === 'string'){
                            fd.append('seved_post_gallery',file)
                        }
                        */
                    }

                    /*
                    $.ajaxSetup({
                        headers:{
                            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        method:'POST',
                        url:"{{route('gallery-post')}}",
                        data:fd,
                        processData:false,
                        contentType:false,
                        beforeSend:function(){

                        },
                        success:function(msg){
                            console.log(msg)
                        },
                        error:function(data){

                        }
                    })
                    */
                }
            }
        }    

        function togglerBtn(){
            console.log({!! isset(auth()->user()->id) ? auth()->user()->id: null !!})
            return{
                chair:[],
                no_kursi:'',
                deskripsi:'',
                total_bayar:0,
                getChair(event){

                    let id = event.target.id;
                    let split_idx = id.split("-")
                    let checkbox_id = 'kursi-'+split_idx[1];

                    let checked   = document.getElementById(checkbox_id).checked
                    let avaiblity = document.getElementById('avaiblity-'+split_idx[1]);
                    let user_id = document.getElementById('user_id-'+split_idx[1]);

                    let no_kursi= document.getElementById(id).dataset.no_kursi;
                    let harga= document.getElementById(id).dataset.harga;
                    let no_urut='';

                    let harga_total_kursi=0;
                    let z = 1;

                    if(checked === false){
                        checked = true;
                        // avaiblity.value= 'on'
                        user_id.value = '{!!  isset(auth()->user()->id) ? auth()->user()->id: null !!}'
                        this.chair.push({
                            no_kursi,
                            harga
                        });

                        for(let i=0; i < this.chair.length; i++){
                            no_urut += this.chair[i].no_kursi
                            if(z != this.chair.length){
                                no_urut +=", "
                            }

                            harga_total_kursi +=parseInt(this.chair[i].harga)
                            z++
                        }

                        this.deskripsi = no_urut
                        this.total_bayar = harga_total_kursi

                        Alpine.store('bus_chair').sequence = this.deskripsi
                        Alpine.store('bus_chair').total_paid = this.total_bayar
                        Alpine.store('bus_chair').ordered = this.chair

                    }else{
                        let no = document.getElementById(checkbox_id).value;
                        this.removeElement(no)
                        checked = false;
                        avaiblity.value= null
                    }

                },
                removeElement(element){

                    const filtered = this.chair.filter((data)=>data.no_kursi!==element);

                    this.chair=filtered;
                    this.calculate();
                    Alpine.store('bus_chair').ordered = this.chair
                    return filtered;
                },
                calculate(){

                    let no_urut='';

                    let harga_total_kursi=0;
                    let z = 1;

                    this.deskripsi = null;
                    this.total_bayar = 0;

                    for(let i=0; i < this.chair.length; i++){
                        no_urut += this.chair[i].no_kursi
                        if(z != this.chair.length){
                            no_urut +=", "
                        }

                        harga_total_kursi +=parseInt(this.chair[i].harga)
                        z++
                    }

                    this.deskripsi = no_urut;
                    this.total_bayar = harga_total_kursi;

                    Alpine.store('bus_chair').sequence = this.deskripsi
                    Alpine.store('bus_chair').total_paid = this.total_bayar
                    return this.deskripsi, this.total_bayar
                }
            }
        }
    </script>
</body>

</html>
