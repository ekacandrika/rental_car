<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tur Detail') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border border-b-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    <div class="container mx-auto my-5">
        <div class="grid grid-cols-6 lg:gap-5">
            {{-- Left --}}
            <div class="col-span-6 lg:col-span-4 p-3">

                <div class="block lg:hidden rounded-md border border-[#4F4F4F]">
                    <div class="mt-5 ml-3 flex">
                        <img src="{{ asset('storage/img/bajo-img.png') }}" alt="bajo-img" height="100px" width="100px">
                        <div class="text-[#333333] ml-1">
                            <p class="font-semibold text-lg">{{ $detail['product_name'] }}</p>
                            <p class="font-medium text-base">{{ $seller->first_name }}</p>
                            <p class="font-medium text-sm">ID Booking: {{$code}}</p>
                        </div>
                    </div>
                    <div class="rounded-md px-3 mb-3 bg-white">
                        <p class="text-sm font-semibold pb-1">Jumat, 12 Agustus 2022 • 09.00</p>
                        <p class="text-sm font-semibold">Dari</p>
                        <div class="flex">
                            <img src="{{ asset('storage/icons/bus-solid 1.svg') }}" alt="bus" width="20px"
                                height="20px">
                            <p class="ml-2 text-sm">Bandara Internasional YIA</p>
                        </div>
                        <img class="py-2" src="{{ asset('storage/icons/line-trans.svg') }}" alt="line">
                        <p class="text-sm font-semibold">Ke</p>
                        <div class="flex">
                            <img src="{{ asset('storage/icons/location-dot-solid 1.svg') }}" alt="loc"
                                width="15px" height="15px">
                            <p class="ml-2 text-sm">Hotel Yogyakarta</p>
                        </div>
                    </div>
                </div>

                {{-- Daftar Peserta --}}
                <div class="py-2.5 px-5 lg:py-5 lg:px-10 my-5 shadow-lg rounded-md border border-[#E0E0E0]">
                    <div class="block sm:flex sm:justify-between my-2">
                        <p class="text-lg lg:text-2xl font-semibold text-[#333333]">Daftar Peserta</p>
                        <p
                            class="text-xs lg:text-sm font-medium mt-2.5 grid justify-items-start lg:justify-items-end text-[#333333]">
                            ID Booking: {{ $code }}</p>
                    </div>
                    <div class="grid grid-cols-1 items-center">
                        <button
                            class="grid justify-items-start lg:justify-items-end text-[#D50006] font-medium text-sm lg:text-base">Hapus
                            Semua</button>
                    </div>
                    <div class="my-5 flex items-center">
                        <input value="user" type="checkbox" class="appearance-none checked:bg-blue-500 mr-2.5 ..." />
                        <label class="text-xs lg:text-sm text-[#333333]"> Sama seperti pemilik akun</label>
                    </div>

                    <div class="user hidden">
                        <div class="mt-5 flex">
                            <img src="{{ asset('storage/img/avatar.png') }}" alt="avatar-img" height="100px"
                                width="100px">
                            <div class="text-[#333333] ml-5">
                                <p class="font-semibold text-2xl">{{ $profile->first_name_booking }}
                                    {{ $profile->last_name_booking }}
                                </p>
                                <p class="font-medium text-base">{{ $profile->email_booking }}</p>
                                <p class="font-medium text-sm mt-6">{{ $profile->phone_number_booking }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="user">
                        {{-- Forms --}}
                        <div class="grid grid-rows-1 my-2">
                            <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                                <div class="relative block">
                                    <label
                                        class="form-label inline-block mb-2 text-[#333333] text-base font-medium">Nama
                                        Depan & Tengah</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="Nama depan" type="text" required>
                                </div>
                                <div class="relative block">
                                    <label
                                        class="form-label inline-block mb-2 text-[#333333] text-base font-medium">Nama
                                        Belakang</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="Nama belakang" type="text" required />
                                </div>
                            </div>
                        </div>

                        <div class="grid grid-rows-1 my-2">
                            <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                                <div class="relative block">
                                    <label class="form-label inline-block mb-2 text-[#333333] text-base">Tanggal
                                        Lahir</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        type="date" required />
                                </div>

                                <div class="relative block">
                                    <label class="form-label inline-block mb-2 text-[#333333] text-base">Jenis
                                        Kelamin</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select required
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white">
                                            <option selected value="male">Pria</option>
                                            <option value="female">Wanita</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sm:grid sm:grid-cols-2 gap-5">
                            <div>
                                <div class="sm:flex my-2 space-y-2 sm:space-y-0">
                                    <div class="flex items-center mr-5">
                                        <input class="rounded-sm form-checkbox mr-2" type="checkbox">
                                        <label class="text-[#333333]">Residen</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input class="rounded-sm form-checkbox mr-2" type="checkbox">
                                        <label class="text-[#333333]">Non Residen</label>
                                    </div>
                                </div>

                                <div class="relative block my-2">
                                    <label
                                        class="form-label inline-block mb-2 text-base text-[#333333]">Kebangsaan</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select required
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white">
                                            <option selected value="indonesia">Indonesia</option>
                                            <option value="japan">Jepang</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="relative block my-2">
                                    <label class="form-label inline-block mb-2 text-base text-[#333333]">No.
                                        HP/WhatsApp</label>
                                    <div class="flex">
                                        <div
                                            class="relative border border-[#828282] rounded-l-md bg-white shadow-sm sm:text-sm">
                                            <select
                                                class="appearance-none form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-l-md bg-white">
                                                <option selected value="+62">+62</option>
                                                <option value="+60">+60</option>
                                            </select>
                                        </div>
                                        <input
                                            class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-r-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                            placeholder="81234567890" type="text" required />
                                    </div>
                                </div>

                                <div class="relative block my-2">
                                    <label for=" email"
                                        class="form-label inline-block mb-2 text-base text-[#333333]">Email</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="emailku@gmail.com" type="email" required />
                                </div>

                                <div class="relative block my-2">
                                    <label class="form-label inline-block mb-2 text-base text-[#333333]">Status
                                        Residen</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select required
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white">
                                            <option value="residen">Residen</option>
                                            <option value="non-residen">Non Residen</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <label class="form-label inline-block mb-2 text-base text-[#333333]">KTP/KITAS</label>
                                <div x-data="displayImage()">
                                    <div class="mb-2">
                                        <template x-if="imageUrl">
                                            <img :src="imageUrl"
                                                class="object-contain rounded border border-gray-300 w-full h-56">
                                        </template>

                                        <template x-if="!imageUrl">
                                            <div
                                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-full h-56">
                                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                    width="20px" height="20px">
                                            </div>
                                        </template>

                                        <input class="mt-2" type="file" accept="image/*"
                                            @change="selectedFile">
                                    </div>
                                </div>
                                <p class="text-xs font-normal"><b>Catatan:</b> Beberapa seller mungkin menerapkan
                                    kebijakan harga berbeda antara residen dan non-residen</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Informasi Penerbangan --}}
                {{-- <div class="py-2.5 px-5 lg:py-5 lg:px-10 mb-5 shadow-lg rounded-md border border-[#E0E0E0]">
                    <div>
                        <div class="grid grid-cols-none lg:grid-cols-2 items-center">
                            <p class="text-base lg:text-2xl font-semibold grid justify-items-start text-[#333333] mb-1 lg:mb-none">Informasi Penerbangan</p>
                        </div>
                        <div class="grid grid-cols-none lg:grid-cols-2">
                            <div class="grid justify-items-start">
                                <label for="text" class="block mb-2 text-xs lg:text-sm font-medium text-[#333333] dark:text-gray-300">Cari berdasarkan nomor penerbangan</label>
                                <input type="text" id="text" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="GA874">
                            </div>   
                        </div> 
                        
                        <div class="grid grid-cols-none lg:grid-cols-2 items-center">
                            <p class="text-base lg:text-2xl font-semibold mt-2.5 lg:mt-5 grid justify-items-start text-[#333333]">Informasi SignBoard</p>
                        </div>
                        <div class="grid grid-cols-1">
                            <div class="grid justify-items-start">
                                <label for="text" class="block my-1 lg:mb-2 text-xs lg:text-sm lg:mt-2.5 font-medium text-[#333333] dark:text-gray-300">Tulisan yang ingin dimasukkan ke signboard saat penjemputan. Signboard ini akan dibawa oleh sopir saat menjemput anda. Ukuran signboard adalah seukuran kertas A4 (21m x 29.7cm). Apabila tidak diisi maka kami akan menggunakan nama peserta yang bertindak sebagai perwakilan diatas.</label>
                                <textarea id="message" rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-white rounded-lg border border-[#4F4F4F] focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Masukan pesan Anda..."></textarea>
                            </div>   
                        </div>
                    </div>
                </div> --}}

                {{-- Info Penerbangan --}}
                <form action="{{ route('booking.store.transfer') }}" method="POST">
                    @csrf
                <div class="py-2.5 px-5 lg:py-5 lg:px-10 mb-5 rounded-md border-y-2 border-[#E0E0E0] bg-[#F7F7F7]">
                    <div>
                        <div class="grid grid-rows-1 my-2">
                            <div class="grid sm:grid-cols-1 gap-2 sm:gap-5">
                                <div class="relative block">
                                    <label class="form-label inline-block mb-2 text-[#333333] text-base">Informasi Penerbangan</label>
                                    <input name="flight_info"
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm" required />
                                </div>
                                <div class="relative block">
                                    <input type="checkbox" class="appearance-none checked:bg-[#27BED3] w-3 h-3 ..." name="airport_pickup" value="ya"/>
                                    <span class="font-medium text-[#333333] ml-1 text-[10px] lg:text-base">Jemput di Bandara</span>
                                </div>
                                <div class="relative block">
                                    <label class="form-label inline-block mb-2 text-[#333333] text-base">Alamat Pickup</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <textarea name="pickup_address" id="pickup_adress"
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm" required></textarea>
                                    </div>
                                </div>
                                <div class="relative block">
                                    <label class="form-label inline-block mb-2 text-[#333333] text-base">Dropoff Traveller</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <textarea name="dropoff_traveller"
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm" required></textarea>
                                    </div>
                                </div>
                                {{-- <div class="relative block">
                                    <label class="form-label inline-block mb-2 text-[#333333] text-base">Drop Off</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <input name="drop_off"
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm" required />
                                    </div>
                                </div> --}}
                                <div class="relative block">
                                    <label class="form-label inline-block mb-2 text-[#333333] text-base">Waktu Pickup</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <input id="pickup_time" name="time_pickup"
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 px-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="grid grid-cols-2 items-center">
                            <p class="text-base lg:text-2xl font-semibold grid justify-items-start text-[#333333]">
                                Informasi Penerbangan</p>
                        </div>
                        <div class="flex lg:grid lg:grid-cols-2">
                            <div class="mt-0 lg:mt-5 w-[222px]">
                                <input type="checkbox" class="appearance-none checked:bg-[#27BED3] w-3 h-3 ..." />
                                <span class="font-medium text-[#333333] ml-1 text-[10px] lg:text-base">Jemputan di luar
                                    jam kerja (23.00 - 06.00)</span>
                            </div>
                            <p
                                class="mt-2 lg:mt-5 font-semibold text-[#27BED3] ml-4 lg:ml-10 text-[10px] lg:text-base">
                                Rp. 50,000</p>
                        </div>

                        <div class="flex lg:grid lg:grid-cols-2">
                            <div class="mt-0 lg:mt-5 w-[222px]">
                                <input type="checkbox" class="appearance-none checked:bg-[#27BED3] w-3 h-3 ..." />
                                <span class="font-medium text-[#333333] ml-1 text-[10px] lg:text-base">Sopir Berbahasa
                                    Inggris</span>
                            </div>
                            <p
                                class="mt-2 lg:mt-5 font-semibold text-[#27BED3] ml-4 lg:ml-10 text-[10px] lg:text-base">
                                Rp. 1,500,000</p>
                        </div> --}}
                    </div>
                </div>

                {{-- Pilihan --}}
                <div class="py-2.5 px-5 lg:py-5 lg:px-10 mb-5 rounded-md border-y-2 border-[#E0E0E0] bg-[#F7F7F7]">
                    <div>
                        @if(count($fields) > 0)
                        <div class="grid grid-cols-2 items-center">
                            <p class="text-base lg:text-2xl font-semibold grid justify-items-start text-[#333333]">
                                Pilihan
                            </p>
                        </div>
                            @foreach ($fields as $item)
                                <div class="flex lg:grid lg:grid-cols-2">
                                    <div class="mt-0 lg:mt-5 w-[222px]" x-data="choiceMandatory()">
                                        @if($item['kewajiban_pilihan']==='wajib')
                                        <input type="checkbox" class="appearance-none checked:bg-[#27BED3] w-3 h-3 ..." name="pilihan[]" value="{{$item['harga_pilihan']}}" checked/>
                                        @else
                                        <input type="checkbox" class="appearance-none checked:bg-[#27BED3] w-3 h-3 ..." name="pilihan[]" value="{{$item['harga_pilihan']}}"/>
                                        @endif
                                        <span class="font-medium text-[#333333] ml-1 text-[10px] lg:text-base">{{$item['nama_pilihan']}}</span>
                                    </div>
                                    <p class="mt-2 lg:mt-5 font-semibold text-[#27BED3] ml-4 lg:ml-10 text-[10px] lg:text-base">{{number_format($item['harga_pilihan'])}}</p>                                    
                                </div>
                            @endforeach
                        @endif
                        <div class="grid grid-cols-2 items-center">
                            <p class="text-base lg:text-2xl font-semibold grid justify-items-start text-[#333333]">
                                Pilihan tidak tersedia
                            </p>
                        </div>
                        {{-- <div class="flex lg:grid lg:grid-cols-2">
                            <div class="mt-0 lg:mt-5 w-[222px]">
                                <input type="checkbox" class="appearance-none checked:bg-[#27BED3] w-3 h-3 ..." />
                                <span class="font-medium text-[#333333] ml-1 text-[10px] lg:text-base">Sopir
                                    Berbahasa
                                    Inggris</span>
                            </div>
                            <p
                                class="mt-2 lg:mt-5 font-semibold text-[#27BED3] ml-4 lg:ml-10 text-[10px] lg:text-base">
                                Rp. 1,500,000</p>
                        </div> --}}
                    </div>
                </div>

                {{-- Extra --}}
                <div class="py-2.5 px-5 lg:py-5 lg:px-10 mb-5 rounded-md border-y-2 border-[#E0E0E0] bg-[#F7F7F7]">
                    <div>
                        <div class="grid grid-cols-1 items-center">
                            <p class="text-base lg:text-2xl font-semibold grid justify-items-start text-[#333333]">
                                Extra</p>
                        </div>
                        <div class="grid grid-cols-1">
                            <table class="w-full text-sm text-left">
                                <thead>
                                    <tr>
                                        <th>Nama Ekstra</th>
                                        <th>Jumlah Ekstra</th>
                                        <th>Total Harga Ekstra</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total = 0;
                                        $total_extra = 0;
                                    @endphp
                                    @if ($order['extra'] != null)
                                        @foreach ($order['extra'] as $key => $value)
                                            @php
                                                $total_harga = $total_extra + $value['jumlah_extra'] * $value['harga_extra'];
                                                $data_ekstra = App\Models\Extra::where('id', $value['id_extra'])->first();
                                                $total += $total_harga;
                                            @endphp
                                            <tr>
                                                <td>
                                                    <p>
                                                        {{ $data_ekstra->nama_ekstra }}
                                                    </p>
                                                </td>
                                                <td>
                                                    <p>
                                                        {{ $value['jumlah_extra'] }}
                                                    </p>
                                                </td>
                                                <td>
                                                    <p>
                                                        Rp. {{ number_format($total_harga) }}
                                                    </p>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <p class="mt-2.5 lg:mt-5 text-sm lg:text-base">Sebelum mengkonfirmasi pesanan Anda, harap untuk
                    memastikan kembali bahwa data yang anda masukkan sudah akurat.</p>
                    
                    <input type="hidden" name="customer_id" value="{{ $profile->id }}">
                    <input type="hidden" name="product_detail_id" value="{{ $detail['product_detail_id'] }}">
                    <input type="hidden" name="harga_ekstra" value="{{ isset($total) ? $total : null }}">
                    <input type="hidden" name="harga_pilihan" value="" id="harga_pilihan">
                    <input type="hidden" name="start_date" value="{{ $detail['start_date'] }}">
                    <input type="hidden" name="total_price" value="{{ $detail['total'] + $total }}" id="total_price">
                    <input type="hidden" name="booking_code" value="{{ $code }}">
                    <input type="hidden" name="toko_id" value="{{ $detail['toko_id'] }}">
                    <input type="hidden" name="type" value="{{ $detail['type'] }}">
                    @if (auth()->user()->role == 'agent')
                        <input type="hidden" name="agent_id" value="{{ $agent_id->id }}">
                        <input type="hidden" name="role" value="{{ $agent_id->role }}">
                    @endif
                    <button type="submit"
                        class="text-white bg-[#27BED3] w-full hover:bg-[#1C97A8] focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-0 lg:mb-5 lg:mb-10 mt-5 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Simpan
                        di Koper</button>
                </form>
            </div>

            {{-- Right --}}
            <div class="col-span-6 lg:col-span-2 space-y-5 pb-2.5 px-2.5 lg:p-3">
                {{-- Informasi --}}
                <div class="hidden lg:block rounded-md border border-[#4F4F4F] lg:mt-5">
                    <div class="mt-5 ml-3 flex">
                        <img src="{{ isset($thumbnail) ? asset($thumbnail) :asset('storage/img/bajo-img.png') }}" alt="bajo-img" height="100px"
                            width="100px">
                        <div class="text-[#333333] ml-1">
                            <p class="font-semibold text-lg">{{$detail['product_name']}}</p>
                            <p class="font-medium text-base">{{ $seller->first_name }}</p>
                            <p class="font-medium text-sm">ID Booking: {{$code}}</p>
                        </div>
                    </div>
                    <div class="rounded-md px-3 mb-3 bg-white">
                        @php
                            $date = DateTime::createFromFormat('Y-m-d', $detail['start_date']);
                        @endphp
                        <p class="text-sm font-semibold pb-1">{{ $date->format('l') }}, {{ $date->format('d F Y') }}
                            • 09.00</p>
                        <p class="text-sm font-semibold">Dari</p>
                        <div class="flex">
                            <img src="{{ asset('storage/icons/bus-solid 1.svg') }}" alt="bus" width="20px"
                                height="20px">
                            <p class="ml-2 text-sm">{{$from}}</p>
                        </div>
                        <img class="py-2" src="{{ asset('storage/icons/line-trans.svg') }}" alt="line">
                        <p class="text-sm font-semibold">Ke</p>
                        <div class="flex">
                            <img src="{{ asset('storage/icons/location-dot-solid 1.svg') }}" alt="loc"
                                width="15px" height="15px">
                            <p class="ml-2 text-sm">{{$to}}</p>
                        </div>
                    </div>
                </div>

                {{-- Biaya --}}
                <div class="border border-[#4F4F4F] rounded-md shadow-lg p-5">
                    <p class="text-xl font-semibold mb-2">Biaya</p>
                    <div class="space-y-1 my-2">
                        <div class="flex justify-between text-base font-semibold">
                            <p class="text-[#333333]">Harga</p>
                            <p class="text-[#23AEC1]">Rp. {{ number_format($detail['total']) }}</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Diskon per tanggal</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Surcharge per tanggal</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                    </div>

                    <hr class="border border-[#4F4F4F] my-5" />

                    <div class="space-y-1 my-2">
                        <div class="flex justify-between text-base font-semibold">
                            <p class="text-[#333333]">Harga per Tanggal</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Diskon Grup</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Harga Bersih</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Kupon Toko</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                    </div>

                    <hr class="border border-[#707070] my-5" />

                    <div class="space-y-1 my-2">
                        <div class="flex justify-between text-base font-semibold">
                            <p class="text-[#333333]">Harga Akhir</p>
                            <p class="text-[#23AEC1]">Rp. {{ number_format($detail['total'] + $total) }}</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Pilihan</p>
                            <p class="text-[#23AEC1]" id="add_price_amount">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Extra</p>
                            <p class="text-[#23AEC1]">Rp. {{ number_format($total) }}</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Biaya Lain-lain</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Biaya Pemesanan</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Pajak</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Promo</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Kupon</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                    </div>

                    <hr class="border border-[#707070] my-5" />

                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-lg font-semibold">Total Bayar</p>
                        <p class="text-lg font-semibold text-[#23AEC1]" id="total_bayar">Rp.
                            {{ number_format($detail['total'] + $total) }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                    this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                    if (!event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('input[type="checkbox"]').click(function() {
                var inputValue = $(this).attr("value");
                $("." + inputValue).toggle();
            });
        });
    </script>
    <script>

        $('#pickup_time').timepicker();

        $('input[name="pilihan[]"]').on('change',function(){
            let checked = $(this).is(":checked");
            let total_amount = '{{$detail['total']}}';
            let extra = '{{isset($total) ? $total : 0}}'
            let new_total_amount = 0;
            var harga = 0;

            if(checked){
                $('input[name="pilihan[]"]:checked').each(function(i, obj){
                    console.log(obj.value);
                    harga += parseInt(obj.value);
                })

                new_total_amount = parseInt(total_amount) + parseInt(extra) + harga;

                console.log('harga total: ', total_amount +' + '+ extra +'='+parseInt(total_amount) + parseInt(extra));
                console.log('harga pilihan: ', harga);

                setHarga(new_total_amount, harga)
            }


            if(!checked){
                
                harga = parseInt($("#harga_pilihan").val());
                total_amount = parseInt($("#total_price").val());
                
                harga -= $(this).val();
                new_total_amount = total_amount - $(this).val();

                console.log('total harga: ', total_amount);
                console.log('harga pilihan: ', harga);

                console.log(harga)
                console.log(new_total_amount)
                setHarga(new_total_amount, harga)

            }
        });

        function setHarga(new_total_amount, harga){
            $("#harga_pilihan").val(harga)
            $("#total_price").val(new_total_amount);
            $("#add_price_amount").html(`Rp. ${harga.toLocaleString('en-US',{ minimumFractionDigits: 0 })}`)
            $("#total_bayar").html(`Rp. ${new_total_amount.toLocaleString('en-US',{ minimumFractionDigits: 0 })}`)
        }

        $("input[name='airport_pickup']").on("change",function(){
           let  clicked = $(this).is(":checked");

           if(clicked){
            // alert('test')
            // element.setAttribute('disabled', 'disabled');
            $("#pickup_adress").prop("disabled",true)

            // getElementById('pickup_adress').setAttribute('disabled', 'disabled');
           }

           if(!clicked){
            $("#pickup_adress").removeAttr("disabled")
           }
        })

        function choiceMandatory(){
            let is_choice = $('input[name="pilihan[]"]').is(":checked");
            let choice = $('input[name="pilihan[]"]:checked');
            let total_amount = '{{$detail['total']}}';
            let new_total_amount = 0;
            let choice_price = 0;

            if(is_choice){
                choice.prop('disabled',true);
                choice.each(function(i, obj){
                    choice_price += parseInt(obj.value);
                });
    
                new_total_amount = parseInt(total_amount) + choice_price;

                console.log(choice_price)
                console.log(new_total_amount)
                setHarga(new_total_amount, choice_price)
            }
        }
    </script>
</body>
