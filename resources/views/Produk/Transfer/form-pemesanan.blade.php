<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tur Detail') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border border-b-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    <div class="container mx-auto my-5">
        <div class="grid grid-cols-6 lg:gap-5">
            {{-- Left --}}
            <div class="col-span-6 lg:col-span-4 p-3">

                <div class="block lg:hidden rounded-md border border-[#4F4F4F]">
                    <div class="mt-5 ml-3 flex">
                        <img src="{{ asset('storage/img/bajo-img.png') }}" alt="bajo-img" height="100px" width="100px">
                        <div class="text-[#333333] ml-1">
                            <p class="font-semibold text-lg">Laboan Bajo</p>
                            <p class="font-medium text-base">Nama Toko</p>
                            <p class="font-medium text-sm">ID Booking: 12NJAFB731</p>
                        </div>
                    </div>
                    <div class="rounded-md px-3 mb-3 bg-white">
                        <p class="text-sm font-semibold pb-1">Jumat, 12 Agustus 2022 • 09.00</p>
                        <p class="text-sm font-semibold">Dari</p>
                        <div class="flex">
                            <img src="{{ asset('storage/icons/bus-solid 1.svg') }}" alt="bus" width="20px"
                                height="20px">
                            <p class="ml-2 text-sm">Bandara Internasional YIA</p>
                        </div>
                        <img class="py-2" src="{{ asset('storage/icons/line-trans.svg') }}" alt="line">
                        <p class="text-sm font-semibold">Ke</p>
                        <div class="flex">
                            <img src="{{ asset('storage/icons/location-dot-solid 1.svg') }}" alt="loc"
                                width="15px" height="15px">
                            <p class="ml-2 text-sm">Hotel Yogyakarta</p>
                        </div>
                    </div>
                </div>

                {{-- Daftar Peserta --}}
                <div class="py-2.5 px-5 lg:py-5 lg:px-10 my-5 shadow-lg rounded-md border border-[#E0E0E0]">
                    <div class="block sm:flex sm:justify-between my-2">
                        <p class="text-lg lg:text-2xl font-semibold text-[#333333]">Daftar Peserta</p>
                        <p
                            class="text-xs lg:text-sm font-medium mt-2.5 grid justify-items-start lg:justify-items-end text-[#333333]">
                            ID Booking: {{ $code }}</p>
                    </div>
                    <div class="grid grid-cols-1 items-center">
                        <button
                            class="grid justify-items-start lg:justify-items-end text-[#D50006] font-medium text-sm lg:text-base">Hapus
                            Semua</button>
                    </div>
                    <label class="my-5 flex items-center">
                        <input value="user" type="checkbox" name="colorCheckbox" class="appearance-none mr-2.5 ..." />
                        <span class="text-xs lg:text-sm text-[#333333]"> Sama seperti pemilik akun</span>
                    </label>

                    <div class="user hidden">
                        <div class="mt-5 flex">
                            <img src="{{ asset('storage/img/avatar.png') }}" alt="avatar-img" height="100px"
                                width="100px">
                            <div class="text-[#333333] ml-5">
                                <p class="font-semibold text-2xl">{{ $profile->first_name_booking }}
                                    {{ $profile->last_name_booking }}
                                </p>
                                <p class="font-medium text-base">{{ $profile->email_booking }}</p>
                                <p class="font-medium text-sm mt-6">{{ $profile->phone_number_booking }}</p>
                            </div>
                        </div>
                    </div>

                    {{-- Forms --}}
                    <div class="user">
                        <div class="grid grid-rows-1 my-2">
                            <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                                <div class="relative block">
                                    <label
                                        class="form-label inline-block mb-2 text-[#333333] text-base font-medium">Nama
                                        Depan & Tengah</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="Nama depan" type="text" required>
                                </div>
                                <div class="relative block">
                                    <label
                                        class="form-label inline-block mb-2 text-[#333333] text-base font-medium">Nama
                                        Belakang</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="Nama belakang" type="text" required />
                                </div>
                            </div>
                        </div>

                        <div class="grid grid-rows-1 my-2">
                            <div class="grid sm:grid-cols-2 gap-2 sm:gap-5">
                                <div class="relative block">
                                    <label class="form-label inline-block mb-2 text-[#333333] text-base">Tanggal
                                        Lahir</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        type="date" required />
                                </div>

                                <div class="relative block">
                                    <label class="form-label inline-block mb-2 text-[#333333] text-base">Jenis
                                        Kelamin</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select required
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white">
                                            <option selected value="male">Pria</option>
                                            <option value="female">Wanita</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sm:grid sm:grid-cols-2 gap-5">
                            <div>
                                <div class="sm:flex my-2 space-y-2 sm:space-y-0">
                                    <div class="flex items-center mr-5">
                                        <input class="rounded-sm form-checkbox mr-2" type="checkbox">
                                        <label class="text-[#333333]">Residen</label>
                                    </div>
                                    <div class="flex items-center">
                                        <input class="rounded-sm form-checkbox mr-2" type="checkbox">
                                        <label class="text-[#333333]">Non Residen</label>
                                    </div>
                                </div>

                                <div class="relative block my-2">
                                    <label
                                        class="form-label inline-block mb-2 text-base text-[#333333]">Kebangsaan</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select required
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white">
                                            <option selected value="indonesia">Indonesia</option>
                                            <option value="japan">Jepang</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="relative block my-2">
                                    <label class="form-label inline-block mb-2 text-base text-[#333333]">No.
                                        HP/WhatsApp</label>
                                    <div class="flex">
                                        <div
                                            class="relative border border-[#828282] rounded-l-md bg-white shadow-sm sm:text-sm">
                                            <select
                                                class="appearance-none form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-l-md bg-white">
                                                <option selected value="+62">+62</option>
                                                <option value="+60">+60</option>
                                            </select>
                                        </div>
                                        <input
                                            class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-r-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                            placeholder="81234567890" type="text" required />
                                    </div>
                                </div>

                                <div class="relative block my-2">
                                    <label for=" email"
                                        class="form-label inline-block mb-2 text-base text-[#333333]">Email</label>
                                    <input
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        placeholder="emailku@gmail.com" type="email" required />
                                </div>

                                <div class="relative block my-2">
                                    <label class="form-label inline-block mb-2 text-base text-[#333333]">Status
                                        Residen</label>
                                    <div
                                        class="relative border border-[#828282] rounded-md bg-white shadow-sm sm:text-sm">
                                        <select required
                                            class="appearance-none w-full form-select border-0 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white">
                                            <option value="residen">Residen</option>
                                            <option value="non-residen">Non Residen</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <label class="form-label inline-block mb-2 text-base text-[#333333]">KTP/KITAS</label>
                                <div x-data="displayImage()">
                                    <div class="mb-2">
                                        <template x-if="imageUrl">
                                            <img :src="imageUrl"
                                                class="object-contain rounded border border-gray-300 w-full h-56">
                                        </template>

                                        <template x-if="!imageUrl">
                                            <div
                                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-full h-56">
                                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                    width="20px" height="20px">
                                            </div>
                                        </template>

                                        <input class="mt-2" type="file" accept="image/*"
                                            @change="selectedFile">
                                    </div>
                                </div>
                                <p class="text-xs font-normal"><b>Catatan:</b> Beberapa seller mungkin menerapkan
                                    kebijakan harga berbeda antara residen dan non-residen</p>
                            </div>
                        </div>
                    </div>

                    {{-- <form class="mb-10">
                        <div class="grid grid-cols-2 pt-5 px-5">
                            <div class="grid justify-items-start">
                                <label for="text" class="block mb-2 text-base font-medium text-[#333333] dark:text-gray-300">Nama Depan & Tengah</label>
                                <input type="text" id="text" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Nama Depan & Tengah">
                            </div>                           
                            <div class="grid justify-items-start pl-5">
                                <label for="text" class="block mb-2 text-base font-medium text-[#333333] dark:text-gray-300 text-left">Nama Belakang</label>
                                <input type="text" id="text" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Nama Belakang">
                            </div>
                        </div> 
                        <div class="grid grid-cols-2 pt-5 px-5">                          
                            <div class="grid justify-items-start">
                                <label for="text" class="block mb-2 text-base font-medium text-[#333333] dark:text-gray-300">Tanggal Lahir</label>
                                <input type="date" id="text" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Nama Depan & Tengah">
                            </div>                           
                            <div class="grid justify-items-start pl-5">
                                <label for="text" class="block mb-2 text-base font-medium text-[#333333] dark:text-gray-300 text-left">Jenis Kelamin</label>
                                <select class="px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                    <option>Pria</option>
                                    <option>Wanita</option>
                                </select>
                            </div>
                        </div>
                        <div class="pt-5 px-5 grid grid-cols-2 grid-rows-5">
                            <div class="flex py-5">
                                <div class="mr-5">
                                    <input type="checkbox" class="appearance-none checked:bg-blue-500 ..." />
                                    <span class="font-medium text-[#333333]">Residen</span>
                                </div>
                                <div class="mr-5">
                                    <input type="checkbox" class="appearance-none checked:bg-blue-500 ..." />
                                    <span class="font-medium text-[#333333]">Non-Residen</span>
                                </div>
                            </div>
                            <div class="pl-5 py-5">
                                <label for="text" class="block mb-2 text-base font-medium text-[#333333] dark:text-gray-300 text-left">KTP/KITAS</label>
                            </div>                                                    
                            <div class="grid justify-items-start">
                                <label for="text" class="block mb-2 text-base font-medium text-[#333333] dark:text-gray-300 text-left">Kebangsaan</label>
                                <select class="px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                    <option>Warga Negara Indonesia</option>
                                    <option>Warga Negara Asing</option>
                                </select>
                            </div>                            
                            <div class="pl-5 block grid col-start-2 row-start-2 row-end-5">
                                <div class="max-w-xl">
                                    <label class="flex justify-center w-full h-full px-4 transition bg-[#F2F2F2] border-[#BDBDBD] border-dashed rounded-md appearance-none cursor-pointer hover:border-gray-400 focus:outline-none">
                                        <span class="flex items-center space-x-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-[#BDBDBD]" fill="none" viewBox="0 0 24 24"
                                                stroke="currentColor" stroke-width="2">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                    d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12" />
                                            </svg>
                                        </span>
                                        <input type="file" name="file_upload" class="hidden">
                                    </label>
                                </div>
                            </div>                                                  
                            <div class="grid justify-items-start pt-5">
                                <label for="text" class="block mb-2 text-base font-medium text-[#333333] dark:text-gray-300 text-left">No. HP/WhatsApp</label>
                                <select class="px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                    <option>Warga Negara Indonesia</option>
                                    <option>Warga Negara Asing</option>
                                </select>
                            </div>                                                   
                            <div class="grid justify-items-start pt-5">
                                <label for="text" class="block mb-2 text-base font-medium text-[#333333] dark:text-gray-300 text-left">Email</label>
                                <input type="email" id="text" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="email@gmail.com">
                            </div>                                                    
                            <div class="grid justify-items-start pt-5">
                                <label for="text" class="block mb-2 text-base font-medium text-[#333333] dark:text-gray-300 text-left">Status Residen</label>
                                <select class="px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                    <option>Residen</option>
                                    <option>Non-Residen</option>
                                </select>
                            </div>                                                      
                            <div class="grid justify-items-start pt-5">
                                <span class="ml-5 text-sm"><b>Catatan:</b> Beberapa seller mungkin menerapkan kebijakan harga berbeda antara residen dan non-residen</span>
                            </div>  
                        </div>
                    </form> --}}
                </div>

                {{-- Pilihan --}}
                <div class="py-2.5 px-5 lg:py-5 lg:px-10 mb-5 rounded-md border-y-2 border-[#E0E0E0] bg-[#F7F7F7]">
                    <div>
                        @if(count($fields) > 0)
                        <div class="grid grid-cols-2 items-center">
                            <p class="text-base lg:text-2xl font-semibold grid justify-items-start text-[#333333]">
                                Pilihan
                            </p>
                        </div>
                            @foreach ($fields as $item)
                                <div class="flex lg:grid lg:grid-cols-2">
                                    <div class="mt-0 lg:mt-5 w-[222px]" x-data="choiceMandatory()">
                                        @if($item['kewajiban_pilihan']==='wajib')
                                        <input type="checkbox" class="appearance-none checked:bg-[#27BED3] w-3 h-3 ..." name="pilihan[]" value="{{$item['harga_pilihan']}}" checked/>
                                        @else
                                        <input type="checkbox" class="appearance-none checked:bg-[#27BED3] w-3 h-3 ..." name="pilihan[]" value="{{$item['harga_pilihan']}}"/>
                                        @endif
                                        <span class="font-medium text-[#333333] ml-1 text-[10px] lg:text-base">{{$item['nama_pilihan']}}</span>
                                    </div>
                                    <p class="mt-2 lg:mt-5 font-semibold text-[#27BED3] ml-4 lg:ml-10 text-[10px] lg:text-base">{{number_format($item['harga_pilihan'])}}</p>                                    
                                </div>
                            @endforeach
                        @endif
                        <div class="grid grid-cols-2 items-center">
                            <p class="text-base lg:text-2xl font-semibold grid justify-items-start text-[#333333]">
                                Pilihan tidak tersedia
                            </p>
                        </div>
                        {{-- <div class="flex lg:grid lg:grid-cols-2">
                            <div class="mt-0 lg:mt-5 w-[222px]">
                                <input type="checkbox" class="appearance-none checked:bg-[#27BED3] w-3 h-3 ..." />
                                <span class="font-medium text-[#333333] ml-1 text-[10px] lg:text-base">Sopir
                                    Berbahasa
                                    Inggris</span>
                            </div>
                            <p
                                class="mt-2 lg:mt-5 font-semibold text-[#27BED3] ml-4 lg:ml-10 text-[10px] lg:text-base">
                                Rp. 1,500,000</p>
                        </div> --}}
                    </div>
                </div>

                {{-- Extra --}}
                <div class="py-2.5 px-5 lg:py-5 lg:px-10 mb-5 rounded-md border-y-2 border-[#E0E0E0] bg-[#F7F7F7]">
                    <div>
                        <div class="grid grid-cols-1 items-center">
                            <p class="text-base lg:text-2xl font-semibold grid justify-items-start text-[#333333]">
                                Extra</p>
                        </div>
                        <div class="grid grid-cols-1">
                            <table class="w-full text-sm text-left">
                                <thead>
                                    <tr>
                                        <th>Nama Ekstra</th>
                                        <th>Jumlah Ekstra</th>
                                        <th>Total Harga Ekstra</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total = 0;
                                        $total_extra = 0;
                                    @endphp
                                    @if ($order['extra'] != null)
                                        @foreach ($order['extra'] as $key => $value)
                                            @php
                                                $total_harga = $total_extra + $value['jumlah_extra'] * $value['harga_extra'];
                                                $data_ekstra = App\Models\Extra::where('id', $value['id_extra'])->first();
                                                $total += $total_harga;
                                            @endphp
                                            <tr>
                                                <td>
                                                    <p>
                                                        {{ $data_ekstra->nama_ekstra }}
                                                    </p>
                                                </td>
                                                <td>
                                                    <p>
                                                        {{ $value['jumlah_extra'] }}
                                                    </p>
                                                </td>
                                                <td>
                                                    <p>
                                                        Rp. {{ number_format($total_harga) }}
                                                    </p>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <p class="mt-2.5 lg:mt-5 text-sm lg:text-base">Sebelum mengkonfirmasi pesanan Anda, harap untuk
                    memastikan kembali bahwa data yang anda masukkan sudah akurat.</p>
                <form action="{{ route('booking.store.transfer') }}" method="POST">
                    @csrf
                    <input type="hidden" name="customer_id" value="{{ $profile->id }}">
                    <input type="hidden" name="product_detail_id" value="{{ $detail['product_detail_id'] }}">
                    <input type="hidden" name="harga_ekstra" value="{{ $total }}">
                    <input type="hidden" name="start_date" value="{{ $detail['start_date'] }}">
                    <input type="hidden" name="total_price" value="{{ $detail['total'] + $total + $harga_kursi }}" id="total_price">
                    <input type="hidden" name="booking_code" value="{{ $code }}">
                    <input type="hidden" name="toko_id" value="{{ $detail['toko_id'] }}">
                    <input type="hidden" name="type" value="{{ $detail['type'] }}">
                    @if (auth()->user()->role == 'agent')
                        <input type="hidden" name="agent_id" value="{{ $agent_id->id }}">
                        <input type="hidden" name="role" value="{{ $agent_id->role }}">
                    @endif
                    <button type="submit"
                        class="text-white bg-[#27BED3] w-full hover:bg-[#1C97A8] focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-0 lg:mb-5 lg:mb-10 mt-5 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Simpan
                        di Koper</button>
                </form>
            </div>

            {{-- Right --}}
            <div class="col-span-6 lg:col-span-2 space-y-5 pb-2.5 px-2.5 lg:p-3">
                {{-- Informasi --}}
                <div class="hidden lg:block rounded-md border border-[#4F4F4F] lg:mt-5">
                    <div class="mt-5 ml-3 flex">
                        <img src="{{ asset('storage/img/bajo-img.png') }}" alt="bajo-img" height="100px"
                            width="100px">
                        <div class="text-[#333333] ml-1">
                            <p class="font-semibold text-lg">{{$detail['product_name']}}</p>
                            <p class="font-medium text-base">{{ $seller->first_name }}</p>
                            <p class="font-medium text-sm">ID Booking: {{$code}}</p>
                            @php
                                $i =1;
                                $no_kursi ="Nomor kursi: ";
                                // dump($order['chair']);
                                foreach ($order['chair'] as $key => $kursi) {
                                    # code...
                                    $no_kursi .= $kursi['no'];

                                    if($i != count($order['chair'])){
                                        $no_kursi .=", "; 
                                    }

                                    $i++;
                                }
                            @endphp
                            <p class="font-medium text-sm">{{$no_kursi}}</p>
                        </div>
                    </div>
                    <div class="rounded-md px-3 mb-3 bg-white">
                        @php
                            $date = DateTime::createFromFormat('Y-m-d', $detail['start_date']);
                        @endphp
                        <p class="text-sm font-semibold pb-1">{{ $date->format('l') }}, {{ $date->format('d F Y') }}
                            • 09.00</p>
                        {{-- @dump($order['route']) --}}
                        @foreach($order['route'] as $key => $rute)
                        <p class="text-sm font-semibold">Rute {{$key+1}}</p>
                        <p class="text-sm font-semibold">Dari</p>
                        <div class="flex">
                            <img src="{{ asset('storage/icons/bus-solid 1.svg') }}" alt="bus" width="20px"
                                height="20px">
                            <p class="ml-2 text-sm">{{$rute['rute.naik']}}</p>
                        </div>
                        <img class="py-2" src="{{ asset('storage/icons/line-trans.svg') }}" alt="line">
                        <p class="text-sm font-semibold">Ke</p>
                        <div class="flex mb-3">
                            <img src="{{ asset('storage/icons/location-dot-solid 1.svg') }}" alt="loc"
                                width="15px" height="15px">
                            <p class="ml-2 text-sm">{{$rute['rute.turun']}}</p>
                        </div>
                        @endforeach
                    </div>
                </div>

                {{-- Biaya --}}
                {{-- @dump($harga_kursi) --}}
                <div class="border border-[#4F4F4F] rounded-md shadow-lg p-5">
                    <p class="text-xl font-semibold mb-2">Biaya</p>
                    <div class="space-y-1 my-2">
                        <div class="flex justify-between text-base font-semibold">
                            <p class="text-[#333333]">Harga</p>
                            <p class="text-[#23AEC1]">Rp. {{ number_format($detail['total']) }}</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Diskon per tanggal</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Surcharge per tanggal</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                    </div>

                    <hr class="border border-[#4F4F4F] my-5" />

                    <div class="space-y-1 my-2">
                        <div class="flex justify-between text-base font-semibold">
                            <p class="text-[#333333]">Harga per Tanggal</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Diskon Grup</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Harga Bersih</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Kupon Toko</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                    </div>

                    <hr class="border border-[#707070] my-5" />

                    <div class="space-y-1 my-2">
                        <div class="flex justify-between text-base font-semibold">
                            <p class="text-[#333333]">Harga Akhir</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Pilihan</p>
                            <p class="text-[#23AEC1]" id="add_price_amount">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Extra</p>
                            <p class="text-[#23AEC1]">Rp. {{ number_format($total) }}</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Harga Kursi</p>
                            <p class="text-[#23AEC1]">Rp. {{ number_format($harga_kursi) }}</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Biaya Lain-lain</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Biaya Pemesanan</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Pajak</p>
                            <p class="text-[#23AEC1]">Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Promo</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                        <div class="flex justify-between text-sm font-normal">
                            <p class="text-[#333333]">Kupon</p>
                            <p class="text-[#23AEC1]">- Rp. 0</p>
                        </div>
                    </div>

                    <hr class="border border-[#707070] my-5" />

                    <div class="flex justify-between text-sm font-normal">
                        <p class="text-lg font-semibold">Total Bayar</p>
                        <p class="text-lg font-semibold text-[#23AEC1]" id="total_bayar">Rp.
                            {{ number_format($detail['total'] + $total + $harga_kursi) }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                    this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                    if (!event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },
            }
        }

        $('input[name="pilihan[]"]').on('change',function(){
            let checked = $(this).is(":checked");
            let total_amount = '{{$detail['total']}}';
            let extra = '{{isset($total) ? $total : 0}}'
            let new_total_amount = 0;
            var harga = 0;

            if(checked){
                $('input[name="pilihan[]"]:checked').each(function(i, obj){
                    console.log(obj.value);
                    harga += parseInt(obj.value);
                })

                new_total_amount = parseInt(total_amount) + parseInt(extra) + harga;

                console.log('harga total: ', total_amount +' + '+ extra +'='+parseInt(total_amount) + parseInt(extra));
                console.log('harga pilihan: ', harga);

                setHarga(new_total_amount, harga)
            }


            if(!checked){
                
                harga = parseInt($("#harga_pilihan").val());
                total_amount = parseInt($("#total_price").val());
                
                harga -= $(this).val();
                new_total_amount = total_amount - $(this).val();

                console.log('total harga: ', total_amount);
                console.log('harga pilihan: ', harga);

                console.log(harga)
                console.log(new_total_amount)
                setHarga(new_total_amount, harga)

            }
        });

        function setHarga(new_total_amount, harga){
            $("#harga_pilihan").val(harga)
            $("#total_price").val(new_total_amount);
            $("#add_price_amount").html(`Rp. ${harga.toLocaleString('en-US',{ minimumFractionDigits: 0 })}`)
            $("#total_bayar").html(`Rp. ${new_total_amount.toLocaleString('en-US',{ minimumFractionDigits: 0 })}`)
        }

        function choiceMandatory(){
            let is_choice = $('input[name="pilihan[]"]').is(":checked");
            let choice = $('input[name="pilihan[]"]:checked');
            let total_amount = '{{$detail['total']}}';
            let new_total_amount = 0;
            let choice_price = 0;

            if(is_choice){
                choice.prop('disabled',true);
                choice.each(function(i, obj){
                    choice_price += parseInt(obj.value);
                });
    
                new_total_amount = parseInt(total_amount) + choice_price;

                console.log(choice_price)
                console.log(new_total_amount)
                setHarga(new_total_amount, choice_price)
            }
        }

        // function getDropPickRute(){
        //     return:{

        //     }
        // }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('input[type="checkbox"]').click(function() {
                var inputValue = $(this).attr("value");
                $("." + inputValue).toggle();
            });
        });
    </script>
</body>
