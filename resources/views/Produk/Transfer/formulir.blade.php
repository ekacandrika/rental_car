<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Destindonesia') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles

    <style>
    </style>

</head>

<body class="bg-white font-Inter">

    <div class="max-w-7xl bg-[#F2F2F2] mx-auto rounded-md m-5">
        <div class="grid p-5 justify-items-center">
            <h1 class="font-semibold text-[26px]">Formulir Custom Transfer</h1>
        </div>

        <div class="p-10">

            <div class="max-w-7xl bg-[#F2F2F2] mx-auto rounded-md m-5">
                <div class="grid p-5 justify-items-center">
                    <h1 class="font-semibold text-[26px]">Formulir Custom Transfer</h1>
                </div>

                <div class="grid max-w-6xl p-5 mx-auto my-5 text-center">
                    <h1 class="font-semibold text-[24px]">Anda tidak menemukan jenis transfer yang sesuai? Anda butuh
                        pengaturan
                        khusus? Beritahukan kami. Tuliskan kebutuhan Anda di kotak berikut.</h1>
                </div>
                <form method="POST" action="{{ route('formulirTransfer.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="grid lg:grid-cols-[33%_33%_33%]">
                        <div>
                            <p class="text-kamtuu-second text-[18px]">ID : CUS01TO0001</p>
                        </div>

                        <div class="grid py-2 grid-cols-[80%_20%]">
                            <div class="p-2 bg-white border border-black rounded-xl">
                                <input name="tanggalPergi" id="datepickerFormulirTransfer" placeholder=" " />
                            </div>
                            <div class="md:hidden lg:hidden">
                                <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                    class="w-[15px] h-[15px] mt-[1rem] -ml-[30px] mr-1" alt="polygon 3" title="Kamtuu">
                            </div>
                        </div>
                        <div>
                            <p class="text-[18px] font-semibold">Jam</p>

                            <div class="grid py-2 grid-cols-[80%_20%]">
                                <div class="p-2 bg-white border border-black rounded-xl">
                                    <input name="jamPergi" id="timepickerFormulirTransfer" placeholder=" " />
                                </div>
                            </div>
                        </div>

                        <div class="grid lg:grid-cols-[33%_33%_33%]">
                            <div>
                                <p class="text-[18px] font-semibold">Dari</p>

                                <div class="grid py-2 grid-cols-[80%_20%]">
                                    <select name="transferDari" id=""
                                        class="p-2 bg-white border border-black focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7 rounded-xl">
                                        @foreach ($districts as $distrik)
                                            <option value="{{ $distrik->name }}">{{ $distrik->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="md:hidden lg:hidden">
                                        <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                            class="w-[15px] h-[15px] mt-[1rem] -ml-[30px] mr-1" alt="polygon 3"
                                            title="Kamtuu">
                                    </div>
                                </div>
                            </div>

                            <div>
                                <p class="text-[18px] font-semibold">Kota</p>

                                <div class="grid py-2 grid-cols-[80%_20%]">
                                    <select name="kotaDari" id=""
                                        class="p-2 bg-white border border-black focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7 rounded-xl">
                                        @foreach ($kabupatens as $kota)
                                            <option value="{{ $kota->name }}">{{ $kota->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="md:hidden lg:hidden">
                                        <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                            class="w-[15px] h-[15px] mt-[1rem] -ml-[30px] mr-1" alt="polygon 3"
                                            title="Kamtuu">
                                    </div>
                                </div>
                            </div>

                            <div>
                                <p class="text-[18px] font-semibold">Provinsi</p>

                                <div class="grid py-2 grid-cols-[80%_20%]">
                                    <select name="provinsiDari" id=""
                                        class="p-2 bg-white border border-black focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7 rounded-xl">
                                        @foreach ($provinsis as $provinsi)
                                            <option value="{{ $provinsi->name }}">{{ $provinsi->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="md:hidden lg:hidden">
                                        <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                            class="w-[15px] h-[15px] mt-[1rem] -ml-[30px] mr-1" alt="polygon 3"
                                            title="Kamtuu">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="grid grid-rows-2 py-5">
                            <div>
                                <p class="text-[18px] font-semibold">Embed Google Maps</p>
                            </div>
                            <div>
                                <input name="embedMapsDari" class="rounded-full" type="text">
                            </div>

                            {{--        <div class="w-[300px] h-[380px] md:w-[710px] md:h-[380px] lg:w-[710px] lg:h-[380px] border border-black rounded-lg m-1 md:m-5 lg:m-5"> --}}
                            {{--            <div class="h-[380px]"> --}}
                            {{--                <iframe class="rounded-lg" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d2965.0824050173574!2d-93.63905729999999!3d41.998507000000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sWebFilings%2C+University+Boulevard%2C+Ames%2C+IA!5e0!3m2!1sen!2sus!4v1390839289319" width="100%" height="100%" frameborder="0" style="border:0"></iframe> --}}
                            {{--            </div> --}}
                            {{--        </div> --}}

                            <div class="grid lg:grid-cols-[33%_33%_33%]">
                                <div>
                                    <p class="text-[18px] font-semibold">Ke</p>

                                    <div class="grid py-2 grid-cols-[80%_20%]">
                                        <select name="transferKe" id=""
                                            class="p-2 bg-white border border-black focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7 rounded-xl">
                                            @foreach ($districts as $distrik)
                                                <option value="{{ $distrik->name }}">{{ $distrik->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="md:hidden lg:hidden">
                                            <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                                class="w-[15px] h-[15px] mt-[1rem] -ml-[30px] mr-1" alt="polygon 3"
                                                title="Kamtuu">
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <p class="text-[18px] font-semibold">Kota</p>

                                    <div class="grid py-2 grid-cols-[80%_20%]">
                                        <select name="kotaKe" id=""
                                            class="p-2 bg-white border border-black focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7 rounded-xl">
                                            @foreach ($kabupatens as $kota)
                                                <option value="{{ $kota->name }}">{{ $kota->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="md:hidden lg:hidden">
                                            <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                                class="w-[15px] h-[15px] mt-[1rem] -ml-[30px] mr-1" alt="polygon 3"
                                                title="Kamtuu">
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <p class="text-[18px] font-semibold">Provinsi</p>

                                    <div class="grid py-2 grid-cols-[80%_20%]">
                                        <select name="provinsiKe" id=""
                                            class="p-2 bg-white border border-black focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7 rounded-xl">
                                            @foreach ($provinsis as $provinsi)
                                                <option value="{{ $provinsi->name }}">{{ $provinsi->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="md:hidden lg:hidden">
                                            <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                                class="w-[15px] h-[15px] mt-[1rem] -ml-[30px] mr-1" alt="polygon 3"
                                                title="Kamtuu">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="grid grid-rows-2 py-5">
                                <div>
                                    <p class="text-[18px] font-semibold">Embed Google Maps</p>
                                </div>
                                <div>
                                    <input name="embedMapsKe" class="rounded-full" type="text">
                                </div>

                                {{--        <div class="w-[300px] h-[380px] md:w-[710px] md:h-[380px] lg:w-[710px] lg:h-[380px] border border-black rounded-lg m-1 md:m-5 lg:m-5"> --}}
                                {{--            <div class="h-[380px]"> --}}
                                {{--                <iframe class="rounded-lg" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d2965.0824050173574!2d-93.63905729999999!3d41.998507000000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sWebFilings%2C+University+Boulevard%2C+Ames%2C+IA!5e0!3m2!1sen!2sus!4v1390839289319" width="100%" height="100%" frameborder="0" style="border:0"></iframe> --}}
                                {{--            </div> --}}
                                {{--        </div> --}}

                                <div>
                                    <fieldset class="question">
                                        <label class="text-[18px] font-semibold" for="coupon_question">Pulang Pergi
                                            ?</label>
                                        <input class="mx-2 coupon_question checked:bg-kamtuu-second" type="checkbox"
                                            name="coupon_question" value="yes" onchange="valueChanged()" />
                                    </fieldset>

                                    <fieldset class="my-5 answer">
                                        <div class="grid lg:grid-cols-[33%_33%_33%]">
                                            <div>
                                                <p class="text-[18px] font-semibold">Tanggal Pulang</p>

                                                <div class="grid py-2 grid-cols-[80%_20%]">
                                                    <div class="p-2 bg-white border border-black rounded-xl">
                                                        <input name="tanggalPulang" id="datepickerFormulirTransferPP"
                                                            placeholder=" " />
                                                    </div>
                                                    <div class="md:hidden lg:hidden">
                                                        <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                                            class="w-[15px] h-[15px] mt-[1rem] -ml-[30px] mr-1"
                                                            alt="polygon 3" title="Kamtuu">
                                                    </div>
                                                </div>
                                                <div>
                                                    <p class="text-[18px] font-semibold">Jam</p>

                                                    <div class="grid py-2 grid-cols-[80%_20%]">
                                                        <div class="p-2 bg-white border border-black rounded-xl">
                                                            <input name="jamPulang" id="timepickerFormulirTransferPP"
                                                                placeholder=" " />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="grid lg:grid-cols-[33%_33%_33%]">
                                                    <div>
                                                        <p class="text-[18px] font-semibold">Jenis Kendaraan</p>

                                                        <div class="grid py-2 grid-cols-[80%_20%]">
                                                            <select name="transport" id=""
                                                                class="p-2 bg-white border border-black focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7 rounded-xl">
                                                                <option value="Kendaraan Umum">Kendaraan Umum</option>
                                                                <option value="Mobil">Mobil</option>
                                                            </select>
                                                            <div class="md:hidden lg:hidden">
                                                                <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                                                    class="w-[15px] h-[15px] mt-[1rem] -ml-[30px] mr-1"
                                                                    alt="polygon 3" title="Kamtuu">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                    </fieldset>
                                </div>

                                <div class="col-span-2 row-span-2">
                                    <p>Dewasa</p>
                                    <p class="text-[#828282]">Diatas 12 tahun</p>
                                </div>
                                <div>
                                    <input name="dewasa" class="numberstyle" type="number" min="1"
                                        step="1" value="1">
                                </div>

                                <div class="col-span-2 row-span-2">
                                    <p>Anak</p>
                                    <p class="text-[#828282]">5 sampai 12 tahun</p>
                                </div>
                                <div>
                                    <input name="anak" class="numberstyle" type="number" min="1"
                                        step="1" value="1">
                                </div>

                                <div class="col-span-2 row-span-2">
                                    <p>Balita</p>
                                    <p class="text-[#828282]">Dibawah 5 tahun</p>
                                </div>
                                <div>
                                    <input name="balita" class="numberstyle" type="number" min="1"
                                        step="1" value="1">
                                </div>
                            </div>

                            <div class="col-span-2 row-span-2">
                                <p>Anak</p>
                                <p class="text-[#828282]">5 sampai 12 tahun</p>
                            </div>
                            <div>
                                <input class="numberstyle" type="number" min="1" step="1"
                                    value="1">
                            </div>
                            <div class="grid col-span-2">
                                <div class="flex">
                                    <input name="barangBesar" class="w-10 h-5 rounded-full" type="text">
                                    <p class="px-2 -mt-[4px]">pcs</p>
                                </div>
                            </div>
                            <div>
                                <input class="numberstyle" type="number" min="1" step="1"
                                    value="1">
                            </div>
                            <div class="grid col-span-2">
                                <div class="flex">
                                    <input name="barangKecil" class="w-10 h-5 rounded-full" type="text">
                                    <p class="px-2 -mt-[4px]">pcs</p>
                                </div>
                            </div>
                        </div>

                        <div class="py-5">
                            <textarea class="w-[15rem] lg:w-[35rem]" name="memo" id="" cols="50" rows="10"
                                placeholder="Tuliskan hal-hal lain yang perlu kami ketahui untuk mengaturkan transfer Anda."></textarea>

                            <div class="flex m-2">
                                <input class="checked:bg-kamtuu-second" type="checkbox" name="kamtuu"
                                    value="yes" id="">
                                <p class="px-3 -mt-1">Serahkan ke Kamtuu</p>
                            </div>


                            <x-destindonesia.button-primary>
                                @slot('button')
                                    Simpan
                                @endslot
                            </x-destindonesia.button-primary>
                </form>
            </div>

        </div>

        <footer>
            <x-destindonesia.footer></x-destindonesia.footer>
        </footer>

        <script>
            $('#datepickerFormulirTransfer').datepicker();
        </script>

        <script>
            $('#timepickerFormulirTransfer').timepicker();
        </script>

        <script>
            $('#datepickerFormulirTransferPP').datepicker();
        </script>

        <script>
            $('#timepickerFormulirTransferPP').timepicker();
        </script>

        <script>
            (function($) {

                $.fn.numberstyle = function(options) {

                    /*
                     * Default settings
                     */
                    var settings = $.extend({
                        value: 0,
                        step: undefined,
                        min: undefined,
                        max: undefined
                    }, options);

                    /*
                     * Init every element
                     */
                    return this.each(function(i) {

                        /*
                         * Base options
                         */
                        var input = $(this);

                        /*
                         * Add new DOM
                         */
                        var container = document.createElement('div'),
                            btnAdd = document.createElement('div'),
                            btnRem = document.createElement('div'),
                            min = (settings.min) ? settings.min : input.attr('min'),
                            max = (settings.max) ? settings.max : input.attr('max'),
                            value = (settings.value) ? settings.value : parseFloat(input.val());
                        container.className = 'numberstyle-qty';
                        btnAdd.className = (max && value >= max) ? 'qty-btn qty-add disabled' :
                            'qty-btn qty-add';
                        btnAdd.innerHTML = '+';
                        btnRem.className = (min && value <= min) ? 'qty-btn qty-rem disabled' :
                            'qty-btn qty-rem';
                        btnRem.innerHTML = '-';
                        input.wrap(container);
                        input.closest('.numberstyle-qty').prepend(btnRem).append(btnAdd);

                        /*
                         * Attach events
                         */
                        // use .off() to prevent triggering twice
                        $(document).off('click', '.qty-btn').on('click', '.qty-btn', function(e) {

                            var input = $(this).siblings('input'),
                                sibBtn = $(this).siblings('.qty-btn'),
                                step = (settings.step) ? parseFloat(settings.step) : parseFloat(input
                                    .attr('step')),
                                min = (settings.min) ? settings.min : (input.attr('min')) ? input.attr(
                                    'min') : undefined,
                                max = (settings.max) ? settings.max : (input.attr('max')) ? input.attr(
                                    'max') : undefined,
                                oldValue = parseFloat(input.val()),
                                newVal;

                            //Add value
                            if ($(this).hasClass('qty-add')) {

                                newVal = (oldValue >= max) ? oldValue : oldValue + step,
                                    newVal = (newVal > max) ? max : newVal;

                                if (newVal == max) {
                                    $(this).addClass('disabled');
                                }
                                sibBtn.removeClass('disabled');

                                //Remove value
                            } else {

                                newVal = (oldValue <= min) ? oldValue : oldValue - step,
                                    newVal = (newVal < min) ? min : newVal;

                                if (newVal == min) {
                                    $(this).addClass('disabled');
                                }
                                sibBtn.removeClass('disabled');

                            }

                            //Update value
                            input.val(newVal).trigger('change');

                        });

                        input.on('change', function() {

                            const val = parseFloat(input.val()),
                                min = (settings.min) ? settings.min : (input.attr('min')) ? input.attr(
                                    'min') : undefined,
                                max = (settings.max) ? settings.max : (input.attr('max')) ? input.attr(
                                    'max') : undefined;

                            if (val > max) {
                                input.val(max);
                            }

                            if (val < min) {
                                input.val(min);
                            }
                        });

                    });
                };

                $('.numberstyle').numberstyle();

            }(jQuery));
        </script>

        <script>
            function handler() {
                return {
                    fields: [],
                    addNewField() {
                        this.fields.push({
                            txt1: ''
                        });
                    },
                    removeField(index) {
                        this.fields.splice(index, 1);
                    }
                }
            }
        </script>

        <script>
            function valueChanged() {
                if ($('.coupon_question').is(":checked"))
                    $(".answer").show();
                else
                    $(".answer").hide();
            };
        </script>

</body>

</html>
