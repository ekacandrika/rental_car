<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tur Detail') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>

    {{-- sweet alert --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.min.css" rel="stylesheet">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    <style>
        input[type='number']::-webkit-outer-spin-button,
        input[type='number']::-webkit-inner-spin-button,
        input[type='number'] {
            -webkit-appearance: none;
            margin: 0;
            -moz-appearance: textfield !important;
        }

        #social-links ul {
            padding-left: 0;
        }

        #social-links ul li {
            display: inline-block;
        }

        #social-links ul li a {
            padding: 6px;
            /* border: 1px solid #ccc; */
            border-radius: 5px;
            margin: 1px;
            font-size: 25px;
        }

        #social-links .fa-facebook {
            color: #0d6efd;
        }

        #social-links .fa-twitter {
            color: deepskyblue;
        }

        #social-links .fa-linkedin {
            color: #0e76a8;
        }

        #social-links .fa-whatsapp {
            color: #25D366
        }

        #social-links .fa-reddit {
            color: #FF4500;
            ;
        }

        #social-links .fa-telegram {
            color: #0088cc;
        }

        @media screen and (max-width: 350px) {
            .gj-unselectable {
                left: 0 !important;
            }
        }

        .rate {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rate:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rate:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rate:not(:checked)>label:before {
            content: '★ ';
        }

        .rate>input:checked~label {
            color: #ffc700;
        }

        .rate:not(:checked)>label:hover,
        .rate:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rate>input:checked+label:hover,
        .rate>input:checked+label:hover~label,
        .rate>input:checked~label:hover,
        .rate>input:checked~label:hover~label,
        .rate>label:hover~input:checked~label {
            color: #c59b08;
        }

        .star-rating-complete {
            color: #c59b08;
        }

        .rating-container .form-control:hover,
        .rating-container .form-control:focus {
            background: #fff;
            border: 1px solid #ced4da;
        }

        .rating-container textarea:focus,
        .rating-container input:focus {
            color: #000;
        }

        .rated {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rated:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ffc700;
        }

        .rated:not(:checked)>label:before {
            content: '★ ';
        }

        .rated>input:checked~label {
            color: #ffc700;
        }

        .rated:not(:checked)>label:hover,
        .rated:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rated>input:checked+label:hover,
        .rated>input:checked+label:hover~label,
        .rated>input:checked~label:hover,
        .rated>input:checked~label:hover~label,
        .rated>label:hover~input:checked~label {
            color: #c59b08;
        }

        .maps-embed {
            padding-bottom: 60%
        }

        .maps-embed iframe {
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
            position: absolute;
        }
    </style>
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Gallery Lightbox --}}
    <div class="container mx-auto my-5">

        @if (count(json_decode($activity_detail['gallery'])) == 1)
        <div class="relative">
            <div class="mb-2 md:mb-0">
                <img class="object-cover object-center max-h-[426px] md:h-[287px] lg:h-[383px] xl:h-[479px] 2xl:h-[575px] w-full md:max-h-full"
                    src="{{ asset(json_decode($activity_detail['gallery'])[0]) }}" alt="detail-1">
            </div>
        </div>
        @endif
        @if (count(json_decode($activity_detail['gallery'])) > 1)
        <div class="relative md:grid md:grid-cols-2 grid-flow-col gap-2">
            <div class="mb-2 md:mb-0">
                <img class="object-cover object-center max-h-[426px] md:h-[287px] lg:h-[383px] xl:h-[479px] 2xl:h-[575px] w-full md:max-h-full"
                    src="{{ asset($galleries[0]) }}" alt="detail-1">
            </div>
            <div>
                <div class="grid grid-cols-2 md:grid-cols-1 md:grid-rows-2 gap-2">
                    <div class="grid grid-cols-2 gap-2">
                        <div>
                            <img class="object-cover object-center h-full md:h-[139px] lg:h-[187px] xl:h-[235px] 2xl:h-[283px] w-full md:max-h-full"
                                src="{{ asset($galleries[1]) }}" alt="detail-2">
                        </div>
                        <div>
                            <img class="object-cover object-center h-full md:h-[139px] lg:h-[187px] xl:h-[235px] 2xl:h-[283px] w-full md:max-h-full"
                                src="{{ asset($galleries[2]) }}" alt="detail-3">
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-2">
                        <div>
                            <img class="object-cover object-center h-full md:h-[139px] lg:h-[187px] xl:h-[235px] 2xl:h-[283px] w-full md:max-h-full"
                                src="{{ asset($galleries[3]) }}" alt="detail-4">
                        </div>
                        <div>
                            <img class="object-cover object-center h-full md:h-[139px] lg:h-[187px] xl:h-[235px] 2xl:h-[283px] w-full md:max-h-full"
                                src="{{ asset($galleries[4]) }}" alt="detail-5">
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex md:absolute md:bottom-0 md:right-0 gap-3">
                <div x-data="{ open: false }" @keydown.escape="open = false">
                    <button @click="open = true"
                        class="w-full md:w-fit md:-translate-x-5 md:-translate-y-5 px-1 md:px-3 py-1 lg:px-5 lg:py-2 text-sm sm:text-base font-semibold border-2 border-gray-300 text-black duration-300 bg-gray-300 rounded-sm hover:bg-gray-100">
                        Semua foto
                    </button>
                    <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                        style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                        <div
                            class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                            <button
                                class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                            </button>
                        </div>
                        <div class="h-full w-full flex items-center justify-center overflow-hidden"
                            x-data="{ active: 0, slides: {{ $activity_detail['gallery'] }} }">
                            <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                    <button type="button"
                                        class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === 0 ? slides.length - 1 : active - 1">
                                        <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>
    
                            <template x-for="(slide, index) in slides" :key="index"
                                x-data="{ url: window.location.host, protocol: window.location.protocol }">
                                <div class="h-full w-full flex items-center justify-center absolute">
                                    <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                        x-show="active === index" x-transition:enter="transition ease-out duration-150"
                                        x-transition:enter-start="opacity-0 transform scale-90"
                                        x-transition:enter-end="opacity-100 transform scale-100"
                                        x-transition:leave="transition ease-in duration-150"
                                        x-transition:leave-start="opacity-100 transform scale-100"
                                        x-transition:leave-end="opacity-0 transform scale-90">
    
                                        <img :src="`${protocol}//${url}/${slide}`"
                                            class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                    </div>
                                    <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                        x-show="active === index">
                                        <span class="w-12 text-right" x-text="index + 1"></span>
                                        <span class="w-4 text-center">/</span>
                                        <span class="w-12 text-left" x-text="slides.length"></span>
                                    </div>
                                </div>
                            </template>
                            <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-start w-12 md:ml-16">
                                    <button type="button"
                                        class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === slides.length - 1 ? 0 : active + 1">
                                        <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div x-data="{ open: false }" @keydown.escape="open = false">
                    <button @click="open = true"
                        class="relative w-full md:w-fit md:-translate-x-5 md:-translate-y-5 px-1 md:px-3 py-1 lg:px-5 lg:py-2 text-sm sm:text-base font-semibold border-2 border-gray-300 text-black duration-300 bg-gray-300 rounded-sm hover:bg-gray-100">
                        <span class="md:pr-[11px]">
                           {{$like_count}} Favorit 
                        </span>
                        <span>
                            <svg class="absolute md:top-[9px] md:right-0 w-6 h-6 text-white hover:fill-red-500 md:pr-[7px]" id="16" onclick="submitLike2(16)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z"></path>
                            </svg>
                        </span>
                    </button>
                </div>
                <div x-data="{ open: false }" @keydown.escape="open = false">
                    <button @click="open = true"
                        class="w-full md:w-fit md:-translate-x-5 md:-translate-y-5 px-1 md:px-3 py-1 lg:px-5 lg:py-2 text-sm sm:text-base font-semibold border-2 border-gray-300 text-black duration-300 bg-gray-300 rounded-sm hover:bg-gray-100">
                        Lihat Video
                    </button>
                    <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                        x-show.transition="open" style="background-color: rgba(0,0,0,.7)">
                        <div
                            class="flex items-center justifty-start w-12 m-2 ml-6 mb-4 md:m-2 z-100 absolute right-1 top-1 transform">
                            <button
                                class="text-white item-center flex w-12 h-12 rounded-full justify-center focus:outline-none"
                                style="background-color: rgba(230, 230, 230, 0.4)" @click="open = false">
                                {{-- relative top-[10px] --}}
                                <img src="{{ asset('storage/icons/close-button.svg') }}"
                                    class="w-6 h-6 relative top-[10px]">
                            </button>
                        </div>
                        <div class="h-full w-full flex items-center justify-center overflow-hidden">
                            {{-- <div class="h-full w-full flex items-center justify-center absolute">
                                <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                    x-transition:enter="transition ease-out duration-150"
                                    x-transition:enter-start="opacity-0 transform scale-90"
                                    x-transition:enter-end="opacity-100 transform scale-100"
                                    x-transition:leave="transition ease-in duration-150"
                                    x-transition:leave-start="opacity-100 transform scale-100"
                                    x-transition:leave-end="opacity-0 transform scale-90">
                                </div> 
                            --}}
                                @php
                                $video_id = explode("https://www.youtube.com/watch?v=",$activity_detail->video_embed);
                                @endphp
    
                                <iframe class="max-w-[1800px] max-h-[700]l"
                                    src="https://www.youtube.com/embed/{{isset($video_id[1]) ? $video_id[1]:null}}" frameborder="0"
                                    allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endif
    </div>

    <div>
        {{-- Tabs --}}
        <div class="container mx-auto my-5 sticky bg-white top-0 z-20" x-data="{
            active: 0,
            tabs: ['Ringkasan', 'Paket', 'Lokasi','Video', 'Ulasan']
        }">
            <div class="flex sm:block">
                <ul
                    class="flex justify-start overflow-x-auto text-sm font-medium text-center sm:text-left text-gray-500 dark:text-gray-400 border-b border-[#BDBDBD]">
                    <template x-for="(tab, index) in tabs" :key="index">
                        <li class="pb-2 sm:mt-2" :class="{ 'border-b-2 border-[#9E3D64]': active == index }"
                            x-on:click="active = index">
                            <a :href="`#` + tab.toLowerCase()">
                                <button
                                    class="inline-block py-1 sm:py-2 px-5 sm:px-9 text-lg sm:text-xl rounded-lg duration-200"
                                    :class="{ 'text-[#9E3D64] bg-white font-bold': active ==
                                        index, 'text-black font-normal hover:text-[#9E3D64]': active != index }"
                                    x-text="tab"></button>
                            </a>
                        </li>
                    </template>
                </ul>
            </div>
        </div>

        {{-- Content --}}
        <div class="container mx-auto">
            <div class="grid grid-cols-5 gap-5">
                {{-- Left Side --}}
                <div class="col-span-5 lg:col-span-4 bg-white p-3">
                    {{-- Ringkasan --}}
                    <div id="ringkasan">
                        <p class="text-3xl py-3 font-semibold text-[#333333] whitespace-normal break-words">
                            {{ $activity['product_name'] }}
                        </p>
                        <p class="text-xl py-1 font-semibold text-[#333333] whitespace-normal break-words">
                            {{ isset($provinsi->name) ? $provinsi->name : null }}</p>
                        <div class="flex py-2 items-center">
                            <img class="mx-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <p class="text-xl font-semibold text-[#333333]">{{ $ratings }} <span class="font-normal">({{
                                    $var2 }} Ulasan)</span></p>
                        </div>
                        <div class="flex flex-wrap my-2 py-1 text-sm">
                            <p class="mt-2 mr-1"><span class="font-semibold">{{ $booking_count }}</span> kali dipesan •
                            </p>
                            <p class="mt-2 mr-1"><span class="font-semibold">{{ $like_count }}</span> orang menyukai ini
                                •</p>
                            <div class="mt-2 flex">
                                <p>Share</p>
                                <div class="flex justify-end">
                                    {!! $share !!}
                                </div>
                            </div>
                        </div>
                        <div class="text-justify my-5">
                            <p class="text-3xl my-3 font-semibold">Deskripsi Activity</p>
                            {!! $activity_detail['deskripsi'] !!}
                        </div>
                        {{-- Paket Termasuk --}}
                        <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                            x-data="{ open: false }">
                            <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-3': !open }">
                                <button
                                    class="text-lg sm:text-xl md:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                    x-on:click="open = !open">Paket Termasuk
                                </button>
                                <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                                    x-on:click="open = !open"
                                    src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}" alt="chevron-down"
                                    width="26px" height="15px">
                            </div>
                            <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                                x-show="open" x-transition>
                                {!! $activity_detail['paket_termasuk'] !!}
                            </ul>
                        </div>

                        {{-- Paket Tidak Termasuk --}}
                        <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                            x-data="{ open: false }">
                            <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-3': !open }">
                                <button
                                    class="text-lg sm:text-xl md:text-2xl  text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                    x-on:click="open = !open">Paket Tidak Termasuk
                                </button>
                                <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                                    x-on:click="open = !open"
                                    src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}" alt="chevron-down"
                                    width="26px" height="15px">
                            </div>
                            <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                                x-show="open" x-transition>
                                {!! $activity_detail['paket_tidak_termasuk'] !!}
                            </ul>
                        </div>
                    </div>

                    {{-- Paket --}}
                    <form action="{{ route('activity.activityOrder', $activity->slug) }}" method="POST" id="paket"
                        class="py-3 px-2 border-y  bg-[#F9F9F9] border-y-[#828282]" x-data="paket()" x-init="jamChange">
                        @csrf
                        <p class="text-3xl py-3 font-semibold text-[#333333]">Paket</p>
                        <input type="hidden" name="product_name" value="{{ $activity['product_name'] }}">
                        <input type="hidden" name="product_id" value="{{ $activity['id'] }}">
                        <input type="hidden" name="paket_id" value="{{ $activity_detail['paket_id'] }}">
                        <input type="hidden" name="pilihan_id" value="{{ $activity_detail['pilihan_id'] }}">
                        <input type="hidden" name="toko_id" value="{{ $activity['user_id'] }}">
                        <input type="hidden" name="product_detail_id" value="{{ $activity_detail['id'] }}">
                        <input type="hidden" name="type" value="{{ $activity['type'] }}">
                        <div class="grid sm:flex justify-between">
                            {{-- <div class="relative block order-last sm:order-none my-3 sm:my-0">
                                <label for="activity-date-picker"
                                    class="form-label inline-block mb-2 text-gray-700">Tanggal
                                    Activity</label>
                                <div
                                    class="placeholder:text-[#BDBDBD] block bg-white w-full md:w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                    <input placeholder="Pilih tanggal" type="text" id="activity_date"
                                        :class="'!m-0 sm:!w-full'" name="activity_date" x-model="activity_date" />
                                </div>
                            </div> --}}
                            @if ($diskon->tgl_start != null && $diskon->tgl_end != null)
                            <div class="relative block order-last sm:order-none my-3 sm:my-0">
                                <label for="activity-date-picker"
                                    class="form-label inline-block mb-2 text-gray-700">Tanggal
                                    Activity</label>
                                <div
                                    class="placeholder:text-[#BDBDBD] block bg-white w-full md:w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                    <input placeholder="Pilih tanggal" type="text" id="activity_date"
                                        :class="'!m-0 sm:!w-full'" name="activity_date" x-model="activity_date"
                                        readonly />
                                </div>
                                <div id="activity_date_error" class="hidden my-1 w-full py-1 text-red-500 rounded-md">
                                    Tanggal activity wajib diisi!</div>
                            </div>
                            @else
                            <div class="relative block order-last sm:order-none my-3 sm:my-0">
                                <label for="activity-date-picker"
                                    class="form-label inline-block mb-2 text-gray-700">Tanggal
                                    Activity</label>
                                <div
                                    class="placeholder:text-[#BDBDBD] block bg-white w-full md:w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                    <input placeholder="Pilih tanggal" type="text" id="activity_date"
                                        :class="'!m-0 sm:!w-full'" name="activity_date" x-model="activity_date"
                                        disabled />
                                </div>
                            </div>
                            @endif
                            <button @click="deleteAll" type="button"
                                class="border border-[#D50006] order-1 sm:order-none bg-transparent text-[#D50006] rounded-md px-3 py-2 h-fit hover:bg-[#D50006] hover:text-white duration-200">
                                Hapus
                                Semua
                            </button>
                        </div>
                        <div class="my-3">
                            <p class="mb-3">Pilih Paket</p>
                            <div>
                                <template x-for="(paket, index) in nama_pakets">
                                    <button @click="current_index = index" type="button" :class="current_index == index ? 'text-white bg-kamtuu-second' :
                                            'text-[#333] bg-white border border-gray-300'"
                                        class="rounded-md w-full sm:w-1/3 text-left m-2 ml-0 px-3 py-2 duration-200">
                                        <div class="pb-3" x-text="paket"></div>
                                        <div class="text-xs">Waktu: <span x-text="jam_pakets[index]"></span></div>
                                        <template x-if="current_index === index">
                                            <div>
                                                <input type="hidden" name="paket[index]" :value="index">
                                                <input type="hidden" name="paket[nama_paket]" :value="paket">
                                                <input type="hidden" name="paket[waktu]" :value="jam_pakets[index]">
                                            </div>
                                        </template>
                                        <template x-if="min_peserta[index] === 0 && max_peserta[index] === 0">
                                            <p class="text-xs">Tidak ada ketentuan jumlah peserta.</p>
                                        </template>
                                        <template
                                            x-if="min_peserta[index] >= 0 && max_peserta[index] != 0 || max_peserta[index] >= 0 && min_peserta[index] != 0">
                                            <div class="flex space-x-2">
                                                <div class="text-xs">Min peserta: <span
                                                        x-text="min_peserta[index]"></span>
                                                </div>
                                                <div class="text-xs">Max peserta: <span
                                                        x-text="max_peserta[index]"></span>
                                                </div>
                                            </div>
                                        </template>
                                    </button>
                                </template>
                            </div>
                        </div>

                        {{-- Tambahan --}}
                        @if (isset($activity_detail['pilihan']) || $activity_detail['izin_ekstra'] == 'Bolehkan')
                        <p class="my-3">Tambahan</p>
                        <div class="my-3 sm:flex space-y-3 sm:space-y-0 sm:space-x-3">
                            @if (isset($activity_detail['pilihan']))
                            <div class="sm:w-1/4">
                                @livewire('tur.modal-pilihan', ['pilihans' => $pilihan])
                            </div>
                            @endif
                            @if ($activity_detail['izin_ekstra'] == 'Bolehkan')
                            <div class="sm:w-1/4">
                                @livewire('tur.modal-ekstra')
                            </div>
                            @endif
                        </div>
                        @endif

                        {{-- Jumlah Peserta --}}
                        <div x-data="price()" x-init="sumTotal()">
                            <div class="my-5">
                                <p class="my-2">Jumlah</p>
                                <div class="flex justify-between my-3">
                                    <div>
                                        <p class="text-sm font-medium">Dewasa</p>
                                        <p class="text-xs font-normal text-[#828282]">Di atas 12 tahun</p>
                                    </div>
                                    <div class="flex justify-end"
                                        x-data="{ increment() { $store.detail.dewasa_count++ }, decrement() { $store.detail.dewasa_count === 1 ? 1 : $store.detail.dewasa_count--; $store.detail.anak_count >= $store.detail.dewasa_count ? $store.detail.anak_count-- : $store.detail.anak_count; $store.detail.balita_count >= $store.detail.dewasa_count ? $store.detail.balita_count-- : $store.detail.balita_count } }">
                                        <button type="button" class="text-3xl font-semibold mx-3"
                                            x-on:click="decrement()">-</button>
                                        <input
                                            class="block w-[100px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                            type="number" min="1" step="1" x-model="$store.detail.dewasa_count"
                                            name="dewasa_count">
                                        <button type="button" class="text-3xl font-semibold mx-3"
                                            x-on:click="increment()">+</button>
                                    </div>
                                </div>
                                <div class="flex justify-between my-3">
                                    <div>
                                        <p class="text-sm font-medium">Anak-anak</p>
                                        <p class="text-xs font-normal text-[#828282]">5 - 12 tahun</p>
                                    </div>
                                    <div class="flex justify-end"
                                        x-data="{ increment() { $store.detail.anak_count >= $store.detail.dewasa_count ? $store.detail.anak_count : $store.detail.anak_count++ }, decrement() { $store.detail.anak_count === 0 ? 0 : $store.detail.anak_count-- } }">
                                        <button type="button" class="text-3xl font-semibold mx-3"
                                            x-on:click="decrement()">-</button>
                                        <input
                                            class="block w-[100px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                            type="number" min="0" step="1" x-model="$store.detail.anak_count"
                                            name="anak_count">
                                        <button type="button" class="text-3xl font-semibold mx-3"
                                            x-on:click="increment()">+</button>
                                    </div>
                                </div>
                                <div class="flex justify-between my-3">
                                    <div>
                                        <p class="text-sm font-medium">Balita</p>
                                        <p class="text-xs font-normal text-[#828282]">Di bawah 5 tahun</p>
                                    </div>
                                    <div class="flex justify-end"
                                        x-data="{ increment() { $store.detail.balita_count >= $store.detail.dewasa_count ? $store.detail.balita_count : $store.detail.balita_count++ }, decrement() { $store.detail.balita_count === 0 ? 0 : $store.detail.balita_count-- } }">
                                        <button type="button" class="text-3xl font-semibold mx-3"
                                            x-on:click="decrement()">-</button>
                                        <input
                                            class="block w-[100px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                            type="number" min="0" step="1" x-model="$store.detail.balita_count"
                                            name="balita_count">
                                        <button type="button" class="text-3xl font-semibold mx-3"
                                            x-on:click="increment()">+</button>
                                    </div>
                                </div>
                            </div>
                            <div id="min_max_peserta_error" class="hidden my-1 w-full py-1 text-red-500 rounded-md">
                                Jumlah peserta belum sesuai!</div>
                            <div class="sm:flex justify-between text-[#333333]">
                                <div class="block">
                                    <p class="text-base font-medium">Mulai</p>
                                    
                                    <template x-if="$store.detail.total_price === '-'">
                                        <p
                                            class="text-xl lg:text-2xl xl:text-3xl sm:text-center py-2 font-bold text-[#23AEC1]">
                                            IDR <span x-text="$store.detail.total_price"></span>
                                        </p>
                                    </template>
                                    <template x-if="$store.detail.total_price !== '-'">
                                        <p
                                            class="text-xl lg:text-2xl xl:text-3xl sm:text-center py-2 font-bold text-[#23AEC1]">
                                            IDR <span
                                                x-text="new Intl.NumberFormat().format($store.detail.total_price)"></span>
                                        </p>
                                    </template>
                                </div>
                                @if(isset(auth()->user()->role))
                                    @if (auth()->user()->role === 'traveller')
                                    <div class="flex justify-center">
                                        <x-destindonesia.button-primary text="">
                                            @slot('button')
                                            Pesan Sekarang
                                            @endslot
                                        </x-destindonesia.button-primary>
                                    </div>
                                    @else
                                    <div class="flex justify-center">
                                        <p
                                            class="grid align-middle content-center my-3 px-8 py-2 md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white cursor-pointer">
                                            Silakan login menggunakan akun traveller.
                                        </p>
                                    </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </form>
                    {{-- Video Embed --}}
                    <div class="pr-2 py-3" id="video">
                        <p class="text-3xl mb-2 pr-3 font-semibold text-[#333333]">Video</p>
                        {{-- begin embed youtube link --}}
                        @php
                        $video_id = explode("https://www.youtube.com/watch?v=",$activity_detail->video_embed);
                        @endphp
                        {{-- {!! $xstay_detail->video_embed !!} --}}
                        <iframe class="max-w-[800px] max-h-[500px]" src="https://www.youtube.com/embed/{{isset($video_id[1]) ? $video_id[1] : null}}"
                            frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>

                {{-- Right Side --}}
                <div class="col-span-5 lg:col-span-1 order-last lg:order-none" x-data="price()">
                    {{-- Price --}}
                    <div class="hidden lg:block rounded-md shadow-lg p-3 mb-3 bg-white border border-gray-200">
                    @if(isset(auth()->user()->role))     
                        {{-- @dump(auth()->user()->role)       --}}
                        @if(auth()->user()->role=='traveller')             
                        <div x-data="{ bookmarked: false }" class="flex justify-between text-[#333333]">
                            <p class="text-base font-medium">Mulai</p>
                            <button x-on:click="bookmarked = !bookmarked" class="flex text-sm">
                                <img class="mr-1" x-show="bookmarked === false"
                                    src="{{ asset('storage/icons/bookmark-regular 1.svg') }}" alt="bookmark"
                                    width="14px">
                                <img class="mr-1" x-show="bookmarked === true"
                                    src="{{ asset('storage/icons/bookmark-solid 1.svg') }}" alt="bookmark" width="14px">
                                Favorit
                            </button>
                        </div>
                        <template x-if="$store.detail.total_price === '-'">
                            <p class="text-xl lg:text-2xl xl:text-3xl sm:text-center py-2 font-bold text-[#23AEC1]">
                                IDR <span x-text="$store.detail.total_price"></span>
                            </p>
                        </template>
                        <template x-if="$store.detail.total_price !== '-'">
                            <p class="text-xl lg:text-2xl xl:text-3xl sm:text-center py-2 font-bold text-[#23AEC1]">
                                IDR <span x-text="new Intl.NumberFormat().format($store.detail.total_price)"></span>
                            </p>
                        </template>
                        <div class="flex justify-center">
                            <a href="#paket">
                                <x-destindonesia.button-primary text="Pesan Sekarang">
                                    @slot('button')
                                    Pesan Sekarang
                                    @endslot
                                </x-destindonesia.button-primary>
                            </a>
                        </div>
                        @else
                        <div class="flex justify-center">
                            <p
                                class="grid align-middle content-center my-3 px-8 py-2 md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white cursor-pointer">
                                Silakan login menggunakan akun traveller.
                            </p>
                        </div>
                        @endif
                    @else
                    <div class="flex justify-center">
                        <a href="#paket">
                            <x-destindonesia.button-primary text="Login Sekarang">
                                @slot('button')
                                Login Sekarang
                                @endslot
                            </x-destindonesia.button-primary>
                        </a>
                    </div>    
                    @endif
                    </div>

                    {{-- Store --}}
                    <div class="lg:rounded-md lg:shadow-lg p-3 my-5 bg-white border-y-2 lg:border border-gray-200">
                        <a href={{route('toko.show',$activity->user->id)}}>
                            <p class="lg:text-2xl xl:text-3xl font-bold text-center text-[#333333]">Kunjungi Toko</p>
                        </a>
                        <div class="flex justify-center py-5">
                            <a href={{route('toko.show',$activity->user->id)}}>
                                <img src="{{ isset($info_toko->logo_toko) ? asset($info_toko->logo_toko) : asset('storage/img/tur-detail-1.png') }}"
                                    class="bg-clip-content lg:w-[96px] lg:h-[96px] xl:w-[100px] xl:h-[100px] shadow-xl bg-white rounded-full"
                                    alt="store-profile">
                            </a>    
                        </div>
                        <p class="pt-3 text-xl font-semibold text-center">{{ isset($info_toko->nama_toko) ? $info_toko->nama_toko :'Nama Toko'}}</p>
                        <p class="text-lg font-normal text-center pb-2">Bergabung sejak {{
                            isset($activity->user->created_at)
                            ? date('Y',strtotime($activity->user->created_at)) : '2021'}}</p>
                        <div class="flex justify-center">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                        </div>
                        <div class="flex justify-center">
                            @if (Auth::user())
                            <x-destindonesia.button-primary text="Kirim Pesan">
                                @slot('button')
                                <a href="{{ route('inbox', [$activity->user_id, $activity->type]) }}">Kirim
                                    Pesan</a>
                                @endslot
                            </x-destindonesia.button-primary>
                            @else
                            <x-destindonesia.button-primary text="Kirim Pesan">
                                @slot('button')
                                <a href="#">Kirim
                                    Pesan</a>
                                @endslot
                            </x-destindonesia.button-primary>
                            @endif
                        </div>
                    </div>

                    {{-- QR Code --}}
                    <div>
                        <p class="text-2xl font-semibold text-center">Kode QR Produk</p>
                        <div class="flex justify-center mt-5">
                            {{-- <img src="{{}}" alt="QR-code"> --}}
                            <img src="{{ " data:image/png;base64,".DNS2D::getBarcodePNG($activity_detail->base_url ??
                            url('/').'/activity/'.$activity->slug,'QRCODE')}}"
                            alt="QR-code">
                        </div>
                        <a class="flex justify-center"
                            href="{{ route('download.qrCode', ['slug' => $activity->slug, 'base64' => DNS2D::getBarcodePNG($activity_detail->base_url ?? url('/').'/activity/'.$activity->slug, 'QRCODE')]) }}"
                            download="QR-code-produk">
                            <x-destindonesia.button-primary text="">
                                @slot('button')
                                Unduh Kode QR
                                @endslot
                            </x-destindonesia.button-primary>
                        </a>
                    </div>


                </div>

                {{-- Content --}}
                <div class="order-1 lg:order-none col-span-5 p-3">
                    <hr class="border border-[#BDBDBD] my-5">

                    {{-- Lokasi --}}
                    <div id="lokasi" class="my-5">
                        <p class="text-3xl py-3 font-semibold text-[#333333]">Lokasi Anda</p>
                        <p class="text-sm font-normal pb-3">{{ isset($regency->name) ? $regency->name .',' : ''}} {{
                            isset($provinsi->name) ? $provinsi->name :null}}</p>
                        <div>
                            {!! $activity_detail['link_maps'] !!}
                            {{-- <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15810.940300744738!2d110.3976435!3d-7.817842!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a57148c5e4d03%3A0x4027a76e35300b0!2sKotagede%2C%20Yogyakarta%20City%2C%20Special%20Region%20of%20Yogyakarta!5e0!3m2!1sen!2sid!4v1666232541267!5m2!1sen!2sid"
                                style="border:0;" allowfullscreen="" loading="lazy" class="rounded-lg w-full h-[450px]"
                                referrerpolicy="no-referrer-when-downgrade"></iframe> --}}
                        </div>
                    </div>

                    {{-- Perlu Diketahui --}}
                    <p class="text-3xl py-3 font-semibold text-[#333333]">Perlu Diketahui</p>
                    {{-- Catatan --}}
                    <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                        x-data="{ open: false }">
                        <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-3': !open }">
                            <button
                                class="text-lg sm:text-xl md:text-2xl  text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open">Catatan
                            </button>
                            <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        <div class="py-5 px-3 text-[#4F4F4F]" x-show="open" x-transition>
                            {!! $activity_detail['catatan'] !!}
                        </div>
                    </div>

                    {{-- Kebijakan Pembatalan --}}
                    <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                        x-data="{ open: false }">
                        <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-3': !open }">
                            <button
                                class="text-lg sm:text-xl md:text-2xl  text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open">Kebijakan Pembatalan
                            </button>
                            <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        {{-- @php
                        dd($activity_detail['kebijakan_pembatalan_sebelumnya']);
                        @endphp --}}
                        <div class="py-5 px-3 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl" x-show="open"
                            x-transition>

                            @if (isset($activity_detail['kebijakan_pembatalan_sebelumnya']))
                            @foreach (json_decode($activity_detail['kebijakan_pembatalan_sebelumnya']) as $key => $kps)
                            <p class="py-3">Pembatalan <strong>{{ $kps }} hari</strong> sampai
                                <strong>{{ json_decode($activity_detail['kebijakan_pembatalan_sesudah'])[$key] }}
                                    hari</strong>
                                sebelumnya potongan
                                <strong>{{ json_decode($activity_detail['kebijakan_pembatalan_potongan'])[$key]
                                    }}%.</strong>
                            </p>
                            @endforeach
                            {{-- {!! $activity_detail['kebijakan_pembatalan_sebelumnya'] !!} --}}
                            @else
                            <p class="text-base sm:text-lg md:text-xl">Tidak ada kebijakan pembatalan.</p>
                            @endif
                        </div>
                    </div>

                    {{-- Sering Ditanyakan (FAQ) --}}
                    <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                        x-data="{ open: false }">
                        <div class="flex justify-between" :class=" { 'pt-3 pb-2': open, 'py-3': !open }">
                            <button
                                class="text-lg sm:text-xl md:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open">Sering Ditanyakan (FAQ)
                            </button>
                            <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        <div class="py-5 px-3 text-[#4F4F4F] text-base sm:text-lg md:text-xl" x-show="open"
                            x-transition>
                            @if (isset($activity_detail['faq']))
                            {!! $activity_detail['faq'] !!}
                            @else
                            <p class="text-base sm:text-lg md:text-xl">Tidak ada FAQ.</p>
                            @endif
                        </div>
                    </div>

                    {{-- Ulasan --}}
                    <div id="ulasan" class="my-5">
                        <div class="container mx-auto">
                            @if(isset($reviews->star_rating))
                            <div class="container">
                                <div class="row">
                                    <div class="col mt-4">
                                        <p class="font-weight-bold ">Review</p>
                                        <div class="form-group row">
                                            <input type="hidden" name="detailObjek_id" value="{{ $reviews->id }}">
                                            <div class="col">
                                                <div class="rated">
                                                    @for($i=1; $i<=$reviews->star_rating; $i++)
                                                        <input type="radio" id="star{{$i}}" class="rate" name="rating"
                                                            value="5" />
                                                        <label class="star-rating-complete" title="text">{{$i}}
                                                            stars</label>
                                                        @endfor
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row mt-4">
                                            <div class="col">
                                                <p>{{ $reviews->comments }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="container">
                                <div class="row">
                                </div>
                            </div>
                            <div>
                                <div class="container max-w-6xl p-4 ">

                                    <div class="flex mb-4">
                                        <div class="col mt-4 w-full">
                                            <form class="py-2 px-4 w-full" action="{{route('reviewProduct.store')}}"
                                                style="box-shadow: 0 0 10px 0 #ddd;" method="POST" autocomplete="off" enctype="multipart/form-data" id="form-activity-private">
                                                @csrf
                                                <p class="font-weight-bold ">Ulasan</p>
                                                <div class="form-group row">
                                                    <input type="hidden" name="product_id" value="{{ $activity->id }}">
                                                    <div class="col">
                                                        <div class="rate">
                                                            <input type="radio" id="star5" class="rate" name="rating"
                                                                value="5" />
                                                            <label for="star5" title="text">5 stars</label>
                                                            <input type="radio" checked id="star4" class="rate"
                                                                name="rating" value="4" />
                                                            <label for="star4" title="text">4 stars</label>
                                                            <input type="radio" id="star3" class="rate" name="rating"
                                                                value="3" />
                                                            <label for="star3" title="text">3 stars</label>
                                                            <input type="radio" id="star2" class="rate" name="rating"
                                                                value="2">
                                                            <label for="star2" title="text">2 stars</label>
                                                            <input type="radio" id="star1" class="rate" name="rating"
                                                                value="1" />
                                                            <label for="star1" title="text">1 star</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row mt-4" x-data="imageUpload">
                                                    <div class="col">
                                                        <textarea
                                                            class=" form-control w-full flex-auto block p-4 font-medium border border-transparent rounded-lg outline-none focus:border-[#9E3D64] focus:text-green-500"
                                                            name="comment" rows="6 " placeholder="Berikan Ulasan Anda"
                                                            maxlength="200" id="comments"></textarea>
                                                        <input type="file" name="images[]" class="form-control w-full flex-auto block border border-transparent outline-none focus:border-[#9E3D64] focus:text-green-500" accept="image/*" @change="selectedFile" multiple>
                                                        <template x-if="imgDetail.length >= 1">
                                                            <div class="flex justify-start items-center mt-2">
                                                                <template x-for="(detail,index) in imgDetail" :key="index">
                                                                    <div class="flex justify-center items-center">
                                                                        <img :src="detail"
                                                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                                            :alt="'upload'+index">
                                                                        <button type="button"
                                                                            class="absolute mx-2 translate-x-12 -translate-y-14">
                                                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                                                alt="" width="25px" @click="removeImage(index)">
                                                                        </button>
                                                                    </div>
                                                                </template>
                                                            </div>
                                                        </template>    
                                                    </div>
                                                </div>
                                                <div class="mt-3 text-right">
                                                    <button
                                                        class="py-2 px-3 rounded-lg bg-kamtuu-second text-white">Submit
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-2 p-5">
                                        <div class="flex">
                                            <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                class="lg:w-[48px] lg:h-[48px] mt-[0.2rem] mr-1 inline-flex"
                                                alt="rating" title="Rating">
                                            <p class="flex"><span
                                                    class="lg:text-[50px] font-semibold ">{{$ratings}}</span> <span
                                                    class="lg:text-[20px] lg:pt-9"> /5.0 dari {{$var2}} ulasan</span>
                                            </p>
                                        </div>
                                    </div>

                                    {{-- ulasan desktop --}}

                                    @foreach($reviews as $review)
                                    <div class="hidden lg:block md:block">
                                        {{-- <x-produk.card-reviews :review="$review"></x-produk.card-reviews> --}}
                                        <div class="grid grid-row-5 rounded-lg shadow-lg justify-items-start">
                                            {{-- <div class="grid grid-row-5 rounded-lg shadow-lg justify-items-start py-5"> --}}
                                                <div class="grid grid:row-2 grid-cols-10 gap-2 pl-3 mt-7">
                                                    @if(isset($review->profile_photo_path))
                                                    <img class="w-20 h-20 rounded-full bg-gray-400" scr="{{isset($review->profile_photo_path) ? asset($review->profile_photo_path):null}}">
                                                    @else
                                                    <div class="w-20 h-20 rounded-full bg-gray-400 relative">
                                                        @php
                                                            $name = $review->first_name;
                                                            $exp_name = explode(' ',$name);
                                                            $inisial ="";

                                                            $inisial = substr($exp_name[0],0,1);

                                                            if(count($exp_name) > 1){
                                                                $inisial .=substr(end($exp_name),0,1);
                                                            }
                                                        @endphp
                                                        @if(count($exp_name) > 1)
                                                        <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 13px;">{{$inisial}}</p>
                                                        @else
                                                        <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 26px;">{{$inisial}}</p>
                                                        @endif
                                                    </div>
                                                    @endif
                                                    <div class="col-span-2 my-auto">
                                                        <p class="font-semibold text-gray-900">{{$review->first_name}}<p/>
                                                        {{-- Carbon::parse($p->created_at)->diffForHumans(); --}}
                                                        <p class="font-light text-gray-600 text-xs">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}<p/>
                                                    </div>
                                                </div>
                                                <div class="grid grid-cols-5 pl-3 mb-2">
                                                    <div class="col-span-5">
                                                        <img class="w-[20px] h-[20] mt-[0.3rem] mr-1  ml-3 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                                        @for ($i = 0; $i < ($review->star_rating-1); $i++)
                                                            <img class="w-[20px] h-[20] mt-[0.3rem] mr-1 -ml-2 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                                        @endfor
                                                    </div>
                                                </div>
                                                <div class="grid grid-cols-5 space-y-5 text-justify pl-3 pr-5">
                                                    <div class="col-span-5 pl-3">
                                                    <p id="text-ulasan">{{$review->comments}}</p>
                                                    </div>
                                                </div>
                                                <div class="mt-3 pl-3">
                                                    <button type="button" id="show-btn-ulasan" class="px-3 py-2 text-black text-sm underline">Tampilkan</button>
                                                    <button type="button" id="hide-btn-ulasan" class="px-3 py-2 text-black text-sm underline hidden">Sembunyikan</button>
                                                </div>
                                                @if($review->gallery_post)
                                                <div class="flex justify-start justify-items-center pl-4 mb-4">
                                                    @php
                                                        $post_gallery = json_decode($review->gallery_post, true);
                                                        //dump($post_gallery);
                                                    @endphp
                                                    <img class="w-50 h-40 bg-slate-400 rounded-lg" src="{{$post_gallery['result'] != null ? Storage::url('gallery_post/'.$post_gallery['result'][0]):'https://via.placeholder.com/540x540'}}">
                                                    <div class="relative" x-data="{open:false}" @keydown.escape="open = false">
                                                        <button @click="open=true" type="button" id="hide-btn-foto-ulasan" 
                                                            class="absolute px-3 py-2 text-black text-sm underline" style="width:169px;top:110px">
                                                            Tampilkan semua foto
                                                        </button>
                                                        <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                                                                    style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                                                                    <div
                                                                        class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                                                                        <button
                                                                            class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                                                            style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                                                            <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                                                                        </button>
                                                                    </div>
                                                                    <div class="h-full w-full flex items-center justify-center overflow-hidden"
                                                                        x-data="{active: 0, slides: {{ ($review->gallery_post) }} }">
                                                                        <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                                                            <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                                                                <button type="button"
                                                                                    class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                                                    style="background-color: rgba(230, 230, 230, 0.4);"
                                                                                    @click="active = active === 0 ? slides.length - 1 : active - 1">
                                                                                    <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                        @if ($post_gallery)
                                                                            @foreach ($post_gallery['result'] as $index=>$gallery)
                                                                                <div class="h-full w-full flex items-center justify-center absolute">
                                                                                    <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                                                                        x-show="active === {{ $index }}"
                                                                                        x-transition:enter="transition ease-out duration-150"
                                                                                        x-transition:enter-start="opacity-0 transform scale-90"
                                                                                        x-transition:enter-end="opacity-100 transform scale-100"
                                                                                        x-transition:leave="transition ease-in duration-150"
                                                                                        x-transition:leave-start="opacity-100 transform scale-100"
                                                                                        x-transition:leave-end="opacity-0 transform scale-90">

                                                                                        <img src="{{ Storage::url('gallery_post/'.$gallery) }}"
                                                                                            class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                                                                    </div>
                                                                                    <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                                                                        x-show="active === {{ $index }}">
                                                                                        <span class="w-12 text-right" x-text="{{ $index }} + 1"></span>
                                                                                        <span class="w-4 text-center">/</span>
                                                                                        <span class="w-12 text-left"
                                                                                            x-text="{{ count($post_gallery['result']) }}"></span>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        @endif
                                                                        
                                                                        <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                                                            <div class="flex items-center justify-start w-12 md:ml-16">
                                                                                <button type="button"
                                                                                    class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                                                    style="background-color: rgba(230, 230, 230, 0.4);"
                                                                                    @click="active = active === slides.length - 1 ? 0 : active + 1">
                                                                                    <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                    </div>
                                                </div>
                                                @endif
                                            {{-- </div> --}}
                                            {{-- <div class="grid p-5 justify-items-center">
                                                <p class="text-[18px] font-bold">Jhonny Wilson</p>
                                                <p class="text-[18px] -mt-2">{{$review->created_at->format('j F, Y')}}
                                                </p>
                                                <div class="flex p-2">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex pt-3"><span
                                                            class="text-[16px] font-semibold ">{{$review->star_rating}}.0</span>
                                                        <span class="text-[16px]"> /5.0</span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div
                                                class="grid justify-start justify-items-center lg:-ml-[15rem] xl:-ml-[23rem] p-5 md:-ml-[10rem]">
                                                <p class="text-[18px]">{{ $review->comments }}</p>
                                            </div> --}}
                                        </div>
                                    </div>

                                    {{-- ulasan mobile --}}
                                    <div class="block lg:hidden md:hidden">
                                        <div
                                            class="grid grid-cols-1 divide-y rounded-lg shadow-xl justify-items-center border border-gray-300">
                                            <div class="grid p-5 justify-items-center">
                                                <p class="text-sm md:text-[18px] font-bold my-3">Jhonny Wilson</p>
                                                <p class="text-sm md:text-[18px] -mt-2">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}</p>
                                                <div class="flex p-2">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex pt-3"><span
                                                            class="text-[16px] font-semibold ">{{$review->star_rating}}</span>
                                                        <span class="text-[16px]"> /5.0</span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="grid justify-start justify-items-center p-5">
                                                <p class="text-sm md:text-[18px]"> {{ $review->comments }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                    {{-- Anda Mungkin Suka --}}
                    <div>
                        <div class="sm:flex justify-between">
                            <p class="text-2xl sm:text-3xl font-bold text-[#D50006]">Anda Mungkin Suka</p>
                            <a href="{{ route('activity.index') }}" class="my-2 text-xl font-bold text-[#9E3D64]">Lihat
                                semua</a>
                        </div>
                        <div class="my-2 sm:my-5 swiper carousel-tur-swiper">
                            <div class="mx-auto py-5 swiper-wrapper">
                                @if (json_decode($card_activity) != null)
                                @foreach (json_decode($card_activity) as $card)
                                <div class="flex justify-center swiper-slide">
                                    <x-produk.card-populer-home :act="$card" :type="'tur'" />
                                </div>
                                @endforeach
                                @else
                                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                                    Produk tidak ditemukan
                                </div>
                                @endif
                            </div>
                            <div class="-mx-2 swiper-button-next rounded-full bg-white hover:border hover:-translate-x-1 hover:shadow-md duration-200 p-7 border border-[#9E3D64]"
                                style="--swiper-navigation-color: #9E3D64; --swiper-pagination-color: #9E3D64"></div>
                            <div class="-mx-2 swiper-button-prev rounded-full bg-white hover:border hover:-translate-x-1 hover:shadow-md duration-200 p-7 border border-[#9E3D64]"
                                style="--swiper-navigation-color: #9E3D64; --swiper-pagination-color: #9E3D64"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    @livewireScripts
    <script>
        var carouselTurSwiper = new Swiper(".carousel-tur-swiper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            breakpoints: {
                768: {
                    slidesPerView: 3,
                },
                1280: {
                    slidesPerView: 5,
                },
            }
        });

        window.addEventListener('content-updated', event => {
            window.mySwiper = new Swiper(".mySwiper", {
                loop: true,
                spaceBetween: 10,
                slidesPerView: 5,
                freeMode: true,
                loopedSlides: 5,
                watchSlidesProgress: true,
                watchSlidesVisibility: true,
                // slideToClickedSlide: true,
            });

            window.mySwiper2 = new Swiper(".mySwiper2", {
                loop: true,
                spaceBetween: 10,
                loopedSlides: 5,
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev"
                },
                thumbs: {
                    swiper: mySwiper
                }
            });
        })
    </script>
    <script>
        @if(!empty($diskon['tgl_start'][0]) && !empty($diskon['tgl_end'][0]))
            var tgl_start = JSON.parse("{{ $diskon['tgl_start'] }}".replace(/&quot;/g,'"'));
            var tgl_end = JSON.parse("{{ $diskon['tgl_end'] }}".replace(/&quot;/g,'"'));
            var disabledDates = [];
            var jml_tgl = tgl_start.length;
            var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            for(var i = 0; i < jml_tgl; i++) {
                var start = new Date(tgl_start[i]);
                var end = new Date(tgl_end[i]);
                var date = new Date(start);
                while (date <= end) {
                    disabledDates.push(date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2));
                    date.setDate(date.getDate() + 1);
                }
            }

            $('#activity_date').datepicker({
                iconsLibrary: 'fontawesome',
                disableDates: function (date) {
                    var formattedDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                    return ($.inArray(formattedDate, disabledDates) != -1);
                },
                minDate: today
            });
        @endif
    </script>
    <script>
        $(document).ready(function() {
            var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            $('#activity_date').datepicker({
                minDate: today
            });
        })

        $(document).ready(function(){
            
            function submitValidate(e, msg){
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'error',
                        title: msg
                    })
                e.preventDefault()
            }

            $('#form-activity-private').on("submit",function(e){
                let role="{{auth()->user() != null ? auth()->user()->role:null}}";
                if(!role){
                    submitValidate(e, 'Silahkan login terlebih dahulu')
                }
                
                if(role!='traveller'){
                        submitValidate(e, 'Silahkan login sebagai traveler terlebih dahulu')
                }

                if($("#comments").val() == null){
                    submitValidate(e, 'Isi komentar tidak kosong')
                }
                
                //e.preventDefault();
            })
        })

        function imageUpload(){
            return{
                imageUrl:'',
                imgDetail:[],
                selectedFile(event){
                    this.fileToUrl(event)
                },
                fileToUrl(event){
                    if (!event.target.files.length) return
                    let file = event.target.files
                    
                    for (let i = 0; i < file.length; i++) {

                        let reader = new FileReader();
                        let srcImg = ''
                        this.imgDetail = []

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.imgDetail = [...this.imgDetail, srcImg]
                        };
                    }

                    //this.fileUpload();
                },
                removeImage(index){
                    this.imgDetail.splice(index, 1)
                },
                fileUpload(){
                    let fd = new FormData()
                    
                    console.log(file)

                    for(const file of this.imgDetail){
                        console.log(file)
                        /*
                        if(typeof file === 'object'){
                            fd.append('gallery_post[]',file, file.name)
                        }
                        if(typeof file === 'string'){
                            fd.append('seved_post_gallery',file)
                        }
                        */
                    }

                    /*
                    $.ajaxSetup({
                        headers:{
                            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        method:'POST',
                        url:"{{route('gallery-post')}}",
                        data:fd,
                        processData:false,
                        contentType:false,
                        beforeSend:function(){

                        },
                        success:function(msg){
                            console.log(msg)
                        },
                        error:function(data){

                        }
                    })
                    */
                }
            }
        }

        function paket() {
            return {
                nama_pakets: {!! $pakets['nama_paket'] !!},
                jam_pakets: {!! $pakets['jam_paket'] !!},
                max_peserta: {!! $pakets['max_peserta'] !!},
                min_peserta: {!! $pakets['min_peserta'] !!},
                current_index: 0,
                activity_date: '',
                deleteAll() {
                    this.current_index = 0
                    this.activity_date = $('#activity_date').datepicker({
                        value: ''
                    });
                    this.activity_date = ''
                    Alpine.store("detail").dewasa_count = 1
                    Alpine.store("detail").anak_count = 0
                    Alpine.store("detail").balita_count = 0

                },
                jamChange() {
                    Alpine.store("detail").current_min_peserta = this.min_peserta[this.current_index]
                    Alpine.store("detail").current_max_peserta = this.max_peserta[this.current_index]
                    
                    this.$watch(
                        'current_index',
                        () => {
                            Alpine.store("detail").current_min_peserta = this.min_peserta[this.current_index]
                            Alpine.store("detail").current_max_peserta = this.max_peserta[this.current_index]
                        })
                },
            }
        }

        $('#paket').on('submit', function(e) {
            e.preventDefault();
            let activity_date_error = false
            let min_max_peserta_error = false

            let activity_date = $('#activity_date').datepicker().value()
            e.preventDefault()

            if(activity_date === ''){
                document.getElementById("activity_date_error").classList.remove("hidden");
                document.getElementById("activity_date_error").classList.add("flex");
                let activity_date_error = true
            } else {
                document.getElementById("activity_date_error").classList.remove("flex");
                document.getElementById("activity_date_error").classList.add("hidden");
                let activity_date_error = false
            }

            if(Alpine.store("detail").total_count < Alpine.store("detail").current_min_peserta || Alpine.store("detail").total_count > Alpine.store("detail").current_max_peserta){
                document.getElementById("min_max_peserta_error").classList.remove("hidden");
                document.getElementById("min_max_peserta_error").classList.add("flex");
                min_max_peserta_error = true
            } else {
                document.getElementById("min_max_peserta_error").classList.remove("flex");
                document.getElementById("min_max_peserta_error").classList.add("hidden");
                min_max_peserta_error = false
            }

            if(activity_date !== '' && Alpine.store("detail").total_count >= Alpine.store("detail").current_min_peserta && Alpine.store("detail").total_count <= Alpine.store("detail").current_max_peserta){
                activity_date_error = false
                min_max_peserta_error = false
            }

            if (activity_date !== '' && min_max_peserta_error !== '' && !activity_date_error && !min_max_peserta_error) {
                this.submit();
            }
        })


        document.addEventListener("alpine:init", () => {
            Alpine.store("detail", {
                total_price: '-',
                dewasa_count: 0,
                anak_count: 0,
                balita_count: 0,
                dewasa_price: {!! $harga['dewasa_residen'] <= $harga['dewasa_non_residen']
                    ? $harga['dewasa_residen']
                    : $harga['dewasa_non_residen'] !!},
                anak_price: {!! $harga['anak_residen'] <= $harga['anak_non_residen'] ? $harga['anak_residen'] : $harga['anak_non_residen'] !!},
                balita_price: {!! $harga['balita_residen'] <= $harga['balita_non_residen']
                    ? $harga['balita_residen']
                    : $harga['balita_non_residen'] !!},
                total_count: 0,
                current_min_peserta: 0,
                current_max_peserta: 0
            });
        });


        function price() {
            return {
                sumTotal() {
                    this.$watch(
                        'Alpine.store("detail").dewasa_count; Alpine.store("detail").anak_count; Alpine.store("detail").balita_count',
                        () => {
                            Alpine.store("detail").total_price = Alpine.store("detail").dewasa_count * Alpine.store(
                                    "detail").dewasa_price + Alpine.store("detail").anak_count * Alpine.store("detail")
                                .anak_price + Alpine.store("detail").balita_count * Alpine.store("detail").balita_price
                            Alpine.store("detail").total_count = Alpine.store("detail").dewasa_count + Alpine.store(
                                "detail").anak_count + Alpine.store("detail").balita_count
                        })
                },
            }
        }
    </script>
</body>

</html>