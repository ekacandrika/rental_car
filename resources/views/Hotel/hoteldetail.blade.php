<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kamtuu Hotel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap" />
    <script src="https://cdn.tailwindcss.com"></script>

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        /* Hide scrollbar for Chrome, Safari and Opera */
        .no-scrollbar::-webkit-scrollbar {
            display: none;
        }

        /* Hide scrollbar for IE, Edge and Firefox */
        .no-scrollbar {
            -ms-overflow-style: none;
            /* IE and Edge */
            scrollbar-width: none;
            /* Firefox */
        }

        .swiper-pagination-bullet {
            background: rgb(173, 151, 151);
            opacity: 1;
        }

        .swiper-pagination-bullet-active {
            background: white;
        }

        .leftcolumn {
            float: left;
            width: 25%;
            box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
        }

        /* Right column */
        .rightcolumn {
            float: left;
            width: 72%;
            margin-left: 20px;
            box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
        }

        .card {
            background-color: white;
            padding: 20px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        @media screen and (max-width: 800px) {

            .leftcolumn,
            .rightcolumn {
                width: 100%;
                padding: 0;
            }
        }

        .kiri {
            float: left;
            display: block;
            width: 200px;
        }

        .kanan {
            float: right;
            display: block;
            width: 200px;
        }
    </style>
    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border-b border-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Image Detail --}}
    <div class="container mx-auto my-5 sm:rounded-lg swiper carousel-banner">
        <!-- Image gird -->
        <div class="grid gap-5">
            <table>
                <tr>
                    <td>
                        <img class="w-full h-full object-cover cursor-pointer"
                            src="{{ asset('storage') }}/img/image-1.png" alt="Flower"
                            onclick="showModal('{{ asset('storage') }}/img/image-1.png')" />
                    </td>
                    <td class="grid grid-cols-2 gap-1">
                        <img class="w-full h-full object-cover cursor-pointer" style="max-width: 360px"
                            src="{{ asset('storage') }}/img/image-1.png" alt="Flower"
                            onclick="showModal('{{ asset('storage') }}/img/image-1.png')" />
                        <img class="w-full h-full object-cover cursor-pointer" style="max-width: 360px"
                            src="{{ asset('storage') }}/img/image-1.png" alt="Flower"
                            onclick="showModal('{{ asset('storage') }}/img/image-1.png')" />
                        <img class="w-full h-full object-cover cursor-pointer" style="max-width: 360px"
                            src="{{ asset('storage') }}/img/image-1.png" alt="Flower"
                            onclick="showModal('{{ asset('storage') }}/img/image-1.png')" />
                        <img class="w-full h-full object-cover cursor-pointer" style="max-width: 360px"
                            src="{{ asset('storage') }}/img/image-1.png" alt="Flower"
                            onclick="showModal('{{ asset('storage') }}/img/image-1.png')" />
                    </td>
                </tr>
            </table>
        </div>

        <!-- The Modal -->
        <div id="modal"
            class="hidden fixed top-0 left-0 z-80 w-screen h-screen bg-black/70 flex justify-center items-center">
            <!-- The close button -->
            <a class="fixed z-90 top-6 right-8 text-white text-5xl font-bold" href="javascript:void(0)"
                onclick="closeModal()">&times;</a>
            <!-- A big image will be displayed here -->
            <img id="modal-img" class="max-w-[800px] max-h-[600px] object-cover" />
        </div>
    </div>

    <div class="container mx-auto my-5 p-5 rounded-lg block md:flex">
        <div
            class="text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700">
            <ul class="flex flex-wrap -mb-px">
                <li class="mr-2">
                    <a href="#"
                        class="inline-block p-4 text-[#9E3D64] rounded-t-lg border-b-2 border-[#9E3D64] active dark:text-[#9E3D64] dark:border-[#9E3D64]"
                        aria-current="page">Ringkasan</a>
                </li>
                <li class="mr-2">
                    <a href="#"
                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300">Kamar</a>
                </li>
                <li class="mr-2">
                    <a href="#"
                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300">Lokasi</a>
                </li>
                <li class="mr-2">
                    <a href="#"
                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300">Kebijakan</a>
                <li class="mr-2">
                    <a href="#"
                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300">Fasilitas</a>
                </li>
                <li class="mr-2">
                    <a href="#"
                        class="inline-block p-4 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300">Ulasan</a>
                </li>
            </ul>
        </div>
    </div>
    {{-- Content Detail --}}
    <div class="container mx-auto pl-5 rounded-lg block md:flex">
        <p class="text-left text-3xl xl:text-3xl">Hotel Cavinton Yogyakarta</p>
    </div>
    <div class="container mx-auto pl-5">
        <div class="flex justify items-center">
            <img src="{{ asset('storage') }}/img/icon/location-dot-solid-1.svg" alt="" style="max-width: 1%">
            &nbsp;
            <p class="text-gray-700 text-base">
                Jl. Letjen Suprapto No.1, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta
            </p>
        </div>
    </div>
    <div class="container mx-auto p-5 rounded-lg block md:flex">
        <p class="text-left text-1xl xl:text-1xl"><strong>Rating 4.7 </strong>(12 Ulasan)</p>
    </div>
    <div class="container mx-auto p-5 rounded-lg block md:flex">
        <p class="text-left text-1xl xl:text-1xl"><strong>Fasilitas Populer</strong></p>
    </div>
    <div class="container mx-auto pl-5">
        <table>
            <tr>
                <td>
                    <div class="flex justify items-center">
                        <img src="{{ asset('storage') }}/img/icon/person-swimming-solid.svg" alt=""
                            style="max-width: 20px">
                        &nbsp;
                        <p class="text-gray-700 text-base">
                            Kolam Renang
                        </p>
                    </div>
                </td>
                <td>
                    <div class="flex justify items-center">
                        <img src="{{ asset('storage') }}/img/icon/wifi-solid.svg" alt=""
                            style="max-width: 20px" class="ml-5">
                        &nbsp;
                        <p class="text-gray-700 text-base">
                            Free Wifi
                        </p>
                    </div>
                </td>
                <td>
                    <div class="flex justify items-center">
                        <img src="{{ asset('storage') }}/img/icon/wifi-solid.svg" alt=""
                            style="max-width: 20px" class="ml-5">
                        &nbsp;
                        <p class="text-gray-700 text-base">
                            Fasilitas
                        </p>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="flex justify items-center">
                        <img src="{{ asset('storage') }}/img/icon/snowflake-solid.svg" alt=""
                            style="max-width: 20px">
                        &nbsp;
                        <p class="text-gray-700 text-base">
                            AC
                        </p>
                    </div>
                </td>
                <td>
                    <div class="flex justify items-center">
                        <img src="{{ asset('storage') }}/img/icon/spa-solid.svg" alt=""
                            style="max-width: 20px" class="ml-5">
                        &nbsp;
                        <p class="text-gray-700 text-base">
                            SPA
                        </p>
                    </div>
                </td>
                <td>
                    <div class="flex justify items-center">
                        <img src="{{ asset('storage') }}/img/icon/wifi-solid.svg" alt=""
                            style="max-width: 20px" class="ml-5">
                        &nbsp;
                        <p class="text-gray-700 text-base">
                            Fasilitas
                        </p>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="flex justify items-center">
                        <img src="{{ asset('storage') }}/img/icon/square-parking-solid.svg" alt=""
                            style="max-width: 20px">
                        &nbsp;
                        <p class="text-gray-700 text-base">
                            Parkir Area
                        </p>
                    </div>
                </td>
                <td>
                    <div class="flex justify items-center">
                        <img src="{{ asset('storage') }}/img/icon/wifi-solid.svg" alt=""
                            style="max-width: 20px" class="ml-5">
                        &nbsp;
                        <p class="text-gray-700 text-base">
                            Fasilitas
                        </p>
                    </div>
                </td>
                <td>
                    <div class="flex justify items-center">
                        <img src="{{ asset('storage') }}/img/icon/wifi-solid.svg" alt=""
                            style="max-width: 20px" class="ml-5">
                        &nbsp;
                        <p class="text-gray-700 text-base">
                            Fasilitas
                        </p>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="container mx-auto p-5 rounded-lg block md:flex">
        <p class="text-left text-1xl xl:text-1xl"><strong>Deskripsi Hotel</strong></p>
    </div>
    <div class="container mx-auto pl-5 rounded-lg block md:flex">
        <p class="text-left text-1xl xl:text-1xl">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis
            tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit
            sed risus. Maecenas eget condimentum velit, sit amet feugiat lectus. Class aptent taciti sociosqu ad litora
            torquent per conubia nostra, per inceptos himenaeos. Praesent auctor purus luctus enim egestas, ac
            scelerisque ante pulvinar. Donec ut rhoncus ex. Suspendisse ac rhoncus nisl, eu tempor urna. Curabitur vel
            bibendum lorem. Morbi convallis convallis diam sit amet lacinia. Aliquam in elementum tellus.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis
            tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit
            sed risus. Maecenas eget condimentum velit, sit amet feugiat lectus. Class aptent taciti sociosqu ad litora
            torquent per conubia nostra, per inceptos himenaeos. Praesent auctor purus luctus enim egestas, ac
            scelerisque ante pulvinar. Donec ut rhoncus ex. Suspendisse ac rhoncus nisl, eu tempor urna. Curabitur vel
            bibendum lorem. Morbi convallis convallis diam sit amet lacinia. Aliquam in elementum tellus.
        </p>
    </div>
    <div class="container mx-auto p-5 rounded-lg block md:flex">
        <p class="text-left text-1xl xl:text-1xl"><strong>Amenitas Kamar</strong></p>
    </div>
    <div class="container mx-auto pl-5 rounded-lg block md:flex">
        <table class="w-full text-sm text-left">
            <thead>
                <th>
                    <tr>
                        <td>
                            <img src="{{ asset('storage') }}/img/icon/bed-solid.svg" alt=""
                                style="max-width: 20px">
                        </td>
                        <td class="text-[#9E3D64]">
                            <p class="text-left text-1xl xl:text-1xl"><strong>Kamar Tidur</strong></p>
                        </td>
                        <td>
                            <img src="{{ asset('storage') }}/img/icon/utensils-solid.svg" alt=""
                                style="max-width: 20px">
                        </td>
                        <td class="text-[#9E3D64]">
                            <p class="text-left text-1xl xl:text-1xl"><strong>Makan dan Minum</strong></p>
                        </td>
                        <td>
                            <img src="{{ asset('storage') }}/img/icon/shower-solid.svg" alt=""
                                style="max-width: 20px">
                        </td>
                        <td class="text-[#9E3D64]">
                            <p class="text-left text-1xl xl:text-1xl"><strong>Kamar Mandi</strong></p>
                        </td>
                        <td>
                            <img src="{{ asset('storage') }}/img/icon/tv-solid-1.svg" alt=""
                                style="max-width: 20px">
                        </td>
                        <td class="text-[#9E3D64]">
                            <p class="text-left text-1xl xl:text-1xl"><strong>Hiburan</strong></p>
                        </td>
                        <td>
                            <img src="{{ asset('storage') }}/img/icon/check-solid-1.svg" alt=""
                                style="max-width: 20px">
                        </td>
                        <td class="text-[#9E3D64]">
                            <p class="text-left text-1xl xl:text-1xl"><strong>Lain-Lain</strong></p>
                        </td>
                    </tr>
                </th>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">AC</p>
                    </td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Pembuat Kopi/Teh</p>
                    </td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Jubah Mandi</p>
                    </td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">TV Led 40-inci Smart TV</p>
                    </td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Akses Melalui Koridor Luar</p>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Sprei Linen</p>
                    </td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Air Minum Kemasan Gratis</p>
                    </td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Perlengkapan Mandi Gratis</p>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Tersedia Kamar Terhubung</p>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Tirai Kedap Cahaya</p>
                    </td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Kulkas</p>
                    </td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Pengering Rambut</p>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Meja Rias</p>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Tempat Tidur Bayi Gratis</p>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Kamar Mandi Pribadi</p>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Kulkas Mini</p>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Shower</p>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Sandal</p>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Handuk</p>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Kedap Suara</p>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <p class="text-left text-1xl xl:text-1xl">Setrika/Meja Setrika</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    {{-- Search Bar --}}
    {{-- <div class="container mx-auto  my-5">
        <p class="text-4xl font-extrabold text-center">Reserved for Search Bar</p>
    </div> --}}

    <div class="container mx-auto p-5 rounded-lg block md:flex">
        <p class="text-left text-2xl xl:text-2xl"><strong>Pilih Kamar Anda</strong></p>
    </div>
    <x-hotel.app-hotel>
        <select name="" id="" style="background-image: none;"
            class="ml-11 px-4 py-2 border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
            <option value="">Destinasi</option>
            @foreach ($hotel as $value)
                @php
                    $regency = App\Models\Regency::where('id', $value->productdetail->regency_id)->get();
                @endphp
                @foreach ($regency as $item)
                    <option style="font-size: 10px" value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            @endforeach
        </select>
    </x-hotel.app-hotel>

    {{-- slider --}}
    <div class="container mx-auto my-5 p-5 rounded-lg block md:flex">

    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>

    <script>
        var bannerSwiper = new Swiper(".carousel-banner", {
            loop: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
                enabled: false,
            },
            autoplay: {
                delay: 3000,
                pauseOnMouseEnter: true,
                disableOnInteraction: false,
            },
            breakpoints: {
                640: {
                    pagination: {
                        enabled: true
                    }
                }
            }
        });

        // Get the modal by id
        var modal = document.getElementById("modal");

        // Get the modal image tag
        var modalImg = document.getElementById("modal-img");

        // this function is called when a small image is clicked
        function showModal(src) {
            modal.classList.remove('hidden');
            modalImg.src = src;
        }

        // this function is called when the close button is clicked
        function closeModal() {
            modal.classList.add('hidden');
        }
    </script>

</body>

</html>
