<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kamtuu Hotel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap" />
    <link rel="stylesheet" href="https://unpkg.com/flowbite@1.5.3/dist/flowbite.min.css" />
    <script src="https://cdn.tailwindcss.com"></script>

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        /* Hide scrollbar for Chrome, Safari and Opera */
        .no-scrollbar::-webkit-scrollbar {
            display: none;
        }

        /* Hide scrollbar for IE, Edge and Firefox */
        .no-scrollbar {
            -ms-overflow-style: none;
            /* IE and Edge */
            scrollbar-width: none;
            /* Firefox */
        }

        .swiper-pagination-bullet {
            background: rgb(173, 151, 151);
            opacity: 1;
        }

        .swiper-pagination-bullet-active {
            background: white;
        }
    </style>
    <style>
        .slide-container {
            max-width: 1120px;
            width: 100%;
            padding: 40px 0;
        }

        .slide-content {
            margin: 0 40px;
            overflow: hidden;
            border-radius: 25px;
        }

        .card {
            border-radius: 25px;
            background-color: #FFF;
        }

        .image-content,
        .card-content {
            display: flex;
            flex-direction: column;
            align-items: center;
            padding: 10px 14px;
        }

        .image-content {
            position: relative;
            row-gap: 5px;
            padding: 25px 0;
        }

        .overlay {
            position: absolute;
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
            background-color: #4070F4;
            border-radius: 25px 25px 0 25px;
        }

        .overlay::before,
        .overlay::after {
            content: '';
            position: absolute;
            right: 0;
            bottom: -40px;
            height: 40px;
            width: 40px;
            background-color: #4070F4;
        }

        .overlay::after {
            border-radius: 0 25px 0 0;
            background-color: #FFF;
        }

        .card-image {
            position: relative;
            height: 150px;
            width: 150px;
            border-radius: 50%;
            background: #FFF;
            padding: 3px;
        }

        .card-image .card-img {
            height: 100%;
            width: 100%;
            object-fit: cover;
            border-radius: 50%;
            border: 4px solid #4070F4;
        }

        .name {
            font-size: 18px;
            font-weight: 500;
            color: #333;
        }

        .description {
            font-size: 14px;
            color: #707070;
            text-align: center;
        }

        .button {
            border: none;
            font-size: 16px;
            color: #FFF;
            padding: 8px 16px;
            background-color: #4070F4;
            border-radius: 6px;
            margin: 14px;
            cursor: pointer;
            transition: all 0.3s ease;
        }

        .button:hover {
            background: #265DF2;
        }

        .swiper-navBtn {
            color: #6E93f7;
            transition: color 0.3s ease;
        }

        .swiper-navBtn:hover {
            color: #4070F4;
        }

        .swiper-navBtn::before,
        .swiper-navBtn::after {
            font-size: 35px;
        }

        .swiper-button-next {
            right: 0;
        }

        .swiper-button-prev {
            left: 0;
        }

        .swiper-pagination-bullet {
            background-color: #6E93f7;
            opacity: 1;
        }

        .swiper-pagination-bullet-active {
            background-color: #4070F4;
        }

        @media screen and (max-width: 768px) {
            .slide-content {
                margin: 0 10px;
            }

            .swiper-navBtn {
                display: none;
            }
        }
    </style>
    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border-b border-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Carousel --}}
    <div class="container mx-auto my-5 sm:rounded-lg swiper carousel-banner">
        <div
            class="swiper-wrapper w-[380px] sm:w-[768px] h-[192px] md:w-[1024px] md:h-[256px] lg:w-[1200px] lg:h-[300px] xl:w-[1536px] xl:h-[384px]">
            <a href="#" class="swiper-slide">
                <img class="w-full h-full px-5 sm:px-0 object-center object-cover sm:rounded-lg"
                    src="https://images.unsplash.com/photo-1501785888041-af3ef285b470?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-1">
            </a>
            <a href="#" class="swiper-slide">
                <img class="w-full h-full px-5 sm:px-0 object-center object-cover rounded-lg"
                    src="https://images.unsplash.com/photo-1433838552652-f9a46b332c40?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-2">
            </a>
            <a href="#" class="swiper-slide">
                <img class="w-full h-full px-5 sm:px-0 object-center object-cover rounded-lg"
                    src="https://images.unsplash.com/photo-1518548419970-58e3b4079ab2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-3">
            </a>
            <a href="#" class="swiper-slide">
                <img class="w-full h-full px-5 sm:px-0 object-center object-cover rounded-lg"
                    src="https://images.unsplash.com/photo-1555899434-94d1368aa7af?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-4">
            </a>
        </div>
        <div class=" pl-3 text-left swiper-pagination"></div>
    </div>


    {{-- Search Bar --}}
    {{-- <div class="container mx-auto  my-5">
        <p class="text-4xl font-extrabold text-center">Reserved for Search Bar</p>
    </div> --}}
    <x-hotel.app-hotel></x-hotel.app-hotel>

    {{-- Kamtuu About #1 --}}
    <div class="my-auto p-4 grid justify-items-center">
        <p class="text-center text-3xl xl:text-4xl font-semibold">Cara mudah cari hotel di Indonesia, cari di Kamtuu!
        </p>
    </div>
    <div class="container mx-auto my-5 p-5 rounded-lg block md:flex">
        {{-- Desktop --}}
        <table class="table-auto text-center xl:text-justify">
            <thead>
                <tr>
                    <th><img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/img/people_1.png') }}" alt="people_1">
                    </th>
                    <th><img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/img/people_1.png') }}" alt="people_1">
                    </th>
                    <th><img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/img/people_1.png') }}" alt="people_1">
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="align-middle xl:align-bottom px-4">
                        <p class="text-lg lg:text-xl text-center xl:text-center font-semibold">Banyak
                            Pilihan
                        </p>
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <p class="text-lg lg:text-xl text-center xl:text-center font-semibold">Banyak
                            Pilihan</p>
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <p class="text-lg lg:text-xl text-center xl:text-center font-semibold">Banyak
                            Pilihan</p>
                    </td>
                </tr>
                <tr>
                    <td class="align-top px-4">
                        <p class="text-sm lg:text-base text-center xl:text-center font-medium my-2">Lorem Ipsum is
                            simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </td>
                    <td class="align-top px-4">
                        <p class="text-sm lg:text-base text-center xl:text-center font-medium my-2">Lorem Ipsum is
                            simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </td>
                    <td class="align-top px-4">
                        <p class="text-sm lg:text-base text-center xl:text-center font-medium my-2">Lorem Ipsum is
                            simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="my-auto p-4 grid justify-items-center">
        <p class="text-center text-3xl xl:text-4xl font-semibold">Cara Memesan
        </p>
    </div>
    <div class="container mx-auto my-5 p-5 rounded-lg block md:flex">
        {{-- Desktop --}}
        <table class="table-auto text-center xl:text-justify">
            <thead>
                <tr>
                    <th><img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/img/Group_109.png') }}"
                            alt="Group_109">
                    </th>
                    <th></th>
                    <th><img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/img/Group_109.png') }}"
                            alt="Group_109">
                    </th>
                    <th></th>
                    <th><img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/img/Group_109.png') }}"
                            alt="Group_109">
                    </th>
                    <th></th>
                    <th><img class="mx-auto w-3/4 md:w-20" src="{{ asset('storage/img/Group_109.png') }}"
                            alt="Group_109">
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="align-middle xl:align-bottom px-4">
                        <p class="text-lg lg:text-xl text-center xl:text-center font-semibold">Banyak
                            Pilihan
                        </p>
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <img class="mx-auto w-3/4 md:w-20"
                            src="{{ asset('storage/img/icon/arrow-right-solid-1.svg') }}" alt="Group_109">
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <p class="text-lg lg:text-xl text-center xl:text-center font-semibold">Banyak
                            Pilihan</p>
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <img class="mx-auto w-3/4 md:w-20"
                            src="{{ asset('storage/img/icon/arrow-right-solid-1.svg') }}" alt="Group_109">
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <p class="text-lg lg:text-xl text-center xl:text-center font-semibold">Banyak
                            Pilihan</p>
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <img class="mx-auto w-3/4 md:w-20"
                            src="{{ asset('storage/img/icon/arrow-right-solid-1.svg') }}" alt="Group_109">
                    </td>
                    <td class="align-middle xl:align-bottom px-4">
                        <p class="text-lg lg:text-xl text-center xl:text-center font-semibold">Banyak
                            Pilihan</p>
                    </td>
                </tr>
                <tr>
                    <td class="align-top px-4">
                        <p class="text-sm lg:text-base text-center xl:text-center font-medium my-2">Lorem Ipsum is
                            simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </td>
                    <td class="align-top px-4"></td>
                    <td class="align-top px-4">
                        <p class="text-sm lg:text-base text-center xl:text-center font-medium my-2">Lorem Ipsum is
                            simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </td>
                    <td class="align-top px-4"></td>
                    <td class="align-top px-4">
                        <p class="text-sm lg:text-base text-center xl:text-center font-medium my-2">Lorem Ipsum is
                            simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </td>
                    <td class="align-top px-4"></td>
                    <td class="align-top px-4">
                        <p class="text-sm lg:text-base text-center xl:text-center font-medium my-2">Lorem Ipsum is
                            simply
                            dummy text of the printing
                            and typesetting industry.</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <hr class="my-10 border-[#F2F2F2]">

    {{-- Official Stores --}}
    <div class="my-auto p-4 grid justify-items-center">
        <p class="text-center text-3xl xl:text-4xl font-semibold">
            Butuh pesan hotel untuk grup besar?<br>Hubungi kami!
        </p>
        <div class="flex justify-center">
            <x-destindonesia.button-primary text="Pesan Hotel Untuk Grup"></x-destindonesia.button-primary>
        </div>
    </div>

    <hr class="my-10 border-[#F2F2F2]">

    <div class="my-auto p-4 grid justify-items-center">
        <p class="text-center text-3xl xl:text-4xl font-semibold">Apa kata mereka?
        </p>
    </div>
    <div class="slide-container swiper">
        <div class="slide-content">
            <div class="card-wrapper swiper-wrapper">
                <div class="card swiper-slide">
                    <div class="image-content">
                        <span class="overlay"></span>

                        <div class="card-image">
                            <!--<img src="images/profile1.jpg" alt="" class="card-img">-->
                        </div>
                    </div>

                    <div class="card-content">
                        <h2 class="name">David Dell</h2>
                        <p class="description">The lorem text the section that contains header with having open
                            functionality. Lorem dolor sit amet consectetur adipisicing elit.</p>

                        <button class="button">View More</button>
                    </div>
                </div>
                <div class="card swiper-slide">
                    <div class="image-content">
                        <span class="overlay"></span>

                        <div class="card-image">
                            <!--<img src="images/profile2.jpg" alt="" class="card-img">-->
                        </div>
                    </div>

                    <div class="card-content">
                        <h2 class="name">David Dell</h2>
                        <p class="description">The lorem text the section that contains header with having open
                            functionality. Lorem dolor sit amet consectetur adipisicing elit.</p>

                        <button class="button">View More</button>
                    </div>
                </div>
                <div class="card swiper-slide">
                    <div class="image-content">
                        <span class="overlay"></span>

                        <div class="card-image">
                            <!--<img src="images/profile3.jpg" alt="" class="card-img">-->
                        </div>
                    </div>

                    <div class="card-content">
                        <h2 class="name">David Dell</h2>
                        <p class="description">The lorem text the section that contains header with having open
                            functionality. Lorem dolor sit amet consectetur adipisicing elit.</p>

                        <button class="button">View More</button>
                    </div>
                </div>
                <div class="card swiper-slide">
                    <div class="image-content">
                        <span class="overlay"></span>

                        <div class="card-image">
                            <!--<img src="images/profile4.jpg" alt="" class="card-img">-->
                        </div>
                    </div>

                    <div class="card-content">
                        <h2 class="name">David Dell</h2>
                        <p class="description">The lorem text the section that contains header with having open
                            functionality. Lorem dolor sit amet consectetur adipisicing elit.</p>

                        <button class="button">View More</button>
                    </div>
                </div>
                <div class="card swiper-slide">
                    <div class="image-content">
                        <span class="overlay"></span>

                        <div class="card-image">
                            <!--<img src="images/profile5.jpg" alt="" class="card-img">-->
                        </div>
                    </div>

                    <div class="card-content">
                        <h2 class="name">David Dell</h2>
                        <p class="description">The lorem text the section that contains header with having open
                            functionality. Lorem dolor sit amet consectetur adipisicing elit.</p>

                        <button class="button">View More</button>
                    </div>
                </div>
                <div class="card swiper-slide">
                    <div class="image-content">
                        <span class="overlay"></span>

                        <div class="card-image">
                            <!--<img src="images/profile6.jpg" alt="" class="card-img">-->
                        </div>
                    </div>

                    <div class="card-content">
                        <h2 class="name">David Dell</h2>
                        <p class="description">The lorem text the section that contains header with having open
                            functionality. Lorem dolor sit amet consectetur adipisicing elit.</p>

                        <button class="button">View More</button>
                    </div>
                </div>
                <div class="card swiper-slide">
                    <div class="image-content">
                        <span class="overlay"></span>

                        <div class="card-image">
                            <!--<img src="images/profile7.jpg" alt="" class="card-img">-->
                        </div>
                    </div>

                    <div class="card-content">
                        <h2 class="name">David Dell</h2>
                        <p class="description">The lorem text the section that contains header with having open
                            functionality. Lorem dolor sit amet consectetur adipisicing elit.</p>

                        <button class="button">View More</button>
                    </div>
                </div>
                <div class="card swiper-slide">
                    <div class="image-content">
                        <span class="overlay"></span>

                        <div class="card-image">
                            <!--<img src="images/profile8.jpg" alt="" class="card-img">-->
                        </div>
                    </div>

                    <div class="card-content">
                        <h2 class="name">David Dell</h2>
                        <p class="description">The lorem text the section that contains header with having open
                            functionality. Lorem dolor sit amet consectetur adipisicing elit.</p>

                        <button class="button">View More</button>
                    </div>
                </div>
                <div class="card swiper-slide">
                    <div class="image-content">
                        <span class="overlay"></span>

                        <div class="card-image">
                            <!--<img src="images/profile9.jpg" alt="" class="card-img">-->
                        </div>
                    </div>

                    <div class="card-content">
                        <h2 class="name">David Dell</h2>
                        <p class="description">The lorem text the section that contains header with having open
                            functionality. Lorem dolor sit amet consectetur adipisicing elit.</p>

                        <button class="button">View More</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="swiper-button-next swiper-navBtn"></div>
        <div class="swiper-button-prev swiper-navBtn"></div>
        <div class="swiper-pagination"></div>
    </div>

    <hr class="my-10 border-[#F2F2F2]">

    <div class="my-auto p-4 grid justify-items-center">
        <p class="text-center text-3xl xl:text-4xl font-semibold">Sering Ditanyakan (FAQ)
        </p>
    </div>
    <div class="container mx-3 sm:mx-auto my-3">
        <div id="accordion-collapse" data-accordion="collapse">
            <h2 id="accordion-collapse-heading-1">
                <button type="button"
                    class="flex items-center justify-between w-full p-5 font-medium text-left border border-b-0 border-gray-200 rounded-t-xl focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 hover:bg-gray-100 dark:hover:bg-gray-800 bg-gray-100 dark:bg-gray-800 text-gray-900 dark:text-white"
                    data-accordion-target="#accordion-collapse-body-1" aria-expanded="true"
                    aria-controls="accordion-collapse-body-1">
                    <span>Lorem Ipsum</span>
                    <svg data-accordion-icon="" class="w-6 h-6 rotate-180 shrink-0" fill="currentColor"
                        viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>
            </h2>
            <div id="accordion-collapse-body-1" class="" aria-labelledby="accordion-collapse-heading-1">
                <div class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                    <p class="mb-2 text-gray-500 dark:text-gray-400">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                        been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                        a galley of type and scrambled it to make a type specimen book.
                    </p>
                </div>
            </div>
            <h2 id="accordion-collapse-heading-2">
                <button type="button"
                    class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                    data-accordion-target="#accordion-collapse-body-2" aria-expanded="false"
                    aria-controls="accordion-collapse-body-2">
                    <span>Lorem Ipsum</span>
                    <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>
            </h2>
            <div id="accordion-collapse-body-2" class="hidden" aria-labelledby="accordion-collapse-heading-2">
                <div class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700">
                    <p class="mb-2 text-gray-500 dark:text-gray-400">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                        been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                        a galley of type and scrambled it to make a type specimen book.
                    </p>
                </div>
            </div>
            <h2 id="accordion-collapse-heading-3">
                <button type="button"
                    class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                    data-accordion-target="#accordion-collapse-body-3" aria-expanded="false"
                    aria-controls="accordion-collapse-body-3">
                    <span>Lorem Ipsum</span>
                    <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>
            </h2>
            <div id="accordion-collapse-body-3" class="hidden" aria-labelledby="accordion-collapse-heading-3">
                <div class="p-5 font-light border border-t-0 border-gray-200 dark:border-gray-700">
                    <p class="mb-2 text-gray-500 dark:text-gray-400">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                        been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                        a galley of type and scrambled it to make a type specimen book.
                    </p>
                </div>
            </div>
            <h2 id="accordion-collapse-heading-3">
                <button type="button"
                    class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                    data-accordion-target="#accordion-collapse-body-3" aria-expanded="false"
                    aria-controls="accordion-collapse-body-3">
                    <span>Lorem Ipsum</span>
                    <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>
            </h2>
            <div id="accordion-collapse-body-3" class="hidden" aria-labelledby="accordion-collapse-heading-3">
                <div class="p-5 font-light border border-t-0 border-gray-200 dark:border-gray-700">
                    <p class="mb-2 text-gray-500 dark:text-gray-400">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                        been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                        a galley of type and scrambled it to make a type specimen book.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script src="https://unpkg.com/flowbite@1.5.3/dist/flowbite.js"></script>
    <script>
        var swiper = new Swiper(".slide-content", {
            slidesPerView: 1,
            spaceBetween: 25,
            loop: true,
            centerSlide: 'true',
            fade: 'true',
            grabCursor: 'true',
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
                dynamicBullets: true,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },

            breakpoints: {
                0: {
                    slidesPerView: 1,
                },
                520: {
                    slidesPerView: 1,
                },
                950: {
                    slidesPerView: 1,
                },
            },
        });
    </script>
    <script>
        var bannerSwiper = new Swiper(".carousel-banner", {
            loop: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
                enabled: false,
            },
            autoplay: {
                delay: 3000,
                pauseOnMouseEnter: true,
                disableOnInteraction: false,
            },
            breakpoints: {
                640: {
                    pagination: {
                        enabled: true
                    }
                }
            }
        });
    </script>

</body>

</html>
