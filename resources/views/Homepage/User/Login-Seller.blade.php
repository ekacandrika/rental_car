<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kamtuu Home') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border-b border-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    <x-homepage.login :role="'seller'">
        @slot('image')
            <img class="absolute inset-0 h-full w-full object-cover"
                src="https://images.pexels.com/photos/6488410/pexels-photo-6488410.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260" />
        @endslot
        @slot('nameUsr')
            Seller
        @endslot
        @slot('linkRegister')
            {{ route('RegisterSeller') }}
        @endslot
    </x-homepage.login>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

</body>

</html>
