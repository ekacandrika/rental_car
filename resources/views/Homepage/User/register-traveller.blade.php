<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kamtuu Home') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border-b border-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    <x-homepage.register :role="'traveller'">
        @slot('image')
        <img class="absolute inset-0 h-full w-full object-cover"
            src="https://images.pexels.com/photos/1271619/pexels-photo-1271619.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2" />
        @endslot
        @slot('nameUsr')
        Traveller
        @endslot
        @slot('linkLogin')
        {{ route('LoginTraveller') }}
        @endslot
        @slot('formRegister')
        <div>
            @if (count($errors) > 0)
            <div class="bg-red-300 rounded-md p-3">
                @foreach ($errors->getMessagebag()->toArray() as $key=>$value)
                @foreach ($value as $index=>$err)
                <p>- {{ $err}}</p>
                @endforeach
                @endforeach
            </div>
            @endif
            <!-- ['Individual', 'Family', 'Group', 'Team'] -->
            <form x-data="{ password: '', password_confirm: '' }" method="POST" action="{{ route('register') }}">
                @csrf
                <input type="hidden" name="role" value="traveller">

                <div class="my-5">
                    <label for="first_name" class="block text-sm font-medium leading-5"> Name </label>

                    <div class="mt-1 rounded-md shadow-sm">
                        <input id="first_name" name="first_name" type="text" required autofocus
                            class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                    </div>
                </div>

                <div class="my-5">
                    <label for="email" class="block text-sm font-medium leading-5"> Email
                        address </label>

                    <div class="mt-1 rounded-md shadow-sm">
                        <input id="email" name="email" type="email" required autofocus
                            class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                    </div>
                </div>

                <div class="my-5">
                    <label for="no_tlp" class="block text-sm font-medium leading-5"> No Telp </label>

                    <div class="mt-1 rounded-md shadow-sm">
                        <input id="no_tlp" name="no_tlp" type="number" required autofocus
                            class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                    </div>
                </div>

                <div class="my-5">
                    <label for="lisensi" class="block text-sm font-medium leading-5"> Jenis Kelamin </label>

                    <div class="mt-1 rounded-md shadow-sm">
                        <select
                            class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                            name="jk" id="lisensi" required autofocus>
                            <option value="Laki-laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                </div>


                <div class="mt-6">
                    <label for="password" class="block text-sm font-medium text-gray-700 leading-5">
                        Password </label>

                    <div class="mt-1 rounded-md shadow-sm">
                        <input x-model="password" id="password" type="password" name="password" required
                            class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                    </div>
                </div>

                <div class="mt-6">
                    <label for="password" class="block text-sm font-medium text-gray-700 leading-5">
                        Re-Enter Password </label>

                    <div class="mt-1 rounded-md shadow-sm">
                        <input x-model="password_confirm" id="password" type="password" name="password_confirmation"
                            required
                            class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                    </div>
                </div>

                <div class="my-5">
                    <!-- Validation Alpine Js -->
                    <div class="flex justify-start mt-3 ml-4 p-1">
                        <ul>
                            <li class="flex items-center py-1">
                                <div :class="{
                                        'bg-green-200 text-green-700': password == password_confirm && password.length >
                                            0,
                                        'bg-red-200 text-red-700': password != password_confirm || password
                                            .length == 0
                                    }" class=" rounded-full p-1 fill-current ">
                                    <svg class="w-4 h-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path x-show="password == password_confirm && password.length > 0"
                                            stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M5 13l4 4L19 7" />
                                        <path x-show="password != password_confirm || password.length == 0"
                                            stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M6 18L18 6M6 6l12 12" />

                                    </svg>
                                </div>
                                <span :class="{
                                            'text-green-700': password == password_confirm && password.length >
                                                0,
                                            'text-red-700': password != password_confirm || password.length == 0
                                        }" class="font-medium text-sm ml-3"
                                    x-text="password == password_confirm && password.length > 0 ? 'Passwords match' : 'Passwords do not match' "></span>
                            </li>
                            <li class="flex items-center py-1">
                                <div :class="{
                                        'bg-green-200 text-green-700': password.length >
                                            7,
                                        'bg-red-200 text-red-700': password.length < 7
                                    }" class=" rounded-full p-1 fill-current ">
                                    <svg class="w-4 h-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path x-show="password.length > 7" stroke-linecap="round"
                                            stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                        <path x-show="password.length < 7" stroke-linecap="round"
                                            stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />

                                    </svg>
                                </div>
                                <span
                                    :class="{ 'text-green-700': password.length > 7, 'text-red-700': password.length < 7 }"
                                    class="font-medium text-sm ml-3"
                                    x-text="password.length > 7 ? 'The minimum length is reached' : 'At least 8 characters required' "></span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="mt-6">
                    <span class="block w-full rounded-md shadow-sm">
                        <x-destindonesia.button-primary>
                            @slot('button')
                            Register
                            @endslot
                        </x-destindonesia.button-primary>
                    </span>
                </div>
            </form>
        </div>
        @endslot
    </x-homepage.register>


    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

</body>

</html>