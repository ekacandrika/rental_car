<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! seo($page ?? null) !!}
    <title>{{ config('app.name', 'Kamtuu Home') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    {{-- select2 --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>

    <script>
        function slider() {
            return {
                slideCount: 4,
                activeIndex: "z-10",
                hideScrollbar: false,
                updateIndex(e) {
                    if (e.scrollLeft < e.clientWidth) {
                        let child = e.childNodes;
                        for (let i = 0; i < child.length; i++) {
                            const element = child[i];
                            this.activeIndex = element.offsetLeft < 0 ? "-z-10" : "z-10";
                            // console.log(i, this.activeIndex, element.offsetLeft);
                        }
                        this.activeIndex = 0;
                    } else {
                        this.activeIndex = Math.round(e.scrollLeft / e.clientWidth);
                    }
                },
            };
        }
    </script>
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <style>
        /* Hide scrollbar for Chrome, Safari and Opera */
        .no-scrollbar::-webkit-scrollbar {
            display: none;
        }

        /* Hide scrollbar for IE, Edge and Firefox */
        .no-scrollbar {
            -ms-overflow-style: none;
            /* IE and Edge */
            scrollbar-width: none;
            /* Firefox */
        }

        .swiper-pagination-bullet {
            background: rgb(173, 151, 151);
            opacity: 1;
        }

        .swiper-pagination-bullet-active {
            background: white;
        }
        .select2-container .select2-selection--single {
            height: 100%;
            padding-top: 0.5rem;
            color:black;
            font-size: 0.875rem; /* 14px */
            /* line-height: 1.25rem; 20px */
            padding-bottom: 0.5rem;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 100%;
        }

        .select2-selection__arrow {
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            /* padding-right: 0.75rem; */
        }
        .select2-container--default .select2-selection--single{
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
        }
        .select2-results__option {
            font-size: 0.875rem;
        }
    </style>
    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border-b border-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Carousel --}}
    <div class="container mx-auto my-5 sm:rounded-lg swiper carousel-banner">
        <div
            class="swiper-wrapper w-[380px] sm:w-[768px] h-[192px] md:w-[1024px] md:h-[256px] lg:w-[1200px] lg:h-[300px] xl:w-[1536px] xl:h-[384px]">
            @if (!empty($home_image_slider))
            @foreach (json_decode($home_image_slider->image_section) as $index => $image)
            <a href="#" class="swiper-slide">
                <img class="object-cover object-center w-full h-full px-5 sm:px-0 sm:rounded-lg"
                    src="{{ asset($image) }}" alt="banner-{{ $index }}">
            </a>
            @endforeach
            @else
            <a href="#" class="swiper-slide">
                <img class="object-cover object-center w-full h-full px-5 sm:px-0 sm:rounded-lg"
                    src="https://images.unsplash.com/photo-1501785888041-af3ef285b470?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-1">
            </a>
            <a href="#" class="swiper-slide">
                <img class="object-cover object-center w-full h-full px-5 rounded-lg sm:px-0"
                    src="https://images.unsplash.com/photo-1433838552652-f9a46b332c40?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-2">
            </a>
            <a href="#" class="swiper-slide">
                <img class="object-cover object-center w-full h-full px-5 rounded-lg sm:px-0"
                    src="https://images.unsplash.com/photo-1518548419970-58e3b4079ab2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-3">
            </a>
            <a href="#" class="swiper-slide">
                <img class="object-cover object-center w-full h-full px-5 rounded-lg sm:px-0"
                    src="https://images.unsplash.com/photo-1555899434-94d1368aa7af?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    alt="banner-4">
            </a>
            @endif
        </div>
        <div class="pl-3 text-left swiper-pagination"></div>
    </div>


    {{-- Search Bar --}}
    {{-- <div class="container mx-auto my-5">
        <p class="text-4xl font-extrabold text-center">Reserved for Search Bar</p>
    </div> --}}
    <x-produk.cari-produk>
        <x-slot name="hotel">
            <select name="search" id="hotel_search_from" required
                class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                <option value="">Dari</option>
                {{-- @foreach ($hotel as $value)
                @php
                $regencyHotel = App\Models\Regency::where('id', $value->productdetail->regency_id)->get();
                @endphp
                @foreach ($regencyHotel as $item)
                <option style="font-size: 10px" value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
                @endforeach --}}
                {{-- @foreach($search_from_to_tour as $lokasi)
                    <option value={{$lokasi->id}}>{{$lokasi->name}}</option>
                    @foreach($lokasi->regencies as $kota)
                        <option value={{$kota->id}}>{{$kota->name}}</option>
                    @endforeach
                @endforeach --}}
                @foreach ($collect_pin as $pin)
                    <option value="{{$pin['regency_id']}}">{{$pin['nama_titik']}}</option>
                @endforeach
            </select>
        </x-slot>
        <x-slot name="xstay">
            <select name="search" id="xstay_search_from" required
                class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                <option value="">Dari</option>
                {{-- @foreach($search_from_to_tour as $lokasi)
                    <option value={{$lokasi->id}}>{{$lokasi->name}}</option>
                    @foreach($lokasi->regencies as $kota)
                        <option value={{$kota->id}}>{{$kota->name}}</option>
                    @endforeach
                @endforeach --}}
                {{-- @foreach ($xstay as $value)
                @php
                $regencyXstay = App\Models\Regency::where('id', $value->productdetail->regency_id)->get();
                @endphp
                @foreach ($regencyXstay as $item)
                <option style="font-size: 10px" value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
                @endforeach --}}
                @foreach ($collect_pin as $pin)
                    <option value="{{$pin['regency_id']}}">{{$pin['nama_titik']}}</option>
                @endforeach
            </select>
        </x-slot>
        <x-slot name="activity">
            <select name="search" id="activity_search_from" required
                class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                {{-- <option value="">Dari</option>
                @foreach($search_from_to_tour as $lokasi)
                    <option value={{$lokasi->id}}>{{$lokasi->name}}</option>
                    @foreach($lokasi->regencies as $kota)
                        <option value={{$kota->id}}>{{$kota->name}}</option>
                    @endforeach
                @endforeach --}}
                {{-- @foreach ($activit as $value)
                @php
                $regencyAct = App\Models\Regency::where('id', $value->productdetail->regency_id)->get();
                @endphp
                @foreach ($regencyAct as $item)
                <option style="font-size: 10px" value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
                @endforeach --}}
                @foreach ($collect_pin as $pin)
                    <option value="{{$pin['regency_id']}}">{{$pin['nama_titik']}}</option>
                @endforeach
            </select>
        </x-slot>
        <x-slot name="turFrom">
            <select name="search" id="tur-from-select2" required
                class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                <option value="" selected>Dari</option>
                {{-- @foreach ($turs as $value)
                @php
                $regencyFrom = App\Models\Regency::where('id', $value->productdetail->regency_id)->get();
                @endphp
                @foreach ($regencyFrom as $item)
                <option style="font-size: 10px" value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
                @endforeach --}}
                {{-- @foreach($search_from_to_tour as $lokasi)
                    <option value={{$lokasi->id}}>{{$lokasi->name}}</option>
                    @foreach($lokasi->regencies as $kota)
                        <option value={{$kota->id}}>{{$kota->name}}</option>
                    @endforeach
                @endforeach --}}
                {{-- @foreach ($location as $provinsi_data)
                <option value="{{ $provinsi_data['provinces_id'] }}" {{isset($add_listing['tag_location_1']) ?
                    ($add_listing['tag_location_1']==$provinsi_data['provinces_id'] ? 'selected' :'') :''}}>{{
                    $provinsi_data['provinces_name'] }}</option>
                @foreach ($provinsi_data['regency'] as $key => $regency)
                <option value="{{ $regency['regencies_id'] }}" {{isset($add_listing['tag_location_1']) ?
                    ($add_listing['tag_location_1']==$regency['regencies_id'] ? 'selected' :'') :''}}>{{
                    $regency['regencies_name'] }}</option>
                @endforeach
                @foreach ($provinsi_data['district'] as $key => $district)
                <option value="{{ $district['districts_id'] }}" {{isset($add_listing['tag_location_1']) ?
                    ($add_listing['tag_location_1']==$district['districts_id'] ? 'selected' :'') :''}}>{{
                    $district['districts_name'] }}</option>
                @endforeach
                @endforeach --}}
                @foreach ($collect_pin as $pin)
                    <option value="{{$pin['regency_id']}}">{{$pin['nama_titik']}}</option>
                @endforeach
            </select>
        </x-slot>
        <x-slot name="turTo">
            <select name="tur_to" id="tur-to-select2" required
                class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                <option value="" seleted>Ke</option>
                {{-- @foreach($search_from_to_tour as $lokasi)
                    <option value={{$lokasi->id}}>{{$lokasi->name}}</option>
                    @foreach($lokasi->regencies as $kota)
                        <option value={{$kota->id}}>{{$kota->name}}</option>
                    @endforeach
                @endforeach --}}
                {{-- @foreach ($regencyTur as $foo)
                <option style="font-size: 10px" value="{{ $foo->id }}">{{ $foo->name }}</option>
                @endforeach --}}
                {{-- @foreach ($location as $provinsi_data)
                <option value="{{ $provinsi_data['provinces_id'] }}" {{isset($add_listing['tag_location_1']) ?
                    ($add_listing['tag_location_1']==$provinsi_data['provinces_id'] ? 'selected' :'') :''}}>{{
                    $provinsi_data['provinces_name'] }}</option>
                @foreach ($provinsi_data['regency'] as $key => $regency)
                <option value="{{ $regency['regencies_id'] }}" {{isset($add_listing['tag_location_1']) ?
                    ($add_listing['tag_location_1']==$regency['regencies_id'] ? 'selected' :'') :''}}>{{
                    $regency['regencies_name'] }}</option>
                @endforeach
                @foreach ($provinsi_data['district'] as $key => $district)
                <option value="{{ $district['districts_id'] }}" {{isset($add_listing['tag_location_1']) ?
                    ($add_listing['tag_location_1']==$district['districts_id'] ? 'selected' :'') :''}}>{{
                    $district['districts_name'] }}</option>
                @endforeach
                @endforeach --}}
                @foreach ($collect_pin as $pin)
                    <option value="{{$pin['regency_id']}}">{{$pin['nama_titik']}}</option>
                @endforeach
            </select>
        </x-slot>
        <x-slot name="rental">
            <select name="search" id="rental_serch_from" required
                class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                <option value="">Lokasi</option>
                @if (isset($rental))
                {{-- @foreach ($rental as $value)
                @php
                $regencyRental = App\Models\Regency::where('id', $value->productdetail->regency_id)->get();
                @endphp
                @foreach ($regencyRental as $item)
                <option style="font-size: 10px" value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
                @endforeach --}}
                {{-- @foreach($search_from_to_tour as $lokasi)
                    <option value={{$lokasi->id}}>{{$lokasi->name}}</option>
                    @foreach($lokasi->regencies as $kota)
                        <option value={{$kota->id}}>{{$kota->name}}</option>
                    @endforeach
                @endforeach --}}
                @foreach ($collect_pin as $pin)
                    <option value="{{$pin['regency_id']}}">{{$pin['nama_titik']}}</option>
                @endforeach
                @endif
            </select>
        </x-slot>
        <x-slot name="transferFrom">
            <select name="search" id="transfer_serch_from" required
                class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                <option value="">Dari</option>
                {{-- @foreach ($regencyTur as $foo)
                <option style="font-size: 10px" value="{{ $foo->id }}">{{ $foo->name }}</option>
                @endforeach --}}
                {{-- @foreach($search_from_to_tour as $lokasi)
                    <option value={{$lokasi->id}}>{{$lokasi->name}}</option>
                    @foreach($lokasi->regencies as $kota)
                        <option value={{$kota->id}}>{{$kota->name}}</option>
                    @endforeach
                @endforeach --}}
                @foreach ($collect_pin as $pin)
                    <option value="{{$pin['regency_id']}}">{{$pin['nama_titik']}}</option>
                @endforeach
            </select>
        </x-slot>
        <x-slot name="transferTo">
            <select name="to" id="transfer_serch_to" required
                class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                <option value="">Ke</option>
                {{-- @foreach ($regencyTur as $foo)
                <option style="font-size: 10px" value="{{ $foo->id }}">{{ $foo->name }}</option>
                @endforeach --}}
                {{-- @foreach($search_from_to_tour as $lokasi)
                    <option value={{$lokasi->id}}>{{$lokasi->name}}</option>
                    @foreach($lokasi->regencies as $kota)
                        <option value={{$kota->id}}>{{$kota->name}}</option>
                    @endforeach
                @endforeach --}}
                
            </select>
        </x-slot>
    </x-produk.cari-produk>
    <hr class="my-10 border-[#9E3D64]">
    {{-- Populer --}}
    <div class="container mx-3 my-3 sm:mx-auto" x-data="{ active_populer: 'Tur' }">
        <div class="flex items-center">
            <p class="mr-5 text-2xl font-medium uppercase xl:text-5xl">Terbaru</p>
            <img class="w-44 xl:w-auto" src="{{ asset('storage/img/new.jpeg') }}" style="width:111px;" alt="terbaru">
            {{-- Dropdown Desktop --}}
            <div class="flex justify-end w-full">
                <form>
                    <fieldset>
                        <div
                            class="relative border border-[#9E3D64] text-gray-500 bg-white shadow-lg focus:border-2 focus:border-[#9E3D64] ">
                            <select class="hidden w-full px-2 py-2 bg-white appearance-none md:block lg:block"
                                name="produk-dropdown" id="produk-dropdown">
                                <option value="Tur" x-on:click="active_populer = 'Tur'">Paket Tur</option>
                                <option value="Transfer" x-on:click="active_populer = 'Transfer'">Transfer</option>
                                <option value="Rental" x-on:click="active_populer = 'Rental'">Rental Kendaraan
                                </option>
                                <option value="Activity" x-on:click="active_populer = 'Activity'">Aktivitas</option>
                                <option value="Hotel" x-on:click="active_populer = 'Hotel'">Hotel</option>
                                <option value="Xstay" x-on:click="active_populer = 'Xstay'">Xstay</option>
                            </select>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        {{-- Dropdown Mobile --}}
        <div class="flex justify-start w-full my-5">
            <form>
                <fieldset>
                    <div
                        class="relative border border-[#9E3D64] text-gray-500 bg-white shadow-lg focus:border-2 focus:border-[#9E3D64] ">
                        <select class="w-full px-2 py-2 bg-white appearance-none md:hidden lg:hidden"
                            name="produk-dropdown" id="produk-dropdown">
                            <option value="Tur" x-on:click="active_populer = 'Tur'">Paket Tur</option>
                            <option value="Transfer" x-on:click="active_populer = 'Transfer'">Transfer</option>
                            <option value="Rental" x-on:click="active_populer = 'Rental'">Rental Kendaraan</option>
                            <option value="Activity" x-on:click="active_populer = 'Activity'">Aktivitas</option>
                            <option value="Hotel" x-on:click="active_populer = 'Hotel'">Hotel</option>
                            <option value="Xstay" x-on:click="active_populer = 'Xstay'">Xstay</option>
                        </select>
                    </div>
                </fieldset>
            </form>
        </div>

        <div x-show="active_populer === 'Tur'" class="swiper populerSwiper">
            <div class="swiper-wrapper">
                @if (json_decode($tur_populer) != null)
                @foreach (json_decode($tur_populer) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-produk.card-populer-home :act="$act" :type="'tur'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Produk Terbaru tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="-mb-2 swiper-pagination"></div>
        </div>
        <div x-show="active_populer === 'Transfer'" class="swiper populerSwiper">
            <div class="swiper-wrapper">
                @if (json_decode($transfer_populer) != null)
                @foreach (json_decode($transfer_populer) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-produk.card-populer-home :act="$act" :type="'transfer'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Produk Terbaru tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="-mb-2 swiper-pagination"></div>
        </div>
        <div x-show="active_populer === 'Rental'" class="swiper populerSwiper">
            <div class="swiper-wrapper">
                @if (json_decode($rental_populer) != null)
                @foreach (json_decode($rental_populer) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-produk.card-populer-home :act="$act" :type="'rental'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Produk Terbaru tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="-mb-2 swiper-pagination"></div>
        </div>
        <div x-show="active_populer === 'Activity'" class="swiper populerSwiper">
            <div class="swiper-wrapper">
                @if (json_decode($activity_populer) != null)
                @foreach (json_decode($activity_populer) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-produk.card-populer-home :act="$act" :type="'activity'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Produk Terbaru tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="-mb-2 swiper-pagination"></div>
        </div>
        <div x-show="active_populer === 'Hotel'" class="swiper populerSwiper">
            <div class="swiper-wrapper">
                @if (json_decode($hotel_populer) != null)
                @foreach (json_decode($hotel_populer) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-produk.card-populer-home :act="$act" :type="'hotel'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Produk Terbaru tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="-mb-2 swiper-pagination"></div>
        </div>
        <div x-show="active_populer === 'Xstay'" class="swiper populerSwiper">
            <div class="swiper-wrapper">
                @if (json_decode($xstay_populer) != null)
                @foreach (json_decode($xstay_populer) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-produk.card-populer-home :act="$act" :type="'xstay'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Produk Terbaru tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="-mb-2 swiper-pagination"></div>
        </div>
        {{-- <a href="#" class="flex justify-center sm:justify-end text-xl text-[#9E3D64] font-bold">Lihat semua</a>
        --}}
    </div>
    <hr class="my-10 border-[#9E3D64]">
    {{-- Kamtuu About #1 --}}
    <div class="container block p-5 mx-auto my-5 border-2 border-gray-300 rounded-lg md:flex ">
        <div class="grid p-4 my-auto justify-items-center">
            <img class="w-2/6 md:w-full" src="{{ asset('storage/img/homepage-1.png') }}" alt="homepage-1">
            <p class="text-center text-xl xl:text-4xl font-semibold text-[#9E3D64]">MENGAPA BOOKING</p>
            <p class="my-2 text-2xl text-center xl:text-5xl">LEWAT KAMTUU?</p>
        </div>
        {{-- Desktop --}}
        <div class="hidden md:block lg:block">
            <table class="hidden text-center table-auto xl:text-justify md:block lg:block">
                <thead>
                    <tr>
                        @foreach ($kontens as $konten)
                        <th>
                            <img class="w-3/4 mx-auto md:w-40" src="{{ Storage::url($konten->image_section) }}"
                                alt="homepage-2">
                        </th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        @foreach ($kontens as $konten)
                        <td class="px-4 align-middle xl:align-bottom">
                            <p class="text-lg lg:text-xl text-center xl:text-left font-semibold text-[#9E3D64]">
                                {{ $konten->title_section }}
                            </p>
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        @foreach ($kontens as $konten)
                        <td class="px-4 align-top">
                            {!! $konten->detail_section !!}
                        </td>
                        @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
        {{-- Mobile Tab --}}
        <div class=" md:hidden lg:hidden">
            <div class="grid grid-cols-1">
                <div>
                    <img class="w-1/4 mx-auto md:w-40" src="{{ asset('storage/img/homepage-2.png') }}" alt="homepage-2">
                    <p class="text-sm lg:text-xl text-center xl:text-left font-semibold text-[#9E3D64]">Lebih
                        Banyak Pilihan
                    </p>
                    <p class="my-2 text-sm font-medium text-center lg:text-base">Dari attraksi terkenal hingga surga
                        tersembunyi, pilihan
                        lebih banyak, dijamin lebih puas!</p>
                </div>
                <div>
                    <img class="w-1/4 mx-auto md:w-40" src="{{ asset('storage/img/homepage-3.png') }}" alt="homepage-3">
                    <p class="text-sm lg:text-xl text-center xl:text-left font-semibold text-[#9E3D64]">Transaksi
                        Aman</p>
                    <p class="my-2 text-sm font-medium text-center lg:text-base">Bagian anda adalah menikmati, bagian
                        kami
                        adalah membuat
                        transaksi anda aman!</p>
                </div>
                <div>
                    <img class="w-1/4 mx-auto md:w-40" src="{{ asset('storage/img/homepage-4.png') }}" alt="homepage-4">
                    <p class="text-sm lg:text-xl text-center xl:text-left font-semibold text-[#9E3D64]">Harga
                        Terbaik</p>
                    <p class="my-2 text-sm font-medium text-center lg:text-base">Pilih sesuai budget, jadikan liburan
                        anda tak
                        ternilai
                        harganya!!</p>
                </div>
                <div>
                    <img class="w-1/4 mx-auto md:w-40" src="{{ asset('storage/img/homepage-5.png') }}" alt="homepage-5">
                    <p class="text-sm lg:text-xl text-center xl:text-left font-semibold text-[#9E3D64]">Support 24/7
                    </p>
                    <p class="my-2 text-sm font-medium text-center lg:text-base">Kami berbagi kegembiraan anda, kami
                        mendukung
                        anda
                        sepanjang perjalanan!</p>
                </div>
            </div>
        </div>
    </div>

    {{-- Official Stores --}}
    <div class="relative w-full my-20" x-data="slider()">
        <div
            class="absolute kamtuu-background bg-[#9E3D64] w-[150px]  md:w-[450px]  lg:w-[450px] h-full px-10 rounded-r-2xl lg:text-left mx-auto grid content-center text-white text-center">
            <img src="{{ asset('storage/img/homepage-6.png') }}" alt="homepage-6">
            <p class="mt-5 text-xl lg:text-5xl md:text-4xl">OFFICIAL</p>
            <p class="mb-10 text-sm lg:text-5xl md:text-4xl">STORES</p>
            <a class="text-sm lg:text-5xl md:text-4xl" href=" #">Lihat </br> Semua</a>
        </div>
        <div class="static flex overflow-x-auto bg-[#FFB800] bg-clip-content"
            @scroll.debounce="updateIndex($event.target)">
            <div class="flex-none mx-3 my-10 w-[250px]">
            </div>
            @foreach (json_decode($sellers) as $seller)
            <x-homepage.card-stores :seller="$seller"></x-homepage.card-stores>
            @endforeach
            {{-- childNodes > clientLeft --}}
            {{-- Official Stores Component --}}
            {{-- <x-homepage.card-stores x-data="activeIndex"></x-homepage.card-stores>
            <x-homepage.card-stores></x-homepage.card-stores>
            <x-homepage.card-stores></x-homepage.card-stores>
            <x-homepage.card-stores></x-homepage.card-stores>
            <x-homepage.card-stores></x-homepage.card-stores>
            <x-homepage.card-stores></x-homepage.card-stores>
            <x-homepage.card-stores></x-homepage.card-stores> --}}
            <a href="#" class="absolute z-10 translate-y-1/2 bottom-1/2 right-5">
                <img src="{{ asset('storage/icons/chevron-right-circle.svg') }}" alt="">
            </a>
        </div>
    </div>

    <hr class="my-10 border-[#9E3D64]">

    {{-- Sedang Promo --}}
    <div class="container mx-3 my-3 sm:mx-auto" x-data="{ active: 'Activity' }">
        <div class="flex items-center">
            <p class="mr-5 text-2xl font-medium uppercase xl:text-5xl">Sedang Promo</p>
            <img class="w-44 xl:w-auto" src="{{ asset('storage/img/sedang-promo.png') }}" alt="sedang-promo">
            {{-- Dropdown Desktop --}}
            <div class="flex justify-end w-full">
                <form>
                    <fieldset>
                        <div
                            class="relative border border-[#9E3D64] text-gray-500 bg-white shadow-lg focus:border-2 focus:border-[#9E3D64]">
                            <select class="hidden w-full px-2 py-2 bg-white appearance-none md:block lg:block"
                                name="produk_promo" id="produk_promo">
                                <option value="Tur" x-on:click="active = 'Tur'">Paket Tur</option>
                                <option value="Transfer" x-on:click="active = 'Transfer'">Transfer</option>
                                <option value="Rental" x-on:click="active = 'Rental'">Rental Kendaraan</option>
                                <option value="Activity" x-on:click="active = 'Activity'">Aktivitas</option>
                                <option value="Hotel" x-on:click="active = 'Hotel'">Hotel</option>
                                <option value="Xstay" x-on:click="active = 'Xstay'">Xstay</option>
                            </select>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        {{-- Dropdown Desktop --}}
        <div class="flex justify-start w-full my-5">
            <form>
                <fieldset>
                    <div
                        class="relative border border-[#9E3D64] text-gray-500 bg-white shadow-lg focus:border-2 focus:border-[#9E3D64]">
                        <select class="w-full px-2 py-2 bg-white appearance-none md:hidden lg:hidden"
                            name="produk_promo" id="produk_promo">
                            <option value="Tur" x-on:click="active = 'Tur'">Paket Tur</option>
                            <option value="Transfer" x-on:click="active = 'Transfer'">Transfer</option>
                            <option value="Rental" x-on:click="active = 'Rental'">Rental Kendaraan</option>
                            <option value="Activity" x-on:click="active = 'Activity'">Aktivitas</option>
                            <option value="Hotel" x-on:click="active = 'Hotel'">Hotel</option>
                            <option value="Xstay" x-on:click="active = 'Xstay'">Xstay</option>
                        </select>
                    </div>
                </fieldset>
            </form>
        </div>

        <div x-show="active === 'Tur'" class="swiper productSwiper">
            <div class="swiper-wrapper">
                @if (json_decode($tur) != null)
                @foreach (json_decode($tur) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-destindonesia.card-produk :act="$act" :type="'tur'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Promo tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="-mb-2 swiper-pagination"></div>
        </div>
        <div x-show="active === 'Transfer'" class="swiper productSwiper">
            <div class="swiper-wrapper">
                @if (json_decode($transfer) != null)
                @foreach (json_decode($transfer) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-destindonesia.card-produk :act="$act" :type="'transfer'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Promo tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="-mb-2 swiper-pagination"></div>
        </div>
        <div x-show="active === 'Rental'" class="swiper productSwiper">
            <div class="swiper-wrapper">
                @if (json_decode($rental) != null)
                @foreach (json_decode($rental) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-destindonesia.card-produk :act="$act" :type="'rental'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Promo tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="-mb-2 swiper-pagination"></div>
        </div>
        <div x-show="active === 'Activity'" class="swiper productSwiper">
            <div class="swiper-wrapper">
                @if (json_decode($activity) != null)
                @foreach (json_decode($activity) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-destindonesia.card-produk :act="$act" :type="'activity'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Promo tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="-mb-2 swiper-pagination"></div>
        </div>
        <div x-show="active === 'Hotel'" class="swiper productSwiper">
            <div class="swiper-wrapper">
                @if (json_decode($hotel) != null)
                @foreach (json_decode($hotel) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-destindonesia.card-produk :act="$act" :type="'hotel'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Promo tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="-mb-2 swiper-pagination"></div>
        </div>
        <div x-show="active === 'Xstay'" class="swiper productSwiper">
            <div class="swiper-wrapper">
                @if (json_decode($xstay) != null)
                @foreach (json_decode($xstay) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-destindonesia.card-produk :act="$act" :type="'xstay'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Promo tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="-mb-2 swiper-pagination"></div>
        </div>
        {{-- <a href="#" class="flex justify-center md:justify-end text-xl text-[#9E3D64] font-bold">Lihat semua</a>
        --}}
    </div>

    <hr class="my-10 border-[#9E3D64]">

    {{-- Top Destinasi --}}
    <div class="my-10 bg-[#9E3D64] p-5">
        <div class="container mx-auto">
            <p class="pb-5 text-4xl font-semibold text-white md:text-5xl">TOP DESTINASI</p>
            <div class="grid grid-cols-2 gap-5 md:grid-cols-3 lg:grid-cols-3">
                {{-- Component Top Destination --}}
                @if (isset($top_destination))
                @forelse ($top_destination as $top)
                <x-homepage.card-top-destination text="{{ $top[0] }}">
                </x-homepage.card-top-destination>
                @empty
                <p>Tidak ada top destinasi.</p>
                @endforelse
                @else
                <p>Tidak ada top destinasi.</p>
                @endif

                {{-- <x-homepage.card-top-destination text="Bali"></x-homepage.card-top-destination>
                <x-homepage.card-top-destination text="Lombok"></x-homepage.card-top-destination>
                <x-homepage.card-top-destination text="Bandung"></x-homepage.card-top-destination>
                <x-homepage.card-top-destination text="Yogyakarta"></x-homepage.card-top-destination>
                <x-homepage.card-top-destination text="Toraja"></x-homepage.card-top-destination>
                <x-homepage.card-top-destination text="Bira"></x-homepage.card-top-destination>
                <x-homepage.card-top-destination text="Danau Toba"></x-homepage.card-top-destination>
                <x-homepage.card-top-destination text="Wakatobi"></x-homepage.card-top-destination>
                <x-homepage.card-top-destination text="Raja Ampat"></x-homepage.card-top-destination> --}}
            </div>
            {{-- <a href="#" class="flex justify-center mt-10 mb-5 text-xl font-bold text-white sm:justify-end">Lihat
                semua</a> --}}
        </div>
    </div>

    {{-- Banner Kamtuu --}}
    <div class="container mx-auto rounded-lg">
        <div class="mx-5 block md:flex bg-[#FFB800] rounded-xl">
            <img class="object-cover mx-auto w-44 sm:w-1/2 md:w-1/2 lg:w-auto md:mx-0"
                src="{{ asset('storage/img/homepage-7.png') }}" alt="homepage-7">
            <div class="grid content-center mx-2 my-5 text-center md:text-left">
                <p class="text-xl font-semibold lg:text-3xl">Masih belum nemu paket yang sesuai?</p>
                <p class="mt-1 mb-5 text-xl font-semibold lg:text-3xl lg:mt-3">Pengen dibuatkan khusus?</p>

                <x-destindonesia.button-primary text="Minta dibuatkan paket khusus">
                    @slot('button')
                    Minta dibuatkan paket khusus
                    @endslot
                </x-destindonesia.button-primary>
            </div>
        </div>
    </div>

    <hr class="my-10 border-[#9E3D64]">

    {{-- Kamtuu Tabs --}}
    <div class="container mx-auto" x-data="{
        active: 0,
        tabs: ['Paket Tur', 'Transfer', 'Rental Kendaraan', 'Aktivitas', 'Hotel', 'Xstay']
    }">
        <p class="mx-2 text-2xl font-medium">Ke mana saja di Indonesia, pencarian dimulai di Kamtuu!</p>

        {{-- Tabs --}}
        <div class="flex sm:block">
            <ul
                class="flex justify-start overflow-x-auto text-sm font-medium text-center text-gray-500 sm:text-left dark:text-gray-400">
                <template x-for="(tab, index) in tabs" :key="index">
                    <li class="mt-2 mb-2" :class="{ 'border-b-2 border-[#9E3D64]': active == index }"
                        x-on:click="active = index">
                        <button class="inline-block py-2 text-xl rounded-lg px-9" :class="{
                                'text-[#9E3D64] bg-white font-bold': active ==
                                    index,
                                'text-black font-normal hover:text-[#9E3D64]': active != index
                            }" x-text="tab"></button>
                    </li>
                </template>
            </ul>
        </div>

        {{-- List --}}
        <div x-show="active === 0" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
            @if(isset($turRegions))
            @foreach($turRegions as $turRegion)
            <a href="{{route('list.tur',['name'=>$turRegion['name']])}}"
                class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke {{$turRegion['name']}}</a>
            @endforeach
            @endif
            <a href="#" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur belum tersedia</a>
            {{-- <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Bali</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Yogyakarta</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Bandung</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Surabaya</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Jakarta</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Semarang</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Solo</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Banyuwangi</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Bali</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Yogyakarta</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Bandung</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Surabaya</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Jakarta</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Semarang</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Solo</a>
            <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Banyuwangi</a> --}}
        </div>
        <div x-show="active === 1" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
            @if(isset($transferRegions))
            @foreach($transferRegions as $transferRegion)
            <a href="{{route('list.transfer',['name'=>$transferRegion['name']])}}"
                class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke {{$transferRegion['name']}}</a>
            @endforeach
            @endif
            <a href="#" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke belum tersedia</a>
            {{-- <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Bali</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Yogyakarta</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Bandung</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Surabaya</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Jakarta</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Semarang</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Solo</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Banyuwangi</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Bali</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Yogyakarta</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Bandung</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Surabaya</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Jakarta</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Semarang</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Solo</a>
            <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Banyuwangi</a> --}}
        </div>
        <div x-show="active === 2" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
            @if(isset($rentalRegions))
            @foreach($rentalRegions as $rentalRegion)
            <a href="{{route('list.rental',['name'=>$rentalRegion['name']])}}"
                class="m-2 hover:text-[#9E3D64] duration-200">Kendaraan {{$rentalRegion['name']}}</a>
            @endforeach
            @endif
            <a href="#" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan belum tersedia</a>

            {{-- <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Bali</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Yogyakarta</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Bandung</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Surabaya</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Jakarta</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Semarang</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Solo</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Banyuwangi</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Bali</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Yogyakarta</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Bandung</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Surabaya</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Jakarta</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Semarang</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Solo</a>
            <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Banyuwangi</a> --}}
        </div>
        <div x-show="active === 3" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
            @if(isset($activityRegions))
            @foreach($activityRegions as $activityRegion)
            <a href="{{route('list.activity',['name'=>$activityRegion['name']])}}"
                class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di {{$activityRegion['name']}}</a>
            @endforeach
            @endif
            <a href="#" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas belum tersedia</a>
            {{-- <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Bali</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Yogyakarta</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Bandung</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Surabaya</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Jakarta</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Semarang</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Solo</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Banyuwangi</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Bali</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Yogyakarta</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Bandung</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Surabaya</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Jakarta</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Semarang</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Solo</a>
            <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Banyuwangi</a> --}}
        </div>
        <div x-show="active === 4" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
            @if(isset($hotelRegions))
            @foreach($hotelRegions as $hotelRegion)
            <a href="{{route('list.hotel',['name'=>$hotelRegion['name']])}}"
                class="m-2 hover:text-[#9E3D64] duration-200">Hotel di {{$hotelRegion['name']}}</a>
            @endforeach
            @endif
            <a href="#" class="m-2 hover:text-[#9E3D64] duration-200">Hotel belum tersedia</a>
            {{-- <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Bali</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Yogyakarta</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Bandung</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Surabaya</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Jakarta</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Semarang</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Solo</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Banyuwangi</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Bali</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Yogyakarta</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Bandung</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Surabaya</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Jakarta</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Semarang</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Solo</a>
            <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Banyuwangi</a> --}}
        </div>
        <div x-show="active === 5" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
            @if(isset($xstayRegions))
            @foreach($xstayRegions as $xstayRegion)
            <a href="{{route('list.xstay',['name'=>$xstayRegion['name']])}}"
                class="m-2 hover:text-[#9E3D64] duration-200">Xstay {{$xstayRegion['name']}}</a>
            @endforeach
            @endif
            <a href="#" class="m-2 hover:text-[#9E3D64] duration-200">Xstay belum tersedia</a>
            {{-- <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Bali</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Yogyakarta</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Bandung</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Surabaya</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Jakarta</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Semarang</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Solo</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Banyuwangi</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Bali</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Yogyakarta</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Bandung</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Surabaya</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Jakarta</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Semarang</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Solo</a>
            <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Banyuwangi</a> --}}
        </div>
    </div>

    <hr class="my-10 border-[#9E3D64]">

    {{-- Kamtuu Description #1 --}}
    <div class="container p-3 mx-auto">
        <p class="text-2xl font-medium text-center sm:text-left">Ke mana saja di Indonesia, pencarian dimulai
            di Kamtuu!</p>
        <div class="grid justify-items-center sm:grid-cols-12">
            <div class="my-5 sm:col-span-9">
                <p class="text-lg text-center sm:text-xl sm:text-left">Daftar sebagai Seller di Kamtuu dan dapatkan
                    akses ke pasar wisatawan
                    domestik dan
                    mancanegara...</p>
                <div class="flex justify-center">
                    <x-destindonesia.button-primary url="{{ route('RegisterSeller') }}" text="Daftar Sebagai Seller">
                        @slot('button')
                        Daftar Sebagai Seller
                        @endslot
                    </x-destindonesia.button-primary>
                </div>
            </div>
            <img class="order-first sm:order-last sm:col-span-3" src="{{ asset('storage/img/homepage-8.png') }}"
                alt="homepage-8">
        </div>
    </div>

    <hr class="my-10 border-[#9E3D64]">

    {{-- Kamtuu Description #2 --}}
    <div class="container p-3 mx-auto">
        <p class="text-2xl font-medium text-center sm:text-left">Ingin brand Travel Ageny anda menjadi lebih dikenal?
        </p>
        <div class="grid justify-items-center sm:grid-cols-12">
            <div class="my-5 sm:col-span-9">
                <p class="text-lg text-center sm:text-xl sm:text-left">Buka Official Store di Kamtuu agar keberadaan
                    brand anda secara online
                    lebih kuat dan
                    lebih menancap di benar para wisatawan. </p>
                <div class="flex justify-center">
                    <x-destindonesia.button-primary url="{{ route('RegisterSeller') }}" text="Buka Official Store">
                        @slot('button')
                        Buka Official Store
                        @endslot
                    </x-destindonesia.button-primary>
                </div>
            </div>
            <img class="order-first sm:order-last sm:col-span-3" src="{{ asset('storage/img/homepage-9.png') }}"
                alt="homepage-9">
        </div>
    </div>

    <hr class="my-10 border-[#9E3D64]">

    {{-- Kamtuu Footer #1 --}}
    <div class="container mx-auto mb-10">
        <div class="grid grid-cols-3 sm:grid-cols-4">
            <div class="p-3">
                <p class="text-base font-medium sm:text-xl lg:text-2xl">Kamtuu</p>
                @foreach ($kamtuuSets as $kamtuu)
                <a href="{{ route('section.show', $kamtuu->id) }}"
                    class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                    {{ $kamtuu->title_section }}</a>
                @endforeach
            </div>
            <div class="p-3">
                <p class="text-base font-medium sm:text-xl lg:text-2xl">Untuk Traveler</p>
                @foreach ($travellerSets as $kamtuu)
                <a href="{{ route('section.show', $kamtuu->id) }}"
                    class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                    {{ $kamtuu->title_section }}</a>
                @endforeach
            </div>
            <div class="p-3">
                <p class="text-base font-medium sm:text-xl lg:text-2xl">Untuk Seller</p>
                @foreach ($sellerSets as $kamtuu)
                <a href="{{ route('section.show', $kamtuu->id) }}"
                    class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                    {{ $kamtuu->title_section }}</a>
                @endforeach
            </div>
            <div class="p-3">
                <p class="text-base font-medium sm:text-xl lg:text-2xl">Untuk Agen</p>
                @foreach ($agenSets as $kamtuu)
                <a href="{{ route('section.show', $kamtuu->id) }}"
                    class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                    {{ $kamtuu->title_section }}</a>
                @endforeach
            </div>
            <div class="p-3">
                <p class="text-base font-medium sm:text-xl lg:text-2xl">Official Store</p>
                @foreach ($storeSets as $kamtuu)
                <a href="{{ route('section.show', $kamtuu->id) }}"
                    class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                    {{ $kamtuu->title_section }}</a>
                @endforeach
            </div>
            <div class="p-3">
                <p class="text-base font-medium sm:text-xl lg:text-2xl">Untuk Pelanggan Corporate</p>
                @foreach ($corporateSets as $kamtuu)
                <a href="{{ route('section.show', $kamtuu->id) }}"
                    class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                    {{ $kamtuu->title_section }}</a>
                @endforeach
            </div>
            <div class="p-3">
                <p class="text-base font-medium sm:text-xl lg:text-2xl">Berbisnis Dengan Kamtuu</p>
                @foreach ($kerjasamaSets as $kamtuu)
                <a href="{{ route('section.show', $kamtuu->id) }}"
                    class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                    {{ $kamtuu->title_section }}</a>
                @endforeach
            </div>
            <div class="p-3">
                <p class="text-base font-medium sm:text-xl lg:text-2xl">Layanan</p>
                @foreach ($layananSets as $kamtuu)
                <a href="{{ route('section.show', $kamtuu->id) }}"
                    class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                    {{ $kamtuu->title_section }}</a>
                @endforeach
            </div>
        </div>
    </div>
    <div class="dialog"></div>
    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        var swiper = new Swiper(".productSwiper", {
            slidesPerView: 1,
            loop: true,
            navigation: {
                enabled: true,
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    navigation: {
                        enabled: true
                    },
                    pagination: {
                        enabled: false
                    }
                },
                1024: {
                    slidesPerView: 3,
                    navigation: {
                        enabled: false
                    },
                    pagination: {
                        enabled: true
                    }
                },
                1280: {
                    slidesPerView: 5,
                    navigation: {
                        enabled: false,
                    },
                    pagination: {
                        enabled: false
                    }
                }
            },
        });

        var swiperPopuler = new Swiper(".populerSwiper", {
            slidesPerView: 1,
            loop: true,
            navigation: {
                enabled: true,
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    navigation: {
                        enabled: true
                    },
                    pagination: {
                        enabled: false
                    }
                },
                1024: {
                    slidesPerView: 3,
                    navigation: {
                        enabled: false
                    },
                    pagination: {
                        enabled: true
                    }
                },
                1280: {
                    slidesPerView: 5,
                    navigation: {
                        enabled: false,
                    },
                    pagination: {
                        enabled: false
                    }
                }
            },
        });

        var bannerSwiper = new Swiper(".carousel-banner", {
            loop: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
                enabled: false,
            },
            autoplay: {
                delay: 3000,
                pauseOnMouseEnter: true,
                disableOnInteraction: false,
            },
            breakpoints: {
                640: {
                    pagination: {
                        enabled: true
                    }
                }
            }
        });

        /* function submitLike2(id){
            $.ajax({
                url:'/like',
                method:'POST',
                dataType:'json',
                cache:false,
                data:{
                    '_token':$("meta[name='csrf-token']").attr("content"),
                    'product_id':id
                },
                success(resp){
                    if(resp.success){
                        if(resp.data.status===1){
                            console.log('status:'+resp.data.status+' and id:'+$id)
                            document.getElementById(id).classList.add("")

                        }else{

                        }
                    }
                },
                failure(resp){
                    console.log('error')
                }
            });
        }
        */

        function submitLike2(id){
            $.ajax({
                url: '/like',
                method: 'POST',
                dataType: 'json',
                cache: false,
                data: {
                    '_token': $("meta[name='csrf-token']").attr("content"),
                    'product_id': id
                },
                success(resp) {
                    if (resp.success) {
                        if (resp.data.status == 1) {
                            $("." + id).addClass("fill-red-500");
                            $("#" + id).addClass("hover:fill-black");
                        } else {
                            $("." + id).addClass("hover:fill-red-500");
                            $("." + id).removeClass("fill-red-500");
                            $("." + id).addClass("text-white");
                        }
                    } else {
                        // console.log(resp.message)
                        // $(".dialog").dialog();
                        alert(resp.message);
                    }
                },
                failure(resp) {
                    console.log('error')
                }
            });
        }

    </script>
    <script>
        console.log($("#tur-from-select2"))
        $("#tur-from-select2").select2()
        $("#tur-to-select2").select2()
        $("#rental_serch_from").select2()
        $('#transfer_serch_from').select2()
        $('#transfer_serch_to').select2()
        $('#hotel_search_from').select2()
        $('#xstay_search_from').select2()
        $('#activity_search_from').select2()
    </script>
    {{-- <script>
        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $(function() {
                // provinsi
                $('#produk_promo').on('change', function() {
                    let produk_promo = $('#produk_promo').val();
                    console.log(produk_promo)

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getPromo') }}",
                        data: {
                            produk_promo: produk_promo
                        },
                        cache: false,

                        success: function(msg) {
                            $('#promo').html(msg);
                            console.log(msg)
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })
            })

        });
    </script> --}}

</body>

</html>
