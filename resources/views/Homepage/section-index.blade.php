<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])


    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
<header class="border-b border-[#9E3D64] bg-white">
    <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
</header>

    <div class="bg-white max-w-6xl p-3 mx-auto my-2">
        <h1 class="text-2xl font-bold">{{$sections->title_section}}</h1>
        <br>
        <img class="mx-auto w-3/4 md:w-40" src="{{ Storage::url($sections->image_section) }}"
             alt="konten_section">
        <br>
        <p>
            {!!$sections->detail_section!!}
        </p>
    </div>

</body>
</html>
