<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kamtuu Home') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css"/>

    <script>
        function slider() {
            return {
                slideCount: 4,
                activeIndex: "z-10",
                hideScrollbar: false,
                updateIndex(e) {
                    if (e.scrollLeft < e.clientWidth) {
                        let child = e.childNodes;
                        for (let i = 0; i < child.length; i++) {
                            const element = child[i];
                            this.activeIndex = element.offsetLeft < 0 ? "-z-10" : "z-10";
                            // console.log(i, this.activeIndex, element.offsetLeft);
                        }
                        this.activeIndex = 0;
                    } else {
                        this.activeIndex = Math.round(e.scrollLeft / e.clientWidth);
                    }
                }
            };
        }
    </script>
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        /* Hide scrollbar for Chrome, Safari and Opera */
        .no-scrollbar::-webkit-scrollbar {
            display: none;
        }

        /* Hide scrollbar for IE, Edge and Firefox */
        .no-scrollbar {
            -ms-overflow-style: none;
            /* IE and Edge */
            scrollbar-width: none;
            /* Firefox */
        }

        .swiper-pagination-bullet {
            background: rgb(173, 151, 151);
            opacity: 1;
        }

        .swiper-pagination-bullet-active {
            background: white;
        }
    </style>
    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
<header class="border-b border-[#9E3D64]">
    <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
</header>


<div class="mx-auto">
    <div class="container mx-auto" x-data="{ active:0,
            tabs:['Paket Tur', 'Transfer', 'Rental Kendaraan', 'Aktivitas', 'Hotel', 'Xstay']}">
        <p class="text-2xl font-medium my-5">List Kategori Kamtuu</p>
        <hr class="my-10 border-[#9E3D64]">

        {{-- Tabs --}}
    <div class="flex sm:block">
        <ul
            class="flex justify-start overflow-x-auto text-sm font-medium text-center sm:text-left text-gray-500 dark:text-gray-400">
            <template x-for="(tab, index) in tabs" :key="index">
                <li class="mb-2 mt-2" :class="{'border-b-2 border-[#9E3D64]': active == index}"
                    x-on:click="active = index">
                    <button class="inline-block py-2 px-9 text-xl rounded-lg"
                            :class="{'text-[#9E3D64] bg-white font-bold': active == index, 'text-black font-normal hover:text-[#9E3D64]': active != index}"
                            x-text="tab"></button>
                </li>
            </template>
        </ul>
    </div>

    {{-- List --}}
    <div x-show="active === 0" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Bali</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Yogyakarta</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Bandung</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Surabaya</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Jakarta</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Semarang</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Solo</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Banyuwangi</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Bali</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Yogyakarta</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Bandung</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Surabaya</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Jakarta</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Semarang</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Solo</a>
        <a href="/tur" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke Banyuwangi</a>
    </div>
    <div x-show="active === 1" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Bali</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Yogyakarta</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Bandung</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Surabaya</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Jakarta</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Semarang</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Solo</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Banyuwangi</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Bali</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Yogyakarta</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Bandung</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Surabaya</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Jakarta</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Semarang</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Solo</a>
        <a href="/transfer" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke Banyuwangi</a>
    </div>
    <div x-show="active === 2" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Bali</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Yogyakarta</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Bandung</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Surabaya</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Jakarta</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Semarang</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Solo</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Banyuwangi</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Bali</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Yogyakarta</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Bandung</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Surabaya</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Jakarta</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Semarang</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Solo</a>
        <a href="/rental" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan Banyuwangi</a>
    </div>
    <div x-show="active === 3" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Bali</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Yogyakarta</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Bandung</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Surabaya</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Jakarta</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Semarang</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Solo</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Banyuwangi</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Bali</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Yogyakarta</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Bandung</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Surabaya</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Jakarta</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Semarang</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Solo</a>
        <a href="/activity" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas di Banyuwangi</a>
    </div>
    <div x-show="active === 4" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Bali</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Yogyakarta</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Bandung</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Surabaya</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Jakarta</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Semarang</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Solo</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Banyuwangi</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Bali</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Yogyakarta</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Bandung</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Surabaya</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Jakarta</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Semarang</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Solo</a>
        <a href="/hotel" class="m-2 hover:text-[#9E3D64] duration-200">Hotel di Banyuwangi</a>
    </div>
    <div x-show="active === 5" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Bali</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Yogyakarta</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Bandung</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Surabaya</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Jakarta</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Semarang</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Solo</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Banyuwangi</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Bali</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Yogyakarta</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Bandung</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Surabaya</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Jakarta</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Semarang</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Solo</a>
        <a href="/xstay" class="m-2 hover:text-[#9E3D64] duration-200">Xstay Banyuwangi</a>
    </div>
</div>

    <hr class="my-10 border-[#9E3D64]">
</div>

<footer>
    <x-destindonesia.footer></x-destindonesia.footer>
</footer>

<script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
<script>
    var swiper = new Swiper(".productSwiper", {
        slidesPerView: 1,
        loop: true,
        navigation: {
            enabled: true,
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        breakpoints: {
            768: {
                slidesPerView: 2,
                navigation: {
                    enabled: true
                },
                pagination: {
                    enabled: false
                }
            },
            1024: {
                slidesPerView: 3,
                navigation: {
                    enabled: false
                },
                pagination: {
                    enabled: true
                }
            },
            1280: {
                slidesPerView: 5,
                navigation: {
                    enabled: false,
                },
                pagination: {
                    enabled: false
                }
            }
        },
    });

    var bannerSwiper = new Swiper(".carousel-banner", {
        loop: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
            enabled: false,
        },
        autoplay: {
            delay: 3000,
            pauseOnMouseEnter: true,
            disableOnInteraction: false,
        },
        breakpoints: {
            640: {
                pagination: {
                    enabled: true
                }
            }
        }
    });
</script>

</body>

</html>
