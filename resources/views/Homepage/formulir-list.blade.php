<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kamtuu Home') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css"/>

    <script>
        function slider() {
            return {
                slideCount: 4,
                activeIndex: "z-10",
                hideScrollbar: false,
                updateIndex(e) {
                    if (e.scrollLeft < e.clientWidth) {
                        let child = e.childNodes;
                        for (let i = 0; i < child.length; i++) {
                            const element = child[i];
                            this.activeIndex = element.offsetLeft < 0 ? "-z-10" : "z-10";
                            // console.log(i, this.activeIndex, element.offsetLeft);
                        }
                        this.activeIndex = 0;
                    } else {
                        this.activeIndex = Math.round(e.scrollLeft / e.clientWidth);
                    }
                }
            };
        }
    </script>
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        /* Hide scrollbar for Chrome, Safari and Opera */
        .no-scrollbar::-webkit-scrollbar {
            display: none;
        }

        /* Hide scrollbar for IE, Edge and Firefox */
        .no-scrollbar {
            -ms-overflow-style: none;
            /* IE and Edge */
            scrollbar-width: none;
            /* Firefox */
        }

        .swiper-pagination-bullet {
            background: rgb(173, 151, 151);
            opacity: 1;
        }

        .swiper-pagination-bullet-active {
            background: white;
        }
    </style>
    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
<header class="border-b border-[#9E3D64]">
    <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
</header>


<div class="mx-auto">
    <div class="grid grid-cols-1 mt-5 lg:grid-cols-6 place-items-center">
        {{--    trip    --}}
        <div class="grid m-2 text-center rounded-lg place-items-center bg-gray-50 hover:shadow-lg hover:transition-transform hover:duration-500 hover:transform hover:scale-110 hover:ease-out">
            <a href="{{route('formulirTour')}}" class=" text-[12px] mb-1">
                <span class="my-auto text-lg font-semibold text-black">Formulir Tour</span>
                <div
                    class="grid grid-cols-1 m-5 border-2 rounded-full place-items-center p-9 border-kamtuu-primary">
                    <img class="w-24 h-24 mx-2" src="{{ asset('storage/images/travel-bag.png') }}" alt="">
                </div>
            </a>
        </div>

        {{--    transfer    --}}
        <div class="grid m-2 text-center rounded-lg place-items-center bg-gray-50 hover:shadow-lg hover:transition-transform hover:duration-500 hover:transform hover:scale-110 hover:ease-out">
            <a href="" class=" text-[12px] mb-1">
                <span class="my-auto text-lg font-semibold text-black">Formulir Transfer</span>
                <div
                    class="grid grid-cols-1 m-5 border-2 rounded-full place-items-center p-9 border-kamtuu-primary">
                    <img class="w-24 h-24 mx-2" src="{{ asset('storage/images/road-trip.png') }}" alt="">
                </div>
            </a>
        </div>

        {{--    rental mobil    --}}
        <div class="grid m-2 text-center rounded-lg place-items-center bg-gray-50 hover:shadow-lg hover:transition-transform hover:duration-500 hover:transform hover:scale-110 hover:ease-out">
            <a href="" class=" text-[12px] mb-1">
                <span class="my-auto text-lg font-semibold text-black">Formulir Hotel</span>
                <div
                    class="grid grid-cols-1 m-5 border-2 rounded-full place-items-center p-9 border-kamtuu-primary">
                    <img class="w-24 h-24 mx-2" src="{{ asset('storage/images/5-stars.png') }}" alt="">
                </div>
            </a>
        </div>

        {{--    activity    --}}
        <div class="grid m-2 text-center rounded-lg place-items-center bg-gray-50 hover:shadow-lg hover:transition-transform hover:duration-500 hover:transform hover:scale-110 hover:ease-out">
            <a href=" " class=" text-[12px] mb-1">
                <span class="my-auto text-lg font-semibold text-black">Formulir Tour</span>
                <div
                    class="grid grid-cols-1 m-5 border-2 rounded-full place-items-center p-9 border-kamtuu-primary">
                    <img class="w-24 h-24 mx-2" src="{{ asset('storage/images/travel-bag.png') }}" alt="">
                </div>
            </a>
        </div>

        {{--    hotel    --}}
        <div class="grid m-2 text-center rounded-lg place-items-center bg-gray-50 hover:shadow-lg hover:transition-transform hover:duration-500 hover:transform hover:scale-110 hover:ease-out">
            <a href=" " class=" text-[12px] mb-1">
                <span class="my-auto text-lg font-semibold text-black">Formulir Tour</span>
                <div
                    class="grid grid-cols-1 m-5 border-2 rounded-full place-items-center p-9 border-kamtuu-primary">
                    <img class="w-24 h-24 mx-2" src="{{ asset('storage/images/travel-bag.png') }}" alt="">
                </div>
            </a>
        </div>

        {{--    xstay    --}}
        <div
            class="grid m-2 text-center rounded-lg place-items-center bg-gray-50 hover:shadow-lg hover:transition-transform hover:duration-500 hover:transform hover:scale-110 hover:ease-out">
            <a href=" " class=" text-[12px] mb-1">
                <span class="my-auto text-lg font-semibold text-black">Formulir Tour</span>
                <div
                    class="grid grid-cols-1 m-5 border-2 rounded-full place-items-center p-9 border-kamtuu-primary">
                    <img class="w-24 h-24 mx-2" src="{{ asset('storage/images/travel-bag.png') }}" alt="">
                </div>
            </a>
        </div>
    </div>

    <hr class="my-10 border-[#9E3D64]">
</div>

<footer>
    <x-destindonesia.footer></x-destindonesia.footer>
</footer>

<script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>

</body>

</html>
