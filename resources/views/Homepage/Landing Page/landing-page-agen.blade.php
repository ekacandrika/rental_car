<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kamtuu Home') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
<header class="border-b border-[#9E3D64]">
    <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
</header>

{{-- Kamtuu Tabs --}}
<div class="container mx-auto" x-data="{ active:0,
            tabs:['Personal', 'Company']}">
    <p class="text-2xl font-medium mx-2 my-5">Keuntungan Yang Anda Dapatkan Jika Anda <span class="text-kamtuu-primary"><a href="{{route('RegisterAgen')}}">Bergabung</a></span> Menjadi Agen Kami</p>

    {{-- Tabs --}}
    <div class="flex sm:block">
        <ul
            class="flex justify-start overflow-x-auto text-sm font-medium text-center sm:text-left text-gray-500 dark:text-gray-400">
            <template x-for="(tab, index) in tabs" :key="index">
                <li class="mb-2 mt-2" :class="{'border-b-2 border-[#9E3D64]': active == index}"
                    x-on:click="active = index">
                    <button class="inline-block py-2 px-9 text-xl rounded-lg"
                            :class="{'text-[#9E3D64] bg-white font-bold': active == index, 'text-black font-normal hover:text-[#9E3D64]': active != index}"
                            x-text="tab"></button>
                </li>
            </template>
        </ul>
    </div>

    {{-- List --}}
    <div x-show="active === 0" class="grid grid-cols-1 text-sm md:text-lg lg:text-xl">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium corporis cupiditate dicta dignissimos earum eos illo iure mollitia necessitatibus, optio provident quidem sequi sint sunt totam vel velit vitae voluptatem!
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut cumque debitis esse et explicabo hic ipsa itaque laborum minima placeat, quae, ratione ut! Illum inventore minus nulla, reiciendis ullam voluptas.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur itaque iure, nihil numquam quo unde vel voluptate voluptatum? Delectus dolore dolorem esse et excepturi iusto pariatur quasi similique veniam!
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque beatae cupiditate, dolor, eligendi in incidunt inventore itaque, modi nihil omnis quam quas quasi quos ratione recusandae rem repellat repudiandae voluptatem.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut cumque debitis esse et explicabo hic ipsa itaque laborum minima placeat, quae, ratione ut! Illum inventore minus nulla, reiciendis ullam voluptas.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur itaque iure, nihil numquam quo unde vel voluptate voluptatum? Delectus dolore dolorem esse et excepturi iusto pariatur quasi similique veniam!
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque beatae cupiditate, dolor, eligendi in incidunt inventore itaque, modi nihil omnis quam quas quasi quos ratione recusandae rem repellat repudiandae voluptatem.
    </div>
    <div x-show="active === 1" class="grid grid-cols-1 text-sm md:text-lg lg:text-xl">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque facilis optio quaerat quam tempore vel. A assumenda consequuntur corporis est ex exercitationem inventore ipsa, iure labore mollitia neque quis reiciendis.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias at illum officiis quos? Ad corporis, expedita incidunt iure natus quidem repellendus. Aperiam architecto aut inventore itaque provident quasi repellendus tempora.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur cupiditate dicta facere ipsam minima necessitatibus officiis ratione vel voluptatem! Accusantium architecto dolor dolorum eius incidunt modi officiis perferendis voluptas voluptatem.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum laudantium necessitatibus pariatur quia sequi, similique tempora unde! Cupiditate dignissimos eum exercitationem impedit nostrum pariatur perspiciatis, sint temporibus ullam, velit voluptatem!
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias at illum officiis quos? Ad corporis, expedita incidunt iure natus quidem repellendus. Aperiam architecto aut inventore itaque provident quasi repellendus tempora.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur cupiditate dicta facere ipsam minima necessitatibus officiis ratione vel voluptatem! Accusantium architecto dolor dolorum eius incidunt modi officiis perferendis voluptas voluptatem.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum laudantium necessitatibus pariatur quia sequi, similique tempora unde! Cupiditate dignissimos eum exercitationem impedit nostrum pariatur perspiciatis, sint temporibus ullam, velit voluptatem!
    </div>
</div>

<footer>
    <x-destindonesia.footer></x-destindonesia.footer>
</footer>

</body>

</html>
