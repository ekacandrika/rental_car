<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! seo($page ?? null) !!}
    <title>{{ config('app.name', 'Kamtuu Home') }}</title>

    {{-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> --}}
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    {{-- <link id="bs-css" href="https://netdna.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet"> --}}
        {{-- <link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet"> --}}
    {{-- <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" /> --}}
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    {{-- select2 --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>

    <script>
        function slider() {
            return {
                slideCount: 4,
                activeIndex: "z-10",
                hideScrollbar: false,
                updateIndex(e) {
                    if (e.scrollLeft < e.clientWidth) {
                        let child = e.childNodes;
                        for (let i = 0; i < child.length; i++) {
                            const element = child[i];
                            this.activeIndex = element.offsetLeft < 0 ? "-z-10" : "z-10";
                            // console.log(i, this.activeIndex, element.offsetLeft);
                        }
                        this.activeIndex = 0;
                    } else {
                        this.activeIndex = Math.round(e.scrollLeft / e.clientWidth);
                    }
                },
            };
        }
    </script>
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <style>
        /* Hide scrollbar for Chrome, Safari and Opera */
        .no-scrollbar::-webkit-scrollbar {
            display: none;
        }

        /* Hide scrollbar for IE, Edge and Firefox */
        .no-scrollbar {
            -ms-overflow-style: none;
            /* IE and Edge */
            scrollbar-width: none;
            /* Firefox */
        }

        .swiper-pagination-bullet {
            background: rgb(173, 151, 151);
            opacity: 1;
        }

        .swiper-pagination-bullet-active {
            background: white;
        }
        .select2-container .select2-selection--single {
            height: 100%;
            padding-top: 0.5rem;
            color:black;
            font-size: 0.875rem; /* 14px */
            /* line-height: 1.25rem; 20px */
            padding-bottom: 0.5rem;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 100%;
        }

        .select2-selection__arrow {
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            /* padding-right: 0.75rem; */
        }
        .select2-container--default .select2-selection--single{
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
        }
        .select2-results__option {
            font-size: 0.875rem;
        }
        .select2-container.select2-dropdown-open {
            width: 170%;
        }
        .select2-container .select2-container--default .select2-container--open{
            width:100px !important;
        }
        .bigdrop {
            width: 600px !important;
        }
        @media only screen and (min-device-width: 480px){
            .left{
                left: 26%;
            }
        }
        @media only screen and (min-device-width: 768px){
            .left{
                left: 26%;
            }
        }
        .left{
            left: 50%;
        }
        /*#overlayer {
        position: fix;
        z-index: 7100;
        background: rgba(250, 246, 246, 0.61);
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        opacity:1;
        }

        .loader {
        z-index: 7700;
        position: fixed;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        }
        .spinner-border {
            color: #0389ff;
        }
        */
    </style>
    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <div class="relative z-[7700] w-screen h-screen opacity-100" style="background:rgb(62 62 62 / 17%)" id="overlayer">
        <div class="absolute top-1/2 left md:left-1/2 lg:left-1/2">
            <div class="flex">
            <div role="status">
                <svg aria-hidden="true" class="inline w-8 h-8 text-blue-500 animate-spin dark:text-gray-600 fill-blue-60" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg" style="color: #5a5af2cf;">
                    <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                    <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                </svg>
                <span class="sr-only">Loading...</span>
            </div>
            <div role="status">
                <svg aria-hidden="true" class="inline w-8 h-8 text-indigo-300 animate-spin dark:text-gray-600 fill-gray-600 dark:fill-gray-300" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg" style="color:#2525eccf">
                    <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                    <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                </svg>
                <span class="sr-only">Loading...</span>
            </div>
            <div role="status">
                <svg aria-hidden="true" class="inline w-8 h-8 text-gray-200 animate-spin dark:text-gray-600 fill-green-500" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg" style="color:#2525eccf">
                    <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                    <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                </svg>
                <span class="sr-only">Loading...</span>
            </div>
            <div role="status">
                <svg aria-hidden="true" class="inline w-8 h-8 text-gray-200 animate-spin dark:text-gray-600 fill-red-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg" style="color:#2525eccf">
                    <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                    <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                </svg>
                <span class="sr-only">Loading...</span>
            </div>
            <div role="status">
                <svg aria-hidden="true" class="inline w-8 h-8 text-gray-200 animate-spin dark:text-gray-600 fill-yellow-400" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg" style="color:#2525eccf">
                    <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                    <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                </svg>
                <span class="sr-only">Loading...</span>
            </div>
            <div role="status">
                <svg aria-hidden="true" class="inline w-8 h-8 text-gray-200 animate-spin dark:text-gray-600 fill-pink-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg" style="color:#2525eccf">
                    <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                    <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                </svg>
                <span class="sr-only">Loading...</span>
            </div>
            <div role="status">
                <svg aria-hidden="true" class="inline w-8 h-8 text-gray-200 animate-spin dark:text-gray-600 fill-purple-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg" style="color:#2525eccf">
                    <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                    <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                </svg>
                <span class="sr-only">Loading...</span>
            </div>
            </div>
        </div>
    </div>
    <div class="after-load hidden">
        <header class="border-b border-[#9E3D64]">
            <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
        </header>

        {{-- Carousel --}}
        <div class="container mx-auto my-5 sm:rounded-lg swiper carousel-banner">
            <div
                class="swiper-wrapper w-[380px] sm:w-[768px] h-[192px] md:w-[1024px] md:h-[256px] lg:w-[1200px] lg:h-[300px] xl:w-[1536px] xl:h-[384px]">
                @if (!empty($home_image_slider))
                @foreach (json_decode($home_image_slider->image_section) as $index => $image)
                <a href="#" class="swiper-slide">
                    <img class="object-cover object-center w-full h-full px-5 sm:px-0 sm:rounded-lg"
                        src="{{ asset($image) }}" alt="banner-{{ $index }}">
                </a>
                @endforeach
                @else
                <a href="#" class="swiper-slide">
                    <img class="object-cover object-center w-full h-full px-5 sm:px-0 sm:rounded-lg"
                        src="https://images.unsplash.com/photo-1501785888041-af3ef285b470?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                        alt="banner-1">
                </a>
                <a href="#" class="swiper-slide">
                    <img class="object-cover object-center w-full h-full px-5 rounded-lg sm:px-0"
                        src="https://images.unsplash.com/photo-1433838552652-f9a46b332c40?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                        alt="banner-2">
                </a>
                <a href="#" class="swiper-slide">
                    <img class="object-cover object-center w-full h-full px-5 rounded-lg sm:px-0"
                        src="https://images.unsplash.com/photo-1518548419970-58e3b4079ab2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                        alt="banner-3">
                </a>
                <a href="#" class="swiper-slide">
                    <img class="object-cover object-center w-full h-full px-5 rounded-lg sm:px-0"
                        src="https://images.unsplash.com/photo-1555899434-94d1368aa7af?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                        alt="banner-4">
                </a>
                @endif
            </div>
            <div class="pl-3 text-left swiper-pagination"></div>
        </div>

        <x-produk.cari-produk>
            <x-slot name="hotel">
                <select name="search" id="hotel_search_from" required
                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                    <option value="">Dari</option>
                </select>
            </x-slot>
            <x-slot name="xstay">
                <select name="search" id="xstay_search_from" required
                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                    <option value="">Dari</option>
                </select>
            </x-slot>
            <x-slot name="activity">
                <select name="search" id="activity_search_from" required
                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                </select>
            </x-slot>
            <x-slot name="turFrom">
                <select name="search" id="tur-from-select2" required
                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                    <option value="" selected>Dari</option>
                    @foreach ($collect_pin as $pin)
                        <option value="{{$pin['district_id']}}">{{$pin['nama_titik']}}</option>
                    @endforeach
                </select>
            </x-slot>
            <x-slot name="turTo">
                <select name="tur_to" id="tur-to-select2" required
                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                    <option value="" seleted>Ke</option>
                    @foreach ($collect_pin as $pin)
                        <option value="{{$pin['district_id']}}">{{$pin['nama_titik']}}</option>
                    @endforeach
                </select>
            </x-slot>
            <x-slot name="rental">
                <select name="search" id="rental_serch_from" required
                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                    <option value="">Lokasi</option>
                    @if (isset($rental))
                    @endif
                </select>
            </x-slot>
            <x-slot name="transferFrom">
                <select name="search" id="transfer_serch_from" required
                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                    <option value="">Dari</option>
                </select>
            </x-slot>
            <x-slot name="transferTo">
                <select name="to" id="transfer_serch_to" required
                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                    <option value="">Ke</option>
                </select>
            </x-slot>
        </x-produk.cari-produk>
        <hr class="my-10 border-[#9E3D64]">
        {{-- Populer --}}
        {{-- x-data="{ active_populer: ['Tur','Transfer','Rental','Activity','Hotel','Xstay'], index:0}" --}}
    <div class="container mx-3 sm:mx-auto my-3" x-data="filterPopuler()">
        <div class="flex items-center">
            <p class="text-2xl xl:text-5xl font-medium uppercase mr-5">Terbaru</p>
            <img class="w-44 xl:w-auto" src="{{ asset('storage/img/best-selling.png') }}" alt="best-selling">
            {{-- Dropdown Desktop --}}
            <div class="w-full flex justify-end">
                <form>
                    <fieldset>
                        <div
                            class="relative text-gray-500 bg-white shadow-lg border border-[#9E3D64] focus:border-2 focus:border-[#9E3D64] ">
                            {{-- <select class="appearance-none w-full py-2 px-2 bg-white  hidden md:block lg:block"
                                name="produk-dropdown" id="produk-dropdown">
                                <template x-for="(pop, index) in active_popuper" :key="index">
                                    <option :value="pop" x-on:click="active_populer =index" x-text="pop">Paket Tur</option>
                                </template>
                                <option value="Tur" @change="filtered('Tur')">Paket Tur</option>
                                <option value="Transfer" @change="filtered('Transfer')">Transfer</option>
                                <option value="Rental" x-on:click="active_populer = 'Rental'">Rental Kendaraan
                                </option>
                                <option value="Activity" x-on:click="active_populer = 'Activity'">Aktivitas</option>
                                <option value="Hotel" x-on:click="active_populer = 'Hotel'">Hotel</option>
                                <option value="Xstay" x-on:click="active_populer = 'Xstay'">Xstay</option>
                            </select> --}}
                        <div class="flex justify-end hidden md:block lg:block">
                            <div
                            x-on:keydown.escape.prevent.stop="close($refs.button)"
                            x-on:focusin.window="! $refs.panel.contains($event.target) && close()"
                            x-id="['dropdown-button-populer']"
                            {{-- class="relative" --}}
                            >
                                <button
                                    x-ref="button"
                                    x-on:click="toggle()"
                                    :aria-expanded="open"
                                    :aria-controls="$id('dropdown-button-populer')"
                                    type="button"
                                    class="flex items-center gap-2 bg-white px-5 py-2.5 rounded-md shadow"
                                    x-text="active_populer"
                                >
                                    Filter
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                    </svg>
                                </button>
                                <div 
                                    x-ref="panel"
                                    x-show="open"
                                    x-transition.origin.top.left
                                    x-on:click.outside="close($refs.button)"
                                    :id="$id('dropdown-button-populer')"
                                    style="display: none;"
                                    class="absolute left-0 mt-2 w-40 rounded-md bg-white shadow-md z-40"
                                >
                                    <button type="button" @click="filtered('Tur')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                        <span class="text-gray-600">Tur</span>
                                    </button>
                        
                                    <button type="button" @click="filtered('Rental')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                       <span class="text-gray-600">Rental</span>
                                    </button>
                        
                                    <button type="button" @click="filtered('Transfer')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                        <span class="text-gray-600">Transfer</span>
                                    </button>
                                    <button type="button" @click="filtered('Activity')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                        <span class="text-gray-600">Activity</span>
                                    </button>
                                    <button type="button" @click="filtered('Hotel')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                        <span class="text-gray-600">Hotel</span>
                                    </button>
                                    <button type="button" @click="filtered('Xstay')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                        <span class="text-gray-600">Xstay</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        {{-- Dropdown Mobile --}}
        <div class="w-full flex justify-start my-5">
            <form>
                <fieldset>
                    <div
                        class="relative border border-[#9E3D64] text-gray-500 bg-white shadow-lg focus:border-2 focus:border-[#9E3D64] "
                        >
                        {{-- <select class="appearance-none w-full py-2 px-2 bg-white   md:hidden lg:hidden"
                            name="produk-dropdown" id="produk-dropdown">
                            <option value="Tur" x-on:change="active_populer[0] = 'Tur'">Paket Tur</option>
                            <option value="Transfer" x-on:change="active_populer = 'Transfer'">Transfer</option>
                            <option value="Rental" x-on:change="active_populer = 'Rental'">Rental Kendaraan</option>
                            <option value="Activity" x-on:change="active_populer = 'Activity'">Aktivitas</option>
                            <option value="Hotel" x-on:change="active_populer = 'Hotel'">Hotel</option>
                            <option value="Xstay" x-on:change="active_populer = 'Xstay'">Xstay</option>
                        </select> --}}
                        <div class="flex justify-end md:hidden lg:hidden">
                            <div 
                                x-on:keydown.escape.prevent.stop="close($refs.button)"
                                x-on:focusin.window="! $refs.panel.contains($event.target) && close()"
                                x-id="['dropdown-button']"
                                class="relative"
                            >
                                <button
                                    x-ref="button"
                                    x-on:click="toggle()"
                                    :aria-expanded="open"
                                    :aria-controls="$id('dropdown-button')"
                                    type="button"
                                    class="flex items-center gap-2 bg-white px-5 py-2.5 rounded-md shadow"
                                    x-text="active_populer"
                                >
                                    Filter
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                    </svg>
                                </button>
                                <div class="absolute left-0 mt-2 w-40 rounded-md bg-white shadow-md z-50"
                                    x-ref="panel"
                                    x-show="open"
                                    x-transition.origin.top.left
                                    x-on:click.outside="close($refs.button)"
                                    :id="$id('dropdown-button')"
                                    style="display: none;"
                                >
                                    <button type="button" @click="filtered('Tur')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                        <span class="text-gray-600">Tur</span>
                                    </button>
                        
                                    <button type="button" @click="filtered('Rental')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                       <span class="text-gray-600">Rental</span>
                                    </button>
                        
                                    <button type="button" @click="filtered('Transfer')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                        <span class="text-gray-600">Transfer</span>
                                    </button>
                                    <button type="button" @click="filtered('Activity')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                        <span class="text-gray-600">Activity</span>
                                    </button>
                                    <button type="button" @click="filtered('Hotel')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                        <span class="text-gray-600">Hotel</span>
                                    </button>
                                    <button type="button" @click="filtered('Xstay')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                        <span class="text-gray-600">Xstay</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <div>
            <div x-show="active_populer === 'Tur'" class="swiper populerSwiper">
                <div class="swiper-wrapper">
                    @if (count($tur_populer) > 0)
                    @foreach ($tur_populer as $act)
                    <div class="flex justify-center swiper-slide">
                        <x-produk.card-populer-home-new :act="$act" :type="'tur'" />
                    </div>
                    @endforeach
                    @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Produk Terbaru tidak ditemukan
                    </div>
                    @endif
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination -mb-2"></div>
            </div>
            <div x-show="active_populer === 'Transfer'" class="swiper populerSwiper">
                <div class="swiper-wrapper">
                    @if (count($transfer_populer) > 0)
                    @foreach ($transfer_populer as $act)
                    <div class="flex justify-center swiper-slide">
                        <x-produk.card-populer-home-new :act="$act" :type="'transfer'" />
                    </div>
                    @endforeach
                    @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Produk Terbaru tidak ditemukan
                    </div>
                    @endif
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination -mb-2"></div>
            </div>
            <div x-show="active_populer === 'Rental'" class="swiper populerSwiper">
                <div class="swiper-wrapper">
                    @if (count($rental_populer) > 0)
                    @foreach ($rental_populer as $act)
                    test
                    {{-- <div class="flex justify-center swiper-slide">
                        <x-produk.card-populer-home-new :act="$act" :type="'rental'" />
                    </div> --}}
                    @endforeach
                    @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Produk Terbaru tidak ditemukan
                    </div>
                    @endif
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination -mb-2"></div>
            </div>
            <div x-show="active_populer === 'Activity'" class="swiper populerSwiper">
                <div class="swiper-wrapper">
                    @if (count($activity_populer) > 0)
                    @foreach ($activity_populer as $act)
                    <div class="flex justify-center swiper-slide">
                        <x-produk.card-populer-home-new :act="$act" :type="'activity'" />
                    </div>
                    @endforeach
                    @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Produk Terbaru tidak ditemukan
                    </div>
                    @endif
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination -mb-2"></div>
            </div>
            <div x-show="active_populer === 'Hotel'" class="swiper populerSwiper">
                <div class="swiper-wrapper">
                    @if (count($hotel_populer) > 0)
                    @foreach ($hotel_populer as $act)
                    <div class="flex justify-center swiper-slide">
                        <x-produk.card-populer-home-new :act="$act" :type="'hotel'" />
                    </div>
                    @endforeach
                    @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Produk Terbaru tidak ditemukan
                    </div>
                    @endif
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination -mb-2"></div>
            </div>
            <div x-show="active_populer === 'Xstay'" class="swiper populerSwiper">
                <div class="swiper-wrapper">
                    @if ($xstay_populer != null)
                    @foreach ($xstay_populer as $act)
                    <div class="flex justify-center swiper-slide">
                        <x-produk.card-populer-home-new :act="$act" :type="'xstay'" />
                    </div>
                    @endforeach
                    @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Produk Terbaru tidak ditemukan
                    </div>
                    @endif
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination -mb-2"></div>
            </div>
            {{-- <a href="#" class="flex justify-center sm:justify-end text-xl text-[#9E3D64] font-bold">Lihat semua</a>
        --}}
        </div>
    </div>
        <hr class="my-10 border-[#9E3D64]">
        {{-- Kamtuu About #1 --}}
        <div class="container block p-5 mx-auto my-5 border-2 border-gray-300 rounded-lg md:flex ">
            <div class="grid p-4 my-auto justify-items-center">
                <img class="w-2/6 md:w-full" src="{{ asset('storage/img/homepage-1.png') }}" alt="homepage-1">
                <p class="text-center text-xl xl:text-4xl font-semibold text-[#9E3D64]">MENGAPA BOOKING</p>
                <p class="my-2 text-2xl text-center xl:text-5xl">LEWAT KAMTUU?</p>
            </div>
            {{-- Desktop --}}
            <div class="hidden md:block lg:block" x-data="konten()">
                <table class="hidden text-center table-auto xl:text-justify md:block lg:block">
                    <thead>
                        <tr>
                            {{-- @foreach ($kontens as $konten) --}}
                            <template x-if="contents.length >= 1">
                                <template x-for="(content, index) contents">
                                <th>
                                    {{-- {{ Storage::url($konten->image_section) }} --}}
                                    <img class="w-3/4 mx-auto md:w-40" :src="content.img"
                                        alt="homepage-2">
                                </th>
                                </template>
                            </template>
                            {{-- @endforeach --}}
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            {{-- @foreach ($kontens as $konten) --}}
                            <template x-if="contents.length >= 1">
                                <template x-for="(content, index) contents">
                                <td class="px-4 align-middle xl:align-bottom">
                                    <p class="text-lg lg:text-xl text-center xl:text-left font-semibold text-[#9E3D64]" x-text="content.judul">
                                        {{-- {{ $konten->title_section }} --}}
                                    </p>
                                </td>
                                </template>
                            </template>
                            {{-- @endforeach --}}
                        </tr>
                        <tr>
                            <template x-if="contents.length >= 1">
                                <template x-for="(content, index) contents">
                                <td class="px-4 align-top"  x-text="content.detail">
                                    {{-- {!! $konten->detail_section !!} --}}
                                </td>
                                </template>
                            </template>
                        </tr>
                    </tbody>
                </table>
            </div>
            {{-- Mobile Tab --}}
            <div class=" md:hidden lg:hidden">
                <div class="grid grid-cols-1">
                    <div>
                        <img class="w-1/4 mx-auto md:w-40" src="{{ asset('storage/img/homepage-2.png') }}" alt="homepage-2">
                        <p class="text-sm lg:text-xl text-center xl:text-left font-semibold text-[#9E3D64]">Lebih
                            Banyak Pilihan
                        </p>
                        <p class="my-2 text-sm font-medium text-center lg:text-base">Dari attraksi terkenal hingga surga
                            tersembunyi, pilihan
                            lebih banyak, dijamin lebih puas!</p>
                    </div>
                    <div>
                        <img class="w-1/4 mx-auto md:w-40" src="{{ asset('storage/img/homepage-3.png') }}" alt="homepage-3">
                        <p class="text-sm lg:text-xl text-center xl:text-left font-semibold text-[#9E3D64]">Transaksi
                            Aman</p>
                        <p class="my-2 text-sm font-medium text-center lg:text-base">Bagian anda adalah menikmati, bagian
                            kami
                            adalah membuat
                            transaksi anda aman!</p>
                    </div>
                    <div>
                        <img class="w-1/4 mx-auto md:w-40" src="{{ asset('storage/img/homepage-4.png') }}" alt="homepage-4">
                        <p class="text-sm lg:text-xl text-center xl:text-left font-semibold text-[#9E3D64]">Harga
                            Terbaik</p>
                        <p class="my-2 text-sm font-medium text-center lg:text-base">Pilih sesuai budget, jadikan liburan
                            anda tak
                            ternilai
                            harganya!!</p>
                    </div>
                    <div>
                        <img class="w-1/4 mx-auto md:w-40" src="{{ asset('storage/img/homepage-5.png') }}" alt="homepage-5">
                        <p class="text-sm lg:text-xl text-center xl:text-left font-semibold text-[#9E3D64]">Support 24/7
                        </p>
                        <p class="my-2 text-sm font-medium text-center lg:text-base">Kami berbagi kegembiraan anda, kami
                            mendukung
                            anda
                            sepanjang perjalanan!</p>
                    </div>
                </div>
            </div>
        </div>

        {{-- Official Stores --}}
        <div class="relative w-full my-20" x-data="slider()" id="official_store_info">
            <div
                class="absolute kamtuu-background bg-[#9E3D64] w-[150px]  md:w-[450px]  lg:w-[450px] h-full px-10 rounded-r-2xl lg:text-left mx-auto grid content-center text-white text-center">
                <img src="{{ asset('storage/img/homepage-6.png') }}" alt="homepage-6">
                <p class="mt-5 text-xl lg:text-5xl md:text-4xl">OFFICIAL</p>
                <p class="mb-10 text-sm lg:text-5xl md:text-4xl">STORES</p>
                <a class="text-sm lg:text-5xl md:text-4xl" href=" #">Lihat <br> Semua</a>
            </div>
            <div class="static flex overflow-x-auto bg-[#FFB800] bg-clip-content"
                @scroll.debounce="updateIndex($event.target)">
                <div class="flex-none mx-3 my-10 w-[250px]">
                </div>
                @foreach (json_decode($sellers) as $seller)
                <x-homepage.card-stores :seller="$seller"></x-homepage.card-stores>
                @endforeach
                <a href="#" class="absolute z-10 translate-y-1/2 bottom-1/2 right-5">
                    <img src="{{ asset('storage/icons/chevron-right-circle.svg') }}" alt="">
                </a>
            </div>
        </div>

        <hr class="my-10 border-[#9E3D64]">

        {{-- Sedang Promo --}}
        <div class="container mx-3 my-3 sm:mx-auto" x-data="filterPromo()">
            <div class="flex items-center">
                <p class="mr-5 text-2xl font-medium uppercase xl:text-5xl">Sedang Promo</p>
                <img class="w-44 xl:w-auto" src="{{ asset('storage/img/sedang-promo.png') }}" alt="sedang-promo">
                {{-- Dropdown Desktop --}}
                <div class="flex justify-end w-full">
                    <form>
                        <fieldset>
                            <div
                                class="relative border border-[#9E3D64] text-gray-500 bg-white shadow-lg focus:border-2 focus:border-[#9E3D64]">
                                {{-- <select class="hidden w-full px-2 py-2 bg-white appearance-none md:block lg:block"
                                    name="produk_promo" id="produk_promo">
                                    <option value="Tur" x-on:click="active = 'Tur'">Paket Tur</option>
                                    <option value="Transfer" x-on:click="active = 'Transfer'">Transfer</option>
                                    <option value="Rental" x-on:click="active = 'Rental'">Rental Kendaraan</option>
                                    <option value="Activity" x-on:click="active = 'Activity'">Aktivitas</option>
                                    <option value="Hotel" x-on:click="active = 'Hotel'">Hotel</option>
                                    <option value="Xstay" x-on:click="active = 'Xstay'">Xstay</option>
                                </select> 
                                --}}
                                <div class="flex justify-end hidden md:block lg:block">
                                    <div 
                                        x-on:keydown.escape.prevent.stop="close($refs.button)"
                                        x-on:focusin.window="! $refs.panel.contains($event.target) && close()"
                                        x-id="['dropdown-button-promo-mobile']"
                                        class="relative"
                                    >
                                        <button
                                            x-ref="button"
                                            x-on:click="toggle()"
                                            :aria-expanded="open"
                                            :aria-controls="$id('dropdown-button-promo-mobile')"
                                            type="button"
                                            class="flex items-center gap-2 bg-white px-5 py-2.5 rounded-md shadow"
                                             x-text=active
                                        >
                                            Filter
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                                                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                            </svg>
                                        </button>
                                        <div class="absolute left-0 mt-2 w-40 rounded-md bg-white shadow-md z-50"
                                            x-ref="panel"
                                            x-show="open"
                                            x-transition.origin.top.left
                                            x-on:click.outside="close($refs.button)"
                                            :id="$id('dropdown-button-promo-mobile')"
                                            style="display: none;"
                                        >
                                            <button type="button" @click="filtered('Tur')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                                <span class="text-gray-600">Tur</span>
                                            </button>
                                
                                            <button type="button" @click="filtered('Rental')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                            <span class="text-gray-600">Rental</span>
                                            </button>
                                
                                            <button type="button" @click="filtered('Transfer')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                                <span class="text-gray-600">Transfer</span>
                                            </button>
                                            <button type="button" @click="filtered('Activity')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                                <span class="text-gray-600">Activity</span>
                                            </button>
                                            <button type="button" @click="filtered('Hotel')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                                <span class="text-gray-600">Hotel</span>
                                            </button>
                                            <button type="button" @click="filtered('Xstay')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                                <span class="text-gray-600">Xstay</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            {{-- Dropdown Desktop --}}
            <div class="flex justify-start w-full my-5">
                <form>
                    <fieldset>
                        <div
                            class="relative border border-[#9E3D64] text-gray-500 bg-white shadow-lg focus:border-2 focus:border-[#9E3D64]">
                            {{-- <select class="w-full px-2 py-2 bg-white appearance-none md:hidden lg:hidden"
                                name="produk_promo" id="produk_promo">
                                <option value="Tur" x-on:click="active = 'Tur'">Paket Tur</option>
                                <option value="Transfer" x-on:click="active = 'Transfer'">Transfer</option>
                                <option value="Rental" x-on:click="active = 'Rental'">Rental Kendaraan</option>
                                <option value="Activity" x-on:click="active = 'Activity'">Aktivitas</option>
                                <option value="Hotel" x-on:click="active = 'Hotel'">Hotel</option>
                                <option value="Xstay" x-on:click="active = 'Xstay'">Xstay</option>
                            </select> --}}
                            <div class="flex justify-end md:hidden lg:hidden">
                                    <div 
                                        x-on:keydown.escape.prevent.stop="close($refs.button)"
                                        x-on:focusin.window="! $refs.panel.contains($event.target) && close()"
                                        x-id="['dropdown-button-promo-mobile']"
                                        class="relative"
                                    >
                                        <button
                                            x-ref="button"
                                            x-on:click="toggle()"
                                            :aria-expanded="open"
                                            :aria-controls="$id('dropdown-button-promo-mobile')"
                                            type="button"
                                            class="flex items-center gap-2 bg-white px-5 py-2.5 rounded-md shadow"
                                            x-text=active
                                        >
                                            Filter
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                                                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                            </svg>
                                        </button>
                                        <div class="absolute left-0 mt-2 w-40 rounded-md bg-white shadow-md z-50"
                                            x-ref="panel"
                                            x-show="open"
                                            x-transition.origin.top.left
                                            x-on:click.outside="close($refs.button)"
                                            :id="$id('dropdown-button-promo-mobile')"
                                            style="display: none;"
                                        >
                                            <button type="button" @click="filtered('Tur')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                                <span class="text-gray-600">Tur</span>
                                            </button>
                                
                                            <button type="button" @click="filtered('Rental')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                            <span class="text-gray-600">Rental</span>
                                            </button>
                                
                                            <button type="button" @click="filtered('Transfer')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                                <span class="text-gray-600">Transfer</span>
                                            </button>
                                            <button type="button" @click="filtered('Activity')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                                <span class="text-gray-600">Activity</span>
                                            </button>
                                            <button type="button" @click="filtered('Hotel')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                                <span class="text-gray-600">Hotel</span>
                                            </button>
                                            <button type="button" @click="filtered('Xstay')" class="flex items-center gap-2 w-full first-of-type:rounded-t-md last-of-type:rounded-b-md px-4 py-2.5 text-left text-sm hover:bg-gray-50 disabled:text-gray-500">
                                                <span class="text-gray-600">Xstay</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </fieldset>
                </form>
            </div>

            <div x-show="active === 'Tur'" class="swiper productSwiper">
                <div class="swiper-wrapper">
                    @if (count($tur) > 0)
                    @foreach ($tur as $act)
                    <div class="flex justify-center swiper-slide">
                        {{-- <x-destindonesia.card-produk-baru :act="$act" :type="'tur'" /> --}}
                    </div>
                    @endforeach
                    @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Promo tidak ditemukan
                    </div>
                    @endif
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="-mb-2 swiper-pagination"></div>
            </div>
            <div x-show="active === 'Transfer'" class="swiper productSwiper">
                <div class="swiper-wrapper">
                    @if ($transfer != null)
                    @foreach ($transfer as $act)
                    <div class="flex justify-center swiper-slide">
                        <x-destindonesia.card-produk-baru :act="$act" :type="'transfer'" />
                    </div>
                    @endforeach
                    @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Promo tidak ditemukan
                    </div>
                    @endif
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="-mb-2 swiper-pagination"></div>
            </div>
            <div x-show="active === 'Rental'" class="swiper productSwiper">
                <div class="swiper-wrapper">
                    @if ($rental != null)
                    @foreach ($rental as $act)
                    <div class="flex justify-center swiper-slide">
                        <x-destindonesia.card-produk-baru x-lazy :act="$act" :type="'rental'" />
                    </div>
                    @endforeach
                    @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Promo tidak ditemukan
                    </div>
                    @endif
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="-mb-2 swiper-pagination"></div>
            </div>
            <div x-show="active === 'Activity'" class="swiper productSwiper">
                <div class="swiper-wrapper">
                    @if ($activity != null)
                    @foreach ($activity as $act)
                    <div class="flex justify-center swiper-slide">
                        <x-destindonesia.card-produk-baru x-lazy :act="$act" :type="'activity'" />
                    </div>
                    @endforeach
                    @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Promo tidak ditemukan
                    </div>
                    @endif
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="-mb-2 swiper-pagination"></div>
            </div>
            <div x-show="active === 'Hotel'" class="swiper productSwiper">
                <div class="swiper-wrapper">
                    @if ($hotel != null)
                    @foreach ($hotel as $act)
                    <div class="flex justify-center swiper-slide">
                        <x-destindonesia.card-produk-baru x-lazy :act="$act" :type="'hotel'" />
                    </div>
                    @endforeach
                    @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Promo tidak ditemukan
                    </div>
                    @endif
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="-mb-2 swiper-pagination"></div>
            </div>
            <div x-show="active === 'Xstay'" class="swiper productSwiper">
                <div class="swiper-wrapper">
                    @if ($xstay != null)
                    @foreach ($xstay as $act)
                    <div class="flex justify-center swiper-slide">
                        <x-destindonesia.card-produk-baru x-lazy :act="$act" :type="'xstay'" />
                    </div>
                    @endforeach
                    @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Promo tidak ditemukan
                    </div>
                    @endif
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="-mb-2 swiper-pagination"></div>
            </div>
            {{-- <a href="#" class="flex justify-center md:justify-end text-xl text-[#9E3D64] font-bold">Lihat semua</a>
            --}}
        </div>

        <hr class="my-10 border-[#9E3D64]">

        {{-- Top Destinasi --}}
        <div class="my-10 bg-[#9E3D64] p-5">
            <div class="container mx-auto">
                <p class="pb-5 text-4xl font-semibold text-white md:text-5xl">TOP DESTINASI</p>
                <div class="grid grid-cols-2 gap-5 md:grid-cols-3 lg:grid-cols-3" id="top-destination">

                </div>
            </div>
        </div>

        {{-- Banner Kamtuu --}}
        <div class="container mx-auto rounded-lg">
            <div class="mx-5 block md:flex bg-[#FFB800] rounded-xl">
                <img class="object-cover mx-auto w-44 sm:w-1/2 md:w-1/2 lg:w-auto md:mx-0"
                    src="{{ asset('storage/img/homepage-7.png') }}" alt="homepage-7">
                <div class="grid content-center mx-2 my-5 text-center md:text-left">
                    <p class="text-xl font-semibold lg:text-3xl">Masih belum nemu paket yang sesuai?</p>
                    <p class="mt-1 mb-5 text-xl font-semibold lg:text-3xl lg:mt-3">Pengen dibuatkan khusus?</p>

                    <x-destindonesia.button-primary text="Minta dibuatkan paket khusus" url="{{route('inboxChatTraveller',[1,isset(auth()->user()->id) ? auth()->user()->id :'traveller'])}}">
                        @slot('button')
                        Minta dibuatkan paket khusus
                        @endslot
                    </x-destindonesia.button-primary>
                </div>
            </div>
        </div>

        <hr class="my-10 border-[#9E3D64]">

        {{-- Kamtuu Tabs --}}
        <div class="container mx-auto" x-data="kabupaten_list()">
            <p class="mx-2 text-2xl font-medium">Ke mana saja di Indonesia, pencarian dimulai di Kamtuu!</p>

            {{-- Tabs --}}
            <div class="flex sm:block">
                <ul
                    class="flex justify-start overflow-x-auto text-sm font-medium text-center text-gray-500 sm:text-left dark:text-gray-400">
                    <template x-for="(tab, index) in tabs" :key="index">
                        <li class="mt-2 mb-2" :class="{ 'border-b-2 border-[#9E3D64]': active == index }"
                            x-on:click="active = index">
                            <button class="inline-block py-2 text-xl rounded-lg px-9" :class="{
                                    'text-[#9E3D64] bg-white font-bold': active ==
                                        index,
                                    'text-black font-normal hover:text-[#9E3D64]': active != index
                                }" x-text="tab" @click="getList(index)"></button>
                        </li>
                    </template>
                </ul>
            </div>

            {{-- List --}}
            <div>
                <div x-show="active === 0" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
                    @if(isset($turRegions))
                    @foreach($turRegions as $turRegion)
                    <a href="{{route('list.tur',['name'=>$turRegion['name']])}}"
                        class="m-2 hover:text-[#9E3D64] duration-200">Paket tur ke {{$turRegion['name']}}</a>
                    @endforeach
                    @endif
                    <a href="#" class="m-2 hover:text-[#9E3D64] duration-200">Paket tur belum tersedia</a>
                </div>
                <div x-show="active === 1" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
                    <template x-if="list.length >= 0">
                        <template x-for="(item,index) in list" :key="index">
                            <a x-bind:href="`list/transfer/${item.name}`"
                                class="m-2 hover:text-[#9E3D64] duration-200" x-text="item.name">
                            </a>
                        </template>
                    </template>
                    <template x-if="list.length == 0">
                    <a href="#" class="m-2 hover:text-[#9E3D64] duration-200">Transfer ke belum tersedia</a>
                    </template>
                </div>
                <div x-show="active === 2" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
                     <template x-if="list.length >= 0">
                        <template x-for="(item,index) in list" :key="index">
                            <a x-bind:href="`list/rental/${item.name}`"
                                class="m-2 hover:text-[#9E3D64] duration-200" x-text="item.name">
                            </a>
                        </template>
                    </template>
                    <template x-if="list.length == 0">
                    <a href="#" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan belum tersedia</a>
                    </template>
                    {{-- @if(isset($rentalRegions))
                    @foreach($rentalRegions as $rentalRegion)
                    <a href="{{route('list.rental',['name'=>$rentalRegion['name']])}}"
                        class="m-2 hover:text-[#9E3D64] duration-200">Kendaraan {{$rentalRegion['name']}}</a>
                    @endforeach
                    @endif
                    <a href="#" class="m-2 hover:text-[#9E3D64] duration-200">Rental Kendaraan belum tersedia</a> --}}
                </div>
                <div x-show="active === 3" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
                    <template x-if="list.length >= 0">
                    <template x-for="(item,index) in list" :key="index">
                        <a href=""
                        class="m-2 hover:text-[#9E3D64] duration-200" x-text="'Aktivitas di '+item.name"></a>
                    </template>
                    </template>
                    <template x-if="list.length == 0">
                        <a href="#" class="m-2 hover:text-[#9E3D64] duration-200">Aktivitas belum tersedia</a>
                    </template>
                </div>
                <div x-show="active === 4" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
                    <template x-if="list.length >= 0">
                        <template x-for="(item,index) in list" :key="index">
                            <a href=""
                            class="m-2 hover:text-[#9E3D64] duration-200" x-text="item.name"></a>
                        </template>
                    </template>
                    <template x-if="list.length == 0">
                    <a href="#" class="m-2 hover:text-[#9E3D64] duration-200">Hotel belum tersedia</a>
                    </template>
                </div>
                <div x-show="active === 5" class="grid grid-cols-4 text-sm md:text-lg lg:text-xl">
                    <template x-if="list.length >= 0">
                        <template x-for="(item,index) in list" :key="index">
                            <a href=""
                            class="m-2 hover:text-[#9E3D64] duration-200" x-text="item.name"></a>
                        </template>
                    </template>
                    <template x-if="list.length == 0">
                    <a href="#" class="m-2 hover:text-[#9E3D64] duration-200">Xstay belum tersedia</a>
                    </template>
                </div>
            </div>
        </div>

        <hr class="my-10 border-[#9E3D64]">

        {{-- Kamtuu Description #1 --}}
        <div class="container p-3 mx-auto">
            <p class="text-2xl font-medium text-center sm:text-left">Ke mana saja di Indonesia, pencarian dimulai
                di Kamtuu!</p>
            <div class="grid justify-items-center sm:grid-cols-12">
                <div class="my-5 sm:col-span-9">
                    <p class="text-lg text-center sm:text-xl sm:text-left">Daftar sebagai Seller di Kamtuu dan dapatkan
                        akses ke pasar wisatawan
                        domestik dan
                        mancanegara...</p>
                    <div class="flex justify-center">
                        <x-destindonesia.button-primary url="{{ route('RegisterSeller') }}" text="Daftar Sebagai Seller">
                            @slot('button')
                            Daftar Sebagai Seller
                            @endslot
                        </x-destindonesia.button-primary>
                    </div>
                </div>
                <img class="order-first sm:order-last sm:col-span-3" src="{{ asset('storage/img/homepage-8.png') }}"
                    alt="homepage-8">
            </div>
        </div>

        <hr class="my-10 border-[#9E3D64]">

        {{-- Kamtuu Description #2 --}}
        <div class="container p-3 mx-auto">
            <p class="text-2xl font-medium text-center sm:text-left">Ingin brand Travel Ageny anda menjadi lebih dikenal?
            </p>
            <div class="grid justify-items-center sm:grid-cols-12">
                <div class="my-5 sm:col-span-9">
                    <p class="text-lg text-center sm:text-xl sm:text-left">Buka Official Store di Kamtuu agar keberadaan
                        brand anda secara online
                        lebih kuat dan
                        lebih menancap di benar para wisatawan. </p>
                    <div class="flex justify-center">
                        <x-destindonesia.button-primary url="{{ route('RegisterSeller') }}" text="Buka Official Store" url="{{route('seller.profilesetting','seller')}}">
                            @slot('button')
                            Buka Official Store
                            @endslot
                        </x-destindonesia.button-primary>
                    </div>
                </div>
                <img class="order-first sm:order-last sm:col-span-3" src="{{ asset('storage/img/homepage-9.png') }}"
                    alt="homepage-9">
            </div>
        </div>

        <hr class="my-10 border-[#9E3D64]">

        {{-- Kamtuu Footer #1 --}}
        <div class="container mx-auto mb-10">
            <div class="grid grid-cols-3 sm:grid-cols-4">
                <div class="p-3">
                    <p class="text-base font-medium sm:text-xl lg:text-2xl">Kamtuu</p>
                    @foreach ($kamtuuSets as $kamtuu)
                    <a href="{{ route('section.show', $kamtuu->id) }}"
                        class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                        {{ $kamtuu->title_section }}</a>
                    @endforeach
                </div>
                <div class="p-3">
                    <p class="text-base font-medium sm:text-xl lg:text-2xl">Untuk Traveler</p>
                    @foreach ($travellerSets as $kamtuu)
                    <a href="{{ route('section.show', $kamtuu->id) }}"
                        class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                        {{ $kamtuu->title_section }}</a>
                    @endforeach
                </div>
                <div class="p-3">
                    <p class="text-base font-medium sm:text-xl lg:text-2xl">Untuk Seller</p>
                    @foreach ($sellerSets as $kamtuu)
                    <a href="{{ route('section.show', $kamtuu->id) }}"
                        class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                        {{ $kamtuu->title_section }}</a>
                    @endforeach
                </div>
                <div class="p-3">
                    <p class="text-base font-medium sm:text-xl lg:text-2xl">Untuk Agen</p>
                    @foreach ($agenSets as $kamtuu)
                    <a href="{{ route('section.show', $kamtuu->id) }}"
                        class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                        {{ $kamtuu->title_section }}</a>
                    @endforeach
                </div>
                <div class="p-3">
                    <p class="text-base font-medium sm:text-xl lg:text-2xl">Official Store</p>
                    @foreach ($storeSets as $kamtuu)
                    <a href="{{ route('section.show', $kamtuu->id) }}"
                        class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                        {{ $kamtuu->title_section }}</a>
                    @endforeach
                </div>
                <div class="p-3">
                    <p class="text-base font-medium sm:text-xl lg:text-2xl">Untuk Pelanggan Corporate</p>
                    @foreach ($corporateSets as $kamtuu)
                    <a href="{{ route('section.show', $kamtuu->id) }}"
                        class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                        {{ $kamtuu->title_section }}</a>
                    @endforeach
                </div>
                <div class="p-3">
                    <p class="text-base font-medium sm:text-xl lg:text-2xl">Berbisnis Dengan Kamtuu</p>
                    @foreach ($kerjasamaSets as $kamtuu)
                    <a href="{{ route('section.show', $kamtuu->id) }}"
                        class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                        {{ $kamtuu->title_section }}</a>
                    @endforeach
                </div>
                <div class="p-3">
                    <p class="text-base font-medium sm:text-xl lg:text-2xl">Layanan</p>
                    @foreach ($layananSets as $kamtuu)
                    <a href="{{ route('section.show', $kamtuu->id) }}"
                        class="text-sm sm:text-base lg:text-xl font-normal my-1 block hover:text-[#9E3D64] duration-200">
                        {{ $kamtuu->title_section }}</a>
                    @endforeach
                </div>
            </div>
        </div>       
        <div class="dialog"></div>
        <footer>
            <x-destindonesia.footer></x-destindonesia.footer>
        </footer>
    </div>
</body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>

       $("#overlayer").delay(1000).fadeOut("slow")
       $(".after-load").delay(50).fadeIn("fast")

        var swiper = new Swiper(".productSwiper", {
            slidesPerView: 1,
            loop: true,
            navigation: {
                enabled: true,
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    navigation: {
                        enabled: true
                    },
                    pagination: {
                        enabled: false
                    }
                },
                1024: {
                    slidesPerView: 3,
                    navigation: {
                        enabled: false
                    },
                    pagination: {
                        enabled: true
                    }
                },
                1280: {
                    slidesPerView: 5,
                    navigation: {
                        enabled: false,
                    },
                    pagination: {
                        enabled: false
                    }
                }
            },
        });

        var swiperPopuler = new Swiper(".populerSwiper", {
            slidesPerView: 1,
            loop: true,
            navigation: {
                enabled: true,
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    navigation: {
                        enabled: true
                    },
                    pagination: {
                        enabled: false
                    }
                },
                1024: {
                    slidesPerView: 3,
                    navigation: {
                        enabled: false
                    },
                    pagination: {
                        enabled: true
                    }
                },
                1280: {
                    slidesPerView: 5,
                    navigation: {
                        enabled: false,
                    },
                    pagination: {
                        enabled: false
                    }
                }
            },
        });

        var bannerSwiper = new Swiper(".carousel-banner", {
            loop: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
                enabled: false,
            },
            autoplay: {
                delay: 3000,
                pauseOnMouseEnter: true,
                disableOnInteraction: false,
            },
            breakpoints: {
                640: {
                    pagination: {
                        enabled: true
                    }
                }
            }
        });
        
        function submitLike2(id){
            $.ajax({
                url: '/like',
                method: 'POST',
                dataType: 'json',
                cache: false,
                data: {
                    '_token': $("meta[name='csrf-token']").attr("content"),
                    'product_id': id
                },
                success(resp) {
                    if (resp.success) {
                        if (resp.data.status == 1) {
                            $("." + id).addClass("fill-red-500");
                            $("#" + id).addClass("hover:fill-black");
                        } else {
                            $("." + id).addClass("hover:fill-red-500");
                            $("." + id).removeClass("fill-red-500");
                            $("." + id).addClass("text-white");
                        }
                    } else {
                        alert(resp.message);
                    }
                },
                failure(resp) {
                    console.log('error')
                }
            });
        }

        document.addEventListener("alpine:init",()=>{
            Alpine.data("kabupaten_list",()=>({
                active: 0,
                tabs: ['Paket Tur', 'Transfer', 'Rental Kendaraan', 'Aktivitas', 'Hotel', 'Xstay'],
                list:[],
                getList(index){
                    if(this.list.length > 0){
                        this.list = [];
                    }

                    let type =null;

                    if(index == 0){
                        type = 'tour';
                    }
                    else if(index == 1){
                        type ='transfer'
                    }
                    else if(index == 2){
                        type ='rental'
                    }
                    else if(index == 3){
                        type ='activity'
                    }
                    else if(index == 4){
                        type ='hotel'
                    }
                    else if(index == 5){
                        type ='xstay'
                    }
                    fetch(`product/ListRegency/${type}?mode=api`)
                    .then(resp => resp.json())
                    .then(data=>{
                        let obj = data.data

                        Object.keys(obj).forEach(key=>{
                            this.list.push(obj[key])
                        })

                        console.log(this.list)
                    })
                   
                }
            }))
        })

        function filterPopuler(){
            return{
                active_populer:'Tur',
                index:0,
                open: false,
                filtered(product){
                    //alert('test');
                    this.active_populer=product
                    this.open = false
                },
                toggle() {
                    if (this.open) {
                        return this.close()
                    }

                    this.$refs.button.focus()

                    this.open = true
                },
                close(focusAfter) {
                    if (! this.open) return
    
                    this.open = false
    
                    focusAfter && focusAfter.focus()
                }
            }
        }

        function filterPromo(){
            return{
                active:'Activity',
                index:0,
                open: false,
                filtered(product){
                    //alert('test');
                    this.active=product
                    this.open = false
                },
                toggle() {
                    if (this.open) {
                        return this.close()
                    }

                    this.$refs.button.focus()

                    this.open = true
                },
                close(focusAfter) {
                    if (! this.open) return
    
                    this.open = false
    
                    focusAfter && focusAfter.focus()
                }
            }
        }

        /*
        function kabupaten_list(){
           return{
                active: 0,
                tabs: ['Paket Tur', 'Transfer', 'Rental Kendaraan', 'Aktivitas', 'Hotel', 'Xstay'],
                list:[],
                getList(index){
                    if(this.list.length > 0){
                        this.list = [];
                    }

                    let type =null;

                    if(index == 0){
                        type = 'tour';
                    }
                    else if(index == 1){
                        type ='transfer'
                    }
                    else if(index == 2){
                        type ='rental'
                    }
                    else if(index == 3){
                        type ='activity'
                    }
                    else if(index == 4){
                        type ='hotel'
                    }
                    else if(index == 5){
                        type ='xstay'
                    }
                    fetch(`product/ListRegency/${type}?mode=api`)
                    .then(resp => resp.json())
                    .then(data=>{
                        let obj = data.data

                        Object.keys(obj).forEach(key=>{
                            this.list.push(obj[key])
                        })

                        console.log(this.list)
                    })
                   
                }
            }
        }
        */

        /*$(window).scroll(function() {
            //console.log($(window).scrollTop())
            //console.log($(window).height())
            
            //console.log($("#official_store_info").height())
            //console.log($(window).scrollTop()-1664)
            let scrool_official = $(window).scrollTop()-1664;

            if(scrool_official == 209 || scrool_official==456){
                
            }

            if(scrool_official >= 1236){
               
            }

        })
        */

        //get kontents by alpin jsdeliv
        document.addEventListener("alpine:init",()=>{
            Alpine.data("konten",()=>({
                contents:[],
                url:"{{route('home.content')}}",
                async init(){
                    let loads = await fetch(`${this.url}`);
                    let objs  = await loads.json();

                    console.log(objs)

                    this.content = objs.data
                }
            }))
        })
        
        document.addEventListener("alpine:init",()=>{
            Alpine.data("kamtuusets",()=>({
                contents:[],
                url:"{{route('home.content')}}",
                async init(){
                    let loads = await fetch(`${this.url}`);
                    let objs  = await loads.json();

                    console.log(objs)

                    this.content = objs.data
                }
            }))
        })

    </script>
    <script>
        console.log($("#tur-from-select2"))
        $("#tur-from-select2").select2({width: '100%'})
        $("#tur-to-select2").select2({width: '100%'})
        $("#rental_serch_from").select2({width: '100%'})
        $('#transfer_serch_from').select2({width: '100%'})
        $('#transfer_serch_to').select2({width: '100%'})
        $('#hotel_search_from').select2({width: '100%'})
        $('#xstay_search_from').select2({width: '100%'})
        $('#activity_search_from').select2({width: '100%'})
    </script>

</html>