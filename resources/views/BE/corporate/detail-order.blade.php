<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #report:checked+#report {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">

    {{-- Navbar --}}
    <x-be.corporate.navbar-corporate></x-be.corporate.navbar-corporate>

    {{-- Sidebar --}}
    <x-be.corporate.sidebar-corporate></x-be.corporate.sidebar-corporate>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pr-10" style="padding-left: 315px">
            <a href="/dashboard-agent/my-orders">
                <div class="flex">
                    <img id="image" src="{{ asset('storage/icons/chevron-left-arrow.svg') }}"
                        class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    <label for="image" class="mx-3">Detail Pemesanan</label>
                </div>
            </a>

            <div class="bg-gray-200 rounded-md border border-gray-400 max-w-6xl p-5">
                <div class="grid grid-cols-[10%_80%_10%]">
                    <div class="grid justify-items-center">
                        <img class="w-28 h-28 rounded-md border border-gray-300"
                            src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                            alt="">
                    </div>

                    <div class="mx-3">
                        <p>No Booking: 123BD8095</p>
                        <p class="font-semibold">Tur Pantai</p>
                        <p>Tur</p>
                        <p class="text-green-600">Telah dikonfirmasi</p>
                    </div>

                    <div class="">
                        <div class="flex">
                            <a href="#">
                                <p class="mx-2">Unduh</p>
                            </a>

                            <a href="#">
                                <img id="image" src="{{ asset('storage/icons/ic-excel.svg') }}"
                                    class="w-[15px] h-[20px] mx-2" alt="ic-pdf 3" title="Kamtuu">
                            </a>

                            <a href="#">
                                <img id="image" src="{{ asset('storage/icons/ic-pdf.svg') }}"
                                    class="w-[15px] h-[20px] mx-2" alt="polygon 3" title="Kamtuu">
                            </a>

                            <a href="#">
                                <img id="image" src="{{ asset('storage/icons/ic-word.svg') }}"
                                    class="w-[15px] h-[20px] mx-2" alt="polygon 3" title="Kamtuu">
                            </a>
                        </div>
                    </div>

                    <div class="grid col-span-3 mt-5">
                        <p>AKAN berlangsung dalam <span class="text-yellow-600">5 hari 2 jam 32 menit</span></p>
                        <p class="text-xl font-semibold">Toko Tour IDN</p>
                        <p>Tur Pantai</p>
                    </div>

                    <div class="col-span-3 border border-gray-600 rounded-md p-5">
                        <p>27 Agustus 2022 • Paket Pagi</p>
                        <p>Peserta :</p>
                        <p>2 Dewasa • 1 Anak</p>
                    </div>

                    <div class="col-span-3 border border-gray-600 rounded-md p-5 my-3">
                        <p class="text-2xl">Pembayaran</p>
                        <p class="font-bold">Harga Residen</p>
                        <div class="grid grid-cols-2">
                            <div class="text-star">
                                <p>Dewasa</p>
                                <p>Anak-anak</p>
                                <p>Balita</p>
                            </div>
                            <div class="text-end text-blue-500">
                                <p>Rp.125.000</p>
                                <p>Rp.50.000</p>
                                <p>Rp.0</p>
                            </div>
                        </div>

                        <p class="font-bold">Harga Non-Residen</p>
                        <div class="grid grid-cols-2">
                            <div class="text-star">
                                <p>Dewasa</p>
                                <p>Anak-anak</p>
                                <p>Balita</p>
                            </div>
                            <div class="text-end text-blue-500">
                                <p>Rp.125.000</p>
                                <p>Rp.0</p>
                                <p>Rp.0</p>
                            </div>
                        </div>

                        <hr class="my-8 h-px bg-gray-200 border-0 dark:bg-gray-700">

                        <div class="grid grid-cols-2">
                            <div class="text-star">
                                <p class="font-bold">Harga</p>
                                <p>Diskon Per-Tanggal</p>
                                <p>Surcharge Per-Tanggal</p>
                            </div>
                            <div class="text-end text-blue-500">
                                <p>Rp.125.000</p>
                                <p class="text-red-600">- Rp.125.000</p>
                                <p>Rp.125.000</p>
                            </div>
                        </div>

                        <hr class="my-8 h-px bg-gray-200 border-0 dark:bg-gray-700">

                        <div class="grid grid-cols-2">
                            <div class="text-star">
                                <p class="font-bold">Harga Per-Tanggal</p>
                                <p>Diskon Per-Tanggal</p>
                                <p>Surcharge Per-Tanggal</p>
                            </div>
                            <div class="text-end text-blue-500">
                                <p>Rp.250.000</p>
                                <p class="text-red-600">- Rp.0</p>
                                <p class="text-red-600">- Rp.0</p>
                                <p class="text-red-600">- Rp.125.000</p>
                            </div>
                        </div>

                        <hr class="my-8 h-px bg-gray-200 border-0 dark:bg-gray-700">

                        <div class="grid grid-cols-2">
                            <div class="text-star">
                                <p class="font-bold">Harga Akhir</p>
                                <p>Pilihan</p>
                                <p>Extra</p>
                                <p>Biaya Lain-lain</p>
                                <p>Biaya Pemesanan</p>
                                <p>Pajak</p>
                                <p>Promo</p>
                                <p>Kupon</p>
                            </div>
                            <div class="text-end text-blue-500">
                                <p>Rp.125.000</p>
                                <p>Rp.50.000</p>
                                <p>Rp.0</p>
                                <p>Rp.0</p>
                                <p>Rp.10.000</p>
                                <p>Rp.0</p>
                                <p class="text-red-600">- Rp.0</p>
                                <p class="text-red-600">- Rp.125.000</p>
                            </div>
                        </div>

                        <hr class="my-8 h-px bg-gray-200 border-0 dark:bg-gray-700">

                        <div class="grid grid-cols-2">
                            <div class="text-star">
                                <p class="font-bold">Total</p>
                            </div>
                            <div class="text-end text-blue-500">
                                <p>Rp.60.000</p>
                            </div>
                        </div>

                    </div>

                    <div>
                        <button
                            class="my-5 rounded-lg px-8 py-2 text-base bg-red-500 text-blue-100 hover:bg-red-600 duration-300 w-[13rem]">
                            Batalkan Pesanan
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>