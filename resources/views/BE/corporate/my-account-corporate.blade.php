<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #report:checked+#report {
            display: block;
        }

        [x-cloak] {
            display: none;
        }

        .duration-300 {
            transition-duration: 300ms;
        }

        .ease-in {
            transition-timing-function: cubic-bezier(0.4, 0, 1, 1);
        }

        .ease-out {
            transition-timing-function: cubic-bezier(0, 0, 0.2, 1);
        }

        .scale-90 {
            transform: scale(.9);
        }

        .scale-100 {
            transform: scale(1);
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">

    {{-- Navbar --}}
    <x-be.corporate.navbar-corporate></x-be.corporate.navbar-corporate>

    {{-- Sidebar --}}
    <x-be.corporate.sidebar-corporate></x-be.corporate.sidebar-corporate>

    {{--Body--}}
    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pr-10" style="padding-left: 315px">
            <p class="text-gray">
                My Account
            </p>
            <button class="px-8 py-2 lg:text-md md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">Edit</button>
            <div class="max-w-6xl bg-gray-200 rounded-md border border-black p-2 my-5">
                <p class="font-bold text-base">Informasi Manajer Agensi</p>

                {{-- modal informasi manajer--}}
                <div class="" x-data="{ 'showModal': false }" @keydown.escape="showModal = false" x-cloak>
                    <div class="flex justify-end -mt-5 text-sm">
                        <button type="button"
                            class="px-8 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white"
                            @click="showModal = true">Edit Informasi Manajer
                        </button>
                    </div>

                    <!--Overlay-->
                    <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="showModal"
                        :class="{ 'absolute inset-0 z-10 flex items-center justify-center': showModal }">
                        <!--Dialog-->
                        <div class="bg-white w-[42rem] max-w-4xl md:max-w-2xl mx-auto rounded shadow-lg py-4 text-left px-6"
                            x-show="showModal" @click.away="showModal = false"
                            x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0 scale-90"
                            x-transition:enter-end="opacity-100 scale-100" x-transition:leave="ease-in duration-300"
                            x-transition:leave-start="opacity-100 scale-100"
                            x-transition:leave-end="opacity-0 scale-90">

                            <!--Title-->
                            <div class="flex justify-between items-center pb-3">
                                <p class="text-2xl font-bold">Edit Informasi Manajer</p>
                                <div class="cursor-pointer z-50" @click="showModal = false">
                                    <svg class="fill-current text-black" xmlns="http://www.w3.org/2000/svg" width="18"
                                        height="18" viewBox="0 0 18 18">
                                        <path
                                            d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                        </path>
                                    </svg>
                                </div>
                            </div>

                            <!-- content -->
                            <form action="">
                                <p class="text-gray-600 my-2 ">Nama Depan</p>
                                <input class="rounded-md w-full" type="text" placeholder="James">
                                <p class="text-gray-600 my-2 ">Nama Belakang</p>
                                <input class="rounded-md w-full" type="text" placeholder="Lockhart">
                                <p class="text-gray-600 my-2 ">Alamat Email</p>
                                <input class="rounded-md w-full" type="text" placeholder="@gmail.com">
                                <p class="text-gray-600 my-2 ">No Telp</p>
                                <input class="rounded-md w-full" placeholder="+628" type="text">
                            </form>

                            <!--Footer-->
                            <div class="flex justify-end pt-2">
                                <button
                                    class="px-4 bg-transparent p-3 rounded-lg text-blue-600 hover:bg-gray-100 hover:text-indigo-400 mr-2"
                                    @click="alert('Additional Action');">Simpan
                                </button>
                                <button
                                    class="modal-close px-4 bg-blue-600 p-3 rounded-lg text-white hover:bg-indigo-400"
                                    @click="showModal = false">Close
                                </button>
                            </div>
                        </div>
                        <!--/Dialog -->
                    </div><!-- /Overlay -->
                </div>

                {{-- Profile Detail --}}
                <div class="grid grid-cols-3 mx-2">
                    <div>
                        <p class="font-bold">Nama Depan</p>
                        <p>Neanthal</p>
                    </div>
                    <div class="col-span-2">
                        <p class="font-bold">Nama Belakang</p>
                        <p>Wahan</p>
                    </div>

                    <div class="col-span-3 mt-5">
                        <p class="font-bold">Alamat Email</p>
                        <p>Admin@web.com</p>
                    </div>

                    <div class="col-span-3 mt-5">
                        <p class="font-bold">No Telp.</p>
                        <p>08831274889</p>
                    </div>
                </div>
            </div>


            <div class="max-w-6xl bg-gray-200 rounded-md border border-black p-2 my-2">
                <p class="font-bold text-base">Informasi Agensi</p>
                {{-- modal informasi agensi--}}
                <div class="" x-data="{ 'showModalAgensi': false }" @keydown.escape="showModalAgensi = false" x-cloak>
                    <div class="flex justify-end -mt-5 text-sm">
                        <button type="button"
                            class="px-8 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white"
                            @click="showModalAgensi = true">Edit Informasi Agensi
                        </button>
                    </div>

                    <!--Overlay-->
                    <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="showModalAgensi"
                        :class="{ 'absolute inset-0 z-10 flex items-center justify-center': showModalAgensi }">
                        <!--Dialog-->
                        <div class="bg-white w-[42rem] max-w-4xl md:max-w-2xl mx-auto rounded shadow-lg py-4 text-left px-6 overflow-y-auto max-h-80"
                            x-show="showModalAgensi" @click.away="showModalAgensi = false"
                            x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0 scale-90"
                            x-transition:enter-end="opacity-100 scale-100" x-transition:leave="ease-in duration-300"
                            x-transition:leave-start="opacity-100 scale-100"
                            x-transition:leave-end="opacity-0 scale-90">

                            <!--Title-->
                            <div class="flex justify-between items-center pb-3">
                                <p class="text-2xl font-bold">Edit Informasi Agensi</p>
                                <div class="cursor-pointer z-50" @click="showModalAgensi = false">
                                    <svg class="fill-current text-black" xmlns="http://www.w3.org/2000/svg" width="18"
                                        height="18" viewBox="0 0 18 18">
                                        <path
                                            d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                        </path>
                                    </svg>
                                </div>
                            </div>

                            <!-- content -->
                            <form action="">
                                <p class="text-gray-600 my-2 ">Negara</p>
                                <input class="rounded-md w-full" type="text" placeholder="Indonesia">
                                <p class="text-gray-600 my-2 ">Kode Pelacakan</p>
                                <input class="rounded-md w-full" type="text" placeholder="0000">
                                <p class="text-gray-600 my-2 ">Nama Perusahaan</p>
                                <input class="rounded-md w-full" type="text" placeholder="PT. ...">
                                <p class="text-gray-600 my-2 ">Nama Dagang Perusahaan</p>
                                <input class="rounded-md w-full" type="text" placeholder="Jual">
                                <p class="text-gray-600 my-2 ">No Telp</p>
                                <input class="rounded-md w-full" type="text" placeholder="+62">
                                <p class="text-gray-600 my-2 ">Alamat</p>
                                <input class="rounded-md w-full" type="text" placeholder="Jl. ...">
                                <p class="text-gray-600 my-2 ">Kota</p>
                                <input class="rounded-md w-full" type="text" placeholder="DIY">
                                <p class="text-gray-600 my-2 ">Kode Pos</p>
                                <input class="rounded-md w-full" type="text" placeholder="0000">
                                <p class="text-gray-600 my-2 ">Alamat Web</p>
                                <input class="rounded-md w-full" type="text" placeholder="https://">
                            </form>

                            <!--Footer-->
                            <div class="flex justify-end pt-2">
                                <button
                                    class="px-4 bg-transparent p-3 rounded-lg text-blue-500 hover:bg-gray-100 hover:text-blue-400 mr-2"
                                    @click="alert('Additional Action');">Simpan
                                </button>
                                <button class="modal-close px-4 bg-blue-500 p-3 rounded-lg text-white hover:bg-blue-400"
                                    @click="showModalAgensi = false">Close
                                </button>
                            </div>
                        </div>
                        <!--/Dialog -->
                    </div><!-- /Overlay -->
                </div>

                {{-- Profile Detail --}}
                <div class="grid grid-cols-3 mx-2">
                    <div>
                        <p class="font-bold">Negara</p>
                        <p>Indonesia</p>
                    </div>
                    <div class="col-span-2">
                        <p class="font-bold">ID Agen</p>
                        <p>12345</p>
                    </div>

                    <div class=" mt-5">
                        <p class="font-bold">Nama Dagang Agensi</p>
                        <p>Dagang Apa Saja</p>
                    </div>

                    <div class="col-span-2 mt-5">
                        <p class="font-bold">Nama Perusahaan</p>
                        <p>PT. Jual Apa Saja</p>
                    </div>

                    <div class=" mt-5">
                        <p class="font-bold">No Telp.</p>
                        <p>08578398293</p>
                    </div>

                    <div class=" mt-5">
                        <p class="font-bold">Alamat</p>
                        <p>Jl. Tamansiswa Yk</p>
                    </div>

                    <div class=" mt-5">
                        <p class="font-bold">Kota</p>
                        <p>Yogyakarta</p>
                    </div>

                    <div class="col-span-2 mt-5">
                        <p class="font-bold">Kode Pos</p>
                        <p>52304</p>
                    </div>

                    <div class="col-span-2 mt-5">
                        <p class="font-bold">Alamat Web</p>
                        <a href="#">
                            <p class="text-kamtuu-second">jualinaja.com</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>