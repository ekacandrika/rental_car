<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">


    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])


    <style>
        {
                {
                -- watch Start --
            }
        }

        .countdown {
            display: inline-block;
        }

        .countdown .digit {
            display: inline-block;
            width: 25px;
            height: 25px;
            text-align: center;
            background-color: #f1f1f1;
            border-radius: 5px;
            margin-right: 5px;
            font-family: Arial, sans-serif;
            font-size: 18px;
            line-height: 25px;
            transition: background-color 0.2s ease-in-out;
        }

        .countdown .digit:last-child {
            margin-right: 0;
        }


            {
                {
                -- watch End --
            }
        }

        #akun:checked+#akun {
            display: block;
        }


        #report:checked+#report {
            display: block;
        }




        .menu-dropdown li a {
            display: inline-block;
            color: white;
            /* text-align: center; */
            text-decoration: none;
        }


        .menu-dropdown li a:hover {
            background-color: none;
        }


        li.dropdown {
            display: inline-block;
            margin-right: 80px
        }


        .dropdown:hover .isi-dropdown {
            display: block;
        }


        .isi-dropdown a:hover {
            color: #fff !important;
            width: 100%;
        }


        .isi-dropdown {
            position: absolute;
            display: none;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
            background-color: #f9f9f9;
            width: 100%;
        }


        .isi-dropdown a {
            color: #3c3c3c !important;
            padding: 1%;
        }


        .isi-dropdown a:hover {
            color: #232323 !important;
            background: #f3f3f3 !important;
        }
    </style>


    <!-- Styles -->
    @livewireStyles
</head>


<body class="bg-[#F2F2F2] font-inter">


    {{-- Navbar --}}
    <x-be.seller.navbar-seller></x-be.seller.navbar-seller>


    {{-- Sidebar --}}
    <x-be.seller.sidebar-seller></x-be.seller.sidebar-seller>


    <div class="col-start-3 col-end-11 z-0">
        {{-- List data booking --}}
        <div class="p-5 pr-10" style="padding-left: 315px">
            <div class="flex items-center p-2 font-bold text-dark">
                <img src="{{ asset('storage/icons/cart-shopping-solid (1).svg') }}" class="w-[16px] h-[16px] mt-1"
                    alt="user" title="user">
                <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">List Pesanan Need To Confirm</p>
            </div>




            {{-- body --}}
            <div class="max-w-6xl bg-gray-200 rounded-md border border-black p-2 my-5">
                @forelse($count as $item)
                @php
                $detail_produk = App\Models\Productdetail::where('id', $item->product_detail_id)->first();
                $produk = App\Models\Product::where('id', $detail_produk->product_id)->first();
                $traveller = App\Models\Usertraveller::where('id', $item->customer_id)->first();
                @endphp


                <div class="grid grid-cols-[75%_25%]">
                    <div
                        class="rounded-lg bg-kamtuu-second bg-opacity-30 border border-gray-400 shadow-lg my-2 grid grid-cols-[30%_50%_10%]">
                        <div class="">
                            <span class="p-3 font-semibold">
                                {{$produk->product_name}}
                            </span>


                            <div class="">
                                <img class="w-32 h-24 mx-2 my-2 border border-gray-400 shadow-lg rounded-lg"
                                    src=" {{ isset($detail_produk->thumbnail) ? Storage::url($detail_produk->thumbnail) : 'https://via.placeholder.com/109x109' }}"
                                    alt="">
                            </div>
                        </div>


                        <div class="mt-2">
                            <span class="p-1 mx-2 my-2 text-white text-sm capitalize bg-kamtuu-primary rounded-lg">
                                {{$produk->type}}
                            </span>
                            @if( $produk->type == "tour" )
                            <span class="p-1 mx-2 my-2 text-white text-sm capitalize bg-kamtuu-primary rounded-lg">
                                {{$detail_produk->tipe_tur}}
                            </span>
                            @endif
                            <br>
                            @php
                            $targetDate = \Carbon\Carbon::parse($item->activity_date);
                            $currentDate = \Carbon\Carbon::now();
                            $countdown = $currentDate->diff($targetDate);
                            @endphp
                            <div class="mx-auto">
                                @if ($currentDate < $targetDate) <div class="countdown mt-2">
                                    Akan Dimulai Dalam
                                    <span class="digit" id="days"> {{ $countdown->d }} </span>Hari
                                    <span class="digit" id="hours"> {{ $countdown->h }}</span>Jam
                                    <span class="digit" id="minutes"> {{ $countdown->i }}</span>Menit
                                    <span class="digit" id="seconds"> {{ $countdown->s }}</span>Detik
                            </div>
                            <script>
                                function updateCountdown() {
                                           const daysElement = document.getElementById('days');
                                           const hoursElement = document.getElementById('hours');
                                           const minutesElement = document.getElementById('minutes');
                                           const secondsElement = document.getElementById('seconds');


                                           let days = parseInt(daysElement.innerText);
                                           let hours = parseInt(hoursElement.innerText);
                                           let minutes = parseInt(minutesElement.innerText);
                                           let seconds = parseInt(secondsElement.innerText);


                                           seconds--;


                                           if (seconds < 0) {
                                               seconds = 59;
                                               minutes--;


                                               if (minutes < 0) {
                                                   minutes = 59;
                                                   hours--;


                                                   if (hours < 0) {
                                                       hours = 23;
                                                       days--;
                                                   }
                                               }
                                           }


                                           daysElement.innerText = padNumber(days);
                                           hoursElement.innerText = padNumber(hours);
                                           minutesElement.innerText = padNumber(minutes);
                                           secondsElement.innerText = padNumber(seconds);
                                       }


                                       function padNumber(number) {
                                           return number.toString().padStart(2, '0');
                                       }


                                       setInterval(updateCountdown, 1000);
                            </script>
                            @else
                            <p>Expired : Target date has passed.</p>
                            @endif
                        </div>
                    </div>


                </div>
                <div class="">
                    <div class="grid place-items-center">
                        <div class="flex my-2">
                            <label class="p-1">Status : </label>
                            @if($item->confirm_order == "waiting")
                            <span class="rounded-lg p-1 bg-kamtuu-third text-white animate-pulse">Waiting</span>
                            @elseif($item->confirm_order == "approve")
                            <span class="rounded-lg p-1 bg-green-500 text-white">Approve</span>
                            @elseif($item->confirm_order == "reject")
                            <span class="rounded-lg p-1 bg-red-500 text-white">Reject</span>
                            @endif
                        </div>
                        <br>
                        <div class="flex">
                            <label class="inline-flex items-center mx-2">
                                <input type="radio" name="confirm_order" value=""
                                    class="mr-2 checked:ring-green-600 checked:bg-green-600 text-green-600"
                                    data-id="{{ $item->id }}">
                                {{-- {{ $item->confirm_order === '' ? 'checked' : '' }}>--}}
                                <span>Approve</span>
                            </label>
                            <label class="inline-flex items-center mx-2">
                                <input type="radio" name="confirm_order" value="reject"
                                    class="mr-2 checked:ring-red-600 checked:bg-red-600 text-red-600"
                                    data-id="{{ $item->id }}" {{ $item->confirm_order === 'reject' ? 'checked' : '' }}>
                                <span>Reject</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            @empty


            No Request


            @endforelse
        </div>
    </div>
    </div>




    <script>
        $(document).ready(function () {
       $('input[name="confirm_order"]').change(function () {
           var id = $(this).data('id');
           var status = $(this).val();


           $.ajax({
               url: '/radio-button/' + id + '/update-status',
               type: 'POST',
               data: {
                   _token: '{{ csrf_token() }}',
                   // field yang harus diisi
                   confirm_order: status
               },
               success: function (response) {
                   console.log(status)
                   location.reload();
                   console.log(response.message);
               },
               error: function (xhr) {
                   console.log(xhr.responseText);
               }
           });
       });
   });
    </script>
</body>


</html>