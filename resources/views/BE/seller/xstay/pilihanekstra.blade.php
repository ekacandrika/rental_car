<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        .link-not-active {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
            color: #858585;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param || null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <x-be.seller.sidebar-toko>
        <x-slot name="addListingHotel">
            {{ route('hotelinfodasar') }}
        </x-slot>
        <x-slot name="faq">
            @if ($data->faq == null)
            {{ route('hotelfaq') }}
            @else
            {{ route('hotelfaq.edit', $data->product_code) }}
            @endif
        </x-slot>

        {{-- Xstay --}}
        <x-slot name="addListingXstay">
            {{ route('infodasar') }}
        </x-slot>
        <x-slot name="faqXstay">
            @if ($xstay == null)
            {{ route('faq.index') }}
            @else
            {{ route('faq.edit', $xstay->product_code) }}
            @endif
        </x-slot>
    </x-be.seller.sidebar-toko>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10">
            <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
            {{-- Breadcumbs --}}
            <nav class="flex" aria-label="Breadcrumb">
                <ol class="inline-flex items-center space-x-1 md:space-x-3">
                    <li class="inline-flex items-center">
                        @if (isset($data->product_name))
                        <a href="{{ route('infodasar.edit', $data->product_code) }}"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                            Informasi Dasar</a>
                        @else
                        <a href="{{ route('infodasar.edit', $data->product_code) }}"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Isi
                            Informasi Dasar</a>
                        @endif

                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->id_kamar))
                            <a href="{{ route('xstay.infokamar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Informasi
                                Kamar</a>
                            @else
                            <a href="{{ route('xstay.infokamar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Informasi
                                Kamar</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->discount))
                            <a href="{{ route('harga.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Harga
                                & Ketersediaan</a>
                            @else
                            <a href="{{ route('harga.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Harga
                                & Ketersediaan</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->batas_pembayaran))
                            <a href="{{ route('bayar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Batas
                                Pembayaran & Pembatalan</a>
                            @else
                            <a href="{{ route('bayar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Batas
                                Pembayaran & Pembatalan</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->link_maps) || isset($data->productdetail->foto_maps_1))
                            <a href="{{ route('maps.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Peta
                                & Foto</a>
                            @else
                            <a href="{{ route('maps.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Peta
                                & Foto</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            <a href="{{ route('ekstra.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Ekstra</a>
                        </div>
                    </li>
                </ol>
            </nav>

            {{-- Section --}}
            <form action="{{ route('ekstra.update', $data->id) }}" method="POST">
                @csrf
                @method('PUT')
                {{-- Section --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded">

                    {{-- Ekstra --}}
                    <div class="pb-5">
                        <div class="text-lg font-bold font-inter text-[#4F4F4F]">
                            Ekstra
                        </div>

                        <div class="py-2">
                            <div class="flex space-x-2">
                                <div class="form-check">
                                    <input value="Bolehkan"
                                        class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                        type="radio" name="izin_ekstra" id="ekstra_allowed">
                                    <label class="form-check-label inline-block text-gray-800" for="ekstra_allowed">
                                        Bolehkan
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input value="Tidak Usah"
                                        class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                        type="radio" name="izin_ekstra" id="ekstra_not_allowed">
                                    <label class="form-check-label inline-block text-gray-800" for="ekstra_not_allowed">
                                        Tidak Usah
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- TnC --}}
                    <div class="">
                        <div>
                            <div class="form-check">
                                <input name="tnc"
                                    class="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                    type="checkbox" id="tnc">
                                <label class="form-check-label inline-block text-gray-800" for="tnc">
                                    Saya setuju atas syarat dan ketentuan penambahan listing baru di Kamtuu
                                </label>
                            </div>
                        </div>
                    </div>

                    @if ($errors->hasBag('pilihan_ekstra'))
                    <div class="bg-red-300 rounded-md p-3 my-5">
                        @foreach ($errors->pilihan_ekstra->toArray() as $key=>$value)
                        @foreach ($value as $index=>$err)
                        <p>- {{ $err}}</p>
                        @endforeach
                        @endforeach
                    </div>
                    @endif

                    <div class="flex justify-between pt-5">
                        <div>
                            <a href="{{ route('maps.edit', $data->product_code) }}"
                                class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                Sebelumnya
                            </a>
                        </div>

                        <div class="flex space-x-3">
                            <button type="submit"
                                class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                Simpan
                            </button>
                            {{-- <button type="submit"
                                class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                Terbitkan
                            </button> --}}
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>

    <script>
        function handler() {

            return {
                fields: [],
                addNewField() {
                    this.fields.push({
                        detail_1: 'hotel',
                        detail_2: 'bintang_satu',
                        price: 0,
                        isRequired: ''
                    });
                    // console.log(this.fields)
                },
                removeField(index) {
                    // console.log(index)
                    this.fields.splice(index, 1);
                    console.log(this.fields)
                }
            }
        }
    </script>
</body>

</html>