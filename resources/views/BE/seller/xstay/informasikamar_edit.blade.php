<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js" defer></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js" defer></script>


    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        .link-not-active {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
            color: #858585;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param || null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <x-be.seller.sidebar-toko>
        <x-slot name="addListingHotel">
            {{ route('hotelinfodasar') }}
        </x-slot>
        <x-slot name="faq">
            @if ($data->faq == null)
            {{ route('hotelfaq') }}
            @else
            {{ route('hotelfaq.edit', $data->product_code) }}
            @endif
        </x-slot>

        {{-- Xstay --}}
        <x-slot name="addListingXstay">
            {{ route('infodasar') }}
        </x-slot>
        <x-slot name="faqXstay">
            @if ($xstay == null)
            {{ route('faq.index') }}
            @else
            {{ route('faq.edit', $xstay->product_code) }}
            @endif
        </x-slot>
    </x-be.seller.sidebar-toko>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10">
            <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
            {{-- Breadcumbs --}}
            <nav class="flex" aria-label="Breadcrumb">
                <ol class="inline-flex items-center space-x-1 md:space-x-3">
                    <li class="inline-flex items-center">
                        @if (isset($data->product_code))
                        <a href="{{ route('infodasar.edit', $data->product_code) }}"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                            Informasi Dasar</a>
                        @else
                        <a href="#"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Isi
                            Informasi Dasar</a>
                        @endif
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            <a href="{{ route('xstay.infokamar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Informasi
                                Kamar</a>
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (!isset($data->productdetail->discount))
                            <a href="{{ route('hotelharga') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Harga
                                & Ketersediaan</a>
                            @else
                            <a href="{{ route('harga.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Harga
                                & Ketersediaan</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (!isset($data->productdetail->batas_pembayaran))
                            <a href="{{ route('bayar') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Batas
                                Pembayaran & Pembatalan</a>
                            @else
                            <a href="{{ route('bayar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Batas
                                Pembayaran & Pembatalan</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (!isset($data->productdetail->link_maps) || !isset($data->productdetail->foto_maps_1))
                            <a href="{{ route('maps') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Peta
                                & Foto</a>
                            @else
                            <a href="{{ route('maps.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Peta
                                & Foto</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (!isset($data->productdetail->izin_ekstra))
                            <a href="{{ route('ekstra') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Ekstra</a>
                            @else
                            <a href="{{ route('ekstra.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Ekstra</a>
                            @endif
                        </div>
                    </li>
                </ol>
            </nav>

            <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                {{-- @dump($rooms_array) --}}
                <div class="grid grid-cols-2">
                    <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Informasi Kamar</div>
                </div>

                <div>
                    <form action="{{ route('xstay.infokamar.update', $data->id) }}" method="POST"
                        enctype="multipart/form-data" x-data="kamar()" x-init="$nextTick(() => { select2WithAlpine() })">
                        @csrf
                        @method('PUT')
                        <div class="px-5 mb-6 space-y-5">
                            <label class="block text-sm font-bold text-[#333333] dark:text-gray-300"
                                for="amenitas_hotel_label">kamar</label>
                            <div>
                                <select name="kamar_id" id="attributes_kamar"
                                    x-model="attribute_kamar" x-ref="select_kamar" required
                                    class="attributes_kamar mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                    <option selected disabled value="0">Pilih kamar</option>
                                    @foreach ($kamar as $item)
                                    @php
                                        $img = (json_decode($item->foto_kamar, true)['result'][0]);
                                    @endphp
                                    <option value="{{ $item->id }}" data-name="{{ $item->nama_kamar }}"
                                        data-code="{{ $item->kode_kamar }}" data-img="{{ Storage::url($img) }}" data-category="{{$item->jenis_kamar}}" data-stock="{{$item->stock}}">{{ $item->kode_kamar }} - {{ $item->nama_kamar }}</option>
                                    @endforeach
                                </select>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                            </div>
                            <button
                                class="bg-[#9E3D64] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#b94875] duration-200"
                                type="button" @click="addNewkamar">Tambah kamar
                            </button>
                            <template x-if="error_kamar === true">
                                <div x-init="setTimeout(() => error_kamar = false, 3000)"
                                    class="bg-red-300 rounded-md p-3 my-2 w-fit">
                                    <p x-text="error_empty_kamar"></p>
                                </div>
                            </template>
                            {{-- <template x-if="rooms.length > 0" :data-length="rooms.length">
                                <div
                                    class="w-fit min-h-fit max-h-96 overflow-y-auto rounded-md border border-slate-200">
                                    <label
                                        class="sticky top-0 px-2 py-1 block text-base font-bold text-[#333333] dark:text-gray-300 bg-slate-300 rounded-t-md"
                                        for="kamar_name">Icon kamar</label>
                                    <template x-for="(kamar, index) in rooms" :key="index">
                                        <div class="my-1">
                                            <div
                                                class="flex gap-3 hover:bg-slate-100 rounded-md w-fit p-1 items-center">
                                                <img id="icon_kamar_img"
                                                    class="ic_kamar border flex justify-center items-center rounded border-gray-300 p-1"
                                                    :src="kamar.kamar_img" alt="icon_kamar" width="42px"
                                                    height="42px">
                                                <div class="space-y-1">
                                                    <div class="flex">
                                                        <input id="kamar_id" name="kamar_id[]" type="hidden"
                                                            x-model="kamar.kamar_id" required>
                                                        <p id="kamar_name"
                                                            class=" text-gray-900 text-base rounded-lg block break-words w-60 p-2.5"
                                                            x-text="kamar.kamar_name"></p>
                                                        <p id="kamar_code"
                                                            class=" text-gray-900 text-base rounded-lg block break-words w-45 p-2.5"
                                                            x-text="kamar.kode_kamar"></p>
                                                        <div class="flex justify-center">
                                                            <button class="mx-1" type="button"
                                                                @click="removekamar(index)">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                    height="24" viewBox="0 0 24 24">
                                                                    <path fill="currentColor"
                                                                        d="M12 20c-4.41 0-8-3.59-8-8s3.59-8 8-8s8 3.59 8 8s-3.59 8-8 8m0-18A10 10 0 0 0 2 12a10 10 0 0 0 10 10a10 10 0 0 0 10-10A10 10 0 0 0 12 2M7 13h10v-2H7" />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </template>
                                </div>
                            </template> --}}
                            @error('kamar_id')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>
                        <div class="px-5 mb-6 space-y-5">
                            <table class="w-full text-sm text-left mt-5 text-[#333333] bg-[#D25889] dark:bg-[#D25889]">
                                <thead class="text-xs text-gray-700 uppercase bg-gray-500">
                                    <tr>
                                        <th class="px-6 py-3">No</th>
                                        <th class="px-6 py-3">Kode Kamar</th>
                                        <th class="px-6 py-3">Jenis Kamar</th>
                                        <th class="px-6 py-3">Stok Kamar</th>
                                        <th class="px-6 py-3">Nama Kamar</th>
                                        <th class="px-6 py-3"></th>
                                    </tr>
                                </thead>
                                <template x-if="rooms.length > 0" :data-length="rooms.length">
                                    <template x-for="(kamar, index) in rooms" :key="index">
                                        <tr class="bg-white border-b hover:bg-slate-100">
                                            <td class="px-6 py-3" x-text="index+1">No</td>
                                            <td class="px-6 py-3" x-text="kamar.kamar_code">Kode Kamar</td>
                                            <td class="px-6 py-3" x-text="kamar.kamar_category">Jenis Kamar</td>
                                            <td class="px-6 py-3" x-text="kamar.stock">Stok Kamar</td>
                                            <td class="px-6 py-3" x-text="kamar.kamar_name">Nama Kamar</td>
                                            <td class="px-6 py-3">
                                                <button type="button" @click="removekamar(index)" class="text-white bg-red-500 rounded-full duration-200 foucs:ring-4 focus:outline-none focus:ring-red-300 font-medium text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-500 dark:hover:bg-red-600 dark:focus:ring-red-600">
                                                    <img src="{{ asset('storage/icons/trash-can-regular-squared-size.svg') }}"
                                                        alt="" width="16px" height="16px">
                                                    <span class="sr-only">Icon description</span>
                                                </button>
                                                <input type="hidden" name="kamar_id[]" x-model="kamar.kamar_id">
                                            </td>
                                        </tr>
                                    </template>
                                </template>
                                <template x-if="rooms.length < 0">
                                    <tbody>
                                        <tr>
                                            <td class="bg-white text-center text-xl font-semibold" colspan="6">Tidak ada
                                                kamar
                                                ditambahkan.</td>
                                        </tr>
                                    </tbody>
                                </template>
                            </table>
                        </div>
                        <x-be.com.two-button-dup>
                            {{ route('infodasar.edit', $data->product_code) }}
                        </x-be.com.two-button-dup>
                    </form>
                </div>

            </div>
        </div>
    </div>
    </div>

    <script>
         function kamar() {
            return {
                rooms: {!! isset($rooms_array) ? $rooms_array :'[]' !!},
                attribute_kamar: 0,
                kamar_id: 0, 
                nama_kamar:'', 
                kode_kamar:'', 
                jenis_kamar:'', 
                stock:'', 
                gambar_kamar:'',
                error_empty_kamar: '',
                error_kamar: false,
                addNewkamar() {

                    if (this.kamar_id === 0) {
                        this.error_kamar = true
                        this.error_empty_kamar = 'kamar wajib diisi!'
                        return this.error_empty_kamar, this.error_kamar
                    }

                    this.rooms.push({
                        kamar_id: this.kamar_id,
                        kamar_name: this.nama_kamar,
                        kamar_code: this.kode_kamar,
                        kamar_category: this.jenis_kamar,
                        stock: this.stock,
                        kamar_img: this.gambar_kamar
                    });
                    
                    this.error_empty_kamar = ''
                },
                removekamar(index) {
                    this.rooms.splice(index, 1);
                },
                select2WithAlpine() {
                    this.select2 = $(this.$refs.select_kamar).select2({
                        placeholder:'Pilih kamar'
                    });
                    this.select2.on("select2:select", (event) => {
                        console.log(event.target.value);
                        this.kamar_id = event.target.value;
                        this.nama_kamar = this.select2.find(":selected").data("name");
                        this.kode_kamar = this.select2.find(":selected").data("code");
                        this.jenis_kamar = this.select2.find(":selected").data("category");
                        this.stock = this.select2.find(":selected").data("stock");
                        this.gambar_kamar = this.select2.find(":selected").data("img");
                    });
                    this.$watch("attribute_kamar", (value) => {
                        // this.kamar_id = value;
                        // console.log(this.select2.find(":selected").data("name"))
                        this.select2.val(value).trigger("change");
                    });
                    
                }
            }
        }
    </script>

    <script>
        function amenitas() {
            return {
                icons: '',
                title: '',
                description: '',
                fields: [],
                addNewField() {
                    this.fields.push({
                        icon: '',
                        title: this.title,
                        description: this.description,
                    });
                    // console.log(this.fields)
                },
                removeField(index) {
                    // console.log(index)
                    this.fields.splice(index, 1);
                    console.log(this.fields)
                }
            }
        }
    </script>

    <script type="text/javascript">
        $("#dynamic-ar").click(function() {
            $("#dynamicAddRemove").append(
                `<tr>
                    <td>
                        <label for="icon-fasilitas" class="block mb-2 text-sm font-semibold text-[#333333]">Icon</label>
                        <input type="file" name="foto_kamar[]" style="width: 100px" />
                        <img id="icon-fasilitas"class="ic_fasilitas border flex justify-center items-center rounded border-gray-300 bg-gray-200 p-1 mt-1"src="{{ asset('storage/icons/upload.svg') }}" alt="" width="42px">
                    </td>
                    <td>
                        <label for="fasilitas" class="block mb-2 text-sm font-bold text-[#333333]">Keterangan</label>
                        <input type="text" id="fasilitas" name="fasilitas[]" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Kolam Renang">
                    </td>
                    <td class="p-3">
                        <button type="button" class="mx-2 remove-input-field"><img
                            src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                            alt="delete_button" width="25px">
                        </button>
                    </td>
                </tr>`
            );
        });
        $(document).on('click', '.remove-input-field', function() {
            $(this).parents('tr').remove();
        });

        // Change icon preview
        $('#dynamicAddRemove tbody').on('change', '[type=file]', function(e) {  
            let el_index = $(this).index('[type=file]')
            const file = this.files[0];
            if (file) {
                let reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function (e) {
                    $('.ic_fasilitas').each(function(i, el) {
                        if (i === el_index) {
                            $(el).attr("src", e.target.result);
                        }
                    })
                };
            }
        });
    </script>

</body>

</html>