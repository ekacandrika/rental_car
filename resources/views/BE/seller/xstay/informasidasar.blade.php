<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- base | always include -->
    <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.1.1/dist/css/base/jquery.fonticonpicker.min.css">

    <!-- default grey-theme -->
    <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.0.0-alpha.0/dist/css/themes/grey-theme/jquery.fonticonpicker.grey.min.css">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript"
        src="https://unpkg.com/@fonticonpicker/fonticonpicker/dist/js/jquery.fonticonpicker.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js" defer></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js" defer></script>
   {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        .link-not-active {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
            color: #858585;
        }

        .select2-container .select2-selection--single {
            height: 100%;
            /* padding-left: 0.75rem; */
            /* padding-right: 0.75rem; */
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 100%;
        }

        .select2-selection__arrow {
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            /* padding-right: 0.75rem; */
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param || null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <x-be.seller.sidebar-toko>
        {{-- Hotel --}}
        <x-slot name="addListingHotel">
            {{ route('hotelinfodasar') }}
        </x-slot>
        <x-slot name="faq">
            @if ($hotel == null)
            {{ route('hotelfaq') }}
            @else
            {{ route('hotelfaq.edit', $hotel->product_code) }}
            @endif
        </x-slot>

        {{-- Xstay --}}
        <x-slot name="addListingXstay">
            @if ($xstay == null)
            {{ route('infodasar') }}
            @else
            {{ route('infodasar.edit', $xstay->product_code) }}
            @endif
        </x-slot>
        <x-slot name="faqXstay">
            @if ($xstay == null)
            {{ route('faq.index') }}
            @else
            {{ route('faq.edit', $xstay->product_code) }}
            @endif
        </x-slot>
    </x-be.seller.sidebar-toko>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10">
            <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
            {{-- Breadcumbs --}}
            <nav class="flex" aria-label="Breadcrumb">
                <ol class="inline-flex items-center space-x-1 md:space-x-3">
                    <li class="inline-flex items-center">
                        <a href="informasi"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                            Informasi Dasar</a>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data))
                            <a href="{{ route('xstayinfokamar') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Informasi
                                Kamar</a>
                            @else
                            <a href="{{ route('xstayinfokamar') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Informasi
                                Kamar</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data))
                            <a href="{{ route('harga') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Harga
                                & Ketersediaan</a>
                            @else
                            <a href="{{ route('harga') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Harga
                                & Ketersediaan</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data))
                            <a href="{{ route('bayar') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">
                                Pembayaran & Pembatalan</a>
                            @else
                            <a href="{{ route('bayar') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Batas
                                Pembayaran & Pembatalan</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data))
                            <a href="{{ route('maps') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Peta
                                & Foto</a>
                            @else
                            <a href="{{ route('maps') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Peta
                                & Foto</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data))
                            <a href="{{ route('ekstra') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Ekstra</a>
                            @else
                            <a href="{{ route('ekstra') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Ekstra</a>
                            @endif
                        </div>
                    </li>
                </ol>
            </nav>

            <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                <div class="grid grid-cols-2">
                    <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Isi Informasi Dasar</div>
                    <div class="text-sm font-bold font-inter p-5 text-[#333333] text-right">ID Listing :
                        {{ $code }}
                    </div>
                </div>

                <div>
                    <form action="{{ route('infodasar.store') }}" method="POST" enctype="multipart/form-data"
                        id="informasi_dasar_ID">
                        @csrf
                        <input type="hidden" name="product_code" value="{{ $code }}">
                        <div class="px-5 mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Judul</label>
                            <input type="text" id="text" name="product_name" value="{{ old('product_name') }}"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Masukkan nama xstay">
                            @error('product_name')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>

                        <fieldset class="px-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Konfirmasi
                                Paket</label>
                            <div class="grid grid-cols-2 w-80">
                                <div class="flex items-center mb-4">
                                    <input id="country-option-1" type="radio" name="confirmation" value="instant"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                        checked="">
                                    <label for="country-option-1" class="block ml-2 text-sm font-medium text-gray-900">
                                        Instant
                                    </label>
                                </div>
                                <div class="flex items-center mb-4">
                                    <input id="country-option-2" type="radio" name="confirmation" value="by_seller"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                    <label for="country-option-2" class="block ml-2 text-sm font-medium text-gray-900">
                                        Konfirmasi by Seller
                                    </label>
                                </div>
                            </div>
                            @error('confirmation')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </fieldset>

                        {{-- Tag Location --}}
                        <div class="block px-5 mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag
                                Lokasi</label>
                            <select name="regency_id" id="regency_id"
                                class="regency_id mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                <option selected disabled>Pilih Lokasi</option>
                                @foreach ($regency as $item)
                                <option value="{{ $item->id }}" {{ old('regency_id')==$item->id ? 'selected' : '' }}>{{
                                    $item->name }}</option>
                                @endforeach
                            </select>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>
                            @error('regency_id')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>

                        {{-- Address --}}
                        <div class="px-5 mb-6">
                            <label for="address-textarea" class="block mb-2 text-sm font-bold text-[#333333]">Alamat
                                Lengkap</label>
                            <textarea id="address-textarea" name="alamat"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Tulis alamat lengkap" rows="3">{{ old('alamat') }}</textarea>
                            @error('alamat')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>

                        {{-- Fasilitas --}}
                        <div class="px-5 mb-6 space-y-5" x-data="fasilitas()" x-init="$nextTick(()=>{select2WithAlpine()})">
                            <label for="amenitas_xstay_label" class="block text-sm font-bold dark:text-gray">Fasilitas</label>
                            <div>
                                <select name="attributes_fasilitas" id="attributes_fasilitas" x-model="attribute_fasilitas" x-ref="select_fasilitas" required
                                class="attributes_fasilitas mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] tex-gray-900 w-96 focus:outline-none focis:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                <option value="0" selected disabled>Pilih Fasilitas</option>
                                @foreach($attributes as $item)
                                    <option value="{{$item->id}}" data-name="{{$item->text}}" data-img="{{Storage::url($item->image)}}">{{$item->text}}</option>
                                @endforeach
                                </select>
                                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray"></div>
                            </div>    
                            <button type="button" @click="addNewField"
                                class="bg-[#9E3D64] text-white text-sm font-semibold py-2 px-7 rounded rounded-lg hover:bg-[#b94875] duration-2">
                                Tambah Fasilitas
                            </button>
                            <template x-if="error_fasilitas===true">
                                <div x-init="setTimeout(()=> error_fasilitas = false, 3000)" class="bg-red-300 rounded-md p-3 my-2 w-fit">
                                    <p x-text="error_empty_fasilitas"></p>
                                </div>
                            </template>
                            <template x-if="fasilities.length > 0" data-length="fasilities.length">
                                <div class="w-fit min-h-fit max-h-96 overflow-y-auto rounded-md border border-slate-200">
                                    <label for="fasilitas_name" class="sticky top-0 px-2 py-1 block text-base font-bold text-[#333333] dark:text-gray-300 bg-slate-300 rounded-t-md">Icon Fasilitas</label>
                                    <template x-for="(fasilitas, index) in fasilities" :key="index">
                                        <div class="my-1">
                                            <div class="flex gap-3 hover:bg-slate-100 rounded-md w-fit p-1 items-center">
                                                <img :src="fasilitas.fasilitas_img" alt="icon_fasilitas" id="icon_fasilitas_img" class="ic_fasilitas border flex justify-center items-center rounded border-gray-300 p-1" witdh="42px" height="42px">
                                                <div class="space-y-1">
                                                    <div class="flex">
                                                        <input type="hidden" name="fasilitas_id[]" x-model="fasilitas.fasilitas_id" id="fasilitas_id" required>
                                                        <p id="fasilitas_name" class="text-gray-900 text-base rouded-lg block break-words w-96 p-2.5" x-text="fasilitas.fasilitas_name"></p>
                                                        <div class="flex justify-center">
                                                            <button class="mx-1" type="button" @click="removeField(index)">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                    height="24" viewBox="0 0 24 24">
                                                                    <path fill="currentColor"
                                                                        d="M12 20c-4.41 0-8-3.59-8-8s3.59-8 8-8s8 3.59 8 8s-3.59 8-8 8m0-18A10 10 0 0 0 2 12a10 10 0 0 0 10 10a10 10 0 0 0 10-10A10 10 0 0 0 12 2M7 13h10v-2H7" />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </template>
                                </div>
                            </template>
                            @error('fasilitas_id')
                            <div x-data="{show:true}" x-init="setTimeout(()=> show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{$message}}
                                </div>
                            </div>
                            @enderror
                        </div>
                        {{-- Amenitas --}}
                        <div class="px-5 mb-6 space-y-5" x-data="amenitas()" x-init="$nextTick(()=>{select2WithAlpine()})">
                            <label for="" class="block text-sm font-bold text-[#333333] dark:text-gray" for="amenitas_xstay_label">Amenitas Kamar</label>
                            <div>
                                <select name="attributes_amenitas" id="attributes_amenitas" x-model="attribute_amenitas" x-ref="select_amenitas"
                                class="attributes_amenitas mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                    <option value="0" seleted disabled>Pilih Amenitas</option>
                                    @foreach($attributes as $item)
                                    <option value="{{$item->id}}" data-name="{{$item->text}}" data-img="{{Storage::url($item->image)}}">{{$item->text}}</option>
                                    @endforeach
                                </select>
                                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray"></div>
                            </div>
                            <button type="button" @click="addNewField"
                                class="bg-[#9E3D64] text-white text-sm font-semibold py-2 px-7 rounded rounded-lg hover:bg-[#b94875] duration-2">
                                Tambah Fasilitas
                            </button>
                            <template x-if="error_amenitas===true">
                                <div x-init="setTimeout(()=> error_amenitas = false, 3000)" class="bg-red-300 rounded-md p-3 my-2 w-fit">
                                    <p x-text="error_empty_amenitas"></p>
                                </div>
                            </template>
                            <template x-if="amenities.length > 0" data-length="amenities.length">
                                <div class="w-fit min-h-fit max-h-96 overflow-y-auto rounded-md border border-slate-200">
                                    <label for="amenitas_name" class="sticky top-0 px-2 py-1 block text-base font-bold text-[#333333] dark:text-gray-300 bg-slate-300 rounded-t-md">Icon Fasilitas</label>
                                    <template x-for="(amenitas, index) in amenities" :key="index">
                                        <div class="my-1">
                                            <div class="flex gap-3 hover:bg-slate-100 rounded-md w-fit p-1 items-center">
                                                <img :src="amenitas.amenitas_img" alt="icon_amenitas" id="icon_amenitas_img" class="ic_amenitas border flex justify-center items-center rounded border-gray-300 p-1" witdh="42px" height="42px">
                                                <div class="space-y-1">
                                                    <div class="flex">
                                                        <input type="hidden" name="amenitas_id[]" x-model="amenitas.amenitas_id" id="amenitas_id" required>
                                                        <p id="amenitas_name" class="text-gray-900 text-base rouded-lg block break-words w-96 p-2.5" x-text="amenitas.amenitas_name"></p>
                                                        <div class="flex justify-center">
                                                            <button class="mx-1" type="button" @click="removeField(index)">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                    height="24" viewBox="0 0 24 24">
                                                                    <path fill="currentColor"
                                                                        d="M12 20c-4.41 0-8-3.59-8-8s3.59-8 8-8s8 3.59 8 8s-3.59 8-8 8m0-18A10 10 0 0 0 2 12a10 10 0 0 0 10 10a10 10 0 0 0 10-10A10 10 0 0 0 12 2M7 13h10v-2H7" />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </template>
                                </div>
                            </template>
                            @error('amenitas_id')
                            <div x-data="{show:true}" x-init="setTimeout(()=> show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{$message}}
                                </div>
                            </div>
                            @enderror
                        </div>
                        {{-- Video Embed --}}
                        <div class="px-5 mb-6">
                            <label for="vidoe-embed" class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray">Video Embed</label>
                            <input type="text" 
                                class="bg-[#FFFFF] border border-[#4F4F4F4] text-gray-900 text-sm rounded-lg focus:ring-blue-500 block w-96 p-2.5 dark:bg-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
                                placeholder="Link video embed" name="video_embed">
                        </div>
                        {{-- <div class="px-5 mb-6" x-data="fasilitas()">
                            <label class="block mb-2 text-sm font-bold text-[#333333]">Fasilitas</label>
                            <div class="px-3">
                                <div>
                                    <button type="button"
                                        class="px-3 py-1 my-3 text-sm text-white rounded-full btn btn-info bg-kamtuu-second"
                                        id="dynamic-ar">Tambah +</button>
                                </div>
                            </div>
                            <div class="flex space-x-5 mb-5">
                                <table id="dynamicAddRemove">
                                </table>
                            </div>
                            @error('icon_fasilitas')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                            @error('icon_fasilitas.*')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                            @error('deskripsi_fasilitas')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                            @error('deskripsi_fasilitas.*')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div> --}}

                        {{-- Amenitas --}}
                        {{-- <div class="px-5 mb-6" x-data="amenitas()">
                            <label class="block mb-2 text-sm font-bold text-[#333333]">Amenitas Kamar</label>
                            <div class="px-3">
                                <div>
                                    <button type="button"
                                        class="px-3 py-1 my-3 text-sm text-white rounded-full btn btn-info bg-kamtuu-second"
                                        id="dynamic-ar1">Tambah +</button>
                                </div>
                            </div>
                            <div class="flex space-x-5 mb-5">
                                <table id="dynamicAddRemove1">
                                </table>
                            </div>
                            @error('icon_amenitas')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                            @error('icon_amenitas.*')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                            @error('judul_amenitas')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                            @error('judul_amenitas.*')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                            @error('isi_amenitas')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                            @error('isi_amenitas.*')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div> 
                        --}}

                        {{-- Deskripsi Hotel --}}
                        <div class="px-5 pt-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Deskripsi
                                XStay</label>
                            <textarea class="w-[10rem] click2edit" id="deskripsi" name="deskripsi"
                                x-init="$nextTick(() => { initSummerNote() })">{{ old('deskripsi') }}
                            </textarea>
                            @error('deskripsi')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                            <div id="deskripsi_error" class="hidden my-3 w-full py-3 px-2 bg-red-300 rounded-md">
                                Deskripsi Hotel wajib diisi!
                            </div>
                        </div>

                        <div class="px-5 pt-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Catatan</label>
                            <textarea class="w-[10rem] click2edit" id="summernote" name="catatan"
                                x-init="$nextTick(() => { initSummerNote() })">{{ old('catatan') }}
                            </textarea>
                            @error('catatan')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>

                        <x-be.com.one-button></x-be.com.one-button>
                    </form>
                </div>

            </div>
        </div>
    </div>
    </div>

    <script>
        function fasilitas() {
            return {
                fasilities:[],
                attribute_fasilitas:0,
                attribute_id:0,
                attribute_name:'',
                attribute_img:'',
                error_empty_fasilitas:'',
                error_fasilitas:false,
                fields: [],
                addNewField() {
                    if(this.attribute_id === 0){
                        this.error_fasilitas=true
                        this.error_empty_fasilitas = 'Fasilitas wajib diisi'
                        return this.error_empty_fasilitas,this.error_fasilitas
                    }

                    this.fasilities.push({
                        fasilitas_id:this.attribute_id,
                        fasilitas_name:this.attribute_name,
                        fasilitas_img:this.attribute_img,
                    })

                    this.error_empty_fasilitas = ''
                    // this.fields.push({
                    //     icon: '',
                    //     description: this.description,
                    // });
                    console.log(this.attribute_name)
                },
                removeField(index) {
                    // console.log(index)
                    this.fasilities.splice(index,1)
                    // this.fields.splice(index, 1);
                    // console.log(this.fields)
                },
                select2WithAlpine(){
                    this.select2 = $(this.$refs.select_fasilitas).select2({
                        placeholder:'Pilih Fasilitas'
                    });

                    this.select2.on("select2:select",(event)=>{
                        this.attribute_id = event.target.value;
                        this.attribute_name = this.select2.find(":selected").data("name");
                        this.attribute_img = this.select2.find(":selected").data("img");
                    });

                    this.$watch("attribute_fasilitas",(value)=>{
                        this.select2.val(value).trigger("change")
                    })
                }
            }
        }
    </script>

    <script>
        function amenitas() {
            return {
                amenities:[],
                attribute_amenitas:0,
                attribute_id:0,
                attribute_name:'',
                attribute_img:'',
                error_empty_amenitas:'',
                error_amenitas:false,
                fields: [],
                addNewField() {
                    
                    if(this.attribute_id === 0){
                        this.error_amenitas=true
                        this.error_empty_amenitas = 'Amenitas wajib diisi'
                        return this.error_empty_amenitas,this.error_amenitas
                    }

                    this.amenities.push({
                        amenitas_id:this.attribute_id,
                        amenitas_name:this.attribute_name,
                        amenitas_img:this.attribute_img,
                    })

                    this.error_empty_fasilitas = ''
                    // this.fields.push({
                    //     icon: '',
                    //     title: this.title,
                    //     description: this.description,
                    // });
                    // // console.log(this.fields)
                },
                removeField(index) {
                    this.amenities.splice(index,1)
                    // console.log(index)
                    // this.fields.splice(index, 1);
                    // console.log(this.fields)
                },
                select2WithAlpine(){
                    this.select2 = $(this.$refs.select_amenitas).select2({
                        placeholder:'Pilih Amenitas'
                    });

                    this.select2.on("select2:select",(event)=>{
                        this.attribute_id = event.target.value;
                        this.attribute_name = this.select2.find(":selected").data("name");
                        this.attribute_img = this.select2.find(":selected").data("img");
                    });

                    this.$watch("attribute_amenitas",(value)=>{
                        this.select2.val(value).trigger("change")
                    })
                }
            }
        }

        $('#informasi_dasar_ID').on('submit', function(e) {
            e.preventDefault();
            let deskripsi_error = false

            if ($('#deskripsi').summernote('isEmpty')) {
                document.getElementById("deskripsi_error").classList.remove("hidden");
                document.getElementById("deskripsi_error").classList.add("flex");
                deskripsi_error = true
            } else {
                document.getElementById("deskripsi_error").classList.remove("flex");
                document.getElementById("deskripsi_error").classList.add("hidden");
                deskripsi_error = false
            }

            if (!deskripsi_error) {
                this.submit();
            }
        })
    </script>

    <script>
        const initSummerNote = () => {
            $('.click2edit').summernote({
                placeholder: 'Hello stand alone ui',
                tabsize: 2,
                height: 120,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    // ['table', ['table']],
                    // ['insert', ['link', 'picture', 'video']],
                    // ['view', ['fullscreen', 'codeview', 'help']]
                ],
                dialogsInBody: true
            });
        }
    </script>

    {{-- Fasilitas --}}
    <script type="text/javascript">
        $("#dynamic-ar").click(function() {
            $("#dynamicAddRemove").append(
                `<tr>
                    <td>
                        <label for="icon-fasilitas" class="block mb-2 text-sm font-semibold text-[#333333]">Icon</label>
                        <input type="file" name="icon_fasilitas[]" id="input_fasilitas" class="input_fasilitas" style="width: 100px" />
                        <img id="icon-fasilitas[]"class="ic_fasilitas border flex justify-center items-center rounded border-gray-300 bg-gray-200 p-1 mt-1"src="{{ asset('storage/icons/upload.svg') }}" alt="" width="42px">
                    </td>
                    <td>
                        <label for="fasilitas" class="block mb-2 text-sm font-bold text-[#333333]">Keterangan</label>
                        <input type="text" id="fasilitas" name="deskripsi_fasilitas[]" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Kolam Renang">
                    </td>
                    <td class="p-3">
                        <button type="button" class="mx-2 remove-input-field"><img
                            src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                            alt="delete_button" width="25px">
                        </button>
                    </td>
                </tr>`
            );
        });
        $(document).on('click', '.remove-input-field', function() {
            $(this).parents('tr').remove();
        });
                
        // Change icon preview
        $(document).on('change', '.input_fasilitas', function(e) {  
            let el_index = $(this).index('.input_fasilitas')
            const file = this.files[0];
            if (file) {
                let reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function (e) {
                    $('.ic_fasilitas').each(function(i, el) {
                        if (i === el_index) {
                            $(el).attr("src", e.target.result);
                        }
                    })
                };
            }
        });
    </script>

    {{-- Amenitas --}}
    <script type="text/javascript">
        $("#dynamic-ar1").click(function() {
            $("#dynamicAddRemove1").append(
                `<tr>
                    <td>
                        <label for="icon-fasilitas" class="block mb-2 text-sm font-semibold text-[#333333]">Icon</label>
                        <input id="icon-fasilitas" type="file" name="icon_amenitas[]" class="input_amenitas" style="width: 100px">
                        <img id="icon-fasilitas" class="ic_amenitas border flex justify-center items-center rounded border-gray-300 bg-gray-200 p-1 mt-1" src="{{ asset('storage/icons/upload.svg') }}" alt="" width="42px">
                    </td>
                    <td class="space-y-3">
                        <div>
                            <label for="keterangan-fasilitas" class="block mb-2 text-sm font-bold text-[#333333]">Judul Amenitas</label>
                            <input type="text" id="keterangan-fasilitas" name="judul_amenitas[]" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Kolam Renang">
                        </div>
                        <div>
                            <label for="keterangan-fasilitas" class="block mb-2 text-sm font-bold text-[#333333]">Isi Amenitas</label>
                            <input type="text" id="keterangan-fasilitas" name="isi_amenitas[]" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Kolam Renang">
                        </div>
                    </td>
                    <td class="p-3">
                        <button type="button" class="mx-2 remove-input-field1"><img
                            src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                            alt="delete_button" width="25px">
                        </button>
                    </td>
                </tr>`
            );
        });
        $(document).on('click', '.remove-input-field1', function() {
            $(this).parents('tr').remove();
        });
                
        // Change icon preview
        $(document).on('change', '.input_amenitas', function(e) {  
            let el_index = $(this).index('.input_amenitas')
            const file = this.files[0];
            if (file) {
                let reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function (e) {
                    $('.ic_amenitas').each(function(i, el) {
                        if (i === el_index) {
                            $(el).attr("src", e.target.result);
                        }
                    })
                };
            }
        });
    </script>
    <script>
        $('.regency_id').select2({
            placeholder: "Pilih Lokasi",
            allowClear: true
        });
    </script>
</body>

</html>