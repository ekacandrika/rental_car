<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        .link-not-active {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
            color: #858585;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param || null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <x-be.seller.sidebar-toko>
        <x-slot name="addListingHotel">
            @if ($data->productdetail == null)
            {{ route('hotelinfodasar') }}
            @else
            {{ route('hotel.edit', $data->product_code) }}
            @endif
        </x-slot>
        <x-slot name="faq">
            @if ($data->productdetail->faq == null)
            {{ route('hotelfaq') }}
            @else
            {{ route('hotelfaq.edit', $data->product_code) }}
            @endif
        </x-slot>

        {{-- Xstay --}}
        <x-slot name="addListingXstay">
            @if ($xstay->productdetail == null)
            {{ route('infodasar') }}
            @else
            {{ route('infodasar.edit', $xstay->product_code) }}
            @endif
        </x-slot>
        <x-slot name="faqXstay">
            @if ($xstay == null)
            {{ route('faq.index') }}
            @else
            {{ route('faq.edit', $xstay->product_code) }}
            @endif
        </x-slot>
    </x-be.seller.sidebar-toko>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10">
            <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
            {{-- Breadcumbs --}}
            <nav class="flex" aria-label="Breadcrumb">
                <ol class="inline-flex items-center space-x-1 md:space-x-3">
                    <li class="inline-flex items-center">
                        @if (isset($data->product_name))
                        <a href="{{ route('infodasar.edit', $data->product_code) }}"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                            Informasi Dasar</a>
                        @else
                        <a href="{{ route('infodasar.edit', $data->product_code) }}"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Isi
                            Informasi Dasar</a>
                        @endif

                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->id_kamar))
                            <a href="{{ route('xstay.infokamar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Informasi
                                Kamar</a>
                            @else
                            <a href="{{ route('xstay.infokamar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Informasi
                                Kamar</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->discount))
                            <a href="{{ route('harga.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Harga
                                & Ketersediaan</a>
                            @else
                            <a href="{{ route('harga.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Harga
                                & Ketersediaan</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->batas_pembayaran))
                            <a href="{{ route('bayar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Batas
                                Pembayaran & Pembatalan</a>
                            @else
                            <a href="{{ route('bayar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Batas
                                Pembayaran & Pembatalan</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->link_maps) || isset($data->productdetail->foto_maps_1))
                            <a href="{{ route('maps.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Peta
                                & Foto</a>
                            @else
                            <a href="{{ route('maps.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Peta
                                & Foto</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->izin_ekstra))
                            <a href="{{ route('ekstra.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Ekstra</a>
                            @else
                            <a href="{{ route('ekstra.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Ekstra</a>
                            @endif
                        </div>
                    </li>
                </ol>
            </nav>

            {{-- Section 1 --}}
            <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded">
                {{-- <form action="{{ route('hargaResiden.update', $data->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="flex justify-between">
                        <div class="text-lg font-bold font-inter text-[#9E3D64]">Harga Dalam Mata Uang Rupiah (IDR)
                        </div>
                    </div>

                    <div class="grid grid-cols-2 pb-2">
                        <div class="text-sm font-bold text-[#333333]">Residen</div>
                        <div class="text-sm font-bold text-[#333333]">
                            <p>Non-Residen</p>
                            <p class=" text-xs font-normal">(Bila tidak diisi harga berlaku sama)</p>
                        </div>
                    </div>

                    <div class="grid grid-cols-2">
                        <div class="">
                            <p class="block md:hidden text-sm font-bold text-[#333333] mb-6">Residen</p>
                            <div class="mb-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Dewasa</label>
                                <input type="text" id="text" name="dewasa_residen"
                                    value="{{ $data2->harga->dewasa_residen }}"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="120000">
                            </div>
                            <div class="mb-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Anak (5
                                    - 12
                                    Tahun)</label>
                                <input type="text" id="text" name="anak_residen"
                                    value="{{ $data2->harga->anak_residen }}"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="60000">
                            </div>
                            <div class="mb-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Balita
                                    (< 5 Tahun)</label>
                                        <input type="text" id="text" name="balita_residen"
                                            value="{{ $data2->harga->balita_residen }}"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="0">
                            </div>
                            <button class="text-white bg-[#23AEC1] py-1 px-3 rounded-md">Perbarui</button>
                        </div>
                        <div>
                            <div class="block md:hidden text-sm font-bold text-[#333333] mb-6">
                                <p>Non-Residen</p>
                                <p class=" text-xs font-normal">(Bila tidak diisi harga berlaku sama)</p>
                            </div>

                            <div class="mb-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Dewasa</label>
                                <input type="text" id="text" name="dewasa_non_residen"
                                    value="{{ $data2->harga->dewasa_non_residen }}"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="120000">
                            </div>
                            <div class="mb-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Anak (5
                                    -
                                    12
                                    Tahun)</label>
                                <input type="text" id="text" name="anak_non_residen"
                                    value="{{ $data2->harga->anak_non_residen }}"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="60000">
                            </div>
                            <div class="mb-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Balita
                                    (< 5 Tahun)</label>
                                        <input type="text" id="text" name="balita_non_residen"
                                            value="{{ $data2->harga->balita_non_residen }}"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="0">
                            </div>
                        </div>
                    </div>
                </form> --}}

                <form action="{{ route('harga.Grup.update', $data->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="my-2" x-data="handler()">
                        <div class="mb-2">
                            <div class="text-lg font-bold font-inter text-[#9E3D64]">Diskon grup dari jumlah orang
                                dewasa
                                (IDR)
                            </div>
                        </div>
                        <table class="my-5 w-1/2 border table-auto rounded-md" id="dynamicAddRemove">
                            <thead>
                                <tr>
                                    <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Min Jumlah Orang</th>
                                    <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Maks Jumlah Orang</th>
                                    <th class="p-3 border border-slate-500 bg-[#BDBDBD]">%</th>
                                    <th class="p-3 border border-slate-500 bg-[#BDBDBD]"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <div class="px-3">
                                    <div>
                                        <button type="button"
                                            class="px-3 py-1 my-3 text-sm text-white rounded-full btn btn-info bg-kamtuu-second"
                                            id="dynamic-ar">Tambah +</button>
                                    </div>
                                </div>@php
                                $value['max_orang'] = $array['max_orang'];
                                $value['min_orang'] = $array1['min_orang'];
                                $value['diskon_orang'] = $array2['diskon_orang'];
                                @endphp
                                @foreach ($value['min_orang'] as $key => $item)
                                <tr class="align-middle">
                                    <td class="p-3 border border-slate-500 text-center">
                                        <input type="text" id="text" name="min_orang[]" value="{{ $item }}"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="">
                                    </td>
                                    <td class="p-3 border border-slate-500 text-center">
                                        <input type="text" id="text" name="max_orang[]"
                                            value="{{ $value['max_orang'][$key] }}"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="">
                                    </td>
                                    <td class=" p-3 border border-slate-500 text-center">
                                        <input type="text" id="text" name="diskon_orang[]"
                                            value="{{ $value['diskon_orang'][$key] }}"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="">
                                    </td>
                                    <td class="p-3 border border-slate-500 text-center">
                                        <button type="button" class="btn btn-outline-primary remove-input-field-remove">
                                            Delete
                                            <img src="{{ asset('storage/icons/delete-dynamic-data.png') }}" alt=""
                                                width="16px">
                                        </button>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>

                    <button class="text-white bg-[#23AEC1] py-1 px-3 rounded-md">Perbarui</button>
                </form>
                @if ($errors->hasBag('diskon_orang'))
                <div class="bg-red-300 rounded-md p-3 my-5">
                    @foreach ($errors->diskon_orang->toArray() as $key=>$value)
                    @foreach ($value as $index=>$err)
                    <p>- {{ $err}}</p>
                    @endforeach
                    @endforeach
                </div>
                @endif
            </div>

            {{-- Section 2 --}}
            {{-- <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded" x-data="discountDate()">
                <form action="{{ route('harga.diskonDate.update', $data->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="flex md:space-x-10">
                        <div class="mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Dari
                                Tanggal</label>
                            <div
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <input type="text" name="tgl_start" id="tgl_start" placeholder="">
                            </div>
                        </div>
                        <div class="mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Sampai
                                Tanggal</label>
                            <div
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <input type="text" name="tgl_end" id="tgl_end" placeholder="">
                            </div>
                        </div>
                        <div class="mb-6">
                            <label for="text" class="flex mb-2 text-sm font-bold text-[#333333]"
                                x-data="{ tooltip: false }" x-on:mouseover="tooltip = true"
                                x-on:mouseleave="tooltip = false">x/- (%)>
                                <img class="mx-2" width="15px"
                                    src="{{ asset('storage/icons/circle-info-solid (1) 1.svg') }}" alt="">
                                <div x-show="tooltip"
                                    class="text-sm text-white absolute bg-[#9E3D64] rounded-lg p-2 transform -translate-y-8 translate-x-8">
                                    This is the Tooltip.
                                </div>
                            </label>
                            <input type="text" id="datepickerFormulirTur" name="discount"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="">
                        </div>
                    </div>
                    <button class="text-white bg-[#23AEC1] py-1 px-3 rounded-md" @click="addNewField()">Tambah</button>

                    <div class="grid grid-cols-10 my-5">
                        <div class="col-span-9 border border-slate-500 rounded-md ">
                            <div class="my-2 mx-3" x-data="{ open: false }">
                                <div class="flex border-b border-gray-200 pt-1 pb-2">
                                    <button
                                        class="text-lg sm:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg flex space-x-10"
                                        x-on:click="open = !open">
                                        <div class="cursor-pointer">
                                            <p for="text" class="block mb-2 text-sm font-bold text-slate-500">Dari
                                                Tanggal</p>
                                            <p class=" text-base font-bold text-[#333333]">-</p>
                                        </div>
                                        <div class="cursor-pointer">
                                            <p for="text" class="block mb-2 text-sm font-bold text-slate-500">
                                                Sampai
                                                Tanggal</p>
                                            <p class=" text-base font-bold text-[#333333]">-</p>
                                        </div>
                                        <div class="cursor-pointer">
                                            <p for="text" class="block mb-2 text-sm font-bold text-slate-500">+/-
                                                (%)
                                            </p>
                                            <p class=" text-base font-bold text-[#333333]">-</p>
                                        </div>
                                    </button>
                                    <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }"
                                        x-on:click="open = !open"
                                        src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}" alt="chevron-down"
                                        width="26px" height="15px">
                                </div>
                                <div class="py-2" x-show="open" x-transition>
                                    <table class="my-1 w-1/2 table-auto">
                                        <thead>
                                            <tr>
                                                <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]"></th>
                                                <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">Dewasa</th>
                                                <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">Anak</th>
                                                <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">Balita</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="align-middle">
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>Residen</p>
                                                </td>
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>96000</p>
                                                </td>
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>48000</p>
                                                </td>
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>0</p>
                                                </td>
                                            </tr>
                                            <tr class="align-middle">
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>Non-Residen</p>
                                                </td>
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>192000</p>
                                                </td>
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>96000</p>
                                                </td>
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>0</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="px-3">
                            <button
                                class="bg-[#D50006] text-white text-sm font-semibold py-2 px-5 rounded-lg hover:bg-[#de252b]">
                                Hapus
                            </button>
                        </div>

                    </div>
                    <x-be.com.two-button-dup>
                        {{ route('xstay.infokamar.edit', $data->product_code) }}
                    </x-be.com.two-button-dup>
                </form>
            </div> --}}
            {{-- Section 2 --}}
            <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded" x-data="discountDate()">

                <p class="font-bold text-lg text-[#333333] pb-5">Ketersediaan</p>
                <div class="flex md:space-x-10 gap-5">
                    <div class="mb-6">
                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Dari
                            Tanggal</label>
                        <div
                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            <input type="text" id="tgl_start" name="tgl_start" placeholder="" required>
                        </div>
                    </div>
                    <div class="mb-6">
                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Sampai
                            Tanggal</label>
                        <div
                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            <input type="text" id="tgl_end" name="tgl_end" placeholder="" required>
                        </div>
                    </div>
                    <div class="pb-5">
                        <label for="text" class="flex mb-2 text-sm font-medium text-[#333333]"
                            x-data="{ tooltip: false }" x-on:mouseover="tooltip = true"
                            x-on:mouseleave="tooltip = false">x/- (%)
                            <img class="mx-2" width="15px"
                                src="{{ asset('storage/icons/circle-info-solid (1) 1.svg') }}" alt="">
                            <div x-show="tooltip"
                                class="w-96 text-sm text-white absolute bg-[#9E3D64] rounded-lg p-2 transform -translate-y-8 translate-x-8">
                                Apabila diisi <strong>tanpa tanda minus</strong>, maka akan menaikkan harga dasar
                                sebesar %.
                                Apabila diisi <strong>dengan tanda minus</strong>, akan menurunkan harga sebesar %.
                            </div>
                        </label>
                        <input type="text" id="discount" name="discount" x-model="discount"
                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="" required>
                    </div>
                </div>
                <template x-if="error_empty_data != ''">
                    <div class="bg-red-300 rounded-md p-3 my-2">
                        <p x-text="error_empty_data"></p>
                    </div>
                </template>
                <template x-if="error_date != ''">
                    <div class="bg-red-300 rounded-md p-3 my-2">
                        <p x-text="error_date"></p>
                    </div>
                </template>
                <template x-if="error_number != ''">
                    <div class="bg-red-300 rounded-md p-3 my-2">
                        <p x-text="error_number"></p>
                    </div>
                </template>
                @if ($errors->hasBag('diskon_ketersediaan'))
                <div class="bg-red-300 rounded-md p-3 my-5">
                    @foreach ($errors->diskon_ketersediaan->toArray() as $key=>$value)
                    @foreach ($value as $index=>$err)
                    <p>- {{ $err}}</p>
                    @endforeach
                    @endforeach
                </div>
                @endif
                <button class="text-white bg-[#23AEC1] py-1 px-3 rounded-md" @click="addNewField()">Tambah</button>

                <form action="{{ route('harga.diskonDate.update', $data->id) }}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PUT')


                    <div class="grid grid-cols-10 my-5">
                        <template x-if="details.length === 0">
                            <div class="col-span-5 border border-slate-300 rounded-md ">
                                <div class="my-2 mx-3">
                                    <p>Anda belum menambahkan Tanggal ketersedian dan diskon.</p>
                                </div>
                            </div>
                        </template>
                        <template x-if="details.length >= 1">
                            <template x-for="(detail, index) in details" :key="index">
                                <div class="col-span-10 flex">
                                    <div class="w-5/6 border border-slate-500 rounded-md my-5">
                                        <div class="my-2 mx-3" x-data="{ open:false }">
                                            <div class="flex border-b border-gray-200 pt-1 pb-2">
                                                <button type="button"
                                                    class="text-lg sm:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg flex space-x-10"
                                                    x-on:click="open = !open">
                                                    <div class="cursor-pointer pr-5">
                                                        <p for="text"
                                                            class="block mb-2 text-sm font-bold text-slate-500">
                                                            Dari
                                                            Tanggal</p>
                                                        <p class=" text-base font-bold text-[#333333]"
                                                            x-text="detail.tgl_start"></p>
                                                        <input type="hidden" x-model="detail.tgl_start"
                                                            :name="'details['+index+'][tgl_start]'">
                                                    </div>
                                                    <div class="cursor-pointer pr-5">
                                                        <p for="text"
                                                            class="block mb-2 text-sm font-bold text-slate-500">
                                                            Sampai
                                                            Tanggal</p>
                                                        <p class=" text-base font-bold text-[#333333]"
                                                            x-text="detail.tgl_end"></p>
                                                        <input type="hidden" x-model="detail.tgl_end"
                                                            :name="'details['+index+'][tgl_end]'">
                                                    </div>
                                                    <div class="cursor-pointer pr-5">
                                                        <p for="text"
                                                            class="block mb-2 text-sm font-bold text-slate-500">
                                                            +/- (%)
                                                        </p>
                                                        <p class=" text-base font-bold text-[#333333]"
                                                            x-text="detail.discount"></p>
                                                        <input type="hidden" x-model="detail.discount"
                                                            :name="'details['+index+'][discount]'">
                                                    </div>
                                                </button>
                                                <img class="float-right duration-200 cursor-pointer"
                                                    :class="{'rotate-180' : open}" x-on:click="open = !open"
                                                    src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                                    alt="chevron-down" width="26px" height="15px">
                                            </div>
                                            <div class="py-2" x-show="open" x-transition>
                                                <table class="my-1 w-full table-auto">
                                                    <thead>
                                                        <tr>
                                                            <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                Nama Kamar
                                                            </th>
                                                            <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                Kode Kamar
                                                            </th>
                                                            <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                Jenis Kamar
                                                            </th>
                                                            <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                Harga
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {{-- @dump($rooms_array) --}}
                                                        <template x-if="rooms.length > 0" :data-length="rooms.length">
                                                            <template x-for="(kamar, index) in rooms" :key="index">
                                                                <tr class="align-middle">
                                                                    <td class="p-3 border-2 border-slate-500 text-center">
                                                                        <p x-text="kamar.kamar_name">
                                                                        </p>
                                                                    </td>
                                                                    <td class="p-3 border-2 border-slate-500 text-center">
                                                                        <p x-text="kamar.kamar_code">
                                                                        </p>
                                                                    </td>
                                                                    <td class="p-3 border-2 border-slate-500 text-center">
                                                                        <p x-text="kamar.kamar_category">
                                                                        </p>
                                                                    </td>
                                                                    <td class="p-3 border-2 border-slate-500 text-center">
                                                                        <p x-text="parseInt(kamar.room_price)+((detail.discount/100)*parseInt(kamar.room_price))">
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </template>
                                                        </template>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-1/6 px-3 my-5">
                                        <button @click="removeField(index)" type="button"
                                            class="bg-[#D50006] text-white text-sm font-semibold py-2 px-5 rounded-lg hover:bg-[#de252b]">
                                            Hapus
                                        </button>
                                    </div>
                                </div>
                            </template>
                        </template>
                    </div>

                    {{-- <x-be.com.two-button></x-be.com.two-button> --}}
                    {{-- <x-be.com.two-button-dup>
                        {{ isset($isedit) ? route('activity.viewAddListingEdit',$id) : route('activity') }}
                    </x-be.com.two-button-dup> --}}
                    @if ($data2->masterkamar == null)
                    <x-be.com.two-button-dup>
                        {{ route('xstayinfokamar') }}
                    </x-be.com.two-button-dup>
                    @else
                    <x-be.com.two-button-dup>
                        {{ route('xstay.infokamar.edit', $data->product_code) }}
                    </x-be.com.two-button-dup>
                    @endif
                </form>
            </div>
        </div>
    </div>
    </div>

    <script>
        var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        $(document).ready(function() {
            $('#tgl_start').datepicker({
                change: function (e) {
                    today = $('#tgl_start').datepicker().value();
                    $('#tgl_end').datepicker().destroy();
                    $('#tgl_end').datepicker({
                            minDate: today
                        });
                },
                minDate: today
            });
        })
        let datepicker2 = $(document).ready(function() {
            $('#tgl_end').datepicker({
                minDate: today
            });
        })
    </script>

    <script>
        function handler() {

            return {
                fields: [],
                addNewField() {
                    this.fields.push({
                        txt1: ''
                    });
                    // console.log(this.fields)
                },
                removeField(index) {
                    this.fields.splice(index, 1);
                    console.log(this.fields)
                }
            }
        }
    </script>

    <script>
        // function discountDate() {

        //     return {
        //         date_from: '',
        //         date_until: '',
        //         discount: 0,
        //         detail: [],
        //         addNewField() {
        //             this.detail.push({
        //                 date_from: this.date_from,
        //                 date_until: '',
        //                 discount: 0
        //             });
        //             console.log(this.fields)
        //         },
        //         removeField(index) {
        //             this.fields.splice(index, 1);
        //             console.log(this.fields)
        //         }
        //     }
        // }

        function discountDate() {
                
                return {
                    tgl_start: '',
                    tgl_end: '',
                    discount: '',
                    error_empty_data: '',
                    error_date: '',
                    error_number: '',
                    // dewasa_residen: {!! isset($harga['dewasa_residen']) ? $harga['dewasa_residen'] : "null" !!},
                    // anak_residen: {!! isset($harga['anak_residen']) ? $harga['anak_residen'] : "null" !!},
                    // balita_residen: {!! isset($harga['balita_residen']) ? $harga['balita_residen'] : "null" !!},
                    // dewasa_non_residen: {!! isset($harga['dewasa_non_residen']) ? $harga['dewasa_non_residen'] : "null" !!},
                    // anak_non_residen: {!! isset($harga['anak_non_residen']) ? $harga['anak_non_residen'] : "null" !!},
                    // balita_non_residen: {!! isset($harga['balita_non_residen']) ? $harga['balita_non_residen'] : "null" !!},
                    rooms:{!! isset($rooms_array) ? $rooms_array:'[]' !!},
                    details: {!! isset($details) ? $details : "[]" !!},
                    addNewField() {
                        let digit = /^-?\d+\.?\d*$/
                        this.error_empty_data = ''
                        this.error_date = ''
                        this.error_number = ''
    
                        if ($('#tgl_start').datepicker().value() > $('#tgl_end').datepicker().value()) {
                            this.error_date = 'Tanggal mulai harus lebih kecil dari tanggal selesai!'
                        }
                        if ($('#tgl_start').datepicker().value() == '' || $('#tgl_end').datepicker().value() == '' || this.discount == '') {
                            this.error_empty_data = 'Data wajib diisi!'
                        }
    
                        if (!digit.test(this.discount)) {
                            this.error_number = 'Pastikan discount berupa angka!'
                        }
    
                        if (this.error_empty_data != '' || this.error_date != '' || this.error_number != '') {
                            return this.error_date, this.error_empty_data, this.error_number
                        }
    
                        this.details.push({
                            tgl_start: $('#tgl_start').datepicker().value(),
                            tgl_end: $('#tgl_end').datepicker().value(),
                            discount: this.discount
                        });
                        this.tgl_start = $('#tgl_start').datepicker().value('')
                        this.tgl_end = $('#tgl_end').datepicker().value('')
                        this.discount = ''
                        this.error_empty_data = ''
                        this.error_date = ''
                        this.error_number = ''
                        console.log(this.rooms);
                    },
                    removeField(index) {
                        this.details.splice(index, 1);
                    }
                }
            }
    </script>

    <script type="text/javascript">
        $("#dynamic-ar").click(function() {
            $("#dynamicAddRemove").append(
                '<tr><td class="p-3 border border-slate-500 text-center"><input type="text" id="text" name="min_orang[]" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder=""></td><td class="p-3 border border-slate-500 text-center"><input type="text" id="text" name="max_orang[]" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder=""></td><td class=" p-3 border border-slate-500 text-center"><input type="text" id="text" name="diskon_orang[]" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder=""></td><td class="p-3 border border-slate-500 text-center"><button type="button" class="mx-2 remove-input-field">Delete</button></td></tr>'
            );
        });
        $(document).on('click', '.remove-input-field', function() {
            $(this).parents('tr').remove();
        });
    </script>

    <script type="text/javascript">
        $(document).on('click', '.remove-input-field-remove', function() {
        $(this).parents('tr').remove();
    });
    </script>
</body>

</html>