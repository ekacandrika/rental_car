<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- base | always include -->
    <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.1.1/dist/css/base/jquery.fonticonpicker.min.css">

    <!-- default grey-theme -->
    <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.0.0-alpha.0/dist/css/themes/grey-theme/jquery.fonticonpicker.grey.min.css">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js" defer></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        .select2-container .select2-selection--single {
            height: 100%;
            /* padding-left: 0.75rem; */
            /* padding-right: 0.75rem; */
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 100%;
        }

        .select2-selection__arrow {
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            /* padding-right: 0.75rem; */
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param || null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <x-be.seller.sidebar-toko>
        <x-slot name="addListingHotel">
            {{ route('hotelinfodasar') }}
        </x-slot>
        <x-slot name="faq">
            @if (!isset($data->faq))
            {{ route('hotelfaq') }}
            @else
            {{ route('hotelfaq.edit', $data->product_code) }}
            @endif
        </x-slot>

        {{-- Xstay --}}
        <x-slot name="addListingXstay">
            {{ route('infodasar') }}
        </x-slot>
        <x-slot name="faqXstay">
            @if (!isset($xstay))
            {{ route('faq.index') }}
            @else
            {{ route('faq.edit', $xstay->product_code) }}
            @endif
        </x-slot>
    </x-be.seller.sidebar-toko>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10">
            <span class="text-sm font-bold font-inter text-[#000000]">FAQ</span>

            <div class="bg-[#FFFFFF] drop-shadow-xl mt-3 p-10 rounded">
                <p class="px-5 pb-3">Pilih salah satu FAQ</p>
                <form action="{{ route('faq.code') }}" method="POST">
                    @csrf
                    <div class="px-5">
                        <select name="product_xstay" id="product_xstay"
                            class="product_xstay block w-1/3 mx-5 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                            <option selected disabled>Pilih Xstay</option>
                            @foreach ($faqs as $faq)
                            <option value="{{ $faq->id }}" {{ isset($faq_content) && $faq->id == $faq_content->id ?
                                'selected' : '' }}>
                                {{ $faq->slug }} - {{ $faq->product_name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <button id="faqbutton"
                        class="w-1/12 items-center mx-5 mt-3 px-5 py-2 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">Pilih</button>
                </form>

                @if (isset($faq_content))
                <form action="{{ route('faq.update', $faq_content->id) }}" method="POST" x-data="faqHandler()"
                    id="faq_form">
                    @csrf
                    @method('PUT')
                    <div class="px-5 pt-5">
                        <textarea class="w-[10rem] click2edit" id="summernote" name="faq" x-model="faq"
                            x-init="$nextTick(() => { initSummerNote() })">
                        </textarea>
                    </div>

                    {{-- <x-be.com.two-button></x-be.com.two-button> --}}
                    <div class="flex justify-end">
                        <button type="submit"
                            class="w-1/6 items-center mx-5 mt-2 px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                            Simpan
                        </button>
                    </div>
                </form>
                @endif
            </div>
            {{-- @if ($data == null)
            <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded">
                @if ($data == null)
                <textarea name="faq" id="" cols="30" rows="2"></textarea>
                @else
                <textarea name="faq" id="" cols="30" rows="2">{{ $data->productdetail->faq }}</textarea>
                @endif
                <div class="">
                    <div class="w-[10rem] click2edit" id="summernote" x-init="$nextTick(() => { initSummerNote() })">
                    </div>
                </div>

                <div class="flex justify-end pt-5">
                    <div>
                        <button type="submit"
                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                            Simpan
                        </button>
                    </div>
                </div>
            </div>
            @else
            <form action="{{ route('faq.update', $xstay->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded">
                    @if ($xstay->productdetail->faq == null)
                    <textarea name="faq" id="" cols="30" rows="2"></textarea>
                    @else
                    <textarea name="faq" id="" cols="30" rows="2">{{ $xstay->productdetail->faq }}</textarea>
                    @endif
                    <div class="">
                        <div class="w-[10rem] click2edit" id="summernote"
                            x-init="$nextTick(() => { initSummerNote() })">
                        </div>
                    </div>

                    <div class="flex justify-end pt-5">
                        <div>
                            <button type="submit"
                                class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                Simpan
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            @endif --}}
        </div>
    </div>
    </div>

    <script>
        const initSummerNote = () => {
            $('.click2edit').summernote({
                placeholder: 'Hello stand alone ui',
                tabsize: 2,
                height: 120,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    // ['insert', ['link', 'picture', 'video']],
                    // ['view', ['fullscreen', 'codeview', 'help']]
                ]
            });
        }

        $('#faq_form').on('submit', function() {
            if ($('#summernote').summernote('isEmpty')) {
                $('#summernote').val(null);
            }
        })
        
        function faqHandler() {

            return {
                faq: '{!! isset($faq_content->productdetail['faq']) ? $faq_content->productdetail['faq'] : '' !!}',
            }
        }
    </script>
    <script>
        $('.product_xstay').select2({
            placeholder: "Pilih Xstay",
            allowClear: true
        });
    </script>

    {{-- <script src="resource/assets/js/icon-picker.min.js"></script>
    <script>
        // Icon picker with `default` theme
        const iconPickerButton = new IconPicker('.btn', {
            theme: 'default',
            iconSource: ['FontAwesome Brands 5', 'FontAwesome Solid 5', 'FontAwesome Regular 5'],
            closeOnSelect: true
        });
    </script> --}}

</body>

</html>