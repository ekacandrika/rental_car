<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        /* BE Dashboard Navbar */
        #akun:checked+#akun {
            display: block;
        }

        .menu-dropdown li a {
            display: inline-block;
            color: white;
            /* text-align: center; */
            text-decoration: none;
        }

        .menu-dropdown li a:hover {
            background-color: none;
        }

        li.dropdown {
            display: inline-block;
            margin-right: 80px
        }

        .dropdown:hover .isi-dropdown {
            display: block;
        }

        .isi-dropdown a:hover {
            color: #fff !important;
            width: 100%;
        }

        .isi-dropdown {
            position: absolute;
            display: none;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
            background-color: #f9f9f9;
            width: 100%;
        }

        .isi-dropdown a {
            color: #3c3c3c !important;
            padding: 1%;
        }

        .isi-dropdown a:hover {
            color: #232323 !important;
            background: #f3f3f3 !important;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.seller.navbar-seller></x-be.seller.navbar-seller>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        <x-be.seller.sidebar-seller></x-be.seller.sidebar-seller>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-lg font-bold font-inter text-[#000000]">Upload Tanda Terima</span>
                {{-- Foto --}}
                <form action="{{ route('updateTandaTerima', $data->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="py-2" x-data="displayImage()">
                        <p class="font-semibold text-[#BDBDBD]">Upload</p>
                        <input class="py-2" type="file" name="tanda_terima" accept="image/*" @change="selectedFile"
                            multiple>
                        @if ($data->tanda_terima == null)
                            <template x-if="images.length < 1">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                            <template x-if="images.length >= 1">
                                <div class="flex">
                                    <template x-for="(image, index) in images" :key="index">
                                        <div class="flex justify-center items-center">
                                            <img :src="image"
                                                class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                :alt="'upload' + index">
                                            <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                    alt="" width="25px" @click="removeImage(index)">
                                            </button>
                                        </div>
                                    </template>
                                </div>
                            </template>
                        @else
                            <img src="{{ asset($data->tanda_terima) }}" alt="" class="w-[154px] h-[154px] mb-1"
                                @click="removeImage(index)">
                            <template x-if="images.length < 1">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                            <template x-if="images.length >= 1">
                                <div class="flex">
                                    <template x-for="(image, index) in images" :key="index">
                                        <div class="flex justify-center items-center">
                                            <img :src="image"
                                                class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                :alt="'upload' + index">
                                            <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                    alt="" width="25px" @click="removeImage(index)">
                                            </button>
                                        </div>
                                    </template>
                                </div>
                            </template>
                        @endif
                    </div>
                    <button type="submit"
                        class="w-40 items-center px-2 py-2 text-sm font-medium text-center text-white bg-[#9E3D64] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                        Upload
                    </button>
                </form>
            </div>
        </div>

    </div>

    <script>
        function displayImage() {

            return {
                images: [],

                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event) {
                    if (!event.target.files.length) return

                    let file = event.target.files

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                        };
                    }
                },

                removeImage(index) {
                    this.images.splice(index, 1);
                }
            }
        }
    </script>

    <script>
        function featuredImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                    this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                    if (!event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },

                removeImage() {
                    this.imageUrl = '';
                }
            }
        }
    </script>

</body>

</html>
