<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Chart Js -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.seller.navbar-seller></x-be.seller.navbar-seller>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        <x-be.seller.sidebar-seller></x-be.seller.sidebar-seller>
        {{-- @dump(auth()->user()) --}}
        <div class="col-start-3 col-end-11 z-0">
            {{-- style="padding-left:85px" --}}
            <div class="p-5 pr-10">
                <span class="text-sm font-bold font-inter text-[#000000]"></span>
                {{-- card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#ffffff] text-base">Penjualan Bulan Ini</p>
                                <p class="mt-4 font-bold text-[#ffffff] text-xl">Rp.
                                    {{number_format($penjualan_perbulan->sum('total_price'))}}</p>
                                <p class="mt-2 font-semibold text-[#ffffff] text-base">{{$penjualan_perbulan->count()}}
                                    pax</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#ffffff] text-base">Penjualan Tahun Ini</p>
                                <p class="mt-4 font-bold text-[#ffffff] text-xl">Rp.
                                    {{number_format($penjualan_pertahun->sum('total_price'))}}</p>
                                <p class="mt-2 font-semibold text-[#ffffff] text-base">{{$penjualan_pertahun->count()}}
                                    pax</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#ffffff] text-base">Penjualan Sejak Bergabung</p>
                                <p class="mt-4 font-bold text-[#ffffff] text-xl">Rp.
                                    {{number_format($penjualan_sejak_bergabung->sum('total_price'))}}</p>
                                <p class="mt-2 font-semibold text-[#ffffff] text-base">
                                    {{$penjualan_sejak_bergabung->count()}} Pax</p>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- grafik --}}
                <div class="grid grid-cols-2 text-center h-96">
                    <div class="text-center mr-2 rounded bg-[#ffffff] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-base text-[#333333]">Grafik Penjualan</p>
                        <canvas id="sellingChartGraphic"></canvas>
                    </div>
                    <div class="text-center mr-2 rounded bg-[#ffffff] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-base text-[#333333]">Grafik Pendapatan</p>
                        <canvas id="incomeChartGraphic"></canvas>
                    </div>
                </div>
                <div class="drop-shadow-xl py-5 my-5 rounded text-center">
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex rounded mr-5 bg-[#23AEC1]">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-base text-[#ffffff]">Pendapatan Bersih Bulan Ini</p>
                                <p class="mt-4 font-semibold text-xl text-[#ffffff]">Rp.
                                    {{number_format($pendapatan_perbulan->sum('komisi'))}}</p>
                            </div>
                        </div>
                        <div class="w-auto flex rounded mr-5 bg-[#FFB800]">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-base text-[#ffffff]">Pendapatan Bersih Tahun Ini</p>
                                <p class="mt-4 font-semibold text-xl text-[#ffffff]">Rp.
                                    {{number_format($pendapatan_pertahun->sum('komisi'))}}</p>
                            </div>
                        </div>
                        <div class="w-auto flex rounded mr-5 bg-[#51B449]">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-base text-[#ffffff]">Pendapatan Bersih Sejak Bergabung
                                </p>
                                <p class="mt-4 font-semibold text-xl text-[#ffffff]">Rp.
                                    {{number_format($pendapatan_sejak_bergabung->sum('komisi'))}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- table --}}
                <div class="overflow-x-auto relative">
                    <table class="w-full text-sm text-left border-2 border-[#333333]">
                        <tbody>
                            <tr class="bg-white border-b-2 border-[#333333]">
                                <th scope="row"
                                    class="py-4 px-6 border-r-2 border-[#333333] font-semibold text-[#333333] whitespace-nowrap drak:text-white">
                                    Tagihan Belum Dibayar (Rp)</th>
                                <td class="py-4 px-6">Rp. {{$tagihan_belum_terbayar->sum('total_price')}}</td>
                            </tr>
                            <tr class="bg-white border-b-2 border-[#333333]">
                                <th scope="row"
                                    class="py-4 px-6 border-r-2 border-[#333333] font-semibold text-[#333333] whitespace-nowrap drak:text-white">
                                    Jumlah Listing (Item)</th>
                                <td class="py-4 px-6">{{$data_listing->count()}} Item</td>
                            </tr>
                            <tr class="bg-white border-b-2 border-[#333333]">
                                <th scope="row"
                                    class="py-4 px-6 border-r-2 border-[#333333] font-semibold text-[#333333] whitespace-nowrap drak:text-white">
                                    Jumlah Listing Terjual (Item)</th>
                                <td class="py-4 px-6">{{$data->count()}} Item</td>
                            </tr>
                            <tr class="bg-white border-b-2 border-[#333333]">
                                <th scope="row"
                                    class="py-4 px-6 border-r-2 border-[#333333] font-semibold text-[#333333] whitespace-nowrap drak:text-white">
                                    Rangking Total Penghasilan (#)</th>
                                <td class="py-4 px-6">#{{$peringkat[0]+1}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="text-sm font-inter font-semibold mt-5">
                    <p>Anda membutuhkan Rp. <span class="px-5 bg-[#ffffff]">Rp.
                            {{number_format($pendapan_perseller[0])}}</span> untuk menjadi seller dengan total jumlah
                        penjualan tertinggi</p>
                    <p class="text-[#9E3D64] font-inter font-bold text-lg mt-2.5">Ayo Lebih Semangat!!!</p>
                </div>
            </div>
        </div>
    </div>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                    this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                    if (!event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },
            }
        }
    </script>

</body>

</html>
<script>
    //Chart penjualan
    var ctx = document.getElementById('sellingChartGraphic').getContext('2d');
    var  sellingChart = new Chart(ctx,{
        type:'line',
        data:{
            labels:'<?=json_encode($bulan_penjualan)?>',
            datasets:[{
                label:'Penjualan',
                data:'<?= json_encode(array_values($penjualan))?>',
                backgroundColor: '#B4E4FF',
                borderColor: '#B4E4FF',
                borderWidth: 1
            }]
        },
        options:{
            scales:{
                yAxes:[{
                    ticks:{
                        beginAtZero:true
                    }
                }]
            }
        }
    }) 

    //Chart Pendapatan
    var ctx = document.getElementById('incomeChartGraphic').getContext('2d');
    var sellingChart = new Chart(ctx,{
        type:'line',
        data:{
            labels:'<?=json_encode(($bulan_pendapatan))?>',
            datasets:[{
                label:'Pendapatan',
                data:'<?= json_encode(array_values($pendapatan))?>',
                backgroundColor: '#B4E4FF',
                borderColor: '#B4E4FF',
                borderWidth: 1
            }]
        },
        options:{
            scales:{
                yAxes:[{
                    ticks:{
                        beginAtZero:true
                    }
                }]
            }
        }
    }) 
</script>