<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js" defer></script>

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css" rel="stylesheet">

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        .swal-height {
            height: 90vh;
        }

        .disabled {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>
        {{-- @foreach ($locations as $location) --}}
        {{-- <option value="{{ $provinsi_data['provinces_id'] }}">{{ $provinsi_data['provinces_name'] }}</option> --}}
        {{-- @foreach ($location['regency'] as $key => $regency) --}}
        {{-- <option value="{{ $regency['regencies_id'] }}">{{ $regency['regencies_name'] }}</option> --}}
        {{-- @endforeach --}}
        {{-- @foreach ($location['district'] as $key => $district) --}}
        {{-- <option value="{{ $district['districts_id'] }}">{{ $district['districts_name'] }}</option> --}}
        {{-- @endforeach --}}
        {{-- @endforeach --}}
        <div class="col-start-3 col-end-11 z-0">
            {{-- @dump($add_listing) --}}
            <div class="p-5 pl-10 pr-10">
                <div class="flex gap-2">
                    <button type="button"
                        class="text-[#333333] font-medium rounded-lg text-sm py-2 text-center inline-flex items-center mr-2 hover:text-[#23AEC1]">
                        <img src="{{ asset('storage/icons/chevron.svg') }}" alt="user-profile" class="w-5 h-5 mr-1">
                        <span class="text-lg font-bold font-inter text-[#333333]">Tambah Listing</span>
                    </button>
                </div>
                {{-- Breadcumbs --}}
                {{-- @dump($rental['pilihan_ekstra']) --}}
                <nav class="flex py-5 mx-2.5" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            <a href="{{$isedit==true ? route('rental.viewAddListingEdit',$id):route('rental')}}"
                                class="inline-flex items-center text-sm font-bold font-inter text-[#23AEC1]">Detail
                                Kendaraan</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{ $isedit==true ? route('rental.viewPilihanEkstraEdit',$id):route('rental.viewPilihan')}}"
                                    class="inline-flex items-center text-sm font-bold font-inter {{isset($rental['pilihan_ekstra']) ? ($rental['pilihan_ekstra']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled') :'text-[#5f5e5e] disabled' }}">Pilihan</a>
                            </div>
                        </li>
                    </ol>
                </nav>

                <form action="{{ $isedit==true ? route('rental.addListing.edit', $id) : route('rental.addListing') }}"
                    method="POST" enctype="multipart/form-data" x-data="paketHandler()" id="formSubmitRental">
                    @csrf
                    <div class="bg-[#FFFFFF] drop-shadow-xl p-6 px-10 rounded">
                        <div class="grid grid-cols-2">
                            <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Detail Kendaraan</div>
                            <div class="text-sm font-bold font-inter p-5 text-[#333333] text-right">ID Listing:
                                {{ $listing }}</div>
                        </div>

                        <div class="block px-5 mb-5 flex gap-5">
                            <div>
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Jenis
                                    Kendaraan<span class="text-red-500 font-bold">*</span></label>
                                <select name="jenismobil" id="jenismobil"
                                    class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1"
                                    id="jenis_mobil">
                                    <option selected>Pilih Kendaraan</option>
                                    @foreach ($jenismobil as $jm)
                                    <option value="{{ $jm->id }}" {{isset($add_listing['id_jenis_mobil']) ?
                                        ($add_listing['id_jenis_mobil']==$jm->id?'selected':''):''}}>{{
                                        $jm->jenis_kendaraan }}
                                    </option>
                                    @endforeach
                                </select>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                            </div>
                            <div>
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-white dark:text-gray-300">Jenis
                                    Kendaraan</label>
                                <select name="merekmobil" id="merekmobil" x-model="merekmobil"
                                    class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                </select>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                            </div>
                        </div>

                        <div class="px-5 mb-5">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nama
                                Kendaraan</label>
                            <input type="text" id="nama_kendaraan" name="nama_kendaraan"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Nama Kendaraan" x-model="nama_kendaraan">
                        </div>

                        <div class="grid grid-cols-2">
                            <div class="flex mb-5">
                                <div class="px-5">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kapasitas
                                        Kursi</label>
                                    <input type="text" id="kapasitas_kursi" name="kapasitas_kursi"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Kapasitas Kursi" x-model="kapasitas_kursi">
                                </div>

                                <div class="pr-5">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kapasitas
                                        Koper</label>
                                    <input type="text" id="kapasitas_koper" name="kapasitas_koper"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Kapasitas Koper" x-model="kapasitas_koper">
                                </div>
                            </div>
                        </div>

                        {{-- Tag Lokasi --}}
                        <div class="block px-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 1<span
                                    class="text-red-500 font-bold">*</span></label>
                            <div class="mt-1 rounded-md shadow-sm">
                                {{-- class="provinsi_open_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F]
                                text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block
                                w-full rounded-md text-sm focus:ring-1" --}}
                                <select name="tag_location_1" id="tag_lokasi_1" x-model="tag_location_1"
                                    class="block w-96 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                    <option value='' selected>Pilih Lokasi</option>
                                    @foreach ($pulau as $pulau_data)
                                    <option value="{{ $pulau_data->id }}" {{isset($add_listing['tag_location_1']) ? ($add_listing['tag_location_1']==$pulau_data->id ?'selected':'') : ''}}>{{ $pulau_data->island_name }}</option>
                                    @endforeach
                                    {{-- <option value="{{ $provinsi_data['provinces_id'] }}"
                                        {{isset($add_listing['tag_location_1']) ?
                                        ($add_listing['tag_location_1']==$provinsi_data['provinces_id'] ?'selected':'')
                                        :''}}>{{ $provinsi_data['provinces_name'] }}</option>
                                    @foreach ($provinsi_data['regency'] as $key => $regency)
                                    <option value="{{ $regency['regencies_id'] }}"
                                        {{isset($add_listing['tag_location_1']) ?
                                        ($add_listing['tag_location_1']==$regency['regencies_id'] ?'selected':'') :''}}>
                                        {{ $regency['regencies_name'] }}</option>
                                    @endforeach
                                    @foreach ($provinsi_data['district'] as $key => $district)
                                    <option value="{{ $district['districts_id'] }}"
                                        {{isset($add_listing['tag_location_1']) ?
                                        ($add_listing['tag_location_1']==$district['districts_id'] ?'selected':'')
                                        :''}}>{{ $district['districts_name'] }}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>

                            <label for="text" class="block mb-2 mt-2 text-sm font-bold text-[#333333]">Tag Lokasi 2</label>
                            <div class="mt-1 rounded-md shadow-sm">
                                {{-- class="provinsi_open_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F]
                                text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block
                                w-full rounded-md text-sm focus:ring-1" --}}
                                <select name="tag_location_2" id="tag_lokasi_2" x-model="tag_location_2"
                                    class="block w-96 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                    <option value='' selected>Pilih Lokasi</option>
                                    @foreach ($provinsi as $provinsi_data)
                                    <option value="{{ $provinsi_data->id }}" {{isset($add_listing['tag_location_2']) ? ($add_listing['tag_location_2'] == $provinsi_data->id ? 'selected':''):''}}>{{ $provinsi_data->name }}</option>
                                    @endforeach
                                    {{-- <option value="{{ $provinsi_data['provinces_id'] }}"
                                        {{isset($add_listing['tag_location_2']) ?
                                        ($add_listing['tag_location_2']==$provinsi_data['provinces_id'] ?'selected':'')
                                        :''}}>{{ $provinsi_data['provinces_name'] }}</option>
                                    @foreach ($provinsi_data['regency'] as $key => $regency)
                                    <option value="{{ $regency['regencies_id'] }}"
                                        {{isset($add_listing['tag_location_2']) ?
                                        ($add_listing['tag_location_2']==$regency['regencies_id'] ?'selected':'') :''}}>
                                        {{ $regency['regencies_name'] }}</option>
                                    @endforeach
                                    @foreach ($provinsi_data['district'] as $key => $district)
                                    <option value="{{ $district['districts_id'] }}"
                                        {{isset($add_listing['tag_location_2']) ?
                                        ($add_listing['tag_location_2']==$district['districts_id'] ?'selected':'')
                                        :''}}>{{ $district['districts_name'] }}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>

                            <label for="text" class="block mb-2 mt-2 text-sm font-bold text-[#333333]">Tag Lokasi 3</label>
                            <div class="mt-1 rounded-md shadow-sm">
                                {{-- class="provinsi_open_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F]
                                text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block
                                w-full rounded-md text-sm focus:ring-1" --}}
                                <select name="tag_location_3" id="tag_lokasi_3" x-model="tag_location_3"
                                    class="block w-96 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                    <option value='' selected>Pilih Lokasi</option>
                                    @foreach ($kabupaten as $kabupaten_data)
                                    <option value="{{ $kabupaten_data->id }}" {{isset($add_listing['tag_location_3']) ? ($add_listing['tag_location_3']==$kabupaten_data->id ?'selected':'') :''}}>{{ $kabupaten_data->name }}</option>
                                    @endforeach
                                    {{-- <option value="{{ $provinsi_data['provinces_id'] }}"
                                        {{isset($add_listing['tag_location_3']) ?
                                        ($add_listing['tag_location_3']==$provinsi_data['provinces_id'] ?'selected':'')
                                        :''}}>{{ $provinsi_data['provinces_name'] }}</option>
                                    @foreach ($provinsi_data['regency'] as $key => $regency)
                                    <option value="{{ $regency['regencies_id'] }}"
                                        {{isset($add_listing['tag_location_3']) ?
                                        ($add_listing['tag_location_3']==$regency['regencies_id'] ?'selected':'') :''}}>
                                        {{ $regency['regencies_name'] }}</option>
                                    @endforeach
                                    @foreach ($provinsi_data['district'] as $key => $district)
                                    <option value="{{ $district['districts_id'] }}"
                                        {{isset($add_listing['tag_location_3']) ?
                                        ($add_listing['tag_location_3']==$district['districts_id'] ?'selected':'')
                                        :''}}>{{ $district['districts_name'] }}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>

                            <label for="text" class="block mb-2 mt-2 text-sm font-bold text-[#333333]">Tag Lokasi 4</label>
                            <div class="mt-1 rounded-md shadow-sm">
                                {{-- class="provinsi_open_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F]
                                text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block
                                w-full rounded-md text-sm focus:ring-1" --}}
                                <select name="tag_location_4" id="tag_lokasi_4" x-model="tag_location_4"
                                    class="block w-96 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                    <option value='' selected>Pilih Lokasi</option>
                                    @foreach ($objek as $objek_data)
                                    <option value="{{ $objek_data->id }}"{{isset($add_listing['tag_location_4']) ? ($add_listing['tag_location_4']==$objek_data->id ?'selected':'') : ''}}>{{ $objek_data->title }}</option>
                                    @endforeach
                                    {{-- <option value="{{ $provinsi_data['provinces_id'] }}"
                                        {{isset($add_listing['tag_location_4']) ?
                                        ($add_listing['tag_location_4']==$provinsi_data['provinces_id'] ?'selected':'')
                                        :''}}>{{ $provinsi_data['provinces_name'] }}</option>
                                    @foreach ($provinsi_data['regency'] as $key => $regency)
                                    <option value="{{ $regency['regencies_id'] }}"
                                        {{isset($add_listing['tag_location_4']) ?
                                        ($add_listing['tag_location_4']==$regency['regencies_id'] ?'selected':'') :''}}>
                                        {{ $regency['regencies_name'] }}</option>
                                    @endforeach
                                    @foreach ($provinsi_data['district'] as $key => $district)
                                    <option value="{{ $district['districts_id'] }}"
                                        {{isset($add_listing['tag_location_4']) ?
                                        ($add_listing['tag_location_4']==$district['districts_id'] ?'selected':'')
                                        :''}}>{{ $district['districts_name'] }}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>
                            {{-- x-init="$nextTick(() => { select2WithAlpine() })" --}}
                            <div x-data="dynamicTag()" class="mb-11" x-init="$nextTick(() => { select2WithAlpine() })">
                                @if (isset($add_listing['new_tag_location']))
                                    @php
                                        $new_tag_location = json_decode($add_listing['new_tag_location'], true)['results'];
                                        dump($new_tag_location);
                                    @endphp
                                    <div>
                                        <label for="text" class="block mb-2 mt-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis</label>
                                        @foreach ($new_tag_location as $key => $new_tag)
                                        <div class="mt-1 rounded-md shadow-sm">
                                            <select name="new_tag_location[]"  x-ref="select_new_tag"
                                                class="new_tag_location block w-96 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                                <option value='' selected>Pilih Lokasi</option>
                                                @foreach ($dynamic_tag as $key => $tag)
                                                    @foreach ($tag as $pulau)
                                                    <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}" {{$new_tag==$pulau['id_pulau'] ? 'selected':''}}>{{ $pulau['nama_pulau']}}</option>
                                                        @foreach ($pulau['provinsi'] as $provinsi)
                                                            <option value="{{ $provinsi['id_provinsi']}}" {{$new_tag==$provinsi['id_provinsi'] ? 'selected':''}}>{{ $provinsi['nama_provinsi']}}</option>
                                                            @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                <option value="{{ $kabupaten['id_kabupaten']}}" {{$new_tag==$kabupaten['id_kabupaten'] ? 'selected':''}}>{{ $kabupaten['nama_kabupaten']}}</option>
                                                                @if(isset($kabupaten['wisata']))
                                                                @foreach ($kabupaten['wisata'] as $wisata)
                                                                    <option value="{{ $wisata['id_wisata']}}" {{$new_tag==$wisata['id_wisata'] ? 'selected':''}}>{{ $wisata['nama_wisata']}}</option>
                                                                @endforeach
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            </select>
                                        </div>
                                        @endforeach
                                        <div
                                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                        </div>                                    
                                    </div>
                                @else
                                <template x-if="tag_list.length == 0">
                                    <div>
                                        <label for="text" class="block mb-2 mt-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis</label>
                                        <div class="mt-1 rounded-md shadow-sm">
                                            {{-- name="kamar" id="kamar" x-model="attribute_room" x-ref="select_room" --}}
                                            <select name="new_tag_location" x-model="new_tag_location" x-ref="select_new_tag"
                                                class="new_tag_location block w-96 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                                <option value='' selected>Pilih Lokasi</option>
                                                @foreach ($dynamic_tag as $key => $tag)
                                                    @foreach ($tag as $pulau)
                                                    <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}">{{ $pulau['nama_pulau']}}</option>
                                                        @foreach ($pulau['provinsi'] as $provinsi)
                                                            <option value="{{ $provinsi['id_provinsi']}}">{{ $provinsi['nama_provinsi']}}</option>
                                                            @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                <option value="{{ $kabupaten['id_kabupaten']}}">{{ $kabupaten['nama_kabupaten']}}</option>
                                                                @if(isset($kabupaten['wisata']))
                                                                @foreach ($kabupaten['wisata'] as $wisata)
                                                                    <option value="{{ $wisata['id_wisata']}}">{{ $wisata['nama_wisata']}}</option>
                                                                @endforeach
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            </select>
                                        </div>
                                        <div
                                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                        </div>                                    
                                    </div>
                                </template>    
                                @endif
                                <template x-if="tag_list.length >= 1">
                                    <template x-for="(tag, index) in tag_list">
                                        <div>
                                            @if (isset($add_listing['new_tag_location']))
                                            @else
                                                <label for="text" class="block mb-2 mt-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis</label>
                                            @endif
                                            <div class="mt-1 rounded-md shadow-sm flex relative">
                                            <select name="new_tag_location[]" x-ref="select_new_tag"
                                                class="block w-96 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                                <option value='' selected>Pilih Lokasi</option>
                                                @foreach ($dynamic_tag as $key => $tag)
                                                    @foreach ($tag as $pulau)
                                                    <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}">{{ $pulau['nama_pulau']}}</option>
                                                        @foreach ($pulau['provinsi'] as $provinsi)
                                                            <option value="{{ $provinsi['id_provinsi']}}">{{ $provinsi['nama_provinsi']}}</option>
                                                            @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                <option value="{{ $kabupaten['id_kabupaten']}}">{{ $kabupaten['nama_kabupaten']}}</option>
                                                                @if(isset($kabupaten['wisata']))
                                                                @foreach ($kabupaten['wisata'] as $wisata)
                                                                    <option value="{{ $wisata['id_wisata']}}">{{ $wisata['nama_wisata']}}</option>
                                                                @endforeach
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            </select>
                                                <div class="absolute right-[530px] top-1">
                                                    <button type="button" class="rounded-full">
                                                        <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt="" width="25px" @click="removeField(index)">
                                                    </button>
                                                </div>
                                            </div>
                                            <div
                                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                            </div>    
                                                                            
                                        </div>
                                    </template>
                                </template>
                                <div class="absolute py-2">
                                    <button type="button" class="flex bg-kamtuu-primary px-3 py-2 text-white text-sm rounded-lg mb-2" @click="add_list()">
                                        <img src="{{ asset('storage/icons/plus-solid.svg') }}" alt="" width="15px" style="color: #ed0202;">
                                        &nbsp;
                                        Tambah lokasi
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="px-5 mb-5" x-data="featuredImage()">
                            <p class="font-semibold text-[#BDBDBD]">Tambah Gambar<span
                                    class="text-red-500 font-bold">*</span></p>
                            <input class="py-2" type="file" id="gallery" name="edit[]" accept="image/*"
                                @change="selectedFile" multiple>
                            <template x-if="images.length < 1">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                            <template x-if="images.length >= 1">
                                <div class="flex">
                                    <template x-for="(image, index) in images" :key="index">
                                        <div class="flex justify-center items-center">
                                            <img :src="image" id="loadImg"
                                                class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                :alt="'upload' + index">
                                            <button type="button"
                                                    class="absolute mx-2 translate-x-12 -translate-y-14">
                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                            alt="" width="25px" @click="removeImage($event,index)">
                                            </button>
                                        </div>
                                        {{-- <template x-for="(img, key) in image" :key="key"> --}}
                                        {{-- </template> --}}
                                        {{-- <input type="hidden" name="" id="img" x-model="image"> --}}

                                    </template>
                                </div>
                            </template>
                        </div>

                        <div class="px-5 mb-5">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Harga Akomodasi
                                Driver Per Hari<span class="text-red-500 font-bold">*</span></label>
                            <input type="text" id="harga_akomodasi" x-model="harga_akomodasi" name="harga_akomodasi"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Rp. 50.000">
                        </div>

                        <div class="flex gap-5 px-5 mb-5">
                            <div class="flex items-center py-px">
                                <input type="radio" value="Luar Kota" x-model="status" id="status" name="status"
                                    class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="default-radio" class="ml-2 text-sm font-semibold text-[#333333]">Luar
                                    Kota</label>
                            </div>
                            <div class="flex items-center py-px">
                                <input id="status" name="status" type="radio" x-model="status" value="Dalam Kota"
                                    class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="checked-radio" class="ml-2 text-sm font-semibold text-[#333333]">Dalam
                                    Kota</label>
                            </div>
                        </div>

                        <div class="px-5 pt-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Deskripsi
                            </label>
                            <textarea class="deskripsi w-[10rem] click2edit" id="summernote" name="deskripsi"
                                x-model="deskripsi" x-init="$nextTick(() => { initSummerNote() })">
                                </textarea>
                        </div>

                        <div class="px-5 pt-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Komplemen</label>
                            <textarea class="catatan w-[10rem] click2edit" id="summernote" name="catatan"
                                x-model="catatan" x-init="$nextTick(() => { initSummerNote() })">
                                </textarea>
                        </div>

                        <div class="px-5 pt-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Pilihan
                                Tambahan</label>
                            <textarea class="pilihan-tambahan w-[10rem] click2edit" id="summernote"
                                name="pilihan_tambahan" x-model="pilihan_tambahan"
                                x-init="$nextTick(() => { initSummerNote() })">
                                </textarea>
                        </div>

                        <div class="px-5 pt-5">
                            <input id="checked-checkbox" type="checkbox" required
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                {{isset($add_listing) ? 'checked' :''}}>
                            <label for="checked-checkbox" class="ml-2 text-sm font-semibold text-[#333333]">Saya
                                setuju
                                dengan syarat dan ketentuan.</label>
                        </div>

                        <div class="p-5">
                            <div class="grid grid-cols-6">
                                <div class="col-end-8 col-end-2">
                                    <button type="submit"
                                        class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                        Selanjutnya
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        const initSummerNote = () => {
            $('.click2edit').summernote({
                placeholder: 'Hello stand alone ui',
                tabsize: 2,
                height: 120,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']]
                ],
                dialogsInBody: true
            });
        }

        function paketHandler() {

            return {
                merekmobil: "{!! isset($add_listing['id_merek_mobil']) ? $add_listing['id_merek_mobil'] : '' !!}",
                jenismobil: "{!! isset($add_listing['jenis_id']) ? $add_listing['jenis_id'] : '' !!}",
                tag_location_1: {!! isset($add_listing['tag_location_1']) ? $add_listing['tag_location_1'] : 'null' !!},
                tag_location_2: {!! isset($add_listing['tag_location_2']) ? $add_listing['tag_location_2'] : 'null' !!},
                tag_location_3: {!! isset($add_listing['tag_location_3']) ? $add_listing['tag_location_3'] : 'null' !!},
                tag_location_4: {!! isset($add_listing['tag_location_4']) ? $add_listing['tag_location_4'] : 'null' !!},
                deskripsi: "{!! isset($add_listing['deskripsi']) ? $add_listing['deskripsi'] : '' !!}",
                catatan: "{!! isset($add_listing['komplemen']) ? $add_listing['komplemen'] : '' !!}",
                pilihan_tambahan: "{!! isset($add_listing['pilihan_tambahan']) ? $add_listing['pilihan_tambahan'] : '' !!}",
                harga_akomodasi: "{!! isset($add_listing['harga_akomodasi']) ? $add_listing['harga_akomodasi'] : '' !!}",
                status: "{!! isset($add_listing['status']) ? $add_listing['status'] : '' !!}",
                nama_kendaraan: "{!! isset($add_listing['nama_kendaraan']) ? $add_listing['nama_kendaraan'] : '' !!}",
                kapasitas_kursi: "{!! isset($add_listing['kapasitas_kursi']) ? $add_listing['kapasitas_kursi'] : '' !!}",
                kapasitas_koper: "{!! isset($add_listing['kapasitas_koper']) ? $add_listing['kapasitas_koper'] : '' !!}",
            }
        }

        function dynamicTag(){
            return{
                tag_list:[],
                new_tag_location:'',
                id:'',
                add_list(){
                    this.tag_list.push({
                        id:'',
                        label:''
                    })

                    console.log(this.tag_list)
                },
                removeField(index){
                    this.tag_list.splice(index,1)
                },
                select2WithAlpine(){
                    this.select2 = $(this.$refs.select_new_tag).select2();
                    //this.select2 = $(this.$refs.select_new_tag).select2();
                    /*
                    this.select2.on("select2:select", (event) => {
                        this.id = event.target.value;
                    });
                    this.$watch("new_tag_location", (value) => {
                        this.select2.val(value).trigger("change");
                    });
                    */
                }
            }
        }


    </script>
    <script>
        $("#tag_lokasi_1").select2();
        $("#tag_lokasi_2").select2();
        $("#tag_lokasi_3").select2();
        $("#tag_lokasi_4").select2();
        //$(".new_tag_location").select2();

        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            
            // $(function() {
            //     // provinsi
            //     $('#provinsi').on('change', function() {
            //         let id_provinsi = $('#provinsi').val();
            //         $('#kabupaten').val('');
            //         $('#kecamatan').val('');
            //         $('#kelurahan').val('');
            //         var option = $('<option></option>').attr("value", "").text("");
            //         $("#kecamatan").empty().append(option);
            //         $("#kelurahan").empty().append(option);

            //         $.ajax({
            //             type: 'POST',
            //             url: "{{ route('getkabupaten') }}",
            //             data: {
            //                 id_provinsi: id_provinsi
            //             },
            //             cache: false,

            //             success: function(msg) {
            //                 $('#kabupaten').html(msg);
            //             },
            //             error: function(data) {
            //                 console.log('error:', data);
            //             },
            //         })
            //     })

            //     // kabupaten
            //     $('#kabupaten').on('change', function() {
            //         let id_kabupaten = $('#kabupaten').val();
            //         var option = $('<option></option>').attr("value", "").text("");
            //         $("#kelurahan").empty().append(option);

            //         $.ajax({
            //             type: 'POST',
            //             url: "{{ route('getkecamatan') }}",
            //             data: {
            //                 id_kabupaten: id_kabupaten
            //             },
            //             cache: false,

            //             success: function(msg) {
            //                 $('#kecamatan').html(msg);
            //             },
            //             error: function(data) {
            //                 console.log('error:', data);
            //             },
            //         })
            //     })

            //     // kecamatan
            //     $('#kecamatan').on('change', function() {
            //         let id_kecamatan = $('#kecamatan').val();

            //         $.ajax({
            //             type: 'POST',
            //             url: "{{ route('getdesa') }}",
            //             data: {
            //                 id_kecamatan: id_kecamatan
            //             },
            //             cache: false,

            //             success: function(msg) {
            //                 $('#kelurahan').html(msg);
            //             },
            //             error: function(data) {
            //                 console.log('error:', data);
            //             },
            //         })
            //     })
            // })

            $(function(){
                if($('#jenismobil').val()){
                    let id_jenis_mobil = $('#jenismobil').val();

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getmerekmobil') }}",
                        data: {
                            id_jenis_mobil: id_jenis_mobil
                        },
                        cache: false,

                        success: function(msg) {
                            console.log(msg);
                            $('#merekmobil').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })   
                }
            })   
            
            $(function() {
                $('#jenismobil').on('change', function() {
                    let id_jenis_mobil = $('#jenismobil').val();

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getmerekmobil') }}",
                        data: {
                            id_jenis_mobil: id_jenis_mobil
                        },
                        cache: false,

                        success: function(msg) {
                            $('#merekmobil').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })
            })
        });
    </script>
    <script>
        function featuredImage() {
            let data = [];
            const image = [];
            const dataImg = {!! isset($add_listing['gallery']) ? $add_listing['gallery'] : "[]"!!}

            for(let x=0; x < dataImg.length;x++){
                image.push(
                    '{!! url('/').'/' !!}'+dataImg[x].gallery
                )
            }
            console.log(image)
            return {
                images: image,
                data:[],
                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event, index) {
                    if (!event.target.files.length) return

                    let file = event.target.files
                    this.images = []
                    
                    // this.data = []

                    let reader = new FileReader();
                    let srcImg = ''

                    // reader.readAsDataURL(file);
                    // reader.onload = e => {
                    //     srcImg = e.target.result
                    //     this.images[index]=srcImg
                    // };

                    console.log(typeof file)

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                            
                        };

                        data.push({
                           file:file[i]
                        })
                        // this.data = [...this.data, file[i]]                       
                    }

                    console.log(data)
                    this.sendImage();

                },

                removeImage(event, index) {
                    this.images.splice(index, 1);
                    data.splice(index, 1);
                    // console.log(data);
                    this.sendImage()
                }
                ,
                sendImage(){
                    let fd=new FormData();

                    for(const file of data){
                        console.log(file)
                        if(typeof file==='object'){
                            fd.append('images_gallery[]',file.file);
                            fd.append('index_gallery[]',file.index)
                        }
                        if(typeof file==='string'){
                            fd.append('saved_gallery[]',file)
                        }
                    }

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        method:'POST',
                        url:"{{ $isedit == true ? route('rental.editGallery', $id) : route('rental.addGallery') }}",
                        data: fd,
                        processData: false,
                        contentType: false,
                        success:function(msg){
                            console.log(msg)
                        },
                        error:function(data){
                            console.log('error:',data)
                        }
                    });
                }
                // console.log()
            }
        }

        function alert(msg, e){
            Swal.fire({
                icon: 'error',
                text: msg,
                customClass: 'swal-height'
            })   

            e.preventDefault();
        }

        $("#formSubmitRental").on("submit",function(e){
            if(!$("#jenismobil").val()){
                alert('Jenis mobil harap dipilih!', e)
            }
            else if(!$("#merekmobil").val()){
                alert('Merek mobil harap dipilih!', e)
            }
            else if(!$("#nama_kendaraan").val()){
                alert('Nama Kendaraan harap diisi!', e)
            }
            else if(!$("#tag_lokasi_1").val()){
                alert('Lokasi 1 harap dipilih!', e)
            }

            // console.log($("#tag_lokasi_1").val());
            // e.preventDefault();
            else if(!$("#kapasitas_kursi").val()){
                alert('Jumlah kapasitas kursi harap diisi!', e)
            }
            else if(!$("#kapasitas_koper").val()){
                alert('Jumlah kapasitas koper harap diisi!', e)
            }
            else if(!$("#gallery").val()){
                var isedit ={{$isedit}};
                
                if(!isedit){
                    alert('Gambar galeri harap diisi!', e)
                }
            }
            else if(!$("#harga_akomodasi").val()){
                alert('Harga akomodasi harap diisi!', e)
            }
            else if($('input[name="status"]:checked').length==0){
                alert('Tombol status harus dipilih!', e);
            }
            else if(!$(".deskripsi").val()){
                alert('Deskripsi harus dipilih!', e);
            }
            else if(!$(".catatan").val()){
                alert('Komplemen harus dipilih!', e);
            }
            else if(!$(".pilihan-tambahan").val()){
                alert('Pilihan Tambahan harus dipilih!', e);
            }

        })

    </script>
    <script>
        @php
            $img =isset($add_listing['gallery']) ? $add_listing['gallery']:'[]';
        @endphp
        let gambar = '{!! url('/').'/'.$img !!}'
        // attr('src', '/images/sample.gif');
        $("#loadImg").attr(':src',gambar);
            // $("#jenismobil").val()
            // $("#merekmobil").val()
            // $("#nama_kendaraan").val()
            // $("#tag_lokasi_1").val()

            // $("#kapasitas_kursi").val()
            // $("#kapasitas_koper").val()
            // $("#gallery").val()
            // $("#harga_akomodasi").val()
            
            // // $('input[name="status"]:checked').length==0)

            // $(".deskripsi").val()
            // $(".catatan").val()
            // $(".pilihan-tambahan").val()
    </script>
    @livewireScripts
</body>

</html>