<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Text Editor -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">

    <!-- Scripts -->
    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <!-- summernote js -->
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script> 
    
    <!-- Select 2 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>

    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);
        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity:1;
            background-color: rgba(249,250,251,var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }
        #transfer:checked+#transfer {
            display: block;
        }
        #hotel:checked+#hotel {
            display: block;
        }
        #rental:checked+#rental {
            display: block;
        }
        #activity:checked+#activity {
            display: block;
        }
        #xstay:checked+#xstay {
            display: block;
        }
    </style>
    <!-- Styles -->
    @livewireStyles   
</head>
<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.seller.navbar-seller></x-be.seller.navbar-seller>   
    
    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko></x-be.kelolatoko.sidebar-toko>
        
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                
                <span class="text-2xl font-bold font-inter text-[#333333]">Dekorasi Toko</span>

                <div class="bg-[#FFFFFF] drop-shadow-xl p-10 mt-5 rounded">
                    
                @foreach($data as $mm)
                    <form action="{{ route('kelolatoko.editDekorasiToko',$id) }}" enctype="multipart/form-data" method="POST" x-data="paketHandler()">    
                        @csrf
                        @method('PUT')        
                        <div class="px-5 mb-5">
                            <input type="text" id="id" name="id" class=" bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 font-bold text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" value="{{ $mm->id }}">
                        </div>   

                        <div class="px-5 mb-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Nama Toko</label>
                            <input type="text" id="nama_toko" name="nama_toko" class=" bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 font-bold text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" value="{{ $mm->nama_toko }}">
                        </div>  

                        <div class="px-5 mb-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Lokasi Toko</label>
                            {{-- <input type="text" id="nama_toko" name="nama_toko" x-model="nama_toko" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" placeholder="Masukan Nama Toko"> --}}
                            <textarea type="text" id="lokasi_toko" name="lokasi_toko" class=" form-control w-full flex-auto block p-4 font-medium border border-[#4F4F4F] rounded-lg outline-none">{{$mm->lokasi_toko}}</textarea>
                        </div>

                        <div class="px-5 mb-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Info Promo</label>
                            <textarea type="text" id="summernotePromo" name="tentang_toko" class=" form-control w-full flex-auto block p-4 font-medium border border-[#4F4F4F] rounded-lg outline-none" rows="6 " maxlength="200">{{ $mm->tentang_toko }}</textarea>
                        </div> 
                        <div class="px-5 mb-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Deskripsi Toko</label>
                            <textarea type="text" id="nama_toko" name="deskripsi" class=" form-control w-full flex-auto block p-4 font-medium border border-[#4F4F4F] rounded-lg outline-none" rows="6">{{ $mm->deskripsi }}</textarea>
                        </div>
                        {{-- Branch store official --}}
                        <div class="px-5 mb-5">
                            <label class="relative inline-flex items-start cursor-pointer">
                                <input type="checkbox" value="official" name="official_store" class="peer sr-only" id="official-toggler" {{$mm->official_store == 'official' ? 'checked':''}}>
                                <div class="w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-3 peer-focus:ring-blue-300 rounded-full peer peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-blue-600"></div>
                                <span class="ml-3 text-m font-medium text-gray-900">Ingin membuat cabang?</span>
                            </label>
                        </div>
                        <div class="px-5 mb-5 hidden" id="form-branch-store" x-data="createBranchStore()">
                            <label class="block mb-2 text-sm font-bold text-[#333333]">Input Cabang Official</label>
                            <div class="grid grid-cols-2 mt-5 gap-3">
                                <div>
                                    <label class="font-bold text-sm mb-2 text-[#333333] block">Alamat</label>
                                    <textarea type="text" id="alamat_cabang" name="alamat_cabang" class=" form-control w-full flex-auto block p-4 font-medium border border-[#4F4F4F] rounded-lg outline-none" rows="7">{{ $mm->deskripsi }}</textarea>
                                </div>
                                <div class="grid grid-rows-4">
                                    <div>
                                        <label class="font-bold text-sm mb-2 text-[#333333] block">Provinsi</label>
                                        <select name="provinsi_id" id="provinsi_id" class="bg-[#ffffff] border border-[#4f4f4f] text-gray-900 font-bold rounded-lg focus:ring-blue-500 focus:border-blue-500 w-96 p-2.5">
                                            <option>Pilih Provinsi Cabang Anda</option>
                                            @foreach ($provinsi as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div>
                                        <label class="font-bold text-sm mb-2 text-[#333333] block">Kota/Kabupaten</label>
                                        <select name="kabupaten_id" id="kabupaten_id" class="bg-[#ffffff] border border-[#4f4f4f] text-gray-900 font-bold rounded-lg focus:ring-blue-500 focus:border-blue-500 w-96 p-2.5">
                                            <option>Pilih Kota/Kabupaten Cabang Anda</option>
                                        </select>
                                    </div>
                                    <div>
                                        <label class="font-bold text-sm mb-2 text-[#333333] block">Kecamatan</label>
                                        <select name="kecamatan_id" id="kecamatan_id" class="bg-[#ffffff] border border-[#4f4f4f] text-gray-900 font-bold rounded-lg focus:ring-blue-500 focus:border-blue-500 w-96 p-2.5">
                                            <option>Pilih Kecamatan Cabang Anda</option>
                                        </select>
                                    </div>
                                </div>
                                <div style="margin-top:-8px">
                                    <label class="font-bold text-sm mb-2 text-[#333333] block">Desa/Kelurahan</label>
                                    <select name="kelurahan_id" id="kelurahan_id" class="bg-[#ffffff] border border-[#4f4f4f] text-gray-900 font-bold rounded-lg focus:ring-blue-500 focus:border-blue-500 w-96 p-2.5">
                                        <option>Pilih Desa/Kelurahan Cabang Anda</option>
                                    </select>
                                </div>
                                <div>
                                    <button type="button" class="items-end justify-items-end px-5 py-2 bg-sky-500 rounded-md border border-sky-500 hover:bg-sky-300 duration-100" @click="insert()">Insert</button>
                                </div>
                            </div>
                            <div class="mt-5 block mb-2">
                                <table class="w-full text-sm text-[#333333]">
                                    <thead class="text-md text-white bg-[#D25889]">
                                        <th class="py-3 px-6">Alamat</th>
                                        <th class="py-3 px-6">Provinsi</th>
                                        <th class="py-3 px-6">Kabupaten</th>
                                        <th class="py-3 px-6">Kecamatan</th>
                                        <th class="py-3 px-6">Desa</th>
                                        <th class="py-3 px-6">Hapus</th>
                                    </thead>
                                    <tbody>
                                        <template x-if="branchs.length >= 1 ">
                                            <template x-for="(branch, index) in branchs" :key="index">
                                                <tr>
                                                    <input type="hidden" :value="branch.id" name="id[]"/>
                                                    <input type="hidden" :value="branch.alamat" name="address[]"/>
                                                    <input type="hidden" :value="branch.provinsi_id" name="province_id[]"/>
                                                    <input type="hidden" :value="branch.kabupaten_id" name="regency_id[]"/>
                                                    <input type="hidden" :value="branch.kecamatan_id" name="district_id[]"/>
                                                    <input type="hidden" :value="branch.kelurahan_id" name="village_id[]"/>
                                                    <td class="px-6 py-2" x-text="branch.alamat"></td>
                                                    <td class="px-6 py-2" x-text="branch.provinsi"></td>
                                                    <td class="px-6 py-2" x-text="branch.kabupaten"></td>
                                                    <td class="px-6 py-2" x-text="branch.kecamatan"></td>
                                                    <td class="px-6 py-2" x-text="branch.kelurahan"></td>
                                                    <td class="px-6 py-2">
                                                        <button class="bg-red-600 float-right mx-2 text-white px-7 py-2 hover:bg-red-400 rounded-lg" @click="remove(index)">Hapus</button>
                                                    </td>
                                                </tr>
                                            </template>
                                        </template>
                                        <template x-if="branchs.length < 0 ">
                                        <tr>
                                            <td class="px-6 py-2 text-center" colspan="6">Belum ada cabang</td>
                                        </tr>
                                        </template>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        {{-- Logo Toko --}}
                        <div class="px-5 mb-5" x-data="featuredImage()">
                            <p class="font-semibold text-[#333333]">Logo Toko</p>
                            <input class="py-2" type="file" name="logo_toko" accept="image/*"
                                @change="selectedFile" x-model="imageUrl">
                            <template x-if="imageUrl && !savedImage">
                                <div class="flex">
                                    <img :src="imageUrl"
                                        class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                        alt="upload-featured-photo">
                                    <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                        <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                            width="25px" @click="removeImage()">
                                    </button> 
                                </div>
                            </template>
                            <template x-if="!imageUrl && !savedImage">
                                <div class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                            <template x-if="savedImage">
                                @if (isset($mm->logo_toko))
                                <div class="flex">
                                    <img alt="Logo Toko" src="{{ asset($mm->logo_toko) }}" class="object-contain rounded-full h-32 w-32 bg-white p-1" />
                                </div>
                                @endif
                            </template>
                        </div>
                        
                        {{-- Banner Toko --}}
                        <div class="px-5 mb-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Nama Banner</label>
                            <input type="text" id="nama_banner" name="nama_banner" class=" bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 font-bold text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" value="{{ $mm->nama_banner }}">
                        </div>  

                        {{-- Banner Toko --}}
                        <div class="px-5 mb-5" x-data="bannerImage()">
                            <p class="font-semibold text-[#333333]">Banner Toko</p>
                            <input class="py-2" type="file" name="banner_toko" accept="image/*"
                                @change="selectedFile" x-model="bannerImg">
                            <template x-if="bannerImg && !savedImage">
                                <div class="flex">
                                    <img :src="bannerImg"
                                        class="object-contain rounded border border-gray-300 w-[1920px] h-[1080px] mr-5"
                                        alt="upload-featured-photo">
                                    {{-- <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                            width="25px" @click="removeImage()">
                                        </button> 
                                    --}}
                                </div>
                            </template>
                            <template x-if="!bannerImg && !savedImage">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                            <template x-if="savedImage">
                                @if (isset($mm->banner_toko))
                                <div class="flex">
                                    <img alt="Logo Toko" src="{!! isset($mm->banner_toko) ? asset($mm->banner_toko) : asset('storage/img/blank-logo.png') !!}" class="object-contain w-full bg-white p-1" />
                                </div>
                                @endif
                            </template>
                        </div>

                        <div class="px-5 mb-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Deskripsi Tambahan</label>
                            <textarea type="text" id="nama_toko" name="deskripsi_tambahan" class=" form-control w-full flex-auto block p-4 font-medium border border-[#4F4F4F] rounded-lg outline-none" rows="6 " maxlength="100">{{ $mm->deskripsi_tambahan }}</textarea>
                        </div>                       

                        <div class="p-5">
                            <div class="grid grid-cols-6">
                                <div class="col-end-8 col-end-2">
                                    <button type="submit" class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                        Simpan                
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                @endforeach
                </div>
            </div>
        </div>
    </div>    
    
    @livewireScripts
    <script>
        // document.getElementById    
        console.log($("#official-toggler").is(':checked'));
        if($("#official-toggler").is(':checked')){
            $("#form-branch-store").removeClass("hidden");
        }

        let clicked = $("#official-toggler");
        clicked.on("click",function(){
            if($(this).is(':checked')){
               $("#form-branch-store").removeClass("hidden")
            }

            if(!$(this).is(':checked')){
               $("#form-branch-store").addClass("hidden")  
            }
        })

        $("#provinsi_id").select2();        
        $("#provinsi_id").on("change",function(){
            let provinsi_id = $(this).val();

            $.ajax({
                type:'GET',
                url:"/admin/kamtuu/route/regencies/"+provinsi_id,
                cache:'false',
                success:function(resp){
                    //console.log(resp)
                    $("#kabupaten_id").html(resp)
                },
                error:function(data){
                    console.log('error:',data);
                }
            })
        })

        $("#kabupaten_id").select2();        
        $("#kabupaten_id").on("change",function(){
            let kabupaten_id = $(this).val();
            let url = "{{route('master.titik.district',':id')}}";
            url = url.replace(':id',kabupaten_id);

            $.ajax({
                type:'GET',
                url:`${url}`,
                cache:'false',
                success:function(resp){
                   // console.log(resp)
                    $("#kecamatan_id").html(resp)
                },
                error:function(data){
                    console.log('error:',data);
                }
            })
        });
        
        $("#kecamatan_id").select2();        
        $("#kecamatan_id").on("change",function(){
            let kecamatan_id = $(this).val();
            let url = "{{route('master.titik.village',':id')}}";
            url = url.replace(':id',kecamatan_id);

            $.ajax({
                type:'GET',
                url:`${url}`,
                cache:'false',
                success:function(resp){
                   // console.log(resp)
                    $("#kelurahan_id").html(resp)
                },
                error:function(data){
                    console.log('error:',data);
                }
            })
        });
        
        $("#kelurahan_id").select2();


        //console.log(clicked.is(':checked'))
        function createBranchStore(){
            console.log({!! $collect_branch !!});
            return{
                branchs:{!! isset($collect_branch) ? $collect_branch:'[]' !!},
                insert(){
                    let alamat = $("#alamat_cabang").val()

                    let provinsi_id = $("#provinsi_id").val();
                    let provinsi = $("#provinsi_id option:selected").text();
                    
                    let kabupaten_id = $("#kabupaten_id").val();
                    let kabupaten = $("#kabupaten_id option:selected").text();
                    
                    let kecamatan_id = $("#kecamatan_id").val();
                    let kecamatan = $("#kecamatan_id option:selected").text();
                    
                    let kelurahan_id = $("#kelurahan_id").val();
                    let kelurahan = $("#kelurahan_id option:selected").text();

                    this.branchs.push({
                        id,
                        alamat,
                        provinsi_id,
                        provinsi,
                        kabupaten_id,
                        kabupaten,
                        kecamatan_id,
                        kecamatan,
                        kelurahan_id,
                        kelurahan,
                    })

                    console.log(this.branchs)
                },
                remove(index){
                    this.branchs.splice(index,1)
                }
            }
        }

        $('#summernotePromo').summernote({
            placeholder: 'Masukkan promo yang ada di toko anda',
            tabsize: 2,
            height: 150,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                // ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        }); 
    </script>
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
    
        function paketHandler() {
            return {
                nama_toko: "{!! isset($toko['nama_toko']) ? $toko['nama_toko'] : "" !!}",
            }
        }
    </script>
    
    <script>
        function featuredImage() {

            return {
                imageUrl: '',
                savedImage: "{!! isset($mm->logo_toko) ? $mm->logo_toko : null !!}",

                selectedFile(event) {
                    this.fileToUrl(event, src => this.imageUrl = src)
                    this.savedImage = ''
                },

                fileToUrl(event, callback) {
                    if (! event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },

                removeImage() {
                    this.imageUrl = '';
                }
            }
        }
    </script>

    <script>
        function bannerImage() {

            return {
                bannerImg: '',
                savedImage: "{!! isset($mm->banner_toko) ? $mm->banner_toko : null !!}",

                selectedFile(event) {
                    this.fileToUrl(event, src => this.bannerImg = src)
                    this.savedImage = ''
                },

                fileToUrl(event, callback) {
                    if (! event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },

                removeImage() {
                    this.bannerImg = '';
                }
            }
        }
    </script>
</body>
</html>