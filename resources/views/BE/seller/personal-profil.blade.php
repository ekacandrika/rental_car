<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.seller.navbar-seller></x-be.seller.navbar-seller>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko></x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-2xl font-bold font-inter text-[#333333]">Profil</span>

                <div class="bg-white my-5 rounded-md shadow-lg">
                    <div class="ml-12 p-6">
                        <p class="text-lg font-bold">Kategori Seller</p>
                        <div class="grid grid-cols-2 w-80 justify-center">
                            <div class="flex items-center py-4">
                                <input id="country-option-1" type="radio" name="personal" value="personal"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                    checked="">
                                <label for="country-option-1"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Personal
                                </label>
                            </div>
                            <div class="flex items-center py-4">
                                <input id="country-option-2" type="radio" name="company" value="company"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                <label for="country-option-2"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Company
                                </label>
                            </div>
                        </div>
                        <button type="submit"
                            class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                            Ubah
                        </button>
                    </div>
                </div>

                {{-- Lisensi --}}
                <div class="bg-white my-5 rounded-md shadow-lg">
                    <div class="ml-12 p-6">
                        <p class="text-lg font-bold">Lisensi Personal</p>
                        <div class="grid grid-rows-6 w-80 py-4">
                            <div class="flex items-center py-px">
                                <input id="default-checkbox" type="checkbox" value=""
                                    class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="default-checkbox" class="ml-2 text-sm font-semibold text-[#333333]">Personal
                                    Transfer Seller</label>
                            </div>
                            <div class="flex items-center py-px">
                                <input id="checked-checkbox" type="checkbox" value=""
                                    class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="checked-checkbox" class="ml-2 text-sm font-semibold text-[#333333]">Personal
                                    Vehicle Rental Seller</label>
                            </div>
                            <div class="flex items-center py-px">
                                <input id="checked-checkbox" type="checkbox" value=""
                                    class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="checked-checkbox"
                                    class="ml-2 text-sm font-semibold text-[#333333] w-96">Personal Bussiness Transfer &
                                    Vechile Rental Seller</label>
                                <img class="mx-1" src="{{ asset('storage/icons/circle-info-black.svg') }}" alt="info"
                                    width="18px" height="18px">
                            </div>
                            <div class="flex items-center py-px">
                                <input id="checked-checkbox" type="checkbox" value=""
                                    class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="checked-checkbox" class="ml-2 text-sm font-semibold text-[#333333]">Personal
                                    Activity Seller</label>
                            </div>
                            <div class="flex items-center py-px">
                                <input id="checked-checkbox" type="checkbox" value=""
                                    class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="checked-checkbox" class="ml-2 text-sm font-semibold text-[#333333]">Personal
                                    XStay Seller</label>
                            </div>
                            <div class="flex items-center py-px">
                                <input id="checked-checkbox" type="checkbox" value=""
                                    class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="checked-checkbox"
                                    class="ml-2 text-sm font-semibold text-[#333333] w-96">Personal Bussiness Activity &
                                    XStay Seller</label>
                                <img class="mx-1" src="{{ asset('storage/icons/circle-info-black.svg') }}" alt="info"
                                    width="18px" height="18px">
                            </div>
                        </div>
                        <button type="submit"
                            class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                            Ubah
                        </button>
                    </div>
                </div>

                {{-- Data Diri --}}
                <div class="bg-white my-5 rounded-md shadow-lg">
                    <div class="ml-12 p-6">
                        <p class="text-lg font-bold text-[#333333]">Data Diri</p>
                        <div class="py-5">
                            <div class="w-auto flex">
                                <img src="{{ asset('storage/img/logo-traveller.png') }}" alt="user-profile"
                                    class="object-cover object-center w-14 h-14">
                                <div class="grid grid-cols-1">
                                    <p class="ml-4 pl-3 font-inter text-align w-full font-medium text-base">Foto Profil
                                    </p>
                                    <input class="btn-danger" type="file" id="upload" hidden />
                                    <div for="upload"
                                        class="ml-4 flex block w-full h-5 pt-1 text-[#23AEC1] text-xs bg-violet-50 hover:bg-violet-100 justify-center rounded-full">
                                        <img src="{{ asset('storage/icons/ic-upload-blue.svg') }}" alt="user-profile"
                                            class="w-3 h-3 mr-1">Pilih File
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="text-sm font-bold">Status: <b class="text-green-500">Terverifikasi</b></p>
                        <div class="grid grid-cols-2">
                            <div class="my-2">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nama
                                    Depan</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Nama Depan">
                            </div>
                            <div class="my-2">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nama
                                    Belakang</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Nama Belakang">
                            </div>
                            <div class="my-2">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Jenis
                                    Kelamin</label>
                                <select
                                    class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                    <option>Laki-Laki</option>
                                    <option>Perempuan</option>
                                </select>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                            </div>
                            <div class="my-2">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Tanggal
                                    Lahir</label>
                                <input type="date"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Tur Sehat">
                            </div>
                        </div>

                        <button type="submit"
                            class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                            Perbarui
                        </button>
                    </div>
                </div>

                {{-- Alamat --}}
                <div class="bg-white my-5 rounded-md shadow-lg">
                    <div class="ml-12 p-6">
                        <p class="text-lg font-bold text-[#333333]">Alamat</p>
                        <p class="text-sm font-bold">Status: <b class="text-red-500">Belum Terverifikasi</b></p>
                        <div class="grid grid-cols-2">
                            <div class="my-2">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Alamat</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Alamat">
                            </div>
                            <div class="my-2">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Provinsi</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Provinsi">
                            </div>
                            <div class="my-2">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kota/Kabupaten</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Kota/Kabupaten">
                            </div>
                            <div class="my-2">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kecamatan</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Kecamatan">
                            </div>
                            <div class="my-2">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kelurahan</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Kelurahan">
                            </div>
                            <div class="my-2">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kode
                                    Pos</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Kode Pos">
                            </div>
                        </div>

                        <button type="submit"
                            class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                            Perbarui
                        </button>
                    </div>
                </div>

                {{-- Identitas --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                    <div class="ml-12 px-6 pt-6">
                        <div class="flex grid grid-cols-2 justify-center items-center">
                            <div class="py-2.5">
                                <p class="text-lg font-bold font-inter text-[#333333]">Identitas</p>
                            </div>
                        </div>
                        <p class="text-sm font-bold">Status: <b class="text-red-500">Belum Verifikasi</b></p>
                    </div>
                    <div class="grid grid-cols-2">
                        <div class="ml-12 p-6">
                            <div class="my-2">
                                <label for="text"
                                    class="block mb-2 text-md font-bold text-[#333333] dark:text-gray-300">Kartu
                                    Identitas</label>
                                <select
                                    class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                    <option>KTP</option>
                                    <option>KITAS</option>
                                </select>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                            </div>
                            <div class="my-2">
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-400 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="1234567895421">
                            </div>
                            <p class="text-sm text-[#333333] font-reguler">Pastikan unggahan Anda jelas untuk memudahkan
                                proses verifikasi kami. Ketidakjelasan akan menyebabkan proses verifikasi lebih lama dan
                                dapat menyebabkan tertundanya pengaktifan akun Anda.</p>
                        </div>
                        <div class="mr-12 p-6">
                            <div x-data="displayImage()">
                                <div class="mb-2">
                                    <template x-if="imageUrl">
                                        <img :src="imageUrl"
                                            class="object-contain rounded border border-gray-300 w-full h-56">
                                    </template>

                                    <template x-if="!imageUrl">
                                        <div
                                            class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-full h-56">
                                            <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                width="20px" height="20px">
                                        </div>
                                    </template>

                                    <input class="mt-2" type="file" accept="image/*" @change="selectedFile">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Email --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                    <div class="ml-12 p-6">
                        <div class="flex grid grid-cols-2 justify-center items-center">
                            <div class="py-2.5">
                                <p class="text-lg font-bold font-inter text-[#333333]">Email</p>
                            </div>
                            <div
                                class="col-start-5 col-end-7 px-6 py-2 rounded-lg bg-[#23AEC1] items-center justify-center mt-5 mx-5">
                                <a href="#" class="text-[#FFFFFF] text-sm">Tambah Email</a>
                            </div>
                        </div>
                        <p class="text-sm font-bold">Status: <b class="text-red-500">Belum Verifikasi</b></p>
                        <div class="font-inter font-semibold text-sm pt-2">Kamu boleh memasukkan maksimal 2 email</div>
                        <div class="flex grid grid-cols-4">
                            <div class="font-inter">1. irfanfuno@gmail.com</div>
                            <div class="col-start-2 col-span-2">
                                <div class="font-inter text-[#D50006] grid grid-cols-2 gap-5">
                                    <a href="#" class="text-xs hover:text-[#D50006] hover:font-bold">Email ini yang akan
                                        digunakan untuk notifikasi</a>
                                    <img src="{{ asset('storage/icons/circle-minus-red.svg') }}"
                                        class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Nomor Telepon --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                    <div class="ml-12 p-6">
                        <div class="flex grid grid-cols-2 justify-center items-center">
                            <div class="py-2.5">
                                <p class="text-lg font-bold font-inter text-[#333333]">Hp atau WhatsApp</p>
                            </div>
                            <div
                                class="col-start-5 col-end-7 px-6 py-2 rounded-lg bg-[#23AEC1] items-center justify-center mt-5 mx-5">
                                <a href="#" class="text-[#FFFFFF] text-sm">Tambah Nomor</a>
                            </div>
                        </div>
                        <p class="text-sm font-bold">Status: <b class="text-red-500">Belum Verifikasi</b></p>
                        <div class="font-inter font-semibold text-sm pt-2">Kamu boleh memasukkan maksimal 2 nomor
                            telepon</div>
                        <div class="flex grid grid-cols-4">
                            <div class="font-inter">1. +6281234567890</div>
                            <div class="col-start-2 col-span-2">
                                <div class="font-inter text-[#D50006] grid grid-cols-2 gap-5">
                                    <a href="#" class="text-xs hover:text-[#D50006] hover:font-bold">Nomor HP ini yang
                                        akan digunakan untuk notifikasi</a>
                                    <img src="{{ asset('storage/icons/circle-minus-red.svg') }}"
                                        class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                if (! event.target.files.length) return

                let file = event.target.files[0],
                    reader = new FileReader()

                reader.readAsDataURL(file)
                reader.onload = e => callback(e.target.result)
                },
            }
        }
    </script>
</body>

</html>