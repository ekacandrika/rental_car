<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);
        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity:1;
            background-color: rgba(249,250,251,var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }
        #transfer:checked+#transfer {
            display: block;
        }
        #hotel:checked+#hotel {
            display: block;
        }
        #rental:checked+#rental {
            display: block;
        }
        #activity:checked+#activity {
            display: block;
        }
        #xstay:checked+#xstay {
            display: block;
        }
    </style>
    <!-- Styles -->
    @livewireStyles   
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    {{-- @dump($add_listing) --}}
    <x-be.com.navbar></x-be.com.navbar>  
    
    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko></x-be.kelolatoko.sidebar-toko>
        
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <div class="flex gap-2">                    
                    <button type="button" class="text-[#333333] font-medium rounded-lg text-sm py-2 text-center inline-flex items-center mr-2 hover:text-[#23AEC1]">   
                        <img src="{{ asset('storage/icons/chevron.svg') }}" alt="user-profile" class="w-5 h-5 mr-1">
                        <span class="text-lg font-bold font-inter text-[#333333]">Tambah Listing</span>
                    </button>
                </div>
                {{-- Breadcumbs --}}
                <nav class="flex mx-2.5" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            <a href="{{route('transfer.viewAddListingEdit',$id)}}" class="inline-flex items-center text-sm font-bold font-inter text-[#23AEC1]">Detail Kendaraan</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path></svg>
                                <a href="{{route('transfer.viewRute.edit',$id)}}" class="inline-flex items-center text-sm font-bold font-inter text-[#333333] hover:text-[#23AEC1]">Rute Harga dan Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path></svg>
                                <a href="{{route('transfer.viewBatasEdit',$id)}}" class="inline-flex items-center text-sm font-bold font-inter text-[#333333] hover:text-[#23AEC1]">Batas Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path></svg>
                                <a href="{{route('transfer.viewPilihanEdit',$id)}}" class="inline-flex items-center text-sm font-bold font-inter text-[#333333] hover:text-[#23AEC1]">Pilihan dan Ekstra</a>
                            </div>
                        </li>
                    </ol>
                </nav>

                <div class="mt-5">
                    <form action="{{ $isedit==true ? route('transfer.addListingPrivate.edit', $id) : route('transfer.addListingPrivate') }}" method="POST" enctype="multipart/form-data" x-data="paketHandler()">
                        @csrf
                        @method('PUT')
                    
                        <div class="bg-[#FFFFFF] drop-shadow-xl p-6 px-10 rounded">
                            <div class="grid grid-cols-2">
                                <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Detail Kendaraan</div>
                                <div class="text-sm font-bold font-inter p-5 text-[#333333] text-right">ID Listing: {{ $listing }}</div>
                            </div>                            
                                <div class="px-5 mb-5">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Judul Listing</label>
                                    <input type="text" id="product_name" name="product_name" 
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
                                    placeholder="Mobil A" x-model="product_name">
                                </div>
                                
                                <div class="block px-5 mb-5 flex gap-5">
                                    <div>
                                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Jenis Kendaraan</label>
                                        <select name="jenismobil" id="jenismobil" x-model="jenismobil" x-model="jenismobil"
                                            class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option selected disabled>Pilih Kendaraan</option>
                                            @foreach ($jenismobil as $jm)
                                            <option value="{{ $jm->id }}">{{ $jm->jenis_kendaraan }}
                                            </option>
                                            @endforeach
                                        </select>
                                        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                        </div>
                                    </div>
                                    <div>
                                        <label for="text" class="block mb-2 text-sm font-bold text-white dark:text-gray-300">Jenis Kendaraan</label>
                                        <select name="merekmobil" id="merekmobil" x-model="merekmobil" class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        </select>
                                        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="block px-5 mb-5 flex gap-5">
                                    <div>
                                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nama Kendaraan</label>
                                        <input type="text" id="nama_kendaraan" name="nama_kendaraan" x-model="nama_kendaraan" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Nama Kendaraan">
                                    </div>

                                    <div>
                                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Tipe</label>
                                        <input type="text" id="status" name="status" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 placeholder-[#333333]" placeholder="Transfer Private" readonly>
                                    </div>
                                </div>

                                <div class="grid grid-cols-2">
                                    <div class="flex mb-5">
                                        <div class="px-5">
                                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kapasitas Kursi</label>
                                            <input type="text" id="kapasitas_kursi" name="kapasitas_kursi" x-model="kapasitas_kursi" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Kapasitas Kursi">
                                        </div>

                                        <div class="pr-5">
                                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kapasitas Koper</label>
                                            <input type="text" id="kapasitas_koper" name="kapasitas_koper" x-model="kapasitas_koper" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Kapasitas Koper">
                                        </div>
                                    </div>
                                </div>

                                <div class="px-5 mb-5" x-data="featuredImage()">
                                    <p class="font-semibold text-[#BDBDBD]">Tambah Gambar</p>
                                    <input class="py-2" type="file" id="gallery" name="gallery[]" accept="image/*" @change="selectedFile" multiple>
                                    <template x-if="images.length < 1">
                                        <div class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                            <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px" height="20px">
                                        </div>
                                    </template>
                                    <template x-if="images.length >= 1">
                                        <div class="flex">
                                            <template x-for="(image, index) in images" :key="index">
                                                <div class="flex justify-center items-center">
                                                    <img :src="image"
                                                        class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                        :alt="'upload'+index">
                                                        <button type="button"
                                                        class="absolute mx-2 translate-x-12 -translate-y-14">
                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                                alt="" width="25px" @click="removeImage($event,index)">
                                                </button> 
                                                </div>
                                            </template>
                                        </div>
                                    </template>
                                </div>
            
                                <div class="px-5" wire:ignore>
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333] click2edit">Deskripsi</label>
                                    <textarea class="form-control w-[10rem] deskripsi" name="deskripsi" x-model="deskripsi"  id="deskripsi"></textarea>
                                </div>

                                <div class="px-5 pt-5" wire:ignore>
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333] click2edit">Komplemen</label>
                                    <textarea class="form-control w-[10rem] catatan" name="catatan" x-model="catatan"  id="catatan"></textarea>
                                </div>

                                <div class="p-5">
                                    <div class="grid grid-cols-6">
                                        <div class="col-end-8 col-end-2">
                                            <button type="submit" class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                                Selanjutnya                
                                            </button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>   
    
    @livewireScripts

    <script>
        $(".deskripsi").val("{!! isset($add_listing['deskripsi']) ? $add_listing['deskripsi'] : "" !!}")
        $('.deskripsi').summernote({ 
            placeholder: 'Deskripsi...',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['para', ['ul', 'ol']]
            ]
        });

        $(".catatan").val("{!! isset($add_listing['catatan']) ? $add_listing['catatan'] : "" !!}")

        $('.catatan').summernote({
            placeholder: 'Komplemen...',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        }); 
       

        /* 
        $('.tambahan').summernote({
            placeholder: 'Pilihan Tambahan...',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        }); 
        */

        // const initSummerNote = () => {
        //     $('.catatan').summernote({
        //         placeholder: 'Hello stand alone ui',
        //         tabsize: 2,
        //         height: 120,
        //         toolbar: [
        //             ['style', ['style']],
        //             ['font', ['bold', 'underline', 'clear']],
        //             ['color', ['color']],
        //             ['para', ['ul', 'ol', 'paragraph']],
        //             ['table', ['table']],
        //             ['insert', ['link', 'picture', 'video']],
        //             ['view', ['fullscreen', 'codeview', 'help']]
        //         ],
        //         dialogsInBody: true
        //     });

        //     $('.deskripsi').summernote({
        //         placeholder: 'Hello stand alone ui',
        //         tabsize: 2,
        //         height: 120,
        //         toolbar: [
        //             ['style', ['style']],
        //             ['font', ['bold', 'underline', 'clear']],
        //             ['color', ['color']],
        //             ['para', ['ul', 'ol', 'paragraph']],
        //             ['table', ['table']],
        //             ['insert', ['link', 'picture', 'video']],
        //             ['view', ['fullscreen', 'codeview', 'help']]
        //         ],
        //         dialogsInBody: true
        //     });

        // }

        function paketHandler() {
                    
            return {
                jenismobil: "{!! isset($add_listing['id_jenis_mobil']) ? $add_listing['id_jenis_mobil'] : "" !!}",
                product_name: "{!! isset($add_listing['product_name']) ? $add_listing['product_name'] : "" !!}",
                nama_kendaraan: "{!! isset($kendaraan['nama_kendaraan']) ? $kendaraan['nama_kendaraan'] : "" !!}",
                kapasitas_kursi: "{!! isset($kendaraan['kapasitas_kursi']) ? $kendaraan['kapasitas_kursi'] : "" !!}",
                kapasitas_koper: "{!! isset($kendaraan['kapasitas_koper']) ? $kendaraan['kapasitas_koper'] : "" !!}",
                deskripsi: "{!! isset($add_listing['deskripsi']) ? $add_listing['deskripsi'] : "" !!}",
                catatan: "{!! isset($add_listing['catatan']) ? $add_listing['catatan'] : "" !!}",
                pilihan_tambahan: "{!! isset($kendaraan['pilihan_tambahan']) ? $kendaraan['pilihan_tambahan'] : "" !!}",
                gallery: "{!! isset($kendaraan['gallery']) ? $kendaraan['gallery'] : "" !!}",
                jenisbus: "{!! isset($add_listing['id_jenis_bus']) ? $add_listing['id_jenis_bus'] : "" !!}",
            }
        }
    </script>
    <script>        
        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $(function(){
                if({!! $isedit !!}){
                    let id_jenis_mobil = $('#jenismobil').val();
                    if(id_jenis_mobil!=" "){
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('getmerekmobil') }}",
                            data: {
                                id_jenis_mobil: id_jenis_mobil
                            },
                            cache: false,

                            success: function(msg){
                                $('#merekmobil').html(msg);
                            },
                            error: function(data){
                                console.log('error:', data);
                            },
                        })
                    }
                }
            })
            $(function() {
                $('#jenismobil').on('change', function() {
                    let id_jenis_mobil = $('#jenismobil').val();

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getmerekmobil') }}",
                        data: {
                            id_jenis_mobil: id_jenis_mobil
                        },
                        cache: false,

                        success: function(msg){
                            $('#merekmobil').html(msg);
                        },
                        error: function(data){
                            console.log('error:', data);
                        },
                    })
                })
            })
            $(function() {
                $('#jenisbus').on('change', function() {
                    let id_jenis_bus = $('#jenisbus').val();

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getmerekbus') }}",
                        data: {
                            id_jenis_bus: id_jenis_bus
                        },
                        cache: false,

                        success: function(msg){
                            $('#merekbus').html(msg);
                        },
                        error: function(data){
                            console.log('error:', data);
                        },
                    })
                })
            })
        });
    </script>
    <script>
        function featuredImage() {
            const imgUmum=[];
            const data={!!$add_listing["gallery"]!!}
            // console.log(data)
            for(let x = 0;x < data.length;x++){
                console.log(data[x].gallery)
                let gallery ='{!! url('/').'/' !!}' + data[x].gallery
                console.log(gallery)

                imgUmum.push(gallery)
            }
            return {
                images: imgUmum,

                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event) {
                    if (! event.target.files.length) return

                    let file = event.target.files
                    this.images = []
                    // this.data = []

                    console.log(typeof file)

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''
                        
                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                        };   

                        // this.data = [...this.data, file[i]]
                    }

                },

                removeImage(index) {
                    this.images.splice(index, 1);
                }
            }
        }
    </script>
    <script>
        function featuredImageUmum() {
            const imgUmum=[];
            const data={!!$add_listing["gallery"]!!}
            // console.log(data)
            for(let x = 0;x < data.length;x++){
                console.log(data[x].gallery)
                let gallery ='{!! url('/').'/' !!}' + data[x].gallery
                console.log(gallery)

                imgUmum.push(gallery)
            }
            return {
                images: imgUmum,

                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event) {
                    if (! event.target.files.length) return

                    let file = event.target.files
                    this.images = []
                    // this.data = []

                    console.log(typeof file)

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''
                        
                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                        };   

                        // this.data = [...this.data, file[i]]
                    }

                },

                removeImage(index) {
                    this.images.splice(index, 1);
                }
            }
        }
    </script>
</body>
</html>
