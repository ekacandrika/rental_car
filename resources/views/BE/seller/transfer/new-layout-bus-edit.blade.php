<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }
        .disabled {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
        }
    </style>
    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                    {{ route('hotelfaq') }}
                @else
                    {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                    {{ route('faq.index') }}
                @else
                    {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <div class="flex gap-2 mb-5">
                    <button type="button"
                        class="text-[#333333] font-medium rounded-lg text-sm py-2 text-center inline-flex items-center mr-2 hover:text-[#23AEC1]">
                        <img src="{{ asset('storage/icons/chevron.svg') }}" alt="user-profile" class="w-5 h-5 mr-1">
                        <span class="text-lg font-bold font-inter text-[#333333]">Buat Layout Bus</span>
                    </button>
                </div>

                <div>
                <form action="{{ route('layout.editNewLayoutBus',$id) }}" method="POST"
                    enctype="multipart/form-data" x-data="loopBusChair()">
                    @csrf
                    @method('PUT')    
                    <div class="bg-[#FFFFFF] drop-shadow-xl p-6 px-10 rounded">
                        <div class="text-sm font-bold font-inter mb-5 text-[#333333]">
                            <h1 class="text-lg">Tambah Layout Kursi</h1>
                        </div>
                        <div class="flex gap-2">
                            <div class="block mb-5">
                                <label for="kolom_kursi"
                                    class="block text-sm font-bold text-[#333333] dark:text-gray-300">Jumlah Kolom Kursi</label>

                                <div class="grid grid-cols-1">
                                    <div class="w-full pt-5">
                                        <input type="number" nim="1" name="kolom_kursi" id="" class="bg-[#FFFFFF] border broder-[#4F4f4f] text-dark-400 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-45 p-2.5" value="{{$layout_bus->kolom_kursi}}">
                                    </div>
                                </div>
                            </div>
                            <div class="block mb-5">
                                <label for="baris_kursi" 
                                    class="block text-sm font-bold text-[#333333] dark:text-gray-300">Jumlah Baris Kursi</label>

                                <div class="grid grid-cols-2">
                                    <div class="w-full pt-5">
                                        <input type="number" nim="1" name="baris_kursi" id="" class="bg-[#FFFFFF] border broder-[#4F4f4f] text-dark-400 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-45 p-2.5" value="{{$layout_bus->baris_kursi}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex gap-2">
                            <div class="block mb-5">
                                <label for="text"
                                    class="block text-sm font-bold text-[#333333] dark:text-gray-300">Judul layout</label>

                                <div class="grid grid-cols-1">
                                    <div class="w-full pt-5">
                                        <input type="text" name="judul_layout" id="" class="bg-[#FFFFFF] border broder-[#4F4f4f] text-dark-400 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-45 p-2.5" value="{{$layout_bus->judul_layout}}">
                                    </div>
                                </div>
                            </div>
                            <div class="block mb-5">
                                <label for="text"
                                    class="block text-sm font-bold text-[#333333] dark:text-gray-300">Harga kursi</label>
                                <div class="grid grid-cols-2">
                                    <div class="w-full pt-5">
                                        <input type="text" name="harga_kursi" id="harga_kursi" class="bg-[#FFFFFF] border broder-[#4F4f4f] text-dark-400 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-45 p-2.5" value="{{$layout_bus->harga_kursi}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div>
                            <div class="col-end-2 col-end-2">
                                <button type="button" id="create_layout" @click="createChair"
                                    class="w-32 items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]" x-modal='harga_kursi'>
                                    Buat layout
                                </button>
                            </div>
                            <div class="flex mt-3">
                                <template x-if="error_num_col === true">
                                    <div x-init="setTimeout(()=> error_num_col = false, 3000)" class="bg-red-300 rounded-md p-3 my-2 w-fit">
                                        <p x-text="error_col_message"></p>
                                    </div>
                                </template>
                            </div>
                            <div class="flex mt-3">
                                <template x-if="error_num_row === true">
                                    <div x-init="setTimeout(()=> error_num_row = false, 3000)" class="bg-red-300 rounded-md p-3 my-2 w-fit">
                                        <p x-text="error_row_message"></p>
                                    </div>
                                </template>
                            </div>
                            <template x-if="layout_chair.length >= 1">
                                <div class="mt-2 mb-5" id="bus_layout">
                                    <div class="border-2 border-[#9E3D64] rounded-md w-96">
                                        <div class="p-5">
                                            <div class="rounded border border-gray-400 p-2.5 bg-[#9E3D64] mb-2">
                                                <p class="font-bold text-center text-white mb-2">Bagian Depan</p>
                                                {{-- name="layout_kursi" --}}
                                                <input type="hidden" x-model="jumlah_kursi" name="jumlah_kursi">
                                                {{-- <input type="hidden" x-model="judul_layout" name="layout_kursi"> --}}
                                                <p id="jumlah_kursi"
                                                    class="text-center w-full h-14 px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                    <span>Jumlah Kursi:&nbsp;</span><span x-text="jumlah_kursi"></span>
                                                </p>
                                                <p id="layout_kursi" x-text="judul_layout"
                                                    class="text-center w-full h-14 px-2.5 text-[#ffffff] bg-[#9E3D64]"></p>
                                            </div>
                                            {{-- <divlayout_kursi :class="'grid grid-cols-'+layout_chair.length+' gap-2'">
                                                <template x-for="(layout, i) in layout_chair" :key="i">
                                                    <div :class="'gap-2 grid grid-rows-'+layout.length">
                                                        <template x-for="(chair, x) in layout" :key="x">
                                                            <div class="" :data-key="x">
                                                                <input :name="'kursi['+i+'][]'" :id="'kursi['+i+']['+x+']'"
                                                                    class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                                <input :name="'harga['+i+'][]'" :id="'harga['+i+']['+x+']'"
                                                                    class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                    placeholder="Harga">
                                                                <input type="checkbox" class="text-center" :name="'disable['+i+'][]'" :id="'disable['+i+']['+x+']'" @click="disableChair(i, x)">
                                                            </div>
                                                        </template>
                                                    </div>
                                                </template>
                                            </divlayout_kursi
                                             --}}
                                             {{-- 08586170233 --}}
                                            <div :class="'grid grid-cols-4 grid-flow-row gap-2'" id="bus_collumn" x-data="classRemove('bus_collumn')">
                                                <template x-for="(layout, i) in layout_chair" :key="i">  
                                                        <div class="" :data-key="i">
                                                            <input :name="'layout['+i+'][kursi]'" :id="'kursi['+i+']'" x-model="layout.chair_number"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input :name="'layout['+i+'][harga]'" :id="'harga['+i+']'"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="harga" x-model="layout.price_chair">
                                                            <input type="checkbox" class="text-center" :name="'layout['+i+'][avaiblity]'" :id="'disable['+i+']'" @click="disableChair(i)" x-model="layout.disable_chair" x-data="availablity('['+i+']')">
                                                        </div>
                                                </template>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-end-2 col-end-2 mt-10">
                                        <button type="sumbit" id="save_layout"
                                            class="w-32 items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                            Simpan
                                        </button>
                                    </div>
                                </div>
                            </template>
                        </div>
                    </div>
                </form>    
                </div>
            </div>
        </div>
    </div>

    @livewireScripts

    <script>

        // $("#create_layout").on("click",function(e){
        //     e.preventDefault();
        //     let collumn = $("input[name='kolom_kursi']").val();
        //     let row =$("input[name='baris_kursi']").val();

        //     console.log(collumn)
        //     console.log(row)

        //     $("#bus_layout").removeClass("hidden");
        //     $("#bus_layout").append(`<div class="grid grid-cols-${collumn} gap-2" id="chair"><div>`)
        //     for(let i= 0; i < collumn; i++){
        //         // (`<div class="gap-2 grid gird-rows-${row}"></div>`).appendTo($("#chair"))
        //         for(let x =1; x < row; x++){
                    
        //         }
                
        //     }

        //     // $("#bus_layout").append(`<div class="border-2 border-[#9E3D64] rounded-md w-96"><div class="p-5">
        //     //                             <div class="rounded border border-gray-400 p-2.5 bg-[#9E3D64]">
        //     //                                 <p class="font-bold text-center text-white">Bagian Depan</p>
        //     //                                 <input name="jumlah_kursi" id="jumlah_kursi" value="31" class=" numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
        //     //                                 <input name="layout_kursi" id="layout_kursi" value="Bus Medium 31 Kursi" class=" numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
        //     //                             </div>
        //     //                             <div class="grid grid-cols-${collumn} gap-2" id="chair">
        //     //                              ${loopBusChair(collumn, row)}
        //     //                             <div>
        //     //                         </div>`)
        // });

        function loopBusChair(){
            console.log({!! isset($layout) ? $layout : '[]' !!})    
            return{
                collumn:'{!! isset($layout_bus->jumlah_kolom) ? parseInt($layout_bus->kolom_kursi): null !!}',
                row:'{!! isset($layout_bus->jumlah_baris) ? parseInt($layout_bus->baris_kursi): null !!}',
                chair_number:'',
                disable_chair:false,
                price_chair:'',
                layout_chair:{!! isset($layout) ? $layout : '[]' !!},
                judul_layout:'{!! isset($layout_bus->judul_layout) ? $layout_bus->judul_layout: null !!}',
                jumlah_kursi:'{!! isset($layout_bus->jumlah_kursi) ? $layout_bus->jumlah_kursi: null !!}',
                harga_kursi:'{!! isset($layout_bus->harga_kursi) ? $layout_bus->harga_kursi: null !!}',
                error_col_message:'',
                error_num_col:false,
                error_row_message:'',
                error_num_row:false,
                createChair(){
                    // console.log(this.layout_chair);
                    this.checkChairLayout(this.layout_chair)

                    this.collumn = $("input[name='kolom_kursi']").val();
                    this.row  = $("input[name='baris_kursi']").val();
                    this.judul_layout = $("input[name='judul_layout']").val();
                    this.jumlah_kursi = this.collumn * this.row

                    
                    
                    if(this.collumn == 0){
                        this.error_col_message = 'Jumlah kolom harus diisi';
                        this.error_num_col = true;

                        return this.error_num_col, this.error_col_message
                    }

                    if(this.collumn < 2){
                        this.error_col_message = 'Jumlah kolom harus lebih dari 1';
                        this.error_num_col = true;

                        return this.error_num_col, this.error_col_message
                    }

                    if(this.collumn > 6){
                        this.error_col_message = 'Jumlah kolom tidak boleh lebih dari 6';
                        this.error_num_col = true;

                        return this.error_num_col, this.error_col_message 
                    }

                    if(this.row == 0){
                        this.error_row_message = 'Jumlah baris harus diisi';
                        this.error_num_row = true;

                        return this.error_num_row, this.error_num_row
                    }
                    
                    // for(let i= 0; i < this.collumn; i++){
                    //     // this.layout_chair.push(i)
                    //     this.layout_chair[i] = new Array()
                    //     for(let x =1; x < this.row; x++){
                          
                    //         this.chair_number=x;
                    //         this.layout_chair[i][x]={
                    //             chair_number:this.chair_number,
                    //             disable_chair:this.disable_chair,
                    //             price_chair:this.price_chair,
                    //         }
                    //     }
                        
                    // }

                    for(let i=1; i <= this.jumlah_kursi; i++){
                        this.remove(i);
                        this.chair_number = i

                        this.layout_chair.push({
                            chair_number:this.chair_number,
                            price_chair:$("#harga_kursi").val() != '' ? $("#harga_kursi").val(): '',
                            disable_chair:this.disable_chair,
                        })
                    }

                },
                disableChair(index){
                    let checked =  document.getElementById('disable['+index+']').checked;
                    
                    if(checked){
                        document.getElementById('kursi['+index+']').classList.add("disabled");
                        
                        document.getElementById('kursi['+index+']').classList.remove("border-[#9E3D64]");
                        document.getElementById('kursi['+index+']').classList.remove("bg-[#9E3D64]");
                        document.getElementById('kursi['+index+']').classList.add("bg-gray-400");
                        document.getElementById('kursi['+index+']').classList.add("border-gray-400");
                      
                        document.getElementById('harga['+index+']').classList.add("disabled");

                        document.getElementById('kursi['+index+']').classList.remove("border-[#9E3D64]");
                        document.getElementById('kursi['+index+']').classList.add("border-gray-400");
                        document.getElementById('harga['+index+']').classList.add("bg-gray-400");
                        
                        this.disable_chair = true
                    }

                    if(!checked){
                        document.getElementById('kursi['+index+']').disabled= false;
                        
                        document.getElementById('kursi['+index+']').classList.remove("bg-gray-400");
                        document.getElementById('kursi['+index+']').classList.remove("border-gray-400");                        
                        document.getElementById('kursi['+index+']').classList.add("border-[#9E3D64]");
                        document.getElementById('kursi['+index+']').classList.add("bg-[#9E3D64]");
                      
                        document.getElementById('kursi['+index+']').classList.remove("border-gray-400");
                        document.getElementById('harga['+index+']').classList.remove("bg-gray-400");
                        document.getElementById('harga['+index+']').disabled= false;

                        this.disable_chair = false

                    }

                    console.log(this.layout_chair)

                },
                checkChairLayout(data){
                    
                    if(data.length > 0){
                        // for(let i=0; i < data.length; i++){
                        //     // this.remove(i);
                        //     data.splice(i, 1)
                        //     this.layout_chair.splice(0, )
                        // }
                        data.splice(0, data.length)
                        data.splice(0, this.layout_chair.length)
                        console.log(data)
                    }
                },
                remove(index){
                    console.log(index)
                    this.layout_chair.splice(index, 1)
                },
                // classRemove(className, newClass){
                //     $("#"+className).removeClass("grid grid-cols-4 grid-flow-row gap-2");
                // }
            }
           
        }

        function availablity(id){
            Alpine.nextTick(()=>{
                
                let selector = 'disable'+id
                let kursi = 'kursi'+id
                let harga = 'harga'+id

                let disabled = document.getElementById(selector);
                let value = disabled.value

                if(disabled.checked){
                    // alert('test');
                    document.getElementById(kursi).classList.add("disabled");
                    document.getElementById(kursi).classList.remove("border-[#9E3D64]");
                    document.getElementById(kursi).classList.remove("bg-[#9E3D64]");
                    document.getElementById(kursi).classList.add("bg-gray-400");
                    document.getElementById(kursi).classList.add("border-gray-400");
                    
                    document.getElementById(harga).classList.add("disabled");
                    document.getElementById(kursi).classList.remove("border-[#9E3D64]");
                    document.getElementById(kursi).classList.add("border-gray-400");
                    document.getElementById(harga).classList.add("bg-gray-400");
                    // return;
                }   

                // console.log(disabled.checked)
                // console.log(value)
            })
        }

        function classRemove(idName){
            let kolom = $("input[name='kolom_kursi']").val();
            let newClass = `grid grid-cols-${kolom} grid-flow-row gap-2`
            console.log(newClass)
            let id_grid = $("#"+idName);

            id_grid.removeClass("grid grid-cols-4 grid-flow-row gap-2");
            id_grid.addClass(newClass);
        }

        // window.onload = function() {
        //     document.getElementById('bus_medium_31').style.display = 'block';
        //     document.getElementById('bus_medium_39').style.display = 'none';
        // }

        // function radioShow() {
        //     if (document.getElementById('kursi_bus_medium_31').checked) {
        //         document.getElementById('bus_medium_31').style.display = "block";
        //         document.getElementById('bus_medium_39').style.display = "none";
        //     } else if (document.getElementById('kursi_bus_medium_39').checked) {
        //         document.getElementById('bus_medium_31').style.display = "none";
        //         document.getElementById('bus_medium_39').style.display = "block";
        //     }
        // }
    </script>
</body>

</html>
