<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-lg font-bold font-inter text-[#000000]">Edit Merek Bus</span>

                <div class="bg-[#FFFFFF] drop-shadow-xl p-10 mt-5 rounded">
                    @foreach($data as $mm)
                    <form action="{{ route('masterMobil.updateBus') }}" method="POST" x-data="paketHandler()">
                        @csrf

                        <p class="text-lg font-bold font-inter text-[#333333] px-5 mb-5">Masukan Merek Mobil</p>
                        <div class="px-5 mb-5">
                            <input type="text" id="id_merek" name="id_merek"
                                class=" bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 font-bold text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5"
                                value="{{ $mm->id_merek }}">
                        </div>
                        <div class="px-5 mb-5">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Jenis
                                Kendaraan</label>
                            <select name="jenisbus" id="jenisbus" x-model="jenisbus"
                                class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                @foreach ($jenisbus as $jb)
                                <option value="{{ $jb->id_jenis }}">{{ $jb->jenis_kendaraan}}</option>
                                @endforeach
                            </select>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>
                        </div>
                        <div class="px-5 mb-5">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Merek
                                Mobil</label>
                            <input type="text" id="merek_bus" name="merek_bus"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 font-bold text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5"
                                value="{{ $mm->merek_bus }}">
                        </div>
                        <div class="px-5 mb-5">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Plat
                                Mobil</label>
                            <input type="text" id="plat_bus" name="plat_bus"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5"
                                value="{{ $mm->plat_bus }}">
                        </div>

                        <div class="p-5">
                            <div class="grid grid-cols-6">
                                <div class="col-end-8 col-end-2">
                                    <button type="submit"
                                        class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <script>
        function paketHandler() {
        return {
            jenisbus: "{!! isset($master_mobil['id_jenis']) ? $master_mobil['id_jenis'] : "" !!}",
        }
    }
    </script>
</body>

</html>