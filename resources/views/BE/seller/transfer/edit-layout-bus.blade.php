<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        #bus_medium_31 {
            display: block;
        }

        #bus_medium_39 {
            display: none;
        }

        #bus_besar_59 {
            display: none;
        }

        #microbus_19 {
            display: none;
        }

        #minibus_15 {
            display: none;
        }
    </style>
    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <div class="flex gap-2 mb-5">
                    <button type="button"
                        class="text-[#333333] font-medium rounded-lg text-sm py-2 text-center inline-flex items-center mr-2 hover:text-[#23AEC1]">
                        <img src="{{ asset('storage/icons/chevron.svg') }}" alt="user-profile" class="w-5 h-5 mr-1">
                        <span class="text-lg font-bold font-inter text-[#333333]">Edit Layout Bus</span>
                    </button>
                </div>

                <div>
                    @foreach($data as $lb)
                    <form action="{{ route('masterBus.updateBus') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="bg-[#FFFFFF] drop-shadow-xl p-6 px-10 rounded">
                            <div class="text-sm font-bold font-inter mb-5 text-[#333333]">
                                <h1 class="text-lg">Edit Layout Kursi</h1>
                                <div class="px-5 mb-5">
                                    <input id="id_layout" name="id_layout"
                                        class="hidden bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 font-bold text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5"
                                        value="{{ $lb->id_layout }}">
                                </div>
                            </div>
                            <div class="flex gap-5">
                                <div class="block mb-5 gap-5">
                                    <label for="text"
                                        class="block text-sm font-bold text-[#333333] dark:text-gray-300">Layout
                                        Kursi</label>
                                    <div class="flex gap-5 pt-2 pb-2">
                                        <div class="flex items-center py-px">
                                            <input id="layout_kursi" name="layout_kursi" type="radio"
                                                value="Bus Medium 31 Kursi"
                                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                                checked>
                                            <label for="bus_medium_31"
                                                class="ml-2 text-sm font-semibold text-[#333333]">Bus Medium 31
                                                Seat</label>
                                        </div>
                                        <div class="flex items-center py-px">
                                            <input id="layout_kursi" name="layout_kursi" type="radio"
                                                value="Bus Medium 39 Kursi"
                                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="layout_kursi"
                                                class="ml-2 text-sm font-semibold text-[#333333]">Bus Medium 39
                                                Seat</label>
                                        </div>
                                    </div>
                                    <div class="flex gap-5 pt-2 pb-2">
                                        <div class="flex items-center py-px">
                                            <input id="layout_kursi" name="layout_kursi" type="radio"
                                                value="Bus Besar 59 Kursi"
                                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="layout_kursi"
                                                class="ml-2 text-sm font-semibold text-[#333333]">Bus Besar 59
                                                Seat</label>
                                        </div>
                                        <div class="flex items-center py-px">
                                            <input id="layout_kursi" name="layout_kursi" type="radio"
                                                value="Microbus 19 Kursi"
                                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="layout_kursi"
                                                class="ml-2 text-sm font-semibold text-[#333333]">Microbus 19
                                                Seat</label>
                                        </div>
                                    </div>
                                    <div class="flex gap-5 pt-2 pb-5">
                                        <div class="flex items-center py-px">
                                            <input id="layout_kursi" name="layout_kursi" type="radio"
                                                value="Minibus 15 Kursi"
                                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="layout_kursi"
                                                class="ml-2 text-sm font-semibold text-[#333333]">Minibus 15
                                                Seat</label>
                                        </div>
                                    </div>

                                    <div class="mb-5">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Lantai
                                            Bus Lantai Bus</label>
                                        <select name="lantai_bus" id="lantai_bus"
                                            class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option value="">-- Pilih Lantai --</option>
                                            <option value="Lantai 1">Lantai 1</option>
                                            <option value="Lantai 2">Lantai 2</option>
                                        </select>
                                        <div
                                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-2 gap-5">
                                        <div class="w-full pt-5">
                                            {{-- 39 Kursi --}}
                                            <div id="bus_medium_39" class="border-2 border-[#9E3D64] rounded-md">
                                                <div class="p-5">
                                                    <p class="font-bold text-center text-[#333333] mb-2.5">Bus 1 Lantai,
                                                        39 Kursi</p>
                                                    <div class="rounded border border-gray-400 p-2.5 bg-[#9E3D64]">
                                                        <p class="font-bold text-center text-white">Bagian Depan</p>
                                                        <input name="jumlah_kursi" id="jumlah_kursi" value="39"
                                                            class="hidden numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                    </div>
                                                    <div class="grid grid-cols-5 gap-2">
                                                        <div class="gap-2 grid grid-rows-8">
                                                            <input class="w-full h-10 px-2.5 bg-[#FFFFFF]" disabled>

                                                            <input name="kursi_1" id="kursi_1"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]"
                                                                value="{{ $lb->kursi_1 }}">
                                                            <input name="harga_1" id="harga_1"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                value="{{ $lb->harga_1 }}" placeholder="Harga">

                                                            <input name="kursi_5" id="kursi_5"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_5" id="harga_5"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_9" id="kursi_9"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_9" id="harga_9"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_13" id="kursi_13"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_13" id="harga_13"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_17" id="kursi_17"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_17" id="harga_17"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_21" id="kursi_21"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_21" id="harga_21"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_25" id="kursi_25"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_25" id="harga_25"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_31" id="kursi_31"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_31" id="harga_31"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input name="kursi_35" id="kursi_35"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_35" id="harga_35"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">
                                                        </div>
                                                        <div class="gap-2 grid grid-rows-8">
                                                            <input class="w-full h-10 px-2.5 bg-[#FFFFFF]" disabled>

                                                            <input name="kursi_2" id="kursi_2"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_2" id="harga_2"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_6" id="kursi_6"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_6" id="harga_6"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_10" id="kursi_10"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_10" id="harga_10"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_14" id="kursi_14"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_14" id="harga_14"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_18" id="kursi_18"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_18" id="harga_18"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_22" id="kursi_22"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_22" id="harga_22"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_26" id="kursi_26"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_26" id="harga_26"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_32" id="kursi_32"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_32" id="harga_32"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input name="kursi_36" id="kursi_36"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_36" id="harga_36"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">
                                                        </div>
                                                        <div class="gap-2 grid grid-rows-8">
                                                            <input class="w-full h-10 px-2.5 bg-[#FFFFFF]" disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input name="kursi_37" id="kursi_37"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_37" id="harga_37"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">
                                                        </div>
                                                        <div class="gap-2 grid grid-rows-8">
                                                            <input class="w-full h-10 px-2.5 bg-[#FFFFFF]" disabled>

                                                            <input name="kursi_3" id="kursi_3"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_3" id="harga_3"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_7" id="kursi_7"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_7" id="harga_7"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_11" id="kursi_11"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_11" id="harga_11"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_15" id="kursi_15"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_15" id="harga_15"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_19" id="kursi_19"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_19" id="harga_19"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_23" id="kursi_23"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_23" id="harga_23"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_27" id="kursi_27"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_27" id="harga_27"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_29" id="kursi_29"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_29" id="harga_29"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_33" id="kursi_33"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_33" id="harga_33"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_38" id="kursi_38"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_38" id="harga_38"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">
                                                        </div>
                                                        <div class="gap-2 grid grid-rows-8">
                                                            <input class="w-full h-10 px-2.5 bg-[#FFFFFF]" disabled>

                                                            <input name="kursi_4" id="kursi_4"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_4" id="harga_4"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_8" id="kursi_8"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_8" id="harga_8"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_12" id="kursi_12"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_12" id="harga_12"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_16" id="kursi_16"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_16" id="harga_16"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_20" id="kursi_20"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_20" id="harga_20"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_24" id="kursi_24"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_24" id="harga_24"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_28" id="kursi_28"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_28" id="harga_28"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_30" id="kursi_30"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="kursi_30" id="kursi_30"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_34" id="kursi_34"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_34" id="harga_34"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_39" id="kursi_39"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_39" id="harga_39"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- 31 Kursi--}}
                                            <div id="bus_medium_31" class="border-2 border-[#9E3D64] rounded-md">
                                                <div class="p-5">
                                                    <p class="font-bold text-center text-[#333333] mb-2.5">Bus 1 Lantai,
                                                        31 Kursi</p>
                                                    <div class="rounded border border-gray-400 p-2.5 bg-[#9E3D64]">
                                                        <p class="font-bold text-center text-white">Bagian Depan</p>
                                                        <input name="jumlah_kursi" id="jumlah_kursi" value="31"
                                                            class="hidden numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                    </div>
                                                    <div class="grid grid-cols-5 gap-2">
                                                        <div class="gap-2 grid grid-rows-8">
                                                            <input class="w-full h-10 px-2.5 bg-[#FFFFFF]" disabled>

                                                            <input name="kursi_1" id="kursi_1"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]"
                                                                value="{{ $lb->kursi_1 }}">
                                                            <input name="harga_1" id="harga_1"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                value="{{ $lb->harga_1 }}" placeholder="Harga">

                                                            <input name="kursi_5" id="kursi_5"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_5" id="harga_5"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_9" id="kursi_9"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_9" id="harga_9"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_13" id="kursi_13"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_13" id="harga_13"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_17" id="kursi_17"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_17" id="harga_17"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_21" id="kursi_21"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_21" id="harga_21"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input name="kursi_27" id="kursi_27"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_27" id="harga_27"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">
                                                        </div>
                                                        <div class="gap-2 grid grid-rows-8">
                                                            <input class="w-full h-10 px-2.5 bg-[#FFFFFF]" disabled>

                                                            <input name="kursi_2" id="kursi_2"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_2" id="harga_2"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_6" id="kursi_6"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_6" id="harga_6"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_10" id="kursi_10"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_10" id="harga_10"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_14" id="kursi_14"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_14" id="harga_14"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_18" id="kursi_18"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_18" id="harga_18"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_22" id="kursi_22"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_22" id="harga_22"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input name="kursi_28" id="kursi_28"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_28" id="harga_28"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">
                                                        </div>
                                                        <div class="gap-2 grid grid-rows-8">
                                                            <input class="w-full h-10 px-2.5 bg-[#FFFFFF]" disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input
                                                                class="numberstyle text-center w-full h-14 rounded px-2.5 border-[#ffffff] bg-[#ffffff]"
                                                                disabled>
                                                            <input
                                                                class="numberstyle w-full h-8 rounded border-[#ffffff] bg-[#ffffff] px-1"
                                                                disabled>

                                                            <input name="kursi_29" id="kursi_29"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_29" id="harga_29"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">
                                                        </div>
                                                        <div class="gap-2 grid grid-rows-8">
                                                            <input class="w-full h-10 px-2.5 bg-[#FFFFFF]" disabled>

                                                            <input name="kursi_3" id="kursi_3"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_3" id="harga_3"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_7" id="kursi_7"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_7" id="harga_7"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_11" id="kursi_11"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_11" id="harga_11"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_15" id="kursi_15"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_15" id="harga_15"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_19" id="kursi_19"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_19" id="harga_19"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_23" id="kursi_23"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_23" id="harga_23"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_25" id="kursi_25"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_25" id="harga_25"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_30" id="kursi_30"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_30" id="harga_30"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">
                                                        </div>
                                                        <div class="gap-2 grid grid-rows-8">
                                                            <input class="w-full h-10 px-2.5 bg-[#FFFFFF]" disabled>

                                                            <input name="kursi_4" id="kursi_4"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_4" id="harga_4"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_8" id="kursi_8"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_8" id="harga_8"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_12" id="kursi_12"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_12" id="harga_12"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_16" id="kursi_16"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_16" id="harga_16"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_20" id="kursi_20"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_20" id="harga_20"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_24" id="kursi_24"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_24" id="harga_24"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_26" id="kursi_26"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_26" id="harga_26"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">

                                                            <input name="kursi_31" id="kursi_31"
                                                                class="numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                            <input name="harga_31" id="harga_31"
                                                                class="numberstyle w-full h-8 rounded border border-[#9E3D64] px-1"
                                                                placeholder="Harga">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- 19 Kursi--}}
                                            <div id="microbus_19" class="border-2 border-[#9E3D64] rounded-md">
                                                <div class="p-5">
                                                    <p class="font-bold text-center text-[#333333] mb-2.5">Microbus, 19
                                                        Kursi</p>
                                                    <div class="rounded border border-gray-400 p-2.5 bg-[#9E3D64]">
                                                        <p class="font-bold text-center text-white">Bagian Depan</p>
                                                        <input name="jumlah_kursi" id="jumlah_kursi" value="19"
                                                            class="hidden numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- 15 Kursi--}}
                                            <div id="minibus_15" class="border-2 border-[#9E3D64] rounded-md">
                                                <div class="p-5">
                                                    <p class="font-bold text-center text-[#333333] mb-2.5">Minibus, 15
                                                        Kursi</p>
                                                    <div class="rounded border border-gray-400 p-2.5 bg-[#9E3D64]">
                                                        <p class="font-bold text-center text-white">Bagian Depan</p>
                                                        <input name="jumlah_kursi" id="jumlah_kursi" value="15"
                                                            class="hidden numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- 59 Kursi--}}
                                            <div id="bus_besar_59" class="border-2 border-[#9E3D64] rounded-md">
                                                <div class="p-5">
                                                    <p class="font-bold text-center text-[#333333] mb-2.5">Bus Lantai 1,
                                                        59 Kursi</p>
                                                    <div class="rounded border border-gray-400 p-2.5 bg-[#9E3D64]">
                                                        <p class="font-bold text-center text-white">Bagian Depan</p>
                                                        <input name="jumlah_kursi" id="jumlah_kursi" value="59"
                                                            class="hidden numberstyle text-center w-full h-14 rounded border border-[#9E3D64] px-2.5 text-[#ffffff] bg-[#9E3D64]">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="p-5">
                                <div class="grid grid-cols-6">
                                    {{--<div class="col-start-1 col-end-2">
                                        <a href="{{ route('detailproduk.informasi') }}"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">Sebelumnya</a>
                                    </div>--}}
                                    <div class="col-end-8 col-end-2">
                                        <button type="submit"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                            Simpan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    @livewireScripts

    <script>
        $('input[type="radio"]').click(function () {
        var inputValue = $(this).attr("value");
        if (inputValue == "Bus Medium 31 Kursi") {
            $("#bus_besar_59").hide();
            $("#microbus_19").hide();
            $("#minibus_15").hide();
            $("#bus_medium_39").hide();
            $("#bus_medium_31").show();
        } else if (inputValue == "Bus Medium 39 Kursi") {
            $("#bus_besar_59").hide();
            $("#microbus_19").hide();
            $("#minibus_15").hide();
            $("#bus_medium_39").show();
            $("#bus_medium_31").hide();
        } else if (inputValue == "Bus Besar 59 Kursi"){
            $("#bus_besar_59").show();
            $("#microbus_19").hide();
            $("#minibus_15").hide();
            $("#bus_medium_39").hide();
            $("#bus_medium_31").hide();
        } else if (inputValue == "Microbus 19 Kursi"){            
            $("#bus_besar_59").hide();
            $("#microbus_19").show();
            $("#minibus_15").hide();
            $("#bus_medium_39").hide();
            $("#bus_medium_31").hide();
        } else {            
            $("#bus_besar_59").hide();
            $("#microbus_19").hide();
            $("#minibus_15").show();
            $("#bus_medium_39").hide();
            $("#bus_medium_31").hide();
        }
        });
    </script>
</body>

</html>