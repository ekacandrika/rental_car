<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    {{-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script> --}}
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css" rel="stylesheet">

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }
        .disabled{
            pointer-events: none;
            cursor: default;
            text-decoration: none;
            color: #858585;
        }
    </style>
    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                    {{ route('hotelfaq') }}
                @else
                    {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                    {{ route('faq.index') }}
                @else
                    {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>
        <div class="col-start-3 col-end-11 z-0">
            {{-- @dump($transfer) --}}
            @php
              $type            = isset($add_listing['status']) ? $add_listing['status'] : null;    
              $product_name    = isset($add_listing['product_name']) ? $add_listing['product_name'] : null;    
              $nama_kendaraan  = isset($add_listing['nama_kendaraan']) ? $add_listing['nama_kendaraan'] : null;    
              $id_jenis_mobil  = isset($add_listing['id_jenis_mobil']) ? $add_listing['id_jenis_mobil'] : null;    
              $id_jenis_bus    = isset($add_listing['id_jenis_bus']) ? $add_listing['id_jenis_bus'] : null;    
              $kapasitas_kursi = isset($add_listing['kapasitas_kursi']) ? $add_listing['kapasitas_kursi'] : null;    
              $kapasitas_koper = isset($add_listing['kapasitas_koper']) ? $add_listing['kapasitas_koper'] : null;    
              $deskripsi       = isset($add_listing['deskripsi']) ? $add_listing['deskripsi'] : null;    
              $catatan         = isset($add_listing['catatan']) ? $add_listing['catatan'] : null;    
              $pilihan_tambahan= isset($add_listing['pilihan_tambahan']) ? $add_listing['pilihan_tambahan'] : null;    
              $gallery         = isset($add_listing['gallery']) ? json_encode($add_listing['gallery']) : [];    
            //   dump($pilihan_tambahan);
            @endphp
            <div class="p-5 pl-10 pr-10">
                <div class="flex gap-2">
                    <button type="button"
                        class="text-[#333333] font-medium rounded-lg text-sm py-2 text-center inline-flex items-center mr-2 hover:text-[#23AEC1]">
                        <img src="{{ asset('storage/icons/chevron.svg') }}" alt="user-profile" class="w-5 h-5 mr-1">
                        <span class="text-lg font-bold font-inter text-[#333333]">Tambah Listing</span>
                    </button>
                </div>
                {{-- Breadcumbs --}}
                <nav class="flex mx-2.5" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            <a href="{{route('transfer')}}"
                                class="inline-flex items-center text-sm font-bold font-inter text-[#23AEC1]">Detail
                                Kendaraan</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('transfer.viewRuteSchedule')}}"
                                    class="inline-flex items-center text-sm font-bold font-inter {{isset($transfer['rute']) ? ($transfer['rute']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'disabled') :'disabled'}}">Rute
                                    Harga dan Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('transfer.viewBatas')}}"
                                    class="inline-flex items-center text-sm font-bold font-inter {{isset($transfer['batas']) ? ($transfer['batas']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'disabled') :'disabled'}}">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="pilihan"
                                    class="inline-flex items-center text-sm font-bold font-inter {{isset($transfer['pilihan_ekstra']) ? ($transfer['pilihan_ekstra']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'disabled') :'disabled'}}">Pilihan
                                    dan Ekstra</a>
                            </div>
                        </li>
                        
                    </ol>
                </nav>

                <div class="flex gap-5 py-5">
                    <div class="flex items-center py-px">
                        <input id="radio_umum" name="radio_itinerary" x-model="radio_itinerary" type="radio"
                            value="umum"
                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                            onclick="javascript:radioShow();" checked>
                        <label for="radio_umum" class="ml-2 text-sm font-semibold text-[#333333]">Transfer Umum
                            (Schedule)</label>
                    </div>
                    <div class="flex items-center py-px">
                        <input id="radio_private" name="radio_itinerary" x-model="radio_itinerary" type="radio"
                            value="open"
                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                            onclick="javascript:radioShow();">
                        <label for="radio_open" class="ml-2 text-sm font-semibold text-[#333333]">Transfer Private (Non
                            Schedule)</label>
                    </div>
                </div>

                <div id="open" style="display:none">
                    <form
                        action="{{ $isedit ==true ? route('transfer.addListingPrivate.edit', $id) : route('transfer.addListingPrivate') }}"
                        method="POST" enctype="multipart/form-data" x-data="paketHandler()" id="formTransferOpen">
                        @csrf
                        @method('PUT')

                        <div class="bg-[#FFFFFF] drop-shadow-xl p-6 px-10 rounded">
                            <div class="grid grid-cols-2">
                                <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Detail Kendaraan</div>
                                <div class="text-sm font-bold font-inter p-5 text-[#333333] text-right">ID Listing:
                                    {{ $listing }}</div>
                            </div>
                            <div class="px-5 mb-5">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Judul
                                    Listing</label>
                                <input type="text" id="product_name_open" name="product_name"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Mobil A">
                            </div>

                            <div class="block px-5 mb-5 flex gap-5">
                                <div>
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Jenis
                                        Kendaraan</label>
                                    <select name="jenismobil" id="jenismobil" x-model="jenismobil"
                                        class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled>Pilih Kendaraan</option>
                                        @foreach ($jenismobil as $jm)
                                            <option value="{{ $jm->id }}" {{$id_jenis_mobil!=''? ($jm->id == $id_jenis_mobil ?'selected':'') :""}}>{{ $jm->jenis_kendaraan }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>
                                </div>
                                <div>
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-white dark:text-gray-300">Jenis
                                        Kendaraan</label>
                                    <select name="merekmobil" id="merekmobil" x-model="merekmobil"
                                        class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                    </select>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>
                                </div>
                            </div>

                            <div class="block px-5 mb-5 flex gap-5">
                                <div>
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nama
                                        Kendaraan</label>
                                    <input type="text" id="nama_kendaraan" name="nama_kendaraan"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Nama Kendaraan">
                                </div>

                                <div>
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Tipe</label>
                                    <input type="text" id="status" name="status"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 placeholder-[#333333]"
                                        placeholder="Transfer Private" readonly>
                                    <input id="status" name="status" type="radio" value="Transfer Private"
                                        class=" hidden w-4 h-4 text-blue-600 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                        checked>
                                </div>
                            </div>

                            <div class="grid grid-cols-2">
                                <div class="flex mb-5">
                                    <div class="px-5">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kapasitas
                                            Kursi</label>
                                        <input type="text" id="kapasitas_kursi" name="kapasitas_kursi"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Kapasitas Kursi">
                                    </div>

                                    <div class="pr-5">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kapasitas
                                            Koper</label>
                                        <input type="text" id="kapasitas_koper" name="kapasitas_koper"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Kapasitas Koper">
                                    </div>
                                </div>
                            </div>

                            <div class="px-5 mb-5" x-data="featuredImage()">
                                <p class="font-semibold text-[#BDBDBD]">Tambah Gambar</p>
                                <input class="py-2" type="file" id="gallery" name="gallery[]"
                                    accept="image/*" @change="selectedFile" multiple>
                                <template x-if="images.length < 1">
                                    <div
                                        class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                        <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                            width="20px" height="20px">
                                    </div>
                                </template>
                                <template x-if="images.length >= 1">
                                    <div class="flex">
                                        <template x-for="(image, index) in images" :key="index">
                                            <div class="flex justify-center items-center">
                                                <img :src="image"
                                                    class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                    :alt="'upload' + index">
                                                <button type="button"
                                                    class="absolute mx-2 translate-x-12 -translate-y-14">
                                                    <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                                alt="" width="25px" @click="removeImage($event,index)">
                                                </button>
                                            </div>
                                        </template>
                                    </div>
                                </template>
                            </div>

                            <div class="px-5" wire:ignore>
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333]">Deskripsi</label>
                                <textarea class="form-control w-[10rem] deskripsi" name="deskripsi" id="deskripsi"></textarea>
                            </div>

                            <div class="px-5 pt-5" wire:ignore>
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333]">Komplemen</label>
                                <textarea class="form-control w-[10rem] catatan" name="catatan" id="catatan"></textarea>
                            </div>

                            <div class="p-5">
                                <div class="grid grid-cols-6">
                                    <div class="col-end-8 col-end-2">
                                        <button type="submit"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                            Selanjutnya
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div id="umum" style="display:block">
                    <form
                        action="{{ $isedit==true ? route('transfer.addListingUmum.edit', $id) : route('transfer.addListingUmum') }}"
                        method="POST" enctype="multipart/form-data" x-data="paketHandler()" id="formTransferReguler">
                        @csrf
                        @method('PUT')

                        <div class="bg-[#FFFFFF] drop-shadow-xl p-6 px-10 rounded">
                            <div class="grid grid-cols-2">
                                <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Detail Kendaraan</div>
                                <div class="text-sm font-bold font-inter p-5 text-[#333333] text-right">ID Listing:
                                    {{ $listing }}</div>
                            </div>
                            <div class="px-5 mb-5">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Judul
                                    Listing</label>
                                <input type="text" id="product_name_umum" name="product_name"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Mobil A">
                            </div>

                            
                            <div class="block px-5 mb-5 flex gap-5">
                                <div>
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Jenis
                                        Kendaraan</label>
                                    <select name="jenisbus" id="jenisbus" x-model="jenisbus"
                                        class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected>Pilih Kendaraan</option>
                                        @foreach ($jenisbus as $jb)
                                            <option value="{{ $jb->id_jenis }}" {{$id_jenis_bus!=''? ($jb->id_jenis== $id_jenis_bus ?'selected':'') :""}}>{{ $jb->jenis_kendaraan }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>
                                </div>
                                <div>
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-white dark:text-gray-300">Jenis
                                        Kendaraan</label>
                                    <select name="merekbus" id="merekbus" x-model="merekbus"
                                        class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                    </select>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>
                                </div>
                            </div>

                            <div class="block px-5 mb-5 flex gap-5">
                                <div>
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nama
                                        Kendaraan</label>
                                    <input type="text" id="nama_kendaraan_umum" name="nama_kendaraan"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Nama Kendaraan">
                                </div>

                                <div>
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Status/Tipe</label>
                                    <input type="text" id="status" name="status"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 placeholder-[#333333]"
                                        placeholder="Transfer Umum" readonly>
                                    <input id="status" name="status" type="radio" value="Transfer Umum"
                                        class=" hidden w-4 h-4 text-blue-600 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                        checked>
                                </div>
                            </div>

                            <div class="grid grid-cols-2">
                                <div class="flex mb-5">
                                    <div class="px-5">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kapasitas
                                            Kursi</label>
                                        <input type="text" id="kapasitas_kursi_umum" name="kapasitas_kursi"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Kapasitas Kursi">
                                    </div>

                                    <div class="pr-5">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kapasitas
                                            Koper</label>
                                        <input type="text" id="kapasitas_koper_umum" name="kapasitas_koper"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Kapasitas Koper">
                                    </div>
                                </div>
                            </div>

                            <div class="px-5 mb-5" x-data="featuredImageUmum()">
                                <p class="font-semibold text-[#BDBDBD]">Tambah Gambar</p>
                                <input class="py-2" type="file" id="gallery_transfer_umum" name="gallery[]"
                                    accept="image/*" @change="selectedFile" multiple>
                                <template x-if="images.length < 1">
                                    <div
                                        class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                        <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                            width="20px" height="20px">
                                    </div>
                                </template>
                                <template x-if="images.length >= 1">
                                    <div class="flex">
                                        <template x-for="(image, index) in images" :key="index">
                                            {{-- <template x-if="image.length > 1"> --}}
                                                {{-- <template x-for="(img, key) in image" :key="key">
                                                    <div class="flex justify-center items-center">
                                                        <img :src="'{!!url('/')!!}'+'/'+img"
                                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                            :alt="'upload' + key">
                                                    </div>
                                                </template> --}}
                                            {{-- </template> --}}
                                                <div class="flex justify-center items-center">
                                                    <img :src="image"
                                                        class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                        :alt="'upload' + index">
                                                    <button type="button"
                                                        class="absolute mx-2 translate-x-12 -translate-y-14">
                                                        <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                                    alt="" width="25px" @click="removeImage($event,index)">
                                                    </button>
                                                </div>
                                        </template>
                                    </div>
                                </template>
                            </div>

                            <div class="px-5" wire:ignore>
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333]">Deskripsi</label>
                                <textarea class="form-control w-[10rem] deskripsi" name="deskripsi" id="deskripsi_umum"></textarea>
                            </div>

                            <div class="px-5 pt-5" wire:ignore>
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333]">Komplemen</label>
                                <textarea class="form-control w-[10rem] catatan" name="catatan" id="catatan_umum"></textarea>
                            </div>

                            <div class="px-5 pt-5" wire:ignore>
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Pilihan
                                    Tambahan</label>
                                <textarea class="form-control w-[10rem] tambahan" name="pilihan_tambahan" id="pilihan_tambahan_umum"></textarea>
                            </div>

                            <div class="text-sm font-bold font-inter px-5 mt-5 text-[#333333]">Tambah Layout Kursi
                            </div>

                            <div class="block px-5 mb-5 flex gap-5">
                                <div>
                                    <select name="layoutbus" id="layoutbus" x-model="layoutbus"
                                        class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled>Pilih Layout Kursi</option>
                                        @foreach ($layoutbus as $lk)
                                            <option value="{{ $lk->id }}">{{ isset($lk->judul_layout) ? $lk->judul_layout :null}}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>
                                </div>
                            </div>

                            <div class="p-5">
                                <div class="grid grid-cols-6">
                                    <div class="col-end-8 col-end-2">
                                        <button type="submit"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                            Selanjutnya
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @livewireScripts

    <script>
        var transfer_type= '{!! $type !!}'
        // alert(transfer_type)
        console.log(transfer_type)
        if(transfer_type){
            if(transfer_type=='Transfer Umum'){
                window.onload = function() {
                    document.getElementById('radio_private').checked=false;
                    document.getElementById('radio_umum'),checked=true;
                    document.getElementById('open').style.display = 'none';
                    document.getElementById('umum').style.display = "block";
                }
            }else{    
                window.onload = function() {
                    document.getElementById('radio_umum'),checked=false;
                    document.getElementById('radio_private').checked=true;
                    document.getElementById('open').style.display = "block";
                    document.getElementById('umum').style.display = 'none';
                }
            }
        }else{
            window.onload = function() {
                document.getElementById('umum').style.display = 'block';
                document.getElementById('open').style.display = 'none';
            }
        }


        function radioShow() {
            if (document.getElementById('radio_umum').checked) {
                document.getElementById('umum').style.display = "block";
                document.getElementById('open').style.display = "none";
            } else if (document.getElementById('radio_private').checked) {
                document.getElementById('umum').style.display = "none";
                document.getElementById('open').style.display = "block";
            }
        }

        function sweetalert(msg, e){
            Swal.fire({
                icon: 'error',
                text: msg,
                customClass: 'swal-height'
            })   

            e.preventDefault();
        }

        function dynamicTag(){
            return{
                tag_list:[],
                new_tag_location:'',
                id:'',
                add_list(){
                    this.tag_list.push({
                        id:'',
                        label:''
                    })

                    console.log(this.tag_list)
                },
                removeField(index){
                    this.tag_list.splice(index,1)
                },
                select2WithAlpine(){
                    this.select2 = $(this.$refs.select_new_tag).select2();
                    //this.select2 = $(this.$refs.select_new_tag).select2();
                    /*
                    this.select2.on("select2:select", (event) => {
                        this.id = event.target.value;
                    });
                    this.$watch("new_tag_location", (value) => {
                        this.select2.val(value).trigger("change");
                    });
                    */
                }
            }
        }

    </script>

    <script>
        $('.deskripsi').summernote({
            placeholder: 'Deskripsi...',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['para', ['ul', 'ol']]
            ]
        });

        $('.catatan').summernote({
            placeholder: 'Komplemen...',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });

        $('.tambahan').summernote({
            placeholder: 'Pilihan Tambahan...',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });

        function paketHandler() {

            return {
                jenismobil: "{!! isset($add_listing['id_jenis_mobil']) ? $add_listing['id_jenis_mobil'] : '' !!}",
                jenisbus: "{!! isset($add_listing['id_jenis_bus']) ? $add_listing['id_jenis_bus'] : '' !!}",
            }
        }
    </script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })

            $(function(){
                let id_jenis_mobil = $('#jenismobil').val();

                if(id_jenis_mobil){
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getmerekmobil') }}",
                        data: {
                            id_jenis_mobil: id_jenis_mobil
                        },
                        cache: false,
    
                        success: function(msg) {
                            $('#merekmobil').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                }

            })

            $(function() {
                $('#jenismobil').on('change', function() {
                    let id_jenis_mobil = $('#jenismobil').val();

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getmerekmobil') }}",
                        data: {
                            id_jenis_mobil: id_jenis_mobil
                        },
                        cache: false,

                        success: function(msg) {
                            $('#merekmobil').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })
            })

            $(function(){
                let id_jenis_bus = $('#jenisbus').val()
                
                if(id_jenis_bus){
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getmerekbus') }}",
                        data: {
                            id_jenis_bus: id_jenis_bus
                        },
                        cache: false,

                        success: function(msg) {
                            $('#merekbus').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                }
            })

            $(function() {
                $('#jenisbus').on('change', function() {
                    let id_jenis_bus = $('#jenisbus').val();

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getmerekbus') }}",
                        data: {
                            id_jenis_bus: id_jenis_bus
                        },
                        cache: false,

                        success: function(msg) {
                            $('#merekbus').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })
            })
        });
    </script>
    <script>
        function featuredImage() {
            let storage= [];
            const data = [];
            var galeri = {!! $type== 'Transfer Private' ? $gallery : '[]' !!}
            
            for(let x=0; x <  galeri.length;x++){
                data.push('{!! url('/').'/' !!}'+galeri[x].gallery)
            }

            console.log(data)

            return {
                images: data,

                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event) {
                    if (!event.target.files.length) return

                    let file = event.target.files
                    this.images = []
                    // this.data = []

                    console.log(typeof file)

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                        };

                        storage.push({
                            file:file[i]
                        })
                        // this.data = [...this.data, file[i]]
                    }

                    this.saveGallery()
                },

                removeImage(index) {
                    this.images.splice(index, 1);
                    storage.splice(index,1)
                    this.saveGallery()
                },

                saveGallery(){
                    fd = new FormData();

                    for(const file of storage){
                        console.log(file)
                        if(typeof file === 'object'){
                            fd.append('images_gallery[]',file.file)
                        }
                        if(typeof file === 'string'){
                            fd.append('saved_gallery[]',file)
                        }
                    }

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        method:'POST',
                        url:"{{ route('transfer.addScheduleGallery') }}",
                        data: fd,
                        processData: false,
                        contentType: false,
                        success:function(msg){
                            console.log(msg)
                        },
                        error:function(data){
                            console.log('error:',data)
                        }
                    });
                }
            }
        }
    </script>
    <script>
        function featuredImageUmum() {

            let storage =[];
            const data = [];
            var galeri = {!! $type== 'Transfer Umum' ? $gallery : '[]' !!}
            
            for(let y=0; y <  galeri.length;y++){
                data.push('{!! url('/').'/' !!}'+galeri[y].gallery)
            }

            return {
                images:data,

                selectedFile(event) {
                    this.fileToUrl(event)
                    // console.log(this.images)
                },

                fileToUrl(event) {
                    if (!event.target.files.length) return

                    let file = event.target.files
                    this.images = []
                    // this.data = []

                    console.log(typeof file)

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                        };

                        // this.data = [...this.data, file[i]]
                        storage.push({
                            file:file[i]
                        })
                    }

                    this.saveGallery();
                },
    
                removeImage(index) {
                    this.images.splice(index, 1);
                    storage.splice(index,1)
                    this.saveGallery()
                },
                
                saveGallery(){
                    fd = new FormData();

                    for(const file of storage){
                        console.log(file)
                        if(typeof file === 'object'){
                            fd.append('images_gallery[]',file.file)
                        }
                        if(typeof file === 'string'){
                            fd.append('saved_gallery[]',file)
                        }
                    }

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        method:'POST',
                        url:"{{ route('transfer.addRegulerGallery') }}",
                        data: fd,
                        processData: false,
                        contentType: false,
                        success:function(msg){
                            console.log(msg)
                        },
                        error:function(data){
                            console.log('error:',data)
                        }
                    });
                }
            }
        }
    </script>
    <script>
        if(transfer_type){
            if(transfer_type=='Transfer Umum'){
                $("#product_name_umum").val('{!! $product_name !!}')
                $('#nama_kendaraan_umum').val('{!! $nama_kendaraan !!}')
                $('#kapasitas_kursi_umum').val('{!! $kapasitas_kursi !!}')
                $('#kapasitas_koper_umum').val('{!! $kapasitas_koper !!}')
                $('#deskripsi_umum').summernote('code','{!! $deskripsi !!}')
                $('#catatan_umum').summernote('code','{!! $catatan !!}')
                $('#pilihan_tambahan_umum').summernote('code','{!! $pilihan_tambahan !!}')
            }else{
                $("#product_name_open").val('{!! $product_name !!}')
                $('#nama_kendaraan').val('{!! $nama_kendaraan !!}')
                $('#kapasitas_kursi').val('{!! $kapasitas_kursi !!}')
                $('#kapasitas_koper').val('{!! $kapasitas_koper !!}')
                $('#deskripsi').summernote('code','{!! $deskripsi !!}')
                $('#catatan').summernote('code','{!! $catatan !!}')
            }
        }

        // transfer umum validation
        $("#formTransferReguler").on("submit",function(e){
            if(!$("#product_name_umum").val()){
                sweetalert('Judul listing harap diisi!', e)
            }
            else if(!$("#jenisbus").val()){
               sweetalert('Jenis Bus harap dipilih', e)
            }
            else if(!$('#merekbus').val()){
               sweetalert('Merek Bus harap dipilih', e)
            }
            // nama_kendaraan_umum
            else if(!$('#nama_kendaraan_umum').val()){
               sweetalert('Nama Bus harap dipilih', e)
            }
            // kapasitas_kursi_umum
            else if(!$('#kapasitas_kursi_umum').val()){
               sweetalert('Jumlah kapasistas kursi harap diisi!', e)
            }
            else if(!$('#kapasitas_koper_umum').val()){
               sweetalert('Jumlah kapasistas kursi harap diisi!', e)
            }
            // gallery_transfer_umum
            else if(!$('#gallery_transfer_umum').val()){
               sweetalert('Gambar galeri harap diisi!', e)
            }
            else if(!$('#deskripsi_umum').val()){
               sweetalert('Deskripsi harap diisi!', e)
            }
            else if(!$('#catatan_umum').val()){
               sweetalert('Komplemen harap diisi!', e)
            }
            // 
            // e.preventDefault()
        })
    </script>
    <script>

        // transfer private validation
        $("#formTransferOpen").on("submit",function(e){
            if(!$("#product_name_open").val()){
                sweetalert('Judul listing harap diisi!', e)
            }
            else if(!$("#jenismobil").val()){
               sweetalert('Jenis kendaraan harap dipilih', e)
            }
            else if(!$('#merekmobil').val()){
               sweetalert('Merek kendaraan harap dipilih', e)
            }
            // nama_kendaraan_umum
            else if(!$('#nama_kendaraan').val()){
               sweetalert('Nama kendaraan harap dipilih', e)
            }
            // kapasitas_kursi_umum
            else if(!$('#kapasitas_kursi').val()){
               sweetalert('Jumlah kapasistas kursi harap diisi!', e)
            }
            else if(!$('#kapasitas_koper').val()){
               sweetalert('Jumlah kapasistas kursi harap diisi!', e)
            }
            else if(!$('#deskripsi').val()){
               sweetalert('Deskripsi harap diisi!', e)
            }
            else if(!$('#catatan').val()){
               sweetalert('Komplemen harap diisi!', e)
            }
            else if(!$('#gallery').val()){
               sweetalert('Gambar galeri harap diisi!', e)
            }
            // 
            // e.preventDefault()
        })    
    </script>
</body>

</html>
