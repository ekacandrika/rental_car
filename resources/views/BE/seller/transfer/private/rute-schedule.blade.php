<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css" rel="stylesheet">

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 40px;
            height: 24px;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: -8px;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 20px;
            width: 20px;
            left: 4px;
            top: 2px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(20px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 14px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
        .disabled{
            pointer-events: none;
            cursor: default;
            text-decoration: none;
            color:#858585;
        }
        .swal-height {
            height: 50vh;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>
        <div class="col-start-3 col-end-11 z-0">
            {{-- @dump($rute) --}}
            <div class="p-5 pl-10 pr-10">
                <div class="flex gap-2">
                    <button type="button"
                        class="text-[#333333] font-medium rounded-lg text-sm py-2 text-center inline-flex items-center mr-2 hover:text-[#23AEC1]">
                        <img src="{{ asset('storage/icons/chevron.svg') }}" alt="user-profile" class="w-5 h-5 mr-1">
                        <span class="text-lg font-bold font-inter text-[#333333]">Tambah Listing</span>
                    </button>
                </div>
                {{-- Breadcumbs --}}
                <nav class="flex mx-2.5" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            <a href="{{$isedit==true ? route('transfer.viewAddListingEdit',$id):route('transfer')}}"
                                class="inline-flex items-center text-sm font-bold font-inter text-[#333333] hover:text-[#23AEC1]">Detail
                                Kendaraan</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{$isedit==true ? route('transfer.viewRuteSchedule.edit',$id) : route('transfer.viewRuteSchedule')}}"
                                    class="inline-flex items-center text-sm font-bold font-inter text-[#23AEC1]">Rute
                                    Harga dan Ketersediaan</a>
                            </div>
                        </li>             
                        <li>
                            {{-- @dump($transfer) --}}
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{$isedit==true ? route('transfer.viewBatasEdit',$id) : route('transfer.viewBatas')}}"
                                    class="inline-flex items-center text-sm font-bold font-inter {{isset($transfer['batas']) ? ($transfer['batas']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'disabled') :'disabled'}}">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{$isedit ? route('transfer.viewPilihanEdit',$id) : route('transfer.viewPilihan')}}"
                                    class="inline-flex items-center text-sm font-bold font-inter disabled">Pilihan
                                    dan Ekstra</a>
                            </div>
                        </li>
                    </ol>
                </nav>
                @php
                    
                  $id_naik  = isset($transfer['rute']['id_naik']) ? $transfer['rute']['id_naik'] : null;
                  $id_turun = isset($transfer['rute']['id_turun']) ? $transfer['rute']['id_turun'] : null;

                  if(isset($isedit)){
                    $id_naik  = isset($rute['id_naik']) ? $rute['id_naik'] : null;
                    $id_turun = isset($rute['id_turun']) ? $rute['id_turun'] : null;
                  }   
                  
                @endphp
                <div class="bg-[#FFFFFF] drop-shadow-xl p-6 px-10 mt-5 rounded">
                    <form action="{{ $isedit==true ? route('transfer.editRuteSchedule',$id):route('transfer.addRuteSchedule') }}" method="POST" id="formRuteSchedule" x-data="loadData()">
                        @csrf
                        @if($isedit)
                        @method('PUT')
                        @endif
                        <template x-if="rutes.length < 1">
                            <div class="px-5 mb-5">
                                <div class="overflow-x-auto relative">
                                    <button type="button" class="text-white bg-[#23AEC1] mt-3 py-1 px-3 rounded-md" @click="addNewField()">Tambah</button>
                                </div>
                            </div>
                        </template>
                        <template x-if="rutes.length >= 1">
                            <template x-for="(rute, index) in rutes" :key="index">
                                <div>
                                    <div class="px-5 mb-5">
                                        <div class="overflow-x-auto relative">
                                            <p class="text-lg font-bold text-[#9E3D64] mt-5 mb-2">Judul</p>
                                            <input type="text" :id="'rute['+index+'][judul_bus]'" :name="'rute['+index+'][judul_bus]'"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg block w-96 p-2.5"
                                                placeholder="Judul Bus" x-model="rute.judul_bus">
                                        </div>
                                    </div>
            
                                    <div class="px-5 mb-5">
                                        <div class="overflow-x-auto relative">
                                            <p class="text-lg font-bold text-[#9E3D64] my-2">Naik</p>
                                            <select :name="'rute['+index+'][id_naik]'" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg block w-96 p-2.5" id="pickup_point" x-model="rute.id_pickup">
                                                <option selected>Pilih Titik Pickup</option>
                                                @foreach($master_drop_pick as $pick)
                                                <option value="{{$pick->id}}" {{$id_naik == $pick->id ? 'selected':''}}>{{$pick->judulDropPick}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
            
                                    <div class="px-5 mb-5">
                                        <div class="overflow-x-auto relative">
                                            <p class="text-lg font-bold text-[#9E3D64] my-2">Turun</p>
                                            <select :name="'rute['+index+'][id_turun]'" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg block w-96 p-2.5" id="drop_point" x-model="rute.id_drop">
                                                <option selected>Pilih Titik Drop</option>
                                                @foreach($master_drop_pick as $drop)
                                                <option value="{{$drop->id}}" {{$id_turun == $drop->id ? 'selected':''}}>{{$drop->judulDropPick}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
            
                                    <div class="flex px-5 mb-5">
                                        <div class="overflow-x-auto relative">
                                            <p class="text-lg font-bold text-[#9E3D64] my-2">Harga</p>
                                            <input type="text" :id="'rute['+index+'][harga]'" :name="'rute['+index+'][harga]'"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg block w-96 p-2.5"
                                                placeholder="Masukan Harga" x-model="rute.harga">
                                        </div>
                                        <div class="flex my-2 gap-3">
                                            <div class="overflow-x-auto relative">
                                                <button type="button" class="mx-2 relative" style="top:41px;">
                                                    <img src="{{ asset('storage/icons/plus-solid.svg') }}"
                                                        alt="" width="20px"
                                                        @click="addNewField()">
                                                </button>
                                            </div>
                                            <div class="overflow-x-auto relative">
                                                <button type="button" class="mx-2 relative" style="top:41px;">
                                                    <img src="{{ asset('storage/icons/trash-can-solid.svg') }}"
                                                        alt="" width="20px"
                                                        @click="removeField(index)">
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </template>
                        </template>

                        <div class="p-5">
                            <div class="grid grid-cols-6">
                                <div class="col-end-8 col-end-2">
                                    <button type="submit"
                                        class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                        Selanjutnya
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded relative" x-data="availablity()">
                    <div class="w-20 h-10 top-0 right-0" id="custom-target"></div>
                    <p class="font-bold text-lg text-[#333333] pb-5">Ketersediaan</p>
                    <div class="flex md:space-x-10 gap-5">
                        <div class="mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Dari
                                Tanggal</label>
                            <div
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <input type="text" id="tgl_start" name="tgl_start" placeholder="">
                            </div>
                        </div>
                        <div class="mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Sampai
                                Tanggal</label>
                            <div
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <input type="text" id="tgl_end" name="tgl_end" placeholder="">
                            </div>
                        </div>
                        <div class="pb-5">
                            <label for="text" class="flex mb-2 text-sm font-bold text-[#333333]"
                                x-data="{ tooltip: false }" x-on:mouseover="tooltip = true"
                                x-on:mouseleave="tooltip = false">x/- (%)>
                                <img class="mx-2" width="15px"
                                    src="{{ asset('storage/icons/circle-info-solid (1) 1.svg') }}" alt="">
                                <div x-show="tooltip"
                                    class="text-sm text-white absolute bg-[#9E3D64] rounded-lg p-2 transform -translate-y-8 translate-x-8">
                                    Harap semua field diisi
                                </div>
                            </label>
                            <input type="text" id="discount" name="discount" x-model="discount"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="">
                        </div>
                    </div>

                    <button type="button" class="text-white bg-[#23AEC1] py-1 px-3 rounded-md" @click="addField()"
                        id="btnTambah">Tambah</button>
                    @if($isedit)
                        <form action="{{ route('transfer.editAvailability',$id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                    @else
                        <form action="{{ route('transfer.addAvailability') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                    @endif
                            <div class="grid grid-cols-10 my-5">
                                <template x-if="error_validate === false">
                                    <template x-if="details.length === 0">
                                        <div class="col-span-5 border border-slate-300 rounded-md ">
                                            <div class="my-2 mx-3">
                                                <p>Anda belum menambahkan Tanggal ketersedian dan diskon.</p>
                                            </div>
                                        </div>
                                    </template>
                                </template>
                                <template x-if="error_validate === true">
                                    <div class="col-span-5 border border-red-500  bg-red-500 rounded-md" x-init="setTimeout(()=> error_validate=false, 3000)">
                                        <div class="my-2 mx-3">
                                            <p x-text="error_message"></p>
                                        </div>
                                    </div>
                                </template>
                                <template x-if="details.length >= 1">
                                    <template x-for="(detail, index) in details" :key="index">
                                        <div class="col-span-10 flex">
                                            <div class="w-5/6 border border-slate-500 rounded-md my-5">
                                                <div class="my-2 mx-3" x-data="{ open:false }">
                                                    <div class="flex border-b border-gray-200 pt-1 pb-2">
                                                        <button type="button"
                                                            class="text-lg sm:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg flex space-x-10"
                                                            x-on:click="open = !open">
                                                            <div class="cursor-pointer pr-5">
                                                                <p for="text"
                                                                    class="block mb-2 text-sm font-bold text-slate-500">
                                                                    Dari
                                                                    Tanggal</p>
                                                                <p class=" text-base font-bold text-[#333333]"
                                                                    x-text="detail.tgl_awal"></p>
                                                                <input type="hidden" x-model="detail.tgl_awal"
                                                                    :name="'details['+index+'][tgl_awal]'" required>
                                                            </div>
                                                            <div class="cursor-pointer pr-5">
                                                                <p for="text"
                                                                    class="block mb-2 text-sm font-bold text-slate-500">
                                                                    Sampai
                                                                    Tanggal</p>
                                                                <p class=" text-base font-bold text-[#333333]"
                                                                    x-text="detail.tgl_akhir"></p>
                                                                <input type="hidden" x-model="detail.tgl_akhir"
                                                                    :name="'details['+index+'][tgl_akhir]'" required>
                                                            </div>
                                                            <div class="cursor-pointer pr-5">
                                                                <p for="text"
                                                                    class="block mb-2 text-sm font-bold text-slate-500">
                                                                    +/- (%)
                                                                </p>
                                                                <p class=" text-base font-bold text-[#333333]"
                                                                    x-text="detail.diskon"></p>
                                                                <input type="hidden" x-model="detail.diskon"
                                                                    :name="'details['+index+'][diskon]'" required>
                                                            </div>
                                                        </button>
                                                        <img class="float-right duration-200 cursor-pointer"
                                                            :class="{'rotate-180' : open}" x-on:click="open = !open"
                                                            src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                                            alt="chevron-down" width="26px" height="15px">
                                                    </div>
                                                    <div class="py-2" x-show="open" x-transition>
                                                        <div>
                                                            <table class="my-1 w-full table-auto">
                                                                <thead>
                                                                    <tr>    
                                                                        <th
                                                                            class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                            Nama bus
                                                                        </th>
                                                                        <th
                                                                            class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                            Pick up
                                                                        </th>
                                                                        <th
                                                                            class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                            Drop off
                                                                        </th>
                                                                        <th
                                                                            class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                            harga
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                 <tbody>
                                                                    <template x-for="(rute, index) in detail.rute" :key="index">
                                                                        {{-- 
                                                                        harga: "150000"
                                                                        id_drop: "7"                                                                        ​​​​​​
                                                                        id_pickup: "6"
                                                                        judul_bus: "Bus A"
                                                                        latitude_drop: "-7.747350897834475"
                                                                        latitude_pickup: "-7.83454180611418"
                                                                        longitude_drop: "110.36222780990312"
                                                                        longitude_pickup: "110.39225340990393"
                                                                        tempat_drop: "Terminal Sleman"
                                                                        tempat_pickup: "Terminial Giwangan"    
                                                                        --}}
                                                                        <tr class="align-middle">
                                                                            <input type="hidden" x-model="rute.harga" :name="'rute['+index+']harga'">
                                                                            <input type="hidden" x-model="rute.id_drop" :name="'rute['+index+']id_drop'">
                                                                            <input type="hidden" x-model="rute.id_pickup" :name="'rute['+index+']id_pickup'">
                                                                            <input type="hidden" x-model="rute.judul_bus" :name="'rute['+index+']judul_bus'">
                                                                            <input type="hidden" x-model="rute.latitude_drop" :name="'rute['+index+']latitude_pickup'">
                                                                            <input type="hidden" x-model="rute.latitude_drop" :name="'rute['+index+']latitude_pickup'">
                                                                            <input type="hidden" x-model="rute.longitude_drop" :name="'rute['+index+']longitude_drop'">
                                                                            <input type="hidden" x-model="rute.longitude_drop" :name="'rute['+index+']longitude_drop'">
                                                                            <input type="hidden" x-model="rute.tempat_drop" :name="'rute['+index+']tempat_drop'">
                                                                            <input type="hidden" x-model="rute.tempat_pickup" :name="'rute['+index+']tempat_pickup'">
                                                                            <td
                                                                                class="p-3 border-2 border-slate-500 text-center">
                                                                                <p x-text="rute.judul_bus"></p>
                                                                            </td>
                                                                            <td
                                                                                class="p-3 border-2 border-slate-500 text-center">
                                                                                <p x-text="rute.tempat_pickup"></p>
                                                                            </td>
                                                                            <td
                                                                                class="p-3 border-2 border-slate-500 text-center">
                                                                                <p x-text="rute.tempat_drop"></p>
                                                                            </td>
                                                                            <td
                                                                                class="p-3 border-2 border-slate-500 text-center">
                                                                                <p x-text="ruteAvailablityPrice(rute.harga, detail.diskon)"></p>
                                                                            </td>   
                                                                        </tr>
                                                                    </template>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w-1/6 px-3 my-5">
                                                <button @click="remove(index)"
                                                class="bg-[#D50006] text-white text-sm font-semibold py-2 px-5 rounded-lg hover:bg-[#de252b]">
                                                Hapus
                                                </button>
                                            </div>
                                        </template>
                                        </div>
                                    </template>
                                </template>
                            </div>

                            {{-- <x-be.com.two-button></x-be.com.two-button> --}}
                            <div class="p-5">
                                <div class="grid grid-cols-6">
                                    <div class="col-start-1 col-end-2">
                                        <a href="{{ route('tur.viewItenenary') }}"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">Sebelumnya</a>
                                    </div>
                                    <div class="col-start-6 col-end-7">
                                        <button type="submit" id="next_button"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                            Selanjutnya
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        
        var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

        $(document).ready(function() {
            $('#tgl_start').datepicker({
                change:function(e){
                    today:$('#tgl_start').datepicker().value();
                    $('#tgl_end').datepicker().destroy();
                    $('#tgl_end').datepicker({
                        minDate:today
                    })
                },
                minDate:today
            });
        })

        $(document).ready(function() {
            $('#tgl_end').datepicker({
                nimDate:today
            });
        })

        function loadData(){
            console.log({!! isset($transfer['rute'])  ? $transfer['rute']: '[]' !!})
            return{
                judul_rute:"",
                id_pickup: "",
                latitude_pickup:"",
                longitude_pickup:"",
                tempat_pickup:"",
                id_drop: "",
                latitude_drop:"",
                longitude_drop:"",
                tempat_drop:"",
                harga:"",
                rutes:{!! isset($transfer['rute'])  ? $transfer['rute']: '[]' !!},
                addNewField(){
                    console.log('test');

                    this.rutes.push({
                        judul_bus:this.judul_rute,
                        id_pickup: this.id_pickup,
                        latitude_pickup:this.latitude_pickup,
                        longitude_pickup:this.longitude_pickup,
                        tempat_pickup:this.tempat_pickup,
                        id_drop: this.id_drop,
                        latitude_drop:this.longitude_drop,
                        longitude_drop:this.longitude_drop,
                        tempat_drop:this.tempat_drop,
                        harga:this.harga,
                    })

                    console.log(this.rutes);

                    
                },
                removeField(index){
                    this.rutes.splice(index, 1)
                }      
            }
        }

        function sweet(msg, e){
            Swal.fire({
                icon: 'error',
                text: msg,
                customClass: 'swal-height'
            })   

            e.preventDefault();
        }

        function toast(msg){
            Swal.fire({
                text: msg,
                target: '#custom-target',
                // customClass: {
                //     container: 'position-absolute'
                // },
                toast: true,
                position: 'top-right'
            })
        }

        function availablity(){
            console.log({!! isset($transfer['available']) ? $transfer['available']: '[]' !!})
            return{
                details:{!! isset($transfer['available']) ? $transfer['available']: '[]' !!},
                discount:0,
                start_date:"",
                end_date:"",
                error_validate:false,
                error_message:'',
                rute:{!! isset($transfer['rute'])  ? $transfer['rute']: '[]' !!},
                addField(){

                    if($('#tgl_start').val()==''){
                        toast('Tanggal awal harus diisi')
                    }

                    if($('#tgl_end').val()==''){
                        toast('Tanggal akhir harus diisi')
                    }

                    if(this.rute.length == 0){
                        this.error_validate = true
                        this.error_message = 'Harga dan rute masih kosong'

                        return this.error_validate, this.error_message
                    }

                    this.details.push({
                        tgl_awal:$('#tgl_start').datepicker().value(),
                        tgl_akhir:$('#tgl_end').datepicker().value(),
                        diskon:this.discount,
                        rute:this.rute
                    })
                    console.log(this.details);
                    this.error_message = ''
                },
                remove(index){
                    this.details.splice(index, 1);
                }
            }
        }

        function ruteAvailablityPrice(price, discount){
            console.log(price, discount)
            let discount_price = parseInt(price) + (parseInt(price)*(parseInt(discount)/100))
            console.log(discount_price)

            return discount_price
        }
        /* $("#formRuteSchedule").on("submit",function(e){
            if(!$("#judul_rute").val()){
                sweet('Judul bus harap diisi!', e)
            }
            else if(!$("#pickup_point").val()){
                sweet('Pick up point harap diisi!', e)
            }
            // else if(!$("#naik_lat").val()){
            //     sweet('Longitude turun harap diisi!', e)
            // }
            else if(!$("#drop_point").val()){
                sweet('Longitude turun harap diisi!', e)
            }
            // else if(!$("#turun_long").val()){
            //     sweet(msg, e)
            // }
            else if(!$("#harga").val()){
                sweet('Harga harap diisi!', e)
            }
            // e.preventDefault()
        }) */
       
    </script>
</body>

</html>