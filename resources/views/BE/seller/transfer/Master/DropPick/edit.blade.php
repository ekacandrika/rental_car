<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">


    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])


    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>


    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);


        /**
       * tailwind.config.js
       * module.exports = {
       *   variants: {
       *     extend: {
       *       backgroundColor: ['active'],
       *     }
       *   },
       * }
       */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }


        #tur:checked+#tur {
            display: block;
        }


        #transfer:checked+#transfer {
            display: block;
        }


        #hotel:checked+#hotel {
            display: block;
        }


        #rental:checked+#rental {
            display: block;
        }


        #activity:checked+#activity {
            display: block;
        }


        #xstay:checked+#xstay {
            display: block;
        }
    </style>


    <!-- Styles -->
    @livewireStyles
</head>


<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>


    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>


            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>


        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-lg font-bold font-inter text-[#000000]">Tambah Drop Off - Pick Up</span>


                <div class="bg-[#FFFFFF] drop-shadow-xl py-5 mt-5 rounded">
                    <div class="max-w-md">
                        <div class="p-4">
                            <h1 class="text-xl font-bold mb-4">Edit Place</h1>
                            @if ($errors->any())
                            <div class="alert alert-danger " role="alert" id="error-message">
                                <div class="pt-3 px-2 text-white bg-red-400 rounded-lg">
                                    {{ $errors->first('error') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                </div>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li class="bg-kamtuu-third text-white p-2 rounded-lg w-3/4 m-2">{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <form action="{{ route('updateMasterDropPick',$item->id) }}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="mb-4">
                                    <label for="judulDropPick" class="block text-gray-700 font-bold mb-2">Judul Drop Off
                                        - Pick Up</label>
                                    <input value="{{old('judulDropPick',$item->judulDropPick)}}" type="text"
                                        name="judulDropPick" id="judulDropPick"
                                        class="border border-gray-400 p-2 w-full rounded focus:ring-kamtuu-second">
                                </div>
                                <div class="mb-4">
                                    <label for="latitude" class="block text-gray-700 font-bold mb-2">Latitude</label>
                                    <input value="{{old('latitude',$item->latitude)}}" type="text" name="latitude"
                                        id="latitude"
                                        class="border border-gray-400 p-2 w-full rounded focus:ring-kamtuu-second">
                                </div>
                                <div class="mb-4">
                                    <label for="longitude" class="block text-gray-700 font-bold mb-2">Longitude</label>
                                    <input value="{{old('longitude',$item->longitude)}}" type="text" name="longitude"
                                        id="longitude"
                                        class="border border-gray-400 p-2 w-full rounded focus:ring-kamtuu-second">
                                </div>
                                <button type="submit"
                                    class="bg-kamtuu-second hover:bg-kamtuu-second text-white font-bold py-2 px-4 rounded">
                                    Update
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="module">
        $(document).ready(function() {
       setTimeout(function() {
           $("#error-message").fadeOut("slow"); // Use fadeOut method to hide the alert box with animation after 2 seconds
       }, 2000);
   });
    </script>
</body>


</html>