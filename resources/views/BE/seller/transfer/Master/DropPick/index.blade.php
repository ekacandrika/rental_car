<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">


    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])


    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>


    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);


        /**
       * tailwind.config.js
       * module.exports = {
       *   variants: {
       *     extend: {
       *       backgroundColor: ['active'],
       *     }
       *   },
       * }
       */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }


        @keyframes blinker {
            50% {
                opacity: 0;
            }
        }


        .new-data {
            background-color: white;
            animation: blinker 1s linear infinite;
        }


        #tur:checked+#tur {
            display: block;
        }


        #transfer:checked+#transfer {
            display: block;
        }


        #hotel:checked+#hotel {
            display: block;
        }


        #rental:checked+#rental {
            display: block;
        }


        #activity:checked+#activity {
            display: block;
        }


        #xstay:checked+#xstay {
            display: block;
        }
    </style>


    <!-- Styles -->
    @livewireStyles
</head>


<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>


    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>


            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>


        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">




                {{-- modal delete --}}
                <div class="mt-64 fixed z-10 inset-0 overflow-y-auto hidden" id="delete-modal">
                    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                        <!-- Background overlay -->
                        <div class="fixed inset-0 transition-opacity" aria-hidden="true">
                            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
                        </div>
                        <!-- Modal panel -->
                        <div
                            class="inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full sm:p-6">
                            <div>
                                <div class="mt-3 text-center sm:mt-5">
                                    <h3 class="text-lg leading-6 font-medium text-gray-900" id="modal-title">
                                        Delete Attribute
                                    </h3>
                                    <div class="mt-2">
                                        <p class="text-sm text-gray-500">
                                            Apakah anda yakin untuk menghapus Tempat Jemput - Turun?
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-5 sm:mt-6 grid-cols-1 grid place-items-center">
                                <div class="grid-cols-2">
                                    <button type="button"
                                        class="btn btn-primary inline-flex items-center justify-center px-4 py-2 text-sm font-medium text-white bg-kamtuu-second border border-transparent rounded-md hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                        onclick="event.preventDefault(); deletePost();">
                                        Delete
                                    </button>
                                    <button type="button"
                                        class="btn btn-secondary inline-flex items-center justify-center px-4 py-2 ml-3 text-sm font-medium text-gray-700 bg-gray-100 border border-gray-300 rounded-md hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500"
                                        onclick="closeModal();">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <span class="text-lg font-bold font-inter text-[#000000]">Tambah Data Pick Up - Drop Off</span>
                <div class="gap-5">


                    <x-flash-message></x-flash-message>




                    <div class="bg-[#FFFFFF] drop-shadow-xl p-10 mt-5 rounded">


                        <div class="flex justify-between">
                            <p class="text-lg font-bold font-inter text-[#333333]">List Place</p>
                            <div class="">
                                <a type="button" href="{{ route('addMasterDropPick')}}"
                                    class="text-white bg-kamtuu-second hover:bg-kamtuu-third focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">+
                                    Tambah Drop Off - Pick Up</a>
                            </div>
                        </div>




                        <table class="w-full text-sm text-left text-[#333333] bg-[#D25889] dark:bg-[#D25889] mt-5">
                            <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                                <tr>
                                    {{-- <th class="px-6 py-3">No</th>--}}
                                    <th class="px-6 py-3">Nama Tempat</th>
                                    <th class="px-6 py-3">Latitude</th>
                                    <th class="px-6 py-3">Longitude</th>
                                    <th class="px-6 py-3"></th>
                                </tr>
                            </thead>




                            <tbody>
                                @php
                                $latestItemId = session('latestItemId');
                                @endphp


                                @forelse($data as $key => $item)
                                <tr id="{{ 'item-' . $item->id }}"
                                    class="bg-white border-b {{ $item->id === $latestItemId ? 'new-data' : '' }}">
                                    {{-- <td class="px-6 py-2">{{$key+1}}</td>--}}
                                    <td class="px-6 py-2">{{$item->judulDropPick}}</td>
                                    <td class="px-6 py-2">{{$item->longitude}}</td>
                                    <td class="px-6 py-2">{{$item->longitude}}</td>
                                    <td class="px-6 py-2">
                                        <div class="flex">
                                            <a href="{{route('editMasterDropPick', $item->id)}}">
                                                <button class="mx-2 p-2 rounded-lg bg-kamtuu-third text-white"><img
                                                        class="w-5"
                                                        src="{{ asset('storage/icons/pen-square-white.svg') }}"
                                                        alt=""></button>
                                            </a>


                                            <form method="POST" action="{{ route('deleteMasterDropPick', $item->id) }}"
                                                id="delete-form">
                                                @csrf
                                                @method('DELETE')
                                                <button class="p-2 rounded-lg bg-red-500 text-white"
                                                    onclick="event.preventDefault(); confirmDelete({{$item->id}});">
                                                    <img class="w-5" src="{{ asset('storage/icons/trash-white.png') }}"
                                                        alt="">
                                                </button>
                                            </form>
                                        </div>




                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-white uppercase text-center">
                                        tidak ada titik drop off atau pick up
                                    </td>
                                </tr>
                                @endforelse


                            </tbody>
                        </table>
                        <div class="mt-5">


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <script>
        var newData = document.querySelector('.new-data');
   if (newData) {
       var newDataId = newData.getAttribute('id').replace('item-', '');
       setTimeout(function () {
           newData.classList.remove('new-data');
           {!! session(['latestItemId' => null]) !!}
       }, 2000);
   }
    </script>


    {{--<script>
        --}}
{{--    var firstNewData = document.querySelector('.new-data');--}}
{{--    if (firstNewData) {--}}
{{--        var firstNewDataId = firstNewData.getAttribute('id').replace('item-', '');--}}
{{--        firstNewData.addEventListener('click', function() {--}}
{{--            firstNewData.classList.remove('new-data');--}}
{{--            var newDataIds = {!! json_encode($newDataIds) !!};--}}
{{--            var index = newDataIds.indexOf(firstNewDataId);--}}
{{--            if (index > -1) {--}}
{{--                newDataIds.splice(index, 1);--}}
{{--                {!! session(['newDataIds' => 'newDataIds']) !!}--}}
{{--            }--}}
{{--        });--}}
{{--    }--}}
{{--
    </script>--}}




    <script>
        function confirmDelete(id) {
       document.getElementById('delete-form').action = '/dashboard/seller/transfer/master-drop-pick/delete/' + id;
       document.getElementById('delete-modal').classList.remove('hidden');
   }


   function deletePost() {
       document.getElementById('delete-form').submit();
   }


   function closeModal() {
       document.getElementById('delete-modal').classList.add('hidden');
   }
    </script>
</body>


</html>