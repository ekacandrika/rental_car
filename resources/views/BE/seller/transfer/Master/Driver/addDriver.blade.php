<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-lg font-bold font-inter text-[#000000]">Tambah Driver</span>

                <div class="bg-[#FFFFFF] drop-shadow-xl py-5 mt-5 rounded">
                    <form
                        action="{{ isset($isedit) ? route('masterDriver.addDriver.edit', $id) : route('masterDriver.addDriver') }}"
                        method="POST" enctype="multipart/form-data" x-data="paketHandler()">
                        @csrf

                        <p class="text-lg font-bold font-inter text-[#333333] px-5 mb-5">Masukan Data Driver</p>
                        <div class="px-5 mb-5">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nama
                                Driver</label>
                            <input type="text" id="nama_driver" name="nama_driver" x-model="nama_driver"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5"
                                placeholder="Nama Driver"
                                value="{!! isset($driver['nama_driver']) ? $driver['nama_driver'] : '' !!}">
                        </div>
                        <div class="px-5 mb-5">
                            <label for="number"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nomor
                                Telepon</label>
                            <input type="number" id="no_telp" name="no_telp" x-model="no_telp"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5"
                                placeholder="Nomor Telepon"
                                value="{!! isset($driver['no_telp']) ? $driver['no_telp'] : '' !!}">
                        </div>
                        <div class="px-5 mb-5">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Alamat</label>
                            <textarea rows="4" name="alamat" x-model="alamat" id="alamat"
                                class="block p-2.5 w-96 text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500"
                                placeholder="Alamat">{!! isset($driver['alamat']) ? $driver['alamat'] : '' !!}</textarea>
                        </div>

                        <div class="p-5">
                            <div class="grid grid-cols-6">
                                <div class="col-end-8 col-end-2">
                                    <button type="submit"
                                        class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function paketHandler() {
            return {
                nama_driver: "{!! isset($driver['nama_driver']) ? $driver['nama_driver'] : "" !!}",
                no_telp: "{!! isset($driver['no_telp']) ? $driver['no_telp'] : "" !!}",
                alamat: "{!! isset($driver['alamat']) ? $driver['alamat'] : "" !!}",
            }
        }
    </script>
</body>

</html>