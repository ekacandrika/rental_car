<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-lg font-bold font-inter text-[#000000]">Tambah Data Driver</span>
                <div class="gap-5">
                    {{-- Mobil --}}
                    <div class="bg-[#FFFFFF] drop-shadow-xl p-10 mt-5 rounded">
                        <div class="flex justify-between">
                            <p class="text-lg font-bold font-inter text-[#333333]">List Driver</p>
                            <div class="">
                                <a type="button" href="{{ route('masterDriver.addDriver')}}"
                                    class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">+
                                    Tambah Driver</a>
                            </div>
                        </div>
                        <table class="w-full text-sm text-left text-[#333333] bg-[#D25889] dark:bg-[#D25889] mt-5">
                            <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                                <tr>
                                    <th class="px-6 py-3">No</th>
                                    <th class="px-6 py-3">Nama Driver</th>
                                    <th class="px-6 py-3">No Telepon</th>
                                    <th class="px-6 py-3">Alamat</th>
                                    <th class="px-6 py-3"> </th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($data as $k => $item)
                                <tr class="bg-white border-b">
                                    <td class="px-6 py-2">{{$k+1}}</td>
                                    <td class="px-6 py-2">{{$item->nama_driver}}</td>
                                    <td class="px-6 py-2">{{$item->no_telp}}</td>
                                    <td class="px-6 py-2">{{$item->alamat}}</td>
                                    <td class="px-6 py-2">
                                        <a type="button" href="edit/{{ $item->id_driver }}"
                                            class="text-white bg-yellow-500 hover:bg-yellow-600 duration-200 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-yellow-500 dark:hover:bg-yellow-600 dark:focus:ring-blue-800">
                                            <img src="{{ asset('storage/icons/pencil-solid.svg') }}" alt=""
                                                width="16px">
                                            <span class="sr-only">Icon description</span>
                                        </a>

                                        <a type="button" href="hapus/{{ $item->id_driver }}"
                                            class="text-white bg-red-500 hover:bg-red-600 duration-200 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-red-500 dark:hover:bg-red-600 dark:focus:ring-red-800">
                                            <img src="{{ asset('storage/icons/trash-can-regular-squared-size.svg') }}"
                                                alt="" width="16px">
                                            <span class="sr-only">Icon description</span>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="mt-5">
                            {{ $data->links() }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</body>

</html>