<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param||null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <div class="flex gap-2">
                    <button type="button"
                        class="text-[#333333] font-medium rounded-lg text-sm py-2 text-center inline-flex items-center mr-2 hover:text-[#23AEC1]">
                        <img src="{{ asset('storage/icons/chevron.svg') }}" alt="user-profile" class="w-5 h-5 mr-1">
                        <span class="text-lg font-bold font-inter text-[#333333]">Tambah Listing</span>
                    </button>
                </div>
                {{-- Breadcumbs --}}
                <nav class="flex mx-2.5" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            <a href="informasi"
                                class="inline-flex items-center text-sm font-bold font-inter text-[#23AEC1]">Detail
                                Kendaraan</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="rute"
                                    class="inline-flex items-center text-sm font-bold font-inter text-[#333333] hover:text-[#23AEC1]">Rute
                                    Harga dan Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="pilihan"
                                    class="inline-flex items-center text-sm font-bold font-inter text-[#333333] hover:text-[#23AEC1]">Pilihan
                                    dan Ekstra</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="pembayaran"
                                    class="inline-flex items-center text-sm font-bold font-inter text-[#333333] hover:text-[#23AEC1]">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                    </ol>
                </nav>

                <div class="flex gap-5 py-5">
                    <div class="flex items-center py-px">
                        <input id="default-checkbox" type="checkbox" value="checked"
                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                        <label for="default-checkbox" class="ml-2 text-sm font-semibold text-[#333333]">Transfer Umum
                            (Schedule)</label>
                    </div>
                    <div class="flex items-center py-px">
                        <input checked id="checked-checkbox" type="checkbox" value=""
                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                        <label for="checked-checkbox" class="ml-2 text-sm font-semibold text-[#333333]">Transfer Private
                            (Non Schedule)</label>
                    </div>
                </div>

                <div class="bg-[#FFFFFF] drop-shadow-xl p-6 px-10 rounded">
                    <div class="grid grid-cols-2">
                        <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Detail Kendaraan</div>
                        <div class="text-sm font-bold font-inter p-5 text-[#333333] text-right">ID Listing: 1TO101082022
                        </div>
                    </div>

                    <form>
                        <div class="px-5 mb-5">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Judul
                                Listing</label>
                            <input type="text" id="text"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Mobil A">
                        </div>

                        <div class="block px-5 mb-5 flex gap-5">
                            <div>
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Jenis
                                    Kendaraan</label>
                                <select
                                    class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                    <option>Sedan</option>
                                    <option>Option 1</option>
                                    <option>Option 2</option>
                                    <option>Option 3</option>
                                </select>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                            </div>
                            <div>
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-white dark:text-gray-300">Jenis
                                    Kendaraan</label>
                                <select
                                    class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                    <option>Toyota</option>
                                    <option>Option 1</option>
                                    <option>Option 2</option>
                                    <option>Option 3</option>
                                </select>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                            </div>
                        </div>

                        <div class="px-5 mb-5">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nama
                                Kendaraan</label>
                            <input type="text" id="text"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Nama Kendaraan">
                        </div>

                        <div class="grid grid-cols-2">
                            <div class="flex mb-5">
                                <div class="px-5">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kapasitas
                                        Kursi</label>
                                    <input type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Kapasitas Kursi">
                                </div>

                                <div class="pr-5">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kapasitas
                                        Koper</label>
                                    <input type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Kapasitas Koper">
                                </div>
                            </div>
                        </div>

                        <div class="px-5 mb-5">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Tambah
                                Gambar</label>
                            <div x-data="displayImage()">
                                <div class="mb-2">
                                    <input class="my-2" type="file" accept="image/*" @change="selectedFile">
                                    <template x-if="imageUrl">
                                        <img :src="imageUrl"
                                            class="object-contain rounded border border-gray-300 w-full h-[300px]">
                                    </template>

                                    <div class="flex gap-5">
                                        <template x-if="!imageUrl">
                                            <div
                                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-48 h-48">
                                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                    width="20px" height="20px">
                                            </div>
                                        </template>

                                        <template x-if="!imageUrl">
                                            <div
                                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-48 h-48">
                                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                    width="20px" height="20px">
                                            </div>
                                        </template>

                                        <template x-if="!imageUrl">
                                            <div
                                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-48 h-48">
                                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                    width="20px" height="20px">
                                            </div>
                                        </template>

                                        <template x-if="!imageUrl">
                                            <div
                                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-48 h-48">
                                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                    width="20px" height="20px">
                                            </div>
                                        </template>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="px-5">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Deskripsi</label>
                        </div>
                        <div class="min-w-screen min-h-fit items-center justify-center">
                            <div class="w-full max-w-6xl mx-auto rounded-xl px-5 text-black" x-data="app()"
                                x-init="init($refs.wysiwyg)">
                                <div class="border border-gray-200 overflow-hidden rounded-md">
                                    <div class="w-full flex border-b border-gray-200 text-xl text-gray-600">
                                        <button
                                            class="outline-none focus:outline-none border-l border-r border-gray-200 w-10 h-10 hover:text-indigo-500 active:bg-gray-50"
                                            @click="format('insertUnorderedList')">
                                            <i class="mdi mdi-format-list-bulleted"></i>
                                        </button>
                                        <button
                                            class="outline-none focus:outline-none border-r border-gray-200 w-10 h-10 mr-1 hover:text-indigo-500 active:bg-gray-50"
                                            @click="format('insertOrderedList')">
                                            <i class="mdi mdi-format-list-numbered"></i>
                                        </button>
                                    </div>
                                    <div class="w-full">
                                        <iframe x-ref="wysiwyg" class="w-full h-24 overflow-y-auto"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="px-5 pt-5">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Komplemen</label>
                        </div>
                        <x-be.com.wysiwyg></x-be.com.wysiwyg>

                        <div class="px-5 pt-5">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Pilihan
                                Tambahan</label>
                        </div>
                        <x-be.com.wysiwyg></x-be.com.wysiwyg>

                        <div class="text-sm font-bold font-inter px-5 mt-5 text-[#333333]">Tambah Layout Kursi</div>

                        <div class="block px-5 mb-5 flex gap-5">
                            <div>
                                <select
                                    class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                    <option>-- Pilih Lantai --</option>
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                            </div>
                            <div>
                                <select
                                    class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                    <option>-- Layout Kursi --</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                            </div>
                        </div>

                        <div class="grid grid-cols-2 p-5">
                            <div class="border-2 border-[#333333] rounded-md">
                                <div class="p-5">
                                    <p class="font-bold mb-2.5">Lantai 1</p>
                                    <div class="grid grid-cols-8 gap-5">
                                        <div class="grid grid-rows-2 gap-2">
                                            <div class="flex gap-2">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="1">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                            </div>
                                            <div class="flex gap-2 pb-2.5">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                            </div>
                                            <div class="flex gap-2 pt-2.5">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                            </div>
                                            <div class="flex gap-2">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                                <input class="numberstyle w-8 rounded border border-gray-400 px-2.5"
                                                    min="1" step="1" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="px-5">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Harga Umum
                                    Kursi</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Rp. 25.000">

                                <div class="flex items-center py-5">
                                    <input checked id="default-checkbox" type="checkbox" value=""
                                        class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                    <label for="default-checkbox" class="ml-2 text-sm font-semibold text-[#333333]">Buat
                                        harga custom </label>
                                </div>
                            </div>
                        </div>

                        <div class="text-sm font-bold font-inter px-5 mt-5 text-[#333333]">Harga Custom Kursi</div>
                        <div class="px-5">
                            <select
                                class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-20 rounded-md text-sm focus:ring-1">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>
                        </div>
                        <div class="px-5 mt-2.5 mb-5">
                            <input type="text" id="text"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Rp. 35.000">
                        </div>

                        <x-be.com.two-button></x-be.com.two-button>

                    </form>

                </div>
            </div>
        </div>
    </div>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                if (! event.target.files.length) return

                let file = event.target.files[0],
                    reader = new FileReader()

                reader.readAsDataURL(file)
                reader.onload = e => callback(e.target.result)
                },
            }
        }
    </script>
</body>

</html>