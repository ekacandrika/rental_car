<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        [x-cloak] {
            display: none
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-lg font-bold font-inter text-[#000000]">Tambah Kamar</span>
                <div class="gap-5">
                    {{-- Mobil --}}
                    <div class="bg-[#FFFFFF] drop-shadow-xl p-10 mt-5 rounded">
                        <div class="flex justify-between">
                            <p class="text-lg font-bold font-inter text-[#333333]">Data Kamar</p>
                            <div class="">
                                <a type="button" href="/hotel/kamar/tambah"
                                    class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">+
                                    Tambah Kamar</a>
                            </div>
                        </div>
                        <table class="w-full text-sm text-left text-[#333333] bg-[#D25889] dark:bg-[#D25889] mt-5">
                            <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                                <tr>
                                    <th class="px-6 py-3">No</th>
                                    <th class="px-6 py-3">Kode Kamar</th>
                                    <th class="px-6 py-3">Jenis Kamar</th>
                                    <th class="px-6 py-3">Stok Kamar</th>
                                    <th class="px-6 py-3">Nama Kamar</th>
                                    <th class="px-6 py-3">Jenis Produk</th>
                                    <th class="px-6 py-3"></th>
                                </tr>
                            </thead>

                            <tbody>
                                @forelse ($rooms as $key => $room)
                                <tr class="bg-white border-b hover:bg-slate-100">
                                    <td class="px-6 py-2">{{ $rooms->firstItem() + $key }}</td>
                                    <td class="px-6 py-2">{{ $room->kode_kamar }}</td>
                                    <td class="px-6 py-2">{{ $room->jenis_kamar }}</td>
                                    <td class="px-6 py-2">{{ $room->stok }}</td>
                                    <td class="px-6 py-2">{{ $room->nama_kamar }}</td>
                                    <td class="px-6 py-2">{{ $room->jenis_produk }}</td>
                                    <td class="flex px-6 py-2">
                                        <form action="{{ route('kamarhotel.clone', $room->id) }}" method="POST">
                                            @csrf

                                            <button title="Salin"
                                                class="text-white bg-blue-500 hover:bg-blue-600 duration-200 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-red-500 dark:hover:bg-red-600 dark:focus:ring-red-800">
                                                <img src="{{ asset('storage/icons/copy.svg') }}" alt="" width="16px"
                                                    height="16px">
                                                <span class="sr-only">Icon description</span>
                                            </button>
                                        </form>

                                        <a type="button" href="{{ route('kamarhotel.view.edit', $room->id) }}"
                                            title="Edit"
                                            class="text-white bg-yellow-500 hover:bg-yellow-600 duration-200 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-yellow-500 dark:hover:bg-yellow-600 dark:focus:ring-blue-800">
                                            <img class="fill-white" src="{{ asset('storage/icons/pencil-solid.svg') }}"
                                                alt="" width="16px">
                                            <span class="sr-only">Icon description</span>
                                        </a>

                                        <a x-data="showModal" @keydown.escape="show = false" type="button"
                                            title="Hapus">
                                            <button @click="show = true" type="button"
                                                class="text-white bg-red-500 hover:bg-red-600 duration-200 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-red-500 dark:hover:bg-red-600 dark:focus:ring-red-800">
                                                <img src="{{ asset('storage/icons/trash-can-regular-squared-size.svg') }}"
                                                    alt="" width="16px" height="16px">
                                                <span class="sr-only">Icon description</span>
                                            </button>
                                            {{-- <button
                                                @click="$store.show.show = true; $store.show.room = {{ $room }}; console.log($store.show.room)"
                                                type="button"
                                                class="text-white bg-red-500 hover:bg-red-600 duration-200 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-red-500 dark:hover:bg-red-600 dark:focus:ring-red-800">
                                                <img src="{{ asset('storage/icons/trash-can-regular-squared-size.svg') }}"
                                                    alt="" width="16px" height="16px">
                                                <span class="sr-only">Icon description</span>
                                            </button> --}}

                                            <x-modal-delete-hotel-room :room="$room" />
                                        </a>

                                    </td>
                                </tr>
                                @empty

                                @endforelse

                                {{-- @foreach ($data as $k => $item)
                                <tr class="bg-white border-b">
                                    <td class="px-6 py-2">{{ $k + 1 }}</td>
                                    <td class="px-6 py-2">{{ $item->jenismobil->jenis_kendaraan }}</td>
                                    <td class="px-6 py-2">{{ $item->merek_mobil }}</td>
                                    <td class="px-6 py-2">{{ $item->plat_mobil }}</td>
                                    <td class="px-6 py-2">
                                        <a type="button" href="edit/{{ $item->id }}"
                                            class="text-white bg-yellow-500 hover:bg-yellow-600 duration-200 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-yellow-500 dark:hover:bg-yellow-600 dark:focus:ring-blue-800">
                                            <img class="fill-white" src="{{ asset('storage/icons/pencil-solid.svg') }}"
                                                alt="" width="16px">
                                            <span class="sr-only">Icon description</span>
                                        </a>

                                        <a type="button" href="hapus/{{ $item->id }}"
                                            class="text-white bg-red-500 hover:bg-red-600 duration-200 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-red-500 dark:hover:bg-red-600 dark:focus:ring-red-800">
                                            <img src="{{ asset('storage/icons/trash-can-regular-squared-size.svg') }}"
                                                alt="" width="16px" height="16px">
                                            <span class="sr-only">Icon description</span>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach --}}
                            </tbody>
                        </table>
                        <div class="mt-5">
                            {{ $rooms->links() }}
                        </div>
                    </div>

                    {{-- Bus --}}
                    {{-- <div class="bg-[#FFFFFF] drop-shadow-xl p-10 mt-5 rounded">
                        <div class="flex justify-between">
                            <p class="text-lg font-bold font-inter text-[#333333]">Data Bus</p>
                            <div class="">
                                <a type="button" href="{{ route('masterMobil.viewAddJenisBus') }}"
                                    class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">+
                                    Jenis Bus</a>
                                <a type="button" href="{{ route('masterMobil.viewAddMerekBus') }}"
                                    class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">+
                                    Merek Bus</a>
                                <a type="button" href="{{ route('masterBus') }}"
                                    class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">+
                                    Layout Bus</a>
                            </div>
                        </div>
                        <table class="w-full text-sm text-left text-[#333333] bg-[#D25889] dark:bg-[#D25889] mt-5">
                            <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                                <tr>
                                    <th class="px-6 py-3">No</th>
                                    <th class="px-6 py-3">Jenis Bus</th>
                                    <th class="px-6 py-3">Merek Bus</th>
                                    <th class="px-6 py-3">Plat Bus</th>
                                    <th class="px-6 py-3"> </th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($bus as $b => $item)
                                <tr class="bg-white border-b">
                                    <td class="px-6 py-2">{{ $b + 1 }}</td>
                                    <td class="px-6 py-2">{{ $item->jenis_kendaraan }}</td>
                                    <td class="px-6 py-2">{{ $item->merek_bus }}</td>
                                    <td class="px-6 py-2">{{ $item->plat_bus }}</td>
                                    <td class="px-6 py-2">
                                        <a type="button" href="edit-bus/{{ $item->id_merek }}"
                                            class="text-white bg-yellow-500 hover:bg-yellow-600 duration-200 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-yellow-500 dark:hover:bg-yellow-600 dark:focus:ring-blue-800">
                                            <img src="{{ asset('storage/icons/pencil-solid.svg') }}" alt=""
                                                width="16px">
                                            <span class="sr-only">Icon description</span>
                                        </a>

                                        <a type="button" href="hapus-bus/{{ $item->id_merek }}"
                                            class="text-white bg-red-500 hover:bg-red-600 duration-200 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-red-500 dark:hover:bg-red-600 dark:focus:ring-red-800">
                                            <img src="{{ asset('storage/icons/trash-can-regular-squared-size.svg') }}"
                                                alt="" width="16px">
                                            <span class="sr-only">Icon description</span>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="mt-5">
                            {{ $data->links() }}
                        </div>
                    </div> --}}

                    {{-- Modal --}}
                    {{-- <div x-data>
                        <div x-cloak x-show="$store.show.show" class="fixed inset-0 z-[9999] overflow-y-auto"
                            aria-labelledby="modal-title" role="dialog" aria-modal="true">
                            <div
                                class="flex items-end justify-center min-h-screen text-center md:items-center sm:block sm:p-0">
                                <div class="fixed inset-0 duration-200" @click="$store.show.show = false"
                                    style="background-color: rgba(0,0,0,.7);">
                                </div>

                                <div x-cloak x-show="$store.show.show"
                                    x-transition:enter="transition ease-out duration-300 transform"
                                    x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                                    x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
                                    x-transition:leave="transition ease-in duration-200 transform"
                                    x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
                                    x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                                    class="inline-block w-full max-w-xl p-8 my-20 overflow-hidden text-left transition-all transform bg-white rounded-lg shadow-xl 2xl:max-w-2xl">
                                    <div class="flex items-center justify-end space-x-4">
                                        <button @click="$store.show.show = false"
                                            class="text-gray-600 focus:outline-none hover:text-gray-700">
                                            <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                                        </button>
                                    </div>
                                    <h1 class="text-xl font-bold text-black uppercase">Hapus {{ isset($room->nama_kamar)
                                        ?
                                        $room->nama_kamar .
                                        ' - ' . $room->kode_kamar : $room->kode_kamar }}</h1>
                                    <div class="text-md text-gray-700 my-5 text-center space-y-2">
                                        <p>Apakah Anda yakin menghapus kamar <span class="font-bold">{{
                                                isset($room->nama_kamar) ?
                                                $room->nama_kamar . ' - ' . $room->kode_kamar : $room->kode_kamar
                                                }}</span>?
                                        </p>
                                        <p>Kamar yang sudah dihapus tidak dapat dikembalikan.</p>
                                    </div>
                                    <div class="flex justify-end space-x-2">
                                        <form action="{{ route('kamarhotel.delete', $room->id) }}" method="POST">
                                            @csrf
                                            @method('delete')
                                            <button type="submit"
                                                class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">
                                                Hapus
                                            </button>
                                        </form>
                                        <button @click="$store.show.show = false"
                                            class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-[#D50006] bg-[#white] border border-[#D50006] hover:bg-slate-100 duration-200 rounded lg:rounded-lg">
                                            Batal
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}

                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.store('show', {
                show: false,
                room: ''
            })
        })

        function showModal() {
            return {
                show: false
            }
        }
    </script>
</body>

</html>