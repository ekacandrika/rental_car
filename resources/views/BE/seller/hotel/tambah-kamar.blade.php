<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js" defer></script>

    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }

        .select2-container .select2-selection--single {
            height: 100%;
            /* padding-left: 0.75rem; */
            /* padding-right: 0.75rem; */
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 100%;
        }

        .select2-selection__arrow {
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            /* padding-right: 0.75rem; */
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-lg font-bold font-inter text-[#000000]">Tambah Kamar</span>

                <div class="bg-[#FFFFFF] drop-shadow-xl p-10 mt-5 rounded">
                    <form action="{{ route('kamarhotel.tambah') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="grid space-y-5" x-data="kamarHandler">
                            {{-- Jenis Product --}}
                            <div class="space-y-1" x-data="{ product_type: 'hotel' }">
                                <label class="block text-sm font-bold text-[#333333] dark:text-gray-300"
                                    for="product_type">Jenis Produk</label>
                                <div class="flex space-x-5">
                                    <div>
                                        <input type="radio" name="product_type" id="product_type" value="hotel"
                                            x-model="product_type">
                                        <label for="product_type">Hotel</label>
                                    </div>

                                    <div>
                                        <input type="radio" name="product_type" id="product_type" value="xstay"
                                            x-model="product_type">
                                        <label for="product_type">Xstay</label>
                                    </div>
                                </div>

                                @error('product_type')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                            </div>

                            {{-- Nama Kamar --}}
                            <div class="space-y-1">
                                <label class="block text-sm font-bold text-[#333333] dark:text-gray-300"
                                    for="room_name">Nama kamar</label>
                                <input id="room_name" name="room_name"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5"
                                    value="{{ old('room_name') }}" type="text" placeholder="Masukkan nama kamar"
                                    required>
                                @error('room_name')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                            </div>

                            {{-- Kode Kamar --}}
                            <div class="space-y-1">
                                <label class="block text-sm font-bold text-[#333333] dark:text-gray-300"
                                    for="room_code">Kode Kamar</label>
                                <input id="room_code" name="room_code"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5"
                                    value="{{ old('room_code') }}" type="text" placeholder="Masukkan kode kamar"
                                    required>
                                @error('room_code')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                            </div>

                            {{-- Jenis Kamar --}}
                            <div class="space-y-1">
                                <label class="block text-sm font-bold text-[#333333] dark:text-gray-300"
                                    for="room_category">Jenis Kamar</label>
                                <input id="room_category" name="room_category"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5"
                                    value="{{ old('room_category') }}" type="text" placeholder="Masukkan jenis kamar"
                                    required>
                                @error('room_category')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                            </div>

                            {{-- Kapasitas Kamar --}}
                            <div class="space-y-1">
                                <label class="block text-sm font-bold text-[#333333] dark:text-gray-300"
                                    for="room_category">Kapasitas Kamar (orang)</label>
                                <div class="flex space-x-8">
                                    <div>
                                        <label class="block text-xs font-normal text-[#333333] dark:text-gray-300"
                                            for="min_capacity">Min kapasitas</label>
                                        <input id="min_capacity" name="min_capacity"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5"
                                            value="{{ old('min_capacity') }}" type="text"
                                            placeholder="Masukkan min kapasitas" required>
                                    </div>

                                    <div>
                                        <label class="block text-xs font-normal text-[#333333] dark:text-gray-300"
                                            for="max_capacity">Maks kapasitas</label>
                                        <input id="max_capacity" name="max_capacity"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5"
                                            value="{{ old('max_capacity') }}" type="text"
                                            placeholder="Masukkan max kapasitas" required>
                                    </div>

                                </div>

                                @error('min_capacity')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                                @error('max_capacity')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                            </div>

                            {{-- Harga Kamar --}}
                            <div class="space-y-1" x-data="{ price_type: 'price_per_room' }">
                                <label class="block text-sm font-bold text-[#333333] dark:text-gray-300"
                                    for="room_price">Harga Kamar</label>
                                <div class="flex space-x-5">
                                    <div>
                                        <input type="radio" name="price_type" id="price_type" value="price_per_room"
                                            x-model="price_type">
                                        <label for="price_type">Harga per kamar</label>
                                    </div>

                                    <div>
                                        <input type="radio" name="price_type" id="price_type" value="price_per_person"
                                            x-model="price_type">
                                        <label for="price_type">Harga per orang</label>
                                    </div>
                                </div>

                                <input id="room_price" name="room_price"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5"
                                    value="{{ old('room_price') }}" type="number" min="0"
                                    placeholder="Masukkan harga kamar" required>
                                @error('room_price')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                            </div>

                            {{-- Stok Kamar --}}
                            <div class="space-y-1">
                                <label class="block text-sm font-bold text-[#333333] dark:text-gray-300"
                                    for="room_stock">Stok Kamar</label>
                                <input id="room_stock" name="room_stock"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5"
                                    value="{{ old('room_stock') }}" type="number" min="0"
                                    placeholder="Masukkan stok kamar" required>
                                @error('room_stock')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                            </div>

                            {{-- Refundable --}}
                            <div class="grid space-y-1 w-72" x-data="{ refundable: ['full'] }">
                                <label class="block text-sm font-bold text-[#333333] dark:text-gray-300"
                                    for="room_stock">Refundable</label>
                                <div class="space-x-1">
                                    <input type="checkbox" name="refundable[]" id="refundable_non" value="non"
                                        x-model="refundable" class="rounded-md border-solid border-gray-500">
                                    <label for="refundable_non">Non Refundable</label>
                                </div>
                                <template x-if="refundable.includes('non')">
                                    <div class="">
                                        <div class="">
                                            {{-- <label class="text-sm" for="refundable_price">Harga Refundable</label>
                                            --}}
                                            <input
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block"
                                                type="hidden" value="0" min="0" name="refundable_price[]"
                                                id="refundable_price" placeholder="Masukkan harga refundable">
                                        </div>
                                        <input
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-24 p-2.5"
                                            type="hidden" value="0" name="refundable_amount[]" id="refundable_amount">
                                    </div>
                                </template>
                                <div class="space-x-1">
                                    <input type="checkbox" name="refundable[]" id="refundable_full" value="full"
                                        x-model="refundable" class="rounded-md border-solid border-gray-500">
                                    <label for="refundable_full">Refundable penuh (100%)</label>
                                </div>
                                <template x-if="refundable.includes('full')">
                                    <div class="ml-6">
                                        <div class="mb-2">
                                            <label class="text-sm" for="refundable_price">Harga Refundable</label>
                                            <input
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block"
                                                type="number" min="0" name="refundable_price[]" id="refundable_price"
                                                placeholder="Masukkan harga refundable">
                                        </div>
                                        <input
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-24 p-2.5"
                                            type="hidden" value="100" name="refundable_amount[]" id="refundable_amount">
                                    </div>
                                </template>
                                <div class="space-x-1">
                                    <input type="checkbox" name="refundable[]" id="refundable_partial" value="partial"
                                        x-model="refundable" class="rounded-md border-solid border-gray-500">
                                    <label for="refundable_partial">Refundable sebagian</label>
                                </div>
                                <template x-if="refundable.includes('partial')">
                                    <div class="ml-6 flex space-x-3">
                                        <div>
                                            <label class="text-sm" for="refundable_amount">Persen Refundable</label>
                                            <input
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block"
                                                type="number" min="0" max="100" name="refundable_amount[]"
                                                id="refundable_amount" placeholder="Masukkan jumlah refundable">

                                        </div>
                                        <div>
                                            <label class="text-sm" for="refundable_price">Harga Refundable</label>
                                            <input
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block"
                                                type="number" min="0" name="refundable_price[]" id="refundable_price"
                                                placeholder="Masukkan harga refundable">
                                        </div>
                                    </div>
                                </template>
                                @error('refundable')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                                @error('refundable_amount.*')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                                @error('refundable_price.*')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                                @error('refundable_amount')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                                @error('refundable_price')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                            </div>

                            {{-- Gallery Kamar --}}
                            <div class="space-y-1">
                                <label class="block text-sm font-bold text-[#333333] dark:text-gray-300"
                                    for="extra_hotel_label">Gallery Kamar</label>
                                <div x-data="displayImage()">
                                    <input class="py-2" type="file" accept="image/*" id="images_gallery"
                                        @change="selectedFile" multiple>
                                    <template x-if="images.length < 1">
                                        <div
                                            class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                            <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                width="20px" height="20px">
                                        </div>
                                    </template>
                                    <template x-if="images.length >= 1">
                                        <div class="flex flex-wrap gap-5">
                                            <template x-for="(image, index) in images" :key="index">
                                                <div class="flex justify-center items-center rounded">
                                                    <img :src="image"
                                                        class="object-contain rounded border border-gray-300 w-[154px] h-[154px] hover:opacity-70 duration-200"
                                                        :alt="'upload'+index">
                                                    <button class="absolute mx-2 translate-x-12 -translate-y-14"
                                                        type="button">
                                                        <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                            alt="delete-button" width="25px"
                                                            @click="removeImage(index)">
                                                    </button>
                                                </div>
                                            </template>
                                        </div>
                                    </template>
                                </div>

                                @error('image_gallery')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror

                                <div id="error-gallery"
                                    class="hidden bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2">
                                </div>
                            </div>

                            {{-- Ekstra --}}
                            <div class="space-y-3">
                                <label class="block text-sm font-bold text-[#333333] dark:text-gray-300"
                                    for="extra_hotel_label">Ekstra Kamar</label>
                                <template x-if="extras.length === 0">
                                    <button
                                        class="bg-[#9E3D64] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#b94875] duration-200"
                                        type="button" @click="addNewExtra">Tambah Ekstra</button>
                                </template>
                                <template x-if="extras.length > 0" :data-length="extras.length">
                                    <template x-for="(extra, index) in extras" :key="index">
                                        <div class="flex space-x-2">
                                            <div class="space-y-1">
                                                <label class="block text-xs font-bold text-[#333333] dark:text-gray-300"
                                                    for="extra_name">Nama Ekstra</label>
                                                <input id="extra_name" name="extra_name[]"
                                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5"
                                                    type="text" placeholder="Masukkan nama extra"
                                                    x-model="extra.extra_name" required>
                                            </div>

                                            <div class="space-y-1">
                                                <label class="block text-xs font-bold text-[#333333] dark:text-gray-300"
                                                    for="extra_price">Harga Ekstra</label>
                                                <input id="extra_price" name="extra_price[]"
                                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5"
                                                    type="number" min="0" placeholder="Masukkan harga ekstra"
                                                    x-model="extra.extra_price" required>
                                            </div>
                                            <div class="flex justify-center">
                                                <button class="mx-1" type="button" @click="removeExtra(index)">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24">
                                                        <path fill="currentColor"
                                                            d="M12 20c-4.41 0-8-3.59-8-8s3.59-8 8-8s8 3.59 8 8s-3.59 8-8 8m0-18A10 10 0 0 0 2 12a10 10 0 0 0 10 10a10 10 0 0 0 10-10A10 10 0 0 0 12 2M7 13h10v-2H7" />
                                                    </svg>
                                                </button>
                                                <template x-if="index === extras.length - 1">
                                                    <button class="mx-1" type="button" @click="addNewExtra">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24">
                                                            <path fill="currentColor"
                                                                d="M12 20c-4.41 0-8-3.59-8-8s3.59-8 8-8s8 3.59 8 8s-3.59 8-8 8m0-18A10 10 0 0 0 2 12a10 10 0 0 0 10 10a10 10 0 0 0 10-10A10 10 0 0 0 12 2m1 5h-2v4H7v2h4v4h2v-4h4v-2h-4V7Z" />
                                                        </svg>
                                                    </button>
                                                </template>
                                            </div>
                                        </div>
                                    </template>
                                </template>
                                @error('extra_name')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                                @error('extra_price')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                            </div>

                            {{-- Amenitas --}}
                            <div class="space-y-5" x-init="$nextTick(() => { select2WithAlpine() })">
                                <label class="block text-sm font-bold text-[#333333] dark:text-gray-300"
                                    for="amenitas_hotel_label">Amenitas Kamar</label>
                                <div>
                                    <select name="attributes" id="attributes" x-model="attribute" x-ref="select"
                                        required
                                        class="attributes mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled value="0">Pilih Amenitas</option>
                                        @foreach ($attributes as $item)
                                        <option value="{{ $item->id }}" data-name="{{ $item->text }}"
                                            data-img="{{ Storage::url($item->image) }}">{{ $item->text }}</option>
                                        @endforeach
                                    </select>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>
                                </div>
                                <button
                                    class="bg-[#9E3D64] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#b94875] duration-200"
                                    type="button" @click="addNewAmenitas">Tambah Amenitas
                                </button>
                                <template x-if="error_amenitas === true">
                                    <div x-init="setTimeout(() => error_amenitas = false, 3000)"
                                        class="bg-red-300 rounded-md p-3 my-2 w-fit">
                                        <p x-text="error_empty_amenitas"></p>
                                    </div>
                                </template>
                                <template x-if="amenities.length > 0" :data-length="amenities.length">
                                    <div
                                        class="w-fit min-h-fit max-h-96 overflow-y-auto rounded-md border border-slate-200">
                                        <label
                                            class="sticky top-0 px-2 py-1 block text-base font-bold text-[#333333] dark:text-gray-300 bg-slate-300 rounded-t-md"
                                            for="amenitas_name">Icon Amenitas</label>
                                        <template x-for="(amenitas, index) in amenities" :key="index">
                                            <div class="my-1">
                                                <div
                                                    class="flex gap-3 hover:bg-slate-100 rounded-md w-fit p-1 items-center">
                                                    <img id="icon_amenitas_img"
                                                        class="ic_amenitas border flex justify-center items-center rounded border-gray-300 p-1"
                                                        :src="amenitas.amenitas_img" alt="icon_amenitas" width="42px"
                                                        height="42px">
                                                    <div class="space-y-1">
                                                        <div class="flex">
                                                            <input id="amenitas_id" name="amenitas_id[]" type="hidden"
                                                                x-model="amenitas.amenitas_id" required>
                                                            <p id="amenitas_name"
                                                                class=" text-gray-900 text-base rounded-lg block break-words w-96 p-2.5"
                                                                x-text="amenitas.amenitas_name"></p>
                                                            <div class="flex justify-center">
                                                                <button class="mx-1" type="button"
                                                                    @click="removeAmenitas(index)">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                        height="24" viewBox="0 0 24 24">
                                                                        <path fill="currentColor"
                                                                            d="M12 20c-4.41 0-8-3.59-8-8s3.59-8 8-8s8 3.59 8 8s-3.59 8-8 8m0-18A10 10 0 0 0 2 12a10 10 0 0 0 10 10a10 10 0 0 0 10-10A10 10 0 0 0 12 2M7 13h10v-2H7" />
                                                                    </svg>
                                                                </button>
                                                                {{-- <template x-if="index === amenities.length - 1">
                                                                    <button class="mx-1" type="button"
                                                                        @click="addNewAmenitas">
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                            width="24" height="24" viewBox="0 0 24 24">
                                                                            <path fill="currentColor"
                                                                                d="M12 20c-4.41 0-8-3.59-8-8s3.59-8 8-8s8 3.59 8 8s-3.59 8-8 8m0-18A10 10 0 0 0 2 12a10 10 0 0 0 10 10a10 10 0 0 0 10-10A10 10 0 0 0 12 2m1 5h-2v4H7v2h4v4h2v-4h4v-2h-4V7Z" />
                                                                        </svg>
                                                                    </button>
                                                                </template> --}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </template>
                                    </div>
                                </template>
                                @error('amenitas_id')
                                <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                    <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                            </div>

                            <div class="flex mt-5 justify-end">
                                <button
                                    class="bg-[#23AEC1] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#21bbd0] duration-200"
                                    type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function kamarHandler() {

            return {
                extras: [],
                amenities: [],
                attribute: 0,
                attribute_id: 0, 
                attribute_name:'', 
                attribute_img:'',
                error_empty_amenitas: '',
                error_amenitas: false,
                addNewExtra() {
                    this.extras.push({
                        extra_name: '',
                        extra_price: '',
                    });
                },
                removeExtra(index) {
                    this.extras.splice(index, 1);
                },
                addNewAmenitas() {

                    if (this.attribute_id === 0) {
                        this.error_amenitas = true
                        this.error_empty_amenitas = 'Amenitas wajib diisi!'
                        return this.error_empty_amenitas, this.error_amenitas
                    }

                    this.amenities.push({
                        amenitas_id: this.attribute_id,
                        amenitas_name: this.attribute_name,
                        amenitas_img: this.attribute_img
                    });
                    
                    this.error_empty_amenitas = ''
                },
                removeAmenitas(index) {
                    this.amenities.splice(index, 1);
                },
                select2WithAlpine() {
                    this.select2 = $(this.$refs.select).select2({
                        placeholder: "Pilih Amenitas",
                        // allowClear: true
                    });
                    this.select2.on("select2:select", (event) => {
                        this.attribute_id = event.target.value;
                        this.attribute_name = this.select2.find(":selected").data("name");
                        this.attribute_img = this.select2.find(":selected").data("img");
                    });
                    this.$watch("attribute", (value) => {
                        this.select2.val(value).trigger("change");
                    });
                    
                }
            }
        }
    </script>

    <script>
        let data = {!! isset($detail->foto_maps_1) ? $detail->foto_maps_1 : "[]" !!};
        let imagesGallery = [];
    
        document.addEventListener("alpine:init", () => {
            Alpine.store("dataAvailable", {
                    isDataAvailable: data.length > 0 ? true : false,
            });
        });

        if (data.length > 0) {
            const url = window.location.origin
            let array = data
            array.forEach(element => {
                imagesGallery.push(url+'/'+element)
            });
        }
        // Show error toast
        function showToastError(message) {
            console.log(message)
            document.getElementById("error-gallery").classList.remove("hidden");
            document.getElementById("error-gallery").classList.add("flex");
            $(`<span id="gallery-error-message">${message}</span>`).appendTo('#error-gallery');
            // Hide the gallery after 5 seconds
            setTimeout(function () {
                document.getElementById("error-gallery").classList.add("hidden");
                $(".message-error").remove();
                document.getElementById("error-gallery").classList.remove("flex");
                document.getElementById("gallery-error-message").remove();
            }, 5000);
        }

        // // Show success toast
        // function showToastSuccess(message) {
        //     document.getElementById("success-toast").classList.remove("hidden");
        //     document.getElementById("success-toast").classList.add("flex");
        //     $(`<div class="message-success text-white text-lg max-w-xs ">
        //                     ${message}
        //                 </div>`).appendTo('#success-toast');
        //     // Hide the toast after 5 seconds
        //     setTimeout(function () {
        //         document.getElementById("success-toast").classList.add("hidden");
        //         $(".message-success").remove();
        //         document.getElementById("success-toast").classList.remove("flex");
        //     }, 5000);
        // }

        function displayImage() {

            return {
                images: imagesGallery,

                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event) {
                    if (!event.target.files.length) return

                    let file = event.target.files

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]                            
                        };
                        data = [...data, file[i]]
                        // console.log(Alpine.store("dataAvailable").isDataAvailable)
                        // console.log(data);
                    }

                    this.sendImage()
                },

                removeImage(index) {
                    this.images.splice(index, 1);
                    data.splice(index, 1)

                    this.sendImage()
                },

                sendImage() {
                    let fd = new FormData();

                    for (const file of data) {
                        if (typeof file === 'object') {
                            fd.append('images_gallery[]', file, file.name)
                        }
                        if (typeof file === 'string') {
                            fd.append('saved_gallery[]', file)
                        }
                    }

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        method: 'POST',
                        url: "{{ route('kamarhotel.gallery') }}",
                        data: fd,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            // document.getElementById("loading-spinner").classList.remove("hidden");
                            // document.getElementById("loading-spinner").classList.add("flex");
                        },
                        success: function(msg) {
                            console.log(msg)
                            if (msg.status === 0) {
                                showToastError(msg.message)
                            }

                            if (msg.status === 1) {
                                // showToastSuccess(msg.message)
                                Alpine.store("dataAvailable").isDataAvailable = true;
                            }                                
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                        complete: function() {
                            // document.getElementById("loading-spinner").classList.remove("flex");
                            // document.getElementById("loading-spinner").classList.add("hidden");
                        }
                    })
                }
            }
        }
    </script>

</body>

</html>