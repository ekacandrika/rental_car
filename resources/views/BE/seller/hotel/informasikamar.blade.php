<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->

    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js" defer></script>

    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        .link-not-active {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
            color: #858585;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param || null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <x-be.seller.sidebar-toko>
        <x-slot name="addListingHotel">
            {{ route('hotelinfodasar') }}
        </x-slot>
        <x-slot name="faq">
            @if (!isset($hotel->faq))
            {{ route('hotelfaq') }}
            @else
            {{ route('hotelfaq.edit', $hotel->product_code) }}
            @endif
        </x-slot>

        {{-- Xstay --}}
        <x-slot name="addListingXstay">
            {{ route('infodasar') }}
        </x-slot>
        <x-slot name="faqXstay">
            @if ($xstay == null)
            {{ route('faq.index') }}
            @else
            {{ route('faq.edit', $xstay->product_code) }}
            @endif
        </x-slot>
    </x-be.seller.sidebar-toko>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10">
            <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
            {{-- Breadcumbs --}}
            <nav class="flex" aria-label="Breadcrumb">
                <ol class="inline-flex items-center space-x-1 md:space-x-3">
                    <li class="inline-flex items-center">
                        <a href="{{ route('hotel.edit', $data->product_code) }}"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                            Informasi Dasar</a>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            <a href="{{ route('hotel.infokamar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Informasi
                                Kamar</a>
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->discount_id))
                            <a href="{{ route('hotelharga.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Harga
                                & Ketersediaan</a>
                            @else
                            <a href="{{ route('hotelharga.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Harga
                                & Ketersediaan</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->batas_pembayaran))
                            <a href="{{ route('hotelbatasbayar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Batas
                                Pembayaran & Pembatalan</a>
                            @else
                            <a href="{{ route('hotelbatasbayar') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Batas
                                Pembayaran & Pembatalan</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->link_maps) || isset($data->productdetail->foto_maps_1))
                            <a href="{{ route('hotelmapsfoto.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Peta
                                & Foto</a>
                            @else
                            <a href="{{ route('hotelmapsfoto') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Peta
                                & Foto</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->izin_ekstra))
                            <a href="{{ route('hotelpilihanekstra.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Ekstra</a>
                            @else
                            <a href="{{ route('hotelpilihanekstra') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Ekstra</a>
                            @endif
                        </div>
                    </li>
                </ol>
            </nav>

            <div class="bg-[#FFFFFF] drop-shadow-xl pb-1 mt-5 mb-5 rounded">
                <div class="grid grid-cols-2">
                    <div class="text-lg font-bold font-inter px-5 pt-5 pb-1 text-[#9E3D64]">Informasi Kamar</div>
                </div>

                <div x-data="kamar" x-init="$nextTick(() => { select2WithAlpine() })">
                    {{-- {{ $data->id }} --}}
                    <form class="px-5 pt-3 pb-5 space-y-5" action="{{ route('hotel.infokamar.update', $data->id) }}"
                        method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div>
                            <select name="kamar" id="kamar" x-model="attribute_room" x-ref="select_room" required
                                class="kamar mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                <option selected disabled value="0">Pilih Kamar</option>
                                @foreach ($kamar as $room)
                                <option value="{{ $room->id }}" data-name="{{ $room->nama_kamar }}"
                                    data-code="{{ $room->kode_kamar }}" data-category="{{ $room->jenis_kamar }}"
                                    data-stok="{{ $room->stok }}">{{ $room->kode_kamar }} - {{ $room->nama_kamar }}
                                </option>
                                @endforeach
                            </select>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>
                        </div>
                        <button
                            class="bg-[#9E3D64] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#b94875] duration-200"
                            type="button" @click="addNewRoom">Tambah Kamar
                        </button>

                        @error('room_id')
                        <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                            <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                {{ $message }}
                            </div>
                        </div>
                        @enderror

                        <table class="w-full text-sm text-left text-[#333333] bg-[#D25889] dark:bg-[#D25889] mt-5">
                            <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                                <tr>
                                    <th class="px-6 py-3">No</th>
                                    <th class="px-6 py-3">Kode Kamar</th>
                                    <th class="px-6 py-3">Jenis Kamar</th>
                                    <th class="px-6 py-3">Stok Kamar</th>
                                    <th class="px-6 py-3">Nama Kamar</th>
                                    <th class="px-6 py-3"></th>
                                </tr>
                            </thead>

                            <template x-if="rooms.length > 0">
                                <template x-for="(room, index) in rooms" :key="index">
                                    <tbody>
                                        <tr class="bg-white border-b hover:bg-slate-100">
                                            <td class="px-6 py-2" x-text="index+1"></td>
                                            <td class="px-6 py-2" x-text="room.room_code"></td>
                                            <td class="px-6 py-2" x-text="room.room_cat"></td>
                                            <td class="px-6 py-2" x-text="room.room_stok"></td>
                                            <td class="px-6 py-2" x-text="room.room_name"></td>
                                            <td class="flex px-6 py-2">
                                                <button @click="removeRoom(index)" type="button"
                                                    class="text-white bg-red-500 hover:bg-red-600 duration-200 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-red-500 dark:hover:bg-red-600 dark:focus:ring-red-800">
                                                    <img src="{{ asset('storage/icons/trash-can-regular-squared-size.svg') }}"
                                                        alt="" width="16px" height="16px">
                                                    <span class="sr-only">Icon description</span>
                                                </button>
                                                <input id="room_id" name="room_id[]" type="hidden"
                                                    x-model="room.room_id" required>
                                            </td>
                                        </tr>
                                    </tbody>
                                </template>
                            </template>
                            <template x-if="rooms.length <= 0">
                                <tbody>
                                    <tr>
                                        <td class="bg-white text-center text-xl font-semibold" colspan="6">Tidak ada
                                            kamar
                                            ditambahkan.</td>
                                    </tr>
                                </tbody>
                            </template>

                            {{-- <tbody>
                                @forelse ($rooms as $key => $room)
                                <tr class="bg-white border-b hover:bg-slate-100">
                                    <td class="px-6 py-2">{{ $rooms->firstItem() + $key }}</td>
                                    <td class="px-6 py-2">{{ $room->kode_kamar }}</td>
                                    <td class="px-6 py-2">{{ $room->jenis_kamar }}</td>
                                    <td class="px-6 py-2">{{ $room->stok }}</td>
                                    <td class="px-6 py-2">{{ $room->nama_kamar }}</td>
                                    <td class="flex px-6 py-2">
                                        <form action="{{ route('kamarhotel.clone', $room->id) }}" method="POST">
                                            @csrf

                                            <button title="Salin"
                                                class="text-white bg-blue-500 hover:bg-blue-600 duration-200 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-red-500 dark:hover:bg-red-600 dark:focus:ring-red-800">
                                                <img src="{{ asset('storage/icons/copy.svg') }}" alt="" width="16px"
                                                    height="16px">
                                                <span class="sr-only">Icon description</span>
                                            </button>
                                        </form>

                                        <a type="button" href="{{ route('kamarhotel.view.edit', $room->id) }}"
                                            title="Edit"
                                            class="text-white bg-yellow-500 hover:bg-yellow-600 duration-200 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-yellow-500 dark:hover:bg-yellow-600 dark:focus:ring-blue-800">
                                            <img class="fill-white" src="{{ asset('storage/icons/pencil-solid.svg') }}"
                                                alt="" width="16px">
                                            <span class="sr-only">Icon description</span>
                                        </a>

                                        <a x-data="showModal" @keydown.escape="show = false" type="button"
                                            title="Hapus">
                                            <button @click="show = true" type="button"
                                                class="text-white bg-red-500 hover:bg-red-600 duration-200 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-red-500 dark:hover:bg-red-600 dark:focus:ring-red-800">
                                                <img src="{{ asset('storage/icons/trash-can-regular-squared-size.svg') }}"
                                                    alt="" width="16px" height="16px">
                                                <span class="sr-only">Icon description</span>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                                @empty

                                @endforelse

                            </tbody> --}}
                        </table>
                        {{-- <div class="px-5 mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Kode Kamar</label>
                            <input type="text" id="text" name="kode_kamar" value="{{ old('kode_kamar') }}"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Kode Kamar">
                            @error('kode_kamar')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>

                        <div class="px-5 mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Jenis
                                Kamar</label>
                            <input type="text" id="text" name="jenis_kamar" value="{{ old('jenis_kamar') }}"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Jenis Kamar">
                            @error('jenis_kamar')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>

                        <div class="px-5 mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Harga
                                Kamar</label>
                            <input type="number" id="text" name="harga_kamar" value="{{ old('harga_kamar') }}"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Harga Kamar">
                            @error('harga_kamar')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>

                        <div class="px-5 mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Stok
                                Kamar</label>
                            <input type="number" id="text" name="stok" value="{{ old('stok') }}"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Stok Kamar">
                            @error('stok')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div> --}}

                        {{-- Fasilitas --}}
                        {{-- <div class="px-5 mb-6" x-data="fasilitas()">
                            <label class="block mb-2 text-sm font-bold text-[#333333]">Fasilitas Kamar</label>
                            <div class="px-3">
                                <div>
                                    <button type="button"
                                        class="px-3 py-1 my-3 text-sm text-white rounded-full btn btn-info bg-kamtuu-second"
                                        id="dynamic-ar">Tambah +</button>
                                </div>
                            </div>
                            <div class="flex space-x-5 mb-5">
                                <table id="dynamicAddRemove">
                                </table>
                            </div>
                            @error('foto_kamar')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                            @error('foto_kamar.*')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                            @error('fasilitas')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                            @error('fasilitas.*')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div> --}}
                        @if ($hotel == null)
                        <x-be.com.two-button-dup>
                            {{ route('hotelinfodasar') }}
                        </x-be.com.two-button-dup>
                        @else
                        <x-be.com.two-button-dup>
                            {{ route('hotel.edit', $data->product_code) }}
                        </x-be.com.two-button-dup>
                        @endif
                    </form>
                </div>

            </div>
        </div>
    </div>
    </div>

    <script>
        function kamar() {
            return {
                rooms: {!! isset($rooms_array) ? $rooms_array : "[]" !!},
                attribute_room: 0,
                attribute_id: 0, 
                attribute_name:'', 
                attribute_code:'',
                attribute_cat:'',
                attribute_stok:'',
                error_empty_room: '',
                error_room: false,
                addNewRoom() {

                    if (this.attribute_id === 0) {
                        this.error_room = true
                        this.error_empty_room = 'Kamar wajib diisi!'
                        return this.error_empty_room, this.error_room
                    }

                    this.rooms.push({
                        room_id: this.attribute_id,
                        room_name: this.attribute_name,
                        room_code: this.attribute_code,
                        room_cat: this.attribute_cat,
                        room_stok: this.attribute_stok
                    });
                    
                    this.error_empty_room = ''
                },
                removeRoom(index) {
                    this.rooms.splice(index, 1);
                },
                select2WithAlpine() {
                    this.select2 = $(this.$refs.select_room).select2({
                        placeholder: "Pilih kamar",
                        // allowClear: true
                    });
                    this.select2.on("select2:select", (event) => {
                        this.attribute_id = event.target.value;
                        this.attribute_name = this.select2.find(":selected").data("name");
                        this.attribute_code = this.select2.find(":selected").data("code");
                        this.attribute_cat = this.select2.find(":selected").data("category");
                        this.attribute_stok = this.select2.find(":selected").data("stok");
                    });
                    this.$watch("attribute_room", (value) => {
                        this.select2.val(value).trigger("change");
                    });
                    
                }
            }
        }
    </script>

    <script>
        function amenitas() {
            return {
                icons: '',
                title: '',
                description: '',
                fields: [],
                addNewField() {
                    this.fields.push({
                        icon: '',
                        title: this.title,
                        description: this.description,
                    });
                    // console.log(this.fields)
                },
                removeField(index) {
                    // console.log(index)
                    this.fields.splice(index, 1);
                    console.log(this.fields)
                }
            }
        }
    </script>
    <script type="text/javascript">
        $("#dynamic-ar").click(function() {
            $("#dynamicAddRemove").append(
                `<tr>
                    <td>
                        <label for="icon-fasilitas" class="block mb-2 text-sm font-semibold text-[#333333]">Icon</label>
                        <input type="file" name="foto_kamar[]" class="foto_kamar" style="width: 100px" />
                        <img id="icon-fasilitas"class="ic_fasilitas border flex justify-center items-center rounded border-gray-300 bg-gray-200 p-1 mt-1"src="{{ asset('storage/icons/upload.svg') }}" alt="" width="42px">
                    </td>
                    <td>
                        <label for="fasilitas" class="block mb-2 text-sm font-bold text-[#333333]">Keterangan</label>
                        <input type="text" id="fasilitas" name="fasilitas[]" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Kolam Renang">
                    </td>
                    <td class="p-3">
                        <button type="button" class="mx-2 remove-input-field"><img
                            src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                            alt="delete_button" width="25px">
                        </button>
                    </td>
                </tr>`
            );
        });
        $(document).on('click', '.remove-input-field', function() {
            $(this).parents('tr').remove();
        });

        // Change icon preview
        $(document).on('change', '.foto_kamar', function(e) {  
            let el_index = $(this).index('.foto_kamar')
            const file = this.files[0];
            if (file) {
                let reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function (e) {
                    $('.ic_fasilitas').each(function(i, el) {
                        if (i === el_index) {
                            $(el).attr("src", e.target.result);
                        }
                    })
                };
            }
        });
    </script>
</body>

</html>