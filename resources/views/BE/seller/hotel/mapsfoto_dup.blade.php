<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        .link-not-active {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
            color: #858585;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param || null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <x-be.seller.sidebar-toko>
        <x-slot name="addListingHotel">
            {{ route('hotelinfodasar') }}
        </x-slot>
        <x-slot name="faq">
            @if ($data->faq == null)
            {{ route('hotelfaq') }}
            @else
            {{ route('hotelfaq.edit', $data->product_code) }}
            @endif
        </x-slot>

        {{-- Xstay --}}
        <x-slot name="addListingXstay">
            {{ route('infodasar') }}
        </x-slot>
        <x-slot name="faqXstay">
            @if ($xstay == null)
            {{ route('faq.index') }}
            @else
            {{ route('faq.edit', $xstay->product_code) }}
            @endif
        </x-slot>
    </x-be.seller.sidebar-toko>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10">
            {{-- Toast Error --}}
            <div id="error-toast"
                class="hidden fixed right-10 top-20 z-50 items-center bg-red-600 border-l-4 border-red-700 py-3 px-3 shadow-md mb-2">
                <!-- icons -->
                <div class="text-red-500 rounded-full bg-white mr-3">
                    <svg width="1.8em" height="1.8em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z" />
                        <path fill-rule="evenodd"
                            d="M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z" />
                    </svg>
                </div>
            </div>

            {{-- Toast Success --}}
            <div id="success-toast"
                class="hidden fixed right-10 top-20 z-50 items-center bg-green-500 border-l-4 border-green-700 py-2 px-3 shadow-md mb-2">
                <!-- icons -->
                <div class="text-green-500 rounded-full bg-white mr-3">
                    <svg width="1.8em" height="1.8em" viewBox="0 0 16 16" class="bi bi-check" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z" />
                    </svg>
                </div>
            </div>

            <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
            {{-- Breadcumbs --}}
            <nav class="flex" aria-label="Breadcrumb">
                <ol class="inline-flex items-center space-x-1 md:space-x-3">
                    <li class="inline-flex items-center">
                        <a href="{{ route('hotel.edit', $data->product_code) }}"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                            Informasi Dasar</a>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            <a href="{{ route('hotel.infokamar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Informasi
                                Kamar</a>
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->discount_id))
                            <a href="{{ route('hotelharga.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Harga
                                & Ketersediaan</a>
                            @else
                            <a href="{{ route('hotelharga.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Harga
                                & Ketersediaan</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->batas_pembayaran))
                            <a href="{{ route('hotelbatasbayar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Batas
                                Pembayaran & Pembatalan</a>
                            @else
                            <a href="{{ route('hotelbatasbayar.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Batas
                                Pembayaran & Pembatalan</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->link_maps))
                            <a href="{{ route('hotelmapsfoto.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Peta
                                & Foto</a>
                            @else
                            <a href="{{ route('hotelmapsfoto.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Peta
                                & Foto</a>
                            @endif
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            @if (isset($data->productdetail->izin_ekstra))
                            <a href="{{ route('hotelpilihanekstra.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Ekstra</a>
                            @else
                            <a href="{{ route('hotelpilihanekstra.edit', $data->product_code) }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] link-not-active">Ekstra</a>
                            @endif
                        </div>
                    </li>
                </ol>
            </nav>

            {{-- Section Images Gallery --}}
            <div class="bg-[#FFFFFF] drop-shadow-xl my-3 p-6 px-10 rounded">
                <div class="flex justify-between">
                    <div class="text-lg font-bold font-inter text-[#9E3D64]">
                        Foto Gallery
                    </div>
                </div>
                {{-- Foto --}}
                <div class="py-2" x-data="displayImage()">
                    <input class="py-2" type="file" accept="image/*" id="images_gallery" @change="selectedFile"
                        multiple>
                    <template x-if="images.length < 1">
                        <div
                            class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                            <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                height="20px">
                        </div>
                    </template>
                    <template x-if="images.length >= 1">
                        <div class="flex flex-wrap gap-5">
                            <template x-for="(image, index) in images" :key="index">
                                <div class="flex justify-center items-center rounded">
                                    <img :src="image"
                                        class="object-contain rounded border border-gray-300 w-[154px] h-[154px] hover:opacity-70 duration-200"
                                        :alt="'upload'+index">
                                    <button class="absolute mx-2 translate-x-12 -translate-y-14" type="button">
                                        <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                            alt="delete-button" width="25px" @click="removeImage(index)">
                                    </button>
                                </div>
                            </template>
                        </div>
                    </template>
                    <button type="button" @click.prevent="sendImage"
                        class="w-48 items-center px-5 py-2.5 my-3 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-white hover:text-[#23AEC1] hover:border hover:border-[#23AEC1] duration-200">
                        Perbarui Foto Gallery
                    </button>
                    {{-- Loading Spinner --}}
                    <div id="loading-spinner"
                        class="absolute hidden w-full h-full bg-slate-300 opacity-70 top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 flex-row space-x-4 justify-center items-center">
                        <div
                            class="w-12 h-12 rounded-full animate-spin border-8 border-solid border-[#9E3D64] border-t-transparent">
                        </div>
                    </div>
                </div>
            </div>

            <form action="{{ route('hotelmapsfoto.update', $data->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                {{-- Section --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded">
                    <div class="flex justify-between">
                        <div class="text-lg font-bold font-inter text-[#9E3D64]">
                            Pilih Foto
                        </div>
                    </div>

                    {{-- Foto Fitur --}}
                    <div class="py-2" x-data="featuredImage()">
                        <p class="font-semibold text-[#BDBDBD]">Foto Fitur</p>
                        <input class="py-2" type="file" name="foto_maps_2" accept="image/*" @change="selectedFile">
                        <div class="flex">
                            <template x-if="imageUrl && !savedImage">
                                <div class="flex justify-center items-center">
                                    <img :src="imageUrl"
                                        class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                        alt="upload-featured-photo">
                                    {{-- <button class="absolute mx-2 translate-x-12 -translate-y-14"><img
                                            src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                            width="25px" @click="removeImage()">
                                    </button> --}}
                                </div>
                            </template>
                            <template x-if="!imageUrl && !savedImage">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                            <template x-if="savedImage">
                                @if (isset($detail->foto_maps_2))
                                <div class="flex">
                                    <img src="{{ asset($detail->foto_maps_2) }}"
                                        class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                        alt="upload-featured-photo">
                                    <input type="hidden" name="saved_thumbnail" value="{{ $detail->foto_maps_2 }}">
                                </div>
                                @endif
                            </template>
                        </div>
                    </div>
                    @error('foto_maps_2')
                    <div class="bg-red-300 rounded-md w-96 p-3 my-2">
                        {{ $message }}
                    </div>
                    @enderror

                    {{-- Maps --}}
                    <div class="my-5">
                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Link Peta Google
                            Maps</label>
                        <input type="text" id="text" name="link_maps" value="{{ $detail->link_maps}}"
                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="<iframe src=”https://www.map">
                    </div>
                    @error('link_maps')
                    <div class="bg-red-300 rounded-md w-96 p-3 my-2">
                        {{ $message }}
                    </div>
                    @enderror

                    @if ($detail->link_maps == null && $detail->foto_maps2 == null && $detail->foto_maps2)
                    <x-be.com.two-button-dup>
                        {{ route('hotelbatasbayar') }}
                    </x-be.com.two-button-dup>
                    @else
                    <x-be.com.two-button-dup>
                        {{ route('hotelbatasbayar.edit', $data->product_code) }}
                    </x-be.com.two-button-dup>
                    @endif
                </div>
            </form>
        </div>
    </div>
    </div>

    <script>
        let data = {!! isset($detail->foto_maps_1) ? $detail->foto_maps_1 : "[]" !!};
        let imagesGallery = [];

        if (data.length > 0) {
            const url = window.location.origin
            let array = data
            array.forEach(element => {
                imagesGallery.push(url+'/'+element)
            });
        }
        // Show error toast
        function showToastError(message) {
            document.getElementById("error-toast").classList.remove("hidden");
            document.getElementById("error-toast").classList.add("flex");
            $(`<div class="message-error text-white text-lg max-w-xs ">
                            ${message}
                        </div>`).appendTo('#error-toast');
            // Hide the toast after 5 seconds
            setTimeout(function () {
                document.getElementById("error-toast").classList.add("hidden");
                $(".message-error").remove();
                document.getElementById("error-toast").classList.remove("flex");
            }, 5000);
        }

        // Show success toast
        function showToastSuccess(message) {
            document.getElementById("success-toast").classList.remove("hidden");
            document.getElementById("success-toast").classList.add("flex");
            $(`<div class="message-success text-white text-lg max-w-xs ">
                            ${message}
                        </div>`).appendTo('#success-toast');
            // Hide the toast after 5 seconds
            setTimeout(function () {
                document.getElementById("success-toast").classList.add("hidden");
                $(".message-success").remove();
                document.getElementById("success-toast").classList.remove("flex");
            }, 5000);
        }

        function displayImage() {

            return {
                images: imagesGallery,

                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event) {
                    if (!event.target.files.length) return

                    let file = event.target.files

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                            data = [...data, file[i]]
                        };

                        console.log(data);
                    }
                },

                removeImage(index) {
                    this.images.splice(index, 1);
                    data.splice(index, 1)
                },

                sendImage() {
                    let fd = new FormData();

                    for (const file of data) {
                        if (typeof file === 'object') {
                            fd.append('images_gallery[]', file, file.name)
                        }
                        if (typeof file === 'string') {
                            fd.append('saved_gallery[]', file)
                        }
                    }

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        method: 'POST',
                        url: "{{ route('hotelmapsfotogallery.update', $data->id) }}",
                        data: fd,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            document.getElementById("loading-spinner").classList.remove("hidden");
                            document.getElementById("loading-spinner").classList.add("flex");
                        },
                        success: function(msg) {
                            console.log(msg)
                            if (msg.status === 0) {
                                showToastError(msg.message)
                            }

                            if (msg.status === 1) {
                                showToastSuccess(msg.message)
                            }                                
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                        complete: function() {
                            document.getElementById("loading-spinner").classList.remove("flex");
                            document.getElementById("loading-spinner").classList.add("hidden");
                        }
                    })
                }
            }
        }
    </script>

    <script>
        function featuredImage() {

            return {
                imageUrl: '',
                savedImage: "{!! isset($detail->foto_maps_2) ? $detail->foto_maps_2 : null !!}",

                selectedFile(event) {
                    this.fileToUrl(event, src => this.imageUrl = src)
                    this.savedImage = ''
                },

                fileToUrl(event, callback) {
                    if (!event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },

                removeImage() {
                    this.imageUrl = '';
                }
            }
        }
    </script>
</body>

</html>