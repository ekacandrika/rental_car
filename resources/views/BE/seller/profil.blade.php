<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    {{-- <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script> --}}
    {{-- <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" /> --}}

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        h1 {
            font-size: 30px;
            color: #000;
        }

        h2 {
            font-size: 20px;
            color: #000;
        }

        .menu-malasngoding li a {
            display: inline-block;
            color: white;
            /* text-align: center; */
            text-decoration: none;
        }

        .menu-malasngoding li a:hover {
            background-color: none;
        }

        li.dropdown {
            display: inline-block;
            margin-right: 80px
        }

        .dropdown:hover .isi-dropdown {
            display: block;
        }

        .isi-dropdown a:hover {
            color: #fff !important;
            width: 100%;
        }

        .isi-dropdown {
            position: absolute;
            display: none;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
            background-color: #f9f9f9;
            width: 100%;
        }

        .isi-dropdown a {
            color: #3c3c3c !important;
            padding: 1%;
        }

        .isi-dropdown a:hover {
            color: #232323 !important;
            background: #f3f3f3 !important;
        }

        a[disabled="disabled"] {
            pointer-events: none;
        }

        /* Modal CSS */
        .btn {
            background-color: #4CAF50;
            color: white;
            padding: 8px 16px;
            border: none;
            cursor: pointer;
            font-size: 16px;
        }

        /* Style the modal */
        .modal {
            display: none;
            position: fixed;
            z-index: 99;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0, 0, 0, 0.4);
        }

        /* Style the modal content */
        .modal-content {
            background-color: #fefefe;
            margin: 10% auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
            max-width: 600px;
        }

        /* Media query for screens smaller than 600px */
        @media screen and (max-width: 600px) {
            .modal-content {
                margin: 15% auto;
                width: 90%;
            }
        }

        /* Style the close button */
        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        /* Style the form input */
        .form-control {
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            margin-bottom: 10px;
            width: 100%;
        }

        /* Style the submit button */
        .btn-primary {
            background-color: #23AEC1;
            color: white;
            padding: 8px 16px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        .btn-primary:hover {
            background-color: #23AEC1;
        }
        .gj-datepicker-md [role="right-icon"]{
            position: absolute;
            right: 80px;
            top: 8px;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.seller.navbar-seller></x-be.seller.navbar-seller>

    {{-- Sidebar --}}
    <x-be.seller.sidebar-seller></x-be.seller.sidebar-seller>

    <div class="grid grid-cols-10">
        <div class="col-start-3 col-end-11 z-0">
            {{-- @dump(auth()->user()) --}}
            <div class="p-5 pl-10 pr-10">
                <span class="text-2xl font-bold font-inter text-[#333333]">Profil</span>

                <div class="bg-white my-5 rounded-md shadow-lg">
                    <form action="{{ route('seller.update.kategori.seller', auth()->user()->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="ml-12 p-6">
                            <p class="text-lg font-bold">Kategori Seller</p>
                            <div class="grid grid-cols-2 w-80 justify-center">
                                <div class="flex items-center py-4">
                                    <input id="country-option-1" type="radio" name="jenis_registrasi" value="personal"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                        {{ auth()->user()->jenis_registrasi == 'personal' ? 'checked' : '' }}>
                                    <label for="country-option-1"
                                        class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                        Personal
                                    </label>
                                </div>
                                <div class="flex items-center py-4">
                                    <input id="country-option-2" type="radio" name="jenis_registrasi" value="company"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                        {{ auth()->user()->jenis_registrasi == 'company' ? 'checked' : '' }}>
                                    <label for="country-option-2"
                                        class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                        Company
                                    </label>
                                </div>
                            </div>
                            <button type="submit"
                                class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                Ubah
                            </button>
                        </div>
                    </form>
                </div>

                {{-- Lisensi --}}
                {{-- @dump($personal_activity) --}}
                <div class="bg-white my-5 rounded-md shadow-lg">
                    <form action="{{ route('seller.update.lisensi.personal', auth()->user()->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="ml-12 p-6">
                            <p class="text-lg font-bold">Lisensi Personal</p>
                            <div class="grid grid-rows-6 w-80 py-4">
                                <div class="flex items-center py-px">
                                    <input id="default-checkbox" name="lisensi[personal_tur]" type="checkbox"
                                        value="Personal Tur Seller"
                                        class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                        {{ $personal_tur == 'Personal Tur Seller' ? 'checked' : '' }}>
                                    <label for="default-checkbox"
                                        class="ml-2 text-sm font-semibold text-[#333333]">Personal Tur Seller</label>
                                </div>
                                <div class="flex items-center py-px">
                                    <input id="default-checkbox" name="lisensi[personal_hotel]" type="checkbox"
                                        value="Personal Hotel Seller"
                                        class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                        {{ $personal_hotel == 'Personal Hotel Seller' ? 'checked' : '' }}>
                                    <label for="default-checkbox"
                                        class="ml-2 text-sm font-semibold text-[#333333]">Personal Hotel Seller</label>
                                </div>
                                <div class="flex items-center py-px">
                                    <input id="checked-checkbox" name="lisensi[bussiness_tour_hotel]" type="checkbox"
                                        value="Personal Bussiness Tur & Hotel Seller"
                                        class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                        {{ $bussiness_tour_hotel == 'Personal Bussiness Tur & Hotel Seller' ?
                                    'checked' : '' }}>
                                    <label for="checked-checkbox"
                                        class="ml-2 text-sm font-semibold text-[#333333] w-96">Personal Bussiness
                                        Tur & Hotel Seller</label>
                                    <img class="mx-1" src="{{ asset('storage/icons/circle-info-black.svg') }}"
                                        alt="info" width="18px" height="18px">
                                </div>
                                <div class="flex items-center py-px">
                                    <input id="default-checkbox" name="lisensi[personal_transfer]" type="checkbox"
                                        value="Personal Transfer Seller"
                                        class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                        {{ $personal_transfer == 'Personal Transfer Seller' ? 'checked' : '' }}>
                                    <label for="default-checkbox"
                                        class="ml-2 text-sm font-semibold text-[#333333]">Personal Transfer
                                        Seller</label>
                                </div>
                                <div class="flex items-center py-px">
                                    <input id="checked-checkbox" name="lisensi[personal_rental]" type="checkbox"
                                        value="Personal Vehicle Rental Seller"
                                        class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                        {{ $personal_rental == 'Personal Vehicle Rental Seller' ? 'checked' : ''
                                    }}>
                                    <label for="checked-checkbox"
                                        class="ml-2 text-sm font-semibold text-[#333333]">Personal Vehicle Rental
                                        Seller</label>
                                </div>
                                <div class="flex items-center py-px">
                                    <input id="checked-checkbox" name="lisensi[bussiness_transfer_rental]" type="checkbox"
                                        value="Personal Bussiness Transfer & Vechile Rental Seller"
                                        class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                        {{ $bussiness_transfer_rental == 'Personal Bussiness Transfer & Vechile Rental Seller' ? 'checked' : '' }}>
                                    <label for="checked-checkbox"
                                        class="ml-2 text-sm font-semibold text-[#333333] w-96">Personal Bussiness
                                        Transfer & Vechile Rental Seller</label>
                                    <img class="mx-1" src="{{ asset('storage/icons/circle-info-black.svg') }}"
                                        alt="info" width="18px" height="18px">
                                </div>
                                <div class="flex items-center py-px">
                                    <input id="checked-checkbox" name="lisensi[personal_activity]" type="checkbox"
                                        value="Personal Activity Seller"
                                        class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                        {{ $personal_activity == 'Personal Activity Seller' ? 'checked' : '' }}>
                                    <label for="checked-checkbox"
                                        class="ml-2 text-sm font-semibold text-[#333333]">Personal Activity Seller</label>
                                </div>
                                <div class="flex items-center py-px">
                                    <input id="checked-checkbox" name="lisensi[personal_xstay]" type="checkbox"
                                        value="Personal XStay Seller"
                                        class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                        {{ $personal_xstay == 'Personal XStay Seller' ? 'checked' : '' }}>
                                    <label for="checked-checkbox"
                                        class="ml-2 text-sm font-semibold text-[#333333]">Personal XStay Seller</label>
                                </div>
                                <div class="flex items-center py-px">
                                    <input id="checked-checkbox" name="lisensi[bussiness_activity_xstay]" type="checkbox"
                                        value="Personal Bussiness Activity & XStay Seller"
                                        class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                        {{ $bussiness_activity_xstay == 'Personal Bussiness Activity & XStay Seller' ?
                                    'checked' : '' }}>
                                    <label for="checked-checkbox"
                                        class="ml-2 text-sm font-semibold text-[#333333] w-96">Personal Bussiness Activity & XStay Seller</label>
                                    <img class="mx-1" src="{{ asset('storage/icons/circle-info-black.svg') }}"
                                        alt="info" width="18px" height="18px">
                                </div>
                            </div>
                            <button type="submit"
                                class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                Ubah
                            </button>
                        </div>
                    </form>
                </div>

                {{-- Data Diri --}}
                <div class="bg-white my-5 rounded-md shadow-lg hidden" id="personal">
                    <form action="{{ route('seller.update.data.diri', auth()->user()->id) }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="ml-12 p-6">
                            <p class="text-lg font-bold text-[#333333]">Data Diri</p>
                            <div class="py-5">
                                <div class="w-auto flex">
                                    @if (auth()->user()->profile_photo_path == null)
                                    <img src="{{ asset('storage/img/logo-traveller.png') }}"
                                        alt="{{ auth()->user()->first_name }}"
                                        class="object-cover object-center w-14 h-14">
                                    @else
                                    <img src="{{ asset(auth()->user()->profile_photo_path) }}"
                                        alt="{{ auth()->user()->first_name }}"
                                        class="object-cover object-center w-14 h-14">
                                    @endif
                                    <div class="grid grid-cols-1">
                                        <p class="ml-4 pl-3 font-inter text-align w-full font-medium text-base">Foto
                                            Profil
                                        </p>
                                        <div for="upload"
                                            class="ml-4 flex block w-full h-7 pt-1 text-[#23AEC1] text-xs bg-violet-50 hover:bg-violet-100 justify-center rounded-full">
                                            {{-- <img src="{{ asset('storage/icons/ic-upload-blue.svg') }}"
                                                alt="user-profile" class="w-3 h-3 mr-1"> --}}
                                            <input class="btn-danger" name="profile_photo_path" type="file"
                                                id="upload" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p class="text-sm font-bold">Status: <b class="text-green-500">Terverifikasi</b></p>
                            <div class="grid grid-cols-2">
                                <div class="my-2">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nama
                                        Depan</label>
                                    <input type="text" name="first_name" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Nama Depan" value="{{ auth()->user()->first_name }}">
                                </div>
                                {{-- <div class="my-2">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nama
                                        Belakang</label>
                                    <input type="text" name="last_name" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Nama Belakang" value="{{ $userdetail->last_name }}">
                                </div> --}}
                                <div class="my-2">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Jenis
                                        Kelamin</label>
                                    <select name="jk"
                                        class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option {{ auth()->user()->jk == 'Laki-Laki' ? 'selected' : '' }}
                                            value="Laki-Laki">
                                            Laki-Laki</option>
                                        <option {{ auth()->user()->jk == 'Perempuan' ? 'selected' : '' }}
                                            value="Perempuan">
                                            Perempuan</option>
                                    </select>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>
                                </div>
                                {{-- <div class="my-2">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Tanggal
                                        Lahir</label>
                                    <input type="date" name="date_of_birth"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Tur Sehat" value="{{ $userdetail->date_of_birth }}">
                                </div> --}}
                            </div>

                            <button type="submit"
                                class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                Perbarui
                            </button>
                        </div>
                    </form>
                </div>
                <div id="company" class="hidden">
                    {{-- toko --}}
                    <div class="bg-white my-5 rounded-md shadow-lg" id="data_toko">
                        <form action="{{route('seller.update.jenis.toko',auth()->user()->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="ml-12 p-6">
                                <p class="text-lg font-bold">Jenis Toko</p>
                                <div class="grid grid-cols-2 w-80 justify-center">
                                    <div class="flex items-center py-4">
                                        <input id="country-option-1" type="radio" name="jenis_toko" value="unofficial store"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            {{isset($userdetail->store_type) ? ($userdetail->store_type == 'unofficial store' ? 'checked' : '') :'' }}>
                                        <label for="country-option-1"
                                            class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                            Toko Biasa
                                        </label>
                                    </div>
                                    <div class="flex items-center py-4">
                                        <input id="store_type" type="radio" name="jenis_toko" value="official store"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            {{isset($userdetail->store_type) ? ($userdetail->store_type == 'official store' ? 'checked' : '' ) : ''}}>
                                        <label for="store_type"
                                            class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                            Toko Resmi (Official Store)
                                        </label>
                                    </div>
                                </div>
                                <button type="submit"
                                    class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                    Ubah
                                </button>
                            </div>
                        </form>
                    </div>

                    {{-- data cabang --}}
                    <div class="bg-white my-5 rounded-md shadow-lg" id="data_cabang">
                        <div class="ml-12 p-6">
                            <div class="flex grid grid-cols-2 justify-center items-center">
                                <p class="text-lg font-bold text-[#333333]">Data Cabang</p>
                                <div class="col-start-5 col-end-7 items-center justify-center mt-5 mx-5">
                                    <a href="{{route('seller.create-branch')}}">
                                        <button type="button"
                                                class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                                Tambah Cabang
                                        </button>
                                    </a>                      
                                </div>
                            </div>
                            <div class="mt-3">
                                <table class="w-full text-sm text-left text-[#333333]">
                                    <thead class="text-md text-white bg-[#D25889]">
                                        <tr>
                                            <th scope="col" class="py-3 px-6">Alamat</th>
                                            <th scope="col" class="py-3 px-6">Provinsi</th>
                                            <th scope="col" class="py-3 px-6">Kota/Kab</th>
                                            <th scope="col" class="py-3 px-6">Kecamatan</th>
                                            <th scope="col" class="py-3 px-6">Kelurahan</th>
                                            <th scope="col" class="py-3 px-6"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($branch_stores as $branch_store)
                                        <tr clas="py-2">
                                            <td class="pt-2">{{$branch_store->address}}</td>
                                            <td class="pt-2">{{$branch_store->province->name}}</td>
                                            <td class="pt-2">{{$branch_store->regency->name}}</td>
                                            <td class="pt-2">{{$branch_store->district->name}}</td>
                                            <td class="pt-2">{{$branch_store->village->name}}</td>
                                            <td class="pt-2">
                                                <div class="flex grid grid-cols-2 justify-center items-center">
                                                    <a href="{{route('seller.edit-branch',$branch_store->id)}}">
                                                        <button class="mx-1">
                                                             <img src="{{ asset('storage/icons/pencil-solid 1.svg') }}" alt="" width="16px">
                                                        </button>
                                                    </a>
                                                    <form action="{{route('seller.delete-branch',$branch_store->id)}}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                        <button type="submit">
                                                            <svg width="16px" height="50" viewBox="0 0 44 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <g clip-path="url(#clip0_230_9549)">
                                                                <path d="M15.625 39.0625C15.625 39.9219 14.9219 40.625 14.0625 40.625C13.2031 40.625 12.5 39.9219 12.5 39.0625V18.75C12.5 17.8906 13.2031 17.1875 14.0625 17.1875C14.9219 17.1875 15.625 17.8906 15.625 18.75V39.0625ZM23.4375 39.0625C23.4375 39.9219 22.7344 40.625 21.875 40.625C21.0156 40.625 20.3125 39.9219 20.3125 39.0625V18.75C20.3125 17.8906 21.0156 17.1875 21.875 17.1875C22.7344 17.1875 23.4375 17.8906 23.4375 18.75V39.0625ZM31.25 39.0625C31.25 39.9219 30.5469 40.625 29.6875 40.625C28.8281 40.625 28.125 39.9219 28.125 39.0625V18.75C28.125 17.8906 28.8281 17.1875 29.6875 17.1875C30.5469 17.1875 31.25 17.8906 31.25 18.75V39.0625ZM31.0059 2.43555L34.5898 7.8125H41.4062C42.7051 7.8125 43.75 8.8623 43.75 10.1562C43.75 11.4551 42.7051 12.5 41.4062 12.5H40.625V42.1875C40.625 46.5039 37.1289 50 32.8125 50H10.9375C6.62305 50 3.125 46.5039 3.125 42.1875V12.5H2.34375C1.0498 12.5 0 11.4551 0 10.1562C0 8.8623 1.0498 7.8125 2.34375 7.8125H9.16211L12.7441 2.43555C13.7598 0.91377 15.4688 0 17.2949 0H26.4551C28.2812 0 29.9902 0.913867 31.0059 2.43555ZM14.7949 7.8125H28.9551L27.0996 5.03516C26.9531 4.81836 26.709 4.6875 26.4551 4.6875H17.2949C17.041 4.6875 16.709 4.81836 16.6504 5.03516L14.7949 7.8125ZM7.8125 42.1875C7.8125 43.916 9.21191 45.3125 10.9375 45.3125H32.8125C34.541 45.3125 35.9375 43.916 35.9375 42.1875V12.5H7.8125V42.1875Z" fill="#333333"/>
                                                                </g>
                                                                <defs>
                                                                <clipPath id="clip0_230_9549">
                                                                <rect width="50px" height="50" fill="red"/>
                                                                </clipPath>
                                                                </defs>
                                                            </svg>
                                                            {{-- <img src="{{ asset('storage/icons/trash-can-solid.svg') }}" alt="" width="16px" style=""> --}}
                                                        </button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>  
                    </div>  

                    {{-- Data Perusahaan --}}
                    <div class="bg-white my-5 rounded-md shadow-lg" id="data_perusahaan">
                        <form action="{{ route('seller.update.data.perusahaan', auth()->user()->id) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="ml-12 p-6">
                                <p class="text-lg font-bold text-[#333333]">Data Perusahaan</p>
                                <div class="py-5">
                                    
                                </div>
                                @if (isset(auth()->user()->active_status) && auth()->user()->active_status == 'Confrimed')
                                <p class="text-sm font-bold">Status: <b class="text-green-500">Terverifikasi</b>
                                @else 
                                <p class="text-sm font-bold">Status: <b class="text-red-500">Belum Verifikasi</b> 
                                @endif
                                <div class="grid grid-cols-2">
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nama
                                            Perusahaan</label>
                                        <input type="text" name="nama_perusahaan" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Nama Perusahaan" value="{{isset($userdetail->company_name) ? $userdetail->company_name:null}}">
                                        @if($errors->has('nama_perusahaan'))
                                        <p class="text-sm text-red-400 font-bold my-2"><span>{{$errors->first('nama_perusahaan')}}</span></p>
                                        @endif
                                    </div>
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Tanggal Pendirian</label>
                                            <input type="text" name="tgl_pendirian" id="datepicker"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5"          
                                            placeholder="Tanggal Pendirian" value="{{isset($userdetail->date_of_establishment) ? $userdetail->date_of_establishment:null}}" style="border: 1px solid #4F4F4F; background-color:#FFFFFF;width:24rem;padding:0.625rem;border-radius: 0.5rem;--tw-ring-color: rgb(59 130 246)"/>
                                        <div
                                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                        </div>
                                        @if($errors->has('tgl_pendirian'))
                                        <p class="text-sm text-red-400 font-bold my-2"><span>{{$errors->first('tgl_pendirian')}}</span></p>
                                        @endif
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Alamat(Sesuai Akta Pendirian)</label>
                                        <input type="text" name="alamat" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Alamat" value="{{isset($userdetail->address) ? $userdetail->address:null}}">
                                    </div>
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Provinsi</label>
                                        <select name="province_id" id="provinsi"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                            <option selected disabled>Pilih Provinsi</option>
                                            @foreach ($province as $provinsi)
                                            <option value="{{ $provinsi->id }}" {{isset($userdetail->province_id) ? ($userdetail->province_id==$provinsi->id ? 'selected':'') :''}}>{{ $provinsi->name }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('province_id'))
                                        <p class="text-sm text-red-400 font-bold my-2"><span>{{$errors->first('province_id')}}</span></p>
                                        @endif
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kota/Kabupaten</label>
                                        <select name="regency_id" id="kabupaten"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                            <option selected disabled>Pilih Kota/Kabupaten</option>
                                        </select>
                                        @if($errors->has('regency_id'))
                                        <p class="text-sm text-red-400 font-bold my-2"><span>{{$errors->first('regency_id')}}</span></p>
                                        @endif   
                                    </div>
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kecamatan</label>
                                        <select name="district_id" id="kecamatan"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                            <option selected disabled>Pilih Kecamatan</option>
                                        </select>
                                        @if($errors->has('district_id'))
                                        <p class="text-sm text-red-400 font-bold my-2"><span>{{$errors->first('district_id')}}</span></p>
                                        @endif     
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kelurahan/Desa</label>
                                        <select name="village_id" id="kelurahan"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                            <option selected disabled>Pilih Kelurahan</option>
                                        </select>
                                        @if($errors->has('village_id'))
                                        <p class="text-sm text-red-400 font-bold my-2"><span>{{$errors->first('village_id')}}</span></p>
                                        @endif     
                                    </div>
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kode Pos</label>
                                        <input type="text" name="kode_pos" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Kode Pos" value="{{isset($userdetail->postal_code) ? $userdetail->postal_code:''}}">
                                        </div>
                                        @if($errors->has('kode_pos'))
                                        <p class="text-sm text-red-400 font-bold my-2"><span>{{$errors->first('kode_pos')}}</span></p>
                                        @endif    
                                </div>
                                <div class="grid grid-cols-2 mb-2">
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Direktur</label>
                                        <input type="text" name="nama_direktur" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Nama Direktur" value="{{isset($userdetail->directur_name) ? $userdetail->directur_name:''}}">
                                            @if($errors->has('nama_direktur'))
                                            <p class="text-sm text-red-400 font-bold my-2"><span>{{$errors->first('nama_direktur')}}</span></p>
                                            @endif    
                                    </div>
                                </div>
                                <button type="submit"
                                    class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                    Perbarui
                                </button>
                            </div>
                        </form>
                    </div>

                    {{-- Data Penanggung jawab --}}
                    <div class="bg-white my-5 rounded-md shadow-lg" id="data_penanggung_jawab">
                        <form action="{{ route('seller.update.data.penanggungjawab', auth()->user()->id) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="ml-12 p-6">
                                <p class="text-lg font-bold text-[#333333]">Data Penganggung jawab</p>
                                <div class="py-5">
                                    <div class="w-auto flex">
                                        @if (auth()->user()->profile_photo_path == null)
                                        <img src="{{ asset('storage/img/logo-traveller.png') }}"
                                            alt="{{ auth()->user()->first_name }}"
                                            class="object-cover object-center w-14 h-14">
                                        @else
                                        <img src="{{ asset(auth()->user()->profile_photo_path) }}"
                                            alt="{{ auth()->user()->first_name }}"
                                            class="object-cover object-center w-14 h-14">
                                        @endif
                                        <div class="grid grid-cols-1">
                                            <p class="ml-4 pl-3 font-inter text-align w-full font-medium text-base">Foto
                                                Profil
                                            </p>
                                            <div for="upload"
                                                class="ml-4 flex block w-full h-7 pt-1 text-[#23AEC1] text-xs bg-violet-50 hover:bg-violet-100 justify-center rounded-full">
                                                {{-- <img src="{{ asset('storage/icons/ic-upload-blue.svg') }}"
                                                    alt="user-profile" class="w-3 h-3 mr-1"> --}}
                                                <input class="btn-danger" name="profile_photo_path" type="file"
                                                    id="upload" />
                                            </div>
                                        </div>
                                    </div>
                                    @if($errors->has('profile_photo_path'))
                                        <p class="text-sm text-red-400 font-bold my-2"><span>{{$errors->first('profile_photo_path')}}</span></p>
                                    @endif
                                </div>
                                @if (isset(auth()->user()->active_status) && auth()->user()->active_status == 'Confrimed')
                                <p class="text-sm font-bold">Status: <b class="text-green-500">Terverifikasi</b>
                                @else 
                                <p class="text-sm font-bold">Status: <b class="text-red-500">Belum Verifikasi</b> 
                                @endif
                                <div class="grid grid-cols-2">
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nama</label>
                                        <input type="text" name="first_name" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Nama Depan" value="{{ auth()->user()->first_name }}">
                                        @if($errors->has('first_name'))
                                            <p class="text-sm text-red-400 font-bold my-2"><span>{{$errors->first('first_name')}}</span></p>
                                        @endif    
                                    </div>
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Jabatan</label>
                                            <input type="text" name="jabatan" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Jabatan" value="{{isset($userdetail->job_title) ? $userdetail->job_title:null}}">
                                        <div
                                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                        </div>
                                        @if($errors->has('jabatan'))
                                            <p class="text-sm text-red-400 font-bold my-2"><span>{{$errors->first('jabatan')}}</span></p>
                                        @endif 
                                    </div>
                                    {{-- <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Tanggal
                                            Lahir</label>
                                        <input type="date" name="date_of_birth"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Tur Sehat" value="{{ $userdetail->date_of_birth }}">
                                    </div> --}}
                                </div>
                                <div class="grid grid-cols-2">
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Email</label>
                                        <input type="text" name="email" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Email" value="{{ auth()->user()->email }}">
                                        @if($errors->has('email'))
                                            <p class="text-sm text-red-400 font-bold my-2"><span>{{$errors->first('email')}}</span></p>
                                        @endif        
                                    </div>
                                    
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">No Telp.</label>
                                        <input type="text" name="no_tlp" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="No Tetepon" value="{{ auth()->user()->no_tlp }}">
                                        <div
                                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                        </div>
                                    @if($errors->has('no_tlp'))
                                        <p class="text-sm text-red-400 font-bold my-2"><span>{{$errors->first('no_tlp')}}</span></p>
                                    @endif     
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Whatsapp</label>
                                        <input type="text" name="whatsapp" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Whatapp" value="{{auth()->user()->whatsapp_no != null ? auth()->user()->whatsapp_no : auth()->user()->no_tlp }}">
                                    </div>
                                    
                                    <div class="my-2">
                                        <div class="grid grid-rows-1">
                                            <p class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Jenis Identitas</p>
                                            <div class="flex gap-1">
                                                <div class="w-1/4">
                                                    {{-- 
                                                    <input type="text" name="first_name" id="text"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="No Tetepon" value=""> 
                                                    --}}
                                                    <select name="id_card_type" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                                        <option value="1" {{isset($userdetail->ID_card_type) ? ($userdetail->ID_card_type == 1 ? 'selected' : '') : ''}}>SIM</option>
                                                        <option value="2" {{isset($userdetail->ID_card_type) ? ($userdetail->ID_card_type == 2 ? 'selected':'') : ''}}>KTP</option>
                                                    </select>
                                                    <div
                                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                                    </div>
                                                </div>
                                                <div>
                                                    <input type="text" name="identity_card_number" id="text"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-64 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="No Indentitas" value="{{isset($identity_card_number) ? $userdetail->identity_card_number: null}}">
                                                    <div
                                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div class="my-2">
                                    </div>
                                    <div class="my-2">
                                        <div class="col-end-3">
                                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">KTP/SIM</label>
                                            <div class="w-auto flex">
                                                @if (!isset($userdetail->ID_card_photo))
                                                <img src="{{ asset('storage/img/logo-traveller.png') }}"
                                                    alt="{{ auth()->user()->first_name }}"
                                                    class="object-cover object-center w-14 h-14">
                                                @else
                                                <img src="{{ asset($userdetail->ID_card_photo) }}"
                                                    alt="{{ auth()->user()->first_name }}"
                                                    class="object-cover object-center w-14 h-14">
                                                @endif
                                                <div class="grid grid-cols-1">
                                                    <p class="ml-4 pl-3 font-inter text-align w-full font-medium text-base">Foto KTP/SIM
                                                    </p>
                                                    <div for="upload"
                                                        class="ml-4 flex block w-full h-7 pt-1 text-[#23AEC1] text-xs bg-violet-50 hover:bg-violet-100 justify-center rounded-full">
                                                        <input class="btn-danger" name="id_card_photo" type="file"
                                                            id="upload" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit"
                                    class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                    Perbarui
                                </button>
                            </div>
                        </form>
                    </div>

                    {{-- Data dokumen --}}
                    <div class="bg-white my-5 rounded-md shadow-lg">
                        <form action="{{ route('seller.update.data.dokumen', auth()->user()->id) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="ml-12 p-6">
                                <p class="text-lg font-bold text-[#333333]">Dokumen</p>
                            </div>
                            <div class="ml-12 p-6">
                                @if ($doc_value->count() >0)
                                    @foreach ($doc_value as $value)
                                        @if (isset(auth()->user()->active_status) && auth()->user()->active_status == 'Confrimed')
                                        <p class="text-sm font-bold">{{$value['doc_name']}}:&nbsp;<b class="text-green-500">Terverifikasi</b>
                                        @else
                                        <p class="text-sm font-bold">{{$value['doc_name']}}:&nbsp;<b class="text-red-500">Belum Verifikasi</b>
                                        @endif
                                        <div class="my-5">
                                            <div class="w-auto flex">
                                                <img src="{{ isset($value['value']) ? asset($value['value']):asset('storage/icons/upload.svg') }}"
                                                alt="{{$value['doc_name']}}"
                                                class="object-cover object-center w-14 h-14">
                                                <div class="grid grid-cols-1">
                                                    <div for="upload"
                                                        class="ml-4 flex block w-full h-7 pt-1 text-[#23AEC1] text-xs bg-violet-50 hover:bg-violet-100 justify-center rounded-full">
                                                        {{-- <img src="{{ asset('storage/icons/ic-upload-blue.svg') }}"
                                                            alt="user-profile" class="w-3 h-3 mr-1"> --}}
                                                        <input class="btn-danger" name="{{$value['name']}}" type="file"
                                                        id="upload" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>    
                                    @endforeach
                                @else
                                   @foreach ($legal_doc as $legal)
                                        @if (isset(auth()->user()->active_status) && auth()->user()->active_status == 'Confrimed')
                                        <p class="text-sm font-bold">{{$legal['title']}}: <b class="text-green-500">Terverifikasi</b>
                                        @else
                                            @if ($legal['is_mandatory'] !=null)
                                            <p class="text-sm font-bold">{{$legal['title']}}:&nbsp;<b class="text-red-500">*{{$legal['is_mandatory']}}</b>&nbsp;<b class="text-red-500">Belum Verifikasi</b>
                                            @else
                                            <p class="text-sm font-bold">{{$legal['title']}}:&nbsp;<b class="text-red-500">Belum Verifikasi</b>
                                            @endif
                                        @endif
                                        <div class="my-5">
                                            <div class="w-auto flex">
                                                <img src="{{ asset('sstorage/icons/upload.svg') }}"
                                                    alt="legal document"
                                                    class="object-cover object-center w-14 h-14">
                                                <div class="grid grid-cols-1">
                                                    <div for="upload"
                                                        class="ml-4 flex block w-full h-7 pt-1 text-[#23AEC1] text-xs bg-violet-50 hover:bg-violet-100 justify-center rounded-full">
                                                        {{-- <img src="{{ asset('storage/icons/ic-upload-blue.svg') }}"
                                                            alt="user-profile" class="w-3 h-3 mr-1"> --}}
                                                        <input class="btn-danger" name="{{$legal['name']}}" type="file"
                                                        id="upload" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <button type="submit"
                                        class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                        Perbarui
                                    </button>
                                @endif
                            </div>
                        </form>
                    </div>
                    {{-- Alamat --}}
                    {{-- <div class="bg-white my-5 rounded-md shadow-lg">
                        <form action="{{ route('seller.update.alamat', auth()->user()->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="ml-12 p-6">
                                <p class="text-lg font-bold text-[#333333]">Alamat</p>
                                <p class="text-sm font-bold">Status: <b class="text-red-500">Belum Terverifikasi</b></p>
                                <div class="grid grid-cols-2">
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Alamat</label>
                                        <input type="text" name="address" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Alamat" value="{{ $userdetail->address }}">
                                    </div>
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Provinsi</label>
                                        <select name="province_id" id="provinsi"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                            <option selected disabled>Pilih Provinsi</option>
                                            @foreach ($province as $provinsi)
                                            <option value="{{ $provinsi->id }}">{{ $provinsi->name }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kota/Kabupaten</label>
                                        <select name="regency_id" id="kabupaten"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                            <option selected disabled>Pilih Kota/Kabupaten</option>
                                        </select>
                                    </div>
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kecamatan</label>
                                        <select name="district_id" id="kecamatan"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                            <option selected disabled>Pilih Kecamatan</option>
                                        </select>
                                    </div>
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kelurahan</label>
                                        <select name="village_id" id="kelurahan"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                            <option selected disabled>Pilih Kelurahan</option>
                                        </select>
                                    </div>
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kode
                                            Pos</label>
                                        <input type="text" name="postal_code" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Kode Pos" value="{{ $userdetail->postal_code }}">
                                    </div>
                                </div>

                                <button type="submit"
                                    class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                    Perbarui
                                </button>
                            </div>
                        </form>
                    </div> --}}

                    {{-- Identitas --}}
                    {{-- <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                        <div class="ml-12 px-6 pt-6">
                            <div class="flex grid grid-cols-2 justify-center items-center">
                                <div class="py-2.5">
                                    <p class="text-lg font-bold font-inter text-[#333333]">Identitas</p>
                                </div>
                            </div>
                            <p class="text-sm font-bold">Status: <b class="text-red-500">Belum Verifikasi</b></p>
                        </div>
                        <form action="{{ route('seller.update.identitas', auth()->user()->id) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="grid grid-cols-2">
                                <div class="ml-12 p-6">
                                    <div class="my-2">
                                        <label for="text"
                                            class="block mb-2 text-md font-bold text-[#333333] dark:text-gray-300">Kartu
                                            Identitas</label>
                                        <select name="ID_card_type"
                                            class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option {{ $userdetail->ID_card_type == 'KTP' ? 'selected' : '' }}
                                                value="KTP">KTP</option>
                                            <option {{ $userdetail->ID_card_type == 'KITAS' ? 'selected' : '' }}
                                                value="KITAS">KITAS</option>
                                        </select>
                                        <div
                                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                        </div>
                                    </div>
                                    <div class="my-2">
                                        <input type="text" name="identity_card_number" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-dark-400 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="1234567895421" value="{{ $userdetail->identity_card_number }}">
                                    </div>
                                    <p class="text-sm text-[#333333] font-reguler">Pastikan unggahan Anda jelas untuk
                                        memudahkan proses verifikasi kami. Ketidakjelasan akan menyebabkan proses verifikasi
                                        lebih lama dan dapat menyebabkan tertundanya pengaktifan akun Anda.</p>
                                </div>
                                <div class="mr-12 p-6">
                                    <div x-data="displayImage()">
                                        <div class="mb-2">
                                            <template x-if="imageUrl">
                                                <img :src="imageUrl"
                                                    class="object-contain rounded border border-gray-300 w-full h-56">
                                            </template>

                                            <template x-if="!imageUrl">
                                                <div
                                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-full h-56">
                                                    @if ($userdetail->ID_card_photo == null)
                                                    <img src="{{ asset('storage/icons/upload.svg') }}"
                                                        alt="Foto Kartu Identitas" width="200px" height="20px">
                                                    @else
                                                    <img src="{{ asset($userdetail->ID_card_photo) }}"
                                                        alt="Foto Kartu Identitas" class="w-full h-55">
                                                    @endif
                                                </div>
                                            </template>

                                            <input class="mt-2" type="file" name="ID_card_photo" accept="image/*"
                                                @change="selectedFile">
                                        </div>
                                    </div>
                                </div>
                                <div class="ml-12 p-6">
                                    <button type="submit"
                                        class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                        Perbarui
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div> --}}
                </div>
                {{-- Email --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                    <div class="ml-12 p-6">
                        <div class="flex grid grid-cols-2 justify-center items-center">
                            <div class="py-2.5">
                                <p class="text-lg font-bold font-inter text-[#333333]">Email</p>
                            </div>
                            <div
                                class="col-start-5 col-end-7 px-6 py-2 rounded-lg bg-[#23AEC1] items-center justify-center mt-5 mx-5">
                                @if (isset($userdetail->second_email) && $userdetail->second_email == null)
                                <button type="button" class="text-[#FFFFFF] text-sm" id="openModal">
                                    Tambah Email
                                </button>
                                @else
                                <a href="#" class="text-[#FFFFFF] text-sm" disabled="disabled">Tambah
                                    Email</a>
                                @endif
                            </div>
                        </div>
                        @if (auth()->user()->email_verified_at == null)
                        <p class="text-sm font-bold">Status: <b class="text-red-500">Belum Verifikasi</b></p>
                        @else
                        <p class="text-sm font-bold">Status: <b class="text-green-500">Terverifikasi</b></p>
                        @endif
                        <div class="font-inter font-semibold text-sm pt-2">Kamu boleh memasukkan maksimal 2 email</div>
                        <div class="flex grid grid-cols-4">
                            <div class="font-inter">1. {{ auth()->user()->email }}</div>
                            <div class="col-start-2 col-span-2">
                                <div class="font-inter text-[#D50006] grid grid-cols-2 gap-5">
                                    <a href="#" class="text-xs hover:text-[#D50006] hover:font-bold">Email ini
                                        yang akan digunakan untuk notifikasi</a>
                                    <img src="{{ asset('storage/icons/circle-minus-red.svg') }}"
                                        class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                                </div>
                            </div>
                        </div>
                        @if (isset($userdetail->second_email) && $userdetail->second_email != null)
                        <div class="flex grid grid-cols-4">
                            <div class="font-inter">2. {{ $userdetail->second_email }}</div>
                            <div class="col-start-2 col-span-2">
                                <div class="font-inter text-[#D50006] grid grid-cols-2 gap-5">
                                    <a href="#" class="text-xs hover:text-[#D50006] hover:font-bold"></a>
                                    <img src="{{ asset('storage/icons/circle-minus-red.svg') }}"
                                        class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal" id="emailModal">
                    <div class="modal-content">
                        <span class="close">&times;</span>
                        <form action="{{route('seller.update.second.email', auth()->user()->id)}}" method="post"
                            id="emailForm">
                            @csrf
                            @method('PUT')
                            <h2>Masukkan Second Email Anda</h2>
                            <div class="form-group mb-3">
                                <input type="email" id="second_email" name="second_email"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="example@gmail.com" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Kirim</button>
                        </form>
                    </div>
                </div>

                {{-- Nomor Telepon --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                    <div class="ml-12 p-6">
                        <div class="flex grid grid-cols-2 justify-center items-center">
                            <div class="py-2.5">
                                <p class="text-lg font-bold font-inter text-[#333333]">Hp atau WhatsApp</p>
                            </div>
                            <div
                                class="col-start-5 col-end-7 px-6 py-2 rounded-lg bg-[#23AEC1] items-center justify-center mt-5 mx-5">
                                @if (isset($userdetail->second_phone_number) && $userdetail->second_phone_number == null)
                                <a href="#" class="text-[#FFFFFF] text-sm">Tambah Nomor</a>
                                @else
                                <a href="#" class="text-[#FFFFFF] text-sm" disabled="disabled">Tambah
                                    Nomor</a>
                                @endif
                            </div>
                        </div>
                        <p class="text-sm font-bold">Status: <b class="text-red-500">Belum Verifikasi</b></p>
                        <div class="font-inter font-semibold text-sm pt-2">Kamu boleh memasukkan maksimal 2 nomor
                            telepon</div>
                        <div class="flex grid grid-cols-4">
                            <div class="font-inter">1. {{ auth()->user()->no_tlp }}</div>
                            <div class="col-start-2 col-span-2">
                                <div class="font-inter text-[#D50006] grid grid-cols-2 gap-5">
                                    <a href="#" class="text-xs hover:text-[#D50006] hover:font-bold">Nomor HP
                                        ini yang akan digunakan untuk notifikasi</a>
                                    <img src="{{ asset('storage/icons/circle-minus-red.svg') }}"
                                        class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        
    </script>
    <script>
        // Get the modal
        var modal = document.getElementById("emailModal");

        // Get the button that opens the modal
        var btn = document.getElementById("openModal");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal 
        // btn.onclick = function() {
        //     modal.style.display = "block";
        // }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>
    <script>
        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                    this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                    if (!event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },
            }
        }
    </script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script>
        $(function(){
            let seller_type="{!! auth()->user()->jenis_registrasi !!}"
            console.log(seller_type) 

            if(seller_type==='personal'){
                
                window.onload = function() {
                    $("#personal").show();
                };

            }else{
                
                window.onload = function() {
                    $("#company").show();
                };
            }

            $("input[name='jenis_toko']").on("click", function(){
                if($(this).val()=='unofficial store'){
                    $("#data_cabang").addClass("hidden")
                }else{
                    $("#data_cabang").removeClass("hidden")
                }
            })

            console.log($("input[name='jenis_toko']:checked").val())
            if($("input[name='jenis_toko']:checked").val()=='unofficial store'){
                $("#data_cabang").addClass("hidden")
            }else if($("input[name='jenis_toko']:checked").val()=='official store'){
                $("#data_cabang").removeClass("hidden")
            }else{
                $("#data_cabang").addClass("hidden")
            }

            $('#datepicker').datepicker();
            
            $(".gj-datepicker-md [role='right-icon']").css({
                 position: 'absolute',
                right: '80px',
                top: '8px', 
            })
            // $('#datepicker').css({
            //     position: 'absolute',
            //     right: '80px',
            //     top: '8px',
            // });

            // let store_type = "{!! isset($userdetail->store_type) ? $userdetail->store_type : null !!}"
            // console.log(store_type)

            // if(store_type=='unofficial store'){
            //     window.onload = function() {
            //         $("#data_cabang").addClass("hidden")
            //     }
            // }else{
            //     window.onload = function() {
            //         $("#data_cabang").removeClass("hidden")
            //     }
            // }
        });

        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(function() {
                // provinsi
                $('#provinsi').on('change', function() {
                    let id_provinsi = $('#provinsi').val();
                    console.log(id_provinsi);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getkabupaten') }}",
                        data: {
                            id_provinsi: id_provinsi
                        },
                        cache: false,

                        success: function(msg) {
                            $('#kabupaten').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })

                //kabupaten
                $('#kabupaten').on('change', function() {
                    let id_kabupaten = $('#kabupaten').val();
                    console.log(id_kabupaten);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getkecamatan') }}",
                        data: {
                            id_kabupaten: id_kabupaten
                        },
                        cache: false,

                        success: function(msg) {
                            $('#kecamatan').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })
                // Kelurahan
                $('#kecamatan').on('change', function() {
                    let id_kecamatan = $('#kecamatan').val();
                    console.log(id_kecamatan);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getdesa') }}",
                        data: {
                            id_kecamatan: id_kecamatan
                        },
                        cache: false,

                        success: function(msg) {
                            $('#kelurahan').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })
            })
        });

        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            if($("#provinsi").val()!=' '){
                let id_kabupaten = '{!! isset($userdetail->regency_id) ? $userdetail->regency_id:'' !!}';
                let id_provinsi = $("#provinsi").val();


                $.ajax({
                    method:'POST',
                    url:"{{route('getkabupaten')}}",
                    data:{
                        id_provinsi:id_provinsi,
                        id_kabupaten:id_kabupaten,
                    },
                    cache:false,
                    success:function(msg){
                        $("#kabupaten").html(msg)   
                    },
                    error:function(data){
                        console.log('error:',data)
                    }
                })
            }

            if($("#kabupaten").val()!= ' '){
                // let id_
                console.log($("#kabupaten").val())
                $.ajax({
                    method:'POST',
                    url:"{{route('getkecamatan')}}",
                    data:{
                        id_kabupaten:'{!! isset($userdetail->regency_id) ? $userdetail->regency_id:'' !!}',
                        id_kecamatan:'{!! isset($userdetail->district_id) ? $userdetail->district_id:'' !!}',
                    },
                    cache:false,
                    success:function(msg){
                        $('#kecamatan').html(msg)
                    },
                    error:function(data){
                        console.log('error:',data)
                    }
                })
            }

            if($("#kecamatan").val()!= ' '){
                $.ajax({
                    method:'POST',
                    url:"{{route('getdesa')}}",
                    data:{
                        id_kecamatan:'{!! isset($userdetail->district_id) ? $userdetail->district_id:'' !!}',
                        id_kelurahan:'{!! isset($userdetail->village_id) ? $userdetail->village_id:'' !!}',
                    },
                    cache:false,
                    success:function(msg){
                        $('#kelurahan').html(msg)
                    },
                    error:function(data){
                        console.log('error:',data)
                    }
                })
            }

            $("#datepicker").val('{!! isset($userdetail->date_of_establishment) ? \Carbon\Carbon::createFromFormat('Y-m-d',$userdetail->date_of_establishment)->format("d/m/Y") : null !!}')
            // $("#kabupaten").val('{!! isset($userdetail->regency_id) ? $userdetail->regency_id:'' !!}')
            // $("#kecamatan").val('{!! isset($userdetail->district_id) ? $userdetail->district_id:'' !!}')
            // $("#kalurahan").val('{!! isset($userdetail->village_id) ? $userdetail->village_id:'' !!}')
        })
    </script>
</body>

</html>