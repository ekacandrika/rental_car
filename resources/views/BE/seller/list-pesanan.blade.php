<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #report:checked+#report {
            display: block;
        }



        .menu-dropdown li a {
            display: inline-block;
            color: white;
            /* text-align: center; */
            text-decoration: none;
        }

        .menu-dropdown li a:hover {
            background-color: none;
        }

        li.dropdown {
            display: inline-block;
            margin-right: 80px
        }

        .dropdown:hover .isi-dropdown {
            display: block;
        }

        .isi-dropdown a:hover {
            color: #fff !important;
            width: 100%;
        }

        .isi-dropdown {
            position: absolute;
            display: none;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
            background-color: #f9f9f9;
            width: 100%;
        }

        .isi-dropdown a {
            color: #3c3c3c !important;
            padding: 1%;
        }

        .isi-dropdown a:hover {
            color: #232323 !important;
            background: #f3f3f3 !important;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">

    {{-- Navbar --}}
    <x-be.seller.navbar-seller></x-be.seller.navbar-seller>

    {{-- Sidebar --}}
    <x-be.seller.sidebar-seller></x-be.seller.sidebar-seller>

    <div class="col-start-3 col-end-11 z-0">
        {{-- List data booking --}}
        <div class="p-5 pr-10" style="padding-left: 315px">
            <div class="flex items-center p-2 font-bold text-dark">
                <img src="{{ asset('storage/icons/cart-shopping-solid (1).svg') }}" class="w-[16px] h-[16px] mt-1"
                    alt="user" title="user">
                <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">List Pesanan</p>
            </div>

            <div class="place-items-end grid">
                <a href="{{route('confirmListPesanan')}}">
                    <div class="bg-kamtuu-second rounded-lg w-40 h-12">
                        <p class="text-white font-semibold text-center pt-3">Confirm Order</p>
                        @if($count !== 0)
                        <div
                            class=" animate-bounce rounded-full bg-kamtuu-primary w-6 h-6 grid justify-items-center -mt-10">
                            <p class="text-white animate-bounce text-center">{{$count}}</p>
                        </div>
                        @elseif($count >= 99)
                        <div
                            class=" animate-bounce rounded-full bg-kamtuu-primary w-6 h-6 grid justify-items-center -mt-10">
                            <p class="text-white animate-bounce text-center">99</p>
                        </div>
                        @else
                        <div class="invisible">
                        </div>
                        @endif
                    </div>
                </a>
            </div>

            {{-- body --}}
            <div class="max-w-6xl bg-gray-200 rounded-md border border-black p-2 my-5" x-data="booking_datas()">
                <div class="grid grid-cols-2">
                    <div class="grid justify-items-start mb-5">
                    </div>
                    <div class="grid justify-items-end">
                        <div class="pt-2 relative text-gray-600">
                            <input
                                class="border-2 border-gray-300 bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none"
                                type="search" name="search" placeholder="Search" @keyup="searchOrder()" id="search-order">
                            <button type="submit" class="absolute right-0 top-0 mt-5 mr-4">
                                <svg class="text-gray-600 h-4 w-4 fill-current" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px"
                                    viewBox="0 0 56.966 56.966" style="enable-background:new 0 0 56.966 56.966;"
                                    xml:space="preserve" width="512px" height="512px">
                                    <path
                                        d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
                <div>
                    <template x-if="bookings.length >= 1">
                        <template x-for="(booking, index) in bookings" :key="index">
                        <div class="bg-kamtuu-produk-warning bg-opacity-25 p-3 rounded-md border border-gray-400 my-5">

                            <div class="grid grid-cols-12">
                                {{-- <template x-for="booking.productdetail"> --}}
                                {{-- Cols 2 --}}
                                <template x-if="booking.productdetail.thumbnaill">
                                    <img class="w-28 h-28 rounded-md border border-gray-300"
                                        :src="booking.productdetail.thumbnail"
                                        alt="">
                                </template>
                                <template x-if="booking.productdetail.thumbnail == null">
                                    <img class="w-28 h-28 rounded-md border border-gray-300"
                                        src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                        alt="">
                                </template>
                                {{-- Cols 3 {{ $produk->product_name }} {{ $detail_mobil->kapasitas_kursi }} {{ $detail_mobil->kapasitas_koper }} --}}
                                <div class="m-5 col-start-3 col-span-5">
                                    <p>Activity</p>
                                    <p class="text-lg font-bold" x-text="booking.product.product_name"></p>
                                    <div class="flex py-2 gap-1 lg:gap-2">
                                        <div
                                            class="bg-[#9E3D64] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                            Transfer</div>
                                        <div
                                            class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                            Sedan</div>
                                    </div>
                                    <template x-if="booking.product.type=='Transfer'">
                                        <div class="flex py-2 gap-1 lg:gap-2">
                                            <div
                                                class="bg-[#9E3D64] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                                Transfer
                                            </div>
                                            <div
                                                class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                                Sedan
                                            </div>
                                        </div>
                                        <div class="flex">
                                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}"
                                                alt="rating" width="17px" height="17px">
                                            <p class="font-bold text-[8px] lg:text-sm mr-2">Seats</p>
                                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}"
                                                alt="rating" width="17px" height="17px">
                                            <p class="font-bold  text-[8px] lg:text-sm mr-2">Koper</p>
                                        </div>
                                    </template>
                                    <template x-if="booking.product.type=='Rental'">
                                        <div class="flex py-2 gap-1 lg:gap-2">
                                            <div
                                                class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                                Sedan
                                            </div>
                                        </div>
                                        <div class="flex">
                                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}"
                                                alt="rating" width="17px" height="17px">
                                            <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}"
                                                alt="rating" width="17px" height="17px">
                                            <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                                        </div>
                                    </template>
                                    {{-- <div class="flex">
                                        <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}"
                                            alt="rating" width="17px" height="17px">
                                        <p class="font-bold text-[8px] lg:text-sm mr-2">Seats</p>
                                        <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}"
                                            alt="rating" width="17px" height="17px">
                                        <p class="font-bold  text-[8px] lg:text-sm mr-2">Koper</p>
                                    </div> --}}
                                    <template x-if="booking.activity_date != null">
                                    <div class="flex">
                                        <p class="text-gray-600" x-text="booking.activity_date"></p>
                                    </div>
                                    </template>
                                    <div class="pt-8 flex gap-2 items-center">
                                        <template x-if="booking.activity_date != null">
                                            <p class="text-gray-600" x-text="'1 Tiket Dewasa . ' + booking.activity_date">1 Tiket Dewasa</p>
                                        </template>
                                        <template x-if="booking.activity_date == null">
                                        <p class="text-gray-600">1 Tiket Dewasa</p>
                                        </template>
                                    </div>
                                </div>
                                {{-- Cols 4 --}}
                                <div class="col-span-4">
                                    <p  x-text="'Kode Booking: ' + booking.booking_code">Kode Booking:</p>
                                    <br>
                                    <p x-text="'Status Pembayaran: '+ booking.status_pembayaran">Status Pembayaran:</p>
                                    <p x-text="'Tanggal Checkout: '+ booking.tgl_checkout">Tanggal Checkout:
                                        {{-- {{ Carbon\Carbon::parse($item->tgl_checkout)->translatedFormat('d F Y') }} --}}
                                    </p>
                                    <p class="text-blue-600 text-2xl" x-text="'IDR: '+ booking.total_price">IDR 
                                        {{-- {{ number_format($item->total_price) }} --}}
                                        {{-- {{ Carbon\Carbon::parse($item->tgl_checkout)->translatedFormat('Y-m-d') }}   --}}
                                        {{-- {{ $item->status_pembayaran }} --}}
                                    </p>
                                    <p class="text-gray-500">(Diskon IDR 20,000)</p>
                                                                <div class="flex gap-2 items-center">
                                        <form action="{{ route('storeBatalkanPesanan') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="user_id" :value="booking.users.user_id">
                                            <input type="hidden" name="product_id" :value="booking.product.id">
                                            <input type="hidden" name="toko_id" :value="booking.toko_id">
                                            <input type="hidden" name="harga" :value="booking.total_price">
                                            <input type="hidden" name="data_booking_id" :value="booking.id">
                                            <input type="hidden" name="tgl_checkout"
                                                :value="booking.tgl_checkout_orig">
                                            <input type="hidden" name="status_pembayaran"
                                                :value="booking.status_pembayaran">
                                            <button type="submit"
                                                class="items-center px-px py-1 lg:py-2 lg:w-40 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">Tolak     Pesanan
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </template>
                    </template>
                </div>
            </div>
        </div>

        {{-- List request Pembatalan dari traveller --}}
        <div class="p-5 pr-10" style="padding-left: 315px">
            <div class="flex items-center p-2 font-bold text-dark">
                <img src="{{ asset('storage/icons/cart-shopping-solid (1).svg') }}" class="w-[16px] h-[16px] mt-1"
                    alt="user" title="user">
                <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">List
                    Pembatalan</p>
            </div>

            {{-- body --}}
            <div class="max-w-6xl bg-gray-200 rounded-md border border-black p-2 my-5" x-data="reject_datas()">
                <div class="grid grid-cols-2">
                    <div class="grid justify-items-start mb-5">
                    </div>
                    <div class="grid justify-items-end">
                        <div class="pt-2 relative text-gray-600">
                            <input
                                class="border-2 border-gray-300 bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none"
                                type="search" name="search" placeholder="Search" id="reject-order-search" @keyup="searchReject()">
                            <button type="submit" class="absolute right-0 top-0 mt-5 mr-4">
                                <svg class="text-gray-600 h-4 w-4 fill-current" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px"
                                    viewBox="0 0 56.966 56.966" style="enable-background:new 0 0 56.966 56.966;"
                                    xml:space="preserve" width="512px" height="512px">
                                    <path
                                        d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
                <template x-if="rejects.length >= 1">
                    <template x-for="(reject, index) in rejects" :key="index">
                        <div class="bg-kamtuu-produk-warning bg-opacity-25 p-3 rounded-md border border-gray-400 my-5">
                            <div class="grid grid-cols-12">
                                {{-- Cols 2 --}}
                                <div class="col-span-2 m-5">
                                    <template x-if="reject.detail.thumbnail">
                                        <img class="w-28 h-28 rounded-md border border-gray-300"
                                            :src="reject.detail.thumbnail"
                                            alt="">
                                    </template>    
                                    <template x-if="reject.detail.thumbnail==null">
                                        <img class="w-28 h-28 rounded-md border border-gray-300"
                                            src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                            alt="">
                                    </template>         
                                </div>
                                {{-- Cols 3 --}}
                                {{-- {{ $produk->product_name }} --}}
                                <div class="m-5 col-start-3 col-span-5">
                                    <p>Activity</p>
                                    <p class="text-lg font-bold" x-text="reject.detail.product_name"></p>
                                    <template x-if="reject.detail.type=='Transfer'">
                                        <div class="flex py-2 gap-1 lg:gap-2">
                                            <div class="bg-[#9E3D64] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                                Transfer
                                            </div>
                                            <div class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                                Sedan
                                            </div>
                                        </div>
                                        <div class="flex">
                                        {{-- {{ $detail_mobil->kapasitas_kursi }}  --}}
                                        {{-- {{ $detail_mobil->kapasitas_koper }}  --}}
                                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}"
                                                alt="rating" width="17px" height="17px">
                                            <p class="font-bold text-[8px] lg:text-sm mr-2" x-text="reject.detail.kapasitas_kursi+' Seat'">Seats</p>
                                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}"
                                                alt="rating" width="17px" height="17px">
                                            <p class="font-bold  text-[8px] lg:text-sm mr-2" x-text="reject.detail.kapasitas_koper+' Koper'">Koper</p>
                                        </div>
                                    </template>
                                    <template x-if="reject.detail.type=='Rental'">
                                        <div class="flex py-2 gap-1 lg:gap-2">
                                            <div
                                                class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                                Sedan</div>
                                        </div>
                                        <div class="flex">
                                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}"
                                                alt="rating" width="17px" height="17px">
                                            <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}"
                                                alt="rating" width="17px" height="17px">
                                            <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                                        </div>
                                    </template>
                                    <template x-if="reject.activity_date">
                                    <div class="flex">
                                        <p class="text-gray-600">
                                            {{-- {{ Carbon\Carbon::parse($foo->activity_date)->translatedFormat('d F Y') }}  --}}
                                        </p>
                                    </div>
                                    </template>
                                    <div class="pt-8 flex gap-2 items-center">
                                        <template x-if="reject.activity_date">                                    
                                            <p class="text-gray-600" x-text="'1 Tiket Dewasa . '+reject.activity_date"></p>
                                        </template>
                                        <template x-if="reject.activity_date == null">                                    
                                            <p class="text-gray-600">1 Tiket Dewasa</p>
                                        </template>
                                    </div>
                                </div>
                                                        {{-- Cols 4 --}}
                                <div class="col-span-4">
                                    {{-- @if ($foo->kode_pengajuan != null) --}}
                                    <p x-text="'Kode Pengajuan: '+ reject.kode_pengajuan"> 
                                    {{-- {{ $foo->kode_pengajuan }} --}}
                                    </p>
                                    {{-- @else --}}
                                    <p x-text="'Kode Pembatalan: '+reject.kode_pembatalan">Kode Pembatalan: 
                                    {{-- {{ $foo->kode_pembatalan }} --}}
                                    </p>
                                    {{-- @endif --}}
                                    <br>
                                    <p style="font-size: 14px" x-text="'Status: '+ reject.status_pengajuan">Status: 
                                    {{-- {{ $foo->status_pengajuan }} --}}
                                    </p>
                                    {{-- @if ($foo->tgl_pengajuan != null) --}}
                                    <p x-text="'Tanggal Pengajuan: '+reject.tgl_pengajuan">Tanggal Pengajuan:
                                        {{-- {{ Carbon\Carbon::parse($foo->tgl_pengajuan)->translatedFormat('d F Y') }} --}}
                                    </p>
                                    {{-- @endif --}}
                                    <p class="text-blue-600 text-2xl" x-text="'IDR '+reject.harga">IDR 
                                    {{-- {{ number_format($foo->harga) }} --}}
                                    </p>
                                    {{-- @if ($foo->status_pengajuan == 'Proses') --}}
                                    {{-- {{ route('updateStatusPermintaanPembatalan', $foo->id) }} --}}
                                    {{-- x-bind:action="`master-titik/${data.id}`" --}}
                                    <div class="flex gap-2 items-center">
                                        <form x-bind:action="`/setujuiPermintaan/${reject.id}`" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <button type="submit"
                                                class="items-center px-px py-1 lg:py-2 lg:w-40 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-black bg-[#0ed500] rounded lg:rounded-lg">Setujui</button>
                                        </form>
                                    </div>
                                    {{-- @endif --}}
                                </div>
                            </div>
                        </div>
                    </template>
                </template>
            </div>
        </div>
    </div>

</body>
<script>
    document.addEventListener("alpine:init",()=>{
        Alpine.data("booking_datas",()=>({
            bookings:[],
            url:"{{route('list.order.seller')}}",
            async init(){
                let loader = await fetch(`${this.url}`)
                let json   = await loader.json()

                this.bookings = json.datas
                console.log(json.datas[0].product.type);
            },
            searchOrder(){
                let cari = $("#search-order").val();
                console.log(cari)

                fetch(`${this.url}?cari=${cari}`)
                .then(resp => resp.json())
                .then(d =>{
                    this.bookings = d.datas
                })
            }
        }))
    });

    document.addEventListener("alpine:init",()=>{
        Alpine.data("reject_datas",()=>({
            rejects:[],
            url:"{{route('list.reject.seller')}}",
            async init(){
                let loader = await fetch(`${this.url}`)
                let json   = await loader.json()

                this.rejects = json.datas
            },
            searchReject(){
                let cari = $("#reject-order-search").val();
                console.log(cari)

                fetch(`${this.url}?cari=${cari}`)
                .then(resp => resp.json())
                .then(d =>{
                    this.rejects = d.datas
                })
            }
        }))
    });
</script>
</html>