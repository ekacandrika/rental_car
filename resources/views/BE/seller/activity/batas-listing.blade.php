<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param||null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <div class="flex gap-2">
                    <button type="button"
                        class="text-[#333333] font-medium rounded-lg text-sm py-2 text-center inline-flex items-center mr-2 hover:text-[#23AEC1]">
                        <img src="{{ asset('storage/icons/chevron.svg') }}" alt="user-profile" class="w-5 h-5 mr-1">
                        <span class="text-lg font-bold font-inter text-[#333333]">{{ isset($isedit) ? "Edit Listing" :
                            "Tambah Listing"}}</span>
                    </button>
                </div>
                {{-- Breadcumbs --}}
                <nav class="flex py-3" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            <a href="{{ isset($isedit) ? route('activity.viewAddListingEdit',$id) : route('activity') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if (isset($activity['harga']))
                                <a href="{{ isset($isedit) ? route('activity.viewHargaEdit',$id) : route('activity.viewHarga') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] duration-200">Harga
                                    & Ketersediaan</a>
                                @else
                                <a href="{{ isset($isedit) ? route('activity.viewHargaEdit',$id) : route('activity.viewHarga') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#858585] hover:text-gray-500 duration-200 pointer-events-none">Harga
                                    & Ketersediaan</a>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{ isset($isedit) ? route('activity.viewBatasEdit',$id) : route('activity.viewBatas') }}"
                                    class="inline-flex items-center text-sm font-bold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if (isset($activity['peta']))
                                <a href="{{ isset($isedit) ? route('activity.viewPetaEdit',$id) : route('activity.viewPeta') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] duration-200">Peta
                                    & Foto</a>
                                @else
                                <a href="{{ isset($isedit) ? route('activity.viewPetaEdit',$id) : route('activity.viewPeta') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#858585] hover:text-gray-500 duration-200 pointer-events-none">Peta
                                    & Foto</a>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if (isset($activity['pilihan_ekstra']))
                                <a href="{{ isset($isedit) ? route('activity.viewPilihanEkstraEdit',$id) : route('activity.viewPilihanEkstra') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] duration-200">Pilihan
                                    & Extra</a>
                                @else
                                <a href="{{ isset($isedit) ? route('activity.viewPilihanEkstraEdit',$id) : route('activity.viewPilihanEkstra') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#858585] hover:text-gray-500 duration-200 pointer-events-none">Pilihan
                                    & Extra</a>
                                @endif
                            </div>
                        </li>
                    </ol>
                </nav>

                {{-- Section --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl p-6 px-10 rounded">
                    <div class="flex justify-between">
                        <div class="text-lg font-bold font-inter text-[#9E3D64]">
                            Batas Waktu Pembayaran & Pembatalan
                        </div>
                    </div>

                    <form
                        action="{{ isset($isedit) ? route('activity.addBatasWaktuPembayaranDanPembatalan.edit', $id) : route('activity.addBatasWaktuPembayaranDanPembatalan') }}"
                        method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="flex justify-items-center items-center align-middle my-2"
                            x-data="{batas_pembayaran: {{isset($batas['batas_pembayaran']) ? $batas['batas_pembayaran'] : 'null'}}}">
                            <span>Batas waktu pembayaran </span>
                            <input type="number" min="0" id="batas_pembayaran" name="batas_pembayaran"
                                x-model="batas_pembayaran" required
                                class="bg-[#FFFFFF] mx-2 border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-12 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="61">
                            <span>hari sebelum keberangkatan.</span>
                        </div>

                        <p class="text-xs text-[#D50006]">*Batas Waktu Pembayaran Lebih Lama Daripada Batas Waktu
                            Pembatalan
                        </p>

                        @if ($errors->hasBag('batas_waktu'))
                        <div class="bg-red-300 rounded-md p-3 my-5">
                            @foreach ($errors->batas_waktu->toArray() as $key=>$value)
                            @foreach ($value as $index=>$err)
                            <p>- {{ $err}}</p>
                            @endforeach
                            @endforeach
                        </div>
                        @endif

                        <div class="my-5" x-data="handler()">
                            <p class="pb-1">Kebijakan Pembatalan:</p>
                            <button type="button" class="text-white bg-[#23AEC1] py-1 px-3 rounded-md"
                                @click="addNewField()">Tambah</button>

                            {{-- Tabel --}}


                            <div class="my-7">
                                <table class="my-5 w-full border table-auto rounded-md">
                                    <thead>
                                        <tr>
                                            <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Hari Sebelumnya Sampai
                                            </th>
                                            <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Hari Sebelumnya</th>
                                            <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Potongan (%)</th>
                                            <th class="p-3 border border-slate-500 bg-[#BDBDBD]"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <template x-if="fields.length < 1">
                                            <div class="px-3">
                                                <p>Klik tombol Tambah di atas untuk menambahkan.</p>
                                            </div>
                                        </template>
                                        <template x-if="fields.length >= 1" class="col">
                                            <template x-for="(field, index) in fields" :key="index">
                                                <tr class="align-middle">
                                                    <td class="p-3 border border-slate-500 text-center">
                                                        <input type="number" id="kebijakan_pembatalan_sebelumnya"
                                                            :name="'fields['+index+'][kebijakan_pembatalan_sebelumnya]'"
                                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                            placeholder="5" min="0" required
                                                            x-model="field.kebijakan_pembatalan_sebelumnya">
                                                    </td>
                                                    <td class="p-3 border border-slate-500 text-center">
                                                        <input type="number" id="kebijakan_pembatalan_sesudah"
                                                            :name="'fields['+index+'][kebijakan_pembatalan_sesudah]'"
                                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                            placeholder="3" min="0" required
                                                            x-model="field.kebijakan_pembatalan_sesudah">
                                                    </td>
                                                    <td class=" p-3 border border-slate-500 text-center">
                                                        <input type="number" id="kebijakan_pembatalan_potongan"
                                                            :name="'fields['+index+'][kebijakan_pembatalan_potongan]'"
                                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                            placeholder="5" min="0" required
                                                            x-model="field.kebijakan_pembatalan_potongan">
                                                    </td>
                                                    <template x-if="index === (fields.length - 1)">
                                                        <td class="p-3 border border-slate-500 text-center">
                                                            <button class="mx-1">
                                                                <img src="{{ asset('storage/icons/trash-can-solid.svg') }}"
                                                                    alt="" width="16px" @click="removeField(index)">
                                                            </button>
                                                        </td>
                                                    </template>
                                                    <template x-if="index !== (fields.length - 1)">
                                                        <td class="p-3 border border-slate-500 text-center">
                                                            {{-- <button class="mx-1">
                                                                <img src="{{ asset('storage/icons/pencil-solid 1.svg') }}"
                                                                    alt="" width="16px" @click="addNewField()">
                                                            </button> --}}
                                                            <button class="mx-1">
                                                                <img src="{{ asset('storage/icons/trash-can-solid.svg') }}"
                                                                    alt="" width="16px" @click="removeField(index)">
                                                            </button>
                                                        </td>
                                                    </template>
                                                </tr>
                                            </template>
                                        </template>
                                    </tbody>
                                </table>
                            </div>

                            <x-be.com.two-button-dup>
                                {{ isset($isedit) ? route('activity.viewHargaEdit',$id) : route('activity.viewHarga') }}
                            </x-be.com.two-button-dup>

                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>

    <script>
        function handler() {
                    
            return {
                fields: {!! isset($batas['pembatalan']) ? $batas['pembatalan'] : "[]" !!},
                addNewField() {
                    this.fields.push({
                        kebijakan_pembatalan_sebelumnya:'',
                        kebijakan_pembatalan_sesudah:'',
                        kebijakan_pembatalan_potongan:''
                    });
                },
                removeField(index) {
                    this.fields.splice(index, 1);
                }
            }
        }
    </script>
</body>

</html>