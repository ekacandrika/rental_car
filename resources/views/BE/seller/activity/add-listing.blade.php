<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js" defer></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param||null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    <div class="grid grid-cols-10">
        {{--Sidebar--}}
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <div class="flex gap-2">
                    <button type="button"
                        class="text-[#333333] font-medium rounded-lg text-sm py-2 text-center inline-flex items-center mr-2 hover:text-[#23AEC1]">
                        <img src="{{ asset('storage/icons/chevron.svg') }}" alt="user-profile" class="w-5 h-5 mr-1">
                        <span class="text-lg font-bold font-inter text-[#333333]">{{ isset($isedit) ? "Edit Listing" :
                            "Tambah Listing"}}</span>
                    </button>
                </div>
                {{-- Breadcumbs --}}
                <nav class="flex py-3" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            <a href="{{ isset($isedit) ? route('activity.viewAddListingEdit',$id) : route('activity') }}"
                                class="inline-flex items-center text-sm font-bold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if (isset($activity['harga']))
                                <a href="{{ isset($isedit) ? route('activity.viewHargaEdit',$id) : route('activity.viewHarga') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] duration-200">Harga
                                    & Ketersediaan</a>
                                @else
                                <a href="{{ isset($isedit) ? route('activity.viewHargaEdit',$id) : route('activity.viewHarga') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#858585] hover:text-gray-500 duration-200 pointer-events-none">Harga
                                    & Ketersediaan</a>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if (isset($activity['batas']))
                                <a href="{{ isset($isedit) ? route('activity.viewBatasEdit',$id) : route('activity.viewBatas') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] duration-200">Batas
                                    Pembayaran & Pembatalan</a>
                                @else
                                <a href="{{ isset($isedit) ? route('activity.viewBatasEdit',$id) : route('activity.viewBatas') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#858585] hover:text-gray-500 duration-200 pointer-events-none">Batas
                                    Pembayaran & Pembatalan</a>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if (isset($activity['peta']))
                                <a href="{{ isset($isedit) ? route('activity.viewPetaEdit',$id) : route('activity.viewPeta') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] duration-200">Peta
                                    & Foto</a>
                                @else
                                <a href="{{ isset($isedit) ? route('activity.viewPetaEdit',$id) : route('activity.viewPeta') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#858585] hover:text-gray-500 duration-200 pointer-events-none">Peta
                                    & Foto</a>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if (isset($activity['pilihan_ekstra']))
                                <a href="{{ isset($isedit) ? route('activity.viewPilihanEkstraEdit',$id) : route('activity.viewPilihanEkstra') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] duration-200">Pilihan
                                    & Extra</a>
                                @else
                                <a href="{{ isset($isedit) ? route('activity.viewPilihanEkstraEdit',$id) : route('activity.viewPilihanEkstra') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#858585] hover:text-gray-500 duration-200 pointer-events-none">Pilihan
                                    & Extra</a>
                                @endif
                            </div>
                        </li>
                    </ol>
                </nav>
                {{-- @dump($add_listing); --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl p-6 px-10 rounded">
                    <div class="grid grid-cols-2">
                        <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Isi Informasi Dasar</div>
                        <div class="text-sm font-bold font-inter p-5 text-[#333333] text-right">ID Listing: {{ $listing
                            }}</div>
                    </div>

                    @if (count($errors) > 0)
                    <div class="bg-red-300 rounded-md p-3 my-2">
                        @foreach ($errors->getMessagebag()->toArray() as $key=>$value)
                        @foreach ($value as $index=>$err)
                        <p>- {{ $err}}</p>
                        @endforeach
                        @endforeach
                    </div>
                    @endif
                    <form
                        action="{{ isset($isedit) ? route('activity.addListing.edit', $id) : route('activity.addListing') }}"
                        method="POST" enctype="multipart/form-data" x-data="paketHandler()" id="add_listing_ID">
                        @csrf
                        @method('PUT')
                        {{-- <input type="hidden" x-model="fields" name="field[]"> --}}
                        <div class="px-5 mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Judul</label>
                            <input type="text" id="product_name" name="product_name" x-model="product_name"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Judul Activity" required>
                        </div>

                        {{-- Konfirmasi Paket --}}
                        <fieldset class="px-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Konfirmasi
                                Paket</label>
                            <div class="pb-5">
                                <div class="flex space-x-16">
                                    <div class="form-check">
                                        <input
                                            class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                            type="radio" name="confirmation_radio_button"
                                            x-model="confirmation_radio_button" id="instant" value="instant" required>
                                        <label class="form-check-label inline-block text-sm font-medium text-gray-800"
                                            for="instant">
                                            Instant </label>
                                    </div>
                                    <div class="form-check">
                                        <input
                                            class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                            type="radio" name="confirmation_radio_button"
                                            x-model="confirmation_radio_button" id="by_seller" value="by_seller"
                                            required>
                                        <label class="form-check-label inline-block text-sm font-medium text-gray-800"
                                            for="by_seller">
                                            Konfirmasi by Seller </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        {{-- Tag Lokasi --}}
                        <div class="block px-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi</label>
                            <div class="mt-1 rounded-md">
                                <select name="pulau" id="pulau" x-model="pulau"
                                    class="block w-1/3 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                    <option selected disabled>Pilih Pulau</option>
                                    @foreach ($pulau as $pulau_data)
                                    <option value="{{ $pulau_data->id }}" {{isset($add_listing['pulau_id']) ? ($add_listing['pulau_id']==$pulau_data->id ? 'selected' : null) : null}}>{{ $pulau_data->island_name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>

                            <div class="mt-1 rounded-md">
                                <select name="provinsi" id="provinsi" x-model="provinsi" required
                                    class="block w-1/3 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                    <option selected disabled>Pilih Provinsi</option>
                                    @foreach ($provinsi as $provinsi_data)
                                    <option value="{{ $provinsi_data->id }}" {{isset($add_listing['provinsi_id']) ? ($add_listing['provinsi_id']==$provinsi_data->id ? 'selected' : null) : null}}>{{ $provinsi_data->name }}</option>
                                    @endforeach
                                    {{-- <option value="{{ isset($add_listing['regency_id']) ? $add_listing['regency_id']
                                        : '' }}" selected>{{ isset($add_listing['regency_id']) ?
                                        $provinsi->name:"Pilih provinsi"}}</option> --}}
                                </select>
                            </div>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>

                            <div class="mt-1 rounded-md">
                                <select name="kabupaten" id="kabupaten" x-model="kabupaten" required
                                    class="block w-1/3 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                    <option selected disabled>Pilih Kota/Kabupaten</option>
                                    @foreach ($kabupaten as $kabupaten_data)
                                    <option value="{{ $kabupaten_data->id }}" {{isset($add_listing['kabupaten_id']) ? ($add_listing['kabupaten_id']==$kabupaten_data->id ? 'selected' : null) : null}}>{{ $kabupaten_data->name }}</option>
                                    @endforeach
                                    {{-- <option value="{{ isset($add_listing['district_id']) ? $add_listing['district_id']
                                        : '' }}" selected>{{ isset($add_listing['district_id']) ?
                                        $kecamatan->name:"Pilih Kecamatan"}}</option> --}}
                                </select>
                            </div>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>

                            <div class="mt-1 rounded-md">
                                <select name="objek" id="objek" x-model="objek" required
                                    class="block w-1/3 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                    <option selected disabled>Pilih Objek</option>
                                    @foreach ($objek as $objek_data)
                                    <option value="{{ $objek_data->id }}" {{isset($add_listing['objek_id']) ? ($add_listing['objek_id']==$objek_data->id ? 'selected' : null) : null}}>{{ $objek_data->title }}</option>
                                    @endforeach
                                    {{-- <option value="{{ isset($objek_data['village_id']) ? $add_listing['village_id']
                                        : '' }}" selected>{{ isset($add_listing['village_id']) ?
                                        $kelurahan->name:"Pilih Kelurahan"}}</option> --}}
                                </select>
                            </div>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>

                            <div x-data="dynamicTag()" class="mb-11" x-init="$nextTick(() => { select2WithAlpine() })">
                                <label for="text" class="block mb-2 mt-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis</label>
                                @if(isset($add_listing['new_tag_location']))
                                @php
                                    $new_tag_location = json_decode($add_listing['new_tag_location'],true);
                                    $arr_tag = isset($new_tag_location['results']) ? $new_tag_location['results']:$new_tag_location['result'];
                                    //dump($new_tag_location,$arr_tag);
                                @endphp
                                    @foreach ($arr_tag as $key => $new_tag)
                                        <div>
                                            {{-- <label for="text" class="block mb-2 mt-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis</label> --}}
                                            <div class="mt-1 rounded-md shadow-sm flex relative">
                                                <select name="new_tag_location[]" x-ref="select_new_tag"
                                                    class="block w-1/3 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                                    <option value='' selected>Pilih Lokasi</option>
                                                    @foreach ($dynamic_tag as $key => $tag)
                                                        @foreach ($tag as $pulau)
                                                        <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}" {{$new_tag == $pulau['id_pulau'] ? 'selected':''}}>{{ $pulau['nama_pulau']}}</option>
                                                            @foreach ($pulau['provinsi'] as $provinsi)
                                                                <option value="{{ $provinsi['id_provinsi'] }}" {{$new_tag == $provinsi['id_provinsi'] ? 'selected':''}}>{{ $provinsi['nama_provinsi']}}</option>
                                                                @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                    <option value="{{ $kabupaten['id_kabupaten']}}" {{$new_tag == $kabupaten['id_kabupaten'] ? 'selected':''}}>{{ $kabupaten['nama_kabupaten']}}</option>
                                                                    @if(isset($kabupaten['wisata']))
                                                                    @foreach ($kabupaten['wisata'] as $wisata)
                                                                        <option value="{{ $wisata['id_wisata']}}" {{$new_tag == $wisata['id_wisata'] ? 'selected':''}}>{{ $wisata['nama_wisata']}}</option>
                                                                    @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                                </select>
                                                {{-- <div class="absolute right-[595px] top-[7px]">
                                                        <button type="button" class="rounded-full">
                                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt="" width="25px" @click="removeField(index)">
                                                        </button>
                                                </div> --}}
                                            </div>
                                            <div
                                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                            </div>                                  
                                            </div>
                                    @endforeach
                                @else
                                <template x-if="tag_list.length == 0">
                                    <div>
                                        <div class="mt-1 rounded-md shadow-sm">
                                            {{-- name="kamar" id="kamar" x-model="attribute_room" x-ref="select_room" --}}
                                            <select name="new_tag_location" x-model="new_tag_location" x-ref="select_new_tag"
                                                class="new_tag_location block w-1/3 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                                <option value='' selected>Pilih Lokasi</option>
                                                @foreach ($dynamic_tag as $key => $tag)
                                                    @foreach ($tag as $pulau)
                                                    <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}">{{ $pulau['nama_pulau']}}</option>
                                                        @foreach ($pulau['provinsi'] as $provinsi)
                                                            <option value="{{ $provinsi['id_provinsi']}}">{{ $provinsi['nama_provinsi']}}</option>
                                                            @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                <option value="{{ $kabupaten['id_kabupaten']}}">{{ $kabupaten['nama_kabupaten']}}</option>
                                                                @if(isset($kabupaten['wisata']))
                                                                @foreach ($kabupaten['wisata'] as $wisata)
                                                                    <option value="{{ $wisata['id_wisata']}}">{{ $wisata['nama_wisata']}}</option>
                                                                @endforeach
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            </select>
                                        </div>
                                        <div
                                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                        </div>                                    
                                    </div>
                                </template>
                                @endif
                                <template x-if="tag_list.length >= 1">
                                    <template x-for="(tag, index) in tag_list">
                                        <div>
                                            {{-- <label for="text" class="block mb-2 mt-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis</label> --}}
                                            <div class="mt-1 rounded-md shadow-sm flex relative">
                                            <select name="new_tag_location[]" x-ref="select_new_tag"
                                                class="block w-1/3 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                                <option value='' selected>Pilih Lokasi</option>
                                                @foreach ($dynamic_tag as $key => $tag)
                                                    @foreach ($tag as $pulau)
                                                    <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}">{{ $pulau['nama_pulau']}}</option>
                                                        @foreach ($pulau['provinsi'] as $provinsi)
                                                            <option value="{{ $provinsi['id_provinsi']}}">{{ $provinsi['nama_provinsi']}}</option>
                                                            @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                <option value="{{ $kabupaten['id_kabupaten']}}">{{ $kabupaten['nama_kabupaten']}}</option>
                                                                @if(isset($kabupaten['wisata']))
                                                                @foreach ($kabupaten['wisata'] as $wisata)
                                                                    <option value="{{ $wisata['id_wisata']}}">{{ $wisata['nama_wisata']}}</option>
                                                                @endforeach
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            </select>
                                                <div class="absolute right-[595px] top-[7px]">
                                                    <button type="button" class="rounded-full">
                                                        <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt="" width="25px" @click="removeField(index)">
                                                    </button>
                                                </div>
                                            </div>
                                            <div
                                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                            </div>    
                                                                            
                                        </div>
                                    </template>
                                </template>
                                <div class="absolute py-2">
                                    <button type="button" class="flex bg-kamtuu-primary px-3 py-2 text-white text-sm rounded-lg mb-2" @click="add_list()">
                                        <img src="{{ asset('storage/icons/plus-solid.svg') }}" alt="" width="15px" style="color: #ed0202;">
                                        &nbsp;
                                        Tambah lokasi
                                    </button>
                                </div>
                            </div>
                        </div>

                        {{-- Paket --}}
                        <div class="p-5 space-y-3">
                            <p class="text-md text-[#333333] font-bold ">Paket</p>
                            <template x-if="fields.length >= 1" class="col">
                                <template x-for="(field, index) in fields" :key="index">

                                    <div>
                                        <div class="grid grid-cols-2">
                                            <div class="flex space-x-5 gap-4">
                                                <div class="">
                                                    <label for="nama_paket"
                                                        class="block mb-2 text-sm font-bold text-[#828282]">Judul
                                                        Paket</label>
                                                    <input type="text" :name="'fields['+index+'][nama_paket]'"
                                                        id="nama_paket" x-model="field.nama_paket"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="Activity Sehat Pagi" required>
                                                    {{-- <input type="hidden" name="fields[][nama_paket]"
                                                        x-model='field.nama_paket'> --}}
                                                    {{-- :name="'fields'+index+'nama_paket'" --}}
                                                </div>
                                                <div class="mb-6">
                                                    <label for="jam_paket"
                                                        class="block mb-2 text-sm font-bold text-[#828282]">Jam
                                                        Paket</label>
                                                    <div
                                                        class="bg-[#FFFFFF] border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2 dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                                        <input type="text" :id="id" x-data="{id:$id('time-picker')}"
                                                            x-model="field.jam_paket"
                                                            x-init="initTimePickerHandler(id, field.jam_paket)"
                                                            class="jam_paket bg-[#FFFFFF] border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                            :name="'fields['+index+'][jam_paket]'" readonly required>
                                                        {{-- <input type="hidden" name="fields[0][jam_paket]"
                                                            x-model='field.jam_paket'> --}}
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid grid-cols-2">
                                            <div class="flex space-x-5 gap-4">
                                                <div class="">
                                                    <label for="min_peserta"
                                                        class="block mb-2 text-sm font-bold text-[#828282]">Peserta
                                                        Min</label>
                                                    <input type="number" min="0" id="min_peserta"
                                                        :name="'fields['+index+'][min_peserta]'"
                                                        x-model=" field.min_peserta"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="3" required>
                                                    {{-- <input type="hidden" name="fields[]"
                                                        x-model='field.min_peserta'> --}}
                                                </div>

                                                <div class="">
                                                    <label for="max_peserta"
                                                        class="block mb-2 text-sm font-bold text-[#828282]">Peserta
                                                        Max</label>
                                                    <input type="number" min="0" id="max_peserta"
                                                        :name="'fields['+index+'][max_peserta]'"
                                                        x-model=" field.max_peserta"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="5" required>
                                                    {{-- <input type="hidden" name="fields[]"
                                                        x-model='field.max_peserta'> --}}
                                                </div>

                                                <template x-if="fields.length === 1" class="col">
                                                    <div class="flex items-end">
                                                        <button @click="addNewField()" type="button"
                                                            class="bg-[#23AEC1] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#21bbd0]">
                                                            Tambah </button>
                                                    </div>
                                                </template>

                                                <template x-if="fields.length > 1 && fields.length-1 !== index"
                                                    class="col">
                                                    <div class="flex space-x-5 gap-4">
                                                        <div class="flex items-end">
                                                            <button @click="removeField(index)" type="button"
                                                                class="bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]">
                                                                Hapus </button>
                                                        </div>
                                                </template>

                                                <template x-if="fields.length > 1 && fields.length-1 === index"
                                                    class="col">
                                                    <div class="flex space-x-5 gap-4">
                                                        <div class="flex items-end">
                                                            <button @click="removeField(index)" type="button"
                                                                class="bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]">
                                                                Hapus </button>
                                                        </div>

                                                        <div class="flex items-end">
                                                            <button @click="addNewField()" type="button"
                                                                class="bg-[#23AEC1] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#21bbd0]">
                                                                Tambah </button>
                                                        </div>
                                                    </div>
                                                </template>
                                            </div>
                                        </div>
                                    </div>


                                </template>

                            </template>
                        </div>

                        {{-- Ringkasan --}}
                        <div class="px-5 pt-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Ringkasan</label>
                            <textarea class="w-[10rem] click2edit" id="ringkasan" name="ringkasan" x-model="ringkasan"
                                x-init="$nextTick(() => { initSummerNote() })">
                            </textarea>
                            {{-- <input type="hidden" name="ringkasan" x-bind:value='this.ringkasan'> --}}
                            <div id="ringkasan_error" class="hidden my-3 w-full py-3 px-2 bg-red-300 rounded-md">
                                Ringkasan wajib diisi!</div>
                        </div>

                        {{-- Deskripsi --}}
                        <div class="px-5 pt-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Deskripsi
                                Activity</label>
                            <textarea class="w-[10rem] click2edit" id="deskripsi_wisata" name="deskripsi_wisata"
                                x-model="deskripsi_wisata" x-init="$nextTick(() => { initSummerNote() })">
                            </textarea>
                            <div id="deskripsi_wisata_error" class="hidden my-3 w-full py-3 px-2 bg-red-300 rounded-md">
                                Deskripsi Activity wajib
                                diisi!
                            </div>
                        </div>

                        {{-- Paket Termasuk --}}
                        <div class="px-5 pt-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Paket Termasuk</label>
                            <textarea class="w-[10rem] click2edit" id="paket_termasuk" name="paket_termasuk"
                                x-model="paket_termasuk" x-init="$nextTick(() => { initSummerNote() })">
                            </textarea>
                            <div id="paket_termasuk_error" class="hidden my-3 w-full py-3 px-2 bg-red-300 rounded-md">
                                Paket Termasuk wajib diisi!
                            </div>
                        </div>

                        {{-- Paket Tidak Termasuk --}}
                        <div class="px-5 pt-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Paket Tidak
                                Termasuk</label>
                            <textarea class="w-[10rem] click2edit" id="paket_tidak_termasuk" name="paket_tidak_termasuk"
                                x-model="paket_tidak_termasuk" x-init="$nextTick(() => { initSummerNote() })">
                            </textarea>
                            <div id="paket_tidak_termasuk_error"
                                class="hidden my-3 w-full py-3 px-2 bg-red-300 rounded-md">Paket Tidak Termasuk wajib
                                diisi!
                            </div>
                        </div>

                        {{-- Catatan --}}
                        <div class="px-5 pt-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Catatan
                                <span>(Optional)</span></label>
                            <textarea class="w-[10rem] click2edit" id="catatan" name="catatan" x-model="catatan"
                                x-init="$nextTick(() => { initSummerNote() })">
                            </textarea>
                        </div>

                        <x-be.com.one-button></x-be.com.one-button>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <script>
        const initSummerNote = () => {
            $('.click2edit').summernote({
                tabsize: 2,
                height: 120,
                toolbar: [
                    // ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    // ['table', ['table']],
                    // ['insert', ['link', 'picture', 'video']],
                    // ['view', ['fullscreen', 'codeview', 'help']]
                ],
                dialogsInBody: true
            });
        }

        $('#add_listing_ID').on('submit', function(e) {
            e.preventDefault();
            let ringkasan_error = false
            let deskripsi_wisata_error = false
            let paket_termasuk_error = false
            let paket_tidak_termasuk_error = false

            if ($('#ringkasan').summernote('isEmpty')) {
                document.getElementById("ringkasan_error").classList.remove("hidden");
                document.getElementById("ringkasan_error").classList.add("flex");
                ringkasan_error = true
            } else {
                document.getElementById("ringkasan_error").classList.remove("flex");
                document.getElementById("ringkasan_error").classList.add("hidden");
                ringkasan_error = false
            }

            if ($('#deskripsi_wisata').summernote('isEmpty')) {
                document.getElementById("deskripsi_wisata_error").classList.remove("hidden");
                document.getElementById("deskripsi_wisata_error").classList.add("flex");
                deskripsi_wisata_error = true
            } else {
                document.getElementById("deskripsi_wisata_error").classList.remove("flex");
                document.getElementById("deskripsi_wisata_error").classList.add("hidden");
                deskripsi_wisata_error = false
            }

            if ($('#paket_termasuk').summernote('isEmpty')) {
                document.getElementById("paket_termasuk_error").classList.remove("hidden");
                document.getElementById("paket_termasuk_error").classList.add("flex");
                paket_termasuk_error = true
            } else {
                document.getElementById("paket_termasuk_error").classList.remove("flex");
                document.getElementById("paket_termasuk_error").classList.add("hidden");
                paket_termasuk_error = false
            }

            if ($('#paket_tidak_termasuk').summernote('isEmpty')) {
                document.getElementById("paket_tidak_termasuk_error").classList.remove("hidden");
                document.getElementById("paket_tidak_termasuk_error").classList.add("flex");
                paket_tidak_termasuk_error = true
            } else {
                document.getElementById("paket_tidak_termasuk_error").classList.remove("flex");
                document.getElementById("paket_tidak_termasuk_error").classList.add("hidden");
                paket_tidak_termasuk_error = false
            }

            if (!ringkasan_error && !deskripsi_wisata_error && !paket_termasuk_error && !paket_tidak_termasuk_error) {
                this.submit();
            }
        })
        function checkTextArea(params) {
            area = document.getElementById("deskripsi_wisata").value
            // console.log(area)

            if (area === "<br>") {
                console.log('kosong')
            }

            if ($('#deskripsi_wisata').summernote('isEmpty')) {
                alert('editor content is empty');
            } else {
                // console.log('nggak')
            }
        }

        
        // 2023 05 24 -> Changes double quotation marks (" ") to single quotation marks (' ') from paketHandler ringkasan -> catatan
        function paketHandler() {
                    
            return {
                product_name: "{!! isset($add_listing['product_name']) ? $add_listing['product_name'] : "" !!}",
                confirmation_radio_button: "{!! isset($add_listing['confirmation']) ? $add_listing['confirmation'] : "" !!}",
                provinsi: {!! isset($add_listing['province_id']) ? $add_listing['province_id'] : "null" !!},
                kabupaten: {!! isset($add_listing['district_id']) ? $add_listing['district_id'] : "null" !!},
                kecamatan: {!! isset($add_listing['regemcy_id']) ? $add_listing['regemcy_id'] : "null" !!},
                kelurahan: {!! isset($add_listing['villahe_id']) ? $add_listing['kelurahan_id'] : "null" !!},
                fields: {!! isset($add_listing['paket']) ? $add_listing['paket'] : "[1]" !!},
                ringkasan: '{!! isset($add_listing['ringkasan']) ? $add_listing['ringkasan'] : "" !!}',
                deskripsi_wisata: '{!! isset($add_listing['deskripsi_wisata']) ? $add_listing['deskripsi_wisata'] : "" !!}',
                paket_termasuk: '{!! isset($add_listing['paket_termasuk']) ? $add_listing['paket_termasuk'] : "" !!}',
                paket_tidak_termasuk: '{!! isset($add_listing['paket_tidak_termasuk']) ? $add_listing['paket_tidak_termasuk'] : "" !!}',
                catatan: '{!! isset($add_listing['catatan']) ? $add_listing['catatan'] : "" !!}',
                addNewField() {
                    // let reg = /^[1-9]\d*$/

                    this.fields.push({
                        nama_paket: '',
                        jam_paket: '',
                        min_peserta: '',
                        max_peserta: ''
                    });
                },
                removeField(index) {
                    this.fields.splice(index, 1);
                },
                initTimePickerHandler(index, jam) {
                    Alpine.nextTick(() => { $('#'+index).timepicker({
                            value: jam
                        })})
                    this.$watch('fields', () => {
                        $('#'+index).timepicker()
                    })
                },
            }
        }

                function dynamicTag(){
            return{
                tag_list:[],
                new_tag_location:'',
                id:'',
                add_list(){
                    this.tag_list.push({
                        id:'',
                        label:''
                    })

                    console.log(this.tag_list)
                },
                removeField(index){
                    this.tag_list.splice(index,1)
                },
                select2WithAlpine(){
                    this.select2 = $(this.$refs.select_new_tag).select2();
                    //this.select2 = $(this.$refs.select_new_tag).select2();
                    /*
                    this.select2.on("select2:select", (event) => {
                        this.id = event.target.value;
                    });
                    this.$watch("new_tag_location", (value) => {
                        this.select2.val(value).trigger("change");
                    });
                    */
                }
            }
        }

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script>

        $("#pulau").select2();
        $("#provinsi").select2();
        $("#kabupaten").select2();
        $("#objek").select2();

        $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function() {
            // provinsi
           
            // $('#provinsi').on('change', function() {
            //     let id_provinsi = $('#provinsi').val();
            //     $('#kabupaten').val('');
            //     $('#kecamatan').val('');
            //     $('#kelurahan').val('');
            //     var option = $('<option></option>').attr("value", "").text("");
            //     $("#kecamatan").empty().append(option);
            //     $("#kelurahan").empty().append(option);

            //     $.ajax({
            //         type: 'POST',
            //         url: "{{ route('getkabupaten') }}",
            //         data: {
            //             id_provinsi: id_provinsi
            //         },
            //         cache: false,

            //         success: function(msg) {
            //             $('#kabupaten').html(msg);
            //         },
            //         error: function(data) {
            //             console.log('error:', data);
            //         },
            //     })
            // })

            // kabupaten
            // $('#kabupaten').on('change', function() {
            //     let id_kabupaten = $('#kabupaten').val();
            //     var option = $('<option></option>').attr("value", "").text("");
            //     $("#kelurahan").empty().append(option);

            //     $.ajax({
            //         type: 'POST',
            //         url: "{{ route('getkecamatan') }}",
            //         data: {
            //             id_kabupaten: id_kabupaten
            //         },
            //         cache: false,

            //         success: function(msg) {
            //             $('#kecamatan').html(msg);
            //         },
            //         error: function(data) {
            //             console.log('error:', data);
            //         },
            //     })
            // })

            // kecamatan
            // $('#kecamatan').on('change', function() {
            //     let id_kecamatan = $('#kecamatan').val();

            //     $.ajax({
            //         type: 'POST',
            //         url: "{{ route('getdesa') }}",
            //         data: {
            //             id_kecamatan: id_kecamatan
            //         },
            //         cache: false,

            //         success: function(msg) {
            //             $('#kelurahan').html(msg);
            //         },
            //         error: function(data) {
            //             console.log('error:', data);
            //         },
            //     })
            // })
        })
    });
    </script>
</body>

</html>