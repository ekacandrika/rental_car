<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param || null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <div class="flex gap-2">
                    <button type="button"
                        class="text-[#333333] font-medium rounded-lg text-sm py-2 text-center inline-flex items-center mr-2 hover:text-[#23AEC1]">
                        <img src="{{ asset('storage/icons/chevron.svg') }}" alt="user-profile" class="w-5 h-5 mr-1">
                        <span class="text-lg font-bold font-inter text-[#333333]">{{ isset($isedit) ? 'Edit Listing' :
                            'Tambah Listing' }}</span>
                    </button>
                </div>
                {{-- Breadcumbs --}}
                <nav class="flex py-3" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            <a href="{{ isset($isedit) ? route('activity.viewAddListingEdit',$id) : route('activity') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if (isset($activity['harga']))
                                <a href="{{ isset($isedit) ? route('activity.viewHargaEdit',$id) : route('activity.viewHarga') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] duration-200">Harga
                                    & Ketersediaan</a>
                                @else
                                <a href="{{ isset($isedit) ? route('activity.viewHargaEdit',$id) : route('activity.viewHarga') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#858585] hover:text-gray-500 duration-200 pointer-events-none">Harga
                                    & Ketersediaan</a>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if (isset($activity['batas']))
                                <a href="{{ isset($isedit) ? route('activity.viewBatasEdit',$id) : route('activity.viewBatas') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] duration-200">Batas
                                    Pembayaran & Pembatalan</a>
                                @else
                                <a href="{{ isset($isedit) ? route('activity.viewBatasEdit',$id) : route('activity.viewBatas') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#858585] hover:text-gray-500 duration-200 pointer-events-none">Batas
                                    Pembayaran & Pembatalan</a>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if (isset($activity['peta']))
                                <a href="{{ isset($isedit) ? route('activity.viewPetaEdit',$id) : route('activity.viewPeta') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] duration-200">Peta
                                    & Foto</a>
                                @else
                                <a href="{{ isset($isedit) ? route('activity.viewPetaEdit',$id) : route('activity.viewPeta') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#858585] hover:text-gray-500 duration-200 pointer-events-none">Peta
                                    & Foto</a>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{ isset($isedit) ? route('activity.viewPilihanEkstraEdit',$id) : route('activity.viewPilihanEkstra') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Pilihan
                                    & Extra</a>
                            </div>
                        </li>
                    </ol>
                </nav>

                {{-- Section --}}
                <form
                    action="{{ isset($isedit) ? route('activity.addPilihanEkstra.edit', $id) : route('activity.addPilihanEkstra') }}"
                    method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="bg-[#FFFFFF] drop-shadow-xl p-6 px-10 rounded">

                        @if ($errors->hasBag('pilihan_ekstra'))
                        <div class="bg-red-300 rounded-md p-3 my-5">
                            @foreach ($errors->pilihan_ekstra->toArray() as $key=>$value)
                            @foreach ($value as $index=>$err)
                            <p>- {{ $err}}</p>
                            @endforeach
                            @endforeach
                        </div>
                        @endif

                        {{-- Pilihan --}}
                        <div x-data="handler()">
                            <div class="pb-5">
                                <template x-if="fields.length < 1">
                                    <div class="">
                                        <div class="text-lg font-bold font-inter text-[#4F4F4F]"> Pilihan </div>
                                        <button class="text-white bg-[#23AEC1] mt-5 mb-5 py-2.5 px-8 rounded-md"
                                            @click="addNewField()">Tambah</button>
                                        <p>Klik tombol Tambah di atas untuk menambahkan.</p>
                                    </div>
                                </template>
                                <template x-if="fields.length >= 1" class="col">
                                    <template x-for="(field, index) in fields" :key="index">
                                        <div class="pb-5">
                                            <div class="text-lg font-bold font-inter text-[#4F4F4F]"> Pilihan <span
                                                    x-text="index+1"></span>
                                            </div>

                                            <div class="py-2">
                                                <div class="mb-3">
                                                    <input required
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-1/4 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        type="text" :name="'fields[' + index + '][judul_pilihan]'"
                                                        placeholder="Judul" id="judul_pilihan"
                                                        x-model="field.judul_pilihan">
                                                    {{-- <select
                                                        class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700  bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded-lg transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                                        aria-label="Default select example" id="judul_pilihan"
                                                        :name="'fields['+index+'][judul_pilihan]'"
                                                        x-model="field.judul_pilihan">
                                                        <option disabled>Pilihan</option>
                                                        <option value="hotel">Hotel</option>
                                                        <option value="asuransi">Asuransi</option>
                                                    </select> --}}
                                                </div>
                                                <div class="mb-3">
                                                    <input required
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-1/4 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        type="text" :name="'fields[' + index + '][deskripsi_pilihan]'"
                                                        placeholder="Deskripsi" id="deskripsi_pilihan"
                                                        x-model="field.deskripsi_pilihan">
                                                    {{-- <select
                                                        class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700  bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded-lg transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                                        aria-label="Default select example" id="deskripsi_pilihan"
                                                        :name="'fields['+index+'][deskripsi_pilihan]'"
                                                        x-model="field.deskripsi_pilihan">
                                                        <option disabled>Pilihan</option>
                                                        <option value="bintang_satu">Bintang 1</option>
                                                        <option value="bintang_dua">Bintang 2</option>
                                                        <option value="bintang_tiga">Bintang 3</option>
                                                        <option value="bintang_empat">Bintang 4</option>
                                                        <option value="bintang_lima">Bintang 5</option>
                                                    </select> --}}
                                                </div>
                                                <div class="flex my-2 gap-5">
                                                    <input type="number" id="text" required min="0"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-1/4 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="Harga" id="harga_pilihan"
                                                        :name="'fields[' + index + '][harga_pilihan]'"
                                                        x-model="field.harga_pilihan">
                                                    <template x-if="index === (fields.length - 1)">
                                                        <div class="flex items-center">
                                                            <button class="mx-2">
                                                                <img src="{{ asset('storage/icons/plus-solid.svg') }}"
                                                                    alt="" width="20px" @click="addNewField()">
                                                            </button>
                                                            <button class="mx-2">
                                                                <img src="{{ asset('storage/icons/trash-can-solid.svg') }}"
                                                                    alt="" width="20px" @click="removeField(index)">
                                                            </button>
                                                        </div>
                                                    </template>
                                                    <template x-if="index !== (fields.length - 1)">
                                                        <button class="mx-2">
                                                            <img src="{{ asset('storage/icons/trash-can-solid.svg') }}"
                                                                alt="" width="20px" @click="removeField(index)">
                                                        </button>
                                                    </template>
                                                </div>
                                                <div class="flex space-x-2">
                                                    <div class="form-check">
                                                        <input required
                                                            class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                            type="radio"
                                                            :name="'fields[' + index + '][kewajiban_pilihan]'"
                                                            :id="'pilihan_not_required' + index"
                                                            x-model="field.kewajiban_pilihan" value="tidak_wajib">
                                                        <label class="form-check-label inline-block text-gray-800"
                                                            for="pilihan_not_required"> Tidak Wajib </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input required
                                                            class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                            type="radio"
                                                            :name="'fields[' + index + '][kewajiban_pilihan]'"
                                                            :id="'pilihan_required' + index"
                                                            x-model="field.kewajiban_pilihan" value="wajib">
                                                        <label class="form-check-label inline-block text-gray-800"
                                                            for="pilihan_required"> Wajib </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </template>
                                </template>
                            </div>
                        </div>


                        {{-- Ekstra --}}
                        <div class="py-5"
                            x-data="{ ekstra_radio_button: '{{ isset($pilihan_ekstra['ekstra_radio_button']) ? $pilihan_ekstra['ekstra_radio_button'] : '' }}' }">
                            <div class="text-lg font-bold font-inter text-[#4F4F4F]"> Ekstra </div>

                            <div class="py-2">
                                <div class="flex space-x-2">
                                    <div class="form-check">
                                        <input required
                                            class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                            type="radio" name="ekstra_radio_button" id="ekstra_allowed" value="Bolehkan"
                                            x-model="ekstra_radio_button">
                                        <label class="form-check-label inline-block text-gray-800" for="ekstra_allowed">
                                            Bolehkan </label>
                                    </div>
                                    <div class="form-check">
                                        <input required
                                            class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                            type="radio" name="ekstra_radio_button" id="ekstra_not_allowed"
                                            value="Tidak Usah" x-model="ekstra_radio_button">
                                        <label class="form-check-label inline-block text-gray-800"
                                            for="ekstra_not_allowed">
                                            Tidak Usah </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- TnC --}}
                        <div class="">
                            <div class="pb-8">
                                <div class="form-check">
                                    <input name="tnc"
                                        class="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                        type="checkbox" id="tnc" required>
                                    <label class="form-check-label inline-block text-gray-800" for="tnc"> Saya
                                        setuju
                                        atas
                                        syarat dan ketentuan penambahan listing baru di Kamtuu </label>
                                </div>
                            </div>
                        </div>

                        <div class="flex justify-between pt-8">
                            <div>
                                <a href="{{ isset($isedit) ? route('activity.viewPetaEdit',$id) : route('activity.viewPeta') }}"
                                    class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                    Sebelumnya
                                </a>
                            </div>

                            <div class="flex space-x-3">
                                <button type="submit"
                                    class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                    Simpan </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        function handler() {

            return {
                fields: {!! isset($pilihan_ekstra['pilihan_ekstra']) ? $pilihan_ekstra['pilihan_ekstra'] : '[1]' !!},
                addNewField() {
                    this.fields.push({
                        judul_pilihan: '',
                        deskripsi_pilihan: '',
                        harga_pilihan: '',
                        kewajiban_pilihan: ''
                    });
                    // console.log(this.fields)
                },
                removeField(index) {
                    // console.log(index)
                    this.fields.splice(index, 1);
                    // console.log(this.fields)
                }
            }
        }
    </script>
</body>

</html>