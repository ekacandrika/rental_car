<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param||null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}

    <div class="grid grid-cols-10">
        {{--Sidebar--}}
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <div class="flex gap-2">
                    <button type="button"
                        class="text-[#333333] font-medium rounded-lg text-sm py-2 text-center inline-flex items-center mr-2 hover:text-[#23AEC1]">
                        <img src="{{ asset('storage/icons/chevron.svg') }}" alt="user-profile" class="w-5 h-5 mr-1">
                        <span class="text-lg font-bold font-inter text-[#333333]">{{ isset($isedit) ? "Edit Listing" :
                            "Tambah Listing"}}</span>
                    </button>
                </div>
                {{-- Breadcumbs --}}
                <nav class="flex py-3" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            <a href="{{ isset($isedit) ? route('activity.viewAddListingEdit',$id) : route('activity') }}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] duration-200">Isi
                                Informasi Dasar</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{ isset($isedit) ? route('activity.viewHargaEdit',$id) : route('activity.viewHarga') }}"
                                    class="inline-flex items-center text-sm font-bold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Harga
                                    & Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if (isset($activity['batas']))
                                <a href="{{ isset($isedit) ? route('activity.viewBatasEdit',$id) : route('activity.viewBatas') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] duration-200">Batas
                                    Pembayaran & Pembatalan</a>
                                @else
                                <a href="{{ isset($isedit) ? route('activity.viewBatasEdit',$id) : route('activity.viewBatas') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#858585] hover:text-gray-500 duration-200 pointer-events-none">Batas
                                    Pembayaran & Pembatalan</a>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if (isset($activity['peta']))
                                <a href="{{ isset($isedit) ? route('activity.viewPetaEdit',$id) : route('activity.viewPeta') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] duration-200">Peta
                                    & Foto</a>
                                @else
                                <a href="{{ isset($isedit) ? route('activity.viewPetaEdit',$id) : route('activity.viewPeta') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#858585] hover:text-gray-500 duration-200 pointer-events-none">Peta
                                    & Foto</a>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if (isset($activity['pilihan_ekstra']))
                                <a href="{{ isset($isedit) ? route('activity.viewPilihanEkstraEdit',$id) : route('activity.viewPilihanEkstra') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] duration-200">Pilihan
                                    & Extra</a>
                                @else
                                <a href="{{ isset($isedit) ? route('activity.viewPilihanEkstraEdit',$id) : route('activity.viewPilihanEkstra') }}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#858585] hover:text-gray-500 duration-200 pointer-events-none">Pilihan
                                    & Extra</a>
                                @endif
                            </div>
                        </li>
                    </ol>
                </nav>

                {{-- Section 1 --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl p-6 px-10 rounded">
                    <form
                        action="{{ isset($isedit) ? route('activity.addHarga.edit', $id) : route('activity.addHarga') }}"
                        method="POST" enctype="multipart/form-data" x-data="harga()">
                        @csrf
                        @method('PUT')

                        <div class="flex justify-between pb-5">
                            <div class="text-lg font-bold font-inter text-[#9E3D64]">Harga Dalam Mata Uang Rupiah (IDR)
                            </div>
                        </div>
                        @if (session('success_harga'))
                        <div x-data="{ showMessage: true }" x-show="showMessage"
                            class="bg-green-300 rounded-md p-3 mt-1 mb-3 flex justify-between"
                            x-init="setTimeout(() => showMessage = false, 3000)">
                            <p>{{ session('success_harga') }}</p>
                        </div>
                        @endif

                        {{-- Residen and Non Residen --}}
                        <div class="grid grid-cols-2 pb-2">
                            <div class="text-sm font-bold text-[#333333]">Residen</div>
                            <div class="text-sm font-bold text-[#333333]">
                                <p>Non-Residen</p>
                                <p class=" text-xs font-normal">(Bila tidak diisi harga berlaku sama)</p>
                            </div>
                        </div>

                        <div class="grid grid-cols-2 pb-5 border-b-2 mb-5">
                            <div class="">
                                <p class="block md:hidden text-sm font-bold text-[#333333] mb-6">Residen</p>
                                <div class="mb-6">
                                    <label for="dewasa_residen"
                                        class="block mb-2 text-sm font-bold text-[#333333]">Dewasa</label>
                                    <input type="number" id="dewasa_residen" name="dewasa_residen"
                                        x-model="dewasa_residen" min="0"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="120000" required>
                                    @error('dewasa_residen', 'harga')
                                    <div class="bg-red-300 rounded-md w-96 p-3 my-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="mb-6">
                                    <label for="anak_residen" class="block mb-2 text-sm font-bold text-[#333333] ">Anak
                                        (5 -
                                        12
                                        Tahun)</label>
                                    <input type="number" id="anak_residen" name="anak_residen" x-model="anak_residen"
                                        min="0"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="60000" required>
                                    @error('anak_residen', 'harga')
                                    <div class="bg-red-300 rounded-md w-96 p-3 my-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="mb-6">
                                    <label for="balita_residen"
                                        class="block mb-2 text-sm font-bold text-[#333333] ">Balita
                                        (< 5 Tahun)</label>
                                            <input type="number" id="balita_residen" name="balita_residen"
                                                x-model="balita_residen" min="0"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="0" required>
                                            @error('balita_residen', 'harga')
                                            <div class="bg-red-300 rounded-md w-96 p-3 my-2">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                </div>
                                <button class="text-white bg-[#23AEC1] py-1 px-3 rounded-md">Perbarui</button>
                            </div>
                            <div>
                                <div class="block md:hidden text-sm font-bold text-[#333333] mb-6">
                                    <p>Non-Residen</p>
                                    <p class=" text-xs font-normal">(Bila tidak diisi harga berlaku sama)</p>
                                </div>

                                <div class="mb-6">
                                    <label for="dewasa_non_residen"
                                        class="block mb-2 text-sm font-bold text-[#333333] ">Dewasa</label>
                                    <input type="number" id="dewasa_non_residen" name="dewasa_non_residen"
                                        x-model="dewasa_non_residen" min="0"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="120000">
                                    @error('dewasa_non_residen', 'harga')
                                    <div class="bg-red-300 rounded-md w-96 p-3 my-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="mb-6">
                                    <label for="anak_non_residen"
                                        class="block mb-2 text-sm font-bold text-[#333333] ">Anak
                                        (5 - 12
                                        Tahun)</label>
                                    <input type="number" id="anak_non_residen" name="anak_non_residen"
                                        x-model="anak_non_residen" min="0"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="60000">
                                    @error('anak_non_residen', 'harga')
                                    <div class="bg-red-300 rounded-md w-96 p-3 my-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="mb-6">
                                    <label for="balita_non_residen"
                                        class="block mb-2 text-sm font-bold text-[#333333] ">Balita (< 5 Tahun)</label>
                                            <input type="number" id="balita_non_residen" name="balita_non_residen"
                                                x-model="balita_non_residen" min="0"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="0">
                                            @error('balita_non_residen', 'harga')
                                            <div class="bg-red-300 rounded-md w-96 p-3 my-2">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                </div>
                            </div>
                        </div>
                        {{-- @forelse ($activity['add_listing']['paket'] as $paket)
                        <div>
                            <p class="text-base py-2 font-bold">Nama Paket: {{ $paket['nama_paket'] }}</p>
                        </div>
                        <div class="grid grid-cols-2 pb-2">
                            <div class="text-sm font-bold text-[#333333]">Residen</div>
                            <div class="text-sm font-bold text-[#333333]">
                                <p>Non-Residen</p>
                                <p class=" text-xs font-normal">(Bila tidak diisi harga berlaku sama)</p>
                            </div>
                        </div>

                        <div class="grid grid-cols-2 pb-5 border-b-2 mb-5">
                            <div class="">
                                <p class="block md:hidden text-sm font-bold text-[#333333] mb-6">Residen</p>
                                <div class="mb-6">
                                    <label for="dewasa_residen"
                                        class="block mb-2 text-sm font-bold text-[#333333]">Dewasa</label>
                                    <input type="number" id="dewasa_residen" name="dewasa_residen"
                                        x-model="dewasa_residen" min="0"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="120000" required>
                                    @error('dewasa_residen', 'harga')
                                    <div class="bg-red-300 rounded-md w-96 p-3 my-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="mb-6">
                                    <label for="anak_residen" class="block mb-2 text-sm font-bold text-[#333333] ">Anak
                                        (5 -
                                        12
                                        Tahun)</label>
                                    <input type="number" id="anak_residen" name="anak_residen" x-model="anak_residen"
                                        min="0"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="60000" required>
                                    @error('anak_residen', 'harga')
                                    <div class="bg-red-300 rounded-md w-96 p-3 my-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="mb-6">
                                    <label for="balita_residen"
                                        class="block mb-2 text-sm font-bold text-[#333333] ">Balita
                                        (< 5 Tahun)</label>
                                            <input type="number" id="balita_residen" name="balita_residen"
                                                x-model="balita_residen" min="0"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="0" required>
                                            @error('balita_residen', 'harga')
                                            <div class="bg-red-300 rounded-md w-96 p-3 my-2">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                </div>
                                <button class="text-white bg-[#23AEC1] py-1 px-3 rounded-md">Perbarui</button>
                            </div>
                            <div>
                                <div class="block md:hidden text-sm font-bold text-[#333333] mb-6">
                                    <p>Non-Residen</p>
                                    <p class=" text-xs font-normal">(Bila tidak diisi harga berlaku sama)</p>
                                </div>

                                <div class="mb-6">
                                    <label for="dewasa_non_residen"
                                        class="block mb-2 text-sm font-bold text-[#333333] ">Dewasa</label>
                                    <input type="number" id="dewasa_non_residen" name="dewasa_non_residen"
                                        x-model="dewasa_non_residen" min="0"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="120000">
                                    @error('dewasa_non_residen', 'harga')
                                    <div class="bg-red-300 rounded-md w-96 p-3 my-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="mb-6">
                                    <label for="anak_non_residen"
                                        class="block mb-2 text-sm font-bold text-[#333333] ">Anak
                                        (5 - 12
                                        Tahun)</label>
                                    <input type="number" id="anak_non_residen" name="anak_non_residen"
                                        x-model="anak_non_residen" min="0"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="60000">
                                    @error('anak_non_residen', 'harga')
                                    <div class="bg-red-300 rounded-md w-96 p-3 my-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="mb-6">
                                    <label for="balita_non_residen"
                                        class="block mb-2 text-sm font-bold text-[#333333] ">Balita (< 5 Tahun)</label>
                                            <input type="number" id="balita_non_residen" name="balita_non_residen"
                                                x-model="balita_non_residen" min="0"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="0">
                                            @error('balita_non_residen', 'harga')
                                            <div class="bg-red-300 rounded-md w-96 p-3 my-2">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                </div>
                            </div>
                        </div>
                        @empty

                        @endforelse --}}
                    </form>

                    <form
                        action="{{ isset($isedit) ? route('activity.addDiscountGroup.edit', $id) : route('activity.addDiscountGroup') }}"
                        x-data="handler()" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="py-5">
                            <div class="mb-2">
                                <div class="text-lg font-bold font-inter text-[#9E3D64]">Diskon grup dari jumlah orang
                                    dewasa
                                    (IDR)
                                </div>
                            </div>
                            @if (session('success_diskon_orang'))
                            <div x-data="{ showMessage: true }" x-show="showMessage"
                                class="bg-green-300 rounded-md p-3 mt-1 mb-3 flex justify-between"
                                x-init="setTimeout(() => showMessage = false, 3000)">
                                <p>{{ session('success_diskon_orang') }}</p>
                            </div>
                            @endif
                            <table class="my-5 w-full border table-auto rounded-md">
                                <thead>
                                    <tr>
                                        <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Min Jumlah Orang</th>
                                        <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Maks Jumlah Orang</th>
                                        <th class="p-3 border border-slate-500 bg-[#BDBDBD]">%</th>
                                        <th class="p-3 border border-slate-500 bg-[#BDBDBD]"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <template x-if="fields.length < 1">
                                        <div class="px-3">
                                            <div>
                                                <button type="button"
                                                    class="px-3 py-1 my-3 text-sm text-white rounded-full btn btn-info bg-kamtuu-second"
                                                    @click="addNewField()">Tambah +</button>
                                            </div>
                                        </div>
                                    </template>
                                    <template x-if="fields.length >= 1" class="col">
                                        <template x-for="(field, index) in fields" :key="index">
                                            <tr class="align-middle">
                                                <td class="p-3 border border-slate-500 text-center">
                                                    <input type="number" id="min_orang" min="0"
                                                        :name="'fields['+index+'][min_orang]'" x-model="field.min_orang"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="3" required>
                                                </td>
                                                <td class="p-3 border border-slate-500 text-center">
                                                    <input type="number" id="max_orang" min="0"
                                                        :name="'fields['+index+'][max_orang]'" x-model="field.max_orang"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="5" required>
                                                </td>
                                                <td class=" p-3 border border-slate-500 text-center">
                                                    <input type="text" id="diskon_orang" min="0"
                                                        :name="'fields['+index+'][diskon_orang]'"
                                                        x-model="field.diskon_orang"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="5" required>
                                                </td>
                                                <template x-if="index === (fields.length - 1)">
                                                    <td class="p-3 border border-slate-500 text-center">

                                                        <button class="mx-1" @click="removeField(index)">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                height="24" viewBox="0 0 24 24">
                                                                <path fill="currentColor"
                                                                    d="M12 20c-4.41 0-8-3.59-8-8s3.59-8 8-8s8 3.59 8 8s-3.59 8-8 8m0-18A10 10 0 0 0 2 12a10 10 0 0 0 10 10a10 10 0 0 0 10-10A10 10 0 0 0 12 2M7 13h10v-2H7" />
                                                            </svg>
                                                        </button>
                                                        <button class="mx-1" @click="addNewField()">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                height="24" viewBox="0 0 24 24">
                                                                <path fill="currentColor"
                                                                    d="M12 20c-4.41 0-8-3.59-8-8s3.59-8 8-8s8 3.59 8 8s-3.59 8-8 8m0-18A10 10 0 0 0 2 12a10 10 0 0 0 10 10a10 10 0 0 0 10-10A10 10 0 0 0 12 2m1 5h-2v4H7v2h4v4h2v-4h4v-2h-4V7Z" />
                                                            </svg>
                                                        </button>
                                                    </td>
                                                </template>
                                                <template x-if="index !== (fields.length - 1)">
                                                    <td class="p-3 border border-slate-500 text-center">
                                                        <button class="mx-1" @click="removeField(index)">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                height="24" viewBox="0 0 24 24">
                                                                <path fill="currentColor"
                                                                    d="M12 20c-4.41 0-8-3.59-8-8s3.59-8 8-8s8 3.59 8 8s-3.59 8-8 8m0-18A10 10 0 0 0 2 12a10 10 0 0 0 10 10a10 10 0 0 0 10-10A10 10 0 0 0 12 2M7 13h10v-2H7" />
                                                            </svg>
                                                        </button>
                                                    </td>
                                                </template>
                                            </tr>
                                        </template>
                                    </template>
                                </tbody>
                            </table>
                            @if ($errors->hasBag('diskon_orang'))
                            <div class="bg-red-300 rounded-md p-3 my-5">
                                @foreach ($errors->diskon_orang->toArray() as $key=>$value)
                                @foreach ($value as $index=>$err)
                                <p>- {{ $err}}</p>
                                @endforeach
                                @endforeach
                            </div>
                            @endif
                        </div>

                        <button x-init="$nextTick(() => { disabled = fields.length === 0 ? true : false })"
                            :class="fields.length > 0 ? 'bg-[#23AEC1]' : 'bg-slate-300'" :disabled="disabled"
                            class="py-1 px-3 text-white rounded-md">Perbarui</button>
                    </form>

                </div>

                {{-- Section 2 --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded" x-data="discountDate()">

                    <p class="font-bold text-lg text-[#333333] pb-5">Ketersediaan</p>
                    <div class="flex md:space-x-10 gap-5">
                        <div class="mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Dari
                                Tanggal</label>
                            <div
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <input type="text" id="tgl_start" name="tgl_start" placeholder="" required>
                            </div>
                        </div>
                        <div class="mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Sampai
                                Tanggal</label>
                            <div
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <input type="text" id="tgl_end" name="tgl_end" placeholder="" required>
                            </div>
                        </div>
                        <div class="pb-5">
                            <label for="text" class="flex mb-2 text-sm font-medium text-[#333333]"
                                x-data="{ tooltip: false }" x-on:mouseover="tooltip = true"
                                x-on:mouseleave="tooltip = false">x/- (%)
                                <img class="mx-2" width="15px"
                                    src="{{ asset('storage/icons/circle-info-solid (1) 1.svg') }}" alt="">
                                <div x-show="tooltip"
                                    class="w-96 text-sm text-white absolute bg-[#9E3D64] rounded-lg p-2 transform -translate-y-8 translate-x-8">
                                    Apabila diisi <strong>tanpa tanda minus</strong>, maka akan menaikkan harga dasar
                                    sebesar %.
                                    Apabila diisi <strong>dengan tanda minus</strong>, akan menurunkan harga sebesar %.
                                </div>
                            </label>
                            <input type="text" id="discount" name="discount" x-model="discount"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="" required>
                        </div>
                    </div>
                    <template x-if="error_empty_data != ''">
                        <div class="bg-red-300 rounded-md p-3 my-2">
                            <p x-text="error_empty_data"></p>
                        </div>
                    </template>
                    <template x-if="error_date != ''">
                        <div class="bg-red-300 rounded-md p-3 my-2">
                            <p x-text="error_date"></p>
                        </div>
                    </template>
                    <template x-if="error_number != ''">
                        <div class="bg-red-300 rounded-md p-3 my-2">
                            <p x-text="error_number"></p>
                        </div>
                    </template>
                    @if ($errors->hasBag('diskon_ketersediaan'))
                    <div class="bg-red-300 rounded-md p-3 my-5">
                        @foreach ($errors->diskon_ketersediaan->toArray() as $key=>$value)
                        @foreach ($value as $index=>$err)
                        <p>- {{ $err}}</p>
                        @endforeach
                        @endforeach
                    </div>
                    @endif
                    <button class="text-white bg-[#23AEC1] py-1 px-3 rounded-md" @click="addNewField()">Tambah</button>

                    <form
                        action="{{ isset($isedit) ? route('activity.addAvailability.edit', $id) : route('activity.addAvailability') }}"
                        method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')


                        <div class="grid grid-cols-10 my-5">
                            <template x-if="details.length === 0">
                                <div class="col-span-5 border border-slate-300 rounded-md ">
                                    <div class="my-2 mx-3">
                                        <p>Anda belum menambahkan Tanggal ketersedian dan diskon.</p>
                                    </div>
                                </div>
                            </template>
                            <template x-if="details.length >= 1">
                                <template x-for="(detail, index) in details" :key="index">
                                    <div class="col-span-10 flex">
                                        <div class="w-5/6 border border-slate-500 rounded-md my-5">
                                            <div class="my-2 mx-3" x-data="{ open:false }">
                                                <div class="flex border-b border-gray-200 pt-1 pb-2">
                                                    <button type="button"
                                                        class="text-lg sm:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg flex space-x-10"
                                                        x-on:click="open = !open">
                                                        <div class="cursor-pointer pr-5">
                                                            <p for="text"
                                                                class="block mb-2 text-sm font-bold text-slate-500">
                                                                Dari
                                                                Tanggal</p>
                                                            <p class=" text-base font-bold text-[#333333]"
                                                                x-text="detail.tgl_start"></p>
                                                            <input type="hidden" x-model="detail.tgl_start"
                                                                :name="'details['+index+'][tgl_start]'">
                                                        </div>
                                                        <div class="cursor-pointer pr-5">
                                                            <p for="text"
                                                                class="block mb-2 text-sm font-bold text-slate-500">
                                                                Sampai
                                                                Tanggal</p>
                                                            <p class=" text-base font-bold text-[#333333]"
                                                                x-text="detail.tgl_end"></p>
                                                            <input type="hidden" x-model="detail.tgl_end"
                                                                :name="'details['+index+'][tgl_end]'">
                                                        </div>
                                                        <div class="cursor-pointer pr-5">
                                                            <p for="text"
                                                                class="block mb-2 text-sm font-bold text-slate-500">
                                                                +/- (%)
                                                            </p>
                                                            <p class=" text-base font-bold text-[#333333]"
                                                                x-text="detail.discount"></p>
                                                            <input type="hidden" x-model="detail.discount"
                                                                :name="'details['+index+'][discount]'">
                                                        </div>
                                                    </button>
                                                    <img class="float-right duration-200 cursor-pointer"
                                                        :class="{'rotate-180' : open}" x-on:click="open = !open"
                                                        src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                                        alt="chevron-down" width="26px" height="15px">
                                                </div>
                                                <div class="py-2" x-show="open" x-transition>
                                                    <table class="my-1 w-full table-auto">
                                                        <thead>
                                                            <tr>
                                                                <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                </th>
                                                                <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                    Dewasa
                                                                </th>
                                                                <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                    Anak
                                                                </th>
                                                                <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                    Balita
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="align-middle">
                                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                                    <p>Residen</p>
                                                                </td>
                                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                                    <p
                                                                        x-text="dewasa_residen+((detail.discount/100)*dewasa_residen)">
                                                                    </p>
                                                                </td>
                                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                                    <p
                                                                        x-text="anak_residen+((detail.discount/100)*anak_residen)">
                                                                    </p>
                                                                </td>
                                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                                    <p
                                                                        x-text="balita_residen+((detail.discount/100)*balita_residen)">
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr class="align-middle">
                                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                                    <p>Non-Residen</p>
                                                                </td>
                                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                                    <p
                                                                        x-text="dewasa_non_residen+((detail.discount/100)*dewasa_non_residen)">
                                                                    </p>
                                                                </td>
                                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                                    <p
                                                                        x-text="anak_non_residen+((detail.discount/100)*anak_non_residen)">
                                                                    </p>
                                                                </td>
                                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                                    <p
                                                                        x-text="balita_non_residen+((detail.discount/100)*balita_non_residen)">
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-1/6 px-3 my-5">
                                            <button @click="removeField(index)"
                                                class="bg-[#D50006] text-white text-sm font-semibold py-2 px-5 rounded-lg hover:bg-[#de252b]">
                                                Hapus
                                            </button>
                                        </div>
                                    </div>
                                </template>
                            </template>
                        </div>

                        {{-- <x-be.com.two-button></x-be.com.two-button> --}}
                        {{-- <x-be.com.two-button-dup>
                            {{ isset($isedit) ? route('activity.viewAddListingEdit',$id) : route('activity') }}
                        </x-be.com.two-button-dup> --}}
                        <div class="p-5">
                            <div class="grid grid-cols-6">
                                <div class="col-start-1 col-end-2">
                                    <a href="{{ isset($isedit) ? route('activity.viewAddListingEdit',$id) : route('activity') }}"
                                        class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF] duration-200">
                                        Sebelumnya
                                    </a>
                                </div>
                                <div class="col-start-6 col-end-7">
                                    @if (isset($harga['dewasa_residen']) && isset($harga['anak_residen']) &&
                                    isset($harga['balita_residen']))
                                    <button type="submit"
                                        class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                        Selanjutnya
                                    </button>
                                    @else
                                    <button type="button"
                                        class="cursor-not-allowed w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-slate-300 rounded-lg">
                                        Selanjutnya
                                    </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- <script>
        $(document).ready(function() {
            $('#datepickerTurFrom').datepicker();
        })
        $(document).ready(function() {
            $('#datepickerTurUntil').datepicker();
        })
    </script> --}}

    <script>
        function harga() {
            return {
                dewasa_residen: {!! isset($harga['dewasa_residen']) ? $harga['dewasa_residen'] : "null" !!},
                anak_residen: {!! isset($harga['anak_residen']) ? $harga['anak_residen'] : "null" !!},
                balita_residen: {!! isset($harga['balita_residen']) ? $harga['balita_residen'] : "null" !!},
                dewasa_non_residen: {!! isset($harga['dewasa_non_residen']) ? $harga['dewasa_non_residen'] : "null" !!},
                anak_non_residen: {!! isset($harga['anak_non_residen']) ? $harga['anak_non_residen'] : "null" !!},
                balita_non_residen: {!! isset($harga['balita_non_residen']) ? $harga['balita_non_residen'] : "null" !!},
            }
        }

    </script>
    <script>
        function handler() {
                    
            return {
                fields: {!! isset($harga['discount_group']) ? $harga['discount_group'] : "[]" !!},
                disabled: false,
                addNewField() {
                    this.fields.push({
                        min_orang: '',
                        max_orang: '',
                        discount_orang: ''
                    });
                    if (this.fields.length > 0) {
                        this.disabled = false
                    }
                    // console.log(this.fields)
                },
                removeField(index) {
                    this.fields.splice(index, 1);
                    if (this.fields.length === 0) {
                        this.disabled = true
                    }
                }
            }
        }
    </script>

    <script>
        var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        $(document).ready(function() {
            $('#tgl_start').datepicker({
                minDate: today
            });
        })
        $(document).ready(function() {
            $('#tgl_end').datepicker({
                minDate: today
            });
        })

        // var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        // let min_start = ''
        // $(document).ready(function() {
        //     $('#tgl_start').datepicker({
        //         change: function (e) {
        //             today = $('#tgl_start').datepicker().value();
        //             console.log(today)
        //             tgl_end(today)
        //         },
        //         minDate: today
        //     });
        // })
        // function tgl_end(today) {
        //     $(document).ready(function() {
        //         $('#tgl_end').datepicker({
        //             minDate: today
        //         });
        //     })
        // }
        
        function discountDate() {
                
            return {
                tgl_start: '',
                tgl_end: '',
                discount: '',
                error_empty_data: '',
                error_date: '',
                error_number: '',
                dewasa_residen: {!! isset($harga['dewasa_residen']) ? $harga['dewasa_residen'] : "null" !!},
                anak_residen: {!! isset($harga['anak_residen']) ? $harga['anak_residen'] : "null" !!},
                balita_residen: {!! isset($harga['balita_residen']) ? $harga['balita_residen'] : "null" !!},
                dewasa_non_residen: {!! isset($harga['dewasa_non_residen']) ? $harga['dewasa_non_residen'] : "null" !!},
                anak_non_residen: {!! isset($harga['anak_non_residen']) ? $harga['anak_non_residen'] : "null" !!},
                balita_non_residen: {!! isset($harga['balita_non_residen']) ? $harga['balita_non_residen'] : "null" !!},
                details: {!! isset($harga['availability']) ? $harga['availability'] : "[]" !!},
                addNewField() {
                    let digit = /^-?\d+\.?\d*$/
                    this.error_empty_data = ''
                    this.error_date = ''
                    this.error_number = ''

                    if ($('#tgl_start').datepicker().value() > $('#tgl_end').datepicker().value()) {
                        this.error_date = 'Tanggal mulai harus lebih kecil dari tanggal selesai!'
                    }
                    if ($('#tgl_start').datepicker().value() == '' || $('#tgl_end').datepicker().value() == '' || this.discount == '') {
                        this.error_empty_data = 'Data wajib diisi!'
                    }

                    if (!digit.test(this.discount)) {
                        this.error_number = 'Pastikan discount berupa angka!'
                    }

                    if (this.error_empty_data != '' || this.error_date != '' || this.error_number != '') {
                        return this.error_date, this.error_empty_data, this.error_number
                    }

                    this.details.push({
                        tgl_start: $('#tgl_start').datepicker().value(),
                        tgl_end: $('#tgl_end').datepicker().value(),
                        discount: this.discount
                    });
                    this.tgl_start = $('#tgl_start').datepicker().value('')
                    this.tgl_end = $('#tgl_end').datepicker().value('')
                    this.discount = ''
                    this.error_empty_data = ''
                    this.error_date = ''
                    this.error_number = ''
                },
                removeField(index) {
                    this.details.splice(index, 1);
                }
            }
        }
    </script>
</body>

</html>