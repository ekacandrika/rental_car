<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js" defer></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        .select2-container .select2-selection--single {
            height: 100%;
            /* padding-left: 0.75rem; */
            /* padding-right: 0.75rem; */
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 100%;
        }

        .select2-selection__arrow {
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            /* padding-right: 0.75rem; */
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param||null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-lg font-bold font-inter text-[#000000]">FAQ</span>

                <div class="bg-[#FFFFFF] drop-shadow-xl mt-3 p-10 rounded">
                    <p class="px-5 pb-3">Pilih salah satu FAQ</p>
                    <form action="{{ route('activity.viewFaq.code') }}" method="POST">
                        @csrf
                        <div class="px-5">
                            <select name="product_activity" id="product_activity"
                                class="product_activity block w-1/3 mx-5 px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                <option selected disabled>Pilih Activity</option>
                                @foreach ($faqs as $faq)
                                <option value="{{ $faq->id }}" {{ isset($faq_content) && ($faq->id == $faq_content->id)
                                    ?
                                    'selected' : '' }}>{{
                                    $faq->slug }} - {{ $faq->product_name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <button id="faqbutton"
                            class="w-1/12 items-center mx-5 mt-3 px-5 py-2 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">Pilih</button>
                    </form>

                    @if (isset($faq_content))
                    <form action="{{ route('activity.viewFaq.update', $faq_content->id)}}" method="POST"
                        x-data="faqHandler()" id="faq_form">
                        @csrf
                        <div class="px-5 pt-5">
                            <textarea class="w-[10rem] click2edit" id="summernote" name="faq" x-model="faq"
                                x-init="$nextTick(() => { initSummerNote() })">
                            </textarea>
                        </div>

                        {{-- <x-be.com.two-button></x-be.com.two-button> --}}
                        <div class="flex justify-end">
                            <button type="submit"
                                class="w-1/6 items-center mx-5 mt-2 px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                Simpan
                            </button>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        const initSummerNote = () => {
            $('.click2edit').summernote({
                placeholder: 'FAQ',
                tabsize: 2,
                height: 120,
                toolbar: [
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    // ['insert', ['link', 'picture', 'video']],
                ],
                dialogsInBody: true
            });
        }

        $('#faq_form').on('submit', function() {
            if ($('#summernote').summernote('isEmpty')) {
                $('#summernote').val(null);
            }
        })

        function faqHandler() {

            return {
                faq: '{!! isset($faq_content->productdetail['faq']) ? $faq_content->productdetail['faq'] : '' !!}',
            }
        }
    </script>
    <script>
        $('.product_activity').select2({
            placeholder: "Pilih Activity",
            allowClear: true
        });
    </script>
    {{-- <script>
        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $(function() {
                $('#faqbutton').on('click', function() {
                    let id_faq = $('#product_activity').val();
                        $.ajax({
                            type: 'GET',
                            url: "{{ route('activity.viewFaq.edit') }}",
                            data: {
                                id_faq: id_faq
                            },
                            cache: false,

                            success: function(msg) {
                                window.location.replace('faq/edit')
                            },
                            error: function(data) {
                                console.log('error:', data);
                            },
                        })
                    })
                })
            });
    </script> --}}
</body>

</html>