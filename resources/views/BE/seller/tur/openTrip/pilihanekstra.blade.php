<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param||null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        @php
            if($isedit){
                $pilihan = $pilihan_ekstra;
            }
        @endphp
        <x-be.kelolatoko.sidebar-toko></x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
                {{-- Breadcumbs --}}
                <nav class="flex" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            <a href="informasi"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="itinerary"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Buat
                                    Itinerary</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="harga"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Harga
                                    & Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="bayar"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="maps"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Peta
                                    & Foto</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="pilihan"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Pilihan
                                    & Ekstra</a>
                            </div>
                        </li>
                    </ol>
                </nav>

                {{-- Section --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded">

                    {{-- Pilihan --}}
                    <div x-data="handler()">
                        <div class="pb-5">
                            <template x-if="fields.length < 1">
                                <div class="">
                                    <div class="text-lg font-bold font-inter text-[#4F4F4F]">
                                        Pilihan
                                    </div>
                                    <button class="text-white bg-[#23AEC1] mt-3 py-1 px-3 rounded-md"
                                        @click="addNewField()">Tambah</button>
                                    <p>Klik tombol Tambah di atas untuk menambahkan.</p>
                                </div>
                            </template>
                            <template x-if="fields.length >= 1" class="col">
                                <template x-for="(field, index) in fields" :key="index">
                                    <div class="pb-5">
                                        <div class="text-lg font-bold font-inter text-[#4F4F4F]">
                                            Pilihan <span x-text="index+1"></span>
                                        </div>

                                        <div class="py-2">
                                            <div class="mb-3 w-1/4">
                                                <select
                                                    class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700  bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded-lg transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                                    aria-label="Default select example" x-model="field.detail_1">
                                                    <option disabled>Pilihan</option>
                                                    @foreach ($pilihan as $item)
                                                        <option value="{{$item->name}}">{{$item->name}}</option>
                                                    @endforeach
                                                    {{-- <option value="hotel">Hotel</option>
                                                    <option value="asuransi">Asuransi</option> --}}
                                                </select>
                                            </div>
                                            <div class="mb-3 w-1/4">
                                                <select name="hotel_id" class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700  bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded-lg transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                                aria-label="Default select example" x-model="field.detail_3">
                                                    <option disabled>Pilih Nama Hotel</option>
                                                    @foreach ($hotels as $hotel)
                                                        <option value="{{$hotel->id}}">{{$hotel->product_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="mb-3 w-1/4">
                                                <select
                                                    class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700  bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded-lg transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                                    aria-label="Default select example" x-model="field.detail_3">
                                                    <option disabled>Pilihan</option>
                                                    <option value="bintang_satu">Bintang 1</option>
                                                    <option value="bintang_dua">Bintang 2</option>
                                                    <option value="bintang_tiga">Bintang 3</option>
                                                    <option value="bintang_empat">Bintang 4</option>
                                                    <option value="bintang_lima">Bintang 5</option>
                                                </select>
                                            </div>
                                            <div class="flex my-2">
                                                <input type="text" id="text"
                                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-1/4 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                    placeholder="Harga" x-model="field.price">
                                                <template x-if="index === (fields.length - 1)">
                                                    <div class="flex items-center">
                                                        <button class="mx-2"><img
                                                                src="{{ asset('storage/icons/circle-plus-solid-blue.svg') }}"
                                                                alt="" width="25px" @click="addNewField()">
                                                        </button>
                                                        <button class="mx-2"><img
                                                                src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                                alt="" width="25px" @click="removeField(index)">
                                                        </button>
                                                    </div>
                                                </template>
                                                <template x-if="index !== (fields.length - 1)">
                                                    <button class="mx-2"><img
                                                            src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                            alt="" width="25px" @click="removeField(index)">
                                                    </button>
                                                </template>

                                            </div>
                                            <div class="flex space-x-2">
                                                <div class="form-check">
                                                    <input
                                                        class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                        type="radio" :name="'pilihan'+index"
                                                        :id="'pilihan_not_required'+index" x-model="field.isRequired"
                                                        value="not_required">
                                                    <label class="form-check-label inline-block text-gray-800"
                                                        for="pilihan_not_required">
                                                        Tidak Wajib
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input
                                                        class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                        type="radio" :name="'pilihan'+index"
                                                        :id="'pilihan_required'+index" x-model="field.isRequired"
                                                        value="required">
                                                    <label class="form-check-label inline-block text-gray-800"
                                                        for="pilihan_required">
                                                        Wajib
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </template>
                            </template>
                        </div>
                    </div>


                    {{-- Ekstra --}}
                    <div class="pb-5">
                        <div class="text-lg font-bold font-inter text-[#4F4F4F]">
                            Ekstra
                        </div>

                        <div class="py-2">
                            <div class="flex space-x-2">
                                <div class="form-check">
                                    <input
                                        class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                        type="radio" name="ekstra_radio_button" id="ekstra_allowed">
                                    <label class="form-check-label inline-block text-gray-800" for="ekstra_allowed">
                                        Bolehkan
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input
                                        class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                        type="radio" name="ekstra_radio_button" id="ekstra_not_allowed">
                                    <label class="form-check-label inline-block text-gray-800" for="ekstra_not_allowed">
                                        Tidak Usah
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- TnC --}}
                    <div class="">
                        <div>
                            <div class="form-check">
                                <input
                                    class="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                    type="checkbox" value="" id="tnc">
                                <label class="form-check-label inline-block text-gray-800" for="tnc">
                                    Saya setuju atas syarat dan ketentuan penambahan listing baru di Kamtuu
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="flex justify-between pt-5">
                        <div>
                            <button type="submit"
                                class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                Sebelumnya
                            </button>
                        </div>

                        <div class="flex space-x-3">
                            <button type="submit"
                                class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                Simpan
                            </button>
                            <button type="submit"
                                class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                Terbitkan
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function handler() {
                    
            return {
                fields: [],
                addNewField() {
                    this.fields.push({
                        detail_1: 'pilihan',
                        detail_2: 'nama_hotel',
                        detail_3: 'bintang_satu',
                        price: 0,
                        isRequired: ''
                    });
                },
                removeField(index) {
                    // console.log(index)
                    this.fields.splice(index, 1);
                    console.log(this.fields)
                }
            }
        }
    </script>
</body>

</html>