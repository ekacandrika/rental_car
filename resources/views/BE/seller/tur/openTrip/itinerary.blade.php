<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js" defer></script>

    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param||null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko></x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
                {{-- Breadcumbs --}}
                <nav class="flex" aria-label="Breadcrumb">
                    
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            <a href="informasi"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="itinerary"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Buat
                                    Itinerary</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="harga"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Harga
                                    & Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="bayar"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="maps"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Peta
                                    & Foto</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="pilihan"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Pilihan
                                    & Ekstra</a>
                            </div>
                        </li>
                    </ol>
                </nav>

                <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded" x-data="itineraryData()">
                    <div class="flex justify-between">
                        <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Buat Itinerary</div>
                        <div class="p-5">
                            <button type="submit"
                                class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]"
                                @click="addNewField()">
                                Tambah
                            </button>
                        </div>
                    </div>

                    <div class="container mx-auto px-5 space-y-3 py-3">
                        <p class="text-base font-semibold text-[#333333]">Judul</p>
                        <p class="text-sm font-normal text-[#333333]">Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit, sed do
                            eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Odio ut sem nulla pharetra. Venenatis urna cursus eget nunc
                            scelerisque viverra mauris in. Egestas erat imperdiet sed euismod nisi porta. Dis parturient
                            montes nascetur ridiculus mus mauris vitae. Quisque egestas diam in arcu. Orci phasellus
                            egestas
                            tellus rutrum tellus pellentesque eu tincidunt tortor. Tempus egestas sed sed risus pretium
                            quam. Arcu odio ut sem nulla pharetra diam. Molestie ac feugiat sed lectus vestibulum mattis
                            ullamcorper velit sed. Sed arcu non odio euismod lacinia at quis risus sed.</p>
                        <button
                            class="bg-[#D50006] text-white text-sm font-semibold py-1 px-5 rounded-lg hover:bg-[#de252b]">Hapus</button>
                        <hr class="border-[#333333]" />
                    </div>

                    <div class="container mx-auto px-5 space-y-3 py-3">
                        <p class="text-base font-semibold text-[#333333]">Judul</p>
                        <p class="text-sm font-normal text-[#333333]">Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit, sed do
                            eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Odio ut sem nulla pharetra. Venenatis urna cursus eget nunc
                            scelerisque viverra mauris in. Egestas erat imperdiet sed euismod nisi porta. Dis parturient
                            montes nascetur ridiculus mus mauris vitae. Quisque egestas diam in arcu. Orci phasellus
                            egestas
                            tellus rutrum tellus pellentesque eu tincidunt tortor. Tempus egestas sed sed risus pretium
                            quam. Arcu odio ut sem nulla pharetra diam. Molestie ac feugiat sed lectus vestibulum mattis
                            ullamcorper velit sed. Sed arcu non odio euismod lacinia at quis risus sed.</p>
                        <div class="flex flex-wrap">
                            <div class="w-[156px] h-[156px] border rounded-md mr-5 my-2">
                                <img class="w-full h-full object-cover object-center"
                                    src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}" alt="">
                            </div>
                            <div class="w-[156px] h-[156px] border rounded-md mr-5 my-2">
                                <img class="w-full h-full object-cover object-center"
                                    src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}" alt="">
                            </div>
                            <div class="w-[156px] h-[156px] border rounded-md mr-5 my-2">
                                <img class="w-full h-full object-cover object-center"
                                    src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}" alt="">
                            </div>
                        </div>
                        <button
                            class="bg-[#D50006] text-white text-sm font-semibold py-1 px-5 rounded-lg hover:bg-[#de252b]">Hapus</button>
                        <hr class="border-[#333333]" />
                    </div>
                    {{-- <template x-if="fields.length < 1">
                        <div class="">
                            <p>Klik tombol Tambah di atas untuk menambahkan.</p>
                        </div>
                    </template> --}}
                    <template x-if="fields.length >= 1" class="col">
                        <template x-for="(field, index) in fields" :key="index">
                            <div class="container mx-auto px-5 space-y-3 py-3">
                                <p class="text-base font-semibold text-[#333333]" x-text="field.title"></p>
                                <p class="text-sm font-normal text-[#333333]" x-text="field.description"></p>
                                <div class="flex flex-wrap">
                                    <template x-if="field.images.length > 0">
                                        <template x-for="(image, index) in field.images" :key="index">
                                            <div class="w-[156px] h-[156px] border rounded-md mr-5 my-2">
                                                <img class="w-full h-full object-cover object-center" :src="image"
                                                    alt="">
                                            </div>
                                        </template>
                                    </template>
                                    <template x-if="field.video !== ''">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/lJIrF4YjHfQ"
                                            title="YouTube video player" frameborder="0"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                    </template>
                                </div>
                                <button
                                    class="bg-[#D50006] text-white text-sm font-semibold py-1 px-5 rounded-lg hover:bg-[#de252b]"
                                    @click="removeField(index)">Hapus</button>
                                <hr class="border-[#333333]" />
                            </div>
                        </template>
                    </template>


                    {{-- Add Itinerary --}}
                    <div class="p-5">
                        <template x-if="error !== ''">
                            <div class="flex justify-center">
                                <p class="rounded-md px-3 py-2 bg-red-600 text-white font-semibold text-lg"
                                    x-text='error'>
                                </p>
                            </div>
                        </template>
                        <div class="mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Judul</label>
                            <input type="text" id="text"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Tur Sehat" x-model="title">
                        </div>
                        <div class="pt-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Catatan</label>
                            {{-- <input type="text" id="text"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Catatan" x-model="description"> --}}
                            <div class="w-[10rem] click2edit" id="summernote"
                                x-init="$nextTick(() => { initSummerNote() })">
                            </div>
                        </div>
                        {{-- <x-be.com.wysiwyg></x-be.com.wysiwyg> --}}

                        <div class="flex mt-5 my-3 space-x-3">
                            <div class="form-check">
                                <input
                                    class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                    type="radio" name="radio_itinerary" id="radio_image" value="images"
                                    x-model="radio_itinerary">
                                <label class="form-check-label inline-block text-gray-800" for="radio_image">
                                    Image Itinerary
                                </label>
                            </div>
                            <div class="form-check">
                                <input
                                    class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                    type="radio" name="radio_itinerary" id="radio_video" value="videos"
                                    x-model="radio_itinerary">
                                <label class="form-check-label inline-block text-gray-800" for="radio_video">
                                    Video Itinerary
                                </label>

                            </div>
                        </div>

                        {{-- Photo --}}
                        <template x-if="radio_itinerary === 'images'">
                            <div class="py-2 mb-6">
                                <p class="font-semibold text-[#BDBDBD]">Foto</p>
                                <template x-if="images.length < 1">
                                    <div
                                        class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                        <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                            width="20px" height="20px">
                                    </div>
                                </template>
                                <template x-if="images.length >= 1">
                                    <div class="flex">
                                        <template x-for="(image, index) in images" :key="index">
                                            <div class="flex justify-center items-center">
                                                <img :src="image"
                                                    class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                    :alt="'upload'+index">
                                                <button class="absolute mx-2 translate-x-12 -translate-y-14"><img
                                                        src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                        width="25px" @click="removeImage(index)">
                                                </button>
                                            </div>
                                        </template>
                                    </div>
                                </template>
                                <input class="py-2" type="file" accept="image/*" @change="selectedFile" multiple>
                            </div>
                        </template>

                        {{-- Video --}}
                        <template x-if="radio_itinerary === 'videos'">
                            <div>
                                <p class="font-semibold text-[#BDBDBD]">Video</p>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Video URL (Youtube)" x-model="video">
                            </div>
                        </template>
                    </div>

                    <x-be.com.two-button></x-be.com.two-button>

                </div>
            </div>
        </div>
    </div>

    <script>
        const initSummerNote = () => {
            $('.click2edit').summernote({
                placeholder: 'Hello stand alone ui',
                tabsize: 2,
                height: 120,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']]
                ]
            });
        }

        function itineraryData() {
           
            return {
                title: '',
                description: '',
                radio_itinerary: '',
                video: '',
                images: [],
                fields: [],
                error: '',

                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event) {
                    if (! event.target.files.length) return

                    let file = event.target.files

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''
                        
                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                        };   
                    }
                },

                addNewField() {
                    this.description = $('.click2edit').summernote('code')
                    
                    if(this.title !== '' && this.description !== ''){
                        this.error = ''

                        if (this.radio_itinerary === 'images') {
                            this.fields.push({
                                title:this.title,
                                description:$('.click2edit').summernote('code'),
                                images:this.images,
                                video:''
                            });
                        }

                        if (this.radio_itinerary === 'videos') {
                            this.images = []
                            this.fields.push({
                                title:this.title,
                                description:$('.click2edit').summernote('code'),
                                images:[],
                                video:this.video
                            });
                        }

                        this.title = ''
                        this.description = ''
                        this.images = []
                        this.video = ''
                        this.radio_itinerary = ''
                    } else {
                        this.error = 'Judul dan catatan harus terisi'
                    }
                },

                removeField(index) {
                    this.fields.splice(index, 1);
                },

                removeImage(index) {
                    this.images.splice(index, 1);
                }
            }
        }
    </script>

    {{-- <script>
        let data = ''
        const initSummerNote = () => {
            $('.click2edit').summernote({
                placeholder: 'Hello stand alone ui',
                tabsize: 2,
                height: 120,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']]
                ]
            });

            // const getSummerNote = () => {
            //     return $('.click2edit').summernote('code');
            // }

            data = $('.click2edit').summernote('code');

            
        }

        console.log(data)
    </script> --}}

    {{-- <script>
        function displayImage() {

            return {
                imageUrl: '',
                images: [],

                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event) {
                    if (! event.target.files.length) return

                    let file = event.target.files

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''
                        
                        reader.readAsDataURL(file[0]);
                        // srcImg = reader.onload = e => callback(e.target.result);  
                        // srcImg = e.target.result; 
                        // this.image = [...image, srcImg]
                        // console.log(i, reader.result)
                        reader.onload = e => {
                            // console.log(i,  e.target.result)
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                            console.log(i, srcImg)
                            console.log(i, this.images)
                        };
                        
                    }
                
                // let file = event.target.files[0],
                //     reader = new FileReader()

                //     console.log(event.target.result)

                // reader.readAsDataURL(file)
                // reader.onload = e => callback(e.target.result)
                },
            }
        }
    </script> --}}
</body>

</html>