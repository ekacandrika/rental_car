<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param||null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        {{-- <x-be.kelolatoko.sidebar-toko></x-be.kelolatoko.sidebar-toko> --}}

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
                {{-- Breadcumbs --}}
                <nav class="flex" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            <a href="informasi"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="itinerary"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Buat
                                    Itinerary</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="harga"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Harga
                                    & Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="bayar"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="maps"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Peta
                                    & Foto</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="pilihan"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Pilihan
                                    & Ekstra</a>
                            </div>
                        </li>
                    </ol>
                </nav>

                {{-- Section --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded">
                    <div class="flex justify-between">
                        <div class="text-lg font-bold font-inter text-[#9E3D64]">
                            Batas Waktu Pembayaran & Pembatalan
                        </div>
                    </div>

                    <div class="flex justify-items-center items-center align-middle my-2">
                        <span>Batas waktu pembayaran </span>
                        <input type="text" id="text"
                            class="bg-[#FFFFFF] mx-2 border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-12 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="61">
                        <span>hari sebelum keberangkatan.</span>
                    </div>

                    <p class="text-xs text-[#D50006]">*Batas Waktu Pembayaran Lebih Lama Daripada Batas Waktu Pembatalan
                    </p>

                    <div class="my-5" x-data="handler()">
                        <p class="pb-1">Kebijakan Pembatalan:</p>
                        <button class="text-white bg-[#23AEC1] py-1 px-3 rounded-md"
                            @click="addNewField()">Tambah</button>

                        {{-- Tabel --}}
                        <div class="my-7">
                            <table class="my-5 w-1/2 border table-auto rounded-md">
                                <thead>
                                    <tr>
                                        <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Hari Sebelumnya Sampai</th>
                                        <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Hari Sebelumnya</th>
                                        <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Potongan (%)</th>
                                        <th class="p-3 border border-slate-500 bg-[#BDBDBD]"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <template x-if="fields.length < 1">
                                        <div class="px-3">
                                            <p>Klik tombol Tambah di atas untuk menambahkan.</p>
                                        </div>
                                    </template>
                                    <template x-if="fields.length >= 1" class="col">
                                        <template x-for="(field, index) in fields" :key="index">
                                            <tr class="align-middle">
                                                <td class="p-3 border border-slate-500 text-center">
                                                    <input type="text" id="text"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="3" x-model="field.day_before">
                                                </td>
                                                <td class="p-3 border border-slate-500 text-center">
                                                    <input type="text" id="text"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="5" x-model="field.day_until">
                                                </td>
                                                <td class=" p-3 border border-slate-500 text-center">
                                                    <input type="text" id="text"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="5" x-model="field.discount">
                                                </td>
                                                <template x-if="index === (fields.length - 1)">
                                                    <td class="p-3 border border-slate-500 text-center">
                                                        <button class="mx-1"><img
                                                                src="{{ asset('storage/icons/delete-dynamic-data.png') }}"
                                                                alt="" width="16px"
                                                                @click="removeField(index)"></button>
                                                    </td>
                                                </template>
                                                <template x-if="index !== (fields.length - 1)">
                                                    <td class="p-3 border border-slate-500 text-center">
                                                        <button class="mx-1"><img
                                                                src="{{ asset('storage/icons/pencil-solid 1.svg')}}"
                                                                alt="" width="16px" @click="addNewField()"></button>
                                                        <button class="mx-1"><img
                                                                src="{{ asset('storage/icons/delete-dynamic-data.png') }}"
                                                                alt="" width="16px"
                                                                @click="removeField(index)"></button>
                                                    </td>
                                                </template>
                                            </tr>
                                        </template>
                                    </template>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <x-be.com.two-button></x-be.com.two-button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function handler() {
                    
            return {
                fields: [],
                addNewField() {
                    this.fields.push({
                        day_before:'',
                        day_until:'',
                        discount:''
                    });
                },
                removeField(index) {
                    this.fields.splice(index, 1);
                }
            }
        }
    </script>
</body>

</html>