<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
        rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css" rel="stylesheet">

    <!-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>  -->

    {{-- @include(‘sweet::alert’) --}}
    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        #private {
            display: block;
        }

        #open {
            display: none;
        }

        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }

        /*tags*/
        .bootstrap-tagsinput .tag {
            margin-right: 2px;
            color: #ffffff;
            background: #2196f3;
            padding: 3px 7px;
            border-radius: 3px;
        }

        .disabled {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
        }

        .bootstrap-tagsinput {
            width: 100%;
        }

        .swal-height {
            height: 90vh;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
                {{-- @dump($add_listing) --}}
                {{-- Breadcumbs --}}
                <nav class="flex" aria-label="Breadcrumb">
                    @if($isedit)
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            <a href="{{route('tur.viewAddListingEdit',$id)}}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                {{-- 'hover:text-[#23AEC1]':'text-[#5f5e5e] disabled' --}}
                                <a href="{{route('tur.viewItenenarydit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['itenenary']) ? ($tur['itenenary']!= '' ? 'text-[#333333] hover:text-[#23AEC1]' : 'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled'}}"
                                    data-role="disabled">Buat
                                    Itinerary</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewHargaEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['harga']) ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled'}}">Harga
                                    & Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewBatasEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['batas']) ? ($tur['batas']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled'}}">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPetaEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['peta']) ? ($tur['peta']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled'):'text-[#5f5e5e] disabled'}}">Peta
                                    & Foto</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPilihanEkstraEdit',$id)}}" {{-- $tur['pilihan_ekstra']!=''
                                    ? ' hover:text-[#23AEC1]' :'text-[#5f5e5e] disabled' --}}
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['pilihan_ekstra']) ? ($tur['pilihan_ekstra']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled' }}">Pilihan
                                    & Ekstra</a>
                            </div>
                        </li>
                    </ol>
                    @else
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            <a href="{{route('tur')}}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                {{-- 'hover:text-[#23AEC1]':'text-[#5f5e5e] disabled' --}}
                                <a href="{{route('tur.viewItenenary')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['itenerary']) ? ($tur['itenerary']!= '' ? 'text-[#333333] hover:text-[#23AEC1]' : 'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled'}}"
                                    data-role="disabled">Buat
                                    Itinerary</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewHarga')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['harga']) ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled'}}">Harga
                                    & Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewBatas')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['batas']) ? ($tur['batas']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled'}}">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPeta')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['peta']) ? ($tur['peta']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled'):'text-[#5f5e5e] disabled'}}">Peta
                                    & Foto</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPilihanEkstra')}}" {{-- $tur['pilihan_ekstra']!=''
                                    ? ' hover:text-[#23AEC1]' :'text-[#5f5e5e] disabled' --}}
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['pilihan_ekstra']) ? ($tur['pilihan_ekstra']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled' }}">Pilihan
                                    & Ekstra</a>
                            </div>
                        </li>
                    </ol>
                    @endif
                </nav>

                <div class="flex gap-5 pt-5">
                    @if($isedit)
                        @if($add_listing['tipe_tur']=='Tur Private')
                        <div class="flex items-center py-px">
                            <input id="radio_private" name="radio_itinerary" x-model="radio_itinerary" type="radio"
                                value="private"
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                checked>
                            <label for="radio_private" class="ml-2 text-sm font-semibold text-[#333333]">Tur Private</label>
                        </div>
                        @else
                        <div class="flex items-center py-px">
                            <input id="radio_open" name="radio_itinerary" x-model="radio_itinerary" type="radio"
                                value="open"
                                class="w-4 h-4 text-blue-600 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="radio_open" class="ml-2 text-sm font-semibold text-[#333333]">Open Trip</label>
                        </div>
                        @endif
                    @else
                    <div class="flex items-center py-px">
                        <input id="radio_private" name="radio_itinerary" x-model="radio_itinerary" type="radio"
                            value="private"
                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                            checked>
                        <label for="radio_private" class="ml-2 text-sm font-semibold text-[#333333]">Tur Private</label>
                    </div>
                    <div class="flex items-center py-px">
                        <input id="radio_open" name="radio_itinerary" x-model="radio_itinerary" type="radio"
                            value="open"
                            class="w-4 h-4 text-blue-600 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                        <label for="radio_open" class="ml-2 text-sm font-semibold text-[#333333]">Open Trip</label>
                    </div>
                    @endif
                </div>

                <div id="private">
                    <form action="{{ $isedit==true ? route('tur.addListingPrivate.edit',$id) : route('tur.addListingPrivate') }}" method="POST" id="formTurPrivate">
                        @csrf
                        @if($isedit)
                        @method('PUT')
                        @endif
                        <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                            <div class="grid grid-cols-2">
                                <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Isi Informasi Dasar</div>
                                {{-- <div class="text-sm font-bold font-inter p-5 text-[#333333] text-right">ID Listing:
                                    1TO101082022
                                </div> --}}
                            </div>

                            <div class="px-5 mb-6">
                                <label for="" class="block mb-2 text-sm font-bold text-[#333333]">Judul<span
                                        class="text-red-500 font-bold">*</span></label>
                                <input type="text" id="product_name_private" name="product_name"
                                    class="productNamePrivate bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Tur Sehat">
                            </div>
                            @if(isset($add_listing['tipe_tur']))
                            <fieldset class="px-5">
                                @if($add_listing['tipe_tur']=='Tur Private')

                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Konfirmasi
                                    Paket<span class="text-red-500 font-bold">*</span></label>
                                <div class="pb-5">
                                    <div class="flex space-x-16">
                                        <div class="form-check">
                                            <input
                                                class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                type="radio" name="confirmation_radio_button"
                                                x-model="confirmation_radio_button" id="status" value="instant"
                                                {{isset($add_listing['confirmation_radio_button']) ?
                                                ($add_listing['confirmation_radio_button']=='instant' ? "checked=''"
                                                : '' ) : '' }}>
                                            <label
                                                class="form-check-label inline-block text-sm font-medium text-gray-800"
                                                for="status"> Instant </label>
                                        </div>
                                        <div class="form-check">
                                            <input
                                                class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                type="radio" name="confirmation_radio_button"
                                                x-model="confirmation_radio_button" id="status" value="by_seller"
                                                {{isset($add_listing['confirmation_radio_button']) ?
                                                ($add_listing['confirmation_radio_button']=='by_seller' ? "checked=''"
                                                : '' ) : '' }}>
                                            <label
                                                class="form-check-label inline-block text-sm font-medium text-gray-800"
                                                for="status"> Konfirmasi by Seller </label>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Konfirmasi
                                    Paket<span class="text-red-500 font-bold">*</span></label>
                                <div class="pb-5">
                                    <div class="flex space-x-16">
                                        <div class="form-check">
                                            <input
                                                class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                type="radio" name="confirmation_radio_button"
                                                x-model="confirmation_radio_button" id="status" value="instant">
                                            <label
                                                class="form-check-label inline-block text-sm font-medium text-gray-800"
                                                for="status"> Instant </label>
                                        </div>
                                        <div class="form-check">
                                            <input
                                                class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                type="radio" name="confirmation_radio_button"
                                                x-model="confirmation_radio_button" id="status" value="by_seller">
                                            <label
                                                class="form-check-label inline-block text-sm font-medium text-gray-800"
                                                for="status"> Konfirmasi by Seller </label>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </fieldset>
                            @else
                            <fieldset class="px-5">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Konfirmasi
                                    Paket<span class="text-red-500 font-bold">*</span></label>
                                <div class="pb-5">
                                    <div class="flex space-x-16">
                                        <div class="form-check">
                                            <input
                                                class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                type="radio" name="confirmation_radio_button"
                                                x-model="confirmation_radio_button" id="status" value="instant">
                                            <label
                                                class="form-check-label inline-block text-sm font-medium text-gray-800"
                                                for="status"> Instant </label>
                                        </div>
                                        <div class="form-check">
                                            <input
                                                class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                type="radio" name="confirmation_radio_button"
                                                x-model="confirmation_radio_button" id="status" value="by_seller">
                                            <label
                                                class="form-check-label inline-block text-sm font-medium text-gray-800"
                                                for="status"> Konfirmasi by Seller </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            @endif
                            {{-- Tag Lokasi --}}
                            @if(isset($add_listing['tipe_tur']))
                            <div class="block px-5">
                                @if($add_listing['tipe_tur']=='Tur Private')
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 1<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="mt-1 rounded-md shadow-sm">
                                    <select name="tag_location_1" id="provinsi_private_1"
                                        class="provinsi_private_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled>Pilih Lokasi</option>
                                        @foreach ($island as $pulau)
                                        <option value="{{$pulau->id}}" {{isset($add_listing['tag_location_1']) ? ($add_listing['tag_location_1']==$pulau->id ? 'selected':null) : null}}>{{$pulau->island_name}}</option>
                                        @endforeach
                                        {{-- <option value="{{ $provinsi_data['provinces_id'] }}"
                                            {{isset($add_listing['tag_location_1']) ?
                                            ($add_listing['tag_location_1']==$provinsi_data['provinces_id'] ? 'selected'
                                            :'') :''}}>{{ $provinsi_data['provinces_name'] }}</option>
                                        @foreach ($provinsi_data['regency'] as $key => $regency)
                                        <option value="{{ $regency['regencies_id'] }}"
                                            {{isset($add_listing['tag_location_1']) ?
                                            ($add_listing['tag_location_1']==$regency['regencies_id'] ? 'selected' :'')
                                            :''}}>{{ $regency['regencies_name'] }}</option>
                                        @endforeach
                                        @foreach ($provinsi_data['district'] as $key => $district)
                                        <option value="{{ $district['districts_id'] }}"
                                            {{isset($add_listing['tag_location_1']) ?
                                            ($add_listing['tag_location_1']==$district['districts_id'] ? 'selected' :'')
                                            :''}}>{{ $district['districts_name'] }}</option>
                                        @endforeach --}}

                                    </select>
                                </div>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>

                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 2<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="mt-1 rounded-md shadow-sm">
                                    <select name="tag_location_2" id="provinsi_private_2"
                                        class="provinsi_private_2 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled>Pilih Lokasi</option>
                                        @foreach ($provinsi as $provinsi_data)
                                        <option value="{{$provinsi_data->id}}" {{isset($add_listing['tag_location_2']) ? ($add_listing['tag_location_2']==$provinsi_data->id ? 'selected':'') :''}}>{{$provinsi_data->name}}</option>
                                        @endforeach
                                        {{-- @foreach ($provinsi as $provinsi_data)
                                        <option value="{{ $provinsi_data['provinces_id'] }}"
                                            {{isset($add_listing['tag_location_2']) ?
                                            ($add_listing['tag_location_2']==$provinsi_data['provinces_id']? 'selected'
                                            :'') :''}}>{{ $provinsi_data['provinces_name'] }}</option>
                                        @foreach ($provinsi_data['regency'] as $key => $regency)
                                        <option value="{{ $regency['regencies_id'] }}"
                                            {{isset($add_listing['tag_location_2']) ?
                                            ($add_listing['tag_location_2']==$regency['regencies_id'] ? 'selected' :'')
                                            :''}}>{{ $regency['regencies_name'] }}</option>
                                        @endforeach
                                        @foreach ($provinsi_data['district'] as $key => $district)
                                        <option value="{{ $district['districts_id'] }}"
                                            {{isset($add_listing['tag_location_2']) ?
                                            ($add_listing['tag_location_2']==$district['districts_id'] ? 'selected' :'')
                                            :''}}>{{ $district['districts_name'] }}</option>
                                        @endforeach
                                        @endforeach --}}

                                    </select>
                                </div>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>

                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 3<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="mt-1 rounded-md shadow-sm">
                                    <select name="tag_location_3" id="regency_private_1"
                                        class="regency_private_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled>Pilih Lokasi</option>
                                        @foreach ($kota as $kota_data)
                                        <option value="{{$kota_data->id}}" {{isset($add_listing['tag_location_3']) ? ($add_listing['tag_location_3']==$kota_data->id ? 'selected' : '') : ''}}>{{$kota_data->name}}</option>
                                        @endforeach
                                        {{-- @foreach ($provinsi as $provinsi_data)
                                        <option value="{{ $provinsi_data['provinces_id'] }}"
                                            {{isset($add_listing['tag_location_3']) ?
                                            ($add_listing['tag_location_3']==$provinsi_data['provinces_id'] ? 'selected'
                                            :'') :''}}>{{ $provinsi_data['provinces_name'] }}</option>
                                        @foreach ($provinsi_data['regency'] as $key => $regency)
                                        <option value="{{ $regency['regencies_id'] }}"
                                            {{isset($add_listing['tag_location_3']) ?
                                            ($add_listing['tag_location_3']==$regency['regencies_id'] ? 'selected' :'')
                                            :''}}>{{ $regency['regencies_name'] }}</option>
                                        @endforeach
                                        @foreach ($provinsi_data['district'] as $key => $district)
                                        <option value="{{ $district['districts_id'] }}"
                                            {{isset($add_listing['tag_location_3']) ?
                                            ($add_listing['tag_location_3']==$district['districts_id'] ? 'selected' :'')
                                            :''}}>{{ $district['districts_name'] }}</option>
                                        @endforeach
                                        @endforeach --}}

                                    </select>
                                </div>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>

                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 4<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="mt-1 rounded-md shadow-sm">
                                    <select name="tag_location_4" id="regency_private_2"
                                        class="regency_private_2 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled>Pilih Lokasi</option>
                                        @foreach($objek as $obj)
                                        <option value="{{$obj->id}}" {{isset($add_listing['tag_location_4']) ? ($add_listing['tag_location_4'] == $obj->id ? 'selected' : '') : ''}}>{{$obj->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                                <div class="mb-6 mt-2" x-data="tagAdd()">
                                    @if(isset($add_listing['new_tag_location']))
                                    <div class="mb-2" data-id="private tur edit" x-data="tagSelectAlpine()" x-init="$nextTick(() => { select2Alpine() } )">
                                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis<span class="text-red-500 font-bold">*</span></label>
                                        @foreach ($add_listing['new_tag_location']['results'] as $key => $tag_location)
                                        <div class="mt-1 rounded-md shadow-sm">
                                            <select name="new_tag_location[]" id="new_tag_location_1"
                                                x-ref="select_dynamic"
                                                class="new_tag_location mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                                <option selected disabled>Pilih Lokasi</option>
                                                @foreach ($dynamic_tag as $key => $tag)
                                                    @foreach ($tag as $pulau)
                                                    <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}" {{$pulau['id_pulau'] == $tag_location ? 'selected':''}}>{{ $pulau['nama_pulau']}}</option>
                                                        @foreach ($pulau['provinsi'] as $provinsi)
                                                            <option value="{{ $provinsi['id_provinsi']}}" {{$provinsi['id_provinsi'] == $tag_location ? 'selected':''}}>{{ $provinsi['nama_provinsi']}}</option>
                                                            @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                <option value="{{ $kabupaten['id_kabupaten']}}" {{$kabupaten['id_kabupaten'] == $tag_location ? 'selected':''}}>{{ $kabupaten['nama_kabupaten']}}</option>
                                                                @if(isset($kabupaten['wisata']))
                                                                @foreach ($kabupaten['wisata'] as $wisata)
                                                                    <option value="{{ $wisata['id_wisata']}}" {{$wisata['id_wisata'] == $tag_location ? 'selected':''}}>{{ $wisata['nama_wisata']}}</option>
                                                                @endforeach
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            </select>
                                        </div>
                                        @endforeach
                                    </div>
                                    @else
                                    <template x-if="tag_array.length == 0">
                                        <div class="mb-2" data-id="private tur edit" x-data="tagSelectAlpine()" x-init="$nextTick(() => { select2Alpine() } )">
                                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis Edit<span
                                                class="text-red-500 font-bold">*</span></label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <select name="new_tag_location[]" id="new_tag_location_1"
                                                    x-ref="select_dynamic"
                                                    class="new_tag_location mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                                    <option selected disabled>Pilih Lokasi</option>
                                                    @foreach ($dynamic_tag as $key => $tag)
                                                        @foreach ($tag as $pulau)
                                                        <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}">{{ $pulau['nama_pulau']}}</option>
                                                            @foreach ($pulau['provinsi'] as $provinsi)
                                                                <option value="{{ $provinsi['id_provinsi']}}">{{ $provinsi['nama_provinsi']}}</option>
                                                                @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                    <option value="{{ $kabupaten['id_kabupaten']}}">{{ $kabupaten['nama_kabupaten']}}</option>
                                                                    @if(isset($kabupaten['wisata']))
                                                                    @foreach ($kabupaten['wisata'] as $wisata)
                                                                        <option value="{{ $wisata['id_wisata']}}">{{ $wisata['nama_wisata']}}</option>
                                                                    @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div
                                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                            </div>
                                        </div>
                                    </template>
                                    @endif
                                        <template x-if="tag_array.length >= 1">
                                            <div class="mb-2">
                                                <template x-for="(tag, index) in tag_array" :key="index">
                                                        <div class="relative mt-2 rounded-md shadow-sm">
                                                            <select name="new_tag_location[]" id="new_tag_location"
                                                                class="new_tag_location mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                                                <option selected disabled>Pilih Lokasi</option>
                                                                @foreach ($dynamic_tag as $key => $tag)
                                                                    @foreach ($tag as $pulau)
                                                                    <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}">{{ $pulau['nama_pulau']}}</option>
                                                                        @foreach ($pulau['provinsi'] as $provinsi)
                                                                            <option value="{{ $provinsi['id_provinsi']}}">{{ $provinsi['nama_provinsi']}}</option>
                                                                            @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                                <option value="{{ $kabupaten['id_kabupaten']}}">{{ $kabupaten['nama_kabupaten']}}</option>
                                                                                @if(isset($kabupaten['wisata']))
                                                                                @foreach ($kabupaten['wisata'] as $wisata)
                                                                                    <option value="{{ $wisata['id_wisata']}}">{{ $wisata['nama_wisata']}}</option>
                                                                                @endforeach
                                                                                @endif
                                                                            @endforeach
                                                                        @endforeach
                                                                    @endforeach
                                                                @endforeach
                                                            </select>
                                                            <div class="absolute">
                                                                <button type="button" class="rounded-full">
                                                                    <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt="" width="25px" @click="removeField(index)">
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div
                                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                                        </div>
                                                </template>
                                            </div>
                                        </template>
                                    <div class="absolute" style="top:752;">
                                        <button type="button" @click="add()" class="bg-kamtuu-primary rounded-md text-white text-sm px-3 py-2">Tambah Tag</button>
                                    </div>
                                </div>
                                @else
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 1<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="mt-1 rounded-md shadow-sm">
                                    <select name="tag_location_1" id="provinsi_private_1"
                                        class="provinsi_private_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled>Pilih Lokasi</option>
                                        @foreach ($island as $pulau)
                                        <option value="{{$pulau->id}}" {{isset($add_listing['tag_location_1']) ? ($add_listing['tag_location_1']==$pulau->id ? 'selected':null) : null}}>{{$pulau->island_name}}</option>
                                        @endforeach
                                        {{-- @foreach ($provinsi as $provinsi_data)
                                        <option value="{{ $provinsi_data['provinces_id'] }}">{{
                                            $provinsi_data['provinces_name'] }}</option>
                                        @foreach ($provinsi_data['regency'] as $key => $regency)
                                        <option value="{{ $regency['regencies_id'] }}">{{ $regency['regencies_name'] }}
                                        </option>
                                        @endforeach
                                        @foreach ($provinsi_data['district'] as $key => $district)
                                        <option value="{{ $district['districts_id'] }}"></option>
                                        @endforeach
                                        @endforeach --}}

                                    </select>
                                </div>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>

                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 2<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="mt-1 rounded-md shadow-sm">
                                    <select name="tag_location_2" id="provinsi_open_2"
                                        class="provinsi_private_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled>Pilih Lokasi</option>
                                        @foreach ($province as $provinsi_data)
                                        <option value="{{$provinsi_data->id}}" {{isset($add_listing['tag_location_2']) ? ($add_listing['tag_location_2']==$provinsi_data->id ? 'selected':'') :''}}>{{$provinsi_data->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>

                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 3<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="mt-1 rounded-md shadow-sm">
                                    <select name="tag_location_3" id="regency_open_1"
                                        class="provinsi_private_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled>Pilih Lokasi</option>
                                        @foreach ($kota as $kota_data)
                                        <option value="{{$kota_data->id}}" {{isset($add_listing['tag_location_3']) ? ($add_listing['tag_location_3']==$kota_data->id ? 'selected' : '') : ''}}>{{$kota_data->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>

                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 4<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="mt-1 rounded-md shadow-sm">
                                    <select name="tag_location_4" id="regency_open_2"
                                        class="provinsi_private_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled>Pilih Lokasi</option>
                                        @foreach($objek as $obj)
                                        <option value="{{$obj->id}}" {{isset($add_listing['tag_location_4']) ? ($add_listing['tag_location_4'] == $obj->id ? 'selected' : '') : ''}}>{{$obj->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                                <div class="relative mb-6 mt-2" x-data="tagAdd()">
                                    <div class="mb-2">
                                       <template x-if="tag_array.length == 0">
                                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis<span
                                                class="text-red-500 font-bold">*</span></label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <select name="new_tag_location[]" id="new_tag_location"
                                                    class="new_tag_location mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                                    <option selected disabled>Pilih Lokasi</option>
                                                    {{-- @foreach($objek as $obj) --}}
                                                    {{-- <option value="{{$obj->id}}" {{isset($add_listing['tag_location_4']) ? ($add_listing['tag_location_4'] == $obj->id ? 'selected' : '') : ''}}>{{$obj->title}}</option> --}}
                                                    {{-- @endforeach --}}
                                                </select>
                                            </div>
                                            <div
                                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                            </div>
                                        </template>
                                    </div> 
                                    <template x-if="tag_array.length >= 1">
                                        <template x-for="(tag, index) in tag_array" :key="index">
                                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis<span
                                            class="text-red-500 font-bold">*</span></label>
                                        <div class="mb-2">
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <select name="new_tag_location[]" id="new_tag_location"
                                                    class="new_tag_location mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                                    <option selected disabled>Pilih Lokasi</option>
                                                    @foreach($objek as $obj)
                                                    <option value="{{$obj->id}}" {{isset($add_listing['tag_location_4']) ? ($add_listing['tag_location_4'] == $obj->id ? 'selected' : '') : ''}}>{{$obj->title}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="absolute right-[650px]">
                                                    <button type="button" class="rounded-full">
                                                        <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt="" width="25px" @click="removeField(index)">
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                        </div>
                                        </template>
                                    </template>
                                    <div class="absolute top-[72px] left-0" style="top:72px;">
                                        <button type="button" @click="add()" class="bg-kamtuu-primary rounded-md text-white text-sm px-3 py-2">Tambah Tag</button>
                                    </div>
                                </div>                                
                                @endif
                            </div>
                            @else
                            <div class="block px-5">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 1<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="mt-1 rounded-md shadow-sm">
                                    <select name="tag_location_1" id="provinsi_private_1"
                                        class="provinsi_private_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled>Pilih Lokasi</option>
                                        @foreach ($island as $pulau)
                                        <option value="{{$pulau->id}}" {{isset($add_listing['tag_location_1']) ? ($add_listing['tag_location_1']==$pulau->id ? 'selected':null) : null}}>{{$pulau->island_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>

                                <label for="text" class="block mb-2 mt-2 text-sm font-bold text-[#333333]">Tag Lokasi 2</label>
                                <div class="mt-1 rounded-md shadow-sm">
                                    <select name="tag_location_2" id="provinsi_private_2"
                                        class="provinsi_private_2 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled>Pilih Lokasi</option>
                                        @foreach ($province as $provinsi_data)
                                        <option value="{{$provinsi_data->id}}" {{isset($add_listing['tag_location_2']) ? ($add_listing['tag_location_2']==$provinsi_data->id ? 'selected':'') :''}}>{{$provinsi_data->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>

                                <label for="text" class="block mb-2 mt-2 text-sm font-bold text-[#333333]">Tag Lokasi
                                    3</label>
                                {{-- <span class="text-red-500 font-bold">*</span> --}}
                                <div class="mt-1 rounded-md shadow-sm">
                                    <select name="tag_location_3" id="regency_private_1"
                                        class="regency_private_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled>Pilih Lokasi</option>
                                        @foreach ($kota as $kota_data)
                                            <option value="{{$kota_data->id}}">{{$kota_data->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>

                                <label for="text" class="block mb-2 mt-2 text-sm font-bold text-[#333333]">Tag Lokasi 4</label>
                                <div class="mt-1 rounded-md shadow-sm">
                                    <select name="tag_location_4" id="regency_private_2"
                                        class="regency_private_2 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                        <option selected disabled>Pilih Lokasi</option>
                                        @foreach($objek as $obj)
                                        <option value="{{$obj->id}}" {{isset($add_listing['tag_location_4']) ? ($add_listing['tag_location_4'] == $obj->id ? 'selected' : '') : ''}}>{{$obj->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                                <div class="mb-6 mt-2" x-data="tagAdd()">
                                    <template x-if="tag_array.length == 0">
                                        {{-- Tag lokasi dinamis pertama --}}
                                        <div class="mb-2" data-id="1" x-data="tagSelectAlpine()" x-init="$nextTick(() => { select2Alpine() } )">
                                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis<span
                                                class="text-red-500 font-bold">*</span></label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <select name="new_tag_location[]" id="new_tag_location_1"
                                                    x-ref="select_dynamic"
                                                    class="new_tag_location mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                                    <option selected disabled>Pilih Lokasi</option>
                                                    @foreach ($dynamic_tag as $key => $tag)
                                                        @foreach ($tag as $pulau)
                                                        <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}">{{ $pulau['nama_pulau']}}</option>
                                                            @foreach ($pulau['provinsi'] as $provinsi)
                                                                <option value="{{ $provinsi['id_provinsi']}}">{{ $provinsi['nama_provinsi']}}</option>
                                                                @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                    <option value="{{ $kabupaten['id_kabupaten']}}">{{ $kabupaten['nama_kabupaten']}}</option>
                                                                    @if(isset($kabupaten['wisata']))
                                                                    @foreach ($kabupaten['wisata'] as $wisata)
                                                                        <option value="{{ $wisata['id_wisata']}}">{{ $wisata['nama_wisata']}}</option>
                                                                    @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div
                                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                            </div>
                                        </div>
                                    </template>
                                    <template x-if="tag_array.length >= 1">
                                        <template x-for="(tag, index) in tag_array" :key="index">
                                            <div class="mb-2">
                                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis<span
                                                    class="text-red-500 font-bold">*</span></label>
                                                <div class="flex mt-2 rounded-md shadow-sm">
                                                    <select name="new_tag_location[]" id="new_tag_location"
                                                        class="new_tag_location mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                                        <option selected disabled>Pilih Lokasi</option>
                                                        @foreach ($dynamic_tag as $key => $tag)
                                                            @foreach ($tag as $pulau)
                                                            <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}">{{ $pulau['nama_pulau']}}</option>
                                                                @foreach ($pulau['provinsi'] as $provinsi)
                                                                    <option value="{{ $provinsi['id_provinsi']}}">{{ $provinsi['nama_provinsi']}}</option>
                                                                    @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                        <option value="{{ $kabupaten['id_kabupaten']}}">{{ $kabupaten['nama_kabupaten']}}</option>
                                                                        @if(isset($kabupaten['wisata']))
                                                                        @foreach ($kabupaten['wisata'] as $wisata)
                                                                            <option value="{{ $wisata['id_wisata']}}">{{ $wisata['nama_wisata']}}</option>
                                                                        @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach
                                                            @endforeach
                                                        @endforeach
                                                    </select>
                                                    <div class="absolute right-[650px]">
                                                        <button type="button" class="rounded-full">
                                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt="" width="25px" @click="removeField(index)">
                                                        </button>
                                                    </div>
                                                </div>
                                                <div
                                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                                </div>
                                            </div>
                                        </template>
                                    </template>
                                    <div class="absolute" style="top:752;">
                                        <button type="button" @click="add()" class="bg-kamtuu-primary rounded-md text-white text-sm px-3 py-2">Tambah Tag</button>
                                    </div>
                                </div>
                            </div>
                            
                            @endif


                            <div class="grid grid-cols-2">
                                <div class="flex">
                                    <div class="px-5 my-5">
                                        <label for="" class="block mb-2 text-sm font-bold text-[#333333]">Hari<span
                                                class="text-red-500 font-bold">*</span></label>
                                        <input type="text" id="jml_hari" name="jml_hari"
                                            class="jmlHariPrivate bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Hari">
                                    </div>

                                    <div class="pr-5 my-5">
                                        <label for="" class="block mb-2 text-sm font-bold text-[#333333]">Malam<span
                                                class="text-red-500 font-bold">*</span></label>
                                        <input type="text" id="jml_malam" name="jml_malam"
                                            class="jmlMalamPrivate bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Malam">
                                    </div>
                                </div>
                            </div>

                            <fieldset class="px-5">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tipe
                                    Tur</label>
                                <input type="text" id="status" name="status" value="Tur Private"
                                    class="bg-[#F2F2F2] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 placeholder-[#333333]"
                                    placeholder="Tur Private" readonly>
                            </fieldset>

                            <div class="grid grid-cols-2">
                                <div class="flex">
                                    <div class="px-5 my-5">
                                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Peserta
                                            Min<span class="text-red-500 font-bold">*</span></label>
                                        <input type="text" id="min_orang_private" name="min_orang"
                                            class="minOrangPrivate bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="3">
                                    </div>

                                    <div class="pr-5 my-5">
                                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Peserta
                                            Max<span class="text-red-500 font-bold">*</span></label>
                                        <input type="text" id="max_orang_private" name="max_orang"
                                            class="maxOrangPrivate bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="5">
                                    </div>
                                </div>
                            </div>
                            @if(isset($add_listing['tipe_tur']))
                            <fieldset class="px-5">

                                @if($add_listing['tipe_tur']=='Tur Private')
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Makanan<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="grid grid-cols-3 w-[600px]">
                                    <div class="flex items-center mb-4 w-40">
                                        <input id="makanan" type="radio" name="makanan" value="Halal"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            {{isset($add_listing['makanan'])? ($add_listing['makanan']=='Halal'
                                            ? 'checked' :'') :'checked'}}>
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Halal
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="makanan" type="radio" name="makanan" value="Vegetarian Available"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            {{isset($add_listing['makanan'])?
                                            ($add_listing['makanan']=='Vegetarian Available' ? 'checked' :'') :''}}>

                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Vegetarian Available
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="makanan" type="radio" name="makanan"
                                            value="Halal & Vegetarian Available"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300  dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            {{isset($add_listing['makanan'])?
                                            ($add_listing['makanan']=='Halal & Vegetarian Available' ? 'checked' :'')
                                            :''}}>

                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Halal & Vegetarian Available
                                        </label>
                                    </div>
                                </div>
                                @else
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Makanan<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="grid grid-cols-3 w-[600px]">
                                    <div class="flex items-center mb-4 w-40">
                                        <input id="makanan" type="radio" name="makanan" value="Halal"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            checked>
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Halal
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="makanan" type="radio" name="makanan" value="Vegetarian Available"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Vegetarian Available
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="makanan" type="radio" name="makanan"
                                            value="Halal & Vegetarian Available"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300  dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Halal & Vegetarian Available
                                        </label>
                                    </div>
                                </div>
                                @endif
                            </fieldset>
                            @else
                            <fieldset class="px-5">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Makanan<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="grid grid-cols-3 w-[600px]">
                                    <div class="flex items-center mb-4 w-40">
                                        <input id="makanan" type="radio" name="makanan" value="Halal"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            checked>
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Halal
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="makanan" type="radio" name="makanan" value="Vegetarian Available"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Vegetarian Available
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="makanan" type="radio" name="makanan"
                                            value="Halal & Vegetarian Available"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300  dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Halal & Vegetarian Available
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            @endif
                            @if(isset($add_listing['tipe_tur']))
                            <fieldset class="px-5">
                                @if($add_listing['tipe_tur']=='Tur Private')
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Bahasa<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="grid grid-cols-3 w-[600px]">
                                    <div class="flex items-center mb-4 w-40">
                                        <input id="bahasa" type="radio" name="bahasa" value="Bahasa Indonesia"
                                            class="indonesia w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            {{isset($add_listing['bahasa'])? ($add_listing['bahasa']=='Bahasa Indonesia'
                                            ? 'checked' :'') :'checked'}}>
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Bahasa Indonesia
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="bahasa" type="radio" name="bahasa" value="English English"
                                            class="english w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            {{isset($add_listing['bahasa'])? ($add_listing['bahasa']=='English English'
                                            ? 'checked' :'') :''}}>
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            English
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="bahasa" type="radio" name="bahasa" value="Indonesia & English"
                                            class="indonesia&english w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            {{isset($add_listing['bahasa'])?
                                            ($add_listing['bahasa']=='Indonesia & English' ? 'checked' :'') :''}}>
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Indonesia & English
                                        </label>
                                    </div>
                                </div>
                                @else
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Bahasa<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="grid grid-cols-3 w-[600px]">
                                    <div class="flex items-center mb-4 w-40">
                                        <input id="bahasa" type="radio" name="bahasa" value="Bahasa Indonesia"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            checked="">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Bahasa Indonesia
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="bahasa" type="radio" name="bahasa" value="English English"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            English
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="bahasa" type="radio" name="bahasa" value="Indonesia & English"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Indonesia & English
                                        </label>
                                    </div>
                                </div>
                                @endif
                            </fieldset>
                            @else
                            <fieldset class="px-5">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Bahasa<span
                                        class="text-red-500 font-bold">*</span></label>
                                <div class="grid grid-cols-3 w-[600px]">
                                    <div class="flex items-center mb-4 w-40">
                                        <input id="bahasa" type="radio" name="bahasa" value="Bahasa Indonesia"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            checked="">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Bahasa Indonesia
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="bahasa" type="radio" name="bahasa" value="English English"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            English
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="bahasa" type="radio" name="bahasa" value="Indonesia & English"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Indonesia & English
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            @endif

                            @if(isset($add_listing['tipe_tur']))
                            <fieldset class="px-5">
                                @if($add_listing['tipe_tur']=='Tur Private')
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tingkat
                                    Petualangan<span class="text-red-500 font-bold">*</span></label>
                                <div class="grid grid-cols-3 w-[600px]">
                                    <div class="flex items-center mb-4 w-40">
                                        <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                            value="Ekstrim"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            {{isset($add_listing['tingkat_petualangan'])?
                                            ($add_listing['tingkat_petualangan']=='Ekstrim' ? 'checked' :'')
                                            :'checked'}}>
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Ekstrim
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                            value="Semi Ekstrim"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900"
                                            {{isset($add_listing['tingkat_petualangan'])?
                                            ($add_listing['tingkat_petualangan']=='Semi Ekstrim' ? 'checked' :'') :''}}>
                                            Semi Ekstrim
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                            value="Tidak Ekstrim"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            {{isset($add_listing['tingkat_petualangan'])?
                                            ($add_listing['tingkat_petualangan']=='Tidak Ekstrim' ? 'checked' :'')
                                            :'checked'}}>
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Tidak Ekstrim
                                        </label>
                                    </div>
                                </div>
                                @else
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tingkat
                                    Petualangan<span class="text-red-500 font-bold">*</span></label>
                                <div class="grid grid-cols-3 w-[600px]">
                                    <div class="flex items-center mb-4 w-40">
                                        <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                            value="Ekstrim"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            checked="">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Ekstrim
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                            value="Semi Ekstrim English"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Semi Ekstrim
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                            value="Tidak Ekstrim"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Tidak Ekstrim
                                        </label>
                                    </div>
                                </div>
                                @endif
                            </fieldset>
                            @else
                            <fieldset class="px-5">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tingkat
                                    Petualangan<span class="text-red-500 font-bold">*</span></label>
                                <div class="grid grid-cols-3 w-[600px]">
                                    <div class="flex items-center mb-4 w-40">
                                        <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                            value="Ekstrim"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                            checked="">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Ekstrim
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                            value="Semi Ekstrim English"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Semi Ekstrim
                                        </label>
                                    </div>
                                    <div class="flex items-center mb-4 w-52">
                                        <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                            value="Tidak Ekstrim"
                                            class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                        <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                            Tidak Ekstrim
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            @endif

                            <div class="px-5 mb-5">
                                <label for="" class="block mb-2 text-sm font-bold text-[#333333]">Kategori
                                    Tur<span class="text-red-500 font-bold">*</span></label>
                                {{-- <input type="text" id="nama_kategori" name="nama_kategori"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Masukan kategori tur yang sesuai dengan paket"> --}}
                                <input data-role="tagsinput" type="text" name="nama_kategori"
                                    id="nama_kategori_private">
                            </div>

                            <div class="px-5" wire:ignore>
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Deskripsi<span
                                        class="text-red-500 font-bold">*</span></label>
                                <textarea class="form-control w-[10rem] deskripsi" name="deskripsi"
                                    id="deskripsi_private"></textarea>
                            </div>

                            <div class="px-5 pt-5" wire:ignore>
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Paket
                                    Termasuk<span class="text-red-500 font-bold">*</span></label>
                                <textarea class="form-control w-[10rem] paket_termasuk" name="paket_termasuk"
                                    id="paket_termasuk_private"></textarea>
                            </div>

                            <div class="px-5 pt-5" wire:ignore>
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Paket
                                    Tidak Termasuk<span class="text-red-500 font-bold">*</span></label>
                                <textarea class="form-control w-[10rem] paket_tidak_termasuk"
                                    name="paket_tidak_termasuk" id="paket_tidak_termasuk_private"></textarea>
                            </div>

                            <div class="px-5 pt-5" wire:ignore>
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Catatan
                                    (Optional)</label>
                                <textarea class="form-control w-[10rem] catatan" name="catatan"
                                    id="catatan_private"></textarea>
                            </div>

                            <div class="p-5">
                                <div class="grid grid-cols-6">
                                    <div class="col-end-8 col-end-2">
                                        <button type="submit"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                            Selanjutnya
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div id="open">
                    @if($isedit)
                    <form action="{{ route('tur.addListingOpen.edit',$id) }}" method="POST"
                        enctype="multipart/form-data" x-data="paketHandler()" x-init="$nextTick(() => addNewField())"
                        id="formTurOpen">
                        @csrf
                        @method('PUT')
                        @else
                        <form action="{{ route('tur.addListingOpen') }}" method="POST" enctype="multipart/form-data"
                            x-data="paketHandler()" x-init="$nextTick(() => addNewField())" id="formTurOpen">
                            @csrf
                            @endif
                            <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                                <div class="grid grid-cols-2">
                                    <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Isi Informasi Dasar
                                    </div>
                                    <div class="text-sm font-bold font-inter p-5 text-[#333333] text-right">ID Listing:
                                        1TO101082022
                                    </div>
                                </div>

                                <div class="px-5 mb-6">
                                    <label for="" class="block mb-2 text-sm font-bold text-[#333333]">Judul<span
                                            class="text-red-500 font-bold">*</span></label>
                                    <input type="text" id="product_name_open" name="product_name"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Tur Sehat" id="product_name_open">
                                </div>
                                @if(isset($add_listing['tipe_tur']))
                                @if($add_listing['tipe_tur']=='Open Trip')
                                <fieldset class="px-5">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Konfirmasi
                                        Paket<span class="text-red-500 font-bold">*</span></label>
                                    <div class="pb-5">
                                        <div class="flex space-x-16">

                                            <div class="form-check">
                                                <input
                                                    class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                    type="radio" name="confirmation_radio_button" id="instant"
                                                    value="instant" {{isset($add_listing['confirmation_radio_button']) ?
                                                    ($add_listing['confirmation_radio_button']=='instant' ? 'checked'
                                                    : '' ) : '' }}>
                                                <label
                                                    class="form-check-label inline-block text-sm font-medium text-gray-800"
                                                    for="instant"> Instant </label>
                                            </div>
                                            <div class="form-check">
                                                <input
                                                    class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                    type="radio" name="confirmation_radio_button" id="by_seller"
                                                    value="by_seller" {{isset($add_listing['confirmation_radio_button'])
                                                    ? ($add_listing['confirmation_radio_button']=='by_seller'
                                                    ? 'checked' : '' ) : '' }}>
                                                <label
                                                    class="form-check-label inline-block text-sm font-medium text-gray-800"
                                                    for="by_seller"> Konfirmasi by Seller </label>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>

                                @else
                                <fieldset class="px-5">

                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Konfirmasi
                                        Paket<span class="text-red-500 font-bold">*</span></label>
                                    <div class="pb-5">
                                        <div class="flex space-x-16">
                                            <div class="form-check">
                                                <input
                                                    class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                    type="radio" name="confirmation_radio_button"
                                                    x-model="confirmation_radio_button" id="instant" value="instant">
                                                <label
                                                    class="form-check-label inline-block text-sm font-medium text-gray-800"
                                                    for="instant"> Instant </label>
                                            </div>
                                            <div class="form-check">
                                                <input
                                                    class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                    type="radio" name="confirmation_radio_button"
                                                    x-model="confirmation_radio_button" id="by_seller"
                                                    value="by_seller">
                                                <label
                                                    class="form-check-label inline-block text-sm font-medium text-gray-800"
                                                    for="by_seller"> Konfirmasi by Seller </label>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                @endif
                                @else
                                <fieldset class="px-5">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Konfirmasi
                                        Paket<span class="text-red-500 font-bold">*</span></label>
                                    <div class="pb-5">
                                        <div class="flex space-x-16">
                                            <div class="form-check">
                                                <input
                                                    class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                    type="radio" name="confirmation_radio_button"
                                                    x-model="confirmation_radio_button" id="instant" value="instant">
                                                <label
                                                    class="form-check-label inline-block text-sm font-medium text-gray-800"
                                                    for="instant"> Instant </label>
                                            </div>
                                            <div class="form-check">
                                                <input
                                                    class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                    type="radio" name="confirmation_radio_button"
                                                    x-model="confirmation_radio_button" id="by_seller"
                                                    value="by_seller">
                                                <label
                                                    class="form-check-label inline-block text-sm font-medium text-gray-800"
                                                    for="by_seller"> Konfirmasi by Seller </label>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                @endif

                                {{-- Tag Lokasi --}}
                                @if(isset($add_listing['tipe_tur']))
                                <div class="block px-5">
                                    @if($add_listing['tipe_tur']=='Open Trip')
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi
                                        1<span class="text-red-500 font-bold">*</span></label>
                                    <div class="mt-1 rounded-md shadow-sm">
                                        <select name="tag_location_1" id="provinsi_open_1"
                                            class="provinsi_open_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option selected disabled>Pilih Lokasi</option>
                                            @foreach ($island as $pulau)
                                            <option value="{{$pulau->id}}">{{$pulau->island_name}}</option>
                                            @endforeach                                            
                                            {{-- @foreach ($provinsi as $provinsi_data)
                                            <option value="{{ $provinsi_data['provinces_id'] }}"
                                                {{isset($add_listing['tag_location_1']) ?
                                                ($add_listing['tag_location_1']==$provinsi_data['provinces_id']
                                                ? 'selected' :'') :''}}>{{ $provinsi_data['provinces_name'] }}</option>
                                            @foreach ($provinsi_data['regency'] as $key => $regency)
                                            <option value="{{ $regency['regencies_id'] }}"
                                                {{isset($add_listing['tag_location_1']) ?
                                                ($add_listing['tag_location_1']==$regency['regencies_id'] ? 'selected'
                                                :'') :''}}>{{ $regency['regencies_name'] }}</option>
                                            @endforeach
                                            @foreach ($provinsi_data['district'] as $key => $district)
                                            <option value="{{ $district['districts_id'] }}"
                                                {{isset($add_listing['tag_location_1']) ?
                                                ($add_listing['tag_location_1']==$district['districts_id'] ? 'selected'
                                                :'') :''}}>{{ $district['districts_name'] }}</option>
                                            @endforeach
                                            @endforeach --}}

                                        </select>
                                    </div>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>

                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi
                                        2</label>
                                    <div class="mt-1 rounded-md shadow-sm">
                                        <select name="tag_location_2" id="provinsi_open_2"
                                            class="provinsi_open_2 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option selected disabled>Pilih Lokasi</option>
                                            @foreach ($province as $provinsi_data)
                                            <option value="{{$provinsi_data->id}}">{{$provinsi_data->name}}</option>
                                            @endforeach
                                            {{-- @foreach ($provinsi as $provinsi_data)
                                            <option value="{{ $provinsi_data['provinces_id'] }}"
                                                {{isset($add_listing['tag_location_2']) ?
                                                ($add_listing['tag_location_2']==$provinsi_data['provinces_id']? 'selected'
                                                :'') :''}}>{{ $provinsi_data['provinces_name'] }}</option>
                                            @foreach ($provinsi_data['regency'] as $key => $regency)
                                            <option value="{{ $regency['regencies_id'] }}"
                                                {{isset($add_listing['tag_location_2']) ?
                                                ($add_listing['tag_location_2']==$regency['regencies_id'] ? 'selected'
                                                :'') :''}}>{{ $regency['regencies_name'] }}</option>
                                            @endforeach
                                            @foreach ($provinsi_data['district'] as $key => $district)
                                            <option value="{{ $district['districts_id'] }}"
                                                {{isset($add_listing['tag_location_2']) ?
                                                ($add_listing['tag_location_2']==$district['districts_id'] ? 'selected'
                                                :'') :''}}>{{ $district['districts_name'] }}</option>
                                            @endforeach
                                            @endforeach --}}

                                        </select>
                                    </div>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>

                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 3</label>
                                    <div class="mt-1 rounded-md shadow-sm">
                                        <select name="tag_location_3" id="regency_open_1"
                                            class="regency_open_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option selected disabled>Pilih Lokasi</option>
                                            @foreach ($kota as $kota_data)
                                            <option value="{{$kota_data->id}}" {{isset($add_listing['tag_location_3']) ? ($add_listing['tag_location_3']==$kota_data->id ? 'selected' : '') : ''}}>{{ $provinsi_data['provinces_name']}}>{{$kota_data->name}}</option>
                                            @endforeach
                                            {{-- @foreach ($provinsi as $provinsi_data)
                                            <option value="{{ $provinsi_data['provinces_id'] }}"
                                                {{isset($add_listing['tag_location_3']) ?
                                                ($add_listing['tag_location_3']==$provinsi_data['provinces_id']
                                                ? 'selected' :'') :''}}>{{ $provinsi_data['provinces_name'] }}</option>
                                            @foreach ($provinsi_data['regency'] as $key => $regency)
                                            <option value="{{ $regency['regencies_id'] }}"
                                                {{isset($add_listing['tag_location_3']) ?
                                                ($add_listing['tag_location_3']==$regency['regencies_id'] ? 'selected'
                                                :'') :''}}>{{ $regency['regencies_name'] }}</option>
                                            @endforeach
                                            @foreach ($provinsi_data['district'] as $key => $district)
                                            <option value="{{ $district['districts_id'] }}"
                                                {{isset($add_listing['tag_location_3']) ?
                                                ($add_listing['tag_location_3']==$district['districts_id'] ? 'selected'
                                                :'') :''}}>{{ $district['districts_name'] }}</option>
                                            @endforeach
                                            @endforeach --}}

                                        </select>
                                    </div>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>

                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 4 xx</label>
                                    <div class="mt-1 rounded-md shadow-sm">
                                        <select name="tag_location_4" id="regency_open_2"
                                            class="regency_open_2 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option selected disabled>Pilih Lokasi</option>
                                            {{-- @foreach ($provinsi as $provinsi_data)
                                            <option value="{{ $provinsi_data['provinces_id'] }}"
                                                {{isset($add_listing['tag_location_4']) ?
                                                ($add_listing['tag_location_4']==$provinsi_data['provinces_id']
                                                ? 'selected' :'') :''}}>{{ $provinsi_data['provinces_name'] }}</option>
                                            @foreach ($provinsi_data['regency'] as $key => $regency)
                                            <option value="{{ $regency['regencies_id'] }}"
                                                {{isset($add_listing['tag_location_4']) ?
                                                ($add_listing['tag_location_4']==$regency['regencies_id'] ? 'selected'
                                                :'') :''}}>{{ $regency['regencies_name'] }}</option>
                                            @endforeach
                                            @foreach ($provinsi_data['district'] as $key => $district)
                                            <option value="{{ $district['districts_id'] }}"
                                                {{isset($add_listing['tag_location_4']) ?
                                                ($add_listing['tag_location_4']==$district['districts_id'] ? 'selected'
                                                :'') :''}}>{{ $district['districts_name'] }}</option>
                                            @endforeach
                                            @endforeach --}}

                                        </select>
                                    </div>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>
                                    <div class="mb-6 mt-2" x-data="tagAdd()">
                                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis<span
                                                    class="text-red-500 font-bold">*</span></label>
                                        @if(isset($add_listing['new_tag_location']))
                                        <div class="mb-2" data-id="1" x-data="tagSelectAlpine()" x-init="$nextTick(() => { select2Alpine() } )">
                                            {{-- <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis<span class="text-red-500 font-bold">*</span></label> --}}
                                            @foreach ($add_listing['new_tag_location']['results'] as $key => $new_tag)
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select name="new_tag_location[]" id="new_tag_location_{{$key}}" x-ref="select_dynamic" class="new_tag_location mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                                        <option selected disabled>Pilih Lokasi</option>
                                                        @foreach ($dynamic_tag as $key => $tag)
                                                            @foreach ($tag as $pulau)
                                                            <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}" {{$new_tag == $pulau['id_pulau'] ? 'selected':''}}>{{ $pulau['nama_pulau']}}</option>
                                                                @foreach ($pulau['provinsi'] as $provinsi)
                                                                    <option value="{{ $provinsi['id_provinsi']}}" {{$new_tag == $provinsi['id_provinsi'] ? 'selected':''}}>{{ $provinsi['nama_provinsi']}}</option>
                                                                    @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                        <option value="{{ $kabupaten['id_kabupaten']}}" {{$new_tag == $kabupaten['id_kabupaten'] ? 'selected':''}}>{{ $kabupaten['nama_kabupaten']}}</option>
                                                                        @if(isset($kabupaten['wisata']))
                                                                        @foreach ($kabupaten['wisata'] as $wisata)
                                                                            <option value="{{ $wisata['id_wisata']}}" {{$new_tag == $wisata['id_wisata']? 'selected':''}}>{{ $wisata['nama_wisata']}}</option>
                                                                        @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach
                                                            @endforeach
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900"></div>
                                            @endforeach
                                        </div>
                                        @else
                                        <template x-if="tag_array.length == 0">
                                            {{-- Tag lokasi dinamis pertama --}}
                                            <div class="mb-2" data-id="1" x-data="tagSelectAlpine()" x-init="$nextTick(() => { select2Alpine() } )">
                                                {{-- <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis<span
                                                    class="text-red-500 font-bold">*</span></label> --}}
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select name="new_tag_location[]" id="new_tag_location_1"
                                                        x-ref="select_dynamic"
                                                        class="new_tag_location mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                                        <option selected disabled>Pilih Lokasi</option>
                                                        @foreach ($dynamic_tag as $key => $tag)
                                                            @foreach ($tag as $pulau)
                                                            <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}">{{ $pulau['nama_pulau']}}</option>
                                                                @foreach ($pulau['provinsi'] as $provinsi)
                                                                    <option value="{{ $provinsi['id_provinsi']}}">{{ $provinsi['nama_provinsi']}}</option>
                                                                    @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                        <option value="{{ $kabupaten['id_kabupaten']}}">{{ $kabupaten['nama_kabupaten']}}</option>
                                                                        @if(isset($kabupaten['wisata']))
                                                                        @foreach ($kabupaten['wisata'] as $wisata)
                                                                            <option value="{{ $wisata['id_wisata']}}">{{ $wisata['nama_wisata']}}</option>
                                                                        @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach
                                                            @endforeach
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div
                                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                                </div>
                                            </div>
                                        </template>
                                        @endif
                                        <template x-if="tag_array.length >= 1">
                                            <template x-for="(tag, index) in tag_array" :key="index">
                                                <div class="mb-2">
                                                    {{-- <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis<span
                                                        class="text-red-500 font-bold">*</span></label> --}}
                                                    <div class="flex mt-2 rounded-md shadow-sm">
                                                        <select name="new_tag_location[]" id="new_tag_location"
                                                            class="new_tag_location mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                                            <option selected disabled>Pilih Lokasi</option>
                                                            @foreach ($dynamic_tag as $key => $tag)
                                                                @foreach ($tag as $pulau)
                                                                <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}">{{ $pulau['nama_pulau']}}</option>
                                                                    @foreach ($pulau['provinsi'] as $provinsi)
                                                                        <option value="{{ $provinsi['id_provinsi']}}">{{ $provinsi['nama_provinsi']}}</option>
                                                                        @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                            <option value="{{ $kabupaten['id_kabupaten']}}">{{ $kabupaten['nama_kabupaten']}}</option>
                                                                            @if(isset($kabupaten['wisata']))
                                                                            @foreach ($kabupaten['wisata'] as $wisata)
                                                                                <option value="{{ $wisata['id_wisata']}}">{{ $wisata['nama_wisata']}}</option>
                                                                            @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    @endforeach
                                                                @endforeach
                                                            @endforeach
                                                        </select>
                                                        <div class="absolute right-[650px]">
                                                            <button type="button" class="rounded-full">
                                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt="" width="25px" @click="removeField(index)">
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div
                                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                                    </div>
                                                </div>
                                            </template>
                                        </template>
                                        <div class="absolute" style="top:752;">
                                            <button type="button" @click="add()" class="bg-kamtuu-primary rounded-md text-white text-sm px-3 py-2">Tambah Tag</button>
                                        </div>
                                    </div>
                                    @else
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi
                                        1<span class="text-red-500 font-bold">*</span></label>
                                    <div class="mt-1 rounded-md shadow-sm">
                                        <select name="tag_location_1" id="provinsi_open_1"
                                            class="provinsi_open_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option selected disabled>Pilih Lokasi</option>
                                            {{-- @foreach ($provinsi as $provinsi_data)
                                            <option value="{{ $provinsi_data['provinces_id'] }}">{{
                                                $provinsi_data['provinces_name'] }}</option>
                                            @foreach ($provinsi_data['regency'] as $key => $regency)
                                            <option value="{{ $regency['regencies_id'] }}">{{ $regency['regencies_name']
                                                }}
                                            </option>
                                            @endforeach
                                            @foreach ($provinsi_data['district'] as $key => $district)
                                            <option value="{{ $district['districts_id'] }}">{{
                                                $district['districts_name']
                                                }}</option>
                                            @endforeach
                                            @endforeach --}}

                                        </select>
                                    </div>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>

                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi
                                        2</label>
                                    <div class="mt-1 rounded-md shadow-sm">
                                        <select name="tag_location_2" id="provinsi_open_2"
                                            class="provinsi_open_2 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option selected disabled>Pilih Lokasi</option>
                                            {{-- @foreach ($provinsi as $provinsi_data)
                                            <option value="{{ $provinsi_data['provinces_id'] }}">{{
                                                $provinsi_data['provinces_name'] }}</option>
                                            @foreach ($provinsi_data['regency'] as $key => $regency)
                                            <option value="{{ $regency['regencies_id'] }}">{{ $regency['regencies_name']
                                                }}
                                            </option>
                                            @endforeach
                                            @foreach ($provinsi_data['district'] as $key => $district)
                                            <option value="{{ $district['districts_id'] }}">{{
                                                $district['districts_name']
                                                }}</option>
                                            @endforeach
                                            @endforeach --}}

                                        </select>
                                    </div>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>

                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi
                                        3</label>
                                    <div class="mt-1 rounded-md shadow-sm">
                                        <select name="tag_location_3" id="regency_open_1"
                                            class="regency_open_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option selected disabled>Pilih Lokasi</option>
                                            {{-- @foreach ($provinsi as $provinsi_data)
                                            <option value="{{ $provinsi_data['provinces_id'] }}">{{
                                                $provinsi_data['provinces_name'] }}</option>
                                            @foreach ($provinsi_data['regency'] as $key => $regency)
                                            <option value="{{ $regency['regencies_id'] }}">{{ $regency['regencies_name']
                                                }}
                                            </option>
                                            @endforeach
                                            @foreach ($provinsi_data['district'] as $key => $district)
                                            <option value="{{ $district['districts_id'] }}">{{
                                                $district['districts_name']
                                                }}</option>
                                            @endforeach
                                            @endforeach --}}

                                        </select>
                                    </div>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>

                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi
                                        4</label>
                                    <div class="mt-1 rounded-md shadow-sm">
                                        <select name="tag_location_4" id="regency_open_2"
                                            class="regency_open_2 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option selected disabled>Pilih Lokasi</option>
                                        </select>
                                    </div>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>
                                    @endif
                                </div>
                                @else
                                <div class="block px-5">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 1<span class="text-red-500 font-bold">*</span></label>
                                    <div class="mt-1 rounded-md shadow-sm">
                                        <select name="tag_location_1" id="provinsi_open_1"
                                            class="provinsi_private_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option selected disabled>Pilih Lokasi</option>
                                            @foreach ($island as $pulau)
                                            <option value="{{$pulau->id}}" {{isset($add_listing['tag_location_1']) ? ($add_listing['tag_location_1']==$pulau->id ? 'selected':null) : null}}>{{$pulau->island_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>

                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 2</label>
                                    <div class="mt-1 rounded-md shadow-sm">
                                        <select name="tag_location_2" id="provinsi_open_2"
                                            class="provinsi_private_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option selected disabled>Pilih Lokasi</option>
                                            @foreach ($province as $provinsi_data)
                                            <option value="{{$provinsi_data->id}}" {{isset($add_listing['tag_location_2']) ? ($add_listing['tag_location_2'] == $provinsi_data->id ? 'selected':'') :''}}>{{$provinsi_data->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>

                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 3</label>
                                    <div class="mt-1 rounded-md shadow-sm">
                                        <select name="tag_location_3" id="regency_open_1"
                                            class="provinsi_private_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option selected disabled>Pilih Lokasi</option>
                                            @foreach ($kota as $kota_data)
                                            <option value="{{$kota_data->id}}" {{isset($add_listing['tag_location_3']) ? ($add_listing['tag_location_3']==$kota_data->id ? 'selected' : '') : ''}}>{{$kota_data->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>

                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi 4</label>
                                    <div class="mt-1 rounded-md shadow-sm">
                                        <select name="tag_location_4" id="regency_open_2"
                                            class="provinsi_private_1 mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            <option selected disabled>Pilih Lokasi</option>
                                            @foreach ($objek as $obj)
                                            <option value="{{$obj->id}}" {{isset($add_listing['tag_location_4']) ? ($add_listing['tag_location_4'] == $obj->id ? 'selected' : '') : ''}}>{{$obj->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                    </div>
                                    <div class="mb-6 mt-2" x-data="tagAdd()">
                                        <template x-if="tag_array.length == 0">
                                            <div class="mb-2">
                                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis
                                                    <span class="text-red-500 font-bold">*</span>
                                                </label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select name="new_tag_location[]" id="new_tag_location"
                                                        class="new_tag_location mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                                        <option selected disabled>Pilih Lokasi</option>
                                                        @foreach ($dynamic_tag as $key => $tag)
                                                            @foreach ($tag as $pulau)
                                                            <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}">{{ $pulau['nama_pulau']}}</option>
                                                                @foreach ($pulau['provinsi'] as $provinsi)
                                                                    <option value="{{ $provinsi['id_provinsi']}}">{{ $provinsi['nama_provinsi']}}</option>
                                                                    @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                        <option value="{{ $kabupaten['id_kabupaten']}}">{{ $kabupaten['nama_kabupaten']}}</option>
                                                                        @if(isset($kabupaten['wisata']))
                                                                        @foreach ($kabupaten['wisata'] as $wisata)
                                                                            <option value="{{ $wisata['id_wisata']}}">{{ $wisata['nama_wisata']}}</option>
                                                                        @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach
                                                            @endforeach
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div
                                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                                </div>
                                            </div>
                                        </template>
                                        <template x-if="tag_array.length >= 1">
                                            <template x-for="(tag, index) in tag_array" :key="index">
                                                <div class="mb-2">
                                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag Lokasi Dinamis<span
                                                        class="text-red-500 font-bold">*</span></label>
                                                    <div class="mt-2 rounded-md shadow-sm">
                                                        <select name="new_tag_location[]" id="new_tag_location"
                                                            class="new_tag_location mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                                            <option selected disabled>Pilih Lokasi</option>
                                                            @foreach ($dynamic_tag as $key => $tag)
                                                                @foreach ($tag as $pulau)
                                                                <option class="uppercase text-blue-400 bg-blue-200" value="{{ $pulau['id_pulau']}}">{{ $pulau['nama_pulau']}}</option>
                                                                    @foreach ($pulau['provinsi'] as $provinsi)
                                                                        <option value="{{ $provinsi['id_provinsi']}}">{{ $provinsi['nama_provinsi']}}</option>
                                                                        @foreach ($provinsi['kabupaten'] as $kabupaten)
                                                                            <option value="{{ $kabupaten['id_kabupaten']}}">{{ $kabupaten['nama_kabupaten']}}</option>
                                                                            @if(isset($kabupaten['wisata']))
                                                                            @foreach ($kabupaten['wisata'] as $wisata)
                                                                                <option value="{{ $wisata['id_wisata']}}">{{ $wisata['nama_wisata']}}</option>
                                                                            @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    @endforeach
                                                                @endforeach
                                                            @endforeach
                                                        </select>
                                                        <div class="absolute right-[650px]">
                                                            <button type="button" class="rounded-full">
                                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt="" width="25px" @click="removeField(index)">
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div
                                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                                    </div>
                                                </div>
                                            </template>
                                        </template>
                                        <div class="absolute" style="top:752;">
                                            <button type="button" @click="add()" class="bg-kamtuu-primary rounded-md text-white text-sm px-3 py-2">Tambah Tag</button>
                                        </div>
                                    </div>
                                </div>
                                @endif

                                <div class="grid grid-cols-2">
                                    <div class="flex">
                                        <div class="px-5 my-5">
                                            <label for=""
                                                class="block mb-2 text-sm font-bold text-[#333333]">Hari</label>
                                            <input type="text" id="jml_hari_open" name="jml_hari"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="Hari">
                                        </div>

                                        <div class="pr-5 my-5">
                                            <label for=""
                                                class="block mb-2 text-sm font-bold text-[#333333]">Malam</label>
                                            <input type="text" id="jml_malam_open" name="jml_malam"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="Malam">
                                        </div>
                                    </div>
                                </div>

                                <fieldset class="px-5 py-3 flex">
                                    <div>
                                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tipe
                                            Tur</label>
                                        <input type="text" id="status" name="status" value="Open Trip"
                                            class="bg-[#F2F2F2] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 placeholder-[#333333]"
                                            placeholder="Open Trip" readonly>
                                    </div>
                                    <div class="px-5">
                                        <label for="durasi" class="block mb-2 text-sm font-bold text-[#333333]">Durasi
                                            (Jam)</label>
                                        <input type="text" id="durasi" name="durasi"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="5">
                                    </div>
                                </fieldset>

                                {{-- Paket --}}
                                <div class="p-5 space-y-3">
                                    <p class="text-md text-[#333333] font-bold">Paket</p>
                                    <template x-if="fields.length >= 1" class="col" :data-length="fields.length"
                                        id="paketTur">
                                        <template x-for="(field, index) in fields" :key="index">
                                            {{-- <input type="hidden" :value="fields.length" id="paketTur"> --}}
                                            <div>
                                                <div class="grid grid-cols-2">
                                                    <div class="flex space-x-5 gap-4">
                                                        <div class="">
                                                            <label for="nama_paket"
                                                                class="block mb-2 text-sm font-bold text-[#828282]">Judul
                                                                Paket</label>
                                                            <input type="text"
                                                                :name="'fields[' + index + '][nama_paket]'"
                                                                :id="'['+index+']nama_paket'" x-model="field.nama_paket"
                                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                placeholder="Tur Sehat Pagi">
                                                            {{-- <input type="hidden" name="fields[][nama_paket]"
                                                                x-model='field.nama_paket'> --}}
                                                            {{-- :name="'fields'+index+'nama_paket'" --}}
                                                        </div>
                                                        <div class="mb-6">
                                                            <label for="jam_paket"
                                                                class="block mb-2 text-sm font-bold text-[#828282]">Jam
                                                                Paket</label>
                                                            <div
                                                                class="bg-[#FFFFFF] border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2 dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                                                <input type="text" :id="id"
                                                                    x-data="{ id: $id('time-picker') }"
                                                                    x-model="field.jam_paket"
                                                                    x-init="initTimePickerHandler(id, field.jam_paket)"
                                                                    {{-- :class="index" --}}
                                                                    class="jam_paket bg-[#FFFFFF] border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                    :name="'fields[' + index + '][jam_paket]'" readonly>
                                                                {{-- <input type="hidden" name="fields[0][jam_paket]"
                                                                    x-model='field.jam_paket'> --}}
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid grid-cols-2">
                                                    <div class="flex space-x-5 gap-4">
                                                        <div class="">
                                                            <label for="min_peserta"
                                                                class="block mb-2 text-sm font-bold text-[#828282]">Peserta
                                                                Min</label>
                                                            <input type="number" min="0" :id="'['+index+']min_peserta'"
                                                                :name="'fields[' + index + '][min_peserta]'"
                                                                x-model=" field.min_peserta"
                                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                placeholder="3">
                                                            {{-- <input type="hidden" name="fields[]"
                                                                x-model='field.min_peserta'> --}}
                                                        </div>

                                                        <div class="">
                                                            <label for="max_peserta"
                                                                class="block mb-2 text-sm font-bold text-[#828282]">Peserta
                                                                Max</label>
                                                            <input type="number" min="0" :id="'['+index+']max_peserta'"
                                                                :name="'fields[' + index + '][max_peserta]'"
                                                                x-model=" field.max_peserta"
                                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                placeholder="5">
                                                            {{-- <input type="hidden" name="fields[]"
                                                                x-model='field.max_peserta'> --}}
                                                        </div>

                                                        <template x-if="fields.length === 1" class="col">
                                                            <div class="flex items-end">
                                                                <button @click="addNewField()" type="button"
                                                                    class="bg-[#23AEC1] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#21bbd0]">
                                                                    Tambah </button>
                                                            </div>
                                                        </template>

                                                        <template x-if="fields.length > 1 && fields.length-1 !== index"
                                                            class="col">
                                                            <div class="flex space-x-5 gap-4">
                                                                <div class="flex items-end">
                                                                    <button @click="removeField(index)" type="button"
                                                                        class="bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]">
                                                                        Hapus </button>
                                                                </div>
                                                        </template>

                                                        <template x-if="fields.length > 1 && fields.length-1 === index"
                                                            class="col">
                                                            <div class="flex space-x-5 gap-4">
                                                                <div class="flex items-end">
                                                                    <button @click="removeField(index)" type="button"
                                                                        class="bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]">
                                                                        Hapus </button>
                                                                </div>

                                                                <div class="flex items-end">
                                                                    <button @click="addNewField()" type="button"
                                                                        class="bg-[#23AEC1] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#21bbd0]">
                                                                        Tambah </button>
                                                                </div>
                                                            </div>
                                                        </template>
                                                    </div>
                                                </div>
                                            </div>
                                        </template>
                                    </template>
                                </div>

                                {{--
                                <div class="grid grid-cols-2">
                                    <div class="flex">
                                        <div class="px-5 my-5">
                                            <label for="text"
                                                class="block mb-2 text-sm font-bold text-[#333333]">Peserta
                                                Min</label>
                                            <input type="text" id="min_orang" name="min_orang"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="3">
                                        </div>

                                        <div class="pr-5 my-5">
                                            <label for="text"
                                                class="block mb-2 text-sm font-bold text-[#333333]">Peserta
                                                Max</label>
                                            <input type="text" id="max_orang" name="max_orang"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="5">
                                        </div>
                                    </div>
                                </div>
                                --}}

                                @if(isset($add_listing['tipe_tur']))
                                <fieldset class="px-5">
                                    @if($add_listing['tipe_tur']=='Open Trip')
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333]">Makanan</label>
                                    <div class="grid grid-cols-3 w-[600px]">
                                        <div class="flex items-center mb-4 w-40">

                                            <input id="makanan" type="radio" name="makanan" value="Halal"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                {{isset($add_listing['makanan']) ? ($add_listing['makanan']=='Halal'
                                                ? 'checked' :'') :'checked'}}>
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Halal
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="makanan" type="radio" name="makanan" value="Vegetarian Available"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                {{isset($add_listing['makanan']) ?
                                                ($add_listing['makanan']=='Vegetarian Available' ? 'checked' :'') :''}}>
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Vegetarian Available
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="makanan" type="radio" name="makanan"
                                                value="Halal & Vegetarian Available"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300  dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                {{isset($add_listing['makanan']) ?
                                                ($add_listing['makanan']=='Halal & Vegetarian Available' ? 'checked'
                                                :'') :''}}>
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Halal & Vegetarian Available
                                            </label>
                                        </div>
                                    </div>
                                    @else
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333]">Makanan</label>
                                    <div class="grid grid-cols-3 w-[600px]">
                                        <div class="flex items-center mb-4 w-40">
                                            <input id="makanan" type="radio" name="makanan" value="Halal"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                checked>
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Halal
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="makanan" type="radio" name="makanan" value="Vegetarian Available"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Vegetarian Available
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="makanan" type="radio" name="makanan"
                                                value="Halal & Vegetarian Available"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300  dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Halal & Vegetarian Available
                                            </label>
                                        </div>
                                    </div>
                                    @endif
                                </fieldset>

                                @else
                                <fieldset class="px-5">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333]">Makanan</label>
                                    <div class="grid grid-cols-3 w-[600px]">
                                        <div class="flex items-center mb-4 w-40">
                                            <input id="makanan" type="radio" name="makanan" value="Halal"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                checked>
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Halal
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="makanan" type="radio" name="makanan" value="Vegetarian Available"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Vegetarian Available
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="makanan" type="radio" name="makanan"
                                                value="Halal & Vegetarian Available"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300  dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Halal & Vegetarian Available
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>
                                @endif

                                @if(isset($add_listing['tipe_tur']))
                                <fieldset class="px-5">
                                    @if($add_listing['tipe_tur']=='Open Trip')
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Bahasa</label>
                                    <div class="grid grid-cols-3 w-[600px]">
                                        <div class="flex items-center mb-4 w-40">
                                            <input id="bahasa" type="radio" name="bahasa" value="Bahasa Indonesia"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                {{isset($add_listing['bahasa']) ?
                                                ($add_listing['bahasa']=='Bahasa Indonesia' ? 'checked' :'')
                                                :'checked'}}>
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Bahasa Indonesia
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="bahasa" type="radio" name="bahasa" value="English English"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                {{isset($add_listing['bahasa']) ?
                                                ($add_listing['bahasa']=='English English' ? 'checked' :'')
                                                :'checked'}}>

                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                English
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="bahasa" type="radio" name="bahasa" value="Indonesia & English"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                {{isset($add_listing['bahasa']) ?
                                                ($add_listing['bahasa']=='Indonesia & English' ? 'checked' :'')
                                                :'checked'}}>

                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Indonesia & English
                                            </label>
                                        </div>
                                    </div>
                                    @else
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Bahasa</label>
                                    <div class="grid grid-cols-3 w-[600px]">
                                        <div class="flex items-center mb-4 w-40">
                                            <input id="bahasa" type="radio" name="bahasa" value="Bahasa Indonesia"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                checked="">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Bahasa Indonesia
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="bahasa" type="radio" name="bahasa" value="English English"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                English
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="bahasa" type="radio" name="bahasa" value="Indonesia & English"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Indonesia & English
                                            </label>
                                        </div>
                                    </div>
                                    @endif
                                </fieldset>
                                @else
                                <fieldset class="px-5">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Bahasa</label>
                                    <div class="grid grid-cols-3 w-[600px]">
                                        <div class="flex items-center mb-4 w-40">
                                            <input id="bahasa" type="radio" name="bahasa" value="Bahasa Indonesia"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                checked="">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Bahasa Indonesia
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="bahasa" type="radio" name="bahasa" value="English English"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                English
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="bahasa" type="radio" name="bahasa" value="Indonesia & English"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Indonesia & English
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>
                                @endif

                                @if(isset($add_listing['tipe_tur']))
                                <fieldset class="px-5">
                                    @if($add_listing['tipe_tur']=='Open Trip')
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tingkat
                                        Petualangan</label>
                                    <div class="grid grid-cols-3 w-[600px]">
                                        <div class="flex items-center mb-4 w-40">
                                            <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                                value="Ekstrim"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                {{isset($add_listing['tingkat_petualangan']) ?
                                                ($add_listing['tingkat_petualangan']=='Ekstrim' ? 'checked' :'')
                                                :'checked'}}>
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Ekstrim
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                                value="Semi Ekstrim"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                {{isset($add_listing['tingkat_petualangan']) ?
                                                ($add_listing['tingkat_petualangan']=='Semi Ekstrim' ? 'checked' :'')
                                                :''}}>
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Semi Ekstrim
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                                value="Tidak Ekstrim"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                {{isset($add_listing['tingkat_petualangan']) ?
                                                ($add_listing['tingkat_petualangan']=='Tidak Ekstrim' ? 'checked' :'')
                                                :''}}>
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Tidak Ekstrim
                                            </label>
                                        </div>
                                    </div>
                                    @else
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tingkat
                                        Petualangan</label>
                                    <div class="grid grid-cols-3 w-[600px]">
                                        <div class="flex items-center mb-4 w-40">
                                            <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                                value="Ekstrim"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                checked="">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Ekstrim
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                                value="Semi Ekstrim English"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Semi Ekstrim
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                                value="Tidak Ekstrim"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Tidak Ekstrim
                                            </label>
                                        </div>
                                    </div>
                                    @endif
                                </fieldset>
                                @else
                                <fieldset class="px-5">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tingkat
                                        Petualangan</label>
                                    <div class="grid grid-cols-3 w-[600px]">
                                        <div class="flex items-center mb-4 w-40">
                                            <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                                value="Ekstrim"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                                checked="">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Ekstrim
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                                value="Semi Ekstrim English"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Semi Ekstrim
                                            </label>
                                        </div>
                                        <div class="flex items-center mb-4 w-52">
                                            <input id="tingkat_petualangan" type="radio" name="tingkat_petualangan"
                                                value="Tidak Ekstrim"
                                                class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                            <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                                Tidak Ekstrim
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>
                                @endif

                                <div class="px-5 mb-5">
                                    <label for="" class="block mb-2 text-sm font-bold text-[#333333]">Kategori
                                        Tur</label>
                                    {{-- <input type="text" id="nama_kategori" name="nama_kategori"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Masukan kategori tur yang sesuai dengan paket"> --}}
                                    <input data-role="tagsinput" type="text" name="nama_kategori"
                                        id="nama_kategori_open">
                                </div>

                                <div class="px-5" wire:ignore>
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333]">Deskripsi</label>
                                    <textarea class="form-control w-[10rem] deskripsi" name="deskripsi"
                                        id="deskripsi_open"></textarea>
                                </div>

                                <div class="px-5 pt-5" wire:ignore>
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Paket
                                        Termasuk</label>
                                    <textarea class="form-control w-[10rem] paket_termasuk" name="paket_termasuk"
                                        id="paket_termasuk_open"></textarea>
                                </div>

                                <div class="px-5 pt-5" wire:ignore>
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Paket
                                        Tidak Termasuk</label>
                                    <textarea class="form-control w-[10rem] paket_tidak_termasuk"
                                        name="paket_tidak_termasuk" id="paket_tidak_termasuk_open"></textarea>
                                </div>

                                <div class="px-5 pt-5" wire:ignore>
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333]">Catatan</label>
                                    <textarea class="form-control w-[10rem] catatan" name="catatan"
                                        id="catatan_open"></textarea>
                                </div>

                                <div class="p-5">
                                    <div class="grid grid-cols-6">
                                        <div class="col-end-8 col-end-2">
                                            <button type="submit"
                                                class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                                Selanjutnya
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('input[name="radio_itinerary"]').click(function() {
            var inputValue = $(this).attr("value");
            if (inputValue == "private") {
                $("#open").hide();
                $("#private").show();
            } else {
                $("#private").hide();
                $("#open").show();
            }
        });
    </script>
    <script>
        function tagSelectAlpine(){
            return{
                select2Alpine(){
                    
                   // alert('tets');
                    //console.log($(this.$refs.select_dynamic))
                    //$(this.$refs.select_dynamic).select2({
                    //    placeholder: "Pilih lokasi",
                    //    allowClear: true
                    //});
                }
            }
        }
        
        function tagAdd(){
            return{
                tag_array:[],
                add(){
                    //alert('test')
                    this.tag_array.push({
                        id:'',
                    })

                    console.log(this.tag_array)
                },
                removeField(index){
                    this.tag_array.splice(index, 1)
                }
            }
        }
    </script>
    <script>
        $('.deskripsi').summernote({
            placeholder: 'Deskripsi...',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['para', ['ul', 'ol']]
            ]
        });

        $('.paket_termasuk').summernote({
            placeholder: 'Paket Termasuk...',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });

        $('.paket_tidak_termasuk').summernote({
            placeholder: 'Paket Tidak Termasuk...',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });

        $('.catatan').summernote({
            placeholder: 'Catatan...',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });

        function paketHandler() {

            return {
                product_name: "{!! isset($add_listing['product_name']) ? $add_listing['product_name'] : '' !!}",
                confirmation_radio_button: "{!! isset($add_listing['confirmation']) ? $add_listing['confirmation'] : '' !!}",
                provinsi: {!! isset($add_listing['province_id']) ? $add_listing['province_id'] : 'null' !!},
                kabupaten: {!! isset($add_listing['district_id']) ? $add_listing['district_id'] : 'null' !!},
                kecamatan: {!! isset($add_listing['regemcy_id']) ? $add_listing['regemcy_id'] : 'null' !!},
                kelurahan: {!! isset($add_listing['villahe_id']) ? $add_listing['kelurahan_id'] : 'null' !!},
                fields: {!! isset($add_listing['paket']) ? $add_listing['paket'] : '[1]' !!},
                ringkasan: "{!! isset($add_listing['ringkasan']) ? $add_listing['ringkasan'] : '' !!}",
                deskripsi_wisata: "{!! isset($add_listing['deskripsi_wisata']) ? $add_listing['deskripsi_wisata'] : '' !!}",
                paket_termasuk: "{!! isset($add_listing['paket_termasuk']) ? $add_listing['paket_termasuk'] : '' !!}",
                paket_tidak_termasuk: "{!! isset($add_listing['paket_tidak_termasuk']) ? $add_listing['paket_tidak_termasuk'] : '' !!}",
                catatan: "{!! isset($add_listing['catatan']) ? $add_listing['catatan'] : '' !!}",
                fields: {!! isset($add_listing['paket']) ? $add_listing['paket'] : '[]' !!},

                addNewField() {
                    this.fields.push({
                        nama_paket: '',
                        jam_paket: '',
                        min_peserta: 0,
                        max_peserta: 0
                    });
                    // console.log(this.fields)
                },
                removeField(index) {
                    console.log(index)
                    this.fields.splice(index, 1);
                    console.log(this.fields)
                },
                initTimePickerHandler(index, jam) {
                    Alpine.nextTick(() => {
                        $('#' + index).timepicker({
                            value: jam
                        })
                    })
                    console.log(index)
                    console.log(String(jam))
                    this.$watch('fields', () => {
                        $('#' + index).timepicker()
                        // this.field.jam_paket = $('#'+index).timepicker().value()
                        // console.log(this.fields)
                    })
                },

            }
        }
    </script>

    <script>
        // if($('.provinsi_private_1').val() ==null){
        //     // alert('Jancok')
        //     $(".provinsi_private").prop(",true);
        // }else{
        //     $(".provinsi_private").removeAttr(");
        // }

        // $('#myForm').validate({
        //     ignore: 'input[type=hidden], .select2-input, .select2-focusser'
        // });

        $('.provinsi_private_1').select2({
            allowClear: true
        });

        $('.provinsi_private_2').select2({
                // width: '100%',
                allowClear: true
        });

        $('.regency_private_1').select2({
                // width: '100%',
                allowClear: true
        });

        $('.regency_private_2').select2({
                // width: '100%',
                allowClear: true
        });
        console.log(document.getElementById("new_tag_location_1"))
        $("#new_tag_location_1").select2({
            allowClear: true
        })

        function sweetalert2(msg, e){
            Swal.fire({
                icon: 'error',
                text: msg,
                customClass: 'swal-height'
            })   

            e.preventDefault();
        }
        
        $("#formTurPrivate").on("submit",function(e){

            if(!$('#product_name_private').val()){
                sweetalert2('Nama Produk harus diisi!', e);
                // e.preventDefault(); 
            }

            else if($('input[name="confirmation_radio_button"]:checked').length==0){
                sweetalert2('Tombol konfirmasi harus dipilih!', e);
                // e.preventDefault();
            }
            
            else if($('.provinsi_private_1').val() == null){
                sweetalert2('Tag Lokasi 1 harap dipilih!', e);
                // e.preventDefault();
            }

            else if(!$('#jml_hari').val()){
                sweetalert2('Jumlah hari harap diisi!', e);
                // e.preventDefault();
            }

            else if(!$('#jml_malam').val()){
                sweetalert2('Jumlah malam harap diisi!', e);
                // e.preventDefault();
            }
            
            else if(!$('#min_orang_private').val()){
                sweetalert2('Jumlah minimal orang harap diisi!', e);
                // e.preventDefault();
            }

            else if(!$("#max_orang_private").val()){
                sweetalert2('Jumlah max orang harap diisi!', e);
                e.preventDefault();
            }
            
            else if(!$('#nama_kategori_private').val()){
                sweetalert2('Nama kategori harap diisi!', e);
                // e.preventDefault();
            }

            else if(!$('#deskripsi_private').val()){
                sweetalert2('Deskripsi harap diisi!', e);
                // e.preventDefault();
            }
            else if(!$('#paket_termasuk_private').val()){
                sweetalert2('Paket termasuk harap diisi!', e);
                // e.preventDefault();
            }
            
            else if(!$('#paket_tidak_termasuk_private').val()){
                sweetalert2('Paket tidak termasuk harap diisi!', e);
                // e.preventDefault();
            }
            // e.preventDefault();

        });

        /* $(function() {
            // $("#provinsi_private_1").on("change",function(){
            //     // console.log(document.getElementById('provinsi_private_1'))
            //     // alert('test');
            // })
            // $('#provinsi_private_1').select2({
            //     width: '100%',
            //     allowClear: true
            // });
            // $.ajaxSetup({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     }
            // })
            // $(function() {
            //     // provinsi
            //     $('#provinsi').on('change', function() {
            //         let id_provinsi = $('#provinsi').val();
            //         $('#kabupaten').val('');
            //         $('#kecamatan').val('');
            //         $('#kelurahan').val('');
            //         var option = $('<option></option>').attr("value", "").text("");
            //         $("#kecamatan").empty().append(option);
            //         $("#kelurahan").empty().append(option);

            //         $.ajax({
            //             type: 'POST',
            //             url: "{{ route('getkabupaten') }}",
            //             data: {
            //                 id_provinsi: id_provinsi
            //             },
            //             cache: false,

            //             success: function(msg) {
            //                 $('#kabupaten').html(msg);
            //             },
            //             error: function(data) {
            //                 console.log('error:', data);
            //             },
            //         })
            //     })

            //     // kabupaten
            //     $('#kabupaten').on('change', function() {
            //         let id_kabupaten = $('#kabupaten').val();
            //         var option = $('<option></option>').attr("value", "").text("");
            //         $("#kelurahan").empty().append(option);

            //         $.ajax({
            //             type: 'POST',
            //             url: "{{ route('getkecamatan') }}",
            //             data: {
            //                 id_kabupaten: id_kabupaten
            //             },
            //             cache: false,

            //             success: function(msg) {
            //                 $('#kecamatan').html(msg);
            //             },
            //             error: function(data) {
            //                 console.log('error:', data);
            //             },
            //         })
            //     })

            //     // kecamatan
            //     $('#kecamatan').on('change', function() {
            //         let id_kecamatan = $('#kecamatan').val();

            //         $.ajax({
            //             type: 'POST',
            //             url: "{{ route('getdesa') }}",
            //             data: {
            //                 id_kecamatan: id_kecamatan
            //             },
            //             cache: false,

            //             success: function(msg) {
            //                 $('#kelurahan').html(msg);
            //             },
            //             error: function(data) {
            //                 console.log('error:', data);
            //             },
            //         })
            //     })
            // })

        }); */
    </script>
    <script>
        // Open Trip
        $('.provinsi_open_1').select2({
                // width: '100%',
                allowClear: true
        });

        $('.provinsi_open_2').select2({
                // width: '100%',
                allowClear: true
        });

        $('.regency_open_1').select2({
                // width: '100%',
                allowClear: true
        });

        $('.regency_open_2').select2({
                // width: '100%',
                allowClear: true
        });

        $("#formTurOpen").on("submit",function(e){
            // console.log($('#product_name_open').val());
            
            if(!$('#product_name_open').val()){
                sweetalert2('Nama Produk harus diisi!', e);
                // e.preventDefault(); 
            }
            

            else if($('input[name="confirmation_radio_button"]:checked').length==0){
                sweetalert2('Tombol konfirmasi harus dipilih!', e);
                // e.preventDefault();
            }
            
            else if($('#provinsi_open_1').val() == null){
                sweetalert2('Tag Lokasi 1 harap dipilih!', e);
                // e.preventDefault();
            }

            // console.log($('#jml_hari_open').val());
            // e.preventDefault();

            else if(!$('#jml_hari_open').val()){
                alert('test')
                sweetalert2('Jumlah hari harap diisi!', e);
                // e.preventDefault();
            }

            else if(!$('#jml_malam_open').val()){
                alert('test')
                sweetalert2('Jumlah malam harap diisi!', e);
                // e.preventDefault();
            }

            else if(!$('#durasi').val()){
                sweetalert2('Lama durasi harap diisi!', e);
                // e.preventDefault();
            }

            var paket = $('#paketTur').attr("data-length");
            console.log(paket);
            if(paket > 0){
                let i = 0;
                for(i; i < paket; i++){
                    let id = i;
                    // let idtimepicker = id+1;

                    var nama_paket = `[${id}]nama_paket`;
                    var jam_paket  = `time-picker-${id+1}`;
                    var min_peserta = `[${id}]min_peserta`;
                    var max_peserta = `[${id}]max_peserta`;
                    
                    console.log(document.getElementById(jam_paket).value)
                    if(!document.getElementById(nama_paket).value){
                        sweetalert2(`Nama paket pada paket ${id+1} harap diisi!`, e)
                    }
                    else if(!document.getElementById(jam_paket).value){
                        sweetalert2(`Jam paket pada paket ${id+1} harap diisi!`, e)
                    }
                    else if(document.getElementById(min_peserta).value == 0){
                        sweetalert2(`Peserta minimal pada paket ${id+1} tidak boleh 0!`, e)
                    }
                    else if(document.getElementById(max_peserta).value == 0){
                        sweetalert2(`Peserta maksimal pada paket ${id+1} tidak boleh 0!`, e)
                    }
                    // console.log(document.getElementById(min_peserta).value)
                }
                
                if(!$('#nama_kategori_open').val()){
                    sweetalert2('Nama kategori harap diisi!', e);
                }
    
                else if(!$('#deskripsi_open').val()){
                    sweetalert2('Deskripsi harap diisi!', e);
                }
                else if(!$('#paket_termasuk_open').val()){
                    sweetalert2('Paket termasuk harap diisi!', e);
                }
                
                else if(!$('#paket_tidak_termasuk_open').val()){
                    sweetalert2('Paket tidak termasuk harap diisi!', e);
                }
            }else{
                if(!$('#nama_kategori_open').val()){
                    sweetalert2('Nama kategori harap diisi!', e);
                }
    
                else if(!$('#deskripsi_open').val()){
                    sweetalert2('Deskripsi harap diisi!', e);
                }
                else if(!$('#paket_termasuk_open').val()){
                    sweetalert2('Paket termasuk harap diisi!', e);
                }
                
                else if(!$('#paket_tidak_termasuk_open').val()){
                    sweetalert2('Paket tidak termasuk harap diisi!', e);
                }
            }

        })

        $(function() {
            $("#provinsi_private_1").on("change",function(){
                console.log(document.getElementById('provinsi_private_1'))
            })

            /* $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $(function() {
                // provinsi
                $('#provinsi_open').on('change', function() {
                    let id_provinsi = $('#provinsi_open').val();
                    $('#kabupaten_open').val('');
                    $('#kecamatan_open').val('');
                    $('#kelurahan_open').val('');
                    var option = $('<option></option>').attr("value", "").text("");
                    $("#kecamatan_open").empty().append(option);
                    $("#kelurahan_open").empty().append(option);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getkabupaten') }}",
                        data: {
                            id_provinsi: id_provinsi
                        },
                        cache: false,

                        success: function(msg) {
                            $('#kabupaten_open').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })

                // kabupaten
                $('#kabupaten_open').on('change', function() {
                    let id_kabupaten = $('#kabupaten_open').val();
                    var option = $('<option></option>').attr("value", "").text("");
                    $("#kelurahan_open").empty().append(option);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getkecamatan') }}",
                        data: {
                            id_kabupaten: id_kabupaten
                        },
                        cache: false,

                        success: function(msg) {
                            $('#kecamatan_open').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })

                // kecamatan
                $('#kecamatan_open').on('change', function() {
                    let id_kecamatan = $('#kecamatan_open').val();

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getdesa') }}",
                        data: {
                            id_kecamatan: id_kecamatan
                        },
                        cache: false,

                        success: function(msg) {
                            $('#kelurahan_open').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })
            }) */

            // $('#provinsi_open').select2({
            //     width: '100%',
            //     placeholder: "Select an Option",
            //     allowClear: true
            // });

        });

    </script>
    <script>
        var tipe = "{!! isset($add_listing['tipe_tur']) ? $add_listing['tipe_tur'] : '' !!}"
        
        var tag_location_1 = "{!! isset($add_listing['tag_location_1']) ? $add_listing['tag_location_1'] : '' !!}"
        var tag_location_2 = "{!! isset($add_listing['tag_location_2']) ? $add_listing['tag_location_2'] : '' !!}"
        var tag_location_3 = "{!! isset($add_listing['tag_location_3']) ? $add_listing['tag_location_3'] : '' !!}"
        var tag_location_4 = "{!! isset($add_listing['tag_location_4']) ? $add_listing['tag_location_4'] : '' !!}"
        
        var jml_hari = "{!! isset($add_listing['jml_hari']) ? $add_listing['jml_hari'] : '' !!}"
        var jml_malam = "{!! isset($add_listing['jml_malam']) ? $add_listing['jml_malam'] : '' !!}"
        
        var min_orang = "{!! isset($add_listing['min_orang']) ? $add_listing['min_orang'] : '' !!}"
        var max_orang = "{!! isset($add_listing['max_orang']) ? $add_listing['max_orang'] : '' !!}"


        var durasi = "{!! isset($add_listing['durasi']) ? $add_listing['durasi'] : '' !!}"
        var confirmation_radio_button = "{!! isset($add_listing['confirmation_radio_button']) ? $add_listing['confirmation_radio_button'] : '' !!}"
       
        var bahasa = "{!! isset($add_listing['bahasa']) ? $add_listing['bahasa'] : '' !!}"
        var makanan = "{!! isset($add_listing['makanan']) ? $add_listing['makanan'] : '' !!}"
        var tingkat_petualangan = "{!! isset($add_listing['tingkat_petualangan']) ? $add_listing['tingkat_petualangan'] : '' !!}"
        var nama_kategori = "{!! isset($add_listing['nama_kategori']) ? $add_listing['nama_kategori'] : '' !!}"
        
        var deskripsi = "{!! isset($add_listing['deskripsi']) ? $add_listing['deskripsi'] : '' !!}"
        var paket_termasuk = "{!! isset($add_listing['paket_termasuk']) ? $add_listing['paket_termasuk'] : '' !!}"
        var paket_tidak_termasuk = "{!! isset($add_listing['paket_tidak_termasuk']) ? $add_listing['paket_tidak_termasuk'] : '' !!}"
        var catatan = "{!! isset($add_listing['catatan']) ? $add_listing['catatan'] : '' !!}"
        
        // alert(tipe); 
        if(tipe){
            if(tipe=='Open Trip'){
                $("#radio_private").prop("checked",false);
                $("#radio_open").prop("checked",true);
                
                $("#private").hide();
                $("#open").show();
                
                $('#product_name_open').val("{!! isset($add_listing['product_name']) ? $add_listing['product_name']: '' !!}");
    
                $('#jml_hari_open').val(jml_hari)
                $('#jml_malam_open').val(jml_malam)
                $('#durasi').val(durasi)


                $('#nama_kategori_open').val(nama_kategori);

                $('#deskripsi_open').summernote('code',deskripsi);
                $('#paket_termasuk_open').summernote('code',paket_termasuk);
                $('#paket_tidak_termasuk_open').summernote('code',paket_tidak_termasuk);
                $('#catatan_open').summernote('code',catatan);
            }else{
                
                $("#radio_private").prop("checked",true);
                $("#radio_open").prop("checked",false);

                $("#private").show();
                $("#open").hide();

                $('#product_name_private').val("{!! isset($add_listing['product_name']) ? $add_listing['product_name']: '' !!}")

                $('#jml_hari').val(jml_hari)
                $('#jml_malam').val(jml_malam)
                
                $('#min_orang_private').val(min_orang)
                $("#max_orang_private").val(max_orang)

                $('#nama_kategori_private').val(nama_kategori)
                
                $('#deskripsi_private').summernote('code',deskripsi)
                $('#paket_termasuk_private').summernote('code',paket_termasuk)
                $('#paket_tidak_termasuk_private').summernote('code',paket_tidak_termasuk)
                $('#catatan_private').summernote('code',catatan);          
            }

            
        }
    </script>
    @livewireScripts
</body>

</html>