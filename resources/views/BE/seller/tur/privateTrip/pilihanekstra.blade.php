<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css" rel="stylesheet">


    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        .swal-height {
            height: 50vh;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    @php
    if($isedit){
    $batas['pilihan_ekstra'] = $pilihan_ekstra['pilihan_ekstra'];
    // dump($batas['pilihan_ekstra']);
    }
    else{
    if(!isset($batas['pilihan_ekstra'])){
    $batas['pilihan_ekstra']=null;
    }
    }
    @endphp
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
                {{-- Breadcumbs --}}
                <nav class="flex" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        <li class="inline-flex items-center">
                            @if($isedit)
                            <a href="{{route('tur.viewAddListingEdit',$id)}}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                            @else
                            <a href="{{route('tur')}}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                            @endif
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if($isedit)
                                <a href="{{ route('tur.viewItenenarydit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Buat
                                    Itinerary</a>
                                @else
                                <a href="{{route('tur.viewItenenary')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Buat
                                    Itinerary</a>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if($isedit)
                                <a href="{{route('tur.viewHargaEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Harga
                                    & Ketersediaan</a>
                                @else
                                <a href="{{route('tur.viewHarga')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Harga
                                    & Ketersediaan</a>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if($isedit)
                                <a href="{{route('tur.viewBatasEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Batas
                                    Pembayaran & Pembatalan</a>
                                @else
                                <a href="{{route('tur.viewBatas')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Batas
                                    Pembayaran & Pembatalan</a>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if($isedit)
                                <a href="{{route('tur.viewPetaEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Peta
                                    & Foto</a>
                                @else
                                <a href="{{ route('tur.viewPeta')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Peta
                                    & Foto</a>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                @if($isedit)
                                <a href="{{ route('tur.viewPilihanEkstraEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Pilihan
                                    & Ekstra</a>
                                @else
                                <a href="{{ route('tur.viewPilihanEkstra')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1]">Pilihan
                                    & Ekstra</a>
                                @endif
                            </div>
                        </li>
                    </ol>
                </nav>
                @if ($isedit)
                <form action=" {{ route('tur.addPilihanEkstra.edit',$id) }}" method="POST"
                    enctype="multipart/form-data">
                    @else
                    <form action=" {{ route('tur.addPilihanEkstra') }}" method="POST" enctype="multipart/form-data">
                        @endif
                        @csrf
                        @method('PUT')
                        {{-- Section --}}
                        <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded">
                            {{-- @dump($pilihan_ekstra) --}}
                            {{-- Pilihan --}}
                            <div x-data="handler()">
                                <div class="pb-5">
                                    <template x-if="fields.length < 1">
                                        <div class="">
                                            <div class="text-lg font-bold font-inter text-[#4F4F4F]">
                                                Pilihan
                                            </div>
                                            <button type="button"
                                                class="text-white bg-[#23AEC1] mt-3 py-1 px-3 rounded-md"
                                                @click="addNewField()">Tambah</button>
                                            <p>Klik tombol Tambah di atas untuk menambahkan.</p>
                                            @if(\Session::has('error'))
                                            <span id="msg"
                                                class="text-red-500 font-bold">*&nbsp;{{\Session::get('error')}}</span>
                                            @endif
                                        </div>
                                    </template>
                                    <template x-if="fields.length >= 1" class="col">
                                        <template x-for="(field, index) in fields" :key="index">
                                            <div class="pb-5">
                                                <div class="text-lg font-bold font-inter text-[#4F4F4F]">
                                                    Pilihan <span x-text="index+1"></span>
                                                </div>

                                                <div class="py-2">
                                                    <div class="mb-3 w-1/4">
                                                        {{-- <input type="hidden" name=""> --}}
                                                        <select
                                                            class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700  bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded-lg transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                                            aria-label="Default select example" x-model="field.name"
                                                            :name="'fields['+index+'][nama_pilihan]'"
                                                            :id="'['+index+']_pilihan'"
                                                            x-data="swictcher('['+index+']',field.name)"
                                                            onchange="thisChange(this)">
                                                            <option selected>Pilihan</option>
                                                            @foreach ($masterPilihan as $item)
                                                            <option value="{{$item->name}}">
                                                                <span class="uppercase">
                                                                    {{$item->name}}
                                                                </span>
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div :id="'['+index+']field_hotel'" class="hidden">
                                                        <div class="mb-3 w-1/4">
                                                            <input type="text" x-model="field.judul_pilihan"
                                                                :name="'fields['+index+'][nama_hotel]'"
                                                                :id="'['+index+'][nama_hotel]'"
                                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full px-3 py-1.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                placeholder="Nama Hotel">

                                                            {{-- <select name="hotel_id"
                                                                class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700  bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded-lg transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                                                aria-label="Default select example"
                                                                x-model="field.nama_hotel"
                                                                :name="'fields['+index+'][nama_hotel]'">
                                                                <option disabled>Pilih Nama Hotel</option>
                                                                @foreach ($hotels as $hotel)
                                                                <option value="{{$hotel->product_name}}">
                                                                    {{$hotel->product_name}}</option>
                                                                @endforeach
                                                            </select>
                                                            --}}
                                                        </div>
                                                        <div class="mb-3 w-1/4">
                                                            <select
                                                                class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700  bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded-lg transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                                                aria-label="Default select example"
                                                                id="deskripsi_pilihan"
                                                                :name="'fields['+index+'][deskripsi_pilihan]'"
                                                                x-model="field.deskripsi_pilihan">
                                                                <option disabled>Pilihan</option>
                                                                <option value="bintang_satu">Bintang 1</option>
                                                                <option value="bintang_dua">Bintang 2</option>
                                                                <option value="bintang_tiga">Bintang 3</option>
                                                                <option value="bintang_empat">Bintang 4</option>
                                                                <option value="bintang_lima">Bintang 5</option>
                                                            </select>
                                                        </div>
                                                        <div class="flex my-2">
                                                            <input type="text" id="text"
                                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-1/4 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                placeholder="Harga" :id="'['+index+'][harga_hotel]'"
                                                                :name="'fields['+index+'][harga_pilihan]'"
                                                                x-model="field.harga_pilihan">

                                                        </div>
                                                    </div>
                                                    <div :id="'['+index+']field_insurance'" class="hidden">
                                                        <div class="mb-3 w-1/4">
                                                            <input type="text"
                                                                :name="'fields['+index+'][nama_asuransi]'"
                                                                :id="'['+index+'][nama_asuransi]'"
                                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full px-3 py-1.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                placeholder="Nama Asuransi"
                                                                x-model="field.judul_pilihan">
                                                        </div>
                                                        <div class="flex my-2">
                                                            <input type="text" id="text"
                                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-1/4 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                placeholder="Harga" :id="'['+index+'][harga_asuransi]'"
                                                                :name="'fields['+index+'][harga_pilihan]'"
                                                                x-model="field.harga_pilihan">

                                                        </div>
                                                        {{-- <div class="mb-3 w-1/4">
                                                            <select
                                                                class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700  bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded-lg transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                                                aria-label="Default select example"
                                                                id="deskripsi_pilihan"
                                                                :name="'fields['+index+'][deskripsi_pilihan]'"
                                                                x-model="field.deskripsi_pilihan">
                                                                <option disabled>Pilihan</option>
                                                                <option value="bintang_satu">Bintang 1</option>
                                                                <option value="bintang_dua">Bintang 2</option>
                                                                <option value="bintang_tiga">Bintang 3</option>
                                                                <option value="bintang_empat">Bintang 4</option>
                                                                <option value="bintang_lima">Bintang 5</option>
                                                            </select>
                                                        </div> --}}
                                                        {{-- <div class="flex my-2">
                                                            <input type="text" id="text"
                                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-1/4 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                placeholder="Harga" id="harga_pilihan"
                                                                :name="'fields['+index+'][harga_pilihan]'"
                                                                x-model="field.harga_pilihan">

                                                        </div>
                                                        --}}
                                                    </div>
                                                    <div class="flex space-x-2">
                                                        <div class="form-check">
                                                            <input
                                                                class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                                type="radio"
                                                                :name="'fields['+index+'][kewajiban_pilihan]'"
                                                                :id="'pilihan_not_required'+index"
                                                                x-model="field.kewajiban_pilihan" value="tidak_wajib"
                                                                required>
                                                            <label class="form-check-label inline-block text-gray-800"
                                                                for="pilihan_not_required">
                                                                Tidak Wajib
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input
                                                                class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                                type="radio"
                                                                :name="'fields['+index+'][kewajiban_pilihan]'"
                                                                :id="'pilihan_required'+index"
                                                                x-model="field.isRequired" value="wajib">
                                                            <label class="form-check-label inline-block text-gray-800"
                                                                for="pilihan_required">
                                                                Wajib
                                                            </label>
                                                        </div>
                                                        <template x-if="index === (fields.length - 1)">
                                                            <div class="flex items-center">
                                                                <button class="mx-2"><img
                                                                        src="{{ asset('storage/icons/circle-plus-solid.svg') }}"
                                                                        alt="" width="25px" @click="addNewField()"
                                                                        style="color: #ed0202;">
                                                                </button>
                                                                <button class="mx-2"><img
                                                                        src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                                        alt="" width="25px" @click="removeField(index)">
                                                                </button>
                                                            </div>
                                                        </template>
                                                        <template x-if="index !== (fields.length - 1)">
                                                            <button class="mx-2"><img
                                                                    src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                                    alt="" width="25px" @click="removeField(index)">
                                                            </button>
                                                        </template>
                                                    </div>
                                                </div>
                                            </div>
                                        </template>
                                    </template>
                                </div>
                            </div>


                            {{-- Ekstra --}}
                            <div class="pb-5">
                                <div class="text-lg font-bold font-inter text-[#4F4F4F]">
                                    Ekstra
                                </div>

                                <div class="py-2">
                                    <div class="flex space-x-2">
                                        <div class="form-check">
                                            <input
                                                class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                type="radio" name="ekstra" id="ekstra" value="Boleh"
                                                {{isset($pilihan_ekstra['ekstra_radio_button']) ?
                                                ($pilihan_ekstra['ekstra_radio_button']=='Boleh' ? 'checked' :'') :''}}
                                                required>
                                            <label class="form-check-label inline-block text-gray-800"
                                                for="ekstra_allowed">
                                                Bolehkan
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input
                                                class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                type="radio" name="ekstra" id="ekstra" value="Tidak Boleh"
                                                {{isset($pilihan_ekstra['ekstra_radio_button']) ?
                                                ($pilihan_ekstra['ekstra_radio_button']=='Tidak Boleh' ? 'checked' :'')
                                                :''}}>
                                            <label class="form-check-label inline-block text-gray-800"
                                                for="ekstra_not_allowed">
                                                Tidak Usah
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- TnC --}}
                            <div class="">
                                <div>
                                    <div class="form-check">
                                        <input
                                            class="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                            type="checkbox" value="" id="tnc" required>
                                        <label class="form-check-label inline-block text-gray-800" for="tnc">
                                            Saya setuju atas syarat dan ketentuan penambahan listing baru di Kamtuu
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="p-5">
                                <div class="grid grid-cols-6">
                                    {{--<div class="col-start-1 col-end-2">
                                        <a href="{{ route('detailproduk.informasi') }}"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">Sebelumnya</a>
                                    </div>--}}
                                    <div class="col-end-8 col-end-2">
                                        <button type="submit"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                            Simpan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>

    <script>
        // if($("#msg").attr("data-msg")!=""){
        //     console.log($("#msg").data("msg"))
        //     Swal.fire({
        //         icon: 'error',
        //         text: $("#msg").attr("data-msg"),
        //         customClass: 'swal-height'
        //     })   
        // }

        function handler() {
            
            return {
                fields: checkIsEmptyArray({!! isset($batas['pilihan_ekstra']) ? $batas['pilihan_ekstra'] :'[]' !!}),
                addNewField() {
                    this.fields.push({
                        detail_1: '',
                        judul_pilihan:'',
                        deskripsi_pilihan:'',
                        harga_pilihan:'',
                        kewajiban_pilihan:'',
                        name:''
                    });                    
                    
                    console.log('data',Alpine.raw(this.fields));
                },

                removeField(index) {
                    // console.log(index)
                    this.fields.splice(index, 1);
                    console.log(this.fields.detail_1)
                },
            
            }

            
        }

        function checkIsEmptyArray(data) {
            console.log('data', data)
            if (!data) {
                return []
            }

            if (data) {
                return data
            }
        }

        
        function thisChange(t){
            var id = t.id;
            var val = document.getElementById(id);
            let index = id.split("_");
            
            console.log(index);
            console.log(val.value)
            console.log(id)
            
            var field_hotel          = index[0]+'field_hotel';
            var field_insurance      = index[0]+'field_insurance';
            var field_nama_hotel     = index[0]+'[nama_hotel]';
            var field_nama_asuransi  = index[0]+'[nama_asuransi]';
            var field_harga_hotel    = index[0]+'[harga_hotel]';
            var field_harga_asuransi = index[0]+'[harga_asuransi]';
            
            
            console.log(field_hotel)
            console.log(val.value);

            if(val.value.toLowerCase()=='hotel'){
                document.getElementById(field_insurance).classList.add("hidden")
                document.getElementById(field_harga_asuransi).required=false
                document.getElementById(field_nama_asuransi).required=false
                
                document.getElementById(field_hotel).classList.remove("hidden")
                document.getElementById(field_nama_hotel).required=true
                document.getElementById(field_harga_hotel).required=true
            }else if(val.value.toLowerCase()=='asuransi'){
                document.getElementById(field_hotel).classList.add("hidden")
                document.getElementById(field_nama_asuransi).required=false
                document.getElementById(field_harga_hotel).required=false

                document.getElementById(field_insurance).classList.remove("hidden")
                document.getElementById(field_nama_asuransi).required=true
                document.getElementById(field_harga_asuransi).required=true

            }else{
                document.getElementById(field_hotel).classList.remove("hidden")
                document.getElementById(field_insurance).classList.add("hidden")
                document.getElementById(field_harga_asuransi).required=false
                document.getElementById(field_nama_asuransi).required=false
                document.getElementById(field_nama_asuransi).required=false
                document.getElementById(field_harga_hotel).required=false
            }
           
        }

        function swictcher(index, value){
            var pilihan  = index+'_pilihan'
            var field_hotel = index+'field_hotel';
            var field_insurance = index+'field_insurance';
            console.log(value)
            
            Alpine.nextTick(()=>{
                console.log(document.getElementById(pilihan).value)
                if('hotel'==value.toLowerCase() && value.length > 1){
                    document.getElementById(field_insurance).classList.add("hidden")                    
                    document.getElementById(field_hotel).classList.remove("hidden")
                }else if('asuransi'==value.toLowerCase() && value.length > 1){
                    document.getElementById(field_hotel).classList.add("hidden")
                    document.getElementById(field_insurance).classList.remove("hidden")
                }else{
                    document.getElementById(field_hotel).classList.add("hidden")
                    document.getElementById(field_insurance).classList.add("hidden")
                }
            })
        }

    </script>

</body>

</html>