<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css" rel="stylesheet">

    <!-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> -->

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        .disabled {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
            color: #5f5e5e;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            {{-- @dump($it) --}}
            @php
                if($isedit){
                    $itenerary['additenenary']=$it['itenenary'];
                }
            @endphp
            <div class="p-5 pl-10 pr-10">
                <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
                {{-- Breadcumbs --}}
                <nav class="flex" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        @if($isedit)
                        <li class="inline-flex items-center">
                            <a href="{{route('tur.viewAddListingEdit',$id)}}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewItenenarydit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1]">Buat
                                    Itinerary</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewHargaEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['harga']) ? ($tur['harga']!='' ?'text-[#333333] hover:text-[#23AEC1]':'disabled'):'disabled'}}">Harga
                                    & Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewBatasEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['batas']) ? ($tur['batas']!='' ?'text-[#333333] hover:text-[#23AEC1]':'disabled'):'disabled'}}">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPeta')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['peta']) ? ($tur['peta']!='' ?'text-[#333333] hover:text-[#23AEC1]':'disabled'):'disabled'}}">Peta
                                    & Foto</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPilihanEkstra')}}"
                                {{--  --}}
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['pilihan_ekstra']) ? ($tur['pilihan_ekstra'] !='' ? 'text-[#333333] hover:text-[#23AEC1]':'disabled'):'disabled'}}">Pilihan
                                    & Ekstra</a>
                            </div>
                        </li>  
                        @else
                        <li class="inline-flex items-center">
                            <a href="{{route('tur')}}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewItenenary')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1]">Buat
                                    Itinerary</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewHarga')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['harga']) ? ($tur['harga']!='' ?'text-[#333333] hover:text-[#23AEC1]':'disabled'):'disabled'}}">Harga
                                    & Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewBatas')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['batas']) ? ($tur['batas']!='' ?'text-[#333333] hover:text-[#23AEC1]':'disabled'):'disabled'}}">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPeta')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['peta']) ? ($tur['peta']!='' ?'text-[#333333] hover:text-[#23AEC1]':'disabled'):'disabled'}}">Peta
                                    & Foto</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPilihanEkstra')}}" {{-- --}}
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['pilihan_ekstra']) ? ($tur['pilihan_ekstra'] !='' ? 'text-[#333333] hover:text-[#23AEC1]':'disabled'):'disabled'}}">Pilihan
                                    & Ekstra</a>
                            </div>
                        </li>
                        @endif
                    </ol>
                </nav>
                @if($isedit)
                <form action="{{ route('tur.addItenenary.edit',$id) }}" method="POST" enctype="multipart/form-data" id="formItenenary">
                @else
                <form action="{{ route('tur.addItenenary') }}" method="POST" enctype="multipart/form-data" id="formItenenary">
                @endif
                    @csrf
                    @method('PUT')
                    <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded" x-data="handler()">
                        <div class="flex justify-between">
                            <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Buat Itinerary</div>
                            <div class="p-5">
                            </div>
                        </div>
                        {{-- @dump($itenerary) --}}
                        <template x-if="details.length < 1" :data-length="details.length" id="itenenary">
                            {{-- @endif --}}
                            <div class="px-5">
                                <button type="button"
                                    class="w-24 items-center px-5 py-2.5 mb-5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]"
                                    @click="addNewField()">
                                    Tambah
                                </button>
                                @if ($errors->has('details'))
                                <span class="text-rose-900">{{$errors->first('details')}}</span>
                                @endif
                                <div class="flex p-4 mb-4 text-sm text-blue-700 bg-blue-100 rounded-lg w-[50%]"
                                    role="alert">
                                    <svg aria-hidden="true" class="flex-shrink-0 inline w-5 h-5 mr-3"
                                        fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                            d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                                            clip-rule="evenodd"></path>
                                    </svg>
                                    <span class="sr-only">Info</span>
                                    <div>
                                        <span class="font-medium">Info!</span> Klik tombol tambah di atas, untuk
                                        menambahkan!
                                    </div>
                                </div>
                            </div>
                        </template>

                        <template x-if="details.length >= 1" class="col">
                            <template x-for="(detail, index) in details" :key="index">
                                <div class="container mx-auto px-5 space-y-3 py-3">
                                    <div>
                                        <label for="" class="block mb-2 text-sm font-bold text-[#333333]">Judul
                                            Itenerary<span class="text-red-500 font-bold">*</span></label>
                                        <input type="text" id="judul_itenenary"
                                            :name="'details['+index+'][judul_itenenary]'"
                                            x-model="detail.judul_itenenary"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Tur Sehat" required>
                                    </div>

                                    <div class="pt-5">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333]">Deskripsi<span
                                                class="text-red-500 font-bold">*</span></label>
                                        <textarea rows="4" :name="'details['+index+'][deskripsi_itenenary]'"
                                            x-model="detail.deskripsi_itenenary" id="deskripsi_itenerary"
                                            class="block p-2.5 w-96 text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Deskripsi" required></textarea>
                                    </div>

                                    <div class="flex mt-5 my-3 space-x-3">
                                        <div class="form-check">
                                            <input
                                                class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                type="radio" :name="'details['+index+'][radio_itinenary]'"
                                                :id="'radio_image'+index" value="images"
                                                x-model="detail.radio_itinenary">
                                            <label class="form-check-label inline-block text-gray-800"
                                                for="radio_image">
                                                Image Itinerary
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input
                                                class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                type="radio" :name="'details['+index+'][radio_itinenary]'"
                                                :id="'radio_video'+index" value="videos"
                                                x-model="detail.radio_itinenary">
                                            <label class="form-check-label inline-block text-gray-800"
                                                for="radio_video">
                                                Video Itinerary
                                            </label>
                                        </div>
                                    </div>

                                    {{-- Photo --}}
                                    <template x-if="detail.radio_itinenary === 'images'">
                                        <div class="py-2 mb-6">
                                            <p class="font-semibold text-[#BDBDBD]">Foto<span
                                                    class="text-red-500 font-bold">*</span></p>
                                            <template x-if="detail.images === ''">
                                                <div
                                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                                    <img src="{{ asset('storage/icons/upload.svg') }}"
                                                        alt="upload-icons" width="20px" height="20px">
                                                </div>
                                            </template>
                                            <template x-if="detail.images !== ''">
                                                <div class="flex">
                                                    <div class="flex justify-center items-center">
                                                        <img :src="detail.images"
                                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                            :alt="'upload'+index">
                                                        <button type="button"
                                                            class="absolute mx-2 translate-x-12 -translate-y-14">
                                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                                alt="" width="25px" @click="removeImage(index)">
                                                        </button>
                                                    </div>
                                                    {{-- <template x-for="(image, index) in detail.images" :key="index">
                                                        <div class="flex justify-center items-center">
                                                            <img :src="image"
                                                                class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                                :alt="'upload'+index"> --}}
                                                            {{-- <button
                                                                class="absolute mx-2 translate-x-12 -translate-y-14">
                                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                                    alt="" width="25px" @click="removeImage(index)">
                                                            </button> --}}
                                                            {{--
                                                        </div>
                                                    </template> --}}
                                                </div>
                                            </template>
                                            <input class="py-2" type="file" accept="image/*" id="gallery_itenenary"
                                                :name="'details['+index+'][gallery_itenenary]'"
                                                @change="selectedFile($event, index)">
                                        </div>
                                    </template>

                                    {{-- Video --}}
                                    <template x-if="detail.radio_itinenary === 'videos'">
                                        <div>
                                            <p class="font-semibold text-[#BDBDBD]">Video<span
                                                    class="text-red-500 font-bold">*</span></p>
                                            <input type="text" id="tautan_video"
                                                :name="'details['+index+'][tautan_video]'"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="Video URL (Youtube)" x-model="detail.tautan_video"
                                                required>
                                        </div>
                                    </template>

                                    {{-- <div class="flex flex-wrap">
                                        <template x-if="detail.images.length > 0">
                                            <template x-for="(image, index) in detail.images" :key="index">
                                                <div class="w-[156px] h-[156px] border rounded-md mr-5 my-2">
                                                    <img class="w-full h-full object-cover object-center" :src="image"
                                                        alt="">
                                                </div>
                                            </template>
                                        </template>
                                        <template x-if="detail.video !== ''">
                                            <iframe width="560" height="315" src="" title="YouTube video player"
                                                frameborder="0"
                                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                        </template>
                                    </div> --}}

                                    <template x-if="index === (details.length - 1)">
                                        <div>
                                            <button type="button"
                                                class="bg-[#23AEC1] text-white text-sm font-semibold px-5 py-2.5 rounded-lg hover:bg-[#23AEC1]"
                                                @click="addNewField(detail)">Tambah</button>
                                            <button type="button"
                                                class="bg-[#D50006] text-white text-sm font-semibold px-5 py-2.5 rounded-lg hover:bg-[#de252b]"
                                                @click="removeField(detail)">Hapus</button>
                                        </div>
                                    </template>
                                    <template x-if="index !== (details.length - 1)">
                                        <div>
                                            <button type="button"
                                                class="bg-[#D50006] text-white text-sm font-semibold px-5 py-2.5 rounded-lg hover:bg-[#de252b]"
                                                @click="removeField(detail)">Hapus</button>
                                        </div>
                                    </template>

                                    <hr class="border-[#333333]" />
                                </div>
                            </template>
                        </template>
                        <div class="p-5">
                            <div class="grid grid-cols-6">
                                <div class="col-start-1 col-end-2">
                                    <a href="{{ route('tur') }}"
                                        class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">Sebelumnya</a>

                                    {{-- <a href="{{route('tur')}}">
                                        <button type="submit"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                            Sebelumnya
                                        </button>
                                    </a> --}}
                                </div>
                                <div class="col-start-6 col-end-7">
                                    <button type="submit"
                                        class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                        Selanjutnya
                                    </button>
                                </div>
                            </div>
                        </div>
                        {{-- <x-be.com.two-button-dup>
                            {{ route('activity.viewBatas') }}
                        </x-be.com.two-button-dup> --}}
                        {{-- <div class="p-5">
                            <div class="grid grid-cols-6">
                                <div class="col-start-1 col-end-2">
                                    <a href="{{ route('tur') }}"
                                        class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">Sebelumnya</a>
                                </div>
                                <div class="col-end-8 col-end-2">
                                    <button type="submit"
                                        class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                        Selanjutnya
                                    </button>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- <script>
        $('#deskripsi_itenerary').summernote({
            placeholder: 'Hello stand alone ui',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
    </script> --}}
    <script>
        const initSummerNote = () => {
        $('#deskripsi_itenerary').summernote({
            placeholder: 'Description',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
    }
    </script>

    <script>
        let data = {!! isset($itenerary['gallery_itenenary']) ? $itenerary['gallery_itenenary'] : "[]" !!};
        console.log(data)
        // let data = [];
        let imagesGallery = [];

        if (data.length > 0) {
            const url = window.location.origin
            let array = data
            array.forEach(element => {
                imagesGallery.push(url+'/'+element)
            });
            console.log(imagesGallery)
        }
        // gallery_itenenary
        function handler(){

            const itenerary = [];
            const additenerary =  {!! isset($itenerary['additenenary']) ? $itenerary['additenenary'] : "[]" !!}
           
            for(let x=0; x < additenerary.length; x++){
                console.log(additenerary[x].gallery_itenenary)
                itenerary.push({
                    'judul_itenenary': additenerary[x].judul_itenenary,
                    'deskripsi_itenenary':additenerary[x].deskripsi_itenenary,
                    'images': '{!! url('/') !!}'+'/'+additenerary[x].gallery_itenenary,
                    'tautan_video': additenerary[x].tautan_video,
                    'radio_itinenary': additenerary[x].radio_itinenary,
                })
            }

            console.log(itenerary)

            return {
                // judul_itenenary:'',
                // deskripsi_itenenary: 'data',
                radio_itinenary: 'images',
                tautan_video: '',
                images: imagesGallery,
                details: itenerary,
                error: '',

                selectedFile(event, index) {
                    this.fileToUrl(event, index)
                },

                fileToUrl(event, index) {
                    if (! event.target.files.length) return

                    let file = event.target.files[0]
                    
                    let reader = new FileReader();
                    let srcImg = ''
                    
                    reader.readAsDataURL(file);
                    reader.onload = e => {
                        srcImg = e.target.result
                        this.details[index].images = srcImg
                    };

                    data = data.filter(element => element.index != index)

                    data.push({
                        index: index,
                        file: file
                    })

                    console.log(data)
                    
                    // Send image using ajax
                    this.sendImage()
                },
    
                removeImage(index) {
                    this.details[index].images = '';
                    data = data.filter(element => element.index != index)

                    // Update image using ajax
                    this.sendImage()
                },

                checkRadioButton(detail){

                    if (this.details === [] || detail.gallery_itenenary === '' || detail.tautan_video === '' || detail.gallery_itenenary != '') {
                        return 'images'
                    }
                    
                    if(detail.tautan_video != '' ){
                        return 'videos'
                    }
                },

                sendImage() {
                    let fd = new FormData();

                    // console.log(data)
                    for (const file of data) {
                        if (typeof file === 'object') {
                            console.log(file.file.name)
                            fd.append('images_gallery[]', file.file)
                            fd.append('index_gallery[]', file.index)
                        }
                        if (typeof file === 'string') {
                            fd.append('saved_gallery[]', file)
                        }
                    }

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        method: 'POST',
                        url: "{{ route('tur.addIteneraryGallery') }}",
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function(msg) {
                        },
                        error: function(data) {
                            // console.log('error:', data);
                        },
                    })
                },

                addNewField(){    
                    this.details.push({
                        judul_itenenary: '',
                        deskripsi_itenenary:'',
                        radio_itinenary: this.radio_itinenary,
                        images:'',
                        tautan_video: ''
                    });

                    console.log(this.details)

                },
                
                removeField(index){
                    this.details.splice(index,1);
                    console.log(this.details)
                }
                
            }
        }
    </script>
</body>

</html>