<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css" rel="stylesheet">

    <!-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> -->

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        .disabled {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                {{-- @dump($harga['availability']) --}}
                <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
                {{-- Breadcumbs --}}
                <nav class="flex pb-5" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        @if($isedit)
                        <li class="inline-flex items-center">
                            <a href="{{route('tur.viewAddListingEdit',$id)}}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                        </li>

                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewHargaEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['harga']) ? 'text-[#23AEC1] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled'}}">Harga
                                    & Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                {{-- 'hover:text-[#23AEC1]':'text-[#5f5e5e] disabled' --}}
                                <a href="{{route('tur.viewItenenarydit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['itenerary']) ? ($tur['itenerary']!= '' ? 'text-[#333333] hover:text-[#23AEC1]' : 'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled'}}"
                                    data-role="disabled">Buat
                                    Itinerary</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewBatasEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['batas']) ? ($tur['batas']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled'}}">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPetaEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['peta']) ? ($tur['peta']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled'):'text-[#5f5e5e] disabled'}}">Peta
                                    & Foto</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPilihanEkstraEdit',$id)}}" {{-- $tur['pilihan_ekstra']!=''
                                    ? ' hover:text-[#23AEC1]' :'text-[#5f5e5e] disabled' --}}
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['pilihan_ekstra']) ? ($tur['pilihan_ekstra']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled' }}">Pilihan
                                    & Ekstra</a>
                            </div>
                        </li>
                        @else
                        <li class="inline-flex items-center">
                            <a href="{{route('tur')}}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                {{-- 'hover:text-[#23AEC1]':'text-[#5f5e5e] disabled' --}}
                                <a href="{{route('tur.viewItenenary')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['itenerary']) ? ($tur['itenerary']!= '' ? 'text-[#333333] hover:text-[#23AEC1]' : 'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled'}}"
                                    data-role="disabled">Buat
                                    Itinerary</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewHarga')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1]">Harga
                                    & Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewBatas')}}" {{-- --}} {{-- {{isset($tur['batas']) ?
                                    ($$tur['batas']!='' ? 'text-[#333333] hover:text-[#23AEC1]'
                                    : 'text-[#5f5e5e] disabled' ) :'text-[#5f5e5e] disabled'}} --}}
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['batas']) ? ($tur['batas']!='' ? 'text-[#333333] hover:text-[#23AEC1]' : 'text-[#5f5e5e] disabled') :'text-[#5f5e5e] disabled'}}">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPeta')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['peta']) ? ($tur['peta']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled'):'text-[#5f5e5e] disabled'}}">Peta
                                    & Foto</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPilihanEkstra')}}" {{-- $tur['pilihan_ekstra']!=''
                                    ? ' hover:text-[#23AEC1]' :'text-[#5f5e5e] disabled' --}}
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['pilihan_ekstra']) ? ($tur['pilihan_ekstra']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled' }}">Pilihan
                                    & Ekstra</a>
                            </div>
                        </li>
                        @endif
                    </ol>
                </nav>

                {{-- Section 1 --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl p-6 px-10 rounded">
                    {{-- @dump($packet) --}}
                    @if($type=='Open Trip')
                    @if($isedit)
                    <form action="{{ route('tur.addHarga.edit',$id) }}" method="POST" enctype="multipart/form-data">
                        @else
                        <form action="{{ route('tur.addHarga') }}" method="POST" enctype="multipart/form-data">
                            @endif
                            @csrf
                            @method('PUT')
                            {{-- <select name="" id="" @change="changeField"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <option disabled selected>Pilih Paket</option>
                                <option value="{{$paket['nama_paket']}}">{{$paket['nama_paket']}}</option>

                            </select>
                            --}}
                            @foreach($packet as $paket)
                            <div class="mb-3">
                                @php
                                $nama_paket = str_replace(' ','_',$paket['nama_paket']);
                                if(isset($harga['paket']['harga'][$nama_paket])){
                                $harga_paket = $harga['paket']['harga'][($nama_paket)];
                                }else{
                                $harga_paket = [];
                                }
                                @endphp
                                {{-- @dump($harga_paket) --}}
                                <label class="capitalize text-slate-400 font-inter">Nama Paket :
                                    <span>{{$paket['nama_paket']}}</span></label>
                                <div class="flex justify-between pb-5">
                                    <div class="text-lg font-bold font-inter text-[#9E3D64]">Harga Dalam Mata Uang
                                        Rupiah
                                        (IDR)</div>
                                </div>
                                <div class="grid grid-cols-2 pb-2">
                                    <div class="text-sm font-bold text-[#333333]">Residen</div>
                                    <div class="text-sm font-bold text-[#333333]">
                                        <p>Non Residen</p>
                                        <p class="text-xs font-normal"></p>
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div class="">
                                        <p class="block md:hidden text-sm font-bold text-[#333333] mb-6">Residen</p>
                                        <div class="mb-6">
                                            <label for="dewasa_residen"
                                                class="block mb-2 text-sm font-bold text-[#333333]">Dewasa<span
                                                    class="text-red-500 font-bold">*</span></label>
                                            <input type="text" id="[{{$loop->index}}]dewasa_residen"
                                                name="{{$paket['nama_paket']}}[dewasa_residen]"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="120000" required
                                                value="{{isset($harga_paket['dewasa_residen']) ?
                                                $harga_paket['dewasa_residen']:null}}">
                                        </div>
                                        <div class="mb-6">
                                            <label for="anak_residen"
                                                class="block mb-2 text-sm font-bold text-[#333333] ">Anak
                                                (5 -
                                                12
                                                Tahun)<span class="text-red-500 font-bold">*</span></label>
                                            <input type="text" id="[{{$loop->index}}]anak_residen"
                                                name="{{$paket['nama_paket']}}[anak_residen]" 
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="60000" required
                                                value="{{isset($harga_paket['anak_residen']) ? $harga_paket['anak_residen']:null}}">
                                        </div>
                                        <div class="mb-6">
                                            <label for="balita_residen"
                                                class="block mb-2 text-sm font-bold text-[#333333] ">Balita
                                                (< 5 Tahun)</label>
                                                    <input type="text" id="[{{$loop->index}}]balita_residen"
                                                        name="{{$paket['nama_paket']}}[balita_residen]"
                                                       
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="0" value={{isset($harga_paket['balita_residen']) ?
                                                        $harga_paket['balita_residen']:null}}>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="block md:hidden text-sm font-bold text-[#333333] mb-6">
                                            <p>Non-Residen</p>
                                            <p class=" text-xs font-normal">(Bila tidak diisi harga berlaku sama)</p>
                                        </div>

                                        <div class="mb-6">
                                            <label for="dewasa_non_residen"
                                                class="block mb-2 text-sm font-bold text-[#333333] ">Dewasa</label>
                                            {{-- @if(isset($harga_paket['dewasa_residen']))
                                            <input type="text" id="dewasa_non_residen"
                                                name="{{$paket['nama_paket']}}[dewasa_non_residen]"
                                                x-model="dewasa_non_residen"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="120000"
                                                value="{{!isset($harga_paket['dewasa_non_residen']) ? $harga_paket['dewasa_residen']:$harga_paket['dewasa_non_residen']}}">
                                            @else
                                            @endif --}}
                                            <input type="text" id="dewasa_non_residen"
                                                name="{{$paket['nama_paket']}}[dewasa_non_residen]"
                                               
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="120000"
                                                value="{{isset($harga_paket['dewasa_non_residen']) ? $harga_paket['dewasa_non_residen']:null}}">
                                        </div>
                                        <div class="mb-6">
                                            <label for="anak_non_residen"
                                                class="block mb-2 text-sm font-bold text-[#333333] ">Anak
                                                (5 - 12
                                                Tahun)</label>
                                            {{-- @if(isset($harga_paket['anak_residen']))
                                            <input type="text" id="anak_non_residen"
                                                name="{{$paket['nama_paket']}}[anak_non_residen]"
                                                x-model="anak_non_residen"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="60000"
                                                value="{{!isset($harga_paket['anak_non_residen']) ? $harga_paket['anak_residen']:$harga_paket['anak_non_residen']}}">
                                            @else
                                            @endif --}}
                                            <input type="text" id="anak_non_residen"
                                                name="{{$paket['nama_paket']}}[anak_non_residen]"
                                               
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="60000"
                                                value="{{isset($harga_paket['anak_non_residen']) ? $harga_paket['anak_non_residen']:null}}">
                                        </div>
                                        <div class="mb-6">
                                            <label for="balita_non_residen"
                                                class="block mb-2 text-sm font-bold text-[#333333] ">Balita (< 5
                                                    Tahun)</label>
                                                    {{-- @if(isset($harga_paket['balita_residen']))
                                                    <input type="text" id="balita_non_residen"
                                                        name="{{$paket['nama_paket']}}[balita_non_residen]"
                                                        x-model="balita_non_residen"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="0"
                                                        value="{{!isset($harga_paket['balita_non_residen']) ? $harga_paket['balita_residen']:$harga_paket['balita_non_residen']}}">
                                                    @else
                                                    @endif --}}
                                                    <input type="text" id="balita_non_residen"
                                                        name="{{$paket['nama_paket']}}[balita_non_residen]"
                                                       
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="0"
                                                        value="{{isset($harga_paket['balita_non_residen']) ? $harga_paket['balita_non_residen']:null}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <button class="text-white bg-[#23AEC1] py-1 px-3 rounded-md">Perbarui</button>
                        </form>
                        @else
                        @if($isedit)
                        <form action="{{ route('tur.addHarga.edit',$id) }}" method="POST" enctype="multipart/form-data"
                            x-data="harga()">
                            @else
                            <form action="{{ route('tur.addHarga') }}" method="POST" enctype="multipart/form-data"
                                x-data="harga()">
                                @endif
                                @csrf
                                @method('PUT')

                                <div class="flex justify-between pb-5">
                                    <div class="text-lg font-bold font-inter text-[#9E3D64]">Harga Dalam Mata Uang
                                        Rupiah (IDR)
                                    </div>
                                </div>

                                {{-- Residen and Non Residen --}}
                                <div class="grid grid-cols-2 pb-2">
                                    <div class="text-sm font-bold text-[#333333]">Residen</div>
                                    <div class="text-sm font-bold text-[#333333]">
                                        <p>Non-Residen</p>
                                        <p class=" text-xs font-normal">(Bila tidak diisi harga berlaku sama)</p>
                                    </div>
                                </div>

                                <div class="grid grid-cols-2">
                                    <div class="">
                                        <p class="block md:hidden text-sm font-bold text-[#333333] mb-6">Residen</p>
                                        <div class="mb-6">
                                            <label for="dewasa_residen"
                                                class="block mb-2 text-sm font-bold text-[#333333]">Dewasa<span
                                                    class="text-red-500 font-bold">*</span></label>
                                            <input type="text" id="dewasa_residen" name="dewasa_residen"
                                                x-model="dewasa_residen"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="120000" required>
                                        </div>
                                        <div class="mb-6">
                                            <label for="anak_residen"
                                                class="block mb-2 text-sm font-bold text-[#333333] ">Anak
                                                (5 -
                                                12
                                                Tahun)<span class="text-red-500 font-bold">*</span></label>
                                            <input type="text" id="anak_residen" name="anak_residen"
                                                x-model="anak_residen"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="60000" required>
                                        </div>
                                        <div class="mb-6">
                                            <label for="balita_residen"
                                                class="block mb-2 text-sm font-bold text-[#333333] ">Balita
                                                (< 5 Tahun)</label>
                                                    <input type="text" id="balita_residen" name="balita_residen"
                                                        x-model="balita_residen"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="0">
                                        </div>
                                        <button class="text-white bg-[#23AEC1] py-1 px-3 rounded-md">Perbarui</button>
                                    </div>
                                    <div>
                                        <div class="block md:hidden text-sm font-bold text-[#333333] mb-6">
                                            <p>Non-Residen</p>
                                            <p class=" text-xs font-normal">(Bila tidak diisi harga berlaku sama)</p>
                                        </div>

                                        <div class="mb-6">
                                            <label for="dewasa_non_residen"
                                                class="block mb-2 text-sm font-bold text-[#333333] ">Dewasa</label>
                                            <input type="text" id="dewasa_non_residen" name="dewasa_non_residen"
                                                x-model="dewasa_non_residen"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="120000">
                                        </div>
                                        <div class="mb-6">
                                            <label for="anak_non_residen"
                                                class="block mb-2 text-sm font-bold text-[#333333] ">Anak
                                                (5 - 12
                                                Tahun)</label>
                                            <input type="text" id="anak_non_residen" name="anak_non_residen"
                                                x-model="anak_non_residen"
                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="60000">
                                        </div>
                                        <div class="mb-6">
                                            <label for="balita_non_residen"
                                                class="block mb-2 text-sm font-bold text-[#333333] ">Balita (< 5
                                                    Tahun)</label>
                                                    <input type="text" id="balita_non_residen" name="balita_non_residen"
                                                        x-model="balita_non_residen"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="0">
                                        </div>
                                    </div>
                                </div>
                            </form>
                            @endif
                            {{-- @php
                            dd($harga);
                            @endphp --}}
                            @if($isedit)
                            <form action="{{ route('tur.addDiscountGroup.edit',$id) }}" method="POST"
                                enctype="multipart/form-data">
                                @else

                                <form action="{{ route('tur.addDiscountGroup') }}" method="POST"
                                    enctype="multipart/form-data">
                                    @endif
                                    @csrf
                                    @method('PUT')
                                    <div class="py-5" x-data="handler()">
                                        <div class="mb-2">
                                            <div class="text-lg font-bold font-inter text-[#9E3D64]">Diskon grup dari
                                                jumlah orang
                                                dewasa
                                                (IDR)
                                            </div>
                                            @if ($errors->has('fields'))
                                            <span class="text-red-500 font-bold">{{$errors->first('fields')}}</span>
                                            {{-- <span class="text-rose-900">{{$errors->first('details')}}</span> --}}
                                            @endif
                                        </div>
                                        <table class="my-5 w-full border table-auto rounded-md">
                                            <thead>
                                                <tr>
                                                    <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Min Jumlah
                                                        Orang</th>
                                                    <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Maks Jumlah
                                                        Orang</th>
                                                    <th class="p-3 border border-slate-500 bg-[#BDBDBD]">%</th>
                                                    <th class="p-3 border border-slate-500 bg-[#BDBDBD]"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <template x-if="fields.length < 1">
                                                    <div class="px-3">
                                                        <div>
                                                            <button type="button"
                                                                class="px-3 py-1 my-3 text-sm text-white rounded-full btn btn-info bg-kamtuu-second"
                                                                @click="addNewField()">Tambah +</button>
                                                        </div>
                                                    </div>
                                                </template>
                                                <template x-if="fields.length >= 1" class="col">
                                                    <template x-for="(field, index) in fields" :key="index">
                                                        <tr class="align-middle">
                                                            <td class="p-3 border border-slate-500 text-center">
                                                                <input type="text" id="min_orang"
                                                                    :name="'fields['+index+'][min_orang]'"
                                                                    x-model="field.min_orang"
                                                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                    placeholder="3" required>
                                                            </td>
                                                            <td class="p-3 border border-slate-500 text-center">
                                                                <input type="text" id="max_orang"
                                                                    :name="'fields['+index+'][max_orang]'"
                                                                    x-model="field.max_orang"
                                                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                    placeholder="5" required>
                                                            </td>
                                                            <td class=" p-3 border border-slate-500 text-center">
                                                                <input type="text" id="diskon_orang"
                                                                    :name="'fields['+index+'][diskon_orang]'"
                                                                    x-model="field.diskon_orang"
                                                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                    placeholder="5" required>
                                                            </td>
                                                            <template x-if="index === (fields.length - 1)">
                                                                <td class="p-3 border border-slate-500 text-center">

                                                                    <button class="mx-1"><img
                                                                            src="{{ asset('storage/icons/delete-dynamic-data.png') }}"
                                                                            alt="" width="16px"
                                                                            @click="removeField(index)"></button>
                                                                    <button class="mx-1"><img
                                                                            src="{{ asset('storage/icons/circle-plus-solid 1.svg')}}"
                                                                            alt="" width="16px"
                                                                            @click="addNewField()"></button>
                                                                </td>
                                                            </template>
                                                            <template x-if="index !== (fields.length - 1)">
                                                                <td class="p-3 border border-slate-500 text-center">
                                                                    <button class="mx-1"><img
                                                                            src="{{ asset('storage/icons/delete-dynamic-data.png') }}"
                                                                            alt="" width="16px"
                                                                            @click="removeField(index)"></button>
                                                                </td>
                                                            </template>
                                                        </tr>
                                                    </template>
                                                </template>
                                            </tbody>
                                        </table>
                                    </div>

                                    <button class="text-white bg-[#23AEC1] py-1 px-3 rounded-md">Perbarui</button>
                                </form>

                </div>

                {{-- Section 2 --}}
                @if($type=='Open Trip')
                <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded" x-data="discountDatePacket()">

                    <p class="font-bold text-lg text-[#333333] pb-5">Ketersediaan</p>
                    <div class="flex md:space-x-10 gap-5">
                        <div class="mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Dari
                                Tanggal</label>
                            <div
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <input type="text" id="tgl_start" name="tgl_start" placeholder="">
                            </div>
                        </div>
                        <div class="mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Sampai
                                Tanggal</label>
                            <div
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <input type="text" id="tgl_end" name="tgl_end" placeholder="">
                            </div>
                        </div>
                        <div class="pb-5">
                            <label for="text" class="flex mb-2 text-sm font-bold text-[#333333]"
                                x-data="{ tooltip: false }" x-on:mouseover="tooltip = true"
                                x-on:mouseleave="tooltip = false">x/- (%)>
                                <img class="mx-2" width="15px"
                                    src="{{ asset('storage/icons/circle-info-solid (1) 1.svg') }}" alt="">
                                <div x-show="tooltip"
                                    class="text-sm text-white absolute bg-[#9E3D64] rounded-lg p-2 transform -translate-y-8 translate-x-8">
                                    Harap semua field diisi
                                </div>
                            </label>
                            <input type="text" id="discount" name="discount" x-model="discount"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="">
                        </div>
                    </div>

                    <button type="button" class="text-white bg-[#23AEC1] py-1 px-3 rounded-md" @click="addNewField()"
                        id="btnTambah">Tambah</button>
                    @if($isedit)
                    <form action="{{ route('tur.addAvailability.edit',$id) }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        @else
                        <form action="{{ route('tur.addAvailability') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            @endif
                            <div class="grid grid-cols-10 my-5">
                                <template x-if="details.length === 0">
                                    <div class="col-span-5 border border-slate-300 rounded-md ">
                                        <div class="my-2 mx-3">
                                            <p>Anda belum menambahkan Tanggal ketersedian dan diskon.</p>
                                        </div>
                                    </div>
                                </template>
                                <template x-if="details.length >= 1">
                                    <template x-for="(detail, index) in details" :key="index">

                                        <div class="col-span-10 flex">
                                            <div class="w-5/6 border border-slate-500 rounded-md my-5">
                                                <div class="my-2 mx-3" x-data="{ open:false }">
                                                    <div class="flex border-b border-gray-200 pt-1 pb-2">
                                                        <button type="button"
                                                            class="text-lg sm:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg flex space-x-10"
                                                            x-on:click="open = !open">
                                                            <div class="cursor-pointer pr-5">
                                                                <p for="text"
                                                                    class="block mb-2 text-sm font-bold text-slate-500">
                                                                    Dari
                                                                    Tanggal</p>
                                                                <p class=" text-base font-bold text-[#333333]"
                                                                    x-text="detail.tgl_start"></p>
                                                                <input type="hidden" x-model="detail.tgl_start"
                                                                    :name="'details['+index+'][tgl_start]'" required>
                                                            </div>
                                                            <div class="cursor-pointer pr-5">
                                                                <p for="text"
                                                                    class="block mb-2 text-sm font-bold text-slate-500">
                                                                    Sampai
                                                                    Tanggal</p>
                                                                <p class=" text-base font-bold text-[#333333]"
                                                                    x-text="detail.tgl_end"></p>
                                                                <input type="hidden" x-model="detail.tgl_end"
                                                                    :name="'details['+index+'][tgl_end]'" required>
                                                            </div>
                                                            <div class="cursor-pointer pr-5">
                                                                <p for="text"
                                                                    class="block mb-2 text-sm font-bold text-slate-500">
                                                                    +/- (%)
                                                                </p>
                                                                <p class=" text-base font-bold text-[#333333]"
                                                                    x-text="detail.discount"></p>
                                                                <input type="hidden" x-model="detail.discount"
                                                                    :name="'details['+index+'][discount]'" required>
                                                            </div>
                                                        </button>
                                                        <img class="float-right duration-200 cursor-pointer"
                                                            :class="{'rotate-180' : open}" x-on:click="open = !open"
                                                            src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                                            alt="chevron-down" width="26px" height="15px">
                                                    </div>
                                                    <div class="py-2" x-show="open" x-transition>
                                                        {{-- {{$paket['nama_paket']}} --}}
                                                        {{-- x-for="(detail, index) in details" :key="index" --}}
                                                        <template x-for="(paket, i) in detail.paket" :key="i">
                                                            <div>
                                                                <h3
                                                                    class="capitalize text-slate-400 font-inter mb-3 mt-2">
                                                                    Nama Paket : <span x-text="paket.nama_paket"></span>
                                                                </h3>
                                                                <table class="my-1 w-full table-auto">

                                                                    <thead>
                                                                        <tr>
                                                                            <th
                                                                                class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                            </th>
                                                                            <th
                                                                                class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                                Dewasa
                                                                            </th>
                                                                            <th
                                                                                class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                                Anak
                                                                            </th>
                                                                            <th
                                                                                class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                                Balita
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr class="align-middle">
                                                                            <td
                                                                                class="p-3 border-2 border-slate-500 text-center">
                                                                                <p>Residen</p>
                                                                            </td>
                                                                            <td
                                                                                class="p-3 border-2 border-slate-500 text-center">
                                                                                <p
                                                                                    x-text="hargaPaket(detail.harga_paket, paket.nama_paket, detail.discount, 'dewasa_residen')">
                                                                                </p>
                                                                            </td>
                                                                            <td
                                                                                class="p-3 border-2 border-slate-500 text-center">
                                                                                <p
                                                                                    x-text="hargaPaket(detail.harga_paket,paket.nama_paket,detail.discount, 'anak_residen')">
                                                                                </p>
                                                                            </td>
                                                                            <td
                                                                                class="p-3 border-2 border-slate-500 text-center">
                                                                                <p
                                                                                    x-text="hargaPaket(detail.harga_paket,paket.nama_paket,detail.discount, 'balita_residen')">
                                                                                </p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="align-middle">
                                                                            <td
                                                                                class="p-3 border-2 border-slate-500 text-center">
                                                                                <p>Non-Residen</p>
                                                                            </td>
                                                                            <td
                                                                                class="p-3 border-2 border-slate-500 text-center">
                                                                                <p
                                                                                    x-text="hargaPaket(detail.harga_paket, paket.nama_paket, detail.discount, 'dewasa_non_residen')">
                                                                                </p>
                                                                            </td>
                                                                            <td
                                                                                class="p-3 border-2 border-slate-500 text-center">
                                                                                <p
                                                                                    x-text="hargaPaket(detail.harga_paket, paket.nama_paket, detail.discount, 'anak_non_residen')">
                                                                                </p>
                                                                            </td>
                                                                            <td
                                                                                class="p-3 border-2 border-slate-500 text-center">
                                                                                <p
                                                                                    x-text="hargaPaket(detail.harga_paket, paket.nama_paket, detail.discount, 'balita_non_residen')">
                                                                                </p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </template>
                                                        {{-- @foreach($packet as $namaPakat)
                                                        @php
                                                        $nama_paket = str_replace(' ','_',$paket['nama_paket']);
                                                        if(isset($harga['paket'])){
                                                        $harga_paket = $harga['paket']['harga'][($nama_paket)];
                                                        // $diskonDewasa = $harga_paket
                                                        }else{
                                                        $harga_paket = [];
                                                        }
                                                        @endphp
                                                        @dump($harga_paket);
                                                        <label
                                                            class="capitalize text-slate-400 font-inter mb-3 mt-2">Nama
                                                            Paket : <span>{{$paket['nama_paket']}}</span></label>
                                                        <table class="my-1 w-full table-auto">

                                                            <thead>
                                                                <tr>
                                                                    <th
                                                                        class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                    </th>
                                                                    <th
                                                                        class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                        Dewasa
                                                                    </th>
                                                                    <th
                                                                        class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                        Anak
                                                                    </th>
                                                                    <th
                                                                        class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                        Balita
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="align-middle">
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p>Residen</p>
                                                                    </td>
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p
                                                                            x-text="hargaDewasaResiden(detail.harga_paket,detail.paket[0].nama_paket,detail.discount,detail.discount)">
                                                                        </p>
                                                                    </td>
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p
                                                                            x-text="hargaAnakResiden(detail.harga_paket,detail.paket[0].nama_paket,detail.discount,detail.discount)">
                                                                        </p>
                                                                    </td>
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p
                                                                            x-text="hargaBalitaResiden(detail.harga_paket,detail.paket[0].nama_paket,detail.discount,detail.discount)">
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                                <tr class="align-middle">
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p>Non-Residen</p>
                                                                    </td>
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p
                                                                            x-text="hargaDewasaNonResiden(detail.harga_paket,detail.paket[0].nama_paket,detail.discount,detail.discount)">
                                                                        </p>
                                                                    </td>
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p
                                                                            x-text="hargaAnakNonResiden(detail.harga_paket,detail.paket[0].nama_paket,detail.discount,detail.discount)">
                                                                        </p>
                                                                    </td>
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p
                                                                            x-text="hargaBalitaNonResiden(detail.harga_paket,detail.paket[0].nama_paket,detail.discount,detail.discount)">
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        @endforeach --}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w-1/6 px-3 my-5">
                                                <button @click="removeField(index)"
                                                    class="bg-[#D50006] text-white text-sm font-semibold py-2 px-5 rounded-lg hover:bg-[#de252b]">
                                                    Hapus
                                                </button>
                                            </div>
                                        </div>
                                    </template>
                                </template>
                            </div>

                            {{-- <x-be.com.two-button></x-be.com.two-button> --}}
                            <div class="p-5">
                                <div class="grid grid-cols-6">
                                    <div class="col-start-1 col-end-2">
                                        <a href="{{ route('tur.viewItenenary') }}"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">Sebelumnya</a>
                                    </div>
                                    <div class="col-start-6 col-end-7">
                                        <button type="submit" id="next_button"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                            Selanjutnya
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                </div>
                @else
                <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded" x-data="discountDate()">

                    <p class="font-bold text-lg text-[#333333] pb-5">Ketersediaan</p>
                    <div class="flex md:space-x-10 gap-5">
                        <div class="mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Dari
                                Tanggal</label>
                            <div
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <input type="text" id="tgl_start" name="tgl_start" placeholder="">
                            </div>
                        </div>
                        <div class="mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Sampai
                                Tanggal</label>
                            <div
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <input type="text" id="tgl_end" name="tgl_end" placeholder="">
                            </div>
                        </div>
                        <div class="pb-5">
                            <label for="text" class="flex mb-2 text-sm font-bold text-[#333333]"
                                x-data="{ tooltip: false }" x-on:mouseover="tooltip = true"
                                x-on:mouseleave="tooltip = false">x/- (%)>
                                <img class="mx-2" width="15px"
                                    src="{{ asset('storage/icons/circle-info-solid (1) 1.svg') }}" alt="">
                                <div x-show="tooltip"
                                    class="text-sm text-white absolute bg-[#9E3D64] rounded-lg p-2 transform -translate-y-8 translate-x-8">
                                    Harap semua field diisi
                                </div>
                            </label>
                            <input type="text" id="discount" name="discount" x-model="discount"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="">
                        </div>
                    </div>

                    <button type="button" class="text-white bg-[#23AEC1] py-1 px-3 rounded-md" @click="addNewField()"
                        id="discountPacketcal">Tambah</button>
                    @if($isedit)
                    <form action="{{ route('tur.addAvailability.edit',$id) }}" method="POST"
                        enctype="multipart/form-data">

                        @else
                        <form action="{{ route('tur.addAvailability') }}" method="POST" enctype="multipart/form-data">
                            @endif
                            @csrf
                            @method('PUT')
                            <div class="grid grid-cols-10 my-5">
                                <template x-if="details.length === 0">
                                    <div class="col-span-5 border border-slate-300 rounded-md ">
                                        <div class="my-2 mx-3">
                                            <p>Anda belum menambahkan Tanggal ketersedian dan diskon.</p>
                                        </div>
                                    </div>
                                </template>
                                <template x-if="details.length >= 1">
                                    <template x-for="(detail, index) in details" :key="index">
                                        <div class="col-span-10 flex">
                                            <div class="w-5/6 border border-slate-500 rounded-md my-5">
                                                <div class="my-2 mx-3" x-data="{ open:false }">
                                                    <div class="flex border-b border-gray-200 pt-1 pb-2">
                                                        <button type="button"
                                                            class="text-lg sm:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg flex space-x-10"
                                                            x-on:click="open = !open">
                                                            <div class="cursor-pointer pr-5">
                                                                <p for="text"
                                                                    class="block mb-2 text-sm font-bold text-slate-500">
                                                                    Dari
                                                                    Tanggal</p>
                                                                <p class=" text-base font-bold text-[#333333]"
                                                                    x-text="detail.tgl_start"></p>
                                                                <input type="hidden" x-model="detail.tgl_start"
                                                                    :name="'details['+index+'][tgl_start]'" required>
                                                            </div>
                                                            <div class="cursor-pointer pr-5">
                                                                <p for="text"
                                                                    class="block mb-2 text-sm font-bold text-slate-500">
                                                                    Sampai
                                                                    Tanggal</p>
                                                                <p class=" text-base font-bold text-[#333333]"
                                                                    x-text="detail.tgl_end"></p>
                                                                <input type="hidden" x-model="detail.tgl_end"
                                                                    :name="'details['+index+'][tgl_end]'" required>
                                                            </div>
                                                            <div class="cursor-pointer pr-5">
                                                                <p for="text"
                                                                    class="block mb-2 text-sm font-bold text-slate-500">
                                                                    +/- (%)
                                                                </p>
                                                                <p class=" text-base font-bold text-[#333333]"
                                                                    x-text="detail.discount"></p>
                                                                <input type="hidden" x-model="detail.discount"
                                                                    :name="'details['+index+'][discount]'" required>
                                                            </div>
                                                        </button>
                                                        <img class="float-right duration-200 cursor-pointer"
                                                            :class="{'rotate-180' : open}" x-on:click="open = !open"
                                                            src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                                            alt="chevron-down" width="26px" height="15px">
                                                    </div>
                                                    <div class="py-2" x-show="open" x-transition>
                                                        <table class="my-1 w-full table-auto">
                                                            <thead>
                                                                <tr>
                                                                    <th
                                                                        class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                    </th>
                                                                    <th
                                                                        class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                        Dewasa
                                                                    </th>
                                                                    <th
                                                                        class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                        Anak
                                                                    </th>
                                                                    <th
                                                                        class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">
                                                                        Balita
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="align-middle">
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p>Residen</p>
                                                                    </td>
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p
                                                                            x-text="dewasa_residen+((detail.discount/100)*dewasa_residen)">
                                                                        </p>
                                                                    </td>
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p
                                                                            x-text="anak_residen+((detail.discount/100)*anak_residen)">
                                                                        </p>
                                                                    </td>
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p
                                                                            x-text="balita_residen+((detail.discount/100)*balita_residen)">
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                                <tr class="align-middle">
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p>Non-Residen</p>
                                                                    </td>
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p
                                                                            x-text="dewasa_non_residen+((detail.discount/100)*dewasa_non_residen)">
                                                                        </p>
                                                                    </td>
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p
                                                                            x-text="anak_non_residen+((detail.discount/100)*anak_non_residen)">
                                                                        </p>
                                                                    </td>
                                                                    <td
                                                                        class="p-3 border-2 border-slate-500 text-center">
                                                                        <p
                                                                            x-text="balita_non_residen+((detail.discount/100)*balita_non_residen)">
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w-1/6 px-3 my-5">
                                                <button @click="removeField(index)"
                                                    class="bg-[#D50006] text-white text-sm font-semibold py-2 px-5 rounded-lg hover:bg-[#de252b]">
                                                    Hapus
                                                </button>
                                            </div>
                                        </div>
                                    </template>
                                </template>
                            </div>

                            {{-- <x-be.com.two-button></x-be.com.two-button> --}}
                            <div class="p-5">
                                <div class="grid grid-cols-6">
                                    <div class="col-start-1 col-end-2">
                                        <a href="{{ route('tur.viewItenenary') }}"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">Sebelumnya</a>
                                    </div>
                                    <div class="col-start-6 col-end-7">
                                        <button type="submit" id="next_button"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                            Selanjutnya
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                </div>
                @endif

            </div>
        </div>
    </div>
    <script>
        // function hargaPaket(){
        //     return{
        //         // for(let i =0;i<5; i++){

        //         // }
        //     }
        // }
        // function changeField(){
        //    alert('test')
        //     return{
        //         fileds:{}
        //     } 
        // }
    </script>
    <script>
        function harga() {
            return {
                dewasa_residen: {!! isset($harga['dewasa_residen']) ? $harga['dewasa_residen'] : "null" !!},
                anak_residen: {!! isset($harga['anak_residen']) ? $harga['anak_residen'] : "null" !!},
                balita_residen: {!! isset($harga['balita_residen']) ? $harga['balita_residen'] : "null" !!},
                dewasa_non_residen: {!! isset($harga['dewasa_non_residen']) ? $harga['dewasa_non_residen'] : "null" !!},
                anak_non_residen: {!! isset($harga['anak_non_residen']) ? $harga['anak_non_residen'] : "null" !!},
                balita_non_residen: {!! isset($harga['balita_non_residen']) ? $harga['balita_non_residen'] : "null" !!},
            }
        }

        function copyVal(){
            let dewasa_residen = document.getElementById("[0]dewasa_residen").value;
            let anak_residen = document.getElementById("[0]anak_residen").value;
            let balita_residen = document.getElementById("[0]balita_residen").value;
            
            let paket = {!! isset($packet) ? $packet: '[]' !!};
            let jumlah = paket.length;

            if(jumlah > 1){
                for(let i = 1;i < jumlah; i++){
                    let iterasi_dewasa_residen = document.getElementById("["+i+"]"+"dewasa_residen")
                    let iterasi_anak_residen = document.getElementById("["+i+"]"+"anak_residen")
                    let iterasi_balita_residen = document.getElementById("["+i+"]"+"balita_residen")

                    if(dewasa_residen != ' '){
                        console.log(dewasa_residen)
                        iterasi_dewasa_residen.value=dewasa_residen
                    }

                    if(anak_residen != ' '){
                        iterasi_anak_residen.value=anak_residen
                    }
                
                    if(balita_residen != ' '){
                        iterasi_balita_residen.value=balita_residen
                    }
                }
            }

        }

        document.getElementById("[0]dewasa_residen").addEventListener("change",function(){
            copyVal();    
        }); 

        document.getElementById("[0]anak_residen").addEventListener("change",function(){
            copyVal();    
        });

        document.getElementById("[0]balita_residen").addEventListener("change",function(){
            copyVal();    
        }); 

    </script>
    <script>
        function handler() {
                    
            return {
                fields: {!! isset($harga['discount_group']) ? $harga['discount_group'] : "[]" !!},
                addNewField() {
                    this.fields.push({
                        min_orang: '',
                        max_orang: '',
                        discount_orang: ''
                    });
                    // console.log(this.fields)
                },
                removeField(index) {
                    this.fields.splice(index, 1);
                    console.log(this.fields)
                }
            }
        }
    </script>

    <script>
        var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        $(document).ready(function() {
            $('#tgl_start').datepicker({
                change:function(e){
                    today:$('#tgl_start').datepicker().value();
                    $('#tgl_end').datepicker().destroy();
                    $('#tgl_end').datepicker({
                        minDate:today
                    })
                },
                minDate:today
            });
        })
    $(document).ready(function() {
        $('#tgl_end').datepicker({
            nimDate:today
        });
    })

    function sweet(msg){
            Swal.fire({
                icon: 'error',
                text: msg,
                customClass: 'swal-height'
            })   
    }

  function discountDate() {
            
        return {
            tgl_start: '',
            tgl_end: '',
            discount: 0,
            dewasa_residen: {!! isset($harga['dewasa_residen']) ? $harga['dewasa_residen'] : "null" !!},
            anak_residen: {!! isset($harga['anak_residen']) ? $harga['anak_residen'] : "null" !!},
            balita_residen: {!! isset($harga['balita_residen']) ? $harga['balita_residen'] : "null" !!},
            dewasa_non_residen: {!! isset($harga['dewasa_non_residen']) ? $harga['dewasa_non_residen'] : "null" !!},
            anak_non_residen: {!! isset($harga['anak_non_residen']) ? $harga['anak_non_residen'] : "null" !!},
            balita_non_residen: {!! isset($harga['balita_non_residen']) ? $harga['balita_non_residen'] : "null" !!},
            details: {!! isset($harga['availability']) ? $harga['availability'] : "[]" !!},
            addNewField() {
                
                if($('#tgl_start').val()==''){
                    sweet("Tgl start harap diisi")
                    return;
                }

                if($('#tgl_end').val()==''){
                    sweet("Tgl end harap diisi")
                    return;
                }

                // if($("#discount").val()==''){
                //     alert("diskon harap diisi")
                //     return;
                // }

                this.details.push({
                    tgl_start: $('#tgl_start').datepicker().value(),
                    tgl_end: $('#tgl_end').datepicker().value(),
                    discount: this.discount
                });
                console.log(this.details)
                this.tgl_start = ''
                this.tgl_end = ''
                this.discount = 0
            },
            removeField(index) {
                this.details.splice(index, 1);
                console.log(this.fields)
            }
        }
    }

    function discountDatePacket(){
        @php
            if(isset($harga['paket']['harga'])){
                $hargaPaket = $harga['paket']['harga'];
            }else{
                $hargaPaket = [];
            }
        
            // $packet = isset($packet) ? $packet :null;

        @endphp
        
        let paket = {!! isset($packet) ? $packet: "[]" !!};
        let availability ={!! isset($harga['availability']) ? $harga['availability'] : "[]" !!};
        let harga_paket = @json($hargaPaket)

        const newPaket =[];

        for(let i = 0; i < paket.length;i++){
            newPaket.push({
                'nama_paket':paket[i]['nama_paket'].replace(' ','_').toLowerCase()
            })
        }

        // <input type="text" name="paket" id="paketFieldInput">
        // <input type="text" name="harga_paket" id="hargaFieldInput">
        // var nama=newPaket[0].nama_paket
        console.log(availability);
        // alert('test');
        return{
            
            tgl_start: '',
            tgl_end: '',
            discount: 0,
            paket:newPaket,
            harga_paket:harga_paket,
            details: {!! isset($harga['availability']) ? $harga['availability'] : "[]" !!},

            addNewField() {
                
                if($('#tgl_start').val()==''){
                    sweet("Tgl start harap diisi")
                    return;
                }

                if($('#tgl_end').val()==''){
                    sweet("Tgl end harap diisi")
                    return;
                }

                // if($("#discount").val()==''){
                //     alert("diskon harap diisi")
                //     return;
                // }

                this.details.push({
                    tgl_start: $('#tgl_start').datepicker().value(),
                    tgl_end: $('#tgl_end').datepicker().value(),
                    discount: this.discount,
                    paket:paket,
                    harga_paket
                    
                });
                console.log(this.details)
                this.tgl_start = ''
                this.tgl_end = ''
                this.discount = 0
            },
            removeField(index) {
                this.details.splice(index, 1);
                console.log(this.fields)
            }
        }
    }

    function hargaPaket(harga, nama_paket, discount, jenis_harga){
        var nama = nama_paket.replace(/\s/g, '_');
        var obj = JSON.parse(JSON.stringify(harga));
        var harga;
        
        Object.keys(obj).forEach(function(key, index) {
            if(key==nama){
                const test = Object.entries(obj[key])

                for(let i=0; i<test.length;i++){
                    if(test[i][0]==jenis_harga){
                       console.log(test[i][1])
                       harga =test[i][1];
                    }
                }
            }
        });

        var diskon = parseInt(harga)+(parseInt(harga)*discount/100);

        return diskon
    }
    //
    </script>
</body>

</html>