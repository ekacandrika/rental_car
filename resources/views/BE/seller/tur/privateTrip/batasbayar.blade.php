<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> -->

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko>
            <x-slot name="addListingHotel">
                {{ route('hotelinfodasar') }}
            </x-slot>
            <x-slot name="faq">
                @if (!isset($data->faq))
                {{ route('hotelfaq') }}
                @else
                {{ route('hotelfaq.edit', $data->product_code) }}
                @endif
            </x-slot>

            {{-- Xstay --}}
            <x-slot name="addListingXstay">
                {{ route('infodasar') }}
            </x-slot>
            <x-slot name="faqXstay">
                @if (!isset($xstay))
                {{ route('faq.index') }}
                @else
                {{ route('faq.edit', $xstay->product_code) }}
                @endif
            </x-slot>
        </x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
                {{-- Breadcumbs --}}
                <nav class="flex" aria-label="Breadcrumb">
                    <ol class="inline-flex items-center space-x-1 md:space-x-3">
                        @if($isedit)
                        <li class="inline-flex items-center">
                            <a href="{{route('tur.viewAddListingEdit',$id)}}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                {{-- 'hover:text-[#23AEC1]':'text-[#5f5e5e] disabled' --}}
                                <a href="{{route('tur.viewItenenarydit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['itenerary']) ? ($tur['itenerary']!= '' ? 'text-[#333333] hover:text-[#23AEC1]' : 'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled'}}"
                                    data-role="disabled">Buat
                                    Itinerary</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewHargaEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['harga']) ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled'}}">Harga
                                    & Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewBatasEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1]">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPetaEdit',$id)}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['peta']) ? ($tur['peta']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled'):'text-[#5f5e5e] disabled'}}">Peta
                                    & Foto</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPilihanEkstraEdit',$id)}}" {{-- $tur['pilihan_ekstra']!=''
                                    ? ' hover:text-[#23AEC1]' :'text-[#5f5e5e] disabled' --}}
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['pilihan_ekstra']) ? ($tur['pilihan_ekstra']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled' }}">Pilihan
                                    & Ekstra</a>
                            </div>
                        </li>
                        @else
                        <li class="inline-flex items-center">
                            <a href="{{route('tur')}}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                                Informasi Dasar</a>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                {{-- 'hover:text-[#23AEC1]':'text-[#5f5e5e] disabled' --}}
                                <a href="{{route('tur.viewItenenary')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['itenerary']) ? ($tur['itenerary']!= '' ? 'text-[#333333] hover:text-[#23AEC1]' : 'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled'}}"
                                    data-role="disabled">Buat
                                    Itinerary</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewHarga')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['harga']) ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled'}}">Harga
                                    & Ketersediaan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewBatas')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1]">Batas
                                    Pembayaran & Pembatalan</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPeta')}}"
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['peta']) ? ($tur['peta']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled'):'text-[#5f5e5e] disabled'}}">Peta
                                    & Foto</a>
                            </div>
                        </li>
                        <li>
                            <div class="flex items-center">
                                <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{route('tur.viewPilihanEkstra')}}" {{-- $tur['pilihan_ekstra']!=''
                                    ? ' hover:text-[#23AEC1]' :'text-[#5f5e5e] disabled' --}}
                                    class="inline-flex items-center text-sm font-semibold font-inter {{isset($tur['pilihan_ekstra']) ? ($tur['pilihan_ekstra']!='' ? 'text-[#333333] hover:text-[#23AEC1]':'text-[#5f5e5e] disabled') : 'text-[#5f5e5e] disabled' }}">Pilihan
                                    & Ekstra</a>
                            </div>
                        </li>
                        @endif
                    </ol>
                </nav>
                @if($isedit)
                <form action=" {{ route('tur.addBatas.edit',$id) }}" method="POST" enctype="multipart/form-data">
                    @else
                    <form action=" {{ route('tur.addBatas') }}" method="POST" enctype="multipart/form-data">
                        @endif
                        @csrf
                        @method('PUT')
                        {{-- Section --}}
                        <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded">
                            <div class="flex justify-between">
                                <div class="text-lg font-bold font-inter text-[#9E3D64]">
                                    Batas Waktu Pembayaran & Pembatalan
                                </div>
                            </div>

                            <div class="flex justify-items-center items-center align-middle my-2">
                                <span>Batas waktu pembayaran </span>
                                <input type="text" id="batas_pembayaran" name="batas_pembayaran"
                                    class="bg-[#FFFFFF] mx-2 border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-12 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="61"
                                    value="{!! isset($batas['batas_pembayaran']) ? $batas['batas_pembayaran']:''!!}"
                                    required>
                                <span>hari sebelum keberangkatan.</span>
                            </div>

                            <p class="text-xs text-[#D50006]">*Batas Waktu Pembayaran Lebih Lama Daripada Batas Waktu
                                Pembatalan
                            </p>

                            <div class="my-5" x-data="handler()">
                                <p class="pb-1">Kebijakan Pembatalan:</p>
                                <button type="button" class="text-white bg-[#23AEC1] py-1 px-3 rounded-md"
                                    @click="addNewField()">Tambah</button>
                                @if ($errors->has('fields'))
                                <span class="text-red-500 font-bold">{{$errors->first('fields')}}</span>
                                {{-- <span class="text-rose-900">{{$errors->first('details')}}</span> --}}
                                @endif
                                {{-- Tabel --}}
                                <div class="my-7">
                                    <table class="my-5 w-1/2 border table-auto rounded-md">
                                        <thead>
                                            <tr>
                                                <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Hari Sebelumnya
                                                    Sampai
                                                </th>
                                                <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Hari Sebelumnya
                                                </th>
                                                <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Potongan (%)</th>
                                                <th class="p-3 border border-slate-500 bg-[#BDBDBD]"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <template x-if="fields.length < 1">
                                                <div class="px-3">
                                                    <p>Klik tombol Tambah di atas untuk menambahkan.</p>
                                                </div>
                                            </template>
                                            <template x-if="fields.length >= 1" class="col">
                                                <template x-for="(field, index) in fields" :key="index">
                                                    <tr class="align-middle">
                                                        <td class="p-3 border border-slate-500 text-center">
                                                            <input type="text" id="kebijakan_pembatalan_sebelumnya"
                                                                :name="'fields[' + index + '][kebijakan_pembatalan_sebelumnya]'"
                                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                placeholder="3"
                                                                x-model="field.kebijakan_pembatalan_sebelumnya"
                                                                required>
                                                        </td>
                                                        <td class="p-3 border border-slate-500 text-center">
                                                            <input type="text" id="kebijakan_pembatalan_sesudah"
                                                                :name="'fields[' + index + '][kebijakan_pembatalan_sesudah]'"
                                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                placeholder="5"
                                                                x-model="field.kebijakan_pembatalan_sesudah" required>
                                                        </td>
                                                        <td class=" p-3 border border-slate-500 text-center">
                                                            <input type="text" id="kebijakan_pembatalan_potongan"
                                                                :name="'fields[' + index + '][kebijakan_pembatalan_potongan]'"
                                                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                placeholder="5"
                                                                x-model="field.kebijakan_pembatalan_potongan" required>
                                                        </td>
                                                        <template x-if="index === (fields.length - 1)">
                                                            <td class="p-3 border border-slate-500 text-center">
                                                                <button class="mx-1"><img
                                                                        src="{{ asset('storage/icons/delete-dynamic-data.png') }}"
                                                                        alt="" width="16px"
                                                                        @click="removeField(index)"></button>
                                                            </td>
                                                        </template>
                                                        <template x-if="index !== (fields.length - 1)">
                                                            <td class="p-3 border border-slate-500 text-center">
                                                                <button class="mx-1">
                                                                    <img src="{{ asset('storage/icons/pencil-solid 1.svg') }}"
                                                                        alt="" width="16px" @click="addNewField()">
                                                                </button>
                                                                <button class="mx-1">
                                                                    <img src="{{ asset('storage/icons/trash-solid.svg') }}"
                                                                        alt="" width="16px" @click="removeField(index)">
                                                                </button>
                                                            </td>
                                                        </template>
                                                    </tr>
                                                </template>
                                            </template>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="p-5">
                                <div class="grid grid-cols-6">
                                    <div class="col-start-1 col-end-2">
                                        <a href="{{ route('tur.viewHarga') }}"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">Sebelumnya</a>

                                        {{-- <a href="{{route('tur')}}">
                                            <button type="submit"
                                                class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                                Sebelumnya
                                            </button>
                                        </a> --}}
                                    </div>
                                    <div class="col-start-6 col-end-7">
                                        <button type="submit"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                            Selanjutnya
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="p-5">
                                <div class="grid grid-cols-6">
                                    <div class="col-start-1 col-end-2">
                                        <a href="{{ route('tur.viewHarga') }}"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">Sebelumnya</a>
                                    </div>
                                    <div class="col-end-8 col-end-2">
                                        <button type="submit"
                                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                            Selanjutnya
                                        </button>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </form>
            </div>
        </div>
    </div>

    <script>
        function handler() {

            return {
                fields: {!! isset($batas['pembatalan']) ? $batas['pembatalan'] : '[]' !!},
                addNewField() {
                    this.fields.push({
                        kebijakan_pembatalan_sebelumnya: '',
                        kebijakan_pembatalan_sesudah: '',
                        kebijakan_pembatalan_potongan: ''
                    });
                },
                removeField(index) {
                    this.fields.splice(index, 1);
                }
            }
        }

        function checkIsEmptyArray(data) {
            console.log('data', data)
            if (!data) {
                return []
            }

            if (data) {
                return data
            }
        }
    </script>
</body>

</html>