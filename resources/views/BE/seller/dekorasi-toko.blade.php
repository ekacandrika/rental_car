<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    {{-- sweet alert --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.min.css" rel="stylesheet">
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        #plus {
            fill: #23AEC1;
        }

        #c_banner:hover {
            background: #E7F3F4;
        }

        .container {
            margin-top: 5px;
            margin-left: 15px;
            padding: 10px;
        }

        ul,
        ol,
        li {
            margin: 0;
            padding: 0;
            list-style: none;
        }

        .reorder_link {
            color: #3675B4;
            border: solid 2px #3675B4;
            text-transform: uppercase;
            background: #fff;
            font-size: 18px;
            padding: 10px 20px;
            font-weight: bold;
            text-decoration: none;
            transition: all 0.35s;
            -moz-transition: all 0.35s;
            -webkit-transition: all 0.35s;
            -o-transition: all 0.35s;
            white-space: nowrap;
        }

        .reorder_link:hover {
            color: #fff;
            border: solid 2px #3675B4;
            background: #3675B4;
            box-shadow: none;
        }

        #reorder-helper {
            margin: 18px 10px;
            padding: 10px;
        }

        .light_box {
            background: #efefef;
            padding: 20px;
            margin: 15px 0;
            text-align: center;
            font-size: 1.2em;
        }

        /* image gallery */
        .gallery {
            width: 100%;
            float: left;
            margin-top: 15px;
        }

        .gallery ul {
            list-style-type: none;
        }

        .gallery ul li {
            border: 2px solid #ccc;
            float: left;
        }

        /* notice box */
        .notice,
        .notice a {
            color: #fff !important;
        }

        .notice {
            z-index: 8888;
            padding: 10px;
            margin-top: 20px;
        }

        .notice a {
            font-weight: bold;
        }

        .notice_error {
            background: #E46360;
        }

        .notice_success {
            background: #657E3F;
        }
    </style>
    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.seller.navbar-seller></x-be.seller.navbar-seller>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko></x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-2xl font-bold font-inter text-[#333333]">Dekorasi Toko</span>

                @if (isset($toko))
                    <div class="w-full bg-white rounded shadow-lg transform duration-200 easy-in-out mt-5">
                        <div class="h-2/4 sm:h-64 overflow-hidden">
                            <img class="w-full rounded-t" src="{!! isset($toko['banner_toko']) ? asset($toko['banner_toko']) : asset('storage/img/blank-logo.png') !!}" alt="Thumbnail Toko" />
                        </div>
                        <div class="flex justify-start px-5 -mt-12">
                            <span clspanss="block relative h-32 w-32">
                                <img alt="Logo Toko" src="{!! isset($toko['logo_toko']) ? asset($toko['logo_toko']) : asset('storage/img/blank-logo.png') !!}"
                                    class="mx-auto object-cover rounded-full h-24 w-24 bg-white p-1" />
                            </span>
                            <div class="px-5 my-12 mb-8">
                                <h2 class="text-3xl font-bold text-[#333333]">{!! $toko['nama_toko'] !!}</h2>
                                <p class="text-gray-600 mt-px">{!! $toko['nama_banner'] !!}</p>
                            </div>
                        </div>

                        <div class="-mt-32 absolute mr-5 right-0 ">
                            <div class="">
                                <div class="mt-2">
                                    {{-- <a id="c_banner" href="/dashboard/kelola-toko/dekor-toko/edit/{{ $toko->id }}" --}}
                                    {{--  --}}
                                    <a id="c_banner" href="{{ route('kelolatoko.viewEditDekorasiToko',$toko->id ) }}"
                                        class="text-[#23AEC1] font-bold bg-white focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-full border border-[#23AEC1] text-sm px-4 py-4 text-center inline-flex items-center mr-2">
                                        <svg width="20" height="20" viewBox="0 0 50 50" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <g clip-path="url(#clip0_2926_20837)">
                                                <path
                                                    d="M46.0547 2.11914C43.916 -0.0195313 40.459 -0.0195313 38.3203 2.11914L35.3809 5.04883L44.9414 14.6094L47.8809 11.6699C50.0195 9.53125 50.0195 6.07422 47.8809 3.93555L46.0547 2.11914ZM16.8359 23.6035C16.2402 24.1992 15.7812 24.9316 15.5176 25.7422L12.627 34.4141C12.3438 35.2539 12.5684 36.1816 13.1934 36.8164C13.8184 37.4512 14.7461 37.666 15.5957 37.3828L24.2676 34.4922C25.0684 34.2188 25.8008 33.7695 26.4062 33.1738L42.7441 16.8262L33.1738 7.25586L16.8359 23.6035V23.6035ZM9.375 6.25C4.19922 6.25 0 10.4492 0 15.625V40.625C0 45.8008 4.19922 50 9.375 50H34.375C39.5508 50 43.75 45.8008 43.75 40.625V31.25C43.75 29.5215 42.3535 28.125 40.625 28.125C38.8965 28.125 37.5 29.5215 37.5 31.25V40.625C37.5 42.3535 36.1035 43.75 34.375 43.75H9.375C7.64648 43.75 6.25 42.3535 6.25 40.625V15.625C6.25 13.8965 7.64648 12.5 9.375 12.5H18.75C20.4785 12.5 21.875 11.1035 21.875 9.375C21.875 7.64648 20.4785 6.25 18.75 6.25H9.375Z"
                                                    fill="#23AEC1" />
                                            </g>
                                            <defs>
                                                <clipPath id="clip0_2926_20837">
                                                    <rect width="50" height="50" fill="white" />
                                                </clipPath>
                                            </defs>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="mt-5">
                        <div class="flex p-4 mb-4 text-sm text-red-800 border border-red-300 rounded-lg bg-red-50"
                            role="alert">
                            <svg aria-hidden="true" class="flex-shrink-0 inline w-5 h-5 mr-3" fill="currentColor"
                                viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            <span class="sr-only">Info</span>
                            <div>
                                <span class="font-bold">Anda belum membuat toko! </span> Klik tombol + untuk membuat
                                toko.
                            </div>
                        </div>

                        <div class="w-full bg-white rounded shadow-lg transform duration-200 easy-in-out mt-5">
                            <div class="h-2/4 sm:h-64 overflow-hidden">
                                <div
                                    class="flex items-center justify-center h-64 mb-4 bg-gray-300 rounded dark:bg-gray-700">
                                    <svg class="w-12 h-12 text-gray-200 dark:text-gray-600"
                                        xmlns="http://www.w3.org/2000/svg" aria-hidden="true" fill="currentColor"
                                        viewBox="0 0 640 512">
                                        <path
                                            d="M480 80C480 35.82 515.8 0 560 0C604.2 0 640 35.82 640 80C640 124.2 604.2 160 560 160C515.8 160 480 124.2 480 80zM0 456.1C0 445.6 2.964 435.3 8.551 426.4L225.3 81.01C231.9 70.42 243.5 64 256 64C268.5 64 280.1 70.42 286.8 81.01L412.7 281.7L460.9 202.7C464.1 196.1 472.2 192 480 192C487.8 192 495 196.1 499.1 202.7L631.1 419.1C636.9 428.6 640 439.7 640 450.9C640 484.6 612.6 512 578.9 512H55.91C25.03 512 .0006 486.1 .0006 456.1L0 456.1z" />
                                    </svg>
                                </div>
                            </div>
                            <div class="flex justify-start px-5 -mt-12">
                                <span clspanss="block relative h-32 w-32">
                                    <img alt="Logo Toko" src="{{ asset('storage/img/blank-logo.png') }}"
                                        class="mx-auto object-cover rounded-full h-24 w-24 bg-white p-1" />
                                </span>
                                <div class="px-5 my-12 mb-8">
                                    <div class="h-5 bg-gray-800 rounded-full w-32 my-2.5"></div>
                                    <div class="w-48 h-5 bg-gray-600 rounded-full"></div>
                                </div>
                            </div>

                            <div class="-mt-32 absolute mr-5 right-0 ">
                                <div class="">
                                    <div class="mt-2">
                                        <a id="c_banner" href="{{ route('kelolatoko.DekorasiToko') }}"
                                            class="text-[#23AEC1] font-bold bg-white focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-full border border-[#23AEC1] text-sm px-4 py-4 text-center inline-flex items-center mr-2">
                                            <svg id="plus" width="20" height="20" viewBox="0 0 36 36"
                                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M20.1301 6.28399C20.1301 5.10573 19.1782 4.15381 18 4.15381C16.8217 4.15381 15.8698 5.10573 15.8698 6.28399V15.8698H6.28399C5.10573 15.8698 4.15381 16.8217 4.15381 18C4.15381 19.1782 5.10573 20.1301 6.28399 20.1301H15.8698V29.7159C15.8698 30.8942 16.8217 31.8461 18 31.8461C19.1782 31.8461 20.1301 30.8942 20.1301 29.7159V20.1301H29.7159C30.8942 20.1301 31.8461 19.1782 31.8461 18C31.8461 16.8217 30.8942 15.8698 29.7159 15.8698H20.1301V6.28399Z" />
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if (isset($allbanner))
                    <div class="my-5">
                        <div class="col-span-1">
                            <div class="mt-5">
                                <a id="c_banner" href="{{ route('kelolatoko.bannerToko') }}"
                                    class="text-[#23AEC1] font-bold bg-white focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg border border-[#23AEC1] text-sm px-5 py-2.5 text-center inline-flex items-center mr-2">
                                    <svg width="20" height="20" viewBox="0 0 50 50" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <g clip-path="url(#clip0_2926_20837)">
                                            <path
                                                d="M46.0547 2.11914C43.916 -0.0195313 40.459 -0.0195313 38.3203 2.11914L35.3809 5.04883L44.9414 14.6094L47.8809 11.6699C50.0195 9.53125 50.0195 6.07422 47.8809 3.93555L46.0547 2.11914ZM16.8359 23.6035C16.2402 24.1992 15.7812 24.9316 15.5176 25.7422L12.627 34.4141C12.3438 35.2539 12.5684 36.1816 13.1934 36.8164C13.8184 37.4512 14.7461 37.666 15.5957 37.3828L24.2676 34.4922C25.0684 34.2188 25.8008 33.7695 26.4062 33.1738L42.7441 16.8262L33.1738 7.25586L16.8359 23.6035V23.6035ZM9.375 6.25C4.19922 6.25 0 10.4492 0 15.625V40.625C0 45.8008 4.19922 50 9.375 50H34.375C39.5508 50 43.75 45.8008 43.75 40.625V31.25C43.75 29.5215 42.3535 28.125 40.625 28.125C38.8965 28.125 37.5 29.5215 37.5 31.25V40.625C37.5 42.3535 36.1035 43.75 34.375 43.75H9.375C7.64648 43.75 6.25 42.3535 6.25 40.625V15.625C6.25 13.8965 7.64648 12.5 9.375 12.5H18.75C20.4785 12.5 21.875 11.1035 21.875 9.375C21.875 7.64648 20.4785 6.25 18.75 6.25H9.375Z"
                                                fill="#23AEC1" />
                                        </g>
                                        <defs>
                                            <clipPath id="clip0_2926_20837">
                                                <rect width="50" height="50" fill="white" />
                                            </clipPath>
                                        </defs>
                                    </svg>

                                    <p class="ml-2">Edit Banner</p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="my-5">
                        <div class="pt-5">
                            <a href="javascript:void(0);" class="reorder_link rounded-md" id="saveReorder">reorder
                                photos</a>
                            <div id="reorderHelper" class="light_box mt-10" style="display:none;">1. Drag photos to
                                reorder.<br>2. Click 'Save Reordering' when finished.</div>
                            <div class="gallery w-full">
                                <ul class="reorder_ul reorder-photos-list">
                                    @if ($bannerone)
                                    @foreach ($bannerone as $one)
                                        <li id="image_li_{{ $one['id'] }}" class="ui-sortable-handle">
                                            <a href="javascript:void(0);" style="float:none;" class="image_link">
                                                <div class="relative">
                                                    <img class="rounded-lg w-[1080px] h-auto"
                                                        src="{{ asset($one['banner']) }}" alt="">
                                                    <button class="absolute mx-2 translate-x-12 -translate-y-14 " style="top:81px;right:81px;" onclick="removeBanner('{{$one['id']}}')">
                                                        <img src="{{asset('storage/icons/circle-trash-solid.svg')}}" width="25px">
                                                    </button>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                    @endif
                                    
                                    @if ($bannertwo)
                                    @foreach ($bannertwo as $two)
                                        <li id="image_li_{{ $two['id'] }}" class="ui-sortable-handle">
                                            <a href="javascript:void(0);" style="float:none;" class="image_link">
                                                <div class="relative">
                                                    <img class="rounded-lg w-[314px] h-auto"
                                                        src="{{ asset($two['banner']) }}" alt="">
                                                    <button class="absolute mx-2 translate-x-12 -translate-y-14" style="top:72px;right:72px;" onclick="removeBanner('{{$two['id']}}')">
                                                        <img src="{{asset('storage/icons/circle-trash-solid.svg')}}" width="25px">
                                                    </button>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                    @endif

                                    @if ($bannerthree)
                                    @foreach ($bannerthree as $three)
                                        <li id="image_li_{{ $three['id'] }}" class="ui-sortable-handle">
                                            <a href="javascript:void(0);" style="float:none;" class="image_link">
                                                <div class="relative">
                                                    <img class="rounded-lg w-[314px] h-auto"
                                                    src="{{ asset($three['banner']) }}" alt="">
                                                    <button class="absolute mx-2 translate-x-12 -translate-y-14" style="top:72px;right:72px;" onclick="removeBanner('{{$three['id']}}')">
                                                        <img src="{{asset('storage/icons/circle-trash-solid.svg')}}" width="25px">
                                                    </button>
                                                </div>                                                
                                            </a>
                                        </li>
                                    @endforeach
                                    @endif

                                    @if ($bannerfour)
                                    @foreach ($bannerfour as $four)
                                        <li id="image_li_{{ $four['id'] }}" class="ui-sortable-handle">
                                            <a href="javascript:void(0);" style="float:none;" class="image_link">
                                                <div class="relative">
                                                    <img class="rounded-lg w-[314px] h-auto"
                                                        src="{{ asset($four['banner']) }}" alt="">
                                                    <button class="absolute mx-2 translate-x-12 -translate-y-14" style="top:72px;right:72px;" onclick="removeBanner('{{$four['id']}}')">
                                                        <img src="{{asset('storage/icons/circle-trash-solid.svg')}}" width="25px">
                                                    </button> 
                                                </div>   
                                            </a>
                                        </li>
                                    @endforeach
                                    @endif

                                    @if ($bannerfive)
                                    @foreach ($bannerfive as $five)
                                        <li id="image_li_{{ $five['id'] }}" class="ui-sortable-handle">
                                            <div class="relative">
                                                <a href="javascript:void(0);" style="float:none;" class="image_link">
                                                    <img class="rounded-lg w-[512px] h-auto"
                                                        src="{{ asset($five['banner']) }}" alt="">
                                                </a>
                                                <button class="absolute mx-2 translate-x-12 -translate-y-14" style="top:81px;right:81px;" onclick="removeBanner('{{$five['id']}}')">
                                                        <img src="{{asset('storage/icons/circle-trash-solid.svg')}}" width="25px">
                                                </button>
                                            </div>
                                        </li>
                                    @endforeach
                                    @endif
                                     @if ($bannersix)
                                    @foreach ($bannersix as $six)
                                        <li id="image_li_{{ $six['id'] }}" class="ui-sortable-handle">
                                            <div class="relative">
                                                <a href="javascript:void(0);" style="float:none;" class="image_link">
                                                    <img class="rounded-lg w-[512px] h-auto"
                                                        src="{{ asset($six['banner']) }}" alt="">
                                                </a>
                                                <button class="absolute mx-2 translate-x-12 -translate-y-14" style="top:81px;right:81px;" onclick="removeBanner({{$six['id']}})">
                                                        <img src="{{asset('storage/icons/circle-trash-solid.svg')}}" width="25px">
                                                </button>
                                            </div>
                                        </li>
                                    @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="my-5">
                        <div class="col-span-1">
                            <div class="mt-5">
                                <a id="c_banner" href="{{ route('kelolatoko.bannerToko') }}"
                                    class="text-[#23AEC1] font-bold bg-white focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg border border-[#23AEC1] text-sm px-5 py-2.5 text-center inline-flex items-center mr-2">
                                    <svg id="plus" width="20" height="20" viewBox="0 0 36 36"
                                        fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M20.1301 6.28399C20.1301 5.10573 19.1782 4.15381 18 4.15381C16.8217 4.15381 15.8698 5.10573 15.8698 6.28399V15.8698H6.28399C5.10573 15.8698 4.15381 16.8217 4.15381 18C4.15381 19.1782 5.10573 20.1301 6.28399 20.1301H15.8698V29.7159C15.8698 30.8942 16.8217 31.8461 18 31.8461C19.1782 31.8461 20.1301 30.8942 20.1301 29.7159V20.1301H29.7159C30.8942 20.1301 31.8461 19.1782 31.8461 18C31.8461 16.8217 30.8942 15.8698 29.7159 15.8698H20.1301V6.28399Z" />
                                    </svg>
                                    <p class="ml-2">Buat Banner</p>
                                </a>
                            </div>
                        </div>
                    </div>

                @endif

            </div>
        </div>
    </div>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function removeBanner(id){
            //alert(id)
            let route = "{{route('delete.banner',':id')}}"
            route = route.replace(':id',id);
            $.ajax({
                url:`${route}`,
                type:'DELETE',
                cache:'false',
                data:{
                    '_token':$("meta[name='csrf-token']").attr("content")
                },
                success:function(resp){
                    console.log(resp)
                            
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: resp.status,
                        title: resp.message
                    }).then(function(){
                        location.reload();
                    })
                },
                fail:function(data){
                    console.log('error:',data);
                }
            })
        }

        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                    this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                    if (!event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.reorder_link').on('click', function() {
                $("ul.reorder-photos-list").sortable({
                    tolerance: 'pointer'
                });
                $('.reorder_link').html('save reordering');
                $('.reorder_link').attr("id", "saveReorder");
                $('#reorderHelper').slideDown('slow');
                $('.image_link').attr("href", "javascript:void(0);");
                $('.image_link').css("cursor", "move");

                $("#saveReorder").click(function(e) {
                    if (!$("#saveReorder i").length) {
                        $(this).html('').prepend('<img src="images/refresh-animated.gif"/>');
                        $("ul.reorder-photos-list").sortable('destroy');
                        $("#reorderHelper").html(
                            "Reordering Photos - This could take a moment. Please don't navigate away from this page."
                            ).removeClass('light_box').addClass('notice notice_error');

                        var h = [];
                        $("ul.reorder-photos-list li").each(function() {
                            h.push($(this).attr('id').substr(9));
                        });

                        $.ajax({
                            method: "POST",
                            url: "/dashboard/kelola-toko/reorder-banner",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                'data': h
                            },
                            success: function() {
                                window.location.reload();
                            }
                        });
                        return false;
                    }
                    e.preventDefault();
                });
            });
        });
    </script>
</body>

</html>
