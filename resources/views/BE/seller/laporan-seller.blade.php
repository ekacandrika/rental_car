<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        /* BE Dashboard Navbar */
        #akun:checked+#akun {
            display: block;
        }

        .menu-dropdown li a {
            display: inline-block;
            color: white;
            /* text-align: center; */
            text-decoration: none;
        }

        .menu-dropdown li a:hover {
            background-color: none;
        }

        li.dropdown {
            display: inline-block;
            margin-right: 80px
        }

        .dropdown:hover .isi-dropdown {
            display: block;
        }

        .isi-dropdown a:hover {
            color: #fff !important;
            width: 100%;
        }

        .isi-dropdown {
            position: absolute;
            display: none;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
            background-color: #f9f9f9;
            width: 100%;
        }

        .isi-dropdown a {
            color: #3c3c3c !important;
            padding: 1%;
        }

        .isi-dropdown a:hover {
            color: #232323 !important;
            background: #f3f3f3 !important;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.seller.navbar-seller></x-be.seller.navbar-seller>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        <x-be.seller.sidebar-seller></x-be.seller.sidebar-seller>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-sm font-bold font-inter text-[#000000]">Tanggal bergabung:
                    {{ isset(auth()->user()->created_at) ? date('j F Y', strtotime(auth()->user()->created_at)) : '17
                    Agustus 2022' }}
                </span>
                {{-- Table --}}
                <div class="overflow-x-auto relative mt-5">
                    <table class="w-full text-sm text-left border-2 border-[#333333]">
                        <thead>
                            <tr class="bg-[#9E3D64]">
                                <th scope="row" class="py-3 text-center text-[#fff]">
                                    No Booking
                                </th>
                                <th scope="row" class="py-3 text-center text-[#fff]">
                                    Kategori
                                </th>
                                <th scope="row" class="py-3 text-center text-[#fff]">
                                    Judul
                                </th>
                                <th scope="row" class="py-3 text-center text-[#fff]">
                                    ID
                                </th>
                                <th scope="row" class="py-3 text-center text-[#fff]">
                                    Tanggal
                                </th>
                                <th scope="row" class="py-3 text-center text-[#fff]">
                                    Rupiah
                                </th>
                                <th scope="row" class="py-3 text-center text-[#fff]">
                                    Status
                                </th>
                                <th scope="row" class="py-3 text-center text-[#fff]">
                                    Bukti Bayar
                                </th>
                                <th scope="row" class="py-3 text-center text-[#fff]">
                                    Tanda Terima
                                </th>
                                <th scope="row" class="py-3 text-center text-[#fff]">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($data_pengajuan as $item)
                            @php
                            $produk = App\Models\Product::where('id', $item->product_id)->first();
                            $pajak_global = App\Models\pajakAdmin::first();
                            $pajak_global_result = intval($pajak_global->komisi) / 100;
                            $pajak_local = App\Models\pajakLocal::where('user_id', auth()->user()->id)->first();
                            $agent = App\Models\User::where('id', $item->agent_id)->first();
                            @endphp
                            <tr class="bg-white border-b-2 border-[#333333]">
                                <td scope="row" class="py-3 text-center">
                                    {{ $item->booking_code }}
                                </td>
                                <td class="py-3 text-center">
                                    {{ $produk->type }}
                                </td>
                                <td class="py-3 text-center">
                                    <div class="flex">
                                        <img src="{{ asset('storage/icons/arrow-up-solid.svg') }}"
                                            class="w-[15px] h-[16px]" alt="user" title="user"> &nbsp;
                                        {{ $produk->product_name }}
                                    </div>
                                    <a href="{{ route('viewUploadDiscovery', $item->id) }}" class="flex">
                                        <img src="{{ asset('storage/icons/folder-solid.svg') }}"
                                            class="w-[15px] h-[16px]" alt="user" title="user"> &nbsp;
                                        Discovery
                                    </a>
                                    @if ($agent != null)
                                    <div class="flex">
                                        <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}"
                                            class="w-[15px] h-[16px]" alt="user" title="user"> &nbsp;
                                        <b>Dari Agent</b>
                                    </div>
                                    @endif
                                </td>
                                <td class="py-3 text-center">
                                    {{ $produk->id }}
                                </td>
                                <td class="py-3 text-center">
                                    {{ date('j F Y', strtotime($item->tgl_checkout)) }}
                                </td>
                                <td class="py-3 text-center">
                                    @if ($pajak_local == null)
                                    @php
                                    $hasil = intval($item->total_price) * $pajak_global_result;
                                    @endphp
                                    {{ number_format(intval($item->total_price - $hasil)) }}
                                    @else
                                    @if ($pajak_local->komisiLocal == null)
                                    @php
                                    $hasil = intval($item->total_price) * $pajak_global_result;
                                    @endphp
                                    {{ number_format(intval($item->total_price - $hasil)) }}
                                    @else
                                    @if ($pajak_local->statusKomisiLocal == 'percent')
                                    @php
                                    $val = intval($pajak_local->komisiLocal) / 100;
                                    $ab = intval($item->total_price) * $val;
                                    @endphp
                                    {{ number_format($item->total_price - $ab) }}
                                    @else
                                    {{ number_format($item->total_price - $pajak_local->komisiLocal) }}
                                    @endif
                                    @endif
                                    @endif
                                </td>
                                <td class="py-3 text-center">
                                    {{ $item->status }}
                                </td>
                                <td class="py-3 text-center">
                                    <div class="flex">
                                        <a href="" class="mx-auto">
                                            <img src="{{ asset('storage/icons/folder-solid.svg') }}"
                                                class="w-[15px] h-[16px]" alt="user" title="user">
                                        </a>
                                    </div>
                                </td>
                                <td class="py-3 text-center">
                                    <div class="flex">
                                        <a href="{{ route('viewUploadTandaTerima', $item->id) }}" class="mx-auto">
                                            <img src="{{ asset('storage/icons/folder-solid.svg') }}"
                                                class="w-[15px] h-[16px]" alt="user" title="user">
                                        </a>
                                    </div>
                                </td>
                                <td class="py-3 text-center">
                                    <form action="{{ route('store.datapengajuan', $item->id) }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="pengajuan_id" value="{{ $item->id }}">
                                        <input type="hidden" name="tgl_pengajuan" value="{{ date('Y-m-d') }}">
                                        @if ($pajak_local == null)
                                        @php
                                        $hasil = intval($item->total_price) * $pajak_global_result;
                                        @endphp
                                        <input type="hidden" name="nominal"
                                            value="{{ intval($item->total_price - $hasil) }}">
                                        @else
                                        @if ($pajak_local->komisiLocal == null)
                                        @php
                                        $hasil = intval($item->total_price) * $pajak_global_result;
                                        @endphp
                                        <input type="hidden" name="nominal"
                                            value="{{ intval($item->total_price - $hasil) }}">
                                        @else
                                        @if ($pajak_local->statusKomisiLocal == 'percent')
                                        @php
                                        $val = intval($pajak_local->komisiLocal) / 100;
                                        $ab = intval($item->total_price) * $val;
                                        @endphp
                                        <input type="hidden" name="nominal" value="{{ $item->total_price - $ab }}">
                                        <input type="hidden" name="komisi" value="{{ $ab }}">
                                        <input type="hidden" name="type" value="{{ $produk->type }}">
                                        @else
                                        <input type="hidden" name="nominal"
                                            value="{{ $item->total_price - $pajak_local->komisiLocal }}">
                                        <input type="hidden" name="komisi" value="{{ $pajak_local->komisiLocal }}">
                                        <input type="hidden" name="type" value="{{ $produk->type }}">
                                        @endif
                                        @endif
                                        @endif
                                        <button type="submit"
                                            class="items-center px-2 py-2 text-sm font-medium text-center text-white bg-[#9E3D64] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                            Ajukan
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="12" style="text-align: center"><i>Data Kosong !</i>
                                </td>
                            </tr>
                            @endforelse
                            {{-- <tr class="bg-white border-2 border-[#333333]">
                                <td scope="row" class="py-3 text-center">
                                    TO9802022023</td>
                                <td class="py-3 text-center">
                                    Nama Kategori
                                </td>
                                <td class="py-3 text-center">
                                    <div class="flex">
                                        <img src="{{ asset('storage/icons/arrow-up-solid.svg') }}"
                                            class="w-[15px] h-[16px]" alt="user" title="user"> &nbsp;
                                        Tur Bali
                                    </div>
                                    <a href="" class="flex">
                                        <img src="{{ asset('storage/icons/folder-solid.svg') }}"
                                            class="w-[15px] h-[16px]" alt="user" title="user"> &nbsp;
                                        Discovery
                                    </a>
                                </td>
                                <td class="py-3 text-center">
                                    ID
                                </td>
                                <td class="py-3 text-center">
                                    25 Agustus 2023
                                </td>
                                <td class="py-3 text-center">
                                    40,000,000
                                </td>
                                <td class="py-3 text-center">
                                    Sudah Bayar
                                </td>
                                <td class="py-3 text-center">
                                    <div class="flex">
                                        <a href="" class="mx-auto">
                                            <img src="{{ asset('storage/icons/folder-solid.svg') }}"
                                                class="w-[15px] h-[16px]" alt="user" title="user">
                                        </a>
                                    </div>
                                </td>
                                <td class="py-3 text-center">
                                    Tanda Terima
                                </td>
                                <td class="py-3 text-center">
                                    <button type="submit"
                                        class="items-center px-2 py-2 text-sm font-medium text-center text-white bg-[#9E3D64] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                        Ajukan
                                    </button>
                                </td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</body>

</html>