<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);
        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity:1;
            background-color: rgba(249,250,251,var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }
        #transfer:checked+#transfer {
            display: block;
        }
        #hotel:checked+#hotel {
            display: block;
        }
        #rental:checked+#rental {
            display: block;
        }
        #activity:checked+#activity {
            display: block;
        }
        #xstay:checked+#xstay {
            display: block;
        }
        
        .container{
    margin-top:5px;
    margin-left:15px;
    padding: 10px;
}
ul, ol, li {
    margin: 0;
    padding: 0;
    list-style: none;
}
.reorder_link {
    color: #3675B4;
    border: solid 2px #3675B4;
    text-transform: uppercase;
    background: #fff;
    font-size: 18px;
    padding: 10px 20px;
    font-weight: bold;
    text-decoration: none;
    transition: all 0.35s;
    -moz-transition: all 0.35s;
    -webkit-transition: all 0.35s;
    -o-transition: all 0.35s;
    white-space: nowrap;
}
.reorder_link:hover {
    color: #fff;
    border: solid 2px #3675B4;
    background: #3675B4;
    box-shadow: none;
}
#reorder-helper{
    margin: 18px 10px;
    padding: 10px;
}
.light_box {
    background: #efefef;
    padding: 20px;
    margin: 15px 0;
    text-align: center;
    font-size: 1.2em;
}

/* image gallery */
.gallery{
    width:100%;
    float:left;
    margin-top:15px;
}
.gallery ul{
    list-style-type:none;
}
.gallery ul li{
    border:2px solid #ccc;
    float:left;
}

/* notice box */
.notice, .notice a{
    color: #fff !important;
}
.notice {
    z-index: 8888;
    padding: 10px;
    margin-top: 20px;
}
.notice a {
    font-weight: bold;
}
.notice_error {
    background: #E46360;
}
.notice_success {
    background: #657E3F;
}

    </style>
    <!-- Styles -->
    @livewireStyles   
</head>
<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.seller.navbar-seller></x-be.seller.navbar-seller>   
    
    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko></x-be.kelolatoko.sidebar-toko>
        
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-2xl font-bold font-inter text-[#333333]">Dekorasi Toko</span>

                <div class="bg-white my-5 rounded-md shadow-lg">
                    <div class="grid grid-cols-4">
                        <div class="ml-5 p-6">
                            <p class="text-lg font-bold">Logo Toko</p>                            
                            <div class="flex">
                                <img src="{!! isset($toko['logo_toko']) ? asset($toko['logo_toko']) : asset('storage/img/blank-logo.png') !!}" class="bg-clip-content lg:w-[96px] lg:h-[96px] xl:w-[100px] xl:h-[100px] shadow-xl bg-white rounded-full" alt="store-profile">
                            </div>
                        </div>

                        <div class="col-start-2 col-span-2">                            
                            <div class="py-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Nama Toko</label>
                                <input type="text" x-model="nama_toko" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" placeholder="Nama Toko" readonly value="{{isset($toko['nama_toko']) ? $toko['nama_toko'] : 'Belum'}}">
                            </div>
                        </div>

                        <div class="col-span-1">
                            <div class="py-12 px-10 mt-px ml-12">
                                <a href="{{ route('kelolatoko.DekorasiToko') }}" class="text-[#23AEC1] bg-white focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg border border-[#23AEC1] hover:border-[#872F52] text-sm px-5 py-2.5 text-center inline-flex items-center mr-2">
                                <svg class="mr-2" width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0_672_20183)">
                                        <path d="M16.5797 1.26104C15.8098 0.491113 14.5652 0.491113 13.7953 1.26104L12.7371 2.31572L16.1789 5.75752L17.2371 4.69932C18.007 3.92939 18.007 2.68486 17.2371 1.91494L16.5797 1.26104ZM6.06094 8.99541C5.84648 9.20986 5.68125 9.47354 5.58633 9.76533L4.5457 12.8872C4.44375 13.1896 4.52461 13.5235 4.74961 13.7521C4.97461 13.9806 5.30859 14.0579 5.61445 13.956L8.73633 12.9153C9.02461 12.8169 9.28828 12.6552 9.50625 12.4407L15.3879 6.55557L11.9426 3.11025L6.06094 8.99541V8.99541ZM3.375 2.74814C1.51172 2.74814 0 4.25986 0 6.12314V15.1231C0 16.9864 1.51172 18.4981 3.375 18.4981H12.375C14.2383 18.4981 15.75 16.9864 15.75 15.1231V11.7481C15.75 11.1259 15.2473 10.6231 14.625 10.6231C14.0027 10.6231 13.5 11.1259 13.5 11.7481V15.1231C13.5 15.7454 12.9973 16.2481 12.375 16.2481H3.375C2.75273 16.2481 2.25 15.7454 2.25 15.1231V6.12314C2.25 5.50088 2.75273 4.99814 3.375 4.99814H6.75C7.37226 4.99814 7.875 4.49541 7.875 3.87314C7.875 3.25088 7.37226 2.74814 6.75 2.74814H3.375Z" fill="#23AEC1"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_672_20183">
                                            <rect width="18" height="18" fill="#ffffff" transform="translate(0 0.5)"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                                    Edit
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="text-sm font-medium text-justify sm:text-left my-5 px-3 rounded-md bg-white rounded-md shadow-lg" x-data="{ open: false }">
                    <div class="flex justify-between mx-5 " :class=" { 'pt-3 pb-2': open, 'py-3': !open }">
                        <button class="text-lg font-bold text-[#333333] w-full text-left rounded-lg" x-on:click="open = !open">{!! isset($toko['nama_banner']) ? $toko['nama_banner'] : 'Nama Banner' !!}</button>
                        <img class="float-right duration-200 cursor-pointer" :class="{ 'rotate-180': open }" x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}" alt="chevron-down" width="26px" height="15px">
                    </div>                    
                    <div x-show="open" x-transition>
                        <div class="m-5">
                            <img class="rounded-lg h-auto w-[500px]" src="{!! isset($toko['nama_toko']) ? asset($toko['banner_toko']) : 'https://via.placeholder.com/800x300' !!}" alt="">
                            <div class="my-2.5 grid grid-cols-6 items-center">
                                <div class="col-start-1 col-span-4">
                                    <p class="text-sm font-bold mb-5 ">Tautan: <b class="italic text-[#9E3D64]">https://www.initautan.com/gambar.jpg</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="my-5">
                    <div class="col-span-1">
                        <div class="mt-5">
                            <a href="{{ route('kelolatoko.bannerToko') }}" class="text-[#23AEC1] bg-white focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg border border-[#23AEC1] hover:border-[#872F52] text-sm px-5 py-2.5 text-center inline-flex items-center mr-2">
                            <svg class="mr-2" width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0_672_20183)">
                                    <path d="M16.5797 1.26104C15.8098 0.491113 14.5652 0.491113 13.7953 1.26104L12.7371 2.31572L16.1789 5.75752L17.2371 4.69932C18.007 3.92939 18.007 2.68486 17.2371 1.91494L16.5797 1.26104ZM6.06094 8.99541C5.84648 9.20986 5.68125 9.47354 5.58633 9.76533L4.5457 12.8872C4.44375 13.1896 4.52461 13.5235 4.74961 13.7521C4.97461 13.9806 5.30859 14.0579 5.61445 13.956L8.73633 12.9153C9.02461 12.8169 9.28828 12.6552 9.50625 12.4407L15.3879 6.55557L11.9426 3.11025L6.06094 8.99541V8.99541ZM3.375 2.74814C1.51172 2.74814 0 4.25986 0 6.12314V15.1231C0 16.9864 1.51172 18.4981 3.375 18.4981H12.375C14.2383 18.4981 15.75 16.9864 15.75 15.1231V11.7481C15.75 11.1259 15.2473 10.6231 14.625 10.6231C14.0027 10.6231 13.5 11.1259 13.5 11.7481V15.1231C13.5 15.7454 12.9973 16.2481 12.375 16.2481H3.375C2.75273 16.2481 2.25 15.7454 2.25 15.1231V6.12314C2.25 5.50088 2.75273 4.99814 3.375 4.99814H6.75C7.37226 4.99814 7.875 4.49541 7.875 3.87314C7.875 3.25088 7.37226 2.74814 6.75 2.74814H3.375Z" fill="#23AEC1"/>
                                </g>
                                <defs>
                                    <clipPath id="clip0_672_20183">
                                        <rect width="18" height="18" fill="#ffffff" transform="translate(0 0.5)"/>
                                    </clipPath>
                                </defs>
                            </svg>
                                Tambah Banner
                            </a>
                        </div>
                    </div>
                </div>

                <div class="my-5">
                    <div class="pt-5">
                        <a href="javascript:void(0);" class="reorder_link rounded-md" id="saveReorder">reorder photos</a>
                        <div id="reorderHelper" class="light_box mt-10" style="display:none;">1. Drag photos to reorder.<br>2. Click 'Save Reordering' when finished.</div>
                        <div class="gallery w-full">
                            <ul class="reorder_ul reorder-photos-list">
                                @if (isset($allbanner))
                                    @foreach ($bannerone as $one)
                                    <li id="image_li_{{ $one['id'] }}" class="ui-sortable-handle">
                                        <a href="javascript:void(0);" style="float:none;" class="image_link">
                                            <img class="rounded-lg w-[1080px] h-auto" src="{{ asset($one['banner']) }}" alt="">
                                        </a>
                                    </li>
                                    @endforeach
                                    
                                        @foreach ($bannertwo as $two)
                                        <li id="image_li_{{ $two['id'] }}" class="ui-sortable-handle">
                                            <a href="javascript:void(0);" style="float:none;" class="image_link">
                                                <img class="rounded-lg w-[314px] h-auto" src="{{ asset($two['banner']) }}" alt="">
                                            </a>
                                        </li>
                                        @endforeach
                                        @foreach ($bannerthree as $three)
                                        <li id="image_li_{{ $three['id'] }}" class="ui-sortable-handle">
                                            <a href="javascript:void(0);" style="float:none;" class="image_link">
                                                <img class="rounded-lg w-[314px] h-auto" src="{{ asset($three['banner']) }}" alt="">
                                            </a>
                                        </li>
                                        @endforeach
                                        @foreach ($bannerfour as $four)
                                        <li id="image_li_{{ $four['id'] }}" class="ui-sortable-handle">
                                            <a href="javascript:void(0);" style="float:none;" class="image_link">
                                                <img class="rounded-lg w-[314px] h-auto" src="{{ asset($four['banner']) }}" alt="">
                                            </a>
                                        </li>
                                        @endforeach

                                    @foreach ($bannerfive as $five)
                                    <li id="image_li_{{ $five['id'] }}" class="ui-sortable-handle">
                                        <a href="javascript:void(0);" style="float:none;" class="image_link">
                                            <img class="rounded-lg w-[1080px] h-auto" src="{{ asset($five['banner']) }}" alt="">
                                        </a>
                                    </li>
                                    @endforeach                                    
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>    
    
    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                if (! event.target.files.length) return

                let file = event.target.files[0],
                    reader = new FileReader()

                reader.readAsDataURL(file)
                reader.onload = e => callback(e.target.result)
                },
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.reorder_link').on('click',function(){
                $("ul.reorder-photos-list").sortable({ tolerance: 'pointer' });
                $('.reorder_link').html('save reordering');
                $('.reorder_link').attr("id","saveReorder");
                $('#reorderHelper').slideDown('slow');
                $('.image_link').attr("href","javascript:void(0);");
                $('.image_link').css("cursor","move");
                
                $("#saveReorder").click(function( e ){
                    if( !$("#saveReorder i").length ){
                        $(this).html('').prepend('<img src="images/refresh-animated.gif"/>');
                        $("ul.reorder-photos-list").sortable('destroy');
                        $("#reorderHelper").html("Reordering Photos - This could take a moment. Please don't navigate away from this page.").removeClass('light_box').addClass('notice notice_error');
                        
                        var h = [];
                        $("ul.reorder-photos-list li").each(function() {
                            h.push($(this).attr('id').substr(9));
                        });
                        
                        $.ajax({
                            method: "POST",
                            url: "/dashboard/kelola-toko/reorder-banner",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                'data': h
                            },
                            success: function(){
                                window.location.reload();
                            }
                        });	
                        return false;
                    }	
                    e.preventDefault();
                });
            });
        });
    </script>
</body>
</html>