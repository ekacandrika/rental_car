<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- base | always include -->
    <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.1.1/dist/css/base/jquery.fonticonpicker.min.css">

    <!-- default grey-theme -->
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.0.0-alpha.0/dist/css/themes/grey-theme/jquery.fonticonpicker.grey.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />


    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script type="text/javascript" src="https://unpkg.com/@fonticonpicker/fonticonpicker/dist/js/jquery.fonticonpicker.min.js"></script>
 
    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }

        .destindonesia {
            display: none;
        }
        .edit-parent{
            background:#5bf75d;
        }
        .edit-parent:hover{
            
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param || null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                    <div class="grid grid-cols-2">
                        <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Master Route</div>
                    </div>
                    {{-- Master route --}}
                    <form action="{{ route('masterroute.store') }}" method="POST" x-data="addRow()">
                        @csrf
                        <div class="px-3">
                            <button type="button"
                                class="px-3 py-1 my-3 text-sm text-white rounded-full btn btn-info bg-kamtuu-second"
                                id="dynamic-arx" @click="add()">Tambah +</button>
                        </div>
                        <div class="block space-x-5 mb-5" x-init="$nextTick(()=>{select2WithAlpine()})">
                            <table id="dynamicAddRemove">
                                <tr>
                                    <td class="block px-5 mb-6">
                                        <label for="text"
                                            class="block mb-2 text-sm font-bold text-[#333333]">Judul
                                            Rute</label>
                                        <input type="text" name="title" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Judul Rute">
                                        <div
                                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                        </div>
                                    </td>
                                </tr>
                                <template x-if="rows.length >= 1">
                                    <template x-for="(row, index) in rows">
                                        <tr>
                                            <td class="block px-5 mb-6">
                                            {{-- x-init="$nextTick(() => { select2WithAlpine() })" --}}
                                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">From</label>
                                                <select name="district_id_from[]" class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1" id="district_id_from" x-ref="from_select">
                                                    <option>Select District</option>
                                                    @foreach ($kecamatan as $item)
                                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900"></div>
                                            </td>
                                            {{-- <td class="block px-5 mb-6">
                                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Depature</label>
                                                <select name="depature_id[]" class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                                    <option>Select Depature</option>
                                                    @foreach ($from_point as $depature)
                                                        <option value="{{ $depature->id }}">{{ $depature->namaWisata }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900"></div>
                                            </td> --}}
                                            <td class="block px-5 mb-6">
                                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">To</label>
                                                <select name="district_id_to[]" class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1" id="district_id_to">
                                                    <option>Select District</option>
                                                    @foreach ($kecamatan as $item)
                                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900"></div>
                                            </td>
                                            {{-- <td class="block px-5 mb-6">
                                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Destination</label>
                                                <select name="desination_id[]" class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                                    <option>Select Destination</option>
                                                    @foreach ($to_point as $destination)
                                                        <option value="{{ $destination->id }}">{{ $destination->namaWisata }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900"></div>
                                            </td> --}}
                                            <td class="p-3">
                                                <button type="button" class="mx-2 remove-input-field bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]" @click="remove(index)">Delete</button>
                                            </td>
                                        </tr>
                                    </template>
                                </template>
                            </table>
                        </div>
                        
                        <div class="p-5" x-data="masterroutes()">
                            <div class="grid grid-cols-6">
                                <div class="col-start-6 col-end-7">
                                    <button type="submit"
                                        class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    {{-- View Result #dce301 --}}
                    <div x-data="masterroutes()">
                        <div class="grid grid-cols-2">
                            <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Result Master Route</div>
                        </div>
                        <div class="flex items-center col-start-4 col-span-1 px-5 mb-6">
                            <label for="simple-search" class="sr-only">Search</label>
                                <div class="relative w-1/4">
                                    <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                        <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400"
                                            fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                                                clip-rule="evenodd"></path>
                                        </svg>
                                    </div>
                                    <input type="text" id="simple-search"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Search" @keyup="search()">
                                </div>
                        </div>
                        <div class="block px-5 mb-6">
                           <table class="w-full text-sm text-[#333333]">
                            <thead class="text-md text-white bg-[#D25889]">
                                <tr>
                                    <th class="py-3 px-6">Judul Rute</th>
                                    <th class="py-3 px-6" colspan="3"></th>
                                    <th class="py-3 px-6"></th>
                                </tr>
                            </thead>
                            <template x-if="datas.length >= 1">
                                <template x-for="(data, index) in datas">
                                {{-- x-on:click="open = !open" --}}
                                    <tbody>
                                        <tr>
                                            <td class="px-6 py-2" x-text="data.title_route"></td>
                                            <td class="px-6 py-2" colspan="2"></td>
                                            <td class="px-6 py-2">
                                                <div class="flex justify-end">
                                                    <button type="button" class="float-right mx-2 bg-[#5bf75d] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#75f277]" :class="'parent_'+index" @click="opened(index)" style="background:#5bf75d;">Detail</button>
                                                    <a x-bind:href="`masterroute/${data.id}/edit`" class="float-right mx-2 bg-[#e8bf3a] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#f6ba59]" style="background:#e8bf3a;">Edit</a>
                                                    <form x-bind:action="`masterroute/${data.id}/clone/parent`" method="post">
                                                        @csrf
                                                        <button type="submit" class="float-right mx-2 bg-[#398ded] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#398ded]" style="background:#398ded;">
                                                            Salin
                                                        </button>
                                                    </form>
                                                    {{-- masterroute/{masterroute}  --}}
                                                    <form x-bind:action="`masterroute/${data.id}`" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="float-right mx-2 bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]">Hapus</button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr :class="'child-'+index" class="hidden">
                                            <td colspan="4">
                                                {{-- <div x-show="open"> --}}
                                                <table class="w-full text-sm text-[#333333]">
                                                    <thead>
                                                        <tr>
                                                            <th class="py-3 px-6">From</th>
                                                            {{-- <th class="py-3 px-6">Depature</th> --}}
                                                            <th class="py-3 px-6">To</th>
                                                            {{-- <th class="py-3 px-6">Destination</th> --}}
                                                            <th class="py-3 px-6">Action</th>
                                                        </tr>
                                                    </thead>
                                                    {{-- <template x-if="data.rute.length >= 1"> --}}
                                                        <template x-for="(rute, i) in data.routes">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="py-3 px-6" x-text="rute.from"></td>
                                                                    {{-- <td class="py-3 px-6" x-text="rute.depature"></td> --}}
                                                                    <td class="py-3 px-6" x-text="rute.to"></td>
                                                                    {{-- <td class="py-3 px-6" x-text="rute.destination"></td> --}}
                                                                    <td class="py-3 px-6">
                                                                        <div class="flex justify-end">
                                                                            <form x-bind:action="`masterroute/${data.id}/clone`" method="post">
                                                                                @csrf
                                                                                <input type="hidden" name="from" x-model="rute.from_id"/>
                                                                                {{-- <input type="hidden" name="depature" x-model="rute.depature_id"/> --}}
                                                                                <input type="hidden" name="to" x-model="rute.to_id"/>
                                                                                {{-- <input type="hidden" name="destination" x-model="rute.destination_id"/> --}}
                                                                                <button type="submit" class="float-right mx-2 bg-[#398ded] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#398ded]" style="background:#398ded;">Salin
                                                                                </button>
                                                                            </form>

                                                                                <a x-bind:href="`masterroute/${index}/${i}/${data.id}/edit/detail`" class="float-right mx-2 bg-[#e8bf3a] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#f6ba59]" style="background:#e8bf3a;">Edit
                                                                                </a>
                                                                            <form x-bind:action="`masterroute/${index}/${i}/${data.id}/delete/detail`" method="post">
                                                                                @csrf
                                                                                @method('DELETE')
                                                                                <button type="submit" class="float-right mx-2 bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]">Hapus</button>
                                                                            </form>
                                                                        </div>
                                                                    </td>
                                                                <tr>
                                                            </tbody>
                                                        {{-- </template> --}}
                                                    </template>
                                                </table>
                                                {{-- </div> --}}
                                            </td>
                                        </tr>
                                    </tbody>
                                </template>
                            </template>
                            <template x-if="datas.length == 0">
                                <tbody>
                                    <tr>
                                    <td class="py-3 px-6 text-center text-2xl font-bold" colspan="3">Rute tidak ditemukan atau data belum ada</td>
                                    </tr>
                                </tbody>
                            </template>
                           </table>
                           <nav aria-label="Page navigation" class="text-end mt-3">
                            <template x-if="links.length > 1">
                                <ul class="inline-flex -space-x-px text-sm">
                                    {{-- <li>
                                        <button type="button" class="flex items-center justify-center px-3 h-8 ml-0 leading-tight text-gray-500 bg-white border border-gray-300 rounded-l-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white" @click="previousPage()">Previous</button>
                                    </li> --}}
                                    <template x-for="(link, index) in links" :key="index">
                                        <li>
                                            <button type="button" class="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white" x-text="link.label" :class="link.active" @click="getPage(link.url)">
                                            </button>
                                        </li>
                                    </template>
                                    {{-- <li>
                                        <button type="button" class="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 rounded-r-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white" @click="nextPage()">Next</button>
                                    </li> --}}
                                </ul>
                            </nav>
                            </template>
                        </div>                       
                        
                        {{-- <nav aria-label="Page navigation example">
                        <ul class="inline-flex -space-x-px text-base h-10">
                            <li>
                            <a href="#" class="flex items-center justify-center px-4 h-10 ml-0 leading-tight text-gray-500 bg-white border border-gray-300 rounded-l-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">Previous</a>
                            </li>
                            <li>
                            <a href="#" class="flex items-center justify-center px-4 h-10 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">1</a>
                            </li>
                            <li>
                            <a href="#" class="flex items-center justify-center px-4 h-10 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">2</a>
                            </li>
                            <li>
                            <a href="#" aria-current="page" class="flex items-center justify-center px-4 h-10 text-blue-600 border border-gray-300 bg-blue-50 hover:bg-blue-100 hover:text-blue-700 dark:border-gray-700 dark:bg-gray-700 dark:text-white">3</a>
                            </li>
                            <li>
                            <a href="#" class="flex items-center justify-center px-4 h-10 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">4</a>
                            </li>
                            <li>
                            <a href="#" class="flex items-center justify-center px-4 h-10 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">5</a>
                            </li>
                            <li>
                            <a href="#" class="flex items-center justify-center px-4 h-10 leading-tight text-gray-500 bg-white border border-gray-300 rounded-r-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">Next</a>
                            </li>
                        </ul>
                        </nav> --}}
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <script>

        let url = "{{route('masterroute.json.datas')}}";
        var array = {!! isset($collect) ? $collect:'[]' !!};

        function addRow(){
            return{
                rows:[],
                add(){
                    // alert('button clicked')
                    this.rows.push({
                        from_id:'',
                        to_id:''
                    })
                    console.log(this.rows)
                },
                remove(index){
                    this.rows.splice(index, 1)
                },
                select2WithAlpine(){
                    console.log($(this.$refs.from_select));
                }
            }
        }

        document.addEventListener('alpine:init', () => {
            Alpine.data('masterroutes',()=>({
                datas:[],
                links:[],
                total_page:'',
                active:'',
                current_page:null,
                cur_page:null,
                async init(){
                    let loads = await fetch(`${url}`);

                    let obj = await loads.json();
                    let data_obj = obj.datas.data;

                    let links = obj.datas.links;
                    let total = data_obj.total;
                    let curPage = obj.datas.current_page;

                    this.datas = data_obj;
                    this.links = links;
                    
                    this.total_page = total;
                    this.cur_page = curPage;

                    console.log(obj.datas.current_page)
                    
                },
                search(){
                    var cari   = $("#simple-search").val();                   
                    let http = "{{route('master.route.result')}}";
                    let page = '&page=1';

                    if(cari.length > 0){
                        page = '&page='+this.cur_page;
                    }
                    
                    console.log(page)

                    fetch(`${http}?search=${cari}`).
                    then(response =>response.json())
                    .then(data=>{
                        this.datas=data.data.data
                        console.log(data.data.links)
                        this.links=data.data.links
                    })
                },
                opened(index){
                    var parent =".parent_"+index;
                    var name =".child-"+index;

                    $(name).toggle()
                },
                getPage(page){
                    
                    let arr = [];
                    
                    if(page){
                        this.current_page = page;
                    }else{
                        this.current_page = '/?page=1';
                    }
                    
                    fetch(`${url}${this.current_page}`)
                    .then(resp=>resp.json())
                    .then(data=>{
                        let obj = data.datas.data
                        this.cur_page= data.datas.current_page

                        if(obj.length!=undefined){
                            this.datas = obj
                        }else{
                            Object.keys(obj).forEach(key=>{
                                arr.push(obj[key])
                            })

                            this.datas = arr
                        }
                    })

                }
            }))
        })
    </script>
    <script type="text/javascript">
        $("#dynamic-ar").click(function() {
            $("#dynamicAddRemove").append(
                '<tr><td class="block px-5 mb-6"><label for="text" class="block mb-2 text-sm font-bold text-[#333333]">From</label><select name="district_id_from[]" class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1"><option>Select District</option>@foreach ($kecamatan as $item)<option value="{{ $item->id }}">{{ $item->name }}</option>@endforeach</select><div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900"></div></td><td class="block px-5 mb-6"><label for="text" class="block mb-2 text-sm font-bold text-[#333333]">To</label><select name="district_id_to[]" class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1"><option>Select District</option>@foreach ($kecamatan as $item)<option value="{{ $item->id }}">{{ $item->name }}</option>@endforeach</select><div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900"></div></td><td class="p-3"><button type="button" class="mx-2 remove-input-field bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]">Delete</button></td></tr>'
            );
        });

    </script>
    <!-- Select 2 -->
    <script>
        $(document).ready(function(){
          // $('#district_id_from').select2();
          // $('#district_id_to').select2();
        })
    </script>
</body>

</html>
