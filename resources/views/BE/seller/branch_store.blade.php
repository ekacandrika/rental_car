<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    {{-- summernote --}}
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    
    {{-- select2 --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
   

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        h1 {
            font-size: 30px;
            color: #000;
        }

        h2 {
            font-size: 20px;
            color: #000;
        }

        .menu-malasngoding li a {
            display: inline-block;
            color: white;
            /* text-align: center; */
            text-decoration: none;
        }

        .menu-malasngoding li a:hover {
            background-color: none;
        }

        li.dropdown {
            display: inline-block;
            margin-right: 80px
        }

        .dropdown:hover .isi-dropdown {
            display: block;
        }

        .isi-dropdown a:hover {
            color: #fff !important;
            width: 100%;
        }

        .isi-dropdown {
            position: absolute;
            display: none;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
            background-color: #f9f9f9;
            width: 100%;
        }

        .isi-dropdown a {
            color: #3c3c3c !important;
            padding: 1%;
        }

        .isi-dropdown a:hover {
            color: #232323 !important;
            background: #f3f3f3 !important;
        }

        a[disabled="disabled"] {
            pointer-events: none;
        }

        /* Modal CSS */
        .btn {
            background-color: #4CAF50;
            color: white;
            padding: 8px 16px;
            border: none;
            cursor: pointer;
            font-size: 16px;
        }

        /* Style the modal */
        .modal {
            display: none;
            position: fixed;
            z-index: 99;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0, 0, 0, 0.4);
        }

        /* Style the modal content */
        .modal-content {
            background-color: #fefefe;
            margin: 10% auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
            max-width: 600px;
        }

        /* Media query for screens smaller than 600px */
        @media screen and (max-width: 600px) {
            .modal-content {
                margin: 15% auto;
                width: 90%;
            }
        }

        /* Style the close button */
        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        /* Style the form input */
        .form-control {
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            margin-bottom: 10px;
            width: 100%;
        }

        /* Style the submit button */
        .btn-primary {
            background-color: #23AEC1;
            color: white;
            padding: 8px 16px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        .btn-primary:hover {
            background-color: #23AEC1;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.seller.navbar-seller></x-be.seller.navbar-seller>

    {{-- Sidebar --}}
    <x-be.seller.sidebar-seller></x-be.seller.sidebar-seller>

    <div class="grid grid-cols-10">
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10">
                <span class="text-2xl font-bold font-inter text-[#333333]">Input Cabang</span>
                <div class="bg-white my-5 rounded-md shadow-lg">
                    <form action="{{$isedit==true  ? route('seller.update-branch',$branch_store->id) : route('seller.store-branch')}}" method="post">
                        @csrf
                        @if($isedit)
                            @method('PUT')
                        @endif
                        <div class="ml-12 p-6">
                            <div class="grid grid-cols-2">
                                <div class="my-2">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Provinsi</label>
                                    <select name="province_id" id="provinsi"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required>
                                        <option selected disabled>Pilih Provinsi</option>
                                        @foreach ($province as $provinsi)
                                        <option value="{{ $provinsi->id }}" {{isset($branch_store->province_id) ? ($branch_store->province_id==$provinsi->id ? 'selected':''):null}}>{{ $provinsi->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="my-2">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kota/Kabupaten</label>
                                    <select name="regency_id" id="kabupaten"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required>
                                        <option selected disabled>Pilih Kota/Kabupaten</option>
                                    </select>
                                </div>
                            </div>
                            <div class="grid grid-cols-2">
                                <div class="my-2">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kecamatan</label>
                                    <select name="district_id" id="kecamatan"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required>
                                        <option selected disabled>Pilih Kecamatan</option>
                                    </select>
                                </div>
                                <div class="my-2">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kecamatan</label>
                                    <select name="village_id" id="kelurahan"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required>
                                        <option selected disabled>Pilih Kelurahan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="grid grid-cols-2">
                                <div class="my-2">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Alamat</label>
                                    <textarea rows="4" name="address" id="branch_store_address"
                                                class="block p-2.5 w-96 text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-100 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                placeholder="Deskripsi" required>{{isset($branch_store->address) ? $branch_store->address : ''}}
                                    </textarea>
                                </div>
                                <div class="my-2">
                                    <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kode Pos</label>
                                    <input type="text" name="post_code" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Kode Pos" value="{{isset($branch_store->post_code) ? $branch_store->post_code : ''}}">
                                </div>
                            </div>
                            <button type="submit"
                                    class="w-fit items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                    Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
    $(function(){
        // $("#provinsi").select2({
        //     allowClear: true
        // })
    });

    $(function(){
        // provinsi
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#provinsi").on("change",function(){
            let id_provinsi =$("#provinsi").val();

            $.ajax({
                type:"POST",
                url:"{{ route('getkabupaten')}}",
                data:{
                    id_provinsi:id_provinsi
                },
                cache:false,
                success:function(msg){
                    $("#kabupaten").html(msg);
                },
                error:function(data){
                    console.log('error:',data)
                }
            })

        });
       
        // kabupaten
        $("#kabupaten").on("change",function(){
            let id_kabupaten=$(this).val();
            console.log(id_kabupaten);

            $.ajax({
                type:'post',
                url:"{{ route('getkecamatan')}}",
                data:{
                    id_kabupaten:id_kabupaten
                },
                cache:false,
                success:function(msg){
                    $("#kecamatan").html(msg);
                },
                error:function(data){
                    console.log('error:',data)
                }

            });
        })

        $("#kecamatan").on("change",function(){
            let id_kecamatan = $(this).val();
            console.log(id_kecamatan);
            
            $.ajax({
                type:'POST',
                url:"{{ route('getdesa')}}",
                data:{
                    id_kecamatan:id_kecamatan
                },
                cache:false,
                success:function(msg){
                    $("#kelurahan").html(msg);
                },
                error:function(data){
                    console.log('error:',data)
                }
            })
        })
    });
</script>
<script>
    $(function(){
        // ajax setup
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        // edit 
        if($("#provinsi").val() !=' '){
            let id_provinsi = $("#provinsi").val();
            console.log(id_provinsi)
            $.ajax({
                type:"POST",
                url:"{{ route('getkabupaten')}}",
                data:{
                    id_provinsi:id_provinsi,
                    id_kabupaten:'{!! isset($branch_store->regency_id) ? $branch_store->regency_id :null !!}'
                },
                cache:false,
                success:function(msg){
                    $("#kabupaten").html(msg);
                },
                error:function(data){
                    console.log('error:',data)
                }
            })
        }

        if($("#kabupaten").val() !=' '){
            // let id_kabupaten = ;
            // let id_kecamatan = ;

            $.ajax({
                type:"POST",
                url:"{{ route('getkecamatan')}}",
                data:{
                    id_kabupaten:'{!! isset($branch_store->regency_id) ? $branch_store->regency_id :null !!}',
                    id_kecamatan:'{!! isset($branch_store->district_id) ? $branch_store->district_id :null !!}'
                },
                cache:false,
                success:function(msg){
                    $("#kecamatan").html(msg);
                },
                error:function(data){
                    console.log('error:',data)
                }
            })
        }

        if($("#kecamatan").val() !=' '){
            // let id_kecamatan = ;
            // let id_kelurahan = ;

            $.ajax({
                type:"POST",
                url:"{{ route('getdesa')}}",
                data:{
                    id_kecamatan:'{!! isset($branch_store->district_id) ? $branch_store->district_id :null !!}',
                    id_kelurahan:'{!! isset($branch_store->village_id) ? $branch_store->village_id :null !!}'
                },
                cache:false,
                success:function(msg){
                    $("#kelurahan").html(msg);
                    console.log(msg);
                },
                error:function(data){
                    console.log('error:',data)
                }
            })
        }
    })
</script>