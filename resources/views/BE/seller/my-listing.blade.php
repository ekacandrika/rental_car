<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        /* Toogle Switch */
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

            {
                {
                -- slider on off start --
            }
        }

        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        h1 {
            font-size: 30px;
            color: #000;
        }

        h2 {
            font-size: 20px;
            color: #000;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

            {
                {
                -- slider on off end --
            }
        }


        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }

        /* The Modal (background) */
        .modal {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Stay in place */
            z-index: 1;
            /* Sit on top */
            padding-top: 100px;
            /* Location of the box */
            left: 10%;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow: auto;
            /* Enable scroll if needed */
            background-color: rgb(0, 0, 0);
            /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4);
            /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            border: 1px solid #888;
            width: 55%;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }

        /* Add Animation */
        @-webkit-keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }

            to {
                top: 0;
                opacity: 1
            }
        }

        @keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }

            to {
                top: 0;
                opacity: 1
            }
        }

        /* The Close Button */
        .close {
            color: white;
            float: right;
            margin-top: -8px;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        .modal-header {
            padding: 3px 16px;
            font-size: 20px;
            background-color: #9E3D64;
            color: white;
        }

        .modal-body {
            padding: 2px 16px;
        }

        .modal-footer {
            padding: 2px 16px;
            text-align: right;
            background-color: #fff;
        }

        [x-cloak] {
            display: none
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    <x-be.seller.sidebar-toko>
        {{-- Hotel --}}
        <x-slot name="addListingHotel">
            {{ route('hotelinfodasar') }}
        </x-slot>
        <x-slot name="faq">
            @if ($data == null)
            {{ route('hotelfaq') }}
            @else
            {{ route('hotelfaq.edit', $data->product_code) }}
            @endif
        </x-slot>

        {{-- Xstay --}}
        <x-slot name="addListingXstay">
            {{ route('infodasar') }}
        </x-slot>
        <x-slot name="faqXstay">
            @if ($xstay == null)
            {{ route('faq.index') }}
            @else
            {{ route('faq.edit', $xstay->product_code) }}
            @endif
        </x-slot>
    </x-be.seller.sidebar-toko>
    {{-- @dd(session()->has('url')) --}}
    {{-- @if (session()->has('url')) --}}
    {{-- <script>
        window.open('{{session()->get('url')}}', "_blank");
    </script>
    @endif --}}
    {{--  --}}
    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10">
            <span class="text-2xl font-bold font-inter text-[#333333]">My Listing</span>
            <div class="flex grid grid-cols-4 py-5 relative" x-data="{open:false}">
                <button type="button"
                    data-dropdown-toggle="list-dropdown" 
                    id="addListings"
                    class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800 w-[150px]"
                    x-on:click="open = !open">
                    Tambah
                    Listing
                </button>
                <div id="list-dropdown" class="z-10 bg-white divide-y mt-2 divide-gray-100 rounded-lg shadow w-44 absolute" style="top:61px" x-show="open">
                    <ul class="py-2 text-sm text-gray-700 dark:text-gray-200" aria-labelledby="addListings">
                        @php
                            $lisensi = json_decode(auth()->user()->lisensi);
                            //dump($lisensi);
                            $personal_tur='';
                            $personal_hotel='';
                            $bussiness_tour_hotel='';

                            $personal_transfer='';
                            $personal_rental='';
                            $bussiness_transfer_rental='';

                            $personal_activity='';
                            $personal_xstay='';
                            $bussiness_activity_xstay='';

                            if($lisensi){
                                    foreach($lisensi as $key => $v){
                                    $personal_tur = isset($v->personal_tur) ? $v->personal_tur : '';
                                    $personal_hotel=isset($v->personal_hotel) ? $v->personal_hotel : '';
                                    $bussiness_tour_hotel=isset($v->bussiness_tour_hotel) ? $v->bussiness_tour_hotel : '';

                                    $personal_transfer=isset($v->personal_transfer) ? $v->personal_transfer : '';
                                    $personal_rental=isset($v->personal_rental) ? $v->personal_rental : '';
                                    $bussiness_transfer_rental=isset($v->bussiness_transfer_rental) ? $v->bussiness_transfer_rental : '';

                                    $personal_activity=isset($v->personal_activity) ? $v->personal_activity : '';
                                    $personal_xstay=isset($v->personal_xstay) ? $v->personal_xstay : '';
                                    $bussiness_activity_xstay=isset($v->bussiness_activity_xstay) ? $v->bussiness_activity_xstay : '';
                                }
                            }
                        @endphp
                        <li class="flex hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white">
                            @if ($personal_tur || $bussiness_tour_hotel)
                                <a href="{{route('tur')}}" class="block px-4 py-2" id="ingin-kesana">Tur</a>
                            @else
                                <button  class="block px-4 py-2 cursor-not-allowed" id="ingin-kesana" disabled>Tur</button>
                            @endif
                            {{-- <div>
                                <img src="{{ asset('storage/icons/bookmark-star-fill.svg') }}"class="mt-[0.2rem] ml-3" alt="suka" title="Suka" style="width: 21px;">
                            </div> --}}
                        </li>
                        <li class="flex hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white">
                            @if ($personal_transfer || $bussiness_transfer_rental)
                               <a href="{{route('transfer')}}" class="block px-4 py-2 hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white" id="pernah-kesana">Transfer</a>
                            @else
                                <button class="block px-4 py-2 hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white cursor-not-allowed" id="pernah-kesana" disabled>Transfer</button>
                            @endif
                        </li>
                        <li class="flex hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white">
                            @if ($personal_rental || $bussiness_transfer_rental)
                            <a href="{{route('rental')}}" class="block px-4 py-2 hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white" id="pernah-kesana">Rental</a>
                            @else
                            <button class="block px-4 py-2 hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white cursor-not-allowed" id="pernah-kesana" disabled>Rental</button>
                            @endif
                        </li>
                        <li class="flex hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white">
                            @if ($personal_hotel || $bussiness_tour_hotel)
                                <a href="{{route('hotelinfodasar')}}" class="block px-4 py-2 hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white" id="pernah-kesana">Hotel</a>
                            @else
                                <button class="block px-4 py-2 hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white cursor-not-allowed" id="pernah-kesana" disabled>Hotel</button>
                            @endif
                        </li>
                        <li class="flex hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white">
                            @if ($personal_xstay || $bussiness_activity_xstay)
                                <a href="{{route('infodasar')}}" class="block px-4 py-2 hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white" id="pernah-kesana">Xstay</a>
                            @else
                                <button class="block px-4 py-2 hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white cursor-not-allowed" id="pernah-kesana" disabled>Xstay</button>
                            @endif
                        </li>
                        <li class="flex hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white">
                            @if ($personal_activity || $bussiness_activity_xstay)
                                <a href="{{route('activity')}}" class="block px-4 py-2 hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white" id="pernah-kesana">Activicy</a>
                            @else
                                <button class="block px-4 py-2 hover:bg-blue-700 dark:hover:bg-gray-600 dark:hover:text-white cursor-not-allowed" id="pernah-kesana" disabled>Activicy</button>
                            @endif    
                        </li>
                    </ul>
                </div>
                <div class="flex items-center col-start-4 col-span-1">
                    <label for="simple-search" class="sr-only">Search</label>
                    <div class="relative w-[96%]">
                        <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor"
                                viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                                    clip-rule="evenodd"></path>
                            </svg>
                        </div>
                        <input type="text" id="simple-search"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Search" name="search" required>
                    </div>
                    &nbsp;
                    <button type="button" id="searchProduct" class="bg-blue-700 text-white text-sm px-5 py-2.5 font-medium border-gray-900 rounded-md">Search</button>
                </div>
            </div>
            <div class="grid grid-cols-2">
                <div class="grid justify-items-start">
                    <div class="inline-flex p-2 items-center">
                        <input class="checked:bg-kamtuu-second" type="checkbox" name="" id="allChecked">
                        <p class="px-2">Pilih Semua</p>
                        <div class="w-[100px] mx-5">
                            <select
                                class="px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-[100px] rounded-md text-sm focus:ring-1" id="filterActiveNonActive">
                                <option value="publish" {{isset($available) ? ($available=='publish'?'selected':''):''}}>Aktif</option>
                                <option value="draft" {{isset($available) ? ($available=='draft'?'selected':''):''}}>Tidak Aktif</option>
                            </select>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>
                        </div>
                    </div>
                    {{-- @dump($data_listing) --}}
                </div>
                <div class="grid justify-end">
                    <div class="py-2">
                        <select
                            id="typeFilter" class="px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-[250px] rounded-md text-sm focus:ring-1">
                            <option>-- Kategori --</option>
                            <option value="tour" {{isset($type) ? ($type=='tour' ? 'selected' : ''):''}}>Tur</option>
                            <option value="transfer" {{isset($type) ? ($type=='transfer' ? 'selected' : ''):''}}>Transfer</option>
                            <option value="rental" {{isset($type) ? ($type=='rental' ? 'selected' : ''):''}}>Rental</option>
                            <option value="hotel" {{isset($type) ? ($type=='hotel' ? 'selected' : ''):''}}>Hotel</option>
                            <option value="xstay" {{isset($type) ? ($type=='xstay' ? 'selected' : ''):''}}>XStay</option>
                            <option value="activity"  {{isset($type) ? ($type=='activity' ? 'selected' : ''):''}}>Activity</option>
                        </select>
                        <div
                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                        </div>
                    </div>
                </div>
            </div>

            <x-flash-message></x-flash-message>
            @forelse ($data_listing as $key=>$item)
            <div class="bg-white my-5 rounded-md shadow-lg">
                <div class="grid grid-cols-12">
                    {{-- Cols 1 --}}
                    <div class="grid place-content-center">
                        <input class="checked:bg-kamtuu-second list-product-check" type="checkbox" name="">
                    </div>
                    {{-- Cols 2 --}}
                    <div class="col-span-2 m-5">
                        <img src="{{ asset($item->thumbnail ?? $item->foto_maps_2 ?? 'https://via.placeholder.com/100x100')}}"
                            class="w-[150px] h-[150px] inline-flex rounded-lg object-cover" alt="" title="">
                    </div>
                    {{-- Cols 3 --}}
                    <div class="m-5 col-start-4 col-span-5">
                        @if ($item->product->type == 'tour')
                        <a href="{{route('tur.show', $item->product->slug)}}">
                            <p class="text-lg font-bold">{{ $item->product->product_name }}</p>
                        </a>
                        <div class="flex py-2 gap-1 lg:gap-2">
                            <div
                                class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                Tur
                            </div>
                        </div>
                        <div class="flex">
                            @if($item->tipe_tur != null)
                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/user-solid.svg') }}"
                                alt="rating" width="17px" height="17px">
                            <p class="font-bold text-[8px] lg:text-sm mr-2"> {{$item->tipe_tur}}</p>
                            @else
                            <p></p>
                            @endif
                            @if($item->makanan != null)
                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                src="{{ asset('storage/icons/utensils-solid.svg') }}" alt="rating" width="17px"
                                height="17px">
                            <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->makanan}}</p>
                            @else
                            @endif
                            @if($item->jml_hari != null AND $item->jml_malam != null)
                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                src="{{ asset('storage/icons/hourglass-solid.svg') }}" alt="rating" width="17px"
                                height="17px">
                            <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->jml_hari}}
                                D{{$item->jml_malam}}N</p>
                            @else
                            @endif
                        </div>
                        <div class="mt-10">
                            @if($item->available == 'draft' OR $item->available == 'waiting approve')
                            <p class="text-red-600 mt-8">Draft</p>
                            @endif
                            @if($item->available == 'publish' OR $item->availablle == 'approve')
                            <p class="text-green-600">Tersedia</p>
                            @endif
                        </div>
                        @endif
                        @if ($item->product->type == 'hotel')
                        <a href="{{route('hotel.show', $item->product->slug)}}">
                            <p class="text-lg font-bold">{{ $item->product->product_name }}</p>
                        </a>
                        <div class="flex py-2 gap-1 lg:gap-2">
                            <div
                                class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                Hotel
                            </div>
                        </div>
                        <div class="flex">
                            {{-- <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" --}} {{--
                                src="{{ asset('storage/icons/user-solid.svg') }}" alt="rating" width="17px" --}} {{--
                                height="17px">--}}
                            {{-- <p class="font-bold text-[8px] lg:text-sm mr-2"> {{$item->tipe_tur}}</p>--}}
                            @if($item->product->makanan != null)
                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                src="{{ asset('storage/icons/utensils-solid.svg') }}" alt="rating" width="17px"
                                height="17px">
                            <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->product->makanan}}</p>
                            @else

                            @endif
                            {{-- <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" --}} {{--
                                src="{{ asset('storage/icons/hourglass-solid.svg') }}" alt="rating" width="17px" --}}
                                {{-- height="17px">--}}
                            {{-- <p class="font-bold  text-[8px] lg:text-sm mr-2">
                                {{$item->jml_hari}}D{{$item->jml_malam}}N</p>--}}
                        </div>
                        <div class="mt-10">
                            @if($item->available == 'draft' OR $item->available == 'waiting approve')
                            <p class="text-red-600 mt-8">Draft</p>
                            @endif
                            @if($item->available == 'publish' OR $item->availablle == 'approve')
                            <p class="text-green-600">Tersedia</p>
                            @endif
                        </div>
                        @endif
                        @if ($item->product->type == 'xstay')
                        <a href="{{route('xstay.show', $item->product->slug)}}">
                            <p class="text-lg font-bold">{{ $item->product->product_name }}</p>
                        </a>
                        <div class="flex py-2 gap-1 lg:gap-2">
                            <div
                                class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                Xstay
                            </div>
                        </div>
                        <div class="flex">
                            {{-- <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" --}} {{--
                                src="{{ asset('storage/icons/user-solid.svg') }}" alt="rating" width="17px" --}} {{--
                                height="17px">--}}
                            {{-- <p class="font-bold text-[8px] lg:text-sm mr-2"> {{$item->tipe_tur}}</p>--}}
                            {{-- <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" --}} {{--
                                src="{{ asset('storage/icons/utensils-solid.svg') }}" alt="rating" width="17px" --}}
                                {{-- height="17px">--}}
                            {{-- <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->makanan}}</p>--}}
                            {{-- <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" --}} {{--
                                src="{{ asset('storage/icons/hourglass-solid.svg') }}" alt="rating" width="17px" --}}
                                {{-- height="17px">--}}
                            {{-- <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->jml_hari}}--}}
                                {{-- D{{$item->jml_malam}}N</p>--}}
                        </div>
                        <div class="mt-10">
                            @if($item->available == 'draft' OR $item->available == 'waiting approve')
                            <p class="text-red-600 mt-8">Draft</p>
                            @endif
                            @if($item->available == 'publish' OR $item->availablle == 'approve')
                            <p class="text-green-600">Tersedia</p>
                            @endif
                        </div>
                        @endif
                        @if ($item->product->type == 'activity')
                        <a href="{{route('activity.show', $item->product->slug)}}">
                            <p class="text-lg font-bold">{{ $item->product->product_name }}</p>
                        </a>
                        <div class="flex py-2 gap-1 lg:gap-2">
                            <div
                                class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                Activity
                            </div>
                        </div>
                        <div class="flex">
                            {{-- <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" --}} {{--
                                src="{{ asset('storage/icons/user-solid.svg') }}" alt="rating" width="17px" --}} {{--
                                height="17px">--}}
                            {{-- <p class="font-bold text-[8px] lg:text-sm mr-2"> {{$item->tipe_tur}}</p>--}}
                            {{-- <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" --}} {{--
                                src="{{ asset('storage/icons/utensils-solid.svg') }}" alt="rating" width="17px" --}}
                                {{-- height="17px">--}}
                            {{-- <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->makanan}}</p>--}}
                            {{-- <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" --}} {{--
                                src="{{ asset('storage/icons/hourglass-solid.svg') }}" alt="rating" width="17px" --}}
                                {{-- height="17px">--}}
                            {{-- <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->jml_hari}}--}}
                                {{-- D{{$item->jml_malam}}</p>--}}
                        </div>
                        <div class="mt-10">
                            @if($item->available == 'draft' OR $item->available == 'waiting approve')
                            <p class="text-red-600 mt-8">Draft</p>
                            @endif
                            @if($item->available == 'publish' OR $item->availablle == 'approve')
                            <p class="text-green-600">Tersedia</p>
                            @endif
                        </div>
                        @endif
                        @if ($item->product->type == 'transfer')
                        <a href="{{route('transfer.show', $item->product->slug)}}">
                            <p class="text-lg font-bold">{{ $item->product->product_name }}</p>
                        </a>
                        <div class="flex py-2 gap-1 lg:gap-2">
                            {{-- {{dd($item)}} --}}
                            <div
                                class="bg-[#9E3D64] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                Transfer
                            </div>
                            <div
                                class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                Sedan
                            </div>
                        </div>
                        <div class="flex">
                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}"
                                alt="rating" width="17px" height="17px">
                            <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}"
                                alt="rating" width="17px" height="17px">
                            <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                        </div>
                        <div class="mt-10">
                            @if($item->available == 'draft' OR $item->available == 'waiting approve')
                            <p class="text-red-600 mt-8">Draft</p>
                            @endif
                            @if($item->available == 'publish' OR $item->availablle == 'approve')
                            <p class="text-green-600">Tersedia</p>
                            @endif
                        </div>
                        @endif
                        @if ($item->product->type == 'rental')
                        <a href="{{route('rental.show', $item->product->slug)}}">
                            <p class="text-lg font-bold">{{ $item->product->product_name }}</p>
                        </a>
                        <div class="flex py-2 gap-1 lg:gap-2">
                            {{-- {{dd($item)}}--}}
                            <div
                                class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                {{$item->product->type}}
                            </div>
                            <div
                                class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                {{$item->detailKendaraan->jenismobil->jenis_kendaraan}}
                            </div>
                        </div>
                        <div class="flex">
                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}"
                                alt="rating" width="17px" height="17px">
                            <p class="font-bold text-[8px] lg:text-sm mr-2">{{$item->detailKendaraan->kapasitas_kursi}}
                                Kursi</p>
                            <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}"
                                alt="rating" width="17px" height="17px">
                            <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->detailKendaraan->kapasitas_koper}}
                                Koper</p>
                        </div>
                        <div class="mt-10">
                            @if($item->available == 'draft' OR $item->available == 'waiting approve')
                            <p class="text-red-600 mt-8">Draft</p>
                            @endif
                            @if($item->available == 'publish' OR $item->availablle == 'approve')
                            <p class="text-green-600">Tersedia</p>
                            @endif
                        </div>

                        {{-- {{dd($item)}}--}}
                        @endif
                        {{-- <div class="pt-8 flex gap-2 items-center">--}}
                            {{-- <p class="text-sm text-[#FFB800] font-semibold">Sedang Dipesan</p>--}}
                            {{-- <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" --}} {{--
                                src="{{ asset('storage/icons/circle-info-black.svg') }}" alt="rating" --}} {{--
                                width="17px" height="17px">--}}
                            {{-- </div>--}}
                    </div>
                    {{-- Cols 4 --}}
                    <div class="m-5 col-span-4 grid place-items-end">
                        @if($item->available == 'publish')
                        <p class="px-5">Aktif</p>
                        <label class="switch">
                            <input class="toggle-class" data-id="{{$item->id}}" type="checkbox" {{ $item->available ===
                            'publish' ? 'checked' : '' }}>
                            <span class="slider round"></span>
                        </label>
                        @elseif($item->available == 'waiting approve')
                        <p
                            class="bg-yellow-500 px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                            Waiting Approve</p>
                        @else
                        <p class="px-5">Tidak Aktif</p>
                        <label class="switch">
                            <input class="toggle-class" data-id="{{$item->id}}" type="checkbox" {{ $item->available ===
                            'draft' ? 'checked' : '' }}>
                            <span class="slider round"></span>
                        </label>
                        @endif

                        <div class="flex gap-2 items-center mb-8 ">

                            {{-- <p class="font-semibold text-md">--}}
                                {{-- @if($item->available == 'publish')--}}
                                {{--
                            <p
                                class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                Publish</p>--}}
                            {{-- @endif--}}
                            {{-- @if($item->available == 'draft')--}}
                            {{-- <p
                                class="bg-red-500 px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                Draft</p>--}}
                            {{-- @endif--}}
                            {{-- @if($item->available == 'not approved')--}}
                            {{-- <p
                                class="bg-kamtuu-third px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                Not Approve</p>--}}
                            {{-- @endif--}}
                            {{-- @if($item->available == 'approved')--}}
                            {{-- <p
                                class="bg-green-500 px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                Approved</p>--}}
                            {{-- @endif--}}
                            {{-- @if($item->available == 'waiting approve')--}}
                            {{-- <p
                                class="bg-kamtuu-third px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                Waiting Approve</p>--}}
                            {{-- @endif--}}
                            {{-- </p>--}}
                        </div>

                        {{-- @if($item->available == 'approved' OR $item->available == 'publish' OR $item->available ==
                        'draft')--}}
                        {{-- @if ($item->productdetail->available != null)--}}
                        {{-- <form action="{{ route('updateStatus', $item->id) }}" method="POST">--}}
                            {{-- @csrf--}}
                            {{-- @method('PUT')--}}
                            {{-- <div class="flex gap-2 items-center">--}}
                                {{-- <div class="py-2">--}}
                                    {{-- <select name="available" --}} {{--
                                        class="px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-[150px] rounded-md text-sm focus:ring-1">--}}
                                        {{-- <option value="">-- Pilih Status --</option>--}}
                                        {{-- <option value="publish" --}} {{-- {{ $item->productdetail->available ==
                                            'publish' ? 'selected' : '' }}>--}}
                                            {{-- Publish--}}
                                            {{-- </option>--}}
                                        {{-- <option value="draft" --}} {{-- {{ $item->productdetail->available ==
                                            'draft' ? 'selected' : '' }}>--}}
                                            {{-- Draft--}}
                                            {{-- </option>--}}
                                        {{-- --}}{{-- <option value="on_progress" --}} {{-- --}}{{-- {{ $item->
                                            productdetail->confirmation == 'on_progress' ? 'selected' : '' }}>--}}
                                            {{-- --}}{{-- On Progress</option>--}}
                                        {{-- --}}{{-- <option value="success" --}} {{-- --}}{{-- {{ $item->
                                            productdetail->confirmation == 'success' ? 'selected' : '' }}>--}}
                                            {{-- --}}{{-- Success</option>--}}
                                        {{-- --}}{{-- <option value="failed" --}} {{-- --}}{{-- {{ $item->
                                            productdetail->confirmation == 'failed' ? 'selected' : '' }}>--}}
                                            {{-- --}}{{-- Failed</option>--}}
                                        {{-- </select>--}}
                                    {{-- <div--}} {{--
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                        --}}
                                        {{-- </div>--}}
                                {{-- </div>--}}
                            {{-- <button--}} {{--
                                class="items-center px-px py-1 lg:py-2 w-full lg:w-20 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">
                                --}}
                                {{-- Update--}}
                                {{-- </button>--}}
                                {{--
                    </div>--}}
                    {{-- </form>--}}
                    {{-- @endif--}}
                    {{-- @endif--}}
                    <div class="grid-cols-1 place-items-center">
                        <div class="flex my-4">
                            {{-- <span class="mr-10">Ketersediaan</span> --}}
                            <div class="grid w-full justify-end">
                                @if (!empty($item->diskon->tgl_start) && !empty($item->diskon->tgl_end))
                                <div class="relative order-last block my-3 sm:order-none sm:my-0">
                                    <label for="tur-date-picker"
                                        class="inline-block mb-2 text-gray-700 form-label">Ketersediaan</label>
                                    {{-- <div
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                        --}}
                                        <button placeholder="Pilih tanggal" class="calendar"
                                            data-tur-id="{{ $item->id }}" type="text" id="tur_date_{{ $item->id }}"
                                            :class="'!m-0 sm:!w-full'" name="tur_date"></button>
                                        {{--
                                    </div> --}}
                                </div>

                                <script>
                                    $('#tur_date_{{ $item->id }}').each(function () {
                                            var id = $(this).data('tur-id');
                                            var tgl_start = JSON.parse("{{ $item->diskon['tgl_start'] }}".replace(/&quot;/g,'"'));
                                            var tgl_end = JSON.parse("{{ $item->diskon['tgl_end'] }}".replace(/&quot;/g,'"'));
                                            
                                            var disabledDates = [];
                                            var jml_tgl = tgl_start.length;
                                            for(var i = 0; i < jml_tgl; i++) {
                                                var start = new Date(tgl_start[i]);
                                                var end = new Date(tgl_end[i]);
                                                var date = new Date(start);
                                                while (date <= end) {
                                                    disabledDates.push(date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2));
                                                    date.setDate(date.getDate() + 1);
                                                }
                                            }

                                            $(this).datepicker({
                                                // uiLibrary: 'bootstrap4',
                                                disableDates: function (date) {
                                                    var formattedDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                                                    if ($.inArray(formattedDate, disabledDates) != -1) {
                                                        return true; // disable tanggal dalam rentang tgl_start dan tgl_end
                                                    } else {
                                                        return false; // aktifkan tanggal di luar rentang tgl_start dan tgl_end
                                                    }
                                                }
                                            });
                                        });
                                </script>
                                @else
                                <div class="relative order-last block my-3 sm:order-none sm:my-0">
                                    <label for="tur-date-picker"
                                        class="inline-block mb-2 text-gray-700 form-label">Tanggal</label>
                                    <div
                                        class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                        <input placeholder="Pilih tanggal" type="text" id="tur_date_{{ $item->id }}"
                                            :class="'!m-0 sm:!w-full'" name="tur_date" disabled />
                                    </div>
                                </div>

                                <script>
                                    $(document).ready(function() {
                                            today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                                            $('#tur_date_{{ $item->id }}').datepicker({
                                                minDate: today
                                            });
                                        })
                                </script>
                                @endif
                            </div>

                            {{-- <button name="checkIn" id="datepickerKetersediaan{{$key+1}}" placeholder=" " /> --}}
                        </div>
                        @if ($item->product->type == 'hotel')
                        <a href="{{ route('hotel.edit', $item->product->product_code) }}">
                            <button type="submit"
                                class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">
                                Edit
                            </button>
                        </a>
                        <a x-data="showModal" @keydown.escape="show = false">
                            <button @click="show = true" type="button"
                                class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">
                                Hapus
                            </button>
                            <x-modal-my-listing type="Hotel" :item="$item" />
                        </a>
                        @endif
                        @if ($item->product->type == 'xstay')
                        <a href="{{ route('infodasar.edit', $item->product->product_code) }}">
                            <button type="submit"
                                class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">
                                Edit
                            </button>
                        </a>
                        <a x-data="showModal" @keydown.escape="show = false">
                            <button @click="show = true" type="button"
                                class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">
                                Hapus
                            </button>
                            <x-modal-my-listing type="Xstay" :item="$item" />
                        </a>
                        @endif
                        @if ($item->product->type == 'tour')
                        <a href="{{ route('tur.viewAddListingEdit', $item->product->id) }}">
                            <button type="submit"
                                class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">
                                Edit
                            </button>
                        </a>
                        <a x-data="showModal" @keydown.escape="show = false">
                            <button @click="show = true" type="button"
                                class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">
                                Hapus
                            </button>
                            <x-modal-my-listing type="Tur" :item="$item" />
                        </a>
                        @endif
                        @if ($item->product->type == 'activity')
                        <a href="{{ route('activity.viewAddListingEdit', $item->product->id) }}">
                            <button type="submit"
                                class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">
                                Edit
                            </button>
                        </a>
                        <a x-data="showModal" @keydown.escape="show = false">
                            <button @click="show = true" type="button"
                                class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">
                                Hapus
                            </button>
                            <x-modal-my-listing type="Activity" :item="$item" />
                        </a>
                        @endif
                        @if ($item->product->type == 'rental')
                        <a href="{{ route('rental.viewAddListingEdit', $item->product->id) }}">
                            <button type="submit"
                                class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">
                                Edit
                            </button>
                        </a>
                        <a x-data="showModal" @keydown.escape="show = false">
                            <button @click="show = true" type="button"
                                class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">
                                Hapus
                            </button>
                            <x-modal-my-listing type="Rental" :item="$item" />
                        </a>
                        @endif
                        @if ($item->product->type == 'transfer')
                        <a href="{{ route('transfer.viewAddListingEdit', $item->product->id) }}">
                            <button type="submit"
                                class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">
                                Edit
                            </button>
                        </a>
                        <a x-data="showModal" @keydown.escape="show = false">
                            <button @click="show = true" type="button"
                                class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">
                                Hapus
                            </button>
                            <x-modal-my-listing type="Transfer" :item="$item" />
                        </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @empty
        @endforelse
        {{-- {{ $data_listing->links() }} --}}


        {{-- <div class="bg-white my-5 rounded-md shadow-lg">
            <div class="grid grid-cols-12">
                <div class="grid place-content-center">
                    <input class="checked:bg-kamtuu-second" type="checkbox" name="" id="">
                </div>
                <div class="col-span-2 m-5">
                    <img src="https://via.placeholder.com/100x100" class="w-[150px] h-[150px] inline-flex rounded-lg"
                        alt="" title="">
                </div>
                <div class="m-5 col-start-4 col-span-5">
                    <p class="text-lg font-bold">Nama Kendaraan</p>
                    <div class="flex py-2 gap-1 lg:gap-2">
                        <div
                            class="bg-[#9E3D64] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                            Transfer</div>
                        <div
                            class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                            Sedan</div>
                    </div>
                    <div class="flex">
                        <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}"
                            alt="rating" width="17px" height="17px">
                        <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                        <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}"
                            alt="rating" width="17px" height="17px">
                        <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                    </div>
                    <div class="pt-8 flex gap-2 items-center">
                        <p class="text-sm text-[#51B449] font-semibold">Tersedia</p>
                    </div>
                </div>
                <div class="m-5 col-span-4">
                    <p class="px-5">Aktif</p>
                    <div class="flex gap-2 pt-8 items-center">
                        <p class="font-semibold text-md">Ketersediaan</p>
                        <button type="submit"
                            class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">Edit</button>
                        <button type="submit"
                            class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">Hapus</button>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
    </div>

    @livewireScripts
    {{--Calendar--}}
    <script>
        // Slide 1
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepickerKetersediaan1').datepicker({
        minDate: today
    });
    // Slide 2
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepickerKetersediaan2').datepicker({
        minDate: today
    });
    // Slide 3
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepickerKetersediaan3').datepicker({
        minDate: today
    });
    // Slide 4
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepickerKetersediaan4').datepicker({
        minDate: today
    });
    // Slide 5
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepickerKetersediaan5').datepicker({
        minDate: today
    });
    </script>

    {{--Switch On Off--}}
    <script>
    $(function() {
        $('.toggle-class').change(function() {
            var status = $(this).prop('checked') == true ? 'publish' : 'draft';
            var id_product = $(this).data('id');

            $.ajax({
                type: "GET",
                dataType: "json",
                url: '/dahsboard/mylisting/update/status',
                data: {'available': status, 'id': id_product},
                success: function(data){
                    console.log(data.success)
                }
            });
            setTimeout(() => {
                document.location.reload();
            }, 500);
        })
        
        $("#filterActiveNonActive").on("change",function(){
            var status = $(this).val();
            console.log(status)
            document.location.replace('/dashboard/myListing?available='+status)
            // $.ajax({
            //     type:'GET',
            //     dataType:false,
            //     chace:false,
            //     url:'/dashboard/myListing',
            //     data:{
            //         :status
            //     },
            //     success:function(data){
            //         console.log(data)
            //         // document.location.replace('/dashboard/myListing?available='+status)
            //     }
            // })
        });

        $("#allChecked").on("click",function(){
            //alert('test')
            //('input:checkbox').attr('checked','checked');
            if (this.checked) {
                $(".list-product-check").each(function(){
                    this.checked=true
                })
            }
            else{
               $(".list-product-check").each(function(){
                    this.checked=false
                }) 
            }
        });  

        $("#typeFilter").on("change",function(){
            var type = $(this).val()
            console.log(type)

            if(type!="-- Kategori --"){
                //alert(type)
                document.location.replace('/dashboard/myListing?type='+type)
            }else{
                document.location.replace('/dashboard/myListing')
            }
            
        })

        $("#searchProduct").on("click",function(){
            var search = $("input[name='search']").val();
            document.location.replace('/dashboard/myListing?search='+search)
        })
       
        function redirect(url){
            document.location.replace(url);
        }

        $("#addListing").on("click",function(){
            //alert('test');
            var product_type = $("#typeFilter").val()
            console.log(product_type)
            if(product_type == '-- Kategori --'){
                alert('Silahkan pilih kategory terlebih dahulu');
                return;
            }

            if(product_type =='tour'){
                redirect('/dashboard/seller/tur/add-informasi')
            }

            if(product_type =='rental'){
                redirect('/dashboard/seller/rental/add-listing')
            }

            if(product_type =='transfer'){
                redirect('/dashboard/seller/transfer/add-informasi')
            }

            if(product_type =='hotel'){
                redirect('/hotel/informasidasar')
            }

            if(product_type =='xstay'){
                redirect('/xstay/informasidasar')
            }
            if(product_type =='activity'){
                redirect('/dashboard/seller/activity/add-detail')
            }
        })
    })
    </script>

    {{--Display Image--}}
    <script>
        function displayImage() {

        return {
            imageUrl: '',

            selectedFile(event) {
                this.fileToUrl(event, src => this.imageUrl = src)
            },

            fileToUrl(event, callback) {
                if (!event.target.files.length) return

                let file = event.target.files[0],
                    reader = new FileReader()

                reader.readAsDataURL(file)
                reader.onload = e => callback(e.target.result)
            },
        }
    }

    function showModal() {

        return {
            show: false
        }
    }
    </script>
</body>

</html>