<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    
    <link href="https://unpkg.com/filepond@^4/dist/filepond.css" rel="stylesheet" />
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);
        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity:1;
            background-color: rgba(249,250,251,var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }
        #transfer:checked+#transfer {
            display: block;
        }
        #hotel:checked+#hotel {
            display: block;
        }
        #rental:checked+#rental {
            display: block;
        }
        #activity:checked+#activity {
            display: block;
        }
        #xstay:checked+#xstay {
            display: block;
        }
        input[type='file'] {
            color: transparent;
        }
    </style>
    <!-- Styles -->
    @livewireStyles   
</head>
<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.seller.navbar-seller></x-be.seller.navbar-seller>   
    
    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko></x-be.kelolatoko.sidebar-toko>
        <div class="col-start-3 col-end-11 z-0">
            <form action="{{ route('kelolatoko.addBannerToko') }}" enctype="multipart/form-data" method="POST">  
                @csrf   
                <div class="mt-5 px-5 w-full">
                    <h1 class="font-semibold text-[#333333] mb-5 font-bold text-5">Banner Position</h1>
                    {!! $allbanner_id !!} 
                    {{-- @dump($banners) --}}
                    <input type="text" id="all_banner" name="all_banner" value="{{ isset($banner->id) ? $banner->id:null}}" readonly>

                    <div class="mb-5" x-data="bannerOne()">
                        <label class="text-sm text-bg-gray-900">Link banner
                            <input class="block mb-2 text-sm w-full border rounded-lg py-2 p-2.5" name="link_banner[]" type="input">
                        </label>
                        <input class="block mb-2 w-full text-sm text-slate-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-[#9E3D64] file:text-white hover:file:bg-violet-100 hover:file:text-[#9E3D64]" type="file" name="gallery-image[]" accept="image/*" @change="selectedFile" x-model="bannerOne"> 
                        <template x-if="bannerOne && !savedImage">
                            <div class="flex">
                                <img :src="bannerOne" class="object-contain rounded border border-gray-300 w-full h-auto mr-5" alt="upload-featured-photo">
                            </div>
                        </template>
                        <template x-if="!bannerOne && !savedImage">
                            <div class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-auto h-[240px]">
                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px" height="20px">
                            </div> 
                        </template>
                        <template x-if="savedImage">
                            @if (isset($banners[0]['banner']))
                            <div class="flex">
                                <img src="{{ asset($banners[0]['banner']) }}" class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5" alt="upload-featured-photo">
                            </div>
                            @endif
                        </template>@if(count($errors) > 0)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger">{{ $error }}</div>
                            @endforeach
                        @endif
                    </div> 
                    <div class="grid grid-cols-3">
                        <div class="mb-5" x-data="bannerTwo()">
                            <label class="text-sm text-bg-gray-900">Link banner
                                <input class="block mb-2 text-sm w-full border rounded-lg py-2 p-2.5" name="link_banner[]" type="input" style="width:311px;">
                            </label>
                            <input class="mb-2 block w-full text-sm text-slate-500 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100" type="file" name="gallery-image[]" accept="image/*" @change="selectedFile" x-model="bannerTwo"> 
                            <template x-if="bannerTwo && !savedImage">
                                <div class="flex">
                                    <img :src="bannerTwo" class="object-contain rounded border border-gray-300 w-[200px] h-auto" alt="upload-featured-photo">
                                </div>
                            </template>
                            <template x-if="!bannerTwo && !savedImage">
                                <div class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[200px] h-[200px]">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px" height="20px">
                                </div> 
                            </template>
                            <template x-if="savedImage">
                                @if (isset($banners[1]['banner']))
                                <div class="flex">
                                    <img src="{{ asset($banners[1]['banner']) }}" class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5" alt="upload-featured-photo">
                                </div>
                                @endif
                            </template>
                        </div>
                        <div class="mb-5" x-data="bannerThree()">
                            <label class="text-sm text-bg-gray-900">Link banner
                                <input class="block mb-2 text-sm w-full border rounded-lg py-2 p-2.5" name="link_banner[]" type="input" style="width:311px;">
                            </label>
                            <input class="mb-2 block w-full text-sm text-slate-500 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100" type="file" name="gallery-image[]" accept="image/*" @change="selectedFile" x-model="bannerThree"> 
                            <template x-if="bannerThree && !savedImage">
                                <div class="flex">
                                    <img :src="bannerThree" class="object-contain rounded border border-gray-300 w-[200px] h-auto" alt="upload-featured-photo">
                                </div>
                            </template>
                            <template x-if="!bannerThree && !savedImage">
                                <div class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[200px] h-[200px]">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px" height="20px">
                                </div> 
                            </template>
                            <template x-if="savedImage">
                                @if (isset($banners[2]['banner']))
                                <div class="flex">
                                    <img src="{{ asset($banners[2]['banner']) }}" class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5" alt="upload-featured-photo">
                                </div>
                                @endif
                            </template>
                        </div>
                        <div class="mb-5" x-data="bannerFour()">
                            <label class="text-sm text-bg-gray-900">Link banner
                                <input class="block mb-2 text-sm w-full border rounded-lg py-2 p-2.5" name="link_banner[]" type="input" style="width:311px;">
                            </label>
                            <input class="mb-2 block w-full text-sm text-slate-500 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100" type="file" name="gallery-image[]" accept="image/*" @change="selectedFile" x-model="bannerFour"> 
                            <template x-if="bannerFour && !savedImage">
                                <div class="flex">
                                    <img :src="bannerFour" class="object-contain rounded border border-gray-300 w-[200px] h-auto" alt="upload-featured-photo">
                                </div>
                            </template>
                            <template x-if="!bannerFour && !savedImage">
                                <div class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[200px] h-[200px]">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px" height="20px">
                                </div> 
                            </template>
                            <template x-if="savedImage">
                                @if (isset($banners[3]['banner']))
                                <div class="flex">
                                    <img src="{{ asset($banners[3]['banner']) }}" class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5" alt="upload-featured-photo">
                                </div>
                                @endif
                            </template>
                        </div>
                    </div>

                    <div class="grid grid-cols-2">
                        <div class="mb-5" x-data="bannerFive()">
                            <label class="text-sm text-bg-gray-900">Link banner
                                <input class="block mb-2 text-sm w-full border rounded-lg py-2 p-2.5" name="link_banner[]" type="input" style="width: 451px;">
                            </label>
                            <input class="block mb-2 w-full text-sm text-slate-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100" type="file" name="gallery-image[]" accept="image/*" @change="selectedFile" x-model="bannerFive"> 
                            <template x-if="bannerFive && !savedImage">
                                <div class="flex">
                                    <img :src="bannerFive" class="object-contain rounded border border-gray-300 w-[720px] h-auto mr-5" alt="upload-featured-photo">
                                </div>
                            </template>
                            <template x-if="!bannerFive && !savedImage">
                                <div class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-auto h-[240px]" style="width:512px;">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px" height="20px">
                                </div> 
                            </template>
                            <template x-if="savedImage">
                                @if (isset($banners[4]['banner']))
                                <div class="flex">
                                    <img src="{{ asset($banners[4]['banner']) }}" class="object-contain rounded border border-gray-300  w-auto h-[240px] mr-5" alt="upload-featured-photo">
                                </div>
                                @endif
                            </template>
                        </div>
                        <div class="mb-5" x-data="bannerSix()">
                            <label class="text-sm text-bg-gray-900">Link banner
                                <input class="block mb-2 text-sm w-full border rounded-lg py-2 p-2.5" name="link_banner[]" type="input" style="width: 451px;">
                            </label>
                            <input class="block mb-2 w-full text-sm text-slate-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100" type="file" name="gallery-image[]" accept="image/*" @change="selectedFile" x-model="bannerFive"> 
                            <template x-if="bannerSix && !savedImage">
                                <div class="flex">
                                    <img :src="bannerSix" class="object-contain rounded border border-gray-300 w-[720px] h-auto mr-5" alt="upload-featured-photo">
                                </div>
                            </template>
                            <template x-if="!bannerSix && !savedImage">
                                <div class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-auto h-[240px]">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px" height="20px">
                                </div> 
                            </template>
                            <template x-if="savedImage">
                                @if (isset($banners[5]['banner']))
                                <div class="flex">
                                    <img src="{{ asset($banners[5]['banner']) }}" class="object-contain rounded border border-gray-300  w-auto h-[240px] mr-5" alt="upload-featured-photo" style="width:512px;">
                                </div>
                                @endif
                            </template>
                        </div>
                    </div>
                    

                </div>
                <div class="p-5">
                    <div class="grid grid-cols-6">
                        <div class="col-end-8 col-end-2">
                            <button type="submit" class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                                Simpan   
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>    
    
    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    
    <script>
        function bannerOne() {

            return {
                bannerOne: "{!! isset($banners[0]['banner']) ? $banners[0]['banner'] : null !!}",
                savedImage: "{!! isset($banners[0]['banner']) ? $banners[0]['banner'] : null !!}",

                selectedFile(event) {
                    this.fileToUrl(event, src => this.bannerOne = src)
                    this.savedImage = ''
                },

                fileToUrl(event, callback) {
                    if (! event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },

                removeImage() {
                    this.bannerOne = '';
                }
            }
        }
    </script>    
    <script>
        function bannerTwo() {

            return {
                bannerTwo: "{!! isset($banners[1]['banner']) ? $banners[1]['banner'] : null !!}",
                savedImage: "{!! isset($banners[1]['banner']) ? $banners[1]['banner'] : null !!}",

                selectedFile(event) {
                    this.fileToUrl(event, src => this.bannerTwo = src)
                    this.savedImage = ''
                },

                fileToUrl(event, callback) {
                    if (! event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },

                removeImage() {
                    this.bannerTwo = '';
                }
            }
        }
    </script>    
    <script>
        function bannerThree() {

            return {
                bannerThree: "{!! isset($banners[3]['banner']) ? $banners[3]['banner'] : null !!}",
                savedImage: "{!! isset($banners[2]['banner']) ? $banners[2]['banner'] : null !!}",

                selectedFile(event) {
                    this.fileToUrl(event, src => this.bannerThree = src)
                    this.savedImage = ''
                },

                fileToUrl(event, callback) {
                    if (! event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },

                removeImage() {
                    this.bannerThree = '';
                }
            }
        }
    </script>    
    <script>
        function bannerFour() {

            return {
                bannerFour: "{!! isset($banners[3]['banner']) ? $banners[3]['banner'] : null !!}",
                savedImage: "{!! isset($banners[3]['banner']) ? $banners[3]['banner'] : null !!}",

                selectedFile(event) {
                    this.fileToUrl(event, src => this.bannerFour = src)
                    this.savedImage = ''
                },

                fileToUrl(event, callback) {
                    if (! event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },

                removeImage() {
                    this.bannerFour = '';
                }
            }
        }
    </script>    
    <script>
        function bannerFive() {

            return {
                bannerFive: "{!! isset($banners[4]['banner']) ? $banners[4]['banner'] : null !!}",
                savedImage: "{!! isset($banners[4]['banner']) ? $banners[4]['banner'] : null !!}",

                selectedFile(event) {
                    this.fileToUrl(event, src => this.bannerFive = src)
                    this.savedImage = ''
                },

                fileToUrl(event, callback) {
                    if (! event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },

                removeImage() {
                    this.bannerFive = '';
                }
            }
        }
    </script>
        <script>
        function bannerSix() {

            return {
                bannerSix: "{!! isset($banners[5]['banner']) ? $banners[5]['banner'] : null !!}",
                savedImage: "{!! isset($banners[5]['banner']) ? $banners[5]['banner'] : null !!}",

                selectedFile(event) {
                    this.fileToUrl(event, src => this.bannerSix = src)
                    this.savedImage = ''
                },

                fileToUrl(event, callback) {
                    if (! event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },

                removeImage() {
                    this.bannerSix = '';
                }
            }
        }
    </script>
    <script>
        $(document).ready(function() {
            var dropIndex;
            $("#image-list-sports").sortable({
                update: function(event, ui) {
                    dropIndex = ui.item.index();
                }
            });
            $('#submit-sports').click(function(e) {
                var imageIdsArray = [];
                $('#image-list-sports li').each(function(index) {
                    var id = $(this).attr('id');
                    var split_id = id.split("_");
                    console.log(id);
                    imageIdsArray.push(split_id[1]);
                });
                $.ajax({
                    method: 'post', // Type of response and matches what we said in the route
                    url: '/sport/store/reorder', // This is the url we gave in the route
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'data': imageIdsArray,
                        'id':window.location.pathname.substring(21, window.location.pathname.length)
                    }, // a JSON object to send back
                    success: function(response) { // What to do if we succeed
                        console.log({
                            response
                        });
                        if (response.redirect) {
                            // here you actually redirect to the new page
                            window.location = response.redirect;
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
                e.preventDefault();
            });
        });
    </script>
</body>
</html>