<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        h1 {
            font-size: 30px;
            color: #000;
        }

        h2 {
            font-size: 20px;
            color: #000;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>

    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller>
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5">
                <div class="sm:grid sm:grid-cols-2">
                    <div class="p-2">
                        <div>
                            <h1 class="font-bold font-inter">Favorit</h1>
                        </div>
                    </div>
                    <div class="pb-2 sm:p-2">
                        <div>
                            <div class="grid sm:justify-items-end">
                                <select name="" id=""
                                    class="appearance-none ring:bg-kamtuu-primary block w-full sm:w-60 px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                    <option value="">Kategori</option>
                                    <option value="">1</option>
                                    <option value="">2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- content --}}
                <div class="md:grid md:grid-cols-2 space-y-3 md:space-y-0 md:gap-5">
                    @foreach ($datas as $data)
                    {{-- @dump($data) --}}
                    <div
                        class="drop-shadow-md pb-1 duration-200 rounded md:hover:drop-shadow-xl md:hover:-translate-y-1 h-auto">
                        <div class="">
                            <div class="w-auto flex bg-[#ffff] rounded">
                                <div class="w-full">
                                    <div class="sm:flex justify-between">
                                        <div class="flex">
                                            <div class="p-2 shrink-0">
                                                <img src="{{ asset($data['thumbnail']) }}" alt="user-profile"
                                                    class="object-cover object-center w-20 h-20 rounded-xl sm:w-32 sm:h-32 p-2 overflow-hidden border-indigo-600 ">
                                            </div>
                                            <div class="p-2 w-full">
                                                <div class="flex justify-between">
                                                    <p class="text-[#000] text-base pb-2" style="font-size:small">
                                                        {{-- Hotel --}}
                                                        {{ strtoupper($data['type']) }}
                                                    </p>
                                                    <img src="{{ asset('storage/icons/bookmark-solid 1.svg') }}"
                                                        class="w-[18px] h-[18px] mt-1 block sm:hidden" alt="user"
                                                        title="user">
                                                </div>

                                                <p class="text-[#000] text-base font-bold line-clamp-2"
                                                    style="vertical-align: text-top">
                                                    {{-- Hotel Cavinton Yogyakarta --}}
                                                    {{$data['product_name']}}
                                                </p>
                                                @if(isset($data['detail']))
                                                @foreach($data['detail'] as $detail)

                                                <p class="text-[#000] text-base" style="font-size:small">
                                                    @if($detail['alamat'])
                                                    {{$detail['alamat']}}
                                                    @else
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </p>
                                                @php
                                                $rating = App\Models\reviewPost::where('product_id',
                                                $data['id'])->sum('star_rating');
                                                $review_count = App\Models\reviewPost::where('product_id',
                                                $data['id'])->get();
                                                @endphp
                                                <div class="flex items-center pt-3">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[16px] h-[15px] mt-1" alt="user" title="user">
                                                    <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter bold text-[#000] text-base"
                                                        style="font-size:small">
                                                        {{ $rating }} ({{ count($review_count) }} Ulasan)
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="p-2 grid justify-items-end shrink-0" style="vertical-align: top">
                                            <img src="{{ asset('storage/icons/bookmark-solid 1.svg') }}"
                                                class="w-[18px] h-[18px] mt-1 hidden sm:block" alt="user" title="user">
                                            <div class="grid content-end">
                                                <h2 class="text-[#23AEC1] font-bold font-inter">
                                                    {{-- IDR @dump($detail['harga']) --}}
                                                    IDR {{ number_format($detail['harga']['dewasa_residen']) }}
                                                </h2>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </x-be.traveller.sidebar-traveller>

</body>

</html>
