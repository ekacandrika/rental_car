<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        h1 {
            font-size: 30px;
            color: #000;
        }

        h2 {
            font-size: 20px;
            color: #000;
        }

        [x-cloak] {
            display: none;
        }

        .duration-300 {
            transition-duration: 300ms;
        }

        .ease-in {
            transition-timing-function: cubic-bezier(0.4, 0, 1, 1);
        }

        .ease-out {
            transition-timing-function: cubic-bezier(0, 0, 0.2, 1);
        }

        .scale-90 {
            transform: scale(.9);
        }

        .scale-100 {
            transform: scale(1);
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>

    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller>
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5">
                <h1 class="font-bold font-inter">Hapus Akun</h1>

                {{-- Data Diri --}}
                <div x-data="{ 'showModal': false }" @keydown.escape="showModal = false" x-cloak>
                    <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                        <div class="">
                            <div class="w-auto flex bg-[#ffff] rounded">
                                <div class="w-full">
                                    <div class="grid grid-cols-1 p-5">
                                        <div class="p-2">
                                            <div>
                                                <h2 class="font-bold text-[#000] text-center sm:text-left">
                                                    Setelah menghapus akun, Anda tidak dapat mengembalikannya kembali.
                                                </h2>
                                            </div>
                                            <div class="pt-7 grid justify-items-center sm:justify-items-start">
                                                <button type="button" @click="showModal = true"
                                                    class="flex px-8 py-2 lg:text-md md:text-base text-white duration-300 bg-[#D50006] rounded-lg hover:bg-kamtuu-primary hover:text-white">
                                                    <img src="{{ asset('storage/icons/trash-white.png') }}"
                                                        class="w-[16px] h-[16px] mt-1" alt="hapus-icon" title="user">
                                                    <p
                                                        class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">
                                                        Hapus Akun
                                                    </p>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Overlay-->
                    <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="showModal"
                        :class="{ 'absolute inset-0 z-10 flex items-center justify-center': showModal }">
                        <!--Dialog-->
                        <div class="bg-white w-11/12 xl:max-w-xl mx-auto rounded shadow-lg py-4 text-left px-6"
                            x-show="showModal" @click.away="showModal = false"
                            x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0 scale-90"
                            x-transition:enter-end="opacity-100 scale-100" x-transition:leave="ease-in duration-300"
                            x-transition:leave-start="opacity-100 scale-100"
                            x-transition:leave-end="opacity-0 scale-90">

                            <!--Title-->
                            <div class="flex justify-between items-center pb-3">
                                <p class="text-2xl font-bold"></p>
                                <div class="cursor-pointer z-20" @click="showModal = false">
                                    <svg class="fill-current text-black" xmlns="http://www.w3.org/2000/svg" width="18"
                                        height="18" viewBox="0 0 18 18">
                                        <path
                                            d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                        </path>
                                    </svg>
                                </div>
                            </div>

                            <!-- content -->
                            <p class="text-2xl font-bold text-center p-5">Apakah Anda yakin akan menghapus akun?</p>
                            <p class="text-center pb-5">Akun yang sudah dihapus tidak dapat dikembalikan lagi.</p>

                            <!--Footer-->
                            <div class="flex justify-center pt-5">
                                <button
                                    class="px-8 py-2 bg-transparent p-3 rounded-lg border border-red-600 text-[#D50006] hover:bg-kamtuu-primary hover:text-dark-400 mr-2"
                                    @click="alert('Additional Action');">Kembali</button>
                                <div x-data="{ 'selanjutnya': false }" @keydown.escape="selanjutnya = false" x-cloak>
                                    <button type="button" @click="selanjutnya = true"
                                        class="px-8 py-2 lg:text-md md:text-base text-white duration-300 bg-[#D50006] rounded-lg hover:bg-kamtuu-primary hover:text-white">Selanjutnya</button>

                                    <!--Overlay-->
                                    <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)"
                                        x-show="selanjutnya"
                                        :class="{ 'absolute inset-0 z-20 flex items-center justify-center': selanjutnya }">
                                        <!--Dialog-->
                                        <div class="bg-white w-11/12 xl:max-w-xl mx-auto rounded shadow-lg py-10 text-left px-6"
                                            x-show="selanjutnya" @click.away="selanjutnya = false"
                                            x-transition:enter="ease-out duration-300"
                                            x-transition:enter-start="opacity-0 scale-90"
                                            x-transition:enter-end="opacity-100 scale-100"
                                            x-transition:leave="ease-in duration-300"
                                            x-transition:leave-start="opacity-100 scale-100"
                                            x-transition:leave-end="opacity-0 scale-90">

                                            <!--Title-->
                                            <div class="flex justify-between items-center pb-3">
                                                <p class="text-2xl font-bold"></p>
                                                <div class="cursor-pointer z-60" @click="selanjutnya = false">
                                                    <svg class="fill-current text-black"
                                                        xmlns="http://www.w3.org/2000/svg" width="18" height="18"
                                                        viewBox="0 0 18 18">
                                                        <path
                                                            d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                                        </path>
                                                    </svg>
                                                </div>
                                            </div>

                                            <!-- content -->
                                            <p class="text-sm text-center p-5">Kami berkomitmen untuk terus melakukan
                                                perbaikan
                                                dalam segala hal.</p>
                                            <p class="text-2xl font-bold text-center pb-5">Maukah Anda beritahukan kami
                                                alasan menghapus akun? </p>

                                            <p class="border border-gray-200 rounded-lg text-sm text-justify p-5">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis
                                                molestie, dictum est a, mattis tellus. Sed dignissim, metus nec
                                                fringilla
                                                accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed
                                                risus.
                                                Maecenas eget condimentum velit, sit amet feugiat lectus. Class aptent
                                                taciti sociosqu ad litora torquent per conubia nostra, per inceptos
                                                himenaeos. Praesent auctor purus luctus enim egestas, ac scelerisque
                                                ante
                                                pulvinar. Donec ut rhoncus ex. Suspendisse ac rhoncus nisl, eu tempor
                                                urna.
                                                Curabitur vel bibendum lorem. Morbi convallis convallis diam sit amet
                                                lacinia. Aliquam in elementum tellus.
                                            </p>

                                            <!--Footer-->
                                            <div class="flex justify-center pt-5">
                                                <button
                                                    class="px-4 bg-transparent p-3 rounded-lg border border-red-600 text-[#D50006] hover:bg-kamtuu-primary hover:text-dark-400 mr-2"
                                                    @click="alert('Additional Action');">Kembali</button>
                                                <form
                                                    action="{{ route('traveller.destroy.account', auth()->user()->id) }}"
                                                    method="POST"
                                                    class="modal-close px-4 h-[100] text-white bg-[#D50006] rounded-lg hover:bg-kamtuu-primary hover:text-white">
                                                    @csrf
                                                    @method('delete')
                                                    <button
                                                        class="modal-close px-4 pt-3 text-white bg-[#D50006] rounded-lg hover:bg-kamtuu-primary hover:text-white"
                                                        @click="selanjutnya = false">Hapus Akun</button>
                                                </form>
                                            </div>


                                        </div>
                                        <!--/Dialog -->
                                    </div><!-- /Overlay -->
                                </div>
                            </div>
                        </div>
                        <!--/Dialog -->
                    </div><!-- /Overlay -->
                </div>

            </div>
        </div>
    </x-be.traveller.sidebar-traveller>

</body>

</html>
