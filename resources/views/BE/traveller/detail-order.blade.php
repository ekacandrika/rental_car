<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #report:checked+#report {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">

    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>
    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller>
        <div class="col-start-3 col-end-11 z-0">
            {{--  style="padding-left: 315px" --}}
            <div class="p-5 pr-10">
                <div class="flex mb-5">
                    <a href="{{ route('cart') }}">
                        <img id="image" src="{{ asset('storage/icons/chevron-left-arrow.svg') }}"
                            class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    </a>
                    <label for="image" class="mx-3">Detail Pemesanan</label>
                </div>
                <div class="bg-gray-200 rounded-md border border-gray-400 max-w-6xl p-5">
                    <div class="grid grid-cols-[10%_80%_10%]">
                        @if ($produk->type == 'hotel' || $produk->type == 'xstay' )
                        <div class="grid justify-items-center">
                            <img class="w-28 h-28 rounded-md border border-gray-300 bg-cover object-cover" src={{
                                isset($detail_produk->foto_maps_2) ? asset($detail_produk->foto_maps_2) :
                            "https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                            }}
                            alt="thumbnail_produk">
                        </div>
                        @else
                        <div class="grid justify-items-center">
                            <img class="w-28 h-28 rounded-md border border-gray-300 bg-cover object-cover" src={{
                                isset($detail_produk->thumbnail)
                            ? asset($detail_produk->thumbnail) :
                            "https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                            }}
                            alt="thumbnail_produk">
                        </div>
                        @endif
                        <div class="mx-3">
                            <p>No Booking: {{ $data->booking_code }}</p>
                            <p class="font-semibold">{{ $produk->product_name }}</p>
                            <p class="capitalize">{{ $produk->type }}</p>
                            @if ($data->confirm_order == null)
                            <p class="text-green-600">Telah dikonfirmasi</p>
                            @endif
                            @if ($data->confirm_order == 'reject')
                            <p class="text-red-500">Dibatalkan</p>
                            @endif
                            @if ($data->confirm_order == 'waiting')
                            <p class="text-kamtuu-third">Menunggu konfirmasi</p>
                            @endif
                            @if ($data->confirm_order == 'by_seller')
                            <p class="text-kamtuu-third">Belum dikonfirmasi</p>
                            @endif
                            @if ($data->confirm_order == 'instant')
                            <p class="text-green-600">Konfirmasi instant</p>
                            @endif
                        </div>
                        <div class="grid col-span-3 mt-5">
                            @php
                            $today = new DateTime();
                            if (isset($data->check_in_date)) {
                            $checkIn = DateTime::createFromFormat('Y-m-d', $data->check_in_date);
                            }
        
                            if (isset($data->activity_date)) {
                            $checkIn = date('Y-m-d', strtotime($data->activity_date));
                            $checkIn = DateTime::createFromFormat('Y-m-d', $checkIn);
                            }
        
                            $diff = $today->diff($checkIn);
                            $get_diff = (int)$diff->format('%r%a');
                            $days = $diff->format('%a');
                            $hours = $diff->format('%h');
                            $minutes = $diff->format('%i');
                            @endphp
                            @if ($get_diff > 0)
                            <p>AKAN berlangsung dalam <span class="text-yellow-600">
                                    {{ $days }} hari {{ $hours }} jam {{ $minutes }} menit
                                </span>
                            </p>
                            @endif
                            @if ($get_diff < 0) <p>TELAH berlangsung dalam <span class="text-yellow-600">
                                    {{ $days }} hari {{ $hours }} jam {{ $minutes }} menit
                                </span>
                                </p>
                                @endif
                                @if ($get_diff == 0)
                                <p>SEDANG berlangsung</p>
                                @endif
                                <p class="text-xl font-semibold">{{ $user->first_name }}</p>
                                <p>{{ $produk->product_name }}</p>
                        </div>
                        @if ($produk->type == 'hotel')
                    <div class="col-span-3 border border-gray-600 rounded-md p-5">
                        <p>Check-In : {{ Carbon\Carbon::parse($data->check_in_date)->translatedFormat('d F Y') }}
                        </p>
                        <p>Check-Out : {{ Carbon\Carbon::parse($data->check_out_date)->translatedFormat('d F Y') }}
                        </p>
                        <p>Tamu :</p>
                        <p>{{ $data->peserta_dewasa > 0 ? ($data->peserta_dewasa . ' Dewasa') : '' }} {{
                            $data->peserta_anak > 0 ? (' • ' . $data->peserta_anak . ' Anak') :
                            ''
                            }}
                            {{ $data->peserta_balita > 0 ? (' • ' . $data->peserta_balita . ' Balita') :
                            '' }}</p>
                    </div>
                    @endif
                    @if ($produk->type == 'xstay')
                    <div class="col-span-3 border border-gray-600 rounded-md p-5">
                        <p>Check-In : {{ Carbon\Carbon::parse($data->check_in_date)->translatedFormat('d F Y') }}
                        </p>
                        <p>Check-Out : {{ Carbon\Carbon::parse($data->check_out_date)->translatedFormat('d F Y') }}
                        </p>
                        <p>Tamu :</p>
                        <p>{{ $data->peserta_dewasa > 0 ? ($data->peserta_dewasa . ' Dewasa') : '' }} {{
                            $data->peserta_anak > 0 ? (' • ' . $data->peserta_anak . ' Anak') :
                            ''
                            }}
                            {{ $data->peserta_balita > 0 ? (' • ' . $data->peserta_balita . ' Balita') :
                            '' }}</p>
                    </div>
                    @endif
                    @if ($produk->type == 'activity')
                    <div class="col-span-3 border border-gray-600 rounded-md p-5">
                        <p>Activity : {{ Carbon\Carbon::parse($data->activity_date)->translatedFormat('d F Y') }} {{
                            ' • ' . $data->nama_paket ?? '' }} {{ ' • ' . $data->waktu_paket ?? '' }}
                        </p>
                        <p>Peserta :</p>
                        <p>{{ $data->peserta_dewasa > 0 ? ($data->peserta_dewasa . ' Tiket Dewasa') : '' }} {{
                            $data->peserta_anak > 0 ? (' • ' . $data->peserta_anak . ' Tiket Anak') :
                            ''
                            }}
                            {{ $data->peserta_balita > 0 ? (' • ' . $data->peserta_balita . ' Tiket Balita') :
                            '' }}</p>
                    </div>
                    @endif
                    {{-- <p>27 Agustus 2022 • Paket Pagi</p> --}}
    
                    <div class="col-span-3 border border-gray-600 rounded-md p-5 my-3">
                        <p class="text-2xl">Pembayaran</p>
                        @if ($produk->type == 'activity')
                        <div class="grid space-y-5">
                            <div>
                                <p class="font-bold">Harga Residen</p>
                                <div class="grid grid-cols-2">
                                    <div class="text-star">
                                        <p>Dewasa</p>
                                        <p>Anak-anak</p>
                                        <p>Balita</p>
                                    </div>
                                    <div class="text-end text-blue-500">
                                        <p>Rp. {{ number_format($data->harga_residen_dewasa ?? 0) }}
                                        </p>
                                        <p>Rp. {{ number_format($data->harga_residen_anak ?? 0) }}
                                        </p>
                                        <p>Rp. {{ number_format($data->harga_residen_balita ?? 0) }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <p class="font-bold">Harga Non-Residen</p>
                                <div class="grid grid-cols-2">
                                    <div class="text-star">
                                        <p>Dewasa</p>
                                        <p>Anak-anak</p>
                                        <p>Balita</p>
                                    </div>
                                    <div class="text-end text-blue-500">
                                        <p>Rp.
                                            {{ number_format($data->harga_non_residen_dewasa ?? 0) }}
                                        </p>
                                        <p>Rp.
                                            {{ number_format($data->harga_non_residen_anak ?? 0) }}
                                        </p>
                                        <p>Rp.
                                            {{ number_format($data->harga_non_residen_balita ?? 0) }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-5 h-px bg-slate-500 border-0 dark:bg-gray-700">
                        @endif
    
                        @if ($produk->type == 'hotel' || $produk->type == 'xstay')
                        <div class="grid grid-cols-2 my-5">
                            <div class="text-star">
                                <p class="font-bold">Harga {{ isset($data->jumlah_kamar) ? $data->jumlah_kamar . ' kamar
                                    x ' : 'Kamar' }}
                                    {{ isset($data->jumlah_malam) ? $data->jumlah_malam . ' malam' : '' }}</p>
                                <p>Diskon Per-Tanggal</p>
                                <p>Surcharge Per-Tanggal</p>
                            </div>
                            <div class="text-end text-blue-500">
                                <p>Rp. {{ number_format($data->harga_product ?? 0) }}</p>
                                <p class="text-red-600">- Rp. {{ number_format($data->diskon_per_tanggal * (-1) ?? 0) }}
                                </p>
                                <p>Rp. {{ number_format($data->surcharge_per_tanggal ?? 0) }}</p>
                            </div>
                        </div>
                        <hr class="my-5 h-px bg-slate-500 border-0 dark:bg-gray-700">
                        @endif
    
                        @if ($produk->type != 'hotel' && $produk->type != 'xstay')
                        <div class="grid grid-cols-2 my-5">
                            <div class="text-star">
                                <p class="font-bold">Harga</p>
                                <p>Diskon Per-Tanggal</p>
                                <p>Surcharge Per-Tanggal</p>
                            </div>
                            <div class="text-end text-blue-500">
                                <p>Rp. {{ number_format($data->harga_product ?? 0) }}</p>
                                <p class="text-red-600">- Rp. {{ number_format($data->diskon_per_tanggal * (-1) ?? 0) }}
                                </p>
                                <p>Rp. {{ number_format($data->surcharge_per_tanggal ?? 0) }}</p>
                            </div>
                        </div>
                        <hr class="my-5 h-px bg-slate-500 border-0 dark:bg-gray-700">
                        @endif
    
                        <div class="grid grid-cols-2">
                            <div class="text-star">
                                <p class="font-bold">Harga Per-Tanggal</p>
                                <p>Diskon Grup</p>
                            </div>
                            <div class="text-end text-blue-500">
                                <p>Rp. {{ number_format($data->harga_per_tanggal ?? 0) }}</p>
                                <p class="text-red-600">- Rp. {{ number_format($data->diskon_grup ?? 0) }}</p>
                            </div>
                        </div>
    
                        <hr class="my-5 h-px bg-slate-500 border-0 dark:bg-gray-700">
    
                        <div class="grid grid-cols-2">
                            <div class="text-star">
                                <p class="font-bold">Harga Akhir</p>
                                <p>Pilihan</p>
                                <p>Extra</p>
                                <p>Biaya Lain-lain</p>
                                <p>Biaya Pemesanan</p>
                                <p>Pajak</p>
                                <p>Promo</p>
                                <p>Kupon</p>
                            </div>
                            <div class="text-end text-blue-500">
                                <p>Rp. {{ number_format($data->harga_akhir ?? 0) }}</p>
                                <p>Rp. {{ number_format($data->harga_pilihan ?? 0) }}</p>
                                <p>Rp. {{ number_format($data->harga_ekstra * $data->jumlah_extra ?? 0) }}</p>
                                <p>Rp. 0</p>
                                <p>Rp. 0</p>
                                @if ($produk->type == 'hotel')
                                @php
                                $pajak_global = App\Models\PajakAdmin::first();
                                $pajak_local_hotel = App\Models\PajakLocal::where('produk_type', 'hotel')->first();
                                @endphp
                                @if ($pajak_local_hotel == null)
                                <p>{{ $pajak_global->markup }}%</p>
                                @else
                                @if ($pajak_local_hotel->markupLocal == null)
                                <p>{{ $pajak_global->markup }}%</p>
                                @else
                                @if ($pajak_local_hotel->statusMarkupLocal == 'percent')
                                <p>{{ $pajak_local_hotel->markupLocal }}%</p>
                                @else
                                <p>Rp. {{ number_format($pajak_local_hotel->markupLocal) }}</p>
                                @endif
                                @endif
                                @endif
                                @endif
                                @if ($produk->type == 'xstay')
                                @php
                                $pajak_global = App\Models\PajakAdmin::first();
                                $pajak_local_xstay = App\Models\PajakLocal::where('produk_type', 'xstay')->first();
                                @endphp
                                @if ($pajak_local_xstay == null)
                                <p>{{ $pajak_global->markup }}%</p>
                                @else
                                @if ($pajak_local_xstay->markupLocal == null)
                                <p>{{ $pajak_global->markup }}%</p>
                                @else
                                @if ($pajak_local_xstay->statusMarkupLocal == 'percent')
                                <p>{{ $pajak_local_xstay->markupLocal }}%</p>
                                @else
                                <p>Rp. {{ number_format($pajak_local_xstay->markupLocal) }}</p>
                                @endif
                                @endif
                                @endif
                                @endif
                                @if ($produk->type == 'Transfer')
                                @php
                                $pajak_global = App\Models\PajakAdmin::first();
                                $pajak_local_transfer = App\Models\PajakLocal::where('produk_type',
                                'transfer')->first();
                                @endphp
                                @if ($pajak_local_transfer == null)
                                <p>{{ $pajak_global->markup }}%</p>
                                @else
                                @if ($pajak_local_xstay->markupLocal == null)
                                <p>{{ $pajak_global->markup }}%</p>
                                @else
                                @if ($pajak_local_xstay->statusMarkupLocal == 'percent')
                                <p>{{ $pajak_local_xstay->markupLocal }}%</p>
                                @else
                                <p>Rp. {{ number_format($pajak_local_xstay->markupLocal) }}</p>
                                @endif
                                @endif
                                @endif
                                @endif
                                @if ($produk->type == 'Rental')
                                @php
                                $pajak_global = App\Models\PajakAdmin::first();
                                $pajak_local_rental = App\Models\PajakLocal::where('produk_type', 'rental')->first();
                                @endphp
                                @if ($pajak_local_rental == null)
                                <p>{{ $pajak_global->markup }}%</p>
                                @else
                                @if ($pajak_local_xstay->markupLocal == null)
                                <p>{{ $pajak_global->markup }}%</p>
                                @else
                                @if ($pajak_local_xstay->statusMarkupLocal == 'percent')
                                <p>{{ $pajak_local_xstay->markupLocal }}%</p>
                                @else
                                <p>Rp. {{ number_format($pajak_local_xstay->markupLocal) }}</p>
                                @endif
                                @endif
                                @endif
                                @endif
                                @if ($produk->type == 'activity')
                                @php
                                $pajak_global = App\Models\PajakAdmin::first();
                                $pajak_local_activity = App\Models\PajakLocal::where('produk_type',
                                'activity')->first();
                                @endphp
                                @if ($pajak_local_activity == null)
                                <p>{{ $pajak_global->markup }}%</p>
                                @else
                                @if ($pajak_local_activity->markupLocal == null)
                                <p>{{ $pajak_global->markup }}%</p>
                                @else
                                @if ($pajak_local_activity->statusMarkupLocal == 'percent')
                                <p>{{ $pajak_local_activity->markupLocal }}%</p>
                                @else
                                <p>Rp. {{ number_format($pajak_local_activity->markupLocal) }}</p>
                                @endif
                                @endif
                                @endif
                                @endif
                                <p class="text-red-600">- Rp. 0</p>
                                <p class="text-red-600">- Rp. 0</p>
                            </div>
                        </div>
                        <div class="grid grid-cols-2">
                            <div class="text-star">
                                <p class="font-bold">Sub Total</p>
                            </div>
                            <div class="text-end text-blue-500">
                                <p>Rp. {{ number_format($data->harga_asli) }}</p>
                            </div>
                        </div>
    
                        <hr class="my-5 h-px bg-gray-500 border-0 dark:bg-gray-700">
    
                        <div class="grid grid-cols-2">
                            <div class="text-star">
                                <p class="font-bold">Total</p>
                            </div>
                            <div class="text-end text-blue-500">
                                <p>Rp. {{ number_format($data->total_price) }}</p>
                            </div>
                        </div>
    
                    </div>
                    </div> 
                </div> 
            </div>    
        </div> 
    </x-be.traveller.sidebar-traveller>
</body>

</html>
