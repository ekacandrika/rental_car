<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <script src="https://code.jquery.com/jquery-3.5.0.min.js"></script>

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        #akun_mobile:checked+#akun_mobile {
            display: block;
        }

        #inbox_mobile:checked+#inbox_mobile {
            display: block;
        }

        h1 {
            font-size: 30px;
            color: #000;
        }

        h2 {
            font-size: 20px;
            color: #000;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>

    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller>
        <div class="z-0 col-start-3 col-end-11">
            <div class="px-3 sm:px-10 py-10">
                <h1 class="font-bold font-inter">Profile</h1>

                {{-- Data Diri --}}
                <div class="pb-5 mt-5 mb-5 rounded drop-shadow-xl">
                    <div class="">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <h2 class="p-5 font-bold text-[#9E3D64]">Data Diri</h2>
                                @if (session('success_data_diri'))
                                <div x-data="{ showMessage: true }" x-show="showMessage"
                                    class="bg-green-300 rounded-md p-3 m-5 flex justify-between"
                                    x-init="setTimeout(() => showMessage = false, 3000)">
                                    <p>{{ session('success_data_diri') }}</p>
                                </div>
                                @endif
                                <form action="{{ route('profile.updateDataDiri') }}" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="sm:flex mx-5">
                                        <div class="flex justify-center">
                                            @if (auth()->user()->profile_photo_path == null)
                                            <img src="{{ asset('storage/img/logo-traveller.png') }}" alt="user-profile"
                                                class="object-cover object-center overflow-hidden rounded-full w-28 h-28 border-[1px] border-[#9E3D64]">
                                            @else
                                            <img src="{{ asset(auth()->user()->profile_photo_path) }}"
                                                alt="user-profile"
                                                class="object-cover object-center overflow-hidden rounded-full w-28 h-28 border-[1px] border-[#9E3D64]">
                                            @endif
                                        </div>
                                        <div class="px-5" x-data="{files:''}">
                                            <p
                                                class="py-2 font-semibold text-[#000] text-base text-center sm:text-left">
                                                Foto
                                                Profile
                                            </p>
                                            <label for="profile_photo_path"
                                                class="grid sm:flex items-center justify-center sm:justify-start">
                                                <div
                                                    class="flex items-center px-3 py-2 font-bold text-[#23AEC1] bg-[#F2F2F2] rounded-xl cursor-pointer">
                                                    <img src="{{ asset('storage/icons/Vector-1.svg') }}"
                                                        class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                                                    <span
                                                        class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">
                                                        Pilih File</span>
                                                </div>
                                                <span class="px-2 text-slate-500 text-xs break-words"
                                                    x-text="files ? files.name : ''"></span>
                                            </label>
                                            <input class="px-5 hidden" type="file" name="profile_photo_path"
                                                id="profile_photo_path" x-on:change="files = $event.target.files[0]">
                                            <p class="pt-2 text-xs text-center">Foto berukuran maks. 2MB</p>
                                        </div>
                                    </div>
                                    {{-- <table class="mx-5">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    @if (auth()->user()->profile_photo_path == null)
                                                    <img src="{{ asset('storage/img/logo-traveller.png') }}"
                                                        alt="user-profile"
                                                        class="object-cover object-center overflow-hidden rounded-full w-28 h-28 border-[1px] border-[#9E3D64]">
                                                    @else
                                                    <img src="{{ asset(auth()->user()->profile_photo_path) }}"
                                                        alt="user-profile"
                                                        class="object-cover object-center overflow-hidden rounded-full w-28 h-28 border-[1px] border-[#9E3D64]">
                                                    @endif
                                                </td>
                                                <td class="px-5" x-data="{files:''}">
                                                    <p class="py-2 font-semibold text-[#000] text-base">Foto Profile
                                                    </p>
                                                    <label for="profile_photo_path" class="flex items-center">
                                                        <div
                                                            class="flex items-center px-3 py-2 font-bold text-[#23AEC1] bg-[#F2F2F2] rounded-xl cursor-pointer">
                                                            <img src="{{ asset('storage/icons/Vector-1.svg') }}"
                                                                class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                                                            <span
                                                                class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">
                                                                Pilih File</span>
                                                        </div>
                                                        <span class="px-2 text-slate-500"
                                                            x-text="files ? files.name : ''"></span>
                                                    </label>
                                                    <input class="px-5 hidden" type="file" name="profile_photo_path"
                                                        id="profile_photo_path"
                                                        x-on:change="files = $event.target.files[0]">
                                                    <p class="pt-2 text-xs">Foto berukuran maks. 2MB</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table> --}}


                                    <div class="sm:grid sm:grid-cols-2 p-5">
                                        <div class="p-2">
                                            <div>
                                                <label for="first_name" class="block text-sm font-medium leading-5">Nama
                                                    Depan</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <input id="first_name" name="first_name" type="text" required
                                                        value="{{ old('first_name', auth()->user()->first_name ?? '') }}"
                                                        class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div>
                                                <label for="last_name" class="block text-sm font-medium leading-5">Nama
                                                    Belakang</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <input id="last_name" name="last_name" type="text" required
                                                        value="{{ old('last_name', $profile->last_name ?? '') }}"
                                                        class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div>
                                                <label for="jk" class="block text-sm font-medium leading-5">Jenis
                                                    Kelamin</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select name="jk" id="jk"
                                                        class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                                        @if (auth()->user()->jk === "Laki-Laki")
                                                        <option value="Laki-Laki" selected>Laki-Laki</option>
                                                        <option value="perempuan">Perempuan</option>
                                                        @endif
                                                        @if (auth()->user()->jk === "Perempuan")
                                                        <option value="Laki-Laki">Laki-Laki</option>
                                                        <option value="perempuan" selected>Perempuan</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div>
                                                <label for="date_of_birth"
                                                    class="block text-sm font-medium leading-5">Tanggal
                                                    Lahir</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <input id="date_of_birth" name="date_of_birth" type="date" required
                                                        value="{{ old('date_of_birth', $profile->date_of_birth ?? '') }}"
                                                        class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($errors->hasBag('data_diri'))
                                    <div class="bg-red-300 rounded-md p-3 m-5">
                                        @foreach ($errors->data_diri->toArray() as $key=>$value)
                                        @foreach ($value as $index=>$err)
                                        <p>- {{ $err}}</p>
                                        @endforeach
                                        @endforeach
                                    </div>
                                    @endif
                                    <div class="grid p-5 justify-items-start">
                                        <button
                                            class="px-8 py-2 lg:text-md md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">Perbarui</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Alamat --}}
                <div class="pb-5 mt-5 mb-5 rounded drop-shadow-xl">
                    <div class="">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <h2 class="p-5 font-bold text-[#9E3D64]">Alamat</h2>
                                @if (session('success_alamat'))
                                <div x-data="{ showMessage: true }" x-show="showMessage"
                                    class="bg-green-300 rounded-md p-3 m-5 flex justify-between"
                                    x-init="setTimeout(() => showMessage = false, 3000)">
                                    <p>{{ session('success_alamat') }}</p>
                                </div>
                                @endif
                                <form action="{{ route('profile.updateDataAlamat') }}" method="POST">
                                    @csrf
                                    @method('PUT')

                                    <div class="sm:grid grid-cols-2 p-5">
                                        <div class="p-2">
                                            <div>
                                                <label for="address"
                                                    class="block text-sm font-medium leading-5">Alamat</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <textarea name="address" id="address" cols="10" rows="5" required
                                                        class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">{{ old('address', $profile->address ?? '') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div>
                                                <label for="provinsi"
                                                    class="block text-sm font-medium leading-5">Provinsi</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select name="provinsi" id="provinsi" required
                                                        class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                                        <option selected disabled>Pilih Provinsi</option>
                                                        @if ($profile != null)
                                                        @foreach ($provinsi as $provinsi_data)
                                                        @if ($profile->province_id == null)
                                                        <option value="{{ $provinsi_data->id }}">{{ $provinsi_data->name
                                                            }}
                                                        </option>
                                                        @endif

                                                        @if ($profile->province_id != null)
                                                        <option value="{{ $provinsi_data->id }}" {{($provinsi_data->id
                                                            ==
                                                            $profile->province_id) ? "selected":""}}>{{
                                                            $provinsi_data->name
                                                            }}</option>
                                                        @endif

                                                        @endforeach
                                                        @endif

                                                        @if ($profile == null)
                                                        @foreach ($provinsi as $provinsi_data)
                                                        <option value="{{ $provinsi_data->id }}">{{ $provinsi_data->name
                                                            }}
                                                        </option>
                                                        @endforeach
                                                        @endif

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div>
                                                <label for="kabupaten"
                                                    class="block text-sm font-medium leading-5">Kota/Kabupaten</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select name="kabupaten" id="kabupaten" required
                                                        class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                                        @if ($profile != null)
                                                        <option
                                                            value="{{ $profile->regency_id != null ? $profile->regency_id : '' }}"
                                                            selected>{{
                                                            $profile->regency_id !=
                                                            null ? $data['kabupaten']->name:"Pilih Kabupaten"}}</option>
                                                        @endif

                                                        @if ($profile != null)
                                                        <option>Pilih Kabupaten</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div>
                                                <label for="kecamatan"
                                                    class="block text-sm font-medium leading-5">Kecamatan</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select name="kecamatan" id="kecamatan" required
                                                        class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                                        @if ($profile != null)
                                                        <option
                                                            value="{{ $profile->district_id != null ? $profile->district_id : '' }}"
                                                            selected>{{
                                                            $profile->district_id !=
                                                            null ? $data['kecamatan']->name:"Pilih Kecamatan"}}</option>
                                                        @endif

                                                        @if ($profile != null)
                                                        <option>Pilih Kecamatan</option>
                                                        @endif

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div>
                                                <label for="kelurahan"
                                                    class="block text-sm font-medium leading-5">Kelurahan</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select name="kelurahan" id="kelurahan" required
                                                        class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                                        @if ($profile != null)
                                                        <option
                                                            value="{{ $profile->village_id != null ? $profile->village_id : '' }}"
                                                            selected>{{
                                                            $profile->village_id !=
                                                            null ? $data['kelurahan']->name:"Pilih Kelurahan"}}</option>
                                                        @endif

                                                        @if ($profile != null)
                                                        <option>Pilih Kelurahan</option>
                                                        @endif


                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div>
                                                <label for="postal_code"
                                                    class="block text-sm font-medium leading-5">Kode
                                                    Pos</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <input id="postal_code" name="postal_code" type="text" required
                                                        value="{{ old('postal_code', $profile->postal_code ?? '') }}"
                                                        class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($errors->hasBag('alamat'))
                                    <div class="bg-red-300 rounded-md p-3 m-5">
                                        @foreach ($errors->alamat->toArray() as $key=>$value)
                                        @foreach ($value as $index=>$err)
                                        <p>- {{ $err}}</p>
                                        @endforeach
                                        @endforeach
                                    </div>
                                    @endif
                                    <div class="grid p-5 justify-items-start">
                                        <button
                                            class="px-8 py-2 lg:text-md md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">Perbarui</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Email --}}
                <div class="pb-5 mt-5 mb-5 rounded drop-shadow-xl" x-data="emailHandler()">
                    <div class="">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full pb-3">
                                @if (session('success_add_secondary_email'))
                                <div x-data="{ showMessage: true }" x-show="showMessage"
                                    class="bg-green-300 rounded-md p-3 m-5 flex justify-between"
                                    x-init="setTimeout(() => showMessage = false, 3000)">
                                    <p>{{ session('success_add_secondary_email') }}</p>
                                </div>
                                @endif
                                @if (session('success_delete_email'))
                                <div x-data="{ showMessage: true }" x-show="showMessage"
                                    class="bg-green-300 rounded-md p-3 m-5 flex justify-between"
                                    x-init="setTimeout(() => showMessage = false, 3000)">
                                    <p>{{ session('success_delete_email') }}</p>
                                </div>
                                @endif
                                @if (session('success_update_primary_email'))
                                <div x-data="{ showMessage: true }" x-show="showMessage"
                                    class="bg-green-300 rounded-md p-3 m-5 flex justify-between"
                                    x-init="setTimeout(() => showMessage = false, 3000)">
                                    <p>{{ session('success_update_primary_email') }}</p>
                                </div>
                                @endif
                                <div class="sm:grid grid-cols-2 p-5">
                                    <div>
                                        <h2 class="font-bold text-[#9E3D64]">Email</h2>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            @if (count($profile_email) == 1)
                                            <div class="grid sm:justify-items-end">
                                                <button
                                                    class="px-8 py-2 lg:text-md md:text-base text-white duration-300 rounded-lg bg-kamtuu-primary hover:bg-[#9E3D35] hover:text-white"
                                                    @click="open = !open">Tambah
                                                    Email</button>
                                            </div>
                                            @elseif (count($profile_email) == 2)
                                            <div class="grid sm:justify-items-end">
                                                <button
                                                    class="px-8 py-2 lg:text-md md:text-base text-white duration-300 bg-[#BDBDBD] rounded-lg hover:text-white"
                                                    disabled>Tambah Email
                                                </button>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="text-sm font-semibold font-inter p-5">
                                    <p class="text-[#000] font-inter font-bold text-md">Kamu butuh memasukkan maksimal 2
                                        email</p>
                                </div>
                                @foreach ($profile_email as $key=>$email)
                                <div class="sm:grid sm:grid-cols-2 px-5">
                                    <div class="text-[#000] font-inter font-bold text-sm pt-5 pb-2">
                                        {{$key+1}}. {{$email}}
                                    </div>
                                    <div class="flex justify-between">
                                        @if (auth()->user()->email == $email)
                                        <div class="text-[#D50006] font-inter font-bold text-sm sm:pt-5 pb-2">
                                            (Email Utama)
                                        </div>
                                        @endif
                                        @if (auth()->user()->email != $email)
                                        <form action="{{ route('profile.updatePrimaryEmail', ['email' => $email]) }}"
                                            method="POST">
                                            @csrf
                                            @method('PUT')

                                            <div class="text-[#D50006] font-inter font-semibold text-sm sm:pt-5 pb-2">
                                                <button type="submit">Jadikan email utama</button>
                                            </div>

                                        </form>
                                        @endif

                                        @if (count($profile_email) > 1)
                                        <form action="{{ route('profile.deleteEmail', ['email' => $email]) }}"
                                            method="POST">
                                            @csrf
                                            @method('PUT')

                                            <div class="text-[#D50006] font-inter font-normal text-sm sm:pt-5 pb-2">
                                                <button type="submit">
                                                    <img src="{{ asset('storage/icons/Vector.png') }}" alt="hapus-icon"
                                                        class="object-cover object-center overflow-hidden border-indigo-600 rounded-full">
                                                </button>
                                            </div>
                                        </form>
                                        @endif
                                    </div>

                                </div>
                                @endforeach

                                <form action="{{ route('profile.updateEmail', ['email' => 'add']) }}" method="POST">
                                    @csrf
                                    @method('PUT')

                                    <div class="px-5 sm:pl-7" x-show="open">
                                        <div class="text-[#000] font-inter font-bold text-sm pt-5 pb-2">
                                            <input type="email" name="email" id="email"
                                                class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5"
                                                placeholder="Masukkan email baru">
                                        </div>
                                        <div class="flex space-x-5">
                                            <div class="text-[#1B96A7] font-inter font-bold text-sm sm:pt-5 pb-2">
                                                <button type="submit">Simpan</button>
                                            </div>
                                            <div class="text-[#D50006] font-inter font-bold text-sm sm:pt-5 pb-2"
                                                @click="open = false">
                                                <button type="button">Tutup</buttton>
                                                    {{-- <a href="#">
                                                        <img src="{{ asset('storage/icons/Vector.png') }}"
                                                            alt="hapus-icon"
                                                            class="object-cover object-center overflow-hidden border-indigo-600 rounded-full">
                                                    </a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                {{-- <table>
                                    <tbody>
                                        @foreach ($profile_email as $key=>$email)
                                        <tr>
                                            <td class="text-[#000] font-inter font-bold text-sm pl-7 pt-5 pb-2"
                                                style="padding-right: 180px">
                                                {{$key+1}}. {{$email}}
                                            </td>
                                            @if (auth()->user()->email == $email)
                                            <td class="text-[#D50006] font-inter font-bold text-sm pl-20 pt-5 pb-2">
                                                Email Utama
                                            </td>
                                            @endif
                                            @if (auth()->user()->email != $email)
                                            <form
                                                action="{{ route('profile.updatePrimaryEmail', ['email' => $email]) }}"
                                                method="POST">
                                                @csrf
                                                @method('PUT')

                                                <td class="text-[#D50006] font-inter font-bold text-sm pl-20 pt-5 pb-2">
                                                    <button type="submit">Jadikan email utama</button>
                                                </td>

                                            </form>
                                            @endif

                                            @if (count($profile_email) > 1)
                                            <form action="{{ route('profile.deleteEmail', ['email' => $email]) }}"
                                                method="POST">
                                                @csrf
                                                @method('PUT')

                                                <td class="text-[#D50006] font-inter font-bold text-sm pl-7 pt-5 pb-2">
                                                    <button type="submit">
                                                        <img src="{{ asset('storage/icons/Vector.png') }}"
                                                            alt="user-profile"
                                                            class="object-cover object-center overflow-hidden border-indigo-600 rounded-full">
                                                    </button>
                                                </td>
                                            </form>
                                            @endif
                                        </tr>
                                        @endforeach

                                        <form action="{{ route('profile.updateEmail', ['email' => 'add']) }}"
                                            method="POST">
                                            @csrf
                                            @method('PUT')

                                            <tr x-show="open">
                                                <td class="text-[#000] font-inter font-bold text-sm pl-7 pt-5 pb-2"
                                                    style="padding-right: 180px">
                                                    <input type="email" name="email" id="email"
                                                        class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                                </td>
                                                <td class="text-[#1B96A7] font-inter font-bold text-sm pl-20 pt-5 pb-2">
                                                    <button type="submit">Simpan</button>
                                                </td>
                                                <td class="text-[#D50006] font-inter font-bold text-sm pl-7 pt-5 pb-2">
                                                    <a href="#">
                                                        <img src="{{ asset('storage/icons/Vector.png') }}"
                                                            alt="user-profile"
                                                            class="object-cover object-center overflow-hidden border-indigo-600 rounded-full">
                                                    </a>
                                                </td>
                                            </tr>
                                        </form>
                                    </tbody>
                                </table> --}}
                                @if ($errors->hasBag('add_secondary_email'))
                                <div class="bg-red-300 rounded-md p-3 m-5">
                                    @foreach ($errors->add_secondary_email->toArray() as $key=>$value)
                                    @foreach ($value as $index=>$err)
                                    <p>- {{ $err}}</p>
                                    @endforeach
                                    @endforeach
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Nomor Telepon --}}
                <div class="pb-5 mt-5 mb-5 rounded drop-shadow-xl" x-data="phoneNumberHandler()">
                    <div class="">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full pb-3">
                                @if (session('success_add_secondary_phone'))
                                <div x-data="{ showMessage: true }" x-show="showMessage"
                                    class="bg-green-300 rounded-md p-3 m-5 flex justify-between"
                                    x-init="setTimeout(() => showMessage = false, 3000)">
                                    <p>{{ session('success_add_secondary_phone') }}</p>
                                </div>
                                @endif
                                @if (session('success_delete_phone'))
                                <div x-data="{ showMessage: true }" x-show="showMessage"
                                    class="bg-green-300 rounded-md p-3 m-5 flex justify-between"
                                    x-init="setTimeout(() => showMessage = false, 3000)">
                                    <p>{{ session('success_delete_phone') }}</p>
                                </div>
                                @endif
                                @if (session('success_update_primary_phone'))
                                <div x-data="{ showMessage: true }" x-show="showMessage"
                                    class="bg-green-300 rounded-md p-3 m-5 flex justify-between"
                                    x-init="setTimeout(() => showMessage = false, 3000)">
                                    <p>{{ session('success_update_primary_phone') }}</p>
                                </div>
                                @endif
                                <div class="sm:grid sm:grid-cols-2 p-5">
                                    <div>
                                        <h2 class="font-bold text-[#9E3D64]">Nomor Telepon</h2>
                                    </div>
                                    <div class="p-2">
                                        @if (count($profile_phone_number) == 1)
                                        <div class="grid sm:justify-items-end">
                                            <button
                                                class="px-8 py-2 lg:text-md md:text-base text-white duration-300 rounded-lg bg-kamtuu-primary hover:bg-[#9E3D35] hover:text-white"
                                                @click="open = !open">Tambah
                                                Nomor</button>
                                        </div>
                                        @elseif (count($profile_phone_number) == 2)
                                        <div class="grid sm:justify-items-end">
                                            <button
                                                class="px-8 py-2 lg:text-md md:text-base text-white duration-300 bg-[#BDBDBD] rounded-lg hover:text-white"
                                                disabled>Tambah Nomor
                                            </button>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="text-sm font-semibold font-inter p-5">
                                    <p class="text-[#000] font-inter font-bold text-md">Kamu boleh memasukkan maksimal 2
                                        nomor hp</p>
                                </div>
                                @foreach ($profile_phone_number as $key=>$number)
                                <div class="sm:grid sm:grid-cols-2 px-5">
                                    <div class="text-[#000] font-inter font-bold text-sm pt-5 pb-2">
                                        {{$key+1}}. {{$number}}
                                    </div>
                                    <div class="flex justify-between">
                                        @if (auth()->user()->no_tlp == $number)
                                        <div class="text-[#D50006] font-inter font-bold text-sm sm:pt-5 pb-2">
                                            (Nomor Utama)
                                        </div>
                                        @endif
                                        @if (auth()->user()->no_tlp != $number)
                                        <form
                                            action="{{ route('profile.updatePrimaryPhoneNumber', ['no_telp' => $number]) }}"
                                            method="POST">
                                            @csrf
                                            @method('PUT')

                                            <div class="text-[#D50006] font-inter font-semibold text-sm sm:pt-5 pb-2">
                                                <button type="submit">Jadikan nomor utama</button>
                                            </div>

                                        </form>
                                        @endif

                                        @if (count($profile_phone_number) > 1)
                                        <form action="{{ route('profile.deletePhoneNumber', ['no_telp' => $number]) }}"
                                            method="POST">
                                            @csrf
                                            @method('PUT')

                                            <div
                                                class="text-[#D50006] font-inter font-normal text-sm sm:pl-7 sm:pt-5 pb-2">
                                                <button type="submit">
                                                    <img src="{{ asset('storage/icons/Vector.png') }}" alt="hapus-icon"
                                                        class="object-cover object-center overflow-hidden border-indigo-600 rounded-full">
                                                </button>
                                            </div>

                                        </form>
                                        @endif
                                    </div>
                                </div>
                                @endforeach

                                <form action="{{ route('profile.updatePhoneNumber', ['no_telp' => 'add']) }}"
                                    method="POST">
                                    @csrf
                                    @method('PUT')

                                    <div class="px-5 sm:pl-7" x-show="open">
                                        <div class="text-[#000] font-inter font-bold text-sm sm:pt-5 pb-2">
                                            <input type="number" name="no_telp" id="no_telp"
                                                class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5"
                                                placeholder="Masukkan nomor baru">
                                        </div>
                                        <div class="flex space-x-5">
                                            <div class="text-[#1B96A7] font-inter font-bold text-sm sm:pt-5 pb-2">
                                                <button type="submit">Simpan</button>
                                            </div>
                                            <div class="text-[#D50006] font-inter font-bold text-sm sm:pt-5 pb-2"
                                                @click="show = false">
                                                <button type="button">Tutup</button>
                                                {{-- <a href="#">
                                                    <img src="{{ asset('storage/icons/Vector.png') }}"
                                                        alt="user-profile"
                                                        class="object-cover object-center overflow-hidden border-indigo-600 rounded-full">
                                                </a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                {{-- <table>
                                    <tbody>
                                        @foreach ($profile_phone_number as $key=>$number)
                                        <tr>
                                            <td class="text-[#000] font-inter font-bold text-sm pl-7 pt-5 pb-2"
                                                style="padding-right: 180px">
                                                {{$key+1}}. {{$number}}
                                            </td>
                                            @if (auth()->user()->no_tlp == $number)
                                            <td class="text-[#D50006] font-inter font-bold text-sm pl-20 pt-5 pb-2">
                                                Nomor Utama
                                            </td>
                                            @endif
                                            @if (auth()->user()->no_tlp != $number)
                                            <form
                                                action="{{ route('profile.updatePrimaryPhoneNumber', ['no_telp' => $number]) }}"
                                                method="POST">
                                                @csrf
                                                @method('PUT')

                                                <td class="text-[#D50006] font-inter font-bold text-sm pl-20 pt-5 pb-2">
                                                    <button type="submit">Jadikan nomor utama</button>
                                                </td>

                                            </form>
                                            @endif

                                            @if (count($profile_phone_number) > 1)
                                            <form
                                                action="{{ route('profile.deletePhoneNumber', ['no_telp' => $number]) }}"
                                                method="POST">
                                                @csrf
                                                @method('PUT')

                                                <td class="text-[#D50006] font-inter font-bold text-sm pl-7 pt-5 pb-2">
                                                    <button type="submit">
                                                        <img src="{{ asset('storage/icons/Vector.png') }}"
                                                            alt="user-profile"
                                                            class="object-cover object-center overflow-hidden border-indigo-600 rounded-full">
                                                    </button>
                                                </td>

                                            </form>
                                            @endif
                                        </tr>
                                        @endforeach

                                        <form action="{{ route('profile.updatePhoneNumber', ['no_telp' => 'add']) }}"
                                            method="POST">
                                            @csrf
                                            @method('PUT')

                                            <tr x-show="open">
                                                <td class="text-[#000] font-inter font-bold text-sm pl-7 pt-5 pb-2"
                                                    style="padding-right: 180px">
                                                    <input type="number" name="no_telp" id="no_telp"
                                                        class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                                </td>
                                                <td class="text-[#1B96A7] font-inter font-bold text-sm pl-20 pt-5 pb-2">
                                                    <button type="submit">Simpan</button>
                                                </td>
                                                <td class="text-[#D50006] font-inter font-bold text-sm pl-7 pt-5 pb-2">
                                                    <a href="#">
                                                        <img src="{{ asset('storage/icons/Vector.png') }}"
                                                            alt="user-profile"
                                                            class="object-cover object-center overflow-hidden border-indigo-600 rounded-full">
                                                    </a>
                                                </td>
                                            </tr>
                                        </form>
                                    </tbody>
                                </table> --}}
                                @if ($errors->hasBag('add_secondary_phone'))
                                <div class="bg-red-300 rounded-md p-3 m-5">
                                    @foreach ($errors->add_secondary_phone->toArray() as $key=>$value)
                                    @foreach ($value as $index=>$err)
                                    <p>- {{ $err}}</p>
                                    @endforeach
                                    @endforeach
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-be.traveller.sidebar-traveller>

    <script>
        function emailHandler() {

            return {
                fields: {!! json_encode($profile != null ? $profile->email : '') !!},
                open: false,
            }
        }

        function phoneNumberHandler() {

            return {
                fields: {!! json_encode($profile != null ? $profile->phone_number : '') !!},
                open: false,
            }
        }
    </script>

    <script>
        $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function() {
            // provinsi
            $('#provinsi').on('change', function() {
                let id_provinsi = $('#provinsi').val();
                $('#kabupaten').val('');
                $('#kecamatan').val('');
                $('#kelurahan').val('');
                var option = $('<option></option>').attr("value", "").text("");
                $("#kecamatan").empty().append(option);
                $("#kelurahan").empty().append(option);

                $.ajax({
                    type: 'POST',
                    url: "{{ route('getkabupaten') }}",
                    data: {
                        id_provinsi: id_provinsi
                    },
                    cache: false,

                    success: function(msg) {
                        $('#kabupaten').html(msg);
                    },
                    error: function(data) {
                        console.log('error:', data);
                    },
                })
            })

            // kabupaten
            $('#kabupaten').on('change', function() {
                let id_kabupaten = $('#kabupaten').val();
                var option = $('<option></option>').attr("value", "").text("");
                $("#kelurahan").empty().append(option);

                $.ajax({
                    type: 'POST',
                    url: "{{ route('getkecamatan') }}",
                    data: {
                        id_kabupaten: id_kabupaten
                    },
                    cache: false,

                    success: function(msg) {
                        $('#kecamatan').html(msg);
                    },
                    error: function(data) {
                        console.log('error:', data);
                    },
                })
            })

            // kecamatan
            $('#kecamatan').on('change', function() {
                let id_kecamatan = $('#kecamatan').val();

                $.ajax({
                    type: 'POST',
                    url: "{{ route('getdesa') }}",
                    data: {
                        id_kecamatan: id_kecamatan
                    },
                    cache: false,

                    success: function(msg) {
                        $('#kelurahan').html(msg);
                    },
                    error: function(data) {
                        console.log('error:', data);
                    },
                })
            })
        })
    });

    </script>
</body>

</html>
