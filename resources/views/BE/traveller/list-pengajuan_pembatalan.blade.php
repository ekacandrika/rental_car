<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #report:checked+#report {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">

    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>

    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller>
        <div class="col-start-3 col-end-11 z-0">
            <div class="sm:p-5">
                <div class="flex items-center p-2 font-bold text-dark">
                    <img src="{{ asset('storage/icons/cart-shopping-solid (1).svg') }}" class="w-[16px] h-[16px] mt-1"
                        alt="user" title="user">
                    <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">List Pengajuan Pembatalan</p>
                </div>

                {{-- body --}}
                <div class="bg-gray-200 sm:rounded-md border border-slate-200 p-5 my-5">
                    <div class="sm:grid sm:grid-cols-2">
                        <div class="grid justify-items-center sm:justify-items-start mb-5">
                            <div class="flex">
                                <a href="{{ route('welcome') }}"
                                    class="rounded-lg px-5 py-2 text-sm text-center sm:text-base bg-blue-500 text-blue-100 hover:bg-blue-600 duration-300 mr-2">Lanjut
                                    Belanja</a>
                            </div>
                        </div>
                        <div class="grid justify-items-center sm:justify-items-end">
                            <div class="py-2 relative text-gray-600">
                                <input
                                    class="border-2 border-gray-300 bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none"
                                    type="search" name="search" placeholder="Search">
                                <button type="submit" class="absolute right-0 top-0 mt-5 mr-4">
                                    <svg class="text-gray-600 h-4 w-4 fill-current" xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px"
                                        y="0px" viewBox="0 0 56.966 56.966"
                                        style="enable-background:new 0 0 56.966 56.966;" xml:space="preserve"
                                        width="512px" height="512px">
                                        <path
                                            d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>

                    @forelse ($data as $item)
                    @php
                    $booking = App\Models\Bookingorder::where('id', $item->data_booking_id)->first();
                    $detail_produk = App\Models\Productdetail::where('id', $booking->product_detail_id)->first();
                    $produk = App\Models\Product::where('id', $item->product_id)->first();
                    @endphp
                    <div class="bg-kamtuu-produk-warning bg-opacity-25 p-3 rounded-md border border-gray-400 my-5">
                        <div class="sm:grid sm:grid-cols-12 sm:space-x-5">
                            {{-- Cols 2 --}}
                            <div class="grid w-full col-span-3 xl:col-span-2 gap-x-5">
                                {{-- {{dd($detail_produk)}} --}}
                                @if (isset($detail_produk->thumbnail))
                                <img class="max-h-[215px] w-full sm:w-[140px] sm:h-[140px] my-3 sm:my-0 rounded-md border border-gray-300 bg-cover object-cover justify-self-center"
                                    src="{{ isset($detail_produk->thumbnail) ? asset($detail_produk->thumbnail) : "
                                    https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                    }}" alt="thumbail_produk">
                                @endif

                                @if (isset($detail_produk->foto_maps_2))
                                <img class="max-h-[215px] w-full sm:w-[140px] sm:h-[140px] my-3 sm:my-0 rounded-md border border-gray-300 bg-cover object-cover justify-self-center"
                                    src="{{ isset($detail_produk->foto_maps_2) ? asset($detail_produk->foto_maps_2) : "
                                    https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                    }}" alt="thumbail_produk">
                                @endif

                                @if (!isset($detail_produk->thumbnail) && !isset($detail_produk->foto_maps_2))
                                <img class="max-h-[215px] w-full sm:w-[140px] sm:h-[140px] my-3 sm:my-0 rounded-md border border-gray-300 bg-cover object-cover justify-self-center"
                                    src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                    alt="thumbail_produk">
                                @endif
                                {{-- <img class="w-28 h-28 rounded-md border border-gray-300"
                                    src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                    alt=""> --}}
                            </div>
                            {{-- Cols 3 --}}
                            <div class="col-start-4 xl:col-start-3 col-span-5">
                                <p class="capitalize">{{ $produk->type }}</p>
                                <p class="text-lg font-bold">{{ $produk->product_name }}</p>
                                @if ($produk->type == 'Transfer')
                                @php
                                $detail_mobil = App\Models\Mobildetail::where('id',
                                $detail_produk->detailmobil_id)->first();
                                @endphp
                                <div class="flex py-2 gap-1 lg:gap-2">
                                    <div
                                        class="bg-[#9E3D64] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                        Transfer</div>
                                    <div
                                        class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                        Sedan</div>
                                </div>
                                <div class="flex">
                                    <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}"
                                        alt="rating" width="17px" height="17px">
                                    <p class="font-bold text-[8px] lg:text-sm mr-2">
                                        {{ $detail_mobil->kapasitas_kursi }} Seats</p>
                                    <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}"
                                        alt="rating" width="17px" height="17px">
                                    <p class="font-bold  text-[8px] lg:text-sm mr-2">
                                        {{ $detail_mobil->kapasitas_koper }} Koper</p>
                                </div>
                                @endif
                                @if ($produk->type == 'Rental')
                                <div class="flex py-2 gap-1 lg:gap-2">
                                    <div
                                        class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                        Sedan</div>
                                </div>
                                <div class="flex">
                                    <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}"
                                        alt="rating" width="17px" height="17px">
                                    <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                                    <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}"
                                        alt="rating" width="17px" height="17px">
                                    <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                                </div>
                                @endif
                                @if ($item->activity_date != null)
                                <div class="flex">
                                    <p class="text-gray-600">
                                        {{ Carbon\Carbon::parse($item->activity_date)->translatedFormat('d F Y') }}
                                    </p>
                                </div>
                                @endif
                                <div class="pt-8 flex gap-2 items-center">
                                    <p class="text-gray-600">1 Tiket Dewasa
                                        @if ($item->activity_date != null)
                                        •
                                        {{ Carbon\Carbon::parse($item->activity_date)->translatedFormat('d F Y') }}
                                        @endif
                                    </p>
                                </div>
                            </div>
                            {{-- Cols 4 --}}
                            <div
                                class="py-2 sm:py-0 col-span-4 xl:col-span-5 grid text-sm space-y-3 sm:text-end md:text-base sm:justify-items-end">
                                @if ($item->kode_pengajuan != null)
                                <p>Kode Pengajuan: {{ $item->kode_pengajuan }}</p>
                                @else
                                <p>Kode Pembatalan: {{ $item->kode_pembatalan }}</p>
                                @endif
                                <br>
                                <p style="font-size: 14px">Status: {{ $item->status_pengajuan }}</p>
                                @if ($item->tgl_pengajuan != null)
                                <p>Tanggal Pengajuan:
                                    {{ Carbon\Carbon::parse($item->tgl_pengajuan)->translatedFormat('d F Y') }}</p>
                                @endif
                                <p class="py-2 sm:py-0 text-blue-600 text-2xl sm:text-xl md:text-2xl">IDR {{
                                    number_format($item->harga) }}</p>
                            </div>
                        </div>
                    </div>
                    @empty
                    <div class="bg-kamtuu-produk-warning bg-opacity-25 p-3 rounded-md border border-gray-400 my-5">
                        <p class="text-dark-500 text-center"><i>Data Kosong !</i></p>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </x-be.traveller.sidebar-traveller>

</body>

</html>
