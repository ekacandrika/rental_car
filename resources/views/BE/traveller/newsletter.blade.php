<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        h1 {
            font-size: 30px;
            color: #000;
        }

        h2 {
            font-size: 20px;
            color: #000;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>

    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller>
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5">
                <h1 class="font-bold font-inter">Newsletter</h1>

                {{-- Data Diri --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                    <div class="">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <div class="sm:grid sm:grid-cols-2 p-5">
                                    <div class="p-2">
                                        <div>
                                            <h2 class="font-bold text-[#000] text-center sm:text-left">
                                                Langganan Kamtuu Newsletter
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            <div class="grid justify-items-center sm:justify-items-end">
                                                <label class="switch">
                                                    <input name='newslatter_status' type='checkbox'
                                                        id="newslatter_status" value="{{ $profile->newslatter_status }}"
                                                        @if($profile->newslatter_status == 1) checked @endif>
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-be.traveller.sidebar-traveller>


    <script>
        $('#newslatter_status').on('change', function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let newslatter_status = $('#newslatter_status').val();
            
            newslatter_status = newslatter_status === '1'? '0' : '1';

            $('#newslatter_status').val(newslatter_status);

            $.ajax({
                type: 'PUT',
                url: "{{ route('newsletter.updateNewsletter') }}",
                data: {
                    newslatter_status: newslatter_status
                },
                cache: false,

                success: function(msg) {
                    $('#newslatter_status').html(msg);
                },
                error: function(data) {
                    console.log('error:', data);
                },
            })
        })
    </script>

</body>

</html>
