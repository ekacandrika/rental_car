<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>

    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller>
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-10">
                <span class="text-sm font-bold font-inter text-[#000000]">Tanggal bergabung: {{ $date_joined }}</span>

                {{-- Card 1 --}}
                <div class="drop-shadow-xl py-5 my-5 rounded text-center">
                    {{-- Card 2 --}}
                    <div class="flex justify-center pointer-events-none cursor-default">
                        <div class="rounded-lg bg-[#9E3D64] my-2 w-fit">
                            <p class="py-3 px-2 font-bold text-white">Jumlah order</p>
                        </div>
                    </div>

                    <div class="grid sm:grid-cols-2 md:grid-cols-3 gap-3">
                        @foreach ($orders as $key => $order)
                        @if ($key == 0 || $key == 3)
                        <div class="w-auto flex bg-[#23AEC1] rounded hover:shadow-lg duration-200">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base capitalize">{{ $order->type }}</p>
                                <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">{{ $order->type_count }}</p>
                            </div>
                        </div>
                        @endif
                        @if ($key == 1 || $key == 4)
                        <div class="w-auto flex bg-[#FFB800] rounded hover:shadow-lg duration-200">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base capitalize">{{ $order->type }}</p>
                                <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">{{ $order->type_count }}</p>
                            </div>
                        </div>
                        @endif
                        @if ($key == 2 || $key == 5)
                        <div class="w-auto flex bg-[#51B449] rounded hover:shadow-lg duration-200">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base capitalize">{{ $order->type }}</p>
                                <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">{{ $order->type_count }}</p>
                            </div>
                        </div>
                        @endif
                        @endforeach
                        {{-- <div class="w-auto flex bg-[#23AEC1] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Tur</p>
                                <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">Rp 40,000,000</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Activity</p>
                                <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">Rp 100,000,000</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Hotel</p>
                                <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">Rp 500,000,000</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#23AEC1] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Rental</p>
                                <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">Rp 40,000,000</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Transfer</p>
                                <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">Rp 100,000,000</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">XStay</p>
                                <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">Rp 500,000,000</p>
                            </div>
                        </div> --}}
                    </div>
                </div>

                <div class="text-sm font-inter font-semibold mt-5">
                    <p class="text-[#9E3D64] font-inter font-bold text-lg mt-2.5">Ayo Lebih Semangat!!!</p>
                </div>
            </div>
        </div>
    </x-be.traveller.sidebar-traveller>
</body>

</html>