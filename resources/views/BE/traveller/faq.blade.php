<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        h1 {
            font-size: 30px;
            color: #000;
        }

        h2 {
            font-size: 20px;
            color: #000;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>

    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller>
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5">
                <h1 class="font-bold font-inter">Frequently Asked Questions</h1>

                {{-- content --}}
                <div class="drop-shadow-xl pb-2 mt-5 mb-2 rounded">
                    <div class="">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <div>
                                    <div x-data="{
                                        tabs: [{
                                            time: '09.00',
                                            title: 'Lorem Ipsum dolor sit amet',
                                        }]
                                    }">
                                        <ul class="text-sm font-medium text-justify sm:text-left">
                                            <template x-for="(tab, index) in tabs" :key="index">
                                                <li class="my-2 px-3 rounded-md
                                                    :class="'border-l-8 border-[' + tab.color + ']'"
                                                    x-data=" { open: false }">
                                                    <div class="grid grid-cols-12"
                                                        :class="{ 'pt-3 pb-2': open, 'py-3': !open }">
                                                        <div class="col-span-11 md:grid grid-cols-11">
                                                            <p class="text-base sm:text-lg text-[#4F4F4F] font-semibold text-left rounded-lg cursor-pointer"
                                                                x-on:click="open = !open" x-text="tab.time"></p>
                                                            <button
                                                                class="text-base sm:text-lg text-[#4F4F4F] font-semibold col-span-10 w-full text-left rounded-lg whitespace-normal break-words"
                                                                x-on:click="open = !open" x-text="tab.title"></button>
                                                        </div>
                                                        <div class="w-full">
                                                            <img class="float-right duration-200 cursor-pointer"
                                                                :class="{ 'rotate-180': open }"
                                                                x-on:click="open = !open"
                                                                src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                                                alt="chevron-down" width="26px" height="15px">
                                                        </div>
                                                    </div>
                                                    <div class="py-5 md:pl-14 lg:pl-16 xl:pl-20 2xl:pl-24 md:pr-10 lg:pr-12 font-normal text-[#4F4F4F] text-base lg:text-lg xl:text-xl"
                                                        x-show="open" x-transition>
                                                        <p>Lorem Ipsum is simply dummy text of the printing and
                                                            typesetting
                                                            industry. Lorem Ipsum has been the industry's standard
                                                            dummy text ever since the 1500s, when an unknown printer
                                                            took a
                                                            galley of type and scrambled it to make a type specimen
                                                            book.
                                                        </p>
                                                        <img class="my-3 object-cover object-center sm:max-w-md rounded-md"
                                                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                                            alt="itinerary-img">
                                                    </div>

                                                </li>
                                            </template>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="drop-shadow-xl pb-2 mt-5 mb-2 rounded">
                    <div class="">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <div>
                                    <div x-data="{
                                        tabs: [{
                                            time: '09.00',
                                            title: 'Lorem Ipsum dolor sit amet',
                                        }]
                                    }">
                                        <ul class="text-sm font-medium text-justify sm:text-left">
                                            <template x-for="(tab, index) in tabs" :key="index">
                                                <li class="my-2 px-3 rounded-md
                                                    :class="'border-l-8 border-[' + tab.color + ']'"
                                                    x-data=" { open: false }">
                                                    <div class="grid grid-cols-12"
                                                        :class="{ 'pt-3 pb-2': open, 'py-3': !open }">
                                                        <div class="col-span-11 md:grid grid-cols-11">
                                                            <p class="text-base sm:text-lg text-[#4F4F4F] font-semibold text-left rounded-lg cursor-pointer"
                                                                x-on:click="open = !open" x-text="tab.time"></p>
                                                            <button
                                                                class="text-base sm:text-lg text-[#4F4F4F] font-semibold col-span-10 w-full text-left rounded-lg whitespace-normal break-words"
                                                                x-on:click="open = !open" x-text="tab.title"></button>
                                                        </div>
                                                        <div class="w-full">
                                                            <img class="float-right duration-200 cursor-pointer"
                                                                :class="{ 'rotate-180': open }"
                                                                x-on:click="open = !open"
                                                                src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                                                alt="chevron-down" width="26px" height="15px">
                                                        </div>
                                                    </div>
                                                    <div class="py-5 md:pl-14 lg:pl-16 xl:pl-20 2xl:pl-24 md:pr-10 lg:pr-12 font-normal text-[#4F4F4F] text-base lg:text-lg xl:text-xl"
                                                        x-show="open" x-transition>
                                                        <p>Lorem Ipsum is simply dummy text of the printing and
                                                            typesetting
                                                            industry. Lorem Ipsum has been the industry's standard
                                                            dummy text ever since the 1500s, when an unknown printer
                                                            took a
                                                            galley of type and scrambled it to make a type specimen
                                                            book.
                                                        </p>
                                                        <img class="my-3 object-cover object-center sm:max-w-md rounded-md"
                                                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                                            alt="itinerary-img">
                                                    </div>

                                                </li>
                                            </template>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="drop-shadow-xl pb-2 mt-5 mb-2 rounded">
                    <div class="">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <div>
                                    <div x-data="{
                                        tabs: [{
                                            time: '09.00',
                                            title: 'Lorem Ipsum dolor sit amet',
                                        }]
                                    }">
                                        <ul class="text-sm font-medium text-justify sm:text-left">
                                            <template x-for="(tab, index) in tabs" :key="index">
                                                <li class="my-2 px-3 rounded-md
                                                    :class="'border-l-8 border-[' + tab.color + ']'"
                                                    x-data=" { open: false }">
                                                    <div class="grid grid-cols-12"
                                                        :class="{ 'pt-3 pb-2': open, 'py-3': !open }">
                                                        <div class="col-span-11 md:grid grid-cols-11">
                                                            <p class="text-base sm:text-lg text-[#4F4F4F] font-semibold text-left rounded-lg cursor-pointer"
                                                                x-on:click="open = !open" x-text="tab.time"></p>
                                                            <button
                                                                class="text-base sm:text-lg text-[#4F4F4F] font-semibold col-span-10 w-full text-left rounded-lg whitespace-normal break-words"
                                                                x-on:click="open = !open" x-text="tab.title"></button>
                                                        </div>
                                                        <div class="w-full">
                                                            <img class="float-right duration-200 cursor-pointer"
                                                                :class="{ 'rotate-180': open }"
                                                                x-on:click="open = !open"
                                                                src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                                                alt="chevron-down" width="26px" height="15px">
                                                        </div>
                                                    </div>
                                                    <div class="py-5 md:pl-14 lg:pl-16 xl:pl-20 2xl:pl-24 md:pr-10 lg:pr-12 font-normal text-[#4F4F4F] text-base lg:text-lg xl:text-xl"
                                                        x-show="open" x-transition>
                                                        <p>Lorem Ipsum is simply dummy text of the printing and
                                                            typesetting
                                                            industry. Lorem Ipsum has been the industry's standard
                                                            dummy text ever since the 1500s, when an unknown printer
                                                            took a
                                                            galley of type and scrambled it to make a type specimen
                                                            book.
                                                        </p>
                                                        <img class="my-3 object-cover object-center sm:max-w-md rounded-md"
                                                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                                            alt="itinerary-img">
                                                    </div>

                                                </li>
                                            </template>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-be.traveller.sidebar-traveller>

</body>

</html>
