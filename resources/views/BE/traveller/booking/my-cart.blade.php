<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #report:checked+#report {
            display: block;
        }

        .alert {
            padding: 8px;
            background-color: #ee635a;
            color: white;
            border-radius: 10px;
        }

        .closebtn {
            margin-left: 15px;
            color: white;
            font-weight: bold;
            float: right;
            font-size: 22px;
            line-height: 20px;
            cursor: pointer;
            transition: 0.3s;
        }

        .closebtn:hover {
            color: black;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">

    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>

    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller>
        <div class="col-start-3 col-end-11 z-0">
            <div class="py-10 xl:px-5 2xl:px-18" x-data="price">
                <div class="flex items-center p-2 font-bold text-dark">
                    <img src="{{ asset('storage/icons/cart-shopping-solid (1).svg') }}" class="w-[16px] h-[16px] mt-1"
                        alt="user" title="user">
                    <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Koper Pesanan</p>
                </div>

                {{-- body --}}
                <div class="bg-gray-200 sm:rounded-md border border-slate-200 p-5 my-5">
                    <div class="sm:grid sm:grid-cols-2">
                        <div class="grid justify-items-center sm:justify-items-start mb-5">
                            <div class="flex">
                                <a href="{{ route('welcome') }}"
                                    class="rounded-lg px-5 py-2 text-base bg-blue-500 text-blue-100 hover:bg-blue-600 duration-300 mr-2">Lanjut
                                    Belanja</a>
                                <a href="{{ route('viewDataPesanan') }}"
                                    class="rounded-lg px-5 py-2 text-base bg-blue-500 text-blue-100 hover:bg-blue-600 duration-300">List
                                    Order</a>
                            </div>
                        </div>
                        <div class="grid justify-items-center sm:justify-items-end">
                            <div class="py-2 relative text-gray-600">
                            <form method="get" action="{{route('cart')}}">
                                <input
                                    class="border-2 border-gray-300 bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none"
                                    type="search" name="search" placeholder="Search" value="{{$cari}}" id="cart_search">
                                <button type="submit" class="absolute right-0 top-0 mt-5 mr-4">
                                    <svg class="text-gray-600 h-4 w-4 fill-current" xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px"
                                        y="0px" viewBox="0 0 56.966 56.966"
                                        style="enable-background:new 0 0 56.966 56.966;" xml:space="preserve"
                                        width="512px" height="512px">
                                        <path
                                            d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
                                    </svg>
                                </button>
                            </form>    
                            </div>
                        </div>
                        {{-- <div class="grid justify-items-start mb-5">
                        </div> --}}
                    </div>
                    <div class="mb-4">
                        <div class="pt-2 relative text-gray-600">
                            <form class="sm:flex gap-3" action="{{ route('cekKupon') }}" method="POST">
                                @csrf
                                <input
                                    class="w-full border-2 border-gray-300 bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none"
                                    type="search" name="kode_kupon" placeholder="Masukkan Kode Kupon">
                                <button type="submit"
                                    class="w-full sm:w-fit items-center my-3 sm:my-0 px-3 py-2 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                    Gunakan
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="mt-3">
                        @if (session('error'))
                        <div class="alert">
                            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                            <strong>{{ session('error') }}</strong>
                        </div>
                        @endif
                    </div>
                    <div class="flex gap-3 items-center">
                        <input type="checkbox" @click="pilihSemua"
                            class="rounded-sm text-kamtuu-second checked:bg-kamtuu-second" x-model="selectAll">
                        <label for="check_all">Pilih semua</label>
                    </div>

                    @forelse ($data as $key => $item)
                    @php
                    $detail_produk = App\Models\Productdetail::where('id', $item->product_detail_id)->first();
                    $produk = App\Models\Product::where('id', $detail_produk->product_id)->first();

                    $today = \Carbon\Carbon::now();
                    $checkIn = $today;
                    
                    if (isset($item->check_in_date)) {
                    // $checkIn = DateTime::createFromFormat('Y-m-d', $item->check_in_date);
                    $checkIn = isset($item->check_in_date) ? \Carbon\Carbon::parse($item->check_in_date) : $today;
                    }

                    if (isset($item->activity_date)) {
                    $checkIn = isset($item->activity_date) ? \Carbon\Carbon::parse($item->activity_date) : $today;
                    // $checkIn = date('Y-m-d', strtotime($item->activity_date));
                    // $checkIn = DateTime::createFromFormat('Y-m-d', $checkIn);
                    }

                    $diff = $today->diff($checkIn);
                    $get_diff = (int)$diff->format('%r%a');
                    $days = $diff->format('%a');
                    $hours = $diff->format('%h');
                    $minutes = $diff->format('%i');
                    @endphp
                    {{-- @dump($diff->d) --}}
                    <div class="bg-kamtuu-produk-warning bg-opacity-25 p-7 sm:p-5 rounded-md border border-gray-400 my-5"
                        x-data="{ confirm_order: '{{ $item->confirm_order == null ? 'null' : $item->confirm_order }}'}"
                        {{-- harusnya {{ diff->d <= 0 }} --}}
                            :class="{{ $diff->invert }} == 1 || {{ $diff->d }} < 0 ? 'bg-[#A0A0A0]' : confirm_order === 'null' || confirm_order === 'instant' ? 'bg-[#DFFFD7]' : confirm_order === 'waiting' || confirm_order === 'by_seller' ? 'bg-[#F4F4F4]' : confirm_order === 'reject' ? 'bg-[#A0A0A0]' : ''">
                            <div class="sm:grid sm:grid-cols-12 sm:space-x-5">
                                {{-- Cols 2 --}}
                                {{-- {{ $produk->type == 'activity' ? dd($detail_produk) : '' }} --}}
                                <div class="col-span-4 lg:col-span-3 sm:flex gap-x-5">
                                    <div class="flex sm:grid space-x-2 sm:content-center">
                                        {{-- {{ var_dump(($item->confirm_order == null || $item->confirm_order ==
                                        'instant')
                                        ,
                                        $get_diff, $diff->h, $diff->i, $diff->s, $get_diff > 0, $diff->invert == 0) }}
                                        --}}
                                        @if (($item->confirm_order == null || $item->confirm_order == 'instant') &&
                                        ($diff->invert == 0))
                                        {{-- ($get_diff >
                                        0 || $diff->invert == 0)) --}}
                                        <input type="checkbox" class="item_check my-1 sm:my-0"
                                            value="{{ $item->booking_code }}" harga="{{$item->total_price}}"
                                            data-id-product="{{$item->id}}"
                                            class="rounded-sm text-kamtuu-second checked:bg-kamtuu-second"
                                            x-model="booking_code_arr"
                                            @click="sumTotal({{$item->total_price}}, {{$item->id}})">
                                        <div class="block sm:hidden">
                                            <p class="text-lg font-bold">{{ $produk->product_name }}</p>
                                            <p class="capitalize">{{ $produk->type }}</p>
                                        </div>
                                        @else
                                        <div class="mx-2">

                                        </div>
                                        @endif
                                    </div>
                                    <a class="w-full grid"
                                        href="{{ route('viewDetailOrderProduct', $item->booking_code) }}">
                                        @if (isset($detail_produk->thumbnail))
                                        <img class="max-w-[322px] w-full sm:w-[140px] sm:h-[140px] my-3 sm:my-0 rounded-md border border-gray-300 bg-cover object-cover justify-self-center"
                                            src="{{ isset($detail_produk->thumbnail) ? asset($detail_produk->thumbnail) : "
                                            https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                            }}" alt="thumbail_produk">
                                        @endif

                                        @if (isset($detail_produk->foto_maps_2))
                                        <img class="max-w-[322px] w-full sm:w-[140px] sm:h-[140px] my-3 sm:my-0 rounded-md border border-gray-300 bg-cover object-cover justify-self-center"
                                            src="{{ isset($detail_produk->foto_maps_2) ? asset($detail_produk->foto_maps_2) : "
                                            https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                            }}" alt="thumbail_produk">
                                        @endif

                                        @if (!isset($detail_produk->thumbnail) && !isset($detail_produk->foto_maps_2))
                                        <img class="max-w-[322px] w-full sm:w-[140px] sm:h-[140px] my-3 sm:my-0 rounded-md border border-gray-300 bg-cover object-cover justify-self-center"
                                            src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                            alt="thumbail_produk">
                                        @endif
                                    </a>
                                </div>
                                {{-- Cols 3 --}}
                                <div class="col-start-5 lg:col-start-4 col-span-5">
                                    <p class="capitalize">{{ $produk->type }}</p>
                                    <p class="text-lg font-bold">{{ $produk->product_name }}</p>
                                    @if ($produk->type == 'hotel')
                                    <div class="text-xs my-2">
                                        <p>Check-In:
                                            {{ Carbon\Carbon::parse($item->check_in_date)->translatedFormat('d F Y') }}
                                        </p>
                                        <p>Check-Out:
                                            {{ Carbon\Carbon::parse($item->check_out_date)->translatedFormat('d F Y') }}
                                        </p>
                                        <div class="my-1">
                                            <p>Peserta :</p>
                                            <p>{{ $item->peserta_dewasa > 0 ? ($item->peserta_dewasa . ' Dewasa') : ''
                                                }} {{
                                                $item->peserta_anak_anak > 0 ? (' • ' . $item->peserta_anak_anak . '
                                                Anak')
                                                : ''
                                                }}
                                                {{ $item->peserta_balita > 0 ? (' • ' . $item->peserta_balita . '
                                                Balita') :
                                                ''
                                                }}
                                            </p>
                                        </div>
                                    </div>
                                    @endif

                                    @if ($produk->type == 'xstay')
                                    <div class="text-xs my-2">
                                        <p>Check-In:
                                            {{ Carbon\Carbon::parse($item->check_in_date)->translatedFormat('d F Y') }}
                                        </p>
                                        <p>Check-Out:
                                            {{ Carbon\Carbon::parse($item->check_out_date)->translatedFormat('d F Y') }}
                                        </p>
                                        <div class="my-1">
                                            <p>Peserta :</p>
                                            <p>{{ $item->peserta_dewasa > 0 ? ($item->peserta_dewasa . ' Dewasa') : ''
                                                }} {{
                                                $item->peserta_anak_anak > 0 ? (' • ' . $item->peserta_anak_anak . '
                                                Anak')
                                                :
                                                ''
                                                }}
                                                {{ $item->peserta_balita > 0 ? (' • ' . $item->peserta_balita . '
                                                Balita') :
                                                '' }}
                                            </p>
                                        </div>

                                    </div>
                                    @endif

                                    @if ($produk->type == 'Transfer')
                                    @php
                                    $detail_mobil = App\Models\Mobildetail::where('id',
                                    $detail_produk->detailmobil_id)->first();
                                    @endphp
                                    <div class="flex py-2 gap-1 lg:gap-2">
                                        <div
                                            class="bg-[#9E3D64] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                            Transfer</div>
                                        <div
                                            class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                            Sedan</div>
                                    </div>
                                    <div class="flex">
                                        <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                            src="{{ asset('storage/icons/seats.svg') }}" alt="rating" width="17px"
                                            height="17px">
                                        <p class="font-bold text-[8px] lg:text-sm mr-2">
                                            {{ $detail_mobil->kapasitas_kursi }} Seats</p>
                                        <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                            src="{{ asset('storage/icons/koper.svg') }}" alt="rating" width="17px"
                                            height="17px">
                                        <p class="font-bold  text-[8px] lg:text-sm mr-2">
                                            {{ $detail_mobil->kapasitas_koper }} Koper</p>
                                    </div>
                                    <p>Tanggal Mulai:
                                        {{ Carbon\Carbon::parse($item->check_in_date)->translatedFormat('d F Y') }}
                                    </p>
                                    @endif

                                    @if ($produk->type == 'Rental')
                                    <div class="flex py-2 gap-1 lg:gap-2">
                                        <div
                                            class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                            Sedan</div>
                                    </div>
                                    <div class="flex">
                                        <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                            src="{{ asset('storage/icons/seats.svg') }}" alt="rating" width="17px"
                                            height="17px">
                                        <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                                        <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                            src="{{ asset('storage/icons/koper.svg') }}" alt="rating" width="17px"
                                            height="17px">
                                        <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                                    </div>
                                    <p>Tanggal Mulai:
                                        {{ Carbon\Carbon::parse($item->check_in_date)->translatedFormat('d F Y') }}
                                    </p>
                                    <p>Tanggal Selesai:
                                        {{ Carbon\Carbon::parse($item->check_out_date)->translatedFormat('d F Y') }}
                                    </p>
                                    @endif

                                    @if ($produk->type == 'activity')
                                    <div class="my-2">
                                        <p class="text-gray-600 text-xs">{{ $item->peserta_dewasa > 0 ?
                                            ($item->peserta_dewasa .
                                            '
                                            Tiket
                                            Dewasa') : '' }} {{
                                            $item->peserta_anak_anak > 0 ? (' • ' . $item->peserta_anak_anak . ' Tiket
                                            Anak') :
                                            ''
                                            }}
                                            {{ $item->peserta_balita > 0 ? (' • ' . $item->peserta_balita . ' Tiket
                                            Balita')
                                            :
                                            '' }}
                                        </p>
                                        <div class="flex gap-2 items-center text-xs">
                                            <p class="text-gray-600">
                                                @if ($item->activity_date != null)
                                                {{ Carbon\Carbon::parse($item->activity_date)->translatedFormat('d F Y')
                                                }}
                                                @endif
                                            </p>
                                            <p class="text-gray-600">
                                                {{ isset($item->waktu_paket) ? (' • ' . $item->waktu_paket) : '' }}
                                            </p>
                                        </div>
                                    </div>
                                    @endif

                                    @if ($diff->invert == 0 && $diff->d > 0)
                                    <div class="mb-5 mt-5 sm:mb-0 countdown_length" data-countdown-key="{{$key}}">
                                        <span class="font-bold">Akan Dimulai Dalam</span>
                                        <br class="xl:hidden" />
                                        <span class="digit" id="days_{{$key}}"> {{ $diff->d }} </span> Hari
                                        <span class="digit" id="hours_{{$key}}"> {{ $diff->h }} </span> Jam
                                        <span class="digit" id="minutes_{{$key}}"> {{ $diff->i }} </span> Menit
                                        <span class="digit" id="seconds_{{$key}}"> {{ $diff->s }} </span> Detik
                                    </div>
                                    @endif
                                </div>
                                {{-- Cols 4 --}}
                                <div
                                    class="col-span-3 lg:col-span-4 grid text-sm sm:text-end md:text-base sm:justify-items-end">
                                    <p>Kode Booking: {{ $item->booking_code }}</p>
                                    {{-- harusnya $diff->d > 0 --}}
                                    @if (($diff->invert == 0 && $diff->d >= 0))
                                    @if ($item->confirm_order == null)
                                    <p class="text-blue-500">Telah dikonfirmasi</p>
                                    @endif
                                    @if ($item->confirm_order == 'reject')
                                    <p class="text-red-500">Dibatalkan</p>
                                    @endif
                                    @if ($item->confirm_order == 'waiting')
                                    <p class="text-kamtuu-third">Menunggu konfirmasi</p>
                                    @endif
                                    @if ($item->confirm_order == 'instant')
                                    <p class="text-blue-500">Konfirmasi instant</p>
                                    @endif
                                    @else
                                    <p class="text-red-500">Kedaluwarsa</p>
                                    @endif
                                    <p class="text-blue-600 text-2xl sm:text-xl md:text-2xl">IDR {{
                                        number_format($item->total_price) }}</p>
                                    @if ($produk->type == 'hotel')
                                    @php
                                    $pajak_global = App\Models\PajakAdmin::first();
                                    $pajak_local_hotel = App\Models\PajakLocal::where('produk_type', 'hotel')->first();
                                    @endphp
                                    @if ($pajak_local_hotel == null)
                                    <p class="text-gray-500">(Pajak {{ $pajak_global->markup }}%)</p>
                                    @else
                                    @if ($pajak_local_hotel->markupLocal == null)
                                    <p class="text-gray-500">(Pajak {{ $pajak_global->markup }}%)</p>
                                    @else
                                    @if ($pajak_local_hotel->statusMarkupLocal == 'percent')
                                    <p class="text-gray-500">(Pajak
                                        {{ $pajak_local_hotel->markupLocal }}%)
                                    </p>
                                    @else
                                    <p class="text-gray-500">(Pajak IDR
                                        {{ number_format($pajak_local_hotel->markupLocal) }})
                                    </p>
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @if ($produk->type == 'xstay')
                                    @php
                                    $pajak_global = App\Models\PajakAdmin::first();
                                    $pajak_local_xstay = App\Models\PajakLocal::where('produk_type', 'xstay')->first();
                                    @endphp
                                    @if ($pajak_local_xstay == null)
                                    <p class="text-gray-500">(Pajak {{ $pajak_global->markup }}%)</p>
                                    @else
                                    @if ($pajak_local_xstay->markupLocal == null)
                                    <p class="text-gray-500">(Pajak {{ $pajak_global->markup }}%)</p>
                                    @else
                                    <p class="text-gray-500">(Pajak {{ $pajak_local_xstay->markupLocal }}%)
                                    </p>
                                    @endif
                                    @endif
                                    @endif
                                    @if ($produk->type == 'Transfer')
                                    @php
                                    $pajak_global = App\Models\PajakAdmin::first();
                                    $pajak_local_transfer = App\Models\PajakLocal::where('produk_type',
                                    'transfer')->first();
                                    @endphp
                                    @if ($pajak_local_transfer == null)
                                    <p class="text-gray-500">(Pajak {{ $pajak_global->markup }}%)</p>
                                    @else
                                    @if ($pajak_local_transfer->markupLocal == null)
                                    <p class="text-gray-500">(Pajak {{ $pajak_global->markup }}%)</p>
                                    @else
                                    <p class="text-gray-500">(Pajak {{ $pajak_local_transfer->markupLocal }}%)
                                    </p>
                                    @endif
                                    @endif
                                    @endif
                                    @if ($produk->type == 'Rental')
                                    @php
                                    $pajak_global = App\Models\PajakAdmin::first();
                                    $pajak_local_rental = App\Models\PajakLocal::where('produk_type',
                                    'rental')->first();
                                    @endphp
                                    @if ($pajak_local_rental == null)
                                    <p class="text-gray-500">(Pajak {{ $pajak_global->markup }}%)</p>
                                    @else
                                    @if ($pajak_local_rental->markupLocal == null)
                                    <p class="text-gray-500">(Pajak {{ $pajak_global->markup }}%)</p>
                                    @else
                                    <p class="text-gray-500">(Pajak {{ $pajak_local_rental->markupLocal }}%)
                                    </p>
                                    @endif
                                    @endif
                                    @endif
                                    <div class="grid space-y-3 py-5">
                                        @if ($item->confirm_order == 'by_seller')
                                        <div class="flex gap-2 items-center">
                                            <form action="{{ route('booking.update.status_confirm', $item->id) }}"
                                                method="POST">
                                                @csrf
                                                @method('PUT')
                                                <input type="hidden" name="confirm_order" value="waiting">
                                                <button type="submit"
                                                    class="items-center px-px py-1 lg:py-2 w-24 lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-kamtuu-third rounded lg:rounded-lg">Minta
                                                    Konfirmasi Seller</button>
                                            </form>
                                        </div>
                                        @endif
                                        {{-- @if ($item->confirm_order == 'waiting')
                                        <div class="flex gap-2 items-center">
                                            <button type="button"
                                                class="items-center px-px py-1 cursor-default lg:py-2 w-24 lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-black bg-kamtuu-third opacity-30 border-slate-500 rounded lg:rounded-lg">Menunggu
                                                Konfirmasi Seller</button>
                                        </div>
                                        @endif --}}
                                        <div class="flex gap-2 items-center">
                                            <form action="{{ route('booking.destroy', $item->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <input type="hidden" name="kode_booking"
                                                    value="{{ $item->booking_code }}">
                                                <button type="submit"
                                                    class="items-center px-px py-2 lg:py-2 w-24 lg:w-60 mt-px lg:mt-2.5 text-sm lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">Hapus</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    @empty
                    @endforelse
                    @if ($data != '')
                    @if (isset($diskon))
                    @php
                    $hitung_diskon = intval($diskon) / 100;
                    $result = intval($data->sum('total_price')) * $hitung_diskon;
                    @endphp
                    <p class="text-blue-600 text-2xl text-end">Total Harga : IDR
                        {{ number_format($data->sum('total_price') - $result) }}</p>
                    @else
                    {{-- <p class="text-blue-600 text-2xl text-end">Total Harga : IDR
                        {{ number_format($data->sum('total_price')) }}</p> --}}
                    <p class="text-blue-600 text-2xl text-end"
                        x-text="'Total Harga : IDR ' + new Intl.NumberFormat().format(sumPrice)"></p>
                    @endif
                    @endif
                </div>

                {{-- Checkout Order --}}
                <div class="flex space-x-3">
                    @if ($data != '' && $getBookingId != '')
                    <form action="{{ route('order.store') }}" method="POST">
                        @csrf
                        @if (isset($diskon))
                        @php
                        $hitung_diskon = intval($diskon) / 100;
                        $result = intval($data->sum('total_price')) * $hitung_diskon;
                        @endphp
                        <input type="hidden" name="harga" value="{{ $data->sum('total_price') - $result }}">
                        @else
                        <input type="hidden" name="harga" x-model="sumPrice">
                        @endif
                        {{-- <input type="hidden" name="booking_id" value="{{ $getBookingId->id }}"> --}}
                        <input type="hidden" name="booking_id" :value="selected_id">
                        <input type="hidden" name="nama_customer" value="{{ auth()->user()->first_name }}">
                        <input type="hidden" name="customer_id" value="{{ $traveller->id }}">
                        <input type="hidden" name="booking_code_arr" id="booking_code_arr" x-model="booking_code_arr">
                        <button type="submit"
                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                            Check Out Order
                        </button>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </x-be.traveller.sidebar-traveller>

    <script>
        function price() {
            return {
                booking_code_arr: [],
                price_per_booking: [],
                selected_id: [],
                sumPrice: 0,
                selectAll: false,
                sumTotal(price, id) {
                    if (this.selected_id.includes(id)) {
                        const idx = this.selected_id.indexOf(id)

                        this.selected_id = this.selected_id.filter(function(value, index){ 
                            // console.log(index)
                            return value != id;
                        })

                        this.price_per_booking = this.price_per_booking.filter(function(value, index){ 
                            return index != idx;
                        })
                    } else {
                        this.selected_id = [...this.selected_id, id]
                        this.price_per_booking = [...this.price_per_booking, price]
                    }
                    
                    this.sumPrice = this.price_per_booking.reduce((a, b) => Math.round(a) + Math.round(b), 0)

                    let checkboxes_count = document.querySelectorAll('.item_check').length;

                    if (this.price_per_booking.length !== checkboxes_count) {
                        this.selectAll = false
                    }

                    if (this.price_per_booking.length === checkboxes_count) {
                        this.selectAll = true
                    }
                },
                pilihSemua() {
                    this.selectAll = !this.selectAll
                    
                    let checkboxes = document.querySelectorAll('.item_check');
                    let allCheck = [];
                    let allPrice = [];
                    let allID = [];
                    checkboxes.forEach((val) => {
                        
                        console.log(val.value)
                        allCheck.push(val.value)
                        allPrice.push(Number(val.getAttribute('harga')))
                        allID.push(Number(val.getAttribute('data-id-product')))

                        this.booking_code_arr = this.selectAll ? allCheck : []
                        this.price_per_booking = this.selectAll ? allPrice : []
                        this.selected_id = this.selectAll ? allID : []
                    })

                    this.sumPrice = this.price_per_booking.reduce((a, b) => Math.round(a) + Math.round(b), 0)
                },
            }
        }

        function updateCountdown() {
            let countdown_length = document.querySelectorAll('.countdown_length');

            countdown_length.forEach((val) => {
                let countdown_key = val.getAttribute('data-countdown-key')
                const daysElement = document.getElementById(`days_${countdown_key}`);
                const hoursElement = document.getElementById(`hours_${countdown_key}`);
                const minutesElement = document.getElementById(`minutes_${countdown_key}`);
                const secondsElement = document.getElementById(`seconds_${countdown_key}`);


                let days = parseInt(daysElement.innerText);
                let hours = parseInt(hoursElement.innerText);
                let minutes = parseInt(minutesElement.innerText);
                let seconds = parseInt(secondsElement.innerText);

                if (days <= 0 && hours <= 0 && minutes <= 0 && seconds <= 0) {
                    seconds = 0;
                    minutes = 0;
                    hours = 0;
                    days = 0
                } else {

                    seconds--;

                    if (seconds < 0) {
                        seconds = 59;
                        minutes--;


                        if (minutes < 0) {
                            minutes = 59;
                            hours--;


                            if (hours < 0) {
                                hours = 23;
                                days--;
                            }
                        }
                    }
                }

                daysElement.innerText = padNumber(days);
                hoursElement.innerText = padNumber(hours);
                minutesElement.innerText = padNumber(minutes);
                secondsElement.innerText = padNumber(seconds);
            })

        }

        function padNumber(number) {
            return number.toString().padStart(2, '0');
        }

        setInterval(updateCountdown, 1000);
        /*
        var search = $("#cart_search").val();
        if(search!=''){
            location.replace('/cart?search=');
        }
        */
    </script>
</body>

</html>