<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        h1 {
            font-size: 30px;
            color: #000;
        }

        h2 {
            font-size: 20px;
            color: #000;
        }

        .input-group {
            display: flex;
            align-content: stretch;
        }

        .input-group-addon {
            border: 1px solid #ccc;
            padding-right: 2em;
            padding-left: 0.5em;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>

    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller>
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5">
                <h1 class="font-bold font-inter">Info Booking</h1>

                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                    <div class="">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                @if (session('success_data_booking'))
                                <div x-data="{ showMessage: true }" x-show="showMessage"
                                    class="bg-green-300 rounded-md p-3 m-5 flex justify-between"
                                    x-init="setTimeout(() => showMessage = false, 3000)">
                                    <p>{{ session('success_data_booking') }}</p>
                                </div>
                                @endif
                                <div class="text-sm font-inter font-semibold px-7 sm:pl-7 mt-5">
                                    <div class="sm:flex">
                                        <p class="pr-2 text-justify" style="vertical-align: text-top">Catatan :</p>
                                        <p>Informasi ini digunakan ketika Anda melakukan booking. <br>
                                            Masukkan Informasi sesuai identitas resmi yang masih berlaku.
                                        </p>
                                    </div>
                                </div>
                                <form action="{{ route('infobooking.updateDataBooking') }}" method="POST"
                                    x-data="infoBookingHandler()">
                                    @csrf
                                    @method('PUT')
                                    <div class="flex items-center pl-7 mt-5">
                                        <input id="akun_booking_status" name="akun_booking_status" type="checkbox"
                                            class="form-checkbox w-4 h-4 transition duration-150 ease-in-out"
                                            x-model="akun_booking" x-init="akunBookingCheckboxHandler" />
                                        <label for="akun_booking" class="block ml-2 text-sm text-gray-900 leading-5">
                                            Sama
                                            seperti pemilik akun
                                        </label>
                                    </div>
                                    <div class="sm:grid sm:grid-cols-2 p-5">
                                        <div class="p-2">
                                            <div>
                                                <label for="first_name_booking"
                                                    class="block text-sm font-medium leading-5">Nama
                                                    Depan</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <input id="first_name_booking" name="first_name_booking" type="text"
                                                        x-model="nama_depan_booking" required
                                                        x-bind:readonly="akun_booking"
                                                        :class="akun_booking && 'bg-slate-200'"
                                                        class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div>
                                                <label for="last_name_booking"
                                                    class="block text-sm font-medium leading-5">Nama
                                                    Belakang</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <input id="last_name_booking" name="last_name_booking"
                                                        x-model="nama_belakang_booking" type="text" required
                                                        x-bind:readonly="akun_booking"
                                                        :class="akun_booking && 'bg-slate-200'"
                                                        class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div>
                                                <label for="email_booking"
                                                    class="block text-sm font-medium leading-5">Email</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <input id="email_booking" name="email_booking" type="text"
                                                        x-model="email_booking" required x-bind:readonly="akun_booking"
                                                        :class="akun_booking && 'bg-slate-200'"
                                                        class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div>
                                                <label for="" class="block text-sm font-medium leading-5">Mobile</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <div class="input-group">
                                                        <template x-if="kewarganegaraan == 'indonesia'">
                                                            <input
                                                                class="border-gray-300 rounded-l-md shadow-sm sm:text-sm w-[50px] focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 bg-white"
                                                                name="kode_negara" type="text" readonly value="62">
                                                            />
                                                        </template>
                                                        {{-- <input type="hidden" name="kode_negara"
                                                            x-model="kode_negara" /> --}}
                                                        <input type="number" name="phone_number_booking"
                                                            id="phone_number_booking" x-model="no_telp_booking" required
                                                            :class="kewarganegaraan == 'indonesia' ? 'rounded-r-md' : 'rounded-md'"
                                                            class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-r-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                                    </div>
                                                    <template x-if="kewarganegaraan == 'other'">
                                                        <p class="py-2 text-xs text-kamtuu-primary">Please write phone
                                                            number with your
                                                            country
                                                            code.</p>
                                                    </template>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div>
                                                <label for="citizenship"
                                                    class="block text-sm font-medium leading-5">Kewarganegaraan</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select name="citizenship" id="citizenship"
                                                        x-model="kewarganegaraan"
                                                        class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                                        <option value="indonesia">Indonesia</option>
                                                        <option value="other">Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div>
                                                <label for="status_residen"
                                                    class="block text-sm font-medium leading-5">Status
                                                    Residen</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select name="status_residen" id="status_residen"
                                                        x-model="status_residen"
                                                        class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                                        <option value="residen">Residen</option>
                                                        <option value="non-residen">Non-Residen</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($errors->hasBag('data_booking'))
                                    <div class="bg-red-300 rounded-md p-3 m-5">
                                        @foreach ($errors->data_booking->toArray() as $key=>$value)
                                        @foreach ($value as $index=>$err)
                                        <p>- {{ $err}}</p>
                                        @endforeach
                                        @endforeach
                                    </div>
                                    @endif
                                    <div class="text-sm font-inter font-semibold px-7 sm:pl-7 mt-2">
                                        <div class="sm:flex">
                                            <p class="pr-2 text-justify" style="vertical-align: text-top">Catatan :</p>
                                            <p>Beberapa seller mungkin menerapkan kebijakan harga berbeda <br>
                                                antara residen dan non-residen.
                                            </p>
                                        </div>

                                    </div>
                                    <div class="p-5 grid justify-items-start">
                                        <button
                                            class="px-8 py-2 lg:text-md md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">Perbarui</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Identitas --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                    <div class="">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                @if (session('success_kitas_booking'))
                                <div x-data="{ showMessage: true }" x-show="showMessage"
                                    class="bg-green-300 rounded-md p-3 m-5 flex justify-between"
                                    x-init="setTimeout(() => showMessage = false, 3000)">
                                    <p>{{ session('success_kitas_booking') }}</p>
                                </div>
                                @endif
                                <h2 class="p-7 font-bold text-[#9E3D64]">Identitas</h2>
                                <div class="text-sm font-inter font-semibold text-justify px-7 sm:pl-7">
                                    <p class="text-[#000] font-inter font-semibold text-md">
                                        Boleh mengupload salah satunya atau keduanya. Pastikan kitas masih berlaku.
                                    </p>
                                </div>
                                <form action="{{ route('infobooking.updateKitasBooking') }}" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="md:grid md:grid-cols-2 p-5">
                                        <div class="p-2 space-y-5">
                                            <div>
                                                <label for="nik" class="block text-sm font-medium leading-5">KTP</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <input id="nik" name="nik" type="text" placeholder="Masukkan NIK"
                                                        value="{{ $profile->nik }}"
                                                        class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                                </div>
                                            </div>
                                            {{-- Dropzone KTP --}}
                                            <div class="md:m-2" x-data="displayImageKTP" id="fileupload">
                                                <div class="rounded border-gray-300 border-2 w-full h-[336px]">
                                                    <input
                                                        class="absolute z-10 opacity-0 w-full h-[336px] block clear-both"
                                                        type="file" name="ID_card_photo" id="ID_card_photo"
                                                        @change="selectedFile">
                                                    <template x-if="savedImage && !imageUrl">
                                                        <img src="{{ Storage::url($profile->ID_card_photo) }}"
                                                            class="object-contain rounded w-full h-full">
                                                    </template>

                                                    <template x-if="imageUrl">
                                                        <img :src="imageUrl"
                                                            class="object-contain rounded w-full h-full">
                                                    </template>

                                                    <template x-if="!imageUrl && !savedImage">
                                                        <div
                                                            class="flex justify-center items-center bg-gray-200 rounded w-full h-full px-5">
                                                            <div class="space-y-5">
                                                                <img src="{{ asset('storage/icons/Vector-1.svg') }}"
                                                                    class="block w-[25px] h-[25px] mx-auto my-auto"
                                                                    alt="upload-icons" title="upload-icons">
                                                                <p
                                                                    class="text-sm sm:text-base text-slate-500 text-justify md:text-center">
                                                                    Klik atau seret
                                                                    dan lepas foto
                                                                    Kitas
                                                                    yang akan diunggah.</p>
                                                            </div>
                                                        </div>
                                                    </template>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2 space-y-5">
                                            <div>
                                                <label for="passport_number"
                                                    class="block text-sm font-medium leading-5">Passport</label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <input id="passport_number" name="passport_number" type="text"
                                                        placeholder="Masukkan No Passport"
                                                        value="{{ $profile->passport_number }}"
                                                        class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                                </div>
                                            </div>
                                            {{-- Dropzone Passport --}}
                                            <div class="md:m-2" x-data="displayImagePassport" id="fileupload">
                                                <div class="rounded border-gray-300 border-2 w-full h-[336px]">
                                                    <input
                                                        class="absolute z-10 opacity-0 w-full h-[336px] block clear-both"
                                                        type="file" name="passport_photo" id="passport_photo"
                                                        @change="selectedFile">
                                                    <template x-if="savedImage && !imageUrl">
                                                        <img src="{{ Storage::url($profile->passport_photo) }}"
                                                            class="object-contain rounded w-full h-full">
                                                    </template>

                                                    <template x-if="imageUrl">
                                                        <img :src="imageUrl"
                                                            class="object-contain rounded w-full h-full">
                                                    </template>

                                                    <template x-if="!imageUrl && !savedImage">
                                                        <div
                                                            class="flex justify-center items-center bg-gray-200 rounded w-full h-full px-5">
                                                            <div class="space-y-5">
                                                                <img src="{{ asset('storage/icons/Vector-1.svg') }}"
                                                                    class="block w-[25px] h-[25px] mx-auto my-auto"
                                                                    alt="upload-icons" title="upload-icons">
                                                                <p
                                                                    class="text-sm sm:text-base text-slate-500 text-justify md:text-center">
                                                                    Klik atau seret dan lepas foto
                                                                    Kitas
                                                                    yang akan diunggah.</p>
                                                            </div>
                                                        </div>
                                                    </template>
                                                </div>
                                            </div>

                                            <div class="md:p-2">
                                                <div>
                                                    <label for="passport_active_period"
                                                        class="block text-sm font-medium leading-5">Berlaku
                                                        Sampai (Passport)</label>
                                                    <div class="mt-1 rounded-md shadow-sm">
                                                        <input id="passport_active_period" name="passport_active_period"
                                                            type="date" value="{{ $profile->passport_active_period }}"
                                                            class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($errors->hasBag('kitas_booking'))
                                    <div class="bg-red-300 rounded-md p-3 m-5">
                                        @foreach ($errors->kitas_booking->toArray() as $key=>$value)
                                        @foreach ($value as $index=>$err)
                                        <p>- {{ $err}}</p>
                                        @endforeach
                                        @endforeach
                                    </div>
                                    @endif
                                    <div class="p-7 sm:p-5 grid justify-items-start">
                                        <button
                                            class="px-8 py-2 lg:text-md md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">Perbarui</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-be.traveller.sidebar-traveller>

    <script>
        function infoBookingHandler() {

            return {
                nama_depan_booking:'', 
                nama_belakang_booking:'', 
                status_residen:'',
                kewarganegaraan:'indonesia',
                kode_negara:'',
                no_telp_booking:'',
                email_booking:'',
                akun_booking:0,

                akunBookingCheckboxHandler() {

                    Alpine.nextTick(() => { this.akun_booking = '{{$profile->akun_booking_status}}' === '1' ? true : false })
                    console.log('masuk', this.akun_booking)
                    this.$watch('akun_booking', (isChecked) => {
                        console.log('checked', isChecked)
                        console.log('checkbox', this.akun_booking)
                        if(isChecked === true){
                            console.log('isChecked', isChecked)
                            this.nama_depan_booking='{{auth()->user()->first_name}}'
                            this.nama_belakang_booking='{{$profile->last_name}}'
                            this.status_residen='{{$profile->status_residen}}'
                            this.kewarganegaraan='{{$profile->citizenship ?? 'indonesia'}}'
                            this.kode_negara='+62'
                            this.no_telp_booking='{{ auth()->user()->no_tlp }}'
                            this.email_booking='{{ auth()->user()->email }}'
                        }

                        let nama_depan = '{{auth()->user()->first_name}}'
                        if (isChecked === false && nama_depan !== null) {
                            this.nama_depan_booking='{{$profile->first_name_booking}}'
                            this.nama_belakang_booking='{{$profile->last_name_booking}}'
                            this.status_residen='{{$profile->status_residen}}'
                            this.kewarganegaraan='{{$profile->citizenship ?? 'indonesia'}}'
                            this.kode_negara='+81'
                            this.no_telp_booking='{{$profile->phone_number_booking}}'
                            this.email_booking='{{$profile->email_booking}}'
                        }
                    })
                },

            }
        }
    </script>

    <script>
        function displayImagePassport() {

            return {
                savedImage: '{{$profile->passport_photo}}',
                imageUrl: '',
                files:'',

                selectedFile(event) {
                this.files = event.target.files[0]
                this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                if (! event.target.files.length) return

                let file = event.target.files[0],
                    reader = new FileReader()

                reader.readAsDataURL(file)
                reader.onload = e => callback(e.target.result)
                },
            }
        }

        function displayImageKTP() {

            return {
                savedImage: '{{$profile->ID_card_photo}}',
                imageUrl: '',
                files:'',

                selectedFile(event) {
                this.files = event.target.files[0]
                this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                if (! event.target.files.length) return

                let file = event.target.files[0],
                    reader = new FileReader()

                reader.readAsDataURL(file)
                reader.onload = e => callback(e.target.result)
                },
            }
        }
    </script>
</body>

</html>
