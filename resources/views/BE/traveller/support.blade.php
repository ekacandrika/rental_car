<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        h1 {
            font-size: 30px;
            color: #000;
        }

        h2 {
            font-size: 20px;
            color: #000;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>

    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller>
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5">
                <h1 class="flex-1 ml-2 mt-1 whitespace-nowrap font-bold font-inter">Dukungan</h1>

                {{-- Data Diri --}}
                <div class="drop-shadow-xl sm:pb-5 sm:mt-5 mb-5 rounded">
                    <div class="grid sm:grid-cols-2 sm:h-40 gap-3">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <div class="p-5">
                                    <h2 class="font-bold text-[#000]">
                                        Topik Dukungan 1
                                    </h2>
                                    <p class="mt-2 font-semibold text-[#000] text-base">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <div class="p-5">
                                    <h2 class="font-bold text-[#000]">
                                        Topik Dukungan 2
                                    </h2>
                                    <p class="mt-2 font-semibold text-[#000] text-base">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="drop-shadow-xl sm:pb-5 sm:mt-5 mb-5 rounded">
                    <div class="grid sm:grid-cols-2 sm:h-40 gap-3">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <div class="p-5">
                                    <h2 class="font-bold text-[#000]">
                                        Topik Dukungan 3
                                    </h2>
                                    <p class="mt-2 font-semibold text-[#000] text-base">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <div class="p-5">
                                    <h2 class="font-bold text-[#000]">
                                        Topik Dukungan 4
                                    </h2>
                                    <p class="mt-2 font-semibold text-[#000] text-base">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="drop-shadow-xl sm:pb-5 sm:mt-5 mb-5 rounded">
                    <div class="grid sm:grid-cols-2 sm:h-40 gap-3">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <div class="p-5">
                                    <h2 class="font-bold text-[#000]">
                                        Topik Dukungan 5
                                    </h2>
                                    <p class="mt-2 font-semibold text-[#000] text-base">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <div class="p-5">
                                    <h2 class="font-bold text-[#000]">
                                        Topik Dukungan 6
                                    </h2>
                                    <p class="mt-2 font-semibold text-[#000] text-base">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-be.traveller.sidebar-traveller>

</body>

</html>
