<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        h1 {
            font-size: 30px;
            color: #000;
        }

        h2 {
            font-size: 20px;
            color: #000;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>

    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller></x-be.traveller.sidebar-traveller>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pr-10" style="padding-left: 315px">
            <h1 class="font-bold font-inter">Reset Password</h1>

            {{-- Data Diri --}}
            <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                <div class="">
                    <div class="w-auto flex bg-[#ffff] rounded mr-5">
                        <div class="w-full">
                            <div class="grid grid-cols-1 p-10">
                                <form x-data="{ password: '', password_confirm: '' }"
                                    action="{{ route('reset.password.post') }}" method="POST">
                                    @csrf

                                    <input type="hidden" name="token" value="{{ $token }}">

                                    <div class="p-2">
                                        <div>
                                            <label for="" class="block text-sm font-medium leading-5">Email
                                                Address</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <input id="email" name="email" type="email" required
                                                    class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                            </div>
                                        </div>
                                        <div class="mt-6">
                                            <label for="password"
                                                class="block text-sm font-medium text-gray-700 leading-5">
                                                Password </label>

                                            <div class="mt-1 rounded-md shadow-sm">
                                                <input x-model="password" id="password" type="password" name="password"
                                                    required
                                                    class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                            </div>
                                        </div>

                                        <div class="mt-6">
                                            <label for="password"
                                                class="block text-sm font-medium text-gray-700 leading-5">
                                                Re-Enter Password </label>

                                            <div class="mt-1 rounded-md shadow-sm">
                                                <input x-model="password_confirm" id="password" type="password"
                                                    name="password_confirmation" required
                                                    class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                            </div>
                                        </div>

                                        <div class="my-5">
                                            <!-- Validation Alpine Js -->
                                            <div class="flex justify-start mt-3 ml-4 p-1">
                                                <ul>
                                                    <li class="flex items-center py-1">
                                                        <div :class="{
                                                            'bg-green-200 text-green-700': password == password_confirm && password
                                                                .length > 0,
                                                            'bg-red-200 text-red-700': password !=
                                                                password_confirm || password.length == 0
                                                        }" class=" rounded-full p-1 fill-current ">
                                                            <svg class="w-4 h-4" fill="none" viewBox="0 0 24 24"
                                                                stroke="currentColor">
                                                                <path
                                                                    x-show="password == password_confirm && password.length > 0"
                                                                    stroke-linecap="round" stroke-linejoin="round"
                                                                    stroke-width="2" d="M5 13l4 4L19 7" />
                                                                <path
                                                                    x-show="password != password_confirm || password.length == 0"
                                                                    stroke-linecap="round" stroke-linejoin="round"
                                                                    stroke-width="2" d="M6 18L18 6M6 6l12 12" />

                                                            </svg>
                                                        </div>
                                                        <span :class="{
                                                                'text-green-700': password == password_confirm && password.length >
                                                                    0,
                                                                'text-red-700': password != password_confirm || password
                                                                    .length == 0
                                                            }" class="font-medium text-sm ml-3"
                                                            x-text="password == password_confirm && password.length > 0 ? 'Passwords match' : 'Passwords do not match' "></span>
                                                    </li>
                                                    <li class="flex items-center py-1">
                                                        <div :class="{
                                                            'bg-green-200 text-green-700': password.length >
                                                                7,
                                                            'bg-red-200 text-red-700': password.length < 7
                                                        }" class=" rounded-full p-1 fill-current ">
                                                            <svg class="w-4 h-4" fill="none" viewBox="0 0 24 24"
                                                                stroke="currentColor">
                                                                <path x-show="password.length > 7"
                                                                    stroke-linecap="round" stroke-linejoin="round"
                                                                    stroke-width="2" d="M5 13l4 4L19 7" />
                                                                <path x-show="password.length < 7"
                                                                    stroke-linecap="round" stroke-linejoin="round"
                                                                    stroke-width="2" d="M6 18L18 6M6 6l12 12" />

                                                            </svg>
                                                        </div>
                                                        <span :class="{
                                                                'text-green-700': password.length > 7,
                                                                'text-red-700': password
                                                                    .length < 7
                                                            }" class="font-medium text-sm ml-3"
                                                            x-text="password.length > 7 ? 'The minimum length is reached' : 'At least 8 characters required' "></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="pt-10 grid justify-items-start">
                                            <button type="submit"
                                                class="px-8 py-2 lg:text-md md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">Ubah
                                                Sandi</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>