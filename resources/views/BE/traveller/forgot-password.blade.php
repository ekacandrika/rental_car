<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>


    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        h1 {
            font-size: 30px;
            color: #000;
        }

        h2 {
            font-size: 20px;
            color: #000;
        }

        .menu-malasngoding li a {
            display: inline-block;
            color: white;
            /* text-align: center; */
            text-decoration: none;
        }

        .menu-malasngoding li a:hover {
            background-color: none;
        }

        li.dropdown {
            display: inline-block;
            margin-right: 80px
        }

        .dropdown:hover .isi-dropdown {
            display: block;
        }

        .isi-dropdown a:hover {
            color: #fff !important;
            width: 100%;
        }

        .isi-dropdown {
            position: absolute;
            display: none;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
            background-color: #f9f9f9;
            width: 100%;
        }

        .isi-dropdown a {
            color: #3c3c3c !important;
            padding: 1%;
        }

        .isi-dropdown a:hover {
            color: #232323 !important;
            background: #f3f3f3 !important;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>

    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller>
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5">
                <h1 class="font-bold font-inter">Reset Password</h1>

                {{-- Data Diri --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                    <div class="">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <div class="grid grid-cols-1 p-5 md:p-10">

                                    @if (Session::has('message'))
                                    <div class="alert alert-success" role="alert">
                                        {{ Session::get('message') }}
                                    </div>
                                    @endif

                                    <form action="{{ route('forget.password.post') }}" method="POST">
                                        @csrf

                                        <div class="p-2">
                                            <div class="md:mt-6">
                                                <label for="email"
                                                    class="block text-sm font-medium text-gray-700 leading-5">
                                                    Email </label>

                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <input id="email" type="email" name="email" required
                                                        class="appearance-none block w-full lg:w-5/6 px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                                </div>
                                            </div>

                                            <div class="pt-10 grid justify-items-start">
                                                <button type="submit"
                                                    class="px-8 py-2 text-sm md:text-base lg:text-md text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">Email
                                                    Password Reset Link</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-be.traveller.sidebar-traveller>

</body>

</html>
