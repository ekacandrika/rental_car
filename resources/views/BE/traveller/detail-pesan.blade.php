<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">


    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])


    <style>
        #akun:checked+#akun {
            display: block;
        }


        #inbox:checked+#inbox {
            display: block;
        }


        h1 {
            font-size: 30px;
            color: #000;
        }


        h2 {
            font-size: 20px;
            color: #000;
        }
    </style>


    <!-- Styles -->
    @livewireStyles
</head>


<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>


    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller>
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5">
                <a href="" class="flex">
                    <img src="{{ asset('storage/icons/back.png') }}" alt="user-profile"
                        class="object-cover object-center w-25 h-25 p-2 overflow-hidden border-indigo-600 ">
                    <h2 class="flex-1 ml-2 mt-1 whitespace-nowrap font-bold font-inter">Nama Toko</h2>
                </a>


                {{-- Data Diri --}}
                <div id="main" class="m-8 grid grid-row-3 grid-flow-col gap-1">
                    <div class="drop-shadow-xl w-auto bg-[#ffff] row-span-6 rounded-lg p-5 m-2 text-center"
                        style="height: 39.4rem">
                        <span class="mb-2 text-sm font-semibold text-[#000] bg-gray-300 rounded-md">
                            {{-- {{\Illuminate\Support\Carbon::now()->format('d F, Y')}}--}}
                        </span>
                        <table style="margin-right: -5rem" class="text-left mt-5">
                            <tbody>
                                <tr>
                                    <td class="w-[40rem]" style="vertical-align: top">
                                        <div class="p-2">
                                            <div class="chat-box">
                                                <div class="chat-messages">
                                                    @forelse($chatAll as $chat)
                                                    {{-- {{dd($chat->users)}}--}}
                                                    @if($chat->users->role == 'traveller')
                                                    <p class="text-kamtuu-primary text-base font-bold"
                                                        style="vertical-align: text-top">
                                                        {{$chat->users->first_name}}
                                                    </p>
                                                    @else
                                                    <p class="text-kamtuu-third text-base font-bold"
                                                        style="vertical-align: text-top">
                                                        {{$chat->users->first_name}}
                                                    </p>
                                                    @endif
                                                    <p class="text-[#000] text-base" style="font-size:small">
                                                        {{$chat->text}}
                                                    </p>


                                                    <p class="text-[#000] text-base mt-2" style="font-size:small">
                                                        {{$chat->created_at->diffForHumans()}}
                                                    </p>
                                                    <hr />
                                                    @empty


                                                    @endforelse
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="mt-80 rounded-md shadow-sm">
                            {{-- {{dd($produk)}}--}}
                            <form class="chat-form" method="POST"
                                action="{{route('inbox.store',['id' => $produk->id,'user_id' => $produk->user_id])}}">
                                @csrf
                                <div class="flex w-3/4">
                                    <input type="text" name="text" placeholder="Type your message here">
                                    <button type="submit">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="drop-shadow-xl w-auto bg-[#ffff] row-span-2 rounded-lg p-5 m-2" style="height: 17rem">


                        {{-- <p class="mb-1 text-lg font-semibold text-[#51B449] text-center">--}}
                            {{-- <u>Pesanan Telah Dikonfirmasi</u>--}}
                            {{-- </p>--}}
                        {{-- <p class="mb-1 text-lg font-bold text-[#000]">--}}
                            {{-- Nama Mobil--}}
                            {{-- </p>--}}
                        {{-- <p class="mb-2 text-sm font-semibold text-[#000]">--}}
                            {{-- Jumat, 12 Agustus 2022 • 09.00--}}
                            {{-- </p>--}}
                        {{-- <p class="mb-1 text-sm font-bold text-[#000]">--}}
                            {{-- Dari--}}
                            {{-- </p>--}}
                        {{-- <div class="flex lg:text-md md:text-base text-[#000] duration-300 bg-[#FFFFFF] rounded-lg">
                            --}}
                            {{-- <img src="{{ asset('storage/icons/location-dot-solid-1.svg') }}"
                                class="w-[16px] h-[16px] mt-1" --}} {{-- alt="user" title="user">--}}
                            {{-- <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm font-bold">--}}
                                {{-- Bandara International YIA--}}
                                {{-- </p>--}}
                            {{-- </div>--}}
                        {{-- <p class="ml-1">&#166;</p>--}}
                        {{-- <p class="ml-1">&#166;</p>--}}
                        {{-- <p class="mb-1 text-sm font-bold text-[#000]">--}}
                            {{-- Ke--}}
                            {{-- </p>--}}
                        {{-- <div class="flex lg:text-md md:text-base text-[#000] duration-300 bg-[#FFFFFF] rounded-lg">
                            --}}
                            {{-- <img src="{{ asset('storage/icons/location-dot-solid-1.svg') }}"
                                class="w-[16px] h-[16px] mt-1" --}} {{-- alt="user" title="user">--}}
                            {{-- <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm font-bold">--}}
                                {{-- Hotel Yogyakarta--}}
                                {{-- </p>--}}
                            {{-- </div>--}}
                    </div>
                    <div class="drop-shadow-xl w-auto bg-[#ffff] row-span-2 rounded-lg p-5 m-2 text-center"
                        style="height: 16.5rem">
                        <div class="p-2">
                            <img src="{{ asset('storage/img/logo-transfer.png') }}" alt="user-profile"
                                class="object-cover object-center w-30 h-30 p-2 rounded-full overflow-hidden border-indigo-600 ml-auto mr-auto">
                        </div>
                        <p class="flex-1 whitespace-nowrap font-inter text-lg font-bold">
                            Kwantar Trans
                        </p>
                        <p class="flex-1 whitespace-nowrap font-inter text-md">
                            Bergabung sejak 2022
                        </p>
                        <div class="flex p-2" style="margin-left: 6.5rem">
                            <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[30px] h-[29px] mt-1" alt="user"
                                title="user">
                            <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[30px] h-[29px] mt-1" alt="user"
                                title="user">
                            <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[30px] h-[29px] mt-1" alt="user"
                                title="user">
                            <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[30px] h-[29px] mt-1" alt="user"
                                title="user">
                            <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[30px] h-[29px] mt-1" alt="user"
                                title="user">
                        </div>
                    </div>
                    <div class="row-span-2 rounded-lg m-1" style="height: 2rem">
                        <a href="{{route('inboxTravellerAdmin', [$admin->id, $sender])}}">


                            <button
                                class="flex w-full px-8 py-2 lg:text-md md:text-base text-white duration-300 bg-[#51B449] rounded-lg hover:bg-[#51B449] hover:text-white">
                                <img src="{{ asset('storage/icons/phone-solid 1.svg') }}" class="w-[16px] h-[16px] mt-1"
                                    alt="user" title="user">
                                <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">
                                    Hubungi Help Center
                                </p>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </x-be.traveller.sidebar-traveller>

</body>


</html>
