<!DOCTYPE html>
{{--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">--}}

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--
    <meta name="csrf-token" content="{{ csrf_token() }}">--}}

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        h1 {
            font-size: 30px;
            color: #000;
        }

        h2 {
            font-size: 20px;
            color: #000;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>

    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller>
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5">
                <h1 class="font-bold font-inter text-center sm:text-left">Get Claim Your Kupon</h1>

                {{-- Get Kupon --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                    <div class="">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <div class="p-2">
                                    <div class="">
                                        <form method="POST" action="{{ route('traveller.kupon.claim') }}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            @method('PUT')
                                            {{-- Markup --}}
                                            <div class="p-6">
                                                <x-flash-message></x-flash-message>

                                                <input type="hidden" value="{{Auth::user()->id}}" name="user_id">
                                                <label for="text"
                                                    class="block mb-2 text-xl font-bold text-[#333333] ">Code</label>
                                                <input name="kodeKupon" type="text" id="text"
                                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full sm:w-96 p-2.5 "
                                                    placeholder="AAABBBCCC">
                                            </div>


                                            <div class="p-6">
                                                <button class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2">Get
                                                    Kupon
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <h1 class="font-bold font-inter">List Kupon</h1>
                {{-- Get Kupon --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                    <div class="">
                        <div class="w-auto flex bg-[#ffff] rounded">
                            <div class="w-full">
                                <div class="p-2">
                                    <div class="">

                                        <div class="p-5 border rounded-md bg-white my-5 overflow-x-scroll">

                                            {{-- <p class="text-4xl font-semibold my-5">List Kupon</p>--}}
                                            <table
                                                class="table-auto w-full text-center border-separate border border-slate-500 overflow-scroll">
                                                <thead>
                                                    <tr>
                                                        <th class="border border-slate-600">No</th>
                                                        <th class="border border-slate-600">Judul Kupon</th>
                                                        <th class="border border-slate-600">Kode Kupon</th>
                                                        <th class="border border-slate-600">Expired</th>
                                                        {{-- <th class="border border-slate-600">Status</th>--}}
                                                        {{-- <th class="border border-slate-600">Action</th>--}}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse($kuponTraveller as $key=>$kpn)
                                                    <tr>
                                                        <td class="border border-slate-600">{{$key+1}}</td>
                                                        <td class="border border-slate-600">{{$kpn->judulKupon}}</td>
                                                        <td class="border border-slate-600">{{$kpn->kodeKupon}}</td>
                                                        <td class="border border-slate-600">
                                                            <script>
                                                                CountDownTimer('{{$kpn->created_at}}', 'countdown');
                                                        function CountDownTimer(dt, id)
                                                        {
                                                        var end = new Date('{{$kpn->expiredDate}}');
                                                        var _second = 1000;
                                                        var _minute = _second * 60;
                                                        var _hour = _minute * 60;
                                                        var _day = _hour * 24;
                                                        var timer;
                                                        function showRemaining() {
                                                        var now = new Date();
                                                        var distance = end - now;
                                                        if (distance < 0) {
    
                                                        clearInterval(timer);
                                                        document.getElementById(id).innerHTML = '<b>Kupon Telah Kadaluarsa</b> ';
                                                        return;
                                                        }
                                                        var days = Math.floor(distance / _day);
                                                        var hours = Math.floor((distance % _day) / _hour);
                                                        var minutes = Math.floor((distance % _hour) / _minute);
                                                        var seconds = Math.floor((distance % _minute) / _second);
    
                                                            document.getElementById(id).innerHTML ='<h4 class="text-kamtuu-second mx-2">Kupon Dapat Digunakan Sampai</h4>';
                                                            document.getElementById(id).innerHTML += days + 'Hari ';
                                                            document.getElementById(id).innerHTML += hours + 'Jam ';
                                                            document.getElementById(id).innerHTML += minutes + 'Menit ';
                                                            document.getElementById(id).innerHTML += seconds + 'Detik';
                                                           }
                                                        timer = setInterval(showRemaining, 1000);
                                                        }
                                                            </script>
                                                            <div class="text-kamtuu-primary md:flex" id="countdown">
                                                            </div>
                                                        </td>
                                                        {{-- <td class="border border-slate-600">{{$kpn->status ??
                                                            "Active"}}</td>--}}
                                                        {{-- <td class="border border-slate-600"> --}}
                                                            {{-- <form
                                                                action="{{ route('pajak.local.destroy',$kpn->id) }}"
                                                                method="POST">--}}
                                                                {{-- <a class="btn btn-primary" --}} {{--
                                                                    href="{{ route('pajak.local.edit',$kpn->id) }}">Edit</a>--}}
                                                                {{-- @csrf--}}
                                                                {{-- @method('DELETE')--}}

                                                                {{-- <button type="submit"
                                                                    class="btn btn-accent">Delete</button>--}}
                                                                {{-- </form>--}}
                                                            {{-- </td> --}}
                                                        @empty
                                                        <td colspan="4">Tidak Ada Kupon</td>
                                                    </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                            {{-- {{ $kupons->links() }}--}}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-be.traveller.sidebar-traveller>

</body>

</html>
