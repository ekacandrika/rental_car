<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>




    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        h1 {
            font-size: 30px;
            color: #000;
        }

        h2 {
            font-size: 20px;
            color: #000;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.traveller.navbar-traveller></x-be.traveller.navbar-traveller>

    {{-- Sidebar --}}
    <x-be.traveller.sidebar-traveller></x-be.traveller.sidebar-traveller>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pr-10" style="padding-left: 315px">
            <h1 class="font-bold font-inter">Tambah Objek Destinasi</h1>

            {{-- Data Diri --}}
            <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                <div class="">
                    <div class="w-auto flex bg-[#ffff] rounded mr-5">
                        <div class="w-full">
                            <form action="">
                                <div class="grid grid-cols-2 p-5">
                                    <div class="p-2">
                                        <div>
                                            <label for=""
                                                class="block text-sm font-medium leading-5">Pulau</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <select name="" id="pulau"
                                                    class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                                    <option selected disabled>Pilih Pulau</option>
                                                    <option value="">Pulau 1</option>
                                                    <option value="">Pulau 2</option>
                                                    <option value="">Pulau 3</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            <label for=""
                                                class="block text-sm font-medium leading-5">Provinsi</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <select name="" id="provinsi"
                                                    class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                                    <option selected disabled>Pilih Provinsi</option>
                                                    @foreach ($provinces as $provinsi)
                                                        <option value="{{ $provinsi->id }}">{{ $provinsi->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            <label for=""
                                                class="block text-sm font-medium leading-5">Kabupaten</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <select name="" id="kabupaten"
                                                    class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                                    <option selected disabled>Pilih Kabupaten</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            <label for="" class="block text-sm font-medium leading-5">Objek
                                                Destinasi</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <input id="" name="" type="text" required
                                                    class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="p-5 grid justify-items-start">
                                    <button
                                        class="px-8 py-2 lg:text-md md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $('#pulau').select2();

        $('#kabupaten').select2();
    </script>
    <script>
        // $('#provinsi').select2();
    </script>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(function() {
                // provinsi
                $('#provinsi').on('change', function() {
                    let id_provinsi = $('#provinsi').val();
                    console.log(id_provinsi);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getkabupaten') }}",
                        data: {
                            id_provinsi: id_provinsi
                        },
                        cache: false,

                        success: function(msg) {
                            $('#kabupaten').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })

                // kabupaten
                $('#kabupaten').on('change', function() {
                    let id_kabupaten = $('#kabupaten').val();
                    console.log(id_kabupaten);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getkecamatan') }}",
                        data: {
                            id_kabupaten: id_kabupaten
                        },
                        cache: false,

                        success: function(msg) {
                            $('#kecamatan').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })
            })
        });
    </script>

</body>

</html>
