<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        /* CHECKBOX TOGGLE SWITCH */
        /* @apply rules for documentation, these do not work as inline style */
        .switch {
            position: relative;
            display: inline-block;
            width: 100px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #23AEC1;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #9E3D64;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #23AEC1;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(68px);
            -ms-transform: translateX(68px);
            transform: translateX(68px);
        }

        .on {
            display: none;
        }

        .on,
        .off {
            color: white;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            font-size: 10px;
            font-family: Verdana, sans-serif;
        }

        input:checked+.slider .on {
            display: block;
        }

        input:checked+.slider .off {
            display: none;
        }

        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }

        .destindonesia {
            display: none;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>

    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-2xl font-bold font-inter text-[#333333]">Create Section</span>
                <div class="flex gap-5 my-3">
                    <div class="bg-white rounded-lg p-3 w-full">
                        <div>
                            <form method="POST" action="{{ route('section.store') }}" enctype="multipart/form-data">
                                @csrf

                                <div>
                                    <p class="font-bold">Tampilkan Section</p>
                                </div>
                                <div class="flex justify-start">
                                    <label class="switch">
                                        <input type="checkbox" id="checkbox1" name="status_section" value="off">
                                        <div class="slider round">
                                            <span class="on">Show</span>
                                            <span class="off">Hide</span>
                                        </div>
                                    </label>
                                </div>


                                <div class="form-group">
                                    <p class="py-2 font-bold">Section</p>
                                    <select
                                        class="select w-1/3 select-bordered my-2 form-select appearance-none
                                              block
                                              w-full
                                              px-3
                                              py-1.5
                                              text-base
                                              font-normal
                                              text-gray-700
                                              bg-white bg-clip-padding bg-no-repeat
                                              border border-solid border-gray-300
                                              rounded
                                              transition
                                              ease-in-out
                                              m-0
                                              focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                        id="position-option" name="section">
                                        @foreach ($sects as $sect)
                                            <option value="{{ $sect }}">{{ $sect }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Title Section</label>
                                    <br>
                                    <input type="text" name="title_section" class="form-control rounded-lg" />
                                    @error('title_section')
                                    <p class="mx-2 py-2 font-bold text-sm text-red-400"><span>{{$message}}</span></p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <br>
                                    <textarea id="summernoteSection" name="detail_section"></textarea>
                                    @error('detail_section')
                                    <p class="mx-2 py-2 font-bold text-sm text-red-400"><span>{{$message}}</span></p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <div class="flex justify-start">
                                        <div class="mb-4 p-4 text-sm w-80">
                                            <div class="font-bold mb-2">Upload Image Preview</div>
                                            <div class="" x-data="previewImage()">

                                                <label for="logo">
                                                    <div
                                                        class="w-full h-48 rounded bg-gray-100 border border-gray-200 flex items-center justify-center overflow-hidden">
                                                        <img x-show="imageUrl" :src="imageUrl"
                                                            class="w-full object-cover">
                                                        <div x-show="!imageUrl"
                                                            class="text-gray-300 flex flex-col items-center">
                                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 "
                                                                fill="none" viewBox="0 0 24 24" stroke="currentColor"
                                                                stroke-width="2">
                                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                                    d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12" />
                                                            </svg>
                                                            <div>Image Preview</div>
                                                        </div>
                                                    </div>
                                                </label>
                                                <div>
                                                    <label for="logo" class="block mb-2 mt-4 font-bold">Upload
                                                        image..</label>
                                                    <input class="w-full cursor-pointer" type="file"
                                                        name="image_section" id="logo" @change="fileChosen">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @error('image_section')
                                    <p class="mx-2 py-2 font-bold text-sm text-red-400"><span>{{$message}}</span></p>
                                    @enderror
                                </div>
                                <div class="form-group text-end">
                                    <button type="submit"
                                        class="bg-kamtuu-second p-2 rounded-lg text-white hover:bg-kamtuu-primary">
                                        Simpan
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <span class="text-2xl font-bold font-inter text-[#333333]">Home Image Slider</span>
                <div class="flex gap-5 my-3">
                    <div class="bg-white rounded-lg p-3 w-full">
                        <div>
                            <form method="POST" action="{{ route('section.homeGallery') }}"
                                enctype="multipart/form-data">
                                @csrf

                                {{-- Foto --}}
                                <div class="py-2" x-data="displayImage()">
                                    <input class="py-2" type="file" accept="image/*" id="images_gallery"
                                        name="images_gallery[]" @change="selectedFile" multiple>
                                    <template x-if="images.length < 1">
                                        <div
                                            class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                            <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                width="20px" height="20px">
                                        </div>
                                    </template>
                                    <template x-if="images.length >= 1">
                                        <div class="flex">
                                            <template x-for="(image, index) in images" :key="index">
                                                <div class="flex justify-center items-center">
                                                    <img :src="image"
                                                        class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                        :alt="'upload' + index">
                                                    {{-- <input type="hidden" :name="'image['+index+']'"
                                                        x-model="data[index]" multiple> --}}
                                                    {{-- <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                                        <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                            alt="" width="25px" @click="removeImage(index)">
                                                    </button> --}}
                                                </div>
                                            </template>
                                        </div>
                                    </template>
                                </div>
                                <div class="form-group text-end">
                                    <button type="submit"
                                        class="bg-kamtuu-second p-2 rounded-lg text-white hover:bg-kamtuu-primary">
                                        Simpan
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                {{-- list section --}}
                <div class="">

                    <div class="min-h-screen py-6 flex flex-col justify-center sm:py-12 w-full ">
                        <h1 class="text-2xl font-bold">List Section</h1>
                        <section class="my-10 space-y-8 sm:py-20 bg-white">
                            <div style='width:800px;'
                                class="container flex flex-row items-stretch justify-center w-full max-w-4xl space-x-12"
                                x-data="{ tab: 1 }">
                                <div class="flex flex-col justify-start w-1/4 space-y-4">
                                    <a class="px-4 py-2 text-sm"
                                        :class="{ 'z-20 border-l-2 transform translate-x-2 border-blue-500 font-bold': tab ===
                                                1, ' transform -translate-x-2': tab !== 1 }"
                                        href="#" @click.prevent="tab = 1">
                                        Kamtuu
                                    </a>
                                    <a class="px-4 py-2 text-sm"
                                        :class="{ 'z-20 border-l-2 transform translate-x-2 border-blue-500 font-bold': tab ===
                                                2, ' transform -translate-x-2': tab !== 2 }"
                                        href="#" @click.prevent="tab = 2" @click.prevent="tab = 2">
                                        Agen
                                    </a>
                                    <a class="px-4 py-2 text-sm"
                                        :class="{ 'z-20 border-l-2 transform translate-x-2 border-blue-500 font-bold': tab ===
                                                3, ' transform -translate-x-2': tab !== 3 }"
                                        href="#" @click.prevent="tab = 3" @click.prevent="tab = 3">
                                        Traveller
                                    </a>
                                    <a class="px-4 py-2 text-sm"
                                        :class="{ 'z-20 border-l-2 transform translate-x-2 border-blue-500 font-bold': tab ===
                                                4, ' transform -translate-x-2': tab !== 4 }"
                                        href="#" @click.prevent="tab = 4" @click.prevent="tab = 4">
                                        Seller
                                    </a>
                                    <a class="px-4 py-2 text-sm"
                                        :class="{ 'z-20 border-l-2 transform translate-x-2 border-blue-500 font-bold': tab ===
                                                5, ' transform -translate-x-2': tab !== 5 }"
                                        href="#" @click.prevent="tab = 5" @click.prevent="tab = 5">
                                        Corporate
                                    </a>
                                    <a class="px-4 py-2 text-sm"
                                        :class="{ 'z-20 border-l-2 transform translate-x-2 border-blue-500 font-bold': tab ===
                                                6, ' transform -translate-x-2': tab !== 6 }"
                                        href="#" @click.prevent="tab = 6" @click.prevent="tab = 6">
                                        Official Store
                                    </a>
                                    <a class="px-4 py-2 text-sm"
                                        :class="{ 'z-20 border-l-2 transform translate-x-2 border-blue-500 font-bold': tab ===
                                                7, ' transform -translate-x-2': tab !== 7 }"
                                        href="#" @click.prevent="tab = 7" @click.prevent="tab = 7">
                                        Kerja Sama
                                    </a>
                                    <a class="px-4 py-2 text-sm"
                                        :class="{ 'z-20 border-l-2 transform translate-x-2 border-blue-500 font-bold': tab ===
                                                8, ' transform -translate-x-2': tab !== 8 }"
                                        href="#" @click.prevent="tab = 8" @click.prevent="tab = 8">
                                        Layanan
                                    </a>
                                </div>
                                <div class="w-2/4">
                                    <div class="space-y-6" x-show="tab === 1">
                                        @foreach ($kamtuuSets as $konten)
                                            <h3 class="text-xl font-bold leading-tight" x-show="tab === 1"
                                                x-transition:enter="transition duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                                {{ $konten->title_section }}
                                            </h3>
                                            <p class="text-base text-gray-600" x-show="tab === 1"
                                                x-transition:enter="transition delay-100 duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                            <p class="truncate">{!! $konten->detail_section !!}</p>
                                            </p>
                                            <div class="flex">
                                                <a href="{{ route('section.edit', $konten->id) }}"
                                                    class="inline-flex items-center justify-center px-8 pt-3 pb-2 mt-4 text-lg text-center text-white no-underline bg-blue-500 border-blue-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:bg-blue-500 focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text-lg"
                                                    class="text-base" x-show="tab === 1"
                                                    x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                    x-transition:enter-start="opacity-0">
                                                    Edit
                                                </a>
                                                &nbsp;&nbsp;
                                                <form action="{{route("section.delete",$konten->id)}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button 
                                                        class="inline-flex items-center justify-between px-8 py-3 mt-4 text-lg text-white text-center no-underline bg-red-500 border-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text"
                                                        class="text-base" x-show="tab === 1"
                                                        x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                        x-transition:enter-start="opacity-0"
                                                        >
                                                        Hapus
                                                    </button>
                                                </form>
                                            </div>
                                            <hr>
                                        @endforeach
                                    </div>
                                    <div class="space-y-6" x-show="tab === 2">
                                        @foreach ($agenSets as $konten)
                                            <h3 class="text-xl font-bold leading-tight" x-show="tab === 2"
                                                x-transition:enter="transition duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                                {{ $konten->title_section }}
                                            </h3>
                                            <p class="text-base text-gray-600" x-show="tab === 2"
                                                x-transition:enter="transition delay-100 duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                            <p class="truncate">{!! $konten->detail_section !!}</p>
                                            </p>
                                            <div class="flex">
                                                <a href="{{ route('section.edit', $konten->id) }}"
                                                    class="inline-flex items-center justify-center px-8 pt-3 pb-2 mt-4 text-lg text-center text-white no-underline bg-blue-500 border-blue-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:bg-blue-500 focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text-lg"
                                                    class="text-base" x-show="tab === 2"
                                                    x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                    x-transition:enter-start="opacity-0">
                                                    Edit
                                                </a>
                                                &nbsp;&nbsp;
                                                <form action="{{route("section.delete",$konten->id)}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button 
                                                        class="inline-flex items-center justify-between px-8 py-3 mt-4 text-lg text-white text-center no-underline bg-red-500 border-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text"
                                                        class="text-base" x-show="tab === 2"
                                                        x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                        x-transition:enter-start="opacity-0"
                                                        >
                                                        Hapus
                                                    </button>
                                                </form>
                                            </div>
                                            <hr>
                                        @endforeach
                                    </div>
                                    <div class="space-y-6" x-show="tab === 3">
                                        @foreach ($travellerSets as $konten)
                                            <h3 class="text-xl font-bold leading-tight" x-show="tab === 3"
                                                x-transition:enter="transition duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                                {{ $konten->title_section }}
                                            </h3>
                                            <p class="text-base text-gray-600" x-show="tab === 3"
                                                x-transition:enter="transition delay-100 duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                            <p class="truncate">{!! $konten->detail_section !!}</p>
                                            </p>
                                            <div class="flex">
                                                <a href="{{ route('section.edit', $konten->id) }}"
                                                    class="inline-flex items-center justify-center px-8 pt-3 pb-2 mt-4 text-lg text-center text-white no-underline bg-blue-500 border-blue-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:bg-blue-500 focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text-lg"
                                                    class="text-base" x-show="tab === 3"
                                                    x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                    x-transition:enter-start="opacity-0">
                                                    Edit
                                                </a>
                                                &nbsp;&nbsp;
                                                <form action="{{route("section.delete",$konten->id)}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button 
                                                        class="inline-flex items-center justify-between px-8 py-3 mt-4 text-lg text-white text-center no-underline bg-red-500 border-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text"
                                                        class="text-base" x-show="tab === 3"
                                                        x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                        x-transition:enter-start="opacity-0"
                                                        >
                                                        Hapus
                                                    </button>
                                                </form>
                                            </div>
                                            <hr>
                                        @endforeach
                                    </div>
                                    <div class="space-y-6" x-show="tab === 4">
                                        @foreach ($sellerSets as $konten)
                                            <h3 class="text-xl font-bold leading-tight" x-show="tab === 4"
                                                x-transition:enter="transition duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                                {{ $konten->title_section }}
                                            </h3>
                                            <p class="text-base text-gray-600" x-show="tab === 4"
                                                x-transition:enter="transition delay-100 duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                            <p class="truncate">{!! $konten->detail_section !!}</p>
                                            </p>
                                            <div class="flex">
                                                <a href="{{ route('section.edit', $konten->id) }}"
                                                    class="inline-flex items-center justify-center px-8 pt-3 pb-2 mt-4 text-lg text-center text-white no-underline bg-blue-500 border-blue-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:bg-blue-500 focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text-lg"
                                                    class="text-base" x-show="tab === 4"
                                                    x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                    x-transition:enter-start="opacity-0">
                                                    Edit
                                                </a>
                                                &nbsp;&nbsp;
                                                <form action="{{route("section.delete",$konten->id)}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button 
                                                        class="inline-flex items-center justify-between px-8 py-3 mt-4 text-lg text-white text-center no-underline bg-red-500 border-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text"
                                                        class="text-base" x-show="tab === 4"
                                                        x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                        x-transition:enter-start="opacity-0"
                                                        >
                                                        Hapus
                                                    </button>
                                                </form>
                                            </div>
                                            <hr>
                                        @endforeach
                                    </div>
                                    <div class="space-y-6" x-show="tab === 5">
                                        @foreach ($corporateSets as $konten)
                                            <h3 class="text-xl font-bold leading-tight" x-show="tab === 5"
                                                x-transition:enter="transition duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                                {{ $konten->title_section }}
                                            </h3>
                                            <p class="text-base text-gray-600" x-show="tab === 5"
                                                x-transition:enter="transition delay-100 duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                            <p class="truncate">{!! $konten->detail_section !!}</p>
                                            </p>
                                            <div class="flex">
                                                <a href="{{ route('section.edit', $konten->id) }}"
                                                    class="inline-flex items-center justify-center px-8 pt-3 pb-2 mt-4 text-lg text-center text-white no-underline bg-blue-500 border-blue-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:bg-blue-500 focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text-lg"
                                                    class="text-base" x-show="tab === 5"
                                                    x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                    x-transition:enter-start="opacity-0">
                                                    Edit
                                                </a>
                                                &nbsp;&nbsp;
                                                <form action="{{route("section.delete",$konten->id)}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button 
                                                        class="inline-flex items-center justify-between px-8 py-3 mt-4 text-lg text-white text-center no-underline bg-red-500 border-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text"
                                                        class="text-base" x-show="tab === 5"
                                                        x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                        x-transition:enter-start="opacity-0"
                                                        >
                                                        Hapus
                                                    </button>
                                                </form>
                                            </div>
                                            <hr>
                                        @endforeach
                                    </div>
                                    <div class="space-y-6" x-show="tab === 6">
                                        @foreach ($storeSets as $konten)
                                            <h3 class="text-xl font-bold leading-tight" x-show="tab === 6"
                                                x-transition:enter="transition duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                                {{ $konten->title_section }}
                                            </h3>
                                            <p class="text-base text-gray-600" x-show="tab === 6"
                                                x-transition:enter="transition delay-100 duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                            <p class="truncate">{!! $konten->detail_section !!}</p>
                                            </p>
                                            <div class="flex">
                                                <a href="{{ route('section.edit', $konten->id) }}"
                                                    class="inline-flex items-center justify-center px-8 pt-3 pb-2 mt-4 text-lg text-center text-white no-underline bg-blue-500 border-blue-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:bg-blue-500 focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text-lg"
                                                    class="text-base" x-show="tab === 6"
                                                    x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                    x-transition:enter-start="opacity-0">
                                                    Edit
                                                </a>
                                                &nbsp;&nbsp;
                                                <form action="{{route("section.delete",$konten->id)}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button 
                                                        class="inline-flex items-center justify-between px-8 py-3 mt-4 text-lg text-white text-center no-underline bg-red-500 border-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text"
                                                        class="text-base" x-show="tab === 6"
                                                        x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                        x-transition:enter-start="opacity-0"
                                                        >
                                                        Hapus
                                                    </button>
                                                </form>
                                            </div>
                                            <hr>
                                        @endforeach
                                    </div>
                                    <div class="space-y-6" x-show="tab === 7">
                                        @foreach ($kerjasamaSets as $konten)
                                            <h3 class="text-xl font-bold leading-tight" x-show="tab === 7"
                                                x-transition:enter="transition duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                                {{ $konten->title_section }}
                                            </h3>
                                            <p class="text-base text-gray-600" x-show="tab === 7"
                                                x-transition:enter="transition delay-100 duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                            <p class="truncate">{!! $konten->detail_section !!}</p>
                                            </p>
                                            <a href="{{ route('section.edit', $konten->id) }}"
                                                class="inline-flex items-center justify-center px-8 pt-3 pb-2 mt-4 text-lg text-center text-white no-underline bg-blue-500 border-blue-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:bg-blue-500 focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text-lg"
                                                class="text-base" x-show="tab === 7"
                                                x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                                Edit
                                            </a>
                                            <hr>
                                        @endforeach
                                    </div>
                                    <div class="space-y-6" x-show="tab === 8">
                                        @foreach ($layananSets as $konten)
                                            <h3 class="text-xl font-bold leading-tight" x-show="tab === 8"
                                                x-transition:enter="transition duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                                {{ $konten->title_section }}
                                            </h3>
                                            <p class="text-base text-gray-600" x-show="tab === 8"
                                                x-transition:enter="transition delay-100 duration-500 transform ease-in"
                                                x-transition:enter-start="opacity-0">
                                            <p class="truncate">{!! $konten->detail_section !!}</p>
                                            </p>
                                            <div class="flex">
                                                <a href="{{ route('section.edit', $konten->id) }}"
                                                    class="inline-flex items-center justify-center px-8 pt-3 pb-2 mt-4 text-lg text-center text-white no-underline bg-blue-500 border-blue-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:bg-blue-500 focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text-lg"
                                                    class="text-base" x-show="tab === 8"
                                                    x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                    x-transition:enter-start="opacity-0">
                                                    Edit
                                                </a>
                                                &nbsp;&nbsp;
                                                <form action="{{route("section.delete",$konten->id)}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button 
                                                        class="inline-flex items-center justify-between px-8 py-3 mt-4 text-lg text-white text-center no-underline bg-red-500 border-500 cursor-pointer hover:bg-gray-900 rounded-3xl hover:text-white focus-within:border-blue-500 focus-within:text-white sm:text-base lg:text"
                                                        class="text-base" x-show="tab === 8"
                                                        x-transition:enter="transition delay-500 duration-500 transform ease-in"
                                                        x-transition:enter-start="opacity-0"
                                                        >
                                                        Hapus
                                                    </button>
                                                </form>
                                            </div>
                                            <hr>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.getElementById("checkbox1").setAttribute('value', 'off');
        var value = "";
        //you can put the checkbox in a variable,
        //this way you do not need to do a javascript query every time you access the value of the checkbox
        var checkbox1 = document.getElementById("checkbox1")
        checkbox1.checked = value
        document.getElementById("checkbox1").addEventListener("change", function(element) {
            console.log(checkbox1.checked)
            if (checkbox1.checked) {
                console.log("on");
                $('#checkbox1').val("on");
            } else {
                console.log("off");
                $('#checkbox1').val("off");
            }
        });
    </script>
    <script>
        function previewImage() {
            return {
                imageUrl: "",

                fileChosen(event) {
                    this.fileToDataUrl(event, (src) => (this.imageUrl = src));
                },

                fileToDataUrl(event, callback) {
                    if (!event.target.files.length) return;

                    let file = event.target.files[0],
                        reader = new FileReader();

                    reader.readAsDataURL(file);
                    reader.onload = (e) => callback(e.target.result);
                },
            };
        }
    </script>
    <script>
        $('#summernoteSection').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                // ['insert', ['link', 'picture', 'video']],
                // ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteWalikota').summernote({
            placeholder: 'Kata Sambutan',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteProvinsi').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteGubernur').summernote({
            placeholder: 'Kata Sambutan',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteWisata').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteKab').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteMenteri').summernote({
            placeholder: 'Kata Sambutan',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
    </script>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                images: [],

                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event) {
                    if (!event.target.files.length) return

                    let file = event.target.files

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                        };
                    }
                },

                removeImage(index) {
                    this.images.splice(index, 1);
                }
            }
        }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#desti").on('change', function() {
                $(".destindonesia").hide();
                $("#" + $(this).val()).fadeIn(700);
            }).change();
        });
    </script>
    <script>
        function displayImage() {

            return {
                images: [],

                selectedFile(event) {
                    this.fileToUrl(event)
                    this.savedImage = []
                },

                fileToUrl(event) {
                    if (!event.target.files.length) return

                    let file = event.target.files
                    this.images = []
                    // this.data = []

                    console.log(typeof file)

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                        };

                        // this.data = [...this.data, file[i]]
                    }

                },

                removeImage(index) {
                    this.images.splice(index, 1);
                    // let file = $("#images")[0].files;
                    // this.data = Object.values($("#images")[0].files)
                    // console.log(this.data)
                    // console.log(file.filter( (i, item) => i !== index))
                },

                splitImage(image) {
                    return image.split(",")
                }
            }
        }
    </script>
</body>

</html>
