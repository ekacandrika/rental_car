<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- base | always include -->
    <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.1.1/dist/css/base/jquery.fonticonpicker.min.css">

    <!-- default grey-theme -->
    <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.0.0-alpha.0/dist/css/themes/grey-theme/jquery.fonticonpicker.grey.min.css">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript"
        src="https://unpkg.com/@fonticonpicker/fonticonpicker/dist/js/jquery.fonticonpicker.min.js"></script>

    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param || null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{-- Sidebar --}}
    <x-be.admin.sidebar-admin>
        
    </x-be.admin.sidebar-admin>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10">
            <span class="text-sm font-bold font-inter text-[#000000]">Tambah Listing</span>
            {{-- Breadcumbs --}}
            <nav class="flex" aria-label="Breadcrumb">
                <ol class="inline-flex items-center space-x-1 md:space-x-3">
                    <li class="inline-flex items-center">
                        <a href="informasi"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Isi
                            Informasi Dasar</a>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            <a href="harga"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Harga
                                & Ketersediaan</a>
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            <a href="bayar"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Batas
                                Pembayaran & Pembatalan</a>
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            <a href="maps"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Peta
                                & Foto</a>
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            <a href="ekstra"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1]">Ekstra</a>
                        </div>
                    </li>
                </ol>
            </nav>

            <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">

                <div>
                    <div class="grid grid-cols-2">
                        <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Master Route</div>
                    </div>
                    {{-- Master route --}}
                    <form action="{{ route('masterroute.store') }}" method="POST">
                        @csrf
                        <div class="px-3">
                            <button type="button"
                                class="px-3 py-1 my-3 text-sm text-white rounded-full btn btn-info bg-kamtuu-second"
                                id="dynamic-ar">Tambah +</button>
                        </div>
                        <div class="block space-x-5 mb-5">
                            <table id="dynamicAddRemove">
                                <tr>
                                    <td class="block px-5 mb-6">
                                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Judul Rute</label>
                                        <input type="text" name="title" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="Judul Rute">
                                        <div
                                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        {{-- View Result --}}
                        <div class="grid grid-cols-2">
                            <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Result Master Route</div>
                        </div>
                        <div class="block px-5 mb-6">
                            @foreach ($rute as $item)
                                @php
                                    $json = json_decode($item->rute, true);
                                @endphp
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333]">Rute : {{ $json['result']['title'] }}</label>
                                <div class="">
                                    <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333]">From</label>
                                    @foreach ($json['result']['from'] as $result)
                                        @php
                                            $from = App\Models\District::where('id', $result)->first();
                                        @endphp
                                        <p
                                            class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            {{ $from->name }}
                                        </p>
                                    @endforeach

                                    <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333]">To</label>
                                    @foreach ($json['result']['to'] as $value)
                                        @php
                                            $to = App\Models\District::where('id', $value)->first();
                                        @endphp
                                        <p
                                            class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                            {{ $to->name }}
                                        </p>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>

                        <div class="p-5">
                            <div class="grid grid-cols-6">
                                <div class="col-start-6 col-end-7">
                                    <button type="submit"
                                        class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <script>
        function fasilitas() {
            return {
                icons: '',
                description: '',
                fields: [],
                addNewField() {
                    this.fields.push({
                        icon: '',
                        description: this.description,
                    });
                    // console.log(this.fields)
                },
                removeField(index) {
                    // console.log(index)
                    this.fields.splice(index, 1);
                    console.log(this.fields)
                }
            }
        }
    </script>

    <script>
        function amenitas() {
            return {
                icons: '',
                title: '',
                description: '',
                fields: [],
                addNewField() {
                    this.fields.push({
                        icon: '',
                        title: this.title,
                        description: this.description,
                    });
                    // console.log(this.fields)
                },
                removeField(index) {
                    // console.log(index)
                    this.fields.splice(index, 1);
                    console.log(this.fields)
                }
            }
        }
    </script>

    <script type="text/javascript">
        $("#dynamic-ar").click(function() {
            $("#dynamicAddRemove").append(
                '<tr><td class="block px-5 mb-6"><label for="text" class="block mb-2 text-sm font-bold text-[#333333]">From</label><select name="district_id_from[]" class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1"><option>Select District</option>@foreach ($kecamatan as $item)<option value="{{ $item->id }}">{{ $item->name }}</option>@endforeach</select><div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900"></div></td><td class="block px-5 mb-6"><label for="text" class="block mb-2 text-sm font-bold text-[#333333]">To</label><select name="district_id_to[]" class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1"><option>Select District</option>@foreach ($kecamatan as $item)<option value="{{ $item->id }}">{{ $item->name }}</option>@endforeach</select><div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900"></div></td><td class="p-3"><button type="button" class="mx-2 remove-input-field bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]">Delete</button></td></tr>'
            );
        });
        $(document).on('click', '.remove-input-field', function() {
            $(this).parents('tr').remove();
        });
    </script>
</body>

</html>
