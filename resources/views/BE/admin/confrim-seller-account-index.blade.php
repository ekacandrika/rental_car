<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        /* CHECKBOX TOGGLE SWITCH */
        /* @apply rules for documentation, these do not work as inline style */
        .switch {
            position: relative;
            display: inline-block;
            width: 100px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #23AEC1;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #9E3D64;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #23AEC1;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(68px);
            -ms-transform: translateX(68px);
            transform: translateX(68px);
        }

        .on {
            display: none;
        }

        .on,
        .off {
            color: white;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            font-size: 10px;
            font-family: Verdana, sans-serif;
        }

        input:checked+.slider .on {
            display: block;
        }

        input:checked+.slider .off {
            display: none;
        }

        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }

        .destindonesia {
            display: none;
        }
        
    </style>

    <!-- Styles -->
    @livewireStyles
</head>
<body class="bg-[#F2F2F2] font-inter">
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>   
    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-2xl font-bold font-inter text-[#333333]">< List Seller Unconfirmed</span>
                <div class="flex gap-5 my-3">
                    <div class="bg-white rounded-lg p-3 w-full">
                        <span class="py-2 text-base font-bold font-inter text-[#333333]">List Seller</span>
                        <div class="py-3 mx-3">
                            <div class="bg-white">
                                <table class="w-full text-sm text-left text-[#333333] mt-5">
                                    <thead class="uppercase border border-dark text-gray-500 text-xs font-semibold bg-gray-200">
                                        <tr class="hidden md:table-row">
                                            <th class="px-6 py-3">No</th>
                                            <th class="px-6 py-3">Nama Seller</th>
                                            <th class="px-6 py-3">Keterangan</th>
                                            <th class="px-6 py-3">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody class="flex-1 text-gray-700 sm:flex-none">
                                        @foreach($sellers as $seller)
                                        <tr>
                                            <td class="px-6 py-3">
                                                <span class="text-center">
                                                    {{$loop->iteration}}
                                                </span>
                                            </td>
                                            <td class="px-6 py-3">
                                                <span class="text-center">
                                                    {{$seller->first_name}}
                                                </span>
                                            </td>
                                            <td class="px-6 py-3">
                                            @if($seller->active_status!=null)
                                                <span class="text-center">
                                                    {{$seller->active_status}}
                                                </span>
                                            @else
                                                <span class="text-center">
                                                    Unconfirmed
                                                </span>
                                            @endif
                                            </td>
                                            <td class="px-6 py-3">
                                                <a type="button" href="{{route('confrim.seller-account.detail',$seller->id)}}"
                                                    class="text-white bg-yellow-500 hover:bg-yellow-600 duration-200 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-yellow-500 dark:hover:bg-yellow-600 dark:focus:ring-blue-800 w-[37px]">
                                                    <img src="{{ asset('storage/icons/pencil-solid 1.svg') }}" alt="" width="16px">
                                                    <span class="sr-only">Icon description</span>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</body>