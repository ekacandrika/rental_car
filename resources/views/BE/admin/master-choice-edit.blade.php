<!DOCTYPE html>
<html lang="{{str_replace('_','-',app()->getLocale())}}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>{{config('app.name','laaravel')}}</title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 100px;
            height: 34px;
        }
        .switch input {
            display: none;
        }
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #23AEC1;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .slider.round {
            border-radius: 34px;
        }
        .slider.round:before {
            border-radius: 50%;
        }
        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        input:checked + .slider {
            background-color: #9E3D64;
        }
        input:focus + .slider {
            box-shadow: 0 0 1px #23AEC1;
        }
        input:checked + .slider:before {
            -webkit-transform: translateX(68px);
            -ms-transform: translateX(68px);
            transform: translateX(68px);
        }
        .on {
            display: none;
        }
        .on, .off {
            color: white;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            font-size: 10px;
            font-family: Verdana, sans-serif;
        }
        input:checked + .slider .on {
            display: block;
        }
        input:checked + .slider .off {
            display: none;
        }
        #idn:checked+#idn {
            display: block;
        }
        #sett:checked+#sett {
            display: block;
        }
        .destindonesia{
            display: none;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>
<body class="bg-[#F2F2F2] font-inter">
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>

    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin></x-be.admin.sidebar-admin>
        
        <div class="col-start-3 col-end-11 z-10">
            <div class="p-5 pl-10 pr-10">
                <span class="text-2xl font font-bold font-inter text-[#333333]">Create Master Pilihan</span>
                <div class="flex gap-5">
                   <div class="bg-white rounded-lg p-3 w-full">
                       <div>
                           <form action="{{route('choice.update',$choice->id)}}" method="post">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <p class="py-2 font-bold">Jenis Pilihan</p>
                                    {{-- <select class="select w-1/3 select-bordered my-2 form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-900 bg-white bg-clip-padding bg-no-repeat
                                    border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 foxus:outline-none" name="choice">
                                        <option selected disabled></option>
                                        <option value="hotel" {{$choice->name =='hotel' ? 'selected': ''}}>Hotel</option>
                                        <option value="asuransi" {{$choice->name =='asuransi' ? 'selected': ''}}>Asuransi</option>
                                    </select> --}}
                                    <input type="text" 
                                    class="border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 foxus:outline-none" 
                                    name="choice"
                                    id="" value="{{$choice->name}}">
                                </div>
                                @if ($errors->has('choice'))
                                    <span class="text-rose-900">{{$errors->first('choice')}}</span>
                                @endif
                                <div class="form-group">
                                    <p class="py-2 font-bold">Jenis Pilihan</p>
                                    <select class="select w-1/3 select-bordered my-2 form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-900 bg-white bg-clip-padding bg-no-repeat
                                    border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 foxus:outline-none" name="product_type">
                                        <option selected disabled>Pilih Jenis Produk</option>
                                        <option value="tour" {{$choice->product_type =='tour' ? 'selected': ''}}>Tur</option>
                                        <option value="hotel" {{$choice->product_type =='hotel' ? 'selected': ''}}>Hotel</option>
                                        <option value="transfer and rental" {{$choice->product_type =='transfer and rental' ? 'selected': ''}}>Transfer dan Rental</option>
                                        <option value="xstay" {{$choice->product_type =='xstay' ? 'selected': ''}}>Xstay</option>
                                        <option value="activity" {{$choice->product_type =='activity' ? 'selected': ''}}>Aktivitas</option>
                                    </select>
                                </div>
                                @if ($errors->has('product_type'))
                                    <span class="text-rose-900">{{$errors->first('product_type')}}</span>
                                @endif
                                <div class="form-group text-end">
                                    <button type="submit" class="bg-kamtuu-second p-2 rouded-lg text-white hover:bg-kamtuu-primary">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div> 
                </div>

                {{-- <div class="my-5">
                    <span class="text-2xl font font-bold font-inter text-[#333333]">List Pilihan</span>
                    <div class="bg-white">
                        <table class="table flex table-auto w-full leading-normal">
                            <thead class="uppercase text-gray-500 text-xs font-semibold bg-gray-200">
                                <tr class="hidden md:table-row">
                                    <th class="text-left p-3">Name</th>
                                    <th class="text-left p-3">Action</th>
                                </tr>
                            </thead>
                            <tbody class="flex-1 text-gray-700 sm:flex-none">
                                @foreach($choices as $choice)
                                <tr v-for="('name','index') in choice" :key="index">
                                    <td class="border-t first:border-t-0 flex p-1 md:p-3 hover:bg-gray-100 md:table-row flex-col w-full flex-wrap">
                                        <label for="" class="text-xs text-gray-400 uppaercase font-semibold md:hidden">Name</label>
                                        <p class="px-3 py-5">
                                            {{$choice->name}}
                                        </p>
                                    </td>
                                    <td>
                                        <a href="{{route('choice.edit',$choice->id)}}" class="btn btn-primary">Edit</a>
                                        <form action="{{route('choice.delete',$choice->id)}}" method="post">
                                            @method('delete')
                                            <button type="submit" class="btn btn-accent">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</body>
</html>