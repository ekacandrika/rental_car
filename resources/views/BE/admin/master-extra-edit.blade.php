<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    {{-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script> --}}

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js" defer></script>

    <style>
        /* CHECKBOX TOGGLE SWITCH */
        /* @apply rules for documentation, these do not work as inline style */
        .switch {
            position: relative;
            display: inline-block;
            width: 100px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #23AEC1;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #9E3D64;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #23AEC1;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(68px);
            -ms-transform: translateX(68px);
            transform: translateX(68px);
        }

        .on {
            display: none;
        }

        .on,
        .off {
            color: white;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            font-size: 10px;
            font-family: Verdana, sans-serif;
        }

        input:checked+.slider .on {
            display: block;
        }

        input:checked+.slider .off {
            display: none;
        }

        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }

        .destindonesia {
            display: none;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>

    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-2xl font-bold font-inter text-[#333333]">Edit Extra</span>
                <div class="flex gap-5 my-3">
                    <div class="bg-white rounded-lg p-3 w-full">
                        <div x-data="{ detail: '{!! $extra->detail_ekstra !!}', note: '{!! $extra->note_ekstra !!}' }">
                            <form method="POST" action="{{ route('extra.update', $extra->id) }}"
                                enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                                <div>
                                    <p class="font-bold">Tampilkan Extra</p>
                                </div>
                                @if (count($errors) > 0)
                                <div class="bg-red-300 rounded-md p-3 my-3">
                                    @foreach ($errors->getMessagebag()->toArray() as $key=>$value)
                                    @foreach ($value as $index=>$err)
                                    <p>- {{ $err}}</p>
                                    @endforeach
                                    @endforeach
                                </div>
                                @endif
                                <div class="flex justify-start">
                                    <label class="switch">
                                        <input type="checkbox" id="checkbox1" name="status_section"
                                            @if($extra->status_ekstra == "on") checked @endif>
                                        <div class="slider round">
                                            <span class="on">Show</span>
                                            <span class="off">Hide</span>
                                        </div>
                                    </label>
                                </div>
                                <div class="form-group py-5 px-3 grid gap-y-3">
                                    <label for="nama_ekstra">Nama Ekstra</label>
                                    <input type="text" name="nama_ekstra" class="form-control rounded-md w-1/3"
                                        value="{{ $extra->nama_ekstra }}" />
                                </div>
                                <div class="form-group py-5 px-3 grid gap-y-3">
                                    <label for="harga_ekstra">Harga Ekstra</label>
                                    <input type="text" id="harga_ekstra" name="harga_ekstra"
                                        class="form-control rounded-md w-1/3" value="{{ $extra->harga_ekstra }}" />
                                </div>
                                <div class="form-group py-5 px-3 grid gap-y-3">
                                    <label>Detail Ekstra</label>
                                    <textarea id="detail_ekstra" name="detail_ekstra" x-model="detail"
                                        x-init="$nextTick(() => { initDetailNote() })"></textarea>
                                </div>
                                <div class="form-group py-5 px-3 grid gap-y-3">
                                    <label>Note Ekstra</label>
                                    <textarea id="note_ekstra" name="note_ekstra" x-model="note"
                                        x-init="$nextTick(() => { initNoteNote() })"></textarea>
                                </div>

                                <div class="form-group flex gap-5 my-3">
                                    <div class="bg-white rounded-lg p-3 w-full">
                                        <div>
                                            <p>Foto Gallery Ekstra</p>
                                            <div class="py-2" x-data="displayImage()">
                                                <input class="py-2" type="file" accept="image/*" id="images_gallery"
                                                    name="images_gallery[]" @change="selectedFile" multiple>
                                                <template x-if="images.length < 1">
                                                    <div
                                                        class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                                        <img src="{{ asset('storage/icons/upload.svg') }}"
                                                            alt="upload-icons" width="20px" height="20px">
                                                    </div>
                                                </template>
                                                <template x-if="images.length >= 1">
                                                    <div class="flex">
                                                        <template x-for="(image, index) in images" :key="index">
                                                            <div class="flex justify-center items-center">
                                                                <img :src="image"
                                                                    class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                                    :alt="'upload'+index">
                                                            </div>
                                                        </template>
                                                    </div>
                                                </template>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- Foto Fitur --}}
                                <div class="py-2 px-3" x-data="featuredImage()">
                                    <p>Foto Fitur Extra</p>
                                    <input class="py-2" type="file" name="image_thumbnail" accept="image/*"
                                        @change="selectedFile">
                                    <template x-if="imageUrl">
                                        <div class="flex">
                                            <img :src="imageUrl"
                                                class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                alt="upload-featured-photo">
                                            {{-- <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                    width="25px" @click="removeImage()">
                                            </button> --}}
                                        </div>
                                    </template>
                                    <template x-if="!imageUrl">
                                        <div
                                            class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                            <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                width="20px" height="20px">
                                        </div>
                                    </template>
                                </div>


                                <div class="form-group text-end">
                                    <button type="submit"
                                        class="bg-kamtuu-second p-2 rounded-lg text-white hover:bg-kamtuu-primary">
                                        Simpan
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    {{-- <script>
        document.getElementById("checkbox1").setAttribute('value', '{{ $extra->status_ekstra }}');
    var value = '{{ $extra->status_ekstra }}';
    //you can put the checkbox in a variable,
    //this way you do not need to do a javascript query every time you access the value of the checkbox
    var checkbox1 = document.getElementById("checkbox1")
    checkbox1.checked = value
    document.getElementById("checkbox1").addEventListener("change", function (element) {
        console.log(checkbox1.checked)
        if (checkbox1.checked) {
            console.log("on");
            $('#checkbox1').val("on");
        } else {
            console.log("off");
            $('#checkbox1').val("off");
        }
    });
    </script> --}}
    <script>
        function featuredImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                    this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                    if (! event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },

                removeImage() {
                    this.imageUrl = '';
                }
            }
        }
    </script>
    <script>
        const initDetailNote = () => {
            $('#detail_ekstra').summernote({
                placeholder: 'Detail Ekstra',
                tabsize: 2,
                height: 120,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                ]
            });
        }

        const initNoteNote = () => {
            $('#note_ekstra').summernote({
                placeholder: 'Note Ekstra',
                tabsize: 2,
                height: 120,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                ]
            });
        }
    </script>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

        return {
            images: [],

            selectedFile(event) {
                this.fileToUrl(event)
            },

            fileToUrl(event) {
                if (!event.target.files.length) return

                let file = event.target.files

                for (let i = 0; i < file.length; i++) {
                    let reader = new FileReader();
                    let srcImg = ''

                    reader.readAsDataURL(file[i]);
                    reader.onload = e => {
                        srcImg = e.target.result
                        this.images = [...this.images, srcImg]
                    };
                }
            },

            removeImage(index) {
                this.images.splice(index, 1);
            }
        }
    }
    </script>
    <script>
        $(document).ready(function () {
        $("#desti").on('change', function () {
            $(".destindonesia").hide();
            $("#" + $(this).val()).fadeIn(700);
        }).change();
    });
    </script>
</body>

</html>