<!DOCTYPE html>
<html lang="{{str_replace('_','-',app()->getLocale())}}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">

        <title>{{config('app.name','Laravel')}}</title>

        {{-- Font --}}
        <link rel="stylesheet" href="https://fonts.bunny.net/css/base/jquery.fonticonpicker.min.css">

        {{-- base | always include--}}
            <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.1.1/dist/css/base/jquery.fonticonpicker.min.css">

        <!-- default grey-theme -->
        <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.0.0-alpha.0/dist/css/themes/grey-theme/jquery.fonticonpicker.grey.min.css">

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript"
        src="https://unpkg.com/@fonticonpicker/fonticonpicker/dist/js/jquery.fonticonpicker.min.js"></script>

        {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}
    
        <!-- Select 2 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>

        {{-- sweet alert --}}
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.all.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.min.css" rel="stylesheet">

        {{-- data tables --}}
        <style>
            @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

            /**
            * tailwind.config.js
            * module.exports = {
            *   variants: {
            *     extend: {
            *       backgroundColor: ['active'],
            *     }
            *   },
            * }
            */
            .active\:bg-gray-50:active {
                --tw-bg-opacity: 1;
                background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
            }

            #tur:checked+#tur {
                display: block;
            }

            #transfer:checked+#transfer {
                display: block;
            }

            #hotel:checked+#hotel {
                display: block;
            }

            #rental:checked+#rental {
                display: block;
            }

            #activity:checked+#activity {
                display: block;
            }

            #xstay:checked+#xstay {
                display: block;
            }

            #idn:checked+#idn {
                display: block;
            }

            #sett:checked+#sett {
                display: block;
            }

            .destindonesia {
                display: none;
            }
            .select2-container .select2-selection--single {
                box-sizing: border-box;
                cursor: pointer;
                display: block;
                height: 48px;
                user-select: none;
                -webkit-user-select: none;
            }
            .select2-container--default .select2-selection--single .select2-selection__rendered {
                color: #444;
                line-height: 46px;
            }
            .select2-container--default .select2-selection--single .select2-selection__arrow {
                height: 26px;
                position: absolute;
                top: 11px;
                right: 13px;
                width: 20px;
            }
        </style>
    </head>
    <body class="bg-[#F2F2F2] font-inter">
        {{-- Navbar --}}
        <x-be.admin.navbar-admin></x-be.admin.navbar-admin>

        {{-- Sidebar --}}
        <div class="grid grid-cols-10">
            <x-be.admin.sidebar-admin></x-be.admin.sidebar-admin>

            <div class="col-start-3 col-end-11 z-0">
                <div class="p-5 pl-10 pr-10">
                    <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                        <div class="grid grid-cols-2">
                            <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Master Titik</div>
                        </div>
                        <form action="#" id="form-master-point" method="post">
                            @csrf
                            <div class="p-6">
                                <label class="block mb-2 text-xl font-bold text-[#333333]">Provinsi</label>
                                <select class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" id="provinsi-id">
                                    <option>-Pilih Provinsi-</option>
                                    @foreach ($provinsi as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="p-6">
                                <label class="block mb-2 text-xl font-bold text-[#333333]">Kota/Kabupaten</label>
                                <select class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" id="kabupaten-id">
                                    <option>-Pilih Kota/Kabupaten-</option>
                                </select>
                            </div>
                            <div class="p-6">
                                <label class="block mb-2 text-xl font-bold text-[#333333]">Kecamatan</label>
                                <select name="district_id" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" id="kecamatan-id">
                                    <option value>-Pilih Kecamatan-</option>
                                </select>
                            </div>
                            <div class="p-6">
                                <label class="block mb-2 text-xl font-bold text-[#333333]">Nama Titik</label>
                                <input type="hidden" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" id="district-name"/>
                                <input class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" id="name-point-id" name="name_titik"/>
                            </div>
                            <div class="p-6">
                                <label class="block mb-2 text-xl font-bold text-[#333333]">Note:</label>
                                <p class="text-sm font-bold text-red-400">
                                    <span>Tekan Enter untuk menampilkan input di preview</span>
                                </p>
                                <p class="text-sm font-bold text-red-400">
                                    <span>Tekan Tombol Simpan untuk menyimpan data</span>
                                </p>
                            </div>
                            <div class="p-6" x-data="inputPreview()">
                                <label class="block mb-2 text-xl font-bold text-[#333333]">Preview Input</label>
                                <table class="w-2/3 text-center border border-separate table-auto border-slate-500">
                                    <thead>
                                        <tr>
                                            <th class="border border-slate-300">Kecamatan</th>
                                            <th class="border border-slate-300">No</th>
                                            <th class="border border-slate-300">Nama Titik</th>
                                            <th class="border border-slate-300">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <template x-if="points.length >= 1">
                                            <template x-for="(point, index) in points">                                           
                                                <tr>
                                                    <td class="border border-slate-600" x-text="point.nama_kecamatan"></td>
                                                    <td class="border border-slate-600" x-text="index+1"></td>
                                                    <td class="border border-slate-600">
                                                        <input type="hidden" x-model="point.kecamatan_id" name="kecamatan_id[]" id="kecamatan_id">
                                                        <input class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 text-center ml-12" name="name_titik[]" id="name_titik" x-model="point.nama_titik"/>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="mx-2 remove-input-field bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]" @click="remove(index)">Hapus</button>
                                                    </td>
                                                </tr>
                                            </template>
                                        </template>
                                    </tbody>
                                </table>
                                {{-- <input class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" id="point-id" name="name_titik"/> --}}
                            </div>
                            <div class="p-6">
                                <button type="button" class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2" id="btn-submit">Simpan</button>
                            </div>
                        </form>

                        <div x-data="rute()">
                            <div class="grid grid-cols-2">
                                <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Result Route Titik</div>
                            </div>
                            <div class="flex items-center col-start-4 col-span-1 px-5 mb-3">
                                <label for="simple-search" class="sr-only">Search</label>
                                <div class="relative w-1/4">
                                    <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                        <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400"
                                            fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                                                clip-rule="evenodd"></path>
                                        </svg>
                                    </div>
                                    <input type="text" id="simple-search"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  
                                    dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Search" @keyup="search()">
                                </div>
                            </div>
                            <div class="block px-5 mb-6">
                                <template x-if="deleted_arr.length >= 1">
                                    <button 
                                    x-transition:enter="transition ease-out duration-300"
                                    x-transition:enter-start="opacity-0 scale-90"
                                    x-transition:enter-end="opacity-100 scale-100"
                                    x-transition:leave="transition ease-in duration-300"
                                    x-transition:leave-start="opacity-100 scale-100"
                                    x-transition:leave-end="opacity-0 scale-90"
                                    type="button" class="float-left mx-2 my-3 bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]" style="background:#D50006" @click="multipleDelete()">Hapus</button>
                                </template>
                            </div>
                            <div class="block px-5 mb-6">
                                <table class="w-full text-sm text-[#333333]">
                                    <thead class="text-md text-white bg-[#D25889]">
                                        <tr>
                                            <th class="py-3 px-6">Kecamatan</th>
                                            <th class="py-3 px-6">Nama Titik</th>
                                            <th class="py-3 px-6">Action</th>
                                        </tr>
                                    </thead>
                                    <template x-if="datas.length >= 1">
                                        <template x-for="(data, index) in datas">
                                            <tbody>
                                                <tr>
                                                    <td class="px-6 py-2">
                                                        <div class="flex justify-start">
                                                            <p> 
                                                                <span>
                                                                    <form>
                                                                        <input type="checkbox" class="loat-right mx-2" name="checked-button[]" @change="multiple(data.id)" :value="data.id" :id="'select-'+data.id">
                                                                    </form>
                                                                </span>
                                                                <span x-text="data.name"></span>
                                                            </p>
                                                        </div>
                                                    </td>
                                                    <td class="px-6 py-2" x-text="data.name_titik"></td>
                                                    <td class="px-6 py-2">
                                                        <div class="flex justify-end">
                                                            <form x-bind:action="`master-titik/${data.id}/clone`" method="post">
                                                                @csrf
                                                                <button type="submit" class="float-right mx-2 bg-[#398ded] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#398ded]" style="background:#398ded">Salin</button>
                                                            </form>
                                                            <a x-bind:href="`master-titik/${data.id}/edit`" class="float-right mx-2 bg-[#e8bf3a] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#f6ba59]" style="background:#e8bf3a">Edit</a>
                                                            <form x-bind:action="`master-titik/${data.id}`" method="post">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button type="submit" class="float-right mx-2 bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]" style="background:#D50006">Hapus</button>
                                                            </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </template>
                                    </template>
                                    <template x-if="datas.length == 0">
                                        <tbody>
                                            <tr>
                                            <td class="py-3 px-6 text-center text-2xl font-bold" colspan="3">Titik tidak ditemukan atau data belum ada</td>
                                            </tr>
                                        </tbody>
                                    </template>
                                </table>
                                <nav aria-label="Page navigation" class="text-end mt-3">
                                    <template x-if="links.length > 1">
                                    <ul class="inline-flex -space-x-px text-sm">
                                        <template x-for="(link, index) in links" :key="index">
                                            <li>
                                                <button type="button" class="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white" x-text="link.label" :class="link.active" @click="getPage(link.url)">
                                                </button>
                                            </li>
                                        </template>
                                    </ul>
                                    </template>
                                </nav>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>

        const titik_array = [];

        $("#provinsi-id").select2();
        $("#provinsi-id").on("change",function(){

            let provinsi_id = $(this).val();

            $.ajax({
                type:'GET',
                url:"/admin/kamtuu/route/regencies/"+provinsi_id,
                cache:'false',
                success:function(resp){
                    console.log(resp)
                    $("#kabupaten-id").html(resp)
                },
                error:function(data){
                    console.log('error:',data);
                }
            })
        })

        $("#kabupaten-id").select2();
        $("#kabupaten-id").on("change",function(){
          
            let kabupaten_id = $(this).val();
            let url = "{{route('master.titik.district',':id')}}";
            url = url.replace(':id',kabupaten_id);

            $.ajax({
                type:'GET',
                url:`${url}`,
                cache:'false',
                success:function(resp){
                    console.log(resp)
                    $("#kecamatan-id").html(resp)
                },
                error:function(data){
                    console.log('error:',data);
                }
            })

        })

        $("#kecamatan-id").select2();
        $("#kecamatan-id").on("change",function(){
            var name = $(this).find('option:selected').attr("data-name");
            console.log(name);
            $("#district-name").val(name)
        });

        function masterpoint(){
            return{
                master_point:[],
            }
        }

        function inputPreview(){
            return{
                points:Alpine.store('master').points,
                remove(index){
                    this.points.splice(index,1)
                }
            }
        }

        document.addEventListener("alpine:init", () => {
            Alpine.store("master", {
                init(){
                  this.points
                },
                points:[],
                
            })
        })

        document.addEventListener("alpine:init",()=>{
            Alpine.data("rute",()=>({
                datas:[],
                links:[],
                url:"{{route('master.titik.datas')}}",
                total_page:'',
                active:'',
                current_page:null,
                cur_page:null,
                deleted_arr:[],
                async init(){
                    let loads = await fetch(`${this.url}`);
                    let objs  = await loads.json();
                    console.log(objs)
                    this.datas = objs.datas.data;
                    this.links = objs.datas.links;

                    this.total_page = objs.datas.total;
                    this.cur_page = objs.datas.current_page;
                },
                search(){
                    var cari   = $("#simple-search").val();                   
                    let http = "{{route('master.titik.cari')}}";
                    let page = '&page=1';

                    if(cari.length > 0){
                        page = '&page='+this.cur_page;
                    }
                    
                    console.log(page)

                    fetch(`${http}?search=${cari}`).
                    then(response =>response.json())
                    .then(data=>{
                        this.datas=data.datas.data
                        this.links=data.datas.links
                    })
                },
                opened(index){
                    var parent =".parent_"+index;
                    var name =".child-"+index;

                    $(name).toggle()
                },
                getPage(page){
                    let arr = [];

                    if(page){
                        this.current_page = page;
                    }else{
                        this.current_page = '/?page=1';
                    }

                    fetch(`${this.url}${this.current_page}`)
                    .then(resp=>resp.json())
                    .then(data=>{
                        let obj = data.datas.data
                        this.cur_page = data.datas.current_page

                        if(obj.length != undefined){
                            this.datas = obj
                        }else{
                            Object.keys(obj).forEach(key=>{
                                arr.push(obj[key])
                            })

                            this.datas = arr
                        }
                    })
                },
                multiple(id){
                    var element_id = document.getElementById('select-'+id);
                    var value = element_id.value;

                    if(element_id.checked){
                        this.deleted_arr.push({
                            id:element_id.value
                        })
                    }

                    if(!element_id.checked){
                        var deleted = this.deleted_arr.filter(el=> el.id != element_id.value);
                        this.deleted_arr = deleted;
                    }
                    
                },
                multipleDelete(){

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type:'DELETE',
                        url:"{{route('master.titik.multi-delete')}}",
                        cache:false,
                        dataType:'json',
                        data:{
                            deleted_id:this.deleted_arr
                        },
                        success:function(resp){
                            console.log(resp)
                            
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                didOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: resp.status,
                                title: resp.message
                            }).then(function(){
                                location.reload();
                            })


                        },
                        error:function(data){
                            console.log('error:',data);
                        }
                    })
                }
            }))
        })

        $(document).ready(function(){

            function alertValid(msg, e){
                console.log(msg)

                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })

                Toast.fire({
                    icon: 'error',
                    title: msg
                })
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            
            $("#form-master-point").on('submit',(e)=>{
                var key = e.keyCode;
                console.log(key);
                return false;
            })
            
            $("#form-master-point").keyup((e)=>{

                var key = e.keyCode;
                if(key===13){

                    console.log(
                        $("#name-point-id").val(),
                        $("#kecamatan-id").val(),
                        $("#district-name").val()
                    )

                    //return false;

                    if(!$("#district-name").val()){
                        //alert('test');
                        alertValid('Kecamatan harus pilih!', e);
                        return false;
                    }

                    if(!$("#name-point-id").val()){
                        alertValid('Nama titik tidak boleh kosong', e);
                        return false;
                    }
                    
                    if($("#kecamatan-id").val() && $("#name-point-id").val()){
                        Alpine.store("master").points.push({
                            'kecamatan_id':$("#kecamatan-id").val(),
                            'nama_kecamatan':$("#district-name").val(),
                            'nama_titik':$("#name-point-id").val()
                        })
                    }
                    
                    console.log(Alpine.store("master").points);
                }                   
            });
        })

        $("#btn-submit").on("click",function(e){
            e.preventDefault();
            
            var kecamatan_id= $('input[name="kecamatan_id[]"]').serializeArray();
            var nama_titik  = $('input[name="name_titik[]"]').serializeArray();

            console.log(kecamatan_id,nama_titik)
            // return false;
            $.ajax({
                type:'POST',
                url:"{{route('master-titik.store')}}",
                cache:false,
                dataType:'json',
                data:{
                    district_id:kecamatan_id,
                    point_name:nama_titik
                },
                success:function(resp){
                    console.log(resp)
                    
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'success',
                        title: resp.message
                    }).then(function(){
                        location.reload();
                    })


                },
                error:function(data){
                    console.log('error:',data);
                }
            })
        })

    </script>
    <script>

    </script>
</html>