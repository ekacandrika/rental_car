<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #idn:checked+#idn {
            display: block;
        }
        #sett:checked+#sett {
            display: block;
        }
        .destindonesia{
            display: none;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    <div class="grid grid-cols-10 sticky z-0">
        {{-- Sidebar --}}
        <div class="col-start-1 col-end-3 bg-[#9E3D64] border-b-2 border-[#FFFFFF]">
            <div class="bg-[#9E3D64] ">
                <a href="#" class="p-2 pt-3 flex flex-col items-center justify-center">
                    <img src="{{ asset('storage/logos/kamtuu-white.png') }}" class="h-8 sm:h-8 px-[10px]" alt="Kamtuu Logo">
                </a>
            </div>
        </div>

        {{-- Navbar --}}
        <div class="col-start-3 col-end-11">
            <nav class="bg-white border-gray-200 px-2 sm:px-4 dark:bg-gray-900 right-0 sticky drop-shadow-xl">
                <div class="flex flex-row items-center justify-between gap-6 p-2">
                    <span class="text-xl font-semibold font-inter"> </span>
                    <div class="flex flex-row items-center gap-4 pt-3">
                        <img src="{{ asset('storage/img/logo-traveller.png') }}" alt="user-profile" class="object-cover object-center w-8 h-8 overflow-hidden rounded-full border-indigo-600 ">
                    </div>
                </div>
            </nav>
        </div>
    </div>

    <div class="grid grid-cols-10">
        <div class="col-start-1 col-end-3">
            <div class="bg-[#9E3D64] hidden lg:block fixed z-20 inset-0 top-[3.8125rem] right-auto overflow-y-auto w-[270px]">
                <ul>
                    <li class="items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]">
                        <a class="flex" href="/dashboard/admin/kamtuu">
                            <img src="{{ asset('storage/icons/dasbor-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                            <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Dashboard</p>
                        </a>
                    </li>

                    <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                        <a href="/dashboard/admin/kamtuu/master-user" class="flex items-center p-2 font-bold text-white">
                            <img src="{{ asset('storage/icons/user-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                                alt="user" title="user">
                            <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Master User</p>
                        </a>
                    </li>

                    <li>
                        <label class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]" for="idn">
                            <img src="{{ asset('storage/icons/pen-square-white.svg') }}"
                                class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                            <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Destindonesia</p>
                        </label>

                        <input class="hidden" type="checkbox" id="idn" />

                        <ul id="idn" class="hidden">
                            <li>
                                <a href="/dashboard/admin/kamtuu/master-tag" class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master Tag</a>
                            </li>
                            <li>
                                <a href="/dashboard/admin/kamtuu/master-place" class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master Place</a>
                            </li>
                        </ul>
                    </li>

                    <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="flex items-center p-2 font-bold text-white">
                            <img src="{{ asset('storage/icons/logout-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                                alt="user" title="user">
                            <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Keluar</p>
                            <form id="logout-form" method="POST" action="{{ route('logout') }}">
                                @csrf
                            </form>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <div class="flex gap-2">
                    <button type="button" class="text-[#333333] font-medium rounded-lg text-sm py-2 text-center inline-flex items-center mr-2 hover:text-[#23AEC1]">
                        <img src="{{ asset('storage/icons/chevron.svg') }}" alt="user-profile" class="w-5 h-5 mr-1">
                        <span class="text-lg font-bold font-inter text-[#333333]">User</span>
                    </button>
                </div>

                <div class="overflow-x-auto relative">
                    <div class="pb-4 dark:bg-gray-900">
                        <div class="grid grid-cols-6">
                            <label for="table-search" class="sr-only">Search</label>
                            <div class="relative mt-1">
                                <div class="flex absolute inset-y-0 left-0 items-center h-10 pl-3 pointer-events-none">
                                    <svg class="w-5 h-5 text-gray-500 dark:text-gray-400" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
                                </div>
                                <input type="text" id="table-search" class="block p-2 pl-10 w-80 text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search for items">
                            </div>
                            <div class="col-end-7 col-span-1 mt-1">
                                @livewire('admin.modal-add-user')
                            </div>
                        </div>
                    </div>
                    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                        <thead class="text-xs text-white uppercase bg-[#9E3D64] dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="py-3 px-6">
                                    Nama Lengkap
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Email
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Tempat/Tanggal Lahir
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Jenis Kelamin
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Alamat
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Jenis User
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <th scope="row" class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                    Jhonny Deepy
                                </th>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    jhondeep@gmail.com
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    DC, 01 Januari 1990
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    Laki-Laki
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    Alaska
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#51B449]">
                                    Traveller
                                </td>
                                <td class="py-4 px-6">
                                    @livewire('admin.modal-edit-user')
                                    <button class="mx-1">
                                        <img src="{{ asset('storage/icons/trash-red.svg') }}" alt="" width="16px">
                                    </button>
                                </td>
                            </tr>
                            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <th scope="row" class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                    Jhonny Deepy
                                </th>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    jhondeep@gmail.com
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    DC, 01 Januari 1990
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    Laki-Laki
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    Alaska
                                </td>
                                <td class="py-4 px-6 font-semibold text-red-500">
                                    Agent
                                </td>
                                <td class="py-4 px-6">
                                    @livewire('admin.modal-edit-user')
                                    <button class="mx-1">
                                        <img src="{{ asset('storage/icons/trash-red.svg') }}" alt="" width="16px">
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                if (! event.target.files.length) return

                let file = event.target.files[0],
                    reader = new FileReader()

                reader.readAsDataURL(file)
                reader.onload = e => callback(e.target.result)
                },
            }
        }
    </script>
</body>
</html>
