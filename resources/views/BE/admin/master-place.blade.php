<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }

        .destindonesia {
            display: none;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>

    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10 h-full">
                <span class="text-2xl font-bold font-inter text-[#333333]">Master Place</span>

                <div class="mt-4">
                    <form method="POST" action="" enctype="multipart/form-data">
                        <select
                            class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1"
                            id="desti">
                            <option value="pulau">Pulau</option>
                            <option value="provinsi">Provinsi</option>
                            <option value="kota">Kabupaten/Kota</option>
                            <option value="wisata">Wisata</option>
                        </select>
                </div>


                {{-- Provinsi --}}
                <div id="provinsi" class="destindonesia my-4 border rounded-md bg-white">
                    <div class="p-6">
                        <label for="text"
                            class="block mb-2 text-xl font-bold text-[#333333] dark:text-gray-300">Nama Provinsi</label>
                        <input type="text" id="text"
                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Nama Provinsi">
                    </div>

                    {{-- Logo Provinsi --}}
                    <div class="p-6" x-data="displayImage()">
                        <p class="font-bold text-xl">Logo Provinsi</p>
                        <input class="mt-4" type="file" accept="image/*" @change="selectedFile">
                        <template x-if="images.length < 1">
                            <div
                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                    height="20px">
                            </div>
                        </template>
                        <template x-if="images.length >= 1">
                            <div class="flex">
                                <template x-for="(image, index) in images" :key="index">
                                    <div class="flex justify-center items-center">
                                        <img :src="image"
                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                            :alt="'upload' + index">
                                        <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                alt="" width="25px" @click="removeImage(index)">
                                        </button>
                                    </div>
                                </template>
                            </div>
                        </template>
                    </div>

                    {{-- Header --}}
                    <div class="p-6" x-data="displayImage()">
                        <p class="font-bold text-xl">Header</p>
                        <input class="mt-4" type="file" accept="image/*" @change="selectedFile">
                        <template x-if="images.length < 1">
                            <div
                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                    height="20px">
                            </div>
                        </template>
                        <template x-if="images.length >= 1">
                            <div class="flex">
                                <template x-for="(image, index) in images" :key="index">
                                    <div class="flex justify-center items-center">
                                        <img :src="image"
                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                            :alt="'upload' + index">
                                        <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                alt="" width="25px" @click="removeImage(index)">
                                        </button>
                                    </div>
                                </template>
                            </div>
                        </template>
                    </div>

                    {{-- Sambutan --}}
                    <div class="p-6">
                        <p class="font-bold text-xl">Sambutan</p>
                        <div class="px-4 pt-4" x-data="displayImage()">
                            <p class="font-semibold text-sm">Foto Gubernur</p>
                            <div class="flex justify-items-center">
                                <template x-if="images.length < 1">
                                    <div
                                        class="border flex justify-center items-center rounded-full border-gray-300 bg-gray-200 w-[100px] h-[100px]">
                                        <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                            width="20px" height="20px">
                                    </div>
                                </template>
                                <input class="p-4" type="file" accept="image/*" @change="selectedFile">
                                <template x-if="images.length >= 1">
                                    <div class="flex">
                                        <template x-for="(image, index) in images" :key="index">
                                            <div class="flex justify-center items-center">
                                                <img :src="image"
                                                    class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                    :alt="'upload' + index">
                                                <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                                    <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                        alt="" width="25px" @click="removeImage(index)">
                                                </button>
                                            </div>
                                        </template>
                                    </div>
                                </template>
                            </div>
                        </div>

                        <div class="px-4 pt-4">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nama
                                Gubernur</label>
                            <input type="text" id="text"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Nama Gubernur">
                        </div>
                        <div class="max-w-6xl px-4 pt-4">
                            <p class="font-bold text-sm">Kata Sambutan</p>
                            <div class="bg-white">
                                <div id="summernoteGubernur"></div>
                            </div>
                        </div>
                    </div>

                    {{-- Deskripsi --}}
                    <div class="max-w-6xl p-6">
                        <p class="font-bold text-xl">Deskripsi</p>
                        <div class="bg-white">
                            <div id="summernoteProvinsi"></div>
                        </div>
                    </div>

                    {{-- Informasi --}}
                    <div class="p-6">
                        <p class="font-bold text-xl">Informasi</p>
                        <div class="px-4 grid grid-cols-2">

                            <div class="mt-4">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333]">Ibukota</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Nama Ibukota">
                            </div>

                            <div class="mt-4">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333]">Bandara</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Nama Bandara">
                            </div>

                            <div class="mt-4">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Pelabuhan
                                    Laut</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Nama Pelabuhan">
                            </div>

                            <div class="mt-4">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Terminal
                                    Darat</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Nama Terminal Darat">
                            </div>

                            <div class="mt-4">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Akses
                                    Transportasi</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Sebutkan Akses Transportasi">
                            </div>
                        </div>
                    </div>

                    {{-- Gallery --}}
                    <div class="p-6" x-data="displayImage()">
                        <p class="font-bold text-xl">Gallery Foto Pulau</p>
                        <input class="mt-4" type="file" accept="image/*" @change="selectedFile" multiple>
                        <template x-if="images.length < 1">
                            <div
                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                    height="20px">
                            </div>
                        </template>
                        <template x-if="images.length >= 1">
                            <div class="flex">
                                <template x-for="(image, index) in images" :key="index">
                                    <div class="flex justify-center items-center">
                                        <img :src="image"
                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                            :alt="'upload' + index">
                                        <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                alt="" width="25px" @click="removeImage(index)">
                                        </button>
                                    </div>
                                </template>
                            </div>
                        </template>
                    </div>

                    {{-- Maps --}}
                    <div class="p-6">
                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Link Peta Google
                            Maps</label>
                        <input type="text" id="text"
                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="<iframe src=”https://www.map">
                    </div>

                    <div class="p-6">
                        <button class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2">Simpan</button>
                    </div>
                </div>

                {{-- Kabupaten/Kota --}}
                <div id="kota" class="destindonesia my-4 border rounded-md bg-white">
                    <div class="p-6">
                        <label for="text"
                            class="block mb-2 text-xl font-bold text-[#333333] dark:text-gray-300">Nama
                            Kabupaten/Kota</label>
                        <input type="text" id="text"
                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Nama Kabupaten/Kota">
                    </div>

                    {{-- Logo --}}
                    <div class="p-6" x-data="displayImage()">
                        <p class="font-bold text-xl">Logo Kabupaten/Kota</p>
                        <input class="mt-4" type="file" accept="image/*" @change="selectedFile">
                        <template x-if="images.length < 1">
                            <div
                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                    height="20px">
                            </div>
                        </template>
                        <template x-if="images.length >= 1">
                            <div class="flex">
                                <template x-for="(image, index) in images" :key="index">
                                    <div class="flex justify-center items-center">
                                        <img :src="image"
                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                            :alt="'upload' + index">
                                        <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                alt="" width="25px" @click="removeImage(index)">
                                        </button>
                                    </div>
                                </template>
                            </div>
                        </template>
                    </div>

                    {{-- Header --}}
                    <div class="p-6" x-data="displayImage()">
                        <p class="font-bold text-xl">Header</p>
                        <input class="mt-4" type="file" accept="image/*" @change="selectedFile">
                        <template x-if="images.length < 1">
                            <div
                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                    height="20px">
                            </div>
                        </template>
                        <template x-if="images.length >= 1">
                            <div class="flex">
                                <template x-for="(image, index) in images" :key="index">
                                    <div class="flex justify-center items-center">
                                        <img :src="image"
                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                            :alt="'upload' + index">
                                        <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                alt="" width="25px" @click="removeImage(index)">
                                        </button>
                                    </div>
                                </template>
                            </div>
                        </template>
                    </div>

                    {{-- Sambutan --}}
                    <div class="p-6">
                        <p class="font-bold text-xl">Sambutan</p>
                        <div class="px-4 pt-4" x-data="displayImage()">
                            <p class="font-semibold text-sm">Foto Walikota</p>
                            <div class="flex justify-items-center">
                                <template x-if="images.length < 1">
                                    <div
                                        class="border flex justify-center items-center rounded-full border-gray-300 bg-gray-200 w-[100px] h-[100px]">
                                        <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                            width="20px" height="20px">
                                    </div>
                                </template>
                                <input class="p-4" type="file" accept="image/*" @change="selectedFile">
                                <template x-if="images.length >= 1">
                                    <div class="flex">
                                        <template x-for="(image, index) in images" :key="index">
                                            <div class="flex justify-center items-center">
                                                <img :src="image"
                                                    class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                    :alt="'upload' + index">
                                                <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                                    <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                        alt="" width="25px" @click="removeImage(index)">
                                                </button>
                                            </div>
                                        </template>
                                    </div>
                                </template>
                            </div>
                        </div>

                        <div class="px-4 pt-4">
                            <label for="text"
                                class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Nama
                                Walikota</label>
                            <input type="text" id="text"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Nama Walikota">
                        </div>
                        <div class="max-w-6xl px-4 pt-4">
                            <p class="font-bold text-sm">Kata Sambutan</p>
                            <div class="bg-white">
                                <div id="summernoteWalikota"></div>
                            </div>
                        </div>
                    </div>

                    {{-- Deskripsi --}}
                    <div class="max-w-6xl p-6">
                        <p class="font-bold text-xl">Deskripsi</p>
                        <div class="bg-white">
                            <div id="summernoteKab"></div>
                        </div>
                    </div>

                    {{-- Gallery --}}
                    <div class="p-6" x-data="displayImage()">
                        <p class="font-bold text-xl">Gallery Foto Kab/Kota</p>
                        <input class="mt-4" type="file" accept="image/*" @change="selectedFile" multiple>
                        <template x-if="images.length < 1">
                            <div
                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                    height="20px">
                            </div>
                        </template>
                        <template x-if="images.length >= 1">
                            <div class="flex">
                                <template x-for="(image, index) in images" :key="index">
                                    <div class="flex justify-center items-center">
                                        <img :src="image"
                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                            :alt="'upload' + index">
                                        <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                alt="" width="25px" @click="removeImage(index)">
                                        </button>
                                    </div>
                                </template>
                            </div>
                        </template>
                    </div>

                    {{-- Maps --}}
                    <div class="p-6">
                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Link Peta Google
                            Maps</label>
                        <input type="text" id="text"
                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="<iframe src=”https://www.map">
                    </div>

                    <div class="p-6">
                        <button class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2">Simpan</button>
                    </div>
                </div>

                {{-- Wisata --}}
                <div id="wisata" class="destindonesia my-4 border rounded-md bg-white">
                    <div class="p-6">
                        <label for="text"
                            class="block mb-2 text-xl font-bold text-[#333333] dark:text-gray-300">Nama Wisata</label>
                        <input type="text" id="text"
                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Nama Wisata">
                    </div>

                    {{-- Header --}}
                    <div class="p-6" x-data="displayImage()">
                        <p class="font-bold text-xl">Header</p>
                        <input class="mt-4" type="file" accept="image/*" @change="selectedFile">
                        <template x-if="images.length < 1">
                            <div
                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                    height="20px">
                            </div>
                        </template>
                        <template x-if="images.length >= 1">
                            <div class="flex">
                                <template x-for="(image, index) in images" :key="index">
                                    <div class="flex justify-center items-center">
                                        <img :src="image"
                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                            :alt="'upload' + index">
                                        <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                alt="" width="25px" @click="removeImage(index)">
                                        </button>
                                    </div>
                                </template>
                            </div>
                        </template>
                    </div>

                    {{-- Deskripsi --}}
                    <div class="max-w-6xl p-6">
                        <p class="font-bold text-xl">Deskripsi</p>
                        <div class="bg-white">
                            <div id="summernoteWisata"></div>
                        </div>
                    </div>

                    {{-- Informasi --}}
                    <div class="p-6">
                        <p class="font-bold text-xl">Informasi</p>
                        <div class="px-4 grid grid-cols-2">

                            <div class="mt-4">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333]">Kategori</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Kategori">
                            </div>

                            <div class="mt-4">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Hari
                                </label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Hari">
                            </div>

                            <div class="mt-4">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Waktu
                                    Operasional</label>
                                <input type="time" id="time"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Waktu Operasional">
                            </div>

                            <div class="mt-4">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Harga Tiket
                                    Masuk</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Harga Tiket">
                            </div>

                            <div class="mt-4">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Akses
                                    Transportasi</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Sebutkan Akses Transportasi">
                            </div>
                        </div>
                    </div>

                    {{-- Gallery --}}
                    <div class="p-6" x-data="displayImage()">
                        <p class="font-bold text-xl">Gallery Foto Wisata</p>
                        <input class="mt-4" type="file" accept="image/*" @change="selectedFile" multiple>
                        <template x-if="images.length < 1">
                            <div
                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                    height="20px">
                            </div>
                        </template>
                        <template x-if="images.length >= 1">
                            <div class="flex">
                                <template x-for="(image, index) in images" :key="index">
                                    <div class="flex justify-center items-center">
                                        <img :src="image"
                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                            :alt="'upload' + index">
                                        <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                alt="" width="25px" @click="removeImage(index)">
                                        </button>
                                    </div>
                                </template>
                            </div>
                        </template>
                    </div>

                    {{-- Maps --}}
                    <div class="p-6">
                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Link Peta Google
                            Maps</label>
                        <input type="text" id="text"
                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="<iframe src=”https://www.map">
                    </div>

                    <div class="p-6">
                        <button class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2">Simpan</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $('#summernotePulau').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteWalikota').summernote({
            placeholder: 'Kata Sambutan',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteProvinsi').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteGubernur').summernote({
            placeholder: 'Kata Sambutan',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteWisata').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteKab').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteMenteri').summernote({
            placeholder: 'Kata Sambutan',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
    </script>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                images: [],

                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event) {
                    if (!event.target.files.length) return

                    let file = event.target.files

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                        };
                    }
                },

                removeImage(index) {
                    this.images.splice(index, 1);
                }
            }
        }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#desti").on('change', function() {
                $(".destindonesia").hide();
                $("#" + $(this).val()).fadeIn(700);
            }).change();
        });
    </script>
</body>

</html>
