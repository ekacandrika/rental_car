<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        /* CHECKBOX TOGGLE SWITCH */
        /* @apply rules for documentation, these do not work as inline style */
        .switch {
            position: relative;
            display: inline-block;
            width: 100px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #23AEC1;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #9E3D64;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #23AEC1;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(68px);
            -ms-transform: translateX(68px);
            transform: translateX(68px);
        }

        .on {
            display: none;
        }

        .on,
        .off {
            color: white;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            font-size: 10px;
            font-family: Verdana, sans-serif;
        }

        input:checked+.slider .on {
            display: block;
        }

        input:checked+.slider .off {
            display: none;
        }

        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }

        .destindonesia {
            display: none;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>

    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-2xl font-bold font-inter text-[#333333]">Create FAQs</span>
                <div class="flex gap-5">
                    <div class="bg-white rounded-lg p-3 w-full">
                        <div>
                            <form method="POST" action="{{ route('faqs.store') }}" enctype="multipart/form-data">
                                @csrf

                                <div>
                                    <p class="font-bold">Tampilkan Section</p>
                                </div>

                                <div class="container">
                                    @if ($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li class="rounded-md px-3 py5 bg-red-300 text-red-500 mb-3 w-[231px]">{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    @if (Session::has('success'))
                                    <div class="alert alert-success text-center">
                                        <p>{{ Session::get('success') }}</p>
                                    </div>
                                    @endif
                                    <table class="table table-bordered" id="dynamicAddRemove">
                                        <tr>
                                            <th>FAQs :</th>
                                            <th>Judul :</th>
                                            <th>Isi :</th>
                                        </tr>
                                        <tr>
                                            <td><input class="rounded-full my-2 mx-2" type="text" name="section"
                                                    placeholder="Enter Section" class="form-control" />
                                            </td>
                                            <td><input class="rounded-full my-2 mx-2" type="text"
                                                    name="addMoreInputFields1[0][title_section]"
                                                    placeholder="Enter Title" class="form-control" />
                                            </td>
                                            <td><input class="rounded-full my-2" type="text"
                                                    name="addMoreInputFields2[0][isi_section]" placeholder="Enter Isi"
                                                    class="form-control" />
                                            </td>
                                            <td>
                                                <button type="button" name="add" id="dynamic-ar"
                                                    class="btn btn-outline-primary">Add Subject
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="form-group text-end">
                                    <button type="submit"
                                        class="bg-kamtuu-second p-2 rounded-lg text-white hover:bg-kamtuu-primary">
                                        Simpan
                                    </button>
                                </div>
                            </form>
                        </div>

                        <div class="">
                            @if (isset($titles) && isset($isi))
                            @foreach($titles as $test)
                            {{$test->title_section}}
                            @endforeach
                            @foreach($isi as $faq)
                            {{$faq->isi_section}}
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var i = 0;
    $("#dynamic-ar").click(function () {
        ++i;
        $("#dynamicAddRemove").append('<tr>' + '<td></td>' +
            '<td><input class="rounded-full my-2" type="text" name="addMoreInputFields1[' + i +
            '][title_section]" placeholder="Enter Title" class="form-control" /></td>' +
            '<td><input class="rounded-full my-2 mx-2" type="text" name="addMoreInputFields2[' + i +
            '][isi_section]" placeholder="Enter Section" class="form-control" /></td>' +
            '<td><button type="button" class="btn btn-outline-danger remove-input-field">Delete</button></td>' +
            '</tr>'
        );
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });
    </script>
    <script>
        $('#summernoteSection').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
// ['insert', ['link', 'picture', 'video']],
// ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteWalikota').summernote({
        placeholder: 'Kata Sambutan',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteProvinsi').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteGubernur').summernote({
        placeholder: 'Kata Sambutan',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteWisata').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteKab').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteMenteri').summernote({
        placeholder: 'Kata Sambutan',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    </script>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
</body>

</html>