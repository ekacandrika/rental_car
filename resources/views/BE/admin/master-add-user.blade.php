<!DOCTYPE html>
<html lang="{{str_replace('_','-',app()->getLocale())}}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">



    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <style>
        /* CHECKBOX TOGGLE SWITCH */
        /* @apply rules for documentation, these do not work as inline style */
        .switch {
            position: relative;
            display: inline-block;
            width: 100px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #23AEC1;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #9E3D64;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #23AEC1;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(68px);
            -ms-transform: translateX(68px);
            transform: translateX(68px);
        }

        .on {
            display: none;
        }

        .on,
        .off {
            color: white;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            font-size: 10px;
            font-family: Verdana, sans-serif;
        }

        input:checked+.slider .on {
            display: block;
        }

        input:checked+.slider .off {
            display: none;
        }

        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }

        .destindonesia {
            display: none;
        }
    </style>

     <!-- Styles -->
     @livewireStyles
</head>
<body class="bg-[#F2F2F2]">
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>
    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin></x-be.admin.sidebar-admin>
        <div class="col-start-3 col-end-11 z-10">
            <div class="p-5 pl-10 pr-10">
                <span class="text-2xl font-bold font-inter text-black">Create User</span>
                <div class="flex-gap-5 my-3">
                    <div class="bg-white rounded-lg p-3 w-full">
                        <div>
                            <form action="{{route('user.admin-create')}}" method="post">
                                @csrf
                                <div class="grid grid-cols-2 mt-5 gap-5">
                                    <div>
                                        <label for="first_name" class="block mb-2 text-sm font-medium text-gray-700 dark:text-white">Nama Lengkap</label>
                                        <input type="text" name="first_name" value{{@old('first_name')}} id="first_name" class="bg-gray-100 border border-gray-200 text-gray-700 text-sm rounded-lg focus:ring-red-500 focus:border-red-500 block @error('first_name') border border-red-500 focus:ring-red-500 focus:border-red-500 @enderror w-full p-2.5 dark:border-gray-500 dark:placeholder-gray-200 dark:text-white" placeholder="Nama Lengkap">
                                        @if($errors->has('first_name'))
                                            <span class="block text-[#f47070] mt-2">{{$errors->first('first_name')}}</span>
                                        @endif()
                                    </div>
                                    <div>
                                        <label for="no_tlp" class="block mb-2 text-sm font-medium text-gray-700 dark:text-white">No Telepon/Hp</label>
                                        <input type="text" name="no_tlp" value{{@old('no_tlp')}} id="no_tlp" class="bg-gray-100 border border-gray-200 text-gray-700 text-sm rounded-lg focus:ring-red-500 focus:border-red-500 block w-full p-2.5 dark:border-gray-500 dark:placeholder-gray-200 dark:text-white @error('no_tlp') border border-red-500 focus:ring-red-500 focus:border-red-500 @enderror" placeholder="No Telepon/Hp">
                                        @if($errors->has('no_tlp'))
                                            <span class="block text-[#f47070] mt-2">{{$errors->first('no_tlp')}}</span>
                                        @endif()
                                    </div>
                                    <div>
                                        <label for="email" class="block mb-2 text-sm font-medium text-gray-700 dark:text-white">Email</label>
                                        <input type="email" name="email" value{{@old('email')}} id="email" class="bg-gray-100 border border-gray-200 text-gray-700 text-sm rounded-lg focus:ring-red-500 focus:border-red-500 block w-full p-2.5 dark:border-gray-500 dark:placeholder-gray-200 dark:text-white @error('email') border border-red-500 focus:ring-red-500 focus:border-red-500 @enderror" placeholder="yourmail@mail.com">
                                        @if($errors->has('email'))
                                            <span class="block text-[#f47070] mt-2">{{$errors->first('email')}}</span>
                                        @endif()
                                    </div>
                                    <div>
                                        <label for="password" class="block mb-2 text-sm font-medium text-gray-700 dark:text-white">Password</label>
                                        <input type="password" name="password" id="password" class="bg-gray-100 border border-gray-200 text-gray-700 text-sm rounded-lg focus:ring-red-500 focus:border-red-500 block w-full p-2.5 dark:border-gray-500 dark:placeholder-gray-200 dark:text-white @error('password') border border-red-500 focus:ring-red-500 focus:border-red-500 @enderror" placeholder="..........">
                                        @if($errors->has('password'))
                                            <span class="block text-[#f47070] mt-2">{{$errors->first('password')}}</span>
                                        @endif()
                                    </div>
                                    <div>
                                        <label for="jk" class="block mb-2 text-sm font-medium text-gray-700 dark:text-white">Jenis Kelamin</label>
                                        <select name="jk" id="jk" class="bg-gray-100 border border-gray-200 text-gray-700 text-sm rounded-lg focus:ring-red-500 focus:border-red-500 block w-full p-2 dark:border-gray-500 dark-placehorder-gray-200 dark:text-white">
                                            <option value="laki-laki" selected>Laki-Laki</option>
                                            <option value="perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                    <div>
                                        <label for="role" class="block mb-2 text-sm font-medium text-gray-700 dark:text-white">Tipe User</label>
                                        <select name="role" id="role" class="bg-gray-100 border border-gray-200 text-gray-700 text-sm rounded-lg focus:ring-red-500 focus:border-red-500 block w-full p-2 dark:border-gray-500 dark-placehorder-gray-200 dark:text-white">
                                            <option value="admin" selected>Admin</option>
                                            <option value="seller">Seller</option>
                                            <option value="traveller">Traveller</option>
                                            <option value="agent">Agent</option>
                                            <option value="corporate">Corporate</option>
                                            <option value="visitor">Visitor</option>
                                        </select>
                                    </div>
                                    {{-- <div class="col-span-2">
                                        <label for="role" class="block mb2 text font-medium text-gray-700 dark:text-white">Role</label>
                                        <div class="grid grid-cols-2 w-20">
                                            <div class="flex items-start mb-4">
                                                <input type="radio" name="role" value="admin" id="role_1" class="w-4 h-4 border-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-dark-700 dark:border-gray-600" checked="">
                                                <label for="role_1" class="block ml text-gray-700 font-medium dark:text-white">Admin</label>
                                            </div>
                                            <div class="flex items-start mb-4">
                                                <input type="radio" name="role" value="admin" id="role_2" class="w-4 h-4 border-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-dark-700 dark:border-gray-600" checked="">
                                                <label for="role_2" class="block ml text-gray-700 font-medium dark:text-white">Admin</label>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="col-span-2 hidden" id="fieldJenisRegistrasi">
                                        <label for="jenis_registrasi" class="block mb-2 text-sm font-medium text-gray-700 dark:text-white">Jenis Registrasi</label>
                                        <select name="jenis_registrasi" id="jenis_registrasi" class="bg-gray-100 border border-gray-200 text-gray-700 text-sm rounded-lg focus:ring-red-500 focus:border-red-500 block w-full p-2 dark:border-gray-500 dark-placehorder-gray-200 dark:text-white">
                                            <option value="personal" selected>Personal</option>
                                            <option value="company">Company</option>
                                        </select>
                                    </div>
                                    <div class="col-span-2 hidden" id="fieldLisensi">
                                        <label for="lisensi" class="block mb-2 text-sm font-medium text-gray-700 dark:text-white">Lisensi User</label>
                                        <select name="lisensi" id="lisensi" class="bg-gray-100 border border-gray-200 text-gray-700 text-sm rounded-lg focus:ring-red-500 focus:border-red-500 block w-full p-2 dark:border-gray-500 dark-placehorder-gray-200 dark:text-white">
                                            <option value="LISENSI" selected>LISENSI</option>
                                            <option value="LISENSI 1">LISENSI 1</option>
                                        </select>
                                    </div>
                                    <div>
                                        <label for="status_active" class="block mb-2 text-sm font-medium text-gray-700 dark:text-white">Status</label>
                                        <select name="status_active" id="status_active" class="bg-gray-100 border border-gray-200 text-gray-700 text-sm rounded-lg focus:ring-red-500 focus:border-red-500 block w-full p-2 dark:border-gray-500 dark-placehorder-gray-200 dark:text-white">
                                            <option value="active" selected>ACTIVE</option>
                                            <option value="penalty">PENALTY</option>
                                            <option value="banded">BANDED</option>
                                        </select>
                                    </div>
                                    <div class="col-span-2 text-end">
                                        <button type="submit" class="w-[102px] text-white bg-kamtuu-second p-2 rounded-lg hover:bg-kamtuu-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<script>
    $("#role").change(function(){
        console.log($("#role").val());
        if($("#role").val()==='seller'){
            $("#fieldJenisRegistrasi").removeClass("hidden");
            $("#fieldJenisRegistrasi").addClass("block");
        }else{
            // alert('test')
            $("#fieldJenisRegistrasi").removeClass("block");
            $("#fieldJenisRegistrasi").addClass("hidden");
        }
    });
    $("#jenis_registrasi").change(function(){
        console.log($("#lisensi").val());
        if($("#jenis_registrasi").val()==='company'){
            $("#fieldLisensi").removeClass("hidden");
            $("#fieldLisensi").addClass("block");
        }else{
            
            $("#fieldLisensi").removeClass("block");
            $("#fieldLisensi").addClass("hidden");
        }
    })
</script>