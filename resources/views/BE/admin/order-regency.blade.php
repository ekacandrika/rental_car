<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    {{--    select2--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"/>
    {{--  ajax  --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    {{--  select2 js  --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        /* CHECKBOX TOGGLE SWITCH */
        /* @apply rules for documentation, these do not work as inline style */
        .switch {
            position: relative;
            display: inline-block;
            width: 100px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #23AEC1;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #9E3D64;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #23AEC1;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(68px);
            -ms-transform: translateX(68px);
            transform: translateX(68px);
        }

        .on {
            display: none;
        }

        .on, .off {
            color: white;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            font-size: 10px;
            font-family: Verdana, sans-serif;
        }

        input:checked + .slider .on {
            display: block;
        }

        input:checked + .slider .off {
            display: none;
        }

        #idn:checked + #idn {
            display: block;
        }

        #sett:checked + #sett {
            display: block;
        }

        .destindonesia {
            display: none;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
{{-- Navbar --}}
<x-be.admin.navbar-admin></x-be.admin.navbar-admin>

<div class="grid grid-cols-10">
    <x-be.admin.sidebar-admin>
    </x-be.admin.sidebar-admin>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10">
            <span class="text-2xl font-bold font-inter text-[#333333]">Create Order</span>
            <div class="grid grid col-1 gap-5">
                <div class="bg-white rounded-lg p-3 w-full">
                    <div>
                        <form method="POST" action="{{ route('regencyOrder.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="container">
                                @if ($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success text-center">
                                        <p>{{ Session::get('success') }}</p>
                                    </div>
                                @endif
                                <table class="table table-bordered" id="dynamicAddRemove">
                                    <tr>
                                        <th>Regency :</th>
                                    </tr>
                                        <td>
                                        <div class="provinsi box">
                                            <input type="hidden" value="" name="provinsi_id[]" id="valueProvinsi">
                                            <select id='myselect2' name="select2 provinsi_id[]" type="text" id="text"
                                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                                <option value="">Pilih Kota</option>
                                                @foreach($regencies as $regency)
                                                    <option value="{{$regency->id}}">{{$regency->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="provinsi box">
                                                <input type="hidden" value="" name="provinsi_id[]" id="valueProvinsi2">
                                                <select id='myselect3' name="select3 provinsi_id[]" type="text"
                                                        id="text"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                                    <option value="">Pilih Kota</option>
                                                    @foreach($regencies as $regency)
                                                        <option value="{{$regency->id}}">{{$regency->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="provinsi box">
                                                <input type="hidden" value="" name="provinsi_id[]" id="valueProvinsi3">
                                                <select id='myselect4' name="select4 provinsi_id[]" type="text"
                                                        id="text"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                                    <option value="">Pilih Kota</option>
                                                    @foreach($regencies as $regency)
                                                        <option value="{{$regency->id}}">{{$regency->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="provinsi box">
                                                <input type="hidden" value="" name="provinsi_id[]" id="valueProvinsi4">
                                                <select id='myselect5' name="select5 provinsi_id[]" type="text"
                                                        id="text"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                                    <option value="">Pilih Kota</option>
                                                    @foreach($regencies as $regency)
                                                        <option value="{{$regency->id}}">{{$regency->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="provinsi box">
                                                <input type="hidden" value="" name="provinsi_id[]" id="valueProvinsi5">
                                                <select id='myselect6' name="select6 provinsi_id[]" type="text"
                                                        id="text"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                                    <option value="">Pilih Kota</option>
                                                    @foreach($regencies as $regency)
                                                        <option value="{{$regency->id}}">{{$regency->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="provinsi box">
                                                <input type="hidden" value="" name="provinsi_id[]" id="valueProvinsi6">
                                                <select id='myselect7' name="select7 provinsi_id[]" type="text"
                                                        id="text"
                                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                                    <option value="">Pilih Kota</option>
                                                    @foreach($regencies as $regency)
                                                        <option value="{{$regency->id}}">{{$regency->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="form-group text-end">
                                <button type="submit"
                                        class="bg-kamtuu-second p-2 rounded-lg text-white hover:bg-kamtuu-primary">
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<script type="text/javascript">
    $('#myselect2').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    });
    $('#myselect2').on("change", function (e) {
        var data = $("#myselect2").val()
        document.getElementById('valueProvinsi').setAttribute('value', data);
        console.log($("#myselect2").val())
    })
    $('#myselect3').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    });
    $('#myselect3').on("change", function (e) {
        var data = $("#myselect3").val()
        document.getElementById('valueProvinsi2').setAttribute('value', data);
        console.log($("#myselect3").val())
    })
    $('#myselect4').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    });
    $('#myselect4').on("change", function (e) {
        var data = $("#myselect4").val()
        document.getElementById('valueProvinsi3').setAttribute('value', data);
        console.log($("#myselect4").val())
    })
    $('#myselect5').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    });
    $('#myselect5').on("change", function (e) {
        var data = $("#myselect5").val()
        document.getElementById('valueProvinsi4').setAttribute('value', data);
        console.log($("#myselect5").val())
    })
    $('#myselect6').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    });
    $('#myselect6').on("change", function (e) {
        var data = $("#myselect6").val()
        document.getElementById('valueProvinsi5').setAttribute('value', data);
        console.log($("#myselect6").val())
    })
    $('#myselect7').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    });
    $('#myselect7').on("change", function (e) {
        var data = $("#myselect7").val()
        document.getElementById('valueProvinsi6').setAttribute('value', data);
        console.log($("#myselect7").val())
    })
</script>

@livewireScripts

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
</body>
</html>
