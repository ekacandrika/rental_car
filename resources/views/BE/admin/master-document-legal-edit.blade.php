<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    {{--summernote--}}
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    {{--    tag input bootstrap--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
          rel="stylesheet"/>
    {{--    select2--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"/>
    {{--jquery slim js--}}
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    {{--  summernote js  --}}
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    {{--  ajax  --}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>--}}
    {{--  select2 js  --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #dropHide {
            appearance: none;
            padding: 5px;
            color: white;
            border: none;
            font-family: inherit;
            outline: none;
            background-image:none;
        }
        #idn:checked + #idn {
            display: block;
        }

        #sett:checked + #sett {
            display: block;
        }

        .bootstrap-tagsinput .tag {
            margin-right: 2px;
            color: #ffffff;
            background: #2196f3;
            padding: 3px 7px;
            border-radius: 3px;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>
    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10 h-full">
                <div class=" p-5 border rounded-md bg-white my-5">
                    <p class="text-xl font-semibold">Edit Dokumen Legal</p>
                    <form method="POST" action="{{ route('legal.update',$id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="p-2 mb-2">
                            <label for="document_title" class="block mb-2 text-sm font-bold text-[#333333]">Judul/Nama Dokumen</label>
                            <input name="document_title" type="text" class="block mb-2 text-sm border border-[#4F4F4F] text-gray-900 rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" placeholder="Judul/Nama Dokumen" value="{{$dokumen->document_title}}"/>
                        </div>
                        @error('document_title')
                            <div class="p-2 mb-2" x-data="{show:true}" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                        @enderror
                        <div class="p-2">
                            <label for="is_mandatory" class="block mb-2 text-sm font-bold text-[#333333]">Dokumen Wajid</label>
                            <div class="flex gap-3">
                                <div class="flex items-center py-px">
                                    <input type="radio" name="is_mandatory" class="w-4 h-4 text-blue-500 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 focus:border-blue-500" value="yes" {{$dokumen->is_mandatory == 'yes' ? 'checked':''}}/>
                                    <label for="radio_private" class="ml-2 text-sm font-semibold text-[#333333]" valeu>Ya</label>
                                </div>
                                <div class="flex items-center py-px">
                                    <input type="radio" name="is_mandatory" class="w-4 h-4 text-blue-500 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 focus:border-blue-500" value="no" {{$dokumen->is_mandatory == 'no' ? 'checked':''}}/>
                                    <label for="radio_private" class="ml-2 text-sm font-semibold text-[#333333]">Tidak</label>
                                </div>
                            </div>
                        </div>
                        <div class="p-6 relative">
                            <button class="absolute right-0 px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2">Simpan</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</body>
<script>
    document.addEventListener("alpine:init",()=>{
        Alpine.data("documents",()=>({
            datas:[],
            url:"{{route('document.datas')}}",
            async init(){
                let fetching = await fetch(`${this.url}`);
                let json = await fetching.json();

                console.log(json);
            }
        }))
    })
</script>
</html>