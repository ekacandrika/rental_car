<!DOCTYPE html>
<html lang="{{str_replace('_','-',app()->getLocale())}}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">

        <title>{{config('app.name','Laravel')}}</title>

        {{-- Font --}}
        <link rel="stylesheet" href="https://fonts.bunny.net/css/base/jquery.fonticonpicker.min.css">

        {{-- base | always include--}}
            <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.1.1/dist/css/base/jquery.fonticonpicker.min.css">

        <!-- default grey-theme -->
        <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.0.0-alpha.0/dist/css/themes/grey-theme/jquery.fonticonpicker.grey.min.css">

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript"
        src="https://unpkg.com/@fonticonpicker/fonticonpicker/dist/js/jquery.fonticonpicker.min.js"></script>

        {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}
    
        <!-- Select 2 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>

        {{-- sweet alert --}}
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.all.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.min.css" rel="stylesheet">

        {{-- data tables --}}
        <style>
            @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

            /**
            * tailwind.config.js
            * module.exports = {
            *   variants: {
            *     extend: {
            *       backgroundColor: ['active'],
            *     }
            *   },
            * }
            */
            .active\:bg-gray-50:active {
                --tw-bg-opacity: 1;
                background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
            }

            #tur:checked+#tur {
                display: block;
            }

            #transfer:checked+#transfer {
                display: block;
            }

            #hotel:checked+#hotel {
                display: block;
            }

            #rental:checked+#rental {
                display: block;
            }

            #activity:checked+#activity {
                display: block;
            }

            #xstay:checked+#xstay {
                display: block;
            }

            #idn:checked+#idn {
                display: block;
            }

            #sett:checked+#sett {
                display: block;
            }

            .destindonesia {
                display: none;
            }
            .select2-container .select2-selection--single {
                box-sizing: border-box;
                cursor: pointer;
                display: block;
                height: 48px;
                user-select: none;
                -webkit-user-select: none;
            }
            .select2-container--default .select2-selection--single .select2-selection__rendered {
                color: #444;
                line-height: 46px;
            }
            .select2-container--default .select2-selection--single .select2-selection__arrow {
                height: 26px;
                position: absolute;
                top: 11px;
                right: 13px;
                width: 20px;
            }
        </style>
    </head>
    <body class="bg-[#F2F2F2] font-inter">
        {{-- Navbar --}}
        <x-be.admin.navbar-admin></x-be.admin.navbar-admin>

        {{-- Sidebar --}}
        <div class="grid grid-cols-10">
            <x-be.admin.sidebar-admin></x-be.admin.sidebar-admin>

            <div class="col-start-3 col-end-11 z-0">
                <div class="p-5 pl-10 pr-10">
                    <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                        <div class="grid grid-cols-2">
                            <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Master Titik</div>
                        </div>
                        <form action="{{route('master-titik.update',$id)}}" id="form-master-point-edit" method="post">
                            @csrf
                            @method('PUT')
                            <div class="p-6">
                                <label class="block mb-2 text-xl font-bold text-[#333333]">Provinsi</label>
                                <select class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" id="provinsi-id">
                                    <option>-Pilih Provinsi-</option>
                                    @foreach ($provinces as $item)
                                        <option value="{{$item->id}}" {{$item->id == $provinsi_data->id ? 'selected':''}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="p-6">
                                <label class="block mb-2 text-xl font-bold text-[#333333]">Kota/Kabupaten</label>
                                <select class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" id="kabupaten-id">
                                    <option>-Pilih Kota/Kabupaten-</option>
                                    @foreach ($regencies as $item)
                                        <option value="{{$item->id}}" {{$item->id == $regency_data->id ? 'selected':''}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="p-6">
                                <label class="block mb-2 text-xl font-bold text-[#333333]">Kecamatan</label>
                                <select name="district_id" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" id="kecamatan-id">
                                    <option>-Pilih Kecamatan-</option>
                                    @foreach ($districts as $item)
                                        <option value="{{$item->id}}" {{$item->id == $district_data->id ? 'selected':''}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="p-6">
                                <label class="block mb-2 text-xl font-bold text-[#333333]">Nama Titik</label>
                                <input type="hidden" class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" id="district-name" value="{{$district_data->name}}"/>
                                <input class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" id="name-point-id" name="name_titik" value="{{$row->name_titik}}"/>
                            </div>
                            <div class="p-6">
                                <button type="submit" class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2" id="btn-submit-edit">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>

        $("#provinsi-id").select2();
        $("#provinsi-id").on("change",function(){

            let provinsi_id = $(this).val();

            $.ajax({
                type:'GET',
                url:"/admin/kamtuu/route/regencies/"+provinsi_id,
                cache:'false',
                success:function(resp){
                    console.log(resp)
                    $("#kabupaten-id").html(resp)
                },
                error:function(data){
                    console.log('error:',data);
                }
            })
        })

        $("#kabupaten-id").select2();
        $("#kabupaten-id").on("change",function(){
          
            let kabupaten_id = $(this).val();
            let url = "{{route('master.titik.district',':id')}}";
            url = url.replace(':id',kabupaten_id);

            $.ajax({
                type:'GET',
                url:`${url}`,
                cache:'false',
                success:function(resp){
                    console.log(resp)
                    $("#kecamatan-id").html(resp)
                },
                error:function(data){
                    console.log('error:',data);
                }
            })

        })

        $("#kecamatan-id").select2();
        $("#kecamatan-id").on("change",function(){
            var name = $(this).find('option:selected').attr("data-name");
            console.log(name);
            $("#district-name").val(name)
        });
    </script>
</html>