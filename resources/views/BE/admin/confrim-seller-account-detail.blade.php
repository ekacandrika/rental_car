<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    {{-- <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" /> --}}

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css" rel="stylesheet">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        /* CHECKBOX TOGGLE SWITCH */
        /* @apply rules for documentation, these do not work as inline style */
        .switch {
            position: relative;
            display: inline-block;
            width: 100px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #23AEC1;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #9E3D64;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #23AEC1;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(68px);
            -ms-transform: translateX(68px);
            transform: translateX(68px);
        }

        .on {
            display: none;
        }

        .on,
        .off {
            color: white;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            font-size: 10px;
            font-family: Verdana, sans-serif;
        }

        input:checked+.slider .on {
            display: block;
        }

        input:checked+.slider .off {
            display: none;
        }

        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }

        .destindonesia {
            display: none;
        }
        .disabled {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
            cursor: not-allowed;
        }
                /* The Modal (background) */
        .modal {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Stay in place */
            z-index: 1;
            /* Sit on top */
            padding-top: 100px;
            /* Location of the box */
            left: 10%;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow: auto;
            /* Enable scroll if needed */
            background-color: rgb(0, 0, 0);
            /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4);
            /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            border: 1px solid #888;
            width: 55%;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }
                /* Add Animation */
        @-webkit-keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }

            to {
                top: 0;
                opacity: 1
            }
        }

        @keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }

            to {
                top: 0;
                opacity: 1
            }
        }

        /* The Close Button */
        .close {
            color: white;
            float: right;
            margin-top: -8px;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        .modal-header {
            padding: 3px 16px;
            font-size: 20px;
            background-color: #9E3D64;
            color: white;
        }

        .modal-body {
            padding: 2px 16px;
        }

        .modal-footer {
            padding: 2px 16px;
            text-align: right;
            background-color: #fff;
        }

        [x-cloak] {
            display: none
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>
<body class="bg-[#F2F2F2] font-inter">
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>   
    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <div class="my-2">
                    {{-- http://localhost:8000/storage/img/logo-traveller.png --}}
                    <img src="{{isset($seller->profile_photo_path) ? asset($seller->profile_photo_path) :asset('storage/icons/user-solid.svg')}}" alt="user-profile" class="object-cover object-center overflow-hidden rounded-full border-indigo-600 h-24 w-24" width="100px">
                </div>                
                <div class="my-2">
                    <span class="text-base font-bold font-inter text-[#333333]">Nama : {{$seller->first_name}}</span>
                <div>
                <div class="my-2">
                    <span class="text-base font-bold font-inter text-[#333333]">No Hp atau Whatsapp : {{$seller->whatsapp_no != null ? ($seller->whatsapp_no==$seller->no_tlp ? $seller->no_tlp : $seller->whatsapp_no.' atau'.$seller->no_tlp) :$seller->no_tlp }}</span>
                <div>
                <div class="my-2">
                    <span class="text-base font-bold font-inter text-[#333333]">Email : {{$seller->email}}</span>
                <div>
                <div class="flex gap-5 my-3">
                    <div class="bg-white rounded-lg p-3 w-full">
                        <span class="text-base font-bold font-inter text-[#333333]">List Seller</span>
                        <div class="py-2 mx-3">
                            <div class="bg-white">
                                <table class="w-full text-sm text-left text-[#333333] mt-5">
                                    <thead class="uppercase border border-dark text-gray-500 text-xs font-semibold bg-gray-200">
                                        <tr class="hidden md:table-row">
                                            <th class="px-6 py-3">No</th>
                                            <th class="px-6 py-3">Variable</th>
                                            <th class="px-6 py-3">Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody class="flex-1 text-gray-700 sm:flex-none">
                                        @foreach($legal_doc as $legal)
                                        <tr>
                                            <td class="px-6 py-3">
                                                <span class="text-center">
                                                    {{ $loop->iteration}}
                                                </span>
                                            </td>
                                            <td class="px-6 py-3">
                                                <span class="text-center">
                                                    {{isset($legal->value) ?  'Dokumen tersedia' : 'Dokumen tidak tersedia'}}
                                                </span>
                                            </td>
                                            <td class="px-6 py-3">
                                                <a class="{{isset($legal->value) ?'':'disabled'}}"
                                                    {{-- class="text-white bg-yellow-500 hover:bg-yellow-600 duration-200 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center mr-2 dark:bg-yellow-500 dark:hover:bg-yellow-600 dark:focus:ring-blue-800 w-[37px]" --}}
                                                    >
                                                    <button type="button" onclick="showImgModal(true,'{{isset($legal->value) ? asset($legal->value): asset('storage/icons/folder-solid.svg')}}')" class="bg-transparent border-0">
                                                        <img src="{{ asset('storage/icons/folder-solid.svg') }}" alt="" width="16px">
                                                    </button>
                                                    <div id="modal"></div>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <form>
                            <div class="my-3">
                                <span class="text-base font-bold font-inter text-[#333333]">Confrimation Seller Account</span>
                            <div>
                            <div class="my-2">
                                <div class="form-group">
                                <select
                                    class="select w-1/3 select-bordered my-2 form-select appearance-none
                                        block
                                        w-full
                                        px-3
                                        py-1.5
                                        text-base
                                        font-normal
                                        text-gray-700
                                        bg-white bg-clip-padding bg-no-repeat
                                        border border-solid border-gray-300
                                        rounded
                                        transition
                                        ease-in-out
                                        m-0
                                        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                    id="status_active" name="section">
                                        <option value="Confrimed" {{$seller->active_status=='Confrimed'?'selected':""}}>Confrim</option>
                                        <option value="Unconfirmed" {{$seller->active_status=='Unconfirmed'?'selected':""}}>Unconfrim</option>
                                </select>
                                <div>
                            <div>
                            <div class="my-5 form-group text-start">
                                <a>
                                    <button id="btnConfrim" type="button"
                                        class="bg-kamtuu-second p-2 rounded-lg text-white hover:bg-kamtuu-primary">
                                        Simpan
                                    </button>
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</body>
</html>
@livewireScripts    
<script>
function showImgModal(val, img){
    console.log('test');
    distroyModal();

    if(val){
        loadModal(img);
        $(".modal").css("display","block")
        $("#imgModalPopup").removeClass("hidden");
    }
}

function closeImgModal(){
    $(".modal").css("display","none")
    $("#imgModalPopup").addClass("hidden")
    distroyModal()
}

function loadModal(img){
    return $("#modal").html(`
        <div class="modal fixed inset-0 z-[99999] overflow-y-auto hidden" aria-labelledby="modal-title" role="dialog" arial-modal="true" id="imgModalPopup">
            <div class="flex item-end justify-center min-h-screen text-center md:item-center sm:block sm:p-0">
                <div class="flex inset-0 duration-200" onclick="closeImgModal()" style="background-color:rbga(0, 0, 0, .7)">
                </div>
                <div class="opacity-100 translate-y-0 transition ease-in duration-200 trasform sm:scale-100 inline-block w-full max-w-xl p-8 my-20 overflow-hidden text-left transition-all transform bg-white rounded-lg shadow-xl 2xl:max-w-2xl">
                    <div class="flex items-center jjustify-end space-x-4">
                        <button type="button" onclick="closeImgModal()" class="text-gray-600 focus:outline-none hover:text-gray-700" @click="show=false">
                            <img src="{{asset('storage/icons/close-button.svg')}}" class="w-6 h-6"/>
                        </button>
                    </div>
                    <div class="my-5 text-center space-y-2">
                        <img src="${img}" alt="document image"/>
                    <div>
                    <div class="flex justify-end space-x-2">
                        <button type="button" onclick="closeImgModal()" class="item-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px text-xs lg:text-md font-semiboldtext-center text-[#D50006] bg-[#white] border border-[#D50006] hover:bg-slate-100 duration -200 rounded lg:rounded-lg">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    `)
}

function distroyModal(){
    return $('#modal').html("")
}
//  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')


$("#btnConfrim").on("click",function(){
    Swal.fire({
      title: 'Ubah status seller',
      text: "Apakah anda yakin akan mengubah status seller?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Batal',
      confirmButtonText: 'Ok'
    }).then((result) => {
      if (result.isConfirmed) {

        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        
        $.ajax({
            method:'PUT',
            url:"{{route('confrim.seller-account.update',$seller->id)}}",
            data:{
                status_active:$("#status_active").val()
            },
            success:function(msg){
                console.log(msg)
                Swal.fire(
                  'Updated!',
                   msg.message,
                  'success'
                ).then((result)=>{
                    // console.log(result)
                    if(result.isConfirmed){
                          window.location.replace(msg.url)
                    }
                })

            },
            error:function(data){
                console.log('error:',data)
            }
        })
        
      }
    })
});


</script>