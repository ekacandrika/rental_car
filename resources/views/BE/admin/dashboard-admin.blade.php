<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }

        #sett2:checked+#sett2 {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>

    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10 h-full">
                <span class="text-2xl font-bold font-inter text-[#333333]">Dashboard</span>

                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Master Wilayah</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">10.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Tempat</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Banyak Listing</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">15.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Listing</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded ml-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Extra</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">5.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Extra</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Grafik --}}
                <div class="grid grid-cols-2 text-center h-96">
                    <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                    </div>
                    <div class="text-center ml-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                    </div>
                </div>

                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Traveller</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">20.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Traveller</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Seller</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">25.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Seller</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded ml-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Agent</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">20.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Agent</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Corporate</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">500</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Corporate</p>
                            </div>
                        </div>
                    </div>
                </div>

                <span class="text-lg font-bold font-inter text-[#333333]">Data Layanan</span>

                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400 my-4">
                    <thead class="text-xs text-white uppercase bg-[#9E3D64] dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="py-3 px-6">
                                Keterangan Layanan
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Sudah Dibayarkan
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Belum dibayarkan
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Tour
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Travel
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Rental
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Activity
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Hotel
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                XStay
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                    this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                    if (!event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },
            }
        }
    </script>
</body>

</html>
