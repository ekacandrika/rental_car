<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        img {
            display: block;
            max-width: 100%;
        }
        .preview {
            overflow: hidden;
            width: 160px;
            height: 160px;
            margin: 10px;
            border: 1px solid red;
        }
        .modal-lg{
            max-width: 1000px !important;
        }
        #idn:checked + #idn {
            display: block;
        }

        #sett:checked + #sett {
            display: block;
        }

        .destindonesia {
            display: none;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
{{-- Navbar --}}
<x-be.admin.navbar-admin></x-be.admin.navbar-admin>

<div class="grid grid-cols-10">
    <x-be.admin.sidebar-admin>
    </x-be.admin.sidebar-admin>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10">
            <span class="text-2xl font-bold font-inter text-[#333333]">Create Section</span>
            <div class="flex gap-5">
                <div class="bg-white rounded-lg p-3 w-full">
                    <div>
                        <form method="POST" action="{{ route('section.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <div class="flex justify-start">
                                    <div class="mb-4 p-4 text-sm w-80">
                                        <div class="font-bold mb-2">Upload Image Preview</div>
                                        <div class="" x-data="previewImage()">

                                            <label for="logo">
                                                <div
                                                    class="w-full h-48 rounded bg-gray-100 border border-gray-200 flex items-center justify-center overflow-hidden">
                                                    <img x-show="imageUrl" :src="imageUrl" class="w-full object-cover">
                                                    <div x-show="!imageUrl"
                                                         class="text-gray-300 flex flex-col items-center">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 "
                                                             fill="none" viewBox="0 0 24 24" stroke="currentColor"
                                                             stroke-width="2">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                  d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"/>
                                                        </svg>
                                                        <div>Image Preview</div>
                                                    </div>
                                                </div>
                                            </label>
                                            <div>
                                                <label for="logo" class="block mb-2 mt-4 font-bold">Upload
                                                    image..</label>
                                                <input class="w-full cursor-pointer" type="file" name="image_section"
                                                       id="logo" @change="fileChosen">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-end">
                                <button type="submit"
                                        class="bg-kamtuu-second p-2 rounded-lg text-white hover:bg-kamtuu-primary">
                                    Simpan
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<script>
    function previewImage() {
        return {
            imageUrl: "",

            fileChosen(event) {
                this.fileToDataUrl(event, (src) => (this.imageUrl = src));
            },

            fileToDataUrl(event, callback) {
                if (!event.target.files.length) return;

                let file = event.target.files[0],
                    reader = new FileReader();

                reader.readAsDataURL(file);
                reader.onload = (e) => callback(e.target.result);
            },
        };
    }
</script>
</body>
</html>
