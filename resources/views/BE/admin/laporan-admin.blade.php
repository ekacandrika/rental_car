<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Chart Js -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>

    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-2xl font-bold font-inter text-[#333333]">Laporan</span>
            </div>
            <div class="flex items-center justify-center max-w-screen-sm md:mx-2 lg:mx-auto lg:max-w-[85rem]">

                <div class="container flex flex-col items-stretch justify-center h-full p-2 mx-auto sm:p-8"
                    x-data="{ tab: 1 }">

                    <!-- TABS -->
                    <div class="z-10 flex justify-start -space-x-px">
                        <div class="grid grid-cols-3 grid-rows-1 md:flex lg:flex">
                            <a href="!#0" @click.prevent="tab = 1"
                                :class="{
                                    'cursor-default border-b-0 bg-kamtuu-second': tab ===
                                        1,
                                    'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                                        1
                                }"
                                class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none lg:rounded-tl-lg">Paket
                                Tur
                            </a>
                            <a href="!#0" @click.prevent="tab = 2"
                                :class="{
                                    'cursor-default border-b-0 bg-kamtuu-second': tab ===
                                        2,
                                    'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                                        2
                                }"
                                class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none">Transfer
                            </a>
                            <a href="!#0" @click.prevent="tab = 3"
                                :class="{
                                    'cursor-default border-b-0 bg-kamtuu-second': tab ===
                                        3,
                                    'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                                        3
                                }"
                                class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none ">Rental
                                Kendaraan
                            </a>
                            <a href="!#0" @click.prevent="tab = 4"
                                :class="{
                                    'cursor-default border-b-0 bg-kamtuu-second': tab ===
                                        4,
                                    'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                                        4
                                }"
                                class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none">Aktivitas
                            </a>
                            <a href="!#0" @click.prevent="tab = 5"
                                :class="{
                                    'cursor-default border-b-0 bg-kamtuu-second': tab ===
                                        5,
                                    'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                                        5
                                }"
                                class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none">Hotel
                            </a>
                            <a href="!#0" @click.prevent="tab = 6"
                                :class="{
                                    'cursor-default border-b-0 bg-kamtuu-second': tab ===
                                        6,
                                    'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                                        6
                                }"
                                class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none lg:rounded-tr-md">Xstay
                            </a>
                        </div>
                    </div>


                    <!-- Tabs Contains -->
                    {{-- Paket Tur --}}
                    <div x-show="tab === 1"
                        class="z-0 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-7 md:grid-cols-1 bg-white justify-items-center divide-kamtuu-second lg:divide-x">
                        <div class="p-5 pl-10 pr-10 h-full">

                            {{-- Grafik --}}
                            <div class="text-center h-full mb-5">
                                <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                                    <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                                    <canvas id="myChartTour"></canvas>
                                </div>
                            </div>

                            <div class="text-center h-full mb-5">
                                <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                                    <p class="mt-2 font-bold text-[#333333] text-base">Grafik Penjualan Produk</p>
                                    <canvas id="myChartTourPenjualanProduk"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Transfer --}}
                    <div x-show="tab === 2"
                        class="z-0 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-7 md:grid-cols-1 bg-white justify-items-center divide-kamtuu-second lg:divide-x">
                        <div class="p-5 pl-10 pr-10 h-full">

                            {{-- Grafik --}}
                            <div class="text-center h-full mb-5">
                                <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                                    <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                                    <canvas id="myChartTransfer"></canvas>
                                </div>
                            </div>

                            <div class="text-center h-full mb-5">
                                <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                                    <p class="mt-2 font-bold text-[#333333] text-base">Grafik Penjualan Produk</p>
                                    <canvas id="myChartTransferPenjualanProduk"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Rental Kendaraan --}}
                    <div x-show="tab === 3"
                        class="z-0 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-6 md:grid-cols-1 bg-white justify-items-center divide-kamtuu-second lg:divide-x">
                        <div class="p-5 pl-10 pr-10 h-full">

                            {{-- Grafik --}}
                            <div class="text-center h-full mb-5">
                                <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                                    <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                                    <canvas id="myChartRental"></canvas>
                                </div>
                            </div>

                            <div class="text-center h-full mb-5">
                                <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                                    <p class="mt-2 font-bold text-[#333333] text-base">Grafik Penjualan Produk</p>
                                    <canvas id="myChartRentalPenjualanProduk"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Aktivitas --}}
                    <div x-show="tab === 4"
                        class="z-0 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-7 md:grid-cols-1 bg-white justify-items-center divide-kamtuu-second lg:divide-x">
                        <div class="p-5 pl-10 pr-10 h-full">

                            {{-- Grafik --}}
                            <div class="text-center h-full mb-5">
                                <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                                    <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                                    <canvas id="myChartActivity"></canvas>
                                </div>
                            </div>

                            <div class="text-center h-full mb-5">
                                <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                                    <p class="mt-2 font-bold text-[#333333] text-base">Grafik Penjualan Produk</p>
                                    <canvas id="myChartActivityPenjualanProduk"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- HOTEL --}}
                    <div x-show="tab === 5"
                        class="z-0 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-6 md:grid-cols-1 bg-white justify-items-center divide-kamtuu-second lg:divide-x">
                        <div class="p-5 pl-10 pr-10 h-full">

                            {{-- Grafik --}}
                            <div class="text-center h-full mb-5">
                                <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                                    <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                                    <canvas id="myChartHotel"></canvas>
                                </div>
                            </div>

                            <div class="text-center h-full mb-5">
                                <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                                    <p class="mt-2 font-bold text-[#333333] text-base">Grafik Penjualan Produk</p>
                                    <canvas id="myChartHotelPenjualanProduk"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- XSTAY --}}
                    <div x-show="tab === 6"
                        class="z-0 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-6 md:grid-cols-1 bg-white justify-items-center divide-kamtuu-second lg:divide-x">
                        <div class="p-5 pl-10 pr-10 h-full">

                            {{-- Grafik --}}
                            <div class="text-center h-full mb-5">
                                <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                                    <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                                    <canvas id="myChartXstay"></canvas>
                                </div>
                            </div>

                            <div class="text-center h-full mb-5">
                                <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                                    <p class="mt-2 font-bold text-[#333333] text-base">Grafik Penjualan Produk</p>
                                    <canvas id="myChartXstayPenjualanProduk"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="p-5 pl-10 pr-10 h-full">
                {{-- Grafik --}}
                <div class="text-center h-full mb-5">
                    <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Seller Penjualan Produk
                            Terbanyak</p>
                        <canvas id="myChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                    this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                    if (!event.target.files.length) return

                    let file = event.target.files[0],
                        reader = new FileReader()

                    reader.readAsDataURL(file)
                    reader.onload = e => callback(e.target.result)
                },
            }
        }
    </script>

    <script>
        //-- Chart Pendapatan --//
        // Chart Hotel
        var ctx = document.getElementById("myChartHotel").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?= json_encode($months_hotel) ?>,
                datasets: [{
                    label: 'Pendapatan',
                    data: <?= json_encode(array_values($earnings_hotel)) ?>,
                    backgroundColor: '#9E3D64',
                    borderColor: '#9E3D64',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Chart Xstay
        var ctx = document.getElementById("myChartXstay").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?= json_encode($months_xstay) ?>,
                datasets: [{
                    label: 'Pendapatan',
                    data: <?= json_encode(array_values($earnings_xstay)) ?>,
                    backgroundColor: '#9E3D64',
                    borderColor: '#9E3D64',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Chart Rental
        var ctx = document.getElementById("myChartRental").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?= json_encode($months_rental) ?>,
                datasets: [{
                    label: 'Pendapatan',
                    data: <?= json_encode(array_values($earnings_rental)) ?>,
                    backgroundColor: '#9E3D64',
                    borderColor: '#9E3D64',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Chart Transfer
        var ctx = document.getElementById("myChartTransfer").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?= json_encode($months_transfer) ?>,
                datasets: [{
                    label: 'Pendapatan',
                    data: <?= json_encode(array_values($earnings_transfer)) ?>,
                    backgroundColor: '#9E3D64',
                    borderColor: '#9E3D64',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Chart Activity
        var ctx = document.getElementById("myChartActivity").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?= json_encode($months_activity) ?>,
                datasets: [{
                    label: 'Pendapatan',
                    data: <?= json_encode(array_values($earnings_activity)) ?>,
                    backgroundColor: '#9E3D64',
                    borderColor: '#9E3D64',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Chart Tour
        var ctx = document.getElementById("myChartTour").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?= json_encode($months_tour) ?>,
                datasets: [{
                    label: 'Pendapatan',
                    data: <?= json_encode(array_values($earnings_tour)) ?>,
                    backgroundColor: '#9E3D64',
                    borderColor: '#9E3D64',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        //-- Chart Penjualan Produk --//
        var ctx = document.getElementById("myChartHotelPenjualanProduk").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach ($bulanHotel as $b)
                        '{{ $b }}',
                    @endforeach
                ],
                datasets: [{
                    label: 'Penjualan Produk',
                    data: [
                        @foreach ($jumlahHotel as $j)
                            '{{ $j }}',
                        @endforeach
                    ],
                    backgroundColor: '#9E3D64',
                    borderColor: '#9E3D64',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Chart Xstay
        var ctx = document.getElementById("myChartXstayPenjualanProduk").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach ($bulanXstay as $b)
                        '{{ $b }}',
                    @endforeach
                ],
                datasets: [{
                    label: 'Penjualan Produk',
                    data: [
                        @foreach ($jumlahXstay as $j)
                            '{{ $j }}',
                        @endforeach
                    ],
                    backgroundColor: '#9E3D64',
                    borderColor: '#9E3D64',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Chart Rental
        var ctx = document.getElementById("myChartRentalPenjualanProduk").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach ($bulanRental as $b)
                        '{{ $b }}',
                    @endforeach
                ],
                datasets: [{
                    label: 'Penjualan Produk',
                    data: [
                        @foreach ($jumlahRental as $j)
                            '{{ $j }}',
                        @endforeach
                    ],
                    backgroundColor: '#9E3D64',
                    borderColor: '#9E3D64',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Chart Transfer
        var ctx = document.getElementById("myChartTransferPenjualanProduk").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach ($bulanTransfer as $b)
                        '{{ $b }}',
                    @endforeach
                ],
                datasets: [{
                    label: 'Penjualan Produk',
                    data: [
                        @foreach ($jumlahTransfer as $j)
                            '{{ $j }}',
                        @endforeach
                    ],
                    backgroundColor: '#9E3D64',
                    borderColor: '#9E3D64',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Chart Activity
        var ctx = document.getElementById("myChartActivityPenjualanProduk").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach ($bulanActivity as $b)
                        '{{ $b }}',
                    @endforeach
                ],
                datasets: [{
                    label: 'Penjualan Produk',
                    data: [
                        @foreach ($jumlahActivity as $j)
                            '{{ $j }}',
                        @endforeach
                    ],
                    backgroundColor: '#9E3D64',
                    borderColor: '#9E3D64',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Chart Tour
        var ctx = document.getElementById("myChartTourPenjualanProduk").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach ($bulanTour as $b)
                        '{{ $b }}',
                    @endforeach
                ],
                datasets: [{
                    label: 'Penjualan Produk',
                    data: [
                        @foreach ($jumlahTour as $j)
                            '{{ $j }}',
                        @endforeach
                    ],
                    backgroundColor: '#9E3D64',
                    borderColor: '#9E3D64',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Seller Penjualan Produk Terbanyak
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    @foreach ($data as $value)
                        '{{ $value['namaUser'] }}',
                    @endforeach
                ],
                datasets: [{
                    label: 'Seller Penjualan Produk Terbanyak',
                    data: [
                        @foreach ($data as $value)
                            '{{ $value['jumlahTerjual'] }}',
                        @endforeach
                    ],
                    backgroundColor: '#9E3D64',
                    borderColor: '#9E3D64',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>

</body>

</html>
