<!DOCTYPE html>
{{--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">--}}


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>{{ config('app.name', 'Laravel') }}</title>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />


    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />


    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">


    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])


    <style>
        #akun:checked+#akun {
            display: block;
        }


        #inbox:checked+#inbox {
            display: block;
        }


        h1 {
            font-size: 30px;
            color: #000;
        }


        h2 {
            font-size: 20px;
            color: #000;
        }


        .menu-malasngoding li a {
            display: inline-block;
            color: white;
            /* text-align: center; */
            text-decoration: none;
        }


        .menu-malasngoding li a:hover {
            background-color: none;
        }


        li.dropdown {
            display: inline-block;
            margin-right: 80px
        }


        .dropdown:hover .isi-dropdown {
            display: block;
        }


        .isi-dropdown a:hover {
            color: #fff !important;
            width: 100%;
        }


        .isi-dropdown {
            position: absolute;
            display: none;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
            background-color: #f9f9f9;
            width: 100%;
        }


        .isi-dropdown a {
            color: #3c3c3c !important;
            padding: 1%;
        }


        .isi-dropdown a:hover {
            color: #232323 !important;
            background: #f3f3f3 !important;
        }


        a[disabled="disabled"] {
            pointer-events: none;
        }
    </style>


    <!-- Styles -->
    @livewireStyles
</head>


<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>


    {{-- Sidebar --}}
    <x-be.admin.sidebar-admin></x-be.admin.sidebar-admin>


    <div class="grid grid-cols-10">
        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-2xl font-bold font-inter text-[#333333]">Pesan</span>


                <div class="">
                    <div class="container mx-auto px-4 sm:px-8">
                        <div class="py-8">
                            <div>
                                <h2 class="text-2xl font-semibold leading-tight">Messages</h2>
                            </div>
                            <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                                <div class="inline-block min-w-full shadow-md rounded-lg overflow-hidden">
                                    <table class="min-w-full leading-normal">
                                        <thead>
                                            <tr>
                                                <th
                                                    class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-700 uppercase tracking-wider">
                                                    Seller
                                                </th>
                                                <th
                                                    class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-700 uppercase tracking-wider">
                                                    Issued / Due
                                                </th>
                                                {{-- <th--}} {{--
                                                    class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-700 uppercase tracking-wider"
                                                    --}} {{-->--}}
                                                    {{-- Status--}}
                                                    {{-- </th>--}}
                                                    <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100"></th>
                                            </tr>
                                        </thead>
                                        <tbody>


                                            @forelse($chatAll as $chat)
                                            {{-- {{dd($chat)}}--}}
                                            <tr>
                                                <td class="px-5 py-5 bg-white text-sm">
                                                    <div class="flex">
                                                        <div class="flex-shrink-0 w-10 h-10">
                                                            <img class="w-full h-full rounded-full"
                                                                src="https://images.unsplash.com/photo-1522609925277-66fea332c575?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.2&h=160&w=160&q=80"
                                                                alt="" />
                                                        </div>
                                                        <div class="ml-3">
                                                            <p class="text-gray-900 whitespace-no-wrap">
                                                                {{-- {{dd($chat)}}--}}
                                                                {{$chat->first_name}}
                                                            </p>
                                                            {{-- <p class="text-gray-600 whitespace-no-wrap">
                                                                {{$traveller->id}}</p>--}}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="px-5 py-5 bg-white text-sm">
                                                    {{-- {{dd($chat)}}--}}
                                                    <p class="text-gray-900 whitespace-no-wrap">
                                                        {{$chat->updated_at->diffForHumans()}}</p>
                                                    {{$chat->updated_at->format('d F, Y')}}
                                                </td>
                                                {{-- <td class="px-5 py-5 bg-white text-sm">--}}
                                                    {{--<span--}} {{--
                                                        class="relative inline-block px-3 py-1 font-semibold text-red-900 leading-tight"
                                                        --}} {{-->--}}
                                                        {{--<span--}} {{-- aria-hidden--}} {{--
                                                            class="absolute inset-0 bg-red-200 opacity-50 rounded-full"
                                                            --}} {{--></span>--}}
                                                            {{--<span class="relative">Overdue</span>--}}
                                                            {{--</span>--}}
                                                            {{-- </td>--}}
                                                <td class="px-5 py-5 bg-white text-sm text-right">
                                                    <button type="button"
                                                        class="inline-block text-gray-500 hover:text-gray-700">
                                                        {{-- {{dd($chat)}}--}}
                                                        <a
                                                            href="{{route('indexMessagesAdminInbox',  $chat->sender)}}">Chat</a>
                                                    </button>
                                                </td>
                                            </tr>
                                            @empty
                                            Tidak ada pesan
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    </div>


    @livewireScripts
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
</body>


</html>
