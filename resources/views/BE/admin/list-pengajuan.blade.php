<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #report:checked+#report {
            display: block;
        }



        .menu-dropdown li a {
            display: inline-block;
            color: white;
            /* text-align: center; */
            text-decoration: none;
        }

        .menu-dropdown li a:hover {
            background-color: none;
        }

        li.dropdown {
            display: inline-block;
            margin-right: 80px
        }

        .dropdown:hover .isi-dropdown {
            display: block;
        }

        .isi-dropdown a:hover {
            color: #fff !important;
            width: 100%;
        }

        .isi-dropdown {
            position: absolute;
            display: none;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
            background-color: #f9f9f9;
            width: 100%;
        }

        .isi-dropdown a {
            color: #3c3c3c !important;
            padding: 1%;
        }

        .isi-dropdown a:hover {
            color: #232323 !important;
            background: #f3f3f3 !important;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">

    {{-- Navbar --}}
    <x-be.seller.navbar-seller></x-be.seller.navbar-seller>

    {{-- Sidebar --}}
    <x-be.seller.sidebar-seller></x-be.seller.sidebar-seller>

    <div class="col-start-3 col-end-11 z-0">
        {{-- List data booking --}}
        <div class="p-5 pr-10" style="padding-left: 315px">
            <div class="flex items-center p-2 font-bold text-dark">
                <img src="{{ asset('storage/icons/cart-shopping-solid (1).svg') }}" class="w-[16px] h-[16px] mt-1"
                    alt="user" title="user">
                <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">List Pengajuan Penarikan</p>
            </div>

            {{-- body --}}
            <div class="max-w-6xl bg-gray-200 rounded-md border border-black p-2 my-5">
                <div class="grid grid-cols-2">
                    <div class="grid justify-items-start mb-5">
                    </div>
                    <div class="grid justify-items-end">
                        <div class="pt-2 relative text-gray-600">
                            <input
                                class="border-2 border-gray-300 bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none"
                                type="search" name="search" placeholder="Search">
                            <button type="submit" class="absolute right-0 top-0 mt-5 mr-4">
                                <svg class="text-gray-600 h-4 w-4 fill-current" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1"
                                    x="0px" y="0px" viewBox="0 0 56.966 56.966"
                                    style="enable-background:new 0 0 56.966 56.966;" xml:space="preserve" width="512px"
                                    height="512px">
                                    <path
                                        d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>

                @forelse ($data as $item)
                    @php
                        $pengajuan = App\Models\Pengajuan::where('id', $item->pengajuan_id)->first();
                        // dd($pengajuan);
                        $produk = App\Models\Product::with('productdetail')
                            ->where('id', $pengajuan->product_id)
                            ->first();
                        $seller = App\Models\User::where('id', $pengajuan->toko_id)->first();
                    @endphp
                    <div class="bg-kamtuu-produk-warning bg-opacity-25 p-3 rounded-md border border-gray-400 my-5">
                        <div class="grid grid-cols-12">
                            {{-- Cols 2 --}}
                            <div class="col-span-2 m-5">
                                <img class="w-28 h-28 rounded-md border border-gray-300"
                                    src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                    alt="">
                            </div>
                            {{-- Cols 3 --}}
                            <div class="m-5 col-start-3 col-span-5">
                                <p>Activity</p>
                                <p class="text-lg font-bold">{{ $produk->product_name }}</p>
                            </div>
                            {{-- Cols 4 --}}
                            <div class="col-span-4">
                                <p>Status: {{ $item->status }}</p>
                                <p>Tanggal Pengajuan:
                                    {{ Carbon\Carbon::parse($item->tgl_pengajuan)->translatedFormat('d F Y') }}</p>
                                <p>Dari: {{ $seller->first_name }}</p>
                                <p class="text-blue-600 text-2xl">IDR {{ number_format($item->nominal) }}</p>
                                <div class="flex gap-2 items-center">
                                    <form action="{{ route('verifikasiPengajuan', $item->id) }}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <input type="hidden" name="pengajuan_id" value="{{ $item->pengajuan_id }}">
                                        <button type="submit"
                                            class="items-center px-px py-1 lg:py-2 lg:w-40 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-black bg-[#00d532] rounded lg:rounded-lg">Verifikasi
                                            Pengajuan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                @endforelse
            </div>
        </div>

        {{-- List request Pembatalan dari traveller --}}
        <div class="p-5 pr-10" style="padding-left: 315px">
            <div class="flex items-center p-2 font-bold text-dark">
                <img src="{{ asset('storage/icons/cart-shopping-solid (1).svg') }}" class="w-[16px] h-[16px] mt-1"
                    alt="user" title="user">
                <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Telah Diverifikasi</p>
            </div>

            {{-- body --}}
            <div class="max-w-6xl bg-gray-200 rounded-md border border-black p-2 my-5">
                <div class="grid grid-cols-2">
                    <div class="grid justify-items-start mb-5">
                    </div>
                    <div class="grid justify-items-end">
                        <div class="pt-2 relative text-gray-600">
                            <input
                                class="border-2 border-gray-300 bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none"
                                type="search" name="search" placeholder="Search">
                            <button type="submit" class="absolute right-0 top-0 mt-5 mr-4">
                                <svg class="text-gray-600 h-4 w-4 fill-current" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1"
                                    x="0px" y="0px" viewBox="0 0 56.966 56.966"
                                    style="enable-background:new 0 0 56.966 56.966;" xml:space="preserve" width="512px"
                                    height="512px">
                                    <path
                                        d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>

                @forelse ($success as $foo)
                    @php
                        $pengajuan = App\Models\Pengajuan::where('id', $foo->pengajuan_id)->first();
                        $produk = App\Models\Product::with('productdetail')
                            ->where('id', $pengajuan->product_id)
                            ->first();
                        // dd($produk);
                        $seller = App\Models\User::where('id', $pengajuan->toko_id)->first();
                    @endphp
                    <div class="bg-kamtuu-produk-warning bg-opacity-25 p-3 rounded-md border border-gray-400 my-5">
                        <div class="grid grid-cols-12">
                            {{-- Cols 2 --}}
                            <div class="col-span-2 m-5">
                                <img class="w-28 h-28 rounded-md border border-gray-300"
                                    src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                    alt="">
                            </div>
                            {{-- Cols 3 --}}
                            <div class="m-5 col-start-3 col-span-5">
                                <p>Activity</p>
                                <p class="text-lg font-bold">{{ $produk->product_name }}</p>
                            </div>
                            {{-- Cols 4 --}}
                            <div class="col-span-4">
                                <p style="font-size: 14px">Status: {{ $foo->status }}</p>
                                <p>Tanggal Pengajuan:
                                    {{ Carbon\Carbon::parse($foo->tgl_pengajuan)->translatedFormat('d F Y') }}</p>
                                <p>Dari: {{ $seller->first_name }}</p>
                                <p class="text-blue-600 text-2xl">IDR {{ number_format($foo->nominal) }}</p>
                            </div>
                        </div>
                    </div>
                @empty
                @endforelse
            </div>
        </div>
    </div>

</body>

</html>
