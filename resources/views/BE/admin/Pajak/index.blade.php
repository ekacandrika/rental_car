<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    {{--summernote--}}
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    {{--    tag input bootstrap--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
          rel="stylesheet"/>
    {{--    select2--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"/>
    {{--jquery slim js--}}
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    {{--  summernote js  --}}
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    {{--  ajax  --}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>--}}
    {{--  select2 js  --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #dropHide {
            appearance: none;
            padding: 5px;
            color: white;
            border: none;
            font-family: inherit;
            outline: none;
            background-image:none;
        }
        #idn:checked + #idn {
            display: block;
        }

        #sett:checked + #sett {
            display: block;
        }

        .bootstrap-tagsinput .tag {
            margin-right: 2px;
            color: #ffffff;
            background: #2196f3;
            padding: 3px 7px;
            border-radius: 3px;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
{{-- Navbar --}}
<x-be.admin.navbar-admin></x-be.admin.navbar-admin>

<div class="grid grid-cols-10">
    <x-be.admin.sidebar-admin>
    </x-be.admin.sidebar-admin>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10 h-full">

            <div class=" p-5 border rounded-md bg-white">
                <x-flash-message></x-flash-message>

                <p class="text-xl font-semibold">Pajak Global</p>
                <form method="POST" action="{{ route('pajak.update') }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    {{--                    Komisi --}}
                    <div class="p-6">
                        <input type="hidden" value="1" name="user_id">
                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Komisi</label>
                        <input value="{{$pajakAdmin->komisi ?? 5}}" name="komisi" type="text" id="text"
                               class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                               placeholder="Komisi 15%">
                        @if ($errors->has('komisi'))
                            <span class="text-danger">{{ $errors->first('komisi') }}</span>
                        @endif
                    </div>

                    {{--                    Markup --}}
                    <div class="p-6">

                        <input type="hidden" value="1" name="pajak_id">
                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Markup</label>
                        <input value="{{$pajakAdmin->markup ?? 5}}" name="markup" type="text" id="text"
                               class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                               placeholder="Komisi 15%">
                        @if ($errors->has('markup'))
                            <span class="text-danger">{{ $errors->first('markup') }}</span>
                        @endif
                    </div>

                    {{--                    Komisi Agent --}}
                    <div class="p-6">

                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Komisi Agen</label>
                        <input value="{{$pajakAdmin->komisiAgent ?? 5}}" name="komisiAgent" type="text" id="text"
                               class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                               placeholder="Komisi 15%">
                        @if ($errors->has('komisiAgent'))
                            <span class="text-danger">{{ $errors->first('komisiAgent') }}</span>
                        @endif
                    </div>

                    {{--                    Komisi Corporate --}}
                    <div class="p-6">

                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Komisi Corporate</label>
                        <input value="{{$pajakAdmin->komisiCorporate ?? 5}}" name="komisiCorporate" type="text" id="text"
                               class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                               placeholder="Komisi 15%">
                        @if ($errors->has('komisiCorporate'))
                            <span class="text-danger">{{ $errors->first('komisiCorporate') }}</span>
                        @endif
                    </div>


                    <div class="p-6">
                        <button class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2">Update</button>
                    </div>
                </form>
            </div>

            <div class=" p-5 border rounded-md bg-white my-5">
                <p class="text-xl font-semibold">Pajak Local</p>
                <form method="POST" action="{{ route('pajak.local.store') }}" enctype="multipart/form-data">
                    @csrf

                    {{--                    Markup Local --}}
                    <div class="p-6">
                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Markup Local</label>
                        <div class="flex">
                            <input name="markupLocal" type="text" id="text"
                                   class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                   placeholder="Komisi 15% | 10.000">
                            <select class="w-10 bg-kamtuu-primary rounded mx-2"  name="statusMarkupLocal" id="dropHide">
                                <option  value="percent">%</option>
                                <option value="rupiah">Rp</option>
                            </select>
                        </div>
                        <p class="text-gray-400 text-sm">*Apabila Ingin Mengisi Komisi Untuk Agent Maka Tidak Perlu Mengisi Ini</p>
                        @if ($errors->has('markupLocal'))
                            <p>
                                <span class="text-red-300 text-sm">{{ $errors->first('markupLocal') }}</span>
                            </p>
                        @endif
                    </div>

                    {{--                    Markup Local Agent --}}
                    <div class="p-6">
                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Markup Agent Local</label>
                        <div class="flex">
                            <input name="komisiLocalAgent" type="text" id="text"
                                   class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                   placeholder="Markup 15% | 10.000">
                            <select class="w-10 bg-kamtuu-primary rounded mx-2"  name="statusKomisiLocalAgent" id="dropHide">
                                <option  value="percent">%</option>
                                <option value="rupiah">Rp</option>
                            </select>
                        </div>
                        <p class="text-gray-400 text-sm">*Apabila Ingin Mengisi Komisi Untuk Agent Maka Harus Mengisi Ini</p>
                        @if ($errors->has('komisiLocalAgent'))
                            <p>
                                <span class="text-red-300 text-sm">{{ $errors->first('komisiLocalAgent') }}</span>
                            </p>
                        @endif
                    </div>

                    {{--                    Markup Local Corporate --}}
                    <div class="p-6">
                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Markup Corporate Local</label>
                        <div class="flex">
                            <input name="komisiLocalCorporate" type="text" id="text"
                                   class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                   placeholder="Markup 15% | 10.000">
                            <select class="w-10 bg-kamtuu-primary rounded mx-2"  name="statusKomisiLocalCorporate" id="dropHide">
                                <option  value="percent">%</option>
                                <option value="rupiah">Rp</option>
                            </select>
                        </div>
                        <p class="text-gray-400 text-sm">*Apabila Ingin Mengisi Komisi Untuk Agent Maka Harus Mengisi Ini</p>
                        @if ($errors->has('komisiLocalCorporate'))
                            <p>
                                <span class="text-red-300 text-sm">{{ $errors->first('komisiLocalCorporate') }}</span>
                            </p>   
                        @endif
                    </div>

                    <div class="form-group p-6">
                        <strong>Seller :</strong>
                        <input type="hidden" value="" name="user_id" id="valueUser">
                        <select id='myselect' name="select2"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                            >
                            <option value="">Select An Option</option>
                            @foreach($user as $usr)
                                <option value="{{$usr->id}}">Nama : {{$usr->first_name}} | ID :{{$usr->id}} | Role :{{$usr->role}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('user_id'))
                            <p>
                                <span class="text-red-300 text-sm">{{ $errors->first('user_id') }}</span>
                            </p>
                        @endif
                    </div>

                    {{--                    Komisi Local --}}
                    <div class="p-6">
                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Komisi Local</label>
                        <div class="flex">
                            <input name="komisiLocal" type="text" id="text"
                                   class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                   placeholder="Komisi 15% | 10.000">
                            <select class="w-10 bg-kamtuu-primary rounded mx-2"  name="statusKomisiLocal" id="dropHide">
                                <option  value="percent">%</option>
                                <option value="rupiah">Rp</option>
                            </select>
                        </div>
                        <p class="text-gray-400 text-sm">*Apabila Ingin Mengisi Komisi Untuk Agent Maka Tidak Perlu Mengisi Ini</p>
                        @if ($errors->has('komisiLocal'))
                            <p>
                                <span class="text-red-300 text-sm">{{ $errors->first('komisiLocal') }}</span>
                            </p>
                        @endif
                    </div>


                    <div class="form-group p-6">
                        <strong>Produk Tipe :</strong>
                        <input type="hidden" value="" name="produk_type" id="valueProduk">
                        <select id='myselect2' name="select2"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                            >
                            <option value="">Select An Option</option>
                            @foreach($produk as $prod)
                                <option value="{{$prod->type}}">{{$prod->type}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('produk_type'))
                            <p>
                                <span class="text-red-300 text-sm">{{ $errors->first('produk_type') }}</span>
                            </p>
                        @endif
                    </div>
                    <div class="p-6">
                        <button class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2">Simpan</button>
                    </div>

                </form>

                <hr>

                <p class="text-4xl font-semibold my-5">List Markup Local</p>
                <table class="table-auto w-full text-center border-separate border border-slate-500">
                    <thead>
                    <tr>
                        <th class="border border-slate-600">No</th>
                        <th class="border border-slate-600">Nama User</th>
                        <th class="border border-slate-600">Role</th>
                        <th class="border border-slate-600">Markup Local</th>
                        <th class="border border-slate-600">Komisi Local</th>
                        <th class="border border-slate-600">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pajakLocals as $key=>$pajakLocal)
                        <tr>
                            <td class="border border-slate-600">{{$key+1}}</td>
                            <td class="border border-slate-600">{{$pajakLocal->first_name}}</td>
                            <td class="border border-slate-600">{{$pajakLocal->role}}</td>
                            <td class="border border-slate-600">{{$pajakLocal->markupLocal}}%</td>
                            <td class="border border-slate-600">{{$pajakLocal->komisiLocal}}%</td>
                            <td class="border border-slate-600">
                                <form action="{{ route('pajak.local.destroy',$pajakLocal->id) }}" method="POST">
                                    <a class="btn btn-primary"
                                       href="{{ route('pajak.local.edit',$pajakLocal->id) }}">Edit</a>
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-accent">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{--                {{ $provinsiDestindonesias->links() }}--}}
            </div>

        </div>
    </div>
</div>


@livewireScripts
<script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>--}}
<script type="text/javascript">
    $('#myselect').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    })
    $('#myselect').on("change", function () {
        var data = $("#myselect").val()
        document.getElementById('valueUser').setAttribute('value', data);
        console.log($("#myselect").val())
    })

    $('#myselect2').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    })
    $('#myselect2').on("change", function () {
        var data = $("#myselect2").val()
        document.getElementById('valueProduk').setAttribute('value', data);
        console.log($("#myselect2").val())
    })

</script>

<script>
    $(document).ready(function () {
        $("#desti").on('change', function () {
            $(".destindonesia").hide();
            $("#" + $(this).val()).fadeIn(700);
        }).change();
    });
</script>

</body>
</html>
