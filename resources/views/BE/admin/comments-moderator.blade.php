<!DOCTYPE html>
<html lang="{{str_replace('_','-',app()->getLocale())}}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">

        <title>{{config('app.name','Laravel')}}</title>

        {{-- Font --}}
        <link rel="stylesheet" href="https://fonts.bunny.net/css/base/jquery.fonticonpicker.min.css">

        {{-- base | always include--}}
            <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.1.1/dist/css/base/jquery.fonticonpicker.min.css">

        <!-- default grey-theme -->
        <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.0.0-alpha.0/dist/css/themes/grey-theme/jquery.fonticonpicker.grey.min.css">

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript"
        src="https://unpkg.com/@fonticonpicker/fonticonpicker/dist/js/jquery.fonticonpicker.min.js"></script>

        {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}
    
        <!-- Select 2 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>

        {{-- sweet alert --}}
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.all.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.min.css" rel="stylesheet">

        {{-- data tables --}}
        <style>
            @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

            /**
            * tailwind.config.js
            * module.exports = {
            *   variants: {
            *     extend: {
            *       backgroundColor: ['active'],
            *     }
            *   },
            * }
            */
            .active\:bg-gray-50:active {
                --tw-bg-opacity: 1;
                background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
            }

            #tur:checked+#tur {
                display: block;
            }

            #transfer:checked+#transfer {
                display: block;
            }

            #hotel:checked+#hotel {
                display: block;
            }

            #rental:checked+#rental {
                display: block;
            }

            #activity:checked+#activity {
                display: block;
            }

            #xstay:checked+#xstay {
                display: block;
            }

            #idn:checked+#idn {
                display: block;
            }

            #sett:checked+#sett {
                display: block;
            }

            .destindonesia {
                display: none;
            }
            .select2-container .select2-selection--single {
                box-sizing: border-box;
                cursor: pointer;
                display: block;
                height: 48px;
                user-select: none;
                -webkit-user-select: none;
            }
            .select2-container--default .select2-selection--single .select2-selection__rendered {
                color: #444;
                line-height: 46px;
            }
            .select2-container--default .select2-selection--single .select2-selection__arrow {
                height: 26px;
                position: absolute;
                top: 11px;
                right: 13px;
                width: 20px;
            }
            input:checked ~ .dot {
               transform: translateX(100%);
                /* background-color: #132b50; */
            }
        </style>
    </head>
    <body class="bg-[#F2F2F2] font-inter">
        {{-- Navbar --}}
        <x-be.admin.navbar-admin></x-be.admin.navbar-admin>

        {{-- Sidebar --}}
        <div class="grid grid-cols-10">
            <x-be.admin.sidebar-admin></x-be.admin.sidebar-admin>
            <div class="col-start-3 col-end-11 z-0">
                <div class="p-5 pl-10 pr-10">
                    <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                        <div x-data="reviews()">
                            <div class="grid grid-cols-2">
                                <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Moderasi Ulasan</div>
                            </div>
                            <div class="flex items-center col-start-4 col-span-1 px-5 mb-3">
                                <label for="simple-search" class="sr-only">Search</label>
                                <div class="relative w-1/4">
                                    <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                        <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400"
                                            fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                                                clip-rule="evenodd"></path>
                                        </svg>
                                    </div>
                                    <input type="text" id="simple-search"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  
                                    dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Search" @keyup="search()">
                                </div>
                            </div>
                            <div class="block px-5 mb-6">
                                <template x-if="deleted_arr.length >= 1">
                                    <button 
                                    x-transition:enter="transition ease-out duration-300"
                                    x-transition:enter-start="opacity-0 scale-90"
                                    x-transition:enter-end="opacity-100 scale-100"
                                    x-transition:leave="transition ease-in duration-300"
                                    x-transition:leave-start="opacity-100 scale-100"
                                    x-transition:leave-end="opacity-0 scale-90"
                                    type="button" class="float-left mx-2 my-3 bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]" style="background:#D50006" @click="multipleDelete()">Hapus</button>
                                </template>
                            </div>
                            <div class="block px-5 mb-6">
                                <table class="w-full text-sm text-[#333333]">
                                    <thead class="text-md text-white bg-[#D25889]">
                                        <tr>
                                            <th class="py-3 px-6">Nama</th>
                                            <th class="py-3 px-6">Ulasan</th>
                                            <th class="py-3 px-6">Status</th>
                                            <th class="py-3 px-6">Aksi</th>
                                            {{-- <th class="py-3 px-6">Hapus</th> --}}
                                        </tr>
                                    </thead>
                                   <tbody>
                                   <template x-if="datas.length >= 1">
                                        <template x-for="(data, index) in datas">
                                            <tr>
                                                <td class="px-6 py-2" style="width: 118px;">
                                                    <div class="flex justify-start" style="width: 118px;">
                                                        <p>
                                                            <span>
                                                                <form>
                                                                    <input type="checkbox" class="loat-right mx-2" name="checked-button[]" @change="multiple(data.id)" :value="data.id" :id="'select-'+data.id">
                                                                </form>
                                                            </span>
                                                            <span x-text="data.first_name"></span>
                                                        </p>
                                                    </div>
                                                </td>
                                                <td class="px-6 py-2" style="width: 461px;">
                                                    <div class="flex justify-start" style="width: 461px;" x-text="data.comments"></div>
                                                </td>
                                                <td class="px-6 py-2">
                                                    <div class="flex justify-start" x-text="data.status">
                                                    
                                                    </div>
                                                </td>
                                                <td class="px-6 py-2">
                                                    <div class="flex justify-start">
                                                        <div>
                                                            <div class="flex flex-row justify-between toggle">
                                                                <template x-if="data.status == 'active'">
                                                                    <label :for="'status-toggle'+index" class="flex items-center cursor-pointer">
                                                                        <div class="mr-3 text-gray-900 font-sm">Status</div>
                                                                        <div class="relative">
                                                                            <input type="checkbox" name="status-toggle" :id="'status-toggle'+index" class="checkbox hidden" @change="statusChange('status-toggle'+index,data.id)" checked/>
                                                                            <div class="block border border-gray-900 w-14 h-8 rounded-full"></div>
                                                                            <div class="dot absolute left-1 top-1 bg-gray-800 w-6 h-6 rounded-full transition"></div>
                                                                        </div>
                                                                    </label>
                                                                </template>
                                                                <template x-if="data.status == 'deactive'">
                                                                    <label :for="'status-toggle'+index" class="flex items-center cursor-pointer">
                                                                        <div class="mr-3 text-gray-900 font-sm">Status</div>
                                                                        <div class="relative">
                                                                            <input type="checkbox" name="status-toggle" :id="'status-toggle'+index" class="checkbox hidden" @change="statusChange('status-toggle'+index,data.id)"/>
                                                                            <div class="block border border-gray-900 w-14 h-8 rounded-full"></div>
                                                                            <div class="dot absolute left-1 top-1 bg-gray-800 w-6 h-6 rounded-full transition"></div>
                                                                        </div>
                                                                    </label>
                                                                </template>
                                                                
                                                            <div>
                                                        <div>
                                                        <div>
                                                            <button type="button" class="float-right mx-2 bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]" style="background:#D50006" :id="'btn-delete'+index" @click="deleteBtn(data.id)">Hapus</button>
                                                        </div>
                                                    </div>
                                                </td>
                                                {{-- <td class="px-6 py-2">
                                                    <div class="flex justify-start">
                                                        <div>
                                                            <button type="button" class="float-right mx-2 bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]" style="background:#D50006" :id="'btn-delete'+index" @click="deleteBtn(data.id)">Hapus</button>
                                                        </div>
                                                    </div>
                                                </td>     --}}
                                            <tr>
                                        </template>
                                   </template>
                                   <template x-if="datas.length == 0">
                                        <tr>
                                            <td class="py-3 px-6 text-center text-2xl font-bold" colspan="3">Titik tidak ditemukan atau data belum ada</td>
                                        </tr>
                                   </template>
                                   </tbody>  
                                </table>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
        let data_url = "{{route('review.datas')}}";
        let search_url = "{{route('review.search')}}";
        
        document.addEventListener("alpine:init",()=>{
            Alpine.data("reviews",()=>({
                datas:[],
                links:[],
                url:data_url,
                total_page:'',
                active:'',
                current_page:null,
                cur_page:null,
                deleted_arr:[],
                async init(){
                    let loads = await fetch(`${this.url}`);
                    let objs  = await loads.json();

                    console.log(objs)
                    this.datas = objs.datas.data;
                    this.links = objs.datas.links;

                    this.total_page = objs.datas.total;
                    this.cur_page = objs.datas.current_page;
                },
                search(){
                    var cari   = $("#simple-search").val();                   
                    let page = '&page=1';

                    if(cari.length > 0){
                        page = '&page='+this.cur_page;
                    }
                    
                    console.log(page)

                    fetch(`${search_url}?cari=${cari}`).
                    then(response =>response.json())
                    .then(data=>{
                        this.datas=data.datas.data
                        this.links=data.datas.links
                    })
                },
                opened(index){
                    var parent =".parent_"+index;
                    var name =".child-"+index;

                    $(name).toggle()
                },
                getPage(page){
                    let arr = [];

                    if(page){
                        this.current_page = page;
                    }else{
                        this.current_page = '/?page=1';
                    }

                    fetch(`${this.url}${this.current_page}`)
                    .then(resp=>resp.json())
                    .then(data=>{
                        let obj = data.datas.data
                        this.cur_page = data.datas.current_page

                        if(obj.length != undefined){
                            this.datas = obj
                        }else{
                            Object.keys(obj).forEach(key=>{
                                arr.push(obj[key])
                            })

                            this.datas = arr
                        }
                    })
                },
                deleteBtn(valueId){
                    // alert('delete')
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type:'DELETE',
                        url:"ulasan/"+valueId,
                        cache:false,
                        dataType:'json',
                        data:{
                            id:this.valueId
                        },
                        success:function(resp){
                            console.log(resp)
                            
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                didOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: resp.status,
                                title: resp.message
                            }).then(function(){
                                location.reload();
                            })
                        },
                        error:function(data){
                            console.log('error:',data);
                        }
                    })
                },
                statusChange(id,value){

                    let update_url = "ulasan/"+value
                    let checked = $("#"+id).is(':checked');
                    let status  = null;

                    if(checked){
                        status = 'active'
                    }

                    if(!checked){
                        status = 'deactive'
                    }
                    
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type:'PUT',
                        url:update_url,
                        cache:false,
                        dataType:'json',
                        data:{
                            status,
                        },
                        success:function(resp){
                            console.log(resp)
                            
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                didOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: resp.status,
                                title: resp.message
                            }).then(function(){
                                location.reload();
                            })
                            
                        },
                        error:function(data){

                        }
                    })
                    
                },
                multiple(id){
                    var element_id = document.getElementById('select-'+id);
                    var value = element_id.value;

                    if(element_id.checked){
                        this.deleted_arr.push({
                            id:element_id.value
                        })
                    }

                    if(!element_id.checked){
                        var deleted = this.deleted_arr.filter(el=> el.id != element_id.value);
                        this.deleted_arr = deleted;
                    }
                    
                },
                multipleDelete(){

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type:'DELETE',
                        url:"{{route('review.multiple.delete')}}",
                        cache:false,
                        dataType:'json',
                        data:{
                            deleted_id:this.deleted_arr
                        },
                        success:function(resp){
                            console.log(resp)
                            
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                didOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: resp.status,
                                title: resp.message
                            }).then(function(){
                                location.reload();
                            })


                        },
                        error:function(data){
                            console.log('error:',data);
                        }
                    })
                }
            }))
        })
    </script> 
</html>    