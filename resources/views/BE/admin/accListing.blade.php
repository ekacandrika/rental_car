<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->

    <style>
        /* CHECKBOX TOGGLE SWITCH */
        /* @apply rules for documentation, these do not work as inline style */
        .switch {
            position: relative;
            display: inline-block;
            width: 100px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #23AEC1;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #9E3D64;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #23AEC1;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(68px);
            -ms-transform: translateX(68px);
            transform: translateX(68px);
        }

        .on {
            display: none;
        }

        .on,
        .off {
            color: white;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            font-size: 10px;
            font-family: Verdana, sans-serif;
        }

        input:checked+.slider .on {
            display: block;
        }

        input:checked+.slider .off {
            display: none;
        }

        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }

        .destindonesia {
            display: none;
        }
    </style>

    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.com.navbar></x-be.com.navbar>

    {{--sidebar--}}
    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin></x-be.admin.sidebar-admin>
        <div class="z-10 col-start-3 col-end-11">
            <div class="p-5 pl-10 pr-10">
                <span class="text-2xl font-bold font-inter text-[#333333]">Konfirmasi Listing Seller</span>
            </div>
            {{--search--}}
            <div class="w-1/2">
                <label for="simple-search" class="sr-only">Search</label>
                <form method="GET">
                    <div class="flex">
                        <div class="relative w-full">
                            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400"
                                    fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                                        clip-rule="evenodd"></path>
                                </svg>
                            </div>
                            <input type="text" name="search" value="{{ request()->get('search') }}"
                                placeholder="Search..." id="simple-search"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-kamtuu-second focus:border-kamtuu-second block w-full pl-10 p-2.5">
                        </div>


                        <button class="p-2 mx-5 text-white  bg-kamtuu-second rounded-xl" type="submit">Search</button>
                    </div>

                    <div class="p-5 mt-5 bg-white border-black rounded-lg shadow-lg border-1">
                        {{-- <form action="{{ route('listing.search') }}" method="get">--}}
                            <div class="grid grid-cols-3">
                                {{-- Kategori --}}
                                <div class="">
                                    <label for="type">Kategori:</label>
                                    <div>
                                        <input type="checkbox"
                                            class="checked:bg-kamtuu-second text-kamtuu-second border-kamtuu-second focus:ring-kamtuu-second"
                                            name="type[]" value="hotel" id="hotel" {{ in_array('hotel', old('type', []))
                                            ? 'checked' : '' }}>
                                        <label for="hotel">Hotel</label>
                                    </div>
                                    <div>
                                        <input type="checkbox"
                                            class="checked:bg-kamtuu-second text-kamtuu-second border-kamtuu-second focus:ring-kamtuu-second"
                                            name="type[]" value="tour" id="tour" {{ in_array('tour', old('type', []))
                                            ? 'checked' : '' }}>
                                        <label for="tour">Tur</label>
                                    </div>
                                    <div>
                                        <input type="checkbox"
                                            class="checked:bg-kamtuu-second text-kamtuu-second border-kamtuu-second focus:ring-kamtuu-second"
                                            name="type[]" value="xstay" id="xstay" {{ in_array('xstay', old('type', []))
                                            ? 'checked' : '' }}>
                                        <label for="xstay">Xstay</label>
                                    </div>
                                    <div>
                                        <input type="checkbox"
                                            class="checked:bg-kamtuu-second text-kamtuu-second border-kamtuu-second focus:ring-kamtuu-second"
                                            name="type[]" value="rental" id="rental" {{ in_array('rental', old('type',
                                            [])) ? 'checked' : '' }}>
                                        <label for="rental">Rental</label>
                                    </div>
                                    <div>
                                        <input type="checkbox"
                                            class="checked:bg-kamtuu-second text-kamtuu-second border-kamtuu-second focus:ring-kamtuu-second"
                                            name="type[]" value="transfer" id="transfer" {{ in_array('transfer',
                                            old('type', [])) ? 'checked' : '' }}>
                                        <label for="transfer">Transfer</label>
                                    </div>
                                    <div>
                                        <input type="checkbox"
                                            class="checked:bg-kamtuu-second text-kamtuu-second border-kamtuu-second focus:ring-kamtuu-second"
                                            name="type[]" value="activity" id="activity" {{ in_array('activity',
                                            old('type', [])) ? 'checked' : '' }}>
                                        <label for="activity">Activity</label>
                                    </div>
                                </div>

                                {{--Sub Kategori Tur--}}
                                <div class="">
                                    <label for="type">Sub Kategori Tur:</label>
                                    <div>
                                        <input type="checkbox"
                                            class="checked:bg-kamtuu-second text-kamtuu-second border-kamtuu-second focus:ring-kamtuu-second"
                                            name="type[]" value="Open Trip" id="Open Trip" {{ in_array('Open Trip',
                                            old('type', [])) ? 'checked' : '' }}>
                                        <label for="Open Trip">Open Trip</label>
                                    </div>
                                    <div>
                                        <input type="checkbox"
                                            class="checked:bg-kamtuu-second text-kamtuu-second border-kamtuu-second focus:ring-kamtuu-second"
                                            name="type[]" value="Tur Private" id="Tur Private" {{ in_array('Tur
                                            Private', old('type', [])) ? 'checked' : '' }}>
                                        <label for="Tur Private">Tur Private</label>
                                    </div>
                                </div>

                                {{--Approve Or Not--}}
                                <div class="">
                                    <label for="type">Status :</label>
                                    <div>
                                        <input type="checkbox"
                                            class="checked:bg-kamtuu-second text-kamtuu-second border-kamtuu-second focus:ring-kamtuu-second"
                                            name="type[]" value="waiting approve" id="waiting approve" {{
                                            in_array('waiting approve', old('type', [])) ? 'checked' : '' }}>
                                        <label for="waiting approve">Waiting Approve</label>
                                    </div>
                                    <div>
                                        <input type="checkbox"
                                            class="checked:bg-kamtuu-second text-kamtuu-second border-kamtuu-second focus:ring-kamtuu-second"
                                            name="type[]" value="publish" id="publish" {{ in_array('publish',
                                            old('type', [])) ? 'checked' : '' }}>
                                        <label for="publish">Approve</label>
                                    </div>
                                </div>
                            </div>


                            <button class="p-2 my-3 text-white rounded-lg bg-kamtuu-second"
                                type="submit">Filter</button>
                            {{--
                        </form>--}}
                    </div>
                </form>
            </div>
            {{-- <div class="grid grid-cols-2">--}}
                {{-- <div class="grid justify-items-start">--}}
                    {{-- <div class="inline-flex items-center p-2">--}}
                        {{-- <input class="checked:bg-kamtuu-second" type="checkbox" name="" id="">--}}
                        {{-- <p class="px-2">Pilih Semua</p>--}}
                        {{-- <div class="w-[100px] mx-5">--}}
                            {{-- <select--}} {{--
                                class="px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-[100px] rounded-md text-sm focus:ring-1">
                                --}}
                                {{-- <option>Aktif</option>--}}
                                {{-- <option>Tidak Aktif</option>--}}
                                {{-- </select>--}}
                                {{-- <div--}} {{--
                                    class="absolute inset-y-0 right-0 flex items-center px-2 text-gray-900 pointer-events-none">
                                    --}}
                                    {{-- </div>--}}
                        {{-- </div>--}}
                    {{-- </div>--}}
                {{-- </div>--}}
            {{-- <div class="grid justify-end">--}}
                {{-- <div class="py-2">--}}
                    {{-- <select--}} {{--
                        class="px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-[250px] rounded-md text-sm focus:ring-1">
                        --}}
                        {{-- <option>-- Kategori --</option>--}}
                        {{-- <option>Tur</option>--}}
                        {{-- <option>Transfer</option>--}}
                        {{-- <option>Rental</option>--}}
                        {{-- <option>Hotel</option>--}}
                        {{-- <option>XStay</option>--}}
                        {{-- <option>Activity</option>--}}
                        {{-- </select>--}}
                        {{-- <div--}} {{--
                            class="absolute inset-y-0 right-0 flex items-center px-2 text-gray-900 pointer-events-none">
                            --}}
                            {{-- </div>--}}
                {{-- </div>--}}
            {{--
        </div>--}}
        {{--
    </div>--}}

    <x-flash-message></x-flash-message>
    @forelse ($data_listing as $key=>$item)
    <div class="my-5 bg-white rounded-md shadow-lg">
        <div class="grid grid-cols-12">
            {{-- Cols 1 --}}
            {{-- <div class="grid place-content-center">--}}
                {{-- <input class="checked:bg-kamtuu-second" type="checkbox" name="" id="">--}}
                {{-- </div>--}}
            {{-- Cols 2 --}}
            <div class="col-span-2 m-5">
                <img src="{{$item->product_photo}}" class="w-[150px] h-[150px] inline-flex rounded-lg" alt="" title="">
            </div>
            {{-- Cols 3 --}}
            <div class="col-span-5 col-start-4 m-5">
                @if ($item->product->type == 'tour')
                <a href="{{route('tur.show', $item->product->slug)}}">
                    <p class="text-lg font-bold">{{ $item->product->product_name }}</p>
                </a>
                <div class="flex gap-1 py-2 lg:gap-2">
                    <div
                        class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        Tur
                    </div>
                </div>
                <div class="flex">
                    @if($item->tipe_tur != null)
                    <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" src="{{ asset('storage/icons/user-solid.svg') }}"
                        alt="rating" width="17px" height="17px">
                    <p class="font-bold text-[8px] lg:text-sm mr-2"> {{$item->tipe_tur}}</p>
                    @else
                    <p></p>
                    @endif
                    @if($item->makanan != null)
                    <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" src="{{ asset('storage/icons/utensils-solid.svg') }}"
                        alt="rating" width="17px" height="17px">
                    <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->makanan}}</p>
                    @else
                    @endif
                    @if($item->jml_hari != null AND $item->jml_malam != null)
                    <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" src="{{ asset('storage/icons/hourglass-solid.svg') }}"
                        alt="rating" width="17px" height="17px">
                    <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->jml_hari}}
                        D{{$item->jml_malam}}N</p>
                    @else
                    @endif
                </div>
                <div class="mt-10">
                    @if($item->available == 'draft' OR $item->available == 'waiting approve')
                    <p class="mt-8 text-red-600">Draft</p>
                    @endif
                    @if($item->available == 'publish' OR $item->availablle == 'approve')
                    <p class="text-green-600">Tersedia</p>
                    @endif
                </div>
                @endif
                @if ($item->product->type == 'hotel')
                <a href="{{route('hotel.show', $item->product->slug)}}">
                    <p class="text-lg font-bold">{{ $item->product->product_name }}</p>
                </a>
                <div class="flex gap-1 py-2 lg:gap-2">
                    <div
                        class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        Hotel
                    </div>
                </div>
                <div class="flex">
                    {{-- <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" --}} {{--
                        src="{{ asset('storage/icons/user-solid.svg') }}" alt="rating" width="17px" --}} {{--
                        height="17px">--}}
                    {{-- <p class="font-bold text-[8px] lg:text-sm mr-2"> {{$item->tipe_tur}}</p>--}}
                    @if($item->product->makanan != null)
                    <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" src="{{ asset('storage/icons/utensils-solid.svg') }}"
                        alt="rating" width="17px" height="17px">
                    <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->product->makanan}}</p>
                    @else

                    @endif
                    {{-- <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" --}} {{--
                        src="{{ asset('storage/icons/hourglass-solid.svg') }}" alt="rating" width="17px" --}} {{--
                        height="17px">--}}
                    {{-- <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->jml_hari}}D{{$item->jml_malam}}N</p>
                    --}}
                </div>
                <div class="mt-10">
                    @if($item->available == 'draft' OR $item->available == 'waiting approve')
                    <p class="mt-8 text-red-600">Draft</p>
                    @endif
                    @if($item->available == 'publish' OR $item->availablle == 'approve')
                    <p class="text-green-600">Tersedia</p>
                    @endif
                </div>
                @endif
                @if ($item->product->type == 'xstay')
                <a href="{{route('xstay.show', $item->product->slug)}}">
                    <p class="text-lg font-bold">{{ $item->product->product_name }}</p>
                </a>
                <div class="flex gap-1 py-2 lg:gap-2">
                    <div
                        class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        Xstay
                    </div>
                </div>
                <div class="flex">
                    {{-- <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" --}} {{--
                        src="{{ asset('storage/icons/user-solid.svg') }}" alt="rating" width="17px" --}} {{--
                        height="17px">--}}
                    {{-- <p class="font-bold text-[8px] lg:text-sm mr-2"> {{$item->tipe_tur}}</p>--}}
                    {{-- <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" --}} {{--
                        src="{{ asset('storage/icons/utensils-solid.svg') }}" alt="rating" width="17px" --}} {{--
                        height="17px">--}}
                    {{-- <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->makanan}}</p>--}}
                    {{-- <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" --}} {{--
                        src="{{ asset('storage/icons/hourglass-solid.svg') }}" alt="rating" width="17px" --}} {{--
                        height="17px">--}}
                    {{-- <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->jml_hari}}--}}
                        {{-- D{{$item->jml_malam}}N</p>--}}
                </div>
                <div class="mt-10">
                    @if($item->available == 'draft' OR $item->available == 'waiting approve')
                    <p class="mt-8 text-red-600">Draft</p>
                    @endif
                    @if($item->available == 'publish' OR $item->availablle == 'approve')
                    <p class="text-green-600">Tersedia</p>
                    @endif
                </div>
                @endif
                @if ($item->product->type == 'activity')
                <a href="{{route('activity.show', $item->product->slug)}}">
                    <p class="text-lg font-bold">{{ $item->product->product_name }}</p>
                </a>
                <div class="flex gap-1 py-2 lg:gap-2">
                    <div
                        class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        Activity
                    </div>
                </div>
                <div class="flex">
                    {{-- <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" --}} {{--
                        src="{{ asset('storage/icons/user-solid.svg') }}" alt="rating" width="17px" --}} {{--
                        height="17px">--}}
                    {{-- <p class="font-bold text-[8px] lg:text-sm mr-2"> {{$item->tipe_tur}}</p>--}}
                    {{-- <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" --}} {{--
                        src="{{ asset('storage/icons/utensils-solid.svg') }}" alt="rating" width="17px" --}} {{--
                        height="17px">--}}
                    {{-- <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->makanan}}</p>--}}
                    {{-- <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" --}} {{--
                        src="{{ asset('storage/icons/hourglass-solid.svg') }}" alt="rating" width="17px" --}} {{--
                        height="17px">--}}
                    {{-- <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->jml_hari}}--}}
                        {{-- D{{$item->jml_malam}}</p>--}}
                </div>
                <div class="mt-10">
                    @if($item->available == 'draft' OR $item->available == 'waiting approve')
                    <p class="mt-8 text-red-600">Draft</p>
                    @endif
                    @if($item->available == 'publish' OR $item->availablle == 'approve')
                    <p class="text-green-600">Tersedia</p>
                    @endif
                </div>
                @endif
                @if ($item->product->type == 'transfer')
                <a href="{{route('transfer.show', $item->product->slug)}}">
                    <p class="text-lg font-bold">{{ $item->product->product_name }}</p>
                </a>
                <div class="flex gap-1 py-2 lg:gap-2">
                    {{-- {{dd($item)}} --}}
                    <div
                        class="bg-[#9E3D64] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        Transfer
                    </div>
                    <div
                        class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        Sedan
                    </div>
                </div>
                <div class="flex">
                    <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}" alt="rating"
                        width="17px" height="17px">
                    <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                    <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}" alt="rating"
                        width="17px" height="17px">
                    <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                </div>
                <div class="mt-10">
                    @if($item->available == 'draft' OR $item->available == 'waiting approve')
                    <p class="mt-8 text-red-600">Draft</p>
                    @endif
                    @if($item->available == 'publish' OR $item->availablle == 'approve')
                    <p class="text-green-600">Tersedia</p>
                    @endif
                </div>
                @endif
                @if ($item->product->type == 'rental')
                <a href="{{route('rental.show', $item->product->slug)}}">
                    <p class="text-lg font-bold">{{ $item->product->product_name }}</p>
                </a>
                <div class="flex gap-1 py-2 lg:gap-2">
                    {{-- {{dd($item)}}--}}
                    <div
                        class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        {{$item->product->type}}
                    </div>
                    <div
                        class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        {{$item->detailKendaraan->jenismobil->jenis_kendaraan}}
                    </div>
                </div>
                <div class="flex">
                    <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}" alt="rating"
                        width="17px" height="17px">
                    <p class="font-bold text-[8px] lg:text-sm mr-2">{{$item->detailKendaraan->kapasitas_kursi}}
                        Kursi</p>
                    <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}" alt="rating"
                        width="17px" height="17px">
                    <p class="font-bold  text-[8px] lg:text-sm mr-2">{{$item->detailKendaraan->kapasitas_koper}}
                        Koper</p>
                </div>
                <div class="mt-10">
                    @if($item->available == 'draft' OR $item->available == 'waiting approve')
                    <p class="mt-8 text-red-600">Draft</p>
                    @endif
                    @if($item->available == 'publish' OR $item->availablle == 'approve')
                    <p class="text-green-600">Tersedia</p>
                    @endif
                </div>

                {{-- {{dd($item)}}--}}
                @endif
                {{-- <div class="flex items-center gap-2 pt-8">--}}
                    {{-- <p class="text-sm text-[#FFB800] font-semibold">Sedang Dipesan</p>--}}
                    {{-- <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" --}} {{--
                        src="{{ asset('storage/icons/circle-info-black.svg') }}" alt="rating" --}} {{-- width="17px"
                        height="17px">--}}
                    {{-- </div>--}}
            </div>
            {{-- Cols 4 --}}
            <div class="grid col-span-4 m-5 place-items-end">
                {{-- @if($item->available == 'publish')--}}
                {{-- <p class="px-5">Aktif</p>--}}
                {{-- <label class="switch">--}}
                    {{-- <input class="toggle-class" data-id="{{$item->id}}" type="checkbox" {{ $item->available ===
                    'publish' ? 'checked' : '' }}>--}}
                    {{-- <span class="slider round"></span>--}}
                    {{-- </label>--}}
                {{-- @elseif($item->available == 'waiting approve')--}}
                {{-- <p
                    class="bg-yellow-500 px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                    Waiting Approve</p>--}}
                {{-- @else--}}
                {{-- <p class="px-5">Tidak Aktif</p>--}}
                {{-- <label class="switch">--}}
                    {{-- <input class="toggle-class" data-id="{{$item->id}}" type="checkbox" {{ $item->available ===
                    'draft' ? 'checked' : '' }}>--}}
                    {{-- <span class="slider round"></span>--}}
                    {{-- </label>--}}
                {{-- @endif--}}

                <div class="flex items-center gap-2 mb-8 ">

                    {{-- <p class="font-semibold text-md">--}}
                        {{-- @if($item->available == 'publish')--}}
                        {{--
                    <p
                        class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        Publish</p>--}}
                    {{-- @endif--}}
                    {{-- @if($item->available == 'draft')--}}
                    {{-- <p
                        class="bg-red-500 px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        Draft</p>--}}
                    {{-- @endif--}}
                    {{-- @if($item->available == 'not approved')--}}
                    {{-- <p
                        class="bg-kamtuu-third px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        Not Approve</p>--}}
                    {{-- @endif--}}
                    {{-- @if($item->available == 'approved')--}}
                    {{-- <p
                        class="bg-green-500 px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        Approved</p>--}}
                    {{-- @endif--}}
                    {{-- @if($item->available == 'waiting approve')--}}
                    {{-- <p
                        class="bg-kamtuu-third px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        Waiting Approve</p>--}}
                    {{-- @endif--}}
                    {{-- </p>--}}
                </div>

                @if ($item->available != null)
                <form action="{{ route('accListingUpdate', $item->product_id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="flex items-center gap-2">
                        <div class="py-2">
                            <select name="available"
                                class="px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-[150px] rounded-md text-sm focus:ring-1">
                                <option value="">-- Pilih Status --</option>
                                @if($item->available == 'approve' OR $item->available == 'waiting approve' )
                                <option value="approve" {{ $item->available == 'approve' ? 'selected' : '' }}>
                                    Approve
                                </option>
                                <option value="waiting approve" {{ $item->available == 'waiting approve' ? 'selected' :
                                    '' }}>
                                    Waiting Approve
                                </option>
                                @elseif($item->available == 'publish' OR $item->available == 'draft' OR $item->available
                                == 'draft by admin')
                                <option value="publish" {{ $item->available == 'publish' ? 'selected' : '' }}>
                                    Publish
                                </option>
                                <option value="draft" {{ $item->available == 'draft' ? 'selected' : '' }}>
                                    Draft
                                </option>
                                <option value="draft by admin" {{ $item->available == 'draft by admin' ? 'selected' : ''
                                    }}>
                                    Draft By Admin
                                </option>
                                @endif
                            </select>
                            <div
                                class="absolute inset-y-0 right-0 flex items-center px-2 text-gray-900 pointer-events-none">
                            </div>
                        </div>
                        <button
                            class="items-center px-px py-1 lg:py-2 w-full lg:w-20 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">
                            Update
                        </button>
                    </div>
                </form>
                @endif
                <div class="grid-cols-1 place-items-center">
                    <div class="flex my-4">
                        <span class="mr-10">Ketersediaan</span>

                        {{-- <button name="checkIn" id="datepickerKetersediaan{{$key+1}}" placeholder=" " />--}}

                        @if (!empty($item->diskon->tgl_start) && !empty($item->diskon->tgl_end) &&
                        !isset($item->diskon))
                        @foreach (json_decode($item->diskon->tgl_start) as $key => $startDate)
                        @php
                        $endDate = json_decode($item->diskon->tgl_end)[$key];
                        @endphp
                        <div class="relative order-last block my-3 sm:order-none sm:my-0">
                            {{-- <div
                                class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                --}}
                                <button placeholder="Pilih tanggal" type="text" id="tur_date_{{$item->id}}_{{$key}}"
                                    class="'!m-0 sm:!w-full'" name="tur_date" />
                                {{--
                            </div>--}}
                        </div>

                        <script>
                            $(document).ready(function () {
                                                var startDate = new Date("{{ $startDate }}");
                                                var endDate = new Date("{{ $endDate }}");
                                                $('#tur_date_{{$item->id}}_{{$key}}').datepicker({
                                                    showOtherMonths: true,
                                                    selectOtherMonths: true,
                                                    iconsLibrary: 'fontawesome',
                                                    minDate: startDate,
                                                    maxDate: endDate
                                                });
                                            });
                        </script>
                        @endforeach
                        @else
                        <div class="relative order-last block my-3 sm:order-none sm:my-0">
                            {{-- <div
                                class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm">
                                --}}
                                <button placeholder="Pilih tanggal" type="text" id="tur_date" class="'!m-0 sm:!w-full'"
                                    name="tur_date" disabled />
                                {{--
                            </div>--}}
                        </div>

                        <script>
                            $(document).ready(function () {
                                            today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                                            $('#tur_date').datepicker({
                                                minDate: today
                                            });
                                        })
                        </script>
                        @endif
                    </div>
                    {{-- @if ($item->product->type == 'hotel')--}}
                    {{-- <a href="{{ route('hotel.edit', $item->id) }}">--}}
                        {{-- <button type="submit" --}} {{--
                            class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">--}}
                            {{-- Edit--}}
                            {{-- </button>--}}
                        {{-- </a>--}}
                    {{-- <a>--}}
                        {{-- <button type="submit" --}} {{--
                            class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">--}}
                            {{-- Hapus--}}
                            {{-- </button>--}}
                        {{-- </a>--}}
                    {{-- @endif--}}
                    {{-- @if ($item->product->type == 'xstay')--}}
                    {{-- <a href="{{ route('infodasar.edit', $item->id) }}">--}}
                        {{-- <button type="submit" --}} {{--
                            class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">--}}
                            {{-- Edit--}}
                            {{-- </button>--}}
                        {{-- </a>--}}
                    {{-- <a>--}}
                        {{-- <button type="submit" --}} {{--
                            class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">--}}
                            {{-- Hapus--}}
                            {{-- </button>--}}
                        {{-- </a>--}}
                    {{-- @endif--}}
                    {{-- @if ($item->product->type == 'tour')--}}
                    {{-- <a href="{{ route('tur.viewAddListingEdit', $item->product->id) }}">--}}
                        {{-- <button type="submit" --}} {{--
                            class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">--}}
                            {{-- Edit--}}
                            {{-- </button>--}}
                        {{-- </a>--}}
                    {{-- <a>--}}
                        {{-- <button type="submit" --}} {{--
                            class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">--}}
                            {{-- Hapus--}}
                            {{-- </button>--}}
                        {{-- </a>--}}
                    {{-- @endif--}}
                    {{-- @if ($item->product->type == 'activity')--}}
                    {{-- <a href="{{ route('activity.viewAddListingEdit', $item->product->id) }}">--}}
                        {{-- <button type="submit" --}} {{--
                            class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">--}}
                            {{-- Edit--}}
                            {{-- </button>--}}
                        {{-- </a>--}}
                    {{-- <a>--}}
                        {{-- <button type="submit" --}} {{--
                            class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">--}}
                            {{-- Hapus--}}
                            {{-- </button>--}}
                        {{-- </a>--}}
                    {{-- @endif--}}
                    {{-- @if ($item->product->type == 'rental')--}}
                    {{-- <a href="{{ route('rental.viewAddListingEdit', $item->product->id) }}">--}}
                        {{-- <button type="submit" --}} {{--
                            class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">--}}
                            {{-- Edit--}}
                            {{-- </button>--}}
                        {{-- </a>--}}
                    {{-- <a>--}}
                        {{-- <button type="submit" --}} {{--
                            class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">--}}
                            {{-- Hapus--}}
                            {{-- </button>--}}
                        {{-- </a>--}}
                    {{-- @endif--}}
                    {{-- @if ($item->product->type == 'transfer')--}}
                    {{-- <a href="{{ route('transfer.viewAddListingEdit', $item->product->id) }}">--}}
                        {{-- <button type="submit" --}} {{--
                            class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">--}}
                            {{-- Edit--}}
                            {{-- </button>--}}
                        {{-- </a>--}}
                    {{-- <a>--}}
                        {{-- <button type="submit" --}} {{--
                            class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">--}}
                            {{-- Hapus--}}
                            {{-- </button>--}}
                        {{-- </a>--}}
                    {{-- @endif--}}
                </div>
            </div>
        </div>
    </div>

    @empty
    @endforelse
    {{ $data_listing->links() }}



    {{-- <div class="my-5 bg-white rounded-md shadow-lg">
        <div class="grid grid-cols-12">
            <div class="grid place-content-center">
                <input class="checked:bg-kamtuu-second" type="checkbox" name="" id="">
            </div>
            <div class="col-span-2 m-5">
                <img src="https://via.placeholder.com/100x100" class="w-[150px] h-[150px] inline-flex rounded-lg" alt=""
                    title="">
            </div>
            <div class="col-span-5 col-start-4 m-5">
                <p class="text-lg font-bold">Nama Kendaraan</p>
                <div class="flex gap-1 py-2 lg:gap-2">
                    <div
                        class="bg-[#9E3D64] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        Transfer</div>
                    <div
                        class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                        Sedan</div>
                </div>
                <div class="flex">
                    <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}" alt="rating"
                        width="17px" height="17px">
                    <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                    <img class="w-3 h-3 mr-1 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}" alt="rating"
                        width="17px" height="17px">
                    <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                </div>
                <div class="flex items-center gap-2 pt-8">
                    <p class="text-sm text-[#51B449] font-semibold">Tersedia</p>
                </div>
            </div>
            <div class="col-span-4 m-5">
                <p class="px-5">Aktif</p>
                <div class="flex items-center gap-2 pt-8">
                    <p class="font-semibold text-md">Ketersediaan</p>
                    <button type="submit"
                        class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">Edit</button>
                    <button type="submit"
                        class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">Hapus</button>
                </div>
            </div>
        </div>
    </div> --}}
    </div>
    </div>
    </div>


    @livewireScripts

    {{--Calendar--}}
    <script>
        {{--    @if(!empty($diskon['tgl_start'][0]) && !empty($diskon['tgl_end'][0]))--}}
    {{--    $(document).ready(function() {--}}
    {{--        var startDate = new Date("{{ $start }}");--}}
    {{--        var endDate = new Date("{{ $end }}");--}}
    {{--        $('#tur_date').datepicker({--}}
    {{--            showOtherMonths: true,--}}
    {{--            selectOtherMonths: true,--}}
    {{--            iconsLibrary: 'fontawesome',--}}
    {{--            minDate: startDate,--}}
    {{--            maxDate: endDate--}}
    {{--        });--}}
    {{--    })--}}
    {{--    @else--}}
    {{--    $(document).ready(function() {--}}
    {{--        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());--}}
    {{--        $('#tur_date').datepicker({--}}
    {{--            minDate: today--}}
    {{--        });--}}
    {{--    })--}}
    {{--    @endif--}}

    // Slide 1
    // var today, datepicker;
    // today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    // datepicker = $('#datepickerKetersediaan1').datepicker({
    //     minDate: today
    // });
    // Slide 2
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepickerKetersediaan2').datepicker({
        minDate: today
    });
    // Slide 3
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepickerKetersediaan3').datepicker({
        minDate: today
    });
    // Slide 4
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepickerKetersediaan4').datepicker({
        minDate: today
    });
    // Slide 5
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepickerKetersediaan5').datepicker({
        minDate: today
    });
    </script>

    {{--Switch On Off--}}
    <script>
        $(function () {
        $('.toggle-class').change(function () {
            var status = $(this).prop('checked') == true ? 'publish' : 'draft';
            var id_product = $(this).data('id');

            $.ajax({
                type: "GET",
                dataType: "json",
                url: '/dahsboard/mylisting/update/status',
                data: {'available': status, 'id': id_product},
                success: function (data) {
                    console.log(data.success)
                }
            });
            setTimeout(() => {
                document.location.reload();
            }, 500);
        })
    })
    </script>

    {{--Display Image--}}
    <script>
        function displayImage() {

        return {
            imageUrl: '',

            selectedFile(event) {
                this.fileToUrl(event, src => this.imageUrl = src)
            },

            fileToUrl(event, callback) {
                if (!event.target.files.length) return

                let file = event.target.files[0],
                    reader = new FileReader()

                reader.readAsDataURL(file)
                reader.onload = e => callback(e.target.result)
            },
        }
    }
    </script>
</body>

</html>
