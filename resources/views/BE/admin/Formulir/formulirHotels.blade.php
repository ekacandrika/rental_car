<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Destindonesia') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles

    <style>
    </style>

</head>

<body class="bg-white font-Inter">

<header>
    <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
</header>

<div class="max-w-7xl bg-[#F2F2F2] mx-auto rounded-md m-5">
    <div class="grid p-5 justify-items-center">
        <h1 class="font-semibold text-[26px]">Formulir Custom Hotel</h1>
    </div>

    <div class="p-10">

        <div>
            <p class="text-kamtuu-second text-[18px]">ID : {{$formulirHotels->user_id ?? "Nothing"}}</p>
        </div>

        <div class="grid max-w-6xl p-5 mx-auto my-5 text-center">
            <h1 class="font-semibold text-[24px]">Traveller Butuh kamar hotel dalam jumlah besar.
                Traveller sudah menuliskan kebutuhannya di kotak berikut.</h1>
        </div>

        <div class="grid lg:grid-cols-[33%_33%_33%]">
            <div>
                <p class="text-[18px] font-semibold">Tanggal Check-in</p>

                <div class="grid py-2 grid-cols-[80%_20%]">
                    <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                        <p class="text-white">{{$formulirHotels->checkIn}}</p>
                    </div>
                </div>
            </div>
            <div>
                <p class="text-[18px] font-semibold">Tanggal Check-out</p>

                <div class="grid py-2 grid-cols-[80%_20%]">
                    <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                        <p class="text-white">{{$formulirHotels->checkOut}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid lg:grid-cols-[33%_33%_33%]">
            <div>
                <p class="text-[18px] font-semibold">Lokasi Hotel</p>
                <div class="grid py-2">
                    <div class="inline-flex">
                        <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                            <p class="text-white">{{$formulirHotels->lokasi}}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <p class="text-[18px] font-semibold">Hotel Bintang</p>

                <div class="grid py-2 grid-cols-[80%_20%]">
                    <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                        <p class="text-white">{{$formulirHotels->jenisHotel}}</p>
                    </div>
                    <div class="md:hidden lg:hidden">
                        {{--                            <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"--}}
                        {{--                                 class="w-[15px] h-[15px] mt-[1rem] -ml-[30px] mr-1" alt="polygon 3" title="Kamtuu">--}}
                    </div>
                </div>
            </div>
        </div>

        <div class="my-5">

            <div class="font-semibold text-[18px]">
                <p>Jumlah Traveller</p>
            </div>


            <table class="table table-bordered" id="dynamicAddRemove">
                <tr>
                    <th>Jumlah Orang :</th>
                    <th>Jumlah Orang Perkamar :</th>
                    <th>Jenis Kamar :</th>
                </tr>
                <tr>
                    <td>
                        @foreach($jmlOrg as $org)
                            <div class="inline-flex bg-kamtuu-primary w-full rounded-xl border border-black p-2 my-2">
                                <p class="text-white">{{$org->jumlahOrang}} Orang</p>
                            </div>
                            <br>
                        @endforeach
                    </td>
                    <td>
                        @foreach($orgKamar as $org)
                            <div class="inline-flex bg-kamtuu-primary w-full rounded-xl border border-black p-2 my-2">
                                <p class="text-white">{{$org->jumlahOrangPerkamar}} Orang</p>
                            </div>
                            <br>
                        @endforeach
                    </td>
                    <td class="px-4">
                        @foreach($jnKamar as $org)
                            <div class="inline-flex bg-kamtuu-primary w-full rounded-xl border border-black p-2 my-2">
                                <p class="text-white">{{$org->jenisKamar}}</p>
                            </div>
                            <br>
                        @endforeach
                    </td>
                </tr>

            </table>


            <div class="grid px-4 text-[8px] lg:text-base md:text-base">
                    <p>Waktu Makan :</p>
                    <br>
                        <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                            @foreach($makans as $makan)
                            <p class="text-white">{{implode(', ',$makan)}}</p>
                            @endforeach
                        </div>
            </div>

            {{--                <div class="row" x-data="handler()">--}}
            {{--                    <div class="btn btn-primary">--}}
            {{--                        <button type="button" class="p-3 my-3 text-white rounded-full btn btn-info bg-kamtuu-second"--}}
            {{--                                @click="addNewField()">Tambah +--}}
            {{--                        </button>--}}
            {{--                    </div>--}}
            {{--                    <template x-for="(field, index) in fields" :key="index">--}}
            {{--                        <div class="max-w-4xl p-2 my-5 bg-gray-200 rounded-lg md:max-w-2xl lg:max-w-6xl">--}}
            {{--                            <table class="table-fixed w-[25rem] lg:w-[95rem]">--}}
            {{--                                <thead>--}}
            {{--                                <th class="text-[8px] md:text-[18px] lg:text-[18px]">Jumlah Orang</th>--}}
            {{--                                <th class="text-[8px] md:text-[18px] lg:text-[18px]">Jumlah Orang per Kamar</th>--}}
            {{--                                <th class="text-[8px] md:text-[18px] lg:text-[18px]">Jenis Kamar</th>--}}
            {{--                                </thead>--}}
            {{--                                <tbody>--}}
            {{--                                <tr>--}}
            {{--                                    <td class="px-4"><input--}}
            {{--                                            class="w-[4rem] h-[1rem] md:w-full md:h-[3rem] lg:w-full lg:h-[2rem]"--}}
            {{--                                            x-model="field.txt1" type="text" name="traveller[][jumlahOrang]"></td>--}}
            {{--                                    <td class="px-4"><input--}}
            {{--                                            class="w-[4rem] h-[1rem] md:w-full md:h-[3rem] lg:w-full lg:h-[2rem]"--}}
            {{--                                            x-model="field.txt2" type="text" name="traveller[][jumlahOrangPerkamar]"></td>--}}
            {{--                                    <td class="px-4">--}}
            {{--                                        <select name="traveller[][jenisKamar]"--}}
            {{--                                                class="w-[4rem] h-[17.5px] md:w-full md:h-[3rem] lg:w-full lg:h-[2rem] border border-gray-500 ring-gray-500 mt-[5px] lg:mt-0 text-[8px]"--}}
            {{--                                                x-model="field.txt3">--}}
            {{--                                            <option value="smoking">Smoking</option>--}}
            {{--                                            <option value="no smoking">No Smoking</option>--}}
            {{--                                        </select>--}}
            {{--                                    </td>--}}
            {{--                                    <td>--}}
            {{--                                        <button type="button" class="btn btn-danger btn-small"--}}
            {{--                                                @click="removeField(index)">--}}
            {{--                                            <img src="{{ asset('storage/icons/delete-dynamic-data.png') }}"--}}
            {{--                                                 class="h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">--}}
            {{--                                        </button>--}}
            {{--                                    </td>--}}
            {{--                                </tr>--}}
            {{--                                </tbody>--}}
            {{--                            </table>--}}
            {{--                            <div class="grid grid-cols-3 px-4 text-[8px] lg:text-base md:text-base">--}}
            {{--                                <div class="flex items-center">--}}
            {{--                                    <input id="option1" type="checkbox" name="traveller[][jenisMakan]"--}}
            {{--                                           class="w-4 h-4  border-gray-400 text-kamtuu-second form-checkbox" value="Makan Pagi"/>--}}
            {{--                                    <label for="option1" class="ml-3 font-medium text-gray-700">Makan Pagi</label>--}}
            {{--                                </div>--}}
            {{--                                <div class="flex items-center">--}}
            {{--                                    <input id="option1" type="checkbox" name="traveller[][jenisMakan]" value="Makan Siang"--}}
            {{--                                           class="w-4 h-4 border-gray-400 text-kamtuu-second form-checkbox"/>--}}
            {{--                                    <label for="option1"  class="ml-3 font-medium text-gray-700">Makan Siang</label>--}}
            {{--                                </div>--}}
            {{--                                <div class="flex items-center">--}}
            {{--                                    <input id="option1" name="traveller[][jenisMakan]" type="checkbox" value="Makan Malam"--}}
            {{--                                           class="w-4 h-4 border-gray-400 text-kamtuu-second form-checkbox"/>--}}
            {{--                                    <label for="option1" class="ml-3 font-medium text-gray-700">Makan Malam</label>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                    </template>--}}
            {{--                </div>--}}
            <div class="py-5">
                <div class="py-5 font-semibold">
                    <p>Tuliskan fasilitas hotel yang Anda inginkan</p>
                    <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                        <p class="text-white">{{$formulirHotels->fasilitas}}</p>
                    </div>
                </div>

                <div class="font-semibold">
                    <p>Tuliskan hal-hal lain yang perlu kami ketahui sehubungan dengan hotel yang Anda inginkan</p>
                    <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                        <p class="text-white">{{$formulirHotels->kebutuhan}}</p>
                    </div>
                </div>
            </div>

        </div>


    </div>

</div>

<footer>
    <x-destindonesia.footer></x-destindonesia.footer>
</footer>


</body>

</html>
