<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!--Regular Datatables CSS-->
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <!--Responsive Extension Datatables CSS-->
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #idn:checked + #idn {
            display: block;
        }

        #sett:checked + #sett {
            display: block;
        }

        .destindonesia {
            display: none;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
{{-- Navbar --}}
<x-be.admin.navbar-admin></x-be.admin.navbar-admin>

<div class="grid grid-cols-10">
    <x-be.admin.sidebar-admin>
    </x-be.admin.sidebar-admin>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10">
            <span class="text-2xl font-bold font-inter text-[#333333]">Formulir List Tur</span>
            <div class="flex gap-5">
                <div class="bg-white rounded-lg p-3 w-full">
                    <table class="table-auto w-full border-separate border-spacing-2 border border-slate-500">
                        <thead class="bg-gray-200">
                        <th>No</th>
                        <th>No Traveller</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        <tr>@foreach($formulirTurs as $key => $tur)
                                <td class="border border-slate-700 text-center">{{$key+1}}</td>
                                <td class="border border-slate-700 text-center">{{$tur->user_id}}</td>
                                <td class="border border-slate-700 text-center h-14"><a
                                        class="bg-kamtuu-second p-2 rounded-xl text-white"
                                        href="{{route('formulirTur.show', $tur->id)}}">Show</a></td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                    {{ $formulirTurs->links() }}


                    <div class=""></div>
                </div>
            </div>

            <br>

            <span class="text-2xl font-bold font-inter text-[#333333]">Formulir List Hotel</span>
            <div class="flex gap-5">
                <div class="bg-white rounded-lg p-3 w-full">
                    <table class="table-auto w-full border-separate border-spacing-2 border border-slate-500">
                        <thead class="bg-gray-200">
                        <th>No</th>
                        <th>No Traveller</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        <tr>@foreach($formulirHotels as $key => $tur)
                                <td class="border border-slate-700 text-center">{{$key+1}}</td>
                                <td class="border border-slate-700 text-center">{{$tur->user_id}}</td>
                                <td class="border border-slate-700 text-center h-14"><a
                                        class="bg-kamtuu-second p-2 rounded-xl text-white"
                                        href="{{route('formulirHotel.show', $tur->id)}}">Show</a></td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                    {{ $formulirTurs->links() }}


                    <div class=""></div>
                </div>
            </div>

            <br>

            <span class="text-2xl font-bold font-inter text-[#333333]">Formulir List Transfer</span>
            <div class="flex gap-5">
                <div class="bg-white rounded-lg p-3 w-full">
                    <table class="table-auto w-full border-separate border-spacing-2 border border-slate-500">
                        <thead class="bg-gray-200">
                        <th>No</th>
                        <th>No Traveller</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        <tr>@foreach($formulirTransfers as $key => $tur)
                                <td class="border border-slate-700 text-center">{{$key+1}}</td>
                                <td class="border border-slate-700 text-center">{{$tur->user_id}}</td>
                                <td class="border border-slate-700 text-center h-14"><a
                                        class="bg-kamtuu-second p-2 rounded-xl text-white"
                                        href="{{route('formulirTransfer.show', $tur->id)}}">Show</a></td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                    {{ $formulirTurs->links() }}


                    <div class=""></div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
