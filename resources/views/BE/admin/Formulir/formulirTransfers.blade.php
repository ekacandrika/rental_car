<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Destindonesia') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles

    <style>
    </style>

</head>

<body class="bg-white font-Inter">

<header>
    <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
</header>

<div class="max-w-7xl bg-[#F2F2F2] mx-auto rounded-md m-5">
    <div class="grid p-5 justify-items-center">
        <h1 class="font-semibold text-[26px]">Formulir Custom Transfer</h1>
    </div>

    <div class="p-10">

        <div>
            <p class="text-kamtuu-second text-[18px]">ID : {{$formulirTransfers->user_id ?? "Nothing"}}</p>
        </div>

        <div class="grid max-w-6xl p-5 mx-auto my-5 text-center">
            <h1 class="font-semibold text-[24px]">Traveller tidak menemukan jenis transfer yang sesuai, Traveller butuh pengaturan
                khusus. Traveller telah menuliskan kebutuhannya di kotak berikut.</h1>
        </div>

        <div class="grid lg:grid-cols-[33%_33%_33%]">
            <div>
                <p class="text-[18px] font-semibold">Tanggal Pergi</p>

                <div class="grid py-2 grid-cols-[80%_20%]">
                    <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                        <p class="text-white">{{$formulirTransfers->tanggalPergi}}</p>
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                             class="w-[15px] h-[15px] mt-[1rem] -ml-[30px] mr-1" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>
            </div>
            <div>
                <p class="text-[18px] font-semibold">Jam</p>

                <div class="grid py-2 grid-cols-[80%_20%]">
                    <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                        <p class="text-white">{{$formulirTransfers->jamPergi}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid lg:grid-cols-[33%_33%_33%]">
            <div>
                <p class="text-[18px] font-semibold">Dari</p>

                <div class="grid py-2 grid-cols-[80%_20%]">
                    <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                        <p class="text-white">{{$formulirTransfers->transferDari}}</p>
                    </div>
                </div>
            </div>

            <div>
                <p class="text-[18px] font-semibold">Kota</p>

                <div class="grid py-2 grid-cols-[80%_20%]">
                    <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                        <p class="text-white">{{$formulirTransfers->kotaDari}}</p>
                    </div>
                </div>
            </div>

            <div>
                <p class="text-[18px] font-semibold">Provinsi</p>

                <div class="grid py-2 grid-cols-[80%_20%]">
                    <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                        <p class="text-white">{{$formulirTransfers->provinsiDari}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-rows-2 py-5">
            <div>
                <p class="text-[18px] font-semibold">Embed Google Maps</p>
            </div>
        </div>

        {{--            <div class="w-[300px] h-[380px] md:w-[710px] md:h-[380px] lg:w-[710px] lg:h-[380px] border border-black rounded-lg m-1 md:m-5 lg:m-5">--}}
        <div class="h-[380px]">
            <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                <p class="text-white">{{$formulirTransfers->embedMapsDari}}</p>
            </div>
        </div>
        {{--            </div>--}}

        <div class="grid lg:grid-cols-[33%_33%_33%]">
            <div>
                <p class="text-[18px] font-semibold">Ke</p>

                <div class="grid py-2 grid-cols-[80%_20%]">
                    <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                        <p class="text-white">{{$formulirTransfers->transferKe}}</p>
                    </div>
                </div>
            </div>

            <div>
                <p class="text-[18px] font-semibold">Kota</p>

                <div class="grid py-2 grid-cols-[80%_20%]">
                    <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                        <p class="text-white">{{$formulirTransfers->kotaDari}}</p>
                    </div>
                </div>
            </div>

            <div>
                <p class="text-[18px] font-semibold">Provinsi</p>

                <div class="grid py-2 grid-cols-[80%_20%]">
                    <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                        <p class="text-white">{{$formulirTransfers->provinsiDari}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-rows-2 py-5">
            <div>
                <p class="text-[18px] font-semibold">Embed Google Maps</p>
            </div>
        </div>

{{--        <div--}}
{{--            class="w-[300px] h-[380px] md:w-[710px] md:h-[380px] lg:w-[710px] lg:h-[380px] border border-black rounded-lg m-1 md:m-5 lg:m-5">--}}
            <div class="h-[380px]">
                <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                    <p class="text-white">{{$formulirTransfers->embedMapsKe}}</p>
                </div>
            </div>
{{--        </div>--}}

        <div>
            <fieldset class="question">
                <label class="text-[18px] font-semibold" for="coupon_question">Pulang Pergi ?</label>
                <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                    <p class="text-white">{{$formulirTransfers->coupon_question ?? "Tidak"}}</p>
                </div>
            </fieldset>

                <div class="grid lg:grid-cols-[33%_33%_33%]">
                    <div>
                        <p class="text-[18px] font-semibold">Tanggal Pulang</p>

                        <div class="grid py-2 grid-cols-[80%_20%]">
                            <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                                <p class="text-white">{{$formulirTransfers->tanggalPulang ?? "Tidak"}}</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <p class="text-[18px] font-semibold">Jam</p>

                        <div class="grid py-2 grid-cols-[80%_20%]">
                            <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                                <p class="text-white">{{$formulirTransfers->jamPulang ?? "Tidak"}} </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="grid lg:grid-cols-[33%_33%_33%]">
                    <div>
                        <p class="text-[18px] font-semibold">Jenis Kendaraan</p>

                        <div class="grid py-2 grid-cols-[80%_20%]">
                            <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                                <p class="text-white">{{$formulirTransfers->transport}}</p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <div class="grid lg:grid-cols-[20%_20%_60%] py-5 grid-rows-7">
            <div class="col-span-3">
                <p class="text-[18px] font-semibold">Jumlah Orang:</p>
            </div>

            <div class="col-span-2 row-span-2">
                <p>Dewasa</p>
                <p class="text-[#828282]">Diatas 12 tahun</p>
            </div>
            <div>
                <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                    <p class="text-white">{{$formulirTransfers->dewasa}}</p>
                </div>
            </div>

            <div class="col-span-2 row-span-2">
                <p>Anak</p>
                <p class="text-[#828282]">5 sampai 12 tahun</p>
            </div>
            <div>
                <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                    <p class="text-white">{{$formulirTransfers->anak}}</p>
                </div>
            </div>

            <div class="col-span-2 row-span-2">
                <p>Balita</p>
                <p class="text-[#828282]">Dibawah 5 tahun</p>
            </div>
            <div>
                <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                    <p class="text-white">{{$formulirTransfers->balita}}</p>
                </div>
            </div>
        </div>

        <div class="py-5">
            <p class="text-[18px] font-semibold">Barang Bawaan:</p>
            <div class="grid grid-cols-[10%_40%_40%] ">
                <div>
                    Besar
                </div>
                <div class="grid col-span-2">
                    <div class="flex">
                        <div class="p-1 bg-kamtuu-primary border border-black rounded-xl">
                            <p class="text-white">{{$formulirTransfers->barangBesar}}</p>
                        </div>
                        <p class="px-2 -mt-[4px]">pcs</p>
                    </div>
                </div>
                <div>
                    Kecil
                </div>
                <div class="grid col-span-2">
                    <div class="flex">
                        <div class="p-1 bg-kamtuu-primary border border-black rounded-xl">
                            <p class="text-white">{{$formulirTransfers->barangKecil}}</p>
                        </div>
                        <p class="px-2 -mt-[4px]">pcs</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="py-5">
            <p class="text-xl font-semibold my-2">
                Tuliskan hal-hal lain yang perlu kami ketahui untuk mengaturkan transfer Anda.
            </p>
            <div class="p-2 bg-kamtuu-primary border border-black rounded-xl w-[15rem] lg:w-[35rem]">
                <p class="text-white">{{$formulirTransfers->memo}}</p>
            </div>
            <div class="flex my-2">
                <div class="inline-flex bg-kamtuu-third w-1/3 rounded-xl border border-black p-2">
                    <p class="text-white">{{$formulirTurs->kamtuu ?? 'Tidak,'}} Serahkan ke Kamtuu</p>
                </div>
            </div>
        </div>

        </form>
    </div>

</div>

<footer>
    <x-destindonesia.footer></x-destindonesia.footer>
</footer>

</body>

</html>

