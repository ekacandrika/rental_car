<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Destindonesia') }}</title>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    {{-- <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script> --}}
    {{-- <script src="{{ resource_path('assets/js/gijgo.min.js') }}" type="text/javascript"></script>
    <script src="{{ resource_path('assets/js/summernote.js') }}" type="text/javascript"></script>
    <link rel="stylesheet" href="{{ resource_path('assets/css/summernote.css') }}" /> --}}


    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js" defer></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <!-- Styles -->
    @livewireStyles

    <style>
        .note-editable {
            background-color: white;
            /* color: black; */
        }

        .note-toolbar {
            background-color: white;
        }

        .note-editable ul {
            list-style: disc !important;
            list-style-position: inside !important;
        }

        .note-editable ol {
            list-style: decimal !important;
            list-style-position: inside !important;
        }
    </style>

</head>

<body class="bg-white font-Inter">

<header>
    <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
</header>

<div class="max-w-7xl bg-[#F2F2F2] mx-auto rounded-md m-5">
    <div class="grid p-5 justify-items-center">
        <h1 class="font-semibold text-[26px]">Formulir Custom Tour</h1>
    </div>

    <div class="p-5 sm:p-10">

        {{-- <div id="summernote" class="click2edit"></div> --}}


        <div>
            <p class="text-kamtuu-second text-[18px]">ID : {{$formulirTurs->user_id ?? "Nothing"}}</p>
        </div>


            <div class="grid lg:grid-cols-[33%_33%_33%]">
                <div>
                    <p class="text-[18px] font-semibold">Tanggal Mulai</p>

                    <div class="lg:grid py-2 lg:grid-cols-[80%_20%] w-2/4 lg:w-full">
                        <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
                            <p class="text-white">{{$formulirTurs->tanggalMulai}}</p>
                        </div>
                        {{-- <div class="md:hidden lg:hidden">
                            <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                        </div> --}}
                    </div>
                </div>
                <div>
                    <p class="text-[18px] font-semibold">Mulai</p>

                    <div class="lg:grid py-2 lg:grid-cols-[80%_20%] w-2/4 lg:w-full">
                        <div class="p-2 bg-kamtuu-primary border border-black rounded-xl">
{{--                            <input name="waktuMulai" id="timepickerFormulirTur" placeholder=" "/>--}}
                            <p class="text-white"> Jam {{$formulirTurs->waktuMulai}}</p>
                        </div>
                    </div>
                </div>
                <div>
                    <p class="text-[18px] font-semibold">Durasi</p>
                    <div class="grid py-2">
                        <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                            <p class="text-white">{{$formulirTurs->durasi}} Hari</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid grid-rows-2 py-5">
                <div>
                    <p class="text-[18px] font-semibold">Lokasi Mulai Tur:</p>
                </div>
                <div>
                    <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                        <p class="text-white">{{$formulirTurs->lokasi}}</p>
                    </div>
                </div>
            </div>

            <div class="grid lg:grid-cols-[20%_20%_60%] py-5 grid-rows-7">
                <div class="col-span-3">
                    <p class="text-[18px] font-semibold">Jumlah Peserta:</p>
                </div>

                <div class="col-span-2 row-span-2">
                    <p>Dewasa</p>
                    <p class="text-[#828282]">Diatas 12 tahun</p>
                </div>
                <div>
                    <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                        <p class="text-white">{{$formulirTurs->dewasa}} Orang</p>
                    </div>
                </div>

                <div class="col-span-2 row-span-2">
                    <p>Anak</p>
                    <p class="text-[#828282]">5 sampai 12 tahun</p>
                </div>
                <div>
                    <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                        <p class="text-white">{{$formulirTurs->anak}} Orang</p>
                    </div>
                </div>

                <div class="col-span-2 row-span-2">
                    <p>Balita</p>
                    <p class="text-[#828282]">Dibawah 5 tahun</p>
                </div>
                <div>
                    <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                        <p class="text-white">{{$formulirTurs->balita}} Orang</p>
                    </div>
                </div>
            </div>

            <div class="grid lg:grid-cols-3">
                <div class="grid col-span-3">
                    <p class="font-semibold text-[18px]">Jenis Akomodasi</p>
                </div>
                <div class="grid lg:grid-cols-[25%_15%_60%] col-span-3">
                    <div>
                        <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                            <p class="text-white">{{$formulirTurs->transportasi}}</p>
                        </div>
                    </div>

                    <div>
                        <p class="w-3/4">Jumlah Peserta</p>
                    </div>

                    <div>
                        <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                            <p class="text-white">{{$formulirTurs->jumlahPeserta}} Orang</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid lg:grid-cols-3">
                <div class="grid col-span-3">
                    <p class="font-semibold text-[18px]">Makanan</p>
                </div>
                <div class="grid grid-cols-[25%_15%_60%] col-span-3">
                    <div>
                        <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                            <p class="text-white">{{$formulirTurs->makanan}}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="py-5">
                <p class="text-[18px] font-semibold">Centang jenis transportasi yang menjadi preferensi Anda:</p>
            </div>

            <div class="grid ">
                <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                    <p class="text-white">Transportasi @foreach($kendaraans as $transport) {{$transport}} @endforeach</p>
                </div>
            </div>

            <div class="py-5">
                <p class="py-3 text-[20px] sm:text-[24px] text-justify sm:text-left font-semibold">Bila Anda
                    mengetahui,
                    tuliskan nama objek wisata
                    yang ingin
                    dimasukkan ke dalam paket tur Anda. Bila
                    tidak tahu, centeng “Serahkan ke Kamtuu” pada bagian bawah kotak.</p>

                <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                    <p class="text-white">{{$formulirTurs->namaObjek ?? 'Tidak Ada'}}</p>
                </div>

                <div class="flex my-4">
                    <div class="inline-flex bg-kamtuu-third w-1/3 rounded-xl border border-black p-2">
                        <p class="text-white">{{$formulirTurs->kamtuu ?? 'Tidak,'}} Serahkan ke Kamtuu</p>
                    </div>
                </div>
            </div>

            <div class="pt-5 pb-3">
                <p class="text-[20px] sm:text-[24px] text-justify sm:text-left font-semibold">Bila Anda Punya rancangan
                    program
                    tur sendiri,
                    silahkan masukkan di
                    bawah.</p>
            </div>


            <table class="table table-bordered w-full" id="dynamicAddRemove">
                <tr>
                    <th>Deskripsi Tur :</th>
                </tr>
                <tr>
                    <td>@foreach($deskripsis as $deskripsi)
                            <div class="inline-flex bg-kamtuu-primary w-1/3 rounded-xl border border-black p-2">
                                <p class="text-white">{{$deskripsi->deskripsiCustom}}</p>
                            </div>
                        @endforeach</td>
                </tr>

            </table>

    </div>

</div>


<footer>
    <x-destindonesia.footer></x-destindonesia.footer>
</footer>

</body>

</html>
