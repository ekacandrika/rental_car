<!DOCTYPE html>
<html lang="{{str_replace('_','-',app()->getLocale())}}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">

        <title>{{config('app.name','Laravel')}}</title>

        {{-- Font --}}
        <link rel="stylesheet" href="https://fonts.bunny.net/css/base/jquery.fonticonpicker.min.css">

        {{-- base | always include--}}
            <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.1.1/dist/css/base/jquery.fonticonpicker.min.css">

        <!-- default grey-theme -->
        <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/@fonticonpicker/fonticonpicker@3.0.0-alpha.0/dist/css/themes/grey-theme/jquery.fonticonpicker.grey.min.css">

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript"
        src="https://unpkg.com/@fonticonpicker/fonticonpicker/dist/js/jquery.fonticonpicker.min.js"></script>

        {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> --}}
    
        <!-- Select 2 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>

        {{-- sweet alert --}}
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.all.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.min.css" rel="stylesheet">

        {{-- data tables --}}
        <style>
            @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

            /**
            * tailwind.config.js
            * module.exports = {
            *   variants: {
            *     extend: {
            *       backgroundColor: ['active'],
            *     }
            *   },
            * }
            */
            .active\:bg-gray-50:active {
                --tw-bg-opacity: 1;
                background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
            }

            #tur:checked+#tur {
                display: block;
            }

            #transfer:checked+#transfer {
                display: block;
            }

            #hotel:checked+#hotel {
                display: block;
            }

            #rental:checked+#rental {
                display: block;
            }

            #activity:checked+#activity {
                display: block;
            }

            #xstay:checked+#xstay {
                display: block;
            }

            #idn:checked+#idn {
                display: block;
            }

            #sett:checked+#sett {
                display: block;
            }

            .destindonesia {
                display: none;
            }
            .select2-container .select2-selection--single {
                box-sizing: border-box;
                cursor: pointer;
                display: block;
                height: 48px;
                user-select: none;
                -webkit-user-select: none;
            }
            .select2-container--default .select2-selection--single .select2-selection__rendered {
                color: #444;
                line-height: 46px;
            }
            .select2-container--default .select2-selection--single .select2-selection__arrow {
                height: 26px;
                position: absolute;
                top: 11px;
                right: 13px;
                width: 20px;
            }
        </style>
    </head>
    <body class="bg-[#F2F2F2] font-inter">
{{-- Navbar --}}
<x-be.admin.navbar-admin></x-be.admin.navbar-admin>
<div class="grid grid-cols-10">
    <x-be.admin.sidebar-admin>
    </x-be.admin.sidebar-admin>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10 h-full">
            <div class=" p-5 border rounded-md bg-white my-5">
                <p class="text-xl font-semibold">Dokumen Legal</p>
                <form method="POST" action="{{ route('legal.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="p-2 mb-2">
                        <label for="document_title" class="block mb-2 text-sm font-bold text-[#333333]">Judul/Nama Dokumen</label>
                        <input name="document_title" type="text" class="block mb-2 text-sm border border-[#4F4F4F] text-gray-900 rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5" placeholder="Judul/Nama Dokumen"/>
                    </div>
                    @error('document_title')
                        <div class="p-2 mb-2" x-data="{show:true}" x-init="setTimeout(() => show = false, 3000)">
                            <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                {{ $message }}
                            </div>
                        </div>
                    @enderror
                    <div class="p-2">
                        <label for="is_mandaroty" class="block mb-2 text-sm font-bold text-[#333333]">Dokumen Wajib</label>
                        <div class="flex gap-3">
                            <div class="flex items-center py-px">
                                <input type="radio" name="is_mandaroty" class="w-4 h-4 text-blue-500 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 focus:border-blue-500" value="yes">
                                <label for="is_mandaroty" class="ml-2 text-sm font-semibold text-[#333333]">Ya</label>
                            </div>
                            <div class="flex items-center py-px">
                                <input type="radio" name="is_mandaroty" class="w-4 h-4 text-blue-500 bg-gray-100 rounded-full border-gray-300 focus:ring-blue-500 focus:border-blue-500" value="no">
                                <label for="is_mandatory" class="ml-2 text-sm font-semibold text-[#333333]">Tidak</label>
                            </div>
                        </div>
                    </div>
                    <div class="p-6">
                        <button class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2">Simpan</button>
                    </div>
                </form>

                <hr>
                <div x-data="legal()">
                    <p class="text-4xl font-semibold my-5">List Dokumen Legal</p>
                    <div class="flex items-center col-start-4 col-span-1 mb-3">
                        <label for="simple-search" class="mr-4">Search</label>
                        <div class="relative w-1/4">
                            
                            <input type="text" id="simple-search"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-2 p-2.5  
                            dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Search" @keyup="search()">
                            <div class="absolute inset-y-0 right-4 flex items-center pl-3 pointer-events-none">
                                <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400"
                                    fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                                        clip-rule="evenodd"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="block px-5 mb-6">
                        <template x-if="deleted_arr.length >= 1">
                            <button 
                            x-transition:enter="transition ease-out duration-300"
                            x-transition:enter-start="opacity-0 scale-90"
                            x-transition:enter-end="opacity-100 scale-100"
                            x-transition:leave="transition ease-in duration-300"
                            x-transition:leave-start="opacity-100 scale-100"
                            x-transition:leave-end="opacity-0 scale-90"
                            type="button" class="float-left mx-2 my-3 bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]" style="background:#D50006" @click="multipleDelete()">Hapus</button>
                        </template>
                    </div>
                    <table class="table-auto w-full text-center border-separate border border-slate-500">
                        <thead>
                        <tr>
                            <th class="border border-slate-600">No</th>
                            <th class="border border-slate-600">Nama/Judul Dokumen</th>
                            <th class="border border-slate-600">Dokumen Wajib</th>
                            <th class="border border-slate-600">Action</th>
                        </tr>
                        </thead>
                        <template x-if="datas.length >= 1">
                            <template x-for="(data, index) in datas" :key="index">
                            <tbody>
                                <tr class="p-2">
                                    <td class="p-2 border border-slate-600" x-text="index+1"></td>
                                    <td class="p-2 border border-slate-600 capitalize" x-text="data.document_title"></td>
                                    <td class="p-2 border border-slate-600 capitalize" x-text="data.is_mandatory"></td>
                                    <td class="p-2 border border-slate-600">
                                        {{-- master/document/{document}/edit --}}
                                        <form x-bind:action="`legal/${data.id}`" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="float-right mx-2 bg-[#D50006] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#de252b]" style="background:#D50006">Hapus</button>
                                        </form>
                                        <a x-bind:href="`legal/${data.id}/edit`" class="float-right mx-2 bg-[#e8bf3a] text-white text-sm font-semibold py-2 px-7 rounded-lg hover:bg-[#f6ba59]" style="background:#e8bf3a">Edit</a>
                                    </td>
                                </tr>    
                            </tbody>
                            </template>
                        </template>
                        <template x-if="datas.length == 0">
                            <tbody>
                                <tr class="p-2">
                                    <td class="p-2 border border-slate-600" colspan="4">Tidak ada data</td>
                                </tr>    
                            </tbody>   
                        </template>
                    </table>
                    <nav aria-label="Page navigation" class="text-start mt-3">
                        <template x-if="links.length > 1">
                            <div class="flex flex-row justify-between gap-x-36" style="width: 931px;">
                                <!-- Help text -->
                                <span class="text-sm text-gray-700 dark:text-gray-400" style="width:124px;max-width:124px">
                                    Showing <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="from"></span> to <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="to">10</span> of <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="total_page">100</span> Entries
                                </span>
                                <ul class="inline-flex -space-x-px text-sm text-end">
                                    <li>
                                        <button type="button" class="rounded-l-lg flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white capitalize" 
                                        :class="{ 'dark:bg-gray-600 text-gray-200':links[0].label == cur_page, 'bg-red-100 text-gray-400':links[0].label != cur_page}"
                                        x-text="prev" @click="prevPage(links[0].label)"></button>
                                    </li>
                                    <template x-for="(link, index) in links" :key="index">
                                        <li>
                                                <template x-if="link.label >= 1">
                                                    <button type="button" class="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white" x-text="link.label" 
                                                    :class="{ 'dark:bg-gray-600 text-gray-200':link.label == cur_page, 'bg-red-100 text-gray-400':link.label != cur_page}" 
                                                    @click="getPage(link.url,link.label)" :id="'page-'+link.label"></button>
                                                </template>
                                                <template x-if="link.label == '...'">
                                                    <button type="button" class="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400" x-text="link.label" 
                                                    :class="{ 'dark:bg-gray-600 text-gray-200':link.label == cur_page, 'bg-red-100 text-gray-400':link.label != cur_page}" 
                                                    @click="getPage(link.url,link.label)" :id="'page-'+link.label" disabled></button>
                                                </template>
                                        </li>
                                    </template>
                                    <li>
                                        <button type="button" class="rounded-r-lg flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white capitalize" x-text="next" 
                                        :class="{ 'dark:bg-gray-600 text-gray-200':links[(links.length*1)-1].label == cur_page, 'bg-red-100 text-gray-400':links[(links.length*1)-1].label != cur_page}"
                                        :class="{'cursor-not-allowed':links[(links.length*1)-1].url==null,'cursor-pointer':links[(links.length*1)-1].url!=null}" 
                                        @click="nextPage(links[(links.length*1)-1].label)"></button>
                                    </li>
                                </ul>
                            </div>
                        </template>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    
    document.addEventListener("alpine:init",()=>{
        Alpine.data("legal",()=>({
            datas:[],
            links:[],
            current_page:null,
            cur_page:null,
            url:"{{route('document.datas')}}",
            to:'',
            total:'',
            from:'',
            total_page:'',
            prev:'',
            next:'',
            next_page:1,
            prev_page:0,
            first_page:null,
            last_page:null,
            cur_page:null,
            deleted_arr:[],
            async init(){
                let fetching = await fetch(`${this.url}`);
                let json = await fetching.json();

                //console.log(json.datas);

                this.datas = json.datas.data
                this.links = json.datas.links
                this.current_page = json.datas.current_page

                this.to = json.datas.to
                this.total_page = json.datas.total
                this.from = json.datas.from
                
                this.current_page = json.datas.current_page

                last_index = (this.links.length*1)-1;

                this.prev = this.links[0].label.replace('pagination.previous','previous');
                this.next = this.links[last_index].label.replace('pagination.next','next');

                if(this.cur_page == null){
                   this.cur_page = 1
                        
                }

                this.first_page = 1
                this.last_page = json.datas.last_page
            },
            search(){
                var cari   = $("#simple-search").val();                   
                let page = '&page=1';

                if(cari.length > 0){
                    page = '&page='+this.cur_page;
                }
                    
                console.log(page)

                fetch(`${this.url}?cari=${cari}`).
                then(response =>response.json())
                .then(data=>{
                    this.datas = []
                    this.datas = data.datas.data
                    this.links = data.datas.links

                    this.to = data.datas.to
                    this.total_page = data.datas.total
                    this.from = data.datas.from
                    this.last_page = objs.last_page

                    console.log(data.datas);
                })
            },
            getPage(page){
                let arr = [];
                if(page){
                    this.current_page = page;
                }else{
                    this.current_page = '/?page=1';
                }
                
                this.fetching(this.current_page)
            },
            nextPage(label){
                    let search = $("#simple-search").val();   
                     
                    let query_params = "";
                    
                    document.getElementById("page-"+this.cur_page).classList.add("bg-red-100");
                    document.getElementById("page-"+this.cur_page).classList.remove("text-gray-200");
                    
                    if(this.last_page != this.next_page){
                        this.next_page++
                        this.cur_page +=1;
                    }

                    
                    let next = this.next_page;

                    document.getElementById("page-"+next).classList.remove("bg-red-100");
                    document.getElementById("page-"+next).classList.add("text-gray-200");
                    
                    if(search){
                        query_params +=`${this.url}/?cari=${search}&page=${this.next_page}`
                    }else{
                       query_params +=`${this.url}/?page=${this.next_page}` 
                    }

                    console.log(query_params)
                    this.fetching(query_params)
            },
            prevPage(label){
                    let search = $("#simple-search").val(); 
                    let query_params = "";
                    this.prev_page = this.next_page;
                    
                    document.getElementById("page-"+this.cur_page).classList.add("bg-red-100");
                    document.getElementById("page-"+this.cur_page).classList.remove("text-gray-200");

                    if(this.first_page != this.prev_page){
                        this.prev_page--
                        this.cur_page -=1
                    }

                    this.next_page=this.prev_page
                    
                    document.getElementById("page-"+this.prev_page).classList.remove("bg-red-100");
                    document.getElementById("page-"+this.prev_page).classList.add("text-gray-200");

                    if(search){
                        query_params +=`${this.url}/?cari=${search}&page=${this.prev_page}`
                    }else{
                       query_params +=`${this.url}/?page=${this.prev_page}` 
                    }

                    //console.log(query_params)
                    this.fetching(query_params)
            },
            fetching(params){

                let arr = [];
                
                fetch(`${params}`)
                .then(resp=>resp.json())
                .then(data=>{
                    this.datas = []
                    let obj = data.datas.data

                    if(obj.length != undefined){
                        this.datas = obj
                    }else{
                        Object.keys(obj).forEach(key=>{
                            arr.push(obj[key])
                        })

                        this.datas = arr
                    }

                    //this.datas = data.datas.data
                    this.links = data.datas.links

                    this.to = data.datas.to
                    this.total_page = data.datas.total
                    this.from = data.datas.from
                    this.last_page = data.datas.last_page
                    
                    let prev_page = data.datas.current_page-1;

                    if(prev_page > 0 || prev_page!=null){
                        this.prev_page = prev_page ;
                    }

                    //console.log(data.datas);
                })
            }
        }))
    })
</script>
</html>
