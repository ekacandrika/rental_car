<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    {{--summernote--}}
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    {{--    tag input bootstrap--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
          rel="stylesheet"/>
    {{--    select2--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"/>
    {{--jquery slim js--}}
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    {{--  summernote js  --}}
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    {{--  ajax  --}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>--}}
    {{--  select2 js  --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #idn:checked + #idn {
            display: block;
        }

        #sett:checked + #sett {
            display: block;
        }

        .bootstrap-tagsinput .tag {
            margin-right: 2px;
            color: #ffffff;
            background: #2196f3;
            padding: 3px 7px;
            border-radius: 3px;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
{{-- Navbar --}}
<x-be.admin.navbar-admin></x-be.admin.navbar-admin>

<div class="grid grid-cols-10">
    <x-be.admin.sidebar-admin>
    </x-be.admin.sidebar-admin>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10 h-full">

            {{--Wisata--}}

            <form method="POST" action="{{ route('wisata.update', $wisataShow->id) }}" enctype="multipart/form-data">
                @csrf
                <div id="pulau" class="my-4 border rounded-md bg-white">
                    {{-- @dump($wisataShow) --}}
                    {{-- daerah select--}}
                    <div class="p-6">
                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Wisata</label>

                        <div class="pulau box ">
                            <div class="form-group">
                                <strong>Pulau:</strong>
                                <input type="hidden" value="" name="pulau_id" id="valuePulau">
                                {{-- @dump($wisataShow->pulau_id) --}}
                                <select id='myselect' name="pulau_id select2"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                    >
                                    <option value="">Select An Option</option>
                                    @foreach($pulaus as $pulau)
                                        {{--                                    @foreach($news->tags as $tag)--}}
                                        <option value="{{$pulau->id}}" {{isset($wisataShow->pulau_id) ? ($wisataShow->pulau_id==$pulau->id ? 'selected' : '') : '' }}>{{$pulau->island_name}}</option>
                                        {{--                                    @endforeach--}}
                                    @endforeach
                                </select>
                            </div>
                        @error('pulau_id_select2')
                        <div x-data="{show:true}" x-init="setTimeout(()=>show = false, 3000)">
                            <div class="bg-red-500 text-sm rounded-md w-96 py-1 my-2" x-show="show">
                                {{$message}}
                            </div>
                        </div>
                        @enderror
                        </div>
                        <div class="provinsi box">
                            <div class="form-group">
                                <strong>Provinsi:</strong>
                                <input type="hidden" value="" name="provinsi_id" id="valueProvinsi">
                                <select id='myselect2' name="select2 provinsi_id" type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                    >
                                    <option value="">Select An Option</option>
                                    @foreach($provinsis as $provinsi)
                                        {{--                                    @foreach($news->tags as $tag)--}}
                                        <option value="{{$provinsi->id}}" {{isset($wisataShow->provinsi_id) ? ($wisataShow->provinsi_id==$provinsi->id ? 'selected':'') : '' }}>{{$provinsi->name}}</option>
                                        {{--                                    @endforeach--}}
                                    @endforeach
                                </select>
                            </div>
                        @error('select2_provinsi_id')
                        <div x-data="{show:true}" x-init="setTimeout(()=>show = false, 3000)">
                            <div class="bg-red-500 text-sm rounded-md w-96 py-1 my-2" x-show="show">
                                {{$message}}
                            </div>
                        </div>
                        @enderror    
                        </div>
                        <div class="kabupaten box">
                            <div class="form-group">
                                <strong>Provinsi:</strong>
                                <input type="hidden" value="" name="kabupaten_id" id="valueKabupaten">
                                <select id='myselect3' name="select2 kabupaten_id" type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                    >
                                    <option value="">Select An Option</option>
                                    @foreach($kabupatens as $kabupaten)
                                        {{--                                    @foreach($news->tags as $tag)--}}
                                        <option value="{{$kabupaten->id}}" {{isset($wisataShow->kabupaten_id) ? ($wisataShow->kabupaten_id==$kabupaten->id ? 'selected':'') : '' }}>{{$kabupaten->name}}</option>
                                        {{--                                    @endforeach--}}
                                    @endforeach
                                </select>
                            </div>
                            @error('select2_kabupaten_id')
                            <div x-data="{show:true}" x-init="setTimeout(()=>show = false, 3000)">
                                <div class="bg-red-500 text-sm rounded-md w-96 py-1 my-2" x-show="show">
                                    {{$message}}
                                </div>
                            </div>
                            @enderror     
                        </div>
                    </div>

                    {{--                    slug--}}
                    <div class="p-6">
                       <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Slug</label>
                        <input name="slug" type="input" id="text"
                               class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                               placeholder="Nama Pulau" value="{{old('title', $wisataShow->slug)}}">
                    @error('slug')
                    <div x-data="{show:true}" x-init="setTimeout(()=>show = false, 3000)">
                        <div class="bg-red-500 text-sm rounded-md w-96 py-1 my-2" x-show="show">
                            {{$message}}
                        </div>
                    </div>
                    @enderror    
                    </div>
                    {{--                    nama objek--}}
                    <div class="p-6">
                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Nama Objek Wisata</label>
                        <input name="namaWisata" type="text" id="text"
                               class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                               placeholder="Nama Objek Wisata" value="{{old('namaWisata', $wisataShow->namaWisata)}}">
                    @error('namaWisata')
                    <div x-data="{show:true}" x-init="setTimeout(()=>show = false, 3000)">
                        <div class="bg-red-500 text-sm rounded-md w-96 py-1 my-2" x-show="show">
                            {{$message}}
                        </div>
                    </div>
                    @enderror 
                    </div>

                    {{-- Thumbnail --}}
                    <div class="p-6" x-data="displayImage()">
                        <p class="font-bold text-xl">Thumbnail</p>
                        {{-- {{Storage::url($wisataShow->thumbnail)}} --}}
                        <input class="mt-4" type="file" name="thumbnail" accept="image/*" @change="selectedFile">
                        <template x-if="images.length < 1">
                            <div
                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                <img src="{{Storage::url($wisataShow->thumbnail)}}" alt="upload-icons" width="80px"
                                     height="80px">
                            </div>
                        </template>
                        <template x-if="images.length >= 1">
                            <div class="flex">
                                <template x-for="(image, index) in images" :key="index">
                                    <div class="flex justify-center items-center">
                                        <img :src="image"
                                             class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                             :alt="'upload'+index">
                                        <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                 width="25px" @click="removeImage(index)">
                                        </button>
                                    </div>
                                </template>
                            </div>
                        </template>
                    </div>

                    {{-- Header --}}
                    <div class="p-6" x-data="displayImage()">
                        <p class="font-bold text-xl">Header</p>
                        <input class="mt-4" type="file" name="header" accept="image/*" @change="selectedFile">
                        <template x-if="images.length < 1">
                            <div
                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                <img src="{{Storage::url($wisataShow->header)}}" alt="upload-icons" width="90px"
                                     height="90px">
                            </div>
                        </template>
                        <template x-if="images.length >= 1">
                            <div class="flex">
                                <template x-for="(image, index) in images" :key="index">
                                    <div class="flex justify-center items-center">
                                        <img :src="image"
                                             class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                             :alt="'upload'+index">
                                        <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                 width="25px" @click="removeImage(index)">
                                        </button>
                                    </div>
                                </template>
                            </div>
                        </template>
                    </div>

                    {{-- Deskripsi --}}
                    <div class="p-6">
                        <p class="font-bold text-xl">Deskripsi</p>
                        <div class="bg-white pt-4">
                            <textarea name="deskripsi" id="summernotePulau">{!!  old('deskripsi', $wisataShow->deskripsi) !!}</textarea>
                        </div>

                    @error('deskripsi')
                    <div x-data="{show:true}" x-init="setTimeout(()=>show = false, 3000)">
                        <div class="bg-red-500 text-sm rounded-md w-96 py-1 my-2" x-show="show">
                            {{$message}}
                        </div>
                    </div>
                    @enderror     
                    </div>

                    {{--Informasi--}}
                    <div class="p-6">
                        <p class="font-bold text-xl">Informasi</p>
                        <div class="px-4 grid grid-cols-2">

                            <div class="mt-4">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Kategori</label>
                                <input name="kategori" type="text" id="text"
                                       class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                       placeholder="Kategori" value="{{old('kategori', $wisataShow->kategori)}}">
                                       @error('kategori')
                                       <div x-data="{show:true}" x-init="setTimeout(()=>show = false, 3000)">
                                           <div class="bg-red-500 text-sm rounded-md w-96 py-1 my-2" x-show="show">
                                               {{$message}}
                                           </div>
                                       </div>
                                       @enderror        
                            </div>

                            <div class="mt-4">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Harga
                                    Tiket</label>
                                <input name="hargaTiket" type="text" id="text"
                                       class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                       placeholder="Harga Tiket" value="{{old('hargaTiket', $wisataShow->hargaTiket)}}">
                                       @error('hargaTiket')
                                       <div x-data="{show:true}" x-init="setTimeout(()=>show = false, 3000)">
                                           <div class="bg-red-500 text-sm rounded-md w-96 py-1 my-2" x-show="show">
                                               {{$message}}
                                           </div>
                                       </div>
                                       @enderror 
                            </div>

                            <div class="mt-4">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Hari</label>
                                <input name="hari" type="text" id="text"
                                       class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                       placeholder="Hari Buka" value="{{old('hari', $wisataShow->hari)}}">
                                       @error('hargaTiket')
                                       <div x-data="{show:true}" x-init="setTimeout(()=>show = false, 3000)">
                                           <div class="bg-red-500 text-sm rounded-md w-96 py-1 my-2" x-show="show">
                                               {{$message}}
                                           </div>
                                       </div>
                                       @enderror 
                            </div>

                            <div class="mt-4">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Jam
                                    Operasional</label>
                                <input name="jamOperasional" type="text" id="text"
                                       class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                       placeholder="Jam Operasional Objek Wisata" value="{{old('jamOperasional', $wisataShow->jamOperasional)}}">
                                       @error('hargaTiket')
                                       <div x-data="{show:true}" x-init="setTimeout(()=>show = false, 3000)">
                                           <div class="bg-red-500 text-sm rounded-md w-96 py-1 my-2" x-show="show">
                                               {{$message}}
                                           </div>
                                       </div>
                                       @enderror 
                            </div>

                            <div class="mt-4">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Akses
                                    Transportasi</label>
                                <input name="transportasi" type="text" id="text"
                                       class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                       placeholder="Sebutkan Akses Transportasi" value="{{old('transportasi', $wisataShow->transportasi)}}">
                                       @error('hargaTiket')
                                       <div x-data="{show:true}" x-init="setTimeout(()=>show = false, 3000)">
                                           <div class="bg-red-500 text-sm rounded-md w-96 py-1 my-2" x-show="show">
                                               {{$message}}
                                           </div>
                                       </div>
                                       @enderror 
                            </div>
                        </div>
                    </div>

                    {{-- Gallery --}}
                    <div class="p-6" x-data="displayGalleries()">
                        <p class="font-bold text-xl">Gallery Foto Objek Wisata</p>
                        <label class="m-2">Images</label>
                        <input class="pt-4" type="file" accept="image/*" @change="selectedFile" name="images[]"
                               multiple>
                        <template x-if="images.length < 1">
                            <div
                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                {{-- @foreach($value as $val)
                                <img src="{{($val)}}" alt="upload-icons" width="20px"
                                     height="20px">
                                @endforeach --}}
                                <img src="" alt="upload-icons" width="20px" height="20px">
                            </div>
                        </template>
                        <template x-if="images.length >= 1">
                            <div class="flex">
                                <template x-for="(image, index) in images" :key="index">
                                    <div class="flex justify-center items-center">
                                        <img :src="image"
                                             class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                             :alt="'upload'+index">
                                        <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                            <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                 width="25px" @click="removeImage(index)">
                                        </button>
                                    </div>
                                </template>
                            </div>
                        </template>
                        @error('image_gallery')
                        <div x-data="{show:true}" x-init="setTimeout(()=>show = false, 3000)">
                            <div class="bg-red-500 text-sm rounded-md w-96 py-1 my-2" x-show="show">
                                {{$message}}
                            </div>
                        </div>
                        @enderror
                        <div id="error-gallery" class="hidden bg-red-500 text-sm rounded-md w-96 py-1 px-2 my-2"></div>
                    </div>

                    {{-- Maps --}}
                    <div class="p-6">
                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333]">Link Peta Google
                            Maps</label>
                        <input name="embed_maps" type="text" id="text"
                               class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                               placeholder="<iframe src=”https://www.map" value="{{old('embedMaps', $wisataShow->embed_maps)}}">
                        @error('embed_maps')
                        <div x-data="{show:true}" x-init="setTimeout(()=>show = false, 3000)">
                            <div class="bg-red-500 text-sm rounded-md w-96 py-1 my-2" x-show="show">
                                {{$message}}
                            </div>
                        </div>
                        @enderror
                    </div>

                    {{--                    tags--}}

                    <div class="form-group p-6">

                        <label>Tags Custom:</label>

                        <br/>

                        <input data-role="tagsinput" type="text" name="tags" value="{{$tags}}">

                        {{--                        <div class="form-group">--}}
                        {{--                            <strong>Tag Autocomplete:</strong>--}}
                        {{--                            <select id='myselect' name="select2" multiple>--}}
                        {{--                                <option value="">Select An Option</option>--}}
                        {{--                                @foreach($newsletters as $news)--}}
                        {{--                                    @foreach($news->tags as $tag)--}}
                        {{--                                        <option value="{{$tag->name}}">{{$tag->name}}</option>--}}
                        {{--                                    @endforeach--}}
                        {{--                                @endforeach--}}
                        {{--                            </select>--}}
                        {{--                        </div>--}}
                        @if ($errors->has('tags'))

                            <span class="text-danger">{{ $errors->first('tags') }}</span>

                        @endif

                    </div>
                    <div class="p-6">
                        <button class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
<script type="text/javascript">
    $('#myselect').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    })
    $('#myselect').on("change", function (e) {
        var data = $("#myselect").val()
        document.getElementById('valuePulau').setAttribute('value', data);
        console.log($("#myselect").val())
    })

    $('#myselect2').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    });
    $('#myselect2').on("change", function (e) {
        var data = $("#myselect2").val()
        document.getElementById('valueProvinsi').setAttribute('value', data);
        console.log($("#myselect2").val())
    })
    $('#myselect3').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    });
    $('#myselect3').on("change", function (e) {
        var data = $("#myselect3").val()
        document.getElementById('valueKabupaten').setAttribute('value', data);
        console.log($("#myselect3").val())
    })
</script>
<script>
    $('#summernotePulau').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteWalikota').summernote({
        placeholder: 'Kata Sambutan',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteProvinsi').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteGubernur').summernote({
        placeholder: 'Kata Sambutan',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteWisata').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteKab').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteMenteri').summernote({
        placeholder: 'Kata Sambutan',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
</script>

@livewireScripts
<script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
<script>
    let data = [];
    const imagesDB = {!! isset($galleries) ? $galleries : '[]' !!};
    const url = window.location.origin;

    function displayImage() {
        return {
            images: [],
            selectedFile(event) {
                this.fileToUrl(event)
            },

            fileToUrl(event) {
                if (!event.target.files.length) return

                let file = event.target.files

                for (let i = 0; i < file.length; i++) {
                    let reader = new FileReader();
                    let srcImg = ''

                    reader.readAsDataURL(file[i]);
                    reader.onload = e => {
                        srcImg = e.target.result
                        this.images = [...this.images, srcImg]
                    };
                }

                // this.sendImage()
            },

            removeImage(index) {
                this.images.splice(index, 1);
                data.splice(index, 1)
                // this.sendImage()
            },
            
        }
    }

    function displayThumbnail() {
        // console.log({!! isset($galleries) ? $galleries : '[]' !!})
        return {
            images: [],

            selectedFile(event) {
                this.fileToUrl(event)
            },

            fileToUrl(event) {
                if (!event.target.files.length) return

                let file = event.target.files

                for (let i = 0; i < file.length; i++) {
                    let reader = new FileReader();
                    let srcImg = ''

                    reader.readAsDataURL(file[i]);
                    reader.onload = e => {
                        srcImg = e.target.result
                        this.images = [...this.images, srcImg]
                    };
                }
            },

            removeImage(index) {
                this.images.splice(index, 1);
            }
        }
    }

    document.addEventListener("alpine:init",()=>{
        Alpine.store("daataAvailable",{
            isDataAvailable:data.length > 0 ? true : false,
            select2_prop:''
        })
    })

    function showToasError(message){
        console.log(message);
        
        document.getElementById("error-gallery").classList("hidden");
        document.getElementById("error-gallery").classList("flex");
        
        $(`<span id="gallery-error-message">${message}</span>`);
        
        setTimeout(() => {
            document.getElementById("error-gallery").add("hidden");
            $(".message-error").remove();
            document.getElementById("error-gallery").remove("hidden");
            document.getElementById("gallery-error-message").remove("hidden");
        }, 5000);
    }

    function displayGalleries() {
        // console.log({!! isset($galleries) ? $galleries : '[]' !!})
        let array =[];
        imagesDB.forEach(element=>{
            console.log(element.image)
            array.push(`${url}/storage/gallery/${element.image}`)
        })
        return {
            images:array,

            selectedFile(event) {
                this.fileToUrl(event)
            },

            fileToUrl(event) {
                if (!event.target.files.length) return

                let file = event.target.files

                for (let i = 0; i < file.length; i++) {
                    let reader = new FileReader();
                    let srcImg = ''

                    reader.readAsDataURL(file[i]);
                    reader.onload = e => {
                        srcImg = e.target.result
                        this.images = [...this.images, srcImg]
                    };

                    data = [...data, file[i]]
                }

                this.sendImage()
            },

            removeImage(index) {
                this.images.splice(index, 1);
            },
            sendImage(){
                let fd = new FormData();
                console.log(fd)
                for (const file of data){
                    if(typeof file === 'object'){
                        fd.append('images_gallery[]',file ,file.name)
                    }
                    if(typeof file === 'string'){
                        fd.append('saved_gallery[]',file)
                    }
                }

                console.log(data)

                $.ajaxSetup({
                    headers:{
                        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                    }
                })

                $.ajax({
                    method:'POST',
                    url:"{{route('wisata.galeri.edit',$wisataShow->id)}}",
                    data:fd,
                    processData:false,
                    contentType:false,
                    beforeSend:function(){

                    },
                    success:function(msg){
                        console.log(msg);
                        if(msg.status === 0){
                            showToasError(msg.message)
                        }

                        if(msg.status === 1){
                            Alpine.store("dataAvailable").isDataAvailable = true
                            console.log(msg)
                        }
                    },
                    error:function(data){
                        console.log('error:',data)
                    },
                    complate:function(){

                    }
                })
            }
        }
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $("#desti").on('change', function () {
            $(".destindonesia").hide();
            $("#" + $(this).val()).fadeIn(700);
        }).change();
    });
</script>
</body>
</html>
