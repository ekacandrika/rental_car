<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    
    {{-- jquery js --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    {{-- summernote --}}
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    
    {{-- tag input bootstrap --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
        rel="stylesheet" />
    
    {{-- select 2 --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    
    {{-- google fonts --}}
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    
    {{-- sweetalert --}}
    <script src=" https://cdn.jsdelivr.net/npm/sweetalert2@11.7.18/dist/sweetalert2.all.min.js "></script>
    <link href=" https://cdn.jsdelivr.net/npm/sweetalert2@11.7.18/dist/sweetalert2.min.css " rel="stylesheet">
    
    {{-- data tables --}}
    {{-- <script src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script> --}}
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/dataTables.tailwindcss.min.js"></script>
    <!-- https://cdn.datatables.net/1.13.6/css/dataTables.tailwindcss.min.css -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/dataTables.tailwindcss.min.css">
    
    <!-- Scripts -->   
    <script>
        $(document).ready(function(){
            console.log($('#wisata-table'));     
            $('#wisata-table').DataTable();       
        })
    </script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }

        .bootstrap-tagsinput .tag {
            margin-right: 2px;
            color: #ffffff;
            background: #2196f3;
            padding: 3px 7px;
            border-radius: 3px;
        }
        
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>

    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10 h-full">


                <div class=" p-5 border rounded-md bg-white">
                    <p class="text-4xl font-semibold my-2">List Wisata</p>

                    <table class="table-auto w-full text-center border-separate border border-slate-500" id="wisata-table">
                        <thead>
                            <tr>
                                <th class="border border-slate-600">No</th>
                                <th class="border border-slate-600">Judul Objek</th>
                                <th class="border border-slate-600">Header</th>
                                <th class="border border-slate-600">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($wisataDestindonesias as $key=>$objDes)
                            <tr>
                                <td class="border border-slate-600">{{1+$key++}}</td>
                                <td class="border border-slate-600">{{$objDes->namaWisata}}</td>
                                <td class="border border-slate-600 grid place-items-center"><img class="w-24 h-24"
                                        src="{{Storage::url($objDes->header)}}" alt=""></td>
                                <td class="border border-slate-600">
                                    <a href="{{route('wisataShow', $objDes->slug)}}" class="">Show</a>
                                    {{-- <form action="{{ route('newsletter.destroy',$news->id) }}" method="POST">--}}
                                        <a class="btn btn-accent"
                                            href="{{ route('wisata.edit',$objDes->id) }}">Edit</a>
                                        {{-- <a class="btn btn-primary" --}} {{--
                                            href="{{ route('newsletter.edit',$news->id) }}">Edit</a>--}}
                                        {{-- <a href="{{route('newsletter.show', $news->slug)}}">Open</a>--}}
                                        <form action="{{route('wisata.delete',$objDes->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-accent">Delete</button>
                                        </form>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- {{ $wisataDestindonesias->links() }} --}}
                </div>

                {{--Objek--}}

                <form method="POST" action="{{ route('wisata.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div id="pulau" class="my-4 border rounded-md bg-white">
                        {{-- daerah select--}}
                        <div class="p-6">
                            <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Wisata</label>

                            <div class="pulau box ">
                                <div class="form-group">
                                    <strong>Pulau:</strong>
                                    <input type="hidden" value="" name="pulau_id" id="valuePulau">
                                    <select id='myselect' name="pulau_id select2"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                        >
                                        <option value="">Select An Option</option>
                                        @foreach($pulaus as $pulau)
                                        {{-- @foreach($news->tags as $tag)--}}
                                        <option value="{{$pulau->id}}">{{$pulau->island_name}}</option>
                                        {{-- @endforeach--}}
                                        @endforeach
                                    </select>
                                    @error('pulau_id')
                                    <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Pulau wajib dipilih*</span></p>
                                    @enderror
                                </div>
                            </div>
                            <div class="provinsi box">
                                <div class="form-group">
                                    <strong>Provinsi:</strong>
                                    <input type="hidden" value="" name="provinsi_id" id="valueProvinsi">
                                    <select id='myselect2' name="select2 provinsi_id" type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                        >
                                        <option value="">Select An Option</option>
                                        @foreach($provinsis as $provinsi)
                                        {{-- @foreach($news->tags as $tag)--}}
                                        <option value="{{$provinsi->id}}">{{$provinsi->name}}</option>
                                        {{-- @endforeach--}}
                                        @endforeach
                                    </select>
                                    @error('provinsi_id')
                                    <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Provisi wajib dipilih*</span></p>
                                    @enderror
                                </div>
                            </div>
                            <div class="kabupaten box">
                                <div class="form-group">
                                    <strong>kabupaten:</strong>
                                    <input type="hidden" value="" name="kabupaten_id" id="valueKabupaten">
                                    <select id='myselect3' name="select2 kabupaten_id" type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                        >
                                        <option value="">Select An Option</option>
                                        @foreach($kabupatens as $kabupaten)
                                        {{-- @foreach($news->tags as $tag)--}}
                                        <option value="{{$kabupaten->id}}">{{$kabupaten->name}}</option>
                                        {{-- @endforeach--}}
                                        @endforeach
                                    </select>
                                    @error('kabupaten_id')
                                    <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Kabupaten wajib dipilih*</span></p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        {{-- slug--}}
                        <div class="p-6">
                            <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Slug</label>
                            <input name="title" type="text" id="text"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                placeholder="Nama Pulau / Nama Provinsi / Nama Kota">
                            @error('title')
                            <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Slug wajib dipilih*</span></p>
                            @enderror    
                        </div>
                        {{-- nama objek--}}
                        <div class="p-6">
                            <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Nama Objek
                                Wisata</label>
                            <input name="namaWisata" type="text" id="text"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                placeholder="Nama Objek Wisata">
                                @error('namaWisata')
                                <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Nama Objek Wisata wajib diisi*</span></p>
                                @enderror         
                        </div>

                        {{-- Thumbnail --}}
                        <div class="p-6" x-data="displayImage()">
                            <p class="font-bold text-xl">Thumbnail</p>
                            <input class="mt-4" type="file" name="thumbnail" accept="image/*" @change="selectedFile">
                            <template x-if="images.length < 1">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                            <template x-if="images.length >= 1">
                                <div class="flex">
                                    <template x-for="(image, index) in images" :key="index">
                                        <div class="flex justify-center items-center">
                                            <img :src="image"
                                                class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                :alt="'upload'+index">
                                            <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                    width="25px" @click="removeImage(index)">
                                            </button>
                                        </div>
                                    </template>
                                </div>
                            </template>
                            @error('thumbnail')
                            <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Foto Thumbnail wajib diisi*</span></p>
                            @enderror 
                        </div>

                        {{-- Header --}}
                        <div class="p-6" x-data="displayImage()">
                            <p class="font-bold text-xl">Header</p>
                            <input class="mt-4" type="file" name="header" accept="image/*" @change="selectedFile">
                            <template x-if="images.length < 1">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                            <template x-if="images.length >= 1">
                                <div class="flex">
                                    <template x-for="(image, index) in images" :key="index">
                                        <div class="flex justify-center items-center">
                                            <img :src="image"
                                                class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                :alt="'upload'+index">
                                            <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                    width="25px" @click="removeImage(index)">
                                            </button>
                                        </div>
                                    </template>
                                </div>
                            </template>
                            @error('header')
                            <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Foto Header wajib diisi*</span></p>
                            @enderror
                        </div>

                        {{-- Deskripsi --}}
                        <div class="p-6">
                            <p class="font-bold text-xl">Deskripsi</p>
                            <div class="bg-white pt-4">
                                <textarea name="deskripsi" id="summernotePulau"></textarea>
                            </div>
                            @error('deskripsi')
                            <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Deskripsi wajib diisi*</span></p>
                            @enderror
                        </div>

                        {{--Informasi--}}
                        <div class="p-6">
                            <p class="font-bold text-xl">Informasi</p>
                            <div class="px-4 grid grid-cols-2">

                                <div class="mt-4">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333]">Kategori</label>
                                    <input name="kategori" type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Kategori">
                                        @error('kategori')
                                        <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Kategori wajib diisi*</span></p>
                                        @enderror    
                                </div>

                                <div class="mt-4">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Harga
                                        Tiket</label>
                                    <input name="hargaTiket" type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Harga Tiket">
                                        @error('hargaTiket')
                                        <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Harga wajib diisi*</span></p>
                                        @enderror 
                                </div>

                                <div class="mt-4">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Hari</label>
                                    <input name="hari" type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Hari Buka">
                                        @error('hari')
                                        <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Hari wajib diisi*</span></p>
                                        @enderror     
                                </div>

                                <div class="mt-4">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Jam
                                        Operasional</label>
                                    <input name="jamOperasional" type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Jam Operasional Objek Wisata">
                                        @error('jamOperasional')
                                        <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Hari wajib diisi*</span></p>
                                        @enderror     
                                </div>

                                <div class="mt-4">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Akses
                                        Transportasi</label>
                                    <input name="transportasi" type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Sebutkan Akses Transportasi">
                                        @error('transportasi')
                                        <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Akses Transportasi wajib diisi*</span></p>
                                        @enderror 
                                </div>
                            </div>
                        </div>

                        {{-- Gallery --}}
                        <div class="p-6" x-data="displayImage()">
                            <p class="font-bold text-xl">Gallery Foto Objek Wisata</p>
                            <label class="m-2">Images</label>
                            <input class="pt-4" type="file" accept="image/*" @change="selectedFile" name="images[]"
                                multiple>
                            <template x-if="images.length < 1">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                            <template x-if="images.length >= 1">
                                <div class="flex">
                                    <template x-for="(image, index) in images" :key="index">
                                        <div class="flex justify-center items-center">
                                            <img :src="image"
                                                class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                :alt="'upload'+index">
                                            <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                    width="25px" @click="removeImage(index)">
                                            </button>
                                        </div>
                                    </template>
                                </div>
                            </template>
                            @error('images')
                            <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Akses Transportasi wajib diisi*</span></p>
                            @enderror
                        </div>

                        {{-- Maps --}}
                        <div class="p-6">
                            <label for="text" class="block mb-2 text-xl font-bold text-[#333333]">Link Peta Google
                                Maps</label>
                            <input name="embed_maps" type="text" id="text"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                placeholder="<iframe src=”https://www.map">
                            @error('transportasi')
                            <p class="font-bold text-xl mt-1 mb-2"><span class="text-red-500">Akses Transportasi wajib diisi*</span></p>
                            @enderror  
                        </div>

                        {{-- tags--}}

                        <div class="form-group p-6">

                            <label>Tags Custom:</label>

                            <br />

                            <input data-role="tagsinput" type="text" name="tags">

                            {{-- <div class="form-group">--}}
                                {{-- <strong>Tag Autocomplete:</strong>--}}
                                {{-- <select id='myselect' name="select2" multiple>--}}
                                    {{-- <option value="">Select An Option</option>--}}
                                    {{-- @foreach($newsletters as $news)--}}
                                    {{-- @foreach($news->tags as $tag)--}}
                                    {{-- <option value="{{$tag->name}}">{{$tag->name}}</option>--}}
                                    {{-- @endforeach--}}
                                    {{-- @endforeach--}}
                                    {{-- </select>--}}
                                {{-- </div>--}}
                            @if ($errors->has('tags'))

                            <span class="text-danger">{{ $errors->first('tags') }}</span>

                            @endif

                        </div>
                        <div class="p-6">
                            <button class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
    <script type="text/javascript">
        $('#myselect').select2({
            width: '100%',
            placeholder: "Select an Option",
            allowClear: true
        })
        $('#myselect').on("change", function (e) {
            var data = $("#myselect").val()
            document.getElementById('valuePulau').setAttribute('value', data);
            console.log($("#myselect").val())
        })

        $('#myselect2').select2({
            width: '100%',
            placeholder: "Select an Option",
            allowClear: true
        });
        $('#myselect2').on("change", function (e) {
            var data = $("#myselect2").val()
            document.getElementById('valueProvinsi').setAttribute('value', data);
            console.log($("#myselect2").val())
        })
        $('#myselect3').select2({
            width: '100%',
            placeholder: "Select an Option",
            allowClear: true
        });
        $('#myselect3').on("change", function (e) {
            var data = $("#myselect3").val()
            document.getElementById('valueKabupaten').setAttribute('value', data);
            console.log($("#myselect3").val())
        })
    </script>
    <script>
        $('#summernotePulau').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteWalikota').summernote({
            placeholder: 'Kata Sambutan',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteProvinsi').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteGubernur').summernote({
            placeholder: 'Kata Sambutan',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteWisata').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteKab').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteMenteri').summernote({
            placeholder: 'Kata Sambutan',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
    </script>

    @livewireScripts

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                images: [],

                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event) {
                    if (!event.target.files.length) return

                    let file = event.target.files

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                        };
                    }
                },

                removeImage(index) {
                    this.images.splice(index, 1);
                }
            }
        }
    </script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script> --}}
    <script>
        $(document).ready(function () {
            
            $("#desti").on('change', function () {
                $(".destindonesia").hide();
                $("#" + $(this).val()).fadeIn(700);
            }).change();
        });
    </script>
</body>

</html>