<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    {{--summernote--}}
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    {{-- tag input bootstrap--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
        rel="stylesheet" />
    {{-- select2--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
    {{--jquery slim js--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    {{-- https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js --}}
    {{-- <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> --}}
    {{-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script> --}}
    {{-- summernote js --}}
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    {{-- ajax --}}
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>--}}
    {{-- select2 js --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    {{-- sweetalert2 --}}
    <script src=" https://cdn.jsdelivr.net/npm/sweetalert2@11.7.18/dist/sweetalert2.all.min.js "></script>
    <link href=" https://cdn.jsdelivr.net/npm/sweetalert2@11.7.18/dist/sweetalert2.min.css " rel="stylesheet">
    
    <script src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script>
    {{-- 
    https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js
    https://cdn.datatables.net/1.13.5/js/dataTables.bootstrap5.min.js
 --}}
    <script src="https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.5/js/dataTables.bootstrap5.min.js"></script>
    {{-- <link  href="https://cdn.datatables.net/1.13.5/js/dataTables.bootstrap5.min.js" rel="stylesheet"> --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    {{-- 
        <!-- Add jQuery (required for DataTables) -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    
        <script src="https://cdn.datatables.net/1.11.0/js/dataTables.bootstrap5.min.js"></script>
        <!-- Add the DataTables JavaScript -->
        <link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    --}}
       
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }

        .bootstrap-tagsinput .tag {
            margin-right: 2px;
            color: #ffffff;
            background: #2196f3;
            padding: 3px 7px;
            border-radius: 3px;
        }
        .note-modal-backdrop{
            opacity:0;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>

    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>

        <div class="z-0 col-start-3 col-end-11">
            <div class="h-full p-5 pl-10 pr-10">
                <div class="p-5 bg-white border rounded-md" x-data="{status:0}">
                    {{-- <div class="flex grid grid-cols-2 py-5 justify-items-end items-end hidden">
                        <div class="w-[100px] mx-5">
                            <select
                                class="px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-[150px] ml-[271px] rounded-md text-sm focus:ring-1 text-center text-base" 
                                id="filterObjekLokasi" name="objek"
                            >
                                <option value class="text-center" selected>Pilih</option>
                                <option value="Pulau">Pulau</option>
                                <option value="Provinsi">Provinsi</option>
                                <option value="Kabupaten">Kota/kabupaten</option>
                            </select>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>
                        </div>
                        <div class="flex items-center col-start-4 col-span-1">
                            <label for="simple-search" class="sr-only">Search</label>
                            <div class="relative w-[96%]">
                                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor"
                                        viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                                            clip-rule="evenodd"></path>
                                    </svg>
                                </div>
                                <input type="text" id="objek-search"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Search" name="title" required>
                            </div>
                            &nbsp;
                            <button type="button" id="searchObject" class="bg-blue-700 text-white text-sm px-5 py-2.5 font-medium border-gray-900 rounded-md">Search</button>
                        </div>
                    </div> --}}
                    <p class="my-2 text-4xl font-semibold">Place</p>
                    {{-- w-full text-center border border-separate table-auto border-slate-500 --}}
                    <table class="w-full text-center border border-separate table-auto border-slate-500" id="users-table">
                        <thead>
                            <tr>
                                <th class="border border-slate-600">No</th>
                                <th class="border border-slate-600">Judul Objek</th>
                                <th class="border border-slate-600">Header</th>
                                <th class="border border-slate-600">Jenis Objek</th>
                                <th class="border border-slate-600">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pulauDestindonesias as $key=>$objDes)
                            <tr>
                                <td class="border border-slate-600">{{1+$key++}}</td>
                                <td class="border border-slate-600">{{$objDes->nama_pulau}}</td>
                                <td class="grid border border-slate-600 place-items-center"><img class="w-24 h-24"
                                        src="{{Storage::url($objDes->header)}}" alt=""></td>

                                <td class="border border-slate-600">{{$objDes->objek}}</td>
                                <td class="border border-slate-600">
                                    @if($objDes->pulau_id !== null)
                                        <a href="{{route('pulauShow', $objDes->slug)}}" class="btn">Show</a>
                                    @elseif($objDes->kabupaten_id !== null)
                                        <a href="{{route('kabupatenShow', $objDes->slug)}}" class="btn">Show</a>
                                    @elseif($objDes->provinsi_id !== null)
                                        <a href="{{route('provinsiShow', $objDes->slug)}}" class="btn">Show</a>
                                    @endif
                                    {{-- <form action="{{ route('newsletter.destroy',$news->id) }}" method="POST">--}}

                                        <a class="btn" href="{{ route('objek.edit',$objDes->id) }}">Edit</a>
                                        {{-- <a href="{{route('newsletter.show', $news->slug)}}">Open</a>--}}

                                    <form method="POST" action="{{route('objek.delete',$objDes->id)}}">
                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- {{ $pulauDestindonesias->links()}} --}}
                </div>


                {{--Objek--}}
                <form method="POST" action="{{ route('objek.store') }}" enctype="multipart/form-data" id="submitObjectLocation">
                    @csrf
                    <div id="pulau" class="my-4 bg-white border rounded-md">
                        {{-- daerah select--}}
                        <div class="p-6">
                            <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Objek Created</label>
                            <select name="objek" type="text" id="objek"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                <option>-Pilih Area-</option>
                                <option value="pulau">Pulau</option>
                                <option value="provinsi">Provinsi</option>
                                <option value="kabupaten">Kabupaten</option>
                            </select>
                            <div class="box pulau hidden" id="pulau_index">
                                <div class="form-group">
                                    <strong>Pulau:</strong>
                                    <input type="hidden" value="" name="pulau_id" id="valuePulau">
                                    <select id='myselect' name="pulau_id select2"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                        >
                                        <option value="">Select An Option</option>
                                        @foreach($pulaus as $pulau)
                                        {{-- @foreach($news->tags as $tag)--}}
                                        <option value="{{$pulau->id}}">{{$pulau->island_name}}</option>
                                        {{-- @endforeach--}}
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="box provinsi hidden">
                                <div class="form-group">
                                    <strong>Provinsi:</strong>
                                    <input type="hidden" value="" name="provinsi_id" id="valueProvinsi">
                                    <select id='myselect2' name="select2 provinsi_id" type="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                        >
                                        <option value="">Select An Option</option>
                                        @foreach($provinsis as $provinsi)
                                        {{-- @foreach($news->tags as $tag)--}}
                                        <option value="{{$provinsi->id}}">{{$provinsi->name}}</option>
                                        {{-- @endforeach--}}
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="box kabupaten hidden">
                                <div class="form-group">
                                    <strong>Kabupaten:</strong>
                                    <input type="hidden" value="" name="kabupaten_id" id="valueKabupaten">
                                    <select id='myselect3' name="select2 kabupaten_id" type="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                        >
                                        <option value="">Select An Option</option>
                                        @foreach($kabupatens as $kabupaten)
                                        {{-- @foreach($news->tags as $tag)--}}
                                        <option value="{{$kabupaten->id}}">{{$kabupaten->name}}</option>
                                        {{-- @endforeach--}}
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        {{-- slug--}}
                        <div class="p-6">
                            <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Slug</label>
                            <input name="title" type="text" id="slug"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                placeholder="Nama Pulau" value="{!! old('title')!!}">
                            @error('title')
                            <p class="font-bold text-xl mt-1"><span class="text-red-500">Slug wajib diisi*</span></p>
                            @enderror
                        </div>
                        {{-- nama pulau--}}
                        <div class="p-6">
                            <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Nama Destindonesia (Pulau/Kota/Daerah)</label>
                            <input name="nama_pulau" type="text" id="nama_pulau"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                placeholder="Nama Pulau" value="{!! old('nama_pulau')!!}">
                            @error('nama_pulau')
                            <p class="font-bold text-xl mt-1"><span class="text-red-500">Nama Destindonesia wajib diisi*</span></p>
                            @enderror
                        </div>

                        {{-- Thumbnail --}}
                        <div class="p-6" x-data="displayImage()">
                            <p class="text-xl font-bold">Thumbnail</p>
                            <input class="mt-4" type="file" name="thumbnail" accept="image/*" @change="selectedFile">
                            <template x-if="images.length < 1">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                            @error('thumbnail')
                            <p class="font-bold text-xl mt-1"><span class="text-red-500">Thumbnail wajib diisi*</span></p>
                            @enderror
                            <template x-if="images.length >= 1">
                                <div class="flex">
                                    <template x-for="(image, index) in images" :key="index">
                                        <div class="flex items-center justify-center">
                                            <img :src="image"
                                                class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                :alt="'upload'+index">
                                            <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                    width="25px" @click="removeImage(index)">
                                            </button>
                                        </div>
                                    </template>
                                </div>
                            </template>
                        </div>


                        {{-- logo daerah--}}
                        <div class="px-4 pt-4" x-data="displayImage()">
                            <p class="text-lg font-semibold">Logo Daerah</p>
                            <div class="flex justify-items-center">
                                <template x-if="images.length < 1">
                                    <div
                                        class="border flex justify-center items-center rounded-full border-gray-300 bg-gray-200 w-[100px] h-[100px]">
                                        <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                            width="20px" height="20px">
                                    </div>
                                </template>
                                <input name="logo_daerah" type="file" accept="image/*" @change="selectedFile">
                                <template x-if="images.length >= 1">
                                    <div class="flex">
                                        <template x-for="(image, index) in images" :key="index">
                                            <div class="flex items-center justify-center">
                                                <img :src="image"
                                                    class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                    :alt="'upload'+index">
                                                <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                                    <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                        alt="" width="25px" @click="removeImage(index)">
                                                </button>
                                            </div>
                                        </template>
                                    </div>
                                </template>
                            </div>
                        </div>

                        {{-- Header --}}
                        <div class="p-6" x-data="displayImage()">
                            <p class="text-xl font-bold">Header</p>
                            <input class="mt-4" type="file" name="header" accept="image/*" @change="selectedFile">
                            <template x-if="images.length < 1">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                            @error('thumbnail')
                            <p class="font-bold text-xl mt-1"><span class="text-red-500">Thumbnail wajib diisi*</span></p>
                            @enderror
                            <template x-if="images.length >= 1">
                                <div class="flex">
                                    <template x-for="(image, index) in images" :key="index">
                                        <div class="flex items-center justify-center">
                                            <img :src="image"
                                                class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                :alt="'upload'+index">
                                            <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                    width="25px" @click="removeImage(index)">
                                            </button>
                                        </div>
                                    </template>
                                </div>
                            </template>
                        </div>

                        {{-- Sambutan --}}
                        <div class="p-6">
                            <p class="text-xl font-bold" id="sambutan_pemimpin">Sambutan Pemimpin</p>

                            <div class="px-4 pt-4" x-data="displayImage()">
                                <p class="text-lg font-semibold" id="foto_pemimpin">Foto Pemimpin</p>
                                <div class="flex justify-items-center">
                                    <template x-if="images.length < 1">
                                        <div
                                            class="border flex justify-center items-center rounded-full border-gray-300 bg-gray-200 w-[100px] h-[100px]">
                                            <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                                width="20px" height="20px">
                                        </div>
                                    </template>
                                    <input name="foto_menteri" type="file" accept="image/*" @change="selectedFile">
                                    
                                    <template x-if="images.length >= 1">
                                        <div class="flex">
                                            <template x-for="(image, index) in images" :key="index">
                                                <div class="flex items-center justify-center">
                                                    <img :src="image"
                                                        class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                        :alt="'upload'+index">
                                                    <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                                        <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                            alt="" width="25px" @click="removeImage(index)">
                                                    </button>
                                                </div>
                                            </template>
                                        </div>
                                    </template>
                                </div>
                                @error('foto_menteri')
                                <p class="font-bold text-xl mt-1"><span class="text-red-500">Foto Pemimpin/Menteri wajib diisi*</span></p>
                                @enderror
                            </div>


                            <div class="px-4 pt-4">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]" id="nama_pemimpin">Nama
                                    Pemimpin</label>
                                <input name="nama_menteri" type="text" id="nama_menteri"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                    placeholder="Nama Menteri"  value="{!! old('nama_menteri')!!}">
                                @error('nama_menteri')
                                <p class="font-bold text-xl mt-1"><span class="text-red-500">Nama Pemimpin/Menteri wajib diisi*</span></p>
                                @enderror
                            </div>
                            <div class="max-w-6xl px-4 pt-4">
                                <p class="text-sm font-bold">Kata Sambutan Pemimpin</p>
                                <div class="bg-white">
                                    <textarea name="kata_sambutan" id="summernoteMenteri">{!! old('kata_sambutan')!!}</textarea>
                                </div>
                                @error('kata_sambutan')
                                <p class="font-bold text-xl mt-1"><span class="text-red-500">Kata Sambutan Pemimpin/Menteri wajib diisi*</span></p>
                                @enderror
                            </div>
                        </div>

                        {{-- Deskripsi --}}
                        <div class="max-w-6xl p-6">
                            <p class="text-xl font-bold">Deskripsi Place</p>
                            <div class="pt-4 bg-white">
                                <textarea name="deskripsi" id="summernotePulau"></textarea>
                            </div>
                            @error('deskripsi')
                                <p class="font-bold text-xl mt-1"><span class="text-red-500">Deskripsi wajib diisi*</span></p>
                            @enderror
                        </div>
                        {{-- Deskripsi Thumbnail --}}
                        <div class="max-w-6xl p-6">
                            <p class="font-bold text-xl">Deskripsi Thumbnail <span class="text-red-500">* maksimal 199 karakter</span></p>
                            <div class="bg-white pt-4">
                                <textarea name="desk_thumbnail"
                                    id="summernotedeskThumbnail">{!! old('desk_thumbnail')!!}</textarea>
                            </div>
                            <span id="limitCharacter"></span>
                        </div>
                        {{--Informasi--}}
                        <div class="p-6">
                            <p class="text-xl font-bold">Informasi</p>
                            <div class="grid grid-cols-2 px-4">

                                <div class="mt-4">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333]">Ibukota</label>
                                    <input name="ibukota" type="text" id="ibukota"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Nama Ibukota" value="{!! old('ibukota')!!}">
                                    @error('ibukota')
                                        <p class="font-bold text-xl mt-1"><span class="text-red-500">Ibukota wajib diisi*</span></p>
                                    @enderror
                                </div>

                                <div class="mt-4">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333]">Bandara</label>
                                    <input name="bandara" type="text" id="bandara"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Nama Bandara" value="{!! old('bandara')!!}">
                                    @error('bandara')
                                        <p class="font-bold text-xl mt-1"><span class="text-red-500">Bandara wajib diisi*</span></p>
                                    @enderror
                                </div>

                                <div class="mt-4">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Pelabuhan
                                        Laut</label>
                                    <input name="pelabuhan" type="text" id="pelabuhan"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Nama Pelabuhan" value="{!! old('pelabuhan')!!}">
                                    @error('pelabuhan')
                                        <p class="font-bold text-xl mt-1"><span class="text-red-500">Pelabuhan wajib diisi*</span></p>
                                    @enderror
                                </div>

                                <div class="mt-4">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Terminal
                                        Darat</label>
                                    <input name="terminal" type="text" id="terminal"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Nama Terminal Darat" value="{!! old('terminal')!!}">
                                    @error('terminal')
                                        <p class="font-bold text-xl mt-1"><span class="text-red-500">Terminal wajib diisi*</span></p>
                                    @enderror
                                </div>

                                <div class="mt-4">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Akses
                                        Transportasi</label>
                                    <input name="transportasi" type="text" id="transportasi"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Sebutkan Akses Transportasi" value="{!! old('transportasi')!!}">
                                    @error('transportasi')
                                        <p class="font-bold text-xl mt-1"><span class="text-red-500">Akses Transportasi wajib diisi*</span></p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        {{-- Gallery --}}
                        <div class="p-6" x-data="displayImage()">
                            <p class="text-xl font-bold">Gallery Foto Destindonesia</p>
                            <label class="m-2">Images</label>
                            <input class="pt-4" type="file" accept="image/*" @change="selectedFile" name="images[]"
                                multiple>
                            <template x-if="images.length < 1">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                            @error('images')
                                <p class="font-bold text-xl mt-1"><span class="text-red-500">Gallery Foto Destindonesia wajib diisi*</span></p>
                            @enderror
                            <template x-if="images.length >= 1">
                                <div class="flex">
                                    <template x-for="(image, index) in images" :key="index">
                                        <div class="flex items-center justify-center">
                                            <img :src="image"
                                                class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                :alt="'upload'+index">
                                            <button class="absolute mx-2 translate-x-12 -translate-y-14">
                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                    width="25px" @click="removeImage(index)">
                                            </button>
                                        </div>
                                    </template>
                                </div>
                            </template>
                        </div>

                        {{-- Maps --}}
                        <div class="p-6">
                            <label for="text" class="block mb-2 text-xl font-bold text-[#333333]">Link Peta Google
                                Maps</label>
                            <input name="embed_maps" type="text" id="embed_maps"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                placeholder="<iframe src=”https://www.map" value="{!! old('embed_maps')!!}">
                            @error('embed_maps')
                                <p class="font-bold text-xl mt-1"><span class="text-red-500">Maps wajib diisi*</span></p>
                            @enderror
                        </div>

                        {{--tags--}}
                        <div class="p-6 form-group">

                            <label>Tags Custom:</label>

                            <br />

                            <input data-role="tagsinput" type="text" name="tags">

                            {{-- <div class="form-group">--}}
                                {{-- <strong>Tag Autocomplete:</strong>--}}
                                {{-- <select id='myselect' name="select2" multiple>--}}
                                    {{-- <option value="">Select An Option</option>--}}
                                    {{-- @foreach($newsletters as $news)--}}
                                    {{-- @foreach($news->tags as $tag)--}}
                                    {{-- <option value="{{$tag->name}}">{{$tag->name}}</option>--}}
                                    {{-- @endforeach--}}
                                    {{-- @endforeach--}}
                                    {{-- </select>--}}
                                {{-- </div>--}}
                            @if ($errors->has('tags'))

                            <span class="text-danger">{{ $errors->first('tags') }}</span>

                            @endif

                        </div>
                        <div class="p-6">
                            <button type="submit" class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
    <script>
       
    </script>
    <script type="text/javascript">
        $('#myselect').select2({
            width: '100%',
            placeholder: "Select an Option",
            allowClear: true
        })
        $('#myselect').on("change", function (e) {
            var data = $("#myselect").val()
            document.getElementById('valuePulau').setAttribute('value', data);
            console.log($("#myselect").val())
        })

        $('#myselect2').select2({
            width: '100%',
            placeholder: "Select an Option",
            allowClear: true
        });
        $('#myselect2').on("change", function (e) {
            var data = $("#myselect2").val()
            document.getElementById('valueProvinsi').setAttribute('value', data);
            console.log($("#myselect2").val())
        })
        $('#myselect3').select2({
            width: '100%',
            placeholder: "Select an Option",
            allowClear: true
        });
        $('#myselect3').on("change", function (e) {
            var data = $("#myselect3").val()
            document.getElementById('valueKabupaten').setAttribute('value', data);
            console.log($("#myselect3").val())
        })

        // $("#filterObjekLokasi").on("change",function(){
        //     var filter = $(this).val();

        //     if(filter){
        //         // alert(filter)
        //         document.location.replace('/dashboard/admin/kamtuu/place/objek?objek='+filter);
        //     }
        // })

        // $("#searchObject").on("click",function(){
        //     var search = $("#objek-search").val();

        //     if(search){
        //         document.location.replace('/dashboard/admin/kamtuu/place/objek?title='+search);
        //     }
        // })

    </script>
    <script>
        $(document).ready(function () {
            // $("#pulau_index").hide();
            // $(".provinsi").hide();
            // $(".kabupaten").hide()
            // $('#pulau_id').hide();
            // $('#provinsi_id').hide();
            // $('#kabupaten_id').hide();
            
            // $("#text").change(function(){
            //     console.log($(this).val());
            //     if($(this).val()=='pulau'){
            //         $('#pulau_id').show();
            //         $('#provinsi_id').hide();
            //         $('#kabupaten_id').hide();
            //     }
                
            //     if($(this).val()=='provinsi'){
            //         $('#provinsi_id').show();
            //         $('#pulau_id').hide();
            //         $('#kabupaten_id').hide();
            //     }
                
            //     if($(this).val()=='kabupaten'){
            //         $('#pulau_id').show();
            //         $('#provinsi_id').hide();
            //         $('#kabupaten_id').hide();
            //     }
            // });
            
            if($("#objek").val() =='pulau'){
                $(".kabupaten").addClass("hidden")
                $(".provinsi").addClass("hidden");
                $("#pulau_index").show();
                
                $("#sambutan_pemimpin").html("Sambutan Menteri")
                $("#foto_pemimpin").html("Foto Menteri")
                $("#nama_pemimpin").html("Nama Menteri")
            }

            if($("#objek").val() =='provinsi'){
                $("#pulau_index").hide();
                $(".kabupaten").hide()
                $(".provinsi").show();

                $("#sambutan_pemimpin").html("Sambutan Gubernur")
                $("#foto_pemimpin").html("Foto Gubernur")
                $("#nama_pemimpin").html("Nama Gubernur")
            }

            if($("#objek").val() =='kabupaten'){
                $("#pulau_index").hide();
                $(".provinsi").hide();
                $(".kabupaten").show()

                $("#sambutan_pemimpin").html("Sambutan Bupati/Walikota")
                $("#foto_pemimpin").html("Foto Bupati/Walikota")
                $("#nama_pemimpin").html("Nama Bupati/Walikota")
            }

            $("#objek").change(function () {
                var optionValue = $(this).val();
                // console.log(optionValue)
                if (optionValue == 'pulau') {
                    console.log('pulau')
                    $(".kabupaten").hide()
                    $(".provinsi").hide();
                    $("#pulau_index").show();

                    $("#sambutan_pemimpin").html("Sambutan Menteri")
                    $("#foto_pemimpin").html("Foto Menteri")
                    $("#nama_pemimpin").html("Nama Menteri")
                }
                

                if(optionValue == 'provinsi') {
                    $("#pulau_index").hide();
                    $(".kabupaten").hide()
                    $(".provinsi").show();

                    $("#sambutan_pemimpin").html("Sambutan Gubernur")
                    $("#foto_pemimpin").html("Foto Gubernur")
                    $("#nama_pemimpin").html("Nama Gubernur")
                }
                // else{
                //     $("#pulau_index").hide();
                //     $(".kabupaten").hide()
                // }

                if(optionValue == 'kabupaten'){
                    $("#pulau_index").hide();
                    $(".provinsi").hide();
                    $(".kabupaten").show()

                    $("#sambutan_pemimpin").html("Sambutan Bupati/Walikota")
                    $("#foto_pemimpin").html("Foto Bupati/Walikota")
                    $("#nama_pemimpin").html("Nama Bupati/Walikota")
                }
                //else
                // {
                //     $("#pulau_index").hide();
                //     $(".provinsi").hide();
                // }
                // else if(optionValue == 'kabupaten'){
                    //     $(".box").not(".kabupaten").hide();
                    //     $(".kabupaten").show();
                    // }
                // $(this).find("select option:selected").each(function () {
                    // var optionValue = $(this).attr("value");
                    // else{
                    //     alert('xasa')
                    //     $(".provinsi").hide();
                    //     $(".pulau").hide();
                    //     $(".kabupaten").hide();
                    // }
                    // else if(optionValue == 'provinsi'){
                    //     alert('text')
                    //     $(".box").not(".provinsi").hide();
                    //     $(".provinsi").show();
                    // }
                    // else if(optionValue == 'kabupaten'){
                    //     $(".box").not(".kabupaten").hide();
                    //     $(".kabupaten").show();
                    // }
                    // else {
                    
                    // }
                // });
            })
            // .change();
        });
    </script>
    <script>
        $('#summernotePulau').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        
        $('#summernoteWalikota').summernote({
            placeholder: 'Kata Sambutan',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteProvinsi').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteGubernur').summernote({
            placeholder: 'Kata Sambutan',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteWisata').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteKab').summernote({
            placeholder: 'Deskripsi',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#summernoteMenteri').summernote({
            placeholder: 'Kata Sambutan',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });

        function deskThumbnail(element, placeholder, max, callback){
            $(element).summernote({
                placeholder,
                tabsize: 2,
                height: 120,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']]
                ],
                callbacks:{
                    onKeydown:function(e){
                        var t = e.currentTarget.innerText;
                        if(t.length >=max){
                            if(e.keyCode != 8){
                                e.preventDefault();
                            }
                        }
                    },
                    onKeyup:function(e){
                        var t = e.currentTarget.innerText;
                        if(typeof callback=='function'){
                            callback(max-t.length)
                        }
                    },
                    onPaste:function(e){
                        var t = e.currentTarget.innerText;
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        var all = t + bufferText;
                        document.execCommand('insertText', false, all.trim().substring(0, 400));
                        if (typeof callbackMax == 'function') {
                            callbackMax(max - t.length);
                        }
                    }
                }
            })
        }

        deskThumbnail('#summernotedeskThumbnail','Deskripsi thumbnail',200,function(max){
            $("#limitCharacter").text('Jumlah karakter '+max)
        });
    </script>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                images: [],

                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event) {
                    if (!event.target.files.length) return

                    let file = event.target.files

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                        };
                    }
                },

                removeImage(index) {
                    this.images.splice(index, 1);
                }
            }
        }
    </script>
    <script>
        $(document).ready(function () {
            
            console.log($("#tables"));
            $('#users-table').DataTable();
            // document.getElementById("tables").DataTable();

            // $("#desti").on('change', function () {
            //     $(".destindonesia").hide();
            //     $("#" + $(this).val()).fadeIn(700);
            // }).change();
        });
    </script>
    <script>

        $(".note-modal-backdrop").css({"opacity":0})

        function alertMsg(msg, e){
            Swal.fire({
                icon: 'error',
                text:msg
            })

            e.preventDefault()
        }

        $("#submitObjectLocation").on("submit",function(e){

            if(!$("#slug").val()){
               console.log($("#slug").val())
                alertMsg('Title harus diisi',e)
            }

            else if(!$("#nama_pulau").val()){
                alertMsg('Nama pulau harus diisi',e)
            }

            else if(!$("#nama_menteri").val()){
                alertMsg('Nama menteri harus diisi',e)
            }

            else if(!$("#summernoteMenteri").val()){
                alertMsg('Kata sambutan harus diisi',e)
            }

            else if(!$("#summernotePulau").val()){
                alertMsg('Deskripsi harus diisi',e)
            }

            else if(!$("#ibukota").val()){
                alertMsg('Ibu kota harus diisi',e)
            }

            else if(!$("#bandara").val()){
                alertMsg('Bandara harus diisi',e)
            }
            
            else if(!$("#pelabuhan").val()){
                alertMsg('Pelabuhan harus diisi',e)
            }

            else if(!$("#terminal").val()){
                alertMsg('Terminal harus diisi',e)
            }

            else if(!$("#transportasi").val()){
                alertMsg('Transportasi harus diisi',e)
            }

            else if(!$("#embed_maps").val()){
                alertMsg('Link peta harus diisi',e)
            }

        });
    </script>

</body>

</html>
