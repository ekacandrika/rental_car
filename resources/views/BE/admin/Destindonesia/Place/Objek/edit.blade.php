<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    {{--summernote--}}
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    {{-- tag input bootstrap--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
        rel="stylesheet" />
    {{-- select2--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
    {{--jquery slim js--}}
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    {{-- summernote js --}}
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    {{-- ajax --}}
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>--}}
    {{-- select2 js --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    {{-- sweetalert2 --}}
    <script src=" https://cdn.jsdelivr.net/npm/sweetalert2@11.7.18/dist/sweetalert2.all.min.js "></script>
    <link href=" https://cdn.jsdelivr.net/npm/sweetalert2@11.7.18/dist/sweetalert2.min.css " rel="stylesheet">
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #idn:checked+#idn {
            display: block;
        }

        #sett:checked+#sett {
            display: block;
        }

        .bootstrap-tagsinput .tag {
            margin-right: 2px;
            color: #ffffff;
            background: #2196f3;
            padding: 3px 7px;
            border-radius: 3px;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>

    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10 h-full">
                {{-- @dump($pulauDestindonesias) --}}
                {{--Objek--}}
                <form method="POST" action="{{ route('objek.update', $pulauDestindonesias->id) }}"
                    enctype="multipart/form-data" id="submitEditObjectLocation">
                    @csrf
                    <div id="object-selection" class="my-4 border rounded-md bg-white">
                        {{-- daerah select--}}
                        <div class="p-6">
                            <label for="objek" class="block mb-2 text-xl font-bold text-[#333333]"></label>
                            <select name="objek" type="text" id="objek"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                <option>-Pilih Area-</option>
                                <option {{ $pulauDestindonesias->objek == 'Pulau' ? 'selected' : '' }}
                                    value="pulau">Pulau</option>
                                <option {{ $pulauDestindonesias->objek == 'Provinsi' ? 'selected' : '' }}
                                    value="provinsi">Provinsi</option>
                                <option {{ $pulauDestindonesias->objek == 'Kabupaten' ? 'selected' : '' }}
                                    value="kabupaten">Kabupaten</option>
                            </select>
                            <div class="pulau box mt-2" id="pulau">
                                <div class="form-group">
                                    <strong>Pulau:</strong>
                                    <input type="hidden" value="" name="pulau_id" id="valuePulau">
                                    <select id='myselect' name="pulau_id select2"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                        >
                                        <option value="">Select An Option</option>
                                        @foreach($pulaus as $pulau)
                                        {{-- @foreach($news->tags as $tag)--}}
                                        <option value="{{$pulau->id}}" {{isset($pulauDestindonesias->pulau_id) ? ($pulauDestindonesias->pulau_id==$pulau->id ? 'selected':''):''}}>{{$pulau->island_name}}</option>
                                        {{-- @endforeach--}}
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="provinsi box mt-2" id="provinsi">
                                <div class="form-group">
                                    <strong>Provinsi:</strong>
                                    {{-- @dump($pulauDestindonesias->provinsi_id) --}}
                                    <input type="hidden" value="" name="provinsi_id" id="valueProvinsi">
                                    <select id='myselect2' name="select2 provinsi_id" type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                        >
                                        <option value="">Select An Option</option>
                                        @foreach($provinsis as $provinsi)
                                        {{-- @foreach($news->tags as $tag)--}}
                                        <option value="{{$provinsi->id}}" {{isset($pulauDestindonesias->provinsi_id) ? ($pulauDestindonesias->provinsi_id == $provinsi->id ? 'selected':''):''}}>{{$provinsi->name}}</option>
                                        {{-- @endforeach--}}
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="kabupaten box mt-2" id="kabupaten">
                                <div class="form-group">
                                    <strong>Provinsi:</strong>
                                    {{-- @dump($pulauDestindonesias->kabupaten_id) --}}
                                    <input type="hidden" value="" name="kabupaten_id" id="valueKabupaten">
                                    <select id='myselect3' name="select2 kabupaten_id" type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">
                                        >
                                        <option value="">Select An Option</option>
                                        @foreach($kabupatens as $kabupaten)
                                        {{-- @foreach($news->tags as $tag)--}}
                                        <option value="{{$kabupaten->id}}" {{isset($pulauDestindonesias->kabupaten_id) ? ($pulauDestindonesias->kabupaten_id == $kabupaten->id ? 'selected':''):''}}>{{$kabupaten->name}}</option>
                                        {{-- @endforeach--}}
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        {{-- slug--}}
                        <div class="p-6">
                            <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Slug</label>
                            <input name="slug" type="input" id="slug"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                placeholder="Nama Pulau" value="{{old('slug', $pulauDestindonesias->slug)}}">
                            {{-- @error('slug')
                                <p class="font-bold text-xl mt-1"><span class="text-red-500">Slug wajib diisi*</span></p>
                            @enderror --}}
                        </div>

                        {{-- nama pulau--}}
                        <div class="p-6">
                            <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Nama Pulau</label>
                            <input name="nama_pulau" type="text" id="nama_pulau"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                placeholder="Nama Pulau"
                                value="{{old('nama_pulau', $pulauDestindonesias->nama_pulau)}}">
                            @error('nama_pulau')
                                <p class="font-bold text-xl mt-1"><span class="text-red-500">Nama Pulau wajib diisi*</span></p>
                            @enderror    
                        </div>

                        {{-- Thumbnail --}}
                        <div class="p-6" x-data="displayThumbnail()">
                            <p class="font-bold text-xl">Thumbnail</p>
                            <input class="mt-4" type="file" name="thumbnail" accept="image/*" @change="selectedFile">
                            <template x-if="images.length < 1">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                    <img src="{{Storage::url($pulauDestindonesias->thumbnail)}}" alt="upload-icons"
                                        width="80px" height="80px">
                                </div>
                            </template>
                            <template x-if="images.length >= 1">
                                <div class="flex">
                                    <template x-for="(image, index) in images" :key="index">
                                        <div class="flex justify-center items-center">
                                            <img :src="image"
                                                class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                :alt="'upload'+index">
                                            <button class="absolute mx-2 translate-x-12 -translate-y-14" type="button">
                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                    width="25px" @click="removeImage(index)">
                                            </button>
                                        </div>
                                    </template>
                                </div>
                            </template>
                        </div>


                        {{-- logo daerah--}}
                        <div class="px-4 pt-4" x-data="displayThumbnail()">
                            <p class="font-semibold text-lg">Logo Daerah</p>
                            <div class="flex justify-items-center">
                                <template x-if="images.length < 1">
                                    <div
                                        class="border flex justify-center items-center rounded-full border-gray-300 bg-gray-200 w-[100px] h-[100px]">
                                        <img src="{{Storage::url($pulauDestindonesias->logo_daerah)}}" alt="No"
                                            width="20px" height="20px">
                                    </div>
                                </template>
                                <input name="logo_daerah" type="file" accept="image/*" @change="selectedFile">
                                <template x-if="images.length >= 1">
                                    <div class="flex">
                                        <template x-for="(image, index) in images" :key="index">
                                            <div class="flex justify-center items-center">
                                                <img :src="image"
                                                    class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                    :alt="'upload'+index">
                                                <button class="absolute mx-2 translate-x-12 -translate-y-14"
                                                    type="button">
                                                    <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                        alt="" width="25px" @click="removeImage(index)">
                                                </button>
                                            </div>
                                        </template>
                                    </div>
                                </template>
                            </div>
                        </div>

                        {{-- Header --}}
                        <div class="p-6" x-data="displayThumbnail()">
                            <p class="font-bold text-xl">Header</p>
                            <input class="mt-4" type="file" name="header" accept="image/*" @change="selectedFile">
                            <template x-if="images.length < 1">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                    <img src="{{Storage::url($pulauDestindonesias->header)}}" alt="upload-icons"
                                        width="150px" height="150px">
                                </div>
                            </template>
                            <template x-if="images.length >= 1">
                                <div class="flex">
                                    <template x-for="(image, index) in images" :key="index">
                                        <div class="flex justify-center items-center">
                                            <img :src="image"
                                                class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                :alt="'upload'+index">
                                            <button class="absolute mx-2 translate-x-12 -translate-y-14" type="button">
                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                    width="25px" @click="removeImage(index)">
                                            </button>
                                        </div>
                                    </template>
                                </div>
                            </template>
                        </div>

                        {{-- Sambutan --}}
                        <div class="p-6">
                            <p class="font-bold text-xl">Sambutan</p>

                            <div class="px-4 pt-4" x-data="displayThumbnail()">
                                <p class="font-semibold text-lg">Foto Menteri</p>
                                <div class="flex justify-items-center">
                                    <template x-if="images.length < 1">
                                        <div
                                            class="border flex justify-center items-center rounded-full border-gray-300 bg-gray-200 w-[100px] h-[100px]">
                                            <img src="{{Storage::url($pulauDestindonesias->foto_menteri)}}"
                                                alt="upload-icons" width="20px" height="20px">
                                        </div>
                                    </template>
                                    <input name="foto_menteri" type="file" accept="image/*" @change="selectedFile">
                                    <template x-if="images.length >= 1">
                                        <div class="flex">
                                            <template x-for="(image, index) in images" :key="index">
                                                <div class="flex justify-center items-center">
                                                    <img :src="image"
                                                        class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                        :alt="'upload'+index">
                                                    <button class="absolute mx-2 translate-x-12 -translate-y-14"
                                                        type="button">
                                                        <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                            alt="" width="25px" @click="removeImage(index)">
                                                    </button>
                                                </div>
                                            </template>
                                        </div>
                                    </template>
                                </div>
                            </div>


                            <div class="px-4 pt-4">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Nama
                                    Menteri</label>
                                <input name="nama_menteri" type="text" id="nama_menteri"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                    placeholder="Nama Menteri" value="{{$pulauDestindonesias->nama_menteri}}">
                                @error('nama_menteri')
                                <p class="font-bold text-xl mt-1"><span class="text-red-500">Nama Pemimpin/Menteri wajib diisi*</span></p>
                                @enderror
                            </div>
                            <div class="max-w-6xl px-4 pt-4">
                                <p class="font-bold text-sm">Kata Sambutan</p>
                                <div class="bg-white">
                                    <textarea name="kata_sambutan"
                                        id="summernoteMenteri">{!! old('kata_sambutan', $pulauDestindonesias->kata_sambutan) !!}</textarea>
                                </div>
                                @error('kata_sambutan')
                                <p class="font-bold text-xl mt-1"><span class="text-red-500">Kata Sambutan wajib diisi*</span></p>
                                @enderror
                            </div>
                        </div>


                        {{-- Deskripsi --}}
                        <div class="max-w-6xl p-6">
                            <p class="font-bold text-xl">Deskripsi</p>
                            <div class="bg-white pt-4">
                                <textarea name="deskripsi"
                                    id="summernotePulau">{!! old('deskripsi', $pulauDestindonesias->deskripsi) !!}</textarea>
                            </div>
                            @error('deskripsi')
                                <p class="font-bold text-xl mt-1"><span class="text-red-500">Deskripsi wajib diisi*</span></p>
                            @enderror
                        </div>
                        {{-- Deskripsi Thumbnail --}}
                        <div class="max-w-6xl p-6">
                            <p class="font-bold text-xl">Deskripsi Thumbnail <span class="text-red-500">* maksimal 199 karakter</span></p>
                            <div class="bg-white pt-4">
                                <textarea name="desk_thumbnail"
                                    id="summernotedeskThumbnail">{!! old('desk_thumbnail', $pulauDestindonesias->desk_thumbnail) !!}</textarea>
                            </div>
                            <span id="limitCharacter"></span>
                        </div>
                        {{--Informasi--}}
                        <div class="p-6">
                            <p class="font-bold text-xl">Informasi</p>
                            <div class="px-4 grid grid-cols-2">

                                <div class="mt-4">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333]">Ibukota</label>
                                    <input name="ibukota" type="text" id="ibukota"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Nama Ibukota"
                                        value="{{old('ibukota', $pulauDestindonesias->ibukota)}}">
                                    @error('ibukota')
                                        <p class="font-bold text-xl mt-1"><span class="text-red-500">Nama Ibukota wajib diisi*</span></p>
                                    @enderror    
                                </div>

                                <div class="mt-4">
                                    <label for="text"
                                        class="block mb-2 text-sm font-bold text-[#333333]">Bandara</label>
                                    <input name="bandara" type="text" id="bandara"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Nama Bandara"
                                        value="{{old('bandara', $pulauDestindonesias->bandara)}}">
                                    @error('bandara')
                                        <p class="font-bold text-xl mt-1"><span class="text-red-500">Nama Bandara wajib diisi*</span></p>
                                    @enderror
                                </div>

                                <div class="mt-4">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Pelabuhan
                                        Laut</label>
                                    <input name="pelabuhan" type="text" id="pelabuhan"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Nama Pelabuhan"
                                        value="{{old('pelabuhan', $pulauDestindonesias->pelabuhan)}}">
                                    @error('pelabuhan')
                                        <p class="font-bold text-xl mt-1"><span class="text-red-500">Nama Pelabuhan wajib diisi*</span></p>
                                    @enderror
                                </div>

                                <div class="mt-4">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Terminal
                                        Darat</label>
                                    <input name="terminal" type="text" id="terminal"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Nama Terminal Darat"
                                        value="{{old('terminal', $pulauDestindonesias->terminal)}}">
                                    @error('terminal')
                                        <p class="font-bold text-xl mt-1"><span class="text-red-500">Nama Terminal wajib diisi*</span></p>
                                    @enderror
                                </div>

                                <div class="mt-4">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Akses
                                        Transportasi</label>
                                    <input name="transportasi" type="text" id="transportasi"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                        placeholder="Sebutkan Akses Transportasi"
                                        value="{{old('transportasi', $pulauDestindonesias->transportasi)}}">
                                    @error('transportasi')
                                        <p class="font-bold text-xl mt-1"><span class="text-red-500">Akses Transportasi wajib diisi*</span></p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        {{-- Gallery --}}
                        <div class="p-6" x-data="displayImage()">
                            <p class="font-bold text-xl">Gallery Foto Pulau</p>
                            <label class="m-2">Images</label>
                            <input class="pt-4" type="file" accept="image/*" @change="selectedFile" name="images[]"
                                multiple>
                            <template x-if="images.length < 1">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px] mt-4">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                            <template x-if="images.length >= 1">
                                <div class="flex">
                                    <template x-for="(image, index) in images" :key="index">
                                        <div class="flex justify-center items-center">
                                            <img :src="image"
                                                class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                :alt="'upload'+index">
                                            <button class="absolute mx-2 translate-x-12 -translate-y-14" type="button">
                                                <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                    width="25px" @click="removeImage(index)">
                                            </button>
                                        </div>
                                    </template>
                                </div>
                            </template>
                            @error('image_gallery')
                            <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)">
                                <div class="bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2" x-show="show">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                            <div id="error-gallery" class="hidden bg-red-300 text-sm rounded-md w-96 py-1 px-2 my-2">
                            </div>
                        </div>

                        {{-- Maps --}}
                        <div class="p-6">
                            <label for="text" class="block mb-2 text-xl font-bold text-[#333333]">Link Peta Google
                                Maps</label>
                            <input name="embed_maps" type="text" id="embed_maps"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                                placeholder="<iframe src=”https://www.map"
                                value="{{old('embed_maps', $pulauDestindonesias->embed_maps)}}">
                            @error('embed_maps')
                                <p class="font-bold text-xl mt-1"><span class="text-red-500">Link Peta wajib diisi*</span></p>
                            @enderror
                        </div>
                        <div class="p-6">
                            <label for="sort" class="block mb-2 text-xl font-bold text-[#333333]">Sort</label>
                            <select name="sort" type="text" id="sort"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5">
                                <option {{ $pulauDestindonesias->sort == 'active' ? 'selected' : '' }}
                                    value="active">Active</option>
                                <option {{ $pulauDestindonesias->sort == 'non active' ? 'selected' : '' }}
                                    value="non active">Non Active</option>
                            </select>
                        </div>
                        {{--tags--}}
                        <div class="form-group p-6">

                            <label>Tags Custom:</label>

                            <br />

                            <input data-role="tagsinput" type="text" name="tags" value="{{$tags}}">

                            {{-- <div class="form-group">--}}
                                {{-- <strong>Tag Autocomplete:</strong>--}}
                                {{-- <select id='myselect' name="select2" multiple>--}}
                                    {{-- <option value="">Select An Option</option>--}}
                                    {{-- @foreach($newsletters as $news)--}}
                                    {{-- @foreach($news->tags as $tag)--}}
                                    {{-- <option value="{{$tag->name}}">{{$tag->name}}</option>--}}
                                    {{-- @endforeach--}}
                                    {{-- @endforeach--}}
                                    {{-- </select>--}}
                                {{-- </div>--}}
                            @if ($errors->has('tags'))

                            <span class="text-danger">{{ $errors->first('tags') }}</span>

                            @endif

                        </div>
                        
                        <div class="p-6">
                            <button class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
    <script type="text/javascript">
    $('#myselect').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    })
    $('#myselect').on("change", function (e) {
        var data = $("#myselect").val()
        document.getElementById('valuePulau').setAttribute('value', data);
        console.log($("#myselect").val())
    })

    $('#myselect2').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    });
    $('#myselect2').on("change", function (e) {
        var data = $("#myselect2").val()
        document.getElementById('valueProvinsi').setAttribute('value', data);
        console.log($("#myselect2").val())
    })
    $('#myselect3').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    });
    $('#myselect3').on("change", function (e) {
        var data = $("#myselect3").val()
        document.getElementById('valueKabupaten').setAttribute('value', data);
        console.log($("#myselect3").val())
    })
    </script>
    <script>
        $(document).ready(function () {
            let objek = '{!! $pulauDestindonesias->objek !!}'

            /* 
            var $your-original-element = $('.your-original-element');
            var data = $your-original-element.select2('data')[0]['text'];
            alert(data);
            */
            $("#pulau").hide()
            $("#provinsi").hide()
            $("#kabupaten").hide()
            
            var selectedObj = $("#objek").val();
            if(selectedObj==objek.toLowerCase()){
                if(objek.toLowerCase()=='pulau'){
                    document.getElementById("pulau").style.display="block";
                }else if (objek.toLowerCase()=='provinsi') {
                    $("#pulau").hide()
                    // $(".provinsi .box").show();
                    document.getElementById("provinsi").style.display="block";
                } else if(objek.toLowerCase()=='kabupaten'){
                    $("#provinsi").hide()
                    // $(".kabupaten .box").show(); 
                    document.getElementById("kabupaten").style.display="block";
                }else{
                }
            }
            /* 
            $("select").find("option:selected").each(function () {
                var optionValue = $(this).attr("value");
                var objek = "{{$pulauDestindonesias->objek}}"
                objek = objek.toLowerCase()
                console.log(objek)
                
                if (optionValue) {
                    $(".box").not("." + optionValue).hide();
                    $(".box ." + objek).show();
                    // console.log(optionValue)
                    Alpine.store("dataAvailable").select2_prop = optionValue;

                    if(optionValue==objek){
                    }else{
                        $(".box").hide();
                    }
                    // console.log(Alpine.store("dataAvailable").select2_prop);
                } else {
                    $(".box").hide();
                }
                
            }); 
            */

            $("select").change(function () {
                $(this).find("option:selected").each(function () {
                    var optionValue = $(this).attr("value");
                    if (optionValue) {
                        $(".box").not("." + optionValue).hide();
                        $("." + optionValue).show();
                    } else {
                        $(".box").hide();
                    }
                });
            })
            // .change();
        });

    </script>
    <script>
        $('#summernotePulau').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteWalikota').summernote({
        placeholder: 'Kata Sambutan',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteProvinsi').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteGubernur').summernote({
        placeholder: 'Kata Sambutan',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteWisata').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteKab').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteMenteri').summernote({
        placeholder: 'Kata Sambutan',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });

    function deskThumbnail(element, placeholder, max, callback){
            $(element).summernote({
                placeholder,
                tabsize: 2,
                height: 120,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']]
                ],
                callbacks:{
                    onKeydown:function(e){
                        var t = e.currentTarget.innerText;
                        if(t.length >=max){
                            if(e.keyCode != 8){
                                e.preventDefault();
                            }
                        }
                    },
                    onKeyup:function(e){
                        var t = e.currentTarget.innerText;
                        if(typeof callback=='function'){
                            callback(max-t.length)
                        }
                    },
                    onPaste:function(e){
                        var t = e.currentTarget.innerText;
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        var all = t + bufferText;
                        document.execCommand('insertText', false, all.trim().substring(0, 400));
                        if (typeof callbackMax == 'function') {
                            callbackMax(max - t.length);
                        }
                    }
                }
            })
        }

        deskThumbnail('#summernotedeskThumbnail','Deskripsi thumbnail',200,function(max){
            $("#limitCharacter").text('Jumlah karakter '+max)
        });
    </script>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        let dataDB = {!! isset($pulauDestindonesias->galleryObjek) ? ($pulauDestindonesias->galleryObjek) : "[]" !!};
        let imagesGallery = [];
        let data = [];
        console.log(dataDB[0].image)
        // console.log({{asset('/storage/gallery')}})
        document.addEventListener("alpine:init", () => {
            Alpine.store("dataAvailable", {
                    isDataAvailable: data.length > 0 ? true : false,
                    select2_prop: ''
            });
        });

        if (dataDB.length > 0) {
            const url = window.location.origin
            let array = dataDB
            array.forEach(element => {
                imagesGallery.push(url+'/storage/gallery/'+element['image'])
                // console.log({{asset('')}})
                data.push(element['image'])
            });
        }
        // Show error toast
        function showToastError(message) {
            console.log(message)
            document.getElementById("error-gallery").classList.remove("hidden");
            document.getElementById("error-gallery").classList.add("flex");
            $(`<span id="gallery-error-message">${message}</span>`).appendTo('#error-gallery');
            // Hide the gallery after 5 seconds
            setTimeout(function () {
                document.getElementById("error-gallery").classList.add("hidden");
                $(".message-error").remove();
                document.getElementById("error-gallery").classList.remove("flex");
                document.getElementById("gallery-error-message").remove();
            }, 5000);
        }

        function displayImage() {

            return {
                images: imagesGallery,

                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event) {
                    if (!event.target.files.length) return

                    let file = event.target.files

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                        };
                        data = [...data, file[i]]
                    }

                    this.sendImage()
                },

                removeImage(index) {
                    this.images.splice(index, 1);
                    data.splice(index, 1)

                    this.sendImage()
                },

                sendImage() {
                    let fd = new FormData();

                    for (const file of data) {
                        if (typeof file === 'object') {
                            fd.append('images_gallery[]', file, file.name)
                        }
                        if (typeof file === 'string') {
                            fd.append('saved_gallery[]', file)
                        }
                    }
                    // console.log(fd)
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        method: 'POST',
                        // url: "kamarhotel.gallery.edit",
                        url: "{{ route('objek.gallery.edit', $pulauDestindonesias->id) }}",
                        data: fd,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            // document.getElementById("loading-spinner").classList.remove("hidden");
                            // document.getElementById("loading-spinner").classList.add("flex");
                        },
                        success: function(msg) {
                            console.log(msg)
                            if (msg.status === 0) {
                                showToastError(msg.message)
                            }

                            if (msg.status === 1) {
                                // showToastSuccess(msg.message)
                                Alpine.store("dataAvailable").isDataAvailable = true;
                                console.log(msg)
                            }                                
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                        complete: function() {
                            // document.getElementById("loading-spinner").classList.remove("flex");
                            // document.getElementById("loading-spinner").classList.add("hidden");
                        }
                    })
                }
            }
        }

        function displayThumbnail() {
            return {
                images: [],

                selectedFile(event) {
                    this.fileToUrl(event)
                },

                fileToUrl(event) {
                    if (!event.target.files.length) return

                    let file = event.target.files

                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''

                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.images = [...this.images, srcImg]
                        };
                    }
                },

                removeImage(index) {
                    this.images.splice(index, 1);
                },
            }
        }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
        $("#desti").on('change', function () {
            // alert('test')
            $(".destindonesia").hide();
            $("#" + $(this).val()).fadeIn(700);
        }).change();
    });
    </script>
        <script>
        
            function alert(msg, e){
                Swal.fire({
                    icon: 'error',
                    text:msg
                })
    
                e.preventDefault()
            }
    
            $("#submitEditObjectLocation").on("submit",function(e){
    
                if(!$("#slug").val()){
                   console.log($("#slug").val())
                    alert('Title harus diisi',e)
                }
    
                else if(!$("#nama_pulau").val()){
                    alert('Nama pulau harus diisi',e)
                }
    
                else if(!$("#nama_menteri").val()){
                    alert('Nama menteri harus diisi',e)
                }
    
                else if(!$("#summernoteMenteri").val()){
                    alert('Kata sambutan harus diisi',e)
                }
    
                else if(!$("#summernotePulau").val()){
                    alert('Deskripsi harus diisi',e)
                }
    
                else if(!$("#ibukota").val()){
                    alert('Ibu kota harus diisi',e)
                }
    
                else if(!$("#bandara").val()){
                    alert('Bandara harus diisi',e)
                }
                
                else if(!$("#pelabuhan").val()){
                    alert('Pelabuhan harus diisi',e)
                }
    
                else if(!$("#terminal").val()){
                    alert('Terminal harus diisi',e)
                }
    
                else if(!$("#transportasi").val()){
                    alert('Transportasi harus diisi',e)
                }
    
                else if(!$("#embed_maps").val()){
                    alert('Link peta harus diisi',e)
                }
    
            });
        </script>

</body>

</html>