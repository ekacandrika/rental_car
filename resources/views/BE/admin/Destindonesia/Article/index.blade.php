<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
          rel="stylesheet"/>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
</head>
<!-- Scripts -->
@vite(['resources/css/app.css', 'resources/js/app.js'])

<style>
    @media (min-width: 768px) {
        .note-modal-content {
            width: 600px;
            margin: 30px auto;
            margin-top: 30px;
            margin-top: 10rem;
        }

        .note-modal-backdrop {
            z-index: -1;
        }

        #idn:checked + #idn {
            display: block;
        }

        #sett:checked + #sett {
            display: block;
        }

        .destindonesia {
            display: none;
        }

        /*tags*/
        .bootstrap-tagsinput .tag {
            margin-right: 2px;
            color: #ffffff;
            background: #2196f3;
            padding: 3px 7px;
            border-radius: 3px;
        }

        .bootstrap-tagsinput {
            width: 100%;
        }
    }
</style>

<!-- Styles -->
@livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
{{-- Navbar --}}
<x-be.admin.navbar-admin></x-be.admin.navbar-admin>

<div class="grid grid-cols-10">
    <x-be.admin.sidebar-admin>
    </x-be.admin.sidebar-admin>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10">
            <span class="text-2xl font-bold font-inter text-[#333333]">Create Section</span>
            <div class="flex gap-5">
                <div class="bg-white rounded-lg p-3 w-full">
                    <div>
                        <form method="POST" action="{{ route('newsletter.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <p class="py-2 font-bold">Jenis Artikel</p>
                                <select class="select w-1/3 select-bordered my-2 form-select appearance-none
                                              block
                                              w-full
                                              px-3
                                              py-1.5
                                              text-base
                                              font-normal
                                              text-gray-700
                                              bg-white bg-clip-padding bg-no-repeat
                                              border border-solid border-gray-300
                                              rounded
                                              transition
                                              ease-in-out
                                              m-0
                                              focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                        id="position-option"
                                        name="article_section">
                                    @foreach ($sects as $sect)
                                        <option value="{{$sect}}">{{ $sect }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group my-2">
                                <label class="font-bold">Judul Newsletter</label>
                                <br>
                                <input type="text" name="title" class="form-control rounded-lg w-full"/>
                                @error('title')
                                <p class="text-sm text-red-400 mx-2 my-2"><span>{{$message}}</span></p>
                                @enderror
                            </div>

                            <div class="form-group my-2">
                                <label class="font-bold">Sub Judul Newsletter</label>
                                <br>
                                <input type="text" name="subtitle" class="form-control rounded-lg w-full"/>
                                @error('subtitle')
                                <p class="text-sm text-red-400 mx-2 my-2"><span>{{$message}}</span></p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="font-bold">Deskripsi</label>
                                <br>
                                <textarea id="summernoteSection" name="description"></textarea>
                                @error('description')
                                <p class="text-sm text-red-400 mx-2 my-2"><span>{{$message}}</span></p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="flex justify-start">
                                    <div class="mb-4 p-4 text-sm w-80">
                                        <div class="font-bold mb-2">Upload Image Preview</div>
                                        <div class="" x-data="previewImage()">
                                            <div class="mb-5">
                                                <label for="logo" class="block mb-2 mt-4 font-bold">Upload
                                                    image..</label>
                                                <input class="w-full cursor-pointer" type="file" name="image_primary"
                                                       id="logo" @change="fileChosen">
                                            </div>
                                            <label for="logo">
                                                <div
                                                    class="w-full h-48 rounded bg-gray-100 border border-gray-200 flex items-center justify-center overflow-hidden">
                                                    <img x-show="imageUrl" :src="imageUrl" class="w-full object-cover">
                                                    <div x-show="!imageUrl"
                                                         class="text-gray-300 flex flex-col items-center">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 "
                                                             fill="none" viewBox="0 0 24 24" stroke="currentColor"
                                                             stroke-width="2">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                  d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"/>
                                                        </svg>
                                                        <div>Image Preview</div>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                @error('image_primary')
                                <p class="text-sm text-red-400 mx-2 my-2"><span>{{$message}}</span></p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="flex justify-start">
                                    <div class="mb-4 p-4 text-sm w-80">

                                        <div class="font-bold mb-2">Upload Thumbnail Preview</div>
                                        <div class="" x-data="previewImage2()">
                                            <div class="mb-5">
                                                <label for="logo" class="block mb-2 mt-4 font-bold">Upload
                                                    image..</label>
                                                <input class="w-full cursor-pointer" type="file" name="thumbnail"
                                                       id="logo" @change="fileChosen">
                                            </div>
                                            <label for="logo">
                                                <div
                                                    class="w-full h-48 rounded bg-gray-100 border border-gray-200 flex items-center justify-center overflow-hidden">
                                                    <img x-show="imageUrl" :src="imageUrl" class="w-full object-cover">
                                                    <div x-show="!imageUrl"
                                                         class="text-gray-300 flex flex-col items-center">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 "
                                                             fill="none" viewBox="0 0 24 24" stroke="currentColor"
                                                             stroke-width="2">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                  d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"/>
                                                        </svg>
                                                        <div>Image Preview</div>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                @error('image_primary')
                                <p class="text-sm text-red-400 mx-2 my-2"><span>{{$message}}</span></p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Tags Custom:</label>
                                <br/>
                                <input data-role="tagsinput" type="text" name="tags">
                                @error('image_primary')
                                <p class="text-sm text-red-400 mx-2 my-2"><span>{{$message}}</span></p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <strong>Tag Autocomplete:</strong>
                                <select id='myselect' name="select2" multiple>
                                    <option value="">Select An Option</option>
                                    @foreach($newsletters as $news)
                                        @foreach($news->tags as $tag)
                                            <option value="{{$tag->name}}">{{$tag->name}}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>                                
                            <div class="form-group text-end mt-2">
                                <button type="submit"
                                        class="bg-kamtuu-second p-2 rounded-lg text-white hover:bg-kamtuu-primary">
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            {{--            List Newsletter--}}
            <div class="my-5">
                <div>
                    <p class="font-bold text-2xl">
                        Newsletter
                    </p>
                </div>
                <div class="bg-white">
                    <table class="table flex table-auto w-full leading-normal">
                        <thead class="uppercase text-gray-600 text-xs font-semibold bg-gray-200">
                        <tr class="hidden md:table-row">
                            <th class="text-left p-3">
                                <p>Name</p>
                            </th>
                            <th class="text-left p-3">
                                <p>Tags</p>
                            </th>
                            <th class="text-right p-3">
                                <p>Action</p>
                            </th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody class="flex-1 text-gray-700 sm:flex-none">@foreach($newsletters as $news)
                            <tr v-for="(person, index) in persons" :key="index"
                                class="border-t first:border-t-0 flex p-1 md:p-3 hover:bg-gray-100 md:table-row flex-col w-full flex-wrap">
                                <td class="p-1 md:p-3">
                                    <label class="text-xs text-gray-500 uppercase font-semibold md:hidden"
                                           for="">Name</label>
                                    <p class="">
                                        {{$news->title}}
                                    </p>
                                </td>
                                <td class="p-1 md:p-3 md:text-right">
                                    <label class="text-xs text-gray-500 uppercase font-semibold md:hidden" for="">Currency</label>
                                    <div>
                                        <strong>Tag:</strong>

                                        @foreach($news->tags as $tag)
                                                <label
                                                    class="label label-info p-1 rounded bg-kamtuu-third m-5">{{ $tag->name }}</label>
                                            @endforeach
                                    </div>
                                </td>

                                <td>
                                    <form action="{{ route('newsletter.destroy',$news->id) }}" method="POST">

                                        <a class="btn btn-primary"
                                           href="{{ route('newsletter.edit',$news->id) }}">Edit</a>
                                        <a href="{{route('newsletter.show', $news->slug)}}">Open</a>
                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-accent">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
<script>
    $('#myselect').select2({
        width: '100%',
        placeholder: "Select an Option",
        allowClear: true
    });
</script>
<script>
    function previewImage() {
        return {
            imageUrl: "",

            fileChosen(event) {
                this.fileToDataUrl(event, (src) => (this.imageUrl = src));
            },

            fileToDataUrl(event, callback) {
                if (!event.target.files.length) return;

                let file = event.target.files[0],
                    reader = new FileReader();

                reader.readAsDataURL(file);
                reader.onload = (e) => callback(e.target.result);
            },
        };
    }

    function previewImage2() {
        return {
            imageUrl: "",

            fileChosen(event) {
                this.fileToDataUrl(event, (src) => (this.imageUrl = src));
            },

            fileToDataUrl(event, callback) {
                if (!event.target.files.length) return;

                let file = event.target.files[0],
                    reader = new FileReader();

                reader.readAsDataURL(file);
                reader.onload = (e) => callback(e.target.result);
            },
        };
    }
</script>
<script>
    $('#summernoteSection').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
// ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteWalikota').summernote({
        placeholder: 'Kata Sambutan',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteProvinsi').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteGubernur').summernote({
        placeholder: 'Kata Sambutan',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteWisata').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteKab').summernote({
        placeholder: 'Deskripsi',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#summernoteMenteri').summernote({
        placeholder: 'Kata Sambutan',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
</script>

@livewireScripts
<script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
<script>
    function displayImage() {

        return {
            images: [],

            selectedFile(event) {
                this.fileToUrl(event)
            },

            fileToUrl(event) {
                if (!event.target.files.length) return

                let file = event.target.files

                for (let i = 0; i < file.length; i++) {
                    let reader = new FileReader();
                    let srcImg = ''

                    reader.readAsDataURL(file[i]);
                    reader.onload = e => {
                        srcImg = e.target.result
                        this.images = [...this.images, srcImg]
                    };
                }
            },

            removeImage(index) {
                this.images.splice(index, 1);
            }
        }
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $("#desti").on('change', function () {
            $(".destindonesia").hide();
            $("#" + $(this).val()).fadeIn(700);
        }).change();
    });
</script>
</body>
</html>
