<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    {{--  gjigo  --}}

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    {{--summernote--}}
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    {{--    tag input bootstrap--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
          rel="stylesheet"/>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #idn:checked + #idn {
            display: block;
        }

        #sett:checked + #sett {
            display: block;
        }

        .bootstrap-tagsinput .tag {
            margin-right: 2px;
            color: #ffffff;
            background: #2196f3;
            padding: 3px 7px;
            border-radius: 3px;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
{{-- Navbar --}}
<x-be.admin.navbar-admin></x-be.admin.navbar-admin>

<div class="grid grid-cols-10">
    <x-be.admin.sidebar-admin>
    </x-be.admin.sidebar-admin>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10 h-full">

            <div class=" p-5 border rounded-md bg-white">
                <p class="text-xl font-semibold">Kupon Create</p>
                <form method="POST" action="{{ route('kupon.store') }}" enctype="multipart/form-data">
                    @csrf


                    {{--                    Diskon --}}
                    <div class="p-6">

                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Judul Kupon</label>
                        <input name="judulKupon" type="text" id="text"
                               class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                               placeholder="Kupon A">
                    </div>

                    {{--                    Kode Kupon --}}
                    <div class="p-6">
                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Kode Kupon</label>
                        <input value="{{$randomString}}" name="kodeKupon" type="text" id="text"
                               class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                               placeholder="Komisi 15%">
                    </div>

                    {{--                    Diskon --}}
                    <div class="p-6">
                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Diskon Kupon</label>
                        <input name="diskonKupon" type="text" id="text"
                               class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                               placeholder="Diskon 15%">
                    </div>

                    {{--                    Jumlah Kupon --}}
                    <div class="p-6">
                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Jumlah Kupon</label>
                        <input name="jumlahKupon" type="text" id="text"
                               class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "
                               placeholder="100 Kupon">
                    </div>

                    <div class="p-6">
                        <div class="p-2 bg-white border border-black rounded-xl w-1/4">
                            <input name="expiredDate" id="datepickerKupon" placeholder=" "/>
                        </div>
                    </div>


                    <div class="p-6">
                        <button class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2">Create</button>
                    </div>
                </form>
            </div>

            <div class=" p-5 border rounded-md bg-white my-5">
                {{--                <p class="text-xl font-semibold">Pajak Local</p>--}}
                {{--                <form method="POST" action="{{ route('pajak.local.store') }}" enctype="multipart/form-data">--}}
                {{--                    @csrf--}}

                {{--                    --}}{{--                    Markup Local --}}
                {{--                    <div class="p-6">--}}
                {{--                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Markup Local</label>--}}
                {{--                        <input name="markupLocal" type="text" id="text"--}}
                {{--                               class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "--}}
                {{--                               placeholder="Komisi 15%">--}}
                {{--                    </div>--}}

                {{--                    <div class="form-group p-6">--}}
                {{--                        <strong>User :</strong>--}}
                {{--                        <input type="hidden" value="" name="user_id" id="valueUser">--}}
                {{--                        <select id='myselect' name="select2"--}}
                {{--                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">--}}
                {{--                            >--}}
                {{--                            <option value="">Select An Option</option>--}}
                {{--                            @foreach($user as $usr)--}}
                {{--                                <option value="{{$usr->id}}">{{$usr->first_name}}</option>--}}
                {{--                            @endforeach--}}
                {{--                        </select>--}}
                {{--                    </div>--}}

                {{--                    --}}{{--                    Komisi Local --}}
                {{--                    <div class="p-6">--}}
                {{--                        <label for="text" class="block mb-2 text-xl font-bold text-[#333333] ">Komisi Local</label>--}}
                {{--                        <input name="komisiLocal" type="text" id="text"--}}
                {{--                               class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 "--}}
                {{--                               placeholder="Komisi 15%">--}}
                {{--                    </div>--}}


                {{--                    <div class="form-group p-6">--}}
                {{--                        <strong>Produk Tipe :</strong>--}}
                {{--                        <input type="hidden" value="" name="produk_type" id="valueProduk">--}}
                {{--                        <select id='myselect2' name="select2"--}}
                {{--                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 ">--}}
                {{--                            >--}}
                {{--                            <option value="">Select An Option</option>--}}
                {{--                            @foreach($produk as $prod)--}}
                {{--                                <option value="{{$prod->type}}">{{$prod->type}}</option>--}}
                {{--                            @endforeach--}}
                {{--                        </select>--}}
                {{--                    </div>--}}
                {{--                    <div class="p-6">--}}
                {{--                        <button class="px-4 bg-[#23AEC1] p-3 rounded-lg text-white mr-2">Simpan</button>--}}
                {{--                    </div>--}}

                {{--                </form>--}}

                {{--                <hr>--}}

                <p class="text-4xl font-semibold my-5">List Kupon</p>
                <table class="table-auto w-full text-center border-separate border border-slate-500">
                    <thead>
                    <tr>
                        <th class="border border-slate-600">No</th>
                        <th class="border border-slate-600">Judul Kupon</th>
                        <th class="border border-slate-600">Kode Kupon</th>
                        <th class="border border-slate-600">Jumlah Kupon</th>
                        <th class="border border-slate-600">Status</th>
                        <th class="border border-slate-600">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($kupon as $key=>$kpn)
                        <tr>
                            <td class="border border-slate-600">{{$key+1}}</td>
                            <td class="border border-slate-600">{{$kpn->judulKupon}}</td>
                            <td class="border border-slate-600">{{$kpn->kodeKupon}}</td>
                            <td class="border border-slate-600">{{$kpn->jumlahKupon}}</td>
                            <td class="border border-slate-600">{{$kpn->status ?? "Active"}}</td>
                            <td class="border border-slate-600">
                                <form action="{{ route('pajak.local.destroy',$kpn->id) }}" method="POST">
                                    <a class="btn btn-primary"
                                       href="{{ route('pajak.local.edit',$kpn->id) }}">Edit</a>
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-accent">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $kupon->links() }}
            </div>

        </div>
    </div>
</div>


@livewireScripts

<script>
    $('#datepickerKupon').datepicker();
</script>


<script>
    $(document).ready(function () {
        $("#desti").on('change', function () {
            $(".destindonesia").hide();
            $("#" + $(this).val()).fadeIn(700);
        }).change();
    });
</script>

</body>
</html>
