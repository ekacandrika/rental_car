<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 100px;
            height: 34px;
        }
        .switch input {
            display: none;
        }
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #23AEC1;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .slider.round {
            border-radius: 34px;
        }
        .slider.round:before {
            border-radius: 50%;
        }
        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        input:checked + .slider {
            background-color: #9E3D64;
        }
        input:focus + .slider {
            box-shadow: 0 0 1px #23AEC1;
        }
        input:checked + .slider:before {
            -webkit-transform: translateX(68px);
            -ms-transform: translateX(68px);
            transform: translateX(68px);
        }
        .on {
            display: none;
        }
        .on, .off {
            color: white;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            font-size: 10px;
            font-family: Verdana, sans-serif;
        }
        input:checked + .slider .on {
            display: block;
        }
        input:checked + .slider .off {
            display: none;
        }
        #idn:checked+#idn {
            display: block;
        }
        #sett:checked+#sett {
            display: block;
        }
        .destindonesia{
            display: none;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    <x-be.admin.navbar-admin></x-be.admin.navbar-admin>
    {{-- @dd($users) --}}
    <div class="grid grid-cols-10">
        <x-be.admin.sidebar-admin>
        </x-be.admin.sidebar-admin>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                @if($message=Session::get('success'))
                    <div class="bg-cyan-400 border border-cyan-400 text-cyan-600 px-4 py-3 mb-4 rounded relative" role="alert" id="msgAlert">
                        <strong class="font-bold">{{$message}}</strong>
                        <span class="absolute top-0 bottom-0 right-0 px-4 py-3" id="btnClose">
                            <svg class="fill-current h-6 w-6 text-red-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
                        </span>
                    </div>
                @endif
                {{-- <div class="flex gap-2">
                    <button type="button" class="text-[#333333] font-medium rounded-lg text-sm py-2 text-center inline-flex items-center mr-2 hover:text-[#23AEC1]">
                        <img src="{{ asset('storage/icons/chevron.svg') }}" alt="user-profile" class="w-5 h-5 mr-1">
                        <span class="text-lg font-bold font-inter text-[#333333]">User</span>
                    </button>
                </div> --}}

                <div class="overflow-x-auto relative">
                    <div class="pb-4 dark:bg-gray-900">
                        <div class="grid grid-cols-6">
                            {{-- <label for="table-search" class="sr-only">Search</label>
                            <div class="relative mt-1">
                                <div class="flex absolute inset-y-0 left-0 items-center h-10 pl-3 pointer-events-none">
                                    <svg class="w-5 h-5 text-gray-500 dark:text-gray-400" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
                                </div>
                                <input type="text" id="table-search" class="block p-2 pl-10 w-80 text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search for items">
                            </div> --}}
                            <div class="col-end-7 col-span-1 mt-1">
                                {{-- @livewire('admin.modal-add-user') --}}
                                <a href="{{route('user.admin-create')}}" class="pb-2 text-white-200">
                                    <button class="bg-cyan-200 py-2 px-3 rounded-md text-white-50">+&nbsp;Tambah</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                        <thead class="text-xs text-white uppercase bg-[#9E3D64] dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="py-3 px-6">
                                    Nama Lengkap
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Email
                                </th>
                                {{-- <th scope="col" class="py-3 px-6">
                                    Tempat/Tanggal Lahir
                                </th> --}}
                                <th scope="col" class="py-3 px-6">
                                    Lisensi
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    No Telepon
                                </th>
                                <th scope="col" class="py-3 px-6" style="width:121px;">
                                    Jenis User
                                </th>
                                <th style="width:661px;" scope="col" class="py-3 px-6">
                                    Status Aktif
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <th width="151px" scope="row" class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                    {{$user->first_name}}
                                </th>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    {{$user->email}}
                                </td>
                                {{-- 
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    DC, 01 Januari 1990
                                </td> 
                                --}}
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    {{$user->lisensi}}
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    {{$user->no_tlp}}
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#51B449]">
                                    {{$user->role}}
                                </td>
                                @if($user->active_status)
                                    @if($user->active_status=='active')
                                    <td class="py-4 px-6 font-semibold text-teal-500">
                                        {{$user->active_status}}
                                    </td>
                                    @elseif($user->active_status=='penalty')
                                    <td class="py-4 px-6 font-semibold text-[#cce732]">
                                        {{$user->active_status}}
                                    </td>
                                    @elseif($user->active_status=='banded')
                                    <td class="py-4 px-6 font-semibold text-[#fa2c2c]">
                                        {{$user->active_status}}
                                    </td>
                                    @endif
                                @else
                                <td class="py-4 px-6 font-semibold">
                                    {{$user->active_status}}
                                </td>
                                @endif
                                <td class="py-4 px-6">
                                    {{-- @livewire('admin.modal-edit-user') --}}
                                    {{-- <div class="flex"> --}}
                                        <a class="mx-4" href="{{route('user.admin-edit',$user->id)}}">
                                            <img src="{{ asset('storage/icons/pencil-solid 1.svg') }}" alt="" width="16px">
                                        </a>
                                        <form action="{{route('user.admin-delete',$user->id)}}" method="post">
                                            <button class="mx-1">
                                                @csrf
                                                @method('DELETE')
                                                <img src="{{ asset('storage/icons/trash-can-solid.svg') }}" alt="" width="16px">
                                            </button>
                                        </form>   
                                    {{-- </div> --}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        /* 
            let btnClose = getElementById('btnClose');
            btnCLose.addEventListener('click',function(){
            getElementById("msgAlert").classList.add("hidden");
        })
        */
        $("#btnClose").click(function(){
            $("#msgAlert").addClass("hidden");
        });



        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                if (! event.target.files.length) return

                let file = event.target.files[0],
                    reader = new FileReader()

                reader.readAsDataURL(file)
                reader.onload = e => callback(e.target.result)
                },
            }
        }
    </script>
</body>
</html>
