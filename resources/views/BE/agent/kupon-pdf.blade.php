<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    {{-- <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap"> --}}

    <!-- Scripts -->
    {{-- @vite(['resources/css/app.css', 'resources/js/app.js']) --}}
    <link href="{{public_path('css/app.css')}}" rel="stylesheet">
    <style>
      /* @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css') */
        html *{
            font-size: 11px;
            color: #000 !important;
            font-family: Arial !important;
        }

        body {
        background: #ffffff;
        color: #535b61;
        font-family: "Poppins", sans-serif;
        font-size: 14px;
        line-height: 22px;
        }

        form {
        padding: 0;
        margin: 0;
        display: inline;
        }

        img {
        vertical-align: inherit;
        }

        a, a:focus {
        color: #0071cc;
        -webkit-transition: all 0.2s ease;
        transition: all 0.2s ease;
        }

        a:hover, a:active {
        color: #0c2f55;
        text-decoration: none;
        -webkit-transition: all 0.2s ease;
        transition: all 0.2s ease;
        }

        a:focus, a:active,
        .btn.active.focus,
        .btn.active:focus,
        .btn.focus,
        .btn:active.focus,
        .btn:active:focus,
        .btn:focus,
        button:focus,
        button:active {
            outline: none;
        }
        p {
        line-height: 1;
        }
        blockquote {
        border-left: 5px solid #eee;
        padding: 10px 20px;
        }

        iframe {
        border: 0 !important;
        }
        h1{
        color: #0c2f54;
        font-family: "Poppins", sans-serif;
        }
        h2{
            color: #B5B5C3 !important;
            font-size: 24px !important;
            /* font-size: 1rem !important; */
        } 
        h3{
            color: #B5B5C3 !important;
            font-size: 18.72px !important;
            /* font-size: 1rem !important; */
        }
        h4{
            color: #B5B5C3 !important;
            font-size: 16px !important;
            /* font-size: 1rem !important; */
        } 
        h5{
            color: #B5B5C3 !important;
            font-size: 13.28px !important;
            /* font-size: 1rem !important; */
        } 
        h6{
            color: #B5B5C3 !important;
            font-size: 16px !important;
            font-size: 10.72rem !important;
        }
        p{
            margin-bottom:1px;
            /* margin-top: 2px; */
        }
        .table {
            color: #535b61;
        }
        table > tbody > td{
            margin: 0px 0px 0px 0px;
            padding: 0px 0px 0px 0px;
        }
        .table-hover tbody tr:hover {
            background-color: #f6f7f8;
        }
        .rounded-top-0 {
            border-top-left-radius: 0px !important;
            border-top-right-radius: 0px !important;
        }

        .rounded-bottom-0 {
            border-bottom-left-radius: 0px !important;
            border-bottom-right-radius: 0px !important;
        }

        .rounded-left-0 {
        border-top-left-radius: 0px !important;
        border-bottom-left-radius: 0px !important;
        }

        .rounded-right-0 {
        border-top-right-radius: 0px !important;
        border-bottom-right-radius: 0px !important;
        }

        /* Text Size */
        .text-0 {
            font-size: 11px !important;
            font-size: 0.6875rem !important;
        }

        .text-1 {
            font-size: 12px !important;
            font-size: 0.75rem !important;
        }

        .text-2 {
        font-size: 14px !important;
        font-size: 0.875rem !important;
        }

        .text-3 {
        font-size: 16px !important;
        font-size: 1rem !important;
        }

        .text-4 {
        font-size: 18px !important;
        font-size: 1.125rem !important;
        }

        .text-5 {
        font-size: 21px !important;
        font-size: 1.3125rem !important;
        }

        .text-6 {
        font-size: 24px !important;
        font-size: 1.50rem !important;
        }

        .text-7 {
        font-size: 28px !important;
        font-size: 1.75rem !important;
        }

        .text-8 {
        font-size: 32px !important;
        font-size: 2rem !important;
        }

        .text-9 {
        font-size: 36px !important;
        font-size: 2.25rem !important;
        }

        .text-10 {
        font-size: 40px !important;
        font-size: 2.50rem !important;
        }

        .text-11 {
        font-size: 44px !important;
        font-size: 2.75rem !important;
        }

        .text-12 {
        font-size: 48px !important;
        font-size: 3rem !important;
        }

        .text-13 {
        font-size: 52px !important;
        font-size: 3.25rem !important;
        }

        .text-14 {
        font-size: 56px !important;
        font-size: 3.50rem !important;
        }

        .text-15 {
        font-size: 60px !important;
        font-size: 3.75rem !important;
        }

        .text-16 {
        font-size: 64px !important;
        font-size: 4rem !important;
        }

        .text-17 {
        font-size: 72px !important;
        font-size: 4.5rem !important;
        }

        .text-18 {
        font-size: 80px !important;
        font-size: 5rem !important;
        }

        .text-19 {
        font-size: 84px !important;
        font-size: 5.25rem !important;
        }

        .text-20 {
        font-size: 92px !important;
        font-size: 5.75rem !important;
        }

        /* Line height */
        .line-height-07 {
        line-height: 0.7 !important;
        }

        .line-height-1 {
        line-height: 1 !important;
        }

        .line-height-2 {
        line-height: 1.2 !important;
        }

        .line-height-3 {
        line-height: 1.4 !important;
        }

        .line-height-4 {
        line-height: 1.6 !important;
        }

        .line-height-5 {
        line-height: 1.8 !important;
        }

        /* Font Weight */
        .font-weight-100 {
        font-weight: 100 !important;
        }

        .font-weight-200 {
        font-weight: 200 !important;
        }

        .font-weight-300 {
        font-weight: 300 !important;
        }

        .font-weight-400 {
        font-weight: 400 !important;
        }

        .font-weight-500 {
        font-weight: 500 !important;
        }

        .font-weight-600 {
        font-weight: 600 !important;
        }

        .font-weight-700 {
        font-weight: 700 !important;
        }

        .font-weight-800 {
        font-weight: 800 !important;
        }

        .font-weight-900 {
        font-weight: 900 !important;
        }

        /* Opacity */
        .opacity-0 {
        opacity: 0;
        }

        .opacity-1 {
        opacity: 0.1;
        }

        .opacity-2 {
        opacity: 0.2;
        }

        .opacity-3 {
        opacity: 0.3;
        }

        .opacity-4 {
        opacity: 0.4;
        }

        .opacity-5 {
        opacity: 0.5;
        }

        .opacity-6 {
        opacity: 0.6;
        }

        .opacity-7 {
        opacity: 0.7;
        }

        .opacity-8 {
        opacity: 0.8;
        }

        .opacity-9 {
        opacity: 0.9;
        }

        .opacity-10 {
        opacity: 1;
        }

        .m-0 {
        margin: 0 !important;
        }

        .mt-0,
        .my-0 {
          margin-top: 0 !important;
        }

        .mr-0,
        .mx-0 {
          margin-right: 0 !important;
        }

        .mb-0,
        .my-0 {
          margin-bottom: 0 !important;
        }

        .ml-0,
        .mx-0 {
          margin-left: 0 !important;
        }

        .m-1 {
          margin: 0.25rem !important;
        }

        .mt-1,
        .my-1 {
          margin-top: 0.25rem !important;
        }

        .mr-1,
        .mx-1 {
          margin-right: 0.25rem !important;
        }

        .mb-1,
        .my-1 {
          margin-bottom: 0.25rem !important;
        }

        .ml-1,
        .mx-1 {
          margin-left: 0.25rem !important;
        }

        .m-2 {
          margin: 0.5rem !important;
        }

        .mt-2,
        .my-2 {
          margin-top: 0.5rem !important;
        }

        .mr-2,
        .mx-2 {
          margin-right: 0.5rem !important;
        }

        .mb-2,
        .my-2 {
          margin-bottom: 0.5rem !important;
        }

        .ml-2,
        .mx-2 {
          margin-left: 0.5rem !important;
        }

        .m-3 {
          margin: 0.75rem !important;
        }

        .mt-3,
        .my-3 {
          margin-top: 0.75rem !important;
        }

        .mr-3,
        .mx-3 {
          margin-right: 0.75rem !important;
        }

        .mb-3,
        .my-3 {
          margin-bottom: 0.75rem !important;
        }

        .ml-3,
        .mx-3 {
          margin-left: 0.75rem !important;
        }

        .m-4 {
          margin: 1rem !important;
        }

        .mt-4,
        .my-4 {
          margin-top: 1rem !important;
        }

        .mr-4,
        .mx-4 {
          margin-right: 1rem !important;
        }

        .mb-4,
        .my-4 {
          margin-bottom: 1rem !important;
        }

        .ml-4,
        .mx-4 {
          margin-left: 1rem !important;
        }

        .m-5 {
          margin: 1.25rem !important;
        }

        .mt-5,
        .my-5 {
          margin-top: 1.25rem !important;
        }

        .mr-5,
        .mx-5 {
          margin-right: 1.25rem !important;
        }

        .mb-5,
        .my-5 {
          margin-bottom: 1.25rem !important;
        }

        .ml-5,
        .mx-5 {
          margin-left: 1.25rem !important;
        }

        .m-6 {
          margin: 1.5rem !important;
        }

        .mt-6,
        .my-6 {
          margin-top: 1.5rem !important;
        }

        .mr-6,
        .mx-6 {
          margin-right: 1.5rem !important;
        }

        .mb-6,
        .my-6 {
          margin-bottom: 1.5rem !important;
        }

        .ml-6,
        .mx-6 {
          margin-left: 1.5rem !important;
        }

        .m-7 {
          margin: 1.75rem !important;
        }

        .mt-7,
        .my-7 {
          margin-top: 1.75rem !important;
        }

        .mr-7,
        .mx-7 {
          margin-right: 1.75rem !important;
        }

        .mb-7,
        .my-7 {
          margin-bottom: 1.75rem !important;
        }

        .ml-7,
        .mx-7 {
          margin-left: 1.75rem !important;
        }

        .m-8 {
          margin: 2rem !important;
        }

        .mt-8,
        .my-8 {
          margin-top: 2rem !important;
        }

        .mr-8,
        .mx-8 {
          margin-right: 2rem !important;
        }

        .mb-8,
        .my-8 {
          margin-bottom: 2rem !important;
        }

        .ml-8,
        .mx-8 {
          margin-left: 2rem !important;
        }

        .m-9 {
          margin: 2.25rem !important;
        }

        .mt-9,
        .my-9 {
          margin-top: 2.25rem !important;
        }

        .mr-9,
        .mx-9 {
          margin-right: 2.25rem !important;
        }

        .mb-9,
        .my-9 {
          margin-bottom: 2.25rem !important;
        }

        .ml-9,
        .mx-9 {
          margin-left: 2.25rem !important;
        }

        .m-10 {
          margin: 2.5rem !important;
        }

        .mt-10,
        .my-10 {
          margin-top: 2.5rem !important;
        }

        .mr-10,
        .mx-10 {
          margin-right: 2.5rem !important;
        }

        .mb-10,
        .my-10 {
          margin-bottom: 2.5rem !important;
        }

        .ml-10,
        .mx-10 {
          margin-left: 2.5rem !important;
        }

        .m-11 {
          margin: 2.75rem !important;
        }

        .mt-11,
        .my-11 {
          margin-top: 2.75rem !important;
        }

        .mr-11,
        .mx-11 {
          margin-right: 2.75rem !important;
        }

        .mb-11,
        .my-11 {
          margin-bottom: 2.75rem !important;
        }

        .ml-11,
        .mx-11 {
          margin-left: 2.75rem !important;
        }

        .m-12 {
          margin: 3rem !important;
        }

        .mt-12,
        .my-12 {
          margin-top: 3rem !important;
        }

        .mr-12,
        .mx-12 {
          margin-right: 3rem !important;
        }

        .mb-12,
        .my-12 {
          margin-bottom: 3rem !important;
        }

        .ml-12,
        .mx-12 {
          margin-left: 3rem !important;
        }

        .m-13 {
          margin: 3.25rem !important;
        }

        .mt-13,
        .my-13 {
          margin-top: 3.25rem !important;
        }

        .mr-13,
        .mx-13 {
          margin-right: 3.25rem !important;
        }

        .mb-13,
        .my-13 {
          margin-bottom: 3.25rem !important;
        }

        .ml-13,
        .mx-13 {
          margin-left: 3.25rem !important;
        }

        .m-14 {
          margin: 3.5rem !important;
        }

        .mt-14,
        .my-14 {
          margin-top: 3.5rem !important;
        }

        .mr-14,
        .mx-14 {
          margin-right: 3.5rem !important;
        }

        .mb-14,
        .my-14 {
          margin-bottom: 3.5rem !important;
        }

        .ml-14,
        .mx-14 {
          margin-left: 3.5rem !important;
        }

        .m-15 {
          margin: 3.75rem !important;
        }

        .mt-15,
        .my-15 {
          margin-top: 3.75rem !important;
        }

        .mr-15,
        .mx-15 {
          margin-right: 3.75rem !important;
        }

        .mb-15,
        .my-15 {
          margin-bottom: 3.75rem !important;
        }

        .ml-15,
        .mx-15 {
          margin-left: 3.75rem !important;
        }

        .m-16 {
          margin: 4rem !important;
        }

        .mt-16,
        .my-16 {
          margin-top: 4rem !important;
        }

        .mr-16,
        .mx-16 {
          margin-right: 4rem !important;
        }

        .mb-16,
        .my-16 {
          margin-bottom: 4rem !important;
        }

        .ml-16,
        .mx-16 {
          margin-left: 4rem !important;
        }

        .m-17 {
          margin: 4.25rem !important;
        }

        .mt-17,
        .my-17 {
          margin-top: 4.25rem !important;
        }

        .mr-17,
        .mx-17 {
            margin-right: 4.25rem !important;
        }

        .mb-17,
        .my-17 {
          margin-bottom: 4.25rem !important;
        }

        .ml-17,
        .mx-17 {
          margin-left: 4.25rem !important;
        }

        .m-18 {
          margin: 4.5rem !important;
        }

        .mt-18,
        .my-18 {
          margin-top: 4.5rem !important;
        }

        .mr-18,
        .mx-18 {
          margin-right: 4.5rem !important;
        }

        .mb-18,
        .my-18 {
          margin-bottom: 4.5rem !important;
        }

        .ml-18,
        .mx-18 {
          margin-left: 4.5rem !important;
        }

        .m-19 {
          margin: 4.75rem !important;
        }

        .mt-19,
        .my-19 {
          margin-top: 4.75rem !important;
        }

        .mr-19,
        .mx-19 {
          margin-right: 4.75rem !important;
        }

        .mb-19,
        .my-19 {
          margin-bottom: 4.75rem !important;
        }

        .ml-19,
        .mx-19 {
          margin-left: 4.75rem !important;
        }

        .m-20 {
          margin: 5rem !important;
        }

        .mt-20,
        .my-20 {
          margin-top: 5rem !important;
        }

        .mr-20,
        .mx-20 {
          margin-right: 5rem !important;
        }

        .mb-20,
        .my-20 {
          margin-bottom: 5rem !important;
        }

        .ml-20,
        .mx-20 {
          margin-left: 5rem !important;
        }

        .m-21 {
          margin: 5.25rem !important;
        }

        .mt-21,
        .my-21 {
          margin-top: 5.25rem !important;
        }

        .mr-21,
        .mx-21 {
          margin-right: 5.25rem !important;
        }

        .mb-21,
        .my-21 {
          margin-bottom: 5.25rem !important;
        }

        .ml-21,
        .mx-21 {
          margin-left: 5.25rem !important;
        }

        .m-22 {
          margin: 5.5rem !important;
        }

        .mt-22,
        .my-22 {
          margin-top: 5.5rem !important;
        }

        .mr-22,
        .mx-22 {
          margin-right: 5.5rem !important;
        }

        .mb-22,
        .my-22 {
          margin-bottom: 5.5rem !important;
        }

        .ml-22,
        .mx-22 {
          margin-left: 5.5rem !important;
        }

        .m-23 {
          margin: 5.75rem !important;
        }

        .mt-23,
        .my-23 {
          margin-top: 5.75rem !important;
        }

        .mr-23,
        .mx-23 {
          margin-right: 5.75rem !important;
        }

        .mb-23,
        .my-23 {
          margin-bottom: 5.75rem !important;
        }

        .ml-23,
        .mx-23 {
          margin-left: 5.75rem !important;
        }

        .m-24 {
          margin: 6rem !important;
        }

        .mt-24,
        .my-24 {
          margin-top: 6rem !important;
        }

        .mr-24,
        .mx-24 {
          margin-right: 6rem !important;
        }

        .mb-24,
        .my-24 {
          margin-bottom: 6rem !important;
        }

        .ml-24,
        .mx-24 {
          margin-left: 6rem !important;
        }

        .m-25 {
          margin: 6.25rem !important;
        }

        .mt-25,
        .my-25 {
          margin-top: 6.25rem !important;
        }

        .mr-25,
        .mx-25 {
          margin-right: 6.25rem !important;
        }

        .mb-25,
        .my-25 {
          margin-bottom: 6.25rem !important;
        }

        .ml-25,
        .mx-25 {
          margin-left: 6.25rem !important;
        }

        .m-26 {
          margin: 6.5rem !important;
        }

        .mt-26,
        .my-26 {
          margin-top: 6.5rem !important;
        }

        .mr-26,
        .mx-26 {
          margin-right: 6.5rem !important;
        }

        .mb-26,
        .my-26 {
          margin-bottom: 6.5rem !important;
        }

        .ml-26,
        .mx-26 {
          margin-left: 6.5rem !important;
        }

        .m-27 {
          margin: 6.75rem !important;
        }

        .mt-27,
        .my-27 {
          margin-top: 6.75rem !important;
        }

        .mr-27,
        .mx-27 {
          margin-right: 6.75rem !important;
        }

        .mb-27,
        .my-27 {
          margin-bottom: 6.75rem !important;
        }

        .ml-27,
        .mx-27 {
          margin-left: 6.75rem !important;
        }

        .m-28 {
          margin: 7rem !important;
        }

        .mt-28,
        .my-28 {
          margin-top: 7rem !important;
        }

        .mr-28,
        .mx-28 {
          margin-right: 7rem !important;
        }

        .mb-28,
        .my-28 {
          margin-bottom: 7rem !important;
        }

        .ml-28,
        .mx-28 {
          margin-left: 7rem !important;
        }

        .m-29 {
          margin: 7.25rem !important;
        }

        .mt-29,
        .my-29 {
          margin-top: 7.25rem !important;
        }

        .mr-29,
        .mx-29 {
          margin-right: 7.25rem !important;
        }

        .mb-29,
        .my-29 {
          margin-bottom: 7.25rem !important;
        }

        .ml-29,
        .mx-29 {
          margin-left: 7.25rem !important;
        }

        .m-30 {
          margin: 7.5rem !important;
        }

        .mt-30,
        .my-30 {
          margin-top: 7.5rem !important;
        }

        .mr-30,
        .mx-30 {
          margin-right: 7.5rem !important;
        }

        .mb-30,
        .my-30 {
          margin-bottom: 7.5rem !important;
        }

        .ml-30,
        .mx-30 {
          margin-left: 7.5rem !important;
        }

        .m-31 {
          margin: 7.75rem !important;
        }

        .mt-31,
        .my-31 {
          margin-top: 7.75rem !important;
        }

        .mr-31,
        .mx-31 {
          margin-right: 7.75rem !important;
        }

        .mb-31,
        .my-31 {
          margin-bottom: 7.75rem !important;
        }

        .ml-31,
        .mx-31 {
          margin-left: 7.75rem !important;
        }

        .m-32 {
          margin: 8rem !important;
        }

        .mt-32,
        .my-32 {
          margin-top: 8rem !important;
        }

        .mr-32,
        .mx-32 {
          margin-right: 8rem !important;
        }

        .mb-32,
        .my-32 {
          margin-bottom: 8rem !important;
        }

        .ml-32,
        .mx-32 {
          margin-left: 8rem !important;
        }

        .m-33 {
          margin: 8.25rem !important;
        }

        .mt-33,
        .my-33 {
          margin-top: 8.25rem !important;
        }

        .mr-33,
        .mx-33 {
          margin-right: 8.25rem !important;
        }

        .mb-33,
        .my-33 {
          margin-bottom: 8.25rem !important;
        }

        .ml-33,
        .mx-33 {
          margin-left: 8.25rem !important;
        }

        .m-34 {
          margin: 8.5rem !important;
        }

        .mt-34,
        .my-34 {
          margin-top: 8.5rem !important;
        }

        .mr-34,
        .mx-34 {
          margin-right: 8.5rem !important;
        }

        .mb-34,
        .my-34 {
          margin-bottom: 8.5rem !important;
        }

        .ml-34,
        .mx-34 {
          margin-left: 8.5rem !important;
        }

        .m-35 {
          margin: 8.75rem !important;
        }

        .mt-35,
        .my-35 {
          margin-top: 8.75rem !important;
        }

        .mr-35,
        .mx-35 {
          margin-right: 8.75rem !important;
        }

        .mb-35,
        .my-35 {
          margin-bottom: 8.75rem !important;
        }

        .ml-35,
        .mx-35 {
          margin-left: 8.75rem !important;
        }

        .m-36 {
          margin: 9rem !important;
        }

        .mt-36,
        .my-36 {
          margin-top: 9rem !important;
        }

        .mr-36,
        .mx-36 {
          margin-right: 9rem !important;
        }

        .mb-36,
        .my-36 {
          margin-bottom: 9rem !important;
        }

        .ml-36,
        .mx-36 {
          margin-left: 9rem !important;
        }

        .m-37 {
          margin: 9.25rem !important;
        }

        .mt-37,
        .my-37 {
          margin-top: 9.25rem !important;
        }

        .mr-37,
        .mx-37 {
          margin-right: 9.25rem !important;
        }

        .mb-37,
        .my-37 {
          margin-bottom: 9.25rem !important;
        }

        .ml-37,
        .mx-37 {
          margin-left: 9.25rem !important;
        }

        .m-48 {
          margin: 9.5rem !important;
        }

        .mt-48,
        .my-48 {
          margin-top: 9.5rem !important;
        }

        .mr-48,
        .mx-48 {
          margin-right: 9.5rem !important;
        }

        .mb-48,
        .my-48 {
          margin-bottom: 9.5rem !important;
        }

        .ml-48,
        .mx-48 {
          margin-left: 9.5rem !important;
        }

        .m-39 {
          margin: 9.75rem !important;
        }

        .mt-39,
        .my-39 {
          margin-top: 9.75rem !important;
        }

        .mr-39,
        .mx-39 {
          margin-right: 9.75rem !important;
        }

        .mb-39,
        .my-39 {
          margin-bottom: 9.75rem !important;
        }

        .ml-39,
        .mx-39 {
          margin-left: 9.75rem !important;
        }

        .m-40 {
          margin: 10rem !important;
        }

        .mt-40,
        .my-40 {
          margin-top: 10rem !important;
        }

        .mr-40,
        .mx-40 {
          margin-right: 10rem !important;
        }

        .mb-40,
        .my-40 {
          margin-bottom: 10rem !important;
        }

        .ml-40,
        .mx-40 {
          margin-left: 10rem !important;
        }
        .m-n1 {
        margin: -0.25rem !important;
        }

        .mt-n1,
        .my-n1 {
          margin-top: -0.25rem !important;
        }

        .mr-n1,
        .mx-n1 {
          margin-right: -0.25rem !important;
        }

        .mb-n1,
        .my-n1 {
        margin-bottom: -0.25rem !important;
        }

        .ml-n1,
        .mx-n1 {
          margin-left: -0.25rem !important;
        }

        .m-n2 {
          margin: -0.5rem !important;
        }

        .mt-n2,
        .my-n2 {
          margin-top: -0.5rem !important;
        }

        .mr-n2,
        .mx-n2 {
          margin-right: -0.5rem !important;
        }

        .mb-n2,
        .my-n2 {
          margin-bottom: -0.5rem !important;
        }

        .ml-n2,
        .mx-n2 {
          margin-left: -0.5rem !important;
        }

        .m-n3 {
          margin: -0.75rem !important;
        }

        .mt-n3,
        .my-n3 {
          margin-top: -0.75rem !important;
        }

        .mr-n3,
        .mx-n3 {
          margin-right: -0.75rem !important;
        }

        .mb-n3,
        .my-n3 {
          margin-bottom: -0.75rem !important;
        }

        .ml-n3,
        .mx-n3 {
          margin-left: -0.75rem !important;
        }

        .m-n4 {
          margin: -1rem !important;
        }

        .mt-n4,
        .my-n4 {
          margin-top: -1rem !important;
        }

        .mr-n4,
        .mx-n4 {
          margin-right: -1rem !important;
        }

        .mb-n4,
        .my-n4 {
          margin-bottom: -1rem !important;
        }

        .ml-n4,
        .mx-n4 {
          margin-left: -1rem !important;
        }

        .m-n5 {
          margin: -1.25rem !important;
        }

        .mt-n5,
        .my-n5 {
          margin-top: -1.25rem !important;
        }

        .mr-n5,
        .mx-n5 {
          margin-right: -1.25rem !important;
        }

        .mb-n5,
        .my-n5 {
          margin-bottom: -1.25rem !important;
        }

        .ml-n5,
        .mx-n5 {
          margin-left: -1.25rem !important;
        }

        .m-n6 {
          margin: -1.5rem !important;
        }

        .mt-n6,
        .my-n6 {
          margin-top: -1.5rem !important;
        }

        .mr-n6,
        .mx-n6 {
          margin-right: -1.5rem !important;
        }

        .mb-n6,
        .my-n6 {
          margin-bottom: -1.5rem !important;
        }

        .ml-n6,
        .mx-n6 {
          margin-left: -1.5rem !important;
        }

        .m-n7 {
          margin: -1.75rem !important;
        }

        .mt-n7,
        .my-n7 {
          margin-top: -1.75rem !important;
        }

        .mr-n7,
        .mx-n7 {
          margin-right: -1.75rem !important;
        }

        .mb-n7,
        .my-n7 {
          margin-bottom: -1.75rem !important;
        }

        .ml-n7,
        .mx-n7 {
          margin-left: -1.75rem !important;
        }

        .m-n8 {
          margin: -2rem !important;
        }

        .mt-n8,
        .my-n8 {
          margin-top: -2rem !important;
        }

        .mr-n8,
        .mx-n8 {
          margin-right: -2rem !important;
        }

        .mb-n8,
        .my-n8 {
          margin-bottom: -2rem !important;
        }

        .ml-n8,
        .mx-n8 {
          margin-left: -2rem !important;
        }

        .m-n9 {
          margin: -2.25rem !important;
        }

        .mt-n9,
        .my-n9 {
          margin-top: -2.25rem !important;
        }

        .mr-n9,
        .mx-n9 {
          margin-right: -2.25rem !important;
        }

        .mb-n9,
        .my-n9 {
          margin-bottom: -2.25rem !important;
        }

        .ml-n9,
        .mx-n9 {
          margin-left: -2.25rem !important;
        }

        .m-n10 {
          margin: -2.5rem !important;
        }

        .mt-n10,
        .my-n10 {
          margin-top: -2.5rem !important;
        }

        .mr-n10,
        .mx-n10 {
          margin-right: -2.5rem !important;
        }

        .mb-n10,
        .my-n10 {
          margin-bottom: -2.5rem !important;
        }

        .ml-n10,
        .mx-n10 {
          margin-left: -2.5rem !important;
        }

        .m-n11 {
          margin: -2.75rem !important;
        }

        .mt-n11,
        .my-n11 {
          margin-top: -2.75rem !important;
        }

        .mr-n11,
        .mx-n11 {
          margin-right: -2.75rem !important;
        }

        .mb-n11,
        .my-n11 {
          margin-bottom: -2.75rem !important;
        }

        .ml-n11,
        .mx-n11 {
          margin-left: -2.75rem !important;
        }

        .m-n12 {
          margin: -3rem !important;
        }

        .mt-n12,
        .my-n12 {
          margin-top: -3rem !important;
        }

        .mr-n12,
        .mx-n12 {
          margin-right: -3rem !important;
        }

        .mb-n12,
        .my-n12 {
          margin-bottom: -3rem !important;
        }

        .ml-n12,
        .mx-n12 {
          margin-left: -3rem !important;
        }

        .m-n13 {
          margin: -3.25rem !important;
        }

        .mt-n13,
        .my-n13 {
          margin-top: -3.25rem !important;
        }

        .mr-n13,
        .mx-n13 {
          margin-right: -3.25rem !important;
        }

        .mb-n13,
        .my-n13 {
          margin-bottom: -3.25rem !important;
        }

        .ml-n13,
        .mx-n13 {
          margin-left: -3.25rem !important;
        }

        .m-n14 {
          margin: -3.5rem !important;
        }

        .mt-n14,
        .my-n14 {
          margin-top: -3.5rem !important;
        }

        .mr-n14,
        .mx-n14 {
          margin-right: -3.5rem !important;
        }

        .mb-n14,
        .my-n14 {
          margin-bottom: -3.5rem !important;
        }

        .ml-n14,
        .mx-n14 {
          margin-left: -3.5rem !important;
        }

        .m-n15 {
          margin: -3.75rem !important;
        }

        .mt-n15,
        .my-n15 {
          margin-top: -3.75rem !important;
        }

        .mr-n15,
        .mx-n15 {
          margin-right: -3.75rem !important;
        }

        .mb-n15,
        .my-n15 {
          margin-bottom: -3.75rem !important;
        }

        .ml-n15,
        .mx-n15 {
          margin-left: -3.75rem !important;
        }

        .m-n16 {
          margin: -4rem !important;
        }

        .mt-n16,
        .my-n16 {
          margin-top: -4rem !important;
        }

        .mr-n16,
        .mx-n16 {
          margin-right: -4rem !important;
        }

        .mb-n16,
        .my-n16 {
          margin-bottom: -4rem !important;
        }

        .ml-n16,
        .mx-n16 {
          margin-left: -4rem !important;
        }

        .m-n17 {
          margin: -4.25rem !important;
        }

        .mt-n17,
        .my-n17 {
          margin-top: -4.25rem !important;
        }

        .mr-n17,
        .mx-n17 {
          margin-right: -4.25rem !important;
        }

        .mb-n17,
        .my-n17 {
          margin-bottom: -4.25rem !important;
        }

        .ml-n17,
        .mx-n17 {
          margin-left: -4.25rem !important;
        }

        .m-n18 {
          margin: -4.5rem !important;
        }

        .mt-n18,
        .my-n18 {
          margin-top: -4.5rem !important;
        }

        .mr-n18,
        .mx-n18 {
          margin-right: -4.5rem !important;
        }

        .mb-n18,
        .my-n18 {
          margin-bottom: -4.5rem !important;
        }

        .ml-n18,
        .mx-n18 {
          margin-left: -4.5rem !important;
        }

        .m-n19 {
          margin: -4.75rem !important;
        }

        .mt-n19,
        .my-n19 {
          margin-top: -4.75rem !important;
        }

        .mr-n19,
        .mx-n19 {
          margin-right: -4.75rem !important;
        }

        .mb-n19,
        .my-n19 {
          margin-bottom: -4.75rem !important;
        }

        .ml-n19,
        .mx-n19 {
          margin-left: -4.75rem !important;
        }

        .m-n20 {
          margin: -5rem !important;
        }

        .mt-n20,
        .my-n20 {
          margin-top: -5rem !important;
        }

        .mr-n20,
        .mx-n20 {
          margin-right: -5rem !important;
        }

        .mb-n20,
        .my-n20 {
          margin-bottom: -5rem !important;
        }

        .ml-n20,
        .mx-n20 {
          margin-left: -5rem !important;
        }

        .m-n21 {
          margin: -5.25rem !important;
        }

        .mt-n21,
        .my-n21 {
          margin-top: -5.25rem !important;
        }

        .mr-n21,
        .mx-n21 {
          margin-right: -5.25rem !important;
        }

        .mb-n21,
        .my-n21 {
          margin-bottom: -5.25rem !important;
        }

        .ml-n21,
        .mx-n21 {
          margin-left: -5.25rem !important;
        }

        .m-n22 {
          margin: -5.5rem !important;
        }

        .mt-n22,
        .my-n22 {
          margin-top: -5.5rem !important;
        }

        .mr-n22,
        .mx-n22 {
          margin-right: -5.5rem !important;
        }

        .mb-n22,
        .my-n22 {
          margin-bottom: -5.5rem !important;
        }

        .ml-n22,
        .mx-n22 {
          margin-left: -5.5rem !important;
        }

        .m-n23 {
          margin: -5.75rem !important;
        }

        .mt-n23,
        .my-n23 {
          margin-top: -5.75rem !important;
        }

        .mr-n23,
        .mx-n23 {
          margin-right: -5.75rem !important;
        }

        .mb-n23,
        .my-n23 {
          margin-bottom: -5.75rem !important;
        }

        .ml-n23,
        .mx-n23 {
          margin-left: -5.75rem !important;
        }

        .m-n24 {
          margin: -6rem !important;
        }

        .mt-n24,
        .my-n24 {
          margin-top: -6rem !important;
        }

        .mr-n24,
        .mx-n24 {
          margin-right: -6rem !important;
        }

        .mb-n24,
        .my-n24 {
          margin-bottom: -6rem !important;
        }

        .ml-n24,
        .mx-n24 {
          margin-left: -6rem !important;
        }

        .m-n25 {
          margin: -6.25rem !important;
        }

        .mt-n25,
        .my-n25 {
          margin-top: -6.25rem !important;
        }

        .mr-n25,
        .mx-n25 {
          margin-right: -6.25rem !important;
        }

        .mb-n25,
        .my-n25 {
          margin-bottom: -6.25rem !important;
        }

        .ml-n25,
        .mx-n25 {
          margin-left: -6.25rem !important;
        }

        .m-n26 {
          margin: -6.5rem !important;
        }

        .mt-n26,
        .my-n26 {
          margin-top: -6.5rem !important;
        }

        .mr-n26,
        .mx-n26 {
          margin-right: -6.5rem !important;
        }

        .mb-n26,
        .my-n26 {
          margin-bottom: -6.5rem !important;
        }

        .ml-n26,
        .mx-n26 {
          margin-left: -6.5rem !important;
        }

        .m-n27 {
          margin: -6.75rem !important;
        }

        .mt-n27,
        .my-n27 {
          margin-top: -6.75rem !important;
        }

        .mr-n27,
        .mx-n27 {
          margin-right: -6.75rem !important;
        }

        .mb-n27,
        .my-n27 {
          margin-bottom: -6.75rem !important;
        }

        .ml-n27,
        .mx-n27 {
          margin-left: -6.75rem !important;
        }

        .m-n28 {
          margin: -7rem !important;
        }

        .mt-n28,
        .my-n28 {
          margin-top: -7rem !important;
        }

        .mr-n28,
        .mx-n28 {
          margin-right: -7rem !important;
        }

        .mb-n28,
        .my-n28 {
          margin-bottom: -7rem !important;
        }

        .ml-n28,
        .mx-n28 {
          margin-left: -7rem !important;
        }

        .m-n29 {
          margin: -7.25rem !important;
        }

        .mt-n29,
        .my-n29 {
          margin-top: -7.25rem !important;
        }

        .mr-n29,
        .mx-n29 {
          margin-right: -7.25rem !important;
        }

        .mb-n29,
        .my-n29 {
          margin-bottom: -7.25rem !important;
        }

        .ml-n29,
        .mx-n29 {
          margin-left: -7.25rem !important;
        }

        .m-n30 {
          margin: -7.5rem !important;
        }

        .mt-n30,
        .my-n30 {
          margin-top: -7.5rem !important;
        }

        .mr-n30,
        .mx-n30 {
          margin-right: -7.5rem !important;
        }

        .mb-n30,
        .my-n30 {
          margin-bottom: -7.5rem !important;
        }

        .ml-n30,
        .mx-n30 {
          margin-left: -7.5rem !important;
        }

        .m-n31 {
          margin: -7.75rem !important;
        }

        .mt-n31,
        .my-n31 {
          margin-top: -7.75rem !important;
        }

        .mr-n31,
        .mx-n31 {
          margin-right: -7.75rem !important;
        }

        .mb-n31,
        .my-n31 {
          margin-bottom: -7.75rem !important;
        }

        .ml-n31,
        .mx-n31 {
          margin-left: -7.75rem !important;
        }

        .m-n32 {
          margin: -8rem !important;
        }

        .mt-n32,
        .my-n32 {
          margin-top: -8rem !important;
        }

        .mr-n32,
        .mx-n32 {
          margin-right: -8rem !important;
        }

        .mb-n32,
        .my-n32 {
          margin-bottom: -8rem !important;
        }

        .ml-n32,
        .mx-n32 {
          margin-left: -8rem !important;
        }

        .m-n33 {
          margin: -8.25rem !important;
        }

        .mt-n33,
        .my-n33 {
          margin-top: -8.25rem !important;
        }

        .mr-n33,
        .mx-n33 {
          margin-right: -8.25rem !important;
        }

        .mb-n33,
        .my-n33 {
          margin-bottom: -8.25rem !important;
        }

        .ml-n33,
        .mx-n33 {
          margin-left: -8.25rem !important;
        }

        .m-n34 {
          margin: -8.5rem !important;
        }

        .mt-n34,
        .my-n34 {
          margin-top: -8.5rem !important;
        }

        .mr-n34,
        .mx-n34 {
          margin-right: -8.5rem !important;
        }

        .mb-n34,
        .my-n34 {
          margin-bottom: -8.5rem !important;
        }

        .ml-n34,
        .mx-n34 {
          margin-left: -8.5rem !important;
        }

        .m-n35 {
          margin: -8.75rem !important;
        }

        .mt-n35,
        .my-n35 {
          margin-top: -8.75rem !important;
        }

        .mr-n35,
        .mx-n35 {
          margin-right: -8.75rem !important;
        }

        .mb-n35,
        .my-n35 {
          margin-bottom: -8.75rem !important;
        }

        .ml-n35,
        .mx-n35 {
          margin-left: -8.75rem !important;
        }

        .m-n36 {
          margin: -9rem !important;
        }

        .mt-n36,
        .my-n36 {
          margin-top: -9rem !important;
        }

        .mr-n36,
        .mx-n36 {
          margin-right: -9rem !important;
        }

        .mb-n36,
        .my-n36 {
          margin-bottom: -9rem !important;
        }

        .ml-n36,
        .mx-n36 {
          margin-left: -9rem !important;
        }

        .m-n37 {
          margin: -9.25rem !important;
        }

        .mt-n37,
        .my-n37 {
          margin-top: -9.25rem !important;
        }

        .mr-n37,
        .mx-n37 {
          margin-right: -9.25rem !important;
        }

        .mb-n37,
        .my-n37 {
          margin-bottom: -9.25rem !important;
        }

        .ml-n37,
        .mx-n37 {
          margin-left: -9.25rem !important;
        }

        .m-n48 {
          margin: -9.5rem !important;
        }

        .mt-n48,
        .my-n48 {
          margin-top: -9.5rem !important;
        }

        .mr-n48,
        .mx-n48 {
          margin-right: -9.5rem !important;
        }

        .mb-n48,
        .my-n48 {
          margin-bottom: -9.5rem !important;
        }

        .ml-n48,
        .mx-n48 {
          margin-left: -9.5rem !important;
        }

        .m-n39 {
          margin: -9.75rem !important;
        }

        .mt-n39,
        .my-n39 {
          margin-top: -9.75rem !important;
        }

        .mr-n39,
        .mx-n39 {
          margin-right: -9.75rem !important;
        }

        .mb-n39,
        .my-n39 {
          margin-bottom: -9.75rem !important;
        }

        .ml-n39,
        .mx-n39 {
          margin-left: -9.75rem !important;
        }

        .m-n40 {
          margin: -10rem !important;
        }

        .mt-n40,
        .my-n40 {
          margin-top: -10rem !important;
        }

        .mr-n40,
        .mx-n40 {
          margin-right: -10rem !important;
        }

        .mb-n40,
        .my-n40 {
          margin-bottom: -10rem !important;
        }

        .ml-n40,
        .mx-n40 {
          margin-left: -10rem !important;
        }

        .m-auto {
          margin: auto !important;
        }

        .mt-auto,
        .my-auto {
          margin-top: auto !important;
        }

        .mr-auto,
        .mx-auto {
          margin-right: auto !important;
        }

        .mb-auto,
        .my-auto {
          margin-bottom: auto !important;
        }

        .ml-auto,
        .mx-auto {
          margin-left: auto !important;
        }
        
        /* Background light */
        .bg-secondary {
        background-color: #E4E6EF !important;
        }

        a.bg-secondary:hover, a.bg-secondary:focus,
        button.bg-secondary:hover,
        button.bg-secondary:focus {
        background-color: #c4c8dc !important;
        }
        .bg-light {
        background-color: #F3F6F9 !important;
        }
        .bg-light-1 {
        background-color: #e9ecef !important;
        }

        .bg-light-2 {
        background-color: #dee2e6 !important;
        }

        .bg-light-3 {
        background-color: #ced4da !important;
        }

        .bg-light-4 {
        background-color: #adb5bd !important;
        }
        .bg-gray-100 {
        background-color: #F3F6F9 !important;
        }   

        .bg-hover-gray-100 {
          -webkit-transition: all 0.15s ease;
          transition: all 0.15s ease;
          cursor: pointer;
        }
        .bg-hover-gray-100:hover {
          -webkit-transition: all 0.15s ease;
          transition: all 0.15s ease;
          background-color: #F3F6F9 !important;
        }

        .bg-gray-200 {
          background-color: #EBEDF3 !important;
        }

        .bg-hover-gray-200 {
           -webkit-transition: all 0.15s ease;
           transition: all 0.15s ease;
           cursor: pointer;
        }
        .bg-hover-gray-200:hover {
           -webkit-transition: all 0.15s ease;
           transition: all 0.15s ease;
           background-color: #EBEDF3 !important;
        }

        .bg-gray-300 {
           background-color: #E4E6EF !important;
        }

        .bg-hover-gray-300 {
           -webkit-transition: all 0.15s ease;
           transition: all 0.15s ease;
           cursor: pointer;
        }
        .bg-hover-gray-300:hover {
           -webkit-transition: all 0.15s ease;
           transition: all 0.15s ease;
          background-color: #E4E6EF !important;
        }

        .bg-gray-400 {
          background-color: #D1D3E0 !important;
        }

        .bg-hover-gray-400 {
          -webkit-transition: all 0.15s ease;
          transition: all 0.15s ease;
          cursor: pointer;
        }
        .bg-hover-gray-400:hover {
          -webkit-transition: all 0.15s ease;
          transition: all 0.15s ease;
          background-color: #D1D3E0 !important;
        }

        .bg-gray-500 {
          background-color: #B5B5C3 !important;
        }

        .bg-hover-gray-500 {
          -webkit-transition: all 0.15s ease;
          transition: all 0.15s ease;
          cursor: pointer;
        }
        .bg-hover-gray-500:hover {
          -webkit-transition: all 0.15s ease;
          transition: all 0.15s ease;
          background-color: #B5B5C3 !important;
        }

        .bg-gray-600 {
          background-color: #7E8299 !important;
        }

        .bg-hover-gray-600 {
          -webkit-transition: all 0.15s ease;
          transition: all 0.15s ease;
          cursor: pointer;
        }
        .bg-hover-gray-600:hover {
          -webkit-transition: all 0.15s ease;
          transition: all 0.15s ease;
          background-color: #7E8299 !important;
        }

        .bg-gray-700 {
          background-color: #5E6278 !important;
        }

        .bg-hover-gray-700 {
          -webkit-transition: all 0.15s ease;
          transition: all 0.15s ease;
          cursor: pointer;
        }
        .bg-hover-gray-700:hover {
          -webkit-transition: all 0.15s ease;
          transition: all 0.15s ease;
          background-color: #5E6278 !important;
        }

        .bg-gray-800 {
          background-color: #3F4254 !important;
        }

        .bg-hover-gray-800 {
          -webkit-transition: all 0.15s ease;
          transition: all 0.15s ease;
          cursor: pointer;
        }
        .bg-hover-gray-800:hover {
          -webkit-transition: all 0.15s ease;
          transition: all 0.15s ease;
          background-color: #3F4254 !important;
        }

        .bg-gray-900 {
          background-color: #181C32 !important;
        }

        .bg-hover-gray-900 {
          -webkit-transition: all 0.15s ease;
          transition: all 0.15s ease;
          cursor: pointer;
        }
        .bg-hover-gray-900:hover {
          -webkit-transition: all 0.15s ease;
          transition: all 0.15s ease;
          background-color: #181C32 !important;
        }

        .bg-transparent {
          background-color: transparent;
        }

        .text-white {
        color: #ffffff !important;
        }

        .text-primary {
          color: #F64E60 !important;
        }

        a.text-primary:hover, a.text-primary:focus {
          color: #ec0c24 !important;
        }

        .text-secondary {
          color: #E4E6EF !important;
        }

        a.text-secondary:hover, a.text-secondary:focus {
          color: #b4bad3 !important;
        }

        .text-success {
          color: #1BC5BD !important;
        }

        a.text-success:hover, a.text-success:focus {
          color: #12827c !important;
        }

        .text-info {
          color: #8950FC !important;
        }

        a.text-info:hover, a.text-info:focus {
          color: #5605fb !important;
        }

        .text-warning {
          color: #FFA800 !important;
        }

        a.text-warning:hover, a.text-warning:focus {
          color: #b37600 !important;
        }

        .text-danger {
          color: #F64E60 !important;
        }

        a.text-danger:hover, a.text-danger:focus {
          color: #ec0c24 !important;
        }

        .text-light {
          color: #F3F6F9 !important;
        }

        a.text-light:hover, a.text-light:focus {
          color: #c0d0e0 !important;
        }

        .text-dark {
          color: #181C32 !important;
        }

        a.text-dark:hover, a.text-dark:focus {
          color: black !important;
        }

        .text-white {
          color: #ffffff !important;
        }

        a.text-white:hover, a.text-white:focus {
          color: #d9d9d9 !important;
        }

        .text-body {
          color: #3F4254 !important;
        }

        .text-muted {
          color: #B5B5C3 !important;
        }

        .text-black-50 {
          color: rgba(0, 0, 0, 0.5) !important;
        }

        .text-white-50 {
          color: rgba(255, 255, 255, 0.5) !important;
        }
        .text-black-100{
            color: #000;
        }
        .text-hide {
          font: 0/0 a;
          color: transparent;
          text-shadow: none;
          background-color: transparent;
          border: 0;
        }

        .text-decoration-none {
          text-decoration: none !important;
        }

        .text-break {
          word-break: break-word !important;
          word-wrap: break-word !important;
        }

        .text-reset {
          color: inherit !important;
        }

        .visible {
          visibility: visible !important;
        }

        .invisible {
          visibility: hidden !important;
        }

        .rounded-sm {
          border-radius: 0.28rem !important;
        }

        .rounded {
          border-radius: 0.85rem !important;
        }

        .rounded-top {
          border-top-left-radius: 0.85rem !important;
          border-top-right-radius: 0.85rem !important;
        }

        .rounded-right {
          border-top-right-radius: 0.85rem !important;
          border-bottom-right-radius: 0.85rem !important;
        }

        .rounded-bottom {
          border-bottom-right-radius: 0.85rem !important;
          border-bottom-left-radius: 0.85rem !important;
        }

        .rounded-left {
          border-top-left-radius: 0.85rem !important;
          border-bottom-left-radius: 0.85rem !important;
        }

        .rounded-lg {
          border-radius: 0.85rem !important;
        }

        .rounded-circle {
          border-radius: 50% !important;
        }

        .rounded-pill {
          border-radius: 50rem !important;
        }

        .rounded-0 {
          border-radius: 0 !important;
        }
        .font-weight-light {
        font-weight: 300 !important;
        }

        .font-weight-lighter {
        font-weight: lighter !important;
        }

        .font-weight-normal {
        font-weight: 400 !important;
        }
        .font-italic {
        font-style: italic !important;
        }
        .font-weight-bold {
        font-weight: 500 !important;
        }

        .font-weight-bolder {
        font-weight: 600 !important;
        }
    @media print {
        .table td, .table th {
            background-color: transparent !important;
        }

        .table td.bg-light, .table th.bg-light {
            background-color: #FFF !important;
        }

        .table td.bg-light-1, .table th.bg-light-1 {
            background-color: #f9f9fb !important;
        }

        .table td.bg-light-2, .table th.bg-light-2 {
            background-color: #f8f8fa !important;
        }

        .table td.bg-light-3, .table th.bg-light-3 {
            background-color: #f5f5f5 !important;
        }

        .table td.bg-light-4, .table th.bg-light-4 {
            background-color: #eff0f2 !important;
        }
        .table td.bg-light-5, .table th.bg-light-5 {
            background-color: #ececec !important;
        }
    }
        
    /* =================================== */
    /*  Layouts
    /* =================================== */
    .itinerary-container {
        margin: 15px auto;
        padding: 70px;
        max-width: 850px;
        background-color: #fff;
        border: 1px solid #ccc;
    }

    @media (max-width: 767px) {
        .itinerary-container {
            padding: 35px 20px 70px 20px;
            margin-top: 0px;
            border: none;
            border-radius: 0px;
        }
    }
    @page {
            font-size: 13px !important;
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
            color: #000 !important;
            font-family: Arial !important;
            /* line-height: .5 !important; */
            
    }
    .flex-container{
        display: flex;
        align-items: stretch;
        direction: column;
        /* padding-top: px; */
    }
    .flex-items{
        display: flex;
        margin-top:-10rem;
        margin-left: 21rem;
    }
    .bg-circle{
        background-color: #000;
        border-radius: 100%;
        /* z-index: -1; */
    }
    .img-fluid {
    max-width: 100%;
    height: auto;
    }

    .img-thumbnail {
    padding: 0.25rem;
    background-color: #ffffff;
    border: 1px solid #E4E6EF;
    border-radius: 0.85rem;
    -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.075);
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.075);
    max-width: 100%;
    height: auto;
    }

    .figure {
    display: inline-block;
    }

    .figure-img {
    margin-bottom: 0.5rem;
    line-height: 1;
    }

    .figure-caption {
    font-size: 90%;
    color: #7E8299;
    }
    .row {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap;
      margin-right: -12.5px;
      margin-left: -12.5px;
    }
    
    .no-gutters {
      margin-right: 0;
      margin-left: 0;
    }
    .no-gutters > .col,
    .no-gutters > [class*=col-] {
      padding-right: 0;
      padding-left: 0;
}

    .col-xxl,
    .col-xxl-auto, .col-xxl-12, .col-xxl-11, .col-xxl-10, .col-xxl-9, .col-xxl-8, .col-xxl-7, .col-xxl-6, .col-xxl-5, .col-xxl-4, .col-xxl-3, .col-xxl-2, .col-xxl-1, .col-xl,
    .col-xl-auto, .col-xl-12, .col-xl-11, .col-xl-10, .col-xl-9, .col-xl-8, .col-xl-7, .col-xl-6, .col-xl-5, .col-xl-4, .col-xl-3, .col-xl-2, .col-xl-1, .col-lg,
    .col-lg-auto, .col-lg-12, .col-lg-11, .col-lg-10, .col-lg-9, .col-lg-8, .col-lg-7, .col-lg-6, .col-lg-5, .col-lg-4, .col-lg-3, .col-lg-2, .col-lg-1, .col-md,
    .col-md-auto, .col-md-12, .col-md-11, .col-md-10, .col-md-9, .col-md-8, .col-md-7, .col-md-6, .col-md-5, .col-md-4, .col-md-3, .col-md-2, .col-md-1, .col-sm,
    .col-sm-auto, .col-sm-12, .col-sm-11, .col-sm-10, .col-sm-9, .col-sm-8, .col-sm-7, .col-sm-6, .col-sm-5, .col-sm-4, .col-sm-3, .col-sm-2, .col-sm-1, .col,
    .col-auto, .col-12, .col-11, .col-10, .col-9, .col-8, .col-7, .col-6, .col-5, .col-4, .col-3, .col-2, .col-1 {
      position: relative;
      width: 100%;
      padding-right: 12.5px;
      padding-left: 12.5px;
    }

    .col {
      -ms-flex-preferred-size: 0;
      flex-basis: 0;
      -webkit-box-flex: 1;
      -ms-flex-positive: 1;
      flex-grow: 1;
     max-width: 100%;
    }

    .row-cols-1 > * {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 100%;
      flex: 0 0 100%;
      max-width: 100%;
    }

    .row-cols-2 > * {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 50%;
      flex: 0 0 50%;
      max-width: 50%;
    }

    .row-cols-3 > * {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 33.3333333333%;
      flex: 0 0 33.3333333333%;
      max-width: 33.3333333333%;
    }

    .row-cols-4 > * {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 25%;
      flex: 0 0 25%;
      max-width: 25%;
    }

    .row-cols-5 > * {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 20%;
      flex: 0 0 20%;
      max-width: 20%;
    }

    .row-cols-6 > * {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 16.6666666667%;
      flex: 0 0 16.6666666667%;
      max-width: 16.6666666667%;
    }

    .col-auto {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 auto;
      flex: 0 0 auto;
      width: auto;
      max-width: 100%;
    }

    .col-1 {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 8.3333333333%;
      flex: 0 0 8.3333333333%;
      max-width: 8.3333333333%;
    }

    .col-2 {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 16.6666666667%;
      flex: 0 0 16.6666666667%;
      max-width: 16.6666666667%;
    }

    .col-3 {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 25%;
      flex: 0 0 25%;
      max-width: 25%;
    }

    .col-4 {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 33.3333333333%;
      flex: 0 0 33.3333333333%;
      max-width: 33.3333333333%;
    }

    .col-5 {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 41.6666666667%;
      flex: 0 0 41.6666666667%;
      max-width: 41.6666666667%;
    }

    .col-6 {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 50%;
      flex: 0 0 50%;
      max-width: 50%;
    }

    .col-7 {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 58.3333333333%;
      flex: 0 0 58.3333333333%;
      max-width: 58.3333333333%;
    }

    .col-8 {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 66.6666666667%;
      flex: 0 0 66.6666666667%;
      max-width: 66.6666666667%;
    }

    .col-9 {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 75%;
      flex: 0 0 75%;
      max-width: 75%;
    }

    .col-10 {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 83.3333333333%;
      flex: 0 0 83.3333333333%;
      max-width: 83.3333333333%;
    }

    .col-11 {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 91.6666666667%;
      flex: 0 0 91.6666666667%;
      max-width: 91.6666666667%;
    }

    .col-12 {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 100%;
      flex: 0 0 100%;
      max-width: 100%;
    }
    .bintang{
      position: relative;
      display: block;
      color:black;
      width: 0px;
      height: 0px;
      border-bottom: 10px solid #000;
      border-right:8px solid transparent;
      border-left:8px solid transparent;
      -moz-transform: rotate(35deg);
      -webkit-transform: rotate(35deg);
      -ms-transform: rotate(35deg);
      -o-transform: rotate(35deg);
    }
    .bintang:before{
      position: absolute;
      display: orange;
      border-bottom:8px solid orange;
      border-right:10px solid transparent;
      border-left:10px solid transparent;
      height: 0;
      content:'';
      width: 0;
      top:3px;
      left:-15px;
      display: block;
      content: '';
      -moz-transform: rotate(-35deg);
      -webkit-transform: rotate(-35deg);
      -ms-transform: rotate(-35deg);
      -o-transform: rotate(-35deg);
    } 
    .bintang:after{
      display: block;
      color:orange;
      height: 0px;
      width: 0px;
      position: absolute;
      border-right:8px solid transparent;
      border-left:10px solid transparent;
      border-bottom: 10px solid orange;
      left:10px;
      top:12px; 
      -moz-transform: rotate(-70deg);
      -webkit-transform: rotate(-70deg);
      -ms-transform: rotate(-70deg);
      -o-transform: rotate(-70deg); 
      content: '';

    }
    .alert {
        position: relative;
        padding: 0.75rem 1.25rem;
        margin-bottom: 1rem;
        border: 1px solid transparent;  
        content: '';
        left: -105px;
        top:3px;
        -moz-transform: rotate(-70deg);
        -webkit-transform: roteta(-70deg);
        -ms-transform: rotate(-70deg);
        -o-transform: rotate(-70deg);
    }
    .alert-heading {
        color: inherit;
    }

    .alert-link {
        font-weight: 500;
    }
    .alert-primary {
        color: #802932;
        background-color: #fddcdf;
        border-color: #fccdd2;
    }
    .alert-primary hr {
        border-top-color: #fbb5bc;
    }
    .alert-primary .alert-link {
        color: #591d23;
    }
    .alert-secondary {
        color: #77787c;
        background-color: #fafafc;
        border-color: #f7f8fb;
    }
    .alert-secondary hr {
        border-top-color: #e6e9f3;
    }
    .alert-secondary .alert-link {
        color: #5e5f62;
    }
    .alert-success {
        color: #0e6662;
        background-color: #d1f3f2;
        border-color: #bfefed;
    }
    .alert-success hr {
        border-top-color: #abeae7;
    }
    .alert-success .alert-link {
        color: #083937;
    }
    .alert-info {
        color: #472a83;
        background-color: #e7dcfe;
        border-color: #decefe;
    }
    .alert-info hr {
        border-top-color: #cdb5fd;
    }
    .alert-info .alert-link {
        color: #321e5c;
    }

    .alert-warning {
        color: #855700;
        background-color: #ffeecc;
        border-color: #ffe7b8;
    }
    .alert-warning hr {
        border-top-color: #ffde9f;
    }
    .alert-warning .alert-link {
        color: #523600;
    }

    .alert-danger {
        color: #802932;
        background-color: #fddcdf;
        border-color: #fccdd2;
    }
    .alert-danger hr {
        border-top-color: #fbb5bc;
    }
    .alert-danger .alert-link {
        color: #591d23;
    }

    .alert-light {
        color: #7e8081;
        background-color: #fdfdfe;
        border-color: #fcfcfd;
    }
    .alert-light hr {
        border-top-color: #ededf3;
    }
    .alert-light .alert-link {
        color: #656667;
    }

    .alert-dark {
        color: #0c0f1a;
        background-color: #d1d2d6;
        border-color: #bebfc6;
    }
    .alert-dark hr {
        border-top-color: #b0b2ba;
    }
    .alert-dark .alert-link {
        color: black;
    }
    .alert-white {
        color: #858585;
        background-color: white;
        border-color: white;
    }
    .alert-white hr {
        border-top-color: #f2f2f2;
    }
    .alert-white .alert-link {
        color: #6c6c6c;
    }
    .text-lowercase {
    text-transform: lowercase !important;
    }

    .text-uppercase {
    text-transform: uppercase !important;
    }

    .text-capitalize {
    text-transform: capitalize !important;
    }
    .text-right {
    text-align: right !important;
    }
    .fa {
    font-family: var(--fa-style-family, "Font Awesome 6 Free");
    font-weight: var(--fa-style, 900); }
    .fa,
    .fa-classic,
    .fa-sharp,
    .fas,
    .fa-solid,
    .far,
    .fa-regular,
    .fab,
    .fa-brands {
      -moz-osx-font-smoothing: grayscale;
      -webkit-font-smoothing: antialiased;
      display: var(--fa-display, inline-block);
      font-style: normal;
      font-variant: normal;
      line-height: 1;
      text-rendering: auto; 
    }
    .fas,
    .fa-classic,
    .fa-solid,
    .far,
    .fa-regular {
      font-family: 'Font Awesome 6 Free'; }

    .fab,
    .fa-brands {
      font-family: 'Font Awesome 6 Brands'; }
    .fa-star-and-crescent::before {
      content: "\f699"; }
    .fa-star::before {
      content: "\f005"; }
    .fa-star-half-alt::before {
      content: "\f5c0"; }
    .fa-star-half-stroke::before {
      content: "\f5c0"; }
    .fa-star-half-alt::before {
      content: "\f5c0"; }
    .fa-star-of-david::before {
      content: "\f69a"; }
    .fa-star-half::before {
      content: "\f089"; }
    .checked{
      background: #000000;
    }
  </style>
  <title>Document</title>
</head>
<body>
    {{-- @dd($data['coupon']) --}}
    <div class="itinerary-container">
        <div class="items-start justify-start min-h-screen">
            <div class="justify-between p-4">
              <table class="mb-6" style="border:1px">
                <tbody>
                  <tr>
                    <td><b class="text-3">{{$data['coupon']['coupon_name']}}</b></td>
                    @if ($data['coupon']['coupon_logo'])
                    <td>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;
                      <img src="{{public_path($data['coupon']['coupon_logo'])}}" syle="overflow:hidden" alt="logo"></td>    
                    @endif
                    <td class="text-right"><img src="" alt="logo"></td>
                  </tr>
                  <tr>
                    {{-- <td></td> --}}
                    <td class="text-2 text-muted">{{$data['coupon']['coupon_address']}}</td>
                  </tr>
                  <tr>
                    {{-- <td></td> --}}
                    <td class="text-2 text-muted">{{$data['coupon']['coupon_phone']}}</td>
                  </tr>
                </tbody>
              </table>
                {{-- {!! $data->coupon_header !!}          --}}
                <hr style="width:100%;border-top:.5px solid black;margin-top:6px;">
            </div>
            <div class="justity-between">
              {{-- @dd($data['booking']) --}}
                <h1 class="text-4">Pesanan Anda Telah Dikonfirmasi</h1>
                <div class="text-black mt-n1"><p>{{$data['coupon']['coupon_message']}}</p></div>
                <div class="flex mt-10">
                    {{-- <div class=".rounded-circle text-dark"></div> --}}
                    <img id="image" src="{{public_path('icon/hotel-solid.svg')}}" style="z-index:1" class="mr-1" alt="logo 3" weight="15" height="15"/>
                    <label for="image" class="mr-1 text-5 text-black-100 bold">{{$data['detail']->product->product_name}}</label>
                </div>
                <div class="flex">
                    {{-- No Booking: 123BD8095 --}}
                    <span class=" text-muted">-|&nbsp;No. booking&nbsp;:&nbsp;{{$data['booking']['booking_code']}}</span>
                </div>
                {{-- <br> --}}
                <div class="flex-container mt-3">
                    <img src="https://images.unsplash.com/photo-1554629947-334ff61d85dc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1024&h=1280&q=80" alt="" width="250" height="150">
                    <div class="flex-items">
                        <div class="row">
                            <div class="col">
                                <img id="image" src="{{public_path('icon/hotel-solid.svg')}}" alt="logo 3" weight="15" height="15"/>
                                <label for="image" class="mx-3 text-3 text-black font-bold">{{$data['detail']->product->product_name}}, {{$data['detail']->alamat}}</label>
                                <div class="flex d-flex">
                                  {{-- <br> --}}
                                  {{-- <img src="{{public_path('icon/star 1.png')}}" alt="" style="background-color: #000000"> --}}
                                </div>
                                {{-- <div class="rating">
                                    <input type="radio" name="start" id="start1"><label for="star1"></label>
                                    <input type="radio" name="start" id="start2"><label for="star2"></label>
                                    <input type="radio" name="start" id="start3"><label for="star3"></label>
                                    <input type="radio" name="start" id="start4"><label for="star4"></label>
                                    <input type="radio" name="start" id="start5"><label for="star5"></label>
                                </div> 
                                --}}
                            </div>
                        </div>
                        <div class="row mt-2">
                            <table>
                                <tbody>
                                    @if($data['detail']->product->type=='hotel')
                                    <tr>
                                        <td witdh="50%">
                                          &nbsp;&nbsp;&nbsp;<img id="image" src="{{public_path('icon/calendar.png')}}" class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3" weight="15" height="15">
                                            <label for="image" class="text-2 text-black font-bold">{{$data['detail']->checkin}}</label>
                                            <div class="flex d-flex">
                                              <span class="text-muted text-2 ml-9">Check-in</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="ml-15">
                                                {{-- <img id="image" src="{{public_path('icon/calendar.png')}}" class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3" weight="15" height="15"> --}}
                                                <label for="image" class="mx-3 text-3 text-black font-bold">{{$data['detail']->checkout}}</label>
                                                <div class="flex d-flex">
                                                  <span class="text-muted text-2 ml-3">Check-out</span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <hr class="text-light font-thin border-solid">
                <table class="mb-2 mt-1">
                    <tbody>
                        <tr>
                            <td>
                                <div class="flex">
                                    <img id="image" src="{{public_path('icon/maps-point-grey.png')}}" class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu" with="15" height="15"> <label for="image" class="mx-3 text-base font-semi-bold">{{$data['detail']->product->product_name}}, {{$data['detail']->alamat}}</label>
                                </div>
                            </td>
                            <td with="70%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td class="text-right">
                                <div class="flex d-flex">
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="image" src="{{public_path('icon/telephone-black.svg')}}" class="mr-1" alt="polygon 3" with="15" height="15"> <label for="image" class="mx-3 text-base font-semi-bold">{{$data['detail']->no_telp}}</label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr class="text-light font-thin border-solid">
            </div>
            <div class="justify-between">
                <div class="bg-light-4" style="height:25px;">
                    &nbsp;&nbsp;&nbsp;<span class="text-dark font-weight-bold text-3 mr-3">Kamar 1:&nbsp;</span><span class="text-3">No. Konfirmasi</span><span class="text-dark font-weight-bold text-3">: {{$data['booking']['booking_code']}}</span>
                </div>
            </div>
            <div class="justify-between">
                &nbsp;&nbsp;&nbsp;<span class="text-3">{{$data['kamar']['jenis_kamar']}}</span>
              </div>
              <div class="justify-between mt-3">
                &nbsp;&nbsp;&nbsp;<img src="{{public_path('icon/user.png')}}" alt=""  with="15" height="15"><span class="text-3">Direservasi untuk:&nbsp;</span><span class="text-3 text-uppercase">{{$data['user']['first_name']}}</span>
                <br>
                &nbsp;&nbsp;&nbsp;<span class="text-3 text-muted mt-n1">1 Dewasa</span>
                <hr class="text-light font-thin border-solid">
            </div>
           {{-- </div> --}}
           <div class="justify-between">
                <div class="ml-3 mt-2 mb-12">
                  @if($data['detail']->product->type=='hotel')
                    <div class="text-muted text-3">Aturan Pembatalan/Perubahan:</div>
                    <p>
                      {{$data['coupon']['coupon_list']}}
                    </p>
                  @endif
                </div>

                <div class="ml-3 mt-2 mb-7">
                  @if($data['detail']->product->type=='hotel')
                  <div class="text-muted text-3">Petunjuk Kedatangan Terlambatan:</div>
                  {{-- &nbsp;&nbsp;&nbsp;{!!$data->coupon_list!!} --}}
                  <p>
                    Jika Anda akan check-in terlambat, hubungi langsung properti ini untuk mengetahui kebijakan check-in terlambat mereka.
                  </p>
                  @endif
                </div>
                <div class="ml-3 mt-2">
                  @if($data['detail']->product->type=='hotel')
                  <div class="text-muted text-3">Kebijakan Check-in:</div>
                  {{-- &nbsp;&nbsp;&nbsp;{!!$data->coupon_list!!} --}}
                  <p>
                    Waktu check-in mulai pukul 14.00
                    <br>
                    <br>
                    Usia minimal untuk check-in adalah: 17
                  </p>
                  @endif
                </div>
            </div>
            <br>
            <br>
            <div class="justify-between" style="padding-top: 70px;">
                <div class="ml-3">
                  <div class="text-muted text-3">Petunjuk Khusus Check-in:</div>
                  <p>
                      {!!$data['coupon']['notes']!!}
                      {{-- Bellboy atau resepsionis akan menyambut tamu saat kedatangan --}}
                    </p>
                </div>
            </div>
            <div class="justify-between mt-15">
                <hr class="text-light font-thin border-solid mb-3">
                <div class="text-bold text-6">Harga</div>
                <table style="width:75%">
                    <tbody>
                        <tr>
                            <td class="text-muted">Harga Final:</td>
                            <td class="text-muted text-right">{{$data['coupon']['amount']}}</td>
                        </tr>
                        <tr>
                            <td class="text-muted">Pajak dan Biaya:</td>
                            <td class="text-muted text-right">{{$data['coupon']['tax_amount']}}</td>
                        </tr>
                        <tr>
                            <td class="text-bold text-3">Total:</td>
                            <td class="text-bold text-right text-3">{{$data['coupon']['total_amount']}}</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            {{-- <td class="text-bold text-right text-2"></td> --}}
                        </tr>
                        <tr>
                            <td class="text-muted">Bookin Fee</td>
                            <td class="text-muted text-right">{{$data['coupon']['booking_fee']}}</td>
                        </tr>
                    </tbody>
                </table>
                <hr class="text-light font-thin border-solid">
            </div>
        </div>
    </div>
</body>
</html>