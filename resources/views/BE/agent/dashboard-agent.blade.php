<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #report:checked+#report {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">

    {{-- Navbar --}}
    <x-be.agent.navbar-agent></x-be.agent.navbar-agent>

    {{-- Sidebar --}}
    <x-be.agent.sidebar-agent></x-be.agent.sidebar-agent>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pr-10" style="padding-left: 315px">
            <span class="text-sm font-bold font-inter text-[#000000]">Tanggal bergabung: 17 Agutus 2022</span>

            {{-- Card --}}
            <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                {{-- Card --}}
                <div class="grid grid-cols-3">
                    <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                        <div class="w-full">
                            <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Penjualan Bulan Ini</p>
                            <p class="mt-4 font-bold text-[#FFFFFF] text-xl">Rp 50,000,000</p>
                            <p class="mb-2 font-semibold text-[#FFFFFF] text-base">20 Pax</p>
                        </div>
                    </div>
                    <div class="w-auto flex bg-[#FFB800] rounded">
                        <div class="w-full">
                            <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Penjualan Tahun Ini</p>
                            <p class="mt-4 font-bold text-[#FFFFFF] text-xl">Rp 150,000,000</p>
                            <p class="mb-2 font-semibold text-[#FFFFFF] text-base">100 Pax</p>
                        </div>
                    </div>
                    <div class="w-auto flex bg-[#51B449] rounded ml-5">
                        <div class="w-full">
                            <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Penjualan Sejak Bergabung</p>
                            <p class="mt-4 font-bold text-[#FFFFFF] text-xl">Rp 750,000,000</p>
                            <p class="mb-2 font-semibold text-[#FFFFFF] text-base">1200 Pax</p>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Grafik --}}
            <div class="grid grid-cols-2 text-center h-96">
                <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                    <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                </div>
                <div class="text-center ml-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                    <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                </div>
            </div>

            {{-- Card 2 --}}
            <div class="drop-shadow-xl py-5 my-5 rounded text-center">
                {{-- Card 2 --}}
                <div class="grid grid-cols-3">
                    <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                        <div class="w-full">
                            <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Pendapatan Bersih Bulan Ini</p>
                            <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">Rp 40,000,000</p>
                        </div>
                    </div>
                    <div class="w-auto flex bg-[#FFB800] rounded">
                        <div class="w-full">
                            <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Pendapatan Bersih Tahun Ini</p>
                            <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">Rp 100,000,000</p>
                        </div>
                    </div>
                    <div class="w-auto flex bg-[#51B449] rounded ml-5">
                        <div class="w-full">
                            <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Pendapatan Bersih Sejak Bergabung</p>
                            <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">Rp 500,000,000</p>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Table --}}
            <div class="overflow-x-auto relative">
                <table class="w-full text-sm text-left border-2 border-[#333333]">
                    <tbody>
                        <tr class="bg-white border-b-2 border-[#333333]">
                            <th scope="row"
                                class="py-4 px-6 border-r-2 border-[#333333] font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Tagihan Belum dibayar (Rp)
                            </th>
                            <td class="py-4 px-6">
                                Rp. 40,000,000
                            </td>
                        </tr>
                        <tr class="bg-white border-2 border-[#333333]">
                            <th scope="row"
                                class="py-4 px-6 border-r-2 border-[#333333] font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Jumlah listing (Item)</th>
                            <td class="py-4 px-6">
                                1236 Item
                            </td>
                        </tr>
                        <tr class="bg-white border-2 border-[#333333]">
                            <th scope="row"
                                class="py-4 px-6 border-r-2 border-[#333333] font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Jumlah Listing Terjual (Item)
                            </th>
                            <td class="py-4 px-6">
                                3465 Item
                            </td>
                        </tr>
                        <tr class="bg-white border-2 border-[#333333]">
                            <th scope="row"
                                class="py-4 px-6 border-r-2 border-[#333333] font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Ranking Total Penghasilan (#)
                            </th>
                            <td class="py-4 px-6">
                                #1
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="text-sm font-inter font-semibold mt-5">
                <p>Anda membutuhkan Rp <span class="px-5 bg-[#FFFFFF]">450,000</span> untuk menjadi seller dengan total
                    jumlah penjualan tertinggi!</p>
                <p class="text-[#9E3D64] font-inter font-bold text-lg mt-2.5">Ayo Lebih Semangat!!!</p>
            </div>
        </div>
    </div>
</body>

</html>