<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #report:checked+#report {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">

    {{-- Navbar --}}
    <x-be.agent.navbar-agent></x-be.agent.navbar-agent>

    {{-- Sidebar --}}
    <x-be.agent.sidebar-agent></x-be.agent.sidebar-agent>

    {{-- Body --}}
    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pr-10" style="padding-left: 315px">
            <div class="bg-gray-200 rounded-md border-gray-400 p-4">

                <div class="flex">
                    <p>My Order</p>
                    <img id="image" src="{{ asset('storage/icons/chevron-right-arrow.svg') }}"
                        class="w-[15px] h-[20px] mt-[0.2rem] mx-3" alt="polygon 3" title="Kamtuu">
                    <p class="text-blue-600">No Booking: 123BD8095</p>
                </div>

                @if ($method == 'edit')
                    <div class="flex my-3">
                        <a href="{{ route('coupon.show', isset($coupon['id']) ? $coupon->id : null) }}"
                            class="rounded-lg px-8 py-2 text-base bg-yellow-500 text-yellow-100 hover:bg-yellow-600 duration-300 w-[9rem] mx-2">
                            Tampilkan
                        </a>
                        <a href="{{ route('coupon.download', isset($coupon['id']) ? $coupon->id : null) }}" target="_blank"
                            class="rounded-lg px-8 py-2 text-base bg-yellow-500 text-yellow-100 hover:bg-yellow-600 duration-300 w-[7rem] mx-2 disabled:opacity-25"
                            disabled>
                            Export
                        </a>
                        <a href="{{ route('coupon.print', isset($coupon['id']) ? $coupon->id : null) }}" target="_blank"
                            class="rounded-lg px-8 py-2 text-base bg-yellow-500 text-yellow-100 hover:bg-yellow-600 duration-300 w-[7rem] mx-2">
                            Cetak
                        </a>
                        <a href="{{ route('coupon.emaling', isset($coupon['id']) ? $coupon->id : null) }}" target="_blank"
                            class="rounded-lg px-8 py-2 text-base bg-yellow-500 text-yellow-100 hover:bg-yellow-600 duration-300 w-[7rem] mx-2">
                            Email
                        </a>
                    </div>
                @endif

                {{--            wysiwig --}}
                @if ($method == 'edit')
                    <form action="{{ route('coupon.update', isset($coupon->id) ? $coupon->id : null) }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                    @else
                        <form action="{{ route('coupon.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                @endif

                <input type="hidden" name="method" value="{{ $method }}">
                <div class="max-w-6xl bg-white my-5">
                    <div class="grid grid-cols-3 p-3">
                        <div class="p-2">
                            <p class="my-2">
                                Nama Agen
                            </p>
                        </div>
                        <div class="col-span-2">
                            <input class="rounded-md my-2 w-1/2" name="coupon_name" type="text"
                                value="{{ old('coupon_name', $coupon->coupon_name ?? null) }}">
                        </div>
                        <div class="p-2">
                            <p class="my-2">
                                Alamat
                            </p>
                        </div>
                        <div class="col-span-2">
                            <input class="rounded-md my-2 w-1/2" name="coupon_address" type="text"
                                value="{{ old('coupon_address', $coupon->coupon_address ?? null) }}">
                        </div>
                        <div class="p-2">
                            <p class="my-2">
                                No. Telepon
                            </p>
                        </div>
                        <div class="col-span-2">
                            <input class="rounded-md my-2 w-1/2" name="coupon_phone" type="text"
                                value="{{ old('coupon_phone', $coupon->coupon_phone ?? null) }}">
                        </div>

                        <div class="p-2">
                            <p class="my-2">
                                Logo
                            </p>
                        </div>
                        <div class="col-span-2">
                            <input class="my-2 w-full cursor-pointer" id="logos" name="coupon_logo" type="file">
                        </div>
                        <div class="p-2">
                            <div class="my-2">
                                Preview
                            </div>
                        </div>
                        <div class="col-span-2">
                            <input type="hidden" id="coupon_logo"
                                value="{{ isset($coupon->coupon_logo) ? asset($coupon->coupon_logo) : null }}">
                            {{-- <div class="" x-data="printPreview()"> --}}
                            <div
                                class="h-48 w-1/2 rounded bg-gray-100 border border-gray-200 flex item-center justify-center overflow-hidden">
                                <img x-show="imageUrl" src="" alt="" class="w-full object-cover hidden"
                                    id="preview">
                                <div x-show="!imageUrl" class="text-gray-300 flex flex-col items-center relative"
                                    style="top:81px;" id="waterMark">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 " fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12" />
                                    </svg>
                                </div>
                                {{-- <div>Image Preview</div> --}}
                            </div>
                            {{-- </div> --}}
                        </div>
                        <div class="p-2">
                            <p class="my-2">
                                pesanan
                            </p>
                        </div>
                        <div class="col-span-2">
                            <textarea name="coupon_message" class="rounded-md my-2 w-1/2" cols="30" rows="10">{!! old('coupon_message', $coupon->coupon_message ?? null) !!}</textarea>
                            {{-- <input class="rounded-md my-2 w-1/2" name="coupon_phone" type="text" value="{{old('coupon_header',$coupon->coupon_header ?? null)}}"> --}}
                        </div>
                        <div class="p-2">
                            <p class="my-2">
                                Nama Listing
                            </p>
                        </div>
                        <div class="col-span-2">
                            <textarea name="coupon_list" class="rounded-md my-2 w-1/2" cols="30" rows="10">{!! old('coupon_list', $coupon->coupon_list ?? null) !!}</textarea>
                            {{-- <input class="rounded-md my-2 w-1/2" name="coupon_phone" type="text" value="{{old('coupon_header',$coupon->coupon_header ?? null)}}"> --}}
                        </div>
                    </div>
                    {{-- <div class="grid grid-cols-3">
                    </div>
                    <div class="grid grid-cols-3">
                    </div>
                    <div class="grid grid-cols-3">
                    </div> --}}
                    {{-- <div class="bg-white"> --}}
                    {{-- <p class="font-bold text-xl">Nama Agent</p> --}}
                    {{-- <div id="summernoteHeader"></div> --}}
                    {{-- <textarea name="coupon_header" id="summernoteHeader" cols="30" rows="10">{!! old('coupon_header',$coupon->coupon_header ?? null) !!}</textarea> --}}
                    {{-- </div> --}}
                </div>
                {{--            detail kupon --}}
                <div class="grid grid-cols-2">
                    <div class="grid grid-rows-3 grid-flow-col gap-4 border-gray-600 bg-white">
                        <div class="row-span-3">
                            <img class="w-full h-full rounded-md border border-gray-300"
                                src="{{ isset($detail->thumbnail) ? $detail->thumbnail : 'https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"' }}""
                                alt="">
                        </div>
                        <div class="col-span-2">
                            <div class="flex">
                                <img id="image" src="{{ asset('storage/icons/icon-maps.svg') }}"
                                    class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                <label for="image"
                                    class="mx-3 text-base font-semibold">{{ $detail->product->product_name }}</label>
                            </div>
                        </div>
                        <div class="row-span-2">
                            @if ($detail->product->type == 'hotel')
                                <div class="flex"><img id="image"
                                        src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                        class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                    <label for="image"
                                        class="mx-3 text-base font-semibold">{{ Carbon\Carbon::parse($detail->checkin)->translatedFormat('d F Y') }}</label>
                                </div>
                                <p>Start</p>
                            @endif

                        </div>
                        <div class="row-span-2">
                            @if ($detail->product->type == 'hotel')
                                <div class="flex">
                                    <label for="image"
                                        class="mx-3 text-base font-semibold">{{ Carbon\Carbon::parse($detail->checkout)->translatedFormat('d F Y') }}</label>
                                </div>
                                <p>End</p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="grid grid-cols-2">
                    <div class="bg-white">
                        <hr class="my-8 h-px bg-gray-200 border-0 dark:bg-gray-700">
                        <div class="col-span-3">
                            <div class="grid grid-cols-2 pl-2">
                                <div>
                                    <div class="flex"><img id="image"
                                            src="{{ asset('storage/icons/point-maps-black.svg') }}"
                                            class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3"
                                            title="Kamtuu">
                                        <label for="image"
                                            class="mx-3 text-base font-semibold">{{ $detail->alamat }}</label>
                                    </div>
                                </div>
                                <div>
                                    <div class="flex"><img id="image"
                                            src="{{ asset('storage/icons/telephone-black.svg') }}"
                                            class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3"
                                            title="Kamtuu">
                                        <label for="image"
                                            class="mx-3 text-base font-semibold">{{ $detail->no_telp }}</label>
                                    </div>
                                </div>
                                <div class="col-span-2">
                                    <hr class="my-8 h-px bg-gray-200 border-0 dark:bg-gray-700">
                                </div>

                                <div class="col-span-2">
                                    <div class="p-1 bg-blue-500 w-1/2 rounded-md text-white text-center">
                                        <p>Kamar 1 - No. Konfirmasi {{ $booking->booking_code }}</p>
                                    </div>
                                </div>
                                @if ($detail->product->type == 'hotel')
                                    <div class="col-span-2">
                                        <p>{{ $kamar->jenis_kamar }}</p>
                                        <div class="flex"><img id="image"
                                                src="{{ asset('storage/icons/circle-user-solid.svg') }}"
                                                class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3"
                                                title="Kamtuu">
                                            <label for="image" class="mx-3 text-base font-semibold">Direservasi
                                                untuk : {{ $user->first_name }} </label>
                                        </div>
                                        <p>1 Orang Dewasa</p>
                                    </div>
                                @endif
                                <div class="col-span-2">
                                    <hr class="my-8 h-px bg-gray-200 border-0 dark:bg-gray-700">
                                </div>

                                <div class="col-span-2">
                                    <p class="font-bold text-xl">Catatan</p>
                                    <div class="bg-white p-2">
                                        {{-- <div id="summernoteCatatan"></div> --}}
                                        <textarea name="notes" class="rounded" id="summernoteCatatan" cols="30" rows="10">{!! old('coupon_message', $coupon->coupon_message ?? null) !!}</textarea>
                                    </div>
                                </div>
                                @if ($errors->has('notes'))
                                    <span class="text-danger">{{ $errors->first('notes') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                {{--            harga --}}
                <div class="max-w-6xl bg-white my-5">
                    <div class="grid grid-cols-3">
                        <div class="col-span-2 p-5">
                            <p class="text-xl">Harga</p>
                        </div>
                        <div class="grid justify-items-end p-5">
                            <button id="btnHidden"
                                class="rounded-lg px-8 py-2 text-base bg-blue-500 text-blue-100 hover:bg-blue-600 duration-300 w-[9rem] mx-2">
                                Sembunyikan
                            </button>
                        </div>
                        <div class="price_field">
                            <div class="p-5 col-span-3">
                                <p class="my-2">Harga Final</p>
                                <input class="rounded-md my-2 w-1/2 text-right" name="amount" type="text"
                                    value="{{ old('amount', $coupon->amount ?? null) }}">
                                @if ($errors->has('amount'))
                                    <span class="text-danger">{{ $errors->first('amount') }}</span>
                                @endif
                                <p class="my-2">Pajak dan Biaya</p>
                                <input class="rounded-md my-2 w-1/2 text-right" name="tax_amount" type="text"
                                    value="{{ old('tax_amount', $coupon->tax_amount ?? null) }}">
                                @if ($errors->has('tax_amount'))
                                    <span class="text-danger">{{ $errors->first('tax_amount') }}</span>
                                @endif
                                <p class="my-2">Total</p>
                                <input class="rounded-md my-2 w-1/2 text-right" name="total_amount" type="text"
                                    value="{{ old('total_amount', $coupon->total_amount ?? null) }}">
                                @if ($errors->has('total_amount'))
                                    <span class="text-danger">{{ $errors->first('total_amount') }}</span>
                                @endif
                            </div>
                            <div class="px-5">
                                <p class="my-2">Booking Fee</p>
                                <input class="rounded-md my-2 w-full text-right" name="booking_fee" type="text"
                                    value="{{ old('booking_fee', $coupon->booking_fee ?? null) }}">
                            </div>
                            @if ($errors->has('booking_fee'))
                                <span class="text-danger">{{ $errors->first('booking_fee') }}</span>
                            @endif
                            <div class="px-5">
                                <p class="my-2">Catatan Pembayaran</p>
                                <input class="rounded-md my-2 w-full" name="payment_notes" type="text"
                                    value="{{ old('payment_notes', $coupon->payment_notes ?? null) }}">
                            </div>
                            @if ($errors->has('payment_notes'))
                                <span class="text-red-900">{{ $errors->first('payment_notes') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="grid justify-items-end">
                    <button type="submit"
                        class="rounded-lg px-8 py-2 text-base bg-blue-500 text-blue-100 hover:bg-blue-600 duration-300 w-[9rem] mx-2">
                        Simpan
                    </button>
                </div>

                </form>
            </div>
        </div>
    </div>

    <script>
        // $('#summernoteHeader').summernote({
        //     placeholder: 'Tur',
        //     tabsize: 2,
        //     height: 120,
        //     toolbar: [
        //         ['style', ['style']],
        //         ['font', ['bold', 'underline', 'clear']],
        //         ['color', ['color']],
        //         ['para', ['ul', 'ol', 'paragraph']],
        //         ['table', ['table']],
        //         ['insert', ['link', 'picture', 'video']],
        //         ['view', ['fullscreen', 'codeview', 'help']]
        //     ]
        // });

        // $('#summernotePesanan').summernote({
        //     placeholder: 'Pesanan',
        //     tabsize: 2,
        //     height: 120,
        //     toolbar: [
        //         // ['style', ['style']],
        //         ['font', ['bold', 'underline', 'clear']],
        //         ['color', ['color']],
        //         // ['para', ['ul', 'ol', 'paragraph']],
        //         // ['table', ['table']],
        //         // ['insert', ['link', 'picture', 'video']],
        //         // ['view', ['fullscreen', 'codeview', 'help']]
        //     ]
        // });

        // $('#summernoteNamaListing').summernote({
        //     placeholder: 'Nama Listing',
        //     tabsize: 2,
        //     height: 120,
        //     toolbar: [
        //         // ['style', ['style']],
        //         ['font', ['bold', 'underline', 'clear']],
        //         ['color', ['color']],
        //         ['para', ['ul', 'ol', 'paragraph']],
        //         // ['table', ['table']],
        //         // ['insert', ['link', 'picture', 'video']],
        //         // ['view', ['fullscreen', 'codeview', 'help']]
        //     ]
        // });

        // $('#summernoteCatatan').summernote({
        //     placeholder: 'Nama Listing',
        //     tabsize: 2,
        //     height: 120,
        //     toolbar: [
        //         // ['style', ['style']],
        //         ['font', ['bold', 'underline', 'clear']],
        //         ['color', ['color']],
        //         // ['para', ['ul', 'ol', 'paragraph']],
        //         // ['table', ['table']],
        //         // ['insert', ['link', 'picture', 'video']],
        //         // ['view', ['fullscreen', 'codeview', 'help']]
        //     ]
        // });

        $("#btnHidden").click(function(e) {
            e.preventDefault();
            // $(".price_field").addClass()
            $(".price_field").toggle();
        });

        let img = document.getElementById('logos');
        let imgPreview = document.getElementById('preview');
        let waterMark = document.getElementById('waterMark');

        img.addEventListener('change', function() {
            dataFile = img.files[0];
            if (dataFile) {
                const fileReader = new FileReader();
                fileReader.readAsDataURL(dataFile);
                fileReader.addEventListener("load", function() {
                    waterMark.classList.add("hidden");
                    imgPreview.setAttribute("src", this.result);
                    imgPreview.style.display = 'block';
                })
            }
        });

        let coupon_logo = document.getElementById('coupon_logo');

        if (coupon_logo.value !== "") {
            console.log(coupon_logo.value);
            waterMark.classList.add("hidden");
            imgPreview.setAttribute("src", coupon_logo.value);
            imgPreview.style.display = 'block';
        }
    </script>
</body>

</html>
