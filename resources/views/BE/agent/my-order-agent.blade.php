<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #report:checked+#report {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">

    {{-- Navbar --}}
    <x-be.agent.navbar-agent></x-be.agent.navbar-agent>

    {{-- Sidebar --}}
    <x-be.agent.sidebar-agent></x-be.agent.sidebar-agent>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pr-10" style="padding-left: 315px">
            <span class="text-sm font-bold font-inter text-[#000000]">Tanggal bergabung: 17 Agutus 2022</span>

            {{-- body --}}
            <div class="max-w-6xl bg-gray-200 rounded-md border border-black p-2 my-5">
                <div class="grid grid-cols-2">
                    <div class="grid justify-items-start">
                        <p class="font-bold text-base">List Pesanan</p>
                    </div>
                    <div class="grid justify-items-end">
                        <div class="pt-2 relative text-gray-600">
                            <input
                                class="border-2 border-gray-300 bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none"
                                type="search" name="search" placeholder="Search">
                            <button type="submit" class="absolute right-0 top-0 mt-5 mr-4">
                                <svg class="text-gray-600 h-4 w-4 fill-current" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1"
                                    x="0px" y="0px" viewBox="0 0 56.966 56.966"
                                    style="enable-background:new 0 0 56.966 56.966;" xml:space="preserve" width="512px"
                                    height="512px">
                                    <path
                                        d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="grid grid-cols-2 my-2">
                    <div class="grid justify-items-start">
                        {{-- modal filter tanggal --}}
                        <div class="" x-data="{ 'showModal': false }" @keydown.escape="showModal = false" x-cloak>
                            <div class="flex justify-end -mt-5 text-sm">
                                <button type="button"
                                    class="rounded-lg px-5 py-2 text-base bg-blue-500 text-blue-100 hover:bg-blue-600 duration-300 mr-2"
                                    @click="showModal = true">Filter Tanggal
                                </button>
                                <a href="{{ route('welcome') }}"
                                    class="rounded-lg px-5 py-2 text-base bg-blue-500 text-blue-100 hover:bg-blue-600 duration-300">Lanjut
                                    Belanja</a>
                            </div>

                            <!--Overlay-->
                            <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="showModal"
                                :class="{ 'absolute inset-0 z-10 flex items-center justify-center': showModal }">
                                <!--Dialog-->
                                <div class="bg-white w-[42rem] max-w-4xl md:max-w-2xl mx-auto rounded shadow-lg py-4 text-left px-6"
                                    x-show="showModal" @click.away="showModal = false"
                                    x-transition:enter="ease-out duration-300"
                                    x-transition:enter-start="opacity-0 scale-90"
                                    x-transition:enter-end="opacity-100 scale-100"
                                    x-transition:leave="ease-in duration-300"
                                    x-transition:leave-start="opacity-100 scale-100"
                                    x-transition:leave-end="opacity-0 scale-90">

                                    <!--Title-->
                                    <div class="flex justify-between items-center pb-3">
                                        <p class="text-2xl font-bold">Filter Tanggal</p>
                                        <div class="cursor-pointer z-50" @click="showModal = false">
                                            <svg class="fill-current text-black" xmlns="http://www.w3.org/2000/svg"
                                                width="18" height="18" viewBox="0 0 18 18">
                                                <path
                                                    d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                                </path>
                                            </svg>
                                        </div>
                                    </div>

                                    <!-- content -->
                                    <form action="">

                                    </form>

                                    <!--Footer-->
                                    <div class="flex justify-end pt-2">
                                        <button
                                            class="px-4 bg-transparent p-3 rounded-lg text-indigo-500 hover:bg-gray-100 hover:text-indigo-400 mr-2"
                                            @click="alert('Additional Action');">Simpan
                                        </button>
                                        <button
                                            class="modal-close px-4 bg-indigo-500 p-3 rounded-lg text-white hover:bg-indigo-400"
                                            @click="showModal = false">Close
                                        </button>
                                    </div>
                                </div>
                                <!--/Dialog -->
                            </div><!-- /Overlay -->
                        </div>


                    </div>
                    <div class="grid justify-items-end">
                        <select class="pr-7 rounded-md pl-2 bg-gray-200 border border-black" name=""
                            id="">
                            <option value="">Keterangan</option>
                        </select>
                    </div>
                </div>

                {{-- list order --}}
                @foreach ($data as $item)
                    @php
                        $detail_produk = App\Models\Productdetail::where('id', $item->product_detail_id)->first();
                        $produk = App\Models\Product::where('id', $detail_produk->product_id)->first();
                    @endphp
                    <div class="bg-kamtuu-produk-success bg-opacity-25 p-3 rounded-md border border-gray-400 my-5">
                        <div class="grid grid-cols-[13%_57%_30%] ">
                            <div class="grid justify-items-center">
                                <a href="{{ route('viewDetailOrder', $item->booking_code) }}">
                                    <img class="w-28 h-28 rounded-md border border-gray-300"
                                        src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                        alt="">
                                </a>
                            </div>

                            <div>
                                <p>{{ $produk->type }}</p>
                                <p class="text-xl">{{ $produk->product_name }}</p>
                                <p class="text-gray-600">27 Agustus 2022</p>
                                @if ($produk->type == 'hotel')
                                    <p>Check-In: 16 Agustus 2022</p>
                                    <p>Check-Out: 17 Agustus 2022</p>
                                    <p class="text-gray-600">1 Malam • 5 Tamu</p>
                                    <p>AKAN berlangsung dalam <span class="text-yellow-600">5 hari 2 jam 32
                                            menit</span>
                                    </p>
                                @endif
                                @if ($produk->type == 'xstay')
                                    <p>Check-In: 16 Agustus 2022</p>
                                    <p>Check-Out: 17 Agustus 2022</p>
                                    <p class="text-gray-600">1 Malam • 5 Tamu</p>
                                    <p>AKAN berlangsung dalam <span class="text-yellow-600">5 hari 2 jam 32
                                            menit</span>
                                    </p>
                                @endif
                                @if ($produk->type == 'Transfer')
                                    @php
                                        $detail_mobil = App\Models\Mobildetail::where('id', $detail_produk->detailmobil_id)->first();
                                    @endphp
                                    <div class="flex py-2 gap-1 lg:gap-2">
                                        <div
                                            class="bg-[#9E3D64] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                            Transfer</div>
                                        <div
                                            class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                            Sedan</div>
                                    </div>
                                    <div class="flex">
                                        <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                            src="{{ asset('storage/icons/seats.svg') }}" alt="rating"
                                            width="17px" height="17px">
                                        <p class="font-bold text-[8px] lg:text-sm mr-2">
                                            {{ $detail_mobil->kapasitas_kursi }} Seats</p>
                                        <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                            src="{{ asset('storage/icons/koper.svg') }}" alt="rating"
                                            width="17px" height="17px">
                                        <p class="font-bold  text-[8px] lg:text-sm mr-2">
                                            {{ $detail_mobil->kapasitas_koper }} Koper</p>
                                    </div>
                                    <p>Pick up: Yogyakarta International Airport</p>
                                    <p>Drop off: Hotel Yogyakarta</p>
                                    <p>AKAN berlangsung dalam <span class="text-yellow-600">5 hari 2 jam 32
                                            menit</span></p>
                                @endif
                                @if ($produk->type == 'Rental')
                                    <div class="flex py-2 gap-1 lg:gap-2">
                                        <div
                                            class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                            Sedan</div>
                                    </div>
                                    <div class="flex">
                                        <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                            src="{{ asset('storage/icons/seats.svg') }}" alt="rating"
                                            width="17px" height="17px">
                                        <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                                        <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                            src="{{ asset('storage/icons/koper.svg') }}" alt="rating"
                                            width="17px" height="17px">
                                        <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                                    </div>
                                    <p class="text-gray-600">Mulai Rental: 23 Agustus 2022</p>
                                    <p class="text-gray-600">Selesai Rental: 23 Agustus 2022</p>
                                @endif
                            </div>

                            <div class="grid justify-items-end">
                                <p>No Booking: {{ $item->booking_code }}</p>
                                <p class="text-green-600">Telah dikonfirmasi</p>
                                <p class="text-red-600">Belum Dibayar</p>
                                <p class="text-blue-600 text-2xl">IDR {{ number_format($item->total_price) }}</p>
                                <form action="{{ route('booking.destroy', $item->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="hidden" name="kode_booking" value="{{ $item->booking_code }}">
                                    <button type="submit"
                                        class="items-center px-px py-1 lg:py-2 w-24 lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">Hapus</button>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
                @if ($data != '')
                    <p class="text-blue-600 text-2xl text-end">Total Harga : IDR
                        {{ number_format($data->sum('total_price')) }}</p>
                @endif

                {{-- <div class="bg-kamtuu-produk-success bg-opacity-25 p-3 rounded-md border border-gray-400 my-5">
                    <div class="grid grid-cols-[13%_57%_30%] ">
                        <div class="grid justify-items-center">
                            <img class="w-28 h-28 rounded-md border border-gray-300"
                                src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                alt="">
                        </div>

                        <div>
                            <p>Tur</p>
                            <p class="text-xl">Tur Pantai</p>
                            <p class="text-gray-600">27 Agustus 2022</p>
                            <button
                                class="rounded-lg px-8 py-2 text-base bg-blue-500 text-blue-100 hover:bg-blue-600 duration-300">
                                Alihkan
                            </button>
                            <p>AKAN berlangsung dalam <span class="text-yellow-600">5 hari 2 jam 32 menit</span></p>
                        </div>

                        <div class="grid justify-items-end">
                            <p>No Booking: 123BD8095</p>
                            <p class="text-green-600">Telah dikonfirmasi</p>
                            <p class="text-red-600">Dialihkan</p>
                            <p class="text-blue-600 text-2xl">IDR 100,000</p>
                            <p class="text-gray-500">(Diskon IDR 20,000)</p>
                        </div>
                    </div>
                </div>

                <div class="bg-kamtuu-produk-success bg-opacity-25 p-3 rounded-md border border-gray-400 my-5">
                    <div class="grid grid-cols-[13%_57%_30%] ">
                        <div class="grid justify-items-center">
                            <img class="w-28 h-28 rounded-md border border-gray-300"
                                src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                alt="">
                        </div>

                        <div>
                            <p>Transfer</p>
                            <p class="text-xl">Transfer Bandara</p>
                            <p class="text-gray-600">27 Agustus 2022</p>
                            <p>Pick up: Yogyakarta International Airport</p>
                            <p>Drop off: Hotel Yogyakarta</p>
                            <p>AKAN berlangsung dalam <span class="text-yellow-600">5 hari 2 jam 32 menit</span></p>
                        </div>

                        <div class="grid justify-items-end">
                            <p>No Booking: 123BD8095</p>
                            <p class="text-green-600">Telah dikonfirmasi</p>
                            <p class="text-red-600">Dialihkan</p>
                            <p class="text-blue-600 text-2xl">IDR 100,000</p>
                            <p class="text-gray-500">(Diskon IDR 20,000)</p>
                        </div>
                    </div>
                </div>

                <div class="bg-kamtuu-produk-success bg-opacity-25 p-3 rounded-md border border-gray-400 my-5">
                    <div class="grid grid-cols-[13%_57%_30%] ">
                        <div class="grid justify-items-center">
                            <img class="w-28 h-28 rounded-md border border-gray-300"
                                src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                alt="">
                        </div>

                        <div>
                            <p>Hotel</p>
                            <p class="text-xl">Transfer Bandara</p>
                            <p class="text-gray-600">27 Agustus 2022</p>
                            <p>Check-In: 16 Agustus 2022</p>
                            <p>Check-Out: 17 Agustus 2022</p>
                            <p class="text-gray-600">1 Malam • 5 Tamu</p>
                            <p>AKAN berlangsung dalam <span class="text-yellow-600">5 hari 2 jam 32 menit</span></p>
                        </div>

                        <div class="grid justify-items-end">
                            <p>No Booking: 123BD8095</p>
                            <p class="text-green-600">Telah dikonfirmasi</p>
                            <p class="text-red-600">Belum Dibayar</p>
                            <p class="text-blue-600 text-2xl">IDR 100,000</p>
                            <p class="text-gray-500">(Diskon IDR 20,000)</p>
                        </div>
                    </div>
                </div>

                <div class="bg-kamtuu-produk-warning bg-opacity-25 p-3 rounded-md border border-gray-400 my-5">
                    <div class="grid grid-cols-[13%_57%_30%] ">
                        <div class="grid justify-items-center">
                            <img class="w-28 h-28 rounded-md border border-gray-300"
                                src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                alt="">
                        </div>

                        <div>
                            <p>Activity</p>
                            <p class="text-xl">Activity Bandara</p>
                            <p class="text-gray-600">27 Agustus 2022</p>
                            <p class="text-gray-600">1 Tiket Dewasa • 27 Agustus 2022</p>
                            <button
                                class="rounded-lg px-8 py-2 text-base bg-yellow-500 text-yellow-100 hover:bg-yellow-600 duration-300">
                                Tulis Ulasan
                            </button>
                        </div>

                        <div class="grid justify-items-end">
                            <p>No Booking: 123BD8095</p>
                            <p class="text-black"> Selesai</p>
                            <p class="text-blue-600 text-2xl">IDR 100,000</p>
                            <p class="text-gray-500">(Diskon IDR 20,000)</p>
                        </div>
                    </div>
                </div>

                <div class="bg-kamtuu-produk-running bg-opacity-25 p-3 rounded-md border border-gray-400 my-5">
                    <div class="grid grid-cols-[13%_57%_30%] ">
                        <div class="grid justify-items-center">
                            <img class="w-28 h-28 rounded-md border border-gray-300"
                                src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                alt="">
                        </div>

                        <div>
                            <p>Rental</p>
                            <p class="text-xl">Xpander</p>
                            <p class="text-gray-600">27 Agustus 2022</p>
                            <p class="text-gray-600">Mulai Rental: 23 Agustus 2022</p>
                            <p class="text-gray-600">Selesai Rental: 23 Agustus 2022</p>
                        </div>

                        <div class="grid justify-items-end">
                            <p>No Booking: 123BD8095</p>
                            <p class="text-blue-600"> On-Progress</p>
                            <p class="text-blue-600 text-2xl">IDR 100,000</p>
                            <p class="text-gray-500">(Diskon IDR 20,000)</p>
                        </div>
                    </div>
                </div> --}}
            </div>

            {{-- Checkout Order --}}
            <div class="flex space-x-3">
                @if ($data != '' && $getBookingId != '')
                    <form action="{{ route('order.store') }}" method="POST">
                        @csrf
                        @if (isset($diskon))
                            @php
                                $hitung_diskon = intval($diskon) / 100;
                                $result = intval($data->sum('total_price')) * $hitung_diskon;
                            @endphp
                            <input type="hidden" name="harga" value="{{ $data->sum('total_price') - $result }}">
                        @else
                            <input type="hidden" name="harga" value="{{ $data->sum('total_price') }}">
                        @endif
                        <input type="hidden" name="booking_id" value="{{ $getBookingId->id }}">
                        <input type="hidden" name="nama_customer" value="{{ auth()->user()->first_name }}">
                        <input type="hidden" name="customer_id" value="{{ $user->id }}">
                        <button type="submit"
                            class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                            Check Out Order
                        </button>
                    </form>
                @endif
            </div>

        </div>
    </div>


    <script></script>
</body>

</html>
