<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #report:checked+#report {
            display: block;
        }

        [x-cloak] {
            display: none;
        }

        .duration-300 {
            transition-duration: 300ms;
        }

        .ease-in {
            transition-timing-function: cubic-bezier(0.4, 0, 1, 1);
        }

        .ease-out {
            transition-timing-function: cubic-bezier(0, 0, 0.2, 1);
        }

        .scale-90 {
            transform: scale(.9);
        }

        .scale-100 {
            transform: scale(1);
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">

    {{-- Navbar --}}
    <x-be.agent.navbar-agent></x-be.agent.navbar-agent>

    {{-- Sidebar --}}
    <x-be.agent.sidebar-agent></x-be.agent.sidebar-agent>

    {{-- Body --}}
    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pr-10" style="padding-left: 315px">
            <p class="text-gray">
                My Account
            </p>
            <div class="max-w-6xl bg-gray-200 rounded-md border border-black p-2 my-5">
                <p class="font-bold text-base">Informasi Manajer Agensi</p>

                {{-- modal informasi manajer --}}
                <div class="" x-data="{ 'showModal': false }" @keydown.escape="showModal = false" x-cloak>
                    <div class="flex justify-end -mt-5 text-sm">
                        <button type="button"
                            class="px-8 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white"
                            @click="showModal = true">Edit Informasi Manajer
                        </button>
                    </div>

                    <!--Overlay-->
                    <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="showModal"
                        :class="{ 'absolute inset-0 z-10 flex items-center justify-center': showModal }">
                        <!--Dialog-->
                        <div class="bg-white w-[42rem] max-w-4xl md:max-w-2xl mx-auto rounded shadow-lg py-4 text-left px-6"
                            x-show="showModal" @click.away="showModal = false"
                            x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0 scale-90"
                            x-transition:enter-end="opacity-100 scale-100" x-transition:leave="ease-in duration-300"
                            x-transition:leave-start="opacity-100 scale-100"
                            x-transition:leave-end="opacity-0 scale-90">

                            <!--Title-->
                            <div class="flex justify-between items-center pb-3">
                                <p class="text-2xl font-bold">Edit Informasi Manajer</p>
                                <div class="cursor-pointer z-50" @click="showModal = false">
                                    <svg class="fill-current text-black" xmlns="http://www.w3.org/2000/svg"
                                        width="18" height="18" viewBox="0 0 18 18">
                                        <path
                                            d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                        </path>
                                    </svg>
                                </div>
                            </div>

                            <!-- content -->
                            <form action="{{ route('updateAccountManager', auth()->user()->id) }}" method="POST">
                                @csrf
                                @method('PUT')
                                <p class="text-gray-600 my-2 ">Nama Depan</p>
                                <input class="rounded-md w-full" type="text" placeholder="James" name="first_name"
                                    value="{{ auth()->user()->first_name }}">
                                <p class="text-gray-600 my-2 ">Nama Belakang</p>
                                <input class="rounded-md w-full" type="text" placeholder="Lockhart" name="last_name"
                                    value="{{ $profil->last_name }}">
                                <p class="text-gray-600 my-2 ">Alamat Email</p>
                                <input class="rounded-md w-full" type="text" placeholder="@gmail.com" name="email"
                                    value="{{ auth()->user()->email }}" readonly>
                                <p class="text-gray-600 my-2 ">No Telp</p>
                                <input class="rounded-md w-full" placeholder="+628" type="text" name="no_tlp"
                                    value="{{ auth()->user()->no_tlp }}">

                                <!--Footer-->
                                <div class="flex justify-end pt-2">
                                    <button type="submit"
                                        class="px-4 bg-transparent p-3 rounded-lg text-indigo-500 hover:bg-gray-100 hover:text-indigo-400 mr-2">Simpan
                                    </button>
                                    <button type="button"
                                        class="modal-close px-4 bg-indigo-500 p-3 rounded-lg text-white hover:bg-indigo-400"
                                        @click="showModal = false">Close
                                    </button>
                                </div>
                            </form>
                        </div>
                        <!--/Dialog -->
                    </div><!-- /Overlay -->
                </div>

                {{-- Profile Detail --}}
                <div class="grid grid-cols-3 mx-2">
                    <div>
                        <p class="font-bold">Nama Depan</p>
                        <p>{{ auth()->user()->first_name }}</p>
                    </div>
                    <div class="col-span-2">
                        <p class="font-bold">Nama Belakang</p>
                        @if ($profil->last_name == null)
                            <p><i>Not Set !</i></p>
                        @else
                            <p>{{ $profil->last_name }}</p>
                        @endif
                    </div>

                    <div class="col-span-3 mt-5">
                        <p class="font-bold">Alamat Email</p>
                        <p>{{ auth()->user()->email }}</p>
                    </div>

                    <div class="col-span-3 mt-5">
                        <p class="font-bold">No Telp.</p>
                        <p>{{ auth()->user()->no_tlp }}</p>
                    </div>
                </div>
            </div>


            <div class="max-w-6xl bg-gray-200 rounded-md border border-black p-2 my-2">
                <p class="font-bold text-base">Informasi Agensi</p>
                {{-- modal informasi agensi --}}
                <div class="" x-data="{ 'showModalAgensi': false }" @keydown.escape="showModalAgensi = false" x-cloak>
                    <div class="flex justify-end -mt-5 text-sm">
                        <button type="button"
                            class="px-8 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white"
                            @click="showModalAgensi = true">Edit Informasi Agensi
                        </button>
                    </div>

                    <!--Overlay-->
                    <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="showModalAgensi"
                        :class="{ 'absolute inset-0 z-10 flex items-center justify-center': showModalAgensi }">
                        <!--Dialog-->
                        <div class="bg-white w-[42rem] max-w-4xl md:max-w-2xl mx-auto rounded shadow-lg py-4 text-left px-6 overflow-y-auto max-h-80"
                            x-show="showModalAgensi" @click.away="showModalAgensi = false"
                            x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0 scale-90"
                            x-transition:enter-end="opacity-100 scale-100" x-transition:leave="ease-in duration-300"
                            x-transition:leave-start="opacity-100 scale-100"
                            x-transition:leave-end="opacity-0 scale-90">

                            <!--Title-->
                            <div class="flex justify-between items-center pb-3">
                                <p class="text-2xl font-bold">Edit Informasi Agensi</p>
                                <div class="cursor-pointer z-50" @click="showModalAgensi = false">
                                    <svg class="fill-current text-black" xmlns="http://www.w3.org/2000/svg"
                                        width="18" height="18" viewBox="0 0 18 18">
                                        <path
                                            d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                                        </path>
                                    </svg>
                                </div>
                            </div>

                            <!-- content -->
                            <form action="{{ route('updateAccountAgent', auth()->user()->id) }}" method="POST">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="email_booking" value="{{ auth()->user()->email }}">
                                <p class="text-gray-600 my-2 ">Negara</p>
                                <select
                                    class="appearance-none w-full form-select border-1 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                    name="negara" id="" required>
                                    <option value="">Pilih Negara</option>
                                    <option {{ $profil->negara == 'Indonesia' ? 'selected' : '' }} value="Indonesia">
                                        Indonesia</option>
                                    <option {{ $profil->negara == 'Jepang' ? 'selected' : '' }} value="Jepang">Jepang
                                    </option>
                                </select>
                                <p class="text-gray-600 my-2 ">Nama Perusahaan</p>
                                <input class="rounded-md w-full" type="text" placeholder="PT. ..."
                                    name="first_name_booking" value="{{ $profil->first_name_booking }}">
                                <p class="text-gray-600 my-2 ">Nama Dagang Perusahaan</p>
                                <input class="rounded-md w-full" type="text" placeholder="Jual"
                                    name="last_name_booking" value="{{ $profil->last_name_booking }}">
                                <p class="text-gray-600 my-2 ">No Telp</p>
                                <input class="rounded-md w-full" type="text" placeholder="+62"
                                    name="phone_number_booking" value="{{ $profil->phone_number_booking }}">
                                <p class="text-gray-600 my-2 ">Alamat</p>
                                <input class="rounded-md w-full" type="text" placeholder="Jl. ..." name="address"
                                    value="{{ $profil->address }}">
                                <p class="text-gray-600 my-2 ">Kota</p>
                                <select
                                    class="appearance-none w-full form-select border-1 focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 rounded-md bg-white"
                                    name="province_id" id="" required>
                                    <option value="">Pilih Kota</option>
                                    @foreach ($kota as $item)
                                        <option {{ $profil->province_id == $item->id ? 'selected' : '' }}
                                            value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                <p class="text-gray-600 my-2 ">Kode Pos</p>
                                <input class="rounded-md w-full" type="text" placeholder="0000"
                                    name="postal_code" value="{{ $profil->postal_code }}">
                                <p class="text-gray-600 my-2 ">Alamat Web</p>
                                <input class="rounded-md w-full" type="text" placeholder="https://"
                                    name="alamat_web" value="{{ $profil->alamat_web }}">

                                <!--Footer-->
                                <div class="flex justify-end pt-2">
                                    <button type="submit"
                                        class="px-4 bg-transparent p-3 rounded-lg text-indigo-500 hover:bg-gray-100 hover:text-indigo-400 mr-2">Simpan
                                    </button>
                                    <button type="button"
                                        class="modal-close px-4 bg-indigo-500 p-3 rounded-lg text-white hover:bg-indigo-400"
                                        @click="showModalAgensi = false">Close
                                    </button>
                                </div>
                            </form>
                        </div>
                        <!--/Dialog -->
                    </div><!-- /Overlay -->
                </div>

                {{-- Profile Detail --}}
                <div class="grid grid-cols-3 mx-2">
                    <div>
                        <p class="font-bold">Negara</p>
                        @if ($profil->negara == null)
                            <p><i>Not Set !</i></p>
                        @else
                            <p>{{ $profil->negara }}</p>
                        @endif
                    </div>
                    <div class="col-span-2">
                        <p class="font-bold">ID Agen</p>
                        <p>{{ auth()->user()->id }}</p>
                    </div>

                    <div class=" mt-5">
                        <p class="font-bold">Nama Dagang Agensi</p>
                        @if ($profil->first_name_booking == null)
                            <p><i>Not Set !</i></p>
                        @else
                            <p>{{ $profil->first_name_booking }}</p>
                        @endif
                    </div>

                    <div class="col-span-2 mt-5">
                        <p class="font-bold">Nama Perusahaan</p>
                        @if ($profil->last_name_booking == null)
                            <p><i>Not Set !</i></p>
                        @else
                            <p>{{ $profil->last_name_booking }}</p>
                        @endif
                    </div>

                    <div class=" mt-5">
                        <p class="font-bold">No Telp.</p>
                        @if ($profil->phone_number_booking == null)
                            <p><i>Not Set !</i></p>
                        @else
                            <p>{{ $profil->phone_number_booking }}</p>
                        @endif
                    </div>

                    <div class=" mt-5">
                        <p class="font-bold">Alamat</p>
                        @if ($profil->address == null)
                            <p><i>Not Set !</i></p>
                        @else
                            <p>{{ $profil->address }}</p>
                        @endif
                    </div>

                    <div class=" mt-5">
                        <p class="font-bold">Kota</p>
                        @if ($profil->province_id == null)
                            <p><i>Not Set !</i></p>
                        @else
                            <p>{{ $data_kota->name }}</p>
                        @endif
                    </div>

                    <div class="col-span-2 mt-5">
                        <p class="font-bold">Kode Pos</p>
                        @if ($profil->postal_code == null)
                            <p><i>Not Set !</i></p>
                        @else
                            <p>{{ $profil->postal_code }}</p>
                        @endif
                    </div>

                    <div class="col-span-2 mt-5">
                        <p class="font-bold">Alamat Web</p>
                        @if ($profil->alamat_web == null)
                            <p><i>Not Set !</i></p>
                        @else
                            <a href="{{ $profil->alamat_web }}">
                                <p class="text-kamtuu-second">{{ $profil->alamat_web }}</p>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
