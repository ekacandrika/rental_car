<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #report:checked+#report {
            display: block;
        }

        [x-cloak] {
            display: none;
        }

        .duration-300 {
            transition-duration: 300ms;
        }

        .ease-in {
            transition-timing-function: cubic-bezier(0.4, 0, 1, 1);
        }

        .ease-out {
            transition-timing-function: cubic-bezier(0, 0, 0.2, 1);
        }

        .scale-90 {
            transform: scale(.9);
        }

        .scale-100 {
            transform: scale(1);
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">

    {{-- Navbar --}}
    <x-be.agent.navbar-agent></x-be.agent.navbar-agent>

    {{-- Sidebar --}}
    <x-be.agent.sidebar-agent></x-be.agent.sidebar-agent>


    <div class="z-0 col-start-3 col-end-11">
        <div class="p-5 pr-10" style="padding-left: 315px">
            <h1 class="font-bold font-inter">Edit Account</h1>

            {{-- Info Manajer --}}
            <div class="pb-5 mt-5 mb-5 rounded drop-shadow-xl">
                <div class="">
                    <div class="w-auto flex bg-[#ffff] rounded mr-5">
                        <div class="w-full">
                            <h2 class="p-5 font-bold text-[#9E3D64]">Informasi Manajer</h2>
                            <form action="{{ route('agent.updateData', ['id' => $agent[0]->id, 'status' => 1]) }}"
                                method="POST">
                                @csrf
                                @method('PUT')
                                <div class="grid grid-cols-2 p-5">
                                    <div class="p-2">
                                        <div>
                                            <label for="" class="block text-sm font-medium leading-5">Nama
                                                Depan</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <input id="nama_depan" name="nama_depan" type="text" required
                                                    value="{{ old('nama_depan', $agent[0]->nama_depan) }}"
                                                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            <label for="" class="block text-sm font-medium leading-5">Nama
                                                Belakang</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <input id="nama_belakang" name="nama_belakang" type="text" required
                                                    value="{{ $agent[0]->nama_belakang }}"
                                                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            <label for="" class="block text-sm font-medium leading-5">Alamat Email</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <input id="alamat_email" name="alamat_email" type="text" required
                                                    value="{{ $agent[0]->alamat_email }}"
                                                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            <label for="" class="block text-sm font-medium leading-5">No Telepon</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <input id="no_telepon" name="no_telepon" type="text" required
                                                    value="{{ $agent[0]->no_telepon }}"
                                                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid p-5 justify-items-start">
                                    <button
                                        class="px-8 py-2 lg:text-md md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">Perbarui</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Data Diri --}}
            <div class="pb-5 mt-5 mb-5 rounded drop-shadow-xl">
                <div class="">
                    <div class="w-auto flex bg-[#ffff] rounded mr-5">
                        <div class="w-full">
                            <h2 class="p-5 font-bold text-[#9E3D64]">Informasi Agen</h2>
                            <form action="{{ route('agent.updateData', ['id' => $agent[0]->id, 'status' => 2]) }}"
                                method="POST">
                                @csrf
                                @method('PUT')
                                <div class="grid grid-cols-2 p-5">
                                    <div class="p-2">
                                        <div>
                                            <label for="" class="block text-sm font-medium leading-5">Negara</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <input id="negara" name="negara" type="text" required
                                                    value="{{ old('negara', $agent[0]->negara) }}"
                                                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            <label for="" class="block text-sm font-medium leading-5">Nama Dagang</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <input id="nama_dagang" name="nama_dagang" type="text" required
                                                    value="{{ $agent[0]->nama_dagang }}"
                                                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            <label for="" class="block text-sm font-medium leading-5">Nama Perusahaan</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <input id="nama_perusahaan" name="nama_perusahaan" type="text" required
                                                    value="{{ $agent[0]->nama_perusahaan }}"
                                                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            <label for="" class="block text-sm font-medium leading-5">No Telepon</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <input id="no_telp" name="no_telp" type="text" required
                                                    value="{{ $agent[0]->no_telp }}"
                                                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            <label for="" class="block text-sm font-medium leading-5">Alamat</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <textarea name="alamat" id="alamat" cols="10" rows="5"
                                                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">{{ $agent[0]->alamat }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            <label for="" class="block text-sm font-medium leading-5">Kota</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <select name="kota" id="kota"
                                                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                                                    <option value="">Yogyakarta</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            <label for="" class="block text-sm font-medium leading-5">Kode Pos</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <input id="kode_pos" name="kode_pos" type="text" required
                                                    value="{{ $agent[0]->kode_pos }}"
                                                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <div>
                                            <label for="" class="block text-sm font-medium leading-5">Website</label>
                                            <div class="mt-1 rounded-md shadow-sm">
                                                <input id="website" name="website" type="text" required
                                                    value="{{ $agent[0]->website }}"
                                                    class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid p-5 justify-items-start">
                                    <button
                                        class="px-8 py-2 lg:text-md md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">Perbarui</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</body>

</html>