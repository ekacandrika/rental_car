<!DOCTYPE html>
<html>
<head>
    {{-- <title>Itinarary Notify</title> --}}
    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
    crossorigin="anonymous"></script>
    {{-- <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet"> --}}
    {{-- <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script> --}}

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    {{-- <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'> --}}
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    {{-- <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> --}}
    {{-- <link href="vendor/font-awesome/css/all.min.css" rel="stylesheet"> --}}
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <style>
        html *{
            font-size: 11px;
            color: #000 !important;
            font-family: Arial !important;
        }

        body {
        background: #ffffff;
        color: #535b61;
        font-family: "Poppins", sans-serif;
        font-size: 14px;
        line-height: 22px;
        }
        /* =================================== */
        /*  Layouts
        /* =================================== */
        .itinerary-container {
            margin: 15px auto;
            padding: 70px;
            max-width: 850px;
            background-color: #fff;
            border: 1px solid #ccc;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            -o-border-radius: 6px;
            border-radius: 6px;
        }
        .text-gray-50{
            color: rgb(249 250 251) !important;
        }
        .text-gray-100{
            color: rgb(243 244 246) !important;
        }
        .text-gray-200	{
            color: rgb(229 231 235) !important;
        }
        .text-gray-300	{
            color: rgb(209 213 219) !important;
        }
        .text-gray-400	{
            color: rgb(156 163 175) !important;
        }
        .text-gray-500	{
            color: rgb(107 114 128) !important;
        }
        .text-gray-600	{
            color: rgb(75 85 99) !important;
        }
        .text-gray-700	{
            color: rgb(55 65 81) !important;
        }
        .text-gray-800	{
            color: rgb(31 41 55) !important;
        }
        .text-gray-900	{
            color: rgb(17 24 39) !important;
        }
        @media (max-width: 767px) {
            .itinerary-container {
                padding: 35px 20px 70px 20px;
                margin-top: 0px;
                border: none;
                border-radius: 0px;
            }
        }
        @page {
            font-size: 13px !important;
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
            color: #000 !important;
            font-family: Arial !important;
            line-height: .5 !important;

        }
    </style>
</head>
<body>
    <div class="container-fluid itinerary-container">
        <header>
            <div class="row align-items-center">
                <div class="col-sm-5 text-start text-sm-left">
                    <h4 class="text-3 mb-0 font-bold">
                        {{$data->coupon_name}}
                    </h4>
                </div>
                <div class="col-sm-5 text-start text-sm-left">
                    <h4 class="text-3 mb-0" style="color:#9e9a9a !important;">
                        {{$data->coupon_address}}
                    </h4>
                </div>
                <div class="col-sm-5 text-start text-sm-left">
                    <h4 class="text-3 mb-0" style="color:#9e9a9a !important;">
                        {{$data->coupon_phone}}
                    </h4>
                </div>
            </div>
            <hr class="my-4 mb-3 border-2 border-black">
        </header>
        <main>
            <div class="row">
                <div class="col-sm-4">
                    <strong class="font-weight-600" style="font-size:14px">Pesanan Anda Telah Dikonfirmasi!</strong>
                </div>
                <div class="col-sm-4">
                    <p>{{$data->coupon_message}}</p>
                </div>
            </div>
            <div class="grid grid-rows-2 grid-flow-col gap-2 mt-7">
                <div class="col-start-1">
                    <div class="text-start">
                        <div class="flex">
                            <img id="image" src="{{asset('icon/hotel-solid.svg')}}" style="z-index:1;width:15px;height:15px;" class="mr-1" alt="logo 3"/>
                            <span class="font-bold text-2xl" style="margin-top:-9px">{{$detail->product->product_name}}, {{$detail->alamat}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-start-1">
                    <div class="text-start">
                        <span class="text-gray-300">-|&nbsp;No.&nbsp; booking {{$booking->booking_code}}</span>
                    </div>
                </div>
            </div>
            <div class="grid grid-rows-3 grid-flow-col">
                <div class="row-span-3"><img src="https://images.unsplash.com/photo-1554629947-334ff61d85dc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1024&h=1280&q=80" style="width:221px;height: 141px;" alt=""></div>
                <div class="col-span-2">
                    <div class="flex flex-row" style="position: relative;left: -86px;top: 21px;">
                        <img id="image" src="{{asset('icon/hotel-solid.svg')}}" style="weight:12px;height:12px" class="mr-1" alt="logo 3"/>
                        <label for="image" class="mr-1 text-5 text-black-100 bold">{{$detail->product->product_name}}, {{$detail->alamat}}</label>
                    </div>
                </div>
                <div class="col-span-2">
                  <div class="flex flex-row" style="position: relative;left: -88px;">
                    @if($detail->product->type=='hotel')
                    <div>
                        <img id="image" src="{{asset('icon/calendar.png')}}" class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3">
                        <label for="image" style="position: relative;top: -22px;left: 26px;">
                            <p class="font-bold text-3">{{$detail->checkin}}</p>
                            <p class="text-gray-300">Check-in</p>
                        </label>
                    </div>
                    <div style="position: relative;left: 71px;">
                        <label for="">
                            <p class="font-bold">{{$detail->checkout}}</p>
                            <p class="text-gray-300">Check-in</p>
                        </label>
                    </div>
                  </div>
                  @endif
                </div>
                {{-- <div class="col-span-2">03</div> --}}
              </div>
              <div class="row">
                <hr class="my-4 mb-3 border-1 border-black">
                <div class="flex flex-row">
                    <div class="flex flex-row">
                        <img id="image" src="{{asset('icon/maps-point-grey.png')}}" class="w-[15px] h-[20px] mt-[0.2rem]" alt="polygon 3" title="Kamtuu" with="15" height="15"> <label for="image" class="mx-3 text-xs font-light text-gray-300">{{$detail->alamat}}</label>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="flex flex-row">
                        <img id="image" src="{{asset('icon/telephone-black.svg')}}" class="w-[15px] h-[20px] mt-[0.2rem]" alt="polygon 3" with="15" height="15"> <label for="image" class="mx-3 text-xs font-light text-gray-300">{{($detail->no_telp)}}</label>
                    </div>
                </div>
                <hr class="my-4 mb-3 border-1 border-black">
              </div>
              <div class="row">
                <div class="bg-gray-400" style="height:25px;padding-top: 4px;padding-bottom: 27px;">
                    &nbsp;&nbsp;&nbsp;<span class="text-dark font-weight-bold text-3 mr-3">Kamar 1:&nbsp;</span><span class="text-3">No. Konfirmasi</span><span class="text-dark font-weight-bold text-3">: {{$booking->booking_code}}</span>
                </div>
              </div>
              <div class="row">
                <div class="flex flex-col font-bold">
                    <span class="ml-3 text-3 mb-3">{{$kamar->jenis_kamar}}</span>
                    <div class="flex flex-row">
                        <img src="{{asset('icon/user.png')}}" alt=""  class="w-[25px] h-[25px]" pwith="15" height="15"><span class="text-3 ml-1">Direservasi untuk:&nbsp;</span><span class="text-3 uppercase">chandrika eka kurniawan</span>
                    </div>
                </div>
                <hr class="text-light font-thin border-solid border-1">
              </div>
              <div class="row">
                <div class="ml-3 mt-2 mb-12">
                    <div class="text-gray-300 text-sm">Aturan Pembatalan/Perubahan:</div>
                    <p>
                      {{$data->coupon_list}}
                    </p>
                </div>
                <div class="ml-3 mt-2 mb-7">
                    @if($detail->product->type=='hotel')
                    <div class="text-gray-300 text-sm">Petunjuk Kedatangan Terlambatan:</div>
                    {{-- &nbsp;&nbsp;&nbsp;{!!$data->coupon_list!!} --}}
                    <p>
                      Jika Anda akan check-in terlambat, hubungi langsung properti ini untuk mengetahui kebijakan check-in terlambat mereka.
                    </p>
                    @endif
                </div>
                <div class="ml-3 mt-2">
                  @if($detail->product->type=='hotel')
                  <div class="text-gray-300 text-sm">Kebijakan Check-in:</div>
                  {{-- &nbsp;&nbsp;&nbsp;{!!$data->coupon_list!!} --}}
                  <p>
                    Waktu check-in mulai pukul 14.00
                    <br>
                    Usia minimal untuk check-in adalah: 17
                  </p>
                  @endif
                </div>
              </div>
              <div style="page-break-before:always;">
              <div class="row mt-20">
                    <div class="ml-3">
                        @if($detail->product->type=='hotel')        
                        <div class="text-gray-300 text-sm">Petunjuk Khusus Check-in:</div>
                        @else
                        <div class="text-gray-300 text-sm">Petunjuk Khusus</div>
                        @endif
                        <p>
                            {!!$data->notes!!}
                            {{-- Bellboy atau resepsionis akan menyambut tamu saat kedatangan --}}
                        </p>
                    </div>
              </div>
              {{-- <p style="page-break-after: always"> --}}
                  <div class="row mt-10">
                    <hr class="text-light font-thin border-black mb-3">
                    <div class="text-bold text-base">Harga</div>
                        <table style="width:75%;width:75%;position: relative;left: 11px;">
                            <tbody>
                                <tr>
                                    <td class="text-gray-300">Harga Final:</td>
                                    <td class="text-gray-300 text-right">{{$data->amount}}</td>
                                </tr>
                                <tr>
                                    <td class="text-gray-300">Pajak dan Biaya:</td>
                                    <td class="text-gray-300 text-right">{{$data->tax_amount}}</td>
                                </tr>
                                <tr>
                                    <td class="text-bold text-3">Total:</td>
                                    <td class="text-bold text-right text-3">{{$data->total_amount}}</td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    {{-- <td class="text-bold text-right text-2"></td> --}}
                                </tr>
                                <tr>
                                    <td class="text-gray-300">Bookin Fee</td>
                                    <td class="text-gray-300 text-right">{{$data->booking_fee}}</td>
                                </tr>
                            </tbody>
                        </table>
                    <hr class="text-light font-thin border-black mb-3">
                  </div>
             
             </div>
        </main>
    </div>
</body> 
</html>