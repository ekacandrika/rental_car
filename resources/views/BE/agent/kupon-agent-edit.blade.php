<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        #akun:checked + #akun {
            display: block;
        }

        #report:checked + #report {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">

{{-- Navbar --}}
<x-be.agent.navbar-agent></x-be.agent.navbar-agent>

{{-- Sidebar --}}
<x-be.agent.sidebar-agent></x-be.agent.sidebar-agent>

{{--Body--}}
<div class="col-start-3 col-end-11 z-0">
    <div class="p-5 pr-10" style="padding-left: 315px">
        <div class="bg-gray-200 rounded-md border-gray-400 p-4">

            <div class="flex">
                <p>My Order</p>
                <img id="image" src="{{ asset('storage/icons/chevron-right-arrow.svg') }}"
                     class="w-[15px] h-[20px] mt-[0.2rem] mx-3" alt="polygon 3" title="Kamtuu">
                <p class="text-blue-600">No Booking: 123BD8095</p>
            </div>

            <div class="flex my-3">
                <button
                    class="rounded-lg px-8 py-2 text-base bg-yellow-500 text-yellow-100 hover:bg-yellow-600 duration-300 w-[9rem] mx-2">
                    Tampilkan
                </button>
                <a href="{{route("coupon.download",$coupon->id)}}" target="_blank"
                    class="rounded-lg px-8 py-2 text-base bg-yellow-500 text-yellow-100 hover:bg-yellow-600 duration-300 w-[7rem] mx-2">
                    Export
                </a>
                <a href="{{route("coupon.print",$coupon->id)}}" target="_blank"
                    class="rounded-lg px-8 py-2 text-base bg-yellow-500 text-yellow-100 hover:bg-yellow-600 duration-300 w-[7rem] mx-2">
                    Cetak
                </a>
                <button
                    class="rounded-lg px-8 py-2 text-base bg-yellow-500 text-yellow-100 hover:bg-yellow-600 duration-300 w-[7rem] mx-2">
                    Email
                </button>
            </div>

            {{--            wysiwig--}}
            {{-- @if ($method=="edit") --}}
                <form action="{{route('coupon.update', isset($coupon->id) ? $coupon->id:null)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
            {{-- @else
                
                <form action="{{route('coupon.store')}}" method="post" enctype="multipart/form-data">
                @csrf
            @endif --}}
                
                <input type="hidden" name="method" value="{{$method}}">
                <div class="max-w-6xl my-5">
                    <p class="font-bold text-xl">Header</p>
                    <div class="bg-white">
                        {{-- <div id="summernoteHeader"></div> --}}
                        <textarea name="coupon_header" id="summernoteHeader" cols="30" rows="10">{!! old('coupon_header',$coupon->coupon_header ?? null) !!}</textarea>
                    </div>
                </div>
                @if ($errors->has('coupon_header'))
                <span class="text-danger">{{ $errors->first('coupon_header') }}</span>
                @endif
    
                <div class="max-w-6xl my-5">
                    <p class="font-bold text-xl">Pesanan</p>
                    <div class="bg-white">
                        {{-- <div id="summernotePesanan"></div> --}}
                        <textarea name="coupon_message" id="summernotePesanan" cols="30" rows="10">{!! old('coupon_message',$coupon->coupon_message ?? null) !!}</textarea>
                    </div>
                </div>
                @if ($errors->has('coupon_message'))
                <span class="text-danger">{{ $errors->first('coupon_message') }}</span>
                @endif
                <div class="max-w-6xl my-5">
                    <p class="font-bold text-xl">Nama Listing</p>
                    <div class="bg-white">
                        {{-- <div id="summernoteNamaListing"></div> --}}
                        <textarea name="coupon_list" id="summernoteNamaListing" cols="30" rows="10">{!! old('coupon_list',$coupon->coupon_list ?? null) !!}</textarea>
                    </div>
                </div>
                @if ($errors->has('coupon_list'))
                <span class="text-danger">{{ $errors->first('coupon_list') }}</span>
                @endif
                {{--            detail kupon--}}
                <div class="grid grid-cols-2">
                    <div class="grid grid-rows-3 grid-flow-col gap-4 border-gray-600 bg-white">
                        <div class="row-span-3">
                            <img class="w-full h-full rounded-md border border-gray-300"
                                 src="https://images.unsplash.com/photo-1611043714658-af3e56bc5299?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3540&q=80"
                                 alt=""></div>
                        <div class="col-span-2">
                            <div class="flex">
                                <img id="image" src="{{ asset('storage/icons/icon-maps.svg') }}"
                                     class="w-[15px] h-[20px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                <label for="image" class="mx-3 text-base font-semibold">Tur Pantai</label>
                            </div>
                        </div>
                        <div class="row-span-2">
                            <div class="flex"><img id="image"
                                                   src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                                   class="w-[15px] h-[20px] mt-[0.2rem] mr-1"
                                                   alt="polygon 3" title="Kamtuu">
                                <label for="image" class="mx-3 text-base font-semibold">9 September
                                    2022</label>
                            </div>
                            <p>Start</p>
                        </div>
                        <div class="row-span-2">
                            <div class="flex">
                                <label for="image" class="mx-3 text-base font-semibold">10 September
                                    2022</label>
                            </div>
                            <p>End</p>
                        </div>
                    </div>
                </div>
    
                <div class="grid grid-cols-2">
                    <div class="bg-white">
                        <hr class="my-8 h-px bg-gray-200 border-0 dark:bg-gray-700">
                        <div class="col-span-3">
                            <div class="grid grid-cols-2 pl-2">
                                <div>
                                    <div class="flex"><img id="image"
                                                           src="{{ asset('storage/icons/point-maps-black.svg') }}"
                                                           class="w-[15px] h-[20px] mt-[0.2rem] mr-1"
                                                           alt="polygon 3" title="Kamtuu">
                                        <label for="image" class="mx-3 text-base font-semibold">Jl. Turi Raya, Condongcatur,
                                            Kec. Depok, Kabupaten Sleman,
                                            Daerah Istimewa Yogyakarta 55281</label>
                                    </div>
                                </div>
                                <div>
                                    <div class="flex"><img id="image"
                                                           src="{{ asset('storage/icons/telephone-black.svg') }}"
                                                           class="w-[15px] h-[20px] mt-[0.2rem] mr-1"
                                                           alt="polygon 3" title="Kamtuu">
                                        <label for="image" class="mx-3 text-base font-semibold">+62858374824389</label>
                                    </div>
                                </div>
                                <div class="col-span-2">
                                    <hr class="my-8 h-px bg-gray-200 border-0 dark:bg-gray-700">
                                </div>
    
                                <div class="col-span-2">
                                    <div class="p-1 bg-blue-500 w-1/2 rounded-md text-white text-center">
                                        <p>Kamar 1 - No. Konfirmasi 129391820</p>
                                    </div>
                                </div>
    
                                <div class="col-span-2">
                                        <p>Double Deluxe</p>
                                        <div class="flex"><img id="image"
                                                           src="{{ asset('storage/icons/circle-user-solid.svg') }}"
                                                           class="w-[15px] h-[20px] mt-[0.2rem] mr-1"
                                                           alt="polygon 3" title="Kamtuu">
                                        <label for="image" class="mx-3 text-base font-semibold">Direservasi untuk : James Cameroon</label>
                                        </div>
                                        <p>1 Orang Dewasa</p>
                                </div>
    
                                <div class="col-span-2">
                                    <hr class="my-8 h-px bg-gray-200 border-0 dark:bg-gray-700">
                                </div>
    
                                <div class="col-span-2">
                                    <p class="font-bold text-xl">Catatan</p>
                                    <div class="bg-white p-2">
                                        {{-- <div id="summernoteCatatan"></div> --}}
                                        <textarea name="notes" id="summernoteCatatan" cols="30" rows="10">{!! old('coupon_message',$coupon->coupon_message ?? null) !!}</textarea>
                                    </div>
                                </div>
                                @if ($errors->has('notes'))
                                <span class="text-danger">{{ $errors->first('notes') }}</span>
                                @endif
                                </div>
                        </div>
                    </div>
                </div>
    
    {{--            harga--}}
                <div class="max-w-6xl bg-white my-5">
                    <div class="grid grid-cols-3">
                        <div class="col-span-2 p-5">
                            <p class="text-xl">Harga</p>
                        </div>
                        <div class="grid justify-items-end p-5">
                            <button id="btnHidden"
                                class="rounded-lg px-8 py-2 text-base bg-blue-500 text-blue-100 hover:bg-blue-600 duration-300 w-[9rem] mx-2">
                                Sembunyikan
                            </button>
                        </div>

                        <div class="p-5 col-span-3">
                            <p class="my-2">Harga Final</p>
                            <input class="rounded-md my-2 w-1/2" name="amount" type="text" value="{{old('amount',$coupon->amount ?? null)}}">
                            @if ($errors->has('amount'))
                            <span class="text-danger">{{ $errors->first('amount') }}</span>
                            @endif
                            <p class="my-2">Pajak dan Biaya</p>
                            <input class="rounded-md my-2 w-1/2" name="tax_amount" type="text" value="{{old('tax_amount',$coupon->tax_amount ?? null)}}">
                            @if ($errors->has('tax_amount'))
                            <span class="text-danger">{{ $errors->first('tax_amount') }}</span>
                            @endif
                            <p class="my-2">Total</p>
                            <input class="rounded-md my-2 w-1/2" name="total_amount" type="text" value="{{old('total_amount',$coupon->total_amount ?? null)}}">
                            @if ($errors->has('total_amount'))
                            <span class="text-danger">{{ $errors->first('total_amount') }}</span>
                            @endif
                        </div>
                        <div class="px-5">
                            <p class="my-2">Booking Fee</p>
                            <input class="rounded-md my-2 w-full" name="booking_fee" type="text" value="{{old('booking_fee',$coupon->booking_fee ?? null)}}">
                        </div>
                        @if ($errors->has('booking_fee'))
                        <span class="text-danger">{{ $errors->first('booking_fee') }}</span>
                        @endif
                        <div class="px-5">
                            <p class="my-2">Catatan Pembayaran</p>
                            <input class="rounded-md my-2 w-full" name="payment_notes" type="text" value="{{old('payment_notes',$coupon->payment_notes ?? null)}}">
                        </div>
                        @if ($errors->has('payment_notes'))
                        <span class="text-red-900">{{ $errors->first('payment_notes') }}</span>
                        @endif
                    </div>
                </div>
    
                <div class="grid justify-items-end">
                    <button type="submit"
                        class="rounded-lg px-8 py-2 text-base bg-blue-500 text-blue-100 hover:bg-blue-600 duration-300 w-[9rem] mx-2">
                        Simpan
                    </button>
                </div>
            
            </form>
        </div>
    </div>
</div>

<script>
    $('#summernoteHeader').summernote({
        placeholder: 'Tur',
        tabsize: 2,
        height: 120,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });

    $('#summernotePesanan').summernote({
        placeholder: 'Pesanan',
        tabsize: 2,
        height: 120,
        toolbar: [
            // ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            // ['para', ['ul', 'ol', 'paragraph']],
            // ['table', ['table']],
            // ['insert', ['link', 'picture', 'video']],
            // ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });

    $('#summernoteNamaListing').summernote({
        placeholder: 'Nama Listing',
        tabsize: 2,
        height: 120,
        toolbar: [
            // ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            // ['para', ['ul', 'ol', 'paragraph']],
            // ['table', ['table']],
            // ['insert', ['link', 'picture', 'video']],
            // ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });

    $('#summernoteCatatan').summernote({
        placeholder: 'Nama Listing',
        tabsize: 2,
        height: 120,
        toolbar: [
            // ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            // ['para', ['ul', 'ol', 'paragraph']],
            // ['table', ['table']],
            // ['insert', ['link', 'picture', 'video']],
            // ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });

    $("#btnHidden").click(function(e){
        e.preventDefault();

    })
</script>
</body>

</html>
