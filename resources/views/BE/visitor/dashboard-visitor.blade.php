<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <style>
        #akun:checked+#akun {
            display: block;
        }

        #report:checked+#report {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">

    {{-- Navbar --}}
    <x-be.visitor.navbar-visitor></x-be.visitor.navbar-visitor>

    {{-- Sidebar --}}
    <div class="grid grid-cols-10">
        <x-be.visitor.sidebar-visitor></x-be.visitor.sidebar-visitor>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pr-10">

                {{-- Card --}}
                {{-- <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Penjualan Bulan Ini</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">Rp 50,000,000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">20 Pax</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Penjualan Tahun Ini</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">Rp 150,000,000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">100 Pax</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded ml-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Penjualan Sejak Bergabung</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">Rp 750,000,000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">1200 Pax</p>
                            </div>
                        </div>
                    </div>
                </div> --}}

                <p class="text-2xl text-center font-semibold py-3 text-[#9E3D64]">Statistik Visitor</p>

                {{-- Grafik --}}
                <div class="grid grid-cols-2 text-center my-10">
                    {{-- Traveller --}}
                    <div class="text-center mr-2 p-3 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]"
                        x-data="traveller()">
                        <div class="flex justify-between">
                            <p class="mt-2 font-bold text-[#333333] text-base">Grafik Traveller</p>
                            <div class="flex justify-center">
                                <div class="mb-3 xl:w-28">
                                    <select
                                        class="form-select appearance-none block w-full pl-3 pr-7 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0  focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                        x-model="monthly">
                                        <option value="true" @click="chartTraveller()">Bulanan</option>
                                        <option value="false" @click="chartTraveller()">Tahunan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <canvas id="chartTraveller"></canvas>
                    </div>

                    {{-- Visitor --}}
                    <div class="text-center ml-2 p-3 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]" x-data="visitor()">
                        <div class="flex justify-between">
                            <p class="mt-2 font-bold text-[#333333] text-base">Grafik Visitor</p>
                            <div class="flex justify-center">
                                <div class="mb-3 xl:w-28">
                                    <select
                                        class="form-select appearance-none block w-full pl-3 pr-7 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0  focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                        x-model="monthly">
                                        <option value="true" @click="chartVisitor()">Bulanan</option>
                                        <option value="false" @click="chartVisitor()">Tahunan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <canvas id="chartVisitor"></canvas>
                    </div>
                </div>

                <div class="grid grid-cols-2 text-center my-10">
                    {{-- Seller --}}
                    <div class="text-center mr-2 p-3 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Seller</p>
                        {{-- <div class="flex justify-between">
                            <p class="mt-2 font-bold text-[#333333] text-base">Grafik Seller</p>
                            <div class="flex justify-center">
                                <div class="mb-3 xl:w-28">
                                    <select
                                        class="form-select appearance-none block w-full pl-3 pr-7 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0  focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                        x-model="monthly">
                                        <option value="true" @click="chartSeller()">Bulanan</option>
                                        <option value="false" @click="chartSeller()">Tahunan</option>
                                    </select>
                                </div>
                            </div>
                        </div> --}}
                        <canvas id="chartSeller"></canvas>
                    </div>

                    {{-- Agent --}}
                    <div class="text-center ml-2 p-3 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Agent</p>
                        {{-- <div class="flex justify-between">
                            <p class="mt-2 font-bold text-[#333333] text-base">Grafik Agent</p>
                            <div class="flex justify-center">
                                <div class="mb-3 xl:w-28">
                                    <select
                                        class="form-select appearance-none block w-full pl-3 pr-7 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0  focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                        x-model="monthly">
                                        <option value="true" @click="chartAgent()">Bulanan</option>
                                        <option value="false" @click="chartAgent()">Tahunan</option>
                                    </select>
                                </div>
                            </div>
                        </div> --}}
                        <canvas id="chartAgent"></canvas>
                    </div>
                </div>

                {{-- Products --}}
                <div class="grid grid-cols-2 text-center my-10">
                    <div class="h-[411px] px-3 pt-3 pb-10 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <div class="flex justify-center">
                            <p class="mt-2 font-bold text-[#333333] text-base">Grafik Products</p>
                        </div>
                        <canvas id="chartProduct"></canvas>
                    </div>
                </div>

                {{-- <canvas id="myChart"></canvas> --}}

                {{-- Card 2 --}}
                {{-- <div class="drop-shadow-xl py-5 my-5 rounded text-center">
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Pendapatan Bersih Bulan Ini</p>
                                <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">Rp 40,000,000</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Pendapatan Bersih Tahun Ini</p>
                                <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">Rp 100,000,000</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded ml-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Pendapatan Bersih Sejak Bergabung
                                </p>
                                <p class="mt-4 mb-4 font-bold text-[#FFFFFF] text-xl">Rp 500,000,000</p>
                            </div>
                        </div>
                    </div>
                </div> --}}

                {{-- Table --}}
                {{-- <div class="overflow-x-auto relative">
                    <table class="w-full text-sm text-left border-2 border-[#333333]">
                        <tbody>
                            <tr class="bg-white border-b-2 border-[#333333]">
                                <th scope="row"
                                    class="py-4 px-6 border-r-2 border-[#333333] font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                    Tagihan Belum dibayar (Rp)
                                </th>
                                <td class="py-4 px-6">
                                    Rp. 40,000,000
                                </td>
                            </tr>
                            <tr class="bg-white border-2 border-[#333333]">
                                <th scope="row"
                                    class="py-4 px-6 border-r-2 border-[#333333] font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                    Jumlah listing (Item)</th>
                                <td class="py-4 px-6">
                                    1236 Item
                                </td>
                            </tr>
                            <tr class="bg-white border-2 border-[#333333]">
                                <th scope="row"
                                    class="py-4 px-6 border-r-2 border-[#333333] font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                    Jumlah Listing Terjual (Item)
                                </th>
                                <td class="py-4 px-6">
                                    3465 Item
                                </td>
                            </tr>
                            <tr class="bg-white border-2 border-[#333333]">
                                <th scope="row"
                                    class="py-4 px-6 border-r-2 border-[#333333] font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                    Ranking Total Penghasilan (#)
                                </th>
                                <td class="py-4 px-6">
                                    #1
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="text-sm font-inter font-semibold mt-5">
                    <p>Anda membutuhkan Rp <span class="px-5 bg-[#FFFFFF]">450,000</span> untuk menjadi seller dengan
                        total
                        jumlah penjualan tertinggi!</p>
                    <p class="text-[#9E3D64] font-inter font-bold text-lg mt-2.5">Ayo Lebih Semangat!!!</p>
                </div> --}}
            </div>
        </div>
    </div>

    <script>
        let monthly = [
            { week: "Week 1", count: 1002 },
            { week: "Week 2", count: 1200 },
            { week: "Week 3", count: 957 },
            { week: "Week 4", count: 1225 },
        ]
        let annual = [
            { month: "January", count: 4382 },
            { month: "February", count: 4579 },
            { month: "March", count: 5231 },
            { month: "April", count: 3520 },
            { month: "May", count: 7321 },
            { month: "June", count: 7320 },
            { month: "July", count: 5279 },
            { month: "August", count: 6527 },
            { month: "September", count: 6297 },
            { month: "October", count: 4927 },
            { month: "November", count: 7329 },
            { month: "December", count: 9173 },
        ]

        const travellerID = document.getElementById('chartTraveller');
        let travellerInitChart = new Chart(travellerID, {
            type: 'line',
            data: {
                labels: monthly.map( x => x.week ),
                datasets: [{
                    label: 'Traveller',
                    data: monthly.map( x => x.count),
                    borderWidth: 1,
                    tension: 0.1
                }]
            },
            options: {
                scales: {
                y: {
                    beginAtZero: true
                }
                }
            }
        });

        function traveller(){
            return{
                monthly: "true",
                chartTraveller(){

                    if (this.monthly === "true") {
                        travellerInitChart.data.labels = monthly.map( x => x.week );
                        travellerInitChart.data.datasets[0].data = monthly.map( x => x.count );
                    } else {
                        travellerInitChart.data.labels = annual.map( x => x.month );
                        travellerInitChart.data.datasets[0].data = annual.map( x => x.count );
                    }
                    
                    console.log(travellerInitChart.data.datasets[0].data)
                    travellerInitChart.update()
                }
            }
        }

        const visitorID = document.getElementById('chartVisitor');
        let visitorInitChart = new Chart(visitorID, {
            type: 'line',
            data: {
                labels: monthly.map( x => x.week ),
                datasets: [{
                    label: 'Visitor',
                    data: monthly.map( x => x.count),
                    borderColor: '#FFB800',
                    borderWidth: 1,
                    tension: 0.1
                }]
            },
            options: {
                scales: {
                y: {
                    beginAtZero: true
                }
                }
            }
        });

        function visitor(){
            return{
                monthly: "true",
                chartVisitor(){

                    if (this.monthly === "true") {
                        visitorInitChart.data.labels = monthly.map( x => x.week );
                        visitorInitChart.data.datasets[0].data = monthly.map( x => x.count );
                    } else {
                        visitorInitChart.data.labels = annual.map( x => x.month );
                        visitorInitChart.data.datasets[0].data = annual.map( x => x.count );
                    }
                    
                    console.log(visitorInitChart.data.datasets[0].data)
                    visitorInitChart.update()
                }
            }
        }
    </script>
    <script>
        let product_seller = [
            { product: "Tur", count: 2915 },
            { product: "XStay", count: 1027 },
            { product: "Transfer", count: 1650 },
            { product: "Hotel", count: 5017 },
            { product: "Activity", count: 3924 },
            { product: "Rental", count: 957 },
        ]

        let agent_seller = [
            { product: "Tur", count: 1028 },
            { product: "XStay", count: 584 },
            { product: "Transfer", count: 740 },
            { product: "Hotel", count: 2048 },
            { product: "Activity", count: 1503 },
            { product: "Rental", count: 539 },
        ]

        const sellerID = document.getElementById('chartSeller');
        let sellerInitChart = new Chart(sellerID, {
            type: 'bar',
            data: {
                labels: product_seller.map( x => x.product ),
                datasets: [{
                    label: 'Seller',
                    data: product_seller.map( x => x.count),
                    backgroundColor: [
                        'rgba(255, 99, 132)',
                        'rgba(255, 159, 64)',
                        'rgba(255, 205, 86)',
                        'rgba(75, 192, 192)',
                        'rgba(54, 162, 235)',
                        'rgba(153, 102, 255)',
                    ],
                    borderColor: [
                        'rgb(255, 99, 132)',
                        'rgb(255, 159, 64)',
                        'rgb(255, 205, 86)',
                        'rgb(75, 192, 192)',
                        'rgb(54, 162, 235)',
                        'rgb(153, 102, 255)',
                    ],
                    borderWidth: 1,
                    tension: 0.1
                }]
            },
            options: {
                scales: {
                y: {
                    beginAtZero: true
                }
                }
            }
        });

        // function seller(){
        //     return{
        //         monthly: "true",
        //         chartSeller(){

        //             if (this.monthly === "true") {
        //                 sellerInitChart.data.labels = monthly_seller.map( x => x.week );
        //                 sellerInitChart.data.datasets[0].data = monthly_seller.map( x => x.count );
        //             } else {
        //                 sellerInitChart.data.labels = annual_seller.map( x => x.month );
        //                 sellerInitChart.data.datasets[0].data = annual_seller.map( x => x.count );
        //             }
                    
        //             console.log(sellerInitChart.data.datasets[0].data)
        //             sellerInitChart.update()
        //         }
        //     }
        // }

        const agentID = document.getElementById('chartAgent');
        let agentInitChart = new Chart(agentID, {
            type: 'bar',
            data: {
                labels: agent_seller.map( x => x.product ),
                datasets: [{
                    label: 'Agent',
                    data: agent_seller.map( x => x.count),
                    backgroundColor: [
                        'rgba(255, 99, 132)',
                        'rgba(255, 159, 64)',
                        'rgba(255, 205, 86)',
                        'rgba(75, 192, 192)',
                        'rgba(54, 162, 235)',
                        'rgba(153, 102, 255)',
                    ],
                    borderColor: [
                        'rgb(255, 99, 132)',
                        'rgb(255, 159, 64)',
                        'rgb(255, 205, 86)',
                        'rgb(75, 192, 192)',
                        'rgb(54, 162, 235)',
                        'rgb(153, 102, 255)',
                    ],
                    borderWidth: 1,
                    tension: 0.1
                }]
            },
            options: {
                scales: {
                y: {
                    beginAtZero: true
                }
                }
            }
        });

        // function agent(){
        //     return{
        //         monthly: "true",
        //         chartAgent(){

        //             if (this.monthly === "true") {
        //                 agentInitChart.data.labels = monthly_seller.map( x => x.week );
        //                 agentInitChart.data.datasets[0].data = monthly_seller.map( x => x.count );
        //             } else {
        //                 agentInitChart.data.labels = annual_seller.map( x => x.month );
        //                 agentInitChart.data.datasets[0].data = annual_seller.map( x => x.count );
        //             }
                    
        //             console.log(agentInitChart.data.datasets[0].data)
        //             agentInitChart.update()
        //         }
        //     }
        // }

    </script>
    <script>
        let products = [
            { product: "Tur", count: 1257 },
            { product: "XStay", count: 935 },
            { product: "Transfer", count: 1039 },
            { product: "Hotel", count: 2937 },
            { product: "Activity", count: 1723 },
            { product: "Rental", count: 957 },
        ]

        const productID = document.getElementById('chartProduct');
        let productInitChart = new Chart(productID, {
            type: 'bar',
            data: {
                labels: products.map( x => x.product ),
                datasets: [{
                    label: 'Products',
                    data: products.map( x => x.count),
                    backgroundColor: [
                        'rgba(255, 99, 132)',
                        'rgba(255, 159, 64)',
                        'rgba(255, 205, 86)',
                        'rgba(75, 192, 192)',
                        'rgba(54, 162, 235)',
                        'rgba(153, 102, 255)',
                    ],
                    borderColor: [
                        'rgb(255, 99, 132)',
                        'rgb(255, 159, 64)',
                        'rgb(255, 205, 86)',
                        'rgb(75, 192, 192)',
                        'rgb(54, 162, 235)',
                        'rgb(153, 102, 255)',
                    ],
                    borderWidth: 1,
                    tension: 0.1
                }]
            },
            options: {
                scales: {
                y: {
                    beginAtZero: true
                }
                },
                maintainAspectRatio: false
            }
        });

        // function agent(){
        //     return{
        //         monthly: "true",
        //         chartAgent(){

        //             if (this.monthly === "true") {
        //                 agentInitChart.data.labels = monthly_seller.map( x => x.week );
        //                 agentInitChart.data.datasets[0].data = monthly_seller.map( x => x.count );
        //             } else {
        //                 agentInitChart.data.labels = annual_seller.map( x => x.month );
        //                 agentInitChart.data.datasets[0].data = annual_seller.map( x => x.count );
        //             }
                    
        //             console.log(agentInitChart.data.datasets[0].data)
        //             agentInitChart.update()
        //         }
        //     }
        // }
    </script>
</body>

</html>