<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #tur:checked+#tur {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.seller.navbar-seller></x-be.seller.navbar-seller>

    <div class="grid grid-cols-10">
        <x-be.kelolatoko.sidebar-toko></x-be.kelolatoko.sidebar-toko>

        <div class="col-start-3 col-end-11 z-0">
            <div class="p-5 pl-10 pr-10">
                <span class="text-2xl font-bold font-inter text-[#333333]">My Listing</span>
                <div class="flex grid grid-cols-4 py-5">
                    <button type="button"
                        class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800 w-[150px]">Tambah
                        Listing</button>
                    <div class="flex items-center col-start-4 col-span-1">
                        <label for="simple-search" class="sr-only">Search</label>
                        <div class="relative w-full">
                            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400"
                                    fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                                        clip-rule="evenodd"></path>
                                </svg>
                            </div>
                            <input type="text" id="simple-search"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Search" required>
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-2">
                    <div class="grid justify-items-start">
                        <div class="inline-flex p-2 items-center">
                            <input class="checked:bg-kamtuu-second" type="checkbox" name="" id="">
                            <p class="px-2">Pilih Semua</p>
                            <div class="w-[100px] mx-5">
                                <select
                                    class="px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-[100px] rounded-md text-sm focus:ring-1">
                                    <option>Aktif</option>
                                    <option>Tidak Aktif</option>
                                </select>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid justify-end">
                        <div class="py-2">
                            <select
                                class="px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-[250px] rounded-md text-sm focus:ring-1">
                                <option>-- Kategori --</option>
                                <option>Tur</option>
                                <option>Transfer</option>
                                <option>Rental</option>
                                <option>Hotel</option>
                                <option>XStay</option>
                                <option>Activity</option>
                            </select>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bg-white my-5 rounded-md shadow-lg">
                    <div class="grid grid-cols-12">
                        {{-- Cols 1 --}}
                        <div class="grid place-content-center">
                            <input class="checked:bg-kamtuu-second" type="checkbox" name="" id="">
                        </div>
                        {{-- Cols 2 --}}
                        <div class="col-span-2 m-5">
                            <img src="https://via.placeholder.com/100x100"
                                class="w-[150px] h-[150px] inline-flex rounded-lg" alt="" title="">
                        </div>
                        {{-- Cols 3 --}}
                        <div class="m-5 col-start-4 col-span-5">
                            <p class="text-lg font-bold">Nama Kendaraan</p>
                            <div class="flex py-2 gap-1 lg:gap-2">
                                <div
                                    class="bg-[#9E3D64] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                    Transfer</div>
                                <div
                                    class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                    Sedan</div>
                            </div>
                            <div class="flex">
                                <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}"
                                    alt="rating" width="17px" height="17px">
                                <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                                <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}"
                                    alt="rating" width="17px" height="17px">
                                <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                            </div>
                            <div class="pt-8 flex gap-2 items-center">
                                <p class="text-sm text-[#FFB800] font-semibold">Sedang Dipesan</p>
                                <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4"
                                    src="{{ asset('storage/icons/circle-info-black.svg') }}" alt="rating" width="17px"
                                    height="17px">
                            </div>
                        </div>
                        {{-- Cols 4 --}}
                        <div class="m-5 col-span-4">
                            <p class="px-5">Aktif</p>
                            <div class="flex gap-2 pt-8 items-center">
                                <p class="font-semibold text-md">Ketersediaan</p>
                                <button type="submit"
                                    class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">Edit</button>
                                <button type="submit"
                                    class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">Hapus</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bg-white my-5 rounded-md shadow-lg">
                    <div class="grid grid-cols-12">
                        {{-- Cols 1 --}}
                        <div class="grid place-content-center">
                            <input class="checked:bg-kamtuu-second" type="checkbox" name="" id="">
                        </div>
                        {{-- Cols 2 --}}
                        <div class="col-span-2 m-5">
                            <img src="https://via.placeholder.com/100x100"
                                class="w-[150px] h-[150px] inline-flex rounded-lg" alt="" title="">
                        </div>
                        {{-- Cols 3 --}}
                        <div class="m-5 col-start-4 col-span-5">
                            <p class="text-lg font-bold">Nama Kendaraan</p>
                            <div class="flex py-2 gap-1 lg:gap-2">
                                <div
                                    class="bg-[#9E3D64] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                    Transfer</div>
                                <div
                                    class="bg-[#23AEC1] px-2 lg:px-4 py-px lg:py-1 text-white text-[8px] lg:text-xs rounded lg:rounded-full">
                                    Sedan</div>
                            </div>
                            <div class="flex">
                                <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/seats.svg') }}"
                                    alt="rating" width="17px" height="17px">
                                <p class="font-bold text-[8px] lg:text-sm mr-2">7 Seats</p>
                                <img class="mr-1 w-3 h-3 lg:w-4 lg:h-4" src="{{ asset('storage/icons/koper.svg') }}"
                                    alt="rating" width="17px" height="17px">
                                <p class="font-bold  text-[8px] lg:text-sm mr-2">4 Koper</p>
                            </div>
                            <div class="pt-8 flex gap-2 items-center">
                                <p class="text-sm text-[#51B449] font-semibold">Tersedia</p>
                            </div>
                        </div>
                        {{-- Cols 4 --}}
                        <div class="m-5 col-span-4">
                            <p class="px-5">Aktif</p>
                            <div class="flex gap-2 pt-8 items-center">
                                <p class="font-semibold text-md">Ketersediaan</p>
                                <button type="submit"
                                    class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#23AEC1] rounded lg:rounded-lg">Edit</button>
                                <button type="submit"
                                    class="items-center px-px py-1 lg:py-2 w-full lg:w-60 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">Hapus</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        function displayImage() {

            return {
                imageUrl: '',

                selectedFile(event) {
                this.fileToUrl(event, src => this.imageUrl = src)
                },

                fileToUrl(event, callback) {
                if (! event.target.files.length) return

                let file = event.target.files[0],
                    reader = new FileReader()

                reader.readAsDataURL(file)
                reader.onload = e => callback(e.target.result)
                },
            }
        }
    </script>
</body>

</html>