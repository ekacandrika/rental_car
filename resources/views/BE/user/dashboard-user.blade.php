<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>

    <style>
        @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

        /**
        * tailwind.config.js
        * module.exports = {
        *   variants: {
        *     extend: {
        *       backgroundColor: ['active'],
        *     }
        *   },
        * }
        */
        .active\:bg-gray-50:active {
            --tw-bg-opacity: 1;
            background-color: rgba(249, 250, 251, var(--tw-bg-opacity));
        }

        #akun:checked+#akun {
            display: block;
        }

        #inbox:checked+#inbox {
            display: block;
        }

        #transfer:checked+#transfer {
            display: block;
        }

        #hotel:checked+#hotel {
            display: block;
        }

        #rental:checked+#rental {
            display: block;
        }

        #activity:checked+#activity {
            display: block;
        }

        #xstay:checked+#xstay {
            display: block;
        }
    </style>
    <script>
        function app() {
            return {
                wysiwyg: null,
                init: function(el) {
                    // Get el
                    this.wysiwyg = el;
                    // Add CSS
                    this.wysiwyg.contentDocument.querySelector('head').innerHTML += `<style>
                    *, ::after, ::before {box-sizing: border-box;}
                    :root {tab-size: 4;}
                    html {line-height: 1.15;text-size-adjust: 100%;}
                    body {margin: 0px; padding: 1rem 0.5rem;}
                    body {font-family: system-ui, -apple-system, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";}
                    </style>`;
                    this.wysiwyg.contentDocument.body.innerHTML += `
                    `;
                    // Make editable
                    this.wysiwyg.contentDocument.designMode = "on";
                },
                format: function(cmd, param) {
                    this.wysiwyg.contentDocument.execCommand(cmd, !1, param || null)
                }
            }
        }
    </script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-[#F2F2F2] font-inter">
    {{-- Navbar --}}
    <x-be.user.navbar-user></x-be.user.navbar-user>

    {{-- Sidebar --}}
    <x-be.user.sidebar-user></x-be.user.sidebar-user>

    <div class="col-start-3 col-end-11 z-0">
        <div class="p-5 pl-10 pr-10">
            <span class="text-lg font-bold font-inter text-[#000000]">Profil</span>

            <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                <div class="grid grid-cols-2">
                    <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Isi Informasi Dasar</div>
                    <div class="text-sm font-bold font-inter p-5 text-[#333333] text-right">ID Listing: 1TO101082022
                    </div>
                </div>

                <form>
                    <div class="px-5 mb-6">
                        <label for="text"
                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Judul</label>
                        <input type="text" id="text"
                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Tur Sehat">
                    </div>

                    <fieldset class="px-5">
                        <label for="text"
                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Konfirmasi
                            Paket</label>
                        <div class="grid grid-cols-2 w-80">
                            <div class="flex items-center mb-4">
                                <input id="country-option-1" type="radio" name="instant" value="instant"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                    checked="">
                                <label for="country-option-1"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Instant
                                </label>
                            </div>
                            <div class="flex items-center mb-4">
                                <input id="country-option-2" type="radio" name="by seller" value="by seller"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                <label for="country-option-2"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Konfirmasi by Seller
                                </label>
                            </div>
                        </div>
                    </fieldset>

                    <div class="block px-5">
                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Tag
                            Lokasi</label>
                        <select
                            class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                            <option>D I Yogyakarta</option>
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                        </select>
                        <div
                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                        </div>

                        <select
                            class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                            <option>D I Yogyakarta</option>
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                        </select>
                        <div
                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                        </div>
                    </div>

                    <div class="grid grid-cols-2">
                        <div class="flex">
                            <div class="px-5 my-5">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Hari</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Hari">
                            </div>

                            <div class="pr-5 my-5">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Malam</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Malam">
                            </div>
                        </div>
                    </div>

                    <fieldset class="px-5">
                        <label for="text"
                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Tipe Tur</label>
                        <div class="grid grid-cols-2 w-80">
                            <div class="flex items-center mb-4">
                                <input id="country-option-1" type="radio" name="tp" value="Tur Private"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                    checked="">
                                <label for="country-option-1"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Tur Private
                                </label>
                            </div>
                            <div class="flex items-center mb-4">
                                <input id="country-option-2" type="radio" name="ot" value="Open Trip"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                <label for="country-option-2"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Open Trip
                                </label>
                            </div>
                        </div>
                    </fieldset>

                    <div class="block px-5">
                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Tag
                            Lokasi</label>
                        <select
                            class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                            <option>D I Yogyakarta</option>
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                        </select>
                        <div
                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                        </div>
                    </div>

                    <div class="grid grid-cols-2">
                        <div class="flex">
                            <div class="px-5 my-5">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Peserta
                                    Min</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="3">
                            </div>

                            <div class="pr-5 my-5">
                                <label for="text"
                                    class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Peserta
                                    Max</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="5">
                            </div>
                        </div>
                    </div>

                    <fieldset class="px-5">
                        <label for="text"
                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Makanan</label>
                        <div class="grid grid-cols-3 w-[600px]">
                            <div class="flex items-center mb-4 w-40">
                                <input id="country-option-1" type="radio" name="halal" value="Halal"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                    checked="">
                                <label for="country-option-1"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Halal
                                </label>
                            </div>
                            <div class="flex items-center mb-4 w-52">
                                <input id="country-option-2" type="radio" name="va"
                                    value="Vegetarian Available"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                <label for="country-option-2"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Vegetarian Available
                                </label>
                            </div>
                            <div class="flex items-center mb-4 w-52">
                                <input id="country-option-2" type="radio" name="hva"
                                    value="Halal & Vegetarian Available"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                <label for="country-option-2"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Halal & Vegetarian Available
                                </label>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="px-5">
                        <label for="text"
                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Bahasa</label>
                        <div class="grid grid-cols-3 w-[600px]">
                            <div class="flex items-center mb-4 w-40">
                                <input id="country-option-1" type="radio" name="id" value="Bahasa Indonesia"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                    checked="">
                                <label for="country-option-1"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Bahasa Indonesia
                                </label>
                            </div>
                            <div class="flex items-center mb-4 w-52">
                                <input id="country-option-2" type="radio" name="en" value="English English"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                <label for="country-option-2"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    English
                                </label>
                            </div>
                            <div class="flex items-center mb-4 w-52">
                                <input id="country-option-2" type="radio" name="iden"
                                    value="Indonesia & English"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                <label for="country-option-2"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Indonesia & English
                                </label>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="px-5">
                        <label for="text"
                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Tingkat
                            Petualangan</label>
                        <div class="grid grid-cols-3 w-[600px]">
                            <div class="flex items-center mb-4 w-40">
                                <input id="country-option-1" type="radio" name="ekstrim" value="Ekstrim"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                    checked="">
                                <label for="country-option-1"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Ekstrim
                                </label>
                            </div>
                            <div class="flex items-center mb-4 w-52">
                                <input id="country-option-2" type="radio" name="semi"
                                    value="Semi Eksrim English"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                <label for="country-option-2"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Semi Eksrim
                                </label>
                            </div>
                            <div class="flex items-center mb-4 w-52">
                                <input id="country-option-2" type="radio" name="tidak" value="Tidak Ekstrim"
                                    class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                <label for="country-option-2"
                                    class="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Tidak Ekstrim
                                </label>
                            </div>
                        </div>
                    </fieldset>

                    <div class="px-5 mb-5">
                        <label for="text"
                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Kategori Tur</label>
                        <input type="text" id="text"
                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Masukan kategori tur yang sesuai dengan paket">
                    </div>

                    <div class="px-5">
                        <label for="text"
                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Deskripsi</label>
                    </div>
                    <div class="min-w-screen min-h-fit items-center justify-center">
                        <div class="w-full max-w-6xl mx-auto rounded-xl px-5 text-black" x-data="app()"
                            x-init="init($refs.wysiwyg)">
                            <div class="border border-gray-200 overflow-hidden rounded-md">
                                <div class="w-full flex border-b border-gray-200 text-xl text-gray-600">
                                    <button
                                        class="outline-none focus:outline-none border-l border-r border-gray-200 w-10 h-10 hover:text-indigo-500 active:bg-gray-50"
                                        @click="format('insertUnorderedList')">
                                        <i class="mdi mdi-format-list-bulleted"></i>
                                    </button>
                                    <button
                                        class="outline-none focus:outline-none border-r border-gray-200 w-10 h-10 mr-1 hover:text-indigo-500 active:bg-gray-50"
                                        @click="format('insertOrderedList')">
                                        <i class="mdi mdi-format-list-numbered"></i>
                                    </button>
                                </div>
                                <div class="w-full">
                                    <iframe x-ref="wysiwyg" class="w-full h-24 overflow-y-auto"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="px-5 pt-5">
                        <label for="text"
                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Paket
                            Termasuk</label>
                    </div>
                    <x-be.com.wysiwyg></x-be.com.wysiwyg>

                    <div class="px-5 pt-5">
                        <label for="text"
                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Paket Tidak
                            Termasuk</label>
                    </div>
                    <x-be.com.wysiwyg></x-be.com.wysiwyg>

                    <div class="px-5 pt-5">
                        <label for="text"
                            class="block mb-2 text-sm font-bold text-[#333333] dark:text-gray-300">Catatan</label>
                    </div>
                    <x-be.com.wysiwyg></x-be.com.wysiwyg>

                    <x-be.com.two-button></x-be.com.two-button>

                </form>

            </div>
        </div>
    </div>
</body>

</html>
