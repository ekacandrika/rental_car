<style>
    .parent-tooltip-populer:hover .tooltip-populer{
        display:block;   
    }
</style>
@php
//$list_tag = [];
$thumbnail=null;
if(isset($act->productdetail->thumbnail)){
$thumbnail = $act->productdetail->thumbnail;
}elseif(isset(json_decode($act->productdetail->gallery)[0]->gallery)){
$thumbnail = json_decode($act->productdetail->gallery)[0]->gallery;
}else{
$thumbnail = 'https://via.placeholder.com/450x450';
}

// function getLocation($id){
// $province = App\Models\Province::where('id', $id)->first();
// $regency = App\Models\Regency::where('id', $id)->first();
// $district = App\Models\District::where('id', $id)->first();

// // Check if Location is Province
// if ($province) {
// return $province->name;
// }

// // Check if Location is Province
// if ($regency) {
// return $regency->name;
// }

// // Check if Location is Province
// if ($district) {
// return $district->name;
// }
// }

// $tag_1 = getLocation($act->productdetail->tag_location_1);
// $tag_2 = getLocation($act->productdetail->tag_location_2);
// $tag_3 = getLocation($act->productdetail->tag_location_3);
// $tag_4 = getLocation($act->productdetail->tag_location_4);

//$tag_1 = App\Models\Province::where('id', $act->productdetail->tag_location_1)->first() ??
//App\Models\Regency::where('id', $act->productdetail->tag_location_1)->first() ?? App\Models\District::where('id',
//$act->productdetail->tag_location_1)->first() ?? DB::table("pulau")->where('id',$act->productdetail->tag_location_1)->first();
//$tag_2 = App\Models\Province::where('id', $act->productdetail->tag_location_2)->first() ??
//App\Models\Regency::where('id', $act->productdetail->tag_location_2)->first() ?? App\Models\District::where('id',
//$act->productdetail->tag_location_2)->first();
//$tag_3 = App\Models\Province::where('id', $act->productdetail->tag_location_3)->first() ??
//App\Models\Regency::where('id', $act->productdetail->tag_location_3)->first() ?? App\Models\District::where('id',
//$act->productdetail->tag_location_3)->first();
//$tag_4 = App\Models\Province::where('id', $act->productdetail->tag_location_4)->first() ??
//App\Models\Regency::where('id', $act->productdetail->tag_location_4)->first() ?? App\Models\District::where('id',
//$act->productdetail->tag_location_4)->first() ?? App\Models\detailWisata::where('id','id', $act->productdetail->tag_location_4)->first();

// dd($tag_1, $tag_2, $tag_3, $tag_4);
// h-[201px] 411px

@endphp
<div class="mx-auto max-w-7xl">
    <div class="grid hidden grid-cols-4 justify-items-start md:block">
        @if ($type == 'activity' || $type == 'tur')
        <div class="w-full px-3 py-6 max-w-[265px] sm:w-[255px]">
            <div class="h-[553px] overflow-hidden bg-white border-2 border-solid rounded-lg shadrop-shadow-xl border-[#9E3D64]">
                <div class="h-56 p-4 bg-center bg-cover"
                    style="background-image: url({{ isset($act->productdetail) ? asset($act->productdetail->thumbnail) : (isset($act->productdetail->gallery) ? asset(json_decode($act->productdetail->gallery)[0]) : asset('https://via.placeholder.com/450x450'))}})">
                    <div class="flex justify-end">
                        <svg class="w-6 h-6 text-white hover:fill-red-500" id="{{$act->id}}"
                            onclick="submitLike2({{$act->id}})" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path
                                d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z">
                            </path>
                        </svg>
                    </div>
                </div>
                <div class="p-4 border-t-2 border-[#9E3D64] h-[61px]">
                    <a
                        href="{{ $type == 'activity' ? route('activity.show', $act->slug) : route('tur.show', $act->slug) }}">
                        @if($type == 'activity')
                        <p class="parent-tooltip-populer text-[18px] font-bold tracking-wide text-[#D50006] group uppercase truncate">{{
                            $act->product_name != null ? $act->product_name: '-' }}
                            <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">{{$act->product_name}}</span>
                        </p>
                        @elseif($type == 'tur')
                        <p class="parent-tooltip-populer text-[18px] font-bold tracking-wide text-[#D50006] group uppercase truncate">{{
                            $act->product_name != null ? $act->product_name: 'Paket Tur' }}
                            <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">{{$act->product_name}}</span>
                        </p>
                        @endif

                    </a>
                    {{-- 
                    <div class="inline-flex">
                        <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1"
                            alt="polygon 3" title="Kamtuu">
                        <p>4.5 (75 review)</p>
                    </div> 
                    --}}
                </div>
                <div class="p-4 text-gray-700 border-t border-gray-300 h-[157px]">
                    <div class="overflow-y-scroll h-[70px]">
                        <div class="items-center flex-1 py-2">
                            <p class="text-[14px] text-[#333333]">Lokasi:</p>
                            <div class="grid space-y-2">
                                @if (isset($act->productdetail->regency))
                                <x-produk.tag-location-card>@slot('lokasi') {{ isset($act->productdetail->regency->name) ?
                                    $act->productdetail->regency->name : '' }} @endslot
                                </x-produk.tag-location-card>
                                @endif
                                @if (isset($act->productdetail->tag_location_1))
                                @php
                                    // link ke pulau
                                    $pulau = DB::table("pulau")->where('id',$act->productdetail->tag_location_1)->first();
                                    
                                    $url ="#";
                                    $name =null;

                                    if($pulau){

                                        $name = $pulau->island_name ?? null;
                                        $id  = $pulau->id;
                                   
                                        $destPulau = App\Models\detailObjek::where('pulau_id',$id)->first();
                                    
                                        $url = route('pulauShow',$destPulau->slug);
                                    }
                                    
                                @endphp
                                @if($name)
                                <x-produk.tag-location-card url="{{$url}}">@slot('lokasi') {{ isset($tag_1) ?
                                    $name : '' }} @endslot
                                </x-produk.tag-location-card>
                                @endif
                                @endif
                                @if (isset($act->productdetail->tag_location_2))
                                @php
                                    $id  = $act->productdetail->tag_location_2;

                                    $provinsi = App\Models\Province::where('id',$id)->first();
                                    
                                    $url ="#";
                                    $name = null;

                                    if($provinsi){
                                        $destPulau = App\Models\detailObjek::where('provinsi_id',$id)->first();

                                        $name = $provinsi->name;

                                        if($destPulau){
                                            $url = route('pulauShow',$destPulau->slug);
                                        }
                                    }
                                    

                                @endphp
                                @if($name)
                                <x-produk.tag-location-card url="{{$url}}">@slot('lokasi') {{ isset($act->productdetail->tag_location_2) ?
                                    $name : '' }} @endslot
                                </x-produk.tag-location-card>
                                @endif
                                @endif
                                @if (isset($act->productdetail->tag_location_3))
                                @php
                                    $id  = $act->productdetail->tag_location_3;
                                    $url ="#";
                                    $name =null;

                                    $regency = App\Models\Regency::where('id',$id)->first();
                                    if($regency){
                                        $name = $regency->name;
                                        $destKabupaten = App\Models\detailObjek::where('kabupaten_id',$id)->first();
                                    }
                                    

                                @endphp
                                @if($name)
                                <x-produk.tag-location-card url="{{$url}}">@slot('lokasi') {{ isset($act->productdetail->tag_location_3) ?
                                    $name : '' }} @endslot
                                </x-produk.tag-location-card>
                                @endif
                                @endif
                                @if (isset($act->productdetail->tag_location_4))
                                @php
                                    $id  = $act->productdetail->tag_location_4;
                                    $name =null;

                                    $destWisata = App\Models\detailObjek::where('id',$id)->first();

                                    $url ="#";

                                    if($destWisata){
                                        $name =$destWisata->namaWisata;
                                        $url = route('wisataShow',$destWisata->slug);
                                    }

                                @endphp
                                @if($name)
                                <x-produk.tag-location-card url="{{$url}}">@slot('lokasi') {{ isset($act->productdetail->tag_location_4) ?
                                    $name : '' }} @endslot
                                </x-produk.tag-location-card>
                                @endif
                                @endif
                            </div>
                        </div>
                        <div class="items-center flex-1 py-2 mt-[-14px]">
                            @if ($act->tagged != null)
                            <p class="truncate text-[14px] text-[#333333]">Kategori:
                                <br>
                                
                            <div class="flex flex-wrap my-1">
                                {{-- @dump($act->tagged) --}}
                                @forelse ($act->tagged as $index=>$tag)
                                @if ($index == 4)
                                @break
                                @endif
                                <a class="my-1 mr-1" href="">
                                    <span class="bg-[#51B449] rounded-sm p-1 my-1 text-white">
                                        {{ $tag->tag_name }}
                                    </span>
                                </a>
                                {{-- <x-produk.tag-ringkasan-card>@slot('ringkasan') {{ isset($tag) ?
                                    $tag->tag_name : '' }}
                                    @endslot</x-produk.tag-ringkasan-card> --}}
                                @empty
    
                                @endforelse
                                {{-- <x-produk.tag-ringkasan-card>@slot('ringkasan') {!!
                                    isset($act->productdetail->deskripsi) ?
                                    $act->productdetail->deskripsi : '' !!}
                                    @endslot</x-produk.tag-ringkasan-card> --}}
                            </div>
    
                            </p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="px-4 pt-3 pb-4 bg-gray-100 border-t border-gray-300 mt-[-20px] h-[161px]">
                    <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
                    <div class="grid items-center justify-end grid-cols-2 pt-2">
                        @php
                        $harga = [];
                        $dewasa_terendah = 0;
                        if ($act->productdetail->tipe_tur == 'Open Trip') {
                        $harga = App\Models\Price::where('paket_id',
                        $act->productdetail->paket_id)->orderBy("dewasa_residen")->get();

                        if (!isset($harga[0])) {
                        $harga = App\Models\Price::where('id', $act->productdetail->harga_id)->get();
                        }
                        }
                        @endphp
                        <div>
                            @if ($act->productdetail->tipe_tur == 'Tur Private')
                            <p class="text-[16px] text-[#333333]">Mulai dari <br>
                                <span class="font-bold">IDR {{ isset($act->productdetail->harga->dewasa_residen) ?
                                    number_format($act->productdetail->harga->dewasa_residen) : '' }}</span>
                            </p>
                            @endif
                            @if ($act->productdetail->tipe_tur == 'Open Trip')
                            <p class="text-[16px] text-[#333333]">Mulai dari <br>
                                <span class="font-bold">IDR {{ number_format($harga[0]->dewasa_residen) }}</span>
                            </p>
                            @endif
                            @if ($type == 'activity')
                            <p class="text-[16px] text-[#333333]">IDR
                                {{ isset($act->productdetail->harga->dewasa_residen)
                                ? number_format($act->productdetail->harga->dewasa_residen)
                                : '-' }}
                            </p>
                            @endif
                            @php
                            $rating = App\Models\reviewPost::where('product_id', $act->id)->sum('star_rating');
                            $review_count = App\Models\reviewPost::where('product_id', $act->id)->get();
                            @endphp
                            <div class="inline-flex">
                                <img src="{{ asset('storage/icons/Star 2.png') }}"
                                    class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                <p class="-mr-2 w-[411px]"> {{ count($review_count) == 0 ? 0 : round($rating/count($review_count),2) }}
                                    &ThinSpace; {{ round(count($review_count),2) }} review</p>
                            </div>
                        </div>

                        <div class="grid justify-items-end">
                            @php
                                $info_toko = App\Models\Kelolatoko::where('id_seller',$act->user->id)->first();
                            @endphp
                            @if (isset($info_toko->id_seller))
                            <a href="{{route('toko.show',$info_toko->id_seller)}}">
                            @else    
                            <a href="#">
                            @endif
                                <div class="grid w-12 h-12 mr-3 bg-center bg-cover rounded-full"
                                    style="background-image: url({{ isset($info_toko->logo_toko) ? $info_toko->logo_toko : 'https://via.placeholder.com/50x50'}})">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if ($type == 'hotel' || $type == 'xstay')
        <div class="w-full px-3 py-6 max-w-[265px] sm:w-[255px]">

            <div class="h-[553px] overflow-hidden bg-white border-2 border-solid rounded-lg shadrop-shadow-xl border-[#9E3D64]">
                <div class="h-56 p-4 bg-center bg-cover"
                    style="background-image: url({{ isset($act->productdetail->foto_maps_2) ? $act->productdetail->foto_maps_2 : 'https://via.placeholder.com/450x450'}})">
                    <div class="flex justify-end">
                        <svg class="w-6 h-6 text-white hover:fill-red-500" id="{{$act->id}}"
                            onclick="submitLike2({{$act->id}})" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path
                                d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z">
                            </path>
                        </svg>
                    </div>
                    {{-- <div class="grid mt-36 place-content-end">
                        <span class="bg-[#D50006] py-[2px] px-[5px] rounded-sm uppercase text-white font-bold ">16%
                            off</span>
                    </div> --}}
                </div>
                <div class="p-4 border-t-2 border-[#9E3D64]">
                    <a
                        href="{{ $type == 'hotel' ? route('hotel.show', $act->slug) : route('xstay.show', $act->slug) }}">
                        <p class="text-[18px] parent-tooltip-populer group font-bold tracking-wide text-[#D50006] uppercase truncate">{{
                            $act->product_name }}
                            <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">{{$act->product_name}}</span>
                        </p>
                    </a>
                    {{-- <div class="inline-flex">
                        <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1"
                            alt="polygon 3" title="Kamtuu">
                        <p>4.5 (75 review)</p>
                    </div> --}}
                </div>
                <div class="p-4 text-gray-700 border-t border-gray-300 h-[201px]">
                    <div class="items-center flex-1 py-2">
                        <div class="overflow-y-scroll h-[47px]">
                            {{-- @if (isset($act->productdetail->regency))
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($act->productdetail->regency->name) ?
                                $act->productdetail->regency->name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif --}}
                            <div class="grid space-y-2">
                                @if (isset($act->productdetail->regency))
                                @php
                                    $objekLokasi = App\Models\detailObjek::where('kabupaten_id',$act->productdetail->regency->id)->first();
                                    $url = '#';
                                    if($objekLokasi){
                                        $url = route('kabupatenShow',$objekLokasi->slug);
                                    }
                                    // dd($objekLokasi);
                                @endphp
                                <x-produk.tag-location-card url="{{$url}}">@slot('lokasi') {{ isset($act->productdetail->regency->name) ?
                                    $act->productdetail->regency->name : '' }} @endslot
                                </x-produk.tag-location-card>
                                @endif
                                @if (isset($act->productdetail->tag_location_1))
                                @php
                                    //get link pulau
                                    $id = $act->productdetail->tag_location_1;
                                    
                                    $url = "#";
                                    $name = null;

                                    $pulau = DB::table("pulau")->where("id",$id)->first();

                                    if($pulau){
                                        $name = $pulau->island_name;
                                        $destPulau = App\Models\detailObjek::where('pulau_id',$id)->first();

                                        $url = route('pulauShow',$destPulau->slug);
                                    }

                                @endphp
                                @if($name)
                                <x-produk.tag-location-card url="{{$url}}">@slot('lokasi') {{ isset($act->productdetail->tag_location_1) ?
                                    $name : '' }} @endslot
                                </x-produk.tag-location-card>
                                @endif
                                @endif
                                @if (isset($act->productdetail->tag_location_2))
                                @php
                                    $id = $act->productdetail->tag_location_2;
                                    
                                    $url = "#";
                                    $name = null;

                                    $province = App\Models\Province::where('id',$id)->first();

                                    if($province){
                                        $name = $province->name;
                                        $destPulau = App\Models\detailObjek::where('provinsi_id',$id)->first();

                                        $url = route('provinsiShow',$destPulau->slug);
                                    }
                                @endphp
                                @if($name)
                                <x-produk.tag-location-card url="{{$url}}">@slot('lokasi') {{ isset($act->productdetail->tag_location_2) ?
                                    $name : '' }} @endslot
                                </x-produk.tag-location-card>
                                @endif
                                @endif
                                @if (isset($act->productdetail->tag_location_3))
                                    @php
                                        $id = $act->productdetail->tag_location_3;
                                        
                                        $url = "#";
                                        $name = null;

                                        $regency = App\Models\Regency::where('id',$id)->first();

                                        if($regency){
                                            $name = $regency->name;
                                            $destPulau = App\Models\detailObjek::where('kabupaten_id',$id)->first();

                                            $url = route('kabupatenShow',$destPulau->slug);
                                        }
                                    @endphp
                                    @if($name)
                                    <x-produk.tag-location-card url="{{$url}}">@slot('lokasi') {{ isset($act->productdetail->tag_location_3) ?
                                        $name : '' }} @endslot
                                    </x-produk.tag-location-card>
                                    @endif
                                @endif
                                @if (isset($act->productdetail->tag_location_4))
                                    @php
                                        $id = $act->productdetail->tag_location_4;
                                        
                                        $url = "#";
                                        $name = null;

                                        $wisata = App\Models\detailWisata::where('id',$id)->first();

                                        if($regency){
                                            $name = $wisata->namaWisata;
                                            $url = route('wisataShow',$wisata->slug);
                                        }
                                    @endphp
                                    @if($name)
                                    <x-produk.tag-location-card url="{{$url}}">@slot('lokasi') {{ isset($act->productdetail->tag_location_4) ?
                                        $name : '' }} @endslot
                                    </x-produk.tag-location-card>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="items-center flex-1 py-2">
                        {{-- <p class="truncate text-[14px] text-[#333333]">Ringkasan:
                            <br>
                            <x-produk.tag-ringkasan-card>@slot('ringkasan') {!! $act->productdetail->deskripsi !!}
                                @endslot</x-produk.tag-ringkasan-card>
                        </p> --}}
                    </div>

                </div>
                <div class="px-4 pt-3 pb-4 bg-gray-100 border-t border-gray-300 mt-[-87px] h-[191px]">
                    <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
                    <div class="grid items-center justify-end grid-cols-2 pt-2">
                        <div>
                            <p class="text-[16px] text-[#333333]">Mulai dari <br>
                                <span class="font-bold">IDR {{
                                    number_format($act->productdetail->masterkamar->harga_kamar) }}</span>
                            </p>
                            @php
                            $rating = App\Models\reviewPost::where('product_id', $act->id)->sum('star_rating');
                            $review_count = App\Models\reviewPost::where('product_id', $act->id)->get();
                            @endphp
                            <div class="inline-flex">
                                <img src="{{ asset('storage/icons/Star 2.png') }}"
                                    class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                <p class="-mr-2 w-[411px]"> {{ count($review_count) == 0 ? 0 : round($rating/count($review_count),2) }}
                                    &ThinSpace; {{ round(count($review_count),2) }} review</p>
                            </div>
                        </div>

                        <div class="grid justify-items-end">
                            @php
                                $info_toko = App\Models\Kelolatoko::where('id_seller',$act->user->id)->first();
                            @endphp
                            @if (isset($info_toko->id_seller))
                            <a href={{route('toko.show',$info_toko->id_seller)}}>
                            @else
                            <a href="#">
                            @endif
                                <div class="grid w-12 h-12 mr-3 bg-center bg-cover rounded-full"
                                    style="background-image: url({{ isset($info_toko->logo_toko) ? $info_toko->logo_toko : 'https://via.placeholder.com/50x50'}})">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if ($type == 'rental' || $type == 'transfer')
        <div class="w-full px-3 py-6 max-w-[265px] sm:w-[255px]">

            <div class="h-[553px] overflow-hidden bg-white border-2 border-solid rounded-lg shadrop-shadow-xl border-[#9E3D64]">

                {{-- isset($act->productdetail->thumbnail) ? asset($act->productdetail->thumbnail) :
                (isset($act->productdetail->gallery) ? asset(json_decode($act->productdetail->gallery)[0]->gallery) :
                asset('https://via.placeholder.com/450x450')) --}}
                <div class="h-56 p-4 bg-center bg-cover" style="background-image: url({{asset($thumbnail)}})">
                    <div class="flex justify-end">
                        <svg class="w-6 h-6 text-white hover:fill-red-500" id="{{$act->id}}"
                            onclick="submitLike2({{$act->id}})" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path
                                d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z">
                            </path>
                        </svg>
                    </div>
                    {{-- <div class="grid mt-36 place-content-end">
                        <span class="bg-[#D50006] py-[2px] px-[5px] rounded-sm uppercase text-white font-bold ">16%
                            off</span>
                    </div> --}}
                </div>
                <div class="p-4 border-t-2 border-[#9E3D64] h-[61px]">
                    <a
                        href="{{ $type == 'rental' ? route('rental.show', $act->slug) : route('transfer.show', $act->slug) }}">
                        <p class="text-[18px] parent-tooltip-populer group font-bold tracking-wide text-[#D50006] uppercase truncate">{{
                            $act->product_name }}
                            <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">{{$act->product_name}}</span>
                        </p>
                    </a>
                    {{-- <div class="inline-flex">
                        <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1"
                            alt="polygon 3" title="Kamtuu">
                        <p>4.5 (75 review)</p>
                    </div> --}}
                </div>
                <div class="p-4 text-gray-700 border-t border-gray-300 h-[201px]">
                    <div class="items-center flex-1 py-2">
                        @if(isset($act->productdetail_tag_location_1) && isset($act->productdetail_tag_location_2) && isset($act->productdetail_tag_location_3) && isset($act->productdetail_tag_location_4))
                        <div class="overflow-y-scroll h-[32px]">
                        @else
                        <div>
                        @endif
                            {{-- @if (isset($act->productdetail->regency))
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($act->productdetail->regency->name) ?
                                $act->productdetail->regency->name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif --}}
                            <div class="grid space-y-2">
                                @if (isset($act->productdetail->regency))
                                @php
                                    $objDest = App\Models\detailObjek::where('kabupaten_id',$act->productdetail->regency_id)->first();
                                    $url = '#';

                                    if($objDest){
                                        $url = route('kabupatenShow',$objDest->slug);
                                    }

                                @endphp
                                <x-produk.tag-location-card url="{{$url}}">@slot('lokasi') {{ isset($act->productdetail->regency->name) ?
                                    $act->productdetail->regency->name : '' }} @endslot
                                </x-produk.tag-location-card>
                                @endif
                                @if (isset($act->productdetail->tag_location_1))
                                    @php
                                        //get link pulau
                                        $id = $act->productdetail->tag_location_1;
                                        
                                        $url = "#";
                                        $name = null;

                                        $pulau = DB::table("pulau")->where("id",$id)->first();

                                        if($pulau){
                                            $name = $pulau->island_name;
                                            $destPulau = App\Models\detailObjek::where('pulau_id',$id)->first();

                                            $url = route('pulauShow',$destPulau->slug);
                                        }

                                    @endphp
                                    @if($name)
                                    <x-produk.tag-location-card url="{{$url}}">@slot('lokasi') {{ isset($act->productdetail->tag_location_1) ?
                                        $name : '' }} @endslot
                                    </x-produk.tag-location-card>
                                    @endif
                                @endif
                                @if (isset($act->productdetail->tag_location_2))
                                    @php
                                        $id = $act->productdetail->tag_location_2;
                                        
                                        $url = "#";
                                        $name = null;

                                        $provinsi = App\Models\Province::where("id",$id)->first();

                                        if($provinsi){
                                            $name = $provinsi->name;
                                            $destProvinsi = App\Models\detailObjek::where('provinsi_id',$id)->first();

                                            $url = route('provinsiShow',$destProvinsi->slug);
                                        }

                                    @endphp
                                    @if($name)
                                    <x-produk.tag-location-card url="{{$url}}">@slot('lokasi') {{ isset($tag_2) ?
                                        $tag_2->name : '' }} @endslot
                                    </x-produk.tag-location-card>
                                    @endif
                                @endif
                                @if (isset($act->productdetail->tag_location_3))
                                    @php
                                        $id = $act->productdetail->tag_location_3;
                                        
                                        $url = "#";
                                        $name = null;

                                        $kabupaten = App\Models\Regency::where("id",$id)->first();

                                        if($kabupaten){
                                            $name = $kabupaten->name;
                                            $destkabupaten = App\Models\detailObjek::where('kabupaten_id',$id)->first();

                                            $url = route('kabupatenShow',$destkabupaten->slug);
                                        }
                                    @endphp
                                    @if($name)
                                    <x-produk.tag-location-card url="{{$url}}">@slot('lokasi') {{ isset($tag_3) ?
                                        $tag_3->name : '' }} @endslot
                                    </x-produk.tag-location-card>
                                    @endif
                                @endif
                                @if (isset($act->productdetail->tag_location_4))
                                    @php
                                        $id = $act->productdetail->tag_location_3;
                                        $wisata = App\Models\detailWisata::where('id',$id)->first();

                                        $url = '#';

                                        if($wisata){
                                            $url = route('wisataShow',$wisata->slug);
                                        }
                                    @endphp
                                    @if($name)
                                    <x-produk.tag-location-card url="{{$url}}">@slot('lokasi') {{ isset($tag_4) ?
                                        $tag_4->name : '' }} @endslot
                                    </x-produk.tag-location-card>
                                    @endif
                                @endif
                                @if ($act->productdetail->new_tag_location)
                                    @php
                                        $arr_tag_list = isset($act->productdetail->new_tag_location) ? json_decode($act->productdetail->new_tag_location,true) : null;
                                        $list_tag = array();
                                        $i=0;
                                        if($arr_tag_list){
                                            foreach($arr_tag_list as $key => $new_tag){
                                                //  dump($new_tag);
                                                $tag = App\Models\detailObjek::where('pulau_id',$new_tag)->first() ?? 
                                                    App\Models\detailObjek::where('provinsi_id',$id)->first() ??
                                                    App\Models\detailObjek::where('kabupaten_id',$id)->first() ??
                                                    App\Models\detailWisata::where('id',$id)->first();

                                                if($tag){
                                                        
                                                    if($tag->objek =='Pulau'){
                                                        $list_tag[$i]['url'] = route('pulauShow',$tag->slug);
                                                        $list_tag[$i]['name'] = $tag->nama_pulau;
                                                    }else if($tag->objek =='Provinsi'){
                                                        $list_tag[$i]['url'] = route('pulauShow',$tag->slug);
                                                        $list_tag[$i]['name'] = $tag->nama_pulau;
                                                    }else if($tag->objek =='Kabupaten' || $tag->objek=='Kota'){
                                                        $list_tag[$i]['url'] = route('kabupatenShow',$tag->slug);
                                                        $list_tag[$i]['name'] = $tag->nama_pulau;
                                                    }else{
                                                        $list_tag[$i]['url'] = route('kabupatenShow',$tag->slug);
                                                        $list_tag[$i]['name'] = $tag->namaWisata;
                                                    }

                                                    $i++;
                                                } 
                                            }
                                        }

                                    @endphp
                                    @if(count($list_tag) > 0)
                                        @foreach ($list_tag as $list)
                                            <x-produk.tag-location-card url="{{isset($list['url']) ? $list['url']:'#'}}">
                                            @slot('lokasi') 
                                                {{ isset($list['name']) ? $list['name'] : '' }} 
                                            @endslot
                                            </x-produk.tag-location-card>
                                        @endforeach
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="items-center flex-1 py-2">
                        {{-- <p class="truncate text-[14px] text-[#333333]">Ringkasan:
                            <br>
                            <x-produk.tag-ringkasan-card>@slot('ringkasan') {!! $act->productdetail->deskripsi !!}
                                @endslot</x-produk.tag-ringkasan-card>
                        </p> --}}
                    </div>

                </div>
                <div class="px-4 pt-3 pb-4 bg-gray-100 border-t border-gray-300 mt-[-63px]">
                    <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
                    <div class="grid items-center justify-end grid-cols-2 pt-2">
                        <div>
                            <p class="text-[16px] text-[#333333]">Mulai dari <br>
                                <span class="font-bold">IDR {{ number_format($act->productdetail->harga->dewasa_residen
                                    ??
                                    $act->productdetail->detailkendaraan->harga_akomodasi) }}</span>
                            </p>
                            @php
                            $rating = App\Models\reviewPost::where('product_id', $act->id)->sum('star_rating');
                            $review_count = App\Models\reviewPost::where('product_id', $act->id)->get();
                            @endphp
                            <div class="inline-flex">
                                <img src="{{ asset('storage/icons/Star 2.png') }}"
                                    class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                <p class="-mr-2 w-[411px]"> {{ count($review_count) == 0 ? 0 : round($rating/count($review_count),2) }}
                                    &ThinSpace; {{ round(count($review_count),2) }} review</p>
                            </div>
                        </div>

                        <div class="grid justify-items-end">
                            @php
                                $info_toko = App\Models\Kelolatoko::where('id_seller',$act->user->id)->first();
                            @endphp
                            @if (isset($info_toko->id_seller))
                            <a href={{route('toko.show',$info_toko->id_seller)}}>
                            @else    
                            <a href="#">
                            @endif
                                <div class="grid w-12 h-12 mr-3 bg-center bg-cover rounded-full"
                                    style="background-image: url({{ isset($info_toko->logo_toko) ? $info_toko->logo_toko : 'https://via.placeholder.com/50x50'}})">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>

    <div class="grid block grid-cols-1 justify-items-center md:hidden lg:hidden">
        @if ($type == 'activity' || $type == 'tur')
        <div class="w-full px-3 py-6 max-w-[287px] sm:w-full lg:w-full">
            <div class="h-[527px] overflow-hidden bg-white border-2 border-solid rounded-lg shadrop-shadow-xl border-[#9E3D64]">
                <div class="h-56 p-4 bg-center bg-cover"
                    style="background-image: url({{ isset($act->productdetail) ? asset($act->productdetail->thumbnail) : (isset($act->productdetail->gallery) ? asset(json_decode($act->productdetail->gallery)[0]) : asset('https://via.placeholder.com/450x450'))}})">
                    <div class="flex justify-end">
                        <svg class="w-6 h-6 text-white hover:fill-red-500" id="{{$act->id}}"
                            onclick="submitLike2({{$act->id}})" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path
                                d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z">
                            </path>
                        </svg>
                    </div>
                    {{-- <div class="grid mt-36 place-content-end">
                        <span class="bg-[#D50006] py-[2px] px-[5px] rounded-sm uppercase text-white font-bold ">16%
                            off</span>
                    </div> --}}
                </div>
                <div class="p-4 border-t-2 border-[#9E3D64]">
                    <a
                        href="{{ $type == 'activity' ? route('activity.show', $act->slug) : route('tur.show', $act->slug) }}">
                        <p class="parent-tooltip-populer text-[18px] group font-bold tracking-wide text-[#D50006] uppercase">{{ $act->product_name }}
                            <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">{{$act->product_name}}</span>
                        </p>
                    </a>
                    {{-- <div class="inline-flex">
                        <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1"
                            alt="polygon 3" title="Kamtuu">
                        <p>4.5 (75 review)</p>
                    </div> --}}
                </div>
                <div class="p-4 text-gray-700 border-t border-gray-300 h-[80px] overflow-y-scroll">
                    <div class="items-center flex-1 py-2">
                        <p class="text-[14px] text-[#333333]">Lokasi:</p>
                        <div class="grid space-y-2 h-[31px]">  
                            @if (isset($act->productdetail->regency))
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($act->productdetail->regency->name) ?
                                $act->productdetail->regency->name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif
                            @if (isset($act->productdetail_tag_location_1))
                            @php
                                $id = $act->productdetail->tag_location_1;
                                    
                                $url = "#";
                                $name = null;

                                $pulau = DB::table("pulau")->where("id",$id)->first();

                                if($pulau){
                                    $name = $pulau->island_name;
                                    $destPulau = App\Models\detailObjek::where('pulau_id',$id)->first();

                                    $url = route('pulauShow',$destPulau->slug);
                                }
                            @endphp
                            @if($name)
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($pulau) ?
                                $name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif
                            @endif
                            @if (isset($act->productdetail_tag_location_2))
                            @php
                                $id = $act->productdetail->tag_location_2;
                                    
                                $url = "#";
                                $name = null;

                                $provinsi = App\Models\Province::where("id",$id)->first();

                                if($provinsi){
                                    $name = $provinsi->name;
                                    $destprovinsi = App\Models\detailObjek::where('provinsi_id',$id)->first();

                                    $url = route('provinsiShow',$destprovinsi->slug);
                                }
                            @endphp
                            @if($name)
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($tag_2) ?
                                $tag_2->name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif
                            @endif
                            @if (isset($act->productdetail_tag_location_3))
                            @php
                                $id = $act->productdetail->tag_location_3;
                                    
                                $url = "#";
                                $name = null;

                                $kabupaten = App\Models\Regency::where("id",$id)->first();

                                if($kabupaten){
                                    $name = $kabupaten->name;
                                    $destkabupaten = App\Models\detailObjek::where('kabupaten_id',$id)->first();

                                    $url = route('kabupatenShow',$destkabupaten->slug);
                                }
                            @endphp
                            @if($name)
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($tag_3) ?
                                $tag_3->name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif
                            @endif
                            @if (isset($act->productdetail_tag_location_4))
                            @php
                                $id = $act->productdetail->tag_location_4;
                                    
                                $url = "#";
                                $name = null;

                                $wisata = App\Models\detailWisata::where("id",$id)->first();

                                if($wisata){
                                    $name = $wisata->namaWisata;
                                    $url = route('wisataShow',$wisata->slug);
                                }
                            @endphp
                            @if($name)
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($tag_4) ?
                                $tag_4->name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif
                            @endif
                            @if ($act->productdetail->new_tag_location)
                                    @php
                                        $arr_tag_list = isset($act->productdetail->new_tag_location) ? json_decode($act->productdetail->new_tag_location,true) : null;
                                        $list_tag = array();
                                        $i=0;
                                        if($arr_tag_list){
                                            foreach($arr_tag_list as $key => $new_tag){
                                                //  dump($new_tag);
                                                $tag = App\Models\detailObjek::where('pulau_id',$new_tag)->first() ?? 
                                                    App\Models\detailObjek::where('provinsi_id',$id)->first() ??
                                                    App\Models\detailObjek::where('kabupaten_id',$id)->first() ??
                                                    App\Models\detailWisata::where('id',$id)->first();

                                                if($tag){
                                                        
                                                    if($tag->objek =='Pulau'){
                                                        $list_tag[$i]['url'] = route('pulauShow',$tag->slug);
                                                        $list_tag[$i]['name'] = $tag->nama_pulau;
                                                    }else if($tag->objek =='Provinsi'){
                                                        $list_tag[$i]['url'] = route('pulauShow',$tag->slug);
                                                        $list_tag[$i]['name'] = $tag->nama_pulau;
                                                    }else if($tag->objek =='Kabupaten' || $tag->objek=='Kota'){
                                                        $list_tag[$i]['url'] = route('kabupatenShow',$tag->slug);
                                                        $list_tag[$i]['name'] = $tag->nama_pulau;
                                                    }else{
                                                        $list_tag[$i]['url'] = route('kabupatenShow',$tag->slug);
                                                        $list_tag[$i]['name'] = $tag->namaWisata;
                                                    }

                                                    $i++;
                                                } 
                                            }
                                        }

                                    @endphp
                                    @if(count($list_tag) > 0)
                                        @foreach ($list_tag as $list)
                                            <x-produk.tag-location-card url="{{isset($list['url']) ? $list['url']:'#'}}">
                                            @slot('lokasi') 
                                                {{ isset($list['name']) ? $list['name'] : '' }} 
                                            @endslot
                                            </x-produk.tag-location-card>
                                        @endforeach
                                    @endif
                                @endif
                        </div>
                    </div>
                    {{-- <div class="items-center flex-1 py-2">
                        <p class="truncate text-[14px] text-[#333333]">Ringkasan:
                            <br>
                            <x-produk.tag-ringkasan-card>@slot('ringkasan') {!! isset($act->productdetail->deskripsi) ?
                                $act->productdetail->deskripsi : '' !!}
                                @endslot</x-produk.tag-ringkasan-card>
                        </p>
                    </div> --}}
                    <div class="items-center flex-1 py-2 {{isset($act->tagged) ? 'mt-[80px]':null}}">
                        @if ($act->tagged != null)
                        <p class="truncate text-[14px] text-[#333333]">Kategori:
                            <br>
                            {{-- {{ dd($act) }} --}}

                        <div class="grid space-y-2">
                            @forelse ($act->tagged as $index=>$tag)
                            @if ($index == 1)
                            @break
                            @endif
                            <a class="truncate" href="">
                                <span class="bg-[#51B449] rounded-sm p-1 my-1 text-white">
                                    {{ $tag->tag_name }}
                                </span>
                            </a>
                            {{-- <x-produk.tag-ringkasan-card>@slot('ringkasan') {{ isset($tag) ?
                                $tag->tag_name : '' }}
                                @endslot</x-produk.tag-ringkasan-card> --}}
                            @empty

                            @endforelse
                            {{-- <x-produk.tag-ringkasan-card>@slot('ringkasan') {!!
                                isset($act->productdetail->deskripsi) ?
                                $act->productdetail->deskripsi : '' !!}
                                @endslot</x-produk.tag-ringkasan-card> --}}
                        </div>

                        </p>
                        @endif
                    </div>

                </div>
                <div class="px-4 pt-3 pb-4 bg-gray-100 border-t border-gray-300 mt-[6px] h-[152.2px]">
                    <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
                    <div class="grid items-center justify-end grid-cols-2 pt-2">
                        @php
                        $harga = [];
                        $dewasa_terendah = 0;
                        if ($act->productdetail->tipe_tur == 'Open Trip') {
                        $harga = App\Models\Price::where('paket_id',
                        $act->productdetail->paket_id)->orderBy("dewasa_residen")->get();

                        if (!isset($harga[0])) {
                        $harga = App\Models\Price::where('id', $act->productdetail->harga_id)->get();
                        }
                        }
                        @endphp
                        <div>
                            @if ($act->productdetail->tipe_tur == 'Tur Private')
                            <p class="text-[16px] text-[#333333]">Mulai dari <br>
                                <span class="font-bold">IDR {{ isset($act->productdetail->harga->dewasa_residen) ?
                                    number_format($act->productdetail->harga->dewasa_residen) : '' }}</span>
                            </p>
                            @endif
                            @if ($act->productdetail->tipe_tur == 'Open Trip')
                            <p class="text-[16px] text-[#333333]">Mulai dari <br>
                                <span class="font-bold">IDR {{ number_format($harga[0]->dewasa_residen) }}</span>
                            </p>
                            @endif
                            {{-- <p class="text-[16px] text-[#333333]">Mulai dari <br>
                                <span class="font-bold">IDR {{ isset($act->productdetail->harga->dewasa_residen) ?
                                    number_format($act->productdetail->harga->dewasa_residen) : '' }}</span>
                            </p> --}}
                            @php
                            $rating = App\Models\reviewPost::where('product_id', $act->id)->sum('star_rating');
                            $review_count = App\Models\reviewPost::where('product_id', $act->id)->get();
                            @endphp
                            <div class="inline-flex">
                                <img src="{{ asset('storage/icons/Star 2.png') }}"
                                    class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                <p class="-mr-2 w-[411px]"> {{ count($review_count) == 0 ? 0 : round($rating/count($review_count),2) }}
                                    &ThinSpace; {{ round(count($review_count),2) }} review</p>
                            </div>
                        </div>

                        <div class="grid justify-items-end">
                            @php
                                $info_toko = App\Models\Kelolatoko::where('id_seller',$act->user->id)->first();
                            @endphp
                            @if (isset($info_toko->id_seller))
                            <a href={{route('toko.show',$info_toko->id_seller)}}>
                            @else    
                            <a href="#">
                            @endif
                                <div class="grid w-12 h-12 mr-3 bg-center bg-cover rounded-full"
                                    style="background-image: url({{ isset($info_toko->logo_toko) ? $info_toko->logo_toko : 'https://via.placeholder.com/50x50'}})">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if ($type == 'hotel' || $type == 'xstay')
        <div class="w-full px-3 py-6 max-w-[265px] sm:w-full lg:w-full">

            <div class="h-[509px] overflow-hidden bg-white border-2 border-solid rounded-lg shadrop-shadow-xl border-[#9E3D64]">
                <div class="h-56 p-4 bg-center bg-cover"
                    style="background-image: url({{ isset($act->productdetail->foto_maps_2) ? $act->productdetail->foto_maps_2 : 'https://via.placeholder.com/450x450'}})">
                    <div class="flex justify-end">
                        <svg class="w-6 h-6 text-white hover:fill-red-500" id="{{$act->id}}"
                            onclick="submitLike2({{$act->id}})" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path
                                d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z">
                            </path>
                        </svg>
                    </div>
                    {{-- <div class="grid mt-36 place-content-end">
                        <span class="bg-[#D50006] py-[2px] px-[5px] rounded-sm uppercase text-white font-bold ">16%
                            off</span>
                    </div> --}}
                </div>
                <div class="p-4 border-t-2 border-[#9E3D64]">
                    <a
                        href="{{ $type == 'hotel' ? route('hotel.show', $act->slug) : route('xstay.show', $act->slug) }}">
                        <p class="parent-tooltip-populer text-[18px] group font-bold tracking-wide text-[#D50006] uppercase">{{ $act->product_name }}
                            <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">{{$act->product_name}}</span>

                        </p>
                    </a>
                    {{-- <div class="inline-flex">
                        <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1"
                            alt="polygon 3" title="Kamtuu">
                        <p>4.5 (75 review)</p>
                    </div> --}}
                </div>
                <div class="p-4 text-gray-700 border-t border-gray-300 h-[58px] overflow-y-scroll">
                    <div class="items-center flex-1 py-2">
                        {{-- @if (isset($act->productdetail->regency))
                        <x-produk.tag-location-card>@slot('lokasi') {{ isset($act->productdetail->regency->name) ?
                            $act->productdetail->regency->name : '' }} @endslot
                        </x-produk.tag-location-card>
                        @endif --}}
                        <div class="grid space-y-2">
                            @if (isset($act->productdetail->regency))
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($act->productdetail->regency->name) ?
                                $act->productdetail->regency->name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif
                            @if (isset($act->productdetail_tag_location_1))
                            @php
                                $id = $act->productdetail->tag_location_1;
                                    
                                $url = "#";
                                $name = null;

                                $pulau = DB::table("pulau")->where("id",$id)->first();

                                if($pulau){
                                    $name = $pulau->island_name;
                                    $destPulau = App\Models\detailObjek::where('pulau_id',$id)->first();

                                    $url = route('pulauShow',$destPulau->slug);
                                }
                            @endphp
                            @if($name)
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($pulau) ?
                                $name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif
                            @endif
                            @if (isset($act->productdetail_tag_location_2))
                            @php
                                $id = $act->productdetail->tag_location_2;
                                    
                                $url = "#";
                                $name = null;

                                $provinsi = App\Models\Province::where("id",$id)->first();

                                if($provinsi){
                                    $name = $provinsi->name;
                                    $destprovinsi = App\Models\detailObjek::where('provinsi_id',$id)->first();

                                    $url = route('provinsiShow',$destprovinsi->slug);
                                }
                            @endphp
                            @if($name)
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($tag_2) ?
                                $tag_2->name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif
                            @endif
                            @if (isset($act->productdetail_tag_location_3))
                            @php
                                $id = $act->productdetail->tag_location_3;
                                    
                                $url = "#";
                                $name = null;

                                $kabupaten = App\Models\Regency::where("id",$id)->first();

                                if($kabupaten){
                                    $name = $kabupaten->name;
                                    $destkabupaten = App\Models\detailObjek::where('kabupaten_id',$id)->first();

                                    $url = route('kabupatenShow',$destkabupaten->slug);
                                }
                            @endphp
                            @if($name)
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($tag_3) ?
                                $tag_3->name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif
                            @endif
                            @if (isset($act->productdetail_tag_location_4))
                            @php
                                $id = $act->productdetail->tag_location_4;
                                    
                                $url = "#";
                                $name = null;

                                $wisata = App\Models\detailWisata::where("id",$id)->first();

                                if($wisata){
                                    $name = $wisata->namaWisata;
                                    $url = route('wisataShow',$wisata->slug);
                                }
                            @endphp
                            @if($name)
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($tag_4) ?
                                $tag_4->name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif
                            @endif
                        </div>
                    </div>
                    <div class="items-center flex-1 py-2">
                        <p class="truncate text-[14px] text-[#333333]">Ringkasan:
                            <br>
                            <x-produk.tag-ringkasan-card>@slot('ringkasan') {!! $act->productdetail->deskripsi !!}
                                @endslot</x-produk.tag-ringkasan-card>
                        </p>
                    </div>

                </div>
                <div class="px-4 pt-3 pb-4 bg-gray-100 border-t border-gray-300 h-[201px]">
                    <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
                    <div class="grid items-center justify-end grid-cols-2 pt-2">
                        <div>
                            <p class="text-[16px] text-[#333333]">Mulai dari <br>
                                <span class="font-bold">IDR {{
                                    number_format($act->productdetail->masterkamar->harga_kamar) }}</span>
                            </p>
                            @php
                            $rating = App\Models\reviewPost::where('product_id', $act->id)->sum('star_rating');
                            $review_count = App\Models\reviewPost::where('product_id', $act->id)->get();
                            @endphp
                            <div class="inline-flex">
                                <img src="{{ asset('storage/icons/Star 2.png') }}"
                                    class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                <p class="-mr-2 w-[411px]"> {{ count($review_count) == 0 ? 0 : round($rating/count($review_count),2) }}
                                    &ThinSpace; {{ round(count($review_count),2) }} review</p>
                            </div>
                        </div>

                        <div class="grid justify-items-end">
                            @php
                                $info_toko = App\Models\Kelolatoko::where('id_seller',$act->user->id)->first();
                            @endphp
                            @if (isset($info_toko->id_seller))
                            <a href={{route('toko.show',$info_toko->id_seller)}}>
                            @else    
                            <a href="#">
                            @endif
                                <div class="grid w-12 h-12 mr-3 bg-center bg-cover rounded-full"
                                    style="background-image: url({{ isset($info_toko->logo_toko) ? $info_toko->logo_toko : 'https://via.placeholder.com/50x50'}})">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if ($type == 'rental' || $type == 'transfer')
        <div class="w-full px-3 py-6 max-w-[265px] sm:w-full lg:w-full">
            {{-- isset($act->productdetail->thumbnail) ? asset($act->productdetail->thumbnail) :
            (isset($act->productdetail->gallery) ? asset(json_decode($act->productdetail->gallery)[0]->gallery) :
            asset('https://via.placeholder.com/450x450')) --}}
            <div class="h-[509px] overflow-hidden bg-white border-2 border-solid rounded-lg shadrop-shadow-xl border-[#9E3D64]">
                <div class="h-56 p-4 bg-center bg-cover" style="background-image: url({{ asset($thumbnail) }})">
                    <div class="flex justify-end">
                        <svg class="w-6 h-6 text-white hover:fill-red-500" id="{{$act->id}}"
                            onclick="submitLike2({{$act->id}})" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path
                                d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z">
                            </path>
                        </svg>
                    </div>
                    {{-- <div class="grid mt-36 place-content-end">
                        <span class="bg-[#D50006] py-[2px] px-[5px] rounded-sm uppercase text-white font-bold ">16%
                            off</span>
                    </div> --}}
                </div>
                <div class="p-4 border-t-2 border-[#9E3D64]">
                    <a
                        href="{{ $type == 'rental' ? route('rental.show', $act->slug) : route('transfer.show', $act->slug) }}">
                        <p class="parent-tooltip-populer text-[18px] group font-bold tracking-wide text-[#D50006] uppercase">{{ $act->product_name }}
                            <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">{{$act->product_name}}</span>
                        </p>
                    </a>
                    {{-- <div class="inline-flex">
                        <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1"
                            alt="polygon 3" title="Kamtuu">
                        <p>4.5 (75 review)</p>
                    </div> --}}
                </div>
                <div class="p-4 text-gray-700 border-t border-gray-300 h-[67px] overflow-y-scroll">
                    <div class="items-center flex-1 py-2">
                        {{-- @if (isset($act->productdetail->regency))
                        <x-produk.tag-location-card>@slot('lokasi') {{ isset($act->productdetail->regency->name) ?
                            $act->productdetail->regency->name : '' }} @endslot
                        </x-produk.tag-location-card>
                        @endif --}}
                        <div class="grid space-y-2">
                            @if (isset($act->productdetail->regency))
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($act->productdetail->regency->name) ?
                                $act->productdetail->regency->name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif
                            @if (isset($tag_1))
                            @php
                                $name = $tag_1->name ?? $tag_1->island_name;
                            @endphp
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($tag_1) ?
                                $name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif
                            @if (isset($tag_2))
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($tag_2) ?
                                $tag_2->name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif
                            @if (isset($tag_3))
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($tag_3) ?
                                $tag_3->name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif
                            @if (isset($tag_4))
                            <x-produk.tag-location-card>@slot('lokasi') {{ isset($tag_4) ?
                                $tag_4->name : '' }} @endslot
                            </x-produk.tag-location-card>
                            @endif  
                        </div>
                    </div>
                    <div class="items-center flex-1 py-2">
                        <p class="truncate text-[14px] text-[#333333]">Ringkasan:
                            <br>
                            <x-produk.tag-ringkasan-card>@slot('ringkasan') {!! $act->productdetail->deskripsi !!}
                                @endslot</x-produk.tag-ringkasan-card>
                        </p>
                    </div>

                </div>
                <div class="px-4 pt-3 pb-4 bg-gray-100 border-t border-gray-300 h-[153.9px]">
                    <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
                    <div class="grid items-center justify-end grid-cols-2 pt-2">
                        <div>
                            <p class="text-[16px] text-[#333333]">Mulai dari <br>
                                <span class="font-bold">IDR {{ number_format($act->productdetail->harga->dewasa_residen
                                    ??
                                    $act->productdetail->detailkendaraan->harga_akomodasi) }}</span>
                            </p>
                            @php
                            $rating = App\Models\reviewPost::where('product_id', $act->id)->sum('star_rating');
                            $review_count = App\Models\reviewPost::where('product_id', $act->id)->get();
                            @endphp
                            <div class="inline-flex">
                                <img src="{{ asset('storage/icons/Star 2.png') }}"
                                    class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                <p class="-mr-2 w-[411px]"> {{ count($review_count) == 0 ? 0 : round($rating/count($review_count),2) }}
                                    &ThinSpace; {{ round(count($review_count),2) }} review</p>
                            </div>
                        </div>

                        <div class="grid justify-items-end">
                            @php
                                $info_toko = App\Models\Kelolatoko::where('id_seller',$act->user->id)->first();
                            @endphp
                            @if (isset($info_toko->id_seller))
                            <a href={{route('toko.show',$info_toko->id_seller)}}>
                            @else    
                            <a href="#">
                            @endif
                                <div class="grid w-12 h-12 mr-3 bg-center bg-cover rounded-full"
                                    style="background-image: url({{ isset($info_toko->logo_toko) ? $info_toko->logo_toko : 'https://via.placeholder.com/50x50'}})">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
<script>
    var tooltip = document.querySelectorAll('.tooltip-populer');
    document.addEventListener('mousemove',fn,false);

    function fn(e) {
        for (var i=tooltip.length; i--;) {
            tooltip[i].style.left = (12+e.pageX) + 'px';
            tooltip[i].style.top = (20+e.pageY) + 'px';
        }
    }
</script>