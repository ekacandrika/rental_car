<div class="max-w-sm mx-auto overflow-hidden bg-[#F2F2F2] rounded-lg shadow-lg">
    <div class="px-6 py-4 sm:flex sm:items-center">
        <img class="block h-16 mx-auto mb-4 rounded-lg sm:h-24 sm:mb-0 sm:mr-4 sm:ml-0" src="https://avatars2.githubusercontent.com/u/4323180?s=400&u=4962a4441fae9fba5f0f86456c6c506a21ffca4f&v=4" alt="">
      <div class="text-center sm:text-left sm:flex-grow">
        <div class="mb-4">
            <p class="text-xl leading-tight">Adam Wathan</p>
            <div class="inline-flex">
                <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                <p>4.5/5</p>
            </div>
            <p class="text-sm leading-tight text-grey-dark">Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem repellat suscipit quam omnis nisi facere, quaerat perferendis!</p>
        </div>
      </div>
    </div>
</div>
