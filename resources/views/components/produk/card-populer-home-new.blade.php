<div class="mx-auto max-w-7xl">
    <div class="grid hidden grid-cols-4 justify-items-start md:block">
        @if ($type == 'activity' || $type == 'tur')
            <div class="w-full px-3 py-6 max-w-[265px] sm:w-[255px]">
                <div class="h-[553px] overflow-hidden bg-white border-2 border-solid rounded-lg shadrop-shadow-xl border-[#9E3D64]">
                    <div class="h-56 p-4 bg-center bg-cover" style="background-image: url({{$act['thumbnail']}})">
                        <div class="flex justify-end">
                            <svg class="w-6 h-6 text-white hover:fill-red-500" id="{{$act['product']['id']}}"
                                onclick="submitLike2({{$act['product']['id']}})" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path
                                    d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z">
                                </path>
                            </svg>
                        </div>
                    </div>
                    <div class="p-4 border-t-2 border-[#9E3D64] h-[61px]">
                        <a
                            href=" {{ $type == 'activity' ? route('activity.show', $act['product']['slug']) : route('tur.show',$act['product']['slug']) }}">
                            @if($type == 'activity')
                            <p class="parent-tooltip-populer text-[18px] font-bold tracking-wide text-[#D50006] group uppercase truncate">
                                {{$act['product']['product_name'] != null ? $act['product']['product_name']: '-' }}
                                <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">
                                {{$act['product']['product_name']}}
                                </span>
                            </p>
                            @elseif($type == 'tur')
                            <p class="parent-tooltip-populer text-[18px] font-bold tracking-wide text-[#D50006] group uppercase truncate">
                                {{$act['product']['product_name'] != null ? $act['product']['product_name']: '-' }}
                                <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">
                                {{$act['product']['product_name']}}
                                </span>
                            </p>
                            @endif
                        </a>
                    </div>
                    <div class="p-4 text-gray-700 border-t border-gray-300 h-[157px]">
                        <div class="overflow-y-scroll h-[70px]">
                            <div class="items-center flex-1 py-2">
                                <p class="text-[14px] text-[#333333]">Lokasi:</p>
                                <div class="grid space-y-2">
                                @if(count($act['regency']) > 0)
                                    <x-produk.tag-location-card url="{{isset($act['regency'][0]['url']) ? $act['regency'][0]['url'] : ''}}">@slot('lokasi') {{isset($act['regency'][0]['name']) ? $act['regency'][0]['name'] : ''}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(json_decode($act['tag1'])!=null)
                                   @php
                                    $tag1 = json_decode($act['tag1']);
                                   @endphp
                                   <x-produk.tag-location-card url="{{$tag1[0]->url}}">@slot('lokasi') {{$tag1[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(json_decode($act['tag2'])!=null)
                                   @php
                                    $tag2 = json_decode($act['tag2']);
                                   @endphp
                                   <x-produk.tag-location-card url="{{$tag2[0]->url}}">@slot('lokasi') {{$tag2[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(count(json_decode($act['tag3'])) > 0)
                                   @php
                                    $tag3 = json_decode($act['tag3']);
                                   @endphp
                                   <x-produk.tag-location-card url="{{$tag3[0]->url}}">@slot('lokasi') {{$tag3[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(count(json_decode($act['tag4'])) > 0)
                                   @php
                                    $tag4 = json_decode($act['tag4']);
                                   @endphp
                                   <x-produk.tag-location-card url="{{$tag4[0]->url}}">@slot('lokasi') {{$tag4[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                </div>
                            </div>
                            <div class="items-center flex-1 py-2 mt-[-14px]">
                                @if ($act['tagged'] != null)
                                {{-- @dump($act['tagged']) --}}
                                <p class="truncate text-[14px] text-[#333333]">Kategori:
                                    <br>
                                    
                                    <div class="flex flex-wrap my-1">
                                        @forelse ($act['tagged'] as $index=>$tag)
                                        @if ($index == 4)
                                        @break
                                        @endif
                                        <a class="my-1 mr-1" href="">
                                            <span class="bg-[#51B449] rounded-sm p-1 my-1 text-white">
                                                {{ $tag['tag_name'] }}
                                            </span>
                                        </a>
                                        @endforeach
                                    </div>
                                </p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="px-4 pt-3 pb-4 bg-gray-100 border-t border-gray-300 mt-[-20px] h-[161px]">
                        <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
                        <div class="grid items-center justify-end grid-cols-2 pt-2">
                        <div>
                                <p class="text-[16px] text-[#333333]" style="width: 218px;">Mulai dari <br>
                                    <span class="font-bold">IDR {{$act['harga']}}</span>
                                </p>
                                <div class="inline-flex">
                                    <img src="{{ asset('storage/icons/Star 2.png') }}"
                                        class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                    <p class="-mr-2 w-[411px]">{{$act['rating']}}</p>
                                </div>
                            </div>
                            <div class="grid justify-items-end">
                                @if (isset($act['id_seller']))
                                <a href="{{route('toko.show',$act['id_seller'])}}">
                                @else<a href="#">
                                @endif
                                    <div class="grid w-12 h-12 mr-3 bg-center bg-cover border-gray-700 rounded-full" style="background-image: url({{$act['logo_toko']}})">
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if ($type == 'hotel' || $type == 'xstay')
            <div class="w-full px-3 py-6 max-w-[265px] sm:w-[255px]">
                <div class="h-[553px] overflow-hidden bg-white border-2 border-solid rounded-lg shadrop-shadow-xl border-[#9E3D64]">
                    <div class="h-56 p-4 bg-center bg-cover" style="background-image: url({{$act['thumbnail']}})">
                        <div class="flex justify-end">
                            <svg class="w-6 h-6 text-white hover:fill-red-500" id="{{$act['product']['id']}}"
                                onclick="submitLike2({{$act['product']['id']}})" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path
                                    d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z">
                                </path>
                            </svg>
                        </div>
                    </div>
                    <div class="p-4 border-t-2 border-[#9E3D64] h-[61px]">
                        <a
                            href=" {{ $type == 'hotel' ? route('hotel.show', $act['product']['slug']) : route('xstay.show',$act['product']['slug']) }}">
                            <p class="parent-tooltip-populer text-[18px] font-bold tracking-wide text-[#D50006] group uppercase truncate">
                                {{$act['product']['product_name'] != null ? $act['product']['product_name']: '-' }}
                                <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">
                                {{$act['product']['product_name']}}
                                </span>
                            </p>
                        </a>
                    </div>
                    <div class="p-4 text-gray-700 border-t border-gray-300 h-[157px]">
                        <div class="overflow-y-scroll h-[70px]">
                            <div class="items-center flex-1 py-2">
                                <p class="text-[14px] text-[#333333]">Lokasi:</p>
                                <div class="grid space-y-2">
                                @if(count($act['regency']) > 0)
                                    <x-produk.tag-location-card url="{{isset($act['regency'][0]['url']) ? $act['regency'][0]['url'] : ''}}">@slot('lokasi') {{isset($act['regency'][0]['name']) ? $act['regency'][0]['name'] : ''}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(json_decode($act['tag1'])!=null)
                                   @php
                                    $tag1 = json_decode($act['tag1']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag1[0]->url">@slot('lokasi') {{$tag1[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(json_decode($act['tag2'])!=null)
                                   @php
                                    $tag2 = json_decode($act['tag2']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag2[0]->url">@slot('lokasi') {{$tag2[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(count(json_decode($act['tag3'])) > 0)
                                   @php
                                    $tag3 = json_decode($act['tag3']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag3[0]->url">@slot('lokasi') {{$tag3[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(count(json_decode($act['tag4'])) > 0)
                                   @php
                                    $tag4 = json_decode($act['tag4']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag4[0]->url">@slot('lokasi') {{$tag4[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                </div>
                            </div>
                            <div class="items-center flex-1 py-2 mt-[-14px]">
                                @if ($act['tagged'] != null)
                                {{-- @dump($act['tagged']) --}}
                                <p class="truncate text-[14px] text-[#333333]">Kategori:
                                    <br>
                                    
                                    <div class="flex flex-wrap my-1">
                                        @forelse ($act['tagged'] as $index=>$tag)
                                        @if ($index == 4)
                                        @break
                                        @endif
                                        <a class="my-1 mr-1" href="">
                                            <span class="bg-[#51B449] rounded-sm p-1 my-1 text-white">
                                                {{ $tag['tag_name'] }}
                                            </span>
                                        </a>
                                        @endforeach
                                    </div>
                                </p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="px-4 pt-3 pb-4 bg-gray-100 border-t border-gray-300 mt-[-20px] h-[161px]">
                        <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
                        <div class="grid items-center justify-end grid-cols-2 pt-2">
                        <div>
                                <p class="text-[16px] text-[#333333]" style="width: 218px;">Mulai dari <br>
                                    <span class="font-bold">IDR {{$act['harga']}}</span>
                                </p>
                                <div class="inline-flex">
                                    <img src="{{ asset('storage/icons/Star 2.png') }}"
                                        class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                    <p class="-mr-2 w-[411px]">{{$act['rating']}}</p>
                                </div>
                            </div>
                            <div class="grid justify-items-end">
                                @if (isset($act['id_seller']))
                                <a href="{{route('toko.show',$act['id_seller'])}}">
                                @else<a href="#">
                                @endif
                                    <div class="grid w-12 h-12 mr-3 bg-center bg-cover border-gray-700 rounded-full" style="background-image: url({{$act['logo_toko']}})">
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if ($type == 'rental' || $type == 'transfer')
            <div class="w-full px-3 py-6 max-w-[265px] sm:w-[255px]">
                <div class="h-[553px] overflow-hidden bg-white border-2 border-solid rounded-lg shadrop-shadow-xl border-[#9E3D64]">
                    <div class="h-56 p-4 bg-center bg-cover" style="background-image: url({{$act['thumbnail']}})">
                        <div class="flex justify-end">
                            <svg class="w-6 h-6 text-white hover:fill-red-500" id="{{$act['product']['id']}}"
                                onclick="submitLike2({{$act['product']['id']}})" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path
                                    d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z">
                                </path>
                            </svg>
                        </div>
                    </div>
                    <div class="p-4 border-t-2 border-[#9E3D64] h-[61px]">
                        <a
                            href=" {{ $type == 'rental' ? route('rental.show', $act['product']['slug']) : route('transfer.show',$act['product']['slug']) }}">
                            <p class="parent-tooltip-populer text-[18px] font-bold tracking-wide text-[#D50006] group uppercase truncate">
                                {{$act['product']['product_name'] != null ? $act['product']['product_name']: '-' }}
                                <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">
                                {{$act['product']['product_name']}}
                                </span>
                            </p>
                        </a>
                    </div>
                    <div class="p-4 text-gray-700 border-t border-gray-300 h-[157px]">
                        <div class="overflow-y-scroll h-[70px]">
                            <div class="items-center flex-1 py-2">
                                <p class="text-[14px] text-[#333333]">Lokasi:</p>
                                <div class="grid space-y-2">
                                @if(count($act['regency']) > 0)
                                    <x-produk.tag-location-card url="{{isset($act['regency'][0]['url']) ? $act['regency'][0]['url'] : ''}}">@slot('lokasi') {{isset($act['regency'][0]['name']) ? $act['regency'][0]['name'] : ''}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(json_decode($act['tag1'])!=null)
                                   @php
                                    $tag1 = json_decode($act['tag1']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag1[0]->url">@slot('lokasi') {{$tag1[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(json_decode($act['tag2'])!=null)
                                   @php
                                    $tag2 = json_decode($act['tag2']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag2[0]->url">@slot('lokasi') {{$tag2[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(count(json_decode($act['tag3'])) > 0)
                                   @php
                                    $tag3 = json_decode($act['tag3']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag3[0]->url">@slot('lokasi') {{$tag3[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(count(json_decode($act['tag4'])) > 0)
                                   @php
                                    $tag4 = json_decode($act['tag4']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag4[0]->url">@slot('lokasi') {{$tag4[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                </div>
                            </div>
                            <div class="items-center flex-1 py-2 mt-[-14px]">
                                @if ($act['tagged'] != null)
                                {{-- @dump($act['tagged']) --}}
                                <p class="truncate text-[14px] text-[#333333]">Kategori:
                                    <br>
                                    
                                    <div class="flex flex-wrap my-1">
                                        @forelse ($act['tagged'] as $index=>$tag)
                                        @if ($index == 4)
                                        @break
                                        @endif
                                        <a class="my-1 mr-1" href="">
                                            <span class="bg-[#51B449] rounded-sm p-1 my-1 text-white">
                                                {{ $tag['tag_name'] }}
                                            </span>
                                        </a>
                                        @endforeach
                                    </div>
                                </p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="px-4 pt-3 pb-4 bg-gray-100 border-t border-gray-300 mt-[-20px] h-[161px]">
                        <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
                        <div class="grid items-center justify-end grid-cols-2 pt-2">
                        <div>
                                <p class="text-[16px] text-[#333333]" style="width: 218px;">Mulai dari <br>
                                    <span class="font-bold">IDR {{$act['harga']}}</span>
                                </p>
                                <div class="inline-flex">
                                    <img src="{{ asset('storage/icons/Star 2.png') }}"
                                        class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                    <p class="-mr-2 w-[411px]">{{$act['rating']}}</p>
                                </div>
                            </div>
                            <div class="grid justify-items-end">
                                @if (isset($act['id_seller']))
                                <a href="{{route('toko.show',$act['id_seller'])}}">
                                @else<a href="#">
                                @endif
                                    <div class="grid w-12 h-12 mr-3 bg-center bg-cover border-gray-700 rounded-full" style="background-image: url({{$act['logo_toko']}})">
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="grid block grid-cols-1 justify-items-center md:hidden lg:hidden">
        @if ($type == 'activity' || $type == 'tur')
        <div class="w-full px-3 py-6 max-w-[287px] sm:w-full lg:w-full">
            <div class="h-[527px] overflow-hidden bg-white border-2 border-solid rounded-lg shadrop-shadow-xl border-[#9E3D64]">
                <div class="h-56 p-4 bg-center bg-cover" style="background-image: url({{$act['thumbnail']}})">
                    <div class="flex justify-end">
                        <svg class="w-6 h-6 text-white hover:fill-red-500" id="{{$act['product']['id']}}"
                            onclick="submitLike2({{$act['product']['id']}})" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path
                                d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z">
                            </path>
                        </svg>
                    </div>
                </div>
                <div class="p-4 border-t-2 border-[#9E3D64] h-[61px]">
                    <a
                        href=" {{ $type == 'activity' ? route('activity.show', $act['product']['slug']) : route('tur.show',$act['product']['slug']) }}">
                        @if($type == 'activity')
                        <p class="parent-tooltip-populer text-[18px] font-bold tracking-wide text-[#D50006] group uppercase truncate">
                            {{$act['product']['product_name'] != null ? $act['product']['product_name']: '-' }}
                            <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">
                            {{$act['product']['product_name']}}
                            </span>
                        </p>
                        @elseif($type == 'tur')
                        <p class="parent-tooltip-populer text-[18px] font-bold tracking-wide text-[#D50006] group uppercase truncate">
                            {{$act['product']['product_name'] != null ? $act['product']['product_name']: '-' }}
                            <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">
                            {{$act['product']['product_name']}}
                            </span>
                        </p>
                        @endif
                    </a>
                </div>
                <div class="p-4 text-gray-700 border-t border-gray-300 h-[157px]">
                    <div class="overflow-y-scroll h-[70px]">
                        <div class="items-center flex-1 py-2">
                            <p class="text-[14px] text-[#333333]">Lokasi:</p>
                            <div class="grid space-y-2">
                            @if(count($act['regency']) > 0)
                                <x-produk.tag-location-card url="{{isset($act['regency'][0]['url']) ? $act['regency'][0]['url'] : ''}}">@slot('lokasi') {{isset($act['regency'][0]['name']) ? $act['regency'][0]['name'] : ''}} @endslot
                                </x-produk.tag-location-card>
                            @endif
                            @if(json_decode($act['tag1'])!=null)
                            @php
                                $tag1 = json_decode($act['tag1']);
                            @endphp
                            <x-produk.tag-location-card url="$tag1[0]->url">@slot('lokasi') {{$tag1[0]->name}} @endslot
                                </x-produk.tag-location-card>
                            @endif
                            @if(json_decode($act['tag2'])!=null)
                            @php
                                $tag2 = json_decode($act['tag2']);
                            @endphp
                            <x-produk.tag-location-card url="$tag2[0]->url">@slot('lokasi') {{$tag2[0]->name}} @endslot
                                </x-produk.tag-location-card>
                            @endif
                            @if(count(json_decode($act['tag3'])) > 0)
                            @php
                                $tag3 = json_decode($act['tag3']);
                            @endphp
                            <x-produk.tag-location-card url="$tag3[0]->url">@slot('lokasi') {{$tag3[0]->name}} @endslot
                                </x-produk.tag-location-card>
                            @endif
                            @if(count(json_decode($act['tag4'])) > 0)
                            @php
                                $tag4 = json_decode($act['tag4']);
                            @endphp
                            <x-produk.tag-location-card url="$tag4[0]->url">@slot('lokasi') {{$tag4[0]->name}} @endslot
                                </x-produk.tag-location-card>
                            @endif
                            </div>
                        </div>
                        <div class="items-center flex-1 py-2 mt-[-14px]">
                            @if ($act['tagged'] != null)
                            {{-- @dump($act['tagged']) --}}
                            <p class="truncate text-[14px] text-[#333333]">Kategori:
                                <br>
                                
                                <div class="flex flex-wrap my-1">
                                    @forelse ($act['tagged'] as $index=>$tag)
                                    @if ($index == 4)
                                    @break
                                    @endif
                                    <a class="my-1 mr-1" href="">
                                        <span class="bg-[#51B449] rounded-sm p-1 my-1 text-white">
                                            {{ $tag['tag_name'] }}
                                        </span>
                                    </a>
                                    @endforeach
                                </div>
                            </p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="px-4 pt-3 pb-4 bg-gray-100 border-t border-gray-300 mt-[-20px] h-[161px]">
                    <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
                    <div class="grid items-center justify-end grid-cols-2 pt-2">
                    <div>
                            <p class="text-[16px] text-[#333333]" style="width: 218px;">Mulai dari <br>
                                <span class="font-bold">IDR {{$act['harga']}}</span>
                            </p>
                            <div class="inline-flex">
                                <img src="{{ asset('storage/icons/Star 2.png') }}"
                                    class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                <p class="-mr-2 w-[411px]">{{$act['rating']}}</p>
                            </div>
                        </div>
                        <div class="grid justify-items-end">
                            @if (isset($act['id_seller']))
                            <a href="{{route('toko.show',$act['id_seller'])}}">
                            @else<a href="#">
                            @endif
                                <div class="grid w-12 h-12 mr-3 bg-center bg-cover border-gray-700 rounded-full" style="background-image: url({{$act['logo_toko']}})">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if ($type == 'hotel' || $type == 'xstay')
            <div class="w-full px-3 py-6 max-w-[287px] sm:w-full lg:w-full">
                <div class="h-[527px] overflow-hidden bg-white border-2 border-solid rounded-lg shadrop-shadow-xl border-[#9E3D64]">
                    <div class="h-56 p-4 bg-center bg-cover" style="background-image: url({{$act['thumbnail']}})">
                        <div class="flex justify-end">
                            <svg class="w-6 h-6 text-white hover:fill-red-500" id="{{$act['product']['id']}}"
                                onclick="submitLike2({{$act['product']['id']}})" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path
                                    d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z">
                                </path>
                            </svg>
                        </div>
                    </div>
                    <div class="p-4 border-t-2 border-[#9E3D64] h-[61px]">
                        <a
                            href=" {{ $type == 'hotel' ? route('hotel.show', $act['product']['slug']) : route('xstay.show',$act['product']['slug']) }}">
                            <p class="parent-tooltip-populer text-[18px] font-bold tracking-wide text-[#D50006] group uppercase truncate">
                                {{$act['product']['product_name'] != null ? $act['product']['product_name']: '-' }}
                                <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">
                                {{$act['product']['product_name']}}
                                </span>
                            </p>
                        </a>
                    </div>
                    <div class="p-4 text-gray-700 border-t border-gray-300 h-[157px]">
                        <div class="overflow-y-scroll h-[70px]">
                            <div class="items-center flex-1 py-2">
                                <p class="text-[14px] text-[#333333]">Lokasi:</p>
                                <div class="grid space-y-2">
                                @if(count($act['regency']) > 0)
                                    <x-produk.tag-location-card url="{{isset($act['regency'][0]['url']) ? $act['regency'][0]['url'] : ''}}">@slot('lokasi') {{isset($act['regency'][0]['name']) ? $act['regency'][0]['name'] : ''}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(json_decode($act['tag1'])!=null)
                                   @php
                                    $tag1 = json_decode($act['tag1']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag1[0]->url">@slot('lokasi') {{$tag1[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(json_decode($act['tag2'])!=null)
                                   @php
                                    $tag2 = json_decode($act['tag2']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag2[0]->url">@slot('lokasi') {{$tag2[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(count(json_decode($act['tag3'])) > 0)
                                   @php
                                    $tag3 = json_decode($act['tag3']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag3[0]->url">@slot('lokasi') {{$tag3[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(count(json_decode($act['tag4'])) > 0)
                                   @php
                                    $tag4 = json_decode($act['tag4']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag4[0]->url">@slot('lokasi') {{$tag4[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                </div>
                            </div>
                            <div class="items-center flex-1 py-2 mt-[-14px]">
                                @if ($act['tagged'] != null)
                                {{-- @dump($act['tagged']) --}}
                                <p class="truncate text-[14px] text-[#333333]">Kategori:
                                    <br>
                                    
                                    <div class="flex flex-wrap my-1">
                                        @forelse ($act['tagged'] as $index=>$tag)
                                        @if ($index == 4)
                                        @break
                                        @endif
                                        <a class="my-1 mr-1" href="">
                                            <span class="bg-[#51B449] rounded-sm p-1 my-1 text-white">
                                                {{ $tag['tag_name'] }}
                                            </span>
                                        </a>
                                        @endforeach
                                    </div>
                                </p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="px-4 pt-3 pb-4 bg-gray-100 border-t border-gray-300 mt-[-20px] h-[161px]">
                        <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
                        <div class="grid items-center justify-end grid-cols-2 pt-2">
                        <div>
                                <p class="text-[16px] text-[#333333]" style="width: 218px;">Mulai dari <br>
                                    <span class="font-bold">IDR {{$act['harga']}}</span>
                                </p>
                                <div class="inline-flex">
                                    <img src="{{ asset('storage/icons/Star 2.png') }}"
                                        class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                    <p class="-mr-2 w-[411px]">{{$act['rating']}}</p>
                                </div>
                            </div>
                            <div class="grid justify-items-end">
                                @if (isset($act['id_seller']))
                                <a href="{{route('toko.show',$act['id_seller'])}}">
                                @else<a href="#">
                                @endif
                                    <div class="grid w-12 h-12 mr-3 bg-center bg-cover border-gray-700 rounded-full" style="background-image: url({{$act['logo_toko']}})">
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if ($type == 'rental' || $type == 'transfer')
            <div class="w-full px-3 py-6 max-w-[287px] sm:w-full lg:w-full">
                <div class="h-[527px] overflow-hidden bg-white border-2 border-solid rounded-lg shadrop-shadow-xl border-[#9E3D64]">
                    <div class="h-56 p-4 bg-center bg-cover" style="background-image: url({{$act['thumbnail']}})">
                        <div class="flex justify-end">
                            <svg class="w-6 h-6 text-white hover:fill-red-500" id="{{$act['product']['id']}}"
                                onclick="submitLike2({{$act['product']['id']}})" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path
                                    d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z">
                                </path>
                            </svg>
                        </div>
                    </div>
                    <div class="p-4 border-t-2 border-[#9E3D64] h-[61px]">
                        <a
                            href=" {{ $type == 'rental' ? route('rental.show', $act['product']['slug']) : route('transfer.show',$act['product']['slug']) }}">
                            <p class="parent-tooltip-populer text-[18px] font-bold tracking-wide text-[#D50006] group uppercase truncate">
                                {{$act['product']['product_name'] != null ? $act['product']['product_name']: '-' }}
                                <span class="tooltip-populer border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute text-sm text-white left-6 group-hover:block z-999">
                                {{$act['product']['product_name']}}
                                </span>
                            </p>
                        </a>
                    </div>
                    <div class="p-4 text-gray-700 border-t border-gray-300 h-[157px]">
                        <div class="overflow-y-scroll h-[70px]">
                            <div class="items-center flex-1 py-2">
                                <p class="text-[14px] text-[#333333]">Lokasi:</p>
                                <div class="grid space-y-2">
                                @if(count($act['regency']) > 0)
                                    <x-produk.tag-location-card url="{{isset($act['regency'][0]['url']) ? $act['regency'][0]['url'] : ''}}">@slot('lokasi') {{isset($act['regency'][0]['name']) ? $act['regency'][0]['name'] : ''}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(json_decode($act['tag1'])!=null)
                                   @php
                                    $tag1 = json_decode($act['tag1']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag1[0]->url">@slot('lokasi') {{$tag1[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(json_decode($act['tag2'])!=null)
                                   @php
                                    $tag2 = json_decode($act['tag2']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag2[0]->url">@slot('lokasi') {{$tag2[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(count(json_decode($act['tag3'])) > 0)
                                   @php
                                    $tag3 = json_decode($act['tag3']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag3[0]->url">@slot('lokasi') {{$tag3[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                @if(count(json_decode($act['tag4'])) > 0)
                                   @php
                                    $tag4 = json_decode($act['tag4']);
                                   @endphp
                                   <x-produk.tag-location-card url="$tag4[0]->url">@slot('lokasi') {{$tag4[0]->name}} @endslot
                                    </x-produk.tag-location-card>
                                @endif
                                </div>
                            </div>
                            <div class="items-center flex-1 py-2 mt-[-14px]">
                                @if ($act['tagged'] != null)
                                {{-- @dump($act['tagged']) --}}
                                <p class="truncate text-[14px] text-[#333333]">Kategori:
                                    <br>
                                    
                                    <div class="flex flex-wrap my-1">
                                        @forelse ($act['tagged'] as $index=>$tag)
                                        @if ($index == 4)
                                        @break
                                        @endif
                                        <a class="my-1 mr-1" href="">
                                            <span class="bg-[#51B449] rounded-sm p-1 my-1 text-white">
                                                {{ $tag['tag_name'] }}
                                            </span>
                                        </a>
                                        @endforeach
                                    </div>
                                </p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="px-4 pt-3 pb-4 bg-gray-100 border-t border-gray-300 mt-[-20px] h-[161px]">
                        <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
                        <div class="grid items-center justify-end grid-cols-2 pt-2">
                        <div>
                                <p class="text-[16px] text-[#333333]" style="width: 218px;">Mulai dari <br>
                                    <span class="font-bold">IDR {{$act['harga']}}</span>
                                </p>
                                <div class="inline-flex">
                                    <img src="{{ asset('storage/icons/Star 2.png') }}"
                                        class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                    <p class="-mr-2 w-[411px]">{{$act['rating']}}</p>
                                </div>
                            </div>
                            <div class="grid justify-items-end">
                                @if (isset($act['id_seller']))
                                <a href="{{route('toko.show',$act['id_seller'])}}">
                                @else<a href="#">
                                @endif
                                    <div class="grid w-12 h-12 mr-3 bg-center bg-cover border-gray-700 rounded-full" style="background-image: url({{$act['logo_toko']}})">
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
<script>
console.log('{{$type}}')
</script>