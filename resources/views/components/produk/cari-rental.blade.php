<div class="p-3 md:p-0 lg:p-0">
    <div class="rounded-lg bg-kamtuu-primary">
        {{-- Search --}}
        <form action="{{ route('rental.search') }}" method="POST">
            @csrf
            <div
                class="grid grid-cols-1 p-10 divide-y-2 md:grid-cols-1 lg:grid-cols-6 lg:divide-x-2 md:divide-x-2 divide-kamtuu-primary">
                <div class="relative">
                    <span
                        class="absolute inset-y-0 right-0 grid w-10 text-gray-400 text-gray-500 pointer-events-none place-content-center">
                        <img class="mr-2" src="{{ asset('storage/icons/icon-maps.svg') }}" alt="location" width="12px">
                    </span>
                    <label for="FormSearch" class="sr-only"> </label>
                    <div
                        class="w-full lg:rounded-l-lg md:rounded-l-lg sm:text-sm focus:ring-0 focus:border-kamtuu-primary">
                        {{ $rental }}
                    </div>
                </div>
                <div class="relative">
                    <label for="FormSearch" class="sr-only">PerJam</label>
                    <select class="w-full h-full focus:ring-0 focus:border-kamtuu-primary" name="durasi" required
                        id="FormSearch">
                        <option value="">Durasi</option>
                        <option value="Per Jam">Per Jam</option>
                        <option value="Per Hari">Per Hari</option>
                    </select>
                </div>
                <div
                    class="grid w-full bg-white justify-items-center grid-cols-[80%_20%] border border-gray-300 h-[39.5px]">
                    <div class="ml-6 place-self-center">
                        <input class="focus:ring-0 focus:border-kamtuu-primary" id="datepickerStartFromRental"
                            name="tgl_mulai" placeholder="Tanggal Mulai" required />
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                            class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>
                <div
                    class="grid w-full bg-white justify-items-center grid-cols-[80%_20%] border border-gray-300 h-[39.5px]">
                    <div class="ml-6 place-self-center">
                        <input class="focus:ring-0 focus:border-kamtuu-primary" id="timepickerStartFromRental"
                            name="waktu_mulai" placeholder="Waktu Mulai" required />
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                            class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>
                <div
                    class="grid w-full bg-white justify-items-center grid-cols-[80%_20%] border border-gray-300 h-[39.5px]">
                    <div class="ml-6 place-self-center">
                        <input class="focus:ring-0 focus:border-kamtuu-primary" id="datepickerEndFromRental"
                            name="tgl_selesai" placeholder="Tanggal Selesai" required />
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                            class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>
                <div
                    class="grid w-full bg-white justify-items-center grid-cols-[80%_20%] border border-gray-300 h-[39.5px]">
                    <div class="ml-6 place-self-center">
                        <input class="focus:ring-0 focus:border-kamtuu-primary" id="timepickerEndFromRental"
                            name="waktu_selesai" placeholder="Waktu Selesai" required />
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                            class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>
                <div class="relative -mt-[1.5px]">
                    <label for="FormSearch" class="sr-only">Cek</label>
                    <select class="w-full h-full focus:ring-0 focus:border-kamtuu-primary" name="lepas_kunci" required
                        id="FormSearch">
                        <option value="">Keterangan</option>
                        <option value="Lepas Kunci">Lepas Kunci</option>
                        <option value="Tidak Lepas Kunci">Tidak Lepas Kunci</option>
                    </select>
                </div>
                <div class="relative grid justify-items-center">
                    <label for="FormSearch" class="sr-only"> </label>
                    <button class="w-full h-full pt-2 bg-kamtuu-third lg:rounded-r-lg md:rounded-r-lg">
                        <p>Cari</p>
                        <span
                            class="absolute inset-y-0 right-0 grid w-10 text-gray-400 text-gray-500 pointer-events-none place-content-center">
                            <img class="mr-2" src="{{ asset('storage/icons/search-ic.png') }}" alt="location"
                                width="12px">
                        </span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $('#datepickerStartFromRental').datepicker();
</script>
<script>
    $('#timepickerStartFromRental').timepicker();
</script>
<script>
    $('#datepickerEndFromRental').datepicker();
</script>
<script>
    $('#timepickerEndFromRental').timepicker();
</script>
