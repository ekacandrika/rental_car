<div class="grid grid-row-5 rounded-lg shadow-lg justify-items-start py-5">
    <div class="grid grid:row-2 grid-cols-10 gap-2 pl-3">
        <img class="w-20 h-20 rounded-full bg-gray-400" scr="https://via.placeholder.com/50x50">
        <div class="col-span-2 my-auto">
            <p class="font-semibold text-gray-900">Jhony Wilson<p/>
            {{-- Carbon::parse($p->created_at)->diffForHumans(); --}}
            <p class="font-light text-gray-600 text-xs">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}<p/>
        </div>
    </div>
    @if ($review->star_rating==5)
    <div class="grid grid-cols-6 pl-3 mb-2">
    @else  
    <div class="grid grid-cols-5 pl-3 mb-2">
    @endif
        <div class="col-span-6">
            <img class="w-[20px] h-[20] mt-[0.3rem] mr-1  ml-3 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
            @for ($i = 0; $i < ($review->star_rating-1); $i++)
                <img class="w-[20px] h-[20] mt-[0.3rem] mr-1 -ml-2 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
            @endfor
        </div>
    </div>
    <div class="grid grid-cols-5 space-y-5 text-justify pl-3 pr-5">
        <div class="col-span-5 pl-3">
        <p class="text-ulasan">{{$review->comments}}</p>
        </div>
    </div>
    {{-- <div class="mt-3 pl-3">
        <button type="button" id="show-btn-ulasan" class="px-3 py-2 text-black text-sm underline hidden">Tampilkan</button>
        <button type="button" id="hide-btn-ulasan" class="px-3 py-2 text-black text-sm underline hidden">Sembunyikan</button>
    </div> --}}
    @if($review->gallery_post)
    <div class="flex justify-start justify-items-center pl-4">
        @php
            $post_gallery = json_decode($review->gallery_post, true);
            //dump($post_gallery);
        @endphp
        <img class="w-50 h-40 bg-slate-400 rounded-lg" src="{{$post_gallery != null ? Storage::url('gallery_post/'.$post_gallery['result'][0]):'https://via.placeholder.com/540x540'}}">
        <div class="relative" x-data="{open:false}" @keydown.escape="open = false">
            <button @click="open=true" type="button" id="hide-btn-foto-ulasan" 
                class="absolute px-3 py-2 text-black text-sm underline" style="width:169px;top:110px">
                Tampilkan semua foto
            </button>
            <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                        style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                        <div
                            class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                            <button
                                class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                            </button>
                        </div>
                        <div class="h-full w-full flex items-center justify-center overflow-hidden"
                            x-data="{active: 0, slides: {{ ($review->gallery_post) }} }">
                            <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                    <button type="button"
                                        class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === 0 ? slides.length - 1 : active - 1">
                                        <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>
                            @if ($post_gallery)
                                @foreach ($post_gallery['result'] as $index=>$gallery)
                                    <div class="h-full w-full flex items-center justify-center absolute">
                                        <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                            x-show="active === {{ $index }}"
                                            x-transition:enter="transition ease-out duration-150"
                                            x-transition:enter-start="opacity-0 transform scale-90"
                                            x-transition:enter-end="opacity-100 transform scale-100"
                                            x-transition:leave="transition ease-in duration-150"
                                            x-transition:leave-start="opacity-100 transform scale-100"
                                            x-transition:leave-end="opacity-0 transform scale-90">

                                            <img src="{{ Storage::url('gallery_post/'.$gallery) }}"
                                                class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                        </div>
                                        <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                            x-show="active === {{ $index }}">
                                            <span class="w-12 text-right" x-text="{{ $index }} + 1"></span>
                                            <span class="w-4 text-center">/</span>
                                            <span class="w-12 text-left"
                                                x-text="{{ count($post_gallery['result']) }}"></span>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            
                            <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-start w-12 md:ml-16">
                                    <button type="button"
                                        class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === slides.length - 1 ? 0 : active + 1">
                                        <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
    @endif

</div>  
<script>
    
    function eksert(){
        return{
            
        }
    }
    /*
    let text = $(".text-ulasan").text();

    console.log(text.length-442)
    console.log(text.length)
    let cuting_char = text.length - 442;

    if(text.length > 442){
        //alert('test')
        let new_word = text.substr(0,445)
        console.log(text)
        console.log(new_word)
        
        //$("#text-ulasan").html();
        $("#text-ulasan").html(new_word+'..')
        $("#show-btn-ulasan").show()

        $("#show-btn-ulasan").on("click",()=>{

            $("#show-btn-ulasan").hide()
            $("#text-ulasan").html(text)
            $('#hide-btn-ulasan').show();
        })

        $("#hide-btn-ulasan").on("click",()=>{
            
            $('#hide-btn-ulasan').hide();
            $("#text-ulasan").html(new_word+'..')
            $('#show-btn-ulasan').show();
        })

    }else{
       $("#show-btn-ulasan").hide() 
    }
    */
</script>