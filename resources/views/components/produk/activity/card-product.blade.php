@if ($type=='activity')
    <div class="w-full h-[168px] px-3 py-6 max-w-[310px] sm:w-full lg:w-full">
        <div class="overflow-hidden bg-white border-2 border-solid rounded-lg shadrop-shadow-xl border-[#9E3D64]">
            <div class="h-56 p-4 bg-center bg-cover"
                style="background-image: url({{ isset($act->productdetail->thumbnail) ? asset($act->productdetail->thumbnail) : (isset($act->productdetail->gallery) ? asset(json_decode($act->productdetail->gallery)[0]) : asset('https://via.placeholder.com/450x450'))}})">
                <div class="flex justify-end">
                    <svg class="w-6 h-6 text-white hover:fill-red-500" id="{{$act->id}}"
                        onclick="submitLike2({{$act->id}})" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <path
                            d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z">
                        </path>
                    </svg>
                </div>
                <div class="grid mt-36 place-content-end">
                    <span class="bg-[#D50006] py-[2px] px-[5px] rounded-sm uppercase text-white font-bold ">16%
                        off</span>
                </div>
            </div>
            <div class="p-4 border-t-2 border-[#9E3D64]">
                <a href="{{'activity/'.$act->slug}}">
                    <p class="text-[18px] font-bold tracking-wide text-[#D50006] uppercase">{{ $act->product_name }}
                    </p>
                </a>
                <div class="inline-flex">
                    <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1"
                        alt="polygon 3" title="Kamtuu">
                    <p>4.5 (75 review)</p>
                </div>
            </div>
            <div class="p-4 text-gray-700 border-t border-gray-300 ">
                <div class="items-center flex-1 py-2">
                    <p class="text-[14px] text-[#333333]">Lokasi:
                        @if (isset($act->name))
                        <x-produk.tag-location-card>@slot('lokasi') 
                        {{ isset($act->name) ? $act->name : '' }} @endslot
                        </x-produk.tag-location-card>
                        @endif
                    </p>
                </div>
                <div class="items-center flex-1 py-2">
                    <p class="text-[14px] text-[#333333]">Ringkasan:
                        <br>
                        <x-produk.tag-ringkasan-card>
                        @slot('ringkasan') 
                        {!! isset($act->deskripsi) ? $act->deskripsi : '' !!}
                        @endslot</x-produk.tag-ringkasan-card>
                    </p>
                </div>
            </div>
            <div class="px-4 pt-3 pb-4 bg-gray-100 border-t border-gray-300">
                <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
                <div class="grid items-center justify-end grid-cols-2 pt-2">
                    <div>
                        <p class="text-[16px] text-[#333333]">Mulai dari <br>
                            <span class="font-bold">IDR {{ isset($act->dewasa_residen) ?
                                $act->dewasa_residen : '-' }}</span>
                        </p>
                        <div class="inline-flex">
                            <img src="{{ asset('storage/icons/Star 2.png') }}"
                                class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                            <p class="-mr-2"> 4.5 &ThinSpace; 75 review</p>
                        </div>
                    </div>
                    <div class="grid justify-items-end">
                        <a href="#">
                            <div class="grid w-12 h-12 mr-3 bg-center bg-cover rounded-full"
                                style="background-image: url({{ isset($act->user->profile_photo_path) ? $act->user->profile_photo_path : 'https://via.placeholder.com/50x50'}})">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>    
@endif