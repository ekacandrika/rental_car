@props([
    'url' => '',
])

<a href="{{$url}}">
    <span class="bg-[#23AEC1] py-[2px] px-[5px] rounded-sm">{{ $lokasi }}</span>
</a>