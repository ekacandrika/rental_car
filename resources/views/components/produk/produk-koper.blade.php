
<div class="shadow-lg max-w-8xl rounded-xl">
    <div class="grid grid-cols-1 md:grid-cols-[5%_15%_60%_20%] lg:grid-cols-[5%_15%_60%_20%]">
        {{-- Cols 1 --}}
        <div class="grid place-content-center">
            <input class="checked:bg-kamtuu-second" type="checkbox" name="" id="">
        </div>
        {{-- Cols 2 --}}
        <div class="grid py-3 place-content-center">
            <img src="{{ asset('storage/img/dummy-koper.jpg') }}" class="w-[100px] h-[100px] inline-flex rounded-lg" alt="rating" title="Rating">
        </div>
        {{-- Cols 3 --}}
        <div class="grid my-5 justify-items-center lg:justify-items-start">
            <p class="text-[12px]">{{ $kategori }}</p>
            <p class="font-semibold">{{ $judul_produk }}</p>
            <p class="font-semibold">{{ $nama_toko }}</p>
            <p class="pt-8 text-[12px]">{{ $tanggal_produk }}</p>
            <p>{{ $rincian_1 }}</p>
            <p>{{ $rincian_2 }}</p>
        </div>
        {{-- Cols 4 --}}
        <div class="grid my-5 justify-items-center lg:justify-items-end">
            <p class="px-5">{{ $id_booking }}</p>
            <p class="text[18px] text-kamtuu-second px-5">{{ $harga }}</p>
                <x-destindonesia.button-secondary>
                   @slot('button')
                   Hapus
                   @endslot
                </x-destindonesia.button-secondary>
        </div>
    </div>
</div>
