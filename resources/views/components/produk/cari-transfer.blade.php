<div class="p-3 md:p-0 lg:p-0">
    <div class="rounded-lg bg-kamtuu-primary">
        {{-- Search --}}
        <form action="{{ route('transfer.search') }}" method="POST">
            @csrf
            <div
                class="grid grid-cols-1 p-10 divide-y-2 md:grid-cols-7 lg:grid-cols-7 lg:divide-x-2 md:divide-x-2 divide-kamtuu-primary">
                <div class="relative mt-[2px]">
                    <label for="FormSearch" class="sr-only">Dari</label>
                    {{ $transferfrom }}
                    {{-- <select class="w-full h-full focus:ring-0 focus:border-kamtuu-primary md:rounded-l-lg lg:rounded-l-lg"
                    name="" id="FormSearch">
                    <option value="">Dari</option>
                </select> --}}
                </div>
                <div class="relative">
                    <label for="FormSearch" class="sr-only">Ke</label>
                    {{ $transferto }}
                    {{-- <select class="w-full h-full focus:ring-0 focus:border-kamtuu-primary" name="" id="FormSearch">
                    <option value="">Ke</option>
                </select> --}}
                </div>
                <div
                    class="grid w-full bg-white justify-items-center grid-cols-[80%_20%] border border-gray-300 h-[39.5px]">
                    <div class="place-self-center -ml-[3rem] md:-ml-[23rem] lg:ml-6">
                        <input class="focus:ring-0 focus:border-kamtuu-primary" id="datepickerProdukTransfer"
                            placeholder="Tanggal" name="tgl" required />
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                            class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>
                <div
                    class="grid w-full bg-white justify-items-center grid-cols-[80%_20%] border border-gray-300 h-[39.5px]">
                    <div class="place-self-center -ml-[3rem] md:-ml-[23rem] lg:ml-6">
                        <input class="focus:ring-0 focus:border-kamtuu-primary" id="timepickerProdukTransfer"
                            name="waktu_ambil" placeholder="Waktu Ambil" required />
                    </div>
                </div>
                {{-- <div class="relative">
                <label for="FormSearch" class="sr-only">Cek</label>
                <select class="w-full h-full focus:ring-0 focus:border-kamtuu-primary" name=""
                    id="FormSearch">
                    <option value="">Jumlah Orang</option>
                </select>
            </div> --}}
                <div class="relative">
                    <label for="FormSearch" class="sr-only">Cek</label>
                    <select
                        class="w-full h-full focus:ring-0 focus:border-kamtuu-primary md:rounded-r-lg lg:rounded-none"
                        name="metode_transfer" id="FormSearch" required>
                        <option value="">Metode Transfer</option>
                        <option value="Transfer Private">Transfer Private</option>
                        <option value="Transfer Umum">Transfer Umum</option>
                    </select>
                </div>
                <div class="relative grid justify-items-center">
                    <label for="FormSearch" class="sr-only"> </label>
                    <button class="w-full h-full pt-2 bg-kamtuu-third lg:rounded-r-lg md:rounded-r-lg">
                        <p>Cari</p>
                        <span
                            class="absolute inset-y-0 right-0 grid w-10 text-gray-400 text-gray-500 pointer-events-none place-content-center">
                            <img class="mr-2" src="{{ asset('storage/icons/search-ic.png') }}" alt="location"
                                width="12px">
                        </span>
                    </button>
                </div>

            </div>
        </form>
    </div>
</div>
<script>
    $('#datepickerProdukTransfer').datepicker();
</script>
<script>
    $('#timepickerProdukTransfer').timepicker();
</script>
