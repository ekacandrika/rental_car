<style>
@media (min-width: 1024px)
.lg\:ml-6 {
    margin-left: 2.5rem;
}
</style>
<div class="p-3 md:p-0 lg:p-0">
    <div class="rounded-lg bg-kamtuu-primary">
        {{-- Search --}}
        <form action="{{ route('tur.search') }}" method="POST">
            @csrf
            <div
                class="grid grid-cols-1 p-10 divide-y-2 md:grid-cols-1 lg:grid-cols-6 lg:divide-x-2 md:divide-x-2 divide-kamtuu-primary">
                <div class="relative">
                    <span
                        class="absolute inset-y-0 right-0 grid w-10 text-gray-400 text-gray-500 pointer-events-none place-content-center">
                        <img class="mr-2" src="{{ asset('storage/icons/icon-maps.svg') }}" alt="location" width="12px">
                    </span>
                    <label for="FormSearch" class="sr-only"> </label>
                    <div
                        class="w-full lg:rounded-l-lg md:rounded-l-lg sm:text-sm focus:ring-0 focus:border-kamtuu-primary">
                        {{ $turFrom }}
                    </div>
                </div>
                <div class="relative">
                    <label for="FormSearch" class="sr-only"> </label>
                    <div
                        class="w-full lg:rounded-l-lg md:rounded-l-lg sm:text-sm focus:ring-0 focus:border-kamtuu-primary">
                        {{ $turTo }}
                    </div>
                    <span
                        class="absolute inset-y-0 right-0 grid w-10 text-gray-400 text-gray-500 pointer-events-none place-content-center">
                        <img class="mr-2" src="{{ asset('storage/icons/location-dot-solid 1.svg') }}" alt="location"
                            width="12px">
                    </span>
                </div>
                <div
                    class="grid w-full bg-white justify-items-center grid-cols-[80%_20%] border border-gray-300 h-[39.5px]">
                    <div class="place-self-center -ml-[3rem] md:-ml-[23rem] lg:ml-6" style="margin-left: 2.5rem;">
                        <input class="placeholder-black focus:ring-0 focus:border-kamtuu-primary " name="tgl"
                            id="datepickerProdukTur" placeholder="Tanggal" required />
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/icon-kalender.svg') }}" class="w-[15px] h-[15px] mt-[0.6rem]"
                            alt="polygon 3" title="Kamtuu">
                    </div>
                </div>
                <div class="relative flex">
                    <label for="FormSearch" class="sr-only">Cek</label>
                    <select id="maps" class="w-full focus:ring-0 focus:border-kamtuu-primary" name="confirmation"
                        id="FormSearch">
                        <option value="">Confirmation By</option>
                        <option value="instant">Instant</option>
                        <option value="by_seller">Seller</option>
                    </select>
                </div>
                <div class="relative flex">
                    <label for="FormSearch" class="sr-only">Cek</label>
                    <select class="w-full focus:ring-0 focus:border-kamtuu-primary" name="kategori" id="FormSearch"
                        required>
                        <option value="">Kategori</option>
                        <option value="Open Trip">Open Trip</option>
                        <option value="Tur Private">Private Trip</option>
                    </select>
                </div>
                <div class="relative grid justify-items-center">
                    <label for="FormSearch" class="sr-only"> </label>
                    <button class="w-full h-full bg-kamtuu-third lg:rounded-r-lg">
                        <p>Cari</p>
                        <span
                            class="absolute inset-y-0 right-0 grid w-10 text-gray-400 text-gray-500 pointer-events-none place-content-center">
                            <img class="mr-2" src="{{ asset('storage/icons/search-ic.png') }}" alt="location"
                                width="12px">
                        </span>
                    </button>
                </div>

                {{-- open & private --}}
                <div class="grid lg:col-span-2 md:col-span-1">
                    <div class="flex pt-5">
                        <img id="privateTrip" src="{{ asset('storage/icons/info white.png') }}" alt="location">
                        <label class="mx-3 text-white" for="privateTrip">Private Trip</label>

                        <img id="openTrip" src="{{ asset('storage/icons/info white.png') }}" alt="location">
                        <label class="mx-3 text-white" for="openTrip">Open Trip</label>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>
<script>
    $('#datepickerProdukTur').datepicker();
</script>
