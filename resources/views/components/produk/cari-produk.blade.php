<style>
.gj-textbox-md {
    border: none;
    border-bottom: none;
    display: block;
    font-family: Helvetica,Arial,sans-serif;
    font-size: 16px;
    line-height: 16px;
    padding: 4px 0;
    margin: 0;
    width: 100%;
    background: 0 0;
    text-align: left;
    color: rgba(0,0,0,.87);
}
.gj-icon{
    margin-right:-57px;
}
</style>
<div class="flex items-center justify-center max-w-screen-sm md:mx-2 lg:mx-auto lg:max-w-[85rem]">
            <div class="container flex flex-col items-stretch justify-center h-full p-2 mx-auto sm:p-8 sm:-mt-[75px]" x-data="getSearch()" id="formSearch" style="margin-top:0px">
                <!-- TABS -->
                <div class="z-10 flex justify-start -space-x-px">
                    <div class="grid grid-cols-3 grid-rows-1 md:flex lg:flex">
                        <a href="!#0" @click.prevent="tab = 1"
                            :class="{
                                'cursor-default border-b-0 bg-kamtuu-second': tab ===
                                    1,
                                'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                                    1
                            }"
                            class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none lg:rounded-tl-lg" id="tourSearch">Paket
                            Tur
                        </a>
                        <a href="!#0" @click.prevent="tab = 2"
                            :class="{
                                'cursor-default border-b-0 bg-kamtuu-second': tab ===
                                    2,
                                'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                                    2
                            }"
                            class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none" id="transferSearch">Transfer
                        </a>
                        <a href="!#0" @click.prevent="tab = 3"
                            :class="{
                                'cursor-default border-b-0 bg-kamtuu-second': tab ===
                                    3,
                                'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                                    3
                            }"
                            class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none" id="rentalSearch">Rental
                            Kendaraan
                        </a>
                        <a href="!#0" @click.prevent="tab = 4"
                            :class="{
                                'cursor-default border-b-0 bg-kamtuu-second': tab ===
                                    4,
                                'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                                    4
                            }"
                            class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none" id="activitySearch">Aktivitas
                        </a>
                        <a href="!#0" @click.prevent="tab = 5"
                            :class="{
                                'cursor-default border-b-0 bg-kamtuu-second': tab ===
                                    5,
                                'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                                    5
                            }"
                            class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none" id="hotelSearch">Hotel
                        </a>
                        <a href="!#0" @click.prevent="tab = 6"
                            :class="{
                                'cursor-default border-b-0 bg-kamtuu-second': tab ===
                                    6,
                                'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                                    6
                            }"
                            class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none lg:rounded-tr-md" id="xstaySearch">Xstay
                        </a>
                    </div>
                </div>


                <!-- Tabs Contains -->
                {{-- Paket Tur --}}
                <form action="{{ route('tur.search') }}" method="POST">
                    @csrf
                    <div x-show="tab === 1"
                        class="z-0 grid grid-cols-1 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-6 md:grid-cols-1 bg-kamtuu-second justify-items-center divide-kamtuu-second lg:divide-x">
                        <div class="grid w-full p-2 bg-white lg:rounded-l-lg justify-items-left lg:justify-items-left">
                            {{--                <div> --}}
                            {{--                    <img src="{{ asset('storage/icons/icon-maps.svg') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu"> --}}
                            {{--                </div> --}}
                            <div>
                                <div class="flex-inline">
                                    {{ $turFrom }}
                                    {{-- <select name="" id="maps"
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                        <option value="">Destinasi</option>
                                        <option value="">1</option>
                                        <option value="">1</option>
                                    </select> --}}
                                </div>
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-left lg:justify-items-left">
                            <div>
                                <div class="flex-inline">
                                    {{ $turTo }}
                                    {{-- <select name="" id="point"
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                        <option value="">Mulai dari</option>
                                        <option value="">1</option>
                                        <option value="">1</option>
                                    </select> --}}
                                </div>
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-left grid-cols-[80%_20%]">
                            <div class="lg:place-self-center md:place-self-center place-self-start">
                                <input class="ms:!-ml-[9.1rem] xs:!-ml-[9.1rem] md:!w-[31rem] md:!ml-[6rem]  lg:!ml-[2rem] lg:!w-[9rem] grid-cols-[80%_20%] w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7"
                                    name="tgl" id="datepickerSearch" placeholder="Tanggal" />
                            </div>
                            <div class="md:hidden lg:hidden">
                                <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                    class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-left lg:justify-items-left" style="padding-top: 18px;">
                            <div>
                                <div class="flex-inline">
                                    <select name="confirmation" id=""
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                        <option value="">Confirmation By</option>
                                        <option value="instant">Instant</option>
                                        <option value="by_seller">Seller</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-left lg:justify-items-left" style="padding-top: 18px;">
                            <div>
                                <div class="flex-inline">
                                    <select name="kategori" id=""
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7"
                                        required>
                                        <option value="">Kategori</option>
                                        <option value="Open Trip">Open Trip</option>
                                        <option value="Tur Private">Private Trip</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div
                            class="flex justify-center w-full p-2 text-white md:rounded-r-lg lg:rounded-r-lg bg-kamtuu-primary">
                            <button>
                                <div>
                                    <div class="flex-inline">
                                        <h1 class="uppercase">cari</h1>
                                    </div>
                                </div>
                            </button>
                        </div>
                    </div>
                </form>

                {{-- Transfer --}}
                <form action="{{ route('transfer.search') }}" method="POST">
                    @csrf
                    <div x-show="tab === 2"
                        class="z-0 grid grid-cols-1 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-7 md:grid-cols-1 bg-kamtuu-second justify-items-center divide-kamtuu-second lg:divide-x">
                        <div class="grid w-full p-2 bg-white lg:rounded-l-lg justify-items-left lg:justify-items-left">
                            {{--                <div> --}}
                            {{--                    <img src="{{ asset('storage/icons/icon-maps.svg') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu"> --}}
                            {{--                </div> --}}
                            <div>
                                <div class="flex-inline">
                                    {{ $transferFrom }} 
                                    {{-- <template x-if="list_search > 0">
                                        <template x-for="option in list_search">
                                            <option :value="option.id" x-text="option.name"></option>
                                        </template>
                                    </template>
                                    <template x-if="list_search == 0">
                                        <option value="">Dari</option>
                                    </template> --}}
                                    {{-- <select name="" id="maps"
                                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                    <option value="">Dari</option>
                                    <option value="">1</option>
                                    <option value="">1</option>
                                </select> --}}
                                </div>
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-left lg:justify-items-left">
                            <div>
                                <div class="flex-inline">
                                    {{ $transferTo }}
                                    {{-- <select name="" id="point"
                                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                    <option value="">Ke</option>
                                    <option value="">1</option>
                                    <option value="">1</option>
                                </select> --}}
                                </div>
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-center grid-cols-[80%_20%]">
                            <div class="lg:place-self-center md:place-self-center place-self-start">
                                <input class="!ml-[9px] md:!ml-[6rem] lg:!ml-[2rem] lg:!w-[9rem] md:!w-[31rem]"
                                    id="datepickerTransfer" placeholder="Tanggal" name="tgl" required />
                            </div>
                            <div class="md:hidden lg:hidden">
                                <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                    class="w-[15px] h-[17px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-start grid-cols-[70%_30%]">
                            <div class="lg:place-self-center md:place-self-center place-self-start">
                                <input class="!ml-[9px] lg:ml-0 md:!w-[31rem] md:!ml-[9rem]  lg:!ml-[2rem] lg:!w-[9rem] "
                                    id="timepickerTransfer" placeholder="Waktu Ambil" name="waktu_ambil" required />
                            </div>
                        </div>

                        {{-- <div class="grid w-full p-2 bg-white justify-items-left">
                        <div>
                            <div class="flex-inline">
                                <select name="" id=""
                                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                    <option value="">Jumlah Orang</option>
                                    <option value="">1</option>
                                    <option value="">1</option>
                                </select>
                            </div>
                        </div>
                    </div> --}}

                        <div class="grid w-full p-2 bg-white justify-items-left">
                            <div>
                                <div class="ml-1 flex-inline">
                                    <select name="metode_transfer" id="" required
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                        <option value="">Metode Transfer</option>
                                        <option value="Transfer Private">Transfer Private</option>
                                        <option value="Transfer Umum">Transfer Umum</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div
                            class="flex justify-center w-full p-2 text-white md:rounded-r-lg lg:rounded-r-lg bg-kamtuu-primary">
                            <button>
                                <div>
                                    <div class="flex-inline">
                                        <h1 class="uppercase">cari</h1>
                                    </div>
                                </div>
                            </button>
                        </div>
                    </div>
                </form>

                {{-- Rental Kendaraan --}}
                <form action="{{ route('rental.search') }}" method="POST">
                    @csrf
                    <div x-show="tab === 3"
                        class="z-0 grid grid-cols-1 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-6 md:grid-cols-1 bg-kamtuu-second justify-items-center divide-kamtuu-second lg:divide-x">
                        <div class="grid w-full p-2 bg-white lg:rounded-l-lg justify-items-left lg:justify-items-left">
                            {{--                <div> --}}
                            {{--                    <img src="{{ asset('storage/icons/icon-maps.svg') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu"> --}}
                            {{--                </div> --}}
                            <div>
                                <div class="flex-inline">
                                    {{ $rental }}
                                    {{-- <select name="" id="maps"
                                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                    <option value="">Lokasi</option>
                                    <option value="">1</option>
                                    <option value="">1</option>
                                </select> --}}
                                </div>
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-left">
                            <div>
                                <div class="flex-inline">
                                    <select name="durasi" id=""
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7"
                                        required>
                                        <option value="">Durasi</option>
                                        <option value="Per Jam">Per Jam</option>
                                        <option value="Per Hari">Per Hari</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-center grid-cols-[80%_20%]">
                            <div class="lg:place-self-center md:place-self-center place-self-start">
                                <input class="!ml-[9px] md:!w-[31rem] md:!ml-[6rem]  lg:!ml-[2rem] lg:!w-[9rem]"
                                    name="tgl_mulai" id="datepickerStartFromRental" placeholder="Rental Mulai" required />
                            </div>
                            <div class="md:hidden lg:hidden">
                                <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                    class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-start grid-cols-[70%_30%]">
                            <div class="lg:place-self-center md:place-self-center place-self-start">
                                <input class="!ml-[9px] lg:ml-0 md:!w-[31rem] md:!ml-[9rem]  lg:!ml-[2rem] lg:!w-[9rem] "
                                    name="waktu_mulai" id="timepickerPickup" placeholder="Waktu Ambil" required />
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-center grid-cols-[80%_20%]">
                            <div class="lg:place-self-center md:place-self-center place-self-start">
                                <input class="!ml-[9px] lg:ml-0 md:!w-[31rem] md:!ml-[6rem]  lg:!ml-[2rem] lg:!w-[9rem] "
                                    name="tgl_selesai" id="datepickerEndFromRental" placeholder="Rental Selesai" required />
                            </div>
                            <div class="md:hidden lg:hidden">
                                <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                    class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-start grid-cols-[70%_30%]">
                            <div class="lg:place-self-center md:place-self-center place-self-start">
                                <input class="!ml-[9px] lg:ml-0 md:!w-[31rem] md:!ml-[9rem]  lg:!ml-[2rem] lg:!w-[9rem]"
                                    name="waktu_selesai" id="timepickerPickupEnd" placeholder="Waktu Selesai" required />
                            </div>
                        </div>

                        {{-- <div class="grid w-full p-2 bg-white justify-items-left">
                        <div>
                            <div class="flex-inline">
                                <select name="" id=""
                                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                    <option value="">Jumlah Orang</option>
                                    <option value="">1</option>
                                    <option value="">1</option>
                                </select>
                            </div>
                        </div>
                    </div> --}}

                        <div class="grid w-full p-2 bg-white justify-items-left">
                            <div>
                                <div class="flex-inline">
                                    <select name="lepas_kunci" id=""
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7"
                                        required>
                                        <option value="">Keterangan</option>
                                        <option value="Lepas Kunci">Lepas Kunci</option>
                                        <option value="Tidak Lepas Kunci">Tidak Lepas Kunci</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div
                            class="flex justify-center w-full p-2 text-white md:rounded-r-lg lg:rounded-r-lg bg-kamtuu-primary">
                            <button>
                                <div>
                                    <div class="flex-inline">
                                        <h1 class="uppercase">cari</h1>
                                    </div>
                                </div>
                            </button>
                        </div>
                    </div>
                </form>

                {{-- Aktivitas --}}
                <form action="{{ route('activity.search') }}" method="POST">
                    @csrf
                    <div x-show="tab === 4"
                        class="z-0 grid grid-cols-1 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-6 md:grid-cols-1 bg-kamtuu-second justify-items-center divide-kamtuu-second lg:divide-x">
                        <div class="grid w-full p-2 bg-white justify-items-left lg:justify-items-left">
                            <div>
                                <div class="flex-inline">
                                    {{ $activity }}
                                    {{-- <select name="" id=""
                                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                    <option value="">Pilih Lokasi</option>
                                    <option value="">1</option>
                                    <option value="">1</option>
                                </select> --}}
                                </div>
                            </div>
                        </div>

                        <div class="grid w-full grid-cols-1 col-span-4 p-2 bg-white justify-items-center">
                            <div>
                                <div class="flex-inline">
                                    <input placeholder="Cari Activity" name="activity_name" id=""
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                </div>
                            </div>
                        </div>

                        <div
                            class="flex justify-center w-full p-2 text-white md:rounded-r-lg lg:rounded-r-lg bg-kamtuu-primary">
                            <button>
                                <div>
                                    <div class="flex-inline">
                                        <h1 class="uppercase">cari</h1>
                                    </div>
                                </div>
                            </button>
                        </div>
                    </div>
                </form>

                {{-- Hotel --}}
                <form action="{{ route('hotel.search') }}" method="POST">
                    @csrf
                    <div x-show="tab === 5"
                        class="z-0 grid grid-cols-1 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-6 md:grid-cols-1 bg-kamtuu-second justify-items-center divide-kamtuu-second lg:divide-x">
                        <div class="grid w-full p-2 bg-white lg:rounded-l-lg justify-items-left lg:justify-items-left">
                            {{--                <div> --}}
                            {{--                    <img src="{{ asset('storage/icons/icon-maps.svg') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu"> --}}
                            {{--                </div> --}}
                            <div>
                                <div class="flex-inline">
                                    {{ $hotel }}
                                    {{-- <select name="" id="maps"
                                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                    <option value="">Dari</option>
                                    <option value="">1</option>
                                    <option value="">1</option>
                                </select> --}}
                                </div>
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-center grid-cols-[80%_20%]">
                            <div class="place-self-start">
                                <input class="!ml-[4px] lg:ml-0 md:!ml-[0rem] md:!w-[31rem] lg:!ml-[2rem] lg:!w-[9rem]"
                                    id="datepickerCheckin" placeholder="Check-in" />
                            </div>
                            <div class="md:hidden lg:hidden">
                                <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                    class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-center grid-cols-[80%_20%]">
                            <div class="place-self-start">
                                <input class="!ml-[4px] lg:ml-0 md:!ml-[0rem] md:!w-[31rem] lg:!ml-[2rem] lg:!w-[9rem]"
                                    id="datepickerCheckout" placeholder="Check-out" />
                            </div>
                            <div class="md:hidden lg:hidden">
                                <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                    class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-left lg:justify-items-left">
                            <div>
                                <div class="flex-inline">
                                    <select name="" id=""
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                        <option value="">Jumlah Traveller</option>
                                        <option value="">1 Orang</option>
                                        <option value="">Lebih dari 1 Orang</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div
                            class="flex justify-center w-full p-2 text-white md:rounded-r-lg lg:rounded-r-lg bg-kamtuu-primary">
                            <button>
                                <div>
                                    <div class="flex-inline">
                                        <h1 class="uppercase">cari</h1>
                                    </div>
                                </div>
                            </button>
                        </div>
                    </div>
                </form>

                {{-- XSTAY --}}
                <form action="{{ route('xstay.search') }}" method="POST">
                    @csrf
                    <div x-show="tab === 6"
                        class="z-0 grid grid-cols-1 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-6 md:grid-cols-1 bg-kamtuu-second justify-items-center divide-kamtuu-second lg:divide-x">
                        <div class="grid w-full p-2 bg-white lg:rounded-l-lg justify-items-left lg:justify-items-left">
                            {{--                <div> --}}
                            {{--                    <img src="{{ asset('storage/icons/icon-maps.svg') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu"> --}}
                            {{--                </div> --}}
                            <div>
                                <div class="flex-inline">
                                    {{ $xstay }}
                                    {{-- <select name="" id="maps"
                                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                    <option value="">Dari</option>
                                    <option value="">1</option>
                                    <option value="">1</option>
                                </select> --}}
                                </div>
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-center grid-cols-[80%_20%]">
                        {{-- lg:place-self-center md:place-self-center place-self-start --}}
                                <div class="lg:place-self-center md:place-self-center place-self-start">
                                    <div class="ml-[11px] lg:!ml-[9px] md:!ml-[9px] md:!w-[31rem] md:!ml-[6rem] lg:!ml-[2rem] lg:!w-[9rem]">
                                        <input class="lg:ml-0 md:!ml-[6rem] md:!w-[31rem] lg:!ml-[2rem] lg:!w-[9rem]"
                                            id="datepickerCheckinXstay" placeholder="Check-in" />
                                    </div>
                                </div>
                                <div class="md:hidden lg:hidden">
                                    <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                        class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                </div>
                            
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-center grid-cols-[80%_20%]">
                            <div class="lg:place-self-center md:place-self-center place-self-start">
                                <div class="ml-[111px] lg:!ml-[9px] !ml-[9px] md:!w-[31rem] md:!ml-[6rem]  lg:!ml-[2rem] lg:!w-[9rem]">
                                    <input class="lg:ml-0 md:!ml-[6rem] md:!w-[31rem] lg:!ml-[2rem] lg:!w-[9rem]"
                                        id="datepickerCheckoutXstay" placeholder="Check-out" />
                                </div>
                            </div>
                            <div class="md:hidden lg:hidden">
                                <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                    class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-left lg:justify-items-left">
                            {{--                <div> --}}
                            {{--                    <img src="{{ asset('storage/icons/icon-maps.svg') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu"> --}}
                            {{--                </div> --}}
                            <div>
                                <div class="flex-inline">
                                    <select name="" id="maps"
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                        <option value="">Pilih Tamu</option>
                                        <option value="">1 Orang</option>
                                        <option value="">Lebih dari 1 Orang</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div
                            class="flex justify-center w-full p-2 text-white md:rounded-r-lg lg:rounded-r-lg bg-kamtuu-primary">
                            <button>
                                <div>
                                    <div class="flex-inline">
                                        <h1 class="uppercase">cari</h1>
                                    </div>
                                </div>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
    
</div>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js" integrity="sha512-LsnSViqQyaXpD4mBBdRYeP6sRwJiJveh2ZIbW41EBrNmKxgr/LFZIiWT6yr+nycvhvauz8c2nYMhrP80YhG7Cw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css" integrity="sha512-34s5cpvaNG3BknEWSuOncX28vz97bRI59UnVtEEpFX536A7BtZSJHsDyFoCl8S7Dt2TPzcrCEoHBGeM4SUBDBw==" crossorigin="anonymous" referrerpolicy="no-referrer" /> --}}
    
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script>
    $('#datepickerSearch').datepicker();
</script>
<script>
    $('#datepickerTransfer').datepicker();
</script>
<script>
    $('#datepickerStartFromRental').datepicker();
</script>
<script>
    $('#datepickerEndFromRental').datepicker();
</script>
<script>
    $('#datepickerCheckinXstay').datepicker();
</script>

<script>
    $('#datepickerCheckoutXstay').datepicker();
</script>
<script>
    $('#datepickerCheckout').datepicker();
    $('#datepickerCheckin').datepicker();
</script>
<script>
    $('#timepickerTransfer').timepicker();
    $('#timepickerPickup').timepicker();
    $('#timepickerPickupEnd').timepicker();
</script>
<script>
    $(".gj-textbox-md").css({"border-bottom":"none"})
    /*
    $('#tourSearch').on("click",function(){
        //console.log($("#tur-from-select2"));
        $.ajax({
            type:'GET',
            url:"{{route('master.titik')}}",
            cache:false,
            success:function(msg){
                //console.log(msg)
                $("#tur-from-select2").html(msg)
                $("#tur-to-select2").html(msg)
            }
        })
    });
    */
    
    $('#transferSearch').on("click",function(){
        //$("rental_serch_from").html();
        $.ajax({
            type:'GET',
            url:"{{route('master.titik')}}",
            cache:false,
            success:function(msg){
                
                $("#transfer_serch_from").html(msg)
                $("#transfer_serch_to").html(msg)
            
            }
        })
    })
    
    $("#rentalSearch").on("click",function(){
        $.ajax({
            type:'GET',
            url:"{{route('master.titik')}}",
            cache:false,
            success:function(msg){

                $("#rental_serch_from").html(msg)

            }
        })
    })

    $("#activitySearch").on("click",function(){
        $.ajax({
            type:'GET',
            url:"{{route('master.titik')}}",
            cache:false,
            success:function(msg){
 
                $("#activity_search_from").html(msg)
            }
        })
    })

    $("#hotelSearch").on("click",function(){
        $.ajax({
            type:'GET',
            url:"{{route('master.titik')}}",
            cache:false,
            success:function(msg){
                
                $("#hotel_search_from").html(msg)
            }
        })
    })
    

    $("#xstaySearch").on("click",function(){
        $.ajax({
            type:'GET',
            url:"{{route('master.titik')}}",
            cache:false,
            success:function(msg){
                
                $("#xstay_search_from").html(msg)
            }
        })
    })
    
    function getSearch(){
            return{
                list_search:[],
                tab: 1,
                index:0,
                open: false,
                url:"{{route('master.titik')}}",
                getData(product) {
                     console.log(product)    
                    if(this.list_search.length >0){
                        this.list_search=[]
                    }

                    if(product== 'transfer'){
                        //let select_transfer_from = $("#transfer_serch_from").html("<option selected>Dari</option>"); 
                        //let select_transfer_to = $("#transfer_serch_to").html("<option selected>Dari</option>"); 
                    }
                    /*
                    fetch(this.url)
                    .then(resp => resp.json())
                    .then(data=>{
                        let obj = data.data

                        Object.keys(obj).forEach(key=>{
                            //console.log(obj[key])
                            if(product== 'transfer'){
                                console.log(obj[key])
                               //$("#transfer_serch_from").append("<option value="+obj[key].id+">"+obj[key].name+"<option>")
                               // $("#transfer_serch_to").append("<option value="+obj[key].id+">"+obj[key].name+"<option>")
                            }
                            //this.list_search.push(obj[key])
                           // this.list_search.push(obj[key])
                        })

                        console.log(this.list_search)
                    })
                    */
                }
            }
    }
</script>

<script>
    var display = $("#formSearch").attr("style");
    $("#btnFormSearch").on("click",function(){
        
        //console.log(display)

    })
    
    console.log(display)

</script>