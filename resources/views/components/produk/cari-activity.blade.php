<div class="p-3 md:p-0 lg:p-0">
    <div class="rounded-lg bg-kamtuu-primary">
        {{-- Search --}}
        <form action="{{ route('activity.search') }}" method="POST">
            @csrf
            <div
                class="grid grid-cols-1 md:grid-cols-[20%_80%] lg:grid-cols-[20%_80%] p-10 divide-y-2 lg:divide-x-2 md:divide-x-2 divide-kamtuu-primary">
                <div class="relative">
                    {{-- <span
                        class="absolute inset-y-0 right-0 grid w-10 text-gray-400 text-gray-500 pointer-events-none place-content-center">
                        <img class="mr-2" src="{{ asset('storage/icons/icon-maps.svg') }}" alt="location" width="12px">
                    </span> --}}
                    <label for="FormSearch" class="sr-only"> </label>
                    {{-- <input type="email" id="UserEmail" placeholder="Dari"
                        class="w-full lg:rounded-l-lg md:rounded-l-lg sm:text-sm focus:ring-0 focus:border-kamtuu-primary" />
                    --}}
                    <div
                        class="w-full lg:rounded-l-lg md:rounded-l-lg sm:text-sm focus:ring-0 focus:border-kamtuu-primary">
                        {{ $activity }}
                        {{-- <select name="provinsi" id="provinsi"
                            class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 appearance-none ring:bg-kamtuu-primary sm:text-sm sm:leading-5">
                            <option selected disabled>Pilih Provinsi</option>
                            @foreach (json_decode($provinsi) as $provinsi_data)
                            <option value="{{ $provinsi_data->id }}">{{ $provinsi_data->name }}
                            </option>
                            @endforeach
                        </select> --}}
                    </div>
                </div>
                <div class="relative -mt-[2px]">
                    <label for="FormSearch" class="sr-only"> </label>
                    <input type="text" id="FormSearch" placeholder="Cari Activity" name="activity_name"
                        class="w-full sm:text-sm focus:ring-0 focus:border-kamtuu-primary" />
                    <button
                        class="absolute inset-y-0 right-0 grid w-10 text-gray-400 text-gray-500 place-content-center">
                        <img class="mr-2" src="{{ asset('storage/icons/search-ic.png') }}" alt="location"
                            width="12px">
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
