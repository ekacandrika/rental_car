<div class="p-3 md:p-0 lg:p-0">
    <div class="rounded-lg bg-kamtuu-primary">
        {{-- Search --}}
        <form action="{{ route('xstay.search') }}" method="POST">
            @csrf
            <div
                class="grid grid-cols-1 p-10 divide-y-2 md:grid-cols-1 lg:grid-cols-6 lg:divide-x-2 md:divide-x-2 divide-kamtuu-primary">
                {{-- <div class="relative">
                <span
                    class="absolute inset-y-0 right-0 grid w-10 text-gray-400 text-gray-500 pointer-events-none place-content-center">
                    <img class="mr-2" src="{{ asset('storage/icons/icon-maps.svg') }}" alt="location" width="12px">
                </span>
                <label for="FormSearch" class="sr-only"> </label>
                <input type="email" id="UserEmail" placeholder="Dari"
                    class="w-full lg:rounded-l-lg md:rounded-l-lg sm:text-sm focus:ring-0 focus:border-kamtuu-primary" />
            </div> --}}
                <div
                    class="grid w-full bg-white md:rounded-l-lg lg:rounded-l-lg justify-items-center md:grid-cols-[20%_80%] lg:grid-cols-[20%_80%] grid-cols-[20%_80%]">
                    <div>
                        <img src="{{ asset('storage/icons/icon-maps.svg') }}" class="w-[15px] h-[15px] mt-[0.6rem] mr-1 "
                            alt="polygon 3" title="Kamtuu">
                    </div>
                    <div>
                        <div class="flex-inline">
                            {{ $slot }}
                            {{-- <select name="" id="" style="background-image: none;"
                            class="border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7 mt-2">
                            <option value="">Dari</option>
                            <option value="">1</option>
                            <option value="">1</option>
                        </select> --}}
                        </div>
                    </div>
                </div>
                <div
                    class="grid w-full bg-white justify-items-center grid-cols-[80%_20%] border border-gray-300 h-[39.5px]">
                    <div class="ml-6 place-self-center">
                        <input class="focus:ring-0 focus:border-kamtuu-primary" id="datepickerProdukXstayCheckin"
                            placeholder="Check-in" />
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                            class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>
                <div
                    class="grid w-full bg-white justify-items-center grid-cols-[80%_20%] border border-gray-300 h-[39.5px]">
                    <div class="ml-6 place-self-center">
                        <input class="focus:ring-0 focus:border-kamtuu-primary" id="datepickerProdukXstayCheckout"
                            placeholder="Check-out" />
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                            class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>
                <div class="relative">
                    <label for="FormSearch" class="sr-only">Cek</label>
                    <select class="w-full h-full focus:ring-0 focus:border-kamtuu-primary" name=""
                        id="FormSearch">
                        <option value="">Jumlah Orang</option>
                        <option value="">1 Orang</option>
                        <option value="">Lebih dari 1 Orang</option>
                    </select>
                </div>
                <div class="relative grid justify-items-center ">
                    <label for="FormSearch" class="sr-only"> </label>
                    <button class="w-full h-full pt-2 bg-kamtuu-third lg:rounded-r-lg md:rounded-r-lg">
                        <p>Cari</p>
                        <span
                            class="absolute inset-y-0 right-0 grid w-10 text-gray-400 text-gray-500 pointer-events-none place-content-center">
                            <img class="mr-2" src="{{ asset('storage/icons/search-ic.png') }}" alt="location"
                                width="12px">
                        </span>
                    </button>
                </div>


            </div>
        </form>

    </div>
</div>
<script>
    $('#datepickerProdukXstayCheckin').datepicker();
</script>
<script>
    $('#datepickerProdukXstayCheckout').datepicker();
</script>
