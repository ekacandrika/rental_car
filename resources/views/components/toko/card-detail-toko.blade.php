<style>
    .parent-tooltip:hover .tooltip {
        display: block;
    }
</style>
<div class="w-full px-4 py-6" style="width:271px;">
    <div class="border border-[#9E3D64] rounded-lg shadrop-shadow bg-white overflow-hidden" style="max-height:631px;">
        @php
            $thumbnail = null;
            if(isset($act->thumbnail)){
                $thumbnail = $act->thumbnail;
            }
            elseif(isset(json_decode($act->gallery)[0]->gallery)){
                $thumbnail = json_decode($act->gallery)[0]->gallery;
            }else{
            $thumbnail = 'https://via.placeholder.com/450x450'; 
            }
        @endphp
        <div class="bg-cover h-56 p-4" style="background-image:url({{isset($thumbnail) ? asset($thumbnail):'https://via.placeholder.com/540x540'}})"></div>
        <div class="p-4 border-t-2 border-[#9E3D64] overflow-x-auto" style="width: 271px;">
            @php
                $type = str_replace('tour','tur',$act->product->type);
                $url  = $type.'.show';
            @endphp
            <a href="{{route($url,$act->product->slug)}}">
            @php
               $product_name =  $act->product->product_name;
               $lenght_word  = strlen($product_name);
               $word_cuter   = $lenght_word-20;
               if($lenght_word > 16){
                    $product_name = substr($product_name,0,-$word_cuter);

                    $product_name =$product_name.'...';
               }

            @endphp
                <p class="parent-tooltip font-bold text-red-600 group tracking-wide uppercase truncate" id="parent_tooltip">{{$product_name}}
                    <span class="tooltip border rounded-lg bg-kamtuu-primary px-3 py-2 hidden absolute top-5 text-sm text-white left-6 group-hover:block z-999">{{$act->product->product_name}}</span>
                </p>
            </a> 
        </div>
        <div class="p-4 text-gray-700 border-t border-gray-300" syle="height:195;">
            <div class="overflow-y-auto" style="height:87px;">
                <div class="items-center flex-1 py-2">
                    <p class="text-[14px] text-[#333333]">Lokasi</p>
                        <div class="grid space-y-2">
                            @if($act->regency_id)
                                @php
                                    $lokasi = App\Models\detailObjek::where('kabupaten_id',$act->regency_id)->first();                                                             
                                    $nama_lokasi =  App\Models\Regency::where('id',$act->regency_id)->first()->name; 
                                    $url_lokasi = '#';
                                    $slug ='';
                                    if($lokasi){
                                        $url_lokasi = 'kabupatenShow';
                                        $slug = $lokasi->slug;
                                    }
                                @endphp    
                                <a href="{{isset($lokasi) ? route($url_lokasi,$slug):$url_lokasi}}">
                                    <span class="bg-[#23AEC1] py-[2px] px-[5px] rounded-sm">{{$nama_lokasi}}</span>
                                <a>
                            @elseif ($act->tag_location_1 !=null || $act->tag_location_2 || $act->tag_location_3 || $act->tag_location_4)
                                @php
                                    $tag_1 = $act->tag_location_1;
                                    $tag_2 = $act->tag_location_2;
                                    $tag_3 = $act->tag_location_3;
                                    $tag_4 = $act->tag_location_4;
                                    $pulau = App\Models\detailObjek::where('pulau_id',$tag_1)->first();
                                    $provisi = App\Models\detailObjek::where('provinsi_id',$tag_2)->first();
                                    $kabupaten= App\Models\detailObjek::where('kabupaten_id',$tag_3)->first();
                                    $wisata = App\Models\detailWisata::where('id',$tag_4)->first();

                                @endphp 
                                <a href="#">
                                    <span class="bg-[#23AEC1] py-[2px] px-[5px] rounded-sm">Lokasi</span>
                                <a>
                            @else   
                                <a href="#">
                                    <span class="bg-[#23AEC1] py-[2px] px-[5px] rounded-sm">-</span>
                                <a> 
                            @endif
                        </div>
                </div>
                <div class="items-center flex-1 py-2">
                    <p class="truncare text-[14px] text-[#333333]">
                        <a href="#">
                            <div class="bg-[#51B449] w-max-[225px] rounded-sm text-white truncate">
                                Ringkasan
                            </div>
                        </a>
                    </p>
                </div>
            </div>
        </div>
        <div class="px-4 pt-3 pb-4 bg-2 bg-white" style="display: none;"></div>
        <div class="px-4 pt-3 pb-4 bg-2 bg-white w-[201px]">
            <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
            <div class="grid items-center justify-end grid-cols-2 pt-2">
                <div>
                    @php
                    $harga = [];
                    $dewasa_terendah = 0;
                    if($act->tipe_tur == 'Open Trip'){
                        $harga = App\Models\Price::where('paket_id',$act->paket_id)->get();
                    }
                    if(!isset($harga[0])){
                        $harga = App\Models\Price::where('id', $act->harga_id)->get();
                    }
                    @endphp
                    @if ($act->type == 'Tur Private')
                        <p class="text-[16px] text-[#333333]">Mulai dari
                        <br>
                        <span class="font-bold">{{isset($act->harga->dewasa_residen) ? number_format(product->harga->dewasa_residen) : ''}}</span>
                    </p>
                    @endif
                    @if ($act->type == 'Open Trip')
                        <p class="text-[16px] text-[#333333]">Mulai dari
                        <br>
                        <span class="font-bold">{{ number_format($harga[0]->dewasa_residen)}}</span>
                    </p>
                    @endif
                    @php
                        $rating = App\Models\reviewPost::where('product_id',$act->product->id)->sum('star_rating');
                        $review_count = App\Models\reviewPost::where('product_id',$act->product->id)->get();
                    @endphp
                    <div class="inline-flex">
                        <img class="w-[15px] h-[15px] mt-[0.2rem] mr-1" src="{{asset('storage/icons/Star 2.png')}}">
                        <p class="-mr-2 w-[411px]">{{count($review_count) == 0 ? 0 :$rating/count($review_count)}}&ThinSpace;{{ count($review_count) }} review</p>
                    </div>
                </div>
                <div class="grid justify-items-end">
                    <a href="#">
                        <div class="grid w-12 h-12 mr-3 bg-center bg-cover rounded-full" style="background-image:url({{'https://via.placeholder.com/50x50'}})"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var tooltip = document.querySelectorAll('.tooltip');

    document.addEventListener('mousemove',fn,false);

    function fn(e) {
    for (var i=tooltip.length; i--;) {
        tooltip[i].style.left = (12+e.pageX) + 'px';
        tooltip[i].style.top = (20+e.pageY) + 'px';
    }  
}
</script>