{{--                                        flash message success--}}
<div x-data="{ showMessage: true }" x-show="showMessage"
     class="flex justify-end"
     x-init="setTimeout(() => showMessage = false, 3000)">
    @if (session()->has('success'))
        <div
            class="flex items-center justify-between max-w-xl p-4 bg-white border rounded-md shadow-sm">
            <div class="flex items-center">
                <svg xmlns="http://www.w3.org/2000/svg"
                     class="w-8 h-8 text-green-500" viewBox="0 0 20 20"
                     fill="currentColor">
                    <path fill-rule="evenodd"
                          d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                          clip-rule="evenodd"/>
                </svg>
                <p class="ml-3 text-sm font-bold text-green-600">{{ session()->get('success') }}</p>
            </div>
            <span @click="showMessage = false"
                  class="inline-flex items-center cursor-pointer">
            <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 text-gray-600" fill="none" viewBox="0 0 24 24"
                 stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
            </svg>
        </span>
        </div>
    @endif
</div>

{{--                                        flash message warning--}}
<div x-data="{ showMessage: true }"
     x-show="showMessage"
     x-transition:enter="transition ease-out duration-300"
     x-transition:enter-start="opacity-0 scale-90"
     x-transition:enter-end="opacity-100 scale-100"
     x-transition:leave="transition ease-in duration-300"
     x-transition:leave-start="opacity-100 scale-100"
     x-transition:leave-end="opacity-0 scale-90"
     class="flex justify-end"
     x-init="setTimeout(() => showMessage = false, 3000)">
    @if (session()->has('warning'))
        <div
            class="flex items-center justify-between max-w-xl p-4 bg-white border rounded-md shadow-sm">
            <div class="flex items-center">
                <svg xmlns="http://www.w3.org/2000/svg"
                     class="w-8 h-8 text-green-500" viewBox="0 0 20 20"
                     fill="currentColor">
                    <path fill-rule="evenodd"
                          d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                          clip-rule="evenodd"/>
                </svg>
                <p class="ml-3 text-sm font-bold text-green-600">{{ session()->get('warning') }}</p>
            </div>
            <span @click="showMessage = false"
                  class="inline-flex items-center cursor-pointer">
            <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 text-gray-600" fill="none" viewBox="0 0 24 24"
                 stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
            </svg>
        </span>
        </div>
    @endif
</div>

{{--                                        flash message danger--}}
<div  x-data="{ showMessage: true }"
      x-show="showMessage"
      x-transition:enter="transition ease-out duration-300"
      x-transition:enter-start="opacity-0 scale-90"
      x-transition:enter-end="opacity-100 scale-100"
      x-transition:leave="transition ease-in duration-300"
      x-transition:leave-start="opacity-100 scale-100"
      x-transition:leave-end="opacity-0 scale-90"
      class="flex justify-end"
      x-init="setTimeout(() => showMessage = false, 3000)"
>
    @if (session()->has('danger'))
        <div
            class="flex items-center justify-between max-w-xl p-4 bg-white border border-red-300 rounded-md shadow-sm">
            <div class="flex items-center">
                {{--                                                        <svg xmlns="http://www.w3.org/2000/svg"--}}
                {{--                                                             class="w-8 h-8 text-green-500" viewBox="0 0 20 20"--}}
                {{--                                                             fill="currentColor">--}}
                {{--                                                            <path fill-rule="evenodd"--}}
                {{--                                                                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"--}}
                {{--                                                                  clip-rule="evenodd"/>--}}
                {{--                                                        </svg>--}}
                <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8 bg-red-500 rounded-full"
                     viewBox="0 0 24 24" fill="none" stroke="#ffffff"
                     stroke-width="2" stroke-linecap="round"
                     stroke-linejoin="round">
                    <line x1="18" y1="6" x2="6" y2="18"></line>
                    <line x1="6" y1="6" x2="18" y2="18"></line>
                </svg>
                <p class="ml-3 text-sm font-bold text-red-600">{{ session()->get('danger') }}</p>
            </div>
            {{--                                                    <span @click="showMessage = false"--}}
            {{--                                                          class="inline-flex items-center cursor-pointer">--}}
            {{--            <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 text-red-700" fill="none" viewBox="0 0 24 24"--}}
            {{--                 stroke="currentColor">--}}
            {{--                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>--}}
            {{--            </svg>--}}
            {{--        </span>--}}
        </div>
    @endif
</div>
