<div class="flex items-center justify-center max-w-screen-sm mx-auto lg:max-w-[85rem]">

    <div class="container flex flex-col items-stretch justify-center h-full p-2 mx-auto sm:p-8" x-data="{ tab: 1 }">

        <!-- TABS -->
        <div class="z-10 flex justify-start -space-x-px">
            <div class="grid grid-cols-3 grid-rows-1 md:flex lg:flex">

                <a href="!#0" @click.prevent="tab = 1" :class="{
                        'cursor-default border-b-0 bg-kamtuu-second': tab ===
                            1,
                        'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                            1
                    }" {{--
                    class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 rounded-tl-lg shadow-none outline-none"
                    --}}>
                </a>
            </div>
        </div>


        {{-- Hotel Search Global--}}
        @if ($slot->isNotEmpty())
        <form action="{{ route('hotel.search') }}" method="POST">
            @csrf
            <div x-show="tab === 1"
                class="bg-[#9E3D64] z-0 grid grid-cols-1 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none md:divide-x lg:grid-cols-5 justify-items-center divide-kamtuu-second lg:divide-x pl-[15%]" style="padding-left:2%">
                <div
                    class="grid w-full p-2 bg-white md:rounded-l-lg lg:rounded-l-lg justify-items-center md:grid-cols-[20%_80%] lg:grid-cols-[20%_80%] grid-cols-[20%_80%]">
                    <div>
                        <img src="{{ asset('storage/icons/icon-maps.svg') }}"
                            class="w-[15px] h-[15px] mt-[0.6rem] mr-1 " alt="polygon 3" title="Kamtuu">
                    </div>
                    <div>
                        <div class="flex-inline">
                            {{ $slot }}
                            {{-- <select name="" id="" style="background-image: none;"
                                class="border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                <option value="">Destinasi</option>
                                <option value="">1</option>
                                <option value="">1</option>
                            </select> --}}
                        </div>
                    </div>
                </div>
                <div class="grid w-full p-2 bg-white justify-items-center grid-cols-[80%_20%]" style="grid-template-columns: 96% 20%;">
                    <div class="place-self-center">
                        <input id="datepickerCheckin" placeholder="Check-in" />
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                            class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>

                <div class="grid w-full p-2 bg-white justify-items-center grid-cols-[80%_20%]" style="grid-template-columns: 96% 20%;">
                    <div class="place-self-center">
                        <input id="datepickerCheckout" placeholder="Check-out" />
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                            class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>

                <div class="grid w-full grid-cols-[80%_20%]  p-2 bg-white justify-items-center">
                    <div>
                        <div class="flex-inline">
                            {{-- modal informasi agensi --}}
                            <section class="flex items-center justify-center">

                                <button type="button"
                                    class="ml-8 px-4 py-2 text-sm tracking-wider text-black transition duration-300 ease-in-out w-[11rem]"
                                    x-data="{ id: 'modal-example' }" x-on:click="$dispatch('modal-overlay',{id})">
                                    Jumlah Traveller
                                </button>

                                <div class="fixed inset-0 z-10 flex flex-col items-center justify-end overflow-y-auto bg-gray-600 bg-opacity-50 sm:justify-start"
                                    x-data="{ modal: false }" x-show="modal"
                                    x-on:modal-overlay.window="if ($event.detail.id == 'modal-example') modal=true"
                                    x-transition:enter="transition ease-out duration-1000"
                                    x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100"
                                    x-transition:leave="transition ease-in duration-500"
                                    x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0">
                                    <div class="w-full px-2 py-20 transition-all transform sm:max-w-2xl" role="dialog"
                                        aria-modal="true" aria-labelledby="modal-headline" x-show="modal"
                                        x-transition:enter="transition ease-out duration-300"
                                        x-transition:enter-start="opacity-0 -translate-y-4 sm:translate-y-4"
                                        x-transition:enter-end="opacity-100 translate-y-0"
                                        x-transition:leave="transition ease-in duration-300"
                                        x-transition:leave-start="opacity-100 translate-y-0"
                                        x-transition:leave-end="opacity-0 -translate-y-4 sm:translate-y-4"
                                        x-on:click.away="modal=false">

                                        <div class="bg-white rounded-sm shadow-sm p-10">
                                            <p class="text-base font-semibold">Traveller</p>
                                            <div x-data="handler()">
                                                <template x-for="(field, index) in fields" :key="index">
                                                    <div x-model="field.txt1" name="txt1[]" class="">
                                                        <div class="grid grid-cols-2">
                                                            <div class="grid col-span-2">
                                                                <p class="">Kamar <span x-text="index + 1"></span>
                                                                </p>
                                                            </div>
                                                            <div class="grid col-span-2">
                                                                <div class="mt-4">
                                                                    <p class="">Dewasa</p>
                                                                </div>
                                                                <div class="">
                                                                    <div x-data="counter()">
                                                                        <div class="flex items-center justify-end pl-5">
                                                                            <button type="button"
                                                                                x-on:click="decrement()"
                                                                                class="px-4 py-2 text-white rounded-full bg-kamtuu-second hover:bg-kamtuu-third"
                                                                                style="background-color: rgb(35 174 193 / var(--tw-bg-opacity))">-</button>
                                                                            <span class="m-5" x-text="count">0</span>
                                                                            <button type="button"
                                                                                x-on:click="increment()"
                                                                                class="px-4 py-2 text-white rounded-full bg-kamtuu-primary hover:bg-kamtuu-third"
                                                                                style="background-color: rgb(158 61 100 / var(--tw-bg-opacity))">+</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="grid col-span-2">
                                                                <div class="mt-4">
                                                                    <p class="">Anak-anak</p>
                                                                </div>
                                                                <div class="">
                                                                    <div x-data="counter()">
                                                                        <div class="flex items-center justify-end pl-5">
                                                                            <button type="button"
                                                                                x-on:click="decrement()"
                                                                                class="px-4 py-2 text-white rounded-full bg-kamtuu-second hover:bg-kamtuu-third"
                                                                                style="background-color: rgb(35 174 193 / var(--tw-bg-opacity))">-</button>
                                                                            <span class="m-5" x-text="count">0</span>
                                                                            <button type="button"
                                                                                x-on:click="increment()"
                                                                                class="px-4 py-2 text-white rounded-full bg-kamtuu-primary hover:bg-kamtuu-third"
                                                                                style="background-color: rgb(158 61 100 / var(--tw-bg-opacity))">+</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr class="h-px my-8 bg-gray-200 border-0 dark:bg-gray-700">
                                                        <div class="grid justify-items-end">
                                                            <button type="button" class=" text-kamtuu-primary"
                                                                @click="removeField(index)">Hapus Kamar</button>
                                                        </div>
                                                </template>
                                                <div class="grid justify-items-end">
                                                    <button type="button" class="text-kamtuu-primary"
                                                        @click="addNewField()">Tambah Kamar</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </section>
                        </div>
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>

                <div class="flex justify-center w-full p-2 text-white md:rounded-r-lg lg:rounded-r-lg bg-[#FFB800]">
                    <button>
                        <div>
                            <div class="flex-inline">
                                <div class="grid w-full p-2 justify-items-center grid-cols-[80%_20%]">
                                    <img src="{{ asset('storage') }}/icons/magnifying-glass-solid (1) 1.svg"
                                        class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                    <div class="place-self-center">
                                        <h1 class="uppercase">&nbsp;cari</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </button>
                </div>
            </div>
        </form>
        {{-- Hotel Search at specific room--}}
        @else
        <form action="{{ route('hotel.show', $slug) }}" method="GET">
            <div x-show="tab === 1"
                class="bg-[#9E3D64] z-0 grid grid-cols-1 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none md:divide-x lg:grid-cols-5 justify-items-center divide-kamtuu-second lg:divide-x pl-[15%]">
                <div class="grid w-full rounded-l-md p-2 bg-white justify-items-center grid-cols-[80%_20%]">
                    <div class="place-self-center">
                        <input id="datepickerCheckin" name="checkIn" placeholder="Check-in" readonly />
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                            class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>

                <div class="grid w-full p-2 bg-white justify-items-center grid-cols-[80%_20%]">
                    <div class="place-self-center">
                        <input id="datepickerCheckout" name="checkOut" placeholder="Check-out" readonly />
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                            class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>

                <div class="grid w-full grid-cols-[80%_20%]  p-2 bg-white justify-items-center">
                    <div>
                        <div class="flex-inline" x-data="handler()">
                            {{-- modal informasi agensi --}}
                            <section class="flex items-center justify-center">

                                <template x-if="total_traveller <= 0">
                                    <button type="button"
                                        class="ml-8 px-4 py-2 text-sm tracking-wider text-black transition duration-300 ease-in-out w-[11rem]"
                                        x-data="{ id: 'modal-example' }" x-on:click="$dispatch('modal-overlay',{id})">
                                        Jumlah Traveller
                                    </button>
                                </template>
                                <template x-if="total_traveller > 0">
                                    <button type="button"
                                        class="ml-8 px-4 py-2 text-sm tracking-wider text-black transition duration-300 ease-in-out w-[11rem]"
                                        x-data="{ id: 'modal-example' }" x-on:click="$dispatch('modal-overlay',{id})"
                                        x-text="fields.length + ' kamar ' + total_traveller + ' traveller'">
                                    </button>
                                </template>


                                <div class="fixed inset-0 z-10 flex flex-col items-center justify-end overflow-y-auto bg-gray-600 bg-opacity-50 sm:justify-start"
                                    x-data="{ modal: false }" x-show="modal"
                                    x-on:modal-overlay.window="if ($event.detail.id == 'modal-example') modal=true"
                                    x-transition:enter="transition ease-out duration-1000"
                                    x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100"
                                    x-transition:leave="transition ease-in duration-500"
                                    x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0">
                                    <div class="w-full px-2 py-20 transition-all transform sm:max-w-2xl" role="dialog"
                                        aria-modal="true" aria-labelledby="modal-headline" x-show="modal"
                                        x-transition:enter="transition ease-out duration-300"
                                        x-transition:enter-start="opacity-0 -translate-y-4 sm:translate-y-4"
                                        x-transition:enter-end="opacity-100 translate-y-0"
                                        x-transition:leave="transition ease-in duration-300"
                                        x-transition:leave-start="opacity-100 translate-y-0"
                                        x-transition:leave-end="opacity-0 -translate-y-4 sm:translate-y-4"
                                        x-on:click.away="modal=false">

                                        <div class="bg-white rounded-sm shadow-sm p-10">
                                            <p class="text-base font-semibold">Traveller</p>
                                            <div>
                                                <template x-for="(field, index) in fields" :key="index">
                                                    <div x-data="{ sum: field.dewasa + field.anak }"
                                                        x-init="$watch('field.dewasa; field.anak', () => { sum = field.dewasa + field.anak})">
                                                        <div class="grid grid-cols-2">
                                                            <div class="grid col-span-2">
                                                                <p class="">Kamar <span x-text="index + 1"></span>
                                                                </p>
                                                            </div>
                                                            <div class="grid col-span-2">
                                                                <div class="mt-4">
                                                                    <p class="">Dewasa</p>
                                                                </div>
                                                                <div class="">
                                                                    <div>
                                                                        <div class="flex items-center justify-end pl-5">
                                                                            <button type="button"
                                                                                x-on:click="decrementDewasa(index)"
                                                                                class="px-4 py-2 text-white rounded-full bg-kamtuu-second hover:bg-kamtuu-third"
                                                                                style="background-color: rgb(35 174 193 / var(--tw-bg-opacity))">-</button>
                                                                            <p class="m-5" x-text="field.dewasa"></p>
                                                                            <button type="button"
                                                                                x-on:click="incrementDewasa(index)"
                                                                                class="px-4 py-2 text-white rounded-full bg-kamtuu-primary hover:bg-kamtuu-third"
                                                                                style="background-color: rgb(158 61 100 / var(--tw-bg-opacity))">+</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="grid col-span-2">
                                                                <div class="mt-4">
                                                                    <p class="">Anak-anak</p>
                                                                </div>
                                                                <div class="">
                                                                    <div>
                                                                        <div class="flex items-center justify-end pl-5">
                                                                            <button type="button"
                                                                                x-on:click="decrementAnak(index)"
                                                                                class="px-4 py-2 text-white rounded-full bg-kamtuu-second hover:bg-kamtuu-third"
                                                                                style="background-color: rgb(35 174 193 / var(--tw-bg-opacity))">-</button>
                                                                            <p class="m-5" x-text="field.anak"></p>
                                                                            <button type="button"
                                                                                x-on:click="incrementAnak(index)"
                                                                                class="px-4 py-2 text-white rounded-full bg-kamtuu-primary hover:bg-kamtuu-third"
                                                                                style="background-color: rgb(158 61 100 / var(--tw-bg-opacity))">+</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="grid grid-cols-3 gap-2">
                                                                    <template x-for="(id_anak, ids) in field.anak"
                                                                        :key="ids">
                                                                        <div>
                                                                            <label for="anak_option"
                                                                                class="text-sm px-1"
                                                                                x-text="'Umur anak '+id_anak"></label>
                                                                            <div class="border">
                                                                                <select class="border py-2 px-1 w-full"
                                                                                    :name="'anak['+index+']['+ids+']'"
                                                                                    id="anak_option">
                                                                                    <template
                                                                                        x-for="(umur, id_umur) in 18">
                                                                                        <option :value="id_umur"
                                                                                            x-text="id_umur + ' tahun'"
                                                                                            :selected="{{ json_encode($umuranak) }} != null ? id_umur == {{ json_encode($umuranak) }}[index][ids] : 0">
                                                                                        </option>
                                                                                    </template>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </template>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <hr class="h-px my-8 bg-gray-200 border-0 dark:bg-gray-700">
                                                        <template x-if="fields.length > 1">
                                                            <div class="grid justify-items-end">
                                                                <button type="button" class=" text-kamtuu-primary"
                                                                    @click="removeField(index)">Hapus Kamar</button>
                                                            </div>
                                                        </template>
                                                        <input type="hidden" :name="'kmr['+index+']'"
                                                            :value="field.dewasa+'_'+field.anak+'_'+sum">
                                                    </div>
                                                </template>
                                                <input type="hidden" name="total_kmr" :value="fields.length">
                                                <div class="grid justify-items-end">
                                                    <button type="button" class="text-kamtuu-primary"
                                                        @click="addNewField()">Tambah Kamar</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </section>
                        </div>
                    </div>
                    <div class="md:hidden lg:hidden">
                        <img src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                            class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>

                <div class="flex justify-center w-full p-2 text-white md:rounded-r-lg lg:rounded-r-lg bg-[#FFB800]">
                    <button>
                        <div>
                            <div class="flex-inline">
                                <div class="grid w-full p-2 justify-items-center grid-cols-[80%_20%]">
                                    <img src="{{ asset('storage') }}/icons/magnifying-glass-solid (1) 1.svg"
                                        class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                                    <div class="place-self-center">
                                        <h1 class="uppercase">&nbsp;cari</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </button>
                </div>
            </div>
        </form>
        @endif

    </div>
</div>
{{-- <script>
    $('#datepickerCheckin').datepicker();
</script>

<script>
    $('#datepickerCheckout').datepicker();
</script> --}}

<script>
    const tomorrowDate = (selectedDate) => {
        newDate = new Date(new Date(selectedDate).valueOf() + 1000*3600*24);
        newMonth = newDate.getMonth() + 1
        newDay = newDate.getDate()
        newTomorrow = `${ newMonth >= 10 ? newMonth : ('0' + newMonth) }/${ newDay >= 10 ? newDay : ('0' + newDay)}/${newDate.getFullYear()}`

        return newTomorrow
    }
    
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    date_checkin = "{{ $checkin ?? '' }}"
    date_checkout = "{{ $checkout ?? '' }}"
    $('#datepickerCheckin').datepicker({
        value: date_checkin,
        change: function (e) {
            today = $('#datepickerCheckin').datepicker().value();
            tomorrow = tomorrowDate(today)
            // newDate = new Date(new Date(today).valueOf() + 1000*3600*24);
            // newMonth = newDate.getMonth() + 1
            // newDay = newDate.getDate()
            // tomorrow = `${ newMonth >= 10 ? newMonth : ('0' + newMonth) }/${ newDay >= 10 ? newDay : ('0' + newDay)}/${newDate.getFullYear()}`
            $('#datepickerCheckout').datepicker().destroy();
            $('#datepickerCheckout').datepicker({
                    minDate: tomorrow
                });
        },
        minDate: today
    });

    if ((date_checkin !== '') && (date_checkout !== '')) {
        tomorrow = date_checkin === '' ? date_checkin : tomorrowDate(date_checkin)

        $('#datepickerCheckout').datepicker({
            value: "{{ $checkout ?? '' }}",
            minDate: tomorrow
        });
    }

    if ((date_checkin !== '' ) && (date_checkout === '')) {
        tomorrow = date_checkin === '' ? date_checkin : tomorrowDate(date_checkin)

        $('#datepickerCheckout').datepicker({
            minDate: tomorrow
        });
    }

    if ((date_checkin === '') && (date_checkout === '')) {
        $('#datepickerCheckout').datepicker({
            minDate: today
        });
    }
</script>
<script>
    function handler() {
        return {
            fields: {!! json_encode($queryroom) !!}.length > 0 ? {!! json_encode($queryroom) !!} : [{ dewasa: 1, anak: 0 }],
            total_traveller: {{ $totalpengunjung > 0 ? $totalpengunjung : 1 }},
            addNewField() {
                this.fields.push({
                    dewasa: 1,
                    anak: 0
                });
                this.total_traveller++;
            },
            removeField(index) {
                this.total_traveller = this.total_traveller - this.fields[index].dewasa - this.fields[index].anak;
                this.fields.splice(index, 1);
            },
            incrementDewasa(index) {
                this.fields[index].dewasa++;
                this.total_traveller++;
                console.log(this.fields)
            },
            decrementDewasa(index) {
                if (this.fields[index].dewasa <= 1) {
                    return this.fields[index].dewasa
                }

                this.fields[index].dewasa--;
                this.total_traveller--;
            },
            incrementAnak(index) {
                if (this.fields[index].anak >= this.fields[index].dewasa) {
                    return this.fields[index].anak
                }

                this.fields[index].anak++;
                this.total_traveller++;
            },
            decrementAnak(index) {
                if (this.fields[index].anak <= 0) {
                    return this.fields[index].anak
                }

                this.fields[index].anak--;
                this.total_traveller--;
            }
        }
    }
</script>



<script>
    function counter() {
        return {
            count: 0,
            increment() {
                this.count++;
            },
            decrement() {
                this.count--;
            }
        };
    }
</script>