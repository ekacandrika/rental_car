<div x-cloak x-show="show" class="fixed inset-0 z-[9999] overflow-y-auto" aria-labelledby="modal-title" role="dialog"
    aria-modal="true">
    <div class="flex items-end justify-center min-h-screen text-center md:items-center sm:block sm:p-0">
        <div class="fixed inset-0 duration-200" @click="show = false" style="background-color: rgba(0,0,0,.7);">
        </div>
        {{-- <div x-cloak @click="show = false" x-show="show"
            x-transition:enter="transition ease-out duration-300 transform" x-transition:enter-start="opacity-0"
            x-transition:enter-end="opacity-100" x-transition:leave="transition ease-in duration-200 transform"
            x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0"
            class="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-40" aria-hidden="true"></div> --}}

        <div x-cloak x-show="show" x-transition:enter="transition ease-out duration-300 transform"
            x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
            x-transition:leave="transition ease-in duration-200 transform"
            x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
            x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            class="inline-block w-full max-w-xl p-8 my-20 overflow-hidden text-left transition-all transform bg-white rounded-lg shadow-xl 2xl:max-w-2xl">
            <div class="flex items-center justify-end space-x-4">
                <button @click="show = false" class="text-gray-600 focus:outline-none hover:text-gray-700">
                    <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                </button>
            </div>
            <h1 class="text-xl font-bold text-black uppercase">Hapus {{$item->product->product_name}}</h1>
            <div class="flex py-2 gap-1 lg:gap-2">
                <div class="bg-kamtuu-primary px-2 lg:px-4 py-px lg:py-1 text-white text-sm rounded lg:rounded-full">
                    {{ $type }}
                </div>
            </div>
            <div class="text-md text-gray-700 my-5 text-center space-y-2">
                <p>Apakah Anda yakin menghapus produk <span class="font-bold">{{$item->product->product_name}}</span>?
                </p>
                <p>Produk yang sudah dihapus tidak dapat dikembalikan.</p>
            </div>
            <div class="flex justify-end space-x-2">
                <form action="{{ route('deleteListing', $item->product->id) }}" method="POST">
                    @csrf
                    @method('delete')
                    <button type="submit"
                        class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-white bg-[#D50006] rounded lg:rounded-lg">
                        Hapus
                    </button>
                </form>
                <button @click="show = false"
                    class="items-center px-px py-1 lg:py-2 w-24 lg:w-15 mt-px lg:mt-2.5 text-xs lg:text-md font-semibold text-center text-[#D50006] bg-[#white] border border-[#D50006] hover:bg-slate-100 duration-200 rounded lg:rounded-lg">
                    Batal
                </button>
            </div>
        </div>
    </div>
</div>