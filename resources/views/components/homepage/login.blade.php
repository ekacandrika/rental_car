<div class="min-h-screen md:grid md:grid-cols-2 lg:grid-cols-3">
    <div class="hidden md:block lg:col-span-2 relative overflow-hidden bg-gray-400 shadow-2xl bg-cover min-h-screen">
        {{ $image }}
    </div>

    <div class="flex items-center justify-center p-6 min-h-screen w-full">
        <div class="w-full">
            <div class="sm:mx-auto sm:w-full sm:max-w-md">

                <h2 class="mt-6 text-4xl font-extrabold text-center leading-9 text-kamtuu-primary">Welcome Back
                    {{ $nameUsr }}</h2>
                <a href="#" class="flex justify-center font-bold text-4xl">
                    <img src="{{ asset('storage/logos/logo-footer.png') }}" alt="">
                </a>
                <h2 class="mt-6 text-2xl font-extrabold text-center leading-9">Sign in to your account</h2>

                <p class="mt-2 text-sm text-center leading-5 max-w">
                    Or
                    {{-- <x-destindonesia.button-primary>@slot('button') Register @endslot
                    </x-destindonesia.button-primary> --}}
                    <a href="{{ $linkRegister }}"
                        class="font-medium transition ease-in-out duration-150 text-kamtuu-primary hover:text-kamtuu-second">
                        create a new account </a>
                </p>
            </div>

            {{-- @if (count($errors) > 0)
            <div class="bg-red-300 rounded-md p-3">
                @foreach ($errors->getMessagebag()->toArray() as $key=>$value)
                @foreach ($value as $index=>$err)
                <p>- {{ $err}}</p>
                @endforeach
                @endforeach
            </div>
            @endif --}}

            <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div>
                        <label for="email" class="block text-sm font-medium leading-5"> Email address </label>

                        <div class="mt-1 rounded-md shadow-sm">
                            <input id="email" name="email" type="email" required autofocus
                                class="appearance-none ring:bg-kamtuu-primary block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                        </div>
                    </div>

                    <div class="mt-6">
                        <label for="password" class="block text-sm font-medium text-gray-700 leading-5">
                            Password </label>

                        <div class="mt-1 rounded-md shadow-sm">
                            <input id="password" type="password" name="password" required
                                class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                        </div>
                    </div>

                    <div class="flex items-center justify-between mt-6">
                        <div class="flex items-center">
                            <input id="remember" type="checkbox"
                                class="form-checkbox w-4 h-4 transition duration-150 ease-in-out" />
                            <label for="remember" class="block ml-2 text-sm text-gray-900 leading-5"> Remember </label>
                        </div>

                        <div class="text-sm leading-5">
                            <a href="#" class="font-medium transition ease-in-out duration-150"> Forgot your
                                password? </a>
                        </div>
                    </div>

                    <div class="mt-6">
                        <span class="block w-full rounded-md shadow-sm">
                            <x-destindonesia.button-primary>
                                @slot('button')
                                Sign In
                                @endslot
                            </x-destindonesia.button-primary>
                        </span>
                    </div>
                </form>

                <hr class="border border-[#BDBDBD] my-5">

                <div class="my-5">
                    <div class="mt-16 grid space-y-4">
                        <button id="btnLoginGoogle"
                            class="group h-12 my-2 px-6 border-2 border-gray-300 rounded-full transition duration-300 hover:border-blue-400 focus:bg-blue-50 active:bg-blue-100">
                            <div class="relative flex items-center space-x-4 justify-center">
                                <img src="https://tailus.io/sources/blocks/social/preview/images/google.svg"
                                    class="absolute left-0 w-5" alt="google logo">
                                <span
                                    class="block w-max font-semibold tracking-wide text-gray-700 text-sm transition duration-300 group-hover:text-blue-600 sm:text-base">Continue
                                    with Google</span>
                            </div>
                        </button>
                        <button
                            class="group h-12 my-2 px-6 border-2 border-gray-300 rounded-full transition duration-300 hover:border-blue-400 focus:bg-blue-50 active:bg-blue-100">
                            <div class="relative flex items-center space-x-4 justify-center">
                                <img src="https://upload.wikimedia.org/wikipedia/en/0/04/Facebook_f_logo_%282021%29.svg"
                                    class="absolute left-0 w-5" alt="Facebook logo">
                                <span
                                    class="block w-max font-semibold tracking-wide text-gray-700 text-sm transition duration-300 group-hover:text-blue-600 sm:text-base">Continue
                                    with Facebook</span>
                            </div>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
let role ='{{$role}}'

$("#btnLoginGoogle").on("click",function(){
    window.location.href="/auth/google?role="+role
})
</script>