<a href="{{route('toko.show',$seller->id)}}"
    class="flex-none mx-3 my-10 max-w-[250px] grid-row-2 grid-flow-col rounded-2xl border border-[#9E3D64] bg-white shadow-lg target-card z-10">
    {{-- :id="'card'+index"> --}}
    {{-- :class="activeIndex"> --}}
    <div class="max-w-[250px] h-[200px]">
        <img class="w-full h-full border-b-2 object-cover rounded-t-2xl"
            src="{{ isset($seller->profile_photo_path) ? $seller->profile_photo_path : asset('storage/img/homepage-store-1.png') }}"
            alt="store-1">
    </div>
    <div class="py-2 px-5">
        {{-- <strong x-text="activeIndex"></strong> --}}
        <p class="font-semibold text-center">{{ $seller->first_name }}</p>
        <div class="pb-1">
            <p class="inline-block text-sm font-medium">Lisensi: </p>
            @if (isset($seller->lisensi))
            @php
            $licenses = array_filter(array_values(get_object_vars(json_decode($seller->lisensi)[0])));
            @endphp
            @forelse ($licenses as $license)
            <p class="bg-gray-200 text-xs px-1 my-auto rounded-sm mr-1 inline-block ">{{ $license }}</p>
            @empty
            <p class="bg-gray-200 text-xs px-1 my-auto rounded-sm mr-1 inline-block ">Tidak ada lisensi</p>
            @endforelse
            @endif
            @if (!isset($seller->lisensi))
            <p class="bg-gray-200 text-xs px-1 my-auto rounded-sm mr-1 inline-block ">Tidak ada lisensi</p>
            @endif

            {{-- <p class="bg-gray-200 text-xs px-1 my-auto rounded-sm mr-1 inline-block ">{{
                $seller->lisensi->personal_tur
                ?? '' }}</p> --}}
        </div>
        <div class="pb-1">
            <p class="text-sm inline-block font-medium">Layanan: </p>
            @if (isset($seller->product))
            @foreach ($seller->product as $products)
            @if ($products->total > 0)
            <p class="bg-gray-200 text-xs px-1 my-auto rounded-sm mr-1 inline-block">{{ $products->type }}
            </p>
            @endif
            @endforeach
            @else
            <p class="bg-gray-200 text-xs px-1 my-auto rounded-sm mr-1 inline-block">Tidak ada layanan</p>
            @endif
            {{-- @php
            dd($seller->product)
            @endphp
            <p class="bg-gray-200 text-xs px-1 my-auto rounded-sm mr-1 inline-block">Paket Tur</p>
            <p class="bg-gray-200 text-xs px-1 my-auto rounded-sm mr-1 inline-block">Xstay</p>
            <p class="bg-gray-200 text-xs px-1 my-auto rounded-sm mr-1 inline-block">Jemputan Bandara</p>
            <p class="bg-gray-200 text-xs px-1 my-auto rounded-sm mr-1 inline-block">Aktivitas</p>
            <p class="bg-gray-200 text-xs px-1 my-auto rounded-sm mr-1 inline-block">Rental Kendaraan</p> --}}
        </div>
        {{-- <div class="pb-1">
            <p class="inline-block text-sm font-medium">Kantor Pusat: </p>
            <p class="bg-[#23AEC1] text-white text-xs px-1 my-auto rounded-sm mr-1 inline-block">Jakarta</p>
        </div> --}}
    </div>
</a>