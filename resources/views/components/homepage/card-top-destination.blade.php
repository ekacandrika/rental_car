@props([
'text' => 'Bali',
'total' => 0,
'icon' => ''
])

<a href="#" class="rounded-lg bg-white p-5 hover:shadow-lg hover:bg-[#efaac6] duration-200">
    <div class="inline-block align-middle font-medium pb-3 md:pb-2">
        <p class="w-full mr-3 text-lg sm:text-xl xl:text-3xl inline-block align-middle">{{$text}}</p>
        <div class="inline-flex align-middle">
            <img class="w-1/6 xl:w-auto" src="{{ asset('storage/icons/star-kamtuu.svg') }}" alt="rating">
            <img class="w-1/6 xl:w-auto" src="{{ asset('storage/icons/star-kamtuu.svg') }}" alt="rating">
            <img class="w-1/6 xl:w-auto" src="{{ asset('storage/icons/star-kamtuu.svg') }}" alt="rating">
            <img class="w-1/6 xl:w-auto" src="{{ asset('storage/icons/star-kamtuu.svg') }}" alt="rating">
            <img class="w-1/6 xl:w-auto" src="{{ asset('storage/icons/star-kamtuu.svg') }}" alt="rating">
        </div>
    </div>
    {{-- <div class="inline-block md:flex items-center">
        <p class="text-[#9E3D64] text-sm sm:text-base lg:text-xl mr-3">{{ $total }}</p>
        <p class="text-gray-600 text-sm sm:text-base lg:text-xl">layanan terdaftar</p>
    </div> --}}
</a>