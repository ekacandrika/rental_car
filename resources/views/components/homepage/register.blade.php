<div class="min-h-screen md:grid md:grid-cols-2 lg:grid-cols-3">
    <div class="hidden md:block lg:col-span-2 min-h-screen relative overflow-hidden bg-gray-400 shadow-2xl">
        {{ $image }}
    </div>

    <div class="flex items-center justify-center p-6 min-h-screen w-full">
        <div class="w-full">
            <div class="sm:mx-auto sm:w-full sm:max-w-md">

                <h2 class="mt-6 text-4xl font-extrabold text-center leading-9 text-kamtuu-primary">WelKamtuu
                    {{ $nameUsr }}
                </h2>
                <a href="#" class="flex justify-center font-bold text-4xl">
                    <img src="{{ asset('storage/logos/logo-footer.png') }}" alt="">
                </a>
                <h2 class="mt-6 text-2xl font-extrabold text-center leading-9">Register to your account</h2>

                <p class="mt-2 text-sm text-center leading-5 max-w">
                    Or You have a Account?
                    {{-- <x-destindonesia.button-primary>@slot('button') Register @endslot
                    </x-destindonesia.button-primary> --}}
                    <a href="{{ $linkLogin }}"
                        class="font-medium transition ease-in-out duration-150 text-kamtuu-second"> Sign In </a>
                </p>
            </div>

            <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
                {{ $formRegister }}
                <hr class="border border-[#BDBDBD] my-5">

                <div class="my-5">
                    <div class="mt-16 grid space-y-4">
                        <button class="group h-12 my-2 px-6 border-2 border-gray-300 rounded-full transition duration-300
 hover:border-blue-400 focus:bg-blue-50 active:bg-blue-100" id="btnRegisGoogle">
                            <div class="relative flex items-center space-x-4 justify-center">
                                <img src="https://tailus.io/sources/blocks/social/preview/images/google.svg"
                                    class="absolute top-0 left-0 w-5" alt="google logo">
                                <span
                                    class="block w-max font-semibold tracking-wide text-gray-700 text-sm transition duration-300 group-hover:text-blue-600 sm:text-base">Register
                                    with Google</span>
                            </div>
                        </button>
                        <button class="group h-12 my-2 px-6 border-2 border-gray-300 rounded-full transition duration-300
                                     hover:border-blue-400 focus:bg-blue-50 active:bg-blue-100">
                            <div class="relative flex items-center space-x-4 justify-center">
                                <img src="https://upload.wikimedia.org/wikipedia/en/0/04/Facebook_f_logo_%282021%29.svg"
                                    class="absolute left-0 w-5" alt="Facebook logo">
                                <span
                                    class="block w-max font-semibold tracking-wide text-gray-700 text-sm transition duration-300 group-hover:text-blue-600 sm:text-base">Register
                                    with Facebook</span>
                            </div>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
let role ='{{$role}}'

$("#btnRegisGoogle").on("click",function(){
    window.location.href="/auth/google?role="+role
})

</script>