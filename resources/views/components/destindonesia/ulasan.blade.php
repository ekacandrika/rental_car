<div>
    <div class="container max-w-6xl p-4 ">
        <div>
            <p class="text-[32px]">Ulasan</p>
        </div>

        <div class="flex mb-4 border border-gray-400 rounded-lg shadow-lg">
          <input type="text" placeholder="Masukan Ulasan Anda..." class="flex-auto block p-4 font-medium border border-transparent rounded-lg outline-none focus:border-[#9E3D64] focus:text-green-500" />
        </div>

        <div class="grid grid-cols-2 p-5">
            <div class="flex">
            <img src="{{ asset('storage/icons/Star 1.png') }}" class="lg:w-[48px] lg:h-[48px] mt-[0.2rem] mr-1 inline-flex" alt="rating" title="Rating">
            <p class="flex"> <span class="lg:text-[50px] font-semibold ">4.0</span> <span class="lg:text-[20px] lg:pt-9"> /5.0 dari 12 ulasan</span></p>
            </div>
        </div>

        {{-- ulasan desktop --}}
        <div class="hidden lg:block md:block">
            <div class="grid grid-cols-2 divide-x rounded-lg shadow-lg justify-items-start">
                <div class="grid p-5 justify-items-center">
                    <p class="text-[18px] font-bold">Jhonny Wilson</p>
                    <p class="text-[18px] -mt-2">10 Agustus 2022</p>
                    <div class="flex p-2">
                        <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex" alt="rating" title="Rating">
                            <p class="flex pt-3"> <span class="text-[16px] font-semibold ">4.0</span> <span class="text-[16px]"> /5.0</span></p>
                    </div>
                </div>

                <div class="grid justify-start justify-items-center lg:-ml-[23rem] p-5 md:-ml-[10rem]">
                    <p class="text-[18px]">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel placeat, tenetur dolores enim quae quod voluptates dolorem explicabo repellendus ratione voluptatibus aliquam exercitationem hic adipisci sint quibusdam, eos, at quas.</p>
                </div>
            </div>
        </div>

        {{-- ulasan mobile --}}
        <div class="block lg:hidden md:hidden">
            <div class="grid grid-cols-1 divide-y rounded-lg shadow-xl justify-items-center border border-gray-300">
                <div class="grid p-5 justify-items-center">
                    <p class="text-sm md:text-[18px] font-bold my-3">Jhonny Wilson</p>
                    <p class="text-sm md:text-[18px] -mt-2">10 Agustus 2022</p>
                    <div class="flex p-2">
                        <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex" alt="rating" title="Rating">
                            <p class="flex pt-3"> <span class="text-[16px] font-semibold ">4.0</span> <span class="text-[16px]"> /5.0</span></p>
                    </div>
                </div>

                <div class="grid justify-start justify-items-center p-5">
                    <p class="text-sm md:text-[18px]">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel placeat, tenetur dolores enim quae quod voluptates dolorem explicabo repellendus ratione voluptatibus aliquam exercitationem hic adipisci sint quibusdam, eos, at quas.</p>
                </div>
            </div>
        </div>
    </div>
</div>
