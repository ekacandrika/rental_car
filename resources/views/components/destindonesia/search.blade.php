<form>
<div class="bg-[#9E3D64] max-w-[920px] md:max-h-[130px] lg:max-h-[130px] max-h-[95px] rounded-lg">
    <div class="p-2 md:p-10 lg:p-10">
        <div class="grid grid-cols-7 grid-rows-2 divide-x divide-[#9E3D64]">

            <div class="flex justify-between max-h-full p-2 bg-white rounded-l-lg">
                <div class="flex-inline">
                    <select name="" id="" class="border-transparent focus:border-transparent focus:ring-0 text-[6px] md:text-base lg:text-sm pr-7">
                        <option value="">Pilih Tempat</option>
                        <option value="">1</option>
                        <option value="">1</option>
                    </select>
                </div>
            </div>

            <div class="flex justify-between max-h-full p-2 bg-white ">
                <div class="flex-inline">
                    <select name="" id="" class="border-transparent focus:border-transparent focus:ring-0 text-[6px] md:text-base lg:text-sm pr-7">
                        <option value="">Pilih Tempat</option>
                        <option value="">1</option>
                        <option value="">1</option>
                    </select>
                </div>
            </div>

            <div class="flex justify-between max-h-full p-2 bg-white ">
                <input id="datepicker"/>
            </div>

            <div class="flex justify-between max-h-full p-2 bg-white ">
                <input id="time"/>
            </div>

            <div class="flex justify-between max-h-full p-2 bg-white ">
                <div class="flex-inline">
                    <select name="" id="" class="border-transparent focus:border-transparent focus:ring-0 text-[6px] md:text-base lg:text-sm pr-7">
                        <option value="">Penumpang</option>
                        <option value="">1</option>
                        <option value="">1</option>
                    </select>
                </div>
            </div>

            <div class="flex justify-between max-h-full p-2 bg-white ">
                <div class="flex-inline">
                    <select name="" id="" class="border-transparent focus:border-transparent focus:ring-0 text-[6px] md:text-base lg:text-sm pr-7">
                        <option value="">Transfer</option>
                        <option value="">1</option>
                        <option value="">1</option>
                    </select>
                </div>
            </div>

            <div class="bg-[#FFB800] rounded-r-lg ">
                <div class="flex justify-center py-3">
                    <p class="text-[10px] md:text-sm lg:text-sm">
                        Cari
                    </p>
                    <img src="{{ asset('storage/icons/magnifying-glass-solid (1) 1.svg') }}" class="w-[10px] lg:w-[15px] md:w-[15px] mr-1 inline-flex md:ml-8 lg:ml-5 mx-1" alt="pijakan" title="Pijakan">
                </div>
            </div>

            <div class="grid col-span-3 row-span-2 p-2">
                <div class="flex text-[12px] text-white ">
                    <input class="border-transparent rounded-md focus:border-transparent focus:ring-0" type="checkbox" name="" id="">
                    <p class="px-2"> Transfer Bandara </p>
                </div>
            </div>

        </div>
    </div>
</div>
</form>

    <script>
        $('#datepicker').datepicker();
    </script>

    <script>
        $('#time').timepicker();
    </script>

    <script>
      const MONTH_NAMES = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];
      const MONTH_SHORT_NAMES = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
      ];
      const DAYS = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

      function app() {
        return {
          showDatepicker: false,
          datepickerValue: "",
          selectedDate: "2021-02-04",
          dateFormat: "DD-MM-YYYY",
          month: "",
          year: "",
          no_of_days: [],
          blankdays: [],
          initDate() {
            let today;
            if (this.selectedDate) {
              today = new Date(Date.parse(this.selectedDate));
            } else {
              today = new Date();
            }
            this.month = today.getMonth();
            this.year = today.getFullYear();
            this.datepickerValue = this.formatDateForDisplay(
              today
            );
          },
          formatDateForDisplay(date) {
            let formattedDay = DAYS[date.getDay()];
            let formattedDate = ("0" + date.getDate()).slice(
              -2
            ); // appends 0 (zero) in single digit date
            let formattedMonth = MONTH_NAMES[date.getMonth()];
            let formattedMonthShortName =
              MONTH_SHORT_NAMES[date.getMonth()];
            let formattedMonthInNumber = (
              "0" +
              (parseInt(date.getMonth()) + 1)
            ).slice(-2);
            let formattedYear = date.getFullYear();
            if (this.dateFormat === "DD-MM-YYYY") {
              return `${formattedDate}-${formattedMonthInNumber}-${formattedYear}`; // 02-04-2021
            }
            if (this.dateFormat === "YYYY-MM-DD") {
              return `${formattedYear}-${formattedMonthInNumber}-${formattedDate}`; // 2021-04-02
            }
            if (this.dateFormat === "D d M, Y") {
              return `${formattedDay} ${formattedDate} ${formattedMonthShortName} ${formattedYear}`; // Tue 02 Mar 2021
            }
            return `${formattedDay} ${formattedDate} ${formattedMonth} ${formattedYear}`;
          },
          isSelectedDate(date) {
            const d = new Date(this.year, this.month, date);
            return this.datepickerValue ===
              this.formatDateForDisplay(d) ?
              true :
              false;
          },
          isToday(date) {
            const today = new Date();
            const d = new Date(this.year, this.month, date);
            return today.toDateString() === d.toDateString() ?
              true :
              false;
          },
          getDateValue(date) {
            let selectedDate = new Date(
              this.year,
              this.month,
              date
            );
            this.datepickerValue = this.formatDateForDisplay(
              selectedDate
            );
            // this.$refs.date.value = selectedDate.getFullYear() + "-" + ('0' + formattedMonthInNumber).slice(-2) + "-" + ('0' + selectedDate.getDate()).slice(-2);
            this.isSelectedDate(date);
            this.showDatepicker = false;
          },
          getNoOfDays() {
            let daysInMonth = new Date(
              this.year,
              this.month + 1,
              0
            ).getDate();
            // find where to start calendar day of week
            let dayOfWeek = new Date(
              this.year,
              this.month
            ).getDay();
            let blankdaysArray = [];
            for (var i = 1; i <= dayOfWeek; i++) {
              blankdaysArray.push(i);
            }
            let daysArray = [];
            for (var i = 1; i <= daysInMonth; i++) {
              daysArray.push(i);
            }
            this.blankdays = blankdaysArray;
            this.no_of_days = daysArray;
          },
        };
      }
    </script>
