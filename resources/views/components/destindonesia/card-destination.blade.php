@props([
'text' => 'Kota Denpasar',
'lokasi' => 'provinsi',
'slug' => 'provinsi',
'index' => 0,
'header' => 'img/provinsi-detail-3.png'
])

<a href={{ $slug }}
    class="flex-none my-10 grid-row-2 grid-flow-col rounded-lg border bg-white shadow-lg hover:shadow-xl">
    <div class="relative">
        <img class="w-full h-[187px] object-cover rounded-t-lg" src="{{Storage::url($header)}}" alt="store-1">
        <p
            class="absolute p-2 text-white text-2xl font-bold top-0 left-0 rounded-tl-lg bg-kamtuu-primary bg-opacity-80">
            {{
                $index+1 
            }}</p>
    </div>
    <div class="py-2 px-5">
        <p class="font-semibold text-center text-xs md:text-sm uppercase">{{$text}}</p>
    </div>
</a>