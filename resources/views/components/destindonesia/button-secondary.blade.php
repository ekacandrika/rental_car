<div class="p-5 grid justify-items-center">
    <button class="px-8 py-2 lg:text-xl md:text-sm text-white duration-300 bg-[#D50006] rounded-lg hover:bg-kamtuu-primary hover:text-white">{{ $button }}</button>
</div>
