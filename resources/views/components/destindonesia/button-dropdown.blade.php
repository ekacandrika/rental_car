<div class="p-5 grid justify-items-center relative">
    <button id="dropdownDefaultButton" data-dropdown-toggle="dropdown" 
        class="text-white md:text-sm text-white duration-300 bg-[#D50006] hover:bg-kamtuu-primary font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center hover:bg-kamtuu-primary hover:text-white" type="button">
        Tambah ke list
        <?xml version="1.0" encoding="utf-8"?>
        <!-- Generator: Adobe Illustrator 24.1.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
        <svg  class="w-2.5 h-2.5 ml-2.5" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; width:20px; color:red" xml:space="preserve">
        <style type="text/css">
            .st0{fill-rule:evenodd;clip-rule:evenodd;}
        </style>
        <g>
            <path class="st0" d="M78.6,35.5c-9.8,0-17.7,7.9-17.7,17.7v392.5l141.9-94.6l141.9,94.6V266c0-9.8,7.9-17.7,17.7-17.7
                c9.8,0,17.7,7.9,17.7,17.7v246L202.8,393.8L25.4,512V53.2C25.4,23.8,49.3,0,78.6,0h141.9c9.8,0,17.7,7.9,17.7,17.7
                s-7.9,17.7-17.7,17.7H78.6z M397.9,0c9.8,0,17.7,7.9,17.7,17.7v70.9c0,9.8-7.9,17.7-17.7,17.7h-70.9c-9.8,0-17.7-7.9-17.7-17.7
                s7.9-17.7,17.7-17.7h53.2V17.7C380.2,7.9,388.1,0,397.9,0z"/>
            <path class="st0" d="M380.2,88.7c0-9.8,7.9-17.7,17.7-17.7h70.9c9.8,0,17.7,7.9,17.7,17.7s-7.9,17.7-17.7,17.7h-53.2v53.2
                c0,9.8-7.9,17.7-17.7,17.7s-17.7-7.9-17.7-17.7V88.7z"/>
        </g>
        </svg>
    </button>
    <!-- Dropdown menu -->
    <div id="dropdown" class="z-10 hidden bg-white divide-y mt-2 divide-gray-100 rounded-lg shadow w-44 absolute" style="top:61px">
        <ul class="py-2 text-sm text-gray-700 dark:text-gray-200" aria-labelledby="dropdownDefaultButton">
          <li class="flex hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">
            <a href="#" class="block px-4 py-2" id="ingin-kesana">Ingin Kesana 
            </a>
            <div>
                <img src="{{ asset('storage/icons/bookmark-star-fill.svg') }}"class="mt-[0.2rem] ml-3" alt="suka" title="Suka" style="width: 21px;">
            </div>
          </li>
          <li class="flex hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">
            <a href="#" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white" id="pernah-kesana">
                Pernah Kesana
            </a>
            <div>
                <img src="{{ asset('storage/icons/foundation_foot.svg') }}"
                class="mt-[0.2rem] mr-1" alt="suka" title="Suka" style="width: 21px;">
            </span>
          </li>
          <li>
        </ul>
    </div>
</div>