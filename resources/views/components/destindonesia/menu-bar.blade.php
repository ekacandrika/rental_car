<div class="bg-[#9E3D64] py-[22px] px-5 mx-auto md:w-auto lg:w-auto sm:w-auto  max-w-full sticky top-0 z-50">

    {{-- Desktop Menu --}}
    <div class="hidden sm:block md:block lg:block">
        <div class="grid grid-cols-2 lg:gap-x-96">

            {{-- Left Top Bar --}}
            <div class="grid w-auto grid-cols-3 divide-x">

                <div class="flex justify-center flex-auto">
                    {{-- Judul --}}
                    <div x-data="{ tooltip: false }" x-on:mouseover="tooltip = true"
                            x-on:mouseleave="tooltip = false" class="relative">
                        <a href="#" class="text-white text-[12px] pl-[1rem] sm:pl-[1rem] sm:text-[14px] divide-x">Bhs
                            Indonesia</a>
                        <div x-show="tooltip" class="absolute w-96 text-sm text-white absolute bg-black rounded-lg p-2 transform -translate-y-8 translate-x-8" style="top: 53px; left: 55px; display: none;width: 247px;">
                            Harap <strong>Aktifkan</strong>, google translate ekstensi anda
                        </div>    
                    </div>    
                    {{-- Logo --}}
                    <div class="px-2">
                        <a href="">
                            <img src="{{ asset('storage/icons/Polygon 3.svg') }}" class="w-[16px] h-[16px] mt-1"
                                 alt="bahasa" title="bahasa">
                        </a>
                    </div>
                </div>

                <div class="flex justify-center flex-auto">
                    {{-- judul --}}
                    <a href="#" class="text-white text-[12px] pl-[1rem] sm:pl-[1rem] sm:text-[14px] divide-x">Rupiah
                        (Rp)</a>

                    {{-- logo --}}
                    <div class="px-2">
                        <a href="#">
                            <img src="{{ asset('storage/icons/Polygon 3.svg') }}" class="w-[16px] h-[16px] mt-1"
                                 alt="money" title="money">
                        </a>
                    </div>
                </div>

                <div class="flex justify-center flex-auto">
                    {{-- judul --}}
                    <a href="/destindonesia"
                       class="text-white text-[12px] pl-[1rem] sm:pl-[1rem] sm:text-[14px] divide-x">Destindonesia</a>

                    {{-- logo --}}
                    <div class="px-2">
                        <a href="#">
                            <img src="{{ asset('storage/icons/point-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                                 alt="mark-loc" title="mark-loc">
                        </a>
                    </div>
                </div>


            </div>

            {{-- Right Top Bar --}}
            <div class="grid w-auto grid-cols-4 divide-x">

                @if (auth()->user() == null)
                    <div class="flex justify-center flex-auto">
                        {{-- Logo --}}
                        <div class="px-2">
                            <a href="{{ route('RegisterAgen') }}">
                                <img src="{{ asset('storage/icons/icon-gabung-agen.svg') }}"
                                     class="w-[16px] h-[16px] mt-1"
                                     alt="mark-loc" title="mark-loc">
                            </a>
                        </div>

                        {{-- Judul --}}
                        <a href="{{ route('RegisterAgen') }}" class="text-white text-[12px] sm:text-[14px] divide-x">Gabung
                            Agen</a>
                    </div>

                    <div class="flex justify-center flex-auto">
                        {{-- Logo --}}
                        <div class="px-2">
                            <a href="">
                                <a href="{{ route('RegisterSeller') }}">
                                    <img src="{{ asset('storage/icons/icon-gabung-seller.svg') }}"
                                         class="w-[16px] h-[16px] mt-1" alt="mark-loc" title="mark-loc">
                                </a>
                            </a>
                        </div>

                        {{-- Judul --}}
                        <a href="{{ route('RegisterSeller') }}"
                           class="text-white text-[12px] sm:text-[14px] divide-x">Gabung Seller</a>
                    </div>

                    <div class="flex justify-center flex-auto">
                        {{-- Logo --}}
                        <div class="px-2">
                            <a href="">
                                <a href="{{ route('RegisterTraveller') }}">
                                    <img src="{{ asset('storage/icons/icon-masuk-white.png') }}"
                                         class="w-[16px] h-[16px] mt-1" alt="mark-loc" title="mark-loc">
                                </a>
                            </a>
                        </div>

                        {{-- Judul --}}
                        <a href="{{ route('RegisterTraveller') }}"
                           class="text-white text-[12px] sm:text-[14px] divide-x">Daftar</a>
                    </div>

                    <div class="flex justify-center flex-auto">
                        {{-- Logo --}}
                        <div class="px-2">
                            <a href="{{ route('LoginTraveller') }}">
                                <img src="{{ asset('storage/icons/icon-daftar-white.png') }}"
                                     class="w-[16px] h-[16px] mt-1"
                                     alt="mark-loc" title="mark-loc">
                            </a>
                        </div>

                        {{-- Judul --}}
                        <a href="{{ route('LoginTraveller') }}"
                           class="text-white text-[12px] sm:text-[14px] divide-x">Masuk</a>
                    </div>
                @endif

                <div class="flex justify-end col-span-4">
                    @if (auth()->user() != null)

                        {{-- Traveller --}}
                        @if (auth()->user()->role == 'traveller')
                            @if (auth()->user()->profile_photo_path == null)
                                <a href="{{ route('cart') }}">
                                    <img src="{{ asset('storage/img/logo-traveller.png') }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @else
                                <a href="{{ route('cart') }}">
                                    <img src="{{ asset(auth()->user()->profile_photo_path) }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @endif
                        @endif

                        {{-- Agent --}}
                        @if (auth()->user()->role == 'agent')
                            @if (auth()->user()->profile_photo_path == null)
                                <a href="{{ route('viewMyOrder') }}">
                                    <img src="{{ asset('storage/img/logo-traveller.png') }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @else
                                <a href="{{ route('viewMyOrder') }}">
                                    <img src="{{ asset(auth()->user()->profile_photo_path) }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @endif
                        @endif

                        {{-- Seller --}}
                        @if (auth()->user()->role == 'seller')
                            @if (auth()->user()->profile_photo_path == null)
                                <a href="{{ route('listPesanan') }}">
                                    <img src="{{ asset('storage/img/logo-traveller.png') }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @else
                                <a href="{{ route('listPesanan') }}">
                                    <img src="{{ asset(auth()->user()->profile_photo_path) }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @endif
                        @endif

                        {{-- Admin --}}
                        @if (auth()->user()->role == 'admin')
                            @if (auth()->user()->profile_photo_path == null)
                                <a href="{{ route('admin.dashboard') }}">
                                    <img src="{{ asset('storage/img/logo-traveller.png') }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @else
                                <a href="{{ route('admin.dashboard') }}">
                                    <img src="{{ asset(auth()->user()->profile_photo_path) }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @endif
                        @endif

                    @endif
                </div>

                {{-- <div class="grid justify-items-end">
                    <label
                        class="items-center block w-10 px-3 py-2 text-white border rounded cursor-pointer lg:hidden hover:text-white hover:border-white"
                        for="menu-toggle">
                        <svg class="w-3 h-3 fill-current" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <title>Menu</title>
                            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
                        </svg>
                    </label>
                </div> --}}

            </div>
        </div>

        {{-- Mobile Menu --}}


    </div>
    {{-- Mobile Menu --}}
    <div class="block sm:hidden md:hidden lg:hidden xl:hidden">
        <div class="grid grid-cols-2">

            {{-- logo kamtuu --}}

            <div class="grid justify-items-start">
                <img src="{{ asset('storage/icons/kamtuu-logo-putih.png') }}" class="w-[100px] h-[20px] "
                     alt="polygon 3" title="Kamtuu">
            </div>

            <div class="grid justify-items-end">
                <label
                    class="items-center block w-10 px-3 py-2 text-white border rounded cursor-pointer lg:hidden hover:text-white hover:border-white"
                    for="menu-toggle">
                    <svg class="w-3 h-3 fill-current" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <title>Menu</title>
                        <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/>
                    </svg>
                </label>
            </div>

        </div>

        <input class="hidden" type="checkbox" id="menu-toggle"/>

        <div class="flex-grow hidden block w-full" id="menu">

            <div class="grid grid-cols-1 divide-y">
                <div class="py-2">

                </div>

                {{-- Bilingual --}}

                <div class="flex justify-start flex-auto py-2">
                    {{-- judul --}}
                    <a href="#"
                       class="text-white text-[12px] pl-[1rem] -ml-[1rem] sm:text-[14px] divide-x hover:underline hover:underline-offset-8">Bhs
                        Indonesia </a>

                    {{-- logo --}}
                    <div class="px-2">
                        <a href="#">
                            <img src="{{ asset('storage/icons/Polygon 3.svg') }}" class="w-[16px] h-[16px] mt-1"
                                 alt="polygon 3" title="Ubah Bahasa">
                        </a>
                    </div>
                </div>

                {{-- Rupiah --}}
                <div class="flex justify-start flex-auto py-2">
                    {{-- judul --}}
                    <a href="#"
                       class="text-white text-[12px] pl-[1rem] -ml-[1rem] sm:text-[14px] divide-x hover:underline hover:underline-offset-8">Rupiah
                        (Rp)</a>

                    {{-- logo --}}
                    <div class="px-2">
                        <a href="#">
                            <img src="{{ asset('storage/icons/Polygon 3.svg') }}" class="w-[16px] h-[16px] mt-1"
                                 alt="polygon 3" title="Mata Uang">
                        </a>
                    </div>
                </div>

                {{-- Destindonesia --}}
                <div class="flex justify-start flex-auto py-2">
                    {{-- judul --}}
                    <a href="/destindonesia"
                       class="text-white text-[12px] pl-[1rem] -ml-[1rem] sm:text-[14px] divide-x hover:underline hover:underline-offset-8">Destindonesia</a>

                    {{-- logo --}}
                    <div class="px-2">
                        <a href="#">
                            <img src="{{ asset('storage/icons/mark-loc.svg') }}" class="w-[16px] h-[16px] mb-2"
                                 alt="polygon 3" title="Destindonesia">
                        </a>
                    </div>
                </div>

                {{-- Gabung Agen --}}
                <div class="flex justify-start flex-auto py-2">
                    {{-- judul --}}
                    <a href="{{route('RegisterAgen')}}"
                       class="text-white text-[12px] pl-[1rem] -ml-[1rem] sm:text-[14px] divide-x hover:underline hover:underline-offset-8">Gabung
                        Agen</a>

                    {{-- logo --}}
                    <div class="px-2">
                        <a href="{{route('RegisterAgen')}}">
                            <img src="{{ asset('storage/icons/gabung-agen.png') }}" class="w-[16px] h-[16px] mb-2 "
                                 alt="polygon 3" title="Gabung Agen">
                        </a>
                    </div>
                </div>

                {{-- Gabung Seller --}}
                <div class="flex justify-start flex-auto py-2">
                    {{-- judul --}}
                    <a href="{{route('RegisterSeller')}}"
                       class="text-white text-[12px] pl-[1rem] -ml-[1rem] sm:text-[14px] divide-x hover:underline hover:underline-offset-8">Gabung
                        Seller</a>

                    {{-- logo --}}
                    <div class="px-2">
                        <a href="{{route('RegisterSeller')}}">
                            <img src="{{ asset('storage/icons/gabung-seller.png') }}" class="w-[16px] h-[16px] mb-2 "
                                 alt="polygon 3" title="Gabung Seller">
                        </a>
                    </div>
                </div>

                {{-- Daftar --}}
                <div class="flex justify-start flex-auto py-2">
                    {{-- judul --}}
                    <a href="{{route('RegisterTraveller')}}"
                       class="text-white text-[12px] pl-[1rem] -ml-[1rem] sm:text-[14px] divide-x hover:underline hover:underline-offset-8">Daftar</a>

                    {{-- logo --}}
                    <div class="px-2">
                        <a href="{{route('RegisterTraveller')}}">
                            <img src="{{ asset('storage/icons/login.png') }}" class="w-[16px] h-[16px] mb-2 "
                                 alt="polygon 3" title="Daftar">
                        </a>
                    </div>
                </div>
                {{-- Masuk --}}
                <div class="flex justify-start flex-auto py-2">
                    {{-- judul --}}
                    <a href="{{route('LoginTraveller')}}"
                       class="text-white text-[12px] pl-[1rem] -ml-[1rem] sm:text-[14px] divide-x hover:underline hover:underline-offset-8">Masuk</a>

                    {{-- logo --}}
                    <div class="px-2">
                        <a href="{{route('LoginTraveller')}}">
                            <img src="{{ asset('storage/icons/login.png') }}" class="w-[16px] h-[16px] mb-2 "
                                 alt="polygon 3" title="Masuk">
                        </a>
                    </div>
                </div>

                {{-- Keluar --}}
                <div class="flex justify-start flex-auto py-2">
                    {{-- judul --}}
                    {{-- <a href="{{ route('LoginTraveller') }}" --}}
                    <a href="#"
                       class="text-white text-[12px] pl-[1rem] -ml-[1rem] sm:text-[14px] divide-x hover:underline hover:underline-offset-8">Keluar
                       <form id="logout-form" method="POST" action="{{ route('logout') }}">
                                @csrf
                        </form> 
                    </a>

                    {{-- logo --}}
                    <div class="px-2">
                        <a href="#">
                            <img src="{{ asset('storage/icons/keluar.png') }}" class="w-[16px] h-[16px] mb-2 "
                                 alt="polygon 3" title="Keluar">
                            <form id="logout-form" method="POST" action="{{ route('logout') }}">
                                @csrf
                            </form>
                        </a>
                    </div>
                </div>

                {{-- Home --}}
                <div class="flex justify-start flex-auto py-2">
                    <a href="/home"
                       class="text-white text-[12px] hover:underline hover:underline-offset-8 mb-1">HOME</a>
                </div>

                {{-- Jelajahi berdasarkan kategori --}}
                <div class="flex justify-start flex-auto py-2">
                    <a href="#" class="flex text-[12px] text-white hover:underline hover:underline-offset-8 mb-1">
                        <span class="my-auto">Jelajahi berdasarkan kategori</span>
                        <img class="mx-2" src="{{ asset('storage/icons/icon-filter.svg') }}" alt="" width="16px"
                             height="16px">
                    </a>
                </div>

                {{-- Minta dibuatkan paket --}}
                <div class="flex justify-start flex-auto py-2">
                    <a href="{{route('list-formulir')}}"
                       class="flex text-[12px] text-white hover:underline hover:underline-offset-8 mb-1">
                        <span class="my-auto">Minta dibuatkan paket</span>
                        <img class="mx-2" src="{{ asset('storage/icons/icon-create-paket.svg') }}" alt="" width="16px"
                             height="16px">
                    </a>
                </div>

                {{-- Destinasi --}}
                <div class="flex justify-start flex-auto py-2">
                    <a href="#"
                       class="flex text-[12px] text-white hover:underline hover:underline-offset-8 mb-1">Destinasi</a>
                </div>

                {{-- Newsletter --}}
                <div class="flex justify-start flex-auto py-2">
                    <a href="{{route('newsletter.index')}}"
                       class="flex text-[12px] text-white hover:underline hover:underline-offset-8 mb-1">Newsletter</a>
                </div>

                {{-- Event --}}
                <div class="flex justify-start flex-auto py-2">
                    <a href="#"
                       class="flex text-[12px] text-white hover:underline hover:underline-offset-8 mb-1">Event</a>
                </div>

                {{-- Videos --}}
                <div class="flex justify-start flex-auto py-2">
                    <a href="#"
                       class="flex text-[12px] text-white hover:underline hover:underline-offset-8 mb-1">Videos</a>
                </div>
                <div class="flex justify-start flex-auto py-2">
                    @if (auth()->user() != null)

                        {{-- Traveller --}}
                        @if (auth()->user()->role == 'traveller')
                            @if (auth()->user()->profile_photo_path == null)
                                <a href="{{ route('cart') }}">
                                    <img src="{{ asset('storage/img/logo-traveller.png') }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @else
                                <a href="{{ route('cart') }}">
                                    <img src="{{ asset(auth()->user()->profile_photo_path) }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @endif
                        @endif

                        {{-- Agent --}}
                        @if (auth()->user()->role == 'agent')
                            @if (auth()->user()->profile_photo_path == null)
                                <a href="{{ route('viewMyOrder') }}">
                                    <img src="{{ asset('storage/img/logo-traveller.png') }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @else
                                <a href="{{ route('viewMyOrder') }}">
                                    <img src="{{ asset(auth()->user()->profile_photo_path) }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @endif
                        @endif

                        {{-- Seller --}}
                        @if (auth()->user()->role == 'seller')
                            @if (auth()->user()->profile_photo_path == null)
                                <a href="{{ route('listPesanan') }}">
                                    <img src="{{ asset('storage/img/logo-traveller.png') }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @else
                                <a href="{{ route('listPesanan') }}">
                                    <img src="{{ asset(auth()->user()->profile_photo_path) }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @endif
                        @endif

                        {{-- Admin --}}
                        @if (auth()->user()->role == 'admin')
                            @if (auth()->user()->profile_photo_path == null)
                                <a href="{{ route('admin.dashboard') }}">
                                    <img src="{{ asset('storage/img/logo-traveller.png') }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @else
                                <a href="{{ route('admin.dashboard') }}">
                                    <img src="{{ asset(auth()->user()->profile_photo_path) }}" alt="user-profile"
                                         class="object-cover object-center w-8 h-8 overflow-hidden border-indigo-600 rounded-full ">
                                </a>
                            @endif
                        @endif

                    @endif
                </div>
                {{-- Search --}}
                <div class="flex justify-start flex-auto py-2">
                    <div class="flex justify-center w-full my-auto bg-white rounded-full">
                        <input
                            class="block w-full py-1 pl-5 pr-3 bg-white border border-transparent rounded-full shadow-sm placeholder:italic placeholder:text-slate-400 lg:py-2 focus:outline-none focus:border-transparent focus:ring-transparent focus:ring-1 sm:text-sm"
                            placeholder="Search" type="text" name="search" id="form-mobile-search"/>
                        <button class="mx-3" id="btn-mobile-search">
                            <img src="{{ asset('storage/icons/search-ic.png') }}" alt="">
                        </button>
                    </div>
                </div>


            </div>

        </div>

    </div>

    {{-- Top Bar --}}

</div>

{{-- Top Bar --}}
<div class="bg-white lg:container mx-auto max-h-[90px] hidden sm:block relative z-10">
    <div class="grid grid-cols-4 gap-2 p-3 lg:grid-cols-5">

        {{-- Kamtuu Logo --}}
        <a href="/home">
            <div class="flex justify-center my-auto lg:h-3/4">
                <img src="{{ asset('storage/img/kamtuu-logo.png') }}" alt="" width="auto">
            </div>
        </a>

        {{-- Home --}}
        <div class="flex justify-end my-auto lg:col-start-3">
            <a href="/home" class="text-[#9E3D64] font-semibold text-xl mr-3 lg:mr-0">HOME</a>
        </div>

        {{-- Jelajahi berdasarkan kategori --}}
        <div class="flex justify-center my-auto lg:justify-end">

            <div class="relative inline-block text-left">
                <button id="dropdown-button"
                        class="inline-flex justify-center w-full p-2 text-sm font-medium text-gray-700 bg-white rounded-md shadow-sm focus:outline-none focus:ring-1 focus:ring-offset-0 focus:ring-kamtuu-primary">
                    <div href="/home/list-kategori" class="flex text-sm 2xl:text-base">
                        <img class="mr-2 my-auto w-[24px] h-[24px] lg:w-[36px] lg:h-[36px] lg:mr-4"
                             src="{{ asset('storage/icons/icon-filter.svg') }}" alt="">
                        <span class="my-auto">Jelajahi berdasarkan kategori</span>
                    </div>
                </button>

                <div id="dropdown-menu" class="absolute z-10 hidden w-full mt-2 bg-white rounded-md shadow-lg">
                    <a href="/tur" class="block px-4 py-2 text-sm text-gray-700 hover:bg-kamtuu-primary hover:text-white">Tur</a>
                    <a href="/transfer" class="block px-4 py-2 text-sm text-gray-700 hover:bg-kamtuu-primary hover:text-white">Transfer</a>
                    <a href="/hotel" class="block px-4 py-2 text-sm text-gray-700 hover:bg-kamtuu-primary hover:text-white">Hotel</a>
                    <a href="/rental" class="block px-4 py-2 text-sm text-gray-700 hover:bg-kamtuu-primary hover:text-white">Rental</a>
                    <a href="/activity" class="block px-4 py-2 text-sm text-gray-700 hover:bg-kamtuu-primary hover:text-white">Activity</a>
                    <a href="/xstay" class="block px-4 py-2 text-sm text-gray-700 hover:bg-kamtuu-primary hover:text-white">Xstay</a>
                </div>
            </div>

            <script type="module">
                $(document).ready(function () {
                    $('#dropdown-button').click(function () {
                        $('#dropdown-menu').slideToggle(300);
                    });
                });
            </script>

        </div>

        {{-- Minta dibuatkan paket --}}
        <div class="flex justify-center my-auto">
            <a href="{{route('list-formulir')}}" class="flex text-sm 2xl:text-base">
                <img class="mr-2 my-auto w-[24px] h-[24px] lg:w-[36px] lg:h-[36px] lg:mr-4"
                     src="{{ asset('storage/icons/icon-create-paket.svg') }}" alt="">
                <span class="my-auto">Minta dibuatkan paket</span>
            </a>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
    document.getElementById("btn-mobile-search").onclick = function(){
       var form_mobile_search = document.getElementById("form-mobile-search").value;
       let url = "{{route('search.destindo')}}"
        window.location.replace(`${url}?cari=${form_mobile_search}`);
    }
</script>