@props([
    'url' => '',
])

<a href="{{ $url }}" class="p-5 grid justify-items-center">
    <button type="submit"
        class="my-3 px-8 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">{{
        $button }}
    </button>
</a>
