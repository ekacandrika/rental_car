@props([
    'title' => 'Kota Denpasar',
    'subtitle' => 'provinsi',
    'image' => '',
])

{{ dd($act) }}
<figure class="relative max-w-xs cursor-pointer">
    <img class="rounded-lg shadow-xl hover:shadow-2xl w-[200px] h-[100px] sm:w-[300px] sm:h-[185px]"
        src="https://images.unsplash.com/photo-1570169797118-99f5f90c4e25?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80">
    <figcaption class="absolute px-4 -mt-16 text-lg text-white">
        <div>
            <h1>{{ $title }}</h1>
        </div>
        <div class="truncate">
            {!! $subtitle !!}
        </div>
    </figcaption>
</figure>
