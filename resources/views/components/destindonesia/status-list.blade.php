@props([
'ratings' => '0',
'var2' => 0,
'want'=>0,
'been'=>0
])

<div class="hidden md:block lg:block">
    <div
        class="grid max-w-[920px] lg:grid-cols-4 md:grid-cols-4 grid-rows-1 border lg:divide-x md:divide-x rounded-lg border-stone-900">
        <div class="grid lg:m-5 md:m-3 justify-items-center">
            <p>
                <span class="md:text-[16px] lg:text-[50px] font-bold">
                    #1
                </span>
                dari 100
            </p>
        </div>
        <div class="grid pl-5 lg:m-5 md:m-3 justify-items-center">
            <div class="flex">
                <img src="{{ asset('storage/icons/Star 1.png') }}"
                    class="w-[48px] h-[48px] lg:w-[48px] lg:h-[48px] md:w-[20px] md:h-[20px]  mt-[0.2rem] mr-1 inline-flex"
                    alt="rating" title="Rating">
                <p class="flex"> <span class="text-[50px] md:text-[16px] font-semibold ">{{ $ratings }}</span> <span
                        class="text-[30px] md:text-[16px] md:pt-0 pt-6"> /5.0</span></p>
            </div>
            <p class="lg:-mt-3 lg:pl-10 md:text-[16px]">dari {{ $var2 }} ulasan</p>
        </div>
        <div class="grid lg:m-5 md:m-3  justify-items-center">
            <div class="flex">
                <img src="{{ asset('storage/icons/foundation_foot.svg') }}"
                    class="w-[60px] mt-[0.2rem] md:w-[20px] lg:w-[60px] mr-1 inline-flex" alt="pijakan" title="Pijakan">
                <p class="flex pt-4 md:pt-0"> <span class="text-[16px] font-semibold" id="pijakan">{{isset($been) ? $been: '1,000'}} Traveller</span></p>
            </div>
            <p class="lg:-mt-8 lg:pl-16">&nbsp;&nbsp;pernah ke sini</p>
        </div>
        <div class="grid lg:m-5 md:m-3  justify-items-center">
            <div class="flex">
                <img src="{{ asset('storage/icons/bookmark-star-fill.svg') }}"
                    class="w-[60px] md:w-[20px] lg:w-[60px] mt-[0.2rem] mr-1 inline-flex" alt="suka" title="Suka">
                <p class="flex pt-4  md:pt-0"> <span class="text-[16px] font-semibold" id="been-there">{{isset($want) ? $want:'9869'}} Orang</span></p>
            </div>
            <p class="lg:-mt-8 lg:pl-16">&nbsp;&nbsp;ingin ke sini</p>
        </div>
    </div>
</div>

<div class="block m-2 md:hidden lg:hidden">
    <div class="grid max-w-[920px] grid-cols-4 grid-rows-1 border divide-x rounded-lg border-stone-900">
        <div class="text-[12px] grid pt-7 pl-2 pr-2 justify-items-center">
            <p>
                <span class="font-bold ">
                    #1
                </span>
                dari 100
            </p>
        </div>
        <div class="grid pt-2 pl-2 pr-2 justify-items-center">
            <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1 inline-flex"
                alt="rating" title="Rating">
            <p class="flex"> <span class="text-[12px] font-semibold ">4.0</span> <span class="text-[12px]"> /5.0 dari 12
                    ulasan</span></p>
        </div>
        <div class="grid pt-2 pl-2 pr-2 text-center justify-items-center">
            <img src="{{ asset('storage/icons/foundation_foot.svg') }}" class="w-[20px] " alt="pijakan" title="Pijakan">
            <p class="flex pt-4 md:pt-0"> <span class="text-[16px] font-semibold" id="pijakan">{{isset($been) ? $been: '1,000'}} Traveller</span></p>
            <p class="text-[12px]">pernah ke sini</p>
        </div>
        <div class="grid pt-2 pl-2 pr-2 text-center justify-items-center">
            {{-- 
            <div class="flex">
                <img src="{{ asset('storage/icons/bookmark-star-fill.svg') }}"
                    class="w-[60px] md:w-[20px] lg:w-[60px] mt-[0.2rem] mr-1 inline-flex" alt="suka" title="Suka">
                <p class="flex pt-4  md:pt-0"> <span class="text-[16px] font-semibold" id="been-there">{{isset($want) ? $want:'9869'}} Orang</span></p>
            </div>
            <p class="lg:-mt-8 lg:pl-16">&nbsp;&nbsp;ingin ke sini</p>
            --}}
            <img src="{{ asset('storage/icons/bookmark-star-fill.svg') }}" class="w-[15px] mt-[0.2rem] mr-1 inline-flex"
                alt="pijakan" title="Pijakan">
            <p class="flex pt-4  md:pt-0"> <span class="text-[16px] font-semibold" id="been-there">{{isset($want) ? $want:'9869'}} Orang</span></p>
            <p class="text-[12px]">ingin ke sini</p>
        </div>
    </div>
</div>