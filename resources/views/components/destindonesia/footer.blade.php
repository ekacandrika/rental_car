        <footer class="py-8 bg-[#9E3D64] footer-1 sm:py-12 text-white">
            <div class="mx-10">
                <div>
                    <p>Langganan newsletter kami, jangan lewatkan berita, promo, dan event terkini dari Pariwisata Indonesia</p>
                </div>

                <div class="py-5">
                    <div class="flex items-center justify-center pb-6 md:py-0 md:w-1/2 md:ml-28 lg:-ml-">
                        <form>
                            <div class="flex flex-col overflow-hidden rounded-lg h-18 sm:flex-row">
                                <input class="px-4 py-3 text-gray-800 placeholder-gray-500 bg-white border-2 border-gray-300 outline-none focus:bg-gray-100 sm:w-80" type="text" name="email" placeholder="Enter your email">
                                <button class="px-4 py-3 font-semibold text-black bg-[#FFB800] hover:bg-[#ffcc4c] w-60">Langganan</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div>
                    <p class="font-bold text-[26px]">© PT. KAMTUU WISATA INDONESIA (KAMTUU.COM)</p>
                </div>

                <div class="grid grid-cols-2">
                    <div class="grid justify-items-start">
                        <img src="{{ asset('storage/icons/kamtuu-logo-putih.png') }}" class="w-[292px] h-[70.59px] my-5" alt="polygon 3" title="Kamtuu">
                    </div>

                    <div class="grid justify-items-end">
                        <img src="{{ asset('storage/logos/kamtuu-white 1.png') }}" class="w-[75px] h-[128px] my-5" alt="polygon 3" title="Kamtuu">
                    </div>
                </div>

            </div>
        </footer>
