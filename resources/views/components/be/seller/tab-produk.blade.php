<div class="flex items-center justify-center max-w-screen-sm md:mx-2 lg:mx-auto lg:max-w-[85rem]">

    <div class="container flex flex-col items-stretch justify-center h-full p-2 mx-auto sm:p-8" x-data="{ tab: 1 }">

        <!-- TABS -->
        <div class="z-10 flex justify-start -space-x-px">
            <div class="grid grid-cols-3 grid-rows-1 md:flex lg:flex">
                <a href="!#0" @click.prevent="tab = 1"
                    :class="{
                        'cursor-default border-b-0 bg-kamtuu-second': tab ===
                            1,
                        'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                            1
                    }"
                    class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none lg:rounded-tl-lg">Paket
                    Tur
                </a>
                <a href="!#0" @click.prevent="tab = 2"
                    :class="{
                        'cursor-default border-b-0 bg-kamtuu-second': tab ===
                            2,
                        'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                            2
                    }"
                    class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none">Transfer
                </a>
                <a href="!#0" @click.prevent="tab = 3"
                    :class="{
                        'cursor-default border-b-0 bg-kamtuu-second': tab ===
                            3,
                        'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                            3
                    }"
                    class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none ">Rental
                    Kendaraan
                </a>
                <a href="!#0" @click.prevent="tab = 4"
                    :class="{
                        'cursor-default border-b-0 bg-kamtuu-second': tab ===
                            4,
                        'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                            4
                    }"
                    class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none">Aktivitas
                </a>
                <a href="!#0" @click.prevent="tab = 5"
                    :class="{
                        'cursor-default border-b-0 bg-kamtuu-second': tab ===
                            5,
                        'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                            5
                    }"
                    class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none">Hotel
                </a>
                <a href="!#0" @click.prevent="tab = 6"
                    :class="{
                        'cursor-default border-b-0 bg-kamtuu-second': tab ===
                            6,
                        'text-gray-600 bg-kamtuu-primary hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:shadow-outline': tab !==
                            6
                    }"
                    class="block px-6 py-4 text-base font-semibold leading-none text-white uppercase align-middle border border-gray-400 shadow-none outline-none lg:rounded-tr-md">Xstay
                </a>
            </div>
        </div>


        <!-- Tabs Contains -->
        {{-- Paket Tur --}}
        <div x-show="tab === 1"
            class="z-0 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-6 md:grid-cols-1 bg-white justify-items-center divide-kamtuu-second lg:divide-x">
            <div class="col-end-1 z-0">
                <div class="p-5 pl-10 pr-10 h-full">
                    {{-- Card --}}
                    <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                        <div class="grid grid-cols-3">
                            <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                                <div class="w-full">
                                    <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Master Wilayah</p>
                                    <p class="mt-4 font-bold text-[#FFFFFF] text-xl">10.000</p>
                                    <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Tempat</p>
                                </div>
                            </div>
                            <div class="w-auto flex bg-[#FFB800] rounded">
                                <div class="w-full">
                                    <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Banyak Listing</p>
                                    <p class="mt-4 font-bold text-[#FFFFFF] text-xl">15.000</p>
                                    <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Listing</p>
                                </div>
                            </div>
                            <div class="w-auto flex bg-[#51B449] rounded ml-5">
                                <div class="w-full">
                                    <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Extra</p>
                                    <p class="mt-4 font-bold text-[#FFFFFF] text-xl">5.000</p>
                                    <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Extra</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Grafik --}}
                    <div class="grid grid-cols-2 text-center h-96">
                        <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                            <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                        </div>
                        <div class="text-center ml-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                            <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                        </div>
                    </div>

                    {{-- Card --}}
                    <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                        {{-- Card --}}
                        <div class="grid grid-cols-3">
                            <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                                <div class="w-full">
                                    <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Traveller</p>
                                    <p class="mt-4 font-bold text-[#FFFFFF] text-xl">20.000</p>
                                    <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Traveller</p>
                                </div>
                            </div>
                            <div class="w-auto flex bg-[#FFB800] rounded">
                                <div class="w-full">
                                    <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Seller</p>
                                    <p class="mt-4 font-bold text-[#FFFFFF] text-xl">25.000</p>
                                    <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Seller</p>
                                </div>
                            </div>
                            <div class="w-auto flex bg-[#51B449] rounded ml-5">
                                <div class="w-full">
                                    <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Agent</p>
                                    <p class="mt-4 font-bold text-[#FFFFFF] text-xl">20.000</p>
                                    <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Agent</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Card --}}
                    <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                        {{-- Card --}}
                        <div class="grid grid-cols-3">
                            <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                                <div class="w-full">
                                    <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Corporate</p>
                                    <p class="mt-4 font-bold text-[#FFFFFF] text-xl">500</p>
                                    <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Corporate</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <span class="text-lg font-bold font-inter text-[#333333]">Data Layanan</span>

                    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400 my-4">
                        <thead class="text-xs text-white uppercase bg-[#9E3D64] dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="py-3 px-6">
                                    Keterangan Layanan
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Sudah Dibayarkan
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Belum dibayarkan
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr
                                class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <th scope="row"
                                    class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                    Tour
                                </th>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    134
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    200
                                </td>
                            </tr>
                            <tr
                                class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <th scope="row"
                                    class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                    Travel
                                </th>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    134
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    200
                                </td>
                            </tr>
                            <tr
                                class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <th scope="row"
                                    class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                    Rental
                                </th>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    134
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    200
                                </td>
                            </tr>
                            <tr
                                class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <th scope="row"
                                    class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                    Activity
                                </th>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    134
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    200
                                </td>
                            </tr>
                            <tr
                                class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <th scope="row"
                                    class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                    Hotel
                                </th>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    134
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    200
                                </td>
                            </tr>
                            <tr
                                class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <th scope="row"
                                    class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                    XStay
                                </th>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    134
                                </td>
                                <td class="py-4 px-6 font-semibold text-[#333333]">
                                    200
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        {{-- Transfer --}}
        <div x-show="tab === 2"
            class="z-0 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-7 md:grid-cols-1 bg-white justify-items-center divide-kamtuu-second lg:divide-x">
            <div class="p-5 pl-10 pr-10 h-full">
                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Master Wilayah</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">10.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Tempat</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Banyak Listing</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">15.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Listing</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded ml-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Extra</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">5.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Extra</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Grafik --}}
                <div class="grid grid-cols-2 text-center h-96">
                    <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                    </div>
                    <div class="text-center ml-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                    </div>
                </div>

                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Traveller</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">20.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Traveller</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Seller</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">25.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Seller</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded ml-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Agent</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">20.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Agent</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Corporate</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">500</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Corporate</p>
                            </div>
                        </div>
                    </div>
                </div>

                <span class="text-lg font-bold font-inter text-[#333333]">Data Layanan</span>

                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400 my-4">
                    <thead class="text-xs text-white uppercase bg-[#9E3D64] dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="py-3 px-6">
                                Keterangan Layanan
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Sudah Dibayarkan
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Belum dibayarkan
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Tour
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Travel
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Rental
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Activity
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Hotel
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                XStay
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        {{-- Rental Kendaraan --}}
        <div x-show="tab === 3"
            class="z-0 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-6 md:grid-cols-1 bg-white justify-items-center divide-kamtuu-second lg:divide-x">
            <div class="p-5 pl-10 pr-10 h-full">
                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Master Wilayah</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">10.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Tempat</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Banyak Listing</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">15.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Listing</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded ml-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Extra</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">5.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Extra</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Grafik --}}
                <div class="grid grid-cols-2 text-center h-96">
                    <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                    </div>
                    <div class="text-center ml-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                    </div>
                </div>

                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Traveller</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">20.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Traveller</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Seller</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">25.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Seller</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded ml-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Agent</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">20.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Agent</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Corporate</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">500</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Corporate</p>
                            </div>
                        </div>
                    </div>
                </div>

                <span class="text-lg font-bold font-inter text-[#333333]">Data Layanan</span>

                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400 my-4">
                    <thead class="text-xs text-white uppercase bg-[#9E3D64] dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="py-3 px-6">
                                Keterangan Layanan
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Sudah Dibayarkan
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Belum dibayarkan
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Tour
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Travel
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Rental
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Activity
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Hotel
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                XStay
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        {{-- Aktivitas --}}
        <div x-show="tab === 4"
            class="z-0 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-6 md:grid-cols-1 bg-white justify-items-center divide-kamtuu-second lg:divide-x">
            <div class="p-5 pl-10 pr-10 h-full">
                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Master Wilayah</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">10.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Tempat</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Banyak Listing</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">15.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Listing</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded ml-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Extra</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">5.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Extra</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Grafik --}}
                <div class="grid grid-cols-2 text-center h-96">
                    <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                    </div>
                    <div class="text-center ml-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                    </div>
                </div>

                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Traveller</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">20.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Traveller</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Seller</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">25.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Seller</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded ml-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Agent</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">20.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Agent</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Corporate</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">500</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Corporate</p>
                            </div>
                        </div>
                    </div>
                </div>

                <span class="text-lg font-bold font-inter text-[#333333]">Data Layanan</span>

                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400 my-4">
                    <thead class="text-xs text-white uppercase bg-[#9E3D64] dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="py-3 px-6">
                                Keterangan Layanan
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Sudah Dibayarkan
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Belum dibayarkan
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Tour
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Travel
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Rental
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Activity
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Hotel
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                XStay
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        {{-- HOTEL --}}
        <div x-show="tab === 5"
            class="z-0 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-6 md:grid-cols-1 bg-white justify-items-center divide-kamtuu-second lg:divide-x">
            <div class="p-5 pl-10 pr-10 h-full">
                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Master Wilayah</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">10.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Tempat</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Banyak Listing</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">15.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Listing</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded ml-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Extra</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">5.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Extra</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Grafik --}}
                <div class="grid grid-cols-2 text-center h-96">
                    <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                    </div>
                    <div class="text-center ml-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                    </div>
                </div>

                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Traveller</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">20.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Traveller</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Seller</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">25.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Seller</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded ml-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Agent</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">20.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Agent</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Corporate</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">500</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Corporate</p>
                            </div>
                        </div>
                    </div>
                </div>

                <span class="text-lg font-bold font-inter text-[#333333]">Data Layanan</span>

                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400 my-4">
                    <thead class="text-xs text-white uppercase bg-[#9E3D64] dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="py-3 px-6">
                                Keterangan Layanan
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Sudah Dibayarkan
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Belum dibayarkan
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Tour
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Travel
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Rental
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Activity
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Hotel
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                XStay
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        {{-- XSTAY --}}
        <div x-show="tab === 6"
            class="z-0 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-6 md:grid-cols-1 bg-white justify-items-center divide-kamtuu-second lg:divide-x">
            <div class="p-5 pl-10 pr-10 h-full">
                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Master Wilayah</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">10.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Tempat</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Banyak Listing</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">15.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Listing</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded ml-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Extra</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">5.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Extra</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Grafik --}}
                <div class="grid grid-cols-2 text-center h-96">
                    <div class="text-center mr-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                    </div>
                    <div class="text-center ml-2 rounded bg-[#FFFFFF] border-2 border-[#9B9B9B]">
                        <p class="mt-2 font-bold text-[#333333] text-base">Grafik Pendapatan</p>
                    </div>
                </div>

                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Traveller</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">20.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Traveller</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#FFB800] rounded">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Seller</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">25.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Seller</p>
                            </div>
                        </div>
                        <div class="w-auto flex bg-[#51B449] rounded ml-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Agent</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">20.000</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Agent</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Card --}}
                <div class="drop-shadow-xl pb-5 mt-5 mb-5 rounded text-center">
                    {{-- Card --}}
                    <div class="grid grid-cols-3">
                        <div class="w-auto flex bg-[#23AEC1] rounded mr-5">
                            <div class="w-full">
                                <p class="mt-2 font-semibold text-[#FFFFFF] text-base">Jumlah Corporate</p>
                                <p class="mt-4 font-bold text-[#FFFFFF] text-xl">500</p>
                                <p class="mb-2 font-semibold text-[#FFFFFF] text-base">Corporate</p>
                            </div>
                        </div>
                    </div>
                </div>

                <span class="text-lg font-bold font-inter text-[#333333]">Data Layanan</span>

                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400 my-4">
                    <thead class="text-xs text-white uppercase bg-[#9E3D64] dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="py-3 px-6">
                                Keterangan Layanan
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Sudah Dibayarkan
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Belum dibayarkan
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Tour
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Travel
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Rental
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Activity
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                Hotel
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="py-4 px-6 font-semibold text-[#333333] whitespace-nowrap dark:text-white">
                                XStay
                            </th>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                134
                            </td>
                            <td class="py-4 px-6 font-semibold text-[#333333]">
                                200
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
