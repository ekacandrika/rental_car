<div class="grid grid-cols-10">
    <div class="col-start-1 col-end-3">
        {{-- @dump(auth()->user()->jenis_registrasi) --}}
        @php

            $user_datas  = "";
            $kelengkapan = 0;
            $array_datas = [];
            $total_kolom = "";

            if(auth()->user()->jenis_registrasi=='company'){
                $user_datas = DB::select("SELECT
                    a.first_name,
                    a.jenis_registrasi,
                    a.role,
                    a.email,
                    a.no_tlp,
                    a.lisensi,
                    a.jk,
                    a.email_verified_at,
                    a.active_status,
                    a.profile_photo_path,
                    b.province_id,
                    b.regency_id,
                    b.district_id,
                    b.village_id,
                    b.address,
                    b.postal_code,
                    b.identity_card_number,
                    b.ID_card_photo
                FROM
                    users as a
                LEFT JOIN user_detail b ON b.user_id = a.id 
                WHERE
                a.id =".auth()->user()->id);
            }else{
                $user_datas = DB::select("SELECT
                    a.first_name,
                    a.jenis_registrasi,
                    a.role,
                    a.email,
                    a.no_tlp,
                    a.lisensi,
                    a.jk,
                    a.email_verified_at,
                    a.active_status,
                    a.profile_photo_path
                FROM
                    users as a
                LEFT JOIN user_detail b ON b.user_id = a.id 
                WHERE
                a.id =".auth()->user()->id);
            }

            $i = 0;

            if($user_datas){
                foreach($user_datas as $key => $data){
                    $total_kolom = count(collect($data));
                    
                    if($data->first_name){
                        $kelengkapan +=1;
                    }
                    if($data->jenis_registrasi){
                        $kelengkapan +=1;
                    }
                    if($data->role){
                        $kelengkapan +=1;
                    }
                    if($data->email){
                        $kelengkapan +=1;
                    }
                    if($data->no_tlp){
                        $kelengkapan +=1;
                    }
                    if($data->lisensi){
                        $kelengkapan +=1;
                    }
                    if($data->jk){
                        $kelengkapan +=1;
                    }
                    if($data->email_verified_at){
                        $kelengkapan +=1;
                    }
                    if($data->active_status){
                        $kelengkapan +=1;
                    }
                    if($data->profile_photo_path){
                        $kelengkapan +=1;
                    }
                    if(auth()->user()->jenis_registrasi=='company'){
                        if($data->province_id){
                            $kelengkapan +=1;
                        }
                        if($data->regency_id){
                            $kelengkapan +=1;
                        }   
                        if($data->district_id){
                            $kelengkapan +=1;
                        }
                        if($data->village_id){
                            $kelengkapan +=1;
                        }
                        
                        if($data->address){
                            $kelengkapan +=1;
                        }
                        if($data->postal_code){
                            $kelengkapan +=1;
                        }
                        if($data->identity_card_number){
                            $kelengkapan +=1;
                        }
                        if($data->ID_card_photo){
                            $kelengkapan +=1;
                        }
                    }
                }
            }
            
            $persentasi = round(($kelengkapan/$total_kolom)*100);
            
        @endphp
        <div
            class="bg-[#9E3D64] hidden lg:block fixed z-20 inset-0 top-[3.8125rem] right-auto overflow-y-auto px-3 w-[256px]">
            <div class="py-2">
                <div class="px-4 pt-2 flex flex-col items-center justify-center space-y-3">
                    <img src="{{ isset(auth()->user()->profile_photo_path) ? asset(auth()->user()->profile_photo_path) : asset('storage/img/logo-traveller.png') }}"
                        alt="user-profile" class="object-cover object-center w-14 h-14 rounded-full">

                    <p class="text-base font-inter font-semibold text-[#FFFFFF] mt-[2px]">
                        {{ isset(auth()->user()->first_name) ? auth()->user()->first_name : 'Nama Seller' }}</p>

                    <div class="w-full bg-gray-200 rounded-full dark:bg-gray-700 border-[#FFB800]">
                        <div class="bg-[#FFB800] text-xs font-medium text-[#9E3D64] text-center p-0.5 leading-none rounded-full"
                            style="width: {{$persentasi}}%"> {{$persentasi}}%</div>
                    </div>
                </div>
            </div>
            <ul>
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="akun">
                        <img src="{{ asset('storage/icons/dasbor-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Dasbor</p>
                    </label>
                </li>

                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="akun">
                        <img src="{{ asset('storage/icons/circle-user-solid-white.svg') }}"
                            class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Akun</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="akun" />

                    <ul id="akun" class="hidden">
                        <li>
                            <a href="{{ route('seller.profilesetting', auth()->user()->role) }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Profil</a>
                        </li>
                        {{-- <li>
                            <a href="#"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Informasi
                                Bank</a>
                        </li> --}}
                        <li>
                            <a href="{{ route('seller.resetpassword') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Password</a>
                        </li>
                        <li>
                            <a href="{{ route('seller.newsletter') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Newsletter</a>
                        </li>
                        <li>
                            <a href="{{ route('seller.hapus.akun') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Hapus
                                Akun</a>
                        </li>
                    </ul>
                </li>

                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="{{ route('listPesanan') }}" class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/cart-shopping-solid-white.svg') }}"
                            class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Pesanan</p>
                    </a>
                </li>
                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="{{ route('viewLaporanSeller') }}" class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/file-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Laporan</p>
                    </a>
                </li>
                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="{{ route('inbox.seller') }}" class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/comment-solid-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Pesan</p>
                    </a>
                </li>

                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"
                        class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/logout-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Keluar</p>
                        <form id="logout-form" method="POST" action="{{ route('logout') }}">
                            @csrf
                        </form>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
