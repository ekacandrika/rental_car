<style>
    .disabled {
        pointer-events: none;
        cursor: default;
        text-decoration: none;
        cursor: not-allowed;
    }
</style>

<div class="grid grid-cols-10 sticky top-0 z-40">
    {{-- Sidebar --}}
    <div class="col-start-1 col-end-3 bg-[#9E3D64] border-b-2 border-[#FFFFFF]">
        <div class="bg-[#9E3D64] ">
            <a href="#" class="p-2 pt-3 flex flex-col items-center justify-center">
                <img src="{{ asset('storage/logos/kamtuu-white.png') }}" class="h-8 sm:h-8 px-[10px]" alt="Kamtuu Logo">
            </a>
        </div>
    </div>

    {{-- Navbar --}}
    <div class="col-start-3 col-end-11">
        <nav class="bg-white border-gray-200 px-2 sm:px-4 dark:bg-gray-900 right-0 top-0 sticky drop-shadow-xl">
            <div class="flex flex-row items-center justify-between gap-6 p-2">
                <span class="text-xl font-semibold font-inter"> </span>
                <div class="flex flex-row items-center gap-4 pt-3 menu-dropdown">
                    <ul>
                        {{-- <img src="{{ asset('storage/img/logo-traveller.png') }}" alt="user-profile"
                            class="object-cover object-center w-8 h-8 overflow-hidden rounded-full border-indigo-600 ">
                        --}}
                        <li class="{{isset(auth()->user()->active_status) ? (auth()->user()->active_status=='Confrimed'? 'dropdown':'disabled') :'disabled'}}">
                            <a href="#">
                                <img src="{{ isset(auth()->user()->profile_photo_path) ? asset(auth()->user()->profile_photo_path) : asset('storage/img/logo-traveller.png') }}"
                                    alt="user-profile"
                                    class="object-cover object-center w-8 h-8 overflow-hidden rounded-full border-indigo-600 ">
                            </a>
                            <ul class="isi-dropdown">
                                <li><a href="{{ route('dashboardkelolatoko') }}">Kelola Toko</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
