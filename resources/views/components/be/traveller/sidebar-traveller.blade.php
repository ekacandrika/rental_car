<div class="lg:grid lg:grid-cols-10 max-h-screen h-screen">
    <div class="col-start-1 col-end-3 bg-[#9E3D64]" x-init="show = window.innerWidth > 1023 ? true : false"
        x-data="{ show: false }" @resize.window=" show = (window.innerWidth > 1023) ? true : false;">
        <div class="lg:block sticky z-20 inset-0 top-[3.8125rem] right-auto overflow-y-auto w-full ">
            <div class="flex lg:hidden justify-center my-3">
                <label
                    class="items-center block px-3 py-2 text-white border rounded cursor-pointer lg:hidden hover:text-white hover:border-white"
                    for="menu-toggle" @click="show = !show">
                    <svg class="w-5 h-5 fill-current" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <title>Menu</title>
                        <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
                    </svg>
                </label>
            </div>
            @php
                $traveller_datas = DB::select("SELECT
                    a.first_name,
                    a.role,
                    a.email,
                    a.no_tlp,
                    a.jk,
                    a.email_verified_at,
                    a.profile_photo_path,
                    b.last_name,
                    b.date_of_birth,
                    b.address,
                    b.province_id,
                    b.regency_id,
                    b.district_id,
                    b.village_id
                FROM
                    users a
                    JOIN user_traveller b ON b.user_id = a.id 
                WHERE
                a.id =".auth()->user()->id);
                
                $total_datas = null;
                $kelengkapan = 0;

                foreach($traveller_datas as $key => $data){
                    $total_datas = count(collect($data));

                    if($data->first_name){
                        $kelengkapan +=1;
                    }

                    if($data->role){
                        $kelengkapan +=1;
                    }

                    if($data->email){
                        $kelengkapan +=1;
                    }

                    if($data->no_tlp){
                        $kelengkapan +=1;
                    }

                    if($data->jk){
                        $kelengkapan +=1;
                    }

                    if($data->email_verified_at){
                        $kelengkapan +=1;
                    }

                    if($data->profile_photo_path){
                        $kelengkapan +=1;
                    }

                    if($data->last_name){
                        $kelengkapan +=1;
                    }

                    if($data->date_of_birth){
                        $kelengkapan +=1;    
                    }

                    if($data->address){
                        $kelengkapan +=1;
                    }

                    if($data->province_id){
                        $kelengkapan +=1;
                    }

                    if($data->regency_id){
                        $kelengkapan +=1;
                    }

                    if($data->district_id){
                        $kelengkapan +=1;
                    }

                    if($data->village_id){
                        $kelengkapan +=1;
                    }

                }

                $persentase = round(($kelengkapan/$total_datas)*100);
            @endphp
            <aside class="inset-y-0 left-0 overflow-y-scroll" aria-label="Sidebar" x-show="show">
                <div>
                    <ul>
                        <li class="block py-3 space-y-3">
                            @if (auth()->user()->profile_photo_path == null)
                            <div class="flex flex-row items-center gap-4 justify-center">
                                <img src="{{ asset('storage/img/logo-traveller.png') }}" alt="user-profile"
                                    class="object-cover object-center w-12 h-12 overflow-hidden rounded-full border-indigo-600 ">
                            </div>
                            @else
                            <div class="flex flex-row items-center gap-4 justify-center">
                                <img src="{{ asset(auth()->user()->profile_photo_path) }}" alt="user-profile"
                                    class="object-cover object-center w-12 h-12 overflow-hidden rounded-full border-indigo-600 ">
                            </div>
                            @endif
                            @if (auth()->user()->first_name == null)
                            <div class="flex flex-row items-center gap-4 justify-center">
                                <p class="whitespace-nowrap font-bold text-white">Nama Traveller</p>
                            </div>
                            @else
                            <div class="flex flex-row items-center gap-4 justify-center">
                                <p class="whitespace-nowrap font-bold text-white">{{ auth()->user()->first_name }}
                                </p>
                            </div>
                            @endif
                            <div class="flex flex-row items-center gap-4 justify-center">
                                <div class="w-[75%] bg-gray-200 rounded-full dark:bg-gray-700 border-[#FFB800]">
                                    <div class="bg-[#FFB800] text-xs font-medium text-[#9E3D64] text-center p-0.5 leading-none rounded-full"
                                        style="width: {{$persentase}}%"> {{$persentase}}%
                                    </div>
                                </div>
                            </div>
                            
                        </li>
                        <li>

                            <label
                                class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                                for="akun">
                                <img src="{{ asset('storage/icons/circle-user-solid-white.svg') }}"
                                    class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                                <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Akunku</p>
                                <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                                    alt="user" title="user">
                            </label>
                            <input class="hidden" type="checkbox" id="akun" />
                            <ul id="akun" class="hidden">
                                <li>
                                    <a href="{{ route('profile') }}"
                                        class="flex items-center p-2 pl-11 font-inter font-medium text-white hover:text-[#FFB800] hover:font-bold text-sm">Profil</a>
                                </li>
                                <li>
                                    <a href="{{ route('infobooking') }}"
                                        class="flex items-center p-2 pl-11 font-inter font-medium text-white hover:text-[#FFB800] hover:font-bold text-sm">Info
                                        Booking</a>
                                </li>
                                <li>
                                    <a href="{{ route('traveller.resetpassword') }}"
                                        class="flex items-center p-2 pl-11 font-inter font-medium text-white hover:text-[#FFB800] hover:font-bold text-sm">Password</a>
                                </li>
                                <li>
                                    <a href="{{ route('newsletter') }}"
                                        class="flex items-center p-2 pl-11 font-inter font-medium text-white hover:text-[#FFB800] hover:font-bold text-sm">Newsletter</a>
                                </li>
                                <li>
                                    <a href="{{ route('traveller.kupon') }}"
                                        class="flex items-center p-2 pl-11 font-inter font-medium text-white hover:text-[#FFB800] hover:font-bold text-sm">Get
                                        Kupon</a>
                                </li>
                                <li>
                                    <a href="{{ route('hapusakun') }}"
                                        class="flex items-center p-2 pl-11 font-inter font-medium text-white hover:text-[#FFB800] hover:font-bold text-sm">Hapus
                                        Akun</a>
                                </li>
                            </ul>
                        </li>
                        <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                            <a href="{{ route('cart') }}" class="flex items-center p-2 font-bold text-white">
                                <img src="{{ asset('storage/icons/cart-shopping-solid-white.svg') }}"
                                    class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                                <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Pesanan</p>
                            </a>
                        </li>
                        <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                            <a href="{{ route('favorit') }}" class="flex items-center p-2 font-bold text-white">
                                <img src="{{ asset('storage/icons/star-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                                    alt="user" title="user">
                                <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Favorit</p>
                            </a>
                        </li>
                        <li>
                            <label
                                class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                                for="inbox">
                                <img src="{{ asset('storage/icons/comment-solid-white.svg') }}"
                                    class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                                <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Inbox</p>
                                <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                                    alt="user" title="user">
                            </label>

                            <input class="hidden" type="checkbox" id="inbox" />

                            <ul id="inbox" class="hidden">
                                <li>
                                    <a href="{{ route('inboxTraveller') }}"
                                        class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Pesan</a>
                                </li>
                                <li>
                                    <a href="{{ route('faq') }}"
                                        class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">FAQ</a>
                                </li>
                                <li>
                                    <a href="{{ route('support') }}"
                                        class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Dukungan</a>
                                </li>
                            </ul>
                        </li>
                        <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"
                                class="flex items-center p-2 font-bold text-white">
                                <img src="{{ asset('storage/icons/logout-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                                    alt="user" title="user">
                                <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Keluar</p>
                                <form id="logout-form" method="POST" action="{{ route('logout') }}">
                                    @csrf
                                </form>
                            </a>
                        </li>
                    </ul>
                </div>
            </aside>
        </div>
    </div>

    {{ $slot }}
</div>
