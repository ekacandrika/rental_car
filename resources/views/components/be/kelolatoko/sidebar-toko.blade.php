@php
    $id_seller = auth()->user()->id;
    $toko = App\Models\Kelolatoko::where('id_seller',$id_seller)->first();
@endphp
<div class="col-start-1 col-end-3 bg-[#9E3D64] min-h-[1024px]">
    <aside class="w-70 relative" aria-label="Sidebar">
        <div class="overflow-y-auto py-4 inset-y-0 top-[3.8125rem]">
            <div class="py-2">
                <div class="px-4 pt-2 flex flex-col items-center justify-center">
                    <img src="{{ isset(auth()->user()->profile_photo_path) ? asset(auth()->user()->profile_photo_path) : asset('storage/img/logo-traveller.png') }}"
                        alt="user-profile" class="object-cover object-center w-14 h-14 rounded-full">

                    <p class="text-base font-inter font-semibold text-[#FFFFFF] mt-3">
                        {{ isset(auth()->user()->first_name) ? auth()->user()->first_name : 'Nama Seller' }}</p>
                </div>
            </div>
            <ul class="space-y-2">
                <li>
                    <a href="{{ route('dashboardseller') }}"
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="akun">
                        <img src="{{ asset('storage/icons/house-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Halaman Depan</p>
                    </a>
                </li>

                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="/dashboard/seller/dekorasi/toko" class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/pen-square-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Dekorasi Toko</p>
                    </a>
                </li>
                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="{{route('detail.toko')}}" class="flex items-center p-2 font-bold text-white">
                        {{-- <img src="{{ asset('storage/icons/pen-square-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user"> --}}
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-[16px] h-[16px] mt-1" height="1em" viewBox="0 0 576 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><style>svg{fill:#f7f7f7}</style><path d="M547.6 103.8L490.3 13.1C485.2 5 476.1 0 466.4 0H109.6C99.9 0 90.8 5 85.7 13.1L28.3 103.8c-29.6 46.8-3.4 111.9 51.9 119.4c4 .5 8.1 .8 12.1 .8c26.1 0 49.3-11.4 65.2-29c15.9 17.6 39.1 29 65.2 29c26.1 0 49.3-11.4 65.2-29c15.9 17.6 39.1 29 65.2 29c26.2 0 49.3-11.4 65.2-29c16 17.6 39.1 29 65.2 29c4.1 0 8.1-.3 12.1-.8c55.5-7.4 81.8-72.5 52.1-119.4zM499.7 254.9l-.1 0c-5.3 .7-10.7 1.1-16.2 1.1c-12.4 0-24.3-1.9-35.4-5.3V384H128V250.6c-11.2 3.5-23.2 5.4-35.6 5.4c-5.5 0-11-.4-16.3-1.1l-.1 0c-4.1-.6-8.1-1.3-12-2.3V384v64c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V384 252.6c-4 1-8 1.8-12.3 2.3z"/></svg>    

                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Detail Toko</p>
                    </a>
                </li>
                @if ($toko)
                    
                    <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                        <a href="/dashboard/myListing" class="flex items-center p-2 font-bold text-white">
                            <img src="{{ asset('storage/icons/list-ul-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                                alt="user" title="user">
                            <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">My Listing</p>
                        </a>
                    </li>                
                    
                @endif


                @php
                $lisensi = json_decode(auth()->user()->lisensi);
                // dump(auth()->user());
                $personal_tur='';
                $personal_hotel='';
                $bussiness_tour_hotel='';

                $personal_transfer='';
                $personal_rental='';
                $bussiness_transfer_rental='';

                $personal_activity='';
                $personal_xstay='';
                $bussiness_activity_xstay='';

                if($lisensi){
                foreach($lisensi as $key => $v){
                $personal_tur = isset($v->personal_tur) ? $v->personal_tur : '';
                $personal_hotel=isset($v->personal_hotel) ? $v->personal_hotel : '';
                $bussiness_tour_hotel=isset($v->bussiness_tour_hotel) ? $v->bussiness_tour_hotel : '';

                $personal_transfer=isset($v->personal_transfer) ? $v->personal_transfer : '';
                $personal_rental=isset($v->personal_rental) ? $v->personal_rental : '';
                $bussiness_transfer_rental=isset($v->bussiness_transfer_rental) ? $v->bussiness_transfer_rental : '';

                $personal_activity=isset($v->personal_activity) ? $v->personal_activity : '';
                $personal_xstay=isset($v->personal_xstay) ? $v->personal_xstay : '';
                $bussiness_activity_xstay=isset($v->bussiness_activity_xstay) ? $v->bussiness_activity_xstay : '';
                }
                }
                @endphp
                @if($toko)
                @if(auth()->user()->jenis_registrasi=='personal')
                @if($personal_tur)
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="tur">
                        <img src="{{ asset('storage/icons/suitcase-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Tur</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="tur" />

                    <ul id="tur" class="hidden">
                        <li>
                            <a href="/dashboard/seller/tur/add-informasi"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Tambah
                                Listing</a>
                        </li>
                        <li>
                            <a href="/dashboard/seller/tur/faq"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">FAQs</a>
                        </li>
                    </ul>
                </li>
                @endif
                @if($personal_hotel)
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="hotel">
                        <img src="{{ asset('storage/icons/hotel-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Hotel</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="hotel" />

                    <ul id="hotel" class="hidden">
                        <li>
                            <a href="{{ isset($addListingHotel) ? $addListingHotel:route('hotelinfodasar') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Tambah
                                Listing</a>
                        </li>
                        <li>
                            <a href="{{ route('kamarhotel') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Kamar</a>
                        </li>
                        <li>
                            <a href="{{ isset($faq) ? $faq:route('hotelfaq')}}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">FAQs</a>
                        </li>
                    </ul>
                </li>
                @endif
                @endif

                @if(auth()->user()->jenis_registrasi=='company')
                @if($bussiness_tour_hotel)
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="tur">
                        <img src="{{ asset('storage/icons/suitcase-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Tur</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="tur" />

                    <ul id="tur" class="hidden">
                        <li>
                            <a href="/dashboard/seller/tur/add-informasi"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Tambah
                                Listing</a>
                        </li>
                        <li>
                            <a href="/dashboard/seller/tur/faq"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">FAQs</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="hotel">
                        <img src="{{ asset('storage/icons/hotel-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Hotel</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="hotel" />

                    <ul id="hotel" class="hidden">
                        <li>
                            <a href="{{ isset($addListingHotel) ? $addListingHotel:route('hotelinfodasar') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Tambah
                                Listing</a>
                        </li>
                        <li>
                            <a href="{{ route('kamarhotel') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Kamar</a>
                        </li>
                        <li>
                            {{-- hotelfaq --}}
                            <a href="{{ isset($faq) ? $faq:route('hotelfaq')}}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">FAQs</a>
                        </li>
                    </ul>
                </li>
                @endif
                @endif

                @if(auth()->user()->jenis_registrasi=='personal')
                @if($personal_transfer)
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="transfer">
                        <img src="{{ asset('storage/icons/taxi-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Transfer</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="transfer" />

                    <ul id="transfer" class="hidden">
                        <li>
                            <a href="/dashboard/seller/transfer/add-informasi"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Tambah
                                Listing</a>
                        </li>
                        <li>
                            <a href="#"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Driver</a>
                        </li>
                        <li>
                            <a href="/dashboard/seller/transfer/master-kendaraan/all"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Mobil</a>
                        </li>
                        <li>
                            <a href="/dashboard/seller/transfer/faq"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">FAQs</a>
                        </li>
                    </ul>
                </li>
                @endif
                @if($personal_rental)
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="rental">
                        <img src="{{ asset('storage/icons/car-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Rental</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="rental" />

                    <ul id="rental" class="hidden">
                        <li>
                            <a href="/dashboard/seller/rental/add-informasi"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Tambah
                                Listing</a>
                        </li>
                        <li>
                            <a href="#"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Driver</a>
                        </li>
                        <li>
                            <a href="/dashboard/seller/transfer/master-kendaraan/all"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Mobil</a>
                        </li>
                        <li>
                            <a href="/dashboard/seller/rental/faq"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">FAQs</a>
                        </li>
                    </ul>
                </li>
                @endif
                @endif

                @if(auth()->user()->jenis_registrasi=='company')
                @if($bussiness_transfer_rental)
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="rental">
                        <img src="{{ asset('storage/icons/car-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Rental</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="rental" />

                    <ul id="rental" class="hidden">
                        <li>
                            <a href="/dashboard/seller/rental/add-informasi"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Tambah
                                Listing</a>
                        </li>
                        <li>
                            <a href="#"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Driver</a>
                        </li>
                        <li>
                            <a href="/dashboard/seller/transfer/master-kendaraan/all"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Mobil</a>
                        </li>
                        <li>
                            <a href="/dashboard/seller/rental/faq"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">FAQs</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="transfer">
                        <img src="{{ asset('storage/icons/taxi-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Transfer</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="transfer" />

                    <ul id="transfer" class="hidden">
                        <li>
                            <a href="/dashboard/seller/transfer/add-informasi"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Tambah
                                Listing</a>
                        </li>
                        <li>
                            <a href="#"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Driver</a>
                        </li>
                        <li>
                            <a href="/dashboard/seller/transfer/master-kendaraan/all"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Mobil</a>
                        </li>
                        <li>
                            <a href="/dashboard/seller/transfer/faq"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">FAQs</a>
                        </li>
                    </ul>
                </li>
                @endif
                @endif

                @if(auth()->user()->jenis_registrasi=='personal')
                @if($personal_activity)
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="activity">
                        <img src="{{ asset('storage/icons/activity.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Activity</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="activity" />

                    <ul id="activity" class="hidden">
                        <li>
                            <a href="/dashboard/seller/activity/add-detail"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Tambah
                                Listing</a>
                        </li>
                        <li>
                            <a href="/dashboard/seller/activity/faq"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">FAQs</a>
                        </li>
                    </ul>
                </li>
                @endif
                @if($personal_xstay)
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="xstay">
                        <img src="{{ asset('storage/icons/xstay.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">XStay</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="xstay" />

                    <ul id="xstay" class="hidden">
                        <li>
                            <a href="{{ isset($addListingXstay) ? $addListingXstay :route('infodasar') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Tambah
                                Listing</a>
                        </li>
                        <li>
                            <a href="{{ route('kamarhotel') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Kamar</a>
                        </li>
                        <li>
                            <a href="{{ isset($faqXstay) ? $faqXstay:route('faq.index') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">FAQs</a>
                        </li>
                    </ul>
                </li>
                @endif
                @endif

                @if(auth()->user()->jenis_registrasi=='company')
                @if($bussiness_activity_xstay)
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="activity">
                        <img src="{{ asset('storage/icons/activity.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Activity</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="activity" />

                    <ul id="activity" class="hidden">
                        <li>
                            <a href="/dashboard/seller/activity/add-detail"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Tambah
                                Listing</a>
                        </li>
                        <li>
                            <a href="/dashboard/seller/activity/faq"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">FAQs</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="xstay">
                        <img src="{{ asset('storage/icons/xstay.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">XStay</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1" alt="user"
                            title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="xstay" />

                    <ul id="xstay" class="hidden">
                        <li>
                            <a href="{{ isset($addListingXstay) ? $addListingXstay :route('infodasar') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Tambah
                                Listing</a>
                        </li>
                        <li>
                            <a href="{{ route('kamarhotel') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Kamar</a>
                        </li>
                        <li>
                            <a href="{{ isset($faqXstay) ? $faqXstay:route('faq.index') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">FAQs</a>
                        </li>
                    </ul>
                </li>
                @endif
                @endif
                @endif
                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"
                        class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/logout-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Keluar</p>
                        <form id="logout-form" method="POST" action="{{ route('logout') }}">
                            @csrf
                        </form>
                    </a>
                </li>
            </ul>
        </div>
    </aside>
</div>