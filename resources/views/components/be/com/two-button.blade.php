<div class="p-5">
    <div class="grid grid-cols-6">
        <div class="col-start-1 col-end-2">
            <button type="submit" class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]">
                Sebelumnya                
            </button>
        </div>
        <div class="col-start-6 col-end-7">
            <button type="submit" class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                Selanjutnya
            </button>
        </div>
    </div>
</div>