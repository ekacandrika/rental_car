<div class="grid grid-cols-10">
    <div class="col-start-1 col-end-3">
        <div
            class="bg-[#9E3D64] hidden lg:block fixed z-20 inset-0 top-[3.8125rem] right-auto overflow-y-auto w-[270px]">
            <ul>
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="akun">
                        <img src="{{ asset('storage/icons/dasbor-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="dashboard" title="dashboard">
                        <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">Dashboard</p>
                    </label>
                </li>

                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="akun">

                        <img src="{{ asset('storage/icons/circle-user-solid-white.svg') }}"
                            class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                        <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">Akun</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="akun" />

                    <ul id="akun" class="hidden">
                        <li>
                            <a href="{{ route('viewMyAccount') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Profil</a>
                        </li>
                        {{-- <li> --}}
                        {{-- <a href="#" --}} {{--
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Informasi
                                --}}
                        {{-- Bank</a> --}}
                        {{-- </li> --}}
                        <li>
                            <a href="/dashboard-agent/password"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Password</a>
                        </li>
                        {{-- <li> --}}
                        {{-- <a href="#" --}} {{--
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Newsletter</a>
                            --}}
                        {{-- </li> --}}
                        <li>
                            <a href="#"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Hapus
                                Akun</a>
                        </li>
                    </ul>
                </li>

                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="{{ route('viewMyOrder') }}" class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/file-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">My Orders</p>
                    </a>
                </li>

                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="{{ route('viewLaporanAgent') }}" class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/file-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">Laporan</p>
                    </a>
                </li>
                {{-- <li> --}}
                {{-- <label --}} {{--
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        --}} {{-- for="report"> --}}
                {{-- <img src="{{ asset('storage/icons/file-white.svg') }}" class="w-[16px] h-[16px] mt-1" --}}
                {{-- alt="user" title="user"> --}}
                {{-- <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">Report</p> --}}
                {{-- <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1" --}}
                {{-- alt="user" title="user"> --}}
                {{-- </label> --}}

                {{-- <input class="hidden" type="checkbox" id="report" /> --}}

                {{-- <ul id="report" class="hidden"> --}}
                {{-- <li> --}}
                {{-- <a href="#" --}} {{--
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Order</a>
                            --}}
                {{-- </li> --}}
                {{-- </ul> --}}
                {{-- </li> --}}
                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="/dashboard-agent/add-user-agent" class="flex items-center p-2 font-bold text-white">
                        {{-- <img src="{{ asset('storage/icons/comment-solid-white.svg') }}" --}} <img src="{{ asset('storage/icons/icon-group.svg') }}"
                            class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                        <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">User Agen</p>
                    </a>
                </li>

                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                document.getElementById('logout-form').submit();"
                        class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/logout-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">Keluar</p>
                        <form id="logout-form" method="POST" action="{{ route('logout') }}">
                            @csrf
                        </form>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
