<div class="grid grid-cols-10">
    <div class="col-start-2 col-end-3">
        <div
            class="bg-[#9E3D64] hidden lg:block fixed z-20 inset-0 top-[3.8125rem] right-auto overflow-y-auto w-[270px]">
            <ul>
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="akun">
                        <img src="{{ asset('storage/icons/house-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Halaman Depan</p>
                    </label>
                </li>

                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="akun">
                        <img src="{{ asset('storage/icons/circle-user-solid-white.svg') }}"
                            class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Akunku</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="akun" />

                    <ul id="akun" class="hidden">
                        <li>
                            <a href="#"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Profil</a>
                        </li>
                        <li>
                            <a href="#"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Info
                                Booking</a>
                        </li>
                        <li>
                            <a href="#"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Password</a>
                        </li>
                        <li>
                            <a href="#"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Newsletter</a>
                        </li>
                        <li>
                            <a href="#"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Hapus
                                Akun</a>
                        </li>
                    </ul>
                </li>

                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="#" class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/cart-shopping-solid-white.svg') }}"
                            class="w-[16px] h-[16px] mt-1" alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Pesanan</p>
                    </a>
                </li>
                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="#" class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/star-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Favorit</p>
                    </a>
                </li>


                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="inbox">
                        <img src="{{ asset('storage/icons/comment-solid-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Inbox</p>
                        <img src="{{ asset('storage/icons/down-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                    </label>

                    <input class="hidden" type="checkbox" id="inbox" />

                    <ul id="inbox" class="hidden">
                        <li>
                            <a href="#"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Pesan</a>
                        </li>
                        <li>
                            <a href="#"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">FAQ</a>
                        </li>
                        <li>
                            <a href="#"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Dukungan</a>
                        </li>
                    </ul>
                </li>

                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"
                        class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/logout-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Keluar</p>
                        <form id="logout-form" method="POST" action="{{ route('logout') }}">
                            @csrf
                        </form>
                    </a>
                </li>
            </ul>
        </div>
    </div>
