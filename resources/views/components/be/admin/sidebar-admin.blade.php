<div class="grid grid-cols-10">
    <div class="col-start-1 col-end-3">
        <div
            class="bg-[#9E3D64] hidden lg:block fixed z-20 inset-0 top-[3.8125rem] right-auto overflow-y-auto w-[270px]">
            <ul>
                <li
                    class="items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]">
                    <a class="flex" href="/dashboard/admin/kamtuu">
                        <img src="{{ asset('storage/icons/dasbor-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">Dashboard</p>
                    </a>
                </li>

                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="{{ route('formulirList') }}" class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/ic-excel(1).svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">Formulir List</p>
                    </a>
                </li>

                {{-- <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2"> --}}
                {{-- <a href="/dashboard/admin/kamtuu/master-user"
                        class="flex items-center p-2 font-bold text-white"> --}}
                {{-- <img src="{{ asset('storage/icons/data-master.svg') }}" class="w-[16px] h-[16px] mt-1" --}}
                {{-- alt="user" title="user"> --}}
                {{-- <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">Master User</p> --}}
                {{-- </a> --}}
                {{-- </li> --}}

                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="idn">
                        <img src="{{ asset('storage/icons/pen-square-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">Destindonesia</p>
                    </label>

                    <input class="hidden" type="checkbox" id="idn" />

                    <ul id="idn" class="hidden">
                        {{-- <li> --}}
                        {{-- <a href="/dashboard/admin/kamtuu/master-tag"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Tag</a> --}}
                        {{-- </li> --}}
                        <li>
                            <a href="{{ route('objek') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Lokasi</a>
                        </li>
                        <li>
                            <a href="{{ route('wisata') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Objek</a>
                        </li>
                        {{-- <li>
                            <a href="/admin/kamtuu/master-place"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Place</a>
                        </li> --}}
                        <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                            <a href="{{ route('newsletter.index') }}"
                                class="flex items-center p-2 font-bold text-white">
                                <img src="{{ asset('storage/icons/newsletter.svg') }}" class="w-[16px] h-[16px] mt-1"
                                    alt="user" title="user">
                                <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">Newsletter</p>
                            </a>
                        </li>
                        <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                            <a href="{{ route('viewPengajuanPenarikan') }}"
                                class="flex items-center p-2 font-bold text-white">
                                <img src="{{ asset('storage/icons/file-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                                    alt="user" title="user">
                                <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">Pengajuan
                                    Penarikan
                                </p>
                            </a>
                        </li>
                        <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                            <a href="{{ route('viewLaporan') }}" class="flex items-center p-2 font-bold text-white">
                                <img src="{{ asset('storage/icons/file-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                                    alt="user" title="user">
                                <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">Laporan</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <label
                        class="flex items-center p-2 px-4 font-bold text-white hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800]"
                        for="sett">
                        <img src="{{ asset('storage/icons/pen-square-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">Setting</p>
                    </label>

                    <input class="hidden" type="checkbox" id="sett" />

                    <ul id="sett" class="hidden">
                        <li>
                            <a href="{{route('confrim.seller-account.index')}}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Confirmation Seller Account</a>
                        </li>
                        <li>
                            <a href="{{route('accListing')}}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Confirmation List Pesanan</a>
                        </li>
                        <li>
                            <a href="/dashboard/admin/kamtuu/section"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Create
                                Section</a>
                        </li>
                        <li>
                            <a href="{{ route('faqs.index') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Create
                                FAQs</a>
                        </li>
                        <li>
                            <a href="{{ route('review.moderator') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">
                                Moderasi Ulasan</a>
                        </li>
                        <li>
                            <a href="{{ route('extra.index') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Create
                                Extra</a>
                        </li>
                        <li>
                            <a href="{{ route('masterroute.index') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                routes</a>
                        </li>
                        <li>
                            <a href="{{ route('master-titik.index') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Titik</a>
                        </li>
                        <li>
                            <a href="{{ route('user.admin-add') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">
                                User
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('pajak') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Pajak</a>
                        </li>

                        <li>
                            <a href="{{ route('kupon') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">
                                Kupon</a>
                        </li>

                        <li>
                            <a href="{{ route('order.regency') }}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Create
                                Setting Regency Order</a>
                        </li>
                        <li>
                            <a
                            {{-- href="/admin/master/pilihan" --}}
                            href="{{route('choice.index')}}"
                            class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master Pilihan</a>
                        </li>
                        <li>
                            <a
                            {{-- href="/admin/master/pilihan" {{route('choice.index')}} --}}
                            href="{{route('legal.index')}}"
                            class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master Dokumen Legal</a>
                        </li>
                        {{-- <li>
                            <a href="{{route('slider-image.index')}}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Slider
                                Image</a>
                        </li>
                        <li>
                            <a href="{{route('slider-image.index')}}"
                                class="flex items-center p-2 pl-11 font-inter font-medium text-white w-auto hover:text-[#FFB800] hover:font-bold text-sm">Master
                                Route Image</a>
                        </li> --}}
                    </ul>
                </li>


                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                        class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/logout-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 mt-1 ml-2 text-sm whitespace-nowrap font-inter bold">Keluar</p>
                        <form id="logout-form" method="POST" action="{{ route('logout') }}">
                            @csrf
                        </form>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
