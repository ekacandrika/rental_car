<div class="col-start-1 col-end-3 bg-[#9E3D64] min-h-[1024px]">
    <aside class="w-70 relative" aria-label="Sidebar">
        <div class="overflow-y-auto py-4 px-3 inset-y-0 fixed top-[3.8125rem]">
            <div class="py-2">
                <div class="px-4 pt-2 flex flex-col items-center justify-center">
                    {{-- <p class="text-base font-inter font-semibold text-[#FFFFFF] mt-[2px]">Visitor</p> --}}
                </div>
            </div>
            <ul class="space-y-2 w-[245px]">
                <li class="hover:bg-[#872F52] hover:border-l-4 hover:border-[#FFB800] px-2">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"
                        class="flex items-center p-2 font-bold text-white">
                        <img src="{{ asset('storage/icons/logout-white.svg') }}" class="w-[16px] h-[16px] mt-1"
                            alt="user" title="user">
                        <p class="flex-1 ml-2 mt-1 whitespace-nowrap font-inter text-sm bold">Keluar</p>
                        <form id="logout-form" method="POST" action="{{ route('logout') }}">
                            @csrf
                        </form>
                    </a>
                </li>
            </ul>
        </div>
    </aside>
</div>