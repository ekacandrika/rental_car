<!DOCTYPE html>
{{--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">--}}

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
{{--    <meta name="csrf-token" content="{{ csrf_token() }}">--}}

    <title>{{ config('app.name', 'Destindonesia') }}</title>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>


    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js" defer></script>
    {{-- @vite(['resources/assets/js/gijgo.min.js']) --}}
    {{-- @vite(['resources/assets/css/summernote-lite.min.css', 'resources/assets/js/summernote-lite.min.js']) --}}

    <!-- Styles -->
    @livewireStyles

</head>

<body class="bg-white font-Inter">

<header>
    <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
</header>

<div class="max-w-7xl bg-[#F2F2F2] mx-auto rounded-md m-5">
    <div class="grid p-5 justify-items-center">
        <h1 class="font-semibold text-[26px]">Kirim Pesan ke Seller</h1>
    </div>

    <div class="p-5 sm:p-10">

        {{-- <div id="summernote" class="click2edit"></div> --}}


        <div>
            <p class="text-kamtuu-second text-[18px]">ID : {{Auth::user()->id}}</p>
            <p class="text-kamtuu-second text-[18px]">Inbox-ID : {{$inbox->inbox_code ?? "Send To Get Code"}}</p>
        </div>


        <div class="">
            <div class="flex flex-col items-center justify-center min-h-screen bg-gray-100 text-gray-800 p-10">

                <!-- Component Start -->
                <div class="flex flex-col flex-grow w-full max-w-xl bg-white shadow-xl rounded-lg overflow-hidden">
                    <div class="flex flex-col flex-grow h-0 p-4 overflow-auto">
                        <div class="flex w-full mt-2 space-x-3 max-w-xs">
                            <div class="flex-shrink-0 h-10 w-10 rounded-full bg-kamtuu-second"></div>
                            <div>
                                <div class="bg-kamtuu-second p-3 rounded-r-lg rounded-bl-lg">
                                    <p class="text-sm text-white">Apakah ada yang bisa kami bantu?</p>
                                </div>
                                <span class="text-xs text-gray-500 leading-none">1 second ago</span>
                            </div>
                        </div>
                        @foreach($message as $chat)
                            <div class="flex w-full mt-2 space-x-3 max-w-xs ml-auto justify-end">
                                <div>
                                    <div class="bg-kamtuu-primary text-white p-3 rounded-l-lg rounded-br-lg">
                                        <p class="text-sm">{{$chat->text}}</p>
                                    </div>
{{--                                    <span class="text-xs text-gray-500 leading-none">{{$chat->created_at->diffForHumans()}}</span>--}}
                                </div>
                                <div class="flex-shrink-0 h-10 w-10 rounded-full bg-gray-300">
                                </div>
                            </div>
                        @endforeach
                    </div>


                    <div class="bg-kamtuu-second p-4">
                        <form method="POST" action="{{ route('inbox.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="flex">
                                <input type="hidden" name="user2" value="{{$produk->user_id}}">
                                <input type="hidden" name="point2" value="{{$produk->user_id}}">
                                <input type="hidden" name="produk_id" value="{{$produk->id}}">
                                <input class="flex items-center h-10 w-full rounded px-3 text-sm" name="text"
                                       type="text" placeholder="Type your message… ">
                                <button type="submit"><img class="w-10 h-10" src="{{ asset('storage/icons/send.svg') }}"
                                                           alt=""></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Component End  -->

            </div>
        </div>

    </div>

</div>


<footer>
    <x-destindonesia.footer></x-destindonesia.footer>
</footer>
</body>
</html>
