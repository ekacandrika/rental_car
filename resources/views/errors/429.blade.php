<!DOCTYPE html>
<html>
<head>
    <title>Page not found</title>


    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>
<body>


<header>
    <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
</header>

<div class="error-container">
    <img class="animated-image" src="{{ asset('storage/images/traveller_error.png') }}" alt="traveler">
    <h1 class="animated-text">Oops! We can't find the page you're looking for.</h1>
    <h1 class="animated-text">Error 429 Too Many Request</h1>
    <button class="animated-button" onclick="history.back()">Go back</button>
</div>


<footer>
    <x-destindonesia.footer></x-destindonesia.footer>
</footer>
</body>
</html>
