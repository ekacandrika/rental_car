
            <form wire:submit.prevent="informasi">

                {{-- Informasi --}}
                
                @if ($currentStep == 1)
                <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded">
                    <div class="grid grid-cols-2">
                        <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Isi Informasi Dasar</div>
                        <div class="text-sm font-bold font-inter p-5 text-[#333333] text-right">ID Listing: 1TO101082022
                        </div>
                    </div>

                        <div class="px-5 mb-6">
                            <label for="" class="block mb-2 text-sm font-bold text-[#333333]">Judul</label>
                            <input type="text" id="nama_produk" wire:model="nama_produk" required
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Tur Sehat">
                        </div>

                        <fieldset class="px-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Konfirmasi
                                Paket</label>
                            <div class="grid grid-cols-2 w-80" required>
                                <div class="flex items-center mb-4">
                                    <input id="konfirmasi" type="radio" wire:model="konfirmasi" value="instant"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                        checked>
                                    <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                        Instant
                                    </label>
                                </div>
                                <div class="flex items-center mb-4">
                                    <input id="konfirmasi" type="radio" wire:model="konfirmasi" value="by seller"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                    <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                        Konfirmasi by Seller
                                    </label>
                                </div>
                            </div>
                        </fieldset>

                        <div class="block px-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag
                                Lokasi</label>
                            <select
                                class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                <option>D I Yogyakarta</option>
                                <option>Option 1</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>

                            <select
                                class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                <option>D I Yogyakarta</option>
                                <option>Option 1</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>
                        </div>

                        <div class="grid grid-cols-2">
                            <div class="flex">
                                <div class="px-5 my-5">
                                    <label for="" class="block mb-2 text-sm font-bold text-[#333333]">Hari</label>
                                    <input type="text" id="jumlah_hari" wire:model="jumlah_hari" required
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Hari">
                                </div>

                                <div class="pr-5 my-5">
                                    <label for="" class="block mb-2 text-sm font-bold text-[#333333]">Malam</label>
                                    <input type="text" id="jumlah_malam" wire:model="jumlah_malam" required
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Malam">
                                </div>
                            </div>
                        </div>

                        <fieldset class="px-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tipe
                                Tur</label>
                            <div class="grid grid-cols-2 w-80">
                                <div class="flex items-center mb-4">
                                    <input id="tipe" type="radio" wire:model="tipe" value="Tur Private"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                        checked>
                                    <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                        Tur Private
                                    </label>
                                </div>
                                <div class="flex items-center mb-4">
                                    <input id="tipe" type="radio" name="tipe" value="Open Trip"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                    <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                        Open Trip
                                    </label>
                                </div>
                            </div>
                        </fieldset>

                        <div class="block px-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tag
                                Lokasi</label>
                            <select
                                class="mt-1 px-3 py-2 bg-white border shadow-sm border-[#4F4F4F] text-gray-900 w-96 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md text-sm focus:ring-1">
                                <option>D I Yogyakarta</option>
                                <option>Option 1</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                            </div>
                        </div>

                        <div class="grid grid-cols-2">
                            <div class="flex">
                                <div class="px-5 my-5">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Peserta
                                        Min</label>
                                    <input type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="3">
                                </div>

                                <div class="pr-5 my-5">
                                    <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Peserta
                                        Max</label>
                                    <input type="text" id="text"
                                        class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-44 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="5">
                                </div>
                            </div>
                        </div>

                        <fieldset class="px-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Makanan</label>
                            <div class="grid grid-cols-3 w-[600px]">
                                <div class="flex items-center mb-4 w-40">
                                    <input id="makanan" type="radio" wire:model="makanan" value="Halal"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                        checked>
                                    <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                        Halal
                                    </label>
                                </div>
                                <div class="flex items-center mb-4 w-52">
                                    <input id="makanan" type="radio" wire:model="makanan" value="Vegetarian Available"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                    <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                        Vegetarian Available
                                    </label>
                                </div>
                                <div class="flex items-center mb-4 w-52">
                                    <input id="makanan" type="radio" wire:model="makanan"
                                        value="Halal & Vegetarian Available"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                    <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                        Halal & Vegetarian Available
                                    </label>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="px-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Bahasa</label>
                            <div class="grid grid-cols-3 w-[600px]">
                                <div class="flex items-center mb-4 w-40">
                                    <input id="bahasa" type="radio" wire:model="bahasa" value="Bahasa Indonesia"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                        checked="">
                                    <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                        Bahasa Indonesia
                                    </label>
                                </div>
                                <div class="flex items-center mb-4 w-52">
                                    <input id="bahasa" type="radio" wire:model="bahasa" value="English English"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                    <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                        English
                                    </label>
                                </div>
                                <div class="flex items-center mb-4 w-52">
                                    <input id="bahasa" type="radio" name="bahasa" value="Indonesia & English"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                    <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                        Indonesia & English
                                    </label>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="px-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Tingkat
                                Petualangan</label>
                            <div class="grid grid-cols-3 w-[600px]">
                                <div class="flex items-center mb-4 w-40">
                                    <input id="tingkat_petualangan" type="radio" wire:model="tingkat_petualangan" value="Ekstrim"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
                                        checked="">
                                    <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                        Ekstrim
                                    </label>
                                </div>
                                <div class="flex items-center mb-4 w-52">
                                    <input id="tingkat_petualangan" type="radio" wire:model="tingkat_petualangan" value="Semi Eksrim English"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                    <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                        Semi Eksrim
                                    </label>
                                </div>
                                <div class="flex items-center mb-4 w-52">
                                    <input id="tingkat_petualangan" type="radio" wire:model="tingkat_petualangan" value="Tidak Ekstrim"
                                        class="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600">
                                    <label for="" class="block ml-2 text-sm font-medium text-gray-900">
                                        Tidak Ekstrim
                                    </label>
                                </div>
                            </div>
                        </fieldset>

                        <div class="px-5 mb-5">
                            <label for="" class="block mb-2 text-sm font-bold text-[#333333]">Kategori Tur</label>
                            <input type="text" id="nama_kategori" name="nama_kategori" required
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Masukan kategori tur yang sesuai dengan paket">
                        </div>

                        <div class="px-5" wire:ignore>
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Deskripsi</label>
                            <textarea class="form-control w-[10rem]" wire:model="deskripsi" id="deskripsi"></textarea>
                        </div>

                        <div class="px-5 pt-5" wire:ignore>
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Paket
                                Termasuk</label>
                            <textarea class="form-control w-[10rem]" wire:model="paket_termasuk" id="paket_termasuk"></textarea>
                        </div>

                        <div class="px-5 pt-5" wire:ignore>
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Paket
                                Tidak Termasuk</label>
                            <textarea class="form-control w-[10rem]" wire:model="paket_tidak_termasuk" id="paket_tidak_termasuk"></textarea>
                        </div>

                        <div class="px-5 pt-5" wire:ignore>
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Catatan</label>
                            <textarea class="form-control w-[10rem]" wire:model="catatan" id="catatan"></textarea>
                        </div>

                </div>
                @endif

                {{-- Itinerary --}}

                @if ($currentStep == 2)
                <div class="bg-[#FFFFFF] drop-shadow-xl pb-5 mt-5 mb-5 rounded" x-data="itineraryData()">
                    <div class="flex justify-between">
                        <div class="text-lg font-bold font-inter p-5 text-[#9E3D64]">Buat Itinerary</div>
                        <div class="p-5">
                            <button class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]" @click="addNewField()"> Tambah </button>
                        </div>
                    </div>
                    
                    <template x-if="fields.length < 1">
                        <div class="">
                            <p>Klik tombol Tambah di atas untuk menambahkan.</p>
                        </div>
                    </template>

                    <template x-if="fields.length >= 1" class="col">
                        <template x-for="(field, index) in fields" :key="index">
                            <div class="container mx-auto px-5 space-y-3 py-3">
                                <p class="text-base font-semibold text-[#333333]" x-model="field.title"></p>
                                <span class="text-sm font-normal text-[#333333]" x-model="field.description"></span>
                                <div class="flex flex-wrap">
                                    <template x-if="field.images.length > 0">
                                        <template x-for="(image, index) in field.images" :key="index">
                                            <div class="w-[156px] h-[156px] border rounded-md mr-5 my-2">
                                                <img class="w-full h-full object-cover object-center" :src="image"
                                                    alt="">
                                            </div>
                                        </template>
                                    </template>
                                    <template x-if="field.video !== ''">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/lJIrF4YjHfQ"
                                            title="YouTube video player" frameborder="0"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                    </template>
                                </div>
                                <button
                                    class="bg-[#D50006] text-white text-sm font-semibold py-1 px-5 rounded-lg hover:bg-[#de252b]"
                                    @click="removeField(index)">Hapus</button>
                                <hr class="border-[#333333]" />
                            </div>
                        </template>
                    </template>


                    {{-- Add Itinerary --}}
                    <div class="p-5">
                        {{-- Error Notices --}}
                        <template x-if="error !== ''">
                            <div class="flex justify-center">
                                <p class="rounded-md px-3 py-2 bg-red-600 text-white font-semibold text-lg"
                                    x-text='error'>
                                </p>
                            </div>
                        </template>

                        {{-- Title --}}
                        <div class="mb-6">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Judul</label>
                            <input type="text" id="text"
                                class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Tur Sehat" x-model="title">
                        </div>

                        {{-- Description --}}
                        <div class="pt-5">
                            <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Catatan</label>
                            <div class="w-[10rem]" id="click2edit">
                            </div>
                        </div>

                        {{-- Image or Video Itinerary Checkbox --}}
                        <div class="flex mt-5 my-3 space-x-3">
                            <div class="form-check">
                                <input
                                    class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                    type="radio" name="radio_itinerary" id="radio_image" value="images"
                                    x-model="radio_itinerary">
                                <label class="form-check-label inline-block text-gray-800" for="radio_image">
                                    Image Itinerary
                                </label>
                            </div>
                            <div class="form-check">
                                <input
                                    class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                    type="radio" name="radio_itinerary" id="radio_video" value="videos"
                                    x-model="radio_itinerary">
                                <label class="form-check-label inline-block text-gray-800" for="radio_video">
                                    Video Itinerary
                                </label>
                            </div>
                        </div>

                        {{-- Photo --}}
                        <template x-if="radio_itinerary === 'images'">
                            <div class="py-2 mb-6">
                                <p class="font-semibold text-[#BDBDBD]">Foto</p>
                                <template x-if="images.length < 1">
                                    <div
                                        class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                        <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons"
                                            width="20px" height="20px">
                                    </div>
                                </template>
                                <template x-if="images.length >= 1">
                                    <div class="flex">
                                        <template x-for="(image, index) in images" :key="index">
                                            <div class="flex justify-center items-center">
                                                <img :src="image"
                                                    class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                    :alt="'upload'+index">
                                                <button class="absolute mx-2 translate-x-12 -translate-y-14"><img
                                                        src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                        width="25px" @click="removeImage(index)">
                                                </button>
                                            </div>
                                        </template>
                                    </div>
                                </template>
                                <input class="py-2" type="file" accept="image/*" @change="selectedFile" multiple>
                            </div>
                        </template>

                        {{-- Video --}}
                        <template x-if="radio_itinerary === 'videos'">
                            <div>
                                <p class="font-semibold text-[#BDBDBD]">Video</p>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Video URL (Youtube)" x-model="video">
                            </div>
                        </template>
                    </div>
                </div>
                @endif

                {{-- Harga & Ketersediaan --}}

                @if ($currentStep == 3)
            {{--
                {{-- Section 1 --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded">
                    <div class="flex justify-between">
                        <div class="text-lg font-bold font-inter text-[#9E3D64]">Harga Dalam Mata Uang Rupiah (IDR)
                        </div>
                    </div>

                    {{-- Residen and Non Residen --}}
                    <div class="grid grid-cols-2 pb-2">
                        <div class="text-sm font-bold text-[#333333]">Residen</div>
                        <div class="text-sm font-bold text-[#333333]">
                            <p>Non-Residen</p>
                            <p class=" text-xs font-normal">(Bila tidak diisi harga berlaku sama)</p>
                        </div>
                    </div>

                    <div class="grid grid-cols-2">
                        <div class="">
                            <p class="block md:hidden text-sm font-bold text-[#333333] mb-6">Residen</p>
                            <div class="mb-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Dewasa</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="120000">
                            </div>
                            <div class="mb-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Anak (5 - 12
                                    Tahun)</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="60000">
                            </div>
                            <div class="mb-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Balita (< 5
                                        Tahun)</label>
                                        <input type="text" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="0">
                            </div>
                            <button class="text-white bg-[#23AEC1] py-1 px-3 rounded-md">Perbarui</button>
                        </div>
                        <div>
                            <div class="block md:hidden text-sm font-bold text-[#333333] mb-6">
                                <p>Non-Residen</p>
                                <p class=" text-xs font-normal">(Bila tidak diisi harga berlaku sama)</p>
                            </div>

                            <div class="mb-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Dewasa</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="120000">
                            </div>
                            <div class="mb-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Anak (5 - 12
                                    Tahun)</label>
                                <input type="text" id="text"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="60000">
                            </div>
                            <div class="mb-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Balita (< 5
                                        Tahun)</label>
                                        <input type="text" id="text"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="0">
                            </div>
                        </div>
                    </div>

                    <div class="my-7" x-data="handler()">
                        <div class="mb-2">
                            <div class="text-lg font-bold font-inter text-[#9E3D64]">Diskon grup dari jumlah orang
                                dewasa
                                (IDR)
                            </div>
                        </div>
                        <table class="my-5 w-1/2 border table-auto rounded-md">
                            <thead>
                                <tr>
                                    <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Min Jumlah Orang</th>
                                    <th class="p-3 border border-slate-500 bg-[#BDBDBD]">Maks Jumlah Orang</th>
                                    <th class="p-3 border border-slate-500 bg-[#BDBDBD]">%</th>
                                    <th class="p-3 border border-slate-500 bg-[#BDBDBD]"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <template x-if="fields.length < 1">
                                    <div class="px-3">
                                        <div>
                                            <button type="button"
                                                class="px-3 py-1 my-3 text-sm text-white rounded-full btn btn-info bg-kamtuu-second"
                                                @click="addNewField()">Tambah +</button>
                                        </div>
                                    </div>
                                </template>
                                <template x-if="fields.length >= 1" class="col">
                                    <template x-for="(field, index) in fields" :key="index">
                                        <tr class="align-middle">
                                            <td class="p-3 border border-slate-500 text-center">
                                                <input type="text" id="text"
                                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                    placeholder="3">
                                            </td>
                                            <td class="p-3 border border-slate-500 text-center">
                                                <input type="text" id="text"
                                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                    placeholder="5">
                                            </td>
                                            <td class=" p-3 border border-slate-500 text-center">
                                                <input type="text" id="text"
                                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                    placeholder="5">
                                            </td>
                                            <template x-if="index === (fields.length - 1)">
                                                <td class="p-3 border border-slate-500 text-center">

                                                    <button class="mx-1"><img
                                                            src="{{ asset('storage/icons/delete-dynamic-data.png') }}"
                                                            alt="" width="16px" @click="removeField()"></button>
                                                    <button class="mx-1"><img
                                                            src="{{ asset('storage/icons/circle-plus-solid 1.svg')}}"
                                                            alt="" width="16px" @click="addNewField()"></button>
                                                </td>
                                            </template>
                                            <template x-if="index !== (fields.length - 1)">
                                                <td class="p-3 border border-slate-500 text-center">
                                                    <button class="mx-1"><img
                                                            src="{{ asset('storage/icons/delete-dynamic-data.png') }}"
                                                            alt="" width="16px" @click="removeField()"></button>
                                                </td>
                                            </template>
                                        </tr>
                                    </template>
                                </template>
                            </tbody>
                        </table>
                    </div>

                    <button class="text-white bg-[#23AEC1] py-1 px-3 rounded-md">Perbarui</button>

                </div>

                {{-- Section 2 --}}
                <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded">
                    {{-- <form> --}}
                        <div class="flex md:space-x-10">
                            <div class="mb-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333] ">Dari
                                    Tanggal</label>
                                <div
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <input type="text" id="datepickerTurFrom" placeholder="" x-model="date_from">
                                </div>
                            </div>
                            <div class="mb-6">
                                <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Sampai
                                    Tanggal</label>
                                <div
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <input type="text" id="datepickerTurUntil" placeholder="" readonly
                                        x-model="date_until">
                                </div>
                            </div>
                            <div class="mb-6">
                                <label for="text" class="flex mb-2 text-sm font-bold text-[#333333]"
                                    x-data="{ tooltip: false }" x-on:mouseover="tooltip = true"
                                    x-on:mouseleave="tooltip = false">x/- (%)>
                                    <img class="mx-2" width="15px"
                                        src="{{ asset('storage/icons/circle-info-solid (1) 1.svg') }}" alt="">
                                    <div x-show="tooltip"
                                        class="text-sm text-white absolute bg-[#9E3D64] rounded-lg p-2 transform -translate-y-8 translate-x-8">
                                        This is the Tooltip.
                                    </div>
                                </label>
                                <input type="text" id="datepickerFormulirTur"
                                    class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-52 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="" x-model="discount">
                            </div>
                        </div>
                        <button class="text-white bg-[#23AEC1] py-1 px-3 rounded-md"
                            @click="addNewField()">Tambah</button>
                        {{--
                    </form> --}}

                    <div class="grid grid-cols-10 my-5">
                        <div class="col-span-9 border border-slate-500 rounded-md ">
                            <div class="my-2 mx-3" x-data="{ open:false }">
                                <div class="flex border-b border-gray-200 pt-1 pb-2">
                                    <button
                                        class="text-lg sm:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg flex space-x-10"
                                        x-on:click="open = !open">
                                        <div class="cursor-pointer">
                                            <p for="text" class="block mb-2 text-sm font-bold text-slate-500">Dari
                                                Tanggal</p>
                                            <p class=" text-base font-bold text-[#333333]">01/09/2022</p>
                                        </div>
                                        <div class="cursor-pointer">
                                            <p for="text" class="block mb-2 text-sm font-bold text-slate-500">Sampai
                                                Tanggal</p>
                                            <p class=" text-base font-bold text-[#333333]">01/09/2022</p>
                                        </div>
                                        <div class="cursor-pointer">
                                            <p for="text" class="block mb-2 text-sm font-bold text-slate-500">+/- (%)
                                            </p>
                                            <p class=" text-base font-bold text-[#333333]">-20</p>
                                        </div>
                                    </button>
                                    <img class="float-right duration-200 cursor-pointer" :class="{'rotate-180' : open}"
                                        x-on:click="open = !open"
                                        src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}" alt="chevron-down"
                                        width="26px" height="15px">
                                </div>
                                <div class="py-2" x-show="open" x-transition>
                                    <table class="my-1 w-1/2 table-auto">
                                        <thead>
                                            <tr>
                                                <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]"></th>
                                                <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">Dewasa</th>
                                                <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">Anak</th>
                                                <th class="p-3 border-2 border-slate-500 bg-[#BDBDBD]">Balita</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="align-middle">
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>Residen</p>
                                                </td>
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>96000</p>
                                                </td>
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>48000</p>
                                                </td>
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>0</p>
                                                </td>
                                            </tr>
                                            <tr class="align-middle">
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>Non-Residen</p>
                                                </td>
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>192000</p>
                                                </td>
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>96000</p>
                                                </td>
                                                <td class="p-3 border-2 border-slate-500 text-center">
                                                    <p>0</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="px-3">
                            <button
                                class="bg-[#D50006] text-white text-sm font-semibold py-2 px-5 rounded-lg hover:bg-[#de252b]">
                                Hapus
                            </button>
                        </div>

                    </div>
                </div>
            --}}
                @endif 

                {{-- Batas Pembayaran --}}

                @if ($currentStep == 4)
                <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded">
                    <div class="flex justify-between">
                        <div class="text-lg font-bold font-inter text-[#9E3D64]">
                            Batas Waktu Pembayaran & Pembatalan
                        </div>
                    </div>
    
                    <div class="flex justify-items-center items-center align-middle my-2">
                        <span>Batas waktu pembayaran </span>
                        <input type="text" id=""
                            class="bg-[#FFFFFF] mx-2 border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-12 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="61">
                        <span>hari sebelum keberangkatan.</span>
                    </div>
    
                    <p class="text-xs text-[#D50006]">*Batas Waktu Pembayaran Lebih Lama Daripada Batas Waktu Pembatalan
                    </p>
    
                    <div class="my-5">
                        <p class="pb-1">Kebijakan Pembatalan:</p>
                        <button wire:click="addPembayaran()" type="button" class="text-white bg-[#23AEC1] py-1 px-3 rounded-md" >Tambah</button>

                        <div class="my-7">
                            <table class="my-5 w-full border table-auto rounded-md">
                                <thead>
                                    <tr>
                                        <th class="p-3 border border-slate-500 bg-[#9E3D64] text-white">Hari Sebelumnya Sampai</th>
                                        <th class="p-3 border border-slate-500 bg-[#9E3D64] text-white">Hari Sebelumnya</th>
                                        <th class="p-3 border border-slate-500 bg-[#9E3D64] text-white">Potongan (&)</th>
                                        <th class="p-3 border border-slate-500 bg-[#9E3D64] text-white"> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pembayarans as $index => $pembayaran)
                                    <tr class="align-middle">
                                        <td class="p-3 border border-slate-500 text-center">
                                            <input type="text" 
                                            wire:model="pembayarans.{{ $index }}.kebijakan_pembatalan_sebelum"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-white text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="3">
                                        </td>
                                        <td class="p-3 border border-slate-500 text-center">
                                            <input type="text" 
                                            wire:model="pembayarans.{{ $index }}.kebijakan_pembatalan_sesudah"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-white text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="3">
                                        </td>
                                        <td class="p-3 border border-slate-500 text-center">
                                            <input type="text"
                                            wire:model="pembayarans.{{ $index }}.kebijakan_pembatalan_potongan"
                                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-white text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-20 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="3">
                                        </td>
                                        <td class="p-3 border border-slate-500 text-center">
                                            <a class="mx-1">
                                                <img src="{{ asset('storage/icons/trash-solid.svg') }}" alt="" width="16px">
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif

                {{-- Peta & Foto --}}

                @if ($currentStep == 5)
                <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded">
                    <div class="flex justify-between">
                        <div class="text-lg font-bold font-inter text-[#9E3D64]">
                            Pilih Foto
                        </div>
                    </div>

                    {{-- Maps --}}
                    <div class="my-5">
                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Link Peta Google
                            Maps</label>
                        <input type="text" id="text"
                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="<iframe src=”https://www.map">
                    </div>

                    {{-- Foto 
                    <div class="py-2" x-data="displayImage()">
                        <p class="font-semibold text-[#BDBDBD]">Foto</p>
                        <input class="py-2" type="file" accept="image/*" @change="selectedFile" multiple>
                        <template x-if="images.length < 1">
                            <div
                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                    height="20px">
                            </div>
                        </template>
                        <template x-if="images.length >= 1">
                            <div class="flex">
                                <template x-for="(image, index) in images" :key="index">
                                    <div class="flex justify-center items-center">
                                        <img :src="image"
                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                            :alt="'upload'+index">
                                        <button class="absolute mx-2 translate-x-12 -translate-y-14"><img
                                                src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                width="25px" @click="removeImage(index)">
                                        </button>
                                    </div>
                                </template>
                            </div>
                        </template>
                    </div>--}}

                    {{-- Foto Fitur 
                    <div class="py-2" x-data="featuredImage()">
                        <p class="font-semibold text-[#BDBDBD]">Foto Fitur</p>
                        <input class="py-2" type="file" accept="image/*" @change="selectedFile">
                        <div class="flex">
                            <template x-if="imageUrl">
                                <div class="flex justify-center items-center">
                                    <img :src="imageUrl"
                                        class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                        alt="upload-featured-photo">
                                    <button class="absolute mx-2 translate-x-12 -translate-y-14"><img
                                            src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                            width="25px" @click="removeImage()">
                                    </button>
                                </div>
                            </template>
                            <template x-if="!imageUrl">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                        </div>
                    </div>--}}
                </div>
                @endif

                {{-- Pilihan & Extra --}}

                @if ($currentStep == 6)      
                <div class="bg-[#FFFFFF] drop-shadow-xl p-5 mt-5 mb-5 rounded">
                    <div class="flex justify-between">
                        <div class="text-lg font-bold font-inter text-[#9E3D64]">
                            Pilih Foto
                        </div>
                    </div>

                    {{-- Maps --}}
                    <div class="my-5">
                        <label for="text" class="block mb-2 text-sm font-bold text-[#333333]">Link Peta Google
                            Maps</label>
                        <input type="text" id="text"
                            class="bg-[#FFFFFF] border border-[#4F4F4F] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="<iframe src=”https://www.map">
                    </div>

                    {{-- Foto --}}
                    <div class="py-2" x-data="displayImage()">
                        <p class="font-semibold text-[#BDBDBD]">Foto</p>
                        <input class="py-2" type="file" accept="image/*" @change="selectedFile" multiple>
                        <template x-if="images.length < 1">
                            <div
                                class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                    height="20px">
                            </div>
                        </template>
                        <template x-if="images.length >= 1">
                            <div class="flex">
                                <template x-for="(image, index) in images" :key="index">
                                    <div class="flex justify-center items-center">
                                        <img :src="image"
                                            class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                            :alt="'upload'+index">
                                        <button class="absolute mx-2 translate-x-12 -translate-y-14"><img
                                                src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                                width="25px" @click="removeImage(index)">
                                        </button>
                                    </div>
                                </template>
                            </div>
                        </template>
                    </div>

                    {{-- Foto Fitur --}}
                    <div class="py-2" x-data="featuredImage()">
                        <p class="font-semibold text-[#BDBDBD]">Foto Fitur</p>
                        <input class="py-2" type="file" accept="image/*" @change="selectedFile">
                        <div class="flex">
                            <template x-if="imageUrl">
                                <div class="flex justify-center items-center">
                                    <img :src="imageUrl"
                                        class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                        alt="upload-featured-photo">
                                    <button class="absolute mx-2 translate-x-12 -translate-y-14"><img
                                            src="{{ asset('storage/icons/circle-trash-solid.svg') }}" alt=""
                                            width="25px" @click="removeImage()">
                                    </button>
                                </div>
                            </template>
                            <template x-if="!imageUrl">
                                <div
                                    class="border flex justify-center items-center rounded border-gray-300 bg-gray-200 w-[154px] h-[154px]">
                                    <img src="{{ asset('storage/icons/upload.svg') }}" alt="upload-icons" width="20px"
                                        height="20px">
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
                @endif
                
                {{-- Button --}}
                <div class="p-5">
                    <div class="grid grid-cols-6">
                        @if ($currentStep == 1)
                        <div></div>
                        @endif

                        @if ($currentStep == 2 || $currentStep == 3 || $currentStep == 4 || $currentStep == 5 || $currentStep == 6)
                        <div class="col-start-1 col-end-2">
                            <button type="button" class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-[#23AEC1] bg-[#FFFFFF] border border-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#23AEC1] hover:text-[#FFFFFF]" wire:click="decreaseStep()">
                                Sebelumnya                
                            </button>
                        </div>
                        @endif
                            
                        @if ($currentStep == 1 || $currentStep == 2 || $currentStep == 3 || $currentStep == 4 || $currentStep == 5)
                        <div class="col-start-6 col-end-7">
                            <button type="button" class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]" wire:click="increaseStep()">
                                Selanjutnya
                            </button>
                        </div>
                        @endif
                            
                        @if ($currentStep == 6)
                        <div class="col-start-6 col-end-7">
                            <button type="submit" class="w-full items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-[#23AEC1] rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-[#1A95A6]">
                                Submit
                            </button>
                        </div>
                        @endif
                    </div>
                </div>
            
            </form>
            

            </div>
        </div>
    </div>

    <script>
        $('#deskripsi').summernote({
            placeholder: 'Deskripsi...',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['para', ['ul', 'ol']]
            ],
            callbacks: {
                onChange: function(contents, $editable) {
                    @this.set('deskripsi', contents);
                }
            }
        });

        $('#paket_termasuk').summernote({
            placeholder: 'Paket Termasuk...',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });

        $('#paket_tidak_termasuk').summernote({
            placeholder: 'Paket Tidak Termasuk...',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });

        $('#catatan').summernote({
            placeholder: 'Catatan...',
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
    </script>
    @livewireScripts
</body>

</html>
