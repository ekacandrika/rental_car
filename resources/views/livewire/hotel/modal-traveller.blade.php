<div>
    <div class="flex justify-between px-3 py-2 bg-white border border-gray-500 rounded-md">
        <p class="text-base font-medium text-[#BDBDBD]">Pilihan</p>
        <button wire:click="$set('showingModal', true)" wire:loading.attr="disabled"><img
                src="{{ asset('storage/icons/circle-plus-solid 1.svg') }}" alt="plus" width="20px"></button>
    </div>
    <x-jet-dialog-modal wire:model="showingModal">
        <x-slot name="title">
            Pilihan
        </x-slot>
        <x-slot name="content">
            <div>
                <div class="flex items-center justify-between my-3 rounded-md border border-gray-300 py-2 px-3">
                    <p class="text-md font-medium">Pilihan 1</p>
                    <div class="flex justify-end"
                        x-data="{ count: 0, increment() { this.count++ }, decrement() { this.count === 0 ? 0 : this.count-- } }">
                        <button class="text-xl font-semibold mx-3" x-on:click="decrement()">-</button>
                        <input
                            class="block text-center w-[50px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                            type="number" min="0" step="1" value="0" x-model="count">
                        <button class="text-xl font-semibold mx-3" x-on:click="increment()">+</button>
                    </div>
                </div>
                <div class="flex items-center justify-between my-3 rounded-md border border-gray-300 py-2 px-3">
                    <p class="text-md font-medium">Pilihan 1</p>
                    <div class="flex justify-end"
                        x-data="{ count: 0, increment() { this.count++ }, decrement() { this.count === 0 ? 0 : this.count-- } }">
                        <button class="text-xl font-semibold mx-3" x-on:click="decrement()">-</button>
                        <input
                            class="block text-center w-[50px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                            type="number" min="0" step="1" value="0" x-model="count">
                        <button class="text-xl font-semibold mx-3" x-on:click="increment()">+</button>
                    </div>
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-jet-secondary-button wire:click="$set('showingModal', false)" wire:loading.attr="disabled">
                {{ __('Tambahkan') }}
            </x-jet-secondary-button>
        </x-slot>
    </x-jet-dialog-modal>
</div>