<div>
    <div class="flex justify-between px-3 py-2 bg-white border border-gray-500 rounded-md">
        <p class="text-base font-medium text-[#BDBDBD]">Ekstra</p>
        <button type="button" wire:click="$set('showingModal', true)" wire:loading.attr="disabled"><img
                src="{{ asset('storage/icons/circle-plus-solid 1.svg') }}" alt="plus" width="20px"></button>
    </div>

    {{-- Modal --}}
    <div>
        <x-jet-dialog-modal wire:model="showingModal">
            <x-slot name="title">
                <div class="flex justify-between">
                    <p>Ekstra</p>
                    <button type="button" wire:click="$set('showingModal', false)">
                        <img src="{{ asset('storage/icons/close-button.svg') }}" alt="close">
                    </button>
                </div>
            </x-slot>
            <x-slot name="content">
                <p class="text-sm my-2">Catatan</p>
                <div class="space-y-2 sm:space-y-0 sm:flex justify-between">
                    <div>
                        <input
                            class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 px-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                            placeholder="Misal terima di hotel atau bandara" type="text" name="catatan_ekstra" />
                    </div>
                    <div>
                        <button type="button"
                            class="border border-[#D50006] bg-transparent text-[#D50006] rounded-md px-2 py-1 text-xs sm:px-3 sm:py-2 hover:bg-[#D50006] hover:text-white duration-200 sm:text-sm">Hapus
                            semua pilihan</button>
                    </div>
                </div>
                @if (count($extras) == 0)
                <p class="m-5 text-center font-bold">Tidak ada Extra.</p>
                @else
                <div class="grid grid-cols-2 sm:grid-cols-4 mt-5 gap-10 h-96 overflow-y-scroll">
                    @foreach ($extras as $key => $extra)
                    {{-- sn:h-[250px] --}}
                    <div class="sm:relative">
                        <img class="object-center object-cover rounded-md h-[100px] w-full sm:w-full sm:h-[100px] cursor-pointer"
                            src="{{ asset($extra->thumbnail) }}" alt="{{ $extra->nama_ekstra }}"
                            wire:click="showDetail( {{ $key }} )">
                        <div class="flex justify-between items-center my-2">
                            <p class="text-sm line-clamp-2 break-words">{{ $extra->nama_ekstra }}</p>
                            <input class="rounded-sm" type="checkbox" name="extra[{{ $key }}][id_extra]"
                                id="extra[{{ $key }}]" value="{{ $extra->id }}"
                                wire:model="extra_count.{{ $key }}.id_extra">
                        </div>
                        <div class="flex my-2 justify-center">
                            <button type="button" class="text-lg font-semibold mx-3"
                                wire:click="decrement({{ $key }})">-</button>
                            <input
                                class="block text-center w-[50px] bg-white border border-[#828282] rounded-md py-1 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                type="number" min="0" step="1" value="" wire:model="extra_count.{{ $key }}.count">
                            <template x-if="$wire.extra_count[{{ $key }}].id_extra !== ''">
                                <div>
                                    <input type="hidden" wire:model="extra_count.{{ $key }}.count"
                                        name="extra[{{ $key }}][jumlah_extra]">
                                    <input type="hidden" wire:model="extra_count.{{ $key }}.note_extra"
                                        name="extra[{{ $key }}][note_extra]">
                                    <input type="hidden" name="extra[{{ $key }}][harga_extra]"
                                        value="{{ $extra->harga_ekstra }}">
                                </div>
                            </template>
                            <button type="button" class="text-lg font-semibold mx-3"
                                wire:click="increment({{ $key }})">+</button>
                        </div>
                        <p class=" text-center w-full my-3">Rp {{ $extra->harga_ekstra }}</p>
                        <div wire:click="showDetail( {{ $key }} )"
                            class="flex justify-center rounded-md bg-[#23AEC1] text-white py-1 my-2 sm:absolute sm:bottom-0 sm:w-full">
                            <button type="button">Detail</button>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </x-slot>
            <x-slot name="footer">
            </x-slot>
        </x-jet-dialog-modal>
    </div>

    {{-- Detail --}}
    <div>
        <x-jet-dialog-modal wire:model="showingDetail">
            <x-slot name="title">
                <div class="flex justify-between">
                    <p>Ekstra</p>
                    <button type="button" wire:click="$set('showingDetail', false)">
                        <img src="{{ asset('storage/icons/close-button.svg') }}" alt="close">
                    </button>
                </div>
            </x-slot>
            <x-slot name="content">
                <p class="my-3 text-2xl font-bold">
                    {{ $extras[$extra_id]->nama_ekstra ?? 'Nama Ekstra' }}
                </p>
                <p class="my-3 text-2xl font-bold">
                    @if (isset($extras[$extra_id]->harga_ekstra))
                    Rp {{ $extras[$extra_id]->harga_ekstra !=null ? number_format(intval($extras[$extra_id]->harga_ekstra)) : '-' }}
                    @else
                    Rp '-'
                    @endif
                </p>
                <div class="sm:grid grid-cols-3">
                    <div class="col-span-2">
                        <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff"
                            class="swiper mySwiper2">
                            <div class="swiper-wrapper my-2">
                                @if (isset($extras[$extra_id]))
                                @foreach (json_decode($extras[$extra_id]->foto_ekstra) as $foto)
                                <div class="swiper-slide">
                                    <img class="w-full h-[200px] md:w-[416px] md:h-[416px] object-cover object-center"
                                        src="{{ asset($foto) }}" />
                                </div>
                                @endforeach
                                @endif
                            </div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                        <div thumbsSlider="" class="hidden sm:block swiper mySwiper">
                            <div class="swiper-wrapper">
                                @if (isset($extras[$extra_id]))
                                @foreach (json_decode($extras[$extra_id]->foto_ekstra) as $foto)
                                <div class="max-w-[50px] md:max-w-full swiper-slide">
                                    <img class="md:w-[75px] md:h-[75px] object-cover object-center"
                                        src="{{ asset($foto) }}" />
                                </div>
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="space-y-2 pt-3 sm:p-3 sm:pt-0">
                            <p class="font-semibold">Deskripsi Ekstra:</p>
                            {!! $extras[$extra_id]->detail_ekstra ?? '<p>Tidak ada deskripsi</p>' !!}
                        </div>
                        <div class="space-y-2 pt-3 sm:p-3 sm:pt-0 mt-3">
                            <p class="font-semibold">Note Pemilihan Extra:</p>
                            {!! $extras[$extra_id]->note_ekstra ?? '<p>Tidak ada note.</p>' !!}
                        </div>
                        <div class="my-3 sm:my-0 sm:m-3 py-2 px-2 border border-gray-200 rounded-md">
                            <div class="flex justify-center">
                                <button type="button" class="text-3xl font-semibold mx-3"
                                    wire:click="decrement({{ $extra_id }})">-</button>
                                <input
                                    class="block w-[50px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                    type="number" min="0" step="1" value="0"
                                    wire:model="extra_count.{{ $extra_id }}.count">
                                <button type="button" class="text-3xl font-semibold mx-3"
                                    wire:click="increment({{ $extra_id }})">+</button>
                            </div>
                            <input
                                class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-1 pl-3 pr-3 my-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                placeholder="Note" type="text" wire:model="extra_count.{{ $extra_id }}.note_extra" />
                            <div class="flex justify-center my-2">
                                <button {{-- type="button" --}}
                                    class="rounded-md py-1 px-3 bg-[#23AEC1] text-white">Tambahkan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </x-slot>
            <x-slot name="footer">
            </x-slot>
        </x-jet-dialog-modal>
    </div>
</div>