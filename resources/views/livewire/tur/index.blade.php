<div>
    {{-- If you look to others for fulfillment, you will never truly be fulfilled. --}}
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Gallery Lightbox --}}
    <div class="container mx-auto my-5">
        <div class="relative md:grid md:grid-cols-2 grid-flow-col gap-2">
            <div class="cursor-pointer mb-2 md:mb-0">
                <img class="object-cover object-center h-full"
                    src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                    alt="detail-1">
            </div>
            <div>
                <div class="grid grid-cols-2 md:grid-cols-1 md:grid-rows-2 gap-2">
                    <div class="grid grid-cols-2 gap-2">
                        <div class="cursor-pointer">
                            <img class="object-cover object-center h-full"
                                src="https://images.unsplash.com/photo-1543874768-af0b9c4090d5?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80"
                                alt="detail-1">
                        </div>
                        <div class="cursor-pointer">
                            <img class="object-cover object-center h-full"
                                src="https://images.unsplash.com/photo-1602057512587-76d5cc4b34e2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1172&q=80"
                                alt="detail-1">
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-2">
                        <div class="cursor-pointer">
                            <img class="object-cover object-center h-full"
                                src="https://images.unsplash.com/photo-1630214801769-24784bfd2b9c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80"
                                alt="detail-1">
                        </div>
                        <div class="cursor-pointer">
                            <img class="object-cover object-center h-full"
                                src="https://images.unsplash.com/photo-1586319826907-1ff4aadbaddc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzJ8fHlvZ3lha2FydGF8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60"
                                alt="detail-1">
                        </div>
                    </div>
                </div>
            </div>
            <button
                class="w-full md:w-fit md:absolute md:bottom-0 md:right-0 md:-translate-x-5 md:-translate-y-5 px-1 md:px-3 py-1 lg:px-5 lg:py-2 text-sm sm:text-base font-semibold border-2 border-gray-300 text-black duration-300 bg-gray-300 rounded-sm hover:bg-gray-100">Semua
                foto</button>
        </div>
    </div>

    <div>
        {{-- Tabs --}}
        <div class="container mx-auto my-5 sticky bg-white top-0 z-20" x-data="{ active:0, 
        tabs:['Ringkasan', 'Itinerary', 'Paket', 'Lokasi', 'Ulasan']}">
            <div class="flex sm:block">
                <ul
                    class="flex justify-start overflow-x-auto text-sm font-medium text-center sm:text-left text-gray-500 dark:text-gray-400 border-b border-[#BDBDBD]">
                    <template x-for="(tab, index) in tabs" :key="index">
                        <li class="pb-2 mt-2" :class="{'border-b-2 border-[#9E3D64]': active == index}"
                            x-on:click="active = index">
                            <a :href="`#`+tab.toLowerCase()">
                                <button class="inline-block py-2 px-9 text-xl rounded-lg duration-200"
                                    :class="{'text-[#9E3D64] bg-white font-bold': active == index, 'text-black font-normal hover:text-[#9E3D64]': active != index}"
                                    x-text="tab"></button>
                            </a>
                        </li>
                    </template>
                </ul>
            </div>
        </div>

        {{-- Content --}}
        <div class="container mx-auto">
            <div class="grid grid-cols-5 gap-5">
                {{-- Left Side --}}
                <div class="col-span-5 lg:col-span-4 bg-white p-3">
                    {{-- Ringkasan --}}
                    <div id="ringkasan">
                        <p class="text-3xl py-3 font-semibold text-[#333333] whitespace-normal break-words">Nama Wisata
                        </p>
                        <p class="text-xl py-1 font-semibold text-[#333333] whitespace-normal break-words">Lokasi</p>
                        <div class="flex py-2 items-center">
                            <img class="mx-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <p class="text-xl font-semibold text-[#333333]">4.7 <span class="font-normal">(12
                                    Ulasan)</span></p>
                        </div>
                        <div class="flex flex-wrap my-2 py-1 text-sm">
                            <p class="mt-2 mr-1"><span class="font-semibold">100</span> kali dipesan •</p>
                            <p class="mt-2 mr-1"><span class="font-semibold">500</span> orang menyukai ini •</p>
                            <div class="mt-2 flex">
                                <p>Share</p>
                                <a href="#"><img src="{{ asset('storage/icons/square-facebook.svg') }}" class="mx-2"
                                        alt="wa-icon" width="23px" height="23px"></a>
                                <a href="#"><img src="{{ asset('storage/icons/square-whatsapp.svg') }}" alt="fb-icon"
                                        width="23px" height="23px"></a>
                            </div>
                        </div>
                        <div class="flex flex-wrap my-3">
                            <a href="#"
                                class="rounded-full flex py-1 px-5 mt-2 mr-3 bg-[#F2F2F2] hover:shadow-md duration-200">
                                <img class="mr-2" src="{{ asset('storage/icons/bookmark-solid 1.svg') }}" alt="type"
                                    width="12px">Tour
                            </a>
                            <a href="#"
                                class="rounded-full flex py-1 px-5 mt-2 mr-3 bg-[#F2F2F2] hover:shadow-md duration-200">
                                <img class="mr-2" src="{{ asset('storage/icons/location-dot-solid 1.svg') }}"
                                    alt="location" width="12px">Lokasi
                            </a>
                            <a href="#"
                                class="rounded-full flex py-1 px-5 mt-2 bg-[#F2F2F2] hover:shadow-md duration-200">
                                <img class="mr-2" src="{{ asset('storage/icons/tag-solid 1.svg') }}" alt="category"
                                    width="12px">Kategori
                            </a>
                        </div>
                        <div
                            class="border-y border-gray-200 sm:border-y-0 grid sm:grid-cols-2 md:w-3/4 py-3 space-y-2 sm:space-y-0">
                            <div class="space-y-2">
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/user-solid.svg') }}" alt="user"> Tipe Tur: Open
                                    Trip
                                </p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/user-group-solid.svg') }}" alt="user-group">Peserta
                                    Min 3 Max 6</p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/location-dot-solid 1.svg') }}"
                                        alt="location">Berangkat dari: Makassar</p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/hourglass-solid.svg') }}" alt="hourglass">Durasi:
                                    3H2D</p>
                            </div>
                            <div class="space-y-2">
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/utensils-solid.svg') }}" alt="utensils">Makanan:
                                    Halal</p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/earth-asia-solid.svg') }}" alt="user">Bahasa:
                                    Indonesia</p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/check-to-slot-solid.svg') }}" alt="slot">Konfirmasi
                                    Instant</p>
                                <p class="flex font-sm font-medium"><img class="mr-2" width="18px"
                                        src="{{ asset('storage/icons/person-hiking-solid.svg') }}" alt="user">Tingkat
                                    Petualangan: Extreme</p>
                            </div>
                        </div>
                        <div class="text-justify my-5">
                            <p class="text-3xl my-3 font-semibold">Deskripsi Wisata</p>
                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis
                                molestie,
                                dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem
                                sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit,
                                sit amet feugiat lectus. Class aptent taciti sociosqu ad litora torquent per conubia
                                nostra, per inceptos himenaeos. Praesent auctor purus luctus enim egestas, ac
                                scelerisque ante pulvinar. Donec ut rhoncus ex. Suspendisse ac rhoncus nisl, eu tempor
                                urna. Curabitur vel bibendum lorem. Morbi convallis convallis diam sit amet lacinia.
                                Aliquam in elementum tellus.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie,
                                dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem
                                sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit,
                                sit amet feugiat lectus. Class aptent taciti sociosqu ad litora torquent per conubia
                                nostra, per inceptos himenaeos. Praesent auctor purus luctus enim egestas, ac
                                scelerisque ante pulvinar. Donec ut rhoncus ex. Suspendisse ac rhoncus nisl, eu tempor
                                urna. Curabitur vel bibendum lorem. Morbi convallis convallis diam sit amet lacinia.
                                Aliquam in elementum tellus.</p>
                        </div>
                    </div>

                    {{-- Itinerary --}}
                    <div id="itinerary" class="py-3">
                        <p class="text-3xl py-3 font-semibold text-[#333333]">Itinerary</p>
                        <div x-data="{ tabs:[ 
                                {   time:'09.00',
                                    title:'Lorem Ipsum dolor sit amet',
                                    color:'#9E3D64' }, 
                                {   time:'09.10',
                                    title:'Lorem Ipsum dolor sit amet',
                                    color:'#FFB800' }, 
                                {   time:'09.20',
                                    title:'Lorem Ipsum dolor sit amet',
                                    color:'#51B449' } 
                                    ] }">
                            <ul class="text-sm font-medium text-justify sm:text-left">
                                <template x-for="(tab, index) in tabs" :key="index">
                                    <li class="my-2 px-3 rounded-md bg-[#F2F2F2]"
                                        :class="'border-l-8 border-['+tab.color+']'" x-data="{ open:false }">
                                        <div class="grid grid-cols-12" :class="{'pt-3 pb-2' : open, 'py-3' : !open}">
                                            <div class="col-span-11 md:grid grid-cols-11">
                                                <p class="text-base sm:text-lg text-[#4F4F4F] font-semibold text-left rounded-lg cursor-pointer"
                                                    x-on:click="open = !open" x-text="tab.time"></p>
                                                <button
                                                    class="text-base sm:text-lg text-[#4F4F4F] font-semibold col-span-10 w-full text-left rounded-lg whitespace-normal break-words"
                                                    x-on:click="open = !open" x-text="tab.title"></button>
                                            </div>
                                            <div class="w-full">
                                                <img class="float-right duration-200 cursor-pointer"
                                                    :class="{'rotate-180' : open}" x-on:click="open = !open"
                                                    src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                                    alt="chevron-down" width="26px" height="15px">
                                            </div>
                                        </div>
                                        <div class="py-5 md:pl-14 lg:pl-16 xl:pl-20 2xl:pl-24 md:pr-10 lg:pr-12 font-normal text-[#4F4F4F] text-base lg:text-lg xl:text-xl"
                                            x-show="open" x-transition>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum has been the industry's standard
                                                dummy text ever since the 1500s, when an unknown printer took a
                                                galley of type and scrambled it to make a type specimen book. </p>
                                            <img class="my-3 object-cover object-center sm:max-w-md rounded-md"
                                                src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                                alt="itinerary-img">
                                        </div>

                                    </li>
                                </template>
                            </ul>
                        </div>
                    </div>

                    {{-- Paket --}}
                    <div id="paket" class="py-3 px-2 border-y  bg-[#F9F9F9] border-y-[#828282]">
                        <p class="text-3xl py-3 font-semibold text-[#333333]">Paket</p>
                        <div class="flex justify-between">
                            <div class="relative block">
                                <label for="tur-date-picker" class="form-label inline-block mb-2 text-gray-700">Tanggal
                                    Tur</label>
                                <input
                                    class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                    placeholder="09/08/2022" type="text" name="search" />
                            </div>
                            <button
                                class="border border-[#D50006] bg-transparent text-[#D50006] rounded-md px-3 py-2 h-fit hover:bg-[#D50006] hover:text-white duration-200">Hapus
                                Semua</button>
                        </div>
                        <div class="my-3">
                            <p class="mb-3">Pilih Paket</p>
                            <button class="rounded-md w-1/4 text-white bg-kamtuu-second px-3 py-2">
                                Paket 1
                            </button>
                        </div>

                        {{-- Tambahan --}}
                        <p class="my-3">Tambahan</p>
                        <div class="my-3 flex space-x-3">
                            {{-- Pilihan --}}
                            <div
                                class="flex justify-between w-1/4 px-3 py-2 bg-white border border-gray-500 rounded-md">
                                <p class="text-base font-medium text-[#BDBDBD]">Pilihan</p>
                                <button wire:click="$set('showingModal', true)" wire:loading.attr="disabled"><img
                                        src="{{ asset('storage/icons/circle-plus-solid 1.svg') }}" alt="plus"
                                        width="20px"></button>
                            </div>
                            {{-- Ekstra --}}
                            <div
                                class="flex justify-between w-1/4 px-3 py-2 bg-white border border-gray-500 rounded-md">
                                <p class="text-base font-medium text-[#BDBDBD]">Ekstra</p>
                                <button wire:click="showEkstra()" wire:loading.attr="disabled"><img
                                        src="{{ asset('storage/icons/circle-plus-solid 1.svg') }}" alt="plus"
                                        width="20px"></button>
                            </div>
                        </div>

                        {{-- Jumlah Peserta --}}
                        <div class="my-5">
                            <p class="my-2">Jumlah</p>
                            <div class="flex justify-between my-3">
                                <div>
                                    <p class="text-sm font-medium">Dewasa</p>
                                    <p class="text-xs font-normal text-[#828282]">Di atas 12 tahun</p>
                                </div>
                                <div class="flex justify-end"
                                    x-data="{ count: 1, increment() { this.count++ }, decrement() { this.count === 1 ? 1 : this.count-- } }">
                                    <button class="text-3xl font-semibold mx-3" x-on:click="decrement()">-</button>
                                    <input
                                        class="block w-[100px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        type="number" min="1" step="1" value="1" x-model="count">
                                    <button class="text-3xl font-semibold mx-3" x-on:click="increment()">+</button>
                                </div>
                            </div>
                            <div class="flex justify-between my-3">
                                <div>
                                    <p class="text-sm font-medium">Anak-anak</p>
                                    <p class="text-xs font-normal text-[#828282]">5 - 12 tahun</p>
                                </div>
                                <div class="flex justify-end"
                                    x-data="{ count: 0, increment() { this.count++ }, decrement() { this.count === 0 ? 0 : this.count-- } }">
                                    <button class="text-3xl font-semibold mx-3" x-on:click="decrement()">-</button>
                                    <input
                                        class="block w-[100px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        type="number" min="0" step="1" value="1" x-model="count">
                                    <button class="text-3xl font-semibold mx-3" x-on:click="increment()">+</button>
                                </div>
                            </div>
                            <div class="flex justify-between my-3">
                                <div>
                                    <p class="text-sm font-medium">Balita</p>
                                    <p class="text-xs font-normal text-[#828282]">Di bawah 5 tahun</p>
                                </div>
                                <div class="flex justify-end"
                                    x-data="{ count: 0, increment() { this.count++ }, decrement() { this.count === 0 ? 0 : this.count-- } }">
                                    <button class="text-3xl font-semibold mx-3" x-on:click="decrement()">-</button>
                                    <input
                                        class="block w-[100px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        type="number" min="0" step="1" value="1" x-model="count">
                                    <button class="text-3xl font-semibold mx-3" x-on:click="increment()">+</button>
                                </div>
                            </div>
                        </div>

                        <div class="flex justify-between text-[#333333]">
                            <div class="block">
                                <p class="text-base font-medium">Mulai</p>
                                <p class="lg:text-2xl xl:text-3xl text-center py-2 font-bold text-[#23AEC1]">IDR 500,000
                                </p>
                            </div>
                            <div class="flex justify-center">
                                <x-destindonesia.button-primary text="Pesan Sekarang"></x-destindonesia.button-primary>
                            </div>
                        </div>
                    </div>

                    {{-- Gallery --}}
                    <div class="my-5 swiper gallery-tur-swiper">
                        <div class="mx-auto py-5 swiper-wrapper">
                            <div class="flex justify-center swiper-slide">
                                <div class="w-full h-[321px]">
                                    <img class="w-full h-full rounded-md object-cover object-center"
                                        src="https://images.unsplash.com/photo-1543874768-af0b9c4090d5?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80"
                                        alt="gallery-tur">
                                </div>
                            </div>
                            <div class="flex justify-center swiper-slide">
                                <div class="w-full h-[321px]">
                                    <img class="w-full h-full rounded-md object-cover object-center"
                                        src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                                        alt="gallery-tur">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>

                    {{-- Paket Termasuk --}}
                    <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                        x-data="{ open:false }">
                        <div class="flex justify-between" :class=" {'pt-3 pb-2' : open, 'py-3' : !open}">
                            <button
                                class="text-lg sm:text-xl md:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open">Paket Termasuk</button>
                            <img class="float-right duration-200 cursor-pointer" :class="{'rotate-180' : open}"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                            x-show="open" x-transition>
                            <li>Lorem Ipsum</li>
                            <li>Lorem Ipsum</li>
                            <li>Lorem Ipsum</li>
                            <li>Lorem Ipsum</li>
                            <li>Lorem Ipsum</li>
                        </ul>
                    </div>

                    {{-- Paket Tidak Termasuk --}}
                    <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                        x-data="{ open:false }">
                        <div class="flex justify-between" :class=" {'pt-3 pb-2' : open, 'py-3' : !open}">
                            <button
                                class="text-lg sm:text-xl md:text-2xl  text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open">Paket Tidak Termasuk</button>
                            <img class="float-right duration-200 cursor-pointer" :class="{'rotate-180' : open}"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        <ul class="space-y-2 px-3 pt-3 pb-5 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl"
                            x-show="open" x-transition>
                            <li>Lorem Ipsum</li>
                            <li>Lorem Ipsum</li>
                            <li>Lorem Ipsum</li>
                            <li>Lorem Ipsum</li>
                            <li>Lorem Ipsum</li>
                        </ul>
                    </div>
                </div>

                {{-- Right Side --}}
                <div class="col-span-5 lg:col-span-1 order-last lg:order-none">
                    {{-- Price --}}
                    <div class="hidden lg:block rounded-md shadow-lg p-3 mb-3 bg-white border border-gray-200">
                        <div x-data="{bookmarked:false}" class="flex justify-between text-[#333333]">
                            <p class="text-base font-medium">Mulai</p>
                            <button x-on:click="bookmarked = !bookmarked" class="flex text-sm">
                                <img class="mr-1" x-show="bookmarked === false"
                                    src="{{ asset('storage/icons/bookmark-regular 1.svg') }}" alt="bookmark"
                                    width="14px">
                                <img class="mr-1" x-show="bookmarked === true"
                                    src="{{ asset('storage/icons/bookmark-solid 1.svg') }}" alt="bookmark" width="14px">
                                Favorit
                            </button>
                        </div>
                        <p class="lg:text-2xl xl:text-3xl text-center py-2 font-bold text-[#23AEC1]">IDR 500,000</p>
                        <div class="flex justify-center">
                            <x-destindonesia.button-primary text="Pesan Sekarang"></x-destindonesia.button-primary>
                        </div>
                    </div>

                    {{-- Store --}}
                    <div class="lg:rounded-md lg:shadow-lg p-3 my-5 bg-white border-y-2 lg:border border-gray-200">
                        <p class="lg:text-2xl xl:text-3xl font-bold text-center text-[#333333]">Kunjungi Toko</p>
                        <div class="flex justify-center py-3">
                            <img src="{{ asset('storage/img/tur-detail-1.png') }}"
                                class="bg-clip-content lg:w-[96px] lg:h-[96px] xl:w-[100px] xl:h-[100px] shadow-xl bg-white rounded-full"
                                alt="store-profile">
                        </div>
                        <p class="text-xl font-semibold text-center pt-3">Kwantar Trans</p>
                        <p class="text-lg font-normal text-center pb-2">Bergabung sejak 2022</p>
                        <div class="flex justify-center">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                            <img class="px-1" src="{{ asset('storage/icons/Star 1.png') }}" alt="star" width="30px"
                                height="30px">
                        </div>
                        <div class="flex justify-center">
                            <x-destindonesia.button-primary text="Kirim Pesan"></x-destindonesia.button-primary>
                        </div>
                    </div>

                    {{-- QR Code --}}
                    <div>
                        <p class="text-2xl font-semibold text-center">Kode QR Produk</p>
                        <div class="flex justify-center mt-5">
                            <img src="{{ asset('storage/img/tur-detail-2.png') }}" alt="QR-code">
                        </div>
                        <a class="flex justify-center" href="/storage/img/tur-detail-2.png" download="QR-code-produk">
                            <x-destindonesia.button-primary text="Unduh Kode QR"></x-destindonesia.button-primary>
                        </a>
                    </div>


                </div>

                {{-- Content --}}
                <div class="order-1 lg:order-none col-span-5">
                    <hr class="border border-[#BDBDBD] my-5">

                    {{-- Lokasi --}}
                    <div id="lokasi" class="my-5">
                        <p class="text-3xl py-3 font-semibold text-[#333333]">Lokasi Anda</p>
                        <p class="text-sm font-normal pb-3">Kotagede, Daerah Istimewa Yogyakarta, Indonesia</p>
                        <div>
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15810.940300744738!2d110.3976435!3d-7.817842!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a57148c5e4d03%3A0x4027a76e35300b0!2sKotagede%2C%20Yogyakarta%20City%2C%20Special%20Region%20of%20Yogyakarta!5e0!3m2!1sen!2sid!4v1666232541267!5m2!1sen!2sid"
                                style="border:0;" allowfullscreen="" loading="lazy" class="rounded-lg w-full h-[450px]"
                                referrerpolicy="no-referrer-when-downgrade"></iframe>
                        </div>
                    </div>

                    {{--Perlu Diketahui --}}
                    <p class="text-3xl py-3 font-semibold text-[#333333]">Perlu Diketahui</p>
                    {{-- Catatan --}}
                    <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                        x-data="{ open:false }">
                        <div class="flex justify-between" :class=" {'pt-3 pb-2' : open, 'py-3' : !open}">
                            <button
                                class="text-lg sm:text-xl md:text-2xl  text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open">Catatan</button>
                            <img class="float-right duration-200 cursor-pointer" :class="{'rotate-180' : open}"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        <p class="py-5 px-3 font-normal text-[#4F4F4F]  text-base sm:text-lg md:text-xl" x-show="open"
                            x-transition>
                            Lorem
                            Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to make a type
                            specimen book. </p>
                    </div>

                    {{-- Kebijakan Pembatalan --}}
                    <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                        x-data="{ open:false }">
                        <div class="flex justify-between" :class=" {'pt-3 pb-2' : open, 'py-3' : !open}">
                            <button
                                class="text-lg sm:text-xl md:text-2xl  text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open">Kebijakan Pembatalan</button>
                            <img class="float-right duration-200 cursor-pointer" :class="{'rotate-180' : open}"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        <p class="py-5 px-3 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl" x-show="open"
                            x-transition>
                            Lorem
                            Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to make a type
                            specimen book. </p>
                    </div>

                    {{-- Sering Ditanyakan (FAQ) --}}
                    <div class="text-sm font-medium text-justify sm:text-left my-3 px-3 rounded-md bg-[#F2F2F2]"
                        x-data="{ open:false }">
                        <div class="flex justify-between" :class=" {'pt-3 pb-2' : open, 'py-3' : !open}">
                            <button
                                class="text-lg sm:text-xl md:text-2xl text-[#4F4F4F] font-semibold w-full text-left rounded-lg"
                                x-on:click="open = !open">Sering Ditanyakan (FAQ)</button>
                            <img class="float-right duration-200 cursor-pointer" :class="{'rotate-180' : open}"
                                x-on:click="open = !open" src="{{ asset('storage/icons/chevron-down-solid 1.svg') }}"
                                alt="chevron-down" width="26px" height="15px">
                        </div>
                        <p class="py-5 px-3 font-normal text-[#4F4F4F] text-base sm:text-lg md:text-xl" x-show="open"
                            x-transition>
                            Lorem
                            Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to make a type
                            specimen book. </p>
                    </div>

                    {{-- Ulasan --}}
                    <div id="ulasan" class="my-5">
                        <x-destindonesia.ulasan></x-destindonesia.ulasan>
                    </div>

                    <div>
                        <div class="flex justify-between">
                            <p class="text-3xl font-bold text-[#D50006]">Anda Mungkin Suka</p>
                            <a href="#" class="text-xl font-bold text-[#9E3D64]">Lihat semua</a>
                        </div>
                        <div class="my-5 swiper carousel-tur-swiper">
                            <div class="mx-auto py-5 swiper-wrapper">
                                <div class="flex justify-center swiper-slide">
                                    <x-produk.card-populer></x-produk.card-populer>
                                </div>
                                <div class="flex justify-center swiper-slide">
                                    <x-produk.card-populer></x-produk.card-populer>
                                </div>
                                <div class="flex justify-center swiper-slide">
                                    <x-produk.card-populer></x-produk.card-populer>
                                </div>
                                <div class="flex justify-center swiper-slide">
                                    <x-produk.card-populer></x-produk.card-populer>
                                </div>
                                <div class="flex justify-center swiper-slide">
                                    <x-produk.card-populer></x-produk.card-populer>
                                </div>
                            </div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <div>
        <x-jet-dialog-modal wire:model="showingModal">
            <x-slot name="title">
                <div class="flex justify-between">
                    <p>Pilihan</p>
                    <button wire:click="$set('showingModal', false)">
                        <img src="{{ asset('storage/icons/close-button.svg') }}" alt="close">
                    </button>
                </div>
            </x-slot>
            <x-slot name="content">
                <div>
                    <div class="flex items-center justify-between my-3 rounded-md border border-gray-300 py-2 px-3">
                        <p class="text-md font-medium">Pilihan 1</p>
                        <div class="flex justify-end"
                            x-data="{ count: 0, increment() { this.count++ }, decrement() { this.count === 0 ? 0 : this.count-- } }">
                            <button class="text-xl font-semibold mx-3" x-on:click="decrement()">-</button>
                            <input
                                class="block text-center w-[50px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                type="number" min="0" step="1" value="0" x-model="count">
                            <button class="text-xl font-semibold mx-3" x-on:click="increment()">+</button>
                        </div>
                    </div>
                    <div class="flex items-center justify-between my-3 rounded-md border border-gray-300 py-2 px-3">
                        <p class="text-md font-medium">Pilihan 1</p>
                        <div class="flex justify-end"
                            x-data="{ count: 0, increment() { this.count++ }, decrement() { this.count === 0 ? 0 : this.count-- } }">
                            <button class="text-xl font-semibold mx-3" x-on:click="decrement()">-</button>
                            <input
                                class="block text-center w-[50px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                type="number" min="0" step="1" value="0" x-model="count">
                            <button class="text-xl font-semibold mx-3" x-on:click="increment()">+</button>
                        </div>
                    </div>
                </div>
            </x-slot>
            <x-slot name="footer">
                <x-jet-button wire:click="$set('showingModal', false)" wire:loading.attr="disabled">
                    {{ __('Tambahkan') }}
                </x-jet-button>
            </x-slot>
        </x-jet-dialog-modal>
    </div>

    <div>
        <x-jet-dialog-modal wire:model="showingEkstra">
            <x-slot name="title">
                <div class="flex justify-between">
                    <p>Ekstra</p>
                    <button wire:click="$set('showingEkstra', false)">
                        <img src="{{ asset('storage/icons/close-button.svg') }}" alt="close">
                    </button>
                </div>
            </x-slot>
            <x-slot name="content">
                <p class="text-sm my-2">Catatan</p>
                <div class="flex justify-between">
                    <div>
                        <input
                            class="placeholder:text-[#BDBDBD] block bg-white w-full border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                            placeholder="Misal terima di hotel atau bandara" type="text" name="catatan" />
                    </div>
                    <div>
                        <button
                            class="border border-[#D50006] bg-transparent text-[#D50006] rounded-md px-3 py-2 hover:bg-[#D50006] hover:text-white duration-200 sm:text-sm">Hapus
                            semua pilihan</button>
                    </div>
                </div>
                <div class="grid grid-cols-5 mt-5 gap-5">
                    <div>
                        <img class="object-center object-cover rounded-lg h-[100px]"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="">
                        <div class="flex justify-between items-center my-2">
                            <p class="text-sm">Nama Ekstra</p>
                            <input class="rounded-sm" type="checkbox" name="pilihan-tur" id="pilihan-tur">
                        </div>
                        <div class="flex my-2"
                            x-data="{ count: 1, increment() { this.count++ }, decrement() { this.count === 1 ? 1 : this.count-- } }">
                            <button class="text-lg font-semibold mx-3" x-on:click="decrement()">-</button>
                            <input
                                class="block w-[50px] bg-white border border-[#828282] rounded-md py-1 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                type="number" min="1" step="1" value="1" x-model="count">
                            <button class="text-lg font-semibold mx-3" x-on:click="increment()">+</button>
                        </div>
                        <div class="flex justify-center rounded-md bg-[#23AEC1] text-white my-2">
                            <button>Tambahkan</button>
                        </div>
                    </div>
                    <div>
                        <img class="object-center object-cover rounded-lg h-[100px]"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="">
                        <div class="flex justify-between items-center my-2">
                            <p class="text-sm">Nama Ekstra</p>
                            <input class="rounded-sm" type="checkbox" name="pilihan-tur" id="pilihan-tur">
                        </div>
                        <div class="flex my-2"
                            x-data="{ count: 1, increment() { this.count++ }, decrement() { this.count === 1 ? 1 : this.count-- } }">
                            <button class="text-lg font-semibold mx-3" x-on:click="decrement()">-</button>
                            <input
                                class="block w-[50px] bg-white border border-[#828282] rounded-md py-1 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                type="number" min="1" step="1" value="1" x-model="count">
                            <button class="text-lg font-semibold mx-3" x-on:click="increment()">+</button>
                        </div>
                        <div class="flex justify-center rounded-md bg-[#23AEC1] text-white my-2">
                            <button>Tambahkan</button>
                        </div>
                    </div>
                    <div>
                        <img class="object-center object-cover rounded-lg h-[100px]"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="">
                        <div class="flex justify-between items-center my-2">
                            <p class="text-sm">Nama Ekstra</p>
                            <input class="rounded-sm" type="checkbox" name="pilihan-tur" id="pilihan-tur">
                        </div>
                        <div class="flex my-2"
                            x-data="{ count: 1, increment() { this.count++ }, decrement() { this.count === 1 ? 1 : this.count-- } }">
                            <button class="text-lg font-semibold mx-3" x-on:click="decrement()">-</button>
                            <input
                                class="block w-[50px] bg-white border border-[#828282] rounded-md py-1 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                type="number" min="1" step="1" value="1" x-model="count">
                            <button class="text-lg font-semibold mx-3" x-on:click="increment()">+</button>
                        </div>
                        <div class="flex justify-center rounded-md bg-[#23AEC1] text-white my-2">
                            <button>Tambahkan</button>
                        </div>
                    </div>
                    <div>
                        <img class="object-center object-cover rounded-lg h-[100px]"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="">
                        <div class="flex justify-between items-center my-2">
                            <p class="text-sm">Nama Ekstra</p>
                            <input class="rounded-sm" type="checkbox" name="pilihan-tur" id="pilihan-tur">
                        </div>
                        <div class="flex my-2"
                            x-data="{ count: 1, increment() { this.count++ }, decrement() { this.count === 1 ? 1 : this.count-- } }">
                            <button class="text-lg font-semibold mx-3" x-on:click="decrement()">-</button>
                            <input
                                class="block w-[50px] bg-white border border-[#828282] rounded-md py-1 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                type="number" min="1" step="1" value="1" x-model="count">
                            <button class="text-lg font-semibold mx-3" x-on:click="increment()">+</button>
                        </div>
                        <div class="flex justify-center rounded-md bg-[#23AEC1] text-white my-2">
                            <button>Tambahkan</button>
                        </div>
                    </div>
                    <div>
                        <img class="object-center object-cover rounded-lg h-[100px]"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="">
                        <div class="flex justify-between items-center my-2">
                            <p class="text-sm">Nama Ekstra</p>
                            <input class="rounded-sm" type="checkbox" name="pilihan-tur" id="pilihan-tur">
                        </div>
                        <div class="flex my-2"
                            x-data="{ count: 1, increment() { this.count++ }, decrement() { this.count === 1 ? 1 : this.count-- } }">
                            <button class="text-lg font-semibold mx-3" x-on:click="decrement()">-</button>
                            <input
                                class="block w-[50px] bg-white border border-[#828282] rounded-md py-1 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                type="number" min="1" step="1" value="1" x-model="count">
                            <button class="text-lg font-semibold mx-3" x-on:click="increment()">+</button>
                        </div>
                        <div class="flex justify-center rounded-md bg-[#23AEC1] text-white my-2">
                            <button>Tambahkan</button>
                        </div>
                    </div>
                    <div>
                        <img class="object-center object-cover rounded-lg h-[100px]"
                            src="https://images.unsplash.com/photo-1552035191-f10bd9fbf35e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1168&q=80"
                            alt="">
                        <div class="flex justify-between items-center my-2">
                            <p class="text-sm">Nama Ekstra</p>
                            <input class="rounded-sm" type="checkbox" name="pilihan-tur" id="pilihan-tur">
                        </div>
                        <div class="flex my-2"
                            x-data="{ count: 1, increment() { this.count++ }, decrement() { this.count === 1 ? 1 : this.count-- } }">
                            <button class="text-lg font-semibold mx-3" x-on:click="decrement()">-</button>
                            <input
                                class="block w-[50px] bg-white border border-[#828282] rounded-md py-1 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                type="number" min="1" step="1" value="1" x-model="count">
                            <button class="text-lg font-semibold mx-3" x-on:click="increment()">+</button>
                        </div>
                        <div class="flex justify-center rounded-md bg-[#23AEC1] text-white my-2">
                            <button>Tambahkan</button>
                        </div>
                    </div>
                </div>
            </x-slot>
            <x-slot name="footer">
                {{-- <x-jet-button wire:click="$set('showingModal', false)" wire:loading.attr="disabled">
                    {{ __('Tambahkan') }}
                </x-jet-button> --}}
            </x-slot>
        </x-jet-dialog-modal>
    </div>

    {{-- @livewireScripts --}}
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        var galleryTurSwiper = new Swiper(".gallery-tur-swiper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },});

        var carouselTurSwiper = new Swiper(".carousel-tur-swiper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            breakpoints: {
                768: {
                    slidesPerView: 3,
                },
                1280: {
                    slidesPerView: 5,
                },
            }
    });
    </script>
</div>