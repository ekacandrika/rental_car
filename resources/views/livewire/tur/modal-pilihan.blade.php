<div x-data="{ pilihans: {{ $pilihans }} }">
    <div class="flex justify-between px-3 py-2 bg-white border border-gray-500 rounded-md"
        wire:click="$set('showingModal', true)" wire:loading.attr="disabled">
        <p class="text-base font-medium text-[#BDBDBD]">Pilihan</p>
        <button type="button"><img src="{{ asset('storage/icons/circle-plus-solid 1.svg') }}" alt="plus"
                width="20px"></button>
    </div>
    <x-jet-dialog-modal wire:model="showingModal">
        <x-slot name="title">
            Pilihan
        </x-slot>
        <x-slot name="content">
            @if (!$pilihans)
            <p>Tidak ada Pilihan.</p>
            @endif
            <template x-for="(pilihan, index) in JSON.parse(pilihans.judul_pilihan)">
                <div>
                    <div class="flex items-center justify-between my-3 rounded-md border border-gray-300 py-2 px-3">
                        <div>
                            <p class="text-base font-medium" x-text="pilihan"></p>
                            <p class="text-sm font-semibold pb-3"
                                x-text="new Intl.NumberFormat().format(JSON.parse(pilihans.harga_pilihan)[index])"></p>
                            <template x-if="JSON.parse(pilihans.deskripsi_pilihan)[index] !== ''">
                                <p class="text-xs font-normal"
                                    x-text="JSON.parse(pilihans.deskripsi_pilihan)[index].replace('_', ' ').toUpperCase()">
                                </p>
                            </template>
                            <p class="text-xs font-normal"
                                x-text="JSON.parse(pilihans.kewajiban_pilihan)[index].replace('_', ' ').toUpperCase()">
                            </p>
                            <template x-if="$wire.pilihan_count[index].count > 0">
                                <div>
                                    <input type="hidden" :value="index" :name="'pilihan[' + index + '][index]'">
                                    <input type="hidden" :value="JSON.parse(pilihans.harga_pilihan)[index]"
                                        :name="'pilihan[' + index + '][harga]'">
                                    <input type="hidden" :value="JSON.parse(pilihans.deskripsi_pilihan)[index]"
                                        :name="'pilihan[' + index + '][deskripsi]'">
                                    <input type="hidden" :value="JSON.parse(pilihans.kewajiban_pilihan)[index]"
                                        :name="'pilihan[' + index + '][kewajiban]'">
                                </div>
                            </template>
                        </div>

                        <div class="flex justify-end">
                            <button type="button" class="text-xl font-semibold mx-3"
                                x-on:click="$wire.decrement(index)">-</button>
                            <template x-if="JSON.parse(pilihans.kewajiban_pilihan)[index] === 'tidak_wajib'">
                                <input
                                    class="block text-center w-[50px] bg-white border border-[#828282] rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                    type="number" min="0" step="1" value="0"
                                    :wire:model="'pilihan_count.'+index+'.count'"
                                    :name="'pilihan[' + index + '][jumlah]'">
                            </template>
                            <template x-if="JSON.parse(pilihans.kewajiban_pilihan)[index] === 'wajib'">
                                <div>
                                    <input readonly
                                        class="block text-center w-[50px] bg-slate-200 border border-slate-300 rounded-md py-2 pl-3 pr-3 shadow-sm focus:outline-none focus:border-[#23AEC1] focus:ring-[#23AEC1] focus:ring-1 sm:text-sm"
                                        type="text" x-model="$store.detail.total_count"
                                        :name="'pilihan[' + index + '][jumlah]'">
                                    <input type="hidden" :value="index" :name="'pilihan[' + index + '][index]'">
                                    <input type="hidden" :value="JSON.parse(pilihans.harga_pilihan)[index]"
                                        :name="'pilihan[' + index + '][harga]'">
                                    <input type="hidden" :value="JSON.parse(pilihans.deskripsi_pilihan)[index]"
                                        :name="'pilihan[' + index + '][deskripsi]'">
                                    <input type="hidden" :value="JSON.parse(pilihans.kewajiban_pilihan)[index]"
                                        :name="'pilihan[' + index + '][kewajiban]'">
                                </div>
                            </template>
                            <button type="button" class="text-xl font-semibold mx-3"
                                x-on:click="$wire.increment(index)">+</button>
                        </div>
                    </div>
                </div>
            </template>
        </x-slot>
        <x-slot name="footer">
            <x-jet-secondary-button wire:click="$set('showingModal', false)" wire:loading.attr="disabled">
                {{ __('Tambahkan') }}
            </x-jet-secondary-button>
        </x-slot>
    </x-jet-dialog-modal>
</div>