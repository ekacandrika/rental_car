<div>
    {{-- <div class="flex justify-between px-3 py-2 bg-white border border-gray-500 rounded-md">
        <p class="text-base font-semibold text-[#BDBDBD]">Tempat Duduk</p>
        <button type="button" wire:click="$set('showingModal', true)" wire:loading.attr="disabled">
            <img src="{{ asset('storage/icons/circle-plus-solid 1.svg') }}" alt="plus" width="20px">
        </button>
    </div> --}}
    {{-- @dump($seats); --}}
    <button type="button" wire:click="$set('showingModal', true)" wire:loading.attr="disabled"
        class="my-3 px-8 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">
        {{-- {{$button }} --}}
        Pilih Kursi
    </button>
    {{-- Modal --}}
    <div>
        <x-jet-dialog-modal wire:model="showingModal">
            <x-slot name="title">
                <div class="flex justify-between">
                    <p class="text-sm my-2">Pilih kursi yang tersedia</p>
                    <button type="button" wire:click="$set('showingModal', false)">
                        <img src="{{ asset('storage/icons/close-button.svg') }}" alt="close">
                    </button>
                </div>
            </x-slot>
            <x-slot name="content">
                
               <div class="flex justify-between gap-3" x-data="togglerBtn()">
                    <div class="lg:block">
                        <form>
                            <div id="bus_layout">
                                <div class="border-2 border-[#9E3D64] rounded-md w-69">
                                    <div class="flex  gap-1 p-5">
                                        <div class="relative w-5 h-5 rounded border border-gray-400 p-2.5 bg-[#9E3D64] mb-2" style="top: 3px;right: 4px;">
                                            {{-- <p class="font-bold text-center text-white mb-2">Driver</p> --}}
                                            {{-- <input type="radio" class="form-check-input appearance-none w-5 h-5 rounded border border-gray-400 p-2.5 checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 bg-no-repeat bg-center bg-contain cursor-pointer"> --}}
                                        </div>
                                        <div class="grid grid-cols-6 grid-flow-row gap-2">
                                            @foreach($seats as $key => $seat)
                                            <div class="form-check">
                                                <input type="hidden" value="{{$seat['kursi']}}" name="layout[{{$key}}][kursi]"/>
                                                <input type="hidden" value="{{$seat['harga']}}" name="layout[{{$key}}][harga]"/>
                                                <input type="hidden" value="{{isset($seat["avaiblity"]) ? $seat["avaiblity"] : null}}" name="layout[{{$key}}][avaiblity]" id="avaiblity-{{$key}}"/>
                                                <input type="hidden" value="{{isset($seat["user_id"]) ? $seat["user_id"] : null}}" name="layout[{{$key}}][user_id]" id="user_id-{{$key}}"/>
                                                <input
                                                    class="appearance-none rounded h-5 w-5 border border-gray-300 bg-white peer checked:bg-blue-600 focus:outline-none transition duration-200 peer checked:bg-blue-600 peer checked:border-blue-600 bg-no-repeat bg-center bg-contain cursor-pointer hidden"
                                                    type="checkbox" name="confirmation_radio_button" id="kursi-{{$key}}"
                                                    value="{{$seat['kursi']}}">
                                                @if(isset($seat["avaiblity"]))
                                                <label
                                                    class="form-check-input appearance-none rounded h-5 w-5 border border-gray-300 bg-gray-600 focus:outline-none transition duration-200 bg-no-repeat bg-center bg-contain disabled select-none"
                                                    for="kursi-{{$key}}" type="checkbox" data-no_kursi="{{$seat['kursi']}}" data-harga="{{$seat['harga']}}"
                                                    id="nokursi-{{$key}}">
                                                @else
                                                    @if(isset($seat["user_id"]) && $seat["user_id"]!= null)
                                                        @if( ($seat["user_id"] != auth()->user()->id))
                                                        <label
                                                            class="form-check-input appearance-none rounded h-5 w-5 border border-gray-300 bg-gray-400 focus:outline-none transition duration-200 bg-no-repeat bg-center bg-contain disabled select-none"
                                                            for="kursi-{{$key}}" type="checkbox" data-no_kursi="{{$seat['kursi']}}" data-harga="{{$seat['harga']}}"
                                                            id="nokursi-{{$key}}">
                                                        @else
                                                        <label
                                                            class="form-check-input appearance-none rounded h-5 w-5 border border-gray-300 bg-[#23AEC1] focus:outline-none transition duration-200 bg-no-repeat bg-center bg-contain disabled select-none"
                                                            for="kursi-{{$key}}" type="checkbox" data-no_kursi="{{$seat['kursi']}}" data-harga="{{$seat['harga']}}"
                                                            id="nokursi-{{$key}}">
                                                        @endif
                                                    @else    
                                                        <label
                                                            class="form-check-input appearance-none rounded h-5 w-5 border border-gray-300 bg-white peer-checked:bg-[#23AEC1] peer-checked:font-bold peer-checked:text-white focus:outline-none transition duration-200 bg-no-repeat bg-center bg-contain cursor-pointer select-none"
                                                            for="kursi-{{$key}}" type="checkbox" data-no_kursi="{{$seat['kursi']}}" data-harga="{{$seat['harga']}}"
                                                            id="nokursi-{{$key}}" @click="getChair">
                                                    @endif
                                                @endif
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="grid grid-cols-4 gap-2 justify-center">
                            <div class="space-x-2 mt-5">
                                <div class="text-center ml-5 rounded border border-gray-400 p-2.5 bg-[white] mb-2 w-5 h-5"></div>
                                <p class="text-center text-xs text-normal mt-2">Kosong</p>
                            </div>
                            <div class="space-x-2 mt-5">
                                {{--     margin-left: 14px; --}}
                                <div class="ml-2.5 text-center rounded border border-gray-400 p-2.5 bg-gray-400 mb-2 w-5 h-5"></div>
                                <p class="text-xs ml-[14px] text-normal mt-2" style="margin-left: 14px;">isi</p>
                                   
                            </div>
                            <div class="space-x-2 mt-5">
                                <div class="ml-2.5 text-center rounded border border-gray-400 p-2.5 bg-[#23AEC1] mb-2 w-5 h-5"></div>
                                <p class="text-xs text-normal mt-2">aktif</p>
                            </div>
                            <div class="space-x-2 mt-5">
                                <div class="ml-2.5 text-center rounded border border-gray-400 p-2.5 bg-gray-600 mb-2 w-5 h-5"></div>
                                <p class="text-xs text-normal mt-2" style="margin-left: -4px;">disabled</p>
                            </div>
                        </div>
                    </div>
                    <div class="lg:block rounded-md shadow-lg p-5 mb-5 bg-white border border-gray-200">
                        <div class="mt-5 ml-3 flex">
                            {{-- @dump($galleries) --}}
                            <img src="{{isset($galleries) ? asset($galleries) :asset('storage/img/bajo-img.png') }}" alt="bajo-img" height="100px" width="100px">
                            <div class="text-[#33333] ml-2">
                                <p class="font-semibold text-lg">{{$transfer->product_name}}</p>
                                <p class="font-medium text-base">{{auth()->user()->first_name}}</p>
                            </div>
                        </div>
                        @php
                            $date = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
                        @endphp
                        <div class="rounded-md px-3 mb-3 bg-white">
                                <template x-if="$store.rute.array_rute.length <= 0">
                                    <p class="mt-2 text-sm text-center font-semibold">Silahkan pilih rute dahulu</p>
                                </template>
                                <template x-if="$store.rute.array_rute.length >= 1">
                                    <template x-for="(rute, index) in $store.rute.array_rute" :key="index">
                                        <div>
                                            <input type="hidden" :name="'rute['+index+'][rute.naik]'" x-model="rute.naik">
                                            <input type="hidden" :name="'rute['+index+'][rute.turun]'" x-model="rute.turun">
                                            <p class="text-sm font-semibold">Dari</p>
                                            <div class="flex">
                                                <img src="{{ asset('storage/icons/bus-solid 1.svg') }}" alt="bus" width="20px"
                                                    height="20px">
                                                <p class="ml-2 text-sm" id="from-modal-seat" x-text="rute.naik"></p>
                                            </div>
                                            <img class="py-2" src="{{ asset('storage/icons/line-trans.svg') }}" alt="line">
                                            <p class="text-sm font-semibold">Ke</p>
                                            <div class="flex mb-2">
                                                <img src="{{ asset('storage/icons/location-dot-solid 1.svg') }}" alt="loc"
                                                    width="15px" height="15px">
                                                <p class="ml-2 text-sm" id="to-modal-seat" x-text="rute.turun"></p>
                                            </div>
                                        </div>
                                    </template>
                                </template>
                        </div>
                        <template x-if="chair.length <= 0">
                            <div>
                                <p class="text-normal font-semibold text-[#23AEC1] mb-2">Kursi Nomor</p>
                                <div class="space-y-1 my-2"></div>
                                <hr class="border border-[#4F4F4F] my-5"/>
                            </div>
                        </template>
                        {{-- <template x-if="chair.length == 1">
                            <template x-for="(kursi, index) in chair" :key="index"></template>
                            <div>
                                <p class="text-normal font-semibold text-[#23AEC1] mb-2">Kursi Nomor <span x-text="kursi.no_kursi"><span></p>
                                <div class="space-y-1 my-2">
                                    <div class="flex justify-between text-sm font-normal">
                                        <p class="text-[#333333]">Kursi Nomor</p>
                                        <p class="text-[#23AEC1]">- Rp. 0</p>
                                    </div>
                                </div>
                                <hr class="border border-[#4F4F4F] my-5"/>
                            </div>
                        </template> --}}
                        <template x-if="chair.length >= 1">
                            <div>
                                <p class="text-normal font-semibold text-[#23AEC1] mb-2">Kursi Nomor <span x-text="$store.bus_chair.sequence"></span></p>
                                <div class="space-y-1 my-2">
                                <template x-for="(kursi, index) in $store.bus_chair.ordered" :key="index">
                                    <div class="flex justify-between text-sm font-normal">
                                        <input type="hidden" :name="'kursi['+index+'][no]'" x-model="kursi.no_kursi">
                                        <input type="hidden" :name="'kursi['+index+'][harga]'" x-model="kursi.harga">
                                        <p class="text-[#333333]">Kursi Nomor:&nbsp;<span x-text="kursi.no_kursi"><span></p>
                                        <p class="text-[#23AEC1]">Rp. &nbsp;<span x-text="kursi.harga"><span></p>
                                    </div>
                                </template>
                                </div>
                                <hr class="border border-[#4F4F4F] my-5"/>
                                <div class="space-y-1 my-2">
                                    <div class="flex justify-between text-sm font-normal">
                                        <input type="hidden" name='harga_kursi_total' x-model="$store.bus_chair.total_paid">
                                        <p class="text-lg font-semibold">Total Bayar</p>
                                        <p class="text-lg font-semibold text-[#23AEC1]" id="total_bayar">Rp. &nbsp;<span x-text="$store.bus_chair.total_paid"><span></p></p>
                                    </div>
                                </div>
                            </div>
                        </template>
                        <div class="flex justify-between">
                            <button class="rounded-lg py-1 text-white bg-[#23AEC1]" style="padding-left: 5.5rem;padding-right: 5.5rem;">Tambahkan di koper</button>
                        </div>
                    </div>
               </div>
            </x-slot>
            <x-slot name="footer"></x-slot>
        </x-jet-dialog-modal>
    </div>
</div>
