
<!doctype html>
<html>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Kamtuu - Voucher #{{$id_booking}}</title>
    
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet'>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <style>        
        .rate{
          float: right;
          height: 46px;
          position: absolute;
          top: 59px;
          margin-left: 1px;
         }
        .rate:not(:checked)>input{
            position: relative;
            display: none;
        }
        .rate:not(:checked)>label{
            float: right;
            /* width: 3rem; */
            overflow: hidden;
            white-space:nowrap;
            font-size:30px;
            color:#ccc;
            cursor:pointer;
        }
        .rate:not(:checked)>label:before{
            content: '★ ';
        }
        span.rate:not(:checked)>input~label{
            color:#ffc107;
            font-weight: bold;
        }
        .rated{
            float: right;
            height: 46px;
            position: absolute;
            top: 87px;
            margin-left: 1px;
        }
        .rated:not(:checked)>input{
            position: absolute;
            display: none;
        }
        .rated:not(:checked)>label{
            float: right;
            width: 3rem;
            overflow: hidden;
            white-space:nowrap;
            font-size:30px;
            color:#ccc;
            cursor:pointer;
        }
        .rated:not(:checked)>label:before{
            content: '★ ';
        }
        span.rated:not(:checked)>input~label{
            color:#ffc107;
            font-weight: bold;
        }
        .kotak{
            /* text-align:center; */
            background: pink;
            width: 51px;
            height: 31px;
            padding: 2px;
        }
    </style>
</head>
<body>
    <div class="max-w-[800px] m-auto p-[30px] text-[16px] leading-6">
        {{-- @dump($booking) --}}
        <header>
            <div class="flex justify-between">
                <h3 class="text-black text-[24px] font-bold">
                    @if($booking->productdetail->product->type==='hotel')
                    Hotel Voucher
                    @endif
                    @if($booking->productdetail->product->type==='activity')
                    Activity Voucher
                    @endif
                    @if($booking->productdetail->product->type==='tour')
                    Tour Voucher
                    @endif
                    @if($booking->productdetail->product->type==='rental')
                    Rental Voucher
                    @endif
                    @if($booking->productdetail->product->type==='transfer')
                    Transfer Voucher
                    @endif  
                </h3>
                <div>
                    <img src="{{Storage::url('img/kamtuu-logo.png')}}" alt="kamtuu" class="float-right" width="22%">
                </div>
            </div>
            <div class="flex justify-between mt-2">
                <div class="">
                    <p class="text-[16px] mb-2">{{$booking->productdetail->product->type==='hotel' ? 'Itinary ID:':'Booking ID'}}</p>
                    <p class="text-sky-400 text-[30px] mb-3">{{$id_booking}}</p>
                    <p class="font-bold text-[14px]">Pesanan anda telah dikonfirmasi</p>
                </div>
                <div class="">
                    <p class="font-bold text-[22px]">
                        {{$booking->productdetail->product->product_name}}
                        @php
                        $rating =  App\Models\reviewPost::leftJoin('product','product.id','=','review_posts.product_id')
                                   ->where('slug',$booking->productdetail->product->slug)->get();   
                        // dump($rating);
                        @endphp
                        <span class="rate">
                            @if(isset($rating->star_rating))
                                @for($i=0; $i <= $rating->star_rating;$i++)
                                <input type="radio" name="" class="rated" id="">
                                <label for="" class="star-rating"></label>
                                @endfor
                            @endif
                        </span>
                    </p>
                    <div class="flex mt-2">
                        @if($booking->productdetail->product->type==='hotel' || $booking->productdetail->product->type==='xstay')
                         <div class="border-l-4 border-indigo-500 mr-3">
                            <p class="font-thin text-[12px] ml-3">Tgl Check-in</p>
                            <p class="font-bold text-xl ml-3">{{isset($booking->activity_date) ? \Carbon\Carbon::parse($booking->activity_date)->format('d M Y'): \Carbon\Carbon::parse($booking->tgl_checkin)->format('d M Y')}}</p>
                        </div>
                        <div class="border-l-4 border-indigo-500">
                            <p class="font-thin text-[12px] ml-3">Tgl Check-out</p>
                            <p class="font-bold text-xl ml-3">{{isset($booking->activity_date) ? \Carbon\Carbon::parse($booking->activity_date)->format('d M Y'): \Carbon\Carbon::parse($booking->tgl_checkout)->format('d M Y')}}</p>
                        </div>
                        @else
                        <div class="border-l-4 border-indigo-500">
                            <p class="font-thin text-[12px] ml-3">Tgl Booking</p>
                            <p class="font-bold text-xl ml-3">{{isset($booking->activity_date) ? \Carbon\Carbon::parse($booking->activity_date)->format('d M Y'): \Carbon\Carbon::parse($booking->tgl_checkout)->format('d M Y')}}</p>
                        </div>
                        {{-- <div class="border-l-4 border-indigo-500">
                            <p class="font-thin text-[12px] ml-3">Tgl Booking</p>
                            <p class="font-bold text-xl ml-3">{{isset($booking->activity_date) ? \Carbon\Carbon::parse($booking->activity_date)->format('d M Y'): \Carbon\Carbon::parse($booking->tgl_checkout)->format('d M Y')}}</p>
                        </div> --}}
                        @endif
                    </div>
                </div>
                <div class=""></div>
                {{-- <div>
                </div>
                <div>
                    <p class="text-bold text-[16px]">{{$booking->productdetail->product->product_name}}</p>
                </div>
                <div></div> --}}
            </div>
            <div class="mt-4 border-t-4 border-black"></div>
        </header>
        <section>
            <div class="flex mt-4">
                @php
                $gallery = json_decode($booking->productdetail->gallery,true);
                @endphp
                <div class="kotak-1 pr-[37.4px]">
                    <img src={{isset($booking->productdetail->thumbnail) ? asset($booking->productdetail->thumbnail) :"https://source.unsplash.com/600x400?hotel"}} class="bg-cover object-cover w-[370px]" alt="">
                </div>
                <div class="kotak-2 w-[325px]">
                    <div class="kotak-3">
                        <h5 class="text-slate-600">Booking details</h5>
                        <table class="table text-slate-600" style="width: 581px;">
                            <tr>
                                <td width="20%" colspan="2">
                                    <p class="text-muted">Traveller&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    @php
                                    $nama = "";
                                    if(is_array(json_decode($booking->first_name, true)) && is_array(json_decode($booking->last_name, true))){
                                        $first_name = json_decode($booking->first_name, true);
                                        $last_name  = isset($booking->last_name) ? json_decode($booking->last_name): null;
                                        $nama = $first_name[0].' '.$last_name[0];
                                        // dump($first_name[0]);
                                    }else{
                                        $nama =  $booking->first_name.' '.isset($booking->last_name) ? $booking->last_name : '';
                                    }
                                    @endphp
                                    <p class="text-muted">{{$nama}}</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="border-t-2 border-black mt-4">
                    <div class="kotak-4 text-slate-600 mt-4">
                        @if($booking->type==='hotel' || $booking->type==='xstay')
                        <table class="table" style="width: 581px;">
                            <tr>
                                <td width="20%" colspan="2">
                                    <p class="text-muted">Jumlah tamu&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    <p class="text-muted">{{isset($booking->peserta_dewasa) ? $booking->peserta_dewasa.' Dewasa':'1 Dewasa'}} {{isset($booking->peserta_anak) ? $booking->peserta_anak.' Anak-anak':''}} {{isset($booking->peserta_balita) ? $booking->peserta_balita.' Balita':''}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2">
                                    <p class="text-muted">Tipe kamar&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    @php
                                    $kamar =  App\Models\Masterkamar::where('id',$booking->kamar_id)->first();
                                    @endphp
                                    <p class="text-muted">{{$kamar->jenis_kamar}}</p>
                                    <p class="text-muted">{{$kamar->kode_kamar}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2">
                                    <p class="text-muted">Ekstra&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    <p class="text-muted">{{$booking->productdetail->izin_ekstra}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2">
                                    <p class="text-muted">Amenitas&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    @php
                                    $id_amenitas = isset($booking->productdetail->id_amenitas) ? json_decode($booking->productdetail->id_amenitas,true):[];
                                    $amenitas ="";
                                    $x = 0;
                                    foreach ($id_amenitas['result'] as  $value) {
                                        $isi_amenitas = App\Models\Attributes::where('id',$value)->first();
                                        $amenitas .="<img src=".Storage::url($isi_amenitas->image)." alt=".$isi_amenitas->text." width='20px' height='20px'/>";

                                        if(count($id_amenitas['result'])!=$x){
                                            $amenitas .= ", ";
                                        }
    
                                        $x++;
                                    }
                                    @endphp
                                    <p class="flex">
                                        {!! $amenitas !!}
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2">
                                    <p class="text-muted">Fasilitas&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    @php
                                    $id_fasilitas = isset($booking->productdetail->id_fasilitas) ? json_decode($booking->productdetail->id_fasilitas,true):[];
                                    $fasilitas ="";
                                    $i = 0;
                                    foreach ($id_fasilitas['result'] as  $value) {
                                        $isi_fasilitas = App\Models\Attributes::where('id',$value)->first();
                                        $fasilitas .="<img src=".Storage::url($isi_fasilitas->image)." alt=".$isi_fasilitas->text." width='20px' height='20px'/>";

                                        if(count($id_fasilitas['result'])!=$i){
                                            $fasilitas .= ", ";
                                        }
    
                                        $i++;
                                    }
                                    @endphp
                                    <p class="flex">
                                       {!! $fasilitas !!}
                                    </p>
                                </td>
                            </tr>
                        </table>
                        @elseif($booking->type==='transfer')
                        <table class="table" style="width: 581px;">
                            <tr>
                                <td width="20%" colspan="2">
                                    <p class="text-muted">Rute&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    @if($booking->productdetail->rute_id)
                                    @php
                                    $rute = App\Models\Masterroute::where('id',$booking->productdetail->rute_id)->first();
    
                                    $detail_rute = json_decode($rute->rute, true)['result'];
                                    $from = App\Models\District::where('id',$detail_rute['from']);
                                    $to = App\Models\District::where('id',$detail_rute['to']);
                                    @endphp
                                    <p class="text-muted">{{$detail_rute['title']}} dari {{$from}} ke {{$to}}</p>
                                    @else
                                    @php
                                    $rutes = json_decode($booking->productdetail->drop_pick_detail,true)['result'];
                                    @endphp
                                    <ul class="text-muted">
                                    @foreach($rutes as $rute)
                                        <li>
                                            {{$rute['judul_bus']}} dari {{$rute['tempat_pickup']}} ke {{$rute['tempat_drop']}}
                                        </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2">
                                    <p class="text-muted">Ekstra&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    <p class="text-muted">{{$booking->productdetail->izin_ekstra}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2">Pilihan&nbsp;:</td>
                                <td></td>
                                <td>
                                    <p class="text-muted"></p>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2">Harga&nbsp;:</td>
                                <td></td>
                                <td>
                                    @if($booking->total_price)
                                    <p class="text-muted">Rp. {{number_format($booking->total_price)}}</p>
                                    @endif  
                                </td>
                            </tr>
                        </table>
                        @elseif($booking->type==='tour')
                        <table class="table" style="width: 581px;">
                            <tr>
                                <td width="30%" colspan="2">
                                    <p class="text-muted">Jumlah Peserta&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                @php
                                $peserta = App\Models\Discount::where('id',$booking->productdetail->discount_id)->first();
    
                                $min_orang = isset($peserta->min_orang) ? count(json_decode($peserta->min_orang,true)) :0;
                                $max_orang = isset($peserta->max_orang) ? count(json_decode($peserta->max_orang,true)) :0;
    
                                $dewasa = 0;
                                $anak   = 0;
                                $balita = 0;
                                $peserta  = json_decode($booking->kategori_peserta, true);
                                // dd($peserta);    
                                foreach ($peserta as $key => $p) {
                                    if($p=='dewasa'){
                                        $dewasa +=1;
                                    }
                                    else if($p=='anak'){
                                        $anak +=1;
                                    }elseif($p=='balita'){
                                        $balita +=1;
                                    }
                                }
                                
                                @endphp
                                <p class="text-muted"> {{$dewasa != 0 ? $dewasa.' orang dewasa': ''}}  {{$anak != 0 ? $anak.' orang anak': ''}}  {{$balita != 0 ? $balita.' orang balita': ''}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2">
                                    <p class="text-muted">Durasi&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    <p class="text-muted">{{$booking->productdetail->jml_hari.' Hari'}} {{$booking->productdetail->jml_malam.' Malam'}}</p>
                                    <p class="text-muted">{{$booking->productdetail->izin_ekstra}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted" width="20%" colspan="2">Pilihan&nbsp;:</td>
                                <td></td>
                                <td>
                                    <p class="text-muted"></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted" width="20%" colspan="2">Harga&nbsp;:</td>
                                <td></td>
                                <td>
                                    <p class="text-muted">Rp. {{number_format($booking->total_price)}}</p>
                                </td>
                            </tr>
                        </table>
                        @elseif($booking->type==='rental')
                        @php
                        $detail_mobil = App\Models\DetailKendaraan::where("id",$booking->productdetail->id_detail_kendaraan)->first();
                        $jenis_mobil  = App\Models\MerekMobil::where("id",$detail_mobil->id_jenis_mobil)->first();
                        @endphp
                        <table class="table" style="width: 581px;">
                            <tr>
                                <td width="20%" colspan="2">
                                    <p class="text-muted">Untuk&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    <p class="text-muted">{{isset($booking->kategori_peserta) ? count(json_decode($booking->kategori_peserta, true)).' x '.json_decode($booking->kategori_peserta, true)[0]: '1 Dewasa'}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2">
                                    <p class="text-muted">Nama Mobil&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    <p class="text-muted text-capitalize">{{isset($jenis_mobil->merek_mobil) ? $jenis_mobil->merek_mobil : ''}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%" colspan="2">
                                    <p class="text-muted">Jumlah Kursi&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    <p class="text-muted text-capitalize">{{isset($detail_mobil->kapasitas_kursi) ? $detail_mobil->kapasitas_kursi : ''}} Kursi</p>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2">
                                    <p class="text-muted">No Polisi&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    <p class="text-muted text-capitalize">{{isset($jenis_mobil->plat_mobil) ? $jenis_mobil->plat_mobil : ''}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2">
                                    <p class="text-muted">Ekstra&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    <p class="text-muted text-capitalize">{{isset($booking->productdetail->izin_ekstra) ? $booking->productdetail->izin_ekstra : ''}}</p>
                                </td>
                            </tr>
                        </table>
                        @else
                        <table class="table" style="width: 581px;">
                            <td width="20%" colspan="2">
                                <p class="text-muted">Untuk&nbsp;:</p>
                            </td>
                            <td></td>
                            <td>
                                <p class="text-muted">{{isset($booking->kategori_peserta) ? count(json_decode($booking->kategori_peserta, true)).' x '.json_decode($booking->kategori_peserta, true)[0]: '1 Dewasa'}}</p>
                            </td>
                            <tr>
                                <td width="20%" colspan="2">
                                    <p class="text-muted">Ekstra&nbsp;:</p>
                                </td>
                                <td></td>
                                <td>
                                    <p class="text-muted text-capitalize">{{isset($booking->productdetail->izin_ekstra) ? $booking->productdetail->izin_ekstra : ''}}</p>
                                </td>
                            </tr>
                        </table>    
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="mt-4 border-t-4 border-black"></div>
            <div class="flex justify-between mt-4">
                <div class="text-justify pr-[41px] w-[641px]">
                    @if(isset($booking->productdetail->kebijakan_pembatalan_sebelumnya) && isset($booking->productdetail->kebijakan_pembatalan_sebelumnya))
                    <h4 class="text-slate-600 font-bold text-[16px] mb-4">Kebijakan Pembatalan</h4>
                    <p class="text-wrap">
                        Kami mengerti bahwa terkadang rencana dapat gagal. Kami tidak membebankan biaya pembatalan atau perubahan. Jika
                        properti mengenakan biaya seperti ini sesuai dengan kebijakannya, maka biaya tersebut akan disampaikan kepada Anda. {{$booking->productdetail->product->product_name}} mengenakan biaya pembatalan dan perubahan berikut.
                        @php
                            $arr_pembatalan = [];
                            
                            $pembatalan_sebelum = "";
                            $potongan_sebelum = '';
                            
                            $x = 0;
                            if(isset($booking->productdetail->kebijakan_pembatalan_sebelumnya)){
                                $sebelum  = json_decode($booking->productdetail->kebijakan_pembatalan_sebelumnya, true);
                                $potongan = json_decode($booking->productdetail->kebijakan_pembatalan_potongan, true);
                                
                                
                                for ($i = 0; $i < count($sebelum);$i++) {
                                    // echo $sebelum[$i];
                                    if(isset($sebelum['sebelumnya']) && isset($potongan['potongan'])){
                                        $arr_pembatalan[$x]['sebelum']  = $sebelum['sebelumnya'][$i];
                                        $arr_pembatalan[$x]['potongan'] = $potongan['potongan'][$i];


                                    }else{
                                        $arr_pembatalan[$x]['sebelum']  = $sebelum[$i];
                                        $arr_pembatalan[$x]['potongan'] = $potongan[$i];
                                    }
                                    $x++;
                                }
                                // dump($arr_pembatalan);
                            }else{
                                $pembatalan_sebelum =  '1 hari';
                                $potongan_sebelum   = '100%';
                            }
            
                        @endphp
                        @if(count($arr_pembatalan) > 0)
                        Pembatalan atau perubahan yang dilakukan pada:
                        <ul class="list-disc pl-[41px] mb-3">
                            @foreach($arr_pembatalan as $pembatalan_sebelumnya)
                            <li>
                                {{$pembatalan_sebelumnya['sebelum']}} hari sebelumnya (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($booking->tgl_checkout)->format('d M Y')}} atau ketidakdatangan dikenakan biaya properti sebesar {{$pembatalan_sebelumnya['potongan']}}% dari jumlah total yang dibayarkan untuk reservasi
                            </li>
                            @endforeach
                        </ul>
                        @else
                        Pembatalan atau perubahan yang dilakukan pada {{$pembatalan_sebelum}} sebelumnya (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($booking->tgl_checkout)->format('d M Y')}} atau ketidakdatangan dikenakan biaya properti sebesar dari jumlah total yang dibayarkan untuk reservasi.
                        @endif
                        @php
                            $arr_seteleh = [];
                            
                            $pembatalan_setelah = "";
                            $potongan_setelah = '';
                            // dump($booking->productdetail);
                            $x = 0;
                            if(isset($booking->productdetail->kebijakan_pembatalan_sesudah)){
                                $setelah  = json_decode($booking->productdetail->kebijakan_pembatalan_sesudah, true);
                                $potongan_setelah = json_decode($booking->productdetail->kebijakan_pembatalan_potongan, true);
                                
                                // dd($setelah);

                                for ($i = 0; $i < count($setelah);$i++) {

                                    if(isset($setelah['sesudah']) && isset($potongan_setelah['potongan'])){
                                        $arr_seteleh[$x]['setelah']  = $setelah['sesudah'][$i];
                                        $arr_seteleh[$x]['potongan'] = $potongan_setelah['potongan'][$i];


                                    }else{
                                        $arr_seteleh[$x]['setelah']  = $setelah[$i];
                                        $arr_seteleh[$x]['potongan'] = $potongan_setelah[$i];
                                    }
                                    $x++;
                                }
                            }else{
                                $pembatalan_setelah =  '1 hari';
                                $potongan_setelah   = '100%';
                            }
            
                        @endphp
                        {{-- @dump(json_decode($booking->productdetail->kebijakan_pembatalan_setelah, true)) --}}
                        @if(count($arr_seteleh) > 0)
                        <ul class="list-disc pl-[41px]">
                            @foreach($arr_seteleh as $pembatalan_setelah)
                            <li>
                                {{$pembatalan_setelah['setelah']}} hari sebelumnya (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($booking->tgl_checkout)->format('d M Y')}} atau ketidakdatangan dikenakan biaya properti sebesar {{$pembatalan_setelah['potongan']}}% dari jumlah total yang dibayarkan untuk reservasi
                            </li>
                            @endforeach
                        </ul>
                        {{-- @elseif(count($arr_pembatalan) > 0 && )
                        Pembatalan atau perubahan yang dilakukan pada: 
                        <ul class="list-disc pl-[41px]">
                            <li>
                                {{$pembatalan_setelah}} setelah  (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($booking->tgl_checkout)->format('d M Y')}} atau 
                                ketidakdatangan dikenakan biaya properti sebesar {{$potongan_setelah}} dari jumlah total yang dibayarkan untuk reservasi
                            </li>
                        </ul> --}}
                        @else
                        Pembatalan atau perubahan yang dilakukan pada
                        {{$pembatalan_setelah}} setelah  (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($booking->tgl_checkout)->format('d M Y')}} atau
                        ketidakdatangan dikenakan biaya properti sebesar {{$potongan_setelah}} dari jumlah total yang dibayarkan untuk reservasi
                        @endif
                    </p>
                    @endif
                    @if($booking->type==="hotel"  || $booking->type==="xstay")
                    <h4 class="text-slate-600 font-bold text-[16px] mt-4 mb-4">Petunjuk Kedatangan Terlambat</h4>
                    <p>
                        Jika Anda akan check-in terlambat, hubungi langsung properti ini untuk mengetahui kebijakan check-in terlambat mereka.
                    </p>
                    <h4 class="text-slate-600 font-bold text-[16px] mb-4">Kebijakan Checkin</h4>
                    <p class="mb-4">
                        <ul>
                            <li>Waktu check-in mulai pukul 14.00</li>
                            <li>Waktu check-in berakhir pukul tengah malam</li>
                            <li>Usia minimal untuk check-in adalah: 18</li>
                        </ul>
                    </p>
                    @else
                        @if(isset($booking->productdetail->kebijakan_pembatalan_sebelumnya) && isset($booking->productdetail->kebijakan_pembatalan_sebelumnya))
                            <h4 class="text-slate-600 font-bold text-[16px] mb-3 mt-2">Catatan</h4>
                            <p>
                                {!!$booking->productdetail->catatan!!}
                            </p>
                        @else
                            <h4 class="text-slate-600 font-bold text-[16px] mb-3 mt-2">Catatan</h4>
                            <p class="text-wrap"style="width: 751px;text-align: justify;padding-right: 41px">
                                {!!$booking->productdetail->catatan!!}
                            </p>
                        @endif
                    @endif
                </div>
                <div class="text-center">
                    {{-- barkode --}}
                    <h4 class="text-slate-600 font-bold text-[16px] text-center mb-2">Scan disini</h4>
                    <img src="{{isset($booking->productdetail->base_url) ? "data:image/png;base64,".DNS2D::getBarcodePNG($booking->productdetail->base_url,'QRCODE'):asset('img/qrcode.png')}}" alt="">
                </div>
            </div>
        </section>
        <section>
            <div class="mt-10 grid justify-items-center">
                <div id="button-container">
                    <a class="mx-2 p-2 bg-kamtuu-second text-white rounded-lg" href="{{ route('donwload-pdf', [$id_booking]) }}" id="download-button">Download Voucher</a>            
                    <button id="download-button2" class="p-2 bg-kamtuu-primary text-white rounded-lg" onclick="printVoucher()">Print Voucher</button>
                </div>
            </div>
        </section>
    </div>
    <script>
        function printVoucher(){
            var downloadButton2 = document.getElementById("download-button2");
            var downloadButton  = document.getElementById("download-button");
            var buttonContainer = document.getElementById("button-container");

            buttonContainer.removeChild(downloadButton);
            buttonContainer.removeChild(downloadButton2);

            window.print();

            buttonContainer.appendChild(downloadButton);
            buttonContainer.appendChild(downloadButton2);
        }
    </script>
</body>
</html>