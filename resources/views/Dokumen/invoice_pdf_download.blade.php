<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>INVOICE</title>


    @vite(['resources/css/app.css', 'resources/js/app.js'])


    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(4) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(4) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
    </style>
</head>

<body>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="4">
                <table>
                    <tr>
                        <td class="title">
                            <img src="data:image/png;base64,
                                 <?php echo base64_encode(file_get_contents(base_path('public/'.'1670555064kamtuu-logo.jpg'))); ?>"
                                 width="200px"></td>
                        <td></td>
                        <td></td>

                        <td>
                            Invoice #: {{$bookings?->booking_code}}<br>
                            Created: {{\Carbon\Carbon::parse($bookings?->created_at)->format('d \of F, Y')}}<br>
                            {{--                            Due: February 1, 2015--}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="4">
                <table>
                    <tr>
                        <td>
                            PT. Kamtuu Wisata Indonesia<br>
                            Jl. Seruni Blok D29C, <br>
                            Kompleks Hasanuddin (Pandang-pandang)
                            Sungguminsa – Gowa<br>
                            92116
                            Sulawesi Selatan
                            Indonesia
                        </td>
                        <td></td>
                        <td></td>

                        <td>
                            {{$bookings->users->first_name_booking}}<br>
                            {{$bookings->users->last_name_booking}}<br>
                            {{$bookings->users->email_booking}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        {{--        <tr class="heading">--}}
        {{--            <td>--}}
        {{--                Payment Method--}}
        {{--            </td>--}}

        {{--            <td>--}}
        {{--                Check #--}}
        {{--            </td>--}}
        {{--        </tr>--}}

        {{--        <tr class="details">--}}
        {{--            <td>--}}
        {{--                Check--}}
        {{--            </td>--}}

        {{--            <td>--}}
        {{--                1000--}}
        {{--            </td>--}}
        {{--        </tr>--}}

        <tr class="heading">
            <td>
                Item
            </td>

            <td>
                Quantity
            </td>

            <td>
                Category
            </td>

            <td>
                Price
            </td>
        </tr>

        @forelse($data as $item)
            <tr class="item">
                <td>

                    {{--                    {{dd($item)}}--}}

                    {{$item->bookings->productdetail->product->product_name}}
                </td>

                <td>
                    {{$item->bookings->peserta_dewasa+$item->bookings->peserta_anak_anak+$item->bookings->peserta_balita}}
                </td>

                <td>
                    {{$item->bookings->productdetail->product->type}}
                </td>


                <td>
                    Rp. {{number_format($item->bookings->total_price, 2, '.', ',')}}
                </td>
            </tr>
        @empty
            <tr class="item">
                <td>
                    Tidak ada yang harus dibayar</td>
            </tr>
        @endforelse

        {{--        <tr class="item">--}}
        {{--            <td>--}}
        {{--                Hosting (3 months)--}}
        {{--            </td>--}}

        {{--            <td>--}}
        {{--                $75.00--}}
        {{--            </td>--}}
        {{--        </tr>--}}

        {{--        <tr class="item last">--}}
        {{--            <td>--}}
        {{--                Domain name (1 year)--}}
        {{--            </td>--}}

        {{--            <td>--}}
        {{--                $10.00--}}
        {{--            </td>--}}
        {{--        </tr>--}}

        <tr class="total">
            <td></td>
            <td></td>
            <td></td>
            <td>
                @php
                    $sum = 0;
                @endphp
                @foreach ($data as $number)
                    @php
                        $sum += $number->bookings->total_price;
                    @endphp
                @endforeach

                Total : Rp. {{number_format($sum, 2, '.', ',')}}
            </td>
        </tr>
    </table>

    {{--                    {{dd($order)}}--}}
</div>

</body>
</html>
