<!DOCTYPE html>
<html>
<head>
    <title>Receipt</title>
    <style>
        /* Add your CSS styles for the receipt here */
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
        }

        .receipt {
            max-width: 600px;
            margin: 0 auto;
            background-color: #ffffff;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
            padding: 20px;
        }

        .receipt-header {
            text-align: center;
            margin-bottom: 20px;
        }

        .receipt-header h1 {
            font-size: 24px;
            color: #333333;
            margin: 0;
        }

        .receipt-content {
            margin-bottom: 20px;
        }

        .receipt-content p {
            font-size: 16px;
            color: #555555;
            margin: 0;
        }

        .receipt-total {
            text-align: right;
        }

        .receipt-total p {
            /*font-size: 20px;*/
            color: #333333;
            font-weight: 800;
            margin: 0;
        }

        .receipt-footer {
            text-align: center;
            margin-top: 20px;
        }

        .receipt-footer p {
            font-size: 14px;
            color: #777777;
            margin: 0;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.receipt').fadeIn(1000);
        });
    </script>
</head>
<body>
<div class="receipt">
    <div class="receipt-header">
                <h1>Receipt</h1>
        <img src="data:image/png;base64,
 <?php echo base64_encode(file_get_contents(base_path('public/'.'1670555064kamtuu-logo.jpg'))); ?>"
             width="200px">
    </div>

    <div class="receipt-content">
        <p>Invoice Number: {{$order}}/RCPT/KAMTUU/{{\Carbon\Carbon::parse($bookings?->created_at)->format('d-m')}} </p>
        <p>Date: {{\Carbon\Carbon::parse($bookings?->created_at)->format('d \of F, Y')}}</p>

    </div>

    <div class="receipt-content">
        <p>Hallo {{$bookings->users->first_name_booking}}
            {{$bookings->users->last_name_booking}},<br>
            pembayaran anda telah kami terima dengan rincian sebagai berikut :
        </p><br>

        <p>
            @foreach ($orders as $index => $item)
                @if ($index === 0)
                    <span> Kode Checkout Anda : {{ $item->external_id }} <br><br></span>
                    <span>Metode Pembayaran : {{$item->payment_channel}} <br><br></span>
                    <span>Tanggal Pembayaran : {{\Carbon\Carbon::parse($bookings?->updated_at)->format('d \of F, Y')}}<br><br></span>

        @break
        @endif

        @endforeach
        <div class="receipt-total">
            @php
                $sum = 0;
            @endphp
            @foreach ($orders as $number)
                @php
                    $sum += $number->bookings->total_price;
                @endphp
            @endforeach


            <p>Total: Rp. {{number_format($sum, 2, '.', ',')}}</p>
        </div>
        </p>
    </div>



    <div class="receipt-footer">
        <p>Thank you for your purchase!</p>
    </div>

    <hr>
    <p class="receipt-footer">
        PT. Kamtuu Wisata Indonesia
        <br>
        Jl. Seruni Blok D29C, Kompleks Hasanuddin (Pandang-pandang)

        Sungguminsa – Gowa
        92116
        Sulawesi Selatan
        Indonesia
    </p>

</div>

{{--{{dd($bookings)}}--}}

</body>
</html>
