<!doctype html>
<html>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>TEST # {{$booking}}</title>
    <style>
        /*
        ! tailwindcss v3.1.8 | MIT License | https://tailwindcss.com
        */

        /*
        1. Prevent padding and border from affecting element width. (https://github.com/mozdevs/cssremedy/issues/4)
        2. Allow adding a border to an element by just adding a border-width. (https://github.com/tailwindcss/tailwindcss/pull/116)
        */

        *,
        ::before,
        ::after {
        box-sizing: border-box;
        /* 1 */
        border-width: 0;
        /* 2 */
        border-style: solid;
        /* 2 */
        border-color: #e5e7eb;
        /* 2 */
        }

        ::before,
        ::after {
        --tw-content: '';
        }

        /*
        1. Use a consistent sensible line-height in all browsers.
        2. Prevent adjustments of font size after orientation changes in iOS.
        3. Use a more readable tab size.
        4. Use the user's configured `sans` font-family by default.
        */

        html {
        line-height: 1.5;
        /* 1 */
        -webkit-text-size-adjust: 100%;
        /* 2 */
        -moz-tab-size: 4;
        /* 3 */
        -o-tab-size: 4;
            tab-size: 4;
        /* 3 */
        font-family: Nunito, ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        /* 4 */
        }

        /*
        1. Remove the margin in all browsers.
        2. Inherit line-height from `html` so users can set them as a class directly on the `html` element.
        */

        body {
        margin: 0;
        /* 1 */
        line-height: inherit;
        /* 2 */
        }

        /*
        1. Add the correct height in Firefox.
        2. Correct the inheritance of border color in Firefox. (https://bugzilla.mozilla.org/show_bug.cgi?id=190655)
        3. Ensure horizontal rules are visible by default.
        */

        hr {
        height: 0;
        /* 1 */
        color: inherit;
        /* 2 */
        border-top-width: 1px;
        /* 3 */
        }

        /*
        Add the correct text decoration in Chrome, Edge, and Safari.
        */

        abbr:where([title]) {
        -webkit-text-decoration: underline dotted;
                text-decoration: underline dotted;
        }

        /*
        Remove the default font size and weight for headings.
        */

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
        font-size: inherit;
        font-weight: inherit;
        }

        /*
        Reset links to optimize for opt-in styling instead of opt-out.
        */

        a {
        color: inherit;
        text-decoration: inherit;
        }

        /*
        Add the correct font weight in Edge and Safari.
        */

        b,
        strong {
        font-weight: bolder;
        }

        /*
        1. Use the user's configured `mono` font family by default.
        2. Correct the odd `em` font sizing in all browsers.
        */

        code,
        kbd,
        samp,
        pre {
        font-family: ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
        /* 1 */
        font-size: 1em;
        /* 2 */
        }

        /*
        Add the correct font size in all browsers.
        */

        small {
        font-size: 80%;
        }

        /*
        Prevent `sub` and `sup` elements from affecting the line height in all browsers.
        */

        sub,
        sup {
        font-size: 75%;
        line-height: 0;
        position: relative;
        vertical-align: baseline;
        }

        sub {
        bottom: -0.25em;
        }

        sup {
        top: -0.5em;
        }

        /*
        1. Remove text indentation from table contents in Chrome and Safari. (https://bugs.chromium.org/p/chromium/issues/detail?id=999088, https://bugs.webkit.org/show_bug.cgi?id=201297)
        2. Correct table border color inheritance in all Chrome and Safari. (https://bugs.chromium.org/p/chromium/issues/detail?id=935729, https://bugs.webkit.org/show_bug.cgi?id=195016)
        3. Remove gaps between table borders by default.
        */

        table {
        text-indent: 0;
        /* 1 */
        border-color: inherit;
        /* 2 */
        border-collapse: collapse;
        /* 3 */
        }

        /*
        1. Change the font styles in all browsers.
        2. Remove the margin in Firefox and Safari.
        3. Remove default padding in all browsers.
        */

        button,
        input,
        optgroup,
        select,
        textarea {
        font-family: inherit;
        /* 1 */
        font-size: 100%;
        /* 1 */
        font-weight: inherit;
        /* 1 */
        line-height: inherit;
        /* 1 */
        color: inherit;
        /* 1 */
        margin: 0;
        /* 2 */
        padding: 0;
        /* 3 */
        }

        /*
        Remove the inheritance of text transform in Edge and Firefox.
        */

        button,
        select {
        text-transform: none;
        }

        /*
        1. Correct the inability to style clickable types in iOS and Safari.
        2. Remove default button styles.
        */

        button,
        [type='button'],
        [type='reset'],
        [type='submit'] {
        -webkit-appearance: button;
        /* 1 */
        background-color: transparent;
        /* 2 */
        background-image: none;
        /* 2 */
        }

        /*
        Use the modern Firefox focus style for all focusable elements.
        */

        :-moz-focusring {
        outline: auto;
        }

        /*
        Remove the additional `:invalid` styles in Firefox. (https://github.com/mozilla/gecko-dev/blob/2f9eacd9d3d995c937b4251a5557d95d494c9be1/layout/style/res/forms.css#L728-L737)
        */

        :-moz-ui-invalid {
        box-shadow: none;
        }

        /*
        Add the correct vertical alignment in Chrome and Firefox.
        */

        progress {
        vertical-align: baseline;
        }

        /*
        Correct the cursor style of increment and decrement buttons in Safari.
        */

        ::-webkit-inner-spin-button,
        ::-webkit-outer-spin-button {
        height: auto;
        }

        /*
        1. Correct the odd appearance in Chrome and Safari.
        2. Correct the outline style in Safari.
        */

        [type='search'] {
        -webkit-appearance: textfield;
        /* 1 */
        outline-offset: -2px;
        /* 2 */
        }

        /*
        Remove the inner padding in Chrome and Safari on macOS.
        */

        ::-webkit-search-decoration {
        -webkit-appearance: none;
        }

        /*
        1. Correct the inability to style clickable types in iOS and Safari.
        2. Change font properties to `inherit` in Safari.
        */

        ::-webkit-file-upload-button {
        -webkit-appearance: button;
        /* 1 */
        font: inherit;
        /* 2 */
        }

        /*
        Add the correct display in Chrome and Safari.
        */

        summary {
        display: list-item;
        }

        /*
        Removes the default spacing and border for appropriate elements.
        */

        blockquote,
        dl,
        dd,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        hr,
        figure,
        p,
        pre {
        margin: 0;
        }

        fieldset {
        margin: 0;
        padding: 0;
        }

        legend {
        padding: 0;
        }

        ol,
        ul,
        menu {
        list-style: none;
        margin: 0;
        padding: 0;
        }

        /*
        Prevent resizing textareas horizontally by default.
        */

        textarea {
        resize: vertical;
        }

        /*
        1. Reset the default placeholder opacity in Firefox. (https://github.com/tailwindlabs/tailwindcss/issues/3300)
        2. Set the default placeholder color to the user's configured gray 400 color.
        */

        input::-moz-placeholder, textarea::-moz-placeholder {
        opacity: 1;
        /* 1 */
        color: #9ca3af;
        /* 2 */
        }

        input::placeholder,
        textarea::placeholder {
        opacity: 1;
        /* 1 */
        color: #9ca3af;
        /* 2 */
        }

        /*
        Set the default cursor for buttons.
        */

        button,
        [role="button"] {
        cursor: pointer;
        }

        /*
        Make sure disabled buttons don't get the pointer cursor.
        */

        :disabled {
        cursor: default;
        }

        /*
        1. Make replaced elements `display: block` by default. (https://github.com/mozdevs/cssremedy/issues/14)
        2. Add `vertical-align: middle` to align replaced elements more sensibly by default. (https://github.com/jensimmons/cssremedy/issues/14#issuecomment-634934210)
        This can trigger a poorly considered lint error in some tools but is included by design.
        */

        img,
        svg,
        video,
        canvas,
        audio,
        iframe,
        embed,
        object {
        display: block;
        /* 1 */
        vertical-align: middle;
        /* 2 */
        }

        /*
        Constrain images and videos to the parent width and preserve their intrinsic aspect ratio. (https://github.com/mozdevs/cssremedy/issues/14)
        */

        img,
        video {
        max-width: 100%;
        height: auto;
        }

        [type='text'],[type='email'],[type='url'],[type='password'],[type='number'],[type='date'],[type='datetime-local'],[type='month'],[type='search'],[type='tel'],[type='time'],[type='week'],[multiple],textarea,select {
        -webkit-appearance: none;
            -moz-appearance: none;
                appearance: none;
        background-color: #fff;
        border-color: #6b7280;
        border-width: 1px;
        border-radius: 0px;
        padding-top: 0.5rem;
        padding-right: 0.75rem;
        padding-bottom: 0.5rem;
        padding-left: 0.75rem;
        font-size: 1rem;
        line-height: 1.5rem;
        --tw-shadow: 0 0 #0000;
        }

        [type='text']:focus, [type='email']:focus, [type='url']:focus, [type='password']:focus, [type='number']:focus, [type='date']:focus, [type='datetime-local']:focus, [type='month']:focus, [type='search']:focus, [type='tel']:focus, [type='time']:focus, [type='week']:focus, [multiple]:focus, textarea:focus, select:focus {
        outline: 2px solid transparent;
        outline-offset: 2px;
        --tw-ring-inset: var(--tw-empty,/*!*/ /*!*/);
        --tw-ring-offset-width: 0px;
        --tw-ring-offset-color: #fff;
        --tw-ring-color: #2563eb;
        --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);
        --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(1px + var(--tw-ring-offset-width)) var(--tw-ring-color);
        box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow);
        border-color: #2563eb;
        }

        input::-moz-placeholder, textarea::-moz-placeholder {
        color: #6b7280;
        opacity: 1;
        }

        input::placeholder,textarea::placeholder {
        color: #6b7280;
        opacity: 1;
        }

        ::-webkit-datetime-edit-fields-wrapper {
        padding: 0;
        }

        ::-webkit-date-and-time-value {
        min-height: 1.5em;
        }

        ::-webkit-datetime-edit,::-webkit-datetime-edit-year-field,::-webkit-datetime-edit-month-field,::-webkit-datetime-edit-day-field,::-webkit-datetime-edit-hour-field,::-webkit-datetime-edit-minute-field,::-webkit-datetime-edit-second-field,::-webkit-datetime-edit-millisecond-field,::-webkit-datetime-edit-meridiem-field {
        padding-top: 0;
        padding-bottom: 0;
        }

        select {
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 20 20'%3e%3cpath stroke='%236b7280' stroke-linecap='round' stroke-linejoin='round' stroke-width='1.5' d='M6 8l4 4 4-4'/%3e%3c/svg%3e");
        background-position: right 0.5rem center;
        background-repeat: no-repeat;
        background-size: 1.5em 1.5em;
        padding-right: 2.5rem;
        -webkit-print-color-adjust: exact;
                print-color-adjust: exact;
        }

        [multiple] {
        background-image: initial;
        background-position: initial;
        background-repeat: unset;
        background-size: initial;
        padding-right: 0.75rem;
        -webkit-print-color-adjust: unset;
                print-color-adjust: unset;
        }

        [type='checkbox'],[type='radio'] {
        -webkit-appearance: none;
            -moz-appearance: none;
                appearance: none;
        padding: 0;
        -webkit-print-color-adjust: exact;
                print-color-adjust: exact;
        display: inline-block;
        vertical-align: middle;
        background-origin: border-box;
        -webkit-user-select: none;
            -moz-user-select: none;
                user-select: none;
        flex-shrink: 0;
        height: 1rem;
        width: 1rem;
        color: #2563eb;
        background-color: #fff;
        border-color: #6b7280;
        border-width: 1px;
        --tw-shadow: 0 0 #0000;
        }

        [type='checkbox'] {
        border-radius: 0px;
        }

        [type='radio'] {
        border-radius: 100%;
        }

        [type='checkbox']:focus,[type='radio']:focus {
        outline: 2px solid transparent;
        outline-offset: 2px;
        --tw-ring-inset: var(--tw-empty,/*!*/ /*!*/);
        --tw-ring-offset-width: 2px;
        --tw-ring-offset-color: #fff;
        --tw-ring-color: #2563eb;
        --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);
        --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(2px + var(--tw-ring-offset-width)) var(--tw-ring-color);
        box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow);
        }

        [type='checkbox']:checked,[type='radio']:checked {
        border-color: transparent;
        background-color: currentColor;
        background-size: 100% 100%;
        background-position: center;
        background-repeat: no-repeat;
        }

        [type='checkbox']:checked {
        background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 16 16' fill='white' xmlns='http://www.w3.org/2000/svg'%3e%3cpath d='M12.207 4.793a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-2-2a1 1 0 011.414-1.414L6.5 9.086l4.293-4.293a1 1 0 011.414 0z'/%3e%3c/svg%3e");
        }

        [type='radio']:checked {
        background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 16 16' fill='white' xmlns='http://www.w3.org/2000/svg'%3e%3ccircle cx='8' cy='8' r='3'/%3e%3c/svg%3e");
        }

        [type='checkbox']:checked:hover,[type='checkbox']:checked:focus,[type='radio']:checked:hover,[type='radio']:checked:focus {
        border-color: transparent;
        background-color: currentColor;
        }

        [type='checkbox']:indeterminate {
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 16 16'%3e%3cpath stroke='white' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M4 8h8'/%3e%3c/svg%3e");
        border-color: transparent;
        background-color: currentColor;
        background-size: 100% 100%;
        background-position: center;
        background-repeat: no-repeat;
        }

        [type='checkbox']:indeterminate:hover,[type='checkbox']:indeterminate:focus {
        border-color: transparent;
        background-color: currentColor;
        }

        [type='file'] {
        background: unset;
        border-color: inherit;
        border-width: 0;
        border-radius: 0;
        padding: 0;
        font-size: unset;
        line-height: inherit;
        }

        [type='file']:focus {
        outline: 1px solid ButtonText;
        outline: 1px auto -webkit-focus-ring-color;
        }

        *, ::before, ::after {
        --tw-border-spacing-x: 0;
        --tw-border-spacing-y: 0;
        --tw-translate-x: 0;
        --tw-translate-y: 0;
        --tw-rotate: 0;
        --tw-skew-x: 0;
        --tw-skew-y: 0;
        --tw-scale-x: 1;
        --tw-scale-y: 1;
        --tw-pan-x:  ;
        --tw-pan-y:  ;
        --tw-pinch-zoom:  ;
        --tw-scroll-snap-strictness: proximity;
        --tw-ordinal:  ;
        --tw-slashed-zero:  ;
        --tw-numeric-figure:  ;
        --tw-numeric-spacing:  ;
        --tw-numeric-fraction:  ;
        --tw-ring-inset:  ;
        --tw-ring-offset-width: 0px;
        --tw-ring-offset-color: #fff;
        --tw-ring-color: rgb(59 130 246 / 0.5);
        --tw-ring-offset-shadow: 0 0 #0000;
        --tw-ring-shadow: 0 0 #0000;
        --tw-shadow: 0 0 #0000;
        --tw-shadow-colored: 0 0 #0000;
        --tw-blur:  ;
        --tw-brightness:  ;
        --tw-contrast:  ;
        --tw-grayscale:  ;
        --tw-hue-rotate:  ;
        --tw-invert:  ;
        --tw-saturate:  ;
        --tw-sepia:  ;
        --tw-drop-shadow:  ;
        --tw-backdrop-blur:  ;
        --tw-backdrop-brightness:  ;
        --tw-backdrop-contrast:  ;
        --tw-backdrop-grayscale:  ;
        --tw-backdrop-hue-rotate:  ;
        --tw-backdrop-invert:  ;
        --tw-backdrop-opacity:  ;
        --tw-backdrop-saturate:  ;
        --tw-backdrop-sepia:  ;
        }

        ::backdrop {
        --tw-border-spacing-x: 0;
        --tw-border-spacing-y: 0;
        --tw-translate-x: 0;
        --tw-translate-y: 0;
        --tw-rotate: 0;
        --tw-skew-x: 0;
        --tw-skew-y: 0;
        --tw-scale-x: 1;
        --tw-scale-y: 1;
        --tw-pan-x:  ;
        --tw-pan-y:  ;
        --tw-pinch-zoom:  ;
        --tw-scroll-snap-strictness: proximity;
        --tw-ordinal:  ;
        --tw-slashed-zero:  ;
        --tw-numeric-figure:  ;
        --tw-numeric-spacing:  ;
        --tw-numeric-fraction:  ;
        --tw-ring-inset:  ;
        --tw-ring-offset-width: 0px;
        --tw-ring-offset-color: #fff;
        --tw-ring-color: rgb(59 130 246 / 0.5);
        --tw-ring-offset-shadow: 0 0 #0000;
        --tw-ring-shadow: 0 0 #0000;
        --tw-shadow: 0 0 #0000;
        --tw-shadow-colored: 0 0 #0000;
        --tw-blur:  ;
        --tw-brightness:  ;
        --tw-contrast:  ;
        --tw-grayscale:  ;
        --tw-hue-rotate:  ;
        --tw-invert:  ;
        --tw-saturate:  ;
        --tw-sepia:  ;
        --tw-drop-shadow:  ;
        --tw-backdrop-blur:  ;
        --tw-backdrop-brightness:  ;
        --tw-backdrop-contrast:  ;
        --tw-backdrop-grayscale:  ;
        --tw-backdrop-hue-rotate:  ;
        --tw-backdrop-invert:  ;
        --tw-backdrop-opacity:  ;
        --tw-backdrop-saturate:  ;
        --tw-backdrop-sepia:  ;
        }

        .container {
        width: 100%;
        }

        @media (min-width: 640px) {
        .container {
            max-width: 640px;
        }
        }

        @media (min-width: 768px) {
        .container {
            max-width: 768px;
        }
        }

        @media (min-width: 1024px) {
        .container {
            max-width: 1024px;
        }
        }

        @media (min-width: 1280px) {
        .container {
            max-width: 1280px;
        }
        }

        @media (min-width: 1536px) {
        .container {
            max-width: 1536px;
        }
        }

        .form-input,.form-textarea,.form-select,.form-multiselect {
        -webkit-appearance: none;
            -moz-appearance: none;
                appearance: none;
        background-color: #fff;
        border-color: #6b7280;
        border-width: 1px;
        border-radius: 0px;
        padding-top: 0.5rem;
        padding-right: 0.75rem;
        padding-bottom: 0.5rem;
        padding-left: 0.75rem;
        font-size: 1rem;
        line-height: 1.5rem;
        --tw-shadow: 0 0 #0000;
        }

        .form-input:focus, .form-textarea:focus, .form-select:focus, .form-multiselect:focus {
        outline: 2px solid transparent;
        outline-offset: 2px;
        --tw-ring-inset: var(--tw-empty,/*!*/ /*!*/);
        --tw-ring-offset-width: 0px;
        --tw-ring-offset-color: #fff;
        --tw-ring-color: #2563eb;
        --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);
        --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(1px + var(--tw-ring-offset-width)) var(--tw-ring-color);
        box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow);
        border-color: #2563eb;
        }

        .form-select {
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 20 20'%3e%3cpath stroke='%236b7280' stroke-linecap='round' stroke-linejoin='round' stroke-width='1.5' d='M6 8l4 4 4-4'/%3e%3c/svg%3e");
        background-position: right 0.5rem center;
        background-repeat: no-repeat;
        background-size: 1.5em 1.5em;
        padding-right: 2.5rem;
        -webkit-print-color-adjust: exact;
                print-color-adjust: exact;
        }

        .form-checkbox,.form-radio {
        -webkit-appearance: none;
            -moz-appearance: none;
                appearance: none;
        padding: 0;
        -webkit-print-color-adjust: exact;
                print-color-adjust: exact;
        display: inline-block;
        vertical-align: middle;
        background-origin: border-box;
        -webkit-user-select: none;
            -moz-user-select: none;
                user-select: none;
        flex-shrink: 0;
        height: 1rem;
        width: 1rem;
        color: #2563eb;
        background-color: #fff;
        border-color: #6b7280;
        border-width: 1px;
        --tw-shadow: 0 0 #0000;
        }

        .form-checkbox {
        border-radius: 0px;
        }

        .form-checkbox:focus,.form-radio:focus {
        outline: 2px solid transparent;
        outline-offset: 2px;
        --tw-ring-inset: var(--tw-empty,/*!*/ /*!*/);
        --tw-ring-offset-width: 2px;
        --tw-ring-offset-color: #fff;
        --tw-ring-color: #2563eb;
        --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);
        --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(2px + var(--tw-ring-offset-width)) var(--tw-ring-color);
        box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow);
        }

        .form-checkbox:checked,.form-radio:checked {
        border-color: transparent;
        background-color: currentColor;
        background-size: 100% 100%;
        background-position: center;
        background-repeat: no-repeat;
        }

        .form-checkbox:checked {
        background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 16 16' fill='white' xmlns='http://www.w3.org/2000/svg'%3e%3cpath d='M12.207 4.793a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-2-2a1 1 0 011.414-1.414L6.5 9.086l4.293-4.293a1 1 0 011.414 0z'/%3e%3c/svg%3e");
        }

        .form-checkbox:checked:hover,.form-checkbox:checked:focus,.form-radio:checked:hover,.form-radio:checked:focus {
        border-color: transparent;
        background-color: currentColor;
        }

        .form-checkbox:indeterminate {
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 16 16'%3e%3cpath stroke='white' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M4 8h8'/%3e%3c/svg%3e");
        border-color: transparent;
        background-color: currentColor;
        background-size: 100% 100%;
        background-position: center;
        background-repeat: no-repeat;
        }

        .form-checkbox:indeterminate:hover,.form-checkbox:indeterminate:focus {
        border-color: transparent;
        background-color: currentColor;
        }

        .prose {
        color: var(--tw-prose-body);
        max-width: 65ch;
        }

        .prose :where([class~="lead"]):not(:where([class~="not-prose"] *)) {
        color: var(--tw-prose-lead);
        font-size: 1.25em;
        line-height: 1.6;
        margin-top: 1.2em;
        margin-bottom: 1.2em;
        }

        .prose :where(a):not(:where([class~="not-prose"] *)) {
        color: var(--tw-prose-links);
        text-decoration: underline;
        font-weight: 500;
        }

        .prose :where(strong):not(:where([class~="not-prose"] *)) {
        color: var(--tw-prose-bold);
        font-weight: 600;
        }

        .prose :where(a strong):not(:where([class~="not-prose"] *)) {
        color: inherit;
        }

        .prose :where(blockquote strong):not(:where([class~="not-prose"] *)) {
        color: inherit;
        }

        .prose :where(thead th strong):not(:where([class~="not-prose"] *)) {
        color: inherit;
        }

        .prose :where(ol):not(:where([class~="not-prose"] *)) {
        list-style-type: decimal;
        margin-top: 1.25em;
        margin-bottom: 1.25em;
        padding-left: 1.625em;
        }

        .prose :where(ol[type="A"]):not(:where([class~="not-prose"] *)) {
        list-style-type: upper-alpha;
        }

        .prose :where(ol[type="a"]):not(:where([class~="not-prose"] *)) {
        list-style-type: lower-alpha;
        }

        .prose :where(ol[type="A" s]):not(:where([class~="not-prose"] *)) {
        list-style-type: upper-alpha;
        }

        .prose :where(ol[type="a" s]):not(:where([class~="not-prose"] *)) {
        list-style-type: lower-alpha;
        }

        .prose :where(ol[type="I"]):not(:where([class~="not-prose"] *)) {
        list-style-type: upper-roman;
        }

        .prose :where(ol[type="i"]):not(:where([class~="not-prose"] *)) {
        list-style-type: lower-roman;
        }

        .prose :where(ol[type="I" s]):not(:where([class~="not-prose"] *)) {
        list-style-type: upper-roman;
        }

        .prose :where(ol[type="i" s]):not(:where([class~="not-prose"] *)) {
        list-style-type: lower-roman;
        }

        .prose :where(ol[type="1"]):not(:where([class~="not-prose"] *)) {
        list-style-type: decimal;
        }

        .prose :where(ul):not(:where([class~="not-prose"] *)) {
        list-style-type: disc;
        margin-top: 1.25em;
        margin-bottom: 1.25em;
        padding-left: 1.625em;
        }

        .prose :where(ol > li):not(:where([class~="not-prose"] *))::marker {
        font-weight: 400;
        color: var(--tw-prose-counters);
        }

        .prose :where(ul > li):not(:where([class~="not-prose"] *))::marker {
        color: var(--tw-prose-bullets);
        }

        .prose :where(hr):not(:where([class~="not-prose"] *)) {
        border-color: var(--tw-prose-hr);
        border-top-width: 1px;
        margin-top: 3em;
        margin-bottom: 3em;
        }

        .prose :where(blockquote):not(:where([class~="not-prose"] *)) {
        font-weight: 500;
        font-style: italic;
        color: var(--tw-prose-quotes);
        border-left-width: 0.25rem;
        border-left-color: var(--tw-prose-quote-borders);
        quotes: "\201C""\201D""\2018""\2019";
        margin-top: 1.6em;
        margin-bottom: 1.6em;
        padding-left: 1em;
        }

        .prose :where(blockquote p:first-of-type):not(:where([class~="not-prose"] *))::before {
        content: open-quote;
        }

        .prose :where(blockquote p:last-of-type):not(:where([class~="not-prose"] *))::after {
        content: close-quote;
        }

        .prose :where(h1):not(:where([class~="not-prose"] *)) {
        color: var(--tw-prose-headings);
        font-weight: 800;
        font-size: 2.25em;
        margin-top: 0;
        margin-bottom: 0.8888889em;
        line-height: 1.1111111;
        }

        .prose :where(h1 strong):not(:where([class~="not-prose"] *)) {
        font-weight: 900;
        color: inherit;
        }

        .prose :where(h2):not(:where([class~="not-prose"] *)) {
        color: var(--tw-prose-headings);
        font-weight: 700;
        font-size: 1.5em;
        margin-top: 2em;
        margin-bottom: 1em;
        line-height: 1.3333333;
        }

        .prose :where(h2 strong):not(:where([class~="not-prose"] *)) {
        font-weight: 800;
        color: inherit;
        }

        .prose :where(h3):not(:where([class~="not-prose"] *)) {
        color: var(--tw-prose-headings);
        font-weight: 600;
        font-size: 1.25em;
        margin-top: 1.6em;
        margin-bottom: 0.6em;
        line-height: 1.6;
        }

        .prose :where(h3 strong):not(:where([class~="not-prose"] *)) {
        font-weight: 700;
        color: inherit;
        }

        .prose :where(h4):not(:where([class~="not-prose"] *)) {
        color: var(--tw-prose-headings);
        font-weight: 600;
        margin-top: 1.5em;
        margin-bottom: 0.5em;
        line-height: 1.5;
        }

        .prose :where(h4 strong):not(:where([class~="not-prose"] *)) {
        font-weight: 700;
        color: inherit;
        }

        .prose :where(img):not(:where([class~="not-prose"] *)) {
        margin-top: 2em;
        margin-bottom: 2em;
        }

        .prose :where(figure > *):not(:where([class~="not-prose"] *)) {
        margin-top: 0;
        margin-bottom: 0;
        }

        .prose :where(figcaption):not(:where([class~="not-prose"] *)) {
        color: var(--tw-prose-captions);
        font-size: 0.875em;
        line-height: 1.4285714;
        margin-top: 0.8571429em;
        }

        .prose :where(code):not(:where([class~="not-prose"] *)) {
        color: var(--tw-prose-code);
        font-weight: 600;
        font-size: 0.875em;
        }

        .prose :where(code):not(:where([class~="not-prose"] *))::before {
        content: "`";
        }

        .prose :where(code):not(:where([class~="not-prose"] *))::after {
        content: "`";
        }

        .prose :where(a code):not(:where([class~="not-prose"] *)) {
        color: inherit;
        }

        .prose :where(h1 code):not(:where([class~="not-prose"] *)) {
        color: inherit;
        }

        .prose :where(h2 code):not(:where([class~="not-prose"] *)) {
        color: inherit;
        font-size: 0.875em;
        }

        .prose :where(h3 code):not(:where([class~="not-prose"] *)) {
        color: inherit;
        font-size: 0.9em;
        }

        .prose :where(h4 code):not(:where([class~="not-prose"] *)) {
        color: inherit;
        }

        .prose :where(blockquote code):not(:where([class~="not-prose"] *)) {
        color: inherit;
        }

        .prose :where(thead th code):not(:where([class~="not-prose"] *)) {
        color: inherit;
        }

        .prose :where(pre):not(:where([class~="not-prose"] *)) {
        color: var(--tw-prose-pre-code);
        background-color: var(--tw-prose-pre-bg);
        overflow-x: auto;
        font-weight: 400;
        font-size: 0.875em;
        line-height: 1.7142857;
        margin-top: 1.7142857em;
        margin-bottom: 1.7142857em;
        border-radius: 0.375rem;
        padding-top: 0.8571429em;
        padding-right: 1.1428571em;
        padding-bottom: 0.8571429em;
        padding-left: 1.1428571em;
        }

        .prose :where(pre code):not(:where([class~="not-prose"] *)) {
        background-color: transparent;
        border-width: 0;
        border-radius: 0;
        padding: 0;
        font-weight: inherit;
        color: inherit;
        font-size: inherit;
        font-family: inherit;
        line-height: inherit;
        }

        .prose :where(pre code):not(:where([class~="not-prose"] *))::before {
        content: none;
        }

        .prose :where(pre code):not(:where([class~="not-prose"] *))::after {
        content: none;
        }

        .prose :where(table):not(:where([class~="not-prose"] *)) {
        width: 100%;
        table-layout: auto;
        text-align: left;
        margin-top: 2em;
        margin-bottom: 2em;
        font-size: 0.875em;
        line-height: 1.7142857;
        }

        .prose :where(thead):not(:where([class~="not-prose"] *)) {
        border-bottom-width: 1px;
        border-bottom-color: var(--tw-prose-th-borders);
        }

        .prose :where(thead th):not(:where([class~="not-prose"] *)) {
        color: var(--tw-prose-headings);
        font-weight: 600;
        vertical-align: bottom;
        padding-right: 0.5714286em;
        padding-bottom: 0.5714286em;
        padding-left: 0.5714286em;
        }

        .prose :where(tbody tr):not(:where([class~="not-prose"] *)) {
        border-bottom-width: 1px;
        border-bottom-color: var(--tw-prose-td-borders);
        }

        .prose :where(tbody tr:last-child):not(:where([class~="not-prose"] *)) {
        border-bottom-width: 0;
        }

        .prose :where(tbody td):not(:where([class~="not-prose"] *)) {
        vertical-align: baseline;
        }

        .prose :where(tfoot):not(:where([class~="not-prose"] *)) {
        border-top-width: 1px;
        border-top-color: var(--tw-prose-th-borders);
        }

        .prose :where(tfoot td):not(:where([class~="not-prose"] *)) {
        vertical-align: top;
        }

        .prose {
        --tw-prose-body: #374151;
        --tw-prose-headings: #111827;
        --tw-prose-lead: #4b5563;
        --tw-prose-links: #111827;
        --tw-prose-bold: #111827;
        --tw-prose-counters: #6b7280;
        --tw-prose-bullets: #d1d5db;
        --tw-prose-hr: #e5e7eb;
        --tw-prose-quotes: #111827;
        --tw-prose-quote-borders: #e5e7eb;
        --tw-prose-captions: #6b7280;
        --tw-prose-code: #111827;
        --tw-prose-pre-code: #e5e7eb;
        --tw-prose-pre-bg: #1f2937;
        --tw-prose-th-borders: #d1d5db;
        --tw-prose-td-borders: #e5e7eb;
        --tw-prose-invert-body: #d1d5db;
        --tw-prose-invert-headings: #fff;
        --tw-prose-invert-lead: #9ca3af;
        --tw-prose-invert-links: #fff;
        --tw-prose-invert-bold: #fff;
        --tw-prose-invert-counters: #9ca3af;
        --tw-prose-invert-bullets: #4b5563;
        --tw-prose-invert-hr: #374151;
        --tw-prose-invert-quotes: #f3f4f6;
        --tw-prose-invert-quote-borders: #374151;
        --tw-prose-invert-captions: #9ca3af;
        --tw-prose-invert-code: #fff;
        --tw-prose-invert-pre-code: #d1d5db;
        --tw-prose-invert-pre-bg: rgb(0 0 0 / 50%);
        --tw-prose-invert-th-borders: #4b5563;
        --tw-prose-invert-td-borders: #374151;
        font-size: 1rem;
        line-height: 1.75;
        }

        .prose :where(p):not(:where([class~="not-prose"] *)) {
        margin-top: 1.25em;
        margin-bottom: 1.25em;
        }

        .prose :where(video):not(:where([class~="not-prose"] *)) {
        margin-top: 2em;
        margin-bottom: 2em;
        }

        .prose :where(figure):not(:where([class~="not-prose"] *)) {
        margin-top: 2em;
        margin-bottom: 2em;
        }

        .prose :where(li):not(:where([class~="not-prose"] *)) {
        margin-top: 0.5em;
        margin-bottom: 0.5em;
        }

        .prose :where(ol > li):not(:where([class~="not-prose"] *)) {
        padding-left: 0.375em;
        }

        .prose :where(ul > li):not(:where([class~="not-prose"] *)) {
        padding-left: 0.375em;
        }

        .prose :where(.prose > ul > li p):not(:where([class~="not-prose"] *)) {
        margin-top: 0.75em;
        margin-bottom: 0.75em;
        }

        .prose :where(.prose > ul > li > *:first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 1.25em;
        }

        .prose :where(.prose > ul > li > *:last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 1.25em;
        }

        .prose :where(.prose > ol > li > *:first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 1.25em;
        }

        .prose :where(.prose > ol > li > *:last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 1.25em;
        }

        .prose :where(ul ul, ul ol, ol ul, ol ol):not(:where([class~="not-prose"] *)) {
        margin-top: 0.75em;
        margin-bottom: 0.75em;
        }

        .prose :where(hr + *):not(:where([class~="not-prose"] *)) {
        margin-top: 0;
        }

        .prose :where(h2 + *):not(:where([class~="not-prose"] *)) {
        margin-top: 0;
        }

        .prose :where(h3 + *):not(:where([class~="not-prose"] *)) {
        margin-top: 0;
        }

        .prose :where(h4 + *):not(:where([class~="not-prose"] *)) {
        margin-top: 0;
        }

        .prose :where(thead th:first-child):not(:where([class~="not-prose"] *)) {
        padding-left: 0;
        }

        .prose :where(thead th:last-child):not(:where([class~="not-prose"] *)) {
        padding-right: 0;
        }

        .prose :where(tbody td, tfoot td):not(:where([class~="not-prose"] *)) {
        padding-top: 0.5714286em;
        padding-right: 0.5714286em;
        padding-bottom: 0.5714286em;
        padding-left: 0.5714286em;
        }

        .prose :where(tbody td:first-child, tfoot td:first-child):not(:where([class~="not-prose"] *)) {
        padding-left: 0;
        }

        .prose :where(tbody td:last-child, tfoot td:last-child):not(:where([class~="not-prose"] *)) {
        padding-right: 0;
        }

        .prose :where(.prose > :first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 0;
        }

        .prose :where(.prose > :last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 0;
        }

        .prose-sm :where(.prose > ul > li p):not(:where([class~="not-prose"] *)) {
        margin-top: 0.5714286em;
        margin-bottom: 0.5714286em;
        }

        .prose-sm :where(.prose > ul > li > *:first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 1.1428571em;
        }

        .prose-sm :where(.prose > ul > li > *:last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 1.1428571em;
        }

        .prose-sm :where(.prose > ol > li > *:first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 1.1428571em;
        }

        .prose-sm :where(.prose > ol > li > *:last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 1.1428571em;
        }

        .prose-sm :where(.prose > :first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 0;
        }

        .prose-sm :where(.prose > :last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 0;
        }

        .prose-base :where(.prose > ul > li p):not(:where([class~="not-prose"] *)) {
        margin-top: 0.75em;
        margin-bottom: 0.75em;
        }

        .prose-base :where(.prose > ul > li > *:first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 1.25em;
        }

        .prose-base :where(.prose > ul > li > *:last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 1.25em;
        }

        .prose-base :where(.prose > ol > li > *:first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 1.25em;
        }

        .prose-base :where(.prose > ol > li > *:last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 1.25em;
        }

        .prose-base :where(.prose > :first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 0;
        }

        .prose-base :where(.prose > :last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 0;
        }

        .prose-lg :where(.prose > ul > li p):not(:where([class~="not-prose"] *)) {
        margin-top: 0.8888889em;
        margin-bottom: 0.8888889em;
        }

        .prose-lg :where(.prose > ul > li > *:first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 1.3333333em;
        }

        .prose-lg :where(.prose > ul > li > *:last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 1.3333333em;
        }

        .prose-lg :where(.prose > ol > li > *:first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 1.3333333em;
        }

        .prose-lg :where(.prose > ol > li > *:last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 1.3333333em;
        }

        .prose-lg :where(.prose > :first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 0;
        }

        .prose-lg :where(.prose > :last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 0;
        }

        .prose-xl :where(.prose > ul > li p):not(:where([class~="not-prose"] *)) {
        margin-top: 0.8em;
        margin-bottom: 0.8em;
        }

        .prose-xl :where(.prose > ul > li > *:first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 1.2em;
        }

        .prose-xl :where(.prose > ul > li > *:last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 1.2em;
        }

        .prose-xl :where(.prose > ol > li > *:first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 1.2em;
        }

        .prose-xl :where(.prose > ol > li > *:last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 1.2em;
        }

        .prose-xl :where(.prose > :first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 0;
        }

        .prose-xl :where(.prose > :last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 0;
        }

        .prose-2xl :where(.prose > ul > li p):not(:where([class~="not-prose"] *)) {
        margin-top: 0.8333333em;
        margin-bottom: 0.8333333em;
        }

        .prose-2xl :where(.prose > ul > li > *:first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 1.3333333em;
        }

        .prose-2xl :where(.prose > ul > li > *:last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 1.3333333em;
        }

        .prose-2xl :where(.prose > ol > li > *:first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 1.3333333em;
        }

        .prose-2xl :where(.prose > ol > li > *:last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 1.3333333em;
        }

        .prose-2xl :where(.prose > :first-child):not(:where([class~="not-prose"] *)) {
        margin-top: 0;
        }

        .prose-2xl :where(.prose > :last-child):not(:where([class~="not-prose"] *)) {
        margin-bottom: 0;
        }

        .sr-only {
        position: absolute;
        width: 1px;
        height: 1px;
        padding: 0;
        margin: -1px;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        white-space: nowrap;
        border-width: 0;
        }

        .pointer-events-none {
        pointer-events: none;
        }

        .visible {
        visibility: visible;
        }

        .invisible {
        visibility: hidden;
        }

        .static {
        position: static;
        }

        .fixed {
        position: fixed;
        }

        .absolute {
        position: absolute;
        }

        .relative {
        position: relative;
        }

        .sticky {
        position: sticky;
        }

        .inset-0 {
        top: 0px;
        right: 0px;
        bottom: 0px;
        left: 0px;
        }

        .inset-y-0 {
        top: 0px;
        bottom: 0px;
        }

        .left-0 {
        left: 0px;
        }

        .right-0 {
        right: 0px;
        }

        .top-\[3\.8125rem\] {
        top: 3.8125rem;
        }

        .right-auto {
        right: auto;
        }

        .top-0 {
        top: 0px;
        }

        .right-10 {
        right: 2.5rem;
        }

        .top-20 {
        top: 5rem;
        }

        .top-1\/2 {
        top: 50%;
        }

        .left-1\/2 {
        left: 50%;
        }

        .bottom-1\/2 {
        bottom: 50%;
        }

        .right-5 {
        right: 1.25rem;
        }

        .right-1 {
        right: 0.25rem;
        }

        .top-1 {
        top: 0.25rem;
        }

        .left-1 {
        left: 0.25rem;
        }

        .bottom-0 {
        bottom: 0px;
        }

        .top-\[10px\] {
        top: 10px;
        }

        .left-\[100px\] {
        left: 100px;
        }

        .right-1\/2 {
        right: 50%;
        }

        .top-6 {
        top: 1.5rem;
        }

        .right-8 {
        right: 2rem;
        }

        .z-0 {
        z-index: 0;
        }

        .z-50 {
        z-index: 50;
        }

        .z-10 {
        z-index: 10;
        }

        .z-20 {
        z-index: 20;
        }

        .z-40 {
        z-index: 40;
        }

        .-z-10 {
        z-index: -10;
        }

        .z-\[9999\] {
        z-index: 9999;
        }

        .z-\[100\] {
        z-index: 100;
        }

        .z-30 {
        z-index: 30;
        }

        .z-\[99999\] {
        z-index: 99999;
        }

        .order-first {
        order: -9999;
        }

        .order-last {
        order: 9999;
        }

        .order-1 {
        order: 1;
        }

        .col-span-6 {
        grid-column: span 6 / span 6;
        }

        .col-span-4 {
        grid-column: span 4 / span 4;
        }

        .col-span-2 {
        grid-column: span 2 / span 2;
        }

        .col-span-5 {
        grid-column: span 5 / span 5;
        }

        .col-span-3 {
        grid-column: span 3 / span 3;
        }

        .col-span-10 {
        grid-column: span 10 / span 10;
        }

        .col-span-11 {
        grid-column: span 11 / span 11;
        }

        .col-span-1 {
        grid-column: span 1 / span 1;
        }

        .col-span-9 {
        grid-column: span 9 / span 9;
        }

        .col-auto {
        grid-column: auto;
        }

        .col-span-8 {
        grid-column: span 8 / span 8;
        }

        .col-span-12 {
        grid-column: span 12 / span 12;
        }

        .col-start-3 {
        grid-column-start: 3;
        }

        .col-start-1 {
        grid-column-start: 1;
        }

        .col-start-6 {
        grid-column-start: 6;
        }

        .col-start-4 {
        grid-column-start: 4;
        }

        .col-start-5 {
        grid-column-start: 5;
        }

        .col-start-2 {
        grid-column-start: 2;
        }

        .col-end-11 {
        grid-column-end: 11;
        }

        .col-end-3 {
        grid-column-end: 3;
        }

        .col-end-8 {
        grid-column-end: 8;
        }

        .col-end-2 {
        grid-column-end: 2;
        }

        .col-end-7 {
        grid-column-end: 7;
        }

        .col-end-1 {
        grid-column-end: 1;
        }

        .row-span-2 {
        grid-row: span 2 / span 2;
        }

        .row-span-6 {
        grid-row: span 6 / span 6;
        }

        .row-span-3 {
        grid-row: span 3 / span 3;
        }

        .row-span-1 {
        grid-row: span 1 / span 1;
        }

        .row-start-2 {
        grid-row-start: 2;
        }

        .row-end-5 {
        grid-row-end: 5;
        }

        .float-right {
        float: right;
        }

        .float-left {
        float: left;
        }

        .float-none {
        float: none;
        }

        .clear-both {
        clear: both;
        }

        .m-5 {
        margin: 1.25rem;
        }

        .m-2 {
        margin: 0.5rem;
        }

        .m-auto {
        margin: auto;
        }

        .m-0 {
        margin: 0px;
        }

        .\!m-0 {
        margin: 0px !important;
        }

        .m-3 {
        margin: 0.75rem;
        }

        .m-8 {
        margin: 2rem;
        }

        .m-1 {
        margin: 0.25rem;
        }

        .m-4 {
        margin: 1rem;
        }

        .m-6 {
        margin: 1.5rem;
        }

        .m-7 {
        margin: 1.75rem;
        }

        .m-9 {
        margin: 2.25rem;
        }

        .m-10 {
        margin: 2.5rem;
        }

        .m-11 {
        margin: 2.75rem;
        }

        .m-12 {
        margin: 3rem;
        }

        .m-14 {
        margin: 3.5rem;
        }

        .m-16 {
        margin: 4rem;
        }

        .m-20 {
        margin: 5rem;
        }

        .m-24 {
        margin: 6rem;
        }

        .m-28 {
        margin: 7rem;
        }

        .m-32 {
        margin: 8rem;
        }

        .m-36 {
        margin: 9rem;
        }

        .m-48 {
        margin: 12rem;
        }

        .m-40 {
        margin: 10rem;
        }

        .m-2\.5 {
        margin: 0.625rem;
        }

        .mx-auto {
        margin-left: auto;
        margin-right: auto;
        }

        .my-auto {
        margin-top: auto;
        margin-bottom: auto;
        }

        .mx-3 {
        margin-left: 0.75rem;
        margin-right: 0.75rem;
        }

        .my-5 {
        margin-top: 1.25rem;
        margin-bottom: 1.25rem;
        }

        .my-2 {
        margin-top: 0.5rem;
        margin-bottom: 0.5rem;
        }

        .mx-2 {
        margin-left: 0.5rem;
        margin-right: 0.5rem;
        }

        .my-4 {
        margin-top: 1rem;
        margin-bottom: 1rem;
        }

        .-mx-4 {
        margin-left: -1rem;
        margin-right: -1rem;
        }

        .my-8 {
        margin-top: 2rem;
        margin-bottom: 2rem;
        }

        .my-3 {
        margin-top: 0.75rem;
        margin-bottom: 0.75rem;
        }

        .my-20 {
        margin-top: 5rem;
        margin-bottom: 5rem;
        }

        .my-10 {
        margin-top: 2.5rem;
        margin-bottom: 2.5rem;
        }

        .mx-5 {
        margin-left: 1.25rem;
        margin-right: 1.25rem;
        }

        .my-1 {
        margin-top: 0.25rem;
        margin-bottom: 0.25rem;
        }

        .mx-1 {
        margin-left: 0.25rem;
        margin-right: 0.25rem;
        }

        .-mx-2 {
        margin-left: -0.5rem;
        margin-right: -0.5rem;
        }

        .my-7 {
        margin-top: 1.75rem;
        margin-bottom: 1.75rem;
        }

        .my-2\.5 {
        margin-top: 0.625rem;
        margin-bottom: 0.625rem;
        }

        .mx-2\.5 {
        margin-left: 0.625rem;
        margin-right: 0.625rem;
        }

        .mx-10 {
        margin-left: 2.5rem;
        margin-right: 2.5rem;
        }

        .my-0 {
        margin-top: 0px;
        margin-bottom: 0px;
        }

        .mx-px {
        margin-left: 1px;
        margin-right: 1px;
        }

        .mx-4 {
        margin-left: 1rem;
        margin-right: 1rem;
        }

        .mx-0 {
        margin-left: 0px;
        margin-right: 0px;
        }

        .my-6 {
        margin-top: 1.5rem;
        margin-bottom: 1.5rem;
        }

        .mx-6 {
        margin-left: 1.5rem;
        margin-right: 1.5rem;
        }

        .mx-7 {
        margin-left: 1.75rem;
        margin-right: 1.75rem;
        }

        .mx-8 {
        margin-left: 2rem;
        margin-right: 2rem;
        }

        .my-9 {
        margin-top: 2.25rem;
        margin-bottom: 2.25rem;
        }

        .mx-9 {
        margin-left: 2.25rem;
        margin-right: 2.25rem;
        }

        .my-11 {
        margin-top: 2.75rem;
        margin-bottom: 2.75rem;
        }

        .mx-11 {
        margin-left: 2.75rem;
        margin-right: 2.75rem;
        }

        .my-12 {
        margin-top: 3rem;
        margin-bottom: 3rem;
        }

        .mx-12 {
        margin-left: 3rem;
        margin-right: 3rem;
        }

        .my-14 {
        margin-top: 3.5rem;
        margin-bottom: 3.5rem;
        }

        .mx-14 {
        margin-left: 3.5rem;
        margin-right: 3.5rem;
        }

        .my-16 {
        margin-top: 4rem;
        margin-bottom: 4rem;
        }

        .mx-16 {
        margin-left: 4rem;
        margin-right: 4rem;
        }

        .mx-20 {
        margin-left: 5rem;
        margin-right: 5rem;
        }

        .my-24 {
        margin-top: 6rem;
        margin-bottom: 6rem;
        }

        .mx-24 {
        margin-left: 6rem;
        margin-right: 6rem;
        }

        .my-28 {
        margin-top: 7rem;
        margin-bottom: 7rem;
        }

        .mx-28 {
        margin-left: 7rem;
        margin-right: 7rem;
        }

        .my-32 {
        margin-top: 8rem;
        margin-bottom: 8rem;
        }

        .mx-32 {
        margin-left: 8rem;
        margin-right: 8rem;
        }

        .my-36 {
        margin-top: 9rem;
        margin-bottom: 9rem;
        }

        .mx-36 {
        margin-left: 9rem;
        margin-right: 9rem;
        }

        .my-48 {
        margin-top: 12rem;
        margin-bottom: 12rem;
        }

        .mx-48 {
        margin-left: 12rem;
        margin-right: 12rem;
        }

        .my-40 {
        margin-top: 10rem;
        margin-bottom: 10rem;
        }

        .mx-40 {
        margin-left: 10rem;
        margin-right: 10rem;
        }

        .my-px {
        margin-top: 1px;
        margin-bottom: 1px;
        }

        .ml-3 {
        margin-left: 0.75rem;
        }

        .-ml-px {
        margin-left: -1px;
        }

        .mt-5 {
        margin-top: 1.25rem;
        }

        .mt-6 {
        margin-top: 1.5rem;
        }

        .-mr-1 {
        margin-right: -0.25rem;
        }

        .mt-3 {
        margin-top: 0.75rem;
        }

        .mt-2 {
        margin-top: 0.5rem;
        }

        .mt-4 {
        margin-top: 1rem;
        }

        .mt-1 {
        margin-top: 0.25rem;
        }

        .mb-6 {
        margin-bottom: 1.5rem;
        }

        .mr-2 {
        margin-right: 0.5rem;
        }

        .mt-8 {
        margin-top: 2rem;
        }

        .ml-4 {
        margin-left: 1rem;
        }

        .ml-12 {
        margin-left: 3rem;
        }

        .ml-1 {
        margin-left: 0.25rem;
        }

        .ml-2 {
        margin-left: 0.5rem;
        }

        .-mr-0\.5 {
        margin-right: -0.125rem;
        }

        .-mr-0 {
        margin-right: -0px;
        }

        .-mr-2 {
        margin-right: -0.5rem;
        }

        .mr-3 {
        margin-right: 0.75rem;
        }

        .mt-10 {
        margin-top: 2.5rem;
        }

        .ml-6 {
        margin-left: 1.5rem;
        }

        .mb-4 {
        margin-bottom: 1rem;
        }

        .mb-5 {
        margin-bottom: 1.25rem;
        }

        .mt-36 {
        margin-top: 9rem;
        }

        .mt-\[0\.2rem\] {
        margin-top: 0.2rem;
        }

        .mr-1 {
        margin-right: 0.25rem;
        }

        .mt-\[2px\] {
        margin-top: 2px;
        }

        .mb-2 {
        margin-bottom: 0.5rem;
        }

        .mb-3 {
        margin-bottom: 0.75rem;
        }

        .-mt-px {
        margin-top: -1px;
        }

        .\!-ml-\[1rem\] {
        margin-left: -1rem !important;
        }

        .\!ml-\[17px\] {
        margin-left: 17px !important;
        }

        .\!ml-\[4px\] {
        margin-left: 4px !important;
        }

        .mr-5 {
        margin-right: 1.25rem;
        }

        .-ml-\[3rem\] {
        margin-left: -3rem;
        }

        .mt-\[0\.6rem\] {
        margin-top: 0.6rem;
        }

        .ml-8 {
        margin-left: 2rem;
        }

        .mr-4 {
        margin-right: 1rem;
        }

        .mt-px {
        margin-top: 1px;
        }

        .ml-5 {
        margin-left: 1.25rem;
        }

        .mt-40 {
        margin-top: 10rem;
        }

        .mb-10 {
        margin-bottom: 2.5rem;
        }

        .-mb-2 {
        margin-bottom: -0.5rem;
        }

        .ml-0 {
        margin-left: 0px;
        }

        .-mt-2 {
        margin-top: -0.5rem;
        }

        .mt-2\.5 {
        margin-top: 0.625rem;
        }

        .mb-2\.5 {
        margin-bottom: 0.625rem;
        }

        .-mb-px {
        margin-bottom: -1px;
        }

        .ml-2\.5 {
        margin-left: 0.625rem;
        }

        .mt-16 {
        margin-top: 4rem;
        }

        .-ml-\[1rem\] {
        margin-left: -1rem;
        }

        .mb-1 {
        margin-bottom: 0.25rem;
        }

        .mt-0 {
        margin-top: 0px;
        }

        .mb-0 {
        margin-bottom: 0px;
        }

        .ml-\[7px\] {
        margin-left: 7px;
        }

        .mb-px {
        margin-bottom: 1px;
        }

        .-mt-\[1\.5px\] {
        margin-top: -1.5px;
        }

        .-mt-16 {
        margin-top: -4rem;
        }

        .mb-8 {
        margin-bottom: 2rem;
        }

        .mt-auto {
        margin-top: auto;
        }

        .mt-\[20rem\] {
        margin-top: 20rem;
        }

        .ml-auto {
        margin-left: auto;
        }

        .mt-80 {
        margin-top: 20rem;
        }

        .mr-auto {
        margin-right: auto;
        }

        .ml-11 {
        margin-left: 2.75rem;
        }

        .mr-10 {
        margin-right: 2.5rem;
        }

        .mt-7 {
        margin-top: 1.75rem;
        }

        .mb-12 {
        margin-bottom: 3rem;
        }

        .mb-7 {
        margin-bottom: 1.75rem;
        }

        .mt-20 {
        margin-top: 5rem;
        }

        .ml-9 {
        margin-left: 2.25rem;
        }

        .-mt-5 {
        margin-top: -1.25rem;
        }

        .-mt-12 {
        margin-top: -3rem;
        }

        .-mt-32 {
        margin-top: -8rem;
        }

        .-mt-10 {
        margin-top: -2.5rem;
        }

        .mr-12 {
        margin-right: 3rem;
        }

        .-mt-\[2px\] {
        margin-top: -2px;
        }

        .ml-\[14px\] {
        margin-left: 14px;
        }

        .mt-\[1rem\] {
        margin-top: 1rem;
        }

        .-ml-\[30px\] {
        margin-left: -30px;
        }

        .mt-\[5px\] {
        margin-top: 5px;
        }

        .mt-\[6px\] {
        margin-top: 6px;
        }

        .mt-9 {
        margin-top: 2.25rem;
        }

        .ml-20 {
        margin-left: 5rem;
        }

        .mt-3\.5 {
        margin-top: 0.875rem;
        }

        .mr-2\.5 {
        margin-right: 0.625rem;
        }

        .-mt-\[4px\] {
        margin-top: -4px;
        }

        .-mt-1 {
        margin-top: -0.25rem;
        }

        .mt-64 {
        margin-top: 16rem;
        }

        .block {
        display: block;
        }

        .inline-block {
        display: inline-block;
        }

        .inline {
        display: inline;
        }

        .flex {
        display: flex;
        }

        .inline-flex {
        display: inline-flex;
        }

        .table {
        display: table;
        }

        .grid {
        display: grid;
        }

        .contents {
        display: contents;
        }

        .hidden {
        display: none;
        }

        .h-5 {
        height: 1.25rem;
        }

        .h-16 {
        height: 4rem;
        }

        .h-12 {
        height: 3rem;
        }

        .h-6 {
        height: 1.5rem;
        }

        .h-8 {
        height: 2rem;
        }

        .h-4 {
        height: 1rem;
        }

        .h-9 {
        height: 2.25rem;
        }

        .h-10 {
        height: 2.5rem;
        }

        .h-20 {
        height: 5rem;
        }

        .h-\[336px\] {
        height: 336px;
        }

        .h-full {
        height: 100%;
        }

        .h-\[25px\] {
        height: 25px;
        }

        .h-56 {
        height: 14rem;
        }

        .h-\[15px\] {
        height: 15px;
        }

        .h-14 {
        height: 3.5rem;
        }

        .h-\[16px\] {
        height: 16px;
        }

        .h-\[168px\] {
        height: 168px;
        }

        .h-\[17px\] {
        height: 17px;
        }

        .h-\[154px\] {
        height: 154px;
        }

        .h-\[100px\] {
        height: 100px;
        }

        .h-\[39\.5px\] {
        height: 39.5px;
        }

        .h-px {
        height: 1px;
        }

        .h-\[140px\] {
        height: 140px;
        }

        .h-3 {
        height: 0.75rem;
        }

        .h-96 {
        height: 24rem;
        }

        .h-\[192px\] {
        height: 192px;
        }

        .h-\[20px\] {
        height: 20px;
        }

        .h-28 {
        height: 7rem;
        }

        .h-fit {
        height: -moz-fit-content;
        height: fit-content;
        }

        .h-\[30px\] {
        height: 30px;
        }

        .h-24 {
        height: 6rem;
        }

        .h-\[201px\] {
        height: 201px;
        }

        .h-\[611px\] {
        height: 611px;
        }

        .h-\[316px\] {
        height: 316px;
        }

        .h-\[75px\] {
        height: 75px;
        }

        .h-\[66px\] {
        height: 66px;
        }

        .h-\[70\.59px\] {
        height: 70.59px;
        }

        .h-\[128px\] {
        height: 128px;
        }

        .h-\[505px\] {
        height: 505px;
        }

        .h-\[91px\] {
        height: 91px;
        }

        .h-\[200px\] {
        height: 200px;
        }

        .h-auto {
        height: auto;
        }

        .h-\[24px\] {
        height: 24px;
        }

        .h-\[321px\] {
        height: 321px;
        }

        .h-\[180px\] {
        height: 180px;
        }

        .h-\[70px\] {
        height: 70px;
        }

        .h-\[12px\] {
        height: 12px;
        }

        .h-\[50px\] {
        height: 50px;
        }

        .h-\[250px\] {
        height: 250px;
        }

        .h-7 {
        height: 1.75rem;
        }

        .h-\[150px\] {
        height: 150px;
        }

        .h-\[280px\] {
        height: 280px;
        }

        .h-\[48px\] {
        height: 48px;
        }

        .h-\[18px\] {
        height: 18px;
        }

        .h-screen {
        height: 100vh;
        }

        .h-0 {
        height: 0px;
        }

        .h-\[29px\] {
        height: 29px;
        }

        .h-\[450px\] {
        height: 450px;
        }

        .h-48 {
        height: 12rem;
        }

        .h-\[156px\] {
        height: 156px;
        }

        .h-\[240px\] {
        height: 240px;
        }

        .h-2\/4 {
        height: 50%;
        }

        .h-32 {
        height: 8rem;
        }

        .h-64 {
        height: 16rem;
        }

        .h-\[1080px\] {
        height: 1080px;
        }

        .h-\[100\] {
        height: 100;
        }

        .h-\[411px\] {
        height: 411px;
        }

        .h-\[190px\] {
        height: 190px;
        }

        .h-\[17\.5px\] {
        height: 17.5px;
        }

        .h-\[1rem\] {
        height: 1rem;
        }

        .h-\[14px\] {
        height: 14px;
        }

        .h-\[380px\] {
        height: 380px;
        }

        .h-\[300px\] {
        height: 300px;
        }

        .max-h-\[426px\] {
        max-height: 426px;
        }

        .max-h-full {
        max-height: 100%;
        }

        .max-h-\[700\] {
        max-height: 700;
        }

        .max-h-\[500px\] {
        max-height: 500px;
        }

        .max-h-\[317px\] {
        max-height: 317px;
        }

        .max-h-\[400px\] {
        max-height: 400px;
        }

        .max-h-\[90px\] {
        max-height: 90px;
        }

        .max-h-96 {
        max-height: 24rem;
        }

        .max-h-screen {
        max-height: 100vh;
        }

        .max-h-\[600px\] {
        max-height: 600px;
        }

        .max-h-80 {
        max-height: 20rem;
        }

        .max-h-\[215px\] {
        max-height: 215px;
        }

        .max-h-\[95px\] {
        max-height: 95px;
        }

        .min-h-screen {
        min-height: 100vh;
        }

        .min-h-\[1024px\] {
        min-height: 1024px;
        }

        .min-h-full {
        min-height: 100%;
        }

        .min-h-fit {
        min-height: -moz-fit-content;
        min-height: fit-content;
        }

        .w-5 {
        width: 1.25rem;
        }

        .w-16 {
        width: 4rem;
        }

        .w-full {
        width: 100%;
        }

        .w-0 {
        width: 0px;
        }

        .w-12 {
        width: 3rem;
        }

        .w-6 {
        width: 1.5rem;
        }

        .w-3\/4 {
        width: 75%;
        }

        .w-48 {
        width: 12rem;
        }

        .w-auto {
        width: auto;
        }

        .w-8 {
        width: 2rem;
        }

        .w-4 {
        width: 1rem;
        }

        .w-60 {
        width: 15rem;
        }

        .w-10 {
        width: 2.5rem;
        }

        .w-1\/2 {
        width: 50%;
        }

        .w-20 {
        width: 5rem;
        }

        .w-\[50px\] {
        width: 50px;
        }

        .w-\[25px\] {
        width: 25px;
        }

        .w-\[15px\] {
        width: 15px;
        }

        .w-\[256px\] {
        width: 256px;
        }

        .w-14 {
        width: 3.5rem;
        }

        .w-\[16px\] {
        width: 16px;
        }

        .w-96 {
        width: 24rem;
        }

        .w-1\/4 {
        width: 25%;
        }

        .w-\[154px\] {
        width: 154px;
        }

        .w-\[100px\] {
        width: 100px;
        }

        .w-\[11rem\] {
        width: 11rem;
        }

        .w-\[140px\] {
        width: 140px;
        }

        .w-3 {
        width: 0.75rem;
        }

        .w-\[370px\] {
        width: 370px;
        }

        .w-\[325px\] {
        width: 325px;
        }

        .w-\[641px\] {
        width: 641px;
        }

        .w-32 {
        width: 8rem;
        }

        .w-fit {
        width: -moz-fit-content;
        width: fit-content;
        }

        .w-\[380px\] {
        width: 380px;
        }

        .w-2\/6 {
        width: 33.333333%;
        }

        .w-\[150px\] {
        width: 150px;
        }

        .w-\[250px\] {
        width: 250px;
        }

        .w-44 {
        width: 11rem;
        }

        .w-28 {
        width: 7rem;
        }

        .w-40 {
        width: 10rem;
        }

        .w-\[30px\] {
        width: 30px;
        }

        .w-\[320px\] {
        width: 320px;
        }

        .w-24 {
        width: 6rem;
        }

        .w-1\/3 {
        width: 33.333333%;
        }

        .w-\[411px\] {
        width: 411px;
        }

        .w-80 {
        width: 20rem;
        }

        .w-\[64px\] {
        width: 64px;
        }

        .w-52 {
        width: 13rem;
        }

        .w-5\/6 {
        width: 83.333333%;
        }

        .w-1\/6 {
        width: 16.666667%;
        }

        .w-\[10rem\] {
        width: 10rem;
        }

        .w-\[292px\] {
        width: 292px;
        }

        .w-\[75px\] {
        width: 75px;
        }

        .w-\[306px\] {
        width: 306px;
        }

        .w-max {
        width: -moz-max-content;
        width: max-content;
        }

        .w-\[24px\] {
        width: 24px;
        }

        .w-\[600px\] {
        width: 600px;
        }

        .w-\[222px\] {
        width: 222px;
        }

        .w-\[37px\] {
        width: 37px;
        }

        .w-\[300px\] {
        width: 300px;
        }

        .w-\[363px\] {
        width: 363px;
        }

        .w-\[12px\] {
        width: 12px;
        }

        .w-\[50\%\] {
        width: 50%;
        }

        .w-1\/12 {
        width: 8.333333%;
        }

        .w-\[450px\] {
        width: 450px;
        }

        .w-\[451px\] {
        width: 451px;
        }

        .w-\[200px\] {
        width: 200px;
        }

        .w-\[270px\] {
        width: 270px;
        }

        .w-\[405px\] {
        width: 405px;
        }

        .w-\[25rem\] {
        width: 25rem;
        }

        .w-2\/3 {
        width: 66.666667%;
        }

        .w-\[48px\] {
        width: 48px;
        }

        .w-\[60px\] {
        width: 60px;
        }

        .w-\[20px\] {
        width: 20px;
        }

        .w-\[18px\] {
        width: 18px;
        }

        .w-\[40rem\] {
        width: 40rem;
        }

        .w-\[500px\] {
        width: 500px;
        }

        .w-screen {
        width: 100vw;
        }

        .w-\[156px\] {
        width: 156px;
        }

        .w-\[102px\] {
        width: 102px;
        }

        .w-\[42rem\] {
        width: 42rem;
        }

        .w-2\/4 {
        width: 50%;
        }

        .w-\[13rem\] {
        width: 13rem;
        }

        .w-\[9rem\] {
        width: 9rem;
        }

        .w-\[7rem\] {
        width: 7rem;
        }

        .w-\[720px\] {
        width: 720px;
        }

        .w-\[1080px\] {
        width: 1080px;
        }

        .w-\[314px\] {
        width: 314px;
        }

        .w-\[1920px\] {
        width: 1920px;
        }

        .w-11\/12 {
        width: 91.666667%;
        }

        .w-\[10px\] {
        width: 10px;
        }

        .w-\[255px\] {
        width: 255px;
        }

        .w-64 {
        width: 16rem;
        }

        .w-\[4rem\] {
        width: 4rem;
        }

        .w-\[15rem\] {
        width: 15rem;
        }

        .w-\[14px\] {
        width: 14px;
        }

        .w-\[65rem\] {
        width: 65rem;
        }

        .w-72 {
        width: 18rem;
        }

        .w-\[245px\] {
        width: 245px;
        }

        .min-w-0 {
        min-width: 0px;
        }

        .min-w-full {
        min-width: 100%;
        }

        .max-w-screen-xl {
        max-width: 1280px;
        }

        .max-w-7xl {
        max-width: 80rem;
        }

        .max-w-xl {
        max-width: 36rem;
        }

        .max-w-\[310px\] {
        max-width: 310px;
        }

        .max-w-screen-sm {
        max-width: 640px;
        }

        .max-w-6xl {
        max-width: 72rem;
        }

        .max-w-xs {
        max-width: 20rem;
        }

        .max-w-\[800px\] {
        max-width: 800px;
        }

        .max-w-full {
        max-width: 100%;
        }

        .max-w-\[1800px\] {
        max-width: 1800px;
        }

        .max-w-\[100px\] {
        max-width: 100px;
        }

        .max-w-\[520px\] {
        max-width: 520px;
        }

        .max-w-\[250px\] {
        max-width: 250px;
        }

        .max-w-sm {
        max-width: 24rem;
        }

        .max-w-\[50px\] {
        max-width: 50px;
        }

        .max-w-\[920px\] {
        max-width: 920px;
        }

        .max-w-\[322px\] {
        max-width: 322px;
        }

        .max-w-4xl {
        max-width: 56rem;
        }

        .max-w-lg {
        max-width: 32rem;
        }

        .max-w-3xl {
        max-width: 48rem;
        }

        .max-w-md {
        max-width: 28rem;
        }

        .flex-1 {
        flex: 1 1 0%;
        }

        .flex-none {
        flex: none;
        }

        .flex-auto {
        flex: 1 1 auto;
        }

        .flex-shrink-0 {
        flex-shrink: 0;
        }

        .shrink-0 {
        flex-shrink: 0;
        }

        .flex-grow {
        flex-grow: 1;
        }

        .grow {
        flex-grow: 1;
        }

        .table-auto {
        table-layout: auto;
        }

        .table-fixed {
        table-layout: fixed;
        }

        .border-separate {
        border-collapse: separate;
        }

        .border-spacing-2 {
        --tw-border-spacing-x: 0.5rem;
        --tw-border-spacing-y: 0.5rem;
        border-spacing: var(--tw-border-spacing-x) var(--tw-border-spacing-y);
        }

        .origin-top-left {
        transform-origin: top left;
        }

        .origin-top {
        transform-origin: top;
        }

        .origin-top-right {
        transform-origin: top right;
        }

        .translate-y-4 {
        --tw-translate-y: 1rem;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .translate-y-0 {
        --tw-translate-y: 0px;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .translate-x-12 {
        --tw-translate-x: 3rem;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .-translate-y-14 {
        --tw-translate-y: -3.5rem;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .-translate-y-4 {
        --tw-translate-y: -1rem;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .-translate-x-1\/2 {
        --tw-translate-x: -50%;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .-translate-y-1\/2 {
        --tw-translate-y: -50%;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .translate-y-1\/2 {
        --tw-translate-y: 50%;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .-translate-y-8 {
        --tw-translate-y: -2rem;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .translate-x-8 {
        --tw-translate-x: 2rem;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .translate-y-full {
        --tw-translate-y: 100%;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .translate-x-1\/2 {
        --tw-translate-x: 50%;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .-translate-x-5 {
        --tw-translate-x: -1.25rem;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .-translate-y-10 {
        --tw-translate-y: -2.5rem;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .translate-x-2 {
        --tw-translate-x: 0.5rem;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .-translate-x-2 {
        --tw-translate-x: -0.5rem;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .rotate-180 {
        --tw-rotate: 180deg;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .rotate-45 {
        --tw-rotate: 45deg;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .scale-95 {
        --tw-scale-x: .95;
        --tw-scale-y: .95;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .scale-100 {
        --tw-scale-x: 1;
        --tw-scale-y: 1;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .scale-90 {
        --tw-scale-x: .9;
        --tw-scale-y: .9;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .transform {
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        @keyframes spin {
        to {
            transform: rotate(360deg);
        }
        }

        .animate-spin {
        animation: spin 1s linear infinite;
        }

        @keyframes pulse {
        50% {
            opacity: .5;
        }
        }

        .animate-pulse {
        animation: pulse 2s cubic-bezier(0.4, 0, 0.6, 1) infinite;
        }

        @keyframes bounce {
        0%, 100% {
            transform: translateY(-25%);
            animation-timing-function: cubic-bezier(0.8,0,1,1);
        }

        50% {
            transform: none;
            animation-timing-function: cubic-bezier(0,0,0.2,1);
        }
        }

        .animate-bounce {
        animation: bounce 1s infinite;
        }

        .cursor-default {
        cursor: default;
        }

        .cursor-pointer {
        cursor: pointer;
        }

        .cursor-not-allowed {
        cursor: not-allowed;
        }

        .select-none {
        -webkit-user-select: none;
            -moz-user-select: none;
                user-select: none;
        }

        .resize {
        resize: both;
        }

        .snap-x {
        scroll-snap-type: x var(--tw-scroll-snap-strictness);
        }

        .snap-start {
        scroll-snap-align: start;
        }

        .scroll-ml-6 {
        scroll-margin-left: 1.5rem;
        }

        .list-inside {
        list-style-position: inside;
        }

        .list-disc {
        list-style-type: disc;
        }

        .grid-flow-row {
        grid-auto-flow: row;
        }

        .grid-flow-col {
        grid-auto-flow: column;
        }

        .grid-cols-6 {
        grid-template-columns: repeat(6, minmax(0, 1fr));
        }

        .grid-cols-1 {
        grid-template-columns: repeat(1, minmax(0, 1fr));
        }

        .grid-cols-4 {
        grid-template-columns: repeat(4, minmax(0, 1fr));
        }

        .grid-cols-2 {
        grid-template-columns: repeat(2, minmax(0, 1fr));
        }

        .grid-cols-10 {
        grid-template-columns: repeat(10, minmax(0, 1fr));
        }

        .grid-cols-3 {
        grid-template-columns: repeat(3, minmax(0, 1fr));
        }

        .grid-cols-\[80\%_20\%\] {
        grid-template-columns: 80% 20%;
        }

        .grid-cols-\[70\%_30\%\] {
        grid-template-columns: 70% 30%;
        }

        .grid-cols-\[20\%_80\%\] {
        grid-template-columns: 20% 80%;
        }

        .grid-cols-12 {
        grid-template-columns: repeat(12, minmax(0, 1fr));
        }

        .grid-cols-\[10\%_80\%_10\%\] {
        grid-template-columns: 10% 80% 10%;
        }

        .grid-cols-5 {
        grid-template-columns: repeat(5, minmax(0, 1fr));
        }

        .grid-cols-11 {
        grid-template-columns: repeat(11, minmax(0, 1fr));
        }

        .grid-cols-\[13\%_57\%_30\%\] {
        grid-template-columns: 13% 57% 30%;
        }

        .grid-cols-\[75\%_25\%\] {
        grid-template-columns: 75% 25%;
        }

        .grid-cols-\[30\%_50\%_10\%\] {
        grid-template-columns: 30% 50% 10%;
        }

        .grid-cols-7 {
        grid-template-columns: repeat(7, minmax(0, 1fr));
        }

        .grid-cols-none {
        grid-template-columns: none;
        }

        .grid-cols-\[25\%_15\%_60\%\] {
        grid-template-columns: 25% 15% 60%;
        }

        .grid-cols-\[10\%_40\%_40\%\] {
        grid-template-columns: 10% 40% 40%;
        }

        .grid-cols-8 {
        grid-template-columns: repeat(8, minmax(0, 1fr));
        }

        .grid-rows-1 {
        grid-template-rows: repeat(1, minmax(0, 1fr));
        }

        .grid-rows-2 {
        grid-template-rows: repeat(2, minmax(0, 1fr));
        }

        .grid-rows-3 {
        grid-template-rows: repeat(3, minmax(0, 1fr));
        }

        .grid-rows-5 {
        grid-template-rows: repeat(5, minmax(0, 1fr));
        }

        .grid-rows-6 {
        grid-template-rows: repeat(6, minmax(0, 1fr));
        }

        .flex-row {
        flex-direction: row;
        }

        .flex-col {
        flex-direction: column;
        }

        .flex-wrap {
        flex-wrap: wrap;
        }

        .place-content-center {
        place-content: center;
        }

        .place-content-start {
        place-content: start;
        }

        .place-content-end {
        place-content: end;
        }

        .place-items-end {
        place-items: end;
        }

        .place-items-center {
        place-items: center;
        }

        .content-center {
        align-content: center;
        }

        .content-end {
        align-content: flex-end;
        }

        .items-start {
        align-items: flex-start;
        }

        .items-end {
        align-items: flex-end;
        }

        .items-center {
        align-items: center;
        }

        .items-stretch {
        align-items: stretch;
        }

        .justify-start {
        justify-content: flex-start;
        }

        .justify-end {
        justify-content: flex-end;
        }

        .justify-center {
        justify-content: center;
        }

        .justify-between {
        justify-content: space-between;
        }

        .justify-items-start {
        justify-items: start;
        }

        .justify-items-end {
        justify-items: end;
        }

        .justify-items-center {
        justify-items: center;
        }

        .gap-6 {
        gap: 1.5rem;
        }

        .gap-4 {
        gap: 1rem;
        }

        .gap-1 {
        gap: 0.25rem;
        }

        .gap-2 {
        gap: 0.5rem;
        }

        .gap-5 {
        gap: 1.25rem;
        }

        .gap-3 {
        gap: 0.75rem;
        }

        .gap-10 {
        gap: 2.5rem;
        }

        .gap-2\.5 {
        gap: 0.625rem;
        }

        .gap-x-5 {
        -moz-column-gap: 1.25rem;
            column-gap: 1.25rem;
        }

        .gap-y-3 {
        row-gap: 0.75rem;
        }

        .space-x-8 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-x-reverse: 0;
        margin-right: calc(2rem * var(--tw-space-x-reverse));
        margin-left: calc(2rem * calc(1 - var(--tw-space-x-reverse)));
        }

        .space-y-1 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-y-reverse: 0;
        margin-top: calc(0.25rem * calc(1 - var(--tw-space-y-reverse)));
        margin-bottom: calc(0.25rem * var(--tw-space-y-reverse));
        }

        .space-y-6 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-y-reverse: 0;
        margin-top: calc(1.5rem * calc(1 - var(--tw-space-y-reverse)));
        margin-bottom: calc(1.5rem * var(--tw-space-y-reverse));
        }

        .space-y-5 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-y-reverse: 0;
        margin-top: calc(1.25rem * calc(1 - var(--tw-space-y-reverse)));
        margin-bottom: calc(1.25rem * var(--tw-space-y-reverse));
        }

        .space-y-2 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-y-reverse: 0;
        margin-top: calc(0.5rem * calc(1 - var(--tw-space-y-reverse)));
        margin-bottom: calc(0.5rem * var(--tw-space-y-reverse));
        }

        .space-y-3 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-y-reverse: 0;
        margin-top: calc(0.75rem * calc(1 - var(--tw-space-y-reverse)));
        margin-bottom: calc(0.75rem * var(--tw-space-y-reverse));
        }

        .space-x-1 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-x-reverse: 0;
        margin-right: calc(0.25rem * var(--tw-space-x-reverse));
        margin-left: calc(0.25rem * calc(1 - var(--tw-space-x-reverse)));
        }

        .space-x-2 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-x-reverse: 0;
        margin-right: calc(0.5rem * var(--tw-space-x-reverse));
        margin-left: calc(0.5rem * calc(1 - var(--tw-space-x-reverse)));
        }

        .space-x-3 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-x-reverse: 0;
        margin-right: calc(0.75rem * var(--tw-space-x-reverse));
        margin-left: calc(0.75rem * calc(1 - var(--tw-space-x-reverse)));
        }

        .-space-x-px > :not([hidden]) ~ :not([hidden]) {
        --tw-space-x-reverse: 0;
        margin-right: calc(-1px * var(--tw-space-x-reverse));
        margin-left: calc(-1px * calc(1 - var(--tw-space-x-reverse)));
        }

        .space-x-4 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-x-reverse: 0;
        margin-right: calc(1rem * var(--tw-space-x-reverse));
        margin-left: calc(1rem * calc(1 - var(--tw-space-x-reverse)));
        }

        .space-x-10 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-x-reverse: 0;
        margin-right: calc(2.5rem * var(--tw-space-x-reverse));
        margin-left: calc(2.5rem * calc(1 - var(--tw-space-x-reverse)));
        }

        .space-y-4 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-y-reverse: 0;
        margin-top: calc(1rem * calc(1 - var(--tw-space-y-reverse)));
        margin-bottom: calc(1rem * var(--tw-space-y-reverse));
        }

        .space-x-16 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-x-reverse: 0;
        margin-right: calc(4rem * var(--tw-space-x-reverse));
        margin-left: calc(4rem * calc(1 - var(--tw-space-x-reverse)));
        }

        .space-x-5 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-x-reverse: 0;
        margin-right: calc(1.25rem * var(--tw-space-x-reverse));
        margin-left: calc(1.25rem * calc(1 - var(--tw-space-x-reverse)));
        }

        .space-y-8 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-y-reverse: 0;
        margin-top: calc(2rem * calc(1 - var(--tw-space-y-reverse)));
        margin-bottom: calc(2rem * var(--tw-space-y-reverse));
        }

        .space-x-12 > :not([hidden]) ~ :not([hidden]) {
        --tw-space-x-reverse: 0;
        margin-right: calc(3rem * var(--tw-space-x-reverse));
        margin-left: calc(3rem * calc(1 - var(--tw-space-x-reverse)));
        }

        .divide-y > :not([hidden]) ~ :not([hidden]) {
        --tw-divide-y-reverse: 0;
        border-top-width: calc(1px * calc(1 - var(--tw-divide-y-reverse)));
        border-bottom-width: calc(1px * var(--tw-divide-y-reverse));
        }

        .divide-y-2 > :not([hidden]) ~ :not([hidden]) {
        --tw-divide-y-reverse: 0;
        border-top-width: calc(2px * calc(1 - var(--tw-divide-y-reverse)));
        border-bottom-width: calc(2px * var(--tw-divide-y-reverse));
        }

        .divide-x > :not([hidden]) ~ :not([hidden]) {
        --tw-divide-x-reverse: 0;
        border-right-width: calc(1px * var(--tw-divide-x-reverse));
        border-left-width: calc(1px * calc(1 - var(--tw-divide-x-reverse)));
        }

        .divide-y-reverse > :not([hidden]) ~ :not([hidden]) {
        --tw-divide-y-reverse: 1;
        }

        .divide-kamtuu-second > :not([hidden]) ~ :not([hidden]) {
        --tw-divide-opacity: 1;
        border-color: rgb(35 174 193 / var(--tw-divide-opacity));
        }

        .divide-kamtuu-primary > :not([hidden]) ~ :not([hidden]) {
        --tw-divide-opacity: 1;
        border-color: rgb(158 61 100 / var(--tw-divide-opacity));
        }

        .divide-\[\#9E3D64\] > :not([hidden]) ~ :not([hidden]) {
        --tw-divide-opacity: 1;
        border-color: rgb(158 61 100 / var(--tw-divide-opacity));
        }

        .place-self-start {
        place-self: start;
        }

        .place-self-center {
        place-self: center;
        }

        .justify-self-center {
        justify-self: center;
        }

        .justify-self-stretch {
        justify-self: stretch;
        }

        .overflow-auto {
        overflow: auto;
        }

        .overflow-hidden {
        overflow: hidden;
        }

        .overflow-scroll {
        overflow: scroll;
        }

        .overflow-x-auto {
        overflow-x: auto;
        }

        .overflow-y-auto {
        overflow-y: auto;
        }

        .overflow-x-scroll {
        overflow-x: scroll;
        }

        .overflow-y-scroll {
        overflow-y: scroll;
        }

        .truncate {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        }

        .text-ellipsis {
        text-overflow: ellipsis;
        }

        .whitespace-normal {
        white-space: normal;
        }

        .whitespace-nowrap {
        white-space: nowrap;
        }

        .break-words {
        overflow-wrap: break-word;
        }

        .break-all {
        word-break: break-all;
        }

        .rounded-md {
        border-radius: 0.375rem;
        }

        .rounded-lg {
        border-radius: 0.5rem;
        }

        .rounded {
        border-radius: 0.25rem;
        }

        .rounded-full {
        border-radius: 9999px;
        }

        .rounded-sm {
        border-radius: 0.125rem;
        }

        .rounded-xl {
        border-radius: 0.75rem;
        }

        .rounded-2xl {
        border-radius: 1rem;
        }

        .rounded-3xl {
        border-radius: 1.5rem;
        }

        .rounded-l-md {
        border-top-left-radius: 0.375rem;
        border-bottom-left-radius: 0.375rem;
        }

        .rounded-r-md {
        border-top-right-radius: 0.375rem;
        border-bottom-right-radius: 0.375rem;
        }

        .rounded-t-none {
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        }

        .rounded-b-none {
        border-bottom-right-radius: 0px;
        border-bottom-left-radius: 0px;
        }

        .rounded-r-2xl {
        border-top-right-radius: 1rem;
        border-bottom-right-radius: 1rem;
        }

        .rounded-t-lg {
        border-top-left-radius: 0.5rem;
        border-top-right-radius: 0.5rem;
        }

        .rounded-t-2xl {
        border-top-left-radius: 1rem;
        border-top-right-radius: 1rem;
        }

        .rounded-t-md {
        border-top-left-radius: 0.375rem;
        border-top-right-radius: 0.375rem;
        }

        .rounded-l-lg {
        border-top-left-radius: 0.5rem;
        border-bottom-left-radius: 0.5rem;
        }

        .rounded-r-lg {
        border-top-right-radius: 0.5rem;
        border-bottom-right-radius: 0.5rem;
        }

        .rounded-t {
        border-top-left-radius: 0.25rem;
        border-top-right-radius: 0.25rem;
        }

        .rounded-b {
        border-bottom-right-radius: 0.25rem;
        border-bottom-left-radius: 0.25rem;
        }

        .rounded-t-xl {
        border-top-left-radius: 0.75rem;
        border-top-right-radius: 0.75rem;
        }

        .rounded-tl-none {
        border-top-left-radius: 0px;
        }

        .rounded-tl-lg {
        border-top-left-radius: 0.5rem;
        }

        .rounded-bl-lg {
        border-bottom-left-radius: 0.5rem;
        }

        .rounded-br-lg {
        border-bottom-right-radius: 0.5rem;
        }

        .border {
        border-width: 1px;
        }

        .border-2 {
        border-width: 2px;
        }

        .border-0 {
        border-width: 0px;
        }

        .border-8 {
        border-width: 8px;
        }

        .border-\[1px\] {
        border-width: 1px;
        }

        .border-y {
        border-top-width: 1px;
        border-bottom-width: 1px;
        }

        .border-y-2 {
        border-top-width: 2px;
        border-bottom-width: 2px;
        }

        .border-y-\[1px\] {
        border-top-width: 1px;
        border-bottom-width: 1px;
        }

        .border-b-2 {
        border-bottom-width: 2px;
        }

        .border-l-4 {
        border-left-width: 4px;
        }

        .border-t {
        border-top-width: 1px;
        }

        .border-b {
        border-bottom-width: 1px;
        }

        .border-t-2 {
        border-top-width: 2px;
        }

        .border-b-0 {
        border-bottom-width: 0px;
        }

        .border-t-4 {
        border-top-width: 4px;
        }

        .border-r-2 {
        border-right-width: 2px;
        }

        .border-b-\[1px\] {
        border-bottom-width: 1px;
        }

        .border-l-8 {
        border-left-width: 8px;
        }

        .border-r {
        border-right-width: 1px;
        }

        .border-l {
        border-left-width: 1px;
        }

        .border-t-0 {
        border-top-width: 0px;
        }

        .border-l-2 {
        border-left-width: 2px;
        }

        .border-solid {
        border-style: solid;
        }

        .border-dashed {
        border-style: dashed;
        }

        .border-gray-300 {
        --tw-border-opacity: 1;
        border-color: rgb(209 213 219 / var(--tw-border-opacity));
        }

        .border-transparent {
        border-color: transparent;
        }

        .border-indigo-400 {
        --tw-border-opacity: 1;
        border-color: rgb(129 140 248 / var(--tw-border-opacity));
        }

        .border-gray-200 {
        --tw-border-opacity: 1;
        border-color: rgb(229 231 235 / var(--tw-border-opacity));
        }

        .border-gray-100 {
        --tw-border-opacity: 1;
        border-color: rgb(243 244 246 / var(--tw-border-opacity));
        }

        .border-\[\#9E3D64\] {
        --tw-border-opacity: 1;
        border-color: rgb(158 61 100 / var(--tw-border-opacity));
        }

        .border-\[\#FFB800\] {
        --tw-border-opacity: 1;
        border-color: rgb(255 184 0 / var(--tw-border-opacity));
        }

        .border-\[\#4F4F4F\] {
        --tw-border-opacity: 1;
        border-color: rgb(79 79 79 / var(--tw-border-opacity));
        }

        .border-\[\#23AEC1\] {
        --tw-border-opacity: 1;
        border-color: rgb(35 174 193 / var(--tw-border-opacity));
        }

        .border-indigo-600 {
        --tw-border-opacity: 1;
        border-color: rgb(79 70 229 / var(--tw-border-opacity));
        }

        .border-gray-400 {
        --tw-border-opacity: 1;
        border-color: rgb(156 163 175 / var(--tw-border-opacity));
        }

        .border-\[\#FFFFFF\] {
        --tw-border-opacity: 1;
        border-color: rgb(255 255 255 / var(--tw-border-opacity));
        }

        .border-black {
        --tw-border-opacity: 1;
        border-color: rgb(0 0 0 / var(--tw-border-opacity));
        }

        .border-red-700 {
        --tw-border-opacity: 1;
        border-color: rgb(185 28 28 / var(--tw-border-opacity));
        }

        .border-green-700 {
        --tw-border-opacity: 1;
        border-color: rgb(21 128 61 / var(--tw-border-opacity));
        }

        .border-\[\#9B9B9B\] {
        --tw-border-opacity: 1;
        border-color: rgb(155 155 155 / var(--tw-border-opacity));
        }

        .border-indigo-500 {
        --tw-border-opacity: 1;
        border-color: rgb(99 102 241 / var(--tw-border-opacity));
        }

        .border-gray-600 {
        --tw-border-opacity: 1;
        border-color: rgb(75 85 99 / var(--tw-border-opacity));
        }

        .border-\[\#BDBDBD\] {
        --tw-border-opacity: 1;
        border-color: rgb(189 189 189 / var(--tw-border-opacity));
        }

        .border-\[\#828282\] {
        --tw-border-opacity: 1;
        border-color: rgb(130 130 130 / var(--tw-border-opacity));
        }

        .border-\[\#D50006\] {
        --tw-border-opacity: 1;
        border-color: rgb(213 0 6 / var(--tw-border-opacity));
        }

        .border-\[\#333333\] {
        --tw-border-opacity: 1;
        border-color: rgb(51 51 51 / var(--tw-border-opacity));
        }

        .border-slate-500 {
        --tw-border-opacity: 1;
        border-color: rgb(100 116 139 / var(--tw-border-opacity));
        }

        .border-slate-300 {
        --tw-border-opacity: 1;
        border-color: rgb(203 213 225 / var(--tw-border-opacity));
        }

        .border-\[\#E0E0E0\] {
        --tw-border-opacity: 1;
        border-color: rgb(224 224 224 / var(--tw-border-opacity));
        }

        .border-\[\#707070\] {
        --tw-border-opacity: 1;
        border-color: rgb(112 112 112 / var(--tw-border-opacity));
        }

        .border-slate-200 {
        --tw-border-opacity: 1;
        border-color: rgb(226 232 240 / var(--tw-border-opacity));
        }

        .border-slate-600 {
        --tw-border-opacity: 1;
        border-color: rgb(71 85 105 / var(--tw-border-opacity));
        }

        .border-\[\#F2F2F2\] {
        --tw-border-opacity: 1;
        border-color: rgb(242 242 242 / var(--tw-border-opacity));
        }

        .border-red-300 {
        --tw-border-opacity: 1;
        border-color: rgb(252 165 165 / var(--tw-border-opacity));
        }

        .border-gray-500 {
        --tw-border-opacity: 1;
        border-color: rgb(107 114 128 / var(--tw-border-opacity));
        }

        .border-slate-700 {
        --tw-border-opacity: 1;
        border-color: rgb(51 65 85 / var(--tw-border-opacity));
        }

        .border-stone-900 {
        --tw-border-opacity: 1;
        border-color: rgb(28 25 23 / var(--tw-border-opacity));
        }

        .border-kamtuu-primary {
        --tw-border-opacity: 1;
        border-color: rgb(158 61 100 / var(--tw-border-opacity));
        }

        .border-kamtuu-second {
        --tw-border-opacity: 1;
        border-color: rgb(35 174 193 / var(--tw-border-opacity));
        }

        .border-red-500 {
        --tw-border-opacity: 1;
        border-color: rgb(239 68 68 / var(--tw-border-opacity));
        }

        .border-cyan-400 {
        --tw-border-opacity: 1;
        border-color: rgb(34 211 238 / var(--tw-border-opacity));
        }

        .border-blue-500 {
        --tw-border-opacity: 1;
        border-color: rgb(59 130 246 / var(--tw-border-opacity));
        }

        .border-red-600 {
        --tw-border-opacity: 1;
        border-color: rgb(220 38 38 / var(--tw-border-opacity));
        }

        .\!border-kamtuu-primary {
        --tw-border-opacity: 1 !important;
        border-color: rgb(158 61 100 / var(--tw-border-opacity)) !important;
        }

        .border-zinc-50 {
        --tw-border-opacity: 1;
        border-color: rgb(250 250 250 / var(--tw-border-opacity));
        }

        .border-\[\#ffffff\] {
        --tw-border-opacity: 1;
        border-color: rgb(255 255 255 / var(--tw-border-opacity));
        }

        .border-\[\#4f4f4f\] {
        --tw-border-opacity: 1;
        border-color: rgb(79 79 79 / var(--tw-border-opacity));
        }

        .border-white {
        --tw-border-opacity: 1;
        border-color: rgb(255 255 255 / var(--tw-border-opacity));
        }

        .border-y-\[\#828282\] {
        --tw-border-opacity: 1;
        border-top-color: rgb(130 130 130 / var(--tw-border-opacity));
        border-bottom-color: rgb(130 130 130 / var(--tw-border-opacity));
        }

        .border-t-transparent {
        border-top-color: transparent;
        }

        .border-b-\[\#9E3D64\] {
        --tw-border-opacity: 1;
        border-bottom-color: rgb(158 61 100 / var(--tw-border-opacity));
        }

        .border-b-slate-200 {
        --tw-border-opacity: 1;
        border-bottom-color: rgb(226 232 240 / var(--tw-border-opacity));
        }

        .bg-white {
        --tw-bg-opacity: 1;
        background-color: rgb(255 255 255 / var(--tw-bg-opacity));
        }

        .bg-gray-100 {
        --tw-bg-opacity: 1;
        background-color: rgb(243 244 246 / var(--tw-bg-opacity));
        }

        .bg-indigo-500 {
        --tw-bg-opacity: 1;
        background-color: rgb(99 102 241 / var(--tw-bg-opacity));
        }

        .bg-red-700 {
        --tw-bg-opacity: 1;
        background-color: rgb(185 28 28 / var(--tw-bg-opacity));
        }

        .bg-gray-500 {
        --tw-bg-opacity: 1;
        background-color: rgb(107 114 128 / var(--tw-bg-opacity));
        }

        .bg-indigo-600 {
        --tw-bg-opacity: 1;
        background-color: rgb(79 70 229 / var(--tw-bg-opacity));
        }

        .bg-red-600 {
        --tw-bg-opacity: 1;
        background-color: rgb(220 38 38 / var(--tw-bg-opacity));
        }

        .bg-gray-800 {
        --tw-bg-opacity: 1;
        background-color: rgb(31 41 55 / var(--tw-bg-opacity));
        }

        .bg-red-100 {
        --tw-bg-opacity: 1;
        background-color: rgb(254 226 226 / var(--tw-bg-opacity));
        }

        .bg-gray-50 {
        --tw-bg-opacity: 1;
        background-color: rgb(249 250 251 / var(--tw-bg-opacity));
        }

        .bg-indigo-50 {
        --tw-bg-opacity: 1;
        background-color: rgb(238 242 255 / var(--tw-bg-opacity));
        }

        .bg-gray-200 {
        --tw-bg-opacity: 1;
        background-color: rgb(229 231 235 / var(--tw-bg-opacity));
        }

        .bg-\[\#F2F2F2\] {
        --tw-bg-opacity: 1;
        background-color: rgb(242 242 242 / var(--tw-bg-opacity));
        }

        .bg-\[\#ffff\] {
        background-color: #ffff;
        }

        .bg-green-300 {
        --tw-bg-opacity: 1;
        background-color: rgb(134 239 172 / var(--tw-bg-opacity));
        }

        .bg-slate-200 {
        --tw-bg-opacity: 1;
        background-color: rgb(226 232 240 / var(--tw-bg-opacity));
        }

        .bg-red-300 {
        --tw-bg-opacity: 1;
        background-color: rgb(252 165 165 / var(--tw-bg-opacity));
        }

        .bg-\[\#23AEC1\] {
        --tw-bg-opacity: 1;
        background-color: rgb(35 174 193 / var(--tw-bg-opacity));
        }

        .bg-\[\#9E3D64\] {
        --tw-bg-opacity: 1;
        background-color: rgb(158 61 100 / var(--tw-bg-opacity));
        }

        .bg-\[\#FFB800\] {
        --tw-bg-opacity: 1;
        background-color: rgb(255 184 0 / var(--tw-bg-opacity));
        }

        .bg-\[\#D50006\] {
        --tw-bg-opacity: 1;
        background-color: rgb(213 0 6 / var(--tw-bg-opacity));
        }

        .bg-\[\#FFFFFF\] {
        --tw-bg-opacity: 1;
        background-color: rgb(255 255 255 / var(--tw-bg-opacity));
        }

        .bg-kamtuu-second {
        --tw-bg-opacity: 1;
        background-color: rgb(35 174 193 / var(--tw-bg-opacity));
        }

        .bg-kamtuu-primary {
        --tw-bg-opacity: 1;
        background-color: rgb(158 61 100 / var(--tw-bg-opacity));
        }

        .bg-kamtuu-third {
        --tw-bg-opacity: 1;
        background-color: rgb(255 184 0 / var(--tw-bg-opacity));
        }

        .bg-gray-600 {
        --tw-bg-opacity: 1;
        background-color: rgb(75 85 99 / var(--tw-bg-opacity));
        }

        .bg-blue-500 {
        --tw-bg-opacity: 1;
        background-color: rgb(59 130 246 / var(--tw-bg-opacity));
        }

        .bg-green-500 {
        --tw-bg-opacity: 1;
        background-color: rgb(34 197 94 / var(--tw-bg-opacity));
        }

        .bg-slate-300 {
        --tw-bg-opacity: 1;
        background-color: rgb(203 213 225 / var(--tw-bg-opacity));
        }

        .bg-\[\#51B449\] {
        --tw-bg-opacity: 1;
        background-color: rgb(81 180 73 / var(--tw-bg-opacity));
        }

        .bg-gray-400 {
        --tw-bg-opacity: 1;
        background-color: rgb(156 163 175 / var(--tw-bg-opacity));
        }

        .bg-slate-500 {
        --tw-bg-opacity: 1;
        background-color: rgb(100 116 139 / var(--tw-bg-opacity));
        }

        .bg-gray-300 {
        --tw-bg-opacity: 1;
        background-color: rgb(209 213 219 / var(--tw-bg-opacity));
        }

        .bg-\[\#F9F9F9\] {
        --tw-bg-opacity: 1;
        background-color: rgb(249 249 249 / var(--tw-bg-opacity));
        }

        .bg-transparent {
        background-color: transparent;
        }

        .bg-\[\#ffffff\] {
        --tw-bg-opacity: 1;
        background-color: rgb(255 255 255 / var(--tw-bg-opacity));
        }

        .bg-slate-400 {
        --tw-bg-opacity: 1;
        background-color: rgb(148 163 184 / var(--tw-bg-opacity));
        }

        .bg-\[\#BDBDBD\] {
        --tw-bg-opacity: 1;
        background-color: rgb(189 189 189 / var(--tw-bg-opacity));
        }

        .bg-blue-700 {
        --tw-bg-opacity: 1;
        background-color: rgb(29 78 216 / var(--tw-bg-opacity));
        }

        .bg-\[\#D25889\] {
        --tw-bg-opacity: 1;
        background-color: rgb(210 88 137 / var(--tw-bg-opacity));
        }

        .bg-yellow-500 {
        --tw-bg-opacity: 1;
        background-color: rgb(234 179 8 / var(--tw-bg-opacity));
        }

        .bg-red-500 {
        --tw-bg-opacity: 1;
        background-color: rgb(239 68 68 / var(--tw-bg-opacity));
        }

        .bg-slate-100 {
        --tw-bg-opacity: 1;
        background-color: rgb(241 245 249 / var(--tw-bg-opacity));
        }

        .bg-\[\#F7F7F7\] {
        --tw-bg-opacity: 1;
        background-color: rgb(247 247 247 / var(--tw-bg-opacity));
        }

        .bg-\[\#27BED3\] {
        --tw-bg-opacity: 1;
        background-color: rgb(39 190 211 / var(--tw-bg-opacity));
        }

        .bg-blue-100 {
        --tw-bg-opacity: 1;
        background-color: rgb(219 234 254 / var(--tw-bg-opacity));
        }

        .bg-cyan-900 {
        --tw-bg-opacity: 1;
        background-color: rgb(22 78 99 / var(--tw-bg-opacity));
        }

        .bg-violet-50 {
        --tw-bg-opacity: 1;
        background-color: rgb(245 243 255 / var(--tw-bg-opacity));
        }

        .bg-\[\#A0A0A0\] {
        --tw-bg-opacity: 1;
        background-color: rgb(160 160 160 / var(--tw-bg-opacity));
        }

        .bg-\[\#DFFFD7\] {
        --tw-bg-opacity: 1;
        background-color: rgb(223 255 215 / var(--tw-bg-opacity));
        }

        .bg-\[\#F4F4F4\] {
        --tw-bg-opacity: 1;
        background-color: rgb(244 244 244 / var(--tw-bg-opacity));
        }

        .bg-red-200 {
        --tw-bg-opacity: 1;
        background-color: rgb(254 202 202 / var(--tw-bg-opacity));
        }

        .bg-black\/70 {
        background-color: rgb(0 0 0 / 0.7);
        }

        .bg-\[\#00d532\] {
        --tw-bg-opacity: 1;
        background-color: rgb(0 213 50 / var(--tw-bg-opacity));
        }

        .bg-blue-600 {
        --tw-bg-opacity: 1;
        background-color: rgb(37 99 235 / var(--tw-bg-opacity));
        }

        .bg-cyan-400 {
        --tw-bg-opacity: 1;
        background-color: rgb(34 211 238 / var(--tw-bg-opacity));
        }

        .bg-cyan-200 {
        --tw-bg-opacity: 1;
        background-color: rgb(165 243 252 / var(--tw-bg-opacity));
        }

        .bg-gray-700 {
        --tw-bg-opacity: 1;
        background-color: rgb(55 65 81 / var(--tw-bg-opacity));
        }

        .bg-gray-900 {
        --tw-bg-opacity: 1;
        background-color: rgb(17 24 39 / var(--tw-bg-opacity));
        }

        .bg-red-50 {
        --tw-bg-opacity: 1;
        background-color: rgb(254 242 242 / var(--tw-bg-opacity));
        }

        .bg-\[\#0ed500\] {
        --tw-bg-opacity: 1;
        background-color: rgb(14 213 0 / var(--tw-bg-opacity));
        }

        .bg-green-200 {
        --tw-bg-opacity: 1;
        background-color: rgb(187 247 208 / var(--tw-bg-opacity));
        }

        .bg-\[white\] {
        --tw-bg-opacity: 1;
        background-color: rgb(255 255 255 / var(--tw-bg-opacity));
        }

        .bg-red-400 {
        --tw-bg-opacity: 1;
        background-color: rgb(248 113 113 / var(--tw-bg-opacity));
        }

        .bg-opacity-25 {
        --tw-bg-opacity: 0.25;
        }

        .bg-opacity-50 {
        --tw-bg-opacity: 0.5;
        }

        .bg-opacity-80 {
        --tw-bg-opacity: 0.8;
        }

        .bg-opacity-40 {
        --tw-bg-opacity: 0.4;
        }

        .bg-opacity-30 {
        --tw-bg-opacity: 0.3;
        }

        .bg-cover {
        background-size: cover;
        }

        .bg-contain {
        background-size: contain;
        }

        .bg-clip-padding {
        background-clip: padding-box;
        }

        .bg-clip-content {
        background-clip: content-box;
        }

        .bg-center {
        background-position: center;
        }

        .bg-no-repeat {
        background-repeat: no-repeat;
        }

        .fill-current {
        fill: currentColor;
        }

        .fill-red-500 {
        fill: #ef4444;
        }

        .fill-white {
        fill: #fff;
        }

        .object-contain {
        -o-object-fit: contain;
            object-fit: contain;
        }

        .object-cover {
        -o-object-fit: cover;
            object-fit: cover;
        }

        .object-center {
        -o-object-position: center;
            object-position: center;
        }

        .p-2 {
        padding: 0.5rem;
        }

        .p-6 {
        padding: 1.5rem;
        }

        .p-5 {
        padding: 1.25rem;
        }

        .p-3 {
        padding: 0.75rem;
        }

        .p-7 {
        padding: 1.75rem;
        }

        .p-4 {
        padding: 1rem;
        }

        .p-0\.5 {
        padding: 0.125rem;
        }

        .p-0 {
        padding: 0px;
        }

        .p-10 {
        padding: 2.5rem;
        }

        .p-2\.5 {
        padding: 0.625rem;
        }

        .p-\[30px\] {
        padding: 30px;
        }

        .p-8 {
        padding: 2rem;
        }

        .p-1 {
        padding: 0.25rem;
        }

        .p-9 {
        padding: 2.25rem;
        }

        .p-1\.5 {
        padding: 0.375rem;
        }

        .px-4 {
        padding-left: 1rem;
        padding-right: 1rem;
        }

        .py-2 {
        padding-top: 0.5rem;
        padding-bottom: 0.5rem;
        }

        .px-2 {
        padding-left: 0.5rem;
        padding-right: 0.5rem;
        }

        .py-5 {
        padding-top: 1.25rem;
        padding-bottom: 1.25rem;
        }

        .px-6 {
        padding-left: 1.5rem;
        padding-right: 1.5rem;
        }

        .py-4 {
        padding-top: 1rem;
        padding-bottom: 1rem;
        }

        .px-3 {
        padding-left: 0.75rem;
        padding-right: 0.75rem;
        }

        .py-1 {
        padding-top: 0.25rem;
        padding-bottom: 0.25rem;
        }

        .py-3 {
        padding-top: 0.75rem;
        padding-bottom: 0.75rem;
        }

        .py-6 {
        padding-top: 1.5rem;
        padding-bottom: 1.5rem;
        }

        .px-1 {
        padding-left: 0.25rem;
        padding-right: 0.25rem;
        }

        .py-8 {
        padding-top: 2rem;
        padding-bottom: 2rem;
        }

        .py-12 {
        padding-top: 3rem;
        padding-bottom: 3rem;
        }

        .py-10 {
        padding-top: 2.5rem;
        padding-bottom: 2.5rem;
        }

        .px-7 {
        padding-left: 1.75rem;
        padding-right: 1.75rem;
        }

        .px-8 {
        padding-left: 2rem;
        padding-right: 2rem;
        }

        .px-5 {
        padding-left: 1.25rem;
        padding-right: 1.25rem;
        }

        .py-\[2px\] {
        padding-top: 2px;
        padding-bottom: 2px;
        }

        .px-\[5px\] {
        padding-left: 5px;
        padding-right: 5px;
        }

        .py-2\.5 {
        padding-top: 0.625rem;
        padding-bottom: 0.625rem;
        }

        .px-\[10px\] {
        padding-left: 10px;
        padding-right: 10px;
        }

        .px-10 {
        padding-left: 2.5rem;
        padding-right: 2.5rem;
        }

        .py-20 {
        padding-top: 5rem;
        padding-bottom: 5rem;
        }

        .py-px {
        padding-top: 1px;
        padding-bottom: 1px;
        }

        .px-px {
        padding-left: 1px;
        padding-right: 1px;
        }

        .px-2\.5 {
        padding-left: 0.625rem;
        padding-right: 0.625rem;
        }

        .py-1\.5 {
        padding-top: 0.375rem;
        padding-bottom: 0.375rem;
        }

        .px-9 {
        padding-left: 2.25rem;
        padding-right: 2.25rem;
        }

        .py-\[22px\] {
        padding-top: 22px;
        padding-bottom: 22px;
        }

        .py-0 {
        padding-top: 0px;
        padding-bottom: 0px;
        }

        .px-0 {
        padding-left: 0px;
        padding-right: 0px;
        }

        .py-\[3px\] {
        padding-top: 3px;
        padding-bottom: 3px;
        }

        .px-\[3px\] {
        padding-left: 3px;
        padding-right: 3px;
        }

        .pt-6 {
        padding-top: 1.5rem;
        }

        .pt-5 {
        padding-top: 1.25rem;
        }

        .pb-4 {
        padding-bottom: 1rem;
        }

        .pt-1 {
        padding-top: 0.25rem;
        }

        .pl-3 {
        padding-left: 0.75rem;
        }

        .pr-4 {
        padding-right: 1rem;
        }

        .pt-2 {
        padding-top: 0.5rem;
        }

        .pb-3 {
        padding-bottom: 0.75rem;
        }

        .pt-4 {
        padding-top: 1rem;
        }

        .pb-1 {
        padding-bottom: 0.25rem;
        }

        .pb-5 {
        padding-bottom: 1.25rem;
        }

        .pr-2 {
        padding-right: 0.5rem;
        }

        .pl-7 {
        padding-left: 1.75rem;
        }

        .pt-3 {
        padding-top: 0.75rem;
        }

        .pl-11 {
        padding-left: 2.75rem;
        }

        .pl-10 {
        padding-left: 2.5rem;
        }

        .pr-10 {
        padding-right: 2.5rem;
        }

        .pb-8 {
        padding-bottom: 2rem;
        }

        .pt-8 {
        padding-top: 2rem;
        }

        .pr-7 {
        padding-right: 1.75rem;
        }

        .pl-\[15\%\] {
        padding-left: 15%;
        }

        .pl-5 {
        padding-left: 1.25rem;
        }

        .pr-16 {
        padding-right: 4rem;
        }

        .pr-\[37\.4px\] {
        padding-right: 37.4px;
        }

        .pr-\[41px\] {
        padding-right: 41px;
        }

        .pb-2 {
        padding-bottom: 0.5rem;
        }

        .pr-3 {
        padding-right: 0.75rem;
        }

        .pr-5 {
        padding-right: 1.25rem;
        }

        .pb-6 {
        padding-bottom: 1.5rem;
        }

        .pl-\[1rem\] {
        padding-left: 1rem;
        }

        .pt-px {
        padding-top: 1px;
        }

        .pl-2 {
        padding-left: 0.5rem;
        }

        .pt-7 {
        padding-top: 1.75rem;
        }

        .pl-0 {
        padding-left: 0px;
        }

        .pb-0 {
        padding-bottom: 0px;
        }

        .pt-10 {
        padding-top: 2.5rem;
        }

        .pl-20 {
        padding-left: 5rem;
        }

        .pb-10 {
        padding-bottom: 2.5rem;
        }

        .pr-8 {
        padding-right: 2rem;
        }

        .pt-0 {
        padding-top: 0px;
        }

        .pl-8 {
        padding-left: 2rem;
        }

        .pb-2\.5 {
        padding-bottom: 0.625rem;
        }

        .pt-2\.5 {
        padding-top: 0.625rem;
        }

        .pb-20 {
        padding-bottom: 5rem;
        }

        .text-left {
        text-align: left;
        }

        .text-center {
        text-align: center;
        }

        .text-right {
        text-align: right;
        }

        .text-justify {
        text-align: justify;
        }

        .text-start {
        text-align: start;
        }

        .text-end {
        text-align: end;
        }

        .indent-8 {
        text-indent: 2rem;
        }

        .align-top {
        vertical-align: top;
        }

        .align-middle {
        vertical-align: middle;
        }

        .align-bottom {
        vertical-align: bottom;
        }

        .font-sans {
        font-family: Nunito, ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        }

        .font-mono {
        font-family: ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
        }

        .font-Inter {
        font-family: Inter;
        }

        .text-sm {
        font-size: 0.875rem;
        line-height: 1.25rem;
        }

        .text-xs {
        font-size: 0.75rem;
        line-height: 1rem;
        }

        .text-lg {
        font-size: 1.125rem;
        line-height: 1.75rem;
        }

        .text-base {
        font-size: 1rem;
        line-height: 1.5rem;
        }

        .text-2xl {
        font-size: 1.5rem;
        line-height: 2rem;
        }

        .text-xl {
        font-size: 1.25rem;
        line-height: 1.75rem;
        }

        .text-\[18px\] {
        font-size: 18px;
        }

        .text-\[16px\] {
        font-size: 16px;
        }

        .text-\[14px\] {
        font-size: 14px;
        }

        .text-\[8px\] {
        font-size: 8px;
        }

        .text-\[24px\] {
        font-size: 24px;
        }

        .text-\[30px\] {
        font-size: 30px;
        }

        .text-\[22px\] {
        font-size: 22px;
        }

        .text-\[12px\] {
        font-size: 12px;
        }

        .text-4xl {
        font-size: 2.25rem;
        line-height: 2.5rem;
        }

        .text-3xl {
        font-size: 1.875rem;
        line-height: 2.25rem;
        }

        .text-\[26px\] {
        font-size: 26px;
        }

        .text-\[10px\] {
        font-size: 10px;
        }

        .text-\[32px\] {
        font-size: 32px;
        }

        .text-\[50px\] {
        font-size: 50px;
        }

        .text-5xl {
        font-size: 3rem;
        line-height: 1;
        }

        .text-\[6px\] {
        font-size: 6px;
        }

        .text-\[20px\] {
        font-size: 20px;
        }

        .font-medium {
        font-weight: 500;
        }

        .font-semibold {
        font-weight: 600;
        }

        .font-bold {
        font-weight: 700;
        }

        .font-thin {
        font-weight: 100;
        }

        .font-normal {
        font-weight: 400;
        }

        .font-extrabold {
        font-weight: 800;
        }

        .font-light {
        font-weight: 300;
        }

        .uppercase {
        text-transform: uppercase;
        }

        .lowercase {
        text-transform: lowercase;
        }

        .capitalize {
        text-transform: capitalize;
        }

        .italic {
        font-style: italic;
        }

        .leading-5 {
        line-height: 1.25rem;
        }

        .leading-7 {
        line-height: 1.75rem;
        }

        .leading-tight {
        line-height: 1.25;
        }

        .leading-4 {
        line-height: 1rem;
        }

        .leading-none {
        line-height: 1;
        }

        .leading-normal {
        line-height: 1.5;
        }

        .leading-6 {
        line-height: 1.5rem;
        }

        .leading-9 {
        line-height: 2.25rem;
        }

        .tracking-widest {
        letter-spacing: 0.1em;
        }

        .tracking-wide {
        letter-spacing: 0.025em;
        }

        .tracking-wider {
        letter-spacing: 0.05em;
        }

        .tracking-tight {
        letter-spacing: -0.025em;
        }

        .text-gray-500 {
        --tw-text-opacity: 1;
        color: rgb(107 114 128 / var(--tw-text-opacity));
        }

        .text-gray-700 {
        --tw-text-opacity: 1;
        color: rgb(55 65 81 / var(--tw-text-opacity));
        }

        .text-gray-600 {
        --tw-text-opacity: 1;
        color: rgb(75 85 99 / var(--tw-text-opacity));
        }

        .text-white {
        --tw-text-opacity: 1;
        color: rgb(255 255 255 / var(--tw-text-opacity));
        }

        .text-indigo-600 {
        --tw-text-opacity: 1;
        color: rgb(79 70 229 / var(--tw-text-opacity));
        }

        .text-red-600 {
        --tw-text-opacity: 1;
        color: rgb(220 38 38 / var(--tw-text-opacity));
        }

        .text-gray-900 {
        --tw-text-opacity: 1;
        color: rgb(17 24 39 / var(--tw-text-opacity));
        }

        .text-indigo-700 {
        --tw-text-opacity: 1;
        color: rgb(67 56 202 / var(--tw-text-opacity));
        }

        .text-green-400 {
        --tw-text-opacity: 1;
        color: rgb(74 222 128 / var(--tw-text-opacity));
        }

        .text-gray-400 {
        --tw-text-opacity: 1;
        color: rgb(156 163 175 / var(--tw-text-opacity));
        }

        .text-indigo-500 {
        --tw-text-opacity: 1;
        color: rgb(99 102 241 / var(--tw-text-opacity));
        }

        .text-gray-800 {
        --tw-text-opacity: 1;
        color: rgb(31 41 55 / var(--tw-text-opacity));
        }

        .text-red-500 {
        --tw-text-opacity: 1;
        color: rgb(239 68 68 / var(--tw-text-opacity));
        }

        .text-green-600 {
        --tw-text-opacity: 1;
        color: rgb(22 163 74 / var(--tw-text-opacity));
        }

        .text-green-500 {
        --tw-text-opacity: 1;
        color: rgb(34 197 94 / var(--tw-text-opacity));
        }

        .text-kamtuu-primary {
        --tw-text-opacity: 1;
        color: rgb(158 61 100 / var(--tw-text-opacity));
        }

        .text-\[\#9E3D64\] {
        --tw-text-opacity: 1;
        color: rgb(158 61 100 / var(--tw-text-opacity));
        }

        .text-\[\#000\] {
        --tw-text-opacity: 1;
        color: rgb(0 0 0 / var(--tw-text-opacity));
        }

        .text-slate-500 {
        --tw-text-opacity: 1;
        color: rgb(100 116 139 / var(--tw-text-opacity));
        }

        .text-\[\#D50006\] {
        --tw-text-opacity: 1;
        color: rgb(213 0 6 / var(--tw-text-opacity));
        }

        .text-\[\#333333\] {
        --tw-text-opacity: 1;
        color: rgb(51 51 51 / var(--tw-text-opacity));
        }

        .text-\[\#FFFFFF\] {
        --tw-text-opacity: 1;
        color: rgb(255 255 255 / var(--tw-text-opacity));
        }

        .text-\[\#000000\] {
        --tw-text-opacity: 1;
        color: rgb(0 0 0 / var(--tw-text-opacity));
        }

        .text-\[\#23AEC1\] {
        --tw-text-opacity: 1;
        color: rgb(35 174 193 / var(--tw-text-opacity));
        }

        .text-\[\#858585\] {
        --tw-text-opacity: 1;
        color: rgb(133 133 133 / var(--tw-text-opacity));
        }

        .text-\[\#4F4F4F\] {
        --tw-text-opacity: 1;
        color: rgb(79 79 79 / var(--tw-text-opacity));
        }

        .text-black {
        --tw-text-opacity: 1;
        color: rgb(0 0 0 / var(--tw-text-opacity));
        }

        .text-blue-100 {
        --tw-text-opacity: 1;
        color: rgb(219 234 254 / var(--tw-text-opacity));
        }

        .text-blue-600 {
        --tw-text-opacity: 1;
        color: rgb(37 99 235 / var(--tw-text-opacity));
        }

        .text-\[\#5f5e5e\] {
        --tw-text-opacity: 1;
        color: rgb(95 94 94 / var(--tw-text-opacity));
        }

        .text-sky-400 {
        --tw-text-opacity: 1;
        color: rgb(56 189 248 / var(--tw-text-opacity));
        }

        .text-slate-300 {
        --tw-text-opacity: 1;
        color: rgb(203 213 225 / var(--tw-text-opacity));
        }

        .text-slate-400 {
        --tw-text-opacity: 1;
        color: rgb(148 163 184 / var(--tw-text-opacity));
        }

        .text-\[\#ffffff\] {
        --tw-text-opacity: 1;
        color: rgb(255 255 255 / var(--tw-text-opacity));
        }

        .text-kamtuu-third {
        --tw-text-opacity: 1;
        color: rgb(255 184 0 / var(--tw-text-opacity));
        }

        .text-yellow-600 {
        --tw-text-opacity: 1;
        color: rgb(202 138 4 / var(--tw-text-opacity));
        }

        .text-blue-500 {
        --tw-text-opacity: 1;
        color: rgb(59 130 246 / var(--tw-text-opacity));
        }

        .text-\[\#333\] {
        --tw-text-opacity: 1;
        color: rgb(51 51 51 / var(--tw-text-opacity));
        }

        .text-\[\#828282\] {
        --tw-text-opacity: 1;
        color: rgb(130 130 130 / var(--tw-text-opacity));
        }

        .text-\[\#BDBDBD\] {
        --tw-text-opacity: 1;
        color: rgb(189 189 189 / var(--tw-text-opacity));
        }

        .text-rose-900 {
        --tw-text-opacity: 1;
        color: rgb(136 19 55 / var(--tw-text-opacity));
        }

        .text-\[\#27BED3\] {
        --tw-text-opacity: 1;
        color: rgb(39 190 211 / var(--tw-text-opacity));
        }

        .text-\[\#FFB800\] {
        --tw-text-opacity: 1;
        color: rgb(255 184 0 / var(--tw-text-opacity));
        }

        .text-\[\#1B96A7\] {
        --tw-text-opacity: 1;
        color: rgb(27 150 167 / var(--tw-text-opacity));
        }

        .text-blue-700 {
        --tw-text-opacity: 1;
        color: rgb(29 78 216 / var(--tw-text-opacity));
        }

        .text-slate-700 {
        --tw-text-opacity: 1;
        color: rgb(51 65 85 / var(--tw-text-opacity));
        }

        .text-kamtuu-second {
        --tw-text-opacity: 1;
        color: rgb(35 174 193 / var(--tw-text-opacity));
        }

        .text-gray-200 {
        --tw-text-opacity: 1;
        color: rgb(229 231 235 / var(--tw-text-opacity));
        }

        .text-gray-300 {
        --tw-text-opacity: 1;
        color: rgb(209 213 219 / var(--tw-text-opacity));
        }

        .text-\[\#51B449\] {
        --tw-text-opacity: 1;
        color: rgb(81 180 73 / var(--tw-text-opacity));
        }

        .text-red-900 {
        --tw-text-opacity: 1;
        color: rgb(127 29 29 / var(--tw-text-opacity));
        }

        .text-red-700 {
        --tw-text-opacity: 1;
        color: rgb(185 28 28 / var(--tw-text-opacity));
        }

        .text-\[\#f47070\] {
        --tw-text-opacity: 1;
        color: rgb(244 112 112 / var(--tw-text-opacity));
        }

        .text-cyan-600 {
        --tw-text-opacity: 1;
        color: rgb(8 145 178 / var(--tw-text-opacity));
        }

        .text-teal-500 {
        --tw-text-opacity: 1;
        color: rgb(20 184 166 / var(--tw-text-opacity));
        }

        .text-\[\#cce732\] {
        --tw-text-opacity: 1;
        color: rgb(204 231 50 / var(--tw-text-opacity));
        }

        .text-\[\#fa2c2c\] {
        --tw-text-opacity: 1;
        color: rgb(250 44 44 / var(--tw-text-opacity));
        }

        .text-yellow-100 {
        --tw-text-opacity: 1;
        color: rgb(254 249 195 / var(--tw-text-opacity));
        }

        .text-gray-50 {
        --tw-text-opacity: 1;
        color: rgb(249 250 251 / var(--tw-text-opacity));
        }

        .text-gray-100 {
        --tw-text-opacity: 1;
        color: rgb(243 244 246 / var(--tw-text-opacity));
        }

        .text-\[\#F2F2F2\] {
        --tw-text-opacity: 1;
        color: rgb(242 242 242 / var(--tw-text-opacity));
        }

        .text-red-800 {
        --tw-text-opacity: 1;
        color: rgb(153 27 27 / var(--tw-text-opacity));
        }

        .text-\[\#fff\] {
        --tw-text-opacity: 1;
        color: rgb(255 255 255 / var(--tw-text-opacity));
        }

        .text-green-700 {
        --tw-text-opacity: 1;
        color: rgb(21 128 61 / var(--tw-text-opacity));
        }

        .underline {
        text-decoration-line: underline;
        }

        .line-through {
        text-decoration-line: line-through;
        }

        .no-underline {
        text-decoration-line: none;
        }

        .antialiased {
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        }

        .placeholder-gray-400::-moz-placeholder {
        --tw-placeholder-opacity: 1;
        color: rgb(156 163 175 / var(--tw-placeholder-opacity));
        }

        .placeholder-gray-400::placeholder {
        --tw-placeholder-opacity: 1;
        color: rgb(156 163 175 / var(--tw-placeholder-opacity));
        }

        .placeholder-black::-moz-placeholder {
        --tw-placeholder-opacity: 1;
        color: rgb(0 0 0 / var(--tw-placeholder-opacity));
        }

        .placeholder-black::placeholder {
        --tw-placeholder-opacity: 1;
        color: rgb(0 0 0 / var(--tw-placeholder-opacity));
        }

        .placeholder-gray-500::-moz-placeholder {
        --tw-placeholder-opacity: 1;
        color: rgb(107 114 128 / var(--tw-placeholder-opacity));
        }

        .placeholder-gray-500::placeholder {
        --tw-placeholder-opacity: 1;
        color: rgb(107 114 128 / var(--tw-placeholder-opacity));
        }

        .placeholder-\[\#333333\]::-moz-placeholder {
        --tw-placeholder-opacity: 1;
        color: rgb(51 51 51 / var(--tw-placeholder-opacity));
        }

        .placeholder-\[\#333333\]::placeholder {
        --tw-placeholder-opacity: 1;
        color: rgb(51 51 51 / var(--tw-placeholder-opacity));
        }

        .placeholder-gray-600::-moz-placeholder {
        --tw-placeholder-opacity: 1;
        color: rgb(75 85 99 / var(--tw-placeholder-opacity));
        }

        .placeholder-gray-600::placeholder {
        --tw-placeholder-opacity: 1;
        color: rgb(75 85 99 / var(--tw-placeholder-opacity));
        }

        .opacity-0 {
        opacity: 0;
        }

        .opacity-100 {
        opacity: 1;
        }

        .opacity-75 {
        opacity: 0.75;
        }

        .opacity-50 {
        opacity: 0.5;
        }

        .opacity-70 {
        opacity: 0.7;
        }

        .opacity-5 {
        opacity: 0.05;
        }

        .opacity-10 {
        opacity: 0.1;
        }

        .opacity-30 {
        opacity: 0.3;
        }

        .shadow-sm {
        --tw-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);
        --tw-shadow-colored: 0 1px 2px 0 var(--tw-shadow-color);
        box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
        }

        .shadow {
        --tw-shadow: 0 1px 3px 0 rgb(0 0 0 / 0.1), 0 1px 2px -1px rgb(0 0 0 / 0.1);
        --tw-shadow-colored: 0 1px 3px 0 var(--tw-shadow-color), 0 1px 2px -1px var(--tw-shadow-color);
        box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
        }

        .shadow-md {
        --tw-shadow: 0 4px 6px -1px rgb(0 0 0 / 0.1), 0 2px 4px -2px rgb(0 0 0 / 0.1);
        --tw-shadow-colored: 0 4px 6px -1px var(--tw-shadow-color), 0 2px 4px -2px var(--tw-shadow-color);
        box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
        }

        .shadow-lg {
        --tw-shadow: 0 10px 15px -3px rgb(0 0 0 / 0.1), 0 4px 6px -4px rgb(0 0 0 / 0.1);
        --tw-shadow-colored: 0 10px 15px -3px var(--tw-shadow-color), 0 4px 6px -4px var(--tw-shadow-color);
        box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
        }

        .shadow-xl {
        --tw-shadow: 0 20px 25px -5px rgb(0 0 0 / 0.1), 0 8px 10px -6px rgb(0 0 0 / 0.1);
        --tw-shadow-colored: 0 20px 25px -5px var(--tw-shadow-color), 0 8px 10px -6px var(--tw-shadow-color);
        box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
        }

        .shadow-none {
        --tw-shadow: 0 0 #0000;
        --tw-shadow-colored: 0 0 #0000;
        box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
        }

        .shadow-2xl {
        --tw-shadow: 0 25px 50px -12px rgb(0 0 0 / 0.25);
        --tw-shadow-colored: 0 25px 50px -12px var(--tw-shadow-color);
        box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
        }

        .outline-none {
        outline: 2px solid transparent;
        outline-offset: 2px;
        }

        .outline {
        outline-style: solid;
        }

        .ring-1 {
        --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);
        --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(1px + var(--tw-ring-offset-width)) var(--tw-ring-color);
        box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 #0000);
        }

        .ring-gray-300 {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(209 213 219 / var(--tw-ring-opacity));
        }

        .ring-black {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(0 0 0 / var(--tw-ring-opacity));
        }

        .ring-kamtuu-second {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(35 174 193 / var(--tw-ring-opacity));
        }

        .ring-gray-500 {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(107 114 128 / var(--tw-ring-opacity));
        }

        .ring-opacity-5 {
        --tw-ring-opacity: 0.05;
        }

        .drop-shadow-xl {
        --tw-drop-shadow: drop-shadow(0 20px 13px rgb(0 0 0 / 0.03)) drop-shadow(0 8px 5px rgb(0 0 0 / 0.08));
        filter: var(--tw-blur) var(--tw-brightness) var(--tw-contrast) var(--tw-grayscale) var(--tw-hue-rotate) var(--tw-invert) var(--tw-saturate) var(--tw-sepia) var(--tw-drop-shadow);
        }

        .drop-shadow-2xl {
        --tw-drop-shadow: drop-shadow(0 25px 25px rgb(0 0 0 / 0.15));
        filter: var(--tw-blur) var(--tw-brightness) var(--tw-contrast) var(--tw-grayscale) var(--tw-hue-rotate) var(--tw-invert) var(--tw-saturate) var(--tw-sepia) var(--tw-drop-shadow);
        }

        .drop-shadow-md {
        --tw-drop-shadow: drop-shadow(0 4px 3px rgb(0 0 0 / 0.07)) drop-shadow(0 2px 2px rgb(0 0 0 / 0.06));
        filter: var(--tw-blur) var(--tw-brightness) var(--tw-contrast) var(--tw-grayscale) var(--tw-hue-rotate) var(--tw-invert) var(--tw-saturate) var(--tw-sepia) var(--tw-drop-shadow);
        }

        .drop-shadow {
        --tw-drop-shadow: drop-shadow(0 1px 2px rgb(0 0 0 / 0.1)) drop-shadow(0 1px 1px rgb(0 0 0 / 0.06));
        filter: var(--tw-blur) var(--tw-brightness) var(--tw-contrast) var(--tw-grayscale) var(--tw-hue-rotate) var(--tw-invert) var(--tw-saturate) var(--tw-sepia) var(--tw-drop-shadow);
        }

        .grayscale {
        --tw-grayscale: grayscale(100%);
        filter: var(--tw-blur) var(--tw-brightness) var(--tw-contrast) var(--tw-grayscale) var(--tw-hue-rotate) var(--tw-invert) var(--tw-saturate) var(--tw-sepia) var(--tw-drop-shadow);
        }

        .invert {
        --tw-invert: invert(100%);
        filter: var(--tw-blur) var(--tw-brightness) var(--tw-contrast) var(--tw-grayscale) var(--tw-hue-rotate) var(--tw-invert) var(--tw-saturate) var(--tw-sepia) var(--tw-drop-shadow);
        }

        .filter {
        filter: var(--tw-blur) var(--tw-brightness) var(--tw-contrast) var(--tw-grayscale) var(--tw-hue-rotate) var(--tw-invert) var(--tw-saturate) var(--tw-sepia) var(--tw-drop-shadow);
        }

        .transition {
        transition-property: color, background-color, border-color, text-decoration-color, fill, stroke, opacity, box-shadow, transform, filter, -webkit-backdrop-filter;
        transition-property: color, background-color, border-color, text-decoration-color, fill, stroke, opacity, box-shadow, transform, filter, backdrop-filter;
        transition-property: color, background-color, border-color, text-decoration-color, fill, stroke, opacity, box-shadow, transform, filter, backdrop-filter, -webkit-backdrop-filter;
        transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
        transition-duration: 150ms;
        }

        .transition-all {
        transition-property: all;
        transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
        transition-duration: 150ms;
        }

        .transition-opacity {
        transition-property: opacity;
        transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
        transition-duration: 150ms;
        }

        .delay-100 {
        transition-delay: 100ms;
        }

        .delay-500 {
        transition-delay: 500ms;
        }

        .duration-150 {
        transition-duration: 150ms;
        }

        .duration-200 {
        transition-duration: 200ms;
        }

        .duration-75 {
        transition-duration: 75ms;
        }

        .duration-300 {
        transition-duration: 300ms;
        }

        .duration-1000 {
        transition-duration: 1000ms;
        }

        .duration-500 {
        transition-duration: 500ms;
        }

        .duration-700 {
        transition-duration: 700ms;
        }

        .ease-in-out {
        transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
        }

        .ease-out {
        transition-timing-function: cubic-bezier(0, 0, 0.2, 1);
        }

        .ease-in {
        transition-timing-function: cubic-bezier(0.4, 0, 1, 1);
        }

        .line-clamp-2 {
        overflow: hidden;
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 2;
        }

        .file\:mr-4::file-selector-button {
        margin-right: 1rem;
        }

        .file\:rounded-full::file-selector-button {
        border-radius: 9999px;
        }

        .file\:border-0::file-selector-button {
        border-width: 0px;
        }

        .file\:bg-\[\#9E3D64\]::file-selector-button {
        --tw-bg-opacity: 1;
        background-color: rgb(158 61 100 / var(--tw-bg-opacity));
        }

        .file\:bg-violet-50::file-selector-button {
        --tw-bg-opacity: 1;
        background-color: rgb(245 243 255 / var(--tw-bg-opacity));
        }

        .file\:py-2::file-selector-button {
        padding-top: 0.5rem;
        padding-bottom: 0.5rem;
        }

        .file\:px-4::file-selector-button {
        padding-left: 1rem;
        padding-right: 1rem;
        }

        .file\:text-sm::file-selector-button {
        font-size: 0.875rem;
        line-height: 1.25rem;
        }

        .file\:font-semibold::file-selector-button {
        font-weight: 600;
        }

        .file\:text-white::file-selector-button {
        --tw-text-opacity: 1;
        color: rgb(255 255 255 / var(--tw-text-opacity));
        }

        .file\:text-violet-700::file-selector-button {
        --tw-text-opacity: 1;
        color: rgb(109 40 217 / var(--tw-text-opacity));
        }

        .placeholder\:italic::-moz-placeholder {
        font-style: italic;
        }

        .placeholder\:italic::placeholder {
        font-style: italic;
        }

        .placeholder\:text-\[\#BDBDBD\]::-moz-placeholder {
        --tw-text-opacity: 1;
        color: rgb(189 189 189 / var(--tw-text-opacity));
        }

        .placeholder\:text-\[\#BDBDBD\]::placeholder {
        --tw-text-opacity: 1;
        color: rgb(189 189 189 / var(--tw-text-opacity));
        }

        .placeholder\:text-slate-400::-moz-placeholder {
        --tw-text-opacity: 1;
        color: rgb(148 163 184 / var(--tw-text-opacity));
        }

        .placeholder\:text-slate-400::placeholder {
        --tw-text-opacity: 1;
        color: rgb(148 163 184 / var(--tw-text-opacity));
        }

        .first\:border-t-0:first-child {
        border-top-width: 0px;
        }

        .checked\:rounded-sm:checked {
        border-radius: 0.125rem;
        }

        .checked\:border-blue-600:checked {
        --tw-border-opacity: 1;
        border-color: rgb(37 99 235 / var(--tw-border-opacity));
        }

        .checked\:bg-blue-600:checked {
        --tw-bg-opacity: 1;
        background-color: rgb(37 99 235 / var(--tw-bg-opacity));
        }

        .checked\:bg-\[\#27BED3\]:checked {
        --tw-bg-opacity: 1;
        background-color: rgb(39 190 211 / var(--tw-bg-opacity));
        }

        .checked\:bg-kamtuu-second:checked {
        --tw-bg-opacity: 1;
        background-color: rgb(35 174 193 / var(--tw-bg-opacity));
        }

        .checked\:bg-green-600:checked {
        --tw-bg-opacity: 1;
        background-color: rgb(22 163 74 / var(--tw-bg-opacity));
        }

        .checked\:bg-red-600:checked {
        --tw-bg-opacity: 1;
        background-color: rgb(220 38 38 / var(--tw-bg-opacity));
        }

        .checked\:bg-blue-500:checked {
        --tw-bg-opacity: 1;
        background-color: rgb(59 130 246 / var(--tw-bg-opacity));
        }

        .checked\:ring-green-600:checked {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(22 163 74 / var(--tw-ring-opacity));
        }

        .checked\:ring-red-600:checked {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(220 38 38 / var(--tw-ring-opacity));
        }

        .focus-within\:border-blue-500:focus-within {
        --tw-border-opacity: 1;
        border-color: rgb(59 130 246 / var(--tw-border-opacity));
        }

        .focus-within\:bg-blue-500:focus-within {
        --tw-bg-opacity: 1;
        background-color: rgb(59 130 246 / var(--tw-bg-opacity));
        }

        .focus-within\:text-white:focus-within {
        --tw-text-opacity: 1;
        color: rgb(255 255 255 / var(--tw-text-opacity));
        }

        .hover\:-translate-x-1:hover {
        --tw-translate-x: -0.25rem;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .hover\:translate-x-1:hover {
        --tw-translate-x: 0.25rem;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .hover\:-translate-y-2:hover {
        --tw-translate-y: -0.5rem;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .hover\:scale-125:hover {
        --tw-scale-x: 1.25;
        --tw-scale-y: 1.25;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .hover\:scale-110:hover {
        --tw-scale-x: 1.1;
        --tw-scale-y: 1.1;
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .hover\:transform:hover {
        transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .hover\:rounded-full:hover {
        border-radius: 9999px;
        }

        .hover\:border:hover {
        border-width: 1px;
        }

        .hover\:border-l-4:hover {
        border-left-width: 4px;
        }

        .hover\:border-gray-300:hover {
        --tw-border-opacity: 1;
        border-color: rgb(209 213 219 / var(--tw-border-opacity));
        }

        .hover\:border-\[\#FFB800\]:hover {
        --tw-border-opacity: 1;
        border-color: rgb(255 184 0 / var(--tw-border-opacity));
        }

        .hover\:border-\[\#23AEC1\]:hover {
        --tw-border-opacity: 1;
        border-color: rgb(35 174 193 / var(--tw-border-opacity));
        }

        .hover\:border-blue-400:hover {
        --tw-border-opacity: 1;
        border-color: rgb(96 165 250 / var(--tw-border-opacity));
        }

        .hover\:border-white:hover {
        --tw-border-opacity: 1;
        border-color: rgb(255 255 255 / var(--tw-border-opacity));
        }

        .hover\:border-transparent:hover {
        border-color: transparent;
        }

        .hover\:border-gray-400:hover {
        --tw-border-opacity: 1;
        border-color: rgb(156 163 175 / var(--tw-border-opacity));
        }

        .hover\:bg-indigo-600:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(79 70 229 / var(--tw-bg-opacity));
        }

        .hover\:bg-red-600:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(220 38 38 / var(--tw-bg-opacity));
        }

        .hover\:bg-gray-700:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(55 65 81 / var(--tw-bg-opacity));
        }

        .hover\:bg-red-500:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(239 68 68 / var(--tw-bg-opacity));
        }

        .hover\:bg-gray-100:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(243 244 246 / var(--tw-bg-opacity));
        }

        .hover\:bg-gray-50:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(249 250 251 / var(--tw-bg-opacity));
        }

        .hover\:bg-kamtuu-primary:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(158 61 100 / var(--tw-bg-opacity));
        }

        .hover\:bg-\[\#872F52\]:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(135 47 82 / var(--tw-bg-opacity));
        }

        .hover\:bg-\[\#23AEC1\]:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(35 174 193 / var(--tw-bg-opacity));
        }

        .hover\:bg-\[\#1A95A6\]:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(26 149 166 / var(--tw-bg-opacity));
        }

        .hover\:bg-kamtuu-third:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(255 184 0 / var(--tw-bg-opacity));
        }

        .hover\:bg-blue-600:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(37 99 235 / var(--tw-bg-opacity));
        }

        .hover\:bg-white:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(255 255 255 / var(--tw-bg-opacity));
        }

        .hover\:bg-\[\#D50006\]:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(213 0 6 / var(--tw-bg-opacity));
        }

        .hover\:bg-slate-100:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(241 245 249 / var(--tw-bg-opacity));
        }

        .hover\:bg-blue-800:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(30 64 175 / var(--tw-bg-opacity));
        }

        .hover\:bg-yellow-600:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(202 138 4 / var(--tw-bg-opacity));
        }

        .hover\:bg-\[\#de252b\]:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(222 37 43 / var(--tw-bg-opacity));
        }

        .hover\:bg-\[\#ffcc4c\]:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(255 204 76 / var(--tw-bg-opacity));
        }

        .hover\:bg-gray-200:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(229 231 235 / var(--tw-bg-opacity));
        }

        .hover\:bg-\[\#21bbd0\]:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(33 187 208 / var(--tw-bg-opacity));
        }

        .hover\:bg-\[\#1C97A8\]:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(28 151 168 / var(--tw-bg-opacity));
        }

        .hover\:bg-\[\#b94875\]:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(185 72 117 / var(--tw-bg-opacity));
        }

        .hover\:bg-\[\#9E3D35\]:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(158 61 53 / var(--tw-bg-opacity));
        }

        .hover\:bg-\[\#3ac0d2\]:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(58 192 210 / var(--tw-bg-opacity));
        }

        .hover\:bg-violet-100:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(237 233 254 / var(--tw-bg-opacity));
        }

        .hover\:bg-\[\#efaac6\]:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(239 170 198 / var(--tw-bg-opacity));
        }

        .hover\:bg-\[\#51B449\]:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(81 180 73 / var(--tw-bg-opacity));
        }

        .hover\:bg-red-700:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(185 28 28 / var(--tw-bg-opacity));
        }

        .hover\:bg-indigo-400:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(129 140 248 / var(--tw-bg-opacity));
        }

        .hover\:bg-gray-900:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(17 24 39 / var(--tw-bg-opacity));
        }

        .hover\:bg-blue-400:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(96 165 250 / var(--tw-bg-opacity));
        }

        .hover\:bg-\[\#1FBBD0\]:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(31 187 208 / var(--tw-bg-opacity));
        }

        .hover\:bg-slate-300:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(203 213 225 / var(--tw-bg-opacity));
        }

        .hover\:bg-kamtuu-second:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(35 174 193 / var(--tw-bg-opacity));
        }

        .hover\:bg-indigo-700:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(67 56 202 / var(--tw-bg-opacity));
        }

        .hover\:fill-red-500:hover {
        fill: #ef4444;
        }

        .hover\:fill-black:hover {
        fill: #000;
        }

        .hover\:font-bold:hover {
        font-weight: 700;
        }

        .hover\:text-gray-500:hover {
        --tw-text-opacity: 1;
        color: rgb(107 114 128 / var(--tw-text-opacity));
        }

        .hover\:text-gray-400:hover {
        --tw-text-opacity: 1;
        color: rgb(156 163 175 / var(--tw-text-opacity));
        }

        .hover\:text-gray-700:hover {
        --tw-text-opacity: 1;
        color: rgb(55 65 81 / var(--tw-text-opacity));
        }

        .hover\:text-gray-800:hover {
        --tw-text-opacity: 1;
        color: rgb(31 41 55 / var(--tw-text-opacity));
        }

        .hover\:text-gray-900:hover {
        --tw-text-opacity: 1;
        color: rgb(17 24 39 / var(--tw-text-opacity));
        }

        .hover\:text-white:hover {
        --tw-text-opacity: 1;
        color: rgb(255 255 255 / var(--tw-text-opacity));
        }

        .hover\:text-\[\#FFB800\]:hover {
        --tw-text-opacity: 1;
        color: rgb(255 184 0 / var(--tw-text-opacity));
        }

        .hover\:text-\[\#FFFFFF\]:hover {
        --tw-text-opacity: 1;
        color: rgb(255 255 255 / var(--tw-text-opacity));
        }

        .hover\:text-\[\#23AEC1\]:hover {
        --tw-text-opacity: 1;
        color: rgb(35 174 193 / var(--tw-text-opacity));
        }

        .hover\:text-\[\#9E3D64\]:hover {
        --tw-text-opacity: 1;
        color: rgb(158 61 100 / var(--tw-text-opacity));
        }

        .hover\:text-kamtuu-second:hover {
        --tw-text-opacity: 1;
        color: rgb(35 174 193 / var(--tw-text-opacity));
        }

        .hover\:text-\[\#D50006\]:hover {
        --tw-text-opacity: 1;
        color: rgb(213 0 6 / var(--tw-text-opacity));
        }

        .hover\:text-gray-600:hover {
        --tw-text-opacity: 1;
        color: rgb(75 85 99 / var(--tw-text-opacity));
        }

        .hover\:text-indigo-400:hover {
        --tw-text-opacity: 1;
        color: rgb(129 140 248 / var(--tw-text-opacity));
        }

        .hover\:text-blue-400:hover {
        --tw-text-opacity: 1;
        color: rgb(96 165 250 / var(--tw-text-opacity));
        }

        .hover\:text-indigo-500:hover {
        --tw-text-opacity: 1;
        color: rgb(99 102 241 / var(--tw-text-opacity));
        }

        .hover\:text-kamtuu-primary:hover {
        --tw-text-opacity: 1;
        color: rgb(158 61 100 / var(--tw-text-opacity));
        }

        .hover\:underline:hover {
        text-decoration-line: underline;
        }

        .hover\:underline-offset-8:hover {
        text-underline-offset: 8px;
        }

        .hover\:opacity-70:hover {
        opacity: 0.7;
        }

        .hover\:shadow-md:hover {
        --tw-shadow: 0 4px 6px -1px rgb(0 0 0 / 0.1), 0 2px 4px -2px rgb(0 0 0 / 0.1);
        --tw-shadow-colored: 0 4px 6px -1px var(--tw-shadow-color), 0 2px 4px -2px var(--tw-shadow-color);
        box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
        }

        .hover\:shadow-xl:hover {
        --tw-shadow: 0 20px 25px -5px rgb(0 0 0 / 0.1), 0 8px 10px -6px rgb(0 0 0 / 0.1);
        --tw-shadow-colored: 0 20px 25px -5px var(--tw-shadow-color), 0 8px 10px -6px var(--tw-shadow-color);
        box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
        }

        .hover\:shadow-lg:hover {
        --tw-shadow: 0 10px 15px -3px rgb(0 0 0 / 0.1), 0 4px 6px -4px rgb(0 0 0 / 0.1);
        --tw-shadow-colored: 0 10px 15px -3px var(--tw-shadow-color), 0 4px 6px -4px var(--tw-shadow-color);
        box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
        }

        .hover\:shadow-2xl:hover {
        --tw-shadow: 0 25px 50px -12px rgb(0 0 0 / 0.25);
        --tw-shadow-colored: 0 25px 50px -12px var(--tw-shadow-color);
        box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
        }

        .hover\:transition-transform:hover {
        transition-property: transform;
        transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
        transition-duration: 150ms;
        }

        .hover\:duration-500:hover {
        transition-duration: 500ms;
        }

        .hover\:ease-out:hover {
        transition-timing-function: cubic-bezier(0, 0, 0.2, 1);
        }

        .hover\:file\:bg-violet-100::file-selector-button:hover {
        --tw-bg-opacity: 1;
        background-color: rgb(237 233 254 / var(--tw-bg-opacity));
        }

        .hover\:file\:text-\[\#9E3D64\]::file-selector-button:hover {
        --tw-text-opacity: 1;
        color: rgb(158 61 100 / var(--tw-text-opacity));
        }

        .focus\:z-10:focus {
        z-index: 10;
        }

        .focus\:border-2:focus {
        border-width: 2px;
        }

        .focus\:border-blue-300:focus {
        --tw-border-opacity: 1;
        border-color: rgb(147 197 253 / var(--tw-border-opacity));
        }

        .focus\:border-gray-900:focus {
        --tw-border-opacity: 1;
        border-color: rgb(17 24 39 / var(--tw-border-opacity));
        }

        .focus\:border-indigo-300:focus {
        --tw-border-opacity: 1;
        border-color: rgb(165 180 252 / var(--tw-border-opacity));
        }

        .focus\:border-red-700:focus {
        --tw-border-opacity: 1;
        border-color: rgb(185 28 28 / var(--tw-border-opacity));
        }

        .focus\:border-indigo-700:focus {
        --tw-border-opacity: 1;
        border-color: rgb(67 56 202 / var(--tw-border-opacity));
        }

        .focus\:border-gray-300:focus {
        --tw-border-opacity: 1;
        border-color: rgb(209 213 219 / var(--tw-border-opacity));
        }

        .focus\:border-\[\#23AEC1\]:focus {
        --tw-border-opacity: 1;
        border-color: rgb(35 174 193 / var(--tw-border-opacity));
        }

        .focus\:border-sky-500:focus {
        --tw-border-opacity: 1;
        border-color: rgb(14 165 233 / var(--tw-border-opacity));
        }

        .focus\:border-blue-500:focus {
        --tw-border-opacity: 1;
        border-color: rgb(59 130 246 / var(--tw-border-opacity));
        }

        .focus\:border-transparent:focus {
        border-color: transparent;
        }

        .focus\:border-kamtuu-primary:focus {
        --tw-border-opacity: 1;
        border-color: rgb(158 61 100 / var(--tw-border-opacity));
        }

        .focus\:border-blue-600:focus {
        --tw-border-opacity: 1;
        border-color: rgb(37 99 235 / var(--tw-border-opacity));
        }

        .focus\:border-\[\#9E3D64\]:focus {
        --tw-border-opacity: 1;
        border-color: rgb(158 61 100 / var(--tw-border-opacity));
        }

        .focus\:border-\[\#4F4F4F\]:focus {
        --tw-border-opacity: 1;
        border-color: rgb(79 79 79 / var(--tw-border-opacity));
        }

        .focus\:border-kamtuu-second:focus {
        --tw-border-opacity: 1;
        border-color: rgb(35 174 193 / var(--tw-border-opacity));
        }

        .focus\:border-red-500:focus {
        --tw-border-opacity: 1;
        border-color: rgb(239 68 68 / var(--tw-border-opacity));
        }

        .focus\:border-gray-600:focus {
        --tw-border-opacity: 1;
        border-color: rgb(75 85 99 / var(--tw-border-opacity));
        }

        .focus\:bg-indigo-600:focus {
        --tw-bg-opacity: 1;
        background-color: rgb(79 70 229 / var(--tw-bg-opacity));
        }

        .focus\:bg-red-600:focus {
        --tw-bg-opacity: 1;
        background-color: rgb(220 38 38 / var(--tw-bg-opacity));
        }

        .focus\:bg-gray-100:focus {
        --tw-bg-opacity: 1;
        background-color: rgb(243 244 246 / var(--tw-bg-opacity));
        }

        .focus\:bg-indigo-100:focus {
        --tw-bg-opacity: 1;
        background-color: rgb(224 231 255 / var(--tw-bg-opacity));
        }

        .focus\:bg-gray-50:focus {
        --tw-bg-opacity: 1;
        background-color: rgb(249 250 251 / var(--tw-bg-opacity));
        }

        .focus\:bg-white:focus {
        --tw-bg-opacity: 1;
        background-color: rgb(255 255 255 / var(--tw-bg-opacity));
        }

        .focus\:bg-blue-50:focus {
        --tw-bg-opacity: 1;
        background-color: rgb(239 246 255 / var(--tw-bg-opacity));
        }

        .focus\:text-gray-700:focus {
        --tw-text-opacity: 1;
        color: rgb(55 65 81 / var(--tw-text-opacity));
        }

        .focus\:text-indigo-800:focus {
        --tw-text-opacity: 1;
        color: rgb(55 48 163 / var(--tw-text-opacity));
        }

        .focus\:text-gray-800:focus {
        --tw-text-opacity: 1;
        color: rgb(31 41 55 / var(--tw-text-opacity));
        }

        .focus\:text-gray-500:focus {
        --tw-text-opacity: 1;
        color: rgb(107 114 128 / var(--tw-text-opacity));
        }

        .focus\:text-green-500:focus {
        --tw-text-opacity: 1;
        color: rgb(34 197 94 / var(--tw-text-opacity));
        }

        .focus\:placeholder-gray-500:focus::-moz-placeholder {
        --tw-placeholder-opacity: 1;
        color: rgb(107 114 128 / var(--tw-placeholder-opacity));
        }

        .focus\:placeholder-gray-500:focus::placeholder {
        --tw-placeholder-opacity: 1;
        color: rgb(107 114 128 / var(--tw-placeholder-opacity));
        }

        .focus\:outline-none:focus {
        outline: 2px solid transparent;
        outline-offset: 2px;
        }

        .focus\:ring:focus {
        --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);
        --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(3px + var(--tw-ring-offset-width)) var(--tw-ring-color);
        box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 #0000);
        }

        .focus\:ring-1:focus {
        --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);
        --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(1px + var(--tw-ring-offset-width)) var(--tw-ring-color);
        box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 #0000);
        }

        .focus\:ring-4:focus {
        --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);
        --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(4px + var(--tw-ring-offset-width)) var(--tw-ring-color);
        box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 #0000);
        }

        .focus\:ring-0:focus {
        --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);
        --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(0px + var(--tw-ring-offset-width)) var(--tw-ring-color);
        box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 #0000);
        }

        .focus\:ring-2:focus {
        --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);
        --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(2px + var(--tw-ring-offset-width)) var(--tw-ring-color);
        box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 #0000);
        }

        .focus\:ring-gray-300:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(209 213 219 / var(--tw-ring-opacity));
        }

        .focus\:ring-indigo-200:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(199 210 254 / var(--tw-ring-opacity));
        }

        .focus\:ring-red-200:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(254 202 202 / var(--tw-ring-opacity));
        }

        .focus\:ring-blue-200:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(191 219 254 / var(--tw-ring-opacity));
        }

        .focus\:ring-\[\#23AEC1\]:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(35 174 193 / var(--tw-ring-opacity));
        }

        .focus\:ring-sky-500:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(14 165 233 / var(--tw-ring-opacity));
        }

        .focus\:ring-blue-500:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(59 130 246 / var(--tw-ring-opacity));
        }

        .focus\:ring-blue-300:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(147 197 253 / var(--tw-ring-opacity));
        }

        .focus\:ring-yellow-300:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(253 224 71 / var(--tw-ring-opacity));
        }

        .focus\:ring-red-300:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(252 165 165 / var(--tw-ring-opacity));
        }

        .focus\:ring-transparent:focus {
        --tw-ring-color: transparent;
        }

        .focus\:ring-kamtuu-primary:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(158 61 100 / var(--tw-ring-opacity));
        }

        .focus\:ring-gray-200:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(229 231 235 / var(--tw-ring-opacity));
        }

        .focus\:ring-kamtuu-second:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(35 174 193 / var(--tw-ring-opacity));
        }

        .focus\:ring-red-500:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(239 68 68 / var(--tw-ring-opacity));
        }

        .focus\:ring-\[\#9E3D64\]:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(158 61 100 / var(--tw-ring-opacity));
        }

        .focus\:ring-indigo-500:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(99 102 241 / var(--tw-ring-opacity));
        }

        .focus\:ring-gray-500:focus {
        --tw-ring-opacity: 1;
        --tw-ring-color: rgb(107 114 128 / var(--tw-ring-opacity));
        }

        .focus\:ring-opacity-50:focus {
        --tw-ring-opacity: 0.5;
        }

        .focus\:ring-offset-0:focus {
        --tw-ring-offset-width: 0px;
        }

        .focus\:ring-offset-2:focus {
        --tw-ring-offset-width: 2px;
        }

        .active\:bg-gray-100:active {
        --tw-bg-opacity: 1;
        background-color: rgb(243 244 246 / var(--tw-bg-opacity));
        }

        .active\:bg-gray-900:active {
        --tw-bg-opacity: 1;
        background-color: rgb(17 24 39 / var(--tw-bg-opacity));
        }

        .active\:bg-red-600:active {
        --tw-bg-opacity: 1;
        background-color: rgb(220 38 38 / var(--tw-bg-opacity));
        }

        .active\:bg-gray-50:active {
        --tw-bg-opacity: 1;
        background-color: rgb(249 250 251 / var(--tw-bg-opacity));
        }

        .active\:bg-blue-100:active {
        --tw-bg-opacity: 1;
        background-color: rgb(219 234 254 / var(--tw-bg-opacity));
        }

        .active\:text-gray-700:active {
        --tw-text-opacity: 1;
        color: rgb(55 65 81 / var(--tw-text-opacity));
        }

        .active\:text-gray-500:active {
        --tw-text-opacity: 1;
        color: rgb(107 114 128 / var(--tw-text-opacity));
        }

        .active\:text-gray-800:active {
        --tw-text-opacity: 1;
        color: rgb(31 41 55 / var(--tw-text-opacity));
        }

        .disabled\:opacity-25:disabled {
        opacity: 0.25;
        }

        .group:hover .group-hover\:flex {
        display: flex;
        }

        .group:hover .group-hover\:text-blue-600 {
        --tw-text-opacity: 1;
        color: rgb(37 99 235 / var(--tw-text-opacity));
        }

        .peer:checked ~ .peer-checked\:bg-\[\#23AEC1\] {
        --tw-bg-opacity: 1;
        background-color: rgb(35 174 193 / var(--tw-bg-opacity));
        }

        .peer:checked ~ .peer-checked\:font-bold {
        font-weight: 700;
        }

        .peer:checked ~ .peer-checked\:text-white {
        --tw-text-opacity: 1;
        color: rgb(255 255 255 / var(--tw-text-opacity));
        }

        @media (prefers-color-scheme: dark) {
        .dark\:border-gray-600 {
            --tw-border-opacity: 1;
            border-color: rgb(75 85 99 / var(--tw-border-opacity));
        }

        .dark\:border-gray-700 {
            --tw-border-opacity: 1;
            border-color: rgb(55 65 81 / var(--tw-border-opacity));
        }

        .dark\:border-\[\#9E3D64\] {
            --tw-border-opacity: 1;
            border-color: rgb(158 61 100 / var(--tw-border-opacity));
        }

        .dark\:border-gray-500 {
            --tw-border-opacity: 1;
            border-color: rgb(107 114 128 / var(--tw-border-opacity));
        }

        .dark\:bg-gray-700 {
            --tw-bg-opacity: 1;
            background-color: rgb(55 65 81 / var(--tw-bg-opacity));
        }

        .dark\:bg-gray-900 {
            --tw-bg-opacity: 1;
            background-color: rgb(17 24 39 / var(--tw-bg-opacity));
        }

        .dark\:bg-gray-800 {
            --tw-bg-opacity: 1;
            background-color: rgb(31 41 55 / var(--tw-bg-opacity));
        }

        .dark\:bg-blue-600 {
            --tw-bg-opacity: 1;
            background-color: rgb(37 99 235 / var(--tw-bg-opacity));
        }

        .dark\:bg-\[\#D25889\] {
            --tw-bg-opacity: 1;
            background-color: rgb(210 88 137 / var(--tw-bg-opacity));
        }

        .dark\:bg-yellow-500 {
            --tw-bg-opacity: 1;
            background-color: rgb(234 179 8 / var(--tw-bg-opacity));
        }

        .dark\:bg-red-500 {
            --tw-bg-opacity: 1;
            background-color: rgb(239 68 68 / var(--tw-bg-opacity));
        }

        .dark\:bg-\[\#9E3D64\] {
            --tw-bg-opacity: 1;
            background-color: rgb(158 61 100 / var(--tw-bg-opacity));
        }

        .dark\:bg-gray-400 {
            --tw-bg-opacity: 1;
            background-color: rgb(156 163 175 / var(--tw-bg-opacity));
        }

        .dark\:bg-gray-600 {
            --tw-bg-opacity: 1;
            background-color: rgb(75 85 99 / var(--tw-bg-opacity));
        }

        .dark\:bg-blue-500 {
            --tw-bg-opacity: 1;
            background-color: rgb(59 130 246 / var(--tw-bg-opacity));
        }

        .dark\:text-gray-300 {
            --tw-text-opacity: 1;
            color: rgb(209 213 219 / var(--tw-text-opacity));
        }

        .dark\:text-white {
            --tw-text-opacity: 1;
            color: rgb(255 255 255 / var(--tw-text-opacity));
        }

        .dark\:text-gray-400 {
            --tw-text-opacity: 1;
            color: rgb(156 163 175 / var(--tw-text-opacity));
        }

        .dark\:text-gray-500 {
            --tw-text-opacity: 1;
            color: rgb(107 114 128 / var(--tw-text-opacity));
        }

        .dark\:text-\[\#9E3D64\] {
            --tw-text-opacity: 1;
            color: rgb(158 61 100 / var(--tw-text-opacity));
        }

        .dark\:text-blue-500 {
            --tw-text-opacity: 1;
            color: rgb(59 130 246 / var(--tw-text-opacity));
        }

        .dark\:text-gray-600 {
            --tw-text-opacity: 1;
            color: rgb(75 85 99 / var(--tw-text-opacity));
        }

        .dark\:text-gray-900 {
            --tw-text-opacity: 1;
            color: rgb(17 24 39 / var(--tw-text-opacity));
        }

        .dark\:placeholder-gray-400::-moz-placeholder {
            --tw-placeholder-opacity: 1;
            color: rgb(156 163 175 / var(--tw-placeholder-opacity));
        }

        .dark\:placeholder-gray-400::placeholder {
            --tw-placeholder-opacity: 1;
            color: rgb(156 163 175 / var(--tw-placeholder-opacity));
        }

        .dark\:placeholder-gray-200::-moz-placeholder {
            --tw-placeholder-opacity: 1;
            color: rgb(229 231 235 / var(--tw-placeholder-opacity));
        }

        .dark\:placeholder-gray-200::placeholder {
            --tw-placeholder-opacity: 1;
            color: rgb(229 231 235 / var(--tw-placeholder-opacity));
        }

        .dark\:placeholder-gray-100::-moz-placeholder {
            --tw-placeholder-opacity: 1;
            color: rgb(243 244 246 / var(--tw-placeholder-opacity));
        }

        .dark\:placeholder-gray-100::placeholder {
            --tw-placeholder-opacity: 1;
            color: rgb(243 244 246 / var(--tw-placeholder-opacity));
        }

        .dark\:ring-offset-gray-800 {
            --tw-ring-offset-color: #1f2937;
        }

        .dark\:hover\:bg-gray-600:hover {
            --tw-bg-opacity: 1;
            background-color: rgb(75 85 99 / var(--tw-bg-opacity));
        }

        .dark\:hover\:bg-blue-700:hover {
            --tw-bg-opacity: 1;
            background-color: rgb(29 78 216 / var(--tw-bg-opacity));
        }

        .dark\:hover\:bg-yellow-600:hover {
            --tw-bg-opacity: 1;
            background-color: rgb(202 138 4 / var(--tw-bg-opacity));
        }

        .dark\:hover\:bg-red-600:hover {
            --tw-bg-opacity: 1;
            background-color: rgb(220 38 38 / var(--tw-bg-opacity));
        }

        .dark\:hover\:bg-gray-700:hover {
            --tw-bg-opacity: 1;
            background-color: rgb(55 65 81 / var(--tw-bg-opacity));
        }

        .dark\:hover\:bg-gray-800:hover {
            --tw-bg-opacity: 1;
            background-color: rgb(31 41 55 / var(--tw-bg-opacity));
        }

        .dark\:hover\:bg-blue-600:hover {
            --tw-bg-opacity: 1;
            background-color: rgb(37 99 235 / var(--tw-bg-opacity));
        }

        .dark\:hover\:text-gray-300:hover {
            --tw-text-opacity: 1;
            color: rgb(209 213 219 / var(--tw-text-opacity));
        }

        .dark\:focus\:border-blue-500:focus {
            --tw-border-opacity: 1;
            border-color: rgb(59 130 246 / var(--tw-border-opacity));
        }

        .dark\:focus\:bg-blue-600:focus {
            --tw-bg-opacity: 1;
            background-color: rgb(37 99 235 / var(--tw-bg-opacity));
        }

        .dark\:focus\:ring-blue-900:focus {
            --tw-ring-opacity: 1;
            --tw-ring-color: rgb(30 58 138 / var(--tw-ring-opacity));
        }

        .dark\:focus\:ring-blue-500:focus {
            --tw-ring-opacity: 1;
            --tw-ring-color: rgb(59 130 246 / var(--tw-ring-opacity));
        }

        .dark\:focus\:ring-blue-800:focus {
            --tw-ring-opacity: 1;
            --tw-ring-color: rgb(30 64 175 / var(--tw-ring-opacity));
        }

        .dark\:focus\:ring-red-800:focus {
            --tw-ring-opacity: 1;
            --tw-ring-color: rgb(153 27 27 / var(--tw-ring-opacity));
        }

        .dark\:focus\:ring-blue-600:focus {
            --tw-ring-opacity: 1;
            --tw-ring-color: rgb(37 99 235 / var(--tw-ring-opacity));
        }

        .dark\:focus\:ring-gray-800:focus {
            --tw-ring-opacity: 1;
            --tw-ring-color: rgb(31 41 55 / var(--tw-ring-opacity));
        }

        .dark\:focus\:ring-red-600:focus {
            --tw-ring-opacity: 1;
            --tw-ring-color: rgb(220 38 38 / var(--tw-ring-opacity));
        }
        }

        @media (min-width: 640px) {
        .sm\:absolute {
            position: absolute;
        }

        .sm\:relative {
            position: relative;
        }

        .sm\:top-10 {
            top: 2.5rem;
        }

        .sm\:right-10 {
            right: 2.5rem;
        }

        .sm\:bottom-0 {
            bottom: 0px;
        }

        .sm\:bottom-1\/4 {
            bottom: 25%;
        }

        .sm\:right-\[10\%\] {
            right: 10%;
        }

        .sm\:order-last {
            order: 9999;
        }

        .sm\:order-none {
            order: 0;
        }

        .sm\:col-span-4 {
            grid-column: span 4 / span 4;
        }

        .sm\:col-span-9 {
            grid-column: span 9 / span 9;
        }

        .sm\:col-span-3 {
            grid-column: span 3 / span 3;
        }

        .sm\:col-span-2 {
            grid-column: span 2 / span 2;
        }

        .sm\:float-right {
            float: right;
        }

        .sm\:m-3 {
            margin: 0.75rem;
        }

        .sm\:mx-0 {
            margin-left: 0px;
            margin-right: 0px;
        }

        .sm\:mx-auto {
            margin-left: auto;
            margin-right: auto;
        }

        .sm\:-my-px {
            margin-top: -1px;
            margin-bottom: -1px;
        }

        .sm\:-mx-8 {
            margin-left: -2rem;
            margin-right: -2rem;
        }

        .sm\:my-0 {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .sm\:my-5 {
            margin-top: 1.25rem;
            margin-bottom: 1.25rem;
        }

        .sm\:my-2 {
            margin-top: 0.5rem;
            margin-bottom: 0.5rem;
        }

        .sm\:my-3 {
            margin-top: 0.75rem;
            margin-bottom: 0.75rem;
        }

        .sm\:my-8 {
            margin-top: 2rem;
            margin-bottom: 2rem;
        }

        .sm\:ml-3 {
            margin-left: 0.75rem;
        }

        .sm\:-mr-2 {
            margin-right: -0.5rem;
        }

        .sm\:mt-0 {
            margin-top: 0px;
        }

        .sm\:ml-4 {
            margin-left: 1rem;
        }

        .sm\:ml-10 {
            margin-left: 2.5rem;
        }

        .sm\:ml-6 {
            margin-left: 1.5rem;
        }

        .sm\:mt-2 {
            margin-top: 0.5rem;
        }

        .sm\:mt-5 {
            margin-top: 1.25rem;
        }

        .sm\:mb-0 {
            margin-bottom: 0px;
        }

        .sm\:ml-0 {
            margin-left: 0px;
        }

        .sm\:mr-4 {
            margin-right: 1rem;
        }

        .sm\:mt-6 {
            margin-top: 1.5rem;
        }

        .sm\:block {
            display: block;
        }

        .sm\:flex {
            display: flex;
        }

        .sm\:grid {
            display: grid;
        }

        .sm\:hidden {
            display: none;
        }

        .sm\:h-10 {
            height: 2.5rem;
        }

        .sm\:h-8 {
            height: 2rem;
        }

        .sm\:h-\[240px\] {
            height: 240px;
        }

        .sm\:h-40 {
            height: 10rem;
        }

        .sm\:h-\[319px\] {
            height: 319px;
        }

        .sm\:h-\[97px\] {
            height: 97px;
        }

        .sm\:h-1\/3 {
            height: 33.333333%;
        }

        .sm\:h-\[75px\] {
            height: 75px;
        }

        .sm\:h-\[185px\] {
            height: 185px;
        }

        .sm\:h-\[100px\] {
            height: 100px;
        }

        .sm\:h-32 {
            height: 8rem;
        }

        .sm\:h-\[140px\] {
            height: 140px;
        }

        .sm\:h-20 {
            height: 5rem;
        }

        .sm\:h-64 {
            height: 16rem;
        }

        .sm\:h-24 {
            height: 6rem;
        }

        .sm\:w-10 {
            width: 2.5rem;
        }

        .sm\:w-full {
            width: 100%;
        }

        .sm\:w-\[255px\] {
            width: 255px;
        }

        .sm\:w-\[768px\] {
            width: 768px;
        }

        .sm\:w-1\/2 {
            width: 50%;
        }

        .sm\:\!w-full {
            width: 100% !important;
        }

        .sm\:w-1\/3 {
            width: 33.333333%;
        }

        .sm\:w-1\/4 {
            width: 25%;
        }

        .sm\:w-80 {
            width: 20rem;
        }

        .sm\:w-auto {
            width: auto;
        }

        .sm\:w-\[75px\] {
            width: 75px;
        }

        .sm\:w-\[300px\] {
            width: 300px;
        }

        .sm\:w-60 {
            width: 15rem;
        }

        .sm\:w-32 {
            width: 8rem;
        }

        .sm\:w-fit {
            width: -moz-fit-content;
            width: fit-content;
        }

        .sm\:w-\[140px\] {
            width: 140px;
        }

        .sm\:w-3\/4 {
            width: 75%;
        }

        .sm\:w-96 {
            width: 24rem;
        }

        .sm\:max-w-md {
            max-width: 28rem;
        }

        .sm\:max-w-sm {
            max-width: 24rem;
        }

        .sm\:max-w-lg {
            max-width: 32rem;
        }

        .sm\:max-w-xl {
            max-width: 36rem;
        }

        .sm\:max-w-2xl {
            max-width: 42rem;
        }

        .sm\:flex-1 {
            flex: 1 1 0%;
        }

        .sm\:flex-none {
            flex: none;
        }

        .sm\:flex-grow {
            flex-grow: 1;
        }

        .sm\:translate-y-0 {
            --tw-translate-y: 0px;
            transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .sm\:translate-y-4 {
            --tw-translate-y: 1rem;
            transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .sm\:translate-y-1\/2 {
            --tw-translate-y: 50%;
            transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .sm\:translate-x-0 {
            --tw-translate-x: 0px;
            transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .sm\:-translate-y-\[100px\] {
            --tw-translate-y: -100px;
            transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .sm\:scale-95 {
            --tw-scale-x: .95;
            --tw-scale-y: .95;
            transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .sm\:scale-100 {
            --tw-scale-x: 1;
            --tw-scale-y: 1;
            transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .sm\:grid-cols-2 {
            grid-template-columns: repeat(2, minmax(0, 1fr));
        }

        .sm\:grid-cols-12 {
            grid-template-columns: repeat(12, minmax(0, 1fr));
        }

        .sm\:grid-cols-4 {
            grid-template-columns: repeat(4, minmax(0, 1fr));
        }

        .sm\:grid-cols-3 {
            grid-template-columns: repeat(3, minmax(0, 1fr));
        }

        .sm\:grid-cols-1 {
            grid-template-columns: repeat(1, minmax(0, 1fr));
        }

        .sm\:grid-rows-none {
            grid-template-rows: none;
        }

        .sm\:grid-rows-1 {
            grid-template-rows: repeat(1, minmax(0, 1fr));
        }

        .sm\:grid-rows-2 {
            grid-template-rows: repeat(2, minmax(0, 1fr));
        }

        .sm\:flex-row {
            flex-direction: row;
        }

        .sm\:content-center {
            align-content: center;
        }

        .sm\:items-start {
            align-items: flex-start;
        }

        .sm\:items-center {
            align-items: center;
        }

        .sm\:justify-start {
            justify-content: flex-start;
        }

        .sm\:justify-end {
            justify-content: flex-end;
        }

        .sm\:justify-center {
            justify-content: center;
        }

        .sm\:justify-between {
            justify-content: space-between;
        }

        .sm\:justify-items-start {
            justify-items: start;
        }

        .sm\:justify-items-end {
            justify-items: end;
        }

        .sm\:justify-items-center {
            justify-items: center;
        }

        .sm\:gap-5 {
            gap: 1.25rem;
        }

        .sm\:space-y-0 > :not([hidden]) ~ :not([hidden]) {
            --tw-space-y-reverse: 0;
            margin-top: calc(0px * calc(1 - var(--tw-space-y-reverse)));
            margin-bottom: calc(0px * var(--tw-space-y-reverse));
        }

        .sm\:space-x-3 > :not([hidden]) ~ :not([hidden]) {
            --tw-space-x-reverse: 0;
            margin-right: calc(0.75rem * var(--tw-space-x-reverse));
            margin-left: calc(0.75rem * calc(1 - var(--tw-space-x-reverse)));
        }

        .sm\:space-x-2 > :not([hidden]) ~ :not([hidden]) {
            --tw-space-x-reverse: 0;
            margin-right: calc(0.5rem * var(--tw-space-x-reverse));
            margin-left: calc(0.5rem * calc(1 - var(--tw-space-x-reverse)));
        }

        .sm\:space-x-5 > :not([hidden]) ~ :not([hidden]) {
            --tw-space-x-reverse: 0;
            margin-right: calc(1.25rem * var(--tw-space-x-reverse));
            margin-left: calc(1.25rem * calc(1 - var(--tw-space-x-reverse)));
        }

        .sm\:rounded-lg {
            border-radius: 0.5rem;
        }

        .sm\:rounded-md {
            border-radius: 0.375rem;
        }

        .sm\:rounded-tl-md {
            border-top-left-radius: 0.375rem;
        }

        .sm\:rounded-tr-md {
            border-top-right-radius: 0.375rem;
        }

        .sm\:rounded-bl-md {
            border-bottom-left-radius: 0.375rem;
        }

        .sm\:rounded-br-md {
            border-bottom-right-radius: 0.375rem;
        }

        .sm\:border-y-0 {
            border-top-width: 0px;
            border-bottom-width: 0px;
        }

        .sm\:p-6 {
            padding: 1.5rem;
        }

        .sm\:p-5 {
            padding: 1.25rem;
        }

        .sm\:p-8 {
            padding: 2rem;
        }

        .sm\:p-0 {
            padding: 0px;
        }

        .sm\:p-3 {
            padding: 0.75rem;
        }

        .sm\:p-2 {
            padding: 0.5rem;
        }

        .sm\:p-10 {
            padding: 2.5rem;
        }

        .sm\:px-6 {
            padding-left: 1.5rem;
            padding-right: 1.5rem;
        }

        .sm\:px-0 {
            padding-left: 0px;
            padding-right: 0px;
        }

        .sm\:px-20 {
            padding-left: 5rem;
            padding-right: 5rem;
        }

        .sm\:py-2 {
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
        }

        .sm\:px-4 {
            padding-left: 1rem;
            padding-right: 1rem;
        }

        .sm\:px-8 {
            padding-left: 2rem;
            padding-right: 2rem;
        }

        .sm\:px-9 {
            padding-left: 2.25rem;
            padding-right: 2.25rem;
        }

        .sm\:py-12 {
            padding-top: 3rem;
            padding-bottom: 3rem;
        }

        .sm\:px-10 {
            padding-left: 2.5rem;
            padding-right: 2.5rem;
        }

        .sm\:px-3 {
            padding-left: 0.75rem;
            padding-right: 0.75rem;
        }

        .sm\:py-20 {
            padding-top: 5rem;
            padding-bottom: 5rem;
        }

        .sm\:py-0 {
            padding-top: 0px;
            padding-bottom: 0px;
        }

        .sm\:pt-0 {
            padding-top: 0px;
        }

        .sm\:pb-4 {
            padding-bottom: 1rem;
        }

        .sm\:pl-7 {
            padding-left: 1.75rem;
        }

        .sm\:pr-7 {
            padding-right: 1.75rem;
        }

        .sm\:pb-5 {
            padding-bottom: 1.25rem;
        }

        .sm\:pl-\[1rem\] {
            padding-left: 1rem;
        }

        .sm\:pt-5 {
            padding-top: 1.25rem;
        }

        .sm\:text-left {
            text-align: left;
        }

        .sm\:text-center {
            text-align: center;
        }

        .sm\:text-right {
            text-align: right;
        }

        .sm\:text-end {
            text-align: end;
        }

        .sm\:align-middle {
            vertical-align: middle;
        }

        .sm\:text-sm {
            font-size: 0.875rem;
            line-height: 1.25rem;
        }

        .sm\:text-base {
            font-size: 1rem;
            line-height: 1.5rem;
        }

        .sm\:text-xl {
            font-size: 1.25rem;
            line-height: 1.75rem;
        }

        .sm\:text-lg {
            font-size: 1.125rem;
            line-height: 1.75rem;
        }

        .sm\:text-3xl {
            font-size: 1.875rem;
            line-height: 2.25rem;
        }

        .sm\:text-2xl {
            font-size: 1.5rem;
            line-height: 2rem;
        }

        .sm\:text-\[14px\] {
            font-size: 14px;
        }

        .sm\:text-4xl {
            font-size: 2.25rem;
            line-height: 2.5rem;
        }

        .sm\:text-\[24px\] {
            font-size: 24px;
        }

        .sm\:leading-5 {
            line-height: 1.25rem;
        }

        .sm\:leading-10 {
            line-height: 2.5rem;
        }

        .sm\:hover\:shadow-xl:hover {
            --tw-shadow: 0 20px 25px -5px rgb(0 0 0 / 0.1), 0 8px 10px -6px rgb(0 0 0 / 0.1);
            --tw-shadow-colored: 0 20px 25px -5px var(--tw-shadow-color), 0 8px 10px -6px var(--tw-shadow-color);
            box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
        }
        }

        @media (min-width: 768px) {
        .md\:absolute {
            position: absolute;
        }

        .md\:bottom-0 {
            bottom: 0px;
        }

        .md\:right-0 {
            right: 0px;
        }

        .md\:top-\[9px\] {
            top: 9px;
        }

        .md\:col-span-2 {
            grid-column: span 2 / span 2;
        }

        .md\:col-span-1 {
            grid-column: span 1 / span 1;
        }

        .md\:m-2 {
            margin: 0.5rem;
        }

        .md\:m-3 {
            margin: 0.75rem;
        }

        .md\:m-5 {
            margin: 1.25rem;
        }

        .md\:mx-2 {
            margin-left: 0.5rem;
            margin-right: 0.5rem;
        }

        .md\:mx-0 {
            margin-left: 0px;
            margin-right: 0px;
        }

        .md\:mx-1 {
            margin-left: 0.25rem;
            margin-right: 0.25rem;
        }

        .md\:mx-2\.5 {
            margin-left: 0.625rem;
            margin-right: 0.625rem;
        }

        .md\:mt-0 {
            margin-top: 0px;
        }

        .md\:\!ml-\[6rem\] {
            margin-left: 6rem !important;
        }

        .md\:\!ml-\[9rem\] {
            margin-left: 9rem !important;
        }

        .md\:\!ml-\[0rem\] {
            margin-left: 0rem !important;
        }

        .md\:-ml-\[23rem\] {
            margin-left: -23rem;
        }

        .md\:mb-0 {
            margin-bottom: 0px;
        }

        .md\:mb-2 {
            margin-bottom: 0.5rem;
        }

        .md\:mr-16 {
            margin-right: 4rem;
        }

        .md\:ml-16 {
            margin-left: 4rem;
        }

        .md\:-ml-\[10rem\] {
            margin-left: -10rem;
        }

        .md\:ml-28 {
            margin-left: 7rem;
        }

        .md\:mt-6 {
            margin-top: 1.5rem;
        }

        .md\:ml-8 {
            margin-left: 2rem;
        }

        .md\:block {
            display: block;
        }

        .md\:flex {
            display: flex;
        }

        .md\:table-row {
            display: table-row;
        }

        .md\:grid {
            display: grid;
        }

        .md\:hidden {
            display: none;
        }

        .md\:h-\[256px\] {
            height: 256px;
        }

        .md\:h-\[287px\] {
            height: 287px;
        }

        .md\:h-\[139px\] {
            height: 139px;
        }

        .md\:h-\[383px\] {
            height: 383px;
        }

        .md\:h-\[120px\] {
            height: 120px;
        }

        .md\:h-\[125px\] {
            height: 125px;
        }

        .md\:h-\[416px\] {
            height: 416px;
        }

        .md\:h-\[75px\] {
            height: 75px;
        }

        .md\:h-\[20px\] {
            height: 20px;
        }

        .md\:h-\[3rem\] {
            height: 3rem;
        }

        .md\:h-\[380px\] {
            height: 380px;
        }

        .md\:max-h-full {
            max-height: 100%;
        }

        .md\:max-h-\[130px\] {
            max-height: 130px;
        }

        .md\:\!w-\[31rem\] {
            width: 31rem !important;
        }

        .md\:w-\[1024px\] {
            width: 1024px;
        }

        .md\:w-full {
            width: 100%;
        }

        .md\:w-40 {
            width: 10rem;
        }

        .md\:w-\[450px\] {
            width: 450px;
        }

        .md\:w-1\/2 {
            width: 50%;
        }

        .md\:w-fit {
            width: -moz-fit-content;
            width: fit-content;
        }

        .md\:w-auto {
            width: auto;
        }

        .md\:w-3\/4 {
            width: 75%;
        }

        .md\:w-\[416px\] {
            width: 416px;
        }

        .md\:w-\[75px\] {
            width: 75px;
        }

        .md\:w-\[405px\] {
            width: 405px;
        }

        .md\:w-\[20px\] {
            width: 20px;
        }

        .md\:w-20 {
            width: 5rem;
        }

        .md\:w-\[15px\] {
            width: 15px;
        }

        .md\:w-\[110px\] {
            width: 110px;
        }

        .md\:w-\[710px\] {
            width: 710px;
        }

        .md\:max-w-full {
            max-width: 100%;
        }

        .md\:max-w-2xl {
            max-width: 42rem;
        }

        .md\:-translate-x-5 {
            --tw-translate-x: -1.25rem;
            transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .md\:-translate-y-5 {
            --tw-translate-y: -1.25rem;
            transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .md\:grid-cols-3 {
            grid-template-columns: repeat(3, minmax(0, 1fr));
        }

        .md\:grid-cols-2 {
            grid-template-columns: repeat(2, minmax(0, 1fr));
        }

        .md\:grid-cols-1 {
            grid-template-columns: repeat(1, minmax(0, 1fr));
        }

        .md\:grid-cols-\[20\%_80\%\] {
            grid-template-columns: 20% 80%;
        }

        .md\:grid-cols-5 {
            grid-template-columns: repeat(5, minmax(0, 1fr));
        }

        .md\:grid-cols-4 {
            grid-template-columns: repeat(4, minmax(0, 1fr));
        }

        .md\:grid-cols-7 {
            grid-template-columns: repeat(7, minmax(0, 1fr));
        }

        .md\:grid-cols-\[5\%_15\%_60\%_20\%\] {
            grid-template-columns: 5% 15% 60% 20%;
        }

        .md\:grid-cols-\[15\%_45\%_20\%\] {
            grid-template-columns: 15% 45% 20%;
        }

        .md\:grid-cols-\[70\%_30\%\] {
            grid-template-columns: 70% 30%;
        }

        .md\:grid-cols-12 {
            grid-template-columns: repeat(12, minmax(0, 1fr));
        }

        .md\:grid-cols-\[20\%_50\%_30\%\] {
            grid-template-columns: 20% 50% 30%;
        }

        .md\:grid-rows-2 {
            grid-template-rows: repeat(2, minmax(0, 1fr));
        }

        .md\:grid-rows-5 {
            grid-template-rows: repeat(5, minmax(0, 1fr));
        }

        .md\:items-center {
            align-items: center;
        }

        .md\:justify-start {
            justify-content: flex-start;
        }

        .md\:justify-end {
            justify-content: flex-end;
        }

        .md\:justify-items-end {
            justify-items: end;
        }

        .md\:gap-6 {
            gap: 1.5rem;
        }

        .md\:gap-5 {
            gap: 1.25rem;
        }

        .md\:space-x-3 > :not([hidden]) ~ :not([hidden]) {
            --tw-space-x-reverse: 0;
            margin-right: calc(0.75rem * var(--tw-space-x-reverse));
            margin-left: calc(0.75rem * calc(1 - var(--tw-space-x-reverse)));
        }

        .md\:space-x-10 > :not([hidden]) ~ :not([hidden]) {
            --tw-space-x-reverse: 0;
            margin-right: calc(2.5rem * var(--tw-space-x-reverse));
            margin-left: calc(2.5rem * calc(1 - var(--tw-space-x-reverse)));
        }

        .md\:space-y-0 > :not([hidden]) ~ :not([hidden]) {
            --tw-space-y-reverse: 0;
            margin-top: calc(0px * calc(1 - var(--tw-space-y-reverse)));
            margin-bottom: calc(0px * var(--tw-space-y-reverse));
        }

        .md\:divide-x-2 > :not([hidden]) ~ :not([hidden]) {
            --tw-divide-x-reverse: 0;
            border-right-width: calc(2px * var(--tw-divide-x-reverse));
            border-left-width: calc(2px * calc(1 - var(--tw-divide-x-reverse)));
        }

        .md\:divide-x > :not([hidden]) ~ :not([hidden]) {
            --tw-divide-x-reverse: 0;
            border-right-width: calc(1px * var(--tw-divide-x-reverse));
            border-left-width: calc(1px * calc(1 - var(--tw-divide-x-reverse)));
        }

        .md\:rounded-r-lg {
            border-top-right-radius: 0.5rem;
            border-bottom-right-radius: 0.5rem;
        }

        .md\:rounded-l-lg {
            border-top-left-radius: 0.5rem;
            border-bottom-left-radius: 0.5rem;
        }

        .md\:border-t-0 {
            border-top-width: 0px;
        }

        .md\:border-l {
            border-left-width: 1px;
        }

        .md\:p-2 {
            padding: 0.5rem;
        }

        .md\:p-0 {
            padding: 0px;
        }

        .md\:p-3 {
            padding: 0.75rem;
        }

        .md\:p-10 {
            padding: 2.5rem;
        }

        .md\:px-3 {
            padding-left: 0.75rem;
            padding-right: 0.75rem;
        }

        .md\:py-24 {
            padding-top: 6rem;
            padding-bottom: 6rem;
        }

        .md\:py-0 {
            padding-top: 0px;
            padding-bottom: 0px;
        }

        .md\:px-5 {
            padding-left: 1.25rem;
            padding-right: 1.25rem;
        }

        .md\:py-5 {
            padding-top: 1.25rem;
            padding-bottom: 1.25rem;
        }

        .md\:pr-\[11px\] {
            padding-right: 11px;
        }

        .md\:pr-\[7px\] {
            padding-right: 7px;
        }

        .md\:pl-14 {
            padding-left: 3.5rem;
        }

        .md\:pr-10 {
            padding-right: 2.5rem;
        }

        .md\:pb-2 {
            padding-bottom: 0.5rem;
        }

        .md\:pt-0 {
            padding-top: 0px;
        }

        .md\:text-left {
            text-align: left;
        }

        .md\:text-center {
            text-align: center;
        }

        .md\:text-right {
            text-align: right;
        }

        .md\:text-start {
            text-align: start;
        }

        .md\:text-end {
            text-align: end;
        }

        .md\:text-base {
            font-size: 1rem;
            line-height: 1.5rem;
        }

        .md\:text-4xl {
            font-size: 2.25rem;
            line-height: 2.5rem;
        }

        .md\:text-5xl {
            font-size: 3rem;
            line-height: 1;
        }

        .md\:text-lg {
            font-size: 1.125rem;
            line-height: 1.75rem;
        }

        .md\:text-2xl {
            font-size: 1.5rem;
            line-height: 2rem;
        }

        .md\:text-xl {
            font-size: 1.25rem;
            line-height: 1.75rem;
        }

        .md\:text-\[18px\] {
            font-size: 18px;
        }

        .md\:text-sm {
            font-size: 0.875rem;
            line-height: 1.25rem;
        }

        .md\:text-\[16px\] {
            font-size: 16px;
        }

        .md\:hover\:-translate-y-1:hover {
            --tw-translate-y: -0.25rem;
            transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
        }

        .md\:hover\:drop-shadow-xl:hover {
            --tw-drop-shadow: drop-shadow(0 20px 13px rgb(0 0 0 / 0.03)) drop-shadow(0 8px 5px rgb(0 0 0 / 0.08));
            filter: var(--tw-blur) var(--tw-brightness) var(--tw-contrast) var(--tw-grayscale) var(--tw-hue-rotate) var(--tw-invert) var(--tw-saturate) var(--tw-sepia) var(--tw-drop-shadow);
        }
        }

        @media (min-width: 1024px) {
        .lg\:container {
            width: 100%;
        }

        @media (min-width: 640px) {
            .lg\:container {
            max-width: 640px;
            }
        }

        @media (min-width: 768px) {
            .lg\:container {
            max-width: 768px;
            }
        }

        @media (min-width: 1024px) {
            .lg\:container {
            max-width: 1024px;
            }
        }

        @media (min-width: 1280px) {
            .lg\:container {
            max-width: 1280px;
            }
        }

        @media (min-width: 1536px) {
            .lg\:container {
            max-width: 1536px;
            }
        }

        .lg\:right-\[15\%\] {
            right: 15%;
        }

        .lg\:order-none {
            order: 0;
        }

        .lg\:col-span-4 {
            grid-column: span 4 / span 4;
        }

        .lg\:col-span-2 {
            grid-column: span 2 / span 2;
        }

        .lg\:col-span-1 {
            grid-column: span 1 / span 1;
        }

        .lg\:col-span-3 {
            grid-column: span 3 / span 3;
        }

        .lg\:col-span-8 {
            grid-column: span 8 / span 8;
        }

        .lg\:col-start-3 {
            grid-column-start: 3;
        }

        .lg\:col-start-2 {
            grid-column-start: 2;
        }

        .lg\:col-start-4 {
            grid-column-start: 4;
        }

        .lg\:col-start-1 {
            grid-column-start: 1;
        }

        .lg\:col-start-7 {
            grid-column-start: 7;
        }

        .lg\:col-end-7 {
            grid-column-end: 7;
        }

        .lg\:m-5 {
            margin: 1.25rem;
        }

        .lg\:mx-auto {
            margin-left: auto;
            margin-right: auto;
        }

        .lg\:my-5 {
            margin-top: 1.25rem;
            margin-bottom: 1.25rem;
        }

        .lg\:my-10 {
            margin-top: 2.5rem;
            margin-bottom: 2.5rem;
        }

        .lg\:my-3 {
            margin-top: 0.75rem;
            margin-bottom: 0.75rem;
        }

        .lg\:mx-px {
            margin-left: 1px;
            margin-right: 1px;
        }

        .lg\:my-2 {
            margin-top: 0.5rem;
            margin-bottom: 0.5rem;
        }

        .lg\:\!ml-\[2rem\] {
            margin-left: 2rem !important;
        }

        .lg\:ml-0 {
            margin-left: 0px;
        }

        .lg\:ml-6 {
            margin-left: 1.5rem;
        }

        .lg\:mt-2\.5 {
            margin-top: 0.625rem;
        }

        .lg\:mt-2 {
            margin-top: 0.5rem;
        }

        .lg\:mt-3 {
            margin-top: 0.75rem;
        }

        .lg\:-ml-\[15rem\] {
            margin-left: -15rem;
        }

        .lg\:mt-0 {
            margin-top: 0px;
        }

        .lg\:-ml-\[23rem\] {
            margin-left: -23rem;
        }

        .lg\:mr-0 {
            margin-right: 0px;
        }

        .lg\:mr-4 {
            margin-right: 1rem;
        }

        .lg\:mt-5 {
            margin-top: 1.25rem;
        }

        .lg\:ml-10 {
            margin-left: 2.5rem;
        }

        .lg\:mb-5 {
            margin-bottom: 1.25rem;
        }

        .lg\:mb-10 {
            margin-bottom: 2.5rem;
        }

        .lg\:-mt-3 {
            margin-top: -0.75rem;
        }

        .lg\:-mt-8 {
            margin-top: -2rem;
        }

        .lg\:ml-5 {
            margin-left: 1.25rem;
        }

        .lg\:-mt-10 {
            margin-top: -2.5rem;
        }

        .lg\:-mt-16 {
            margin-top: -4rem;
        }

        .lg\:-mt-20 {
            margin-top: -5rem;
        }

        .lg\:mt-10 {
            margin-top: 2.5rem;
        }

        .lg\:ml-1 {
            margin-left: 0.25rem;
        }

        .lg\:mb-20 {
            margin-bottom: 5rem;
        }

        .lg\:mt-\[0\.2rem\] {
            margin-top: 0.2rem;
        }

        .lg\:mb-2 {
            margin-bottom: 0.5rem;
        }

        .lg\:block {
            display: block;
        }

        .lg\:flex {
            display: flex;
        }

        .lg\:grid {
            display: grid;
        }

        .lg\:hidden {
            display: none;
        }

        .lg\:h-4 {
            height: 1rem;
        }

        .lg\:h-\[300px\] {
            height: 300px;
        }

        .lg\:h-\[383px\] {
            height: 383px;
        }

        .lg\:h-\[187px\] {
            height: 187px;
        }

        .lg\:h-\[96px\] {
            height: 96px;
        }

        .lg\:h-\[48px\] {
            height: 48px;
        }

        .lg\:h-\[511px\] {
            height: 511px;
        }

        .lg\:h-\[167px\] {
            height: 167px;
        }

        .lg\:h-auto {
            height: auto;
        }

        .lg\:h-3\/4 {
            height: 75%;
        }

        .lg\:h-\[36px\] {
            height: 36px;
        }

        .lg\:h-\[100px\] {
            height: 100px;
        }

        .lg\:h-8 {
            height: 2rem;
        }

        .lg\:h-16 {
            height: 4rem;
        }

        .lg\:h-\[20px\] {
            height: 20px;
        }

        .lg\:h-60 {
            height: 15rem;
        }

        .lg\:h-\[2rem\] {
            height: 2rem;
        }

        .lg\:h-max {
            height: -moz-max-content;
            height: max-content;
        }

        .lg\:h-3\.5 {
            height: 0.875rem;
        }

        .lg\:h-3 {
            height: 0.75rem;
        }

        .lg\:h-6 {
            height: 1.5rem;
        }

        .lg\:h-5 {
            height: 1.25rem;
        }

        .lg\:h-\[30px\] {
            height: 30px;
        }

        .lg\:h-\[380px\] {
            height: 380px;
        }

        .lg\:max-h-\[130px\] {
            max-height: 130px;
        }

        .lg\:w-full {
            width: 100%;
        }

        .lg\:\!w-\[9rem\] {
            width: 9rem !important;
        }

        .lg\:w-4 {
            width: 1rem;
        }

        .lg\:w-40 {
            width: 10rem;
        }

        .lg\:w-\[1200px\] {
            width: 1200px;
        }

        .lg\:w-\[450px\] {
            width: 450px;
        }

        .lg\:w-auto {
            width: auto;
        }

        .lg\:w-\[96px\] {
            width: 96px;
        }

        .lg\:w-\[48px\] {
            width: 48px;
        }

        .lg\:w-1\/4 {
            width: 25%;
        }

        .lg\:w-96 {
            width: 24rem;
        }

        .lg\:w-\[36px\] {
            width: 36px;
        }

        .lg\:w-\[300px\] {
            width: 300px;
        }

        .lg\:w-8 {
            width: 2rem;
        }

        .lg\:w-\[20px\] {
            width: 20px;
        }

        .lg\:w-\[100px\] {
            width: 100px;
        }

        .lg\:w-\[320px\] {
            width: 320px;
        }

        .lg\:w-\[60px\] {
            width: 60px;
        }

        .lg\:w-60 {
            width: 15rem;
        }

        .lg\:w-48 {
            width: 12rem;
        }

        .lg\:w-20 {
            width: 5rem;
        }

        .lg\:w-5\/6 {
            width: 83.333333%;
        }

        .lg\:w-\[15px\] {
            width: 15px;
        }

        .lg\:w-\[110px\] {
            width: 110px;
        }

        .lg\:w-\[95rem\] {
            width: 95rem;
        }

        .lg\:w-\[35rem\] {
            width: 35rem;
        }

        .lg\:w-3\.5 {
            width: 0.875rem;
        }

        .lg\:w-3 {
            width: 0.75rem;
        }

        .lg\:w-6 {
            width: 1.5rem;
        }

        .lg\:w-5 {
            width: 1.25rem;
        }

        .lg\:w-\[30px\] {
            width: 30px;
        }

        .lg\:w-\[710px\] {
            width: 710px;
        }

        .lg\:max-w-\[85rem\] {
            max-width: 85rem;
        }

        .lg\:max-w-\[520px\] {
            max-width: 520px;
        }

        .lg\:max-w-full {
            max-width: 100%;
        }

        .lg\:max-w-6xl {
            max-width: 72rem;
        }

        .lg\:grid-cols-10 {
            grid-template-columns: repeat(10, minmax(0, 1fr));
        }

        .lg\:grid-cols-6 {
            grid-template-columns: repeat(6, minmax(0, 1fr));
        }

        .lg\:grid-cols-7 {
            grid-template-columns: repeat(7, minmax(0, 1fr));
        }

        .lg\:grid-cols-\[20\%_80\%\] {
            grid-template-columns: 20% 80%;
        }

        .lg\:grid-cols-5 {
            grid-template-columns: repeat(5, minmax(0, 1fr));
        }

        .lg\:grid-cols-3 {
            grid-template-columns: repeat(3, minmax(0, 1fr));
        }

        .lg\:grid-cols-2 {
            grid-template-columns: repeat(2, minmax(0, 1fr));
        }

        .lg\:grid-cols-4 {
            grid-template-columns: repeat(4, minmax(0, 1fr));
        }

        .lg\:grid-cols-\[5\%_15\%_60\%_20\%\] {
            grid-template-columns: 5% 15% 60% 20%;
        }

        .lg\:grid-cols-\[15\%_45\%_20\%\] {
            grid-template-columns: 15% 45% 20%;
        }

        .lg\:grid-cols-\[70\%_30\%\] {
            grid-template-columns: 70% 30%;
        }

        .lg\:grid-cols-\[15\%_85\%\] {
            grid-template-columns: 15% 85%;
        }

        .lg\:grid-cols-12 {
            grid-template-columns: repeat(12, minmax(0, 1fr));
        }

        .lg\:grid-cols-\[33\%_33\%_33\%\] {
            grid-template-columns: 33% 33% 33%;
        }

        .lg\:grid-cols-\[80\%_20\%\] {
            grid-template-columns: 80% 20%;
        }

        .lg\:grid-cols-none {
            grid-template-columns: none;
        }

        .lg\:grid-cols-1 {
            grid-template-columns: repeat(1, minmax(0, 1fr));
        }

        .lg\:grid-cols-\[20\%_20\%_60\%\] {
            grid-template-columns: 20% 20% 60%;
        }

        .lg\:grid-cols-\[25\%_15\%_60\%\] {
            grid-template-columns: 25% 15% 60%;
        }

        .lg\:grid-cols-\[10\%_10\%_80\%\] {
            grid-template-columns: 10% 10% 80%;
        }

        .lg\:grid-rows-none {
            grid-template-rows: none;
        }

        .lg\:justify-end {
            justify-content: flex-end;
        }

        .lg\:justify-center {
            justify-content: center;
        }

        .lg\:justify-items-start {
            justify-items: start;
        }

        .lg\:justify-items-end {
            justify-items: end;
        }

        .lg\:gap-2 {
            gap: 0.5rem;
        }

        .lg\:gap-5 {
            gap: 1.25rem;
        }

        .lg\:gap-10 {
            gap: 2.5rem;
        }

        .lg\:gap-x-96 {
            -moz-column-gap: 24rem;
                column-gap: 24rem;
        }

        .lg\:divide-x > :not([hidden]) ~ :not([hidden]) {
            --tw-divide-x-reverse: 0;
            border-right-width: calc(1px * var(--tw-divide-x-reverse));
            border-left-width: calc(1px * calc(1 - var(--tw-divide-x-reverse)));
        }

        .lg\:divide-x-2 > :not([hidden]) ~ :not([hidden]) {
            --tw-divide-x-reverse: 0;
            border-right-width: calc(2px * var(--tw-divide-x-reverse));
            border-left-width: calc(2px * calc(1 - var(--tw-divide-x-reverse)));
        }

        .lg\:overflow-x-auto {
            overflow-x: auto;
        }

        .lg\:rounded-full {
            border-radius: 9999px;
        }

        .lg\:rounded-lg {
            border-radius: 0.5rem;
        }

        .lg\:rounded-md {
            border-radius: 0.375rem;
        }

        .lg\:rounded-none {
            border-radius: 0px;
        }

        .lg\:rounded-l-lg {
            border-top-left-radius: 0.5rem;
            border-bottom-left-radius: 0.5rem;
        }

        .lg\:rounded-r-lg {
            border-top-right-radius: 0.5rem;
            border-bottom-right-radius: 0.5rem;
        }

        .lg\:rounded-t-none {
            border-top-left-radius: 0px;
            border-top-right-radius: 0px;
        }

        .lg\:rounded-l {
            border-top-left-radius: 0.25rem;
            border-bottom-left-radius: 0.25rem;
        }

        .lg\:rounded-b-none {
            border-bottom-right-radius: 0px;
            border-bottom-left-radius: 0px;
        }

        .lg\:rounded-r {
            border-top-right-radius: 0.25rem;
            border-bottom-right-radius: 0.25rem;
        }

        .lg\:rounded-tl-lg {
            border-top-left-radius: 0.5rem;
        }

        .lg\:rounded-tr-md {
            border-top-right-radius: 0.375rem;
        }

        .lg\:border {
            border-width: 1px;
        }

        .lg\:border-b-2 {
            border-bottom-width: 2px;
        }

        .lg\:border-l-0 {
            border-left-width: 0px;
        }

        .lg\:border-t {
            border-top-width: 1px;
        }

        .lg\:border-\[\#FFFFFF\] {
            --tw-border-opacity: 1;
            border-color: rgb(255 255 255 / var(--tw-border-opacity));
        }

        .lg\:border-gray-400 {
            --tw-border-opacity: 1;
            border-color: rgb(156 163 175 / var(--tw-border-opacity));
        }

        .lg\:p-0 {
            padding: 0px;
        }

        .lg\:p-5 {
            padding: 1.25rem;
        }

        .lg\:p-4 {
            padding: 1rem;
        }

        .lg\:p-12 {
            padding: 3rem;
        }

        .lg\:p-10 {
            padding: 2.5rem;
        }

        .lg\:p-2\.5 {
            padding: 0.625rem;
        }

        .lg\:p-2 {
            padding: 0.5rem;
        }

        .lg\:p-3 {
            padding: 0.75rem;
        }

        .lg\:px-8 {
            padding-left: 2rem;
            padding-right: 2rem;
        }

        .lg\:px-4 {
            padding-left: 1rem;
            padding-right: 1rem;
        }

        .lg\:py-1 {
            padding-top: 0.25rem;
            padding-bottom: 0.25rem;
        }

        .lg\:py-2 {
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
        }

        .lg\:px-5 {
            padding-left: 1.25rem;
            padding-right: 1.25rem;
        }

        .lg\:px-44 {
            padding-left: 11rem;
            padding-right: 11rem;
        }

        .lg\:px-9 {
            padding-left: 2.25rem;
            padding-right: 2.25rem;
        }

        .lg\:px-10 {
            padding-left: 2.5rem;
            padding-right: 2.5rem;
        }

        .lg\:py-5 {
            padding-top: 1.25rem;
            padding-bottom: 1.25rem;
        }

        .lg\:px-px {
            padding-left: 1px;
            padding-right: 1px;
        }

        .lg\:py-10 {
            padding-top: 2.5rem;
            padding-bottom: 2.5rem;
        }

        .lg\:py-0 {
            padding-top: 0px;
            padding-bottom: 0px;
        }

        .lg\:py-2\.5 {
            padding-top: 0.625rem;
            padding-bottom: 0.625rem;
        }

        .lg\:py-12 {
            padding-top: 3rem;
            padding-bottom: 3rem;
        }

        .lg\:px-2 {
            padding-left: 0.5rem;
            padding-right: 0.5rem;
        }

        .lg\:pt-9 {
            padding-top: 2.25rem;
        }

        .lg\:pt-5 {
            padding-top: 1.25rem;
        }

        .lg\:pl-16 {
            padding-left: 4rem;
        }

        .lg\:pr-12 {
            padding-right: 3rem;
        }

        .lg\:pr-48 {
            padding-right: 12rem;
        }

        .lg\:pl-10 {
            padding-left: 2.5rem;
        }

        .lg\:pt-2 {
            padding-top: 0.5rem;
        }

        .lg\:pt-3 {
            padding-top: 0.75rem;
        }

        .lg\:text-left {
            text-align: left;
        }

        .lg\:text-center {
            text-align: center;
        }

        .lg\:text-start {
            text-align: start;
        }

        .lg\:text-end {
            text-align: end;
        }

        .lg\:text-sm {
            font-size: 0.875rem;
            line-height: 1.25rem;
        }

        .lg\:text-xs {
            font-size: 0.75rem;
            line-height: 1rem;
        }

        .lg\:text-xl {
            font-size: 1.25rem;
            line-height: 1.75rem;
        }

        .lg\:text-base {
            font-size: 1rem;
            line-height: 1.5rem;
        }

        .lg\:text-5xl {
            font-size: 3rem;
            line-height: 1;
        }

        .lg\:text-3xl {
            font-size: 1.875rem;
            line-height: 2.25rem;
        }

        .lg\:text-2xl {
            font-size: 1.5rem;
            line-height: 2rem;
        }

        .lg\:text-\[50px\] {
            font-size: 50px;
        }

        .lg\:text-\[20px\] {
            font-size: 20px;
        }

        .lg\:text-lg {
            font-size: 1.125rem;
            line-height: 1.75rem;
        }

        .lg\:text-6xl {
            font-size: 3.75rem;
            line-height: 1;
        }

        .lg\:text-4xl {
            font-size: 2.25rem;
            line-height: 2.5rem;
        }

        .lg\:text-\[18px\] {
            font-size: 18px;
        }

        .lg\:text-\[16px\] {
            font-size: 16px;
        }

        .lg\:font-semibold {
            font-weight: 600;
        }

        .lg\:shadow-lg {
            --tw-shadow: 0 10px 15px -3px rgb(0 0 0 / 0.1), 0 4px 6px -4px rgb(0 0 0 / 0.1);
            --tw-shadow-colored: 0 10px 15px -3px var(--tw-shadow-color), 0 4px 6px -4px var(--tw-shadow-color);
            box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
        }

        @media (min-width: 768px) {
            .lg\:md\:grid-cols-\[15\%_55\%_30\%\] {
            grid-template-columns: 15% 55% 30%;
            }
        }
        }

        @media (min-width: 1280px) {
        .xl\:col-span-3 {
            grid-column: span 3 / span 3;
        }

        .xl\:col-span-2 {
            grid-column: span 2 / span 2;
        }

        .xl\:col-span-5 {
            grid-column: span 5 / span 5;
        }

        .xl\:col-start-3 {
            grid-column-start: 3;
        }

        .xl\:-ml-\[23rem\] {
            margin-left: -23rem;
        }

        .xl\:mr-24 {
            margin-right: 6rem;
        }

        .xl\:hidden {
            display: none;
        }

        .xl\:h-\[384px\] {
            height: 384px;
        }

        .xl\:h-\[479px\] {
            height: 479px;
        }

        .xl\:h-\[235px\] {
            height: 235px;
        }

        .xl\:h-\[100px\] {
            height: 100px;
        }

        .xl\:h-\[639px\] {
            height: 639px;
        }

        .xl\:h-\[214px\] {
            height: 214px;
        }

        .xl\:w-\[1536px\] {
            width: 1536px;
        }

        .xl\:w-auto {
            width: auto;
        }

        .xl\:w-\[100px\] {
            width: 100px;
        }

        .xl\:w-1\/6 {
            width: 16.666667%;
        }

        .xl\:w-28 {
            width: 7rem;
        }

        .xl\:max-w-xl {
            max-width: 36rem;
        }

        .xl\:grid-cols-4 {
            grid-template-columns: repeat(4, minmax(0, 1fr));
        }

        .xl\:grid-cols-2 {
            grid-template-columns: repeat(2, minmax(0, 1fr));
        }

        .xl\:grid-cols-5 {
            grid-template-columns: repeat(5, minmax(0, 1fr));
        }

        .xl\:px-64 {
            padding-left: 16rem;
            padding-right: 16rem;
        }

        .xl\:px-5 {
            padding-left: 1.25rem;
            padding-right: 1.25rem;
        }

        .xl\:pl-20 {
            padding-left: 5rem;
        }

        .xl\:text-left {
            text-align: left;
        }

        .xl\:text-center {
            text-align: center;
        }

        .xl\:text-justify {
            text-align: justify;
        }

        .xl\:align-bottom {
            vertical-align: bottom;
        }

        .xl\:text-4xl {
            font-size: 2.25rem;
            line-height: 2.5rem;
        }

        .xl\:text-5xl {
            font-size: 3rem;
            line-height: 1;
        }

        .xl\:text-3xl {
            font-size: 1.875rem;
            line-height: 2.25rem;
        }

        .xl\:text-xl {
            font-size: 1.25rem;
            line-height: 1.75rem;
        }

        .xl\:text-2xl {
            font-size: 1.5rem;
            line-height: 2rem;
        }
        }

        @media (min-width: 1536px) {
        .\32xl\:my-0 {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .\32xl\:flex {
            display: flex;
        }

        .\32xl\:h-\[575px\] {
            height: 575px;
        }

        .\32xl\:h-\[283px\] {
            height: 283px;
        }

        .\32xl\:h-\[767px\] {
            height: 767px;
        }

        .\32xl\:h-\[261px\] {
            height: 261px;
        }

        .\32xl\:max-w-2xl {
            max-width: 42rem;
        }

        .\32xl\:justify-end {
            justify-content: flex-end;
        }

        .\32xl\:px-96 {
            padding-left: 24rem;
            padding-right: 24rem;
        }

        .\32xl\:pl-24 {
            padding-left: 6rem;
        }

        .\32xl\:text-base {
            font-size: 1rem;
            line-height: 1.5rem;
        }
        }
        .rate{
          float: right;
          height: 46px;
          position: absolute;
          top: 59px;
          margin-left: 1px;
      }
      .rate:not(:checked)>input{
          position: relative;
          display: none;
      }
      .rate:not(:checked)>label{
          float: right;
          /* width: 3rem; */
          overflow: hidden;
          white-space:nowrap;
          font-size:30px;
          color:#ccc;
          cursor:pointer;
      }
      .rate:not(:checked)>label:before{
          content: '★ ';
      }
      span.rate:not(:checked)>input~label{
          color:#ffc107;
          font-weight: bold;
      }
      .rated{
          float: right;
          height: 46px;
          position: absolute;
          top: 87px;
          margin-left: 1px;
      }
      .rated:not(:checked)>input{
          position: absolute;
          display: none;
      }
      .rated:not(:checked)>label{
          float: right;
          width: 3rem;
          overflow: hidden;
          white-space:nowrap;
          font-size:30px;
          color:#ccc;
          cursor:pointer;
      }
      .rated:not(:checked)>label:before{
          content: '★ ';
      }
      span.rated:not(:checked)>input~label{
          color:#ffc107;
          font-weight: bold;
      }
      .kotak{
          /* text-align:center; */
          background: pink;
          width: 51px;
          height: 31px;
          padding: 2px;
      }
    </style>
</head>
<body style="font-size: 16px">
    <div class="max-w-[800px] m-auto p-[30px] text-[16px] leading-6">
        <header>
            <div class="flex justify-between">
                {{-- <table>
                    <tbody>
                        <tr>
                            <td></td>
                        </tr>
                    </tbody>
                </table> --}}
                <div>
                    <img src="data:image/png;base64,<?php echo base64_encode(file_get_contents(base_path('storage/app/public/img/kamtuu-logo.png')))?>" alt="kamtuu" class="float-left" width="22%">
                </div>
                <br>
                <div>
                    <h3 class="text-black text-[24px] font-bold mt-10">
                        @if($bookings->productdetail->product->type==='hotel')
                        Hotel Voucher
                        @endif
                        @if($bookings->productdetail->product->type==='activity')
                        Activity Voucher
                        @endif
                        @if($bookings->productdetail->product->type==='tour')
                        Tour Voucher
                        @endif
                        @if($bookings->productdetail->product->type==='rental')
                        Rental Voucher
                        @endif
                        @if($bookings->productdetail->product->type==='transfer')
                        Transfer Voucher
                        @endif  
                    </h3>
                </div>
            </div>
            <div class="flex justify-between mt-2">      
               <table class="table" style="border:solid 0px black">
                    <tbody>
                        <tr>
                            <td>
                                <p class="text-[16px]">
                                    {{$bookings->productdetail->product->type==='hotel' ? 'Itinary ID:':'Booking ID'}}
                                </p>
                            </td>
                            <td colspan="4" wigth="40%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td colspan="4" wigth="40%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td colspan="4" wigth="40%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td colspan="4" wigth="40%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td colspan="4" wigth="40%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td colspan="4" wigth="40%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <p class="font-bold text-[22px]">
                                    {{$bookings->productdetail->product->product_name}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="text-sky-400 text-[30px] mb-3">{{$booking}}</p>
                            </td>
                            <td colspan="4" wigth="40%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td colspan="4" wigth="40%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td colspan="4" wigth="40%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td colspan="4" wigth="40%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td colspan="4" wigth="40%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td colspan="4" wigth="40%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                @php
                                $checkin  = "";
                                $checkout = "";
                                $booking_bate  = "";
                             
                                if(isset($bookings->$bookings->tgl_checkin)){
                                    $checkin = \Carbon\Carbon::parse($bookings->tgl_checkout)->format('d M Y');
                                }

                                if(isset($bookings->$bookings->tgl_checkout)){
                                    $checkout = \Carbon\Carbon::parse($bookings->tgl_checkout)->format('d M Y');
                                }
                                // isset($bookings->activity_date) ? \Carbon\Carbon::parse($bookings->activity_date)->format('d M Y'): \Carbon\Carbon::parse($bookings->tgl_checkout)->format('d M Y')
                                if(isset($bookings->activity_date)){
                                    $booking_date = \Carbon\Carbon::parse($bookings->activity_date)->format('d M Y');
                                }else{
                                    $booking_date = \Carbon\Carbon::parse($bookings->tgl_checkout)->format('d M Y');
                                }

                                @endphp
                                @if($bookings->productdetail->product->type==='hotel' || $bookings->productdetail->product->type==='xstay')
                                    <div class="border-l-4 border-indigo-500 mr-3">
                                        <p class="font-thin text-[12px] ml-3">Tgl Check-in</p>
                                        <p class="font-bold text-xl ml-3">{{$checkin}}</p>
                                    </div>
                                    <div class="border-l-4 border-indigo-500">
                                        <p class="font-thin text-[12px] ml-3">Tgl Check-out</p>
                                        <p class="font-bold text-xl ml-3">{{$checkout}}</p>
                                    </div>
                                @else
                                    <div class="border-l-4 border-indigo-500">
                                        <p class="font-thin text-[12px] ml-3">Tgl Booking</p>
                                        <p class="font-bold text-xl ml-3">{{$booking_date}}</p>
                                    </div>
                                @endif
                            </td>
                        </tr>
                    </tbody>
               </table>
            </div>
            <div class="mt-4 border-t-4 border-black"></div>
        </header>
        <div>
            <div class="flex mt-4">
                @php
                $gallery = json_decode($bookings->productdetail->gallery,true);
                @endphp
                <table class="float-left">
                    <tbody>
                        <tr>
                            <td>
                                <img src={{isset($bookings->productdetail->thumbnail) ? public_path($bookings->productdetail->thumbnail) :"https://source.unsplash.com/300x250?hotel"}} class="bg-cover object-cover overflow:hidden" style="witdh:300px;height:250px" alt="">
                            </td>
                        </tr>
                    </tbody>
                </table>
                @if($bookings->type=='activity')
                <table style="float:right;width:250px;" class="overflow-hidden">
                @else
                <table style="float:right;width:350px;" class="overflow-hidden">
                @endif
                    <tbody>
                        <tr>
                            <td>
                                <div class="kotak-3">
                                    <h5 class="text-slate-500">Booking details</h5>
                                    <table class="table text-slate-500">
                                        <tr>
                                            <td width="20%">
                                                <p class="text-muted">Traveller&nbsp;:</p>
                                            </td>
                                            <td></td>
                                            <td width="20%">
                                                <p class="text-muted">{{$first_name}} {{$last_name}}</p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><div class="mt-4 border-t-2 border-black"></div></td>
                        </tr>
                        <tr>
                            <td>
                                @if($bookings->type==='hotel' || $bookings->type==='xstay')
                                <table>
                                    <tr>
                                        <td width="20%" colspan="2">
                                            <p class="text-muted">Jumlah tamu</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p class="text-muted">{{$dewasa}} {{$anak}} {{$balita}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="2">
                                            <p class="text-muted">Tipe kamar</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p class="text-muted">{{$jenis_kamar}} {{$kode_kamar}}</p>
                                            {{-- <p class="text-muted">{{$kode_kamar}}</p> --}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="2">
                                            <p class="text-muted">Ekstra</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p class="text-muted">{{$bookings->extra_name}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="2">
                                            <p class="text-muted">Amenitas:</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                        <p class="text-muted inline">{!!$amenitas!!}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="2">
                                            <p class="text-muted">Fasilitas</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                           <p class="inline">
                                               {!!$fasilitas!!}
                                            </p> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="2">
                                            <p class="text-muted">Harga</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            {{isset($bookings->harga_akhir) ? number_format($bookings->harga_akhir*1) : number_format($bookings->total_price*1)}}
                                        </td>
                                    </tr>
                                </table>
                                @elseif($bookings->type==='tour')
                                <table>
                                    <tr>
                                        <td width="25%">
                                            <p class="text-muted">Jumlah Peserta</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                        <p class="text-muted"> {{$jumlah_dewasa != 0 ? $jumlah_dewasa.' orang dewasa': ''}}</p> 
                                        <p class="text-muted"> {{$jumlah_anak != 0 ? $jumlah_anak.' orang anak': ''}}</p>
                                        <p class="text-muted"> {{$jumlah_balita != 0 ? $jumlah_balita.' orang balita': ''}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <p class="text-muted">Durasi</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p class="text-muted">{{$tour->jml_hari.' Hari'}} {{$tour->jml_malam.' Malam'}}</p>
                                            <p class="text-muted">{{$tour->izin_ekstra}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-muted" width="20%">Pilihan</td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p class="text-muted"></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-muted" width="20%">Harga</td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p class="text-muted">Rp. {{number_format($bookings->total_price)}}</p>
                                        </td>
                                    </tr>
                                </table>
                                @elseif($bookings->type==='transfer')
                                <table>
                                    @if($rute_id)
                                    <tr></tr>
                                        <td width="20%">
                                            <p class="text-muted">Rute</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                           <p class="text-muted">{{$detail_rute['title']}} dari {{$from}} ke {{$to}}</p>
                                        </td>
                                    </tr>    
                                    @else
                                    @foreach($rutes as $key => $rute)
                                    <tr>
                                        <td width="20%">
                                            <p class="text-muted">Rute {{$key+1}}</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p>{{$rute['judul_bus']}}</p>
                                        </td>
                                    </tr>    
                                    <tr>
                                        <td width="20%">
                                            <p class="text-muted">Keberangakatan {{$key+1}}</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p>{{$rute['tempat_pickup']}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <p class="text-muted">Pemberhentian {{$key+1}}</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p>{{$rute['tempat_drop']}}</p>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                    <tr>
                                        <td width="20%">
                                            <p class="text-muted">Ekstra</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p class="text-muted">{{$bookings->productdetail->izin_ekstra}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Pilihan</td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p class="text-muted"></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Harga</td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            @if($bookings->total_price)
                                            <p class="text-muted">Rp. {{number_format($bookings->total_price)}}</p>
                                            @endif  
                                        </td>
                                    </tr>
                                </table>
                                @elseif($bookings->type==='activity')
                                <table>
                                    <tbody>
                                        <tr>
                                            <td width="20%">Nama Aktivitas</td>
                                            <td>&nbsp;:&nbsp;</td>
                                            <td>{{$activity_name}}</td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Lokasi/Tempat</td>
                                            <td>&nbsp;:&nbsp;</td>
                                            <td class="capitalize">{{$nama_lokasi}}</td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Harga</td>
                                            <td>&nbsp;:&nbsp;</td>
                                            <td>
                                                @if($bookings->total_price)
                                                <p class="text-muted">Rp. {{number_format($bookings->total_price)}}</p>
                                                @endif  
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                @elseif($bookings->type==='rental')
                                <table>
                                    <tr>
                                        <td width="20%">
                                            <p class="text-muted">Untuk</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p class="text-muted">{{isset($booking->kategori_peserta) ? count(json_decode($booking->kategori_peserta, true)).' x '.json_decode($booking->kategori_peserta, true)[0]: '1 Dewasa'}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <p class="text-muted">Nama Mobil</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p class="text-muted text-capitalize">{{isset($nama_mobil) ? $nama_mobil : ''}} {{isset($type_mobil) ? $type_mobil : ''}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <p class="text-muted">Jumlah Kursi</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p class="text-muted text-capitalize">{{isset($kapasitas_mobil) ? $kapasitas_mobil : ''}} Kursi</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <p class="text-muted">No Polisi</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p class="text-muted text-capitalize">{{isset($nopol) ? $nopol : ''}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <p class="text-muted">Ekstra</p>
                                        </td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            <p class="text-muted text-capitalize">{{isset($booking->productdetail->izin_ekstra) ? $booking->productdetail->izin_ekstra : ''}}</p>
                                        </td>
                                    </tr>
                                </table>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <div class="border-t-2 border-black" style="margin-top:41%"></div>
            <div>
                <div class="flex justify-between mt-4">
                    <table>
                        <tbody>
                            <tr>
                                <td width="400px">
                                    <div class="text-justify overflow-hidden" style="width:400px;padding-right:41px;">
                                        @if(isset($bookings->productdetail->kebijakan_pembatalan_sebelumnya) && isset($bookings->productdetail->kebijakan_pembatalan_sebelumnya))
                                        <h4 class="text-slate-500 font-bold text-[16px] mb-4">Kebijakan Pembatalan</h4>
                                        <p class="text-wrap">
                                            Kami mengerti bahwa terkadang rencana dapat gagal. Kami tidak membebankan biaya pembatalan atau perubahan. Jika
                                            properti mengenakan biaya seperti ini sesuai dengan kebijakannya, maka biaya tersebut akan disampaikan kepada Anda. {{$bookings->productdetail->product->product_name}} mengenakan biaya pembatalan dan perubahan berikut.
                                            @php
                                                $arr_pembatalan = [];
                                                
                                                $pembatalan_sebelum = "";
                                                $potongan_sebelum = '';
                                                
                                                $x = 0;
                                                if(isset($bookings->productdetail->kebijakan_pembatalan_sebelumnya)){
                                                    $sebelum  = json_decode($bookings->productdetail->kebijakan_pembatalan_sebelumnya, true);
                                                    $potongan = json_decode($bookings->productdetail->kebijakan_pembatalan_potongan, true);
                                                    
                                                    for ($i = 0; $i < count($sebelum);$i++) {

                                                        if(isset($sebelum['sebelumnya']) && isset($potongan['potongan'])){
                                                            $arr_pembatalan[$x]['sebelum']  = $sebelum['sebelumnya'][$i];
                                                            $arr_pembatalan[$x]['potongan'] = $potongan['potongan'][$i];
                    
                                                        }else{
                                                            $arr_pembatalan[$x]['sebelum']  = $sebelum[$i];
                                                            $arr_pembatalan[$x]['potongan'] = $potongan[$i];
                                                        }
                                                    }
                                                }else{
                                                    $pembatalan_sebelum =  '1 hari';
                                                    $potongan_sebelum   = '100%';
                                                }
                                
                                            @endphp
                                            @if(count($arr_pembatalan) > 0)
                                            Pembatalan atau perubahan yang dilakukan pada:
                                            <ul>
                                                @foreach($arr_pembatalan as $pembatalan_sebelumnya)
                                                <li>
                                                    {{$pembatalan_sebelumnya['sebelum']}} hari sebelumnya (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($bookings->tgl_checkout)->format('d M Y')}} atau ketidakdatangan dikenakan biaya properti sebesar {{$pembatalan_sebelumnya['potongan']}}% dari jumlah total yang dibayarkan untuk reservasi
                                                </li>
                                                @endforeach
                                            </ul>
                                            @else
                                            Pembatalan atau perubahan yang dilakukan pada {{$pembatalan_sebelum}} sebelumnya (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($bookings->tgl_checkout)->format('d M Y')}} atau ketidakdatangan dikenakan biaya properti sebesar dari jumlah total yang dibayarkan untuk reservasi.
                                            @endif
                                            @php
                                                $arr_seteleh = [];
                                                
                                                $pembatalan_setelah = "";
                                                $potongan_setelah = '';
                                                
                                                $x = 0;
                                                if(isset($bookings->productdetail->kebijakan_pembatalan_setelah)){
                                                    $setelah  = json_decode($bookings->productdetail->kebijakan_pembatalan_setelah, true);
                                                    $potongan_setelah = json_decode($bookings->productdetail->kebijakan_pembatalan_potongan, true);
                                                    
                                                    for ($i = 0; $i < count($setelah);$i++) {
                                                        if(isset($setelah['sesudah']) && isset($potongan_setelah['potongan'])){
                                                            $arr_seteleh[$x]['setelah']  = $setelah['sesudah'][$i];
                                                            $arr_seteleh[$x]['potongan'] = $potongan_setelah['potongan'][$i];

                                                        }else{
                                                            $arr_seteleh[$x]['setelah']  = $setelah[$i];
                                                            $arr_seteleh[$x]['potongan'] = $potongan_setelah[$i];
                                                        }
                                                        $x++;
                                                    }
                                                }else{
                                                    $pembatalan_setelah =  '1 hari';
                                                    $potongan_setelah   = '100%';
                                                }
                                
                                            @endphp
                                            @if(count($arr_seteleh) > 0)
                                            <ul>
                                                @foreach($arr_seteleh as $pembatalan_setelah)
                                                <li>
                                                    {{$pembatalan_setelah['setelah']}} hari sebelumnya (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($bookings->tgl_checkout)->format('d M Y')}} atau ketidakdatangan dikenakan biaya properti sebesar {{$pembatalan_setelah['potongan']}}% dari jumlah total yang dibayarkan untuk reservasi
                                                </li>
                                                @endforeach
                                            </ul>
                                            @elseif(count($arr_pembatalan) > 0)
                                            Pembatalan atau perubahan yang dilakukan pada: 
                                            <ul>
                                                <li>
                                                    {{$pembatalan_setelah}} setelah  (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($bookings->tgl_checkout)->format('d M Y')}} atau 
                                                    ketidakdatangan dikenakan biaya properti sebesar {{$potongan_setelah}} dari jumlah total yang dibayarkan untuk reservasi
                                                </li>
                                            </ul>
                                            @else
                                            Pembatalan atau perubahan yang dilakukan pada
                                            {{$pembatalan_setelah}} setelah  (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($bookings->tgl_checkout)->format('d M Y')}} atau
                                            ketidakdatangan dikenakan biaya properti sebesar {{$potongan_setelah}} dari jumlah total yang dibayarkan untuk reservasi
                                            @endif
                                        </p>
                                        @endif
                                    </div>
                                </td>
                                <td>
                                    @if(isset($bookings->productdetail->kebijakan_pembatalan_sebelumnya) && isset($bookings->productdetail->kebijakan_pembatalan_sebelumnya))
                                    <div class="text-center overflow-hidden" style="float-right;margin-top:-220px">
                                        <h4 class="text-slate-500 font-bold mb-4" style="text-align:center;font-size:16px">Scan disini</h4>
                                        <img src="{{isset($bookings->productdetail->base_url) ? "data:image/png;base64,".DNS2D::getBarcodePNG($bookings->productdetail->base_url,'QRCODE'):asset('img/qrcode.png')}}" alt="">
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td width="400px">
                                    <div class="text-justify overflow-hidden" style="width:400px;padding-right:41px;">
                                        @if($bookings->type==="hotel"  || $bookings->type==="xstay")
                                        <h4 class="text-slate-500 font-bold text-[16px] mb-4">Petunjuk Kedatangan Terlambat</h4>
                                        <p>
                                            Jika Anda akan check-in terlambat, hubungi langsung properti ini untuk mengetahui kebijakan check-in terlambat mereka.
                                        </p>
                                        <h4 class="text-slate-300 font-bold text-[16px] mb-4">Kebijakan Checkin</h4>
                                        <p class="mb-4">
                                            <ul>
                                                <li>Waktu check-in mulai pukul 14.00</li>
                                                <li>Waktu check-in berakhir pukul tengah malam</li>
                                                <li>Usia minimal untuk check-in adalah: 18</li>
                                            </ul>
                                        </p>
                                        @else
                                            @if(isset($bookings->productdetail->kebijakan_pembatalan_sebelumnya) && isset($bookings->productdetail->kebijakan_pembatalan_sebelumnya))
                                                <h4 class="text-slate-300 font-bold text-[16px] mb-3 mt-2">Catatan</h4>
                                                <p>
                                                    {!!$bookings->productdetail->catatan!!}
                                                </p>
                                            @else
                                                <div style="margin-top:-72px">
                                                    <h4 class="text-slate-300 font-bold text-[16px] mb-3">Catatan</h4>
                                                    <p class="text-wrap"style="width: 751px;text-align: justify;padding-right: 41px">
                                                        {!!$bookings->productdetail->catatan!!}
                                                    </p>
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </td>
                                <td>
                                    @if(!isset($bookings->productdetail->kebijakan_pembatalan_sebelumnya) && !isset($bookings->productdetail->kebijakan_pembatalan_sebelumnya))
                                    <div class="text-center overflow-hidden" style="float-right;">
                                        <h4 class="text-slate-500 font-bold mb-4" style="text-align:center;font-size:16px">Scan disini</h4>
                                        <img src="{{isset($bookings->productdetail->base_url) ? "data:image/png;base64,".DNS2D::getBarcodePNG($bookings->productdetail->base_url,'QRCODE'):asset('img/qrcode.png')}}" alt="">
                                    </div>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    {{-- <div class="text-justify overflow-hidden" style="width:400px;padding-right:41px; float:left">
                        @if(isset($bookings->productdetail->kebijakan_pembatalan_sebelumnya) && isset($bookings->productdetail->kebijakan_pembatalan_sebelumnya))
                        <h4 class="text-slate-500 font-bold text-[16px] mb-4">Kebijakan Pembatalan</h4>
                        <p class="text-wrap">
                            Kami mengerti bahwa terkadang rencana dapat gagal. Kami tidak membebankan biaya pembatalan atau perubahan. Jika
                            properti mengenakan biaya seperti ini sesuai dengan kebijakannya, maka biaya tersebut akan disampaikan kepada Anda. {{$bookings->productdetail->product->product_name}} mengenakan biaya pembatalan dan perubahan berikut.
                            @php
                                $arr_pembatalan = [];
                                
                                $pembatalan_sebelum = "";
                                $potongan_sebelum = '';
                                
                                $x = 0;
                                if(isset($bookings->productdetail->kebijakan_pembatalan_sebelumnya)){
                                    $sebelum  = json_decode($bookings->productdetail->kebijakan_pembatalan_sebelumnya, true);
                                    $potongan = json_decode($bookings->productdetail->kebijakan_pembatalan_potongan, true);
                                    
                                    for ($i = 0; $i < count($sebelum);$i++) {

                                        if(isset($sebelum['sebelumnya']) && isset($potongan['potongan'])){
                                            $arr_pembatalan[$x]['sebelum']  = $sebelum['sebelumnya'][$i];
                                            $arr_pembatalan[$x]['potongan'] = $potongan['potongan'][$i];
    
                                        }else{
                                            $arr_pembatalan[$x]['sebelum']  = $sebelum[$i];
                                            $arr_pembatalan[$x]['potongan'] = $potongan[$i];
                                        }
                                    }
                                }else{
                                    $pembatalan_sebelum =  '1 hari';
                                    $potongan_sebelum   = '100%';
                                }
                
                            @endphp
                            @if(count($arr_pembatalan) > 0)
                            Pembatalan atau perubahan yang dilakukan pada:
                            <ul>
                                @foreach($arr_pembatalan as $pembatalan_sebelumnya)
                                <li>
                                    {{$pembatalan_sebelumnya['sebelum']}} hari sebelumnya (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($bookings->tgl_checkout)->format('d M Y')}} atau ketidakdatangan dikenakan biaya properti sebesar {{$pembatalan_sebelumnya['potongan']}}% dari jumlah total yang dibayarkan untuk reservasi
                                </li>
                                @endforeach
                            </ul>
                            @else
                            Pembatalan atau perubahan yang dilakukan pada {{$pembatalan_sebelum}} sebelumnya (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($bookings->tgl_checkout)->format('d M Y')}} atau ketidakdatangan dikenakan biaya properti sebesar dari jumlah total yang dibayarkan untuk reservasi.
                            @endif
                            @php
                                $arr_seteleh = [];
                                
                                $pembatalan_setelah = "";
                                $potongan_setelah = '';
                                
                                $x = 0;
                                if(isset($bookings->productdetail->kebijakan_pembatalan_setelah)){
                                    $setelah  = json_decode($bookings->productdetail->kebijakan_pembatalan_setelah, true);
                                    $potongan_setelah = json_decode($bookings->productdetail->kebijakan_pembatalan_potongan, true);
                                    
                                    for ($i = 0; $i < count($setelah);$i++) {
                                        if(isset($setelah['sesudah']) && isset($potongan_setelah['potongan'])){
                                            $arr_seteleh[$x]['setelah']  = $setelah['sesudah'][$i];
                                            $arr_seteleh[$x]['potongan'] = $potongan_setelah['potongan'][$i];

                                        }else{
                                            $arr_seteleh[$x]['setelah']  = $setelah[$i];
                                            $arr_seteleh[$x]['potongan'] = $potongan_setelah[$i];
                                        }
                                        $x++;
                                    }
                                }else{
                                    $pembatalan_setelah =  '1 hari';
                                    $potongan_setelah   = '100%';
                                }
                
                            @endphp
                            @if(count($arr_seteleh) > 0)
                            <ul>
                                @foreach($arr_seteleh as $pembatalan_setelah)
                                <li>
                                    {{$pembatalan_setelah['setelah']}} hari sebelumnya (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($bookings->tgl_checkout)->format('d M Y')}} atau ketidakdatangan dikenakan biaya properti sebesar {{$pembatalan_setelah['potongan']}}% dari jumlah total yang dibayarkan untuk reservasi
                                </li>
                                @endforeach
                            </ul>
                            @elseif(count($arr_pembatalan) > 0)
                            Pembatalan atau perubahan yang dilakukan pada: 
                            <ul>
                                <li>
                                    {{$pembatalan_setelah}} setelah  (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($bookings->tgl_checkout)->format('d M Y')}} atau 
                                    ketidakdatangan dikenakan biaya properti sebesar {{$potongan_setelah}} dari jumlah total yang dibayarkan untuk reservasi
                                </li>
                            </ul>
                            @else
                            Pembatalan atau perubahan yang dilakukan pada
                            {{$pembatalan_setelah}} setelah  (waktu lokal properti) pada tanggal {{\Carbon\carbon::parse($bookings->tgl_checkout)->format('d M Y')}} atau
                            ketidakdatangan dikenakan biaya properti sebesar {{$potongan_setelah}} dari jumlah total yang dibayarkan untuk reservasi
                            @endif
                        </p>
                        @endif
                        @if($bookings->type==="hotel"  || $bookings->type==="xstay")
                        <h4 class="text-slate-500 font-bold text-[16px] mb-4">Petunjuk Kedatangan Terlambat</h4>
                        <p>
                            Jika Anda akan check-in terlambat, hubungi langsung properti ini untuk mengetahui kebijakan check-in terlambat mereka.
                        </p>
                        <h4 class="text-slate-300 font-bold text-[16px] mb-4">Kebijakan Checkin</h4>
                        <p>
                            <ul>
                                <li>Waktu check-in mulai pukul 14.00</li>
                                <li>Waktu check-in berakhir pukul tengah malam</li>
                                <li>Usia minimal untuk check-in adalah: 18</li>
                            </ul>
                        </p>
                        @else
                            @if(isset($bookings->productdetail->kebijakan_pembatalan_sebelumnya) && isset($bookings->productdetail->kebijakan_pembatalan_sebelumnya))
                                <h4 class="text-slate-300 font-bold text-[16px] mb-3 mt-2">Catatan</h4>
                                <p>
                                    {!!$bookings->productdetail->catatan!!}
                                </p>
                            @else
                                <h4 class="text-slate-300 font-bold text-[16px] mb-3 mt-2">Catatan</h4>
                                <p class="text-wrap"style="width: 751px;text-align: justify;padding-right: 41px">
                                    {!!$bookings->productdetail->catatan!!}
                                </p>
                            @endif
                        @endif
                    </div>
                    <div class="text-center overflow-hidden" style="float-right;">
                        <h4 class="text-slate-500 font-bold text-center mb-2" style="font-size:16px;">Scan disini</h4>
                        <img src="{{isset($bookings->productdetail->base_url) ? "data:image/png;base64,".DNS2D::getBarcodePNG($bookings->productdetail->base_url,'QRCODE'):asset('img/qrcode.png')}}" alt="">
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</body>
</html>
