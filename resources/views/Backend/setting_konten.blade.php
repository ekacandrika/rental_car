@extends('layouts.backend.main')

@section('style')
    <style>
        .column {
            float: left;
            width: 10%;
            /* padding: 5px; */
        }

        /* Clearfix (clear floats) */
        .row::after {
            content: "";
            clear: both;
            display: table;
        }

        .action {
            width: 100px;
        }

        @media screen and (max-width: 800px) {
            .column {
                width: 100%;
                float: left;
            }
        }

        .table td a img,
        .jsgrid .jsgrid-table td a img {
            width: 15px;
            height: 36px;
            border-radius: 100%;
        }

        .table td img,
        .jsgrid .jsgrid-table td img {
            width: 75%;
            height: 75%;
            border-radius: 0;
        }
    </style>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <h4 class="card-title">Carousel Image</h4>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <a href="#" type="button" class="btn btn-primary btn-icon-text" style="float: right">
                            <i class="ti-upload btn-icon-prepend"></i>
                            Upload
                        </a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <img src="{{ asset('asset') }}/images/dashboard/people.svg" alt="people">
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="column">
                                            <a class="nav-link" href="{{ route('settingkonten') }}">
                                                <img class="action"
                                                    src="{{ asset('asset') }}/icons/pen-to-square-solid.svg">
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
