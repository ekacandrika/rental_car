@extends('layouts.backend.main')

@section('content')
    <div class="content-wrapper">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add Destination</h4>
                    <form action="{{ route('destindonesia.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Provinsi</label>
                            <input type="hidden" name="provinsi_id" id="provinsi_id">
                            <select class="js-example-basic-single w-100" id="provinsi">
                                <option selected>=== PILIH PROVINSI ===</option>
                                @foreach ($provinces as $provinsi)
                                    <option value="{{ $provinsi->id }}">{{ $provinsi->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect2">Kota / Kabupaten</label>
                            <input type="hidden" name="kota_id" id="kabupaten_id">
                            <select class="js-example-basic-single w-100" id="kabupaten">
                                <option selected>=== PILIH KABUPATEN ===</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect3">Kecamatan</label>
                            <input type="hidden" name="kecamatan_id" id="kecamatan_id">
                            <select class="js-example-basic-single w-100" id="kecamatan">
                                <option selected>=== PILIH KECAMATAN ===</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName1">Objek Wisata</label>
                            <input type="text" name="objekwisata" class="form-control" id="exampleInputName1"
                                placeholder="Objek Wisata">
                        </div>
                        <button type="submit" class="btn btn-primary btn-rounded btn-fw">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(function() {
                // provinsi
                $('#provinsi').on('change', function() {
                    let id_provinsi = $('#provinsi').val();
                    console.log(id_provinsi);
                    $('#provinsi_id').val(id_provinsi);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getkabupaten') }}",
                        data: {
                            id_provinsi: id_provinsi
                        },
                        cache: false,

                        success: function(msg) {
                            $('#kabupaten').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })

                // kabupaten
                $('#kabupaten').on('change', function() {
                    let id_kabupaten = $('#kabupaten').val();
                    console.log(id_kabupaten);
                    $('#kabupaten_id').val(id_kabupaten);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getkecamatan') }}",
                        data: {
                            id_kabupaten: id_kabupaten
                        },
                        cache: false,

                        success: function(msg) {
                            $('#kecamatan').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })

                $('#kecamatan').on('change', function() {
                    let id_kecamatan = $('#kecamatan').val();
                    console.log(id_kecamatan);
                    $('#kecamatan_id').val(id_kecamatan);
                })
            })
        });
    </script>
@endpush
