@extends('layouts.backend.main')

@section('content')
    <div class="content-wrapper">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <h4 class="card-title">Destinasi Wisata Indonesia</h4>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <a href="{{ route('destindonesia.create') }}" type="button" class="btn btn-primary btn-icon-text"
                                style="float: right">
                                <i class="mdi mdi-plus-circle-outline"></i> Add Destination
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Objek Wisata</th>
                                    <th>Provinsi</th>
                                    <th>Kota</th>
                                    <th>Kecamatan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($data as $item)
                                    <tr>
                                        <td>{{ $item->objekwisata }}</td>
                                        @php
                                            $provinsi = App\Models\Province::where('id', $item->provinsi_id)->first();
                                            $kota = App\Models\Regency::where('id', $item->kota_id)->first();
                                            $kec = App\Models\District::where('id', $item->kecamatan_id)->first();
                                            // dd($provinsi);
                                        @endphp
                                        <td>{{ $provinsi->name }}</td>
                                        <td>{{ $kota->name }}</td>
                                        <td>{{ $kec->name }}</td>
                                        <td>Action</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5"><i>Data tidak tersedia !</i></td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
