<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kota Kabupaten') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    {{-- sweet alert --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.min.css" rel="stylesheet">
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])



    {{--
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">--}}
    {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>--}}
    <style>
        .rate {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rate:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rate:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rate:not(:checked)>label:before {
            content: '★ ';
        }

        .rate>input:checked~label {
            color: #ffc700;
        }

        .rate:not(:checked)>label:hover,
        .rate:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rate>input:checked+label:hover,
        .rate>input:checked+label:hover~label,
        .rate>input:checked~label:hover,
        .rate>input:checked~label:hover~label,
        .rate>label:hover~input:checked~label {
            color: #c59b08;
        }

        .star-rating-complete {
            color: #c59b08;
        }

        .rating-container .form-control:hover,
        .rating-container .form-control:focus {
            background: #fff;
            border: 1px solid #ced4da;
        }

        .rating-container textarea:focus,
        .rating-container input:focus {
            color: #000;
        }

        .rated {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rated:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ffc700;
        }

        .rated:not(:checked)>label:before {
            content: '★ ';
        }

        .rated>input:checked~label {
            color: #ffc700;
        }

        .rated:not(:checked)>label:hover,
        .rated:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rated>input:checked+label:hover,
        .rated>input:checked+label:hover~label,
        .rated>input:checked~label:hover,
        .rated>input:checked~label:hover~label,
        .rated>label:hover~input:checked~label {
            color: #c59b08;
        }

        .maps-embed {
            padding-bottom: 60%
        }

        .maps-embed iframe {
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
            position: absolute;
        }
        /* Extra small devices (phones, 600px and down) */
        @media only screen and (max-width: 600px) {
            iframe.note-video-clip{
                width: 320px;
            }
        }
        /* Small devices (portrait tablets and large phones, 600px and up) */
        @media only screen and (min-width: 600px) {

        }
        /* Medium devices (landscape tablets, 768px and up) */
        @media only screen and (min-width: 768px) {

        }
        /* Large devices (laptops/desktops, 992px and up) */
        @media only screen and (min-width: 992px) {

        }
        /* Extra large devices (large laptops and desktops, 1200px and up) */
        @media only screen and (min-width: 1200px) {

        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Hero --}}
    <div class="relative bg-slate-400">
        <img class="object-cover w-full opacity-70" src="{{Storage::url($pulaus->header)}}" alt="">

        {{-- Kabupaten/Kota Logo --}}
        <img class="absolute translate-x-1/2 bottom-1/2 right-1/2 w-[50px] h-[50px] sm:w-[75px] sm:h-[75px] lg:w-[100px] lg:h-[100px] sm:top-10 sm:right-10 sm:bottom-0 sm:translate-x-0 shadow-xl bg-white rounded-full"
            src="{{Storage::url($pulaus->logo_daerah)}}" alt="">

        {{-- Kabupaten/Kota Name --}}
        <div class="absolute translate-x-1/2 translate-y-full sm:translate-y-1/2 bottom-1/2 right-1/2">
            <p class="text-lg font-bold text-center text-white sm:text-2xl lg:text-6xl drop-shadow-2xl">
                {{$pulaus->nama_pulau}}</p>
        </div>
    </div>

    <div class="container grid h-auto min-h-full gap-10 px-5 mx-auto my-5 sm:grid-cols-3 xl:grid-cols-4">
        {{-- Detail --}}
        <div class="flex flex-col sm:col-span-2 xl:col-span-3">
            <div class="mx-auto">
                <x-destindonesia.status-list ratings="{{ $ratings }}" var2="{{ $var2 }}" want="{{$want}}" been="{{$been}}"></x-destindonesia.status-list>
            </div>
            <nav class="flex mt-2" aria-label="Breadcrumb">
                <ol class="inline-flex items-center space-x-1 md:space-x-3 text-base lg:text-lg">
                    <li class="inline-flex items-center">
                        <a href="{{isset($pulau_slug) ? route('pulauShow',$pulau_slug) : '#'}}"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] capitalize">{{$pulau_nama}}</a>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            <a href="{{isset($provinsi_slug) ? route('provinsiShow',$provinsi_slug) : '#'}}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] capitalize">{{$provinsi_nama}}</a>
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                                </svg>
                                <a href="#"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1] capitalize">{{$pulaus->nama_pulau}}</a>
                        </div>
                    </li>
                    {{-- <li>
                        <div class="flex items-center">
                           
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                          
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                           
                        </div>
                    </li> --}}
                </ol>
            </nav>
            <div class="grid grid-rows-2 sm:grid-rows-1 sm:grid-cols-2">
                {{-- Share FB/WA --}}
                <div class="flex items-center text-base lg:text-lg">
                    <p>Share</p>
                    <img src="{{ asset('storage/icons/square-facebook.svg') }}" class="mx-3" width="23px" height="23px"
                        alt="wa-icon">
                    <img src="{{ asset('storage/icons/square-whatsapp.svg') }}" width="23px" height="23px"
                        alt="fb-icon">
                </div>

                {{-- Button --}}
                <div class="flex sm:justify-end">
                    <x-destindonesia.button-dropdown></x-destindonesia.button-dropdown>
                    {{-- <x-destindonesia.button-secondary text="Ingin Kesana">@slot('button')
                        Ingin Ke Sana
                        @endslot</x-destindonesia.button-secondary> --}}
                    <x-destindonesia.button-primary text="Pesan Sekarang" icon="icons/foundation_foot_white.svg">
                        @slot('button')
                        Pesan Sekarang
                        @endslot
                    </x-destindonesia.button-primary>
                </div>
            </div>

            {{-- Goverment Opening --}}
            <div class="bg-gray-100 border border-gray-300 rounded-md">
                <div class="grid gap-5 mx-10 my-10 grid:row-2 lg:grid-cols-5 xl:mr-24">
                    {{-- Goverment Profile --}}
                    <div class="flex justify-center lg:justify-end">
                        <img src="{{Storage::url($pulaus->foto_menteri)}}"
                            class="bg-clip-content w-24 h-24 lg:w-[96px] lg:h-[96px] xl:w-[100px] xl:h-[100px] shadow-xl bg-white rounded-full"
                            alt="goverment-profile">
                    </div>

                    {{-- Goverment Opening Text --}}
                    <div class="lg:col-span-4">
                        @if($pulaus->objek=='Kabupaten')
                        <p class="text-sm text-center lg:text-left text-slate-700">Bupati</p>
                        @else
                        <p class="text-sm text-center lg:text-left text-slate-700">Walikota</p>
                        @endif
                        <p class="mb-1 text-xl font-semibold text-center lg:text-left">{{$pulaus->nama_menteri}}
                        </p>
                        <p class="text-sm text-justify text-slate-700">{!! $pulaus->kata_sambutan !!}</p>
                    </div>
                </div>
            </div>

            {{-- Description --}}
            <div class="py-5 text-justify text-md lg:text-xl">
                <p> {!! $pulaus->deskripsi !!}</p>
            </div>

            {{-- Table --}}
            <div class="flex justify-center py-10 border-y-2 border-slate-300">
                <table class="font-medium border border-black table-auto">
                    <tbody>
                        <tr>
                            <td class="p-3 border border-black">Ibukota</td>
                            <td class="p-3 border border-black">{{ $pulaus->ibukota }}</td>
                        </tr>
                        <tr>
                            <td class="p-3 border border-black">Bandara</td>
                            <td class="p-3 border border-black">{{ $pulaus->bandara }}</td>
                        </tr>
                        <tr>
                            <td class="p-3 border border-black">Pelabuhan Laut</td>
                            <td class="p-3 border border-black">{{ $pulaus->pelabuhan }}</td>
                        </tr>
                        <tr>
                            <td class="p-3 border border-black">Terminal Darat</td>
                            <td class="p-3 border border-black">{{ $pulaus->terminal }}</td>
                        </tr>
                        <tr>
                            <td class="p-3 border border-black">Transportasi ke sana</td>
                            <td class="p-3 border border-black">{{ $pulaus->transportasi }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            {{-- {{ dd(isset($pulaus->galleryObjek), count(json_decode($pulaus->galleryObjek)),
            json_decode($pulaus->galleryObjek)[0]->image) }} --}}

            {{-- Gallery --}}
            @if (count(json_decode($pulaus->galleryObjek)) > 0 && isset($pulaus->galleryObjek))
            <div class="relative h-full max-h-[400px] my-5 bg-slate-400">
                <img class="object-cover w-full h-full opacity-70"
                    src="{{ Storage::url('public/gallery/'.json_decode($pulaus->galleryObjek)[0]->image) }}" alt="">

                {{-- Lightbox --}}
                <div x-data="{ open: false }" @keydown.escape="open = false">
                    <button @click="open = true"
                        class="absolute bottom-0 right-0 px-5 py-2 text-base font-semibold text-black duration-300 -translate-x-5 -translate-y-10 bg-gray-100 border-2 border-gray-300 rounded-md hover:bg-gray-200">
                        Semua foto
                    </button>
                    <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                        style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                        <div
                            class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                            <button
                                class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                            </button>
                        </div>
                        <div class="h-full w-full flex items-center justify-center overflow-hidden"
                            x-data="{active: 0, slides: {{ ($pulaus->galleryObjek) }} }">
                            <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                    <button type="button"
                                        class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === 0 ? slides.length - 1 : active - 1">
                                        <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>

                            @foreach (json_decode($pulaus->galleryObjek) as $index=>$gallery)
                            <div class="h-full w-full flex items-center justify-center absolute">
                                <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                    x-show="active === {{ $index }}"
                                    x-transition:enter="transition ease-out duration-150"
                                    x-transition:enter-start="opacity-0 transform scale-90"
                                    x-transition:enter-end="opacity-100 transform scale-100"
                                    x-transition:leave="transition ease-in duration-150"
                                    x-transition:leave-start="opacity-100 transform scale-100"
                                    x-transition:leave-end="opacity-0 transform scale-90">

                                    <img src="{{ Storage::url('public/gallery/'.$gallery->image) }}"
                                        class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                </div>
                                <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                    x-show="active === {{ $index }}">
                                    <span class="w-12 text-right" x-text="{{ $index }} + 1"></span>
                                    <span class="w-4 text-center">/</span>
                                    <span class="w-12 text-left"
                                        x-text="{{ count(json_decode($pulaus->galleryObjek)) }}"></span>
                                </div>
                            </div>
                            @endforeach

                            <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-start w-12 md:ml-16">
                                    <button type="button"
                                        class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === slides.length - 1 ? 0 : active + 1">
                                        <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif


            {{-- Maps --}}
            <div class="my-5 w-full">
                <p class="my-5 text-3xl font-semibold">Maps</p>
                {{-- <iframe--}} {{--
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d505145.82556779945!2d114.79138629830669!3d-8.455371757311914!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd141d3e8100fa1%3A0x24910fb14b24e690!2sBali!5e0!3m2!1sen!2sid!4v1665471945807!5m2!1sen!2sid"
                    --}} {{-- style="border:0;" allowfullscreen="" loading="lazy" class="rounded-lg w-full h-[450px]"
                    --}} {{-- referrerpolicy="no-referrer-when-downgrade"></iframe>--}}
                    <div class="overflow-hidden relative maps-embed">{!!$pulaus->embed_maps!!}</div>
            </div>

            {{-- Kamtuu Transfer Search Bar --}}
            <div class="hidden my-10 text-center sm:text-left lg:block">
            </p>
            {{-- <x-destindonesia.search></x-destindonesia.search> --}}
            <form action="{{ route('transfer.search') }}" method="POST">
                @csrf
                <div x-show="tab === 2"
                    class="z-0 grid grid-cols-1 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-7 md:grid-cols-1 bg-kamtuu-second justify-items-center divide-kamtuu-second lg:divide-x">
                    <div class="grid w-full p-2 bg-white lg:rounded-l-lg justify-items-left lg:justify-items-left">
                        {{-- <div> --}}
                        {{-- <img src="{{ asset('storage/icons/icon-maps.svg') }}"
                                class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu"> --}}
                        {{-- </div> --}}
                        <div>
                            <div class="flex-inline">
                                <select name="search" id="maps" required
                                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                    <option value="">Dari</option>
                                    @foreach ($regencyTur as $foo)
                                        <option style="font-size: 10px" value="{{ $foo->id }}">
                                            {{ $foo->name }}</option>
                                    @endforeach
                                </select>
                                {{-- <select name="" id="maps"
                                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                    <option value="">Dari</option>
                                    <option value="">1</option>
                                    <option value="">1</option>
                                </select> --}}
                            </div>
                        </div>
                    </div>

                    <div class="grid w-full p-2 bg-white justify-items-left lg:justify-items-left">
                        <div>
                            <div class="flex-inline">
                                <select name="to" id="maps" required
                                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                    <option value="">Ke</option>
                                    @foreach ($regencyTur as $foo)
                                        <option style="font-size: 10px" value="{{ $foo->id }}">
                                            {{ $foo->name }}</option>
                                    @endforeach
                                </select>
                                {{-- <select name="" id="point"
                                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                    <option value="">Ke</option>
                                    <option value="">1</option>
                                    <option value="">1</option>
                                </select> --}}
                            </div>
                        </div>
                    </div>

                    <div class="grid w-full p-2 bg-white justify-items-center grid-cols-[80%_20%]">
                        <div class="place-self-center">
                            <input class="!-ml-[1rem] md:!ml-[6rem] lg:!ml-[2rem] lg:!w-[9rem] md:!w-[31rem]"
                                id="datepickerTransfer" placeholder="Tanggal" name="tgl" required />
                        </div>
                        <div class="md:hidden lg:hidden">
                            <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                class="w-[15px] h-[17px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                        </div>
                    </div>

                    <div class="grid w-full p-2 bg-white justify-items-start grid-cols-[70%_30%]">
                        <div class="place-self-center">
                            <input
                                class="!ml-[17px] lg:ml-0 md:!w-[31rem] md:!ml-[9rem]  lg:!ml-[2rem] lg:!w-[9rem] "
                                id="timepickerTransfer" placeholder="Waktu Ambil" name="waktu_ambil" required />
                        </div>
                    </div>

                    {{-- <div class="grid w-full p-2 bg-white justify-items-left">
                        <div>
                            <div class="flex-inline">
                                <select name="" id=""
                                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                    <option value="">Jumlah Orang</option>
                                    <option value="">1</option>
                                    <option value="">1</option>
                                </select>
                            </div>
                        </div>
                    </div> --}}

                    <div class="grid w-full p-2 bg-white justify-items-left">
                        <div>
                            <div class="ml-1 flex-inline">
                                <select name="metode_transfer" id="" required
                                    class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                    <option value="">Metode Transfer</option>
                                    <option value="Transfer Private">Transfer Private</option>
                                    <option value="Transfer Umum">Transfer Umum</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div
                        class="flex justify-center w-full p-2 text-white md:rounded-r-lg lg:rounded-r-lg bg-kamtuu-primary">
                        <button>
                            <div>
                                <div class="flex-inline">
                                    <h1 class="uppercase">cari</h1>
                                </div>
                            </div>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        </div>

        {{-- Ads Desktop --}}
        <div class="hidden sm:block">
            <p class="mx-5 my-3 text-3xl font-semibold">Article (Ads)</p>
            <x-destindonesia.ads-list></x-destindonesia.ads-list>
        </div>
    </div>

        {{-- Objek Wisata Kota Wilayah  {active: 0,tabs: ['Bali', 'Nusa Tenggara', 'Bali']}--}}
        <div class="container mx-auto my-5">
        
            <p class="mx-3 text-xl sm:text-3xl font-semibold my-3 text-center sm:text-left">Objek Wisata</p>
    
            {{-- Tabs --}}
            <div class="flex sm:block mx-5" x-data="kategoriWisata()">
                <ul
                    class="flex justify-start overflow-x-auto text-sm font-medium text-center text-gray-500 dark:text-gray-400">
                    <template x-for="(tab, index) in tabs" :key="index">
                        {{-- 
                        <li class="mx-2 mt-2">
                            <button x-on:click="active = 'Tur'" aria-current="page"
                                class="inline-block py-2 px-9 rounded-lg active"
                                :class="active == 'Tur' ? 'text-white bg-[#23AEC1]' :
                                    'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]'">Tour</button>
                        </li>
                        --}}
                        <li class="mx-2 mt-2">
                            <button x-on:click="active = index" class="inline-block py-2 px-9 h-full text-white bg-[#23AEC1] rounded-lg"
                                :class="{ 'text-white bg-[#23AEC1]': active ==
                                    index, 'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]': active !==
                                        index }"
                                x-text="tab" @click="change(index)">
                            </button>
                        </li>
                    </template>
                </ul>
            </div>    
        </div>
        {{--  collectWisata()--}}
        <div class="container mx-auto my-5" x-data="kategoriWisata()">
            @if(count($wisata) > 0)
            <div class="flex justify-start">
                <div id="objek-wisata">
                    <div class="w-full hidden lg:grid gap-5 lg:grid-cols-4">
                        @foreach($wisata as $index => $objek_wisata)
                        <x-destindonesia.card-destination index="{{$index}}" lokasi="provinsi" 
                        text="{{$objek_wisata->title}}" slug="{{route('wisataShow',$objek_wisata->slug)}}"
                        header="{{$objek_wisata->header}}">
                        </x-destindonesia.card-destination>
                        @endforeach
                    </div>
                    <div class="w-full hidden md:grid lg:hidden md:grid-cols-3">
                        @foreach($wisata as $index => $objek_wisata)
                        <x-destindonesia.card-destination index="{{$index}}" lokasi="provinsi" 
                        text="{{$objek_wisata->title}}" slug="{{route('wisataShow',$objek_wisata->slug)}}"
                        header="{{$objek_wisata->header}}">
                        </x-destindonesia.card-destination>
                        @if($index==2)
                        @break
                        @endif
                        @endforeach
                    </div>
                    <div class="w-full grid md:hidden gap-5 grid-cols-2 lg:overflow-x-auto">
                        @foreach($wisata as $index => $objek_wisata)
                        <x-destindonesia.card-destination index="{{$index}}" lokasi="provinsi" 
                        text="{{$objek_wisata->title}}" slug="{{route('wisataShow',$objek_wisata->slug)}}"
                        header="{{$objek_wisata->header}}">
                        </x-destindonesia.card-destination>
                        {{-- @if($index==1) --}}
                        {{-- @break --}}
                        {{-- @endif --}}
                        @endforeach
                    </div>
                </div>
                <div id="objek-wisata-filter">

                </div>
                {{-- <div class="flex hidden md:hidden justify-center w-full">
                    <x-destindonesia.card-destination></x-destindonesia.card-destination>
                </div> --}}
            </div>
            <div class="flex justify-center">
                
                 <a href="{{route('wisataShow.all',$pulaus->slug)}}" class="p-5 grid justify-items-center">
                    <button type="submit" class="my-3 px-8 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">
                        Lihat Semua Wisata
                    </button>
                </a>    
               
                {{-- <x-destindonesia.button-primary>
                    @slot('button')
                        Lihat Semua Wisata
                    @endslot
                </x-destindonesia.button-primary> --}}
            </div>
            @else
            <div class="flex justify-center">
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                Objek wisata tidak ditemukan
                </div>
            </div>
            @endif
        </div>
    
        {{-- Destinasi and Tabs --}}
        <div class="container px-3 mx-auto" x-data="{ active: 'Tur' }">
            <p class="mx-3 text-xl sm:text-3xl font-semibold my-3 text-center sm:text-left">Produk terkait yang dengan kota/kabupaten <span class="capitalize">{{$pulaus->title}}</span></p>
        {{-- Tabs --}}
        <div class="flex mx-5 sm:block sm:mx-0">
            <ul
                class="flex justify-start overflow-x-auto text-sm font-medium text-center text-gray-500 dark:text-gray-400">
                <li class="mx-2 mt-2">
                    <button x-on:click="active = 'Tur'" aria-current="page"
                        class="inline-block py-2 px-9 rounded-lg active"
                        :class="active == 'Tur' ? 'text-white bg-[#23AEC1]' :
                            'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]'">Tour</button>
                </li>
                <li class="mx-2 mt-2">
                    <button x-on:click="active = 'Transfer'" class="inline-block py-2 px-9 rounded-lg active"
                        :class="active == 'Transfer' ? 'text-white bg-[#23AEC1]' :
                            'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]'">Transfer</button>
                </li>
                <li class="mx-2 mt-2">
                    <button x-on:click="active = 'Rental'" class="inline-block py-2 px-9 rounded-lg active"
                        :class="active == 'Rental' ? 'text-white bg-[#23AEC1]' :
                            'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]'">Rental</button>
                </li>
                <li class="mx-2 mt-2">
                    <button x-on:click="active = 'Hotel'" class="inline-block py-2 px-9 rounded-lg active"
                        :class="active == 'Hotel' ? 'text-white bg-[#23AEC1]' :
                            'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]'">Hotel</button>
                </li>
                <li class="mx-2 mt-2">
                    <button x-on:click="active = 'Activity'" class="inline-block py-2 px-9 rounded-lg active"
                        :class="active == 'Activity' ? 'text-white bg-[#23AEC1]' :
                            'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]'">Activity</button>
                </li>
                <li class="mx-2 mt-2">
                    <button x-on:click="active = 'Xstay'" class="inline-block py-2 px-9 rounded-lg active"
                        :class="active == 'Xstay' ? 'text-white bg-[#23AEC1]' :
                            'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]'">Xstay</button>
                </li>
                <li class="mx-2 mt-2">
                    <button x-on:click="active = 'Wisata'" class="inline-block py-2 px-9 rounded-lg active"
                        :class="active == 'Wisata' ? 'text-white bg-[#23AEC1]' :
                            'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]'">Wisata</button>
                </li>
            </ul>
        </div>
    
        <div x-show="active === 'Tur'" class="my-5 swiper carousel-kota-swiper">
            <div class="py-5 mx-auto swiper-wrapper">
                @if (json_decode($tur) != null)
                    @foreach (json_decode($tur) as $act)
                        <div class="flex justify-center swiper-slide">
                            <x-destindonesia.card-produk :act="$act" :type="'tur'" />
                        </div>
                    @endforeach
                @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Product tidak ditemukan
                    </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination -mb-2"></div>
        </div>
        <div x-show="active === 'Transfer'" class="my-5 swiper carousel-kota-swiper">
            <div class="py-5 mx-auto swiper-wrapper">
                {{-- @dump($transfer); --}}
                @if (json_decode($transfer) != null)
                    @foreach (json_decode($transfer) as $act)
                        <div class="flex justify-center swiper-slide">
                            <div class="overflow-hidden bg-white border-2 border-solid rounded-lg shadrop-shadow-xl border-[#9E3D64]">
                                <div class="h-56 p-4 bg-center bg-cover"
                                    style="background-image: url({{ !empty(json_decode($act->productdetail->gallery)) ? asset(json_decode($act->productdetail->gallery)[0]->gallery) : 'https://via.placeholder.com/450x450'}})">
                                    <div class="flex justify-end">
                                        <svg class="w-6 h-6 text-white hover:fill-red-500" id="{{$act->id}}"
                                            onclick="submitLike2({{$act->id}})" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                            <path>
                                                d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z">
                                            </path>
                                        </svg>
                                    </div>
                                    {{-- <div class="grid mt-36 place-content-end">
                                        <span class="bg-[#D50006] py-[2px] px-[5px] rounded-sm uppercase text-white font-bold ">16%
                                            off</span>
                                    </div> --}}
                                </div>
                                <div class="p-4 border-t-2 border-[#9E3D64]">
                                    <a href="{{ route('transfer.show', $act->slug) }}">
                                        <p class="text-[18px] font-bold tracking-wide text-[#D50006] uppercase">{{ $act->product_name }}
                                        </p>
                                    </a>
                                    @php
                                    $rating = App\Models\reviewPost::where('product_id', $act->id)->sum('star_rating');
                                    $review_count = App\Models\reviewPost::where('product_id', $act->id)->get();
                                    @endphp
                                    <div class="inline-flex">
                                        <img src="{{ asset('storage/icons/Star 1.png') }}" class="w-[15px] h-[15px] mt-[0.2rem] mr-1"
                                            alt="polygon 3" title="Kamtuu">
                                        <p>{{ count($review_count) == 0 ? 0 : $rating/count($review_count) }} ({{ count($review_count)
                                            }}
                                            review)</p>
                                    </div>
                                </div>
                                <div class="p-4 text-gray-700 border-t border-gray-300 ">
                                    <div class="items-center flex-1 py-2">
                                        <p class="text-[14px] text-[#333333]">Dimulai dari:
                                            <x-destindonesia.tag-start-place :act="$act"></x-destindonesia.tag-start-place>
                                        </p>
                                    </div>
                                    {{-- <div class="items-center flex-1 py-2">
                                        <p class="text-[14px] text-[#333333]">Destinasi:
                                            <x-destindonesia.tag-destinasi></x-destindonesia.tag-destinasi>
                                        </p>
                                    </div> --}}
                                    {{-- <div class="items-center flex-1 py-2">
                                        <p class="text-[14px] text-[#333333]">Jenis:
                                            <x-destindonesia.tag-jenis :act="$act"></x-destindonesia.tag-jenis>
                                        </p>
                                    </div> --}}
                
                                </div>
                                <div class="px-4 pt-3 pb-4 bg-gray-100 border-t border-gray-300">
                                    <div class="text-xs font-bold tracking-wide text-gray-600 uppercase">Harga</div>
                                    <div class="grid items-center justify-end grid-cols-2 pt-2">
                                        <div>
                                            {{-- <p class="text-[12px] line-through text-[#D50006]">IDR 4.250.000,-</p> --}}
                                            <p class="text-[16px] text-[#333333]">IDR {{
                                                number_format($act->productdetail->harga->dewasa_residen ??
                                                $act->productdetail->detailkendaraan->harga_akomodasi) }}
                                            </p>
                
                                        </div>
                
                                        <div class="grid justify-items-end">
                                            <a href="#">
                                                <div class="grid w-12 h-12 mr-3 bg-center bg-cover rounded-full"
                                                    style="background-image: url({{ isset($act->user->profile_photo_path) ? $act->user->profile_photo_path : 'https://via.placeholder.com/50x50' }})">
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Product tidak ditemukan
                    </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination -mb-2"></div>
        </div>
        <div x-show="active === 'Rental'" class="my-5 swiper carousel-kota-swiper">
            <div class="py-5 mx-auto swiper-wrapper">
                @if (json_decode($rental) != null)
                    @foreach (json_decode($rental) as $act)
                        <div class="flex justify-center swiper-slide">
                            <x-destindonesia.card-produk :act="$act" :type="'rental'" />
                        </div>
                    @endforeach
                @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Product tidak ditemukan
                    </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination -mb-2"></div>
        </div>
        <div x-show="active === 'Hotel'" class="my-5 swiper carousel-kota-swiper">
            <div class="py-5 mx-auto swiper-wrapper">
                @if (json_decode($hotel) != null)
                    @foreach (json_decode($hotel) as $act)
                        <div class="flex justify-center swiper-slide">
                            <x-destindonesia.card-produk :act="$act" :type="'hotel'" />
                        </div>
                    @endforeach
                @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Product tidak ditemukan
                    </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination -mb-2"></div>
        </div>
        <div x-show="active === 'Xstay'" class="my-5 swiper carousel-kota-swiper">
            <div class="py-5 mx-auto swiper-wrapper">
                @if (json_decode($xstay) != null)
                    @foreach (json_decode($xstay) as $act)
                        <div class="flex justify-center swiper-slide">
                            <x-destindonesia.card-produk :act="$act" :type="'xstay'" />
                        </div>
                    @endforeach
                @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Product tidak ditemukan
                    </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination -mb-2"></div>
        </div>
        <div x-show="active === 'Activity'" class="my-5 swiper carousel-kota-swiper">
            <div class="py-5 mx-auto swiper-wrapper">
                @if (json_decode($activity) != null)
                    @foreach (json_decode($activity) as $act)
                        <div class="flex justify-center swiper-slide">
                            <x-destindonesia.card-produk :act="$act" :type="'activity'" />
                        </div>
                    @endforeach
                @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Product tidak ditemukan
                    </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination -mb-2"></div>
        </div>
        </div>

    {{-- Ulasan --}}
    <div class="container mx-auto">
        @if(isset($reviews->star_rating))
        <div class="container">
            <div class="row">
                <div class="col mt-4">
                    <p class="font-weight-bold ">Review</p>
                    <div class="form-group row">
                        <input type="hidden" name="detailObjek_id" value="{{ $reviews->id }}">
                        <div class="col">
                            <div class="rated">
                                @for($i=1; $i<=$reviews->star_rating; $i++)
                                    <input type="radio" id="star{{$i}}" class="rate" name="rating" value="5" />
                                    <label class="star-rating-complete" title="text">{{$i}} stars</label>
                                    @endfor
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mt-4">
                        <div class="col">
                            <p>{{ $reviews->comments }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="container">
            <div class="row">
            </div>
        </div>
        <div>
            <div class="container max-w-6xl p-4 ">

                <div class="flex mb-4">
                    <div class="col mt-4 w-full">
                        <form class="py-2 px-4 w-full" action="{{route('reviewObjek.store')}}"
                            style="box-shadow: 0 0 10px 0 #ddd;" method="POST" autocomplete="off" id="review-regencies-object">
                            @csrf
                            <p class="font-weight-bold ">Ulasan</p>
                            <div class="form-group row">
                                <input type="hidden" name="detailObjek_id" value="{{ $pulaus->id }}">
                                <div class="col">
                                    <div class="rate">
                                        <input type="radio" id="star5" class="rate" name="rating" value="5" />
                                        <label for="star5" title="text">5 stars</label>
                                        <input type="radio" checked id="star4" class="rate" name="rating" value="4" />
                                        <label for="star4" title="text">4 stars</label>
                                        <input type="radio" id="star3" class="rate" name="rating" value="3" />
                                        <label for="star3" title="text">3 stars</label>
                                        <input type="radio" id="star2" class="rate" name="rating" value="2">
                                        <label for="star2" title="text">2 stars</label>
                                        <input type="radio" id="star1" class="rate" name="rating" value="1" />
                                        <label for="star1" title="text">1 star</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mt-4" x-data="imageUpload">
                                <div class="col">
                                    <textarea
                                        class=" form-control w-full flex-auto block p-4 font-medium border border-transparent rounded-lg outline-none focus:border-[#9E3D64] focus:text-green-500"
                                        name="comment" rows="6 " placeholder="Berikan Ulasan Anda"
                                        maxlength="200"></textarea>
                                    <input type="file" name="images[]" class="form-control w-full flex-auto block border border-transparent outline-none focus:border-[#9E3D64] focus:text-green-500" accept="image/*" @change="selectedFile" multiple>
                                    <template x-if="imgDetail.length >= 1">
                                        <div class="flex justify-start items-center mt-2">
                                            <template x-for="(detail,index) in imgDetail" :key="index">
                                                <div class="flex justify-center items-center">
                                                    <img :src="detail"
                                                        class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                        :alt="'upload'+index">
                                                    <button type="button"
                                                        class="absolute mx-2 translate-x-12 -translate-y-14">
                                                        <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                            alt="" width="25px" @click="removeImage(index)">
                                                    </button>
                                                </div>
                                            </template>
                                        </div>
                                    </template>                                         
                                </div>
                            </div>
                            <div class="mt-3 text-right">
                                <button class="py-2 px-3 rounded-lg bg-kamtuu-second text-white">Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="grid grid-cols-2 p-5">
                    <div class="flex">
                        <img src="{{ asset('storage/icons/Star 1.png') }}"
                            class="lg:w-[48px] lg:h-[48px] mt-[0.2rem] mr-1 inline-flex" alt="rating" title="Rating">
                        <p class="flex"><span class="lg:text-[50px] font-semibold ">{{$ratings}}</span> <span
                                class="lg:text-[20px] lg:pt-9"> /5.0 dari {{$var2}} ulasan</span></p>
                    </div>
                </div>

                {{-- ulasan desktop --}}

                @foreach($reviews as $review)
                <div class="hidden lg:block md:block">
                    <div class="grid grid-row-5 rounded shadow-lg justify-items justify-items-start">
                        <div class="grid grid:row-2 grid-cols-10 gap-2 pl-3 mt-7">
                            @if(isset($review->profile_photo_path))
                            <img class="w-20 h-20 rounded-full bg-gray-400" scr="{{isset($review->profile_photo_path) ? asset($review->profile_photo_path):null}}">
                            @else
                            <div class="w-20 h-20 rounded-full bg-gray-400 relative">
                                @php
                                    $name = $review->first_name;
                                    $exp_name = explode(' ',$name);
                                    $inisial ="";

                                    $inisial = substr($exp_name[0],0,1);

                                    if(count($exp_name) > 1){
                                        $inisial .=substr(end($exp_name),0,1);
                                    }
                                @endphp
                                @if(count($exp_name) > 1)
                                <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 13px;">{{$inisial}}</p>
                                @else
                                    <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 26px;">{{$inisial}}</p>
                                @endif
                            </div>
                            @endif
                            <div class="col-span-2 my-auto">
                            <p class="font-semibold text-gray-900">{{$review->first_name}}<p/>
                            <p class="font-light text-gray-600 text-xs">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}<p/>
                            </div>
                        </div>
                        <div class="grid grid-cols-5 pl-3 mb-2">
                            <div class="col-span-5">
                                <img class="w-[20px] h-[20] mt-[0.3rem] mr-1  ml-3 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                @for ($i = 0; $i < ($review->star_rating-1); $i++)
                                <img class="w-[20px] h-[20] mt-[0.3rem] mr-1 -ml-2 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                @endfor
                            </div>
                        </div>
                        <div class="mt-3 pl-3">
                            <button type="button" id="show-btn-ulasan" class="px-3 py-2 text-black text-sm underline">Tampilkan</button>
                            <button type="button" id="hide-btn-ulasan" class="px-3 py-2 text-black text-sm underline hidden">Sembunyikan</button>
                        </div>
                        @if($review->gallery_post)
                        <div class="flex justify-start justify-items-center pl-4 pb-4">
                            @php
                                $post_gallery = json_decode($review->gallery_post, true);
                            @endphp
                            <img class="w-50 h-40 bg-slate-400 rounded-lg" src="{{$post_gallery['result'] != null ? Storage::url('gallery_post/'.$post_gallery['result'][0]):'https://via.placeholder.com/540x540'}}">
                            <div class="relative" x-data="{open:false}" @keydown.escape="open = false">
                                <button @click="open=true" type="button" id="hide-btn-foto-ulasan" 
                                                            class="absolute px-3 py-2 text-black text-sm underline" style="width:169px;top:110px">
                                                            Tampilkan semua foto
                                </button>
                                <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                                    style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                                <div
                                    class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                                        <button
                                            class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                            style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                            <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                                        </button>
                                </div>
                                <div class="h-full w-full flex items-center justify-center overflow-hidden"
                                    x-data="{active: 0, slides: {{ ($review->gallery_post) }} }">
                                    <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                        <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                            <button type="button"
                                                class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                style="background-color: rgba(230, 230, 230, 0.4);"
                                                @click="active = active === 0 ? slides.length - 1 : active - 1">
                                                <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                            </button>
                                        </div>
                                    </div>
                                    @if ($post_gallery)
                                        @foreach ($post_gallery['result'] as $index=>$gallery)
                                            <div class="h-full w-full flex items-center justify-center absolute">
                                                <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                                    x-show="active === {{ $index }}"
                                                    x-transition:enter="transition ease-out duration-150"
                                                    x-transition:enter-start="opacity-0 transform scale-90"
                                                    x-transition:enter-end="opacity-100 transform scale-100"
                                                    x-transition:leave="transition ease-in duration-150"
                                                    x-transition:leave-start="opacity-100 transform scale-100"
                                                    x-transition:leave-end="opacity-0 transform scale-90">

                                                    <img src="{{ Storage::url('gallery_post/'.$gallery) }}"
                                                        class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                                </div>
                                                <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                                x-show="active === {{ $index }}">
                                                    <span class="w-12 text-right" x-text="{{ $index }} + 1"></span>
                                                    <span class="w-4 text-center">/</span>
                                                    <span class="w-12 text-left"
                                                        x-text="{{ count($post_gallery['result']) }}"></span>
                                                </div>
                                            </div>
                                        @endforeach
                                        @endif
                                                                        
                                        <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                            <div class="flex items-center justify-start w-12 md:ml-16">
                                                <button type="button"
                                                    class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                    style="background-color: rgba(230, 230, 230, 0.4);"
                                                    @click="active = active === slides.length - 1 ? 0 : active + 1">
                                                    <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                                </button>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>

                        </div>
                        @endif
                    </div>
                </div>

                {{-- ulasan mobile --}}
                <div class="block lg:hidden md:hidden">
                    <div
                        class="grid grid-cols-1 divide-y rounded-lg shadow-xl justify-items-center border border-gray-300">
                        <div class="grid p-5 justify-items-center">
                            <p class="text-sm md:text-[18px] font-bold my-3">{{$review->frist_name}}</p>
                            <p class="text-sm md:text-[18px] -mt-2">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}</p>
                            <div class="flex p-2">
                                <img src="{{ asset('storage/icons/Star 1.png') }}"
                                    class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex" alt="rating" title="Rating">
                                <p class="flex pt-3"><span
                                        class="text-[16px] font-semibold ">{{$review->star_rating}}.0</span>
                                    <span class="text-[16px]"> /5.0</span>
                                </p>
                            </div>
                        </div>

                        <div class="grid justify-start justify-items-center p-5">
                            <p class="text-sm md:text-[18px]">{{isset($review->comments) ? $review->comments :'Lorem ipsum dolor sit amet consectetur adipisicing
                                elit.
                                Vel placeat, tenetur dolores enim quae quod voluptates dolorem explicabo repellendus
                                ratione voluptatibus aliquam exercitationem hic adipisci sint quibusdam, eos, at
                                quas.'}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
    </div>

    {{-- Ads Mobile --}}
    <div class="block mt-10 sm:hidden">
        <p class="mb-3 text-xl font-semibold text-center sm:text-left">Article (Ads)</p>
        <x-destindonesia.ads-list></x-destindonesia.ads-list>
    </div>


    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>
</body>
<script>

    function kategoriWisata(){
        var objs =  {!! isset($collectWisata) ? $collectWisata:'[]' !!}
        var obj  = [];

        $.each(objs, function(key, value){
           
        });


        
        return{
            tabs:{!! isset($kategoriWisata) ? $kategoriWisata:'[]' !!},
            active:0,
            change(index){
                this.ajaxRequest(this.tabs[index])
                // let datas = {!! isset($collectWisata) ? $collectWisata:'[]' !!}
                // if(this.tabs[index]=='All'){
                //     console.log(Apline.store('wisata').collect_wisata)
                    
                // }else{
                    
                //     var newObj = Apline.store('wisata').collect_wisata.filter(element=>element.kategori==this.tabs[index]);
                // }

            },
            remove(data){
                let count = data.length;

                for(let i = 0; i < count;i++){
                    data.splice(i,1)
                }

                return data;
            },
            ajaxRequest(param){

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })

                $.ajax({
                    method:'GET',
                    url:"{{route('object.by-category')}}",
                    // dataType:"html",
                    data:{
                        category:param,
                        kabupaten_id:'{!! $pulaus->kabupaten_id !!}',
                        objek:'kabupaten'
                    },
                    success:function(resp){
                        console.log(resp)
                        console.log($("#objek-wisata"))
                        $("#objek-wisata").hide(); 
                        $("#objek-wisata-filter").show();
                        $("#objek-wisata-filter").html(resp)             
                    }
                })
            }
        }
    }
</script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

    function inginKesana(){
        $.ajax({
            type:'POST',
            url:"{{route('ingin-kesana-submit')}}",
            cache: false,
            dataType:"json",
            data:{
                objek:'kabupaten'
            },
            success:function(resp){
                var json = resp;
                if(resp.succses){
                    var count = json.data;
                    console.log(count)
                    $("#dropdown").hide();
                    $("#been-there").html(`${count} Orang`)
                }
            },
            error:function(data){
                console.log("error:",data)
            }
        })
    }

    function pernahKesana(){
        $.ajax({
            type:'POST',
            url:"{{route('pernah-kesana-submit')}}",
            cache: false,
            dataType:"json",
            data:{
                objek:'kabupaten'
            },
            success:function(resp){
                var json = resp;
                if(resp.succses){
                    var count = json.data;
                    console.log(count)
                    $("#dropdown").hide();
                    $("#pijakan").html(`${count} Traveler`)
                }
            },
            error:function(data){
                console.log("error:",data)
            }
        })
    }

    $("#dropdownDefaultButton").on("click",function(){
        $("#dropdown").toggle()
    })
    
    $("#ingin-kesana").on("click",function(){
        inginKesana();
    });
    
    $("#pernah-kesana").on("click",function(){
        pernahKesana()
    });
</script>
<script>
    $(document).ready(function(){
                
        function submitValidate(e, msg){
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'error',
                title: msg
            })
                
            e.preventDefault()
        }

        $('#review-regencies-object').on("submit",function(e){
            let role="{{auth()->user() != null ? auth()->user()->role:null}}";

            console.log(role)
            if(!role){
                submitValidate(e, 'Silahkan login terlebih dahulu')
            }
                    
            else if(role!='traveller'){
                submitValidate(e, 'Silahkan login sebagai traveller terlebih dahulu')
            }

            else if(!$("#comments").val()){
                submitValidate(e, 'Isi komentar tidak kosong')
            }
            
            //e.preventDefault();
        })
    })

    function imageUpload(){
        return{
            imageUrl:'',
            imgDetail:[],
            selectedFile(event){
                this.fileToUrl(event)
            },
            fileToUrl(event){
                if (!event.target.files.length) return
                    let file = event.target.files
                        
                    for (let i = 0; i < file.length; i++) {
                        let reader = new FileReader();
                        let srcImg = ''
                        this.imgDetail = []
                        reader.readAsDataURL(file[i]);
                        reader.onload = e => {
                            srcImg = e.target.result
                            this.imgDetail = [...this.imgDetail, srcImg]
                        };
                    }
                },
            removeImage(index){
                this.imgDetail.splice(index, 1)
            }
        }
    }
</script>

</html>