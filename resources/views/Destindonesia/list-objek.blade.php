<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! seo($page ?? null) !!}
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <div x-data="cardListWisata()">
        <header>
            <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
            {{-- Destindonesia NavBar --}}
            <div
                class="bg-transparent lg:container mx-auto px-2 h-[75px] lg:h-[100px] border-y-[1px] border-[#F2F2F2] hidden sm:block relative z-10">
                <div class="grid grid-cols-6 h-full">
                    <a href="#"
                        class="text-lg md:text-xl lg:text-3xl text-[#9E3D64] font-extrabold lg:font-semibold my-auto">DestIndonesia</a>
                    <a href="#"
                        class="text-sm lg:text-base hover:underline flex justify-center my-auto text-[#333333]">Destinasi</a>
                    <a href="#"
                        class="text-sm lg:text-base hover:underline flex justify-center my-auto text-[#333333]">Newsletter</a>
                    <a href="#"
                        class="text-sm lg:text-base hover:underline flex justify-center my-auto text-[#333333]">Event</a>
                    <a href="#"
                        class="text-sm lg:text-base hover:underline flex justify-center my-auto text-[#333333]">Videos</a>
                    <div class="flex justify-center my-auto bg-white rounded-full border">
                        <input
                            class="placeholder:italic placeholder:text-slate-400 block bg-white w-full border border-transparent rounded-full py-1 lg:py-2 pl-5 pr-3 shadow-sm focus:outline-none focus:border-transparent focus:ring-transparent focus:ring-1 sm:text-sm"
                            placeholder="Search" type="text" name="search" id="search-object"/>
                        <button class="mx-3"><img src="{{ asset('storage/icons/search-ic.png') }}" alt="" id="btn-object-search"></button>
                    </div>
                </div>
            </div>
            <nav class="flex ml-9 mt-2" aria-label="Breadcrumb">
                <ol class="inline-flex items-center space-x-1 md:space-x-3 text-base lg:text-lg">
                    <li class="inline-flex items-center">
                        <a href="{{route('destindonesia.index')}}"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] capitalize">Destindonesia</a>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            <a href="{{route('destindonesia.list')}}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] capitalize">List</a>
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            <a href="#"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1] capitalize">List Objek</a>
                        </div>
                    </li>
                </ol>
            </nav>
        </header>
        {{-- Hero --}}
        <div class="relative">
            {{-- Kabupaten/Kota Name --}}
            <div class="mt-8">
                <p class="text-lg lg:text-6xl text-center text-[#333333] font-bold">Indonesia</p>
                <p class="text-lg lg:text-xl text-center text-[#9E3D64] font-bold">Temukan Destinasimu</p>
            </div>
        </div>
        <div class="container mx-auto my-5">
            <p class="mx-3 text-xl sm:text-3xl font-semibold my-3 text-center sm:text-left">Objek Wisata</p>
            <div class="flex sm:block mx-5">
                <ul class="flex justify-start overflow-x-auto text-sm font-medium text-center text-gray-500 dark:text-gray-400">
                    <template x-for="(tab, index) in tabs" :key="index">
                        <li class="mx-2 mt-2">
                            <button x-text="tab" class="inline-block py-2 px-9 h-full text-white bg-[#23AEC1] rounded-lg"
                            x-on:click="active=index" :class="{'text-white bg-[#23AEC1]':active==index,'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]':active!==index}" @click="change(index)">
                            </button>
                        </li>
                    </template>
                </ul>
            </div>
        </div>
        {{-- Terbaru dan Ads Desktop --}}
        <div class="container mx-auto grid grid-cols-4 gap-5 my-5">

            {{-- Ads Desktop --}}
            <div class="hidden sm:block">
                <p class="mx-5 text-3xl font-semibold my-3">Article (Ads)</p>
                <div class="w-[306px] shadow-xl divide-y rounded-lg p-5 mx-5 my-5">
                    @foreach ($news as $iklan => $item)
                    {{-- {{ dd($item) }} --}}
                    <div class="grid grid-cols-2">
                        <div class="p-3">
                            <img src="{{ isset($item->header) ? Storage::url($item->header) : 'https://via.placeholder.com/109x109' }}"
                                alt="tailwind logo" class="rounded-xl object-cover object-center" />
                        </div>

                        <a href="{{ route('newsletter.show', $item->slug) }}" class="p-3 ">
                            <h2 class="font-bold">{{ $item->title }}</h2>
                            <div class="truncate">
                                {!! $item->description !!}
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
                <p class="mx-5 text-xl font-semibold my-3">List Kota/Kabupaten</p>
                <div class="mx-5 text-md">
                    <ol>
                        <input type="hidden" id="category-wisata" value="All">
                        <template x-for="(regency, index) in regencies" :key="index">
                            <li>
                                <input :id="'regency-'+regency.id" type="checkbox" name="list-kota[]" :value="regency.id"
                                            class="border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600" @click="getList(regency.id)">
                                <label for="country-option-1" class="ml-2 text-sm font-medium text-gray-900" x-text="regency.name"></label>   
                            </li>
                        </template>    
                    </ol>
                </div>
            </div>
            {{-- Top Destinasi --}}
            <div class="col-span-4 lg:col-span-3 mx-2">
                <div class="mt-2">
                    <p class="text-lg lg:text-2xl text-center text-[#333333] font-bold">Top Destinasi di Indonesia</p>
                    <p class="text-md lg:text-lg text-center text-[#BDBDBD] font-semibold">By Kamtuu</p>
                </div>
                <div id="list-card">
                    <div class="grid lg:grid-cols-3 lg:grid-rows-none grid-rows-3 gap-2 lg:gap-5 lg:my-5 my-0 lg:mt-5 mt-5">
                        <template x-if="lists.length >=1">
                            <template x-for="(list, index)  in lists">
                                <div class="hover:shadow-lg hover:-translate-y-2">
                                    <a class="lg:block hidden" x-bind:href="`wisata/${list.slug}`">
                                        <div class="px-4 absolute h-20 items-center justify-center flex">
                                            <p
                                                class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white shadow-md font-bold text-[#D50006]" x-text="index+1">
                                            </p>
                                        </div>
                                        <div
                                            class="max-w-sm w-[150px] lg:w-full bg-white rounded-lg border border-gray-200 shadow dark:border-gray-700">
                                            <img class="rounded-t-lg h-[201px] w-[301px]" :src="list.header" alt="">
                                            <div class="p-2 lg:p-4">
                                                <h5
                                                    class="mb-px text-sm lg:text-xl font-bold tracking-tight text-[#333333] hover:text-[#9E3D64]" x-text="list.nama">
                                                </h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </template>
                        </template>
                        <template x-if="lists.length == 0">
                            <div class="text-center">
                                <p>
                                    Belum Ada Objek
                                </p>
                            </div>
                        </template>
                        <template x-if="lists.length >=1">
                            <template x-for="(list, index)  in lists">
                                    <a x-bind:href="`wisata/${list.slug}`"
                                    class="lg:hidden block flex flex-cols bg-white rounded-lg border shadow-md hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700">
                                    <div class="lg:hidden block px-4 my-2 absolute w-52 h-14 items-center justify-center flex">
                                        <p
                                            class="inline-flex  items-center justify-center lg:text-lg text-sm w-6 h-6 rounded-full lg:w-8 lg:h-8 bg-white shadow-md font-bold text-[#D50006]" x-text="index+1">
                                        </p>
                                    </div>
                                    <img class="object-cover h-[70px] w-[100px] rounded-l-lg"
                                        :src="list.header" alt="">
                                    <div class="p-2 px-4 lg:px-px lg:p-4">
                                        <h5
                                            class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-[#f8f5f5] hover:text-[#9E3D64]" x-text="list.nama">
                                        </h5>
                                    </div>
                                </a>
                            </template>
                        </template>
                        <template x-if="lists.length == 0">
                            <div class="text-center lg:hidden block">
                                <p>
                                    Belum Ada Objek
                                </p>
                            </div>
                        </template>
                    </div>
                        <nav aria-label="Page navigation" class="text-end mt-3 lg:block hidden">
                            <template x-if="links.length > 1">
                                <div class="flex flex-row justify-between gap-x-36">
                                    <span class="text-sm text-gray-700 dark:text-gray-400">
                                        Showing <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="from"></span> to <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="to">10</span> of <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="total_page">100</span> Entries
                                    </span>
                                    <ul class="inline-flex -space-x-px text-sm">
                                        <li>
                                            <button type="button" class="rounded-l-lg flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white capitalize" 
                                            :class="{ 'dark:bg-gray-600 text-gray-200':links[0].label == cur_page, 'bg-red-100 text-gray-400':links[0].label != cur_page}"
                                            x-text="prev" @click="prevPage(links[0].url,links[0].label)"></button>
                                        </li>
                                        <template x-for="(link, index) in links" :key="index">
                                            <li>
                                                <template x-if="link.label >= 1">
                                                    <button type="button" class="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white" x-text="link.label" :class="{ 'dark:bg-gray-600 text-gray-200':link.label == cur_page, 'bg-red-100 text-gray-400':link.label != cur_page}" @click="getPage(link.url,link.label)" :id="'page-'+link.label"></button>
                                                </template>
                                                <template x-if="link.label == '...'">
                                                    <button type="button" class="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400" x-text="link.label" :class="{ 'dark:bg-gray-600 text-gray-200':link.label == cur_page, 'bg-red-100 text-gray-400':link.label != cur_page}" @click="getPage(link.url,link.label)" :id="'page-'+link.label" disabled></button>
                                                </template>
                                            </li>
                                        </template>
                                        <li>
                                            <button type="button" class="rounded-r-lg flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white capitalize" x-text="next" 
                                            :class="{ 'dark:bg-gray-600 text-gray-200':links[(links.length*1)-1].label == cur_page, 'bg-red-100 text-gray-400':links[(links.length*1)-1].label != cur_page}"
                                            :class="{'cursor-not-allowed':links[(links.length*1)-1].url==null,'cursor-pointer':links[(links.length*1)-1].url!=null}" 
                                            @click="nextPage(links[(links.length*1)-1].url,links[(links.length*1)-1].label)"></button>
                                        </li>
                                    </ul>
                                </div>
                                
                            </template>
                        </nav> 
                        <nav aria-label="Page navigation" class="text-center mt-3 lg:hidden block">
                            <template x-if="links.length > 1">
                                <div class="flex flex-col items-center">
                                    <span class="text-sm text-gray-700 dark:text-gray-400">
                                        Showing <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="from"></span> to <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="to">10</span> of <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="total_page">100</span> Entries
                                    </span>
                                    <div class="inline-flex mt-2 text-sm">
                                        <button type="button" class="rounded-l-lg flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white capitalize" 
                                            :class="{ 'dark:bg-gray-600 text-gray-200':links[0].label == cur_page, 'bg-red-100 text-gray-400':links[0].label != cur_page}"
                                            x-text="prev" @click="prevPage(links[0].url,links[0].label)"></button>
                                        <button type="button" class="rounded-r-lg flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white capitalize" x-text="next" 
                                            :class="{ 'dark:bg-gray-600 text-gray-200':links[(links.length*1)-1].label == cur_page, 'bg-red-100 text-gray-400':links[(links.length*1)-1].label != cur_page}"
                                            :class="{'cursor-not-allowed':links[(links.length*1)-1].url==null,'cursor-pointer':links[(links.length*1)-1].url!=null}" 
                                            @click="nextPage(links[(links.length*1)-1].url,links[(links.length*1)-1].label)"></button>
                                        
                                    </div>
                                </div>
                            </template>            
                        </nav> 
                    <div class="hidden grid lg:grid-cols-3 lg:grid-rows-none grid-rows-3 gap-2 lg:gap-5 lg:my-5 my-0 lg:mt-5 mt-5" id="list-card-new">
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Ads Mobile --}}
    <div class="block sm:hidden mt-10">
        <p class="text-xl font-semibold mb-3 text-center sm:text-left">Article (Ads)</p>
        <x-destindonesia.ads-list></x-destindonesia.ads-list>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>
</body>
<script>

    let url = "/destindonesi/filter/card-cat/list/";
    let id  = "{{$id}}";

    let listArray = [];
    let kategori_wisata = '';

    $("#btn-object-search").on("click",function(){
        let cari = $('#search-object').val();
        let url = "{{route('search.destindo')}}";
        window.location.replace(`${url}?cari=${cari}`);
    });

    document.addEventListener("alpine:init",()=>{
        Alpine.data("cardListWisata",()=>({
            lists:[],
            links:[],
            url:url,
            total_page:'',
            active:'',
            count_data:null,
            current_page:null,
            cur_page:null,
            prev:null,
            next:null,
            query_cat:'All',
            active:0,
            query_reg:null,
            tabs:{!! isset($collect) ? $collect:'[]' !!},
            regencies:{!! isset($collect_list) ? $collect_list:'[]' !!},
            arr_value:[],
            next_page:1,
            prev_page:0,
            firs_page:null,
            last_page:null,
            to:'',
            from:'',
            async init(){
                var category = $("#category-wisata").val();
               
                if(category != this.query_cat){
                    this.query_cat = $("#category-wisata").val()
                }

                let listWisata = await fetch(`${this.url}${id}`);
                let lists =  await listWisata.json();
               
                let list_arr = [];
                let links_arr = [];

                Object.keys(lists.datas.data).forEach(key=>{
                    
                    this.lists.push({
                        'id':lists.datas.data[key].id,
                        'title':lists.datas.data[key].title,
                        'slug':lists.datas.data[key].slug,
                        'nama':lists.datas.data[key].namaWisata,
                        'thumbnail':lists.datas.data[key].thumbnail,
                        'deskripsi':lists.datas.data[key].deskripsi,
                        'kategori':lists.datas.data[key].kategori,
                        'hari':lists.datas.data[key].hari,
                        'hargaTiket':lists.datas.data[key].hargaTiket,
                        'jamOperasional':lists.datas.data[key].jamOperasional,
                        'transportasi':lists.datas.data[key].transportasi,
                        'gallery':lists.datas.data[key].galleryWisata,
                        'embed_maps':lists.datas.data[key].embed_maps,
                        'header':lists.datas.data[key].header,
                    })                    
                })

                Object.keys(lists.datas.links).forEach(key=>{
                    links_arr.push(lists.datas.links[key])
                })

                this.links = links_arr;
                last_index = (this.links.length*1)-1;

                this.prev = this.links[0].label.replace('pagination.previous','previous');
                this.next = this.links[last_index].label.replace('pagination.next','next');

                this.total_page = lists.datas.total;
                this.cur_page = lists.datas.current_page
                this.count_datas = this.total_page

                this.from = lists.datas.from;
                this.to = lists.datas.to;

                this.first_page = 1
                this.last_page = lists.datas.last_page
                console.log(this.from,this.to)

            },
            getList(id){
               
                let checked =  document.getElementById('regency-'+id).checked;
                let value =  document.getElementById('regency-'+id).value;
                
                if(checked){
                    
                    this.arr_value.push({
                        id:value*1
                    });

                    listArray = this.arr_value;
                    console.log(listArray)
                }
                if(!checked){
                    let data_array = this.arr_value.filter(element => element.id!=value);

                    listArray = data_array;
                    this.arr_value = data_array;
                }
                
                this.changeFilter(kategori_wisata);
            },
            change(index){
                $("#category-wisata").val(this.tabs[index]);
                this.changeFilter(this.tabs[index])

            },
            changeFilter(kategori = "  "){
                let json = '';
                kategori_wisata = kategori;

                if(listArray.length > 0){
                    json = JSON.stringify(listArray) 
                }
                
                if(!kategori_wisata){
                    kategori_wisata ='All';
                }

                console.log(kategori_wisata)
                let changeCat = `${this.url}${id}/?regency_id=${json}&category=${kategori_wisata}`;
                this.dataFetching(changeCat);
            },
            getPage(page, label){
                this.cur_page = label;
                let json = '';
                let query_params =`${this.url}${id}`;

                if(listArray.length > 0){
                    json = JSON.stringify(listArray)    
                }
             
                if(page){
                    if(listArray.length == 0 && kategori_wisata == ' '){
                        query_params += `${page}`;
                    }else{
                        query_params +=`${page}&regency_id=${json}&category=${kategori_wisata}`
                    }
                }
                console.log(query_params)
                this.dataFetching(query_params);
                
            },
            nextPage(url, label){
                let json = '';
                let query_params = `${this.url}${id}`;

                if(listArray.length > 0){
                    json = JSON.stringify(listArray)    
                }

                document.getElementById("page-"+this.cur_page).classList.add("bg-red-100");
                document.getElementById("page-"+this.cur_page).classList.remove("text-gray-200");
                    
                if(this.last_page != this.next_page){
                    this.next_page++
                }

                this.cur_page +=1;
                let next = this.next_page;

                document.getElementById("page-"+next).classList.remove("bg-red-100");
                document.getElementById("page-"+next).classList.add("text-gray-200");

                query_params +=`/?page=${this.next_page}&regency_id=${json}&category=${kategori_wisata}`;
                this.dataFetching(query_params);
            },
            prevPage(url, label){
                let json = '';
                let query_params = `${this.url}${id}`;
                this.prev_page = this.next_page;

                if(listArray.length > 0){
                    json = JSON.stringify(listArray)    
                }

                document.getElementById("page-"+this.cur_page).classList.add("bg-red-100");
                document.getElementById("page-"+this.cur_page).classList.remove("text-gray-200");
               
                console.log(this.first_page,this.next_page)
                
                if(this.first_page != this.next_page){
                    this.next_page--
                }

                this.cur_page -=1

                document.getElementById("page-"+this.next_page).classList.remove("bg-red-100");
                document.getElementById("page-"+this.next_page).classList.add("text-gray-200");

                query_params +=`/?page=${this.next_page}&regency_id=${json}&category=${kategori_wisata}`;
                this.dataFetching(query_params);
            },
            dataFetching(params){
                fetch(params).
                then(resp => resp.json()).
                then(data=>{
                    this.lists = [];
                    this.links = [];

                    Object.keys(data.datas.data).forEach(key=>{
                        this.lists.push({
                            'id':data.datas.data[key].id,
                            'title':data.datas.data[key].title,
                            'slug':data.datas.data[key].slug,
                            'nama':data.datas.data[key].namaWisata,
                            'thumbnail':data.datas.data[key].thumbnail,
                            'deskripsi':data.datas.data[key].deskripsi,
                            'kategori':data.datas.data[key].kategori,
                            'hari':data.datas.data[key].hari,
                            'hargaTiket':data.datas.data[key].hargaTiket,
                            'jamOperasional':data.datas.data[key].jamOperasional,
                            'transportasi':data.datas.data[key].transportasi,
                            'gallery':data.datas.data[key].galleryWisata,
                            'embed_maps':data.datas.data[key].embed_maps,
                            'header':data.datas.data[key].header,
                        }) 
                    })

                    this.links=data.datas.links
                    this.count_datas = data.datas.total;
                    
                    this.total_page = data.datas.total;
                    this.from = data.datas.from;
                    this.to = data.datas.to;
                })
                
            }
        }))
    })  
</script>