<div class="w-full grid gap-5 grid-cols-2 sm:grid-cols-3 lg:grid-cols-4">
    @foreach ($wisata as $index=>$objek)
    <x-destindonesia.card-destination index="{{ $index }}" lokasi="wisata"
        text="{{ $objek->title }}" slug="{{ route('wisataShow', $objek->slug) }}"
        header="{{ $objek->header }}">
    </x-destindonesia.card-destination>
    @endforeach
</div>