<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! seo($page ?? null) !!}
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
        {{-- Destindonesia NavBar --}}
        <div
            class="bg-transparent lg:container mx-auto px-2 h-[75px] lg:h-[100px] border-b-[1px] border-white-600 hidden sm:block relative z-10">
            <div class="grid grid-cols-6 h-full">
                <a href="#"
                    class="text-lg md:text-xl lg:text-3xl text-white font-extrabold lg:font-semibold my-auto">DestIndonesia</a>
                <a href="#"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-white">Destinasi</a>
                <a href="{{ route('newsletter.destindonesia') }}"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-white">Newsletter</a>
                <a href="#"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-white">Event</a>
                <a href="#"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-white">Videos</a>
                <div class="flex justify-center my-auto bg-white rounded-full">
                    <input
                        class="placeholder:italic placeholder:text-slate-400 block bg-white w-full border border-transparent rounded-full py-1 lg:py-2 pl-5 pr-3 shadow-sm focus:outline-none focus:border-transparent focus:ring-transparent focus:ring-1 sm:text-sm"
                        placeholder="Search" type="text" name="search" id="search-input"/>
                    <button class="mx-3" id="btn-search">
                        <img src="{{ asset('storage/icons/search-ic.png') }}" alt="">
                    </button>
                </div>
            </div>
        </div>
    </header>

    {{-- Hero --}}
    <div class="relative sm:-translate-y-[100px]">
        <img class="w-full object-cover" src="{{ asset('storage/img/destIndonesia-1.png') }}" alt="">
        <div
            class="absolute translate-y-1/2 translate-x-1/2 bottom-1/2 right-1/2 sm:bottom-1/4 sm:right-[10%] lg:right-[15%]">
            <p class="text-sm lg:text-xl text-white text-center sm:text-right">WelKamtuu</p>
            <p class="text-lg sm:text-2xl lg:text-4xl text-white font-bold">INDONESIA</p>
        </div>
    </div>

    {{-- Terbaru dan Ads Desktop --}}
    <div class="container mx-auto grid sm:grid-cols-3 xl:grid-cols-4 gap-10 my-5">

        {{-- Terbaru --}}
        <div class="sm:col-span-2 xl:col-span-3">
            <p class="text-center sm:text-left text-xl sm:text-3xl font-semibold mt-10 mb-5 sm:my-3">Terbaru Untuk
                dikunjungi
            </p>
            {{-- Desktop  sm:grid grid-cols-3 grid-flow-row--}}
            <div class="hidden sm:grid grid-cols-2 gap-3">
                {{-- Photo 1 --}}
                {{-- <div class="bg-cyan-900 rounded-lg w-[450px] h-[250px] cursor-pointer"> --}}
                    <a href="{{ isset($objek1->slug) ? route('pulauShow', $objek1->slug) : '#'}}" class="relative bg-cyan-900 rounded-lg w-[450px] h-[250px] cursor-pointer">
                        <img class="hover:opacity-70 duration-200 object-cover w-full h-full rounded-lg"
                             src="{{ isset($objek1->header) ? Storage::url($objek1->header) : 'https://source.unsplash.com/600x400?nature' }}"
                        alt="x">
                        <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48 w-full">
                            <p class="text-2xl lg:text-3xl text-white sm:text-left font-bold">
                                {{ isset($objek1->nama_pulau) ? ($objek1->nama_pulau) : null }}</p>
                            <div class="hidden w-[411px] p-[6px] relative overflow-hidden text-ellipsis whitespace-normal backdrop-blur-ms bg-white/40 text-sm font-bold text-white" style="text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;"">{!! isset($objek1->desk_thumbnail) ? $objek1->desk_thumbnail : null !!} </div>
                        </div>        
                    </a>
                {{-- </div> --}}
                {{-- Photo 2 --}}
                {{-- <div class="bg-cyan-900 rounded-lg w-[450px] h-[250px] cursor-pointer"> --}}
                    <a href="{{ isset($objek2->slug) ? route('pulauShow', $objek2->slug) : '#'}}" class="relative bg-cyan-900 rounded-lg w-[450px] h-[250px] cursor-pointer">
                        <img class="hover:opacity-70 duration-200 object-cover w-full h-full rounded-lg"
                             src="{{ isset($objek2->header) ? Storage::url($objek2->header) : 'https://source.unsplash.com/600x400?nature' }}"
                        alt="x">
                        <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48 w-full">
                            <p class="text-2xl lg:text-3xl text-white sm:text-left font-bold">
                                {{ isset($objek2->nama_pulau) ? ($objek2->nama_pulau) : null }}</p>
                            <div class="hidden w-[411px] p-[6px] relative overflow-hidden text-ellipsis whitespace-normal backdrop-blur-ms bg-white/40 text-sm font-bold text-white" style="text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;"">{!! isset($objek2->desk_thumbnail) ? $objek2->desk_thumbnail : null !!} </div>
                        </div>        
                    </a>
                {{-- </div> --}}
                {{-- Photo 3 --}}
                {{-- <div class="bg-cyan-900 rounded-lg w-[450px] h-[250px] cursor-pointer"> --}}
                    <a href="{{ isset($objek3->slug) ? route('pulauShow', $objek3->slug) : '#'}}" class="relative bg-cyan-900 rounded-lg w-[450px] h-[250px] cursor-pointer">
                        <img class="hover:opacity-70 duration-200 object-cover w-full h-full rounded-lg"
                             src="{{ isset($objek3->header) ? Storage::url($objek3->header) : 'https://source.unsplash.com/600x400?nature' }}"
                        alt="x">
                        <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48 w-full">
                            <p class="text-2xl lg:text-3xl text-white sm:text-left font-bold">
                                {{ isset($objek3->nama_pulau) ? ($objek3->nama_pulau) : null }}</p>
                            <div class="hidden w-[411px] p-[6px] relative overflow-hidden text-ellipsis whitespace-normal backdrop-blur-ms bg-white/40 text-sm font-bold text-white" style="text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;">{!! isset($objek3->desk_thumbnail) ? $objek3->desk_thumbnail : null !!} </div>
                        </div>        
                    </a>
                {{-- </div> --}}
                {{-- Photo 4 --}}
                {{-- <div class="bg-cyan-900 rounded-lg w-[450px] h-[250px] cursor-pointer"> --}}
                    <a href="{{ isset($objek4->slug) ? route('pulauShow', $objek4->slug) : '#'}}" class="relative bg-cyan-900 rounded-lg w-[450px] h-[250px] cursor-pointer">
                        <img class="hover:opacity-70 duration-200 object-cover w-full h-full rounded-lg"
                             src="{{ isset($objek4->header) ? Storage::url($objek4->header) : 'https://source.unsplash.com/600x400?nature' }}"
                        alt="x">
                        <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48 w-full">
                            <p class="text-2xl lg:text-3xl text-white sm:text-left font-bold">
                                {{ isset($objek4->nama_pulau) ? ($objek4->nama_pulau) : null }}</p>
                            <div class="hidden w-[411px] relative overflow-hidden text-ellipsis whitespace-normal text-sm bg-clip-text bg-kamtuu-primary font-bold text-white">{!! isset($objek4->desk_thumbnail) ? $objek4->desk_thumbnail : null !!} </div>
                        </div>        
                    </a>
                {{-- </div> --}}
                {{-- Photo 5 --}}
                {{-- <div class="bg-cyan-900 rounded-lg w-[450px] h-[250px] cursor-pointer"> --}}
                    <a href="{{ isset($objek5->slug) ? route('pulauShow', $objek5->slug) : '#'}}" class="relative bg-cyan-900 rounded-lg w-[450px] h-[250px] cursor-pointer">
                        <img class="hover:opacity-70 duration-200 object-cover w-full h-full rounded-lg"
                             src="{{ isset($objek5->header) ? Storage::url($objek5->header) : 'https://source.unsplash.com/600x400?nature' }}"
                        alt="x">
                        <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48 w-full">
                            <p class="text-2xl lg:text-3xl text-white sm:text-left font-bold">
                                {{ isset($objek5->nama_pulau) ? ($objek5->nama_pulau) : null }}</p>
                            <div class="hidden w-[411px] p-[6px] relative overflow-hidden text-ellipsis whitespace-normal backdrop-blur-ms bg-white/40 text-sm font-bold text-white" style="text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;"">{!! isset($objek5->desk_thumbnail) ? $objek5->desk_thumbnail : null !!} </div>
                        </div>        
                    </a>
                {{-- Photo 6 --}}
                <a href="{{ isset($objek6->slug) ? route('pulauShow', $objek6->slug) : '#'}}" class="relative bg-cyan-900 rounded-lg w-[450px] h-[250px] cursor-pointer">
                    <img class="hover:opacity-70 duration-200 object-cover w-full h-full rounded-lg"
                         src="{{ isset($objek6->header) ? Storage::url($objek6->header) : 'https://source.unsplash.com/600x400?nature' }}"
                    alt="x">
                    <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48 w-full">
                        <p class="text-2xl lg:text-3xl text-white sm:text-left font-bold">
                            {{ isset($objek6->nama_pulau) ? ($objek6->nama_pulau) : null }}</p>
                        <div class="hidden w-[411px] p-[6px] relative overflow-hidden text-ellipsis whitespace-normal backdrop-blur-ms bg-white/40 text-sm font-bold text-white" style="text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;"">{!! isset($objek5->desk_thumbnail) ? $objek5->desk_thumbnail : null !!} </div>
                    </div>        
                </a>   
                {{-- </div> --}}
                {{-- <a  href="{{ isset($objek1->slug) ? route('pulauShow', $objek1->slug) : '#' }}"
                    class="relative bg-slate-400 cursor-pointer col-span-2 rounded-lg w-[500px] h-[250px]">
                    <img class="hover:opacity-70 duration-200 object-cover rounded-lg w-[500px] h-[250px]"
                        src="{{ isset($objek1->thumbnail) ? Storage::url($objek1->thumbnail) : asset('storage/img/destIndonesia-2.png') }}"
                        alt="">
                    <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48 w-full">
                        <p class="text-2xl lg:text-3xl text-white sm:text-left font-bold">
                            {{ isset($objek1->nama_pulau) ? ($objek1->nama_pulau) : null }}</p>
                        <div class="truncate text-sm text-white">{!! isset($objek1->deskripsi) ? $objek1->deskripsi : null !!} </div>
                    </div>
                </a> --}}
                {{-- Photo 2 --}}
                {{-- <a href="{{ isset($objek2->slug) ? route('pulauShow', $objek2->slug) : '#' }}"
                    class="relative bg-slate-400 cursor-pointer rounded-lg w-[500px] h-[250px]">
                    <img class="hover:opacity-70 duration-200 object-cover rounded-lg w-[500px] h-[250px]"
                        src="{{ isset($objek2->thumbnail) ? Storage::url($objek2->thumbnail) : asset('storage/img/destIndonesia-3.png') }}"
                        alt="">
                    <div class="absolute bottom-0 left-0 p-2 lg:p-5">
                        <p class="text-md lg:text-2xl text-white sm:text-left font-bold">
                            {{ isset($objek2->nama_pulau) ? $objek2->nama_pulau : null }}</p>
                    </div>
                </a> --}}
                {{-- Photo 3 --}}
                {{-- <a href="{{ isset($objek3->slug) ? route('pulauShow', $objek3->slug) : '#' }}"
                    class="relative bg-slate-400 cursor-pointer rounded-lg w-[500px] h-[250px]">
                    <img class="hover:opacity-70 duration-200 object-cover rounded-lg w-[500px] h-[250px]"
                        src="{{ isset($objek3->thumbnail) ? Storage::url($objek3->thumbnail) : asset('storage/img/destIndonesia-4.png') }}"
                        alt="">
                    <div class="absolute bottom-0 left-0 p-2 lg:p-5">
                        <p class="text-md lg:text-2xl text-white sm:text-left font-bold">
                            {{ isset($objek3->nama_pulau) ? $objek3->nama_pulau : null }}</p>
                    </div>
                </a> --}}
                {{-- Photo 4 --}}
                {{-- <a href="{{ isset($objek4->slug) ? route('pulauShow', $objek4->slug) :'#' }}"
                    class="relative bg-slate-400 cursor-pointer rounded-lg w-[500px] h-[250px]">
                    <img class="hover:opacity-70 duration-200 object-cover rounded-lg w-[500px] h-[250px]"
                        src="{{ isset($objek4->thumbnail) ? Storage::url($objek4->thumbnail) : asset('storage/img/destIndonesia-5.png') }}"
                        alt="">
                    <div class="absolute bottom-0 left-0 p-2 lg:p-5">
                        <p class="text-md lg:text-2xl text-white sm:text-left font-bold">{{ isset($objek4->nama_pulau) ? $objek4->nama_pulau:null }}
                        </p>
                    </div>
                </a> --}}
                {{-- Photo 5 --}}
                {{-- <a href="{{ isset($objek5->slug) ? route('pulauShow', $objek5->slug) :'#'}}"
                    class="relative bg-slate-400 cursor-pointer rounded-lg w-[500px] h-[250px]">
                    <img class="hover:opacity-70 duration-200 object-cover rounded-lg w-[500px] h-[250px]"
                        src="{{ isset($objek5->thumbnail) ? Storage::url($objek5->thumbnail) : asset('storage/img/destIndonesia-6.png') }}"
                        alt="">
                    <div class="absolute bottom-0 left-0 p-2 lg:p-5">
                        <p class="text-md lg:text-2xl text-white sm:text-left font-bold">{{ isset($objek5->nama_pulau) ? $objek5->nama_pulau:null }}
                        </p>
                    </div>
                </a>  --}}
                {{-- <div class="grid grid-rows-2 gap-4"> --}}
                {{-- <div class="row-span-2">
                    <div class="grid grid-cols-3 gap-3">
                        Photo 1
                        <a href="{{ isset($objek1->slug) ? route('pulauShow', $objek1->slug) : '#' }}"
                            class="relative bg-slate-400 cursor-pointer col-span-2 rounded-lg">
                            <img class="hover:opacity-70 duration-200 object-cover w-full h-full rounded-lg"
                                src="{{ isset($objek1->thumbnail) ? Storage::url($objek1->thumbnail) : asset('storage/img/destIndonesia-2.png') }}"
                                alt="">
                            <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48 w-full">
                                <p class="text-2xl lg:text-3xl text-white sm:text-left font-bold">
                                    {{ isset($objek1->nama_pulau) ? ($objek1->nama_pulau) : null }}</p>
                                <div class="truncate text-sm text-white">{!! isset($objek1->deskripsi) ? $objek1->deskripsi : null !!} </div>
                            </div>
                        </a>

                        <div class="grid grid-rows-2 gap-4">
                            Photo 2
                            <a href="{{ isset($objek2->slug) ? route('pulauShow', $objek2->slug) : '#' }}"
                                class="relative bg-slate-400 cursor-pointer rounded-lg">
                                <img class="hover:opacity-70 duration-200 object-cover w-full h-full rounded-lg"
                                    src="{{ isset($objek2->thumbnail) ? Storage::url($objek2->thumbnail) : asset('storage/img/destIndonesia-3.png') }}"
                                    alt="">
                                <div class="absolute bottom-0 left-0 p-2 lg:p-5">
                                    <p class="text-md lg:text-2xl text-white sm:text-left font-bold">
                                        {{ isset($objek2->nama_pulau) ? $objek2->nama_pulau : null }}</p>
                                </div>
                            </a>

                            Photo 3
                            <a href="{{ isset($objek3->slug) ? route('pulauShow', $objek3->slug) : '#' }}"
                                class="relative bg-slate-400 cursor-pointer rounded-lg">
                                <img class="hover:opacity-70 duration-200 object-cover w-full h-full rounded-lg"
                                    src="{{ isset($objek3->thumbnail) ? Storage::url($objek3->thumbnail) : asset('storage/img/destIndonesia-4.png') }}"
                                    alt="">
                                <div class="absolute bottom-0 left-0 p-2 lg:p-5">
                                    <p class="text-md lg:text-2xl text-white sm:text-left font-bold">
                                        {{ isset($objek3->nama_pulau) ? $objek3->nama_pulau : null }}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-2 h-full">
                    Photo 4
                    <a href="{{ isset($objek4->slug) ? route('pulauShow', $objek4->slug) :'#' }}"
                        class="relative bg-slate-400 cursor-pointer rounded-lg">
                        <img class="hover:opacity-70 duration-200 object-cover w-full h-full rounded-lg"
                            src="{{ isset($objek4->thumbnail) ? Storage::url($objek4->thumbnail) : asset('storage/img/destIndonesia-5.png') }}"
                            alt="">
                        <div class="absolute bottom-0 left-0 p-2 lg:p-5">
                            <p class="text-md lg:text-2xl text-white sm:text-left font-bold">{{ isset($objek4->nama_pulau) ? $objek4->nama_pulau:null }}
                            </p>
                        </div>
                    </a>
                    Photo 5
                    <a href="{{ isset($objek5->slug) ? route('pulauShow', $objek5->slug) :'#'}}"
                        class="relative bg-slate-400 cursor-pointer rounded-lg">
                        <img class="hover:opacity-70 duration-200 object-cover w-full h-full rounded-lg"
                            src="{{ isset($objek5->thumbnail) ? Storage::url($objek5->thumbnail) : asset('storage/img/destIndonesia-6.png') }}"
                            alt="">
                        <div class="absolute bottom-0 left-0 p-2 lg:p-5">
                            <p class="text-md lg:text-2xl text-white sm:text-left font-bold">{{ isset($objek5->nama_pulau) ? $objek5->nama_pulau:null }}
                            </p>
                        </div>
                    </a>
                </div> --}}
            </div>

            {{-- Mobile --}}
            {{-- <div class="block sm:hidden mx-auto my-5 swiper carousel-destindonesia-swiper">
                <div class="mx-auto py-5 swiper-wrapper">
                    <div class="flex justify-center swiper-slide">
                        <div class="relative bg-slate-400 cursor-pointer mx-auto rounded-lg w-1/2">
                            <img class="hover:opacity-70 object-cover h-full rounded-lg"
                                src="{{ asset('storage/img/destIndonesia-2.png') }}" alt="">
                            <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48">
                                <p class="text-xl lg:text-3xl text-white font-bold">Lorem Ipsum</p>
                            </div>
                        </div>
                    </div>
                    <div class="flex justify-center swiper-slide">
                        <div class="relative bg-slate-400 cursor-pointer mx-auto rounded-lg w-1/2">
                            <img class="hover:opacity-70 object-cover h-full rounded-lg"
                                src="{{ asset('storage/img/destIndonesia-3.png') }}" alt="">
                            <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48">
                                <p class="text-xl text-white font-bold">Lorem Ipsum</p>
                            </div>
                        </div>
                    </div>
                    <div class="flex justify-center swiper-slide">
                        <div class="relative bg-slate-400 cursor-pointer mx-auto rounded-lg w-1/2">
                            <img class="hover:opacity-70 object-cover h-full rounded-lg"
                                src="{{ asset('storage/img/destIndonesia-4.png') }}" alt="">
                            <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48">
                                <p class="text-xl text-white font-bold">Lorem Ipsum</p>
                            </div>
                        </div>
                    </div>
                    <div class="flex justify-center swiper-slide">
                        <div class="relative bg-slate-400 cursor-pointer mx-auto rounded-lg w-1/2">
                            <img class="hover:opacity-70 object-cover h-full rounded-lg"
                                src="{{ asset('storage/img/destIndonesia-5.png') }}" alt="">
                            <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48">
                                <p class="text-xl text-white font-bold">Lorem Ipsum</p>
                            </div>
                        </div>
                    </div>
                    <div class="flex justify-center swiper-slide">
                        <div class="relative bg-slate-400 cursor-pointer mx-auto rounded-lg w-1/2">
                            <img class="hover:opacity-70 object-cover h-full rounded-lg"
                                src="{{ asset('storage/img/destIndonesia-6.png') }}" alt="">
                            <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48">
                                <p class="text-xl text-white font-bold">Lorem Ipsum</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div> --}}
            <div class="sm:hidden grid grid-rows-5 grid-flow-col gap-3 mx-auto">
                {{-- 
                <a href="{{ isset($objek5->slug) ? route('pulauShow', $objek5->slug) : '#'}}" class="relative bg-cyan-900 rounded-lg w-[450px] h-[250px] cursor-pointer">
                        <img class="hover:opacity-70 duration-200 object-cover w-full h-full rounded-lg"
                             src="{{ isset($objek5->header) ? Storage::url($objek5->header) : 'https://source.unsplash.com/600x400?nature' }}"
                        alt="x">
                        <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48 w-full">
                            <p class="text-2xl lg:text-3xl text-white sm:text-left font-bold">
                                {{ isset($objek5->nama_pulau) ? ($objek5->nama_pulau) : null }}</p>
                            <div class="hidden w-[411px] p-[6px] relative overflow-hidden text-ellipsis whitespace-normal backdrop-blur-ms bg-white/40 text-sm font-bold text-white" style="text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;"">{!! isset($objek5->desk_thumbnail) ? $objek5->desk_thumbnail : null !!} </div>
                        </div>        
                    </a>    
                --}}
                <div class="relative bg-slate-400 cursor-pointer mx-auto rounded-lg w-1/2">
                    <img class="hover:opacity-70 object-cover h-full rounded-lg"
                        src="{{ isset($objek1->header) ? Storage::url($objek1->header) : asset('storage/img/destIndonesia-2.png') }}" alt="">
                    <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48">
                        <a href="{{ isset($objek1->slug) ? route('pulauShow', $objek1->slug) : '#'}}">
                            <p class="text-xl lg:text-3xl text-white font-bold">{{ isset($objek1->nama_pulau) ? ($objek1->nama_pulau) : null }}</p>
                        </a>
                    </div>
                </div>
                <div class="relative bg-slate-400 cursor-pointer mx-auto rounded-lg w-1/2">
                    <img class="hover:opacity-70 object-cover h-full rounded-lg"
                        src="{{ isset($objek2->header) ? Storage::url($objek2->header) : asset('storage/img/destIndonesia-3.png') }}" alt="">
                    <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48">
                        <a href="{{ isset($objek2->slug) ? route('pulauShow', $objek2->slug) : '#'}}">
                            <p class="text-xl text-white font-bold">{{ isset($objek2->nama_pulau) ? ($objek2->nama_pulau) : null }}</p>
                        </a>
                    </div>
                </div>
                <div class="relative bg-slate-400 cursor-pointer mx-auto rounded-lg w-1/2">
                    <img class="hover:opacity-70 object-cover h-full rounded-lg"
                        src="{{ isset($objek3->header) ? Storage::url($objek3->header) : asset('storage/img/destIndonesia-4.png') }}" alt="">
                    <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48">
                        <a href="{{ isset($objek3->slug) ? route('pulauShow', $objek3->slug) : '#'}}">
                            <p class="text-xl text-white font-bold">{{ isset($objek3->nama_pulau) ? ($objek3->nama_pulau) : null }}</p>
                        </a>
                    </div>
                </div>
                <div class="relative bg-slate-400 cursor-pointer mx-auto rounded-lg w-1/2">
                    <img class="hover:opacity-70 object-cover h-full rounded-lg"
                        src="{{ isset($objek4->header) ? Storage::url($objek4->header) : asset('storage/img/destIndonesia-5.png') }}" alt="">
                    <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48">
                        <a href="{{ isset($objek4->slug) ? route('pulauShow', $objek4->slug) : '#'}}">
                            <p class="text-xl text-white font-bold">{{ isset($objek4->nama_pulau) ? ($objek4->nama_pulau) : null }}</p>
                        </a>
                    </div>
                </div>
                <div class="relative bg-slate-400 cursor-pointer mx-auto rounded-lg w-1/2">
                    <img class="hover:opacity-70 object-cover h-full rounded-lg"
                        src="{{ isset($objek5->header) ? Storage::url($objek5->header) : asset('storage/img/destIndonesia-6.png') }}" alt="">
                    <div class="absolute bottom-0 left-0 p-3 lg:p-5 lg:pr-48">
                        <a href="{{ isset($objek5->slug) ? route('pulauShow', $objek5->slug) : '#'}}">
                            <p class="text-xl text-white font-bold">{{ isset($objek5->nama_pulau) ? ($objek5->nama_pulau) : null }}</p>
                        </a>
                    </div>
                </div>
            </div>

            {{-- Button Lebih Banyak --}}
            <a href="{{ route('destindonesia.list') }}" class="flex justify-center sm:block m-3">
                <button
                    class="rounded-full flex justify-center bg-white text-[#D50006] duration-200 hover:bg-[#D50006] hover:text-white text-sm font-medium shadow-md p-2 my-3 border border-slate-200 hover:border-transparent sm:float-right">Lihat
                    lebih banyak tempat untuk dikunjungi</button>
            </a>
        </div>

        {{-- Ads Desktop --}}
        <div class="hidden sm:block">
            <p class="mx-5 text-3xl font-semibold my-3">Article (Ads)</p>
            <div class="w-[306px] shadow-xl divide-y rounded-lg p-5 mx-5 my-5">
                @if($news)
                    @foreach ($news as $iklan => $item)
                    {{-- {{ dd($item) }} --}}
                    <div class="grid grid-cols-2">
                        <div class="p-3">
                            <img src="{{ isset($item->thumbnail) ? Storage::url($item->thumbnail) : 'https://via.placeholder.com/109x109' }}"
                                alt="tailwind logo" class="rounded-xl object-cover object-center" />
                        </div>

                        <a href="{{ route('newsletter.show', $item->slug) }}" class="p-3 ">
                            <h2 class="font-bold">{{ $item->title }}</h2>
                            <div class="truncate">
                                {!! $item->description !!}
                            </div>
                        </a>
                    </div>
                    @endforeach
                {{-- @else --}}
                @endif
                @if(!$news)
                <div class="grid grid-cols-2">
                    <div class="p-3">
                        <img src="https://via.placeholder.com/109x109"
                            alt="tailwind logo" class="rounded-xl object-cover object-center" />
                    </div>

                    <a href="#" class="p-3 ">
                        <h2 class="font-bold">Ads</h2>
                        <div class="truncate">
                            Ads Deskripsi...
                        </div>
                    </a>
                </div>
                @endif    
            </div>
        </div>
    </div>

    {{-- SVG Maps --}}
    <div class="my-5 mx-auto container map" id="map">
        <div class="container grid grid-cols-10">
            <div class="cardSlide col-start-6 col-span-1">
                {{-- Sumatra --}}
                <div id="list-a" class="cardList">
                    @foreach ($sumatraSVG as $ssvg => $ss)
                        <div
                            class="max-w-sm w-[150px] lg:w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                            <a href="{{ route('wisataShow', $ss->slug) }}">
                                <img class="rounded-t-lg w-[300px] h-[180px]"
                                    src="{{ isset($ss->thumbnail) ? Storage::url($ss->thumbnail) : 'https://via.placeholder.com/300x180' }}"
                                    alt="">
                                <div class="p-2 lg:p-5">
                                    <p class="text-[#FFB800] text-[10px] lg:text-sm font-semibold">Explore</p>
                                    <a href="{{ route('wisataShow', $ss->slug) }}">
                                        <h5
                                            class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                            {{ $ss->namaWisata }}</h5>
                                    </a>
                                    <p
                                        class="truncate mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                        {!! $ss->deskripsi !!}</p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                {{-- Jawa --}}
                <div id="list-b" class="cardList">

                    @foreach ($jawaSVG as $jsvg => $js)
                        <div
                            class="max-w-sm w-[150px] lg:w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                            <a href="{{ route('wisataShow', $js->slug) }}">
                                <img class="rounded-t-lg w-[300px] h-[180px]"
                                    src="{{ isset($js->thumbnail) ? Storage::url($js->thumbnail) : 'https://via.placeholder.com/300x180' }}"
                                    alt="">
                                <div class="p-2 lg:p-5">
                                    <p class="text-[#FFB800] text-[10px] lg:text-sm font-semibold">Explore</p>
                                    <a href="{{ route('wisataShow', $js->slug) }}">
                                        <h5
                                            class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                            {{ $js->namaWisata }}</h5>
                                    </a>
                                    <p
                                        class="truncate mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                        {!! $js->deskripsi !!}</p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                {{-- Maluku --}}
                <div id="list-c" class="cardList">
                    @foreach ($malukuSVG as $msvg => $ms)
                        <div
                            class="max-w-sm w-[150px] lg:w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                            <a href="{{ route('wisataShow', $ms->slug) }}">
                                <img class="rounded-t-lg"
                                    src="{{ isset($ms->thumbnail) ? Storage::url($ms->thumbnail) : 'https://via.placeholder.com/300x180' }}"
                                    alt="">
                                <div class="p-2 lg:p-5">
                                    <p class="text-[#FFB800] text-[10px] lg:text-sm font-semibold">Explore</p>
                                    <a href="{{ route('wisataShow', $ms->slug) }}">
                                        <h5
                                            class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                            {{ $ms->namaWisata }}</h5>
                                    </a>
                                    <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                        {!! $ms->namaWisata !!}</p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                {{-- Kalimantan --}}
                <div id="list-d" class="cardList">
                    @foreach ($kalimantanSVG as $ksvg => $ks)
                        <div
                            class="max-w-sm w-[150px] lg:w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                            <a href="{{ route('wisataShow', $ks->slug) }}">
                                <img class="rounded-t-lg"
                                    src="{{ isset($ks->thumbnail) ? Storage::url($ks->thumbnail) : 'https://via.placeholder.com/300x180' }}"
                                    alt="">
                                <div class="p-2 lg:p-5">
                                    <p class="text-[#FFB800] text-[10px] lg:text-sm font-semibold">Explore</p>
                                    <a href="{{ route('wisataShow', $ks->slug) }}">
                                        <h5
                                            class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                            {{ $ks->namaWisata }}</h5>
                                    </a>
                                    <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                        {!! $ks->deskripsi !!}</p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                {{-- Sulawesi --}}
                <div id="list-e" class="cardList">
                    @foreach ($sulawesiSVG as $slsvg => $sls)
                        <div
                            class="max-w-sm w-[150px] lg:w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                            <a href="{{ route('wisataShow', $sls->slug) }}">
                                <img class="rounded-t-lg"
                                    src="{{ isset($sls->thumbnail) ? Storage::url($sls->thumbnail) : 'https://via.placeholder.com/300x180' }}"
                                    alt="">
                                <div class="p-2 lg:p-5">
                                    <p class="text-[#FFB800] text-[10px] lg:text-sm font-semibold">Explore</p>
                                    <a href="{{ route('wisataShow', $sls->slug) }}">
                                        <h5
                                            class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                            {{ $sls->namaWisata }}</h5>
                                    </a>
                                    <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                        {!! $sls->deskripsi !!}</p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                {{-- Nusa --}}
                <div id="list-f" class="cardList">
                    @foreach ($nusaSVG as $nsvg => $ns)
                        <div
                            class="max-w-sm w-[150px] lg:w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                            <a href="{{ route('wisataShow', $ns->slug) }}">
                                <img class="rounded-t-lg"
                                    src="{{ isset($ns->thumbnail) ? Storage::url($ns->thumbnail) : 'https://via.placeholder.com/300x180' }}"
                                    alt="">
                                <div class="p-2 lg:p-5">
                                    <p class="text-[#FFB800] text-[10px] lg:text-sm font-semibold">Explore</p>
                                    <a href="{{ route('wisataShow', $ns->slug) }}">
                                        <h5
                                            class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                            {{ $ns->namaWisata }}</h5>
                                    </a>
                                    <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                        {!! $ns->deskripsi !!}</p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                {{-- Papua --}}
                <div id="list-g" class="cardList">
                    @foreach ($papuaSVG as $psvg => $ps)
                        <div
                            class="max-w-sm w-[150px] lg:w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                            <a href="{{ route('wisataShow', $ps->slug) }}">
                                <img class="rounded-t-lg"
                                    src="{{ isset($ps->thumbnail) ? Storage::url($ps->thumbnail) : 'https://via.placeholder.com/300x180' }}"
                                    alt="">
                                <div class="p-2 lg:p-5">
                                    <p class="text-[#FFB800] text-[10px] lg:text-sm font-semibold">Explore</p>
                                    <a href="{{ route('wisataShow', $ps->slug) }}">
                                        <h5
                                            class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                            {{ $ps->namaWisata }}</h5>
                                    </a>
                                    <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                        {!! $ps->deskripsi !!}</p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                {{-- <div id="buttons">
                    <a id="prev" class="prev" onclick="plusSlides(-1)">❮</a>
                    <a id="next" class="next" onclick="plusSlides(1)">❯</a>
                </div> --}}
            </div>
        </div>
        <x-destindonesia.svg-map></x-destindonesia.svg-map>
    </div>

    {{-- Paling Populer --}}
    <div class="container mx-auto py-10" x-data="{
        active: 'Tur',
        tabs: ['Tur', 'Transfer', 'Rental', 'Activity', 'Hotel', 'Xstay']
    }">
        <p class="mx-3 text-xl sm:text-3xl font-semibold my-3 text-center sm:text-left">Paling Popular</p>

        {{-- Tabs --}}
        <div class="flex sm:block mx-5">
            <ul
                class="flex justify-start overflow-x-auto text-sm font-medium text-center text-gray-500 dark:text-gray-400">
                <template x-for="(tab, index) in tabs" :key="index">
                    <li class="mx-2 mt-2" x-on:click="active = tab">
                        <button class="inline-block py-2 px-9 h-full text-white bg-[#23AEC1] rounded-lg"
                            :class="{ 'text-white bg-[#23AEC1]': active ==
                                tab, 'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]': active !== tab }"
                            x-text="tab"></button>
                    </li>
                </template>
            </ul>
        </div>

        <div class="swiper productSwiper">
            <div x-show="active === 'Tur'" class="swiper productSwiper">
                <div class="swiper-wrapper">
                    @if (json_decode($tur) != null)
                        @foreach (json_decode($tur) as $act)
                            <div class="flex justify-center swiper-slide">
                                <x-destindonesia.card-produk :act="$act" :type="'tur'" />
                            </div>
                        @endforeach
                    @else
                        <div
                            class="p-5 text-3xl font-bold flex justify-center items-center text-center text-[#9E3D64] w-full md:h-[125px]">
                            Tur tidak tersedia
                        </div>
                    @endif
                </div>
            </div>
            <div x-show="active === 'Transfer'" class="swiper productSwiper">
                <div class="swiper-wrapper">
                    @if (json_decode($transfer) != null)
                        @foreach (json_decode($transfer) as $act)
                            <div class="flex justify-center swiper-slide">
                                <x-destindonesia.card-produk :act="$act" :type="'transfer'" />
                            </div>
                        @endforeach
                    @else
                        <div
                            class="p-5 text-3xl font-bold flex justify-center items-center text-center text-[#9E3D64] w-full md:h-[125px]">
                            Transfer tidak tersedia
                        </div>
                    @endif
                </div>
            </div>
            <div x-show="active === 'Rental'" class="swiper productSwiper">
                <div class="swiper-wrapper">
                    @if (json_decode($rental) != null)
                        @foreach (json_decode($rental) as $act)
                            <div class="flex justify-center swiper-slide">
                                <x-destindonesia.card-produk :act="$act" :type="'rental'" />
                            </div>
                        @endforeach
                    @else
                        <div
                            class="p-5 text-3xl font-bold flex justify-center items-center text-center text-[#9E3D64] w-full md:h-[125px]">
                            Rental tidak tersedia
                        </div>
                    @endif
                </div>
            </div>
            <div x-show="active === 'Activity'" class="swiper productSwiper">
                <div class="swiper-wrapper">
                    @if (json_decode($activity) != null)
                        @foreach (json_decode($activity) as $act)
                            <div class="flex justify-center swiper-slide">
                                <x-destindonesia.card-produk :act="$act" :type="'activity'" />
                            </div>
                        @endforeach
                    @else
                        <div
                            class="p-5 text-3xl font-bold flex justify-center items-center text-center text-[#9E3D64] w-full md:h-[125px]">
                            Activity tidak tersedia
                        </div>
                    @endif
                </div>
            </div>
            <div x-show="active === 'Hotel'" class="swiper productSwiper">
                <div class="swiper-wrapper">
                    @if (json_decode($hotel) != null)
                        @foreach (json_decode($hotel) as $act)
                            <div class="flex justify-center swiper-slide">
                                <x-destindonesia.card-produk :act="$act" :type="'hotel'" />
                            </div>
                        @endforeach
                    @else
                        <div
                            class="p-5 text-3xl font-bold flex justify-center items-center text-center text-[#9E3D64] w-full md:h-[125px]">
                            Hotel tidak tersedia
                        </div>
                    @endif
                </div>
            </div>
            <div x-show="active === 'Xstay'" class="swiper productSwiper">
                <div class="swiper-wrapper">
                    @if (json_decode($xstay) != null)
                        @foreach (json_decode($xstay) as $act)
                            <div class="flex justify-center swiper-slide">
                                <x-destindonesia.card-produk :act="$act" :type="'xstay'" />
                            </div>
                        @endforeach
                    @else
                        <div
                            class="p-5 text-3xl font-bold flex justify-center items-center text-center text-[#9E3D64] w-full md:h-[125px]">
                            Xstay tidak tersedia
                        </div>
                    @endif
                </div>
            </div>
            <div class="-mx-2 swiper-button-next rounded-full bg-white hover:border hover:-translate-x-1 hover:shadow-md duration-200 p-7 border border-[#9E3D64]"
                style="--swiper-navigation-color: #9E3D64; --swiper-pagination-color: #9E3D64"></div>
            <div class="-mx-2 swiper-button-prev rounded-full bg-white hover:border hover:-translate-x-1 hover:shadow-md duration-200 p-7 border border-[#9E3D64]"
                style="--swiper-navigation-color: #9E3D64; --swiper-pagination-color: #9E3D64"></div>
            <div class="swiper-pagination -mb-2"></div>
        </div>
    </div>

    {{-- Destinasi Populer --}}
    <div class="container mx-auto" x-data="{
        active: 0,
        tabs: {{ json_encode($regencyName) }}
    }">
        <p class="mx-3 text-xl sm:text-3xl font-semibold my-3 text-center sm:text-left">Destinasi Populer di Indonesia
        </p>

        {{-- Tabs --}}
        <div class="flex sm:block mx-5">
            <ul
                class="flex overflow-x-auto justify-start text-sm font-medium text-center text-gray-500 dark:text-gray-400">
                <template x-for="(tab, index) in tabs" :key="index">
                    <li class="mx-2 mt-2" x-on:click="active = index">
                        <button class="inline-block py-2 px-9 h-full text-white bg-[#23AEC1] rounded-lg"
                            :class="{ 'text-white bg-[#23AEC1]': active ==
                                index, 'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]': active !==
                                    index }"
                            x-text="tab"></button>
                    </li>
                </template>
            </ul>
        </div>

        {{-- Object List --}}
        <div x-show="active === 0" class="swiper objectSwiper p-5">
            <div class="p-5 swiper-wrapper">
                @if ($product_tur_wisata_0 != null)
                    @foreach ($product_tur_wisata_0 as $act)
                        <div class="flex justify-center swiper-slide">
                            <a
                                href="{{ isset($act['productdetail']) ? route('tur.show', $act['slug']) : route('wisataShow', $act['slug']) }}">
                                <figure class="relative max-w-xs cursor-pointer">
                                    <img class="rounded-lg shadow-xl hover:shadow-2xl w-[200px] h-[100px] sm:w-[300px] sm:h-[185px]"
                                        src="{{ isset($act['header']) ? Storage::url($act['header']) : (isset($act['productdetail']['thumbnail']) ? asset($act['productdetail']['thumbnail']) : 'https://via.placeholder.com/300x185') }}">
                                    <figcaption
                                        class="absolute px-4 -mt-16 text-lg text-white w-full bg-slate-500 bg-opacity-50">
                                        <div>
                                            <h1>{{ $act['product_name'] ?? $act['title'] }}</h1>
                                        </div>
                                        <div class="truncate">
                                            {!! $act['productdetail']['deskripsi'] ?? $act['deskripsi'] !!}
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    @endforeach
                @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Product tidak ditemukan
                    </div>
                @endif
            </div>
            <div class="swiper-pagination"></div>
        </div>
        <div x-show="active === 1" class="swiper objectSwiper p-5">
            <div class="p-5 swiper-wrapper">
                @if ($product_tur_wisata_1 != null)
                    @foreach ($product_tur_wisata_1 as $act)
                        <div class="flex justify-center swiper-slide">
                            <a
                                href="{{ isset($act['productdetail']) ? route('tur.show', $act['slug']) : route('wisataShow', $act['slug']) }}">
                                <figure class="relative max-w-xs cursor-pointer">
                                    <img class="rounded-lg shadow-xl hover:shadow-2xl w-[200px] h-[100px] sm:w-[300px] sm:h-[185px]"
                                        src="{{ isset($act['header']) ? Storage::url($act['header']) : (isset($act['productdetail']['thumbnail']) ? asset($act['productdetail']['thumbnail']) : 'https://via.placeholder.com/300x185') }}">
                                    <figcaption
                                        class="absolute px-4 -mt-16 text-lg text-white w-full bg-slate-500 bg-opacity-50">
                                        <div>
                                            <h1>{{ $act['product_name'] ?? $act['title'] }}</h1>
                                        </div>
                                        <div class="truncate">
                                            {!! $act['productdetail']['deskripsi'] ?? $act['deskripsi'] !!}
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    @endforeach
                @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Product tidak ditemukan
                    </div>
                @endif
            </div>
            <div class="swiper-pagination"></div>
        </div>
        <div x-show="active === 2" class="swiper objectSwiper p-5">
            <div class="p-5 swiper-wrapper">
                @if ($product_tur_wisata_2 != null)
                    @foreach ($product_tur_wisata_2 as $act)
                        <div class="flex justify-center swiper-slide">
                            <a
                                href="{{ isset($act['productdetail']) ? route('tur.show', $act['slug']) : route('wisataShow', $act['slug']) }}">
                                <figure class="relative max-w-xs cursor-pointer">
                                    <img class="rounded-lg shadow-xl hover:shadow-2xl w-[200px] h-[100px] sm:w-[300px] sm:h-[185px]"
                                        src="{{ isset($act['header']) ? Storage::url($act['header']) : (isset($act['productdetail']['thumbnail']) ? asset($act['productdetail']['thumbnail']) : 'https://via.placeholder.com/300x185') }}">
                                    <figcaption
                                        class="absolute px-4 -mt-16 text-lg text-white w-full bg-slate-500 bg-opacity-50">
                                        <div>
                                            <h1>{{ $act['product_name'] ?? $act['title'] }}</h1>
                                        </div>
                                        <div class="truncate">
                                            {!! $act['productdetail']['deskripsi'] ?? $act['deskripsi'] !!}
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    @endforeach
                @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Product tidak ditemukan
                    </div>
                @endif
            </div>
            <div class="swiper-pagination"></div>
        </div>
        <div x-show="active === 3" class="swiper objectSwiper p-5">
            <div class="p-5 swiper-wrapper">
                @if ($product_tur_wisata_3 != null)
                    @foreach ($product_tur_wisata_3 as $act)
                        <div class="flex justify-center swiper-slide">
                            <a
                                href="{{ isset($act['productdetail']) ? route('tur.show', $act['slug']) : route('wisataShow', $act['slug']) }}">
                                <figure class="relative max-w-xs cursor-pointer">
                                    <img class="rounded-lg shadow-xl hover:shadow-2xl w-[200px] h-[100px] sm:w-[300px] sm:h-[185px]"
                                        src="{{ isset($act['header']) ? Storage::url($act['header']) : (isset($act['productdetail']['thumbnail']) ? asset($act['productdetail']['thumbnail']) : 'https://via.placeholder.com/300x185') }}">
                                    <figcaption
                                        class="absolute px-4 -mt-16 text-lg text-white w-full bg-slate-500 bg-opacity-50">
                                        <div>
                                            <h1>{{ $act['product_name'] ?? $act['title'] }}</h1>
                                        </div>
                                        <div class="truncate">
                                            {!! $act['productdetail']['deskripsi'] ?? $act['deskripsi'] !!}
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    @endforeach
                @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Product tidak ditemukan
                    </div>
                @endif
            </div>
            <div class="swiper-pagination"></div>
        </div>
        <div x-show="active === 4" class="swiper objectSwiper p-5">
            <div class="p-5 swiper-wrapper">
                @if ($product_tur_wisata_4 != null)
                    @foreach ($product_tur_wisata_4 as $act)
                        <div class="flex justify-center swiper-slide">
                            <a
                                href="{{ isset($act['productdetail']) ? route('tur.show', $act['slug']) : route('wisataShow', $act['slug']) }}">
                                <figure class="relative max-w-xs cursor-pointer">
                                    <img class="rounded-lg shadow-xl hover:shadow-2xl w-[200px] h-[100px] sm:w-[300px] sm:h-[185px]"
                                        src="{{ isset($act['header']) ? Storage::url($act['header']) : (isset($act['productdetail']['thumbnail']) ? asset($act['productdetail']['thumbnail']) : 'https://via.placeholder.com/300x185') }}">
                                    <figcaption
                                        class="absolute px-4 -mt-16 text-lg text-white w-full bg-slate-500 bg-opacity-50">
                                        <div>
                                            <h1>{{ $act['product_name'] ?? $act['title'] }}</h1>
                                        </div>
                                        <div class="truncate">
                                            {!! $act['productdetail']['deskripsi'] ?? $act['deskripsi'] !!}
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    @endforeach
                @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Product tidak ditemukan
                    </div>
                @endif
            </div>
            <div class="swiper-pagination"></div>
        </div>
        <div x-show="active === 5" class="swiper objectSwiper p-5">
            <div class="p-5 swiper-wrapper">
                @if ($product_tur_wisata_5 != null)
                    @foreach ($product_tur_wisata_5 as $act)
                        <div class="flex justify-center swiper-slide">
                            <a
                                href="{{ isset($act['productdetail']) ? route('tur.show', $act['slug']) : route('wisataShow', $act['slug']) }}">
                                <figure class="relative max-w-xs cursor-pointer">
                                    <img class="rounded-lg shadow-xl hover:shadow-2xl w-[200px] h-[100px] sm:w-[300px] sm:h-[185px]"
                                        src="{{ isset($act['header']) ? Storage::url($act['header']) : (isset($act['productdetail']['thumbnail']) ? asset($act['productdetail']['thumbnail']) : 'https://via.placeholder.com/300x185') }}">
                                    <figcaption
                                        class="absolute px-4 -mt-16 text-lg text-white w-full bg-slate-500 bg-opacity-50">
                                        <div>
                                            <h1>{{ $act['product_name'] ?? $act['title'] }}</h1>
                                        </div>
                                        <div class="truncate">
                                            {!! $act['productdetail']['deskripsi'] ?? $act['deskripsi'] !!}
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    @endforeach
                @else
                    <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                        Product tidak ditemukan
                    </div>
                @endif
            </div>
            <div class="swiper-pagination"></div>
        </div>
        {{-- <div class="swiper-slide">
            <x-destindonesia.object-list></x-destindonesia.object-list>
        </div> --}}
        {{-- <div class="swiper-slide">
            <x-destindonesia.object-list></x-destindonesia.object-list>
        </div>
        <div class="swiper-slide">
            <x-destindonesia.object-list></x-destindonesia.object-list>
        </div>
        <div class="swiper-slide">
            <x-destindonesia.object-list></x-destindonesia.object-list>
        </div>
        <div class="swiper-slide">
            <x-destindonesia.object-list></x-destindonesia.object-list>
        </div>
        <div class="swiper-slide">
            <x-destindonesia.object-list></x-destindonesia.object-list>
        </div>
        <div class="swiper-slide">
            <x-destindonesia.object-list></x-destindonesia.object-list>
        </div>
        <div class="swiper-slide">
            <x-destindonesia.object-list></x-destindonesia.object-list>
        </div>
        <div class="swiper-slide">
            <x-destindonesia.object-list></x-destindonesia.object-list>
        </div> --}}

        {{-- <div class="flex overflow-x-auto justify-center p-5 gap-3">
            <x-destindonesia.object-list></x-destindonesia.object-list>
            <x-destindonesia.object-list></x-destindonesia.object-list>
            <x-destindonesia.object-list></x-destindonesia.object-list>
            <x-destindonesia.object-list></x-destindonesia.object-list>
            <x-destindonesia.object-list></x-destindonesia.object-list>
            <x-destindonesia.object-list></x-destindonesia.object-list>
            <x-destindonesia.object-list></x-destindonesia.object-list>
            <x-destindonesia.object-list></x-destindonesia.object-list>
        </div> --}}
        {{-- <div class="grid grid-row-2 overflow-x-auto p-5 gap-3">
            <div class="grid grid-cols-2 lg:grid-cols-4 gap-3 justify-items-center">
                <x-destindonesia.object-list></x-destindonesia.object-list>
                <x-destindonesia.object-list></x-destindonesia.object-list>
                <x-destindonesia.object-list></x-destindonesia.object-list>
                <x-destindonesia.object-list></x-destindonesia.object-list>
            </div>
            <div class="grid grid-cols-2 lg:grid-cols-4 gap-3 justify-items-center">
                <x-destindonesia.object-list></x-destindonesia.object-list>
                <x-destindonesia.object-list></x-destindonesia.object-list>
                <x-destindonesia.object-list></x-destindonesia.object-list>
                <x-destindonesia.object-list></x-destindonesia.object-list>
            </div>
        </div> --}}
    </div>

    {{-- Ads Mobile --}}
    <div class="block lg:hidden mt-10">
        <p class="text-xl font-semibold mb-3 text-center sm:text-left">Article (Ads)</p>
        <x-destindonesia.ads-list></x-destindonesia.ads-list>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>

        $("#btn-search").on("click",function(){
            let cari = $('#search-input').val();
            let url = "{{route('search.destindo')}}"
            window.location.replace(`${url}?cari=${cari}`);
        });

        var swiper = new Swiper(".productSwiper", {
            slidesPerView: 1,
            loop: true,
            navigation: {
                enabled: true,
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    navigation: {
                        enabled: true
                    },
                    pagination: {
                        enabled: false
                    }
                },
                1024: {
                    slidesPerView: 3,
                    navigation: {
                        enabled: false
                    },
                    pagination: {
                        enabled: true
                    }
                },
                1280: {
                    slidesPerView: 5,
                    navigation: {
                        enabled: false,
                    },
                    pagination: {
                        enabled: false
                    }
                }
            },
        });

    </script>
    <script>
        var objectSwiper = new Swiper(".objectSwiper", {
            slidesPerView: 3,
            grid: {
                rows: 2,
                fill: 'row'
            },
            spaceBetween: 30,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
        });
    </script>
    <script>
        var carouselDestindonesiaSwiper = new Swiper(".carousel-destindonesia-swiper", {
            // loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            }
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        var reviewSwipper = new Swiper(".review-swipper", {
            loop: false,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    </script>
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>

    <script>
        let slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            let i;
            let slides = document.getElementsByClassName("cardList");
            if (n > slides.length) {
                slideIndex = 1
            }
            if (n < 1) {
                slideIndex = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }

            let spans = $(".span");
            spans.click(function() {
                spans.removeClass("active");

                this.classList.add("active");
            });

            slides[slideIndex - 1].style.display = "block";
        }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body>

</html>
