<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
        {{-- Destindonesia NavBar --}}
        <div
            class="bg-transparent lg:container mx-auto px-2 h-[75px] lg:h-[100px] border-y-[1px] border-[#F2F2F2] hidden sm:block relative z-10">
            <div class="grid grid-cols-6 h-full">
                <a href="#"
                    class="text-lg md:text-xl lg:text-3xl text-[#9E3D64] font-extrabold lg:font-semibold my-auto">DestIndonesia</a>
                <a href="#"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-[#333333]">Destinasi</a>
                <a href="#"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-[#333333]">Newsletter</a>
                <a href="#"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-[#333333]">Event</a>
                <a href="#"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-[#333333]">Videos</a>
                <div class="flex justify-center my-auto bg-white rounded-full border">
                    <input
                        class="placeholder:italic placeholder:text-slate-400 block bg-white w-full border border-transparent rounded-full py-1 lg:py-2 pl-5 pr-3 shadow-sm focus:outline-none focus:border-transparent focus:ring-transparent focus:ring-1 sm:text-sm"
                        placeholder="Search" type="text" name="search" />
                    <button class="mx-3"><img src="{{ asset('storage/icons/search-ic.png') }}"
                            alt=""></button>
                </div>
            </div>
        </div>
    </header>
    {{-- Hero --}}
    <div class="relative">
        {{-- Kabupaten/Kota Name --}}
        <div class="mt-8">
            <p class="text-lg lg:text-6xl text-center text-[#333333] font-bold">Indonesia</p>
            <p class="text-lg lg:text-xl text-center text-[#9E3D64] font-bold">Temukan Destinasimu</p>
        </div>
    </div>

    {{-- Terbaru dan Ads Desktop --}}
    <div class="container mx-auto grid grid-cols-4 gap-5 my-5">
        {{-- SVG Maps --}}
        <div class="lg:col-span-3 col-span-4 lg:mx-px mx-2">
            <div class="my-5 mx-auto container map" id="map">
                <img class="p-5 lg:block hidden" src="{{ asset($map) }}" alt="list-jawa">
                <div class="lg:h-16 h-8 border-b lg:mx-px mx-2 ">
                    <p class="lg:text-xl text-md w-full font-bold text-[#333333] py-0 lg:py-2">Pulau Jawa</p>
                </div>
                {{-- text --}}
                {{-- @dump($pulau) --}}
                @forelse($provinces as $province)
                <div class="grid grid-cols-10 my-2">
                    <div class="flex col-start-1 col-span-3 lg:col-span-2 items-center justify-end mr-4">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                    </div>
                    <a href="{{ route('destindonesia.prov', $province->id) }}"
                        class="flex items-center col-start-4 lg:col-start-3 col-span-5">
                        <p class="text-[#333333] hover:text-[#9E3D64] lg:text-lg text-[12px] font-semibold">
                            {{ $province->name }}
                        </p>
                    </a>
                </div>
                @empty
                @endforelse
                {{-- <div class="container grid grid-cols-3">
                    <div class="cardSlide col-start-4 col-span-1">
                    </div>
                </div> --}}
            </div>
        </div>

        {{-- Ads Desktop --}}
        <div class="hidden sm:block">
            <p class="mx-5 text-3xl font-semibold my-3">Article (Ads)</p>
            <div class="w-[306px] shadow-xl divide-y rounded-lg p-5 mx-5 my-5">
                @foreach ($news as $iklan => $item)
                    {{-- {{ dd($item) }} --}}
                    <div class="grid grid-cols-2">
                        <div class="p-3">
                            <img src="{{ isset($item->thumbnail) ? Storage::url($item->thumbnail) : 'https://via.placeholder.com/109x109' }}"
                                alt="tailwind logo" class="rounded-xl object-cover object-center" />
                        </div>

                        <a href="{{ route('newsletter.show', $item->slug) }}" class="p-3 ">
                            <h2 class="font-bold">{{ $item->title }}</h2>
                            <div class="truncate">
                                {!! $item->description !!}
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    {{-- Ads Mobile --}}
    <div class="block sm:hidden mt-10">
        <p class="text-xl font-semibold mb-3 text-center sm:text-left">Article (Ads)</p>
        <x-destindonesia.ads-list></x-destindonesia.ads-list>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>

    <script>
        let slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            let i;
            let slides = document.getElementsByClassName("cardList");
            if (n > slides.length) {
                slideIndex = 1
            }
            if (n < 1) {
                slideIndex = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }

            let spans = $(".span");
            spans.click(function() {
                spans.removeClass("active");

                this.classList.add("active");
            });

            slides[slideIndex - 1].style.display = "block";
        }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body>
