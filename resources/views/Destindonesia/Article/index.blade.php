<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! seo($page ?? null) !!}
    <title>{{ config('app.name', 'Kamtuu Home') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header class="border-b border-[#9E3D64]">
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>
    <div class="m-5">
        <div class="max-w-7xl mx-auto grid grid-cols-1 lg:grid-cols-[15%_45%_20%] md:grid-cols-[15%_45%_20%]">
            <p class="text-4xl"> News </p>
            <hr class="col-span-3 my-5">

            {{--        artikel --}}
            {{--        <div class="col-span-3"> --}}
            {{--            <p class="font-semibold text-gray-400 text-lg">Most recent posts</p> --}}
            {{--        </div> --}}

            {{-- list artikel --}}
            <div class="col-span-3 lg:col-span-2 md:col-span-2">

                {{--            artikel --}}
                @foreach ($newsletters as $news)
                    <div>
                        <div class="grid lg:grid-cols-[70%_30%]  md:grid-cols-[70%_30%] p-2">
                            <div>
                                <a href="{{ route('newsletter.show', $news->slug) }}">
                                    <p class="text-2xl hover:text-kamtuu-primary font-semibold">{{ $news->title }}</p>
                                    <p class="text-gray-400">{{ $news->subtitle }}</p>
                                    <br><br>
                                    <p class="text-gray-500">By <span class="font-semibold text-black"> Raina Ong
                                        </span>·
                                        <span>{{ $news->updated_at->format('Y-m-d') }}</span>
                                    </p>
                                </a>
                            </div>
                            <div>
                                <img class="rounded-lg w-[255px] h-[150px] object-cover"
                                    src="{{ Storage::url($news->image_primary) }}" alt="">
                            </div>
                        </div>
                    </div>
                    <hr>
                @endforeach

                {{--            --}}{{-- artikel --}}
                {{--            <div> --}}
                {{--                <div class="grid lg:grid-cols-[70%_30%]  md:grid-cols-[70%_30%] p-2"> --}}
                {{--                    <div> --}}
                {{--                        <a href="/destindonesia/article"> --}}
                {{--                            <p class="text-2xl hover:text-kamtuu-primary font-semibold">Fukushima Sake Brewery</p> --}}
                {{--                            <p class="text-gray-400">Visiting Suehiro in Aizu-Wakamatsu City, one of Fukushima…</p> --}}
                {{--                            <br><br> --}}
                {{--                            <p class="text-gray-500">By <span class="font-semibold text-black"> Raina Ong </span>· --}}
                {{--                                <span>September 14, 2021</span></p> --}}
                {{--                        </a> --}}
                {{--                    </div> --}}
                {{--                    <div> --}}
                {{--                        <img class="rounded-lg" src="https://www.japan-guide.com/blog/g/koyo20_201104_karuizawa_t2.jpg" --}}
                {{--                             alt=""> --}}
                {{--                    </div> --}}
                {{--                </div> --}}
                {{--            </div> --}}
                {{--            <hr> --}}

                {{--            --}}{{-- artikel --}}
                {{--            <div> --}}
                {{--                <div class="grid lg:grid-cols-[70%_30%]  md:grid-cols-[70%_30%] p-2"> --}}
                {{--                    <div> --}}
                {{--                        <a href="/destindonesia/article"> --}}
                {{--                            <p class="text-2xl hover:text-kamtuu-primary font-semibold">Fukushima Sake Brewery</p> --}}
                {{--                            <p class="text-gray-400">Visiting Suehiro in Aizu-Wakamatsu City, one of Fukushima…</p> --}}
                {{--                            <br><br> --}}
                {{--                            <p class="text-gray-500">By <span class="font-semibold text-black"> Raina Ong </span>· --}}
                {{--                                <span>September 14, 2021</span></p> --}}
                {{--                        </a> --}}
                {{--                    </div> --}}
                {{--                    <div> --}}
                {{--                        <img class="rounded-lg" src="https://www.japan-guide.com/blog/g/koyo20_201104_karuizawa_t2.jpg" --}}
                {{--                             alt=""> --}}
                {{--                    </div> --}}
                {{--                </div> --}}
                {{--            </div> --}}
                {{--            <hr> --}}

            </div>
        </div>
    </div>


    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

</body>

</html>
