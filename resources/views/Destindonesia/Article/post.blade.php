<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kamtuu Home') }}</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <style>
        #social-links ul {
            padding-left: 0;
        }

        #social-links ul li {
            display: inline-block;
        }

        #social-links ul li a {
            padding: 6px;
            border: 1px solid #ccc;
            border-radius: 5px;
            margin: 1px;
            font-size: 25px;
        }

        #social-links .fa-facebook {
            color: #0d6efd;
        }

        #social-links .fa-twitter {
            color: deepskyblue;
        }

        #social-links .fa-linkedin {
            color: #0e76a8;
        }

        #social-links .fa-whatsapp {
            color: #25D366
        }

        #social-links .fa-reddit {
            color: #FF4500;;
        }

        #social-links .fa-telegram {
            color: #0088cc;
        }
    </style>
    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
<header class="border-b border-[#9E3D64]">
    <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
</header>
<div class="card-body">


</div>
<div class="max-w-4xl mx-auto my-14">
    <div class="mx-2">
        {{--    kategori artikel--}}
        <div class="max-w-lg grid grid-cols-3">
            @foreach($newsletters->tags as $tagged)
                <a href="{{route('newsletter.category', $tagged->slug)}}">
                <div class="rounded bg-kamtuu-second p-2 hover:bg-kamtuu-primary duration-500 m-2 flex text-white">
                    <img class="mr-2" width="18px"
                         src="{{ asset('storage/icons/circle-white-category.svg') }}"
                         alt="user-group">
                    {{$tagged->name}}
                </div>
                </a>
                {{--                <div class="mr-5">--}}
                {{--                    <a href="#">--}}
                {{--                        <div--}}
                {{--                            class="border border-gray-500 bg-kamtuu-second text-white w-full h-10 rounded-lg--}}
                {{--                            place-content-center--}}
                {{--                             p-3 hover:bg-kamtuu-primary duration-500">--}}
                {{--                            <div class="flex">--}}
                {{--                            <img class="mr-2" width="18px"--}}
                {{--                                 src="{{ asset('storage/icons/circle-white-category.svg') }}"--}}
                {{--                                 alt="user-group">--}}
                {{--                                <p class="">{{$tag->name}}</p>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </a>--}}
                {{--                </div>--}}
            @endforeach


        </div>


        {{--Title--}}
        {{--        @foreach($newsletters as $news)--}}
        <h1 class="font-semibold text-5xl my-2">{{$newsletters->title}}</h1>
        <div class="grid grid-cols-[70%_30%] my-5">
            <div>
                <h2 class="text-gray-500 text-xl my-2 font-normal">{{$newsletters->subtitle}}</h2>
            </div>
            <div class="flex justify-end">
                {{--            <div class="hover:bg-gray-200 hover:rounded-full duration-700">--}}
                {{--                <img class="p-1" width="48px"--}}
                {{--                     src="{{ asset('storage/icons/icons8-facebook.svg') }}"--}}
                {{--                     alt="user-group">--}}
                {!! $shareButtons1 !!}
                {{--            </div>--}}
                {{--            <div>--}}
                {{--                <img class="hover:bg-gray-200 hover:rounded-full duration-700" width="48px"--}}
                {{--                     src="{{ asset('storage/icons/icons8-twitter.svg') }}"--}}
                {{--                     alt="user-group">--}}
                {{--            </div>--}}
            </div>
        </div>

        {{--image title--}}
        <img src="{{ Storage::url($newsletters->image_primary) }}" alt="">
        <p class="text-center text-gray-400">Image Name</p>

        <div class="grid  justify-items-center">
            <div class="max-w-3xl grid grid-cols-1 lg:grid-cols-[15%_85%]">

                {{--            Navigation--}}
                <div class="grid col-span-2 lg:justify-items-end md:justify-items-end justify-items-center">
                    <div class="border-t-2 rounded-t-xl border-kamtuu-second">
                        <div class="border border-gray-400 p-5 shadow-lg rounded-xl">
                            <div class="font-semibold text-left">
                                <p>In this article</p>
                            </div>
                            <hr>
                            <div>
                                <a href="#">
                                    <p class="flex font-sm font-medium text-left"><img class="mr-2" width="18px"
                                                                                       src="{{ asset('storage/icons/circle-white-category.svg') }}"
                                                                                       alt="user-group">Day 1 : Lorem
                                        ipsum
                                        dolor sit amet.</p>
                                </a>
                            </div>
                            <hr>
                            <div>
                                <a href="#">
                                    <p class="flex font-sm font-medium text-left"><img class="mr-2" width="18px"
                                                                                       src="{{ asset('storage/icons/circle-white-category.svg') }}"
                                                                                       alt="user-group">Day 2 : Lorem
                                        ipsum
                                        dolor sit amet.</p>
                                </a>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>

                {{--            profile desktop--}}
                <div class="justify-self-stretch hidden lg:block">
                    <a href="/destindonesia/article/profile">
                        <img class="rounded-full w-24 h-24" src="{{ asset('storage/img/foto-profile.png') }}" alt="">
                        <hr class="w-8 my-2 !border-kamtuu-primary border-b">
                        <p class="hover:text-kamtuu-second">Minnie</p>
                        <p class="text-gray-400">Staff Writer,</p>
                        <p class="text-gray-400">Kamtuu</p>
                    </a>
                </div>

                {{--            Isi Article--}}
                <div class="justify-self-stretch">
                    {{--                <p class="my-2">--}}
                    {{--                    The mountainous region of Okutama lies in western Tokyo and can be--}}
                    {{--                    conveniently accessed from central Tokyo by train in two hours or less. Okutama is by no means the--}}
                    {{--                    metropolitan, bustling city commonly associated with the name Tokyo.--}}
                    {{--                </p>--}}
                    {{--                <p class="my-2">--}}
                    {{--                    Most of Okutama is within the Chichibu-Tama-Kai National Park and the majority of it is covered by--}}
                    {{--                    mountains and forests. As such, visitors can look forward to peace and quiet, an abundance of nature--}}
                    {{--                    and a multitude of outdoor activities. In other words, Okutama is the perfect escape from central--}}
                    {{--                    Tokyo.--}}
                    {{--                </p>--}}
                    {{--                <p class="my-2">--}}
                    {{--                    For my maiden visit, I went for some of the best activities Okutama has to offer like forest therapy--}}
                    {{--                    and--}}
                    {{--                    wasabi farming. Many of the activities I participated in had a recurring theme of engaging my five--}}
                    {{--                    senses to fully appreciate the present, which compelled me to slow down. My overnight trip to--}}
                    {{--                    Okutama--}}
                    {{--                    was like an extended mindfulness retreat, which was extremely refreshing and made me reluctant to--}}
                    {{--                    return--}}
                    {{--                    to the city.--}}
                    {{--                </p>--}}
                    {!!  $newsletters->description !!}
                </div>
            </div>
            {{--            profile mobile--}}
            <div class="block lg:hidden md:block grid justify-items-center">
                <a href="/destindonesia/article/profile">
                    <img class="rounded-full w-24 h-24" src="{{ asset('storage/img/foto-profile.png') }}" alt="">
                    <hr class="w-8 my-2 !border-kamtuu-primary border-b">
                    <p>Minnie</p>
                    <p class="text-gray-400">Staff Writer,</p>
                    <p class="text-gray-400">Kamtuu</p>
                </a>
            </div>
        </div>
    </div>

</div>
{{--@endforeach--}}

<footer>
    <x-destindonesia.footer></x-destindonesia.footer>
</footer>

</body>

</html>
