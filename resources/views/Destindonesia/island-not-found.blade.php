<!DOCTYPE html>
<html>
<head>
    <title>Request not found</title>


    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>
<body>
<header>
    <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
</header>
<div class="error-container">
    <img class="animated-image" src="{{ asset('storage/images/traveller_error.png') }}" alt="traveler">
    <h1 class="animated-text">Oops! We can't find the request.</h1>
    <h1 class="animated-text">Error 401</h1>
    <a href="{{route('destindonesia.index')}}">
        <button class="animated-button">Go back</button>
    </a>
</div>

<footer>
    <x-destindonesia.footer></x-destindonesia.footer>
</footer>
</body>
</html>
