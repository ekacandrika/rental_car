<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
        {{-- Destindonesia NavBar --}}
        <div
            class="bg-transparent lg:container mx-auto px-2 h-[75px] lg:h-[100px] border-b-[1px] border-white-600 hidden sm:block relative z-10">
            <div class="grid grid-cols-6 h-full">
                <a href="#"
                    class="text-lg md:text-xl lg:text-3xl text-kamtuu-primary font-extrabold lg:font-semibold my-auto">DestIndonesia</a>
                <a href="#"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-kamtuu-primary">Destinasi</a>
                <a href="{{ route('newsletter.destindonesia') }}"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-kamtuu-primary">Newsletter</a>
                <a href="#"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-kamtuu-primary">Event</a>
                <a href="#"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-kamtuu-primary">Videos</a>
                
            </div>
        </div>
    </header>

    {{-- Hero --}}
    <div class="relative sm:-translate-y-[100px] hidden">
        <img class="w-full object-cover" src="{{ asset('storage/img/destIndonesia-1.png') }}" alt="">
        <div
            class="absolute translate-y-1/2 translate-x-1/2 bottom-1/2 right-1/2 sm:bottom-1/4 sm:right-[10%] lg:right-[15%]">
            <p class="text-sm lg:text-xl text-white text-center sm:text-right">WelKamtuu</p>
            <p class="text-lg sm:text-2xl lg:text-4xl text-white font-bold">INDONESIA</p>
        </div>
    </div>

    {{-- Terbaru dan Ads Desktop --}}
    <div class="container mx-auto grid sm:grid-cols-3 xl:grid-cols-4 gap-10 my-5">
        <div class="sm:col-span-2 xl:col-span-3">
            <div class="hidden sm:block" x-data="cari()">
                <div class="flex justify-center my-auto bg-white rounded-full">
                    <input
                        class="placeholder:italic placeholder:text-slate-400 block bg-white w-full border border-gray-900 rounded-lg py-1 lg:py-2 pl-5 pr-3 shadow-sm focus:outline-none focus:border-gray-900 focus:ring-transparent focus:ring-1 sm:text-sm"
                        placeholder="Search" type="text" name="search" value="{{$search}}" id="search" @keyup="search()"/>
                    <button class="mx-3" @click="search()">
                        <img src="{{ asset('storage/icons/search-ic.png') }}" alt="">
                    </button>
                </div>
                <p class="text-center sm:text-left text-xs sm:text-base font-semibold mt-10 mb-5 sm:my-3">
                    Sekitar :&nbsp;<span x-text="count_datas"></span>&nbsp;dari&nbsp;<span>hasil pencarian</span>
                </p>
                {{-- Desktop --}}
                <div class="hidden sm:grid grid-cols-2 border-t border-gray-800">
                    {{-- @foreach ($result_search as $result) --}}
                    <template x-if="datas.length >= 1">
                        <template x-for="(data, index) in datas" :key="index">
                        <div class="my-5 hover:border w-full col-span-2 hover:border-gray-900 px-5 py-2">
                            <div class="grid grid-flow-row grid-rows-4">
                                <a x-bind:href="data.link">
                                    <p class="text-red-600 text-lg capitalize">
                                        <span class="font-bold" x-text="data.judul"></span>
                                    </p>
                                </a>
                                <p class="text-slate-400 font-thin text-sm truncate" x-text="data.link">
                                    link artikel ><span>...</span>
                                </p>
                                <p class="text-slate-900 font-thin text-base truncate">
                                    <span x-text="data.tgl_terbit"></span>&nbsp;<span>...</span>&nbsp;<span class="truncate" x-text="data.isi"></span>
                                </p>
                            </div>
                        </div>  
                        </template>      
                    </template>   
                    <nav aria-label="Page navigation" class="text-start mt-3">
                        <template x-if="links.length > 1">
                            <div class="flex flex-row justify-between gap-x-36" style="width: 931px;">
                                <!-- Help text -->
                                <span class="text-sm text-gray-700 dark:text-gray-400" style="width:124px;max-width:124px">
                                    Showing <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="from"></span> to <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="to">10</span> of <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="total_page">100</span> Entries
                                </span>
                                <ul class="inline-flex -space-x-px text-sm text-end">
                                    <li>
                                        <button type="button" class="rounded-l-lg flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white capitalize" 
                                        :class="{ 'dark:bg-gray-600 text-gray-200':links[0].label == cur_page, 'bg-red-100 text-gray-400':links[0].label != cur_page}"
                                        x-text="prev" @click="prevPage(links[0].label)"></button>
                                    </li>
                                    <template x-for="(link, index) in links" :key="index">
                                        <li>
                                                <template x-if="link.label >= 1">
                                                    <button type="button" class="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white" x-text="link.label" 
                                                    :class="{ 'dark:bg-gray-600 text-gray-200':link.label == cur_page, 'bg-red-100 text-gray-400':link.label != cur_page}" 
                                                    @click="getPage(link.url,link.label)" :id="'page-'+link.label"></button>
                                                </template>
                                                <template x-if="link.label == '...'">
                                                    <button type="button" class="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400" x-text="link.label" 
                                                    :class="{ 'dark:bg-gray-600 text-gray-200':link.label == cur_page, 'bg-red-100 text-gray-400':link.label != cur_page}" 
                                                    @click="getPage(link.url,link.label)" :id="'page-'+link.label" disabled></button>
                                                </template>
                                        </li>
                                    </template>
                                    <li>
                                        <button type="button" class="rounded-r-lg flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white capitalize" x-text="next" 
                                        :class="{ 'dark:bg-gray-600 text-gray-200':links[(links.length*1)-1].label == cur_page, 'bg-red-100 text-gray-400':links[(links.length*1)-1].label != cur_page}"
                                        :class="{'cursor-not-allowed':links[(links.length*1)-1].url==null,'cursor-pointer':links[(links.length*1)-1].url!=null}" 
                                        @click="nextPage(links[(links.length*1)-1].label)"></button>
                                    </li>
                                </ul>
                            </div>
                        </template>
                    </nav>   
                    {{-- @endforeach --}}
                    {{-- {{$result_search->links()}} --}}
                </div>
            </div>
            <div class="sm:hidden" x-data="mobileSearch()">
                <div class="flex justify-center my-auto bg-white rounded-full">
                    <input
                        class="placeholder:italic placeholder:text-slate-400 block bg-white w-full border border-gray-900 rounded-lg py-1 lg:py-2 pl-5 pr-3 shadow-sm focus:outline-none focus:border-gray-900 focus:ring-transparent focus:ring-1 sm:text-sm"
                        placeholder="Search" type="text" name="mobil-search" value="{{$search}}" id="mobile-search" @keyup="search()" style="width: 291px;"/>
                    <button class="mx-3" @click="search()">
                        <img src="{{ asset('storage/icons/search-ic.png') }}" alt="">
                    </button>
                </div>
                <p class="text-center text-xs sm:text-base font-semibold mt-5 mb-5 sm:my-3">
                    Sekitar :&nbsp;<span x-text="count_datas"></span>&nbsp;dari&nbsp;<span>hasil pencarian</span>
                </p>
                <div class="sm:grid grid-cols-2 border-t border-gray-800">
                    {{-- @foreach ($result_search as $result) --}}
                    <template x-if="datas.length >= 1">
                        <template x-for="(data, index) in datas" :key="index">
                        <div class="my-5 hover:border w-full col-span-2 hover:border-gray-900 px-5 py-2">
                            <div class="grid grid-flow-row grid-rows-4">
                                <a x-bind:href="data.link">
                                    <p class="text-red-600 text-lg capitalize">
                                        <span class="font-bold" x-text="data.judul"></span>
                                    </p>
                                </a>
                                <p class="text-slate-400 font-thin text-sm truncate" x-text="data.link">
                                    link artikel ><span>...</span>
                                </p>
                                <p class="text-slate-900 font-thin text-base truncate">
                                    <span x-text="data.tgl_terbit"></span>&nbsp;<span>...</span>&nbsp;<span class="truncate" x-text="data.isi"></span>
                                </p>
                            </div>
                        </div>  
                        </template>      
                    </template>   
                    <nav aria-label="Page navigation" class="text-center mt-3">
                        
                        <template x-if="links.length > 1">
                            <div class="flex flex-col items-center">
                                <!-- Help text -->
                                <span class="text-sm text-gray-700 dark:text-gray-400">
                                    Showing <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="from"></span> to <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="to">10</span> of <span class="font-semibold text-gray-900 dark:text-gray-400" x-text="total_page">100</span> Entries
                                </span>
                                <!-- Buttons -->
                                <div class="inline-flex mt-2 xs:mt-0">
                                    <button type="button" class="rounded-l-lg flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white capitalize" 
                                    :class="{ 'dark:bg-gray-600 text-gray-200':links[0].label == cur_page, 'bg-red-100 text-gray-400':links[0].label != cur_page}"
                                    x-text="prev" @click="prevPage(links[0].label)"></button>
                                    <button type="button" class="rounded-r-lg flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white capitalize" x-text="next" 
                                    :class="{ 'dark:bg-gray-600 text-gray-200':links[(links.length*1)-1].label == cur_page, 'bg-red-100 text-gray-400':links[(links.length*1)-1].label != cur_page}"
                                    :class="{'cursor-not-allowed':links[(links.length*1)-1].url==null,'cursor-pointer':links[(links.length*1)-1].url!=null}" 
                                    @click="nextPage(links[(links.length*1)-1].label)"></button>
                                </div>
                            </div>
                        </template>
                    </nav>
                </div>
            </div>
        </div>
        <div class="hidden sm:block">
            <p class="mx-5 text-3xl font-semibold my-3">Article (Ads)</p>
            <div class="w-[306px] shadow-xl divide-y rounded-y rounded-lg p-5 mx-5 my-5">
                @if($articles)
                    @foreach ($articles as $iklan => $item)
                        <div class="grid grid-cols-2">
                            <div class="p-3">
                                <img src="{{ isset($item->thumbnail) ? Storage::url($item->thumbnail) : 'https://via.placeholder.com/109x109' }}"
                                    alt="tailwind logo" class="rounded-xl object-cover object-center" />
                            </div>

                            <a href="{{ route('newsletter.show', $item->slug) }}" class="p-3 ">
                                <h2 class="font-bold">{{ $item->title }}</h2>
                                <div class="truncate">
                                    {!! $item->description !!}
                                </div>
                            </a>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    {{-- Ads Mobile --}}
    <div class="block lg:hidden mt-10">
        <p class="text-xl font-semibold mb-3 text-center sm:text-left">Article (Ads)</p>
        <x-destindonesia.ads-list></x-destindonesia.ads-list>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>

        function clearTagHtml(html){
            if(html){
               return html.replace( /(<([^>]+)>)/ig, '').replace(/\&nbsp;/g, ' ');

            }else{
                return html;
            }
        }

        document.addEventListener("alpine:init",()=>{
            Alpine.data("cari",()=>({
                datas:[],
                links:[],
                url:"{{route('search.result')}}",
                total_page:'',
                active:'',
                to:'',
                from:'',
                count_datas:0,
                current_page:null,
                cur_page:null,
                prev:null,
                next:null,
                next_page:1,
                prev_page:0,
                firs_page:null,
                last_page:null,
                async init(){
                    
                    let search = $("#search").val();
                   
                    if(search){
                        let loads = await fetch(`${this.url}?cari=${search}`);
                        let objs  = await loads.json();
                        
                        let arr_link = [];
                        let arr_datas = [];

                        Object.keys(objs.data).forEach(key=>{
                            this.datas.push({
                                'judul':objs.data[key].judul,   
                                'link':objs.data[key].link,   
                                'tgl_terbit':objs.data[key].tgl_terbit,   
                                //'isi':objs.data[key].isi.replace( /(<([^>]+)>)/ig, '').replace(/\&nbsp;/g, ''),   
                                'isi':clearTagHtml(objs.data[key].isi)
                            })
                        })

                        Object.keys(objs.links).forEach(key=>{
                            arr_link.push(objs.links[key])
                        })
                        
                        this.links = arr_link;
                        last_index = (this.links.length*1)-1;

                        this.prev = this.links[0].label.replace('pagination.previous','previous');
                        this.next = this.links[last_index].label.replace('pagination.next','next')

                        this.total_page = objs.total;
                        this.cur_page = objs.current_page;  
                        this.current_page = objs.current_page;  
                        this.count_datas = objs.total;
                        this.from = objs.from;
                        this.to = objs.to;
                        
                        //console.log(objs)

                        if(this.cur_page == null){
                            this.cur_page = 1
                           
                        }
                        
                        this.first_page = 1
                        this.last_page = objs.last_page
                    }
                    
                },
                search(){
                   
                    var cari   = $("#search").val();                   
                    let page = '&cur_page=1';

                    if(cari.length > 0){
                        page = '&cur_page='+this.cur_page;
                    }

                    console.log(page)
                    
                    fetch(`${this.url}?cari=${cari}`)
                    .then(response =>response.json())
                    .then(data=>{
                        this.datas=[]
                       
                        Object.keys(data.data).forEach(key=>{
                            this.datas.push({
                                'judul':data.data[key].judul,   
                                'link':data.data[key].link,   
                                'tgl_terbit':data.data[key].tgl_terbit, 
                                'isi':clearTagHtml(data.data[key].isi)    
                            })
                        })

                        this.links=data.links
                        this.count_datas = data.total;

                        this.total_page = data.total
                        this.from = data.from;
                        this.to = data.to;
                        this.count_datas=this.total_page;
                        this.last_page = data.last_page;
                    })
                },
                fetching(params){
                    fetch(`${this.url}${params}`)
                    .then(resp => resp.json())
                    .then(data=>{
                        this.datas = [];
                        this.links = [];

                        let obj = data.data;
                        
                        if(obj.length != undefined){
                            Object.keys(obj).forEach(key=>{
                                this.datas.push({
                                   'judul':obj[key].judul,
                                   'link':obj[key].link,
                                   'tgl_terbit':obj[key].tgl_terbit,
                                   'isi':clearTagHtml(obj[key].isi)
                                   //'isi':obj[key].isi.replace(/(<([^>]+)>)/ig, ''),
                                })
                            })
                        }else{

                            Object.keys(obj).forEach(key=>{
                                this.datas.push({
                                   'judul':obj[key].judul,
                                   'link':obj[key].link,
                                   'tgl_terbit':obj[key].tgl_terbit,
                                   'isi':clearTagHtml(obj[key].isi)
                                })
                            })
                        }

                        this.links = data.links;
                        this.total_page = data.total
                        this.from = data.from;
                        this.to = data.to;
                        this.count_datas=this.total_page;
                        this.last_page = data.last_page;
                    })
                },
                getPage(page, label){

                    this.cur_page = label

                    let search = $("#search").val();
                    let query_params ="";

                    this.cur_page = label
                    
                    if(page){
                        page = page.replace("/?page=","cur_page=");    
                    }else{
                       page = 'cur_page=1'; 
                    }

                    if(search){
                        query_params +=`/?cari=${search}&${page}`
                    }else{
                       query_params +=`/?${page}` 
                    }
                    console.log(query_params)
                    
                    this.current_page = page;
                    this.next_page = label;

                    this.fetching(query_params)

                },
                nextPage(label){
                    let search = $("#search").val();
                    let query_params = "";
                    
                    document.getElementById("page-"+this.cur_page).classList.add("bg-red-100");
                    document.getElementById("page-"+this.cur_page).classList.remove("text-gray-200");
                    
                    if(this.last_page != this.next_page){
                        this.next_page++
                    }

                    this.cur_page +=1;
                    let next = this.next_page;

                    document.getElementById("page-"+next).classList.remove("bg-red-100");
                    document.getElementById("page-"+next).classList.add("text-gray-200");
                    
                    if(search){
                        query_params +=`/?cari=${search}&cur_page=${this.next_page}`
                    }else{
                       query_params +=`/?cur_page=${this.next_page}` 
                    }

                    this.fetching(query_params)
                },
                prevPage(label){
                    let search = $("#search").val();
                    let query_params = "";
                    this.prev_page = this.next_page;

                    document.getElementById("page-"+this.cur_page).classList.add("bg-red-100");
                    document.getElementById("page-"+this.cur_page).classList.remove("text-gray-200");

                    if(this.first_page != this.prev_page){
                        this.prev_page--
                    }
                    this.cur_page -=1

                    this.next_page=this.prev_page
                    
                    document.getElementById("page-"+this.prev_page).classList.remove("bg-red-100");
                    document.getElementById("page-"+this.prev_page).classList.add("text-gray-200");

                    if(search){
                        query_params +=`/?cari=${search}&cur_page=${this.prev_page}`
                    }else{
                       query_params +=`/?cur_page=${this.prev_page}` 
                    }

                    console.log(this.prev_page)
                    this.fetching(query_params)
                },
            }))
        })

        document.addEventListener("alpine:init",()=>{
            Alpine.data("mobileSearch",()=>({
                datas:[],
                links:[],
                url:"{{route('search.result')}}",
                total_page:'',
                active:'',
                to:'',
                from:'',
                count_datas:0,
                current_page:null,
                cur_page:null,
                prev:null,
                next:null,
                next_page:1,
                prev_page:0,
                firs_page:null,
                last_page:null,
                async init(){
                    
                    let search = $("#mobile-search").val();
                   
                    if(search){
                        let loads = await fetch(`${this.url}?cari=${search}`);
                        let objs  = await loads.json();
                        let arr_link = [];

                        Object.keys(objs.data).forEach(key=>{
                            this.datas.push({
                                'judul':objs.data[key].judul,   
                                'link':objs.data[key].link,   
                                'tgl_terbit':objs.data[key].tgl_terbit,   
                                'isi':clearTagHtml(objs.data[key].isi)
                            })
                        })
                       
                        Object.keys(objs.links).forEach(key=>{
                            arr_link.push(objs.links[key])
                        })
                        
                        this.links = arr_link;
                        last_index = (this.links.length*1)-1;

                        this.prev = this.links[0].label.replace('pagination.previous','previous');
                        this.next = this.links[last_index].label.replace('pagination.next','next')

                        this.total_page = objs.total;
                        this.cur_page = objs.current_page;  
                        this.current_page = objs.current_page;  
                        this.count_datas = objs.total;
                        this.from = objs.from;
                        this.to = objs.to;
                        
                        //console.log(objs)

                        if(this.cur_page == null){
                            this.cur_page = 1
                           
                        }
                        
                        this.first_page = 1
                        this.last_page = objs.last_page
                    }
                    
                },
                search(){
                   
                    var cari   = $("#mobile-search").val();                   
                    let page = '&cur_page=1';

                    if(cari.length > 0){
                        page = '&cur_page='+this.cur_page;
                    }

                    console.log(page)
                    let query_params = `/?cari=${cari}`;
                    
                    this.fetching(query_params)
                },
                fetching(query){
                    console.log(query);

                    fetch(`${this.url}${query}`)
                    .then(resp => resp.json())
                    .then(data=>{

                        this.datas = [];
                        this.total_page = 0;
                        
                        this.from = 0;
                        this.to = 0;
                        
                        let obj = data.data;
                        if(obj.length != undefined){
                            Object.keys(obj).forEach(key=>{
                                this.datas.push({
                                   'judul':obj[key].judul,
                                   'link':obj[key].link,
                                   'tgl_terbit':obj[key].tgl_terbit,
                                   'isi':clearTagHtml(obj[key].isi)
                                })
                            })
                        }else{

                            Object.keys(obj).forEach(key=>{
                                this.datas.push({
                                   'judul':obj[key].judul,
                                   'link':obj[key].link,
                                   'tgl_terbit':obj[key].tgl_terbit,
                                   'isi':clearTagHtml(obj[key].isi)
                                })
                            })
                        }

                        this.total_page = data.total
                        this.from = data.from;
                        this.to = data.to;
                        this.count_datas=this.total_page;
                        this.last_page = data.last_page;
                    })
                },
                nextPage(label){
                    let search = $("#mobile-search").val();
                    let query_params = "";

                    if(this.last_page != this.next_page){
                        this.next_page++
                    }
                    
                    if(search){
                        query_params +=`/?cari=${search}&cur_page=${this.next_page}`
                    }else{
                       query_params +=`/?cur_page=${this.next_page}` 
                    }
                    console.log(this.next_page)

                    this.fetching(query_params);
                },
                prevPage(label){
                    let search = $("#mobile-search").val();
                    let query_params = "";
                    this.prev_page = this.next_page;

                    if(this.first_page != this.prev_page){
                        this.prev_page--
                    }
                    
                    this.next_page=this.prev_page

                    if(search){
                        query_params +=`/?cari=${search}&cur_page=${this.prev_page}`
                    }else{
                       query_params +=`/?cur_page=${this.prev_page}` 
                    }

                      this.fetching(query_params);

                }
            }))
        })

        var swiper = new Swiper(".productSwiper", {
            slidesPerView: 1,
            loop: true,
            navigation: {
                enabled: true,
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    navigation: {
                        enabled: true
                    },
                    pagination: {
                        enabled: false
                    }
                },
                1024: {
                    slidesPerView: 3,
                    navigation: {
                        enabled: false
                    },
                    pagination: {
                        enabled: true
                    }
                },
                1280: {
                    slidesPerView: 5,
                    navigation: {
                        enabled: false,
                    },
                    pagination: {
                        enabled: false
                    }
                }
            },
        });
    </script>
    <script>
        var objectSwiper = new Swiper(".objectSwiper", {
            slidesPerView: 3,
            grid: {
                rows: 2,
                fill: 'row'
            },
            spaceBetween: 30,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
        });
    </script>
    <script>
        var carouselDestindonesiaSwiper = new Swiper(".carousel-destindonesia-swiper", {
            // loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            }
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        var reviewSwipper = new Swiper(".review-swipper", {
            loop: false,
            slidesPerView: 1,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    </script>
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>

    <script>

    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body>

</html>
