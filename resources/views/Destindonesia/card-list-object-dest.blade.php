<div class="grid lg:grid-cols-3 lg:grid-rows-none grid-rows-3 gap-2 lg:gap-5 lg:my-5 my-0 lg:mt-5 mt-5">
 
 @forelse($objeks as $key => $obj)
    <div class="hover:shadow-lg hover:-translate-y-2">
        <a class="lg:block hidden" href="{{route('wisataShow',$obj->slug)}}">
            <div class="px-4 absolute h-20 items-center justify-center flex">
                <p
                    class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white shadow-md font-bold text-[#D50006]">
                    {{$key+1}}</p>
            </div>
            <div
                class="max-w-sm w-[150px] lg:w-full bg-white rounded-lg border border-gray-200 shadow dark:border-gray-700">
                <img class="rounded-t-lg h-[201px] w-[301px]" src="{{Storage::url($obj->header)}}" alt="">
                <div class="p-2 lg:p-4">
                    <h5
                        class="mb-px text-sm lg:text-xl font-bold tracking-tight text-[#333333] hover:text-[#9E3D64]">
                        {{$obj->title}}</h5>
                    {{-- <p
                        class="truncate mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                        {!!$obj->deskripsi!!}
                    </p> --}}
                </div>
            </div>
        </a>
    </div>
    @empty
    <div class="text-center">
        <p>
            Belum Ada Objek
        </p>
    </div>
@endforelse

{{-- For Mobile --}}
@forelse($objeks as $key => $obj)

    <a href="{{route('wisataShow',$obj->slug)}}"
        class="lg:hidden block flex flex-cols bg-white rounded-lg border shadow-md hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700">
        <div class="lg:hidden block px-4 my-2 absolute w-52 h-14 items-center justify-center flex">
            <p
                class="inline-flex items-center justify-center lg:text-lg text-sm w-6 h-6 rounded-full lg:w-8 lg:h-8 bg-white shadow-md font-bold text-[#D50006]">
                {{$key+1}}
            </p>
        </div>
        <img class="object-cover h-[70px] w-[100px] rounded-l-lg"
            src="{{ isset($obj->header) ? Storage::url($obj->header) : 'https://via.placeholder.com/100x70' }}"
            alt="">
        <div class="p-2 px-4 lg:px-px lg:p-4">
            <h5
                class="mb-px text-sm lg:text-xl font-bold tracking-tight text-[#333333] hover:text-[#9E3D64]">
                {{$obj->title}}</h5>
        </div>
    </a>
    @empty
    <div class="text-center lg:hidden block">
        Objek Belum ditambahkan
    </div>
@endforelse
</div>
{{$objeks->links()}}

