
<div class="w-full hidden lg:grid gap-5 lg:grid-cols-4">
    @foreach($wisata as $index => $objek_wisata)
    <x-destindonesia.card-destination index="{{$index}}" lokasi="provinsi" 
    text="{{$objek_wisata->title}}" slug="{{route('wisataShow',$objek_wisata->slug)}}"
    header="{{$objek_wisata->header}}">
    </x-destindonesia.card-destination>
    @endforeach
</div>
<div class="w-full hidden md:grid lg:hidden md:grid-cols-3">
    @foreach($wisata as $index => $objek_wisata)
    <x-destindonesia.card-destination index="{{$index}}" lokasi="provinsi" 
    text="{{$objek_wisata->title}}" slug="{{route('wisataShow',$objek_wisata->slug)}}"
    header="{{$objek_wisata->header}}">
    </x-destindonesia.card-destination>
    @if($index==2)
    @break
    @endif
    @endforeach
</div>
<div class="w-full grid md:hidden gap-5 grid-cols-2 lg:overflow-x-auto">
    @foreach($wisata as $index => $objek_wisata)
    <x-destindonesia.card-destination index="{{$index}}" lokasi="provinsi" 
    text="{{$objek_wisata->title}}" slug="{{route('wisataShow',$objek_wisata->slug)}}"
    header="{{$objek_wisata->header}}">
    </x-destindonesia.card-destination>
    {{-- @if($index==1) --}}
    {{-- @break --}}
    {{-- @endif --}}
    @endforeach
</div>
