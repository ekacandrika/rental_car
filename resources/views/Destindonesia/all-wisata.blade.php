<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kota Kabupaten') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    {{--
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">--}}
    {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>--}}
    <style>
        .rate {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rate:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rate:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rate:not(:checked)>label:before {
            content: '★ ';
        }

        .rate>input:checked~label {
            color: #ffc700;
        }

        .rate:not(:checked)>label:hover,
        .rate:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rate>input:checked+label:hover,
        .rate>input:checked+label:hover~label,
        .rate>input:checked~label:hover,
        .rate>input:checked~label:hover~label,
        .rate>label:hover~input:checked~label {
            color: #c59b08;
        }

        .star-rating-complete {
            color: #c59b08;
        }

        .rating-container .form-control:hover,
        .rating-container .form-control:focus {
            background: #fff;
            border: 1px solid #ced4da;
        }

        .rating-container textarea:focus,
        .rating-container input:focus {
            color: #000;
        }

        .rated {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rated:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ffc700;
        }

        .rated:not(:checked)>label:before {
            content: '★ ';
        }

        .rated>input:checked~label {
            color: #ffc700;
        }

        .rated:not(:checked)>label:hover,
        .rated:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rated>input:checked+label:hover,
        .rated>input:checked+label:hover~label,
        .rated>input:checked~label:hover,
        .rated>input:checked~label:hover~label,
        .rated>label:hover~input:checked~label {
            color: #c59b08;
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>
    <nav class="flex ml-9 mt-2" aria-label="Breadcrumb">
        {{-- @dump($pulaus) --}}
        <ol class="inline-flex items-center space-x-1 md:space-x-3 text-base lg:text-lg">
            <li class="inline-flex items-center">
                <a href="{{isset($pulaus->slug) ? route('kabupatenShow',$pulaus->slug) : '#'}}"
                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] capitalize">{{$pulaus->nama_pulau}}</a>
            </li>
            <li>
                <div class="flex items-center">
                    <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clip-rule="evenodd"></path>
                        </svg>
                        <a href="#"
                    class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1] capitalize">semua wisata di kota/kabupaten {{$pulaus->nama_pulau}}</a>
                </div>
            </li>
        </ol>
    </nav>
    <div class="p-5">
        {{-- {{dd($provinces);}} --}}
        <div class="container mx-auto m-5">
            <p class="my-5 text-center text-3xl font-semibold">Semua Wisata di {{ $pulaus->title }}</p>
            <div class="flex sm:block mx-5" x-data="kategoriWisata()">
                <ul
                    class="flex justify-start overflow-x-auto text-sm font-medium text-center text-gray-500 dark:text-gray-400">
                    <template x-for="(tab, index) in tabs" :key="index">
                        {{-- 
                        <li class="mx-2 mt-2">
                            <button x-on:click="active = 'Tur'" aria-current="page"
                                class="inline-block py-2 px-9 rounded-lg active"
                                :class="active == 'Tur' ? 'text-white bg-[#23AEC1]' :
                                    'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]'">Tour</button>
                        </li>
                        --}}
                        <li class="mx-2 mt-2">
                            <button x-on:click="active = index" class="inline-block py-2 px-9 h-full text-white bg-[#23AEC1] rounded-lg"
                                :class="{ 'text-white bg-[#23AEC1]': active ==
                                    index, 'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]': active !==
                                        index }"
                                x-text="tab" @click="change(index)">
                            </button>
                        </li>
                    </template>
                </ul>
            </div> 
            <div class="flex justify-start">
                <div id="objek-wisata-all">
                    <div class="w-full grid gap-5 grid-cols-2 sm:grid-cols-3 lg:grid-cols-4">
                        @foreach ($wisata as $index=>$objek)
                        <x-destindonesia.card-destination index="{{ $index }}" lokasi="wisata"
                            text="{{ $objek->title }}" slug="{{ route('wisataShow', $objek->slug) }}"
                            header="{{ $objek->header }}">
                        </x-destindonesia.card-destination>
                        @endforeach
                    </div>
                </div>
                <div id="objek-wisata-filter-all">

                </div>
                {{-- <div class="flex justify-center w-full md:hidden">
                    @foreach ($provinces as $index=>$province)
                    <x-destindonesia.card-destination index="{{ $index }}" lokasi="provinsi"
                        text="{{ $province->title }}" slug="{{ $province->slug }}">
                    </x-destindonesia.card-destination>
                    @endforeach
                </div> --}}
            </div>
        </div>

    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>
</body>
<script>
    function kategoriWisata(){
        return{
            tabs:{!! isset($kategoriWisata) ? $kategoriWisata:'[]' !!},
            active:0,
            change(index){
                this.ajaxRequest(this.tabs[index])
            },
            remove(data){
                let count = data.length;

                for(let i = 0; i < count;i++){
                    data.splice(i,1)
                }

                return data;
            },
            ajaxRequest(param){

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })

                $.ajax({
                    method:'GET',
                    url:"{{route('all-object.by-category')}}",
                    // dataType:"html",
                    data:{
                        category:param,
                        kabupaten_id:'{!! $pulaus->kabupaten_id !!}',
                        objek:'wisata'
                    },
                    success:function(resp){
                        console.log(resp)
                        console.log($("#objek-wisata-all"))
                        $("#objek-wisata-all").hide(); 
                        $("#objek-wisata-filter-all").show();
                        $("#objek-wisata-filter-all").html(resp)             
                    }
                })
            }
        }
    }
</script>
</html>