<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kota Kabupaten') }}</title>

    {{-- jquery --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    {{-- sweet alert --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.all.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.27/dist/sweetalert2.min.css" rel="stylesheet">
    
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        .rate {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rate:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rate:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rate:not(:checked)>label:before {
            content: '★ ';
        }

        .rate>input:checked~label {
            color: #ffc700;
        }

        .rate:not(:checked)>label:hover,
        .rate:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rate>input:checked+label:hover,
        .rate>input:checked+label:hover~label,
        .rate>input:checked~label:hover,
        .rate>input:checked~label:hover~label,
        .rate>label:hover~input:checked~label {
            color: #c59b08;
        }

        .star-rating-complete {
            color: #c59b08;
        }

        .rating-container .form-control:hover,
        .rating-container .form-control:focus {
            background: #fff;
            border: 1px solid #ced4da;
        }

        .rating-container textarea:focus,
        .rating-container input:focus {
            color: #000;
        }

        .rated {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rated:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ffc700;
        }

        .rated:not(:checked)>label:before {
            content: '★ ';
        }

        .rated>input:checked~label {
            color: #ffc700;
        }

        .rated:not(:checked)>label:hover,
        .rated:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rated>input:checked+label:hover,
        .rated>input:checked+label:hover~label,
        .rated>input:checked~label:hover,
        .rated>input:checked~label:hover~label,
        .rated>label:hover~input:checked~label {
            color: #c59b08;
        }

        .maps-embed {
            padding-bottom: 60%
        }

        .maps-embed iframe {
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
            position: absolute;
        }
        @media only screen and (max-width: 600px) {
            iframe.note-video-clip{
                width: 320px;
            }
        }
    </style>
    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Hero --}}
    <div class="relative bg-slate-400">
        <img class="w-full opacity-70 object-cover" src="{{Storage::url($pulaus->header)}}" alt="">

        {{-- Kabupaten/Kota Logo --}}
        <img class="absolute translate-x-1/2 bottom-1/2 right-1/2 w-[50px] h-[50px] sm:w-[75px] sm:h-[75px] lg:w-[100px] lg:h-[100px] sm:top-10 sm:right-10 sm:bottom-0 sm:translate-x-0 shadow-xl bg-white rounded-full"
            src="{{Storage::url($pulaus->logo_daerah)}}" alt="">

        {{-- Kabupaten/Kota Name --}}
        <div class="absolute translate-y-full sm:translate-y-1/2 translate-x-1/2 bottom-1/2 right-1/2">
            <p class="text-lg sm:text-2xl lg:text-6xl text-center drop-shadow-2xl text-white font-bold">
                {{$pulaus->nama_pulau}}
            </p>
        </div>
    </div>

    <div class="container mx-auto px-5 grid sm:grid-cols-3 xl:grid-cols-4 gap-10 my-5 h-auto min-h-full">
        {{-- detail --}}
        {{-- @dump($pulau_slug) --}}
        <div class="sm:col-span-2 xl:col-span-3 flex flex-col">
            <div class="mx-auto">
                <x-destindonesia.status-list want="{{$want}}" been="{{$been}}"></x-destindonesia.status-list>
            </div>
            <nav class="flex mt-2" aria-label="Breadcrumb">
                <ol class="inline-flex items-center space-x-1 md:space-x-3 text-base lg:text-lg">
                    <li class="inline-flex items-center">
                        <a href="{{isset($pulau_slug) ? route('pulauShow',$pulau_slug) : '#'}}"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] capitalize">{{$pulau_nama}}</a>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            <a href="#"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1] capitalize">{{$pulaus->nama_pulau}}</a>
                        </div>
                    </li>
                    {{-- <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                        </div>
                    </li> --}}
                    {{-- <li>
                        <div class="flex items-center">
                           
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                          
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                           
                        </div>
                    </li> --}}
                </ol>
            </nav>
            <div class="grid grid-cols-2">
                {{-- Share FB/WA --}}
                <div class="text-base lg:text-lg flex items-center">
                    <p class="">Share</p>
                    <img src="{{asset('storage/icons/square-facebook.svg')}}" alt="fb-icon" class="mx-3" width="23px" height="23px">
                    <img src="{{asset('storage/icons/square-whatsapp.svg')}}" alt="wa-icon" width="23px" height="23px">
                </div>
                {{-- Button --}}
                <div class="flex justify-end">
                    <x-destindonesia.button-dropdown></x-destindonesia.button-dropdown>
                    {{-- <x-destindonesia.button-secondary text="Ingin Kesana">@slot('button') Ingin ke Sana @endslot
                    </x-destindonesia.button-secondary> --}}
                </div>
            </div>
            {{-- Goverment Opening --}}
            <div class="bg-gray-100 border border-gray-300 rounded-md">
                <div class="grid grid:row-2 lg:grid-cols-5 gap-5 my-10 xl:mr-24">
                    <div class="flex justify-center lg:justify-end">
                        <img src="{{Storage::url($pulaus->foto_menteri)}}" alt="" class="bg-clip-content w-24 h-24 lg:w-[96px] lg:h-[96px] xl:w-[100px] xl:h-[100px] shadow-xl bg-white rounded-full" alt="goverment-profile">
                    </div>
                    <div class="lg:col-span-4">
                        <p class="text-center lg:text-left text-slate-700">Gubernur</p>
                        <p class="text-center lg:text-left text-xl font-semibold mb-1">{{$pulaus->nama_menteri}}</p>
                        <p class="text-sm text-slate-700 text-justify">{!! $pulaus->kata_sambutan !!}</p>
                    </div>
                </div>
            </div>
            {{-- Description --}}
            <div class="text-md lg:text-xl text-justify py-5">
                <p>
                    {!! $pulaus->deskripsi !!}
                </p>
            </div>
            {{-- Table --}}
            <div class="flex justify-center py-10 border-y-2 border-slate-300">
                <table class="font-medium border border-black table-auto">
                    <tbody>
                        <tr>
                            <td class="p-3 border border-black">Ibukota</td>
                            <td class="p-3 border border-black">{{ $pulaus->ibukota }}</td>
                        </tr>
                        <tr>
                            <td class="p-3 border border-black">Bandara</td>
                            <td class="p-3 border border-black">{{ $pulaus->bandara }}</td>
                        </tr>
                        <tr>
                            <td class="p-3 border border-black">Pelabuhan Laut</td>
                            <td class="p-3 border border-black">{{ $pulaus->pelabuhan }}</td>
                        </tr>
                        <tr>
                            <td class="p-3 border border-black">Terminal Darat</td>
                            <td class="p-3 border border-black">{{ $pulaus->terminal }}</td>
                        </tr>
                        <tr>
                            <td class="p-3 border border-black">Transportasi ke sana</td>
                            <td class="p-3 border border-black">{{ $pulaus->transportasi }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            @if(count(json_decode($pulaus->galleryObjek)) > 0 && isset($pulaus->galleryObjek))
            <div class="relative h-full max-h-[400px] my-5 bg-slate-400">
                <img class="object-cover w-full h-full opacity-70"
                    src="{{ Storage::url('public/gallery/'.json_decode($pulaus->galleryObjek)[0]->image) }}" alt="">

                {{-- Lightbox --}}
                <div x-data="{ open: false }" @keydown.escape="open = false">
                    <button @click="open = true"
                        class="absolute bottom-0 right-0 px-5 py-2 text-base font-semibold text-black duration-300 -translate-x-5 -translate-y-10 bg-gray-100 border-2 border-gray-300 rounded-md hover:bg-gray-200 ">
                        Semua foto
                    </button>
                    <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                        style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                        <div
                            class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                            <button
                                class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                            </button>
                        </div>
                        <div class="h-full w-full flex items-center justify-center overflow-hidden"
                            x-data="{active: 0, slides: {{ ($pulaus->galleryObjek) }} }">
                            <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                    <button type="button"
                                        class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === 0 ? slides.length - 1 : active - 1">
                                        <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>

                            @foreach (json_decode($pulaus->galleryObjek) as $index=>$gallery)
                            <div class="h-full w-full flex items-center justify-center absolute">
                                <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                    x-show="active === {{ $index }}"
                                    x-transition:enter="transition ease-out duration-150"
                                    x-transition:enter-start="opacity-0 transform scale-90"
                                    x-transition:enter-end="opacity-100 transform scale-100"
                                    x-transition:leave="transition ease-in duration-150"
                                    x-transition:leave-start="opacity-100 transform scale-100"
                                    x-transition:leave-end="opacity-0 transform scale-90">

                                    <img src="{{ Storage::url('public/gallery/'.$gallery->image) }}"
                                        class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                </div>
                                <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                    x-show="active === {{ $index }}">
                                    <span class="w-12 text-right" x-text="{{ $index }} + 1"></span>
                                    <span class="w-4 text-center">/</span>
                                    <span class="w-12 text-left"
                                        x-text="{{ count(json_decode($pulaus->galleryObjek)) }}"></span>
                                </div>
                            </div>
                            @endforeach

                            <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-start w-12 md:ml-16">
                                    <button type="button"
                                        class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === slides.length - 1 ? 0 : active + 1">
                                        <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            {{-- Maps --}}
            <div class="my-5 w-full">
                <p class="my-5 text-3xl font-semibold">Maps</p>
                <div class="overflow-hidden relative maps-embed w-full">{{!! $pulaus->embed_maps !!}}</div>
            </div>
            @if(count($kabupaten) > 0)
            <div class="container mx-auto my-5">
                <p class="my-5 text-3xl font-semibold">Semua Kota/Kabupaten di <span class="capitalize">{{$pulaus->title}}</span></p>
                <div class="flex justify-start">
                    <div class="w-full hidden lg:grid gap-5 lg:grid-cols-4">
                        @foreach ($kabupaten as $index=>$item)
                        <x-destindonesia.card-destination-province index="{{ $index }}" lokasi="provinsi"
                            text="{{ $item->title }}" slug="{{ route('kabupatenShow', $item->slug) }}"
                            header="{{ $item->header }}">
                        </x-destindonesia.card-destination-province>
                        @endforeach
                    </div>
                    {{--  --}}
                    <div class="w-full hidden md:grid lg:hidden gap-5 md:grid-cols-3">
                        @foreach ($kabupaten as $index=>$item)
                        <x-destindonesia.card-destination-province index="{{ $index }}" lokasi="provinsi"
                            text="{{ $item->title }}" slug="{{ route('kabupatenShow', $item->slug) }}"
                            header="{{ $item->header }}">
                        </x-destindonesia.card-destination-province>
                        @if ($index == 2)
                        @break
                        @endif
                        @endforeach
                    </div>
                    <div class="w-full grid md:hidden gap-5 grid-cols-2 lg:overflow-x-auto">
                        @foreach ($kabupaten as $index=>$item)
                        <x-destindonesia.card-destination index="{{ $index }}" lokasi="provinsi"
                            text="{{ $item->title }}" slug="{{ route('kabupatenShow', $item->slug) }}"
                            header="{{ $item->header }}">
                        </x-destindonesia.card-destination>
                        @if ($index == 1)
                        @break
                        @endif
                        @endforeach
                    </div>
                </div>
                {{-- @dump($pulaus) --}}
                {{-- {{ route('destindonesia.prov', $jawa->id) }} --}}
                <div class="flex justify-center">
                    <div class="flex justify-center">
                        <a href="{{route('provinsiShow.all',$pulaus->slug)}}" class="p-5 grid justify-items-center">
                            <button type="submit" class="my-3 px-8 py-2 lg:text-xl md:text-base text-white duration-300 bg-[#23AEC1] rounded-lg hover:bg-kamtuu-primary hover:text-white">
                                Lihat Semua Kota/Kabupaten
                            </button>
                        </a>
                    </div>
                </div>
            </div>
            @endif
        </div>
        {{-- Ads Desktop --}}
        <div class="hidden sm:block">
            <p class="mx-5 text-3xl font-semibold my-3">Article (Ads)</p>
            <x-destindonesia.ads-list></x-destindonesia.ads-list>
        </div>
    </div>
    <div class="container mx-auto">
    
        @if(isset($reviews->star_rating))
        <div class="container">
            <div class="row">
                <div class="col mt-4">
                    <p class="font-weight-bold ">Review</p>
                    <div class="form-group row">
                        <input type="hidden" name="detailObjek_id" value="{{ $reviews->id }}">
                        <div class="col">
                            <div class="rated">
                                @for($i=1; $i<=$reviews->star_rating; $i++)
                                    <input type="radio" id="star{{$i}}" class="rate" name="rating" value="5" />
                                    <label class="star-rating-complete" title="text">{{$i}} stars</label>
                                    @endfor
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mt-4">
                        <div class="col">
                            <p>{{ $reviews->comments }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="container">
            <div class="row">
            </div>
        </div>
        <div>
            <div class="container max-w-6xl p-4 ">
    
                <div class="flex mb-4">
                    <div class="col mt-4 w-full">
                        <form class="py-2 px-4 w-full" action="{{route('reviewObjek.store')}}"
                            style="box-shadow: 0 0 10px 0 #ddd;" method="POST" autocomplete="off" id="review-province-object">
                            @csrf
                            <p class="font-weight-bold ">Ulasan</p>
                            <div class="form-group row">
                                <input type="hidden" name="detailObjek_id" value="{{ $pulaus->id }}">
                                <div class="col">
                                    <div class="rate">
                                        <input type="radio" id="star5" class="rate" name="rating" value="5" />
                                        <label for="star5" title="text">5 stars</label>
                                        <input type="radio" checked id="star4" class="rate" name="rating" value="4" />
                                        <label for="star4" title="text">4 stars</label>
                                        <input type="radio" id="star3" class="rate" name="rating" value="3" />
                                        <label for="star3" title="text">3 stars</label>
                                        <input type="radio" id="star2" class="rate" name="rating" value="2">
                                        <label for="star2" title="text">2 stars</label>
                                        <input type="radio" id="star1" class="rate" name="rating" value="1" />
                                        <label for="star1" title="text">1 star</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mt-4" x-data="imageUpload">
                                <div class="col">
                                    <textarea
                                        class=" form-control w-full flex-auto block p-4 font-medium border border-transparent rounded-lg outline-none focus:border-[#9E3D64] focus:text-green-500"
                                        name="comment" rows="6 " placeholder="Berikan Ulasan Anda"
                                        maxlength="200"></textarea>
                                    <input type="file" name="images[]" class="form-control w-full flex-auto block border border-transparent outline-none focus:border-[#9E3D64] focus:text-green-500" accept="image/*" @change="selectedFile" multiple>
                                    <template x-if="imgDetail.length >= 1">
                                        <div class="flex justify-start items-center mt-2">
                                            <template x-for="(detail,index) in imgDetail" :key="index">
                                                <div class="flex justify-center items-center">
                                                    <img :src="detail"
                                                        class="object-contain rounded border border-gray-300 w-[154px] h-[154px] mr-5"
                                                        :alt="'upload'+index">
                                                    <button type="button"
                                                        class="absolute mx-2 translate-x-12 -translate-y-14">
                                                        <img src="{{ asset('storage/icons/circle-trash-solid.svg') }}"
                                                            alt="" width="25px" @click="removeImage(index)">
                                                    </button>
                                                </div>
                                            </template>
                                        </div>
                                    </template>                                         
                                </div>
                            </div>
                            <div class="mt-3 text-right">
                                <button class="py-2 px-3 rounded-lg bg-kamtuu-second text-white">Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
    
                <div class="grid grid-cols-2 p-5">
                    <div class="flex">
                        <img src="{{ asset('storage/icons/Star 1.png') }}"
                            class="lg:w-[48px] lg:h-[48px] mt-[0.2rem] mr-1 inline-flex" alt="rating" title="Rating">
                        <p class="flex"><span class="lg:text-[50px] font-semibold ">{{$ratings}}.0</span> <span
                                class="lg:text-[20px] lg:pt-9"> /5.0 dari {{$var2}} ulasan</span></p>
                    </div>
                </div>
    
                {{-- ulasan desktop --}}
    
                @foreach($reviews as $review)
                <div class="hidden lg:block md:block">
                    <div class="grid grid-row-5 rounded shadow-lg justify-items justify-items-start">
                        <div class="grid grid:row-2 grid-cols-10 gap-2 pl-3 mt-7">
                            @if(isset($review->profile_photo_path))
                            <img class="w-20 h-20 rounded-full bg-gray-400" scr="{{isset($review->profile_photo_path) ? asset($review->profile_photo_path):null}}">
                            @else
                            <div class="w-20 h-20 rounded-full bg-gray-400 relative">
                                @php
                                    $name = $review->first_name;
                                    $exp_name = explode(' ',$name);
                                    $inisial ="";

                                    $inisial = substr($exp_name[0],0,1);

                                    if(count($exp_name) > 1){
                                        $inisial .=substr(end($exp_name),0,1);
                                    }
                                @endphp
                                @if(count($exp_name) > 1)
                                <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 13px;">{{$inisial}}</p>
                                @else
                                    <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 26px;">{{$inisial}}</p>
                                @endif
                            </div>
                            @endif
                            <div class="col-span-2 my-auto">
                            <p class="font-semibold text-gray-900">{{$review->first_name}}<p/>
                            <p class="font-light text-gray-600 text-xs">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}<p/>
                            </div>
                        </div>
                        <div class="grid grid-cols-5 pl-3 mb-2">
                            <div class="col-span-5">
                                <img class="w-[20px] h-[20] mt-[0.3rem] mr-1  ml-3 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                @for ($i = 0; $i < ($review->star_rating-1); $i++)
                                <img class="w-[20px] h-[20] mt-[0.3rem] mr-1 -ml-2 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                @endfor
                            </div>
                        </div>
                        <div class="mt-3 pl-3">
                            <button type="button" id="show-btn-ulasan" class="px-3 py-2 text-black text-sm underline">Tampilkan</button>
                            <button type="button" id="hide-btn-ulasan" class="px-3 py-2 text-black text-sm underline hidden">Sembunyikan</button>
                        </div>
                        @if($review->gallery_post)
                        <div class="flex justify-start justify-items-center pl-4 pb-4">
                            @php
                                $post_gallery = json_decode($review->gallery_post, true);
                            @endphp
                            <img class="w-50 h-40 bg-slate-400 rounded-lg" src="{{$post_gallery['result'] != null ? Storage::url('gallery_post/'.$post_gallery['result'][0]):'https://via.placeholder.com/540x540'}}">
                            <div class="relative" x-data="{open:false}" @keydown.escape="open = false">
                                <button @click="open=true" type="button" id="hide-btn-foto-ulasan" 
                                                            class="absolute px-3 py-2 text-black text-sm underline" style="width:169px;top:110px">
                                                            Tampilkan semua foto
                                </button>
                                <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                                    style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                                <div
                                    class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                                        <button
                                            class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                            style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                            <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                                        </button>
                                </div>
                                <div class="h-full w-full flex items-center justify-center overflow-hidden"
                                    x-data="{active: 0, slides: {{ ($review->gallery_post) }} }">
                                    <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                        <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                            <button type="button"
                                                class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                style="background-color: rgba(230, 230, 230, 0.4);"
                                                @click="active = active === 0 ? slides.length - 1 : active - 1">
                                                <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                            </button>
                                        </div>
                                    </div>
                                    @if ($post_gallery)
                                        @foreach ($post_gallery['result'] as $index=>$gallery)
                                            <div class="h-full w-full flex items-center justify-center absolute">
                                                <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                                    x-show="active === {{ $index }}"
                                                    x-transition:enter="transition ease-out duration-150"
                                                    x-transition:enter-start="opacity-0 transform scale-90"
                                                    x-transition:enter-end="opacity-100 transform scale-100"
                                                    x-transition:leave="transition ease-in duration-150"
                                                    x-transition:leave-start="opacity-100 transform scale-100"
                                                    x-transition:leave-end="opacity-0 transform scale-90">

                                                    <img src="{{ Storage::url('gallery_post/'.$gallery) }}"
                                                        class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                                </div>
                                                <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                                x-show="active === {{ $index }}">
                                                    <span class="w-12 text-right" x-text="{{ $index }} + 1"></span>
                                                    <span class="w-4 text-center">/</span>
                                                    <span class="w-12 text-left"
                                                        x-text="{{ count($post_gallery['result']) }}"></span>
                                                </div>
                                            </div>
                                        @endforeach
                                        @endif
                                                                        
                                        <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                            <div class="flex items-center justify-start w-12 md:ml-16">
                                                <button type="button"
                                                    class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                    style="background-color: rgba(230, 230, 230, 0.4);"
                                                    @click="active = active === slides.length - 1 ? 0 : active + 1">
                                                    <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                                </button>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>

                        </div>
                        @endif
                    </div>
                </div>

                {{-- ulasan mobile --}}
                <div class="block lg:hidden md:hidden">
                    <div
                        class="grid grid-cols-1 divide-y rounded-lg shadow-xl justify-items-center border border-gray-300">
                        <div class="grid p-5 justify-items-center">
                            <p class="text-sm md:text-[18px] font-bold my-3">{{$review->frist_name}}</p>
                            <p class="text-sm md:text-[18px] -mt-2">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}</p>
                            <div class="flex p-2">
                                <img src="{{ asset('storage/icons/Star 1.png') }}"
                                    class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex" alt="rating" title="Rating">
                                <p class="flex pt-3"><span
                                        class="text-[16px] font-semibold ">{{$review->star_rating}}.0</span>
                                    <span class="text-[16px]"> /5.0</span>
                                </p>
                            </div>
                        </div>

                        <div class="grid justify-start justify-items-center p-5">
                            <p class="text-sm md:text-[18px]">{{isset($review->comments) ? $review->comments :'Lorem ipsum dolor sit amet consectetur adipisicing
                                elit.
                                Vel placeat, tenetur dolores enim quae quod voluptates dolorem explicabo repellendus
                                ratione voluptatibus aliquam exercitationem hic adipisci sint quibusdam, eos, at
                                quas.'}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
    </div>
    {{-- Ads Mobile --}}
    <div class="block sm:hidden mt-10">
        <p class="text-xl font-semibold mb-3 text-center sm:text-left">Article (Ads)</p>
        <x-destindonesia.ads-list></x-destindonesia.ads-list>
    </div>
    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        var carouselProvSwiper = new Swiper(".carousel-prov-swiper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                enabled: true,
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            breakpoints: {
                768: {
                    slidesPerView: 3,
                    navigation: {
                        enabled: true,
                    }
                },
                1280: {
                    slidesPerView: 4,
                    navigation: {
                        enabled: false,
                    }
                },
            }
    });
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })

        function inginKesana(){
            $.ajax({
                type:'POST',
                url:"{{route('ingin-kesana-submit')}}",
                cache: false,
                dataType:"json",
                data:{
                    objek:'provinsi'
                },
                success:function(resp){
                    var json = resp;
                    if(resp.succses){
                        var count = json.data;
                        console.log(count)
                        $("#dropdown").hide();
                        $("#been-there").html(`${count} Orang`)
                    }
                },
                error:function(data){
                    console.log("error:",data)
                }
            })
        }

        function pernahKesana(){
            $.ajax({
                type:'POST',
                url:"{{route('pernah-kesana-submit')}}",
                cache: false,
                dataType:"json",
                data:{
                    objek:'provinsi'
                },
                success:function(resp){
                    var json = resp;
                    if(resp.succses){
                        var count = json.data;
                        console.log(count)
                        $("#dropdown").hide();
                        $("#pijakan").html(`${count} Traveler`)
                    }
                },
                error:function(data){
                    console.log("error:",data)
                }
            })
        }

        $("#dropdownDefaultButton").on("click",function(){
            $("#dropdown").toggle()
        })
        
        $("#ingin-kesana").on("click",function(){
            inginKesana();
        });
        
        $("#pernah-kesana").on("click",function(){
            pernahKesana()
        });
    </script>
    <script>
        $(document).ready(function(){
                
            function submitValidate(e, msg){
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })

                Toast.fire({
                    icon: 'error',
                    title: msg
                })
                
                e.preventDefault()
            }

            $('#review-province-object').on("submit",function(e){
                let role="{{auth()->user() != null ? auth()->user()->role:null}}";
                console.log(role)
                if(!role){
                    submitValidate(e, 'Silahkan login terlebih dahulu')
                }
                    
                else if(role!='traveller'){
                    submitValidate(e, 'Silahkan login sebagai traveller terlebih dahulu')
                }

                else if(!$("#comments").val()){
                    submitValidate(e, 'Isi komentar tidak kosong')
                }
                //e.preventDefault();
            })
        })

        function imageUpload(){
                return{
                    imageUrl:'',
                    imgDetail:[],
                    selectedFile(event){
                        this.fileToUrl(event)
                    },
                    fileToUrl(event){
                        if (!event.target.files.length) return
                        let file = event.target.files
                        
                        for (let i = 0; i < file.length; i++) {
                            let reader = new FileReader();
                            let srcImg = ''
                            this.imgDetail = []
                            reader.readAsDataURL(file[i]);
                            reader.onload = e => {
                                srcImg = e.target.result
                                this.imgDetail = [...this.imgDetail, srcImg]
                            };
                        }
                    },
                    removeImage(index){
                        this.imgDetail.splice(index, 1)
                    }
                }
        }
    </script>
{{-- review-province-object --}}
</body>

</html>