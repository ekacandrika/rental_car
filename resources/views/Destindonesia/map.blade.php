<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
    <style>
        .cardList {
            display: block;
        }

        .active {
            fill: rgba(255, 52, 52, .3);
        }

        #region path:hover {
            fill: rgba(255, 52, 52, .3);
        }

        .prev,
        .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            padding: 16px;
            margin-top: -22px;
            color: black;
            font-weight: bold;
            font-size: 18px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
        }

        .next {
            left: 40%;
            border-radius: 3px 0 0 3px;
        }

        .prev {
            left: 5%;
        }

        #next,
        #prev {
            color: #D50006;
            padding: 10px 20px 10px 20px;
            border: 1px;
            border-radius: 100px;
            border-style: solid;
            background-color: #ffffff;
        }

        #prev:hover {
            box-shadow: 0 2px 10px 0 rgba(0, 0, 0, 0.2);
        }

        #next:hover {
            box-shadow: 0 2px 10px 0 rgba(0, 0, 0, 0.2);
        }

        path {
            fill: #97DA8B
        }

        .fil2 {
            fill: #F25353
        }

        .fil1 {
            fill: white;
            -webkit-filter: drop-shadow(0 0 1px rgba(0, 0, 0, .3));
            filter: drop-shadow(0 0 1px rgba(0, 0, 0, .3));
        }
    </style>
</head>

<body class="bg-white font-Inter">
    <div class="my-5 mx-auto container map" id="map">
        <div class="container grid grid-cols-6">
            <div class="cardSlide col-start-3 col-span-1">
                <div data-cont="r-1" id="list-a" class="cardList">
                    <div
                        class="max-w-sm w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                        <a href="#">
                            <img class="rounded-t-lg" src="https://via.placeholder.com/300x180" alt="">
                            <div class="p-5">
                                <p class="text-[#FFB800] text-sm font-semibold">Explore</p>
                                <a href="#">
                                    <h5 class="mb-px text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                        Lombok</h5>
                                </a>
                                <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-sm">Here are the
                                    biggest enterprise technology acquisitions of 2021 so far, in reverse chronological
                                    order.</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div data-cont="r-2" id="list-b" class="cardList">
                    <div
                        class="max-w-sm w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                        <a href="#">
                            <img class="rounded-t-lg" src="https://via.placeholder.com/300x180" alt="">
                            <div class="p-5">
                                <p class="text-[#FFB800] text-sm font-semibold">Explore</p>
                                <a href="#">
                                    <h5 class="mb-px text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                        Yogyakarta</h5>
                                </a>
                                <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-sm">Here are the
                                    biggest enterprise technology acquisitions of 2021 so far, in reverse chronological
                                    order.</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div data-cont="r-3" id="list-c" class="cardList">
                    <div
                        class="max-w-sm w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                        <a href="#">
                            <img class="rounded-t-lg" src="https://via.placeholder.com/300x180" alt="">
                            <div class="p-5">
                                <p class="text-[#FFB800] text-sm font-semibold">Explore</p>
                                <a href="#">
                                    <h5 class="mb-px text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                        Kalimantan</h5>
                                </a>
                                <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-sm">Here are the
                                    biggest enterprise technology acquisitions of 2021 so far, in reverse chronological
                                    order.</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div data-cont="r-4" id="list-d" class="cardList">
                    <div
                        class="max-w-sm w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                        <a href="#">
                            <img class="rounded-t-lg" src="https://via.placeholder.com/300x180" alt="">
                            <div class="p-5">
                                <p class="text-[#FFB800] text-sm font-semibold">Explore</p>
                                <a href="#">
                                    <h5 class="mb-px text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                        Maluku</h5>
                                </a>
                                <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-sm">Here are the
                                    biggest enterprise technology acquisitions of 2021 so far, in reverse chronological
                                    order.</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div data-cont="r-5" id="list-e" class="cardList">
                    <div
                        class="max-w-sm w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                        <a href="#">
                            <img class="rounded-t-lg" src="https://via.placeholder.com/300x180" alt="">
                            <div class="p-5">
                                <p class="text-[#FFB800] text-sm font-semibold">Explore</p>
                                <a href="#">
                                    <h5 class="mb-px text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                        Sulawesi</h5>
                                </a>
                                <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-sm">Here are the
                                    biggest enterprise technology acquisitions of 2021 so far, in reverse chronological
                                    order.</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div data-cont="r-6" id="list-f" class="cardList">
                    <div
                        class="max-w-sm w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                        <a href="#">
                            <img class="rounded-t-lg" src="https://via.placeholder.com/300x180" alt="">
                            <div class="p-5">
                                <p class="text-[#FFB800] text-sm font-semibold">Explore</p>
                                <a href="#">
                                    <h5 class="mb-px text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                        Bali</h5>
                                </a>
                                <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-sm">Here are the
                                    biggest enterprise technology acquisitions of 2021 so far, in reverse chronological
                                    order.</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div data-cont="r-7" id="list-g" class="cardList">
                    <div
                        class="max-w-sm w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                        <a href="#">
                            <img class="rounded-t-lg" src="https://via.placeholder.com/300x180" alt="">
                            <div class="p-5">
                                <p class="text-[#FFB800] text-sm font-semibold">Explore</p>
                                <a href="#">
                                    <h5 class="mb-px text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                        Papua</h5>
                                </a>
                                <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-sm">Here are the
                                    biggest enterprise technology acquisitions of 2021 so far, in reverse chronological
                                    order.</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div id="buttons">
                    <a id="prev" class="prev" onclick="plusSlides(-1)">❮</a>
                    <a id="next" class="next" onclick="plusSlides(1)">❯</a>
                </div>
            </div>
        </div>
        <x-destindonesia.svg-map></x-destindonesia.svg-map>
    </div>

    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>

    <script>
        let slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            let i;
            let slides = document.getElementsByClassName("cardList");
            if (n > slides.length) {
                slideIndex = 1
            }
            if (n < 1) {
                slideIndex = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }

            let spans = $(".span");
            spans.click(function() {
                spans.removeClass("active");
                for (i = 0; i < spans.length; i++) {
                    this.classList.add("active");
                }
            });

            slides[slideIndex - 1].style.display = "block";
        }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

</body>

</html>
