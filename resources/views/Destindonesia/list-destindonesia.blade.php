<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
        {{-- Destindonesia NavBar --}}
        <div
            class="bg-transparent lg:container mx-auto px-2 h-[75px] lg:h-[100px] border-y-[1px] border-[#F2F2F2] hidden sm:block relative z-10">
            <div class="grid grid-cols-6 h-full">
                <a href="#"
                    class="text-lg md:text-xl lg:text-3xl text-[#9E3D64] font-extrabold lg:font-semibold my-auto">DestIndonesia</a>
                <a href="#"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-[#333333]">Destinasi</a>
                <a href="#"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-[#333333]">Newsletter</a>
                <a href="#"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-[#333333]">Event</a>
                <a href="#"
                    class="text-sm lg:text-base hover:underline flex justify-center my-auto text-[#333333]">Videos</a>
                <div class="flex justify-center my-auto bg-white rounded-full border">
                    <input
                        class="placeholder:italic placeholder:text-slate-400 block bg-white w-full border border-transparent rounded-full py-1 lg:py-2 pl-5 pr-3 shadow-sm focus:outline-none focus:border-transparent focus:ring-transparent focus:ring-1 sm:text-sm"
                        placeholder="Search" type="text" name="search" />
                    <button class="mx-3"><img src="{{ asset('storage/icons/search-ic.png') }}"
                            alt=""></button>
                </div>
            </div>
        </div>
    </header>
    <nav class="flex ml-9 mt-2" aria-label="Breadcrumb">
        <ol class="inline-flex items-center space-x-1 md:space-x-3 text-base lg:text-lg">
            <li class="inline-flex items-center">
                <a href="{{route('destindonesia.index')}}"
                    class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] capitalize">Destindonesia</a>
            </li>
            <li>
                <div class="flex items-center">
                    <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                            clip-rule="evenodd"></path>
                    </svg>
                    <a href="#"
                        class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1] capitalize">List</a>
                </div>
            </li>
        </ol>
    </nav>
    {{-- Hero --}}
    <div class="relative">
        {{-- Kabupaten/Kota Name --}}
        <div class="mt-8">
            <p class="text-lg lg:text-6xl text-center text-[#333333] font-bold">Indonesia</p>
            <p class="text-lg lg:text-xl text-center text-[#9E3D64] font-bold">Temukan Destinasimu</p>
        </div>
    </div>

    {{-- Terbaru dan Ads Desktop --}}
    <div class="container mx-auto grid grid-cols-4 gap-5 my-5">
        {{-- SVG Maps --}}
        <div class="lg:col-span-3 col-span-4 lg:mx-px mx-2">
            <div class="my-5 mx-auto container map" id="map">
                <div class="container grid grid-cols-3">
                    <div class="cardSlide col-start-4 col-span-1">
                        {{-- Sumatra --}}
                        <div id="list-a" class="cardList">
                            @foreach ($sumatraSVG as $ssvg => $ss)
                                <div
                                    class="max-w-sm w-[150px] lg:w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                                    <a href="{{ route('wisataShow', $ss->slug) }}">
                                        <img class="rounded-t-lg w-[300px] h-[180px] object-cover object-center"
                                            src="{{ isset($ss->thumbnail) ? Storage::url($ss->thumbnail) : 'https://via.placeholder.com/300x180' }}"
                                            alt="">
                                        <div class="p-2 lg:p-5">
                                            <p class="text-[#FFB800] text-[10px] lg:text-sm font-semibold">Explore</p>
                                            <a href="{{ route('wisataShow', $ss->slug) }}">
                                                <h5
                                                    class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                                    {{ $ss->namaWisata }}</h5>
                                            </a>
                                            <p
                                                class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                                {!! $ss->deskripsi !!}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        {{-- Jawa --}}
                        <div id="list-b" class="cardList">

                            @foreach ($jawaSVG as $jsvg => $js)
                                <div
                                    class="max-w-sm w-[150px] lg:w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                                    <a href="{{ route('wisataShow', $js->slug) }}">
                                        <img class="rounded-t-lg w-[300px] h-[180px] object-cover object-center"
                                            src="{{ isset($js->thumbnail) ? Storage::url($js->thumbnail) : 'https://via.placeholder.com/300x180' }}"
                                            alt="">
                                        <div class="p-2 lg:p-5">
                                            <p class="text-[#FFB800] text-[10px] lg:text-sm font-semibold">Explore</p>
                                            <a href="{{ route('wisataShow', $js->slug) }}">
                                                <h5
                                                    class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                                    {{ $js->namaWisata }}</h5>
                                            </a>
                                            <p
                                                class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                                {!! $js->deskripsi !!}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        {{-- Maluku --}}
                        <div id="list-c" class="cardList">
                            @foreach ($malukuSVG as $msvg => $ms)
                                <div
                                    class="max-w-sm w-[150px] lg:w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                                    <a href="{{ route('wisataShow', $ms->slug) }}">
                                        <img class="rounded-t-lg w-[300px] h-[180px] object-cover object-center"
                                            src="{{ isset($ms->thumbnail) ? Storage::url($ms->thumbnail) : 'https://via.placeholder.com/300x180' }}"
                                            alt="">
                                        <div class="p-2 lg:p-5">
                                            <p class="text-[#FFB800] text-[10px] lg:text-sm font-semibold">Explore</p>
                                            <a href="{{ route('wisataShow', $ms->slug) }}">
                                                <h5
                                                    class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                                    {{ $ms->namaWisata }}</h5>
                                            </a>
                                            <p
                                                class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                                {!! $ms->namaWisata !!}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        {{-- Kalimantan --}}
                        <div id="list-d" class="cardList">
                            @foreach ($kalimantanSVG as $ksvg => $ks)
                                <div
                                    class="max-w-sm w-[150px] lg:w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                                    <a href="{{ route('wisataShow', $ks->slug) }}">
                                        <img class="rounded-t-lg w-[300px] h-[180px] object-cover object-center"
                                            src="{{ isset($ks->thumbnail) ? Storage::url($ks->thumbnail) : 'https://via.placeholder.com/300x180' }}"
                                            alt="">
                                        <div class="p-2 lg:p-5">
                                            <p class="text-[#FFB800] text-[10px] lg:text-sm font-semibold">Explore</p>
                                            <a href="{{ route('wisataShow', $ks->slug) }}">
                                                <h5
                                                    class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                                    {{ $ks->namaWisata }}</h5>
                                            </a>
                                            <p
                                                class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                                {!! $ks->deskripsi !!}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        {{-- Sulawesi --}}
                        <div id="list-e" class="cardList">
                            @foreach ($sulawesiSVG as $slsvg => $sls)
                                <div
                                    class="max-w-sm w-[150px] lg:w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                                    <a href="{{ route('wisataShow', $sls->slug) }}">
                                        <img class="rounded-t-lg w-[300px] h-[180px] object-cover object-center"
                                            src="{{ isset($sls->thumbnail) ? Storage::url($sls->thumbnail) : 'https://via.placeholder.com/300x180' }}"
                                            alt="">
                                        <div class="p-2 lg:p-5">
                                            <p class="text-[#FFB800] text-[10px] lg:text-sm font-semibold">Explore</p>
                                            <a href="{{ route('wisataShow', $sls->slug) }}">
                                                <h5
                                                    class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                                    {{ $sls->namaWisata }}</h5>
                                            </a>
                                            <p
                                                class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                                {!! $sls->deskripsi !!}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        {{-- Nusa --}}
                        <div id="list-f" class="cardList">
                            @foreach ($nusaSVG as $nsvg => $ns)
                                <div
                                    class="max-w-sm w-[150px] lg:w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                                    <a href="{{ route('wisataShow', $ns->slug) }}">
                                        <img class="rounded-t-lg w-[300px] h-[180px] object-cover object-center"
                                            src="{{ isset($ns->thumbnail) ? Storage::url($ns->thumbnail) : 'https://via.placeholder.com/300x180' }}"
                                            alt="">
                                        <div class="p-2 lg:p-5">
                                            <p class="text-[#FFB800] text-[10px] lg:text-sm font-semibold">Explore</p>
                                            <a href="{{ route('wisataShow', $ns->slug) }}">
                                                <h5
                                                    class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                                    {{ $ns->namaWisata }}</h5>
                                            </a>
                                            <p
                                                class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                                {!! $ns->deskripsi !!}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        {{-- Papua --}}
                        <div id="list-g" class="cardList">
                            @foreach ($papuaSVG as $psvg => $ps)
                                <div
                                    class="max-w-sm w-[150px] lg:w-[300px] bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                                    <a href="{{ route('wisataShow', $ps->slug) }}">
                                        <img class="rounded-t-lg w-[300px] h-[180px] object-cover object-center"
                                            src="{{ isset($ps->thumbnail) ? Storage::url($ps->thumbnail) : 'https://via.placeholder.com/300x180' }}"
                                            alt="">
                                        <div class="p-2 lg:p-5">
                                            <p class="text-[#FFB800] text-[10px] lg:text-sm font-semibold">Explore</p>
                                            <a href="{{ route('wisataShow', $ps->slug) }}">
                                                <h5
                                                    class="mb-px text-sm lg:text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                                    {{ $ps->namaWisata }}</h5>
                                            </a>
                                            <p
                                                class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                                {!! $ps->deskripsi !!}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        {{-- <div id="buttons">
                            <a id="prev" class="prev" onclick="plusSlides(-1)">❮</a>
                            <a id="next" class="next" onclick="plusSlides(1)">❯</a>
                        </div> --}}
                    </div>
                </div>
                <x-destindonesia.svg-map></x-destindonesia.svg-map>
            </div>
        </div>

        {{-- Ads Desktop --}}
        <div class="hidden sm:block">
            <p class="mx-5 text-3xl font-semibold my-3">Article (Ads)</p>
            <div class="w-[306px] shadow-xl divide-y rounded-lg p-5 mx-5 my-5">
                @foreach ($news as $iklan => $item)
                    {{-- {{ dd($item) }} --}}
                    <div class="grid grid-cols-2">
                        <div class="p-3">
                            <img src="{{ isset($item->thumbnail) ? Storage::url($item->thumbnail) : 'https://via.placeholder.com/109x109' }}"
                                alt="tailwind logo" class="rounded-xl object-cover object-center" />
                        </div>

                        <a href="{{ route('newsletter.show', $item->slug) }}" class="p-3 ">
                            <h2 class="font-bold">{{ $item->title }}</h2>
                            {{-- <div class="truncate">
                                {!! $item->description !!}
                            </div> --}}
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-pan-4 lg:col-span-3 mx-2">
            <div class="m-2">
                <p class="text-lg lg:text-2xl text-center text-[#333333] font-bold">Top Destinasi di Indonesia</p>
                <p class="text-md lg:text-lg text-center text-[#BDBDBD] font-semibold">By Kamtuu</p>
            </div>
            <div class="grid lg:grid-cols-3 lg:grid-rows-none grid-rows-3 gap-2 lg:gap-5 lg:my-5 my-0 lg:mt-5 mt-5">
                @foreach ($wisata as $objek => $item)
                <div class="hover:shadow-lg hover:-translate-y-2">
                    <a class="lg:block hidden" href="{{ route('wisataShow', $item->slug) }}">
                        <div class="px-4 absolute h-20 items-center justify-center flex">
                            <p
                                class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white shadow-md font-bold text-[#D50006]">
                                {{ $objek + 1 }}</p>
                        </div>
                        <div
                            class="max-w-sm w-[150px] lg:w-full bg-white rounded-lg border border-gray-200 shadow ">
                            <img class="rounded-t-lg object-center object-cover w-[363px] h-[180px]"
                                src="{{ isset($item->header) ? Storage::url($item->header) : 'https://via.placeholder.com/363x180' }}"
                                alt="">
                            <div class="p-2 lg:p-4">
                                <h5
                                    class="mb-px text-sm lg:text-xl font-bold tracking-tight text-[#333333] hover:text-[#9E3D64]">
                                    {{ $item->namaWisata }}</h5>
                                {{-- <div
                                    class="truncate mb-3 font-normal text-gray-700 dark:text-gray-400 text-[8px] lg:text-sm">
                                    {!! $item->deskripsi !!}
                                </div> --}}
                            </div>
                        </div>
                    </a>
                </div>
                @if($objek==5)
                @break
                @endif
                @endforeach
            </div>
        </div>
    </div>

    {{-- Maps List --}}
<div class="col-span-4 lg:col-span-3">
    <div class="grid grid-cols-4 lg:mb-5 mb-2">
        <img class="p-5 lg:block hidden" src="{{ asset('storage/img/jawa.png') }}" alt="list-jawa">
        <div class="lg:col-start-2 lg:col-span-3 col-start-1 col-span-4">
            <div class="lg:h-16 h-8 border-b lg:mx-px mx-2 ">
                <p class="lg:text-xl text-md w-full font-bold text-[#333333] py-0 lg:py-2">Pulau Jawa</p>
            </div>
            @forelse($pulauJawa as $jawa)
                <div class="grid grid-cols-10 my-2">
                    <div class="flex col-start-1 col-span-3 lg:col-span-2 items-center justify-end mr-4">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                    </div>
                    <a href="{{ route('destindonesia.prov', $jawa->id) }}"
                        class="flex items-center col-start-4 lg:col-start-3 col-span-5">
                        <p class="text-[#333333] hover:text-[#9E3D64] lg:text-lg text-[12px] font-semibold">
                            {{ $jawa->name }}
                        </p>
                    </a>
                </div>
            @empty
            @endforelse
        </div>
    </div>

    <div class="grid grid-cols-4 lg:mb-5 mb-2">
        <img class="p-5 lg:block hidden" src="{{ asset('storage/img/kalimantan.png') }}"
            alt="list-kalimantan">
        <div class="lg:col-start-2 lg:col-span-3 col-start-1 col-span-4">
            <div class="lg:h-16 h-8 border-b lg:mx-px mx-2 ">
                <p class="lg:text-xl text-md w-full font-bold text-[#333333] py-0 lg:py-2">Pulau Sumatra</p>
            </div>
            @forelse($pulauSumatra as $sumatra)
                <div class="grid grid-cols-10 my-2">
                    <div class="flex col-start-1 col-span-3 lg:col-span-2 items-center justify-end mr-4">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                    </div>
                    <a href="{{ route('destindonesia.prov', $sumatra->id) }}"
                        class="flex items-center col-start-4 lg:col-start-3 col-span-5">
                        <p class="text-[#333333] hover:text-[#9E3D64] lg:text-lg text-[12px] font-semibold">
                            {{ $sumatra->name }}
                        </p>
                    </a>
                </div>
            @empty
            @endforelse
        </div>
    </div>

    <div class="grid grid-cols-4 lg:mb-5 mb-2">
        <img class="p-5 lg:block hidden" src="{{ asset('storage/img/sumatra.png') }}" alt="list-sumatra">
        <div class="lg:col-start-2 lg:col-span-3 col-start-1 col-span-4">
            <div class="lg:h-16 h-8 border-b lg:mx-px mx-2 ">
                <p class="lg:text-xl text-md w-full font-bold text-[#333333] py-0 lg:py-2">Pulau Kalimantan</p>
            </div>
            @forelse($pulauKalimantan as $kalimantan)
                <div class="grid grid-cols-10 my-2">
                    <div class="flex col-start-1 col-span-3 lg:col-span-2 items-center justify-end mr-4">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                    </div>
                    <a href="{{ route('destindonesia.prov', $kalimantan->id) }}"
                        class="flex items-center col-start-4 lg:col-start-3 col-span-5">
                        <p class="text-[#333333] hover:text-[#9E3D64] lg:text-lg text-[12px] font-semibold">
                            {{ $kalimantan->name }}
                        </p>
                    </a>
                </div>
            @empty
            @endforelse
        </div>
    </div>

    <div class="grid grid-cols-4 lg:mb-5 mb-2">
        <img class="p-5 lg:block hidden" src="{{ asset('storage/img/nusa.png') }}" alt="list-nusa">
        <div class="lg:col-start-2 lg:col-span-3 col-start-1 col-span-4">
            <div class="lg:h-16 h-8 border-b lg:mx-px mx-2 ">
                <p class="lg:text-xl text-md w-full font-bold text-[#333333] py-0 lg:py-2">Pulau Bali dan
                    Kepulauan Nusa</p>
            </div>

            @forelse($kepulauan as $pulau)
                <div class="grid grid-cols-10 my-2">
                    <div class="flex col-start-1 col-span-3 lg:col-span-2 items-center justify-end mr-4">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                    </div>
                    <a href="{{ route('destindonesia.prov', $pulau->id) }}"
                        class="flex items-center col-start-4 lg:col-start-3 col-span-5">
                        <p class="text-[#333333] hover:text-[#9E3D64] lg:text-lg text-[12px] font-semibold">
                            {{ $pulau->name }}
                        </p>
                    </a>
                </div>
            @empty
            @endforelse
        </div>
    </div>

    <div class="grid grid-cols-4 lg:mb-5 mb-2">
        <img class="p-5 lg:block hidden" src="{{ asset('storage/img/maluku.png') }}" alt="list-maluku">
        <div class="lg:col-start-2 lg:col-span-3 col-start-1 col-span-4">
            <div class="lg:h-16 h-8 border-b lg:mx-px mx-2 ">
                <p class="lg:text-xl text-md w-full font-bold text-[#333333] py-0 lg:py-2">Pulau Maluku</p>
            </div>
            @forelse($maluku as $malukus)
                <div class="grid grid-cols-10 my-2">
                    <div class="flex col-start-1 col-span-3 lg:col-span-2 items-center justify-end mr-4">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                    </div>
                    <a href="{{ route('destindonesia.prov', $malukus->id) }}"
                        class="flex items-center col-start-4 lg:col-start-3 col-span-5">
                        <p class="text-[#333333] hover:text-[#9E3D64] lg:text-lg text-[12px] font-semibold">
                            {{ $malukus->name }}
                        </p>
                    </a>
                </div>
            @empty
            @endforelse
        </div>
    </div>
    <div class="grid grid-cols-4 lg:mb-5 mb-2">
        <img class="p-5 lg:block hidden" src="{{ asset('storage/img/sulawesi.png') }}" alt="list-sulawesi">
        <div class="lg:col-start-2 lg:col-span-3 col-start-1 col-span-4">
            <div class="lg:h-16 h-8 border-b lg:mx-px mx-2 ">
                <p class="lg:text-xl text-md w-full font-bold text-[#333333] py-0 lg:py-2">Pulau Sulawesi</p>
            </div>

            @forelse($pulauSulawesi as $sulawesi)
                <div class="grid grid-cols-10 my-2">
                    <div class="flex col-start-1 col-span-3 lg:col-span-2 items-center justify-end mr-4">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                    </div>
                    <a href="{{ route('destindonesia.prov', $sulawesi->id) }}"
                        class="flex items-center col-start-4 lg:col-start-3 col-span-5">
                        <p class="text-[#333333] hover:text-[#9E3D64] lg:text-lg text-[12px] font-semibold">
                            {{ $sulawesi->name }}
                        </p>
                    </a>
                </div>
            @empty
            @endforelse
        </div>
    </div>
    <div class="grid grid-cols-4 lg:mb-5 mb-2">
        <img class="p-5 lg:block hidden" src="{{ asset('storage/img/papua.png') }}" alt="list-papua">
        <div class="lg:col-start-2 lg:col-span-3 col-start-1 col-span-4">
            <div class="lg:h-16 h-8 border-b lg:mx-px mx-2 ">
                <p class="lg:text-xl text-md w-full font-bold text-[#333333] py-0 lg:py-2">Pulau Papua</p>
            </div>
            @forelse($pulauPapua as $papua)
                <div class="grid grid-cols-10 my-2">
                    <div class="flex col-start-1 col-span-3 lg:col-span-2 items-center justify-end mr-4">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                        <img src="{{ asset('storage/icons/start-purple.svg') }}"
                            class="mx-px top-0 lg:w-[20px] lg:h-[20px] w-[12px] h-[12px]" alt="start">
                    </div>
                    <a href="{{ route('destindonesia.prov', $papua->id) }}"
                        class="flex items-center col-start-4 lg:col-start-3 col-span-5">
                        <p class="text-[#333333] hover:text-[#9E3D64] lg:text-lg text-[12px] font-semibold">
                            {{ $papua->name }}
                        </p>
                    </a>
                </div>
            @empty
            @endforelse
        </div>
    </div>
</div>

    {{-- Ads Mobile --}}
    <div class="block sm:hidden mt-10">
        <p class="text-xl font-semibold mb-3 text-center sm:text-left">Article (Ads)</p>
        <x-destindonesia.ads-list></x-destindonesia.ads-list>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>

    <script>
        let slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            let i;
            let slides = document.getElementsByClassName("cardList");
            if (n > slides.length) {
                slideIndex = 1
            }
            if (n < 1) {
                slideIndex = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }

            let spans = $(".span");
            spans.click(function() {
                spans.removeClass("active");

                this.classList.add("active");
            });

            slides[slideIndex - 1].style.display = "block";
        }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body>
