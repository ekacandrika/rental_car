<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kota Kabupaten') }}</title>

    {{-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> --}}

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <style>
        .rate {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rate:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rate:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rate:not(:checked)>label:before {
            content: '★ ';
        }

        .rate>input:checked~label {
            color: #ffc700;
        }

        .rate:not(:checked)>label:hover,
        .rate:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rate>input:checked+label:hover,
        .rate>input:checked+label:hover~label,
        .rate>input:checked~label:hover,
        .rate>input:checked~label:hover~label,
        .rate>label:hover~input:checked~label {
            color: #c59b08;
        }

        .star-rating-complete {
            color: #c59b08;
        }

        .rating-container .form-control:hover,
        .rating-container .form-control:focus {
            background: #fff;
            border: 1px solid #ced4da;
        }

        .rating-container textarea:focus,
        .rating-container input:focus {
            color: #000;
        }

        .rated {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rated:not(:checked)>input {
            position: absolute;
            display: none;
        }

        .rated:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ffc700;
        }

        .rated:not(:checked)>label:before {
            content: '★ ';
        }

        .rated>input:checked~label {
            color: #ffc700;
        }

        .rated:not(:checked)>label:hover,
        .rated:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rated>input:checked+label:hover,
        .rated>input:checked+label:hover~label,
        .rated>input:checked~label:hover,
        .rated>input:checked~label:hover~label,
        .rated>label:hover~input:checked~label {
            color: #c59b08;
        }

        .maps-embed {
            padding-bottom: 60%
        }

        .maps-embed iframe {
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
            position: absolute;
        }
        @media only screen and (max-width: 600px) {
            iframe.note-video-clip{
                width: 320px;
            }
        }
    </style>

    <!-- Styles -->
    @livewireStyles
</head>

<body class="bg-white font-Inter">
    <header>
        <x-destindonesia.menu-bar></x-destindonesia.menu-bar>
    </header>

    {{-- Hero --}}
    <div class="relative bg-slate-400">
        <img class="w-full opacity-70 object-cover" src="{{Storage::url($pulaus->header)}}" alt="">

        {{-- Kabupaten/Kota Name --}}
        <div class="absolute translate-y-full sm:translate-y-1/2 translate-x-1/2 bottom-1/2 right-1/2">
            <p class="text-lg sm:text-2xl lg:text-6xl text-center drop-shadow-2xl text-white font-bold">
                {{$pulaus->namaWisata}}
            </p>
        </div>
    </div>

    <div class="container mx-auto px-5 grid sm:grid-cols-3 xl:grid-cols-4 gap-10 my-5 h-auto min-h-full">
        {{-- Detail --}}
        <div class="sm:col-span-2 xl:col-span-3 flex flex-col">
            <div class="mx-auto">
                <x-destindonesia.status-list ratings="{{ $ratings }}" var2="{{ $var2 }}" want="{{$want}}" been="{{$been}}"></x-destindonesia.status-list>
            </div>
            <nav class="flex mt-2" aria-label="Breadcrumb">
                <ol class="inline-flex items-center space-x-1 md:space-x-3 text-base lg:text-lg">
                    <li class="inline-flex items-center">
                        <a href="{{isset($pulau_slug) ? route('pulauShow',$pulau_slug) : '#'}}"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] capitalize">{{$pulau_nama}}</a>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            <a href="{{isset($provinsi_slug) ? route('provinsiShow',$provinsi_slug) : '#'}}"
                                class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] capitalize">{{$provinsi_nama}}</a>
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                                </svg>
                                <a href="{{isset($kota_slug) ? route('kabupatenShow',$kota_slug) : '#'}}"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#333333] hover:text-[#23AEC1] capitalize">{{$kota_nama}}</a>
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                            <svg class="w-4 h-4 text-[#333333]" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd"></path>
                                </svg>
                                <a href="#"
                            class="inline-flex items-center text-sm font-semibold font-inter text-[#23AEC1] hover:text-[#23AEC1] capitalize">{{$pulaus->namaWisata}}</a>
                        </div>
                    </li>
                    {{-- 
                    <li>
                        <div class="flex items-center">
                          
                        </div>
                    </li>
                    <li>
                        <div class="flex items-center">
                           
                        </div>
                    </li> 
                    --}}
                </ol>
            </nav>
            <div class="grid grid-rows-2 sm:grid-rows-1 sm:grid-cols-2">
                {{-- Share FB/WA --}}
                <div class="text-base lg:text-lg flex items-center">
                    <p>Share</p>
                    <img src="{{ asset('storage/icons/square-facebook.svg') }}" class="mx-3" width="23px" height="23px"
                        alt="wa-icon">
                    <img src="{{ asset('storage/icons/square-whatsapp.svg') }}" width="23px" height="23px"
                        alt="fb-icon">
                </div>

                {{-- Button --}}
                <div class="flex sm:justify-end">
                    <x-destindonesia.button-dropdown></x-destindonesia.button-dropdown>
                    {{-- <x-destindonesia.button-secondary text="Ingin Kesana">@slot('button') Ingin ke Sana @endslot
                    </x-destindonesia.button-secondary> --}}
                    <x-destindonesia.button-primary text="Pesan Sekarang" icon="icons/foundation_foot_white.svg">
                        @slot('button') Pesan Sekarang @endslot
                    </x-destindonesia.button-primary>
                </div>
            </div>

            {{-- Description --}}
            <div class="text-md lg:text-xl text-justify py-5">
                <p>{!! $pulaus->deskripsi !!}</p>
            </div>

            {{-- Table --}}
            <div class="flex justify-center py-10 border-y-2 border-slate-300">
                <table class="table-auto border border-black font-medium">
                    <tbody>
                        <tr>
                            <td class="border border-black p-3">Kategori</td>
                            <td class="border border-black p-3">{{$pulaus->kategori}}</td>
                        </tr>
                        <tr>
                            <td class="border border-black p-3">Hari Buka</td>
                            <td class="border border-black p-3">{{$pulaus->hari}}</td>
                        </tr>
                        <tr>
                            <td class="border border-black p-3">Harga Tiket</td>
                            <td class="border border-black p-3">{{$pulaus->hargaTiket}}</td>
                        </tr>
                        <tr>
                            <td class="border border-black p-3">Jam Operasional</td>
                            <td class="border border-black p-3">{{$pulaus->jamOperasional}}</td>
                        </tr>
                        <tr>
                            <td class="border border-black p-3">Transportasi ke sana</td>
                            <td class="border border-black p-3">{{$pulaus->transportasi}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            {{-- Gallery --}}
            {{-- @dump(url('storage/gallery/'.json_decode($pulaus->galleryWisata)[0]->image)) --}}
            @if (count(json_decode($pulaus->galleryWisata)) > 0 && isset($pulaus->galleryWisata))
            <div class="relative h-full max-h-[400px] my-5 bg-slate-400">
                <img class="object-cover w-full h-full opacity-70"
                    src="{{ url('storage/gallery/'.json_decode($pulaus->galleryWisata)[0]->image) }}" alt="">

                {{-- Lightbox --}}
                <div x-data="{ open: false }" @keydown.escape="open = false">
                    <button @click="open = true"
                        class="absolute bottom-0 right-0 px-5 py-2 text-base font-semibold text-black duration-300 -translate-x-5 -translate-y-10 bg-gray-100 border-2 border-gray-300 rounded-md hover:bg-gray-200 ">
                        Semua foto
                    </button>
                    <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                        style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                        <div
                            class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                            <button
                                class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                            </button>
                        </div>
                        <div class="h-full w-full flex items-center justify-center overflow-hidden"
                            x-data="{active: 0, slides: {{ ($pulaus->galleryWisata  ) }} }">
                            <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                    <button type="button"
                                        class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === 0 ? slides.length - 1 : active - 1">
                                        <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>

                            @foreach (json_decode($pulaus->galleryWisata ) as $index=>$gallery)
                            <div class="h-full w-full flex items-center justify-center absolute">
                                <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                    x-show="active === {{ $index }}"
                                    x-transition:enter="transition ease-out duration-150"
                                    x-transition:enter-start="opacity-0 transform scale-90"
                                    x-transition:enter-end="opacity-100 transform scale-100"
                                    x-transition:leave="transition ease-in duration-150"
                                    x-transition:leave-start="opacity-100 transform scale-100"
                                    x-transition:leave-end="opacity-0 transform scale-90">

                                    <img src="{{ url('storage/gallery/'.$gallery->image) }}"
                                        class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                </div>
                                <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                    x-show="active === {{ $index }}">
                                    <span class="w-12 text-right" x-text="{{ $index }} + 1"></span>
                                    <span class="w-4 text-center">/</span>
                                    <span class="w-12 text-left"
                                        x-text="{{ count(json_decode($pulaus->galleryWisata )) }}"></span>
                                </div>
                            </div>
                            @endforeach

                            <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                <div class="flex items-center justify-start w-12 md:ml-16">
                                    <button type="button"
                                        class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                        style="background-color: rgba(230, 230, 230, 0.4);"
                                        @click="active = active === slides.length - 1 ? 0 : active + 1">
                                        <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            {{-- Maps --}}
            <div class="my-5">
                <p class="font-semibold my-5 text-3xl">Maps</p>
                <div class="overflow-hidden relative maps-embed">{!!$pulaus->embed_maps!!}</div>
            </div>

            {{-- Kamtuu Transfer Search Bar --}}
            <div class="text-center sm:text-left my-10">
                <p class="font-semibold my-5 text-2xl xl:text-3xl">Cari Transfer Menuju Destinasi</p>
                {{-- <x-destindonesia.search></x-destindonesia.search> --}}
                <form action="{{ route('transfer.search') }}" method="POST">
                    @csrf
                    <div x-show="tab === 2"
                        class="z-0 grid grid-cols-1 px-6 py-8 -mt-px border border-gray-400 divide-y rounded-md rounded-tl-none lg:grid-cols-7 md:grid-cols-1 bg-kamtuu-second justify-items-center divide-kamtuu-second lg:divide-x">
                        <div class="grid w-full p-2 bg-white lg:rounded-l-lg justify-items-left lg:justify-items-left">
                            {{-- <div> --}}
                                {{-- <img src="{{ asset('storage/icons/icon-maps.svg') }}"
                                    class="w-[15px] h-[15px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu"> --}}
                                {{-- </div> --}}
                            <div>
                                <div class="flex-inline">
                                    <select name="search" id="maps" required
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                        <option value="">Dari</option>
                                        @foreach ($regencyTur as $foo)
                                        <option style="font-size: 10px" value="{{ $foo->id }}">{{ $foo->name }}</option>
                                        @endforeach
                                    </select>
                                    {{-- <select name="" id="maps"
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                        <option value="">Dari</option>
                                        <option value="">1</option>
                                        <option value="">1</option>
                                    </select> --}}
                                </div>
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-left lg:justify-items-left">
                            <div>
                                <div class="flex-inline">
                                    <select name="to" id="maps" required
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                        <option value="">Ke</option>
                                        @foreach ($regencyTur as $foo)
                                        <option style="font-size: 10px" value="{{ $foo->id }}">{{ $foo->name }}</option>
                                        @endforeach
                                    </select>
                                    {{-- <select name="" id="point"
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                        <option value="">Ke</option>
                                        <option value="">1</option>
                                        <option value="">1</option>
                                    </select> --}}
                                </div>
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-center grid-cols-[80%_20%]">
                            <div class="place-self-center">
                                <input class="!-ml-[1rem] md:!ml-[6rem] lg:!ml-[2rem] lg:!w-[9rem] md:!w-[31rem]"
                                    id="datepickerTransfer" placeholder="Tanggal" name="tgl" required />
                            </div>
                            <div class="md:hidden lg:hidden">
                                <img src="{{ asset('storage/icons/icon-kalender.svg') }}"
                                    class="w-[15px] h-[17px] mt-[0.2rem] mr-1" alt="polygon 3" title="Kamtuu">
                            </div>
                        </div>

                        <div class="grid w-full p-2 bg-white justify-items-start grid-cols-[70%_30%]">
                            <div class="place-self-center">
                                <input
                                    class="!ml-[17px] lg:ml-0 md:!w-[31rem] md:!ml-[9rem]  lg:!ml-[2rem] lg:!w-[9rem] "
                                    id="timepickerTransfer" placeholder="Waktu Ambil" name="waktu_ambil" required />
                            </div>
                        </div>

                        {{-- <div class="grid w-full p-2 bg-white justify-items-left">
                            <div>
                                <div class="flex-inline">
                                    <select name="" id=""
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                        <option value="">Jumlah Orang</option>
                                        <option value="">1</option>
                                        <option value="">1</option>
                                    </select>
                                </div>
                            </div>
                        </div> --}}

                        <div class="grid w-full p-2 bg-white justify-items-left">
                            <div>
                                <div class="ml-1 flex-inline">
                                    <select name="metode_transfer" id="" required
                                        class="w-full border-transparent focus:border-transparent focus:ring-0 md:text-base lg:text-sm pr-7">
                                        <option value="">Metode Transfer</option>
                                        <option value="Transfer Private">Transfer Private</option>
                                        <option value="Transfer Umum">Transfer Umum</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div
                            class="flex justify-center w-full p-2 text-white md:rounded-r-lg lg:rounded-r-lg bg-kamtuu-primary">
                            <button>
                                <div>
                                    <div class="flex-inline">
                                        <h1 class="uppercase">cari</h1>
                                    </div>
                                </div>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        {{-- Ads Desktop --}}
        <div class="hidden sm:block">
            <p class="mx-5 text-3xl font-semibold my-3">Article (Ads)</p>
            <div class="w-[306px] shadow-xl divide-y rounded-lg p-5 mx-5 my-5">
                @foreach ($news as $iklan => $item)
                {{-- {{ dd($item) }} --}}
                <div class="grid grid-cols-2">
                    <div class="p-3">
                        <img src="{{ isset($item->thumbnail) ? Storage::url($item->thumbnail) : 'https://via.placeholder.com/109x109' }}"
                            alt="tailwind logo" class="rounded-xl object-cover object-center" />
                    </div>

                    <a href="{{ route('newsletter.show', $item->slug) }}" class="p-3 ">
                        <h2 class="font-bold">{{ $item->title }}</h2>
                        <div class="truncate">
                            {!! $item->description !!}
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    {{-- Destinasi and Tabs --}}
    <div class="container px-3 mx-auto" x-data="{ active:'Tur' }">
        {{-- Tabs --}}
        <div class="flex mx-5 sm:block sm:mx-0">
            <ul
                class="flex justify-start overflow-x-auto text-sm font-medium text-center text-gray-500 dark:text-gray-400">
                <li class="mx-2 mt-2">
                    <button x-on:click="active = 'Tur'" aria-current="page"
                        class="inline-block py-2 px-9 rounded-lg active"
                        :class="active == 'Tur' ? 'text-white bg-[#23AEC1]' : 'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]' ">Tour</button>
                </li>
                <li class="mx-2 mt-2">
                    <button x-on:click="active = 'Transfer'" class="inline-block py-2 px-9 rounded-lg active"
                        :class="active == 'Transfer' ? 'text-white bg-[#23AEC1]' : 'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]' ">Transfer</button>
                </li>
                <li class="mx-2 mt-2">
                    <button x-on:click="active = 'Rental'" class="inline-block py-2 px-9 rounded-lg active"
                        :class="active == 'Rental' ? 'text-white bg-[#23AEC1]' : 'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]' ">Rental</button>
                </li>
                <li class="mx-2 mt-2">
                    <button x-on:click="active = 'Hotel'" class="inline-block py-2 px-9 rounded-lg active"
                        :class="active == 'Hotel' ? 'text-white bg-[#23AEC1]' : 'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]' ">Hotel</button>
                </li>
                <li class="mx-2 mt-2">
                    <button x-on:click="active = 'Activity'" class="inline-block py-2 px-9 rounded-lg active"
                        :class="active == 'Activity' ? 'text-white bg-[#23AEC1]' : 'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]' ">Activity</button>
                </li>
                <li class="mx-2 mt-2">
                    <button x-on:click="active = 'Xstay'" class="inline-block py-2 px-9 rounded-lg active"
                        :class="active == 'Xstay' ? 'text-white bg-[#23AEC1]' : 'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]' ">Xstay</button>
                </li>
                <li class="mx-2 mt-2">
                    <button x-on:click="active = 'Wisata'" class="inline-block py-2 px-9 rounded-lg active"
                        :class="active == 'Wisata' ? 'text-white bg-[#23AEC1]' : 'text-[#23AEC1] bg-slate-100 hover:text-white hover:bg-[#23AEC1]' ">Wisata</button>
                </li>
            </ul>
        </div>

        <div x-show="active === 'Tur'" class="my-5 swiper carousel-wisata-swiper">
            <div class="py-5 mx-auto swiper-wrapper">
                @if (json_decode($tur) != null)
                @foreach (json_decode($tur) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-destindonesia.card-produk :act="$act" :type="'tur'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Product tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination -mb-2"></div>
        </div>
        <div x-show="active === 'Transfer'" class="my-5 swiper carousel-wisata-swiper">
            <div class="py-5 mx-auto swiper-wrapper">
                @if (json_decode($transfer) != null)
                @foreach (json_decode($transfer) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-destindonesia.card-produk :act="$act" :type="'transfer'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Product tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination -mb-2"></div>
        </div>
        <div x-show="active === 'Rental'" class="my-5 swiper carousel-wisata-swiper">
            <div class="py-5 mx-auto swiper-wrapper">
                @if (json_decode($rental) != null)
                @foreach (json_decode($rental) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-destindonesia.card-produk :act="$act" :type="'rental'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Product tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination -mb-2"></div>
        </div>
        <div x-show="active === 'Hotel'" class="my-5 swiper carousel-wisata-swiper">
            <div class="py-5 mx-auto swiper-wrapper">
                @if (json_decode($hotel) != null)
                @foreach (json_decode($hotel) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-destindonesia.card-produk :act="$act" :type="'hotel'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Product tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination -mb-2"></div>
        </div>
        <div x-show="active === 'Xstay'" class="my-5 swiper carousel-wisata-swiper">
            <div class="py-5 mx-auto swiper-wrapper">
                @if (json_decode($xstay) != null)
                @foreach (json_decode($xstay) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-destindonesia.card-produk :act="$act" :type="'xstay'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Product tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination -mb-2"></div>
        </div>
        <div x-show="active === 'Activity'" class="my-5 swiper carousel-wisata-swiper">
            <div class="py-5 mx-auto swiper-wrapper">
                @if (json_decode($activity) != null)
                @foreach (json_decode($activity) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-destindonesia.card-produk :act="$act" :type="'activity'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Product tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination -mb-2"></div>
        </div>
        {{-- <div x-show="active === 'Wisata'" class="my-5 swiper carousel-wisata-swiper">
            <div class="py-5 mx-auto swiper-wrapper">
                @if (json_decode($wisata) != null)
                @foreach (json_decode($wisata) as $act)
                <div class="flex justify-center swiper-slide">
                    <x-destindonesia.card-produk :act="$act" :type="'wisata'" />
                </div>
                @endforeach
                @else
                <div class="text-4xl font-semibold text-center text-[#9E3D64]">
                    Product tidak ditemukan
                </div>
                @endif
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination -mb-2"></div>
        </div> --}}
        {{-- <div class="flex justify-start">
            <div class="hidden grid-cols-3 md:grid lg:grid-cols-4 xl:grid-cols-5">
                <x-destindonesia.card-produk></x-destindonesia.card-produk>
                <x-destindonesia.card-produk></x-destindonesia.card-produk>
                <x-destindonesia.card-produk></x-destindonesia.card-produk>
                <x-destindonesia.card-produk></x-destindonesia.card-produk>
                <x-destindonesia.card-produk></x-destindonesia.card-produk>
            </div>
            <div class="flex justify-center w-full md:hidden">
                <x-destindonesia.card-produk></x-destindonesia.card-produk>
            </div>
        </div> --}}
        {{-- <div class="flex justify-center">
            <x-destindonesia.button-primary text="Lihat Semua"></x-destindonesia.button-primary>
        </div> --}}
    </div>


    {{-- Ulasan --}}
    <div class="container mx-auto">
        @if(isset($reviews->star_rating))
        <div class="container">
            <div class="row">
                <div class="col mt-4">
                    <p class="font-weight-bold ">Review</p>
                    <div class="form-group row">
                        <input type="hidden" name="detailObjek_id" value="{{ $reviews->id }}">
                        <div class="col">
                            <div class="rated">
                                @for($i=1; $i<=$reviews->star_rating; $i++)
                                    <input type="radio" id="star{{$i}}" class="rate" name="rating" value="5" />
                                    <label class="star-rating-complete" title="text">{{$i}} stars</label>
                                    @endfor
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mt-4">
                        <div class="col">
                            <p>{{ $reviews->comments }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="container">
            <div class="row">
            </div>
        </div>
        <div>
            <div class="container max-w-6xl p-4 ">

                <div class="flex mb-4">
                    <div class="col mt-4 w-full">
                        <form class="py-2 px-4 w-full" action="{{route('reviewObjek.store')}}"
                            style="box-shadow: 0 0 10px 0 #ddd;" method="POST" autocomplete="off">
                            @csrf
                            <p class="font-weight-bold ">Ulasan</p>
                            <div class="form-group row">
                                <input type="hidden" name="detailObjek_id" value="{{ $pulaus->id }}">
                                <div class="col">
                                    <div class="rate">
                                        <input type="radio" id="star5" class="rate" name="rating" value="5" />
                                        <label for="star5" title="text">5 stars</label>
                                        <input type="radio" checked id="star4" class="rate" name="rating" value="4" />
                                        <label for="star4" title="text">4 stars</label>
                                        <input type="radio" id="star3" class="rate" name="rating" value="3" />
                                        <label for="star3" title="text">3 stars</label>
                                        <input type="radio" id="star2" class="rate" name="rating" value="2">
                                        <label for="star2" title="text">2 stars</label>
                                        <input type="radio" id="star1" class="rate" name="rating" value="1" />
                                        <label for="star1" title="text">1 star</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mt-4">
                                <div class="col">
                                    <textarea
                                        class=" form-control w-full flex-auto block p-4 font-medium border border-transparent rounded-lg outline-none focus:border-[#9E3D64] focus:text-green-500"
                                        name="comment" rows="6 " placeholder="Berikan Ulasan Anda"
                                        maxlength="200"></textarea>
                                </div>
                            </div>
                            <div class="mt-3 text-right">
                                <button class="py-2 px-3 rounded-lg bg-kamtuu-second text-white">Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="grid grid-cols-2 p-5">
                    <div class="flex">
                        <img src="{{ asset('storage/icons/Star 1.png') }}"
                            class="lg:w-[48px] lg:h-[48px] mt-[0.2rem] mr-1 inline-flex" alt="rating" title="Rating">
                        <p class="flex"><span class="lg:text-[50px] font-semibold ">{{$ratings}}.0</span> <span
                                class="lg:text-[20px] lg:pt-9"> /5.0 dari {{$var2}} ulasan</span></p>
                    </div>
                </div>

                                    {{-- ulasan desktop --}}
                                    @foreach($reviews as $review)
                                    <div class="hidden lg:block md:block">
                                        {{-- <x-produk.card-reviews :review="$review"></x-produk.card-reviews> --}}
                                        <div class="grid grid-row-5 rounded-lg shadow-lg justify-items-start mb-4">
                                            {{-- <div class="grid grid-row-5 rounded-lg shadow-lg justify-items-start py-5"> --}}
                                                <div class="grid grid:row-2 grid-cols-10 gap-2 pl-3 mt-7">
                                                    @if(isset($review->profile_photo_path))
                                                    <img class="w-20 h-20 rounded-full bg-gray-400" scr="{{isset($review->profile_photo_path) ? asset($review->profile_photo_path):null}}">
                                                    @else
                                                    <div class="w-20 h-20 rounded-full bg-gray-400 relative">
                                                        @php
                                                            $name = $review->first_name;
                                                            $exp_name = explode(' ',$name);
                                                            $inisial ="";

                                                            $inisial = substr($exp_name[0],0,1);

                                                            if(count($exp_name) > 1){
                                                                $inisial .=substr(end($exp_name),0,1);
                                                            }
                                                        @endphp
                                                        @if(count($exp_name) > 1)
                                                        <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 13px;">{{$inisial}}</p>
                                                        @else
                                                        <p class="absolute mx-auto font-bold" style="font-size: 41px;top: 11px;left: 26px;">{{$inisial}}</p>
                                                        @endif
                                                    </div>
                                                    @endif
                                                    <div class="col-span-2 my-auto">
                                                        <p class="font-semibold text-gray-900">{{$review->first_name}}<p/>
                                                        {{-- Carbon::parse($p->created_at)->diffForHumans(); --}}
                                                        <p class="font-light text-gray-600 text-xs">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}<p/>
                                                    </div>
                                                </div>
                                                <div class="grid grid-cols-5 pl-3 mb-2">
                                                    <div class="col-span-5">
                                                        <img class="w-[20px] h-[20] mt-[0.3rem] mr-1  ml-3 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                                        @for ($i = 0; $i < ($review->star_rating-1); $i++)
                                                            <img class="w-[20px] h-[20] mt-[0.3rem] mr-1 -ml-2 inline-flex bg-transparent" src="{{asset('/icons/Star 1.png')}}" alt="rating" title="Rating"/>
                                                        @endfor
                                                    </div>
                                                </div>
                                                <div class="grid grid-cols-5 space-y-5 text-justify pl-3 pr-5">
                                                    <div class="col-span-5 pl-3">
                                                    <p id="text-ulasan">{{$review->comments}}</p>
                                                    </div>
                                                </div>
                                                <div class="mt-3 pl-3">
                                                    <button type="button" id="show-btn-ulasan" class="px-3 py-2 text-black text-sm underline">Tampilkan</button>
                                                    <button type="button" id="hide-btn-ulasan" class="px-3 py-2 text-black text-sm underline hidden">Sembunyikan</button>
                                                </div>
                                                @if($review->gallery_post)
                                                <div class="flex justify-start justify-items-center pl-4 pb-4">
                                                    @php
                                                        $post_gallery = json_decode($review->gallery_post, true);
                                                        //dump($post_gallery);
                                                    @endphp
                                                    <img class="w-50 h-40 bg-slate-400 rounded-lg" src="{{$post_gallery['result'] != null ? Storage::url('gallery_post/'.$post_gallery['result'][0]):'https://via.placeholder.com/540x540'}}">
                                                    <div class="relative" x-data="{open:false}" @keydown.escape="open = false">
                                                        <button @click="open=true" type="button" id="hide-btn-foto-ulasan" 
                                                            class="absolute px-3 py-2 text-black text-sm underline" style="width:169px;top:110px">
                                                            Tampilkan semua foto
                                                        </button>
                                                        <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-[9999]"
                                                                    style="background-color: rgba(0,0,0,.7);" x-show.transition="open">
                                                                    <div
                                                                        class="flex items-center justify-start w-12 m-2 ml-6 mb-4 md:m-2 z-[100] absolute right-1 top-1 transform">
                                                                        <button
                                                                            class="text-white w-12 h-12 rounded-full flex items-center justify-center focus:outline-none"
                                                                            style="background-color: rgba(230,230,230,.4);" @click="open = false">
                                                                            <img src="{{ asset('storage/icons/close-button.svg') }}" class="w-6 h-6">
                                                                        </button>
                                                                    </div>
                                                                    <div class="h-full w-full flex items-center justify-center overflow-hidden"
                                                                        x-data="{active: 0, slides: {{ ($review->gallery_post) }} }">
                                                                        <div class="fixed left-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                                                            <div class="flex items-center justify-end w-12 mr-3 md:mr-16">
                                                                                <button type="button"
                                                                                    class="w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                                                    style="background-color: rgba(230, 230, 230, 0.4);"
                                                                                    @click="active = active === 0 ? slides.length - 1 : active - 1">
                                                                                    <img src="{{ asset('storage/icons/arrow-left-solid.svg') }}" class="w-6 h-6">
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                        @if ($post_gallery)
                                                                            @foreach ($post_gallery['result'] as $index=>$gallery)
                                                                                <div class="h-full w-full flex items-center justify-center absolute">
                                                                                    <div class="absolute top-0 bottom-0 py-2 md:py-24 px-2 flex flex-col items-center justify-center"
                                                                                        x-show="active === {{ $index }}"
                                                                                        x-transition:enter="transition ease-out duration-150"
                                                                                        x-transition:enter-start="opacity-0 transform scale-90"
                                                                                        x-transition:enter-end="opacity-100 transform scale-100"
                                                                                        x-transition:leave="transition ease-in duration-150"
                                                                                        x-transition:leave-start="opacity-100 transform scale-100"
                                                                                        x-transition:leave-end="opacity-0 transform scale-90">

                                                                                        <img src="{{ Storage::url('gallery_post/'.$gallery) }}"
                                                                                            class="object-contain max-w-full max-h-full rounded shadow-lg" />
                                                                                    </div>
                                                                                    <div class="fixed text-white text-sm font-bold bottom-0 transform w-40 h-12 mb-2 hidden md:flex justify-center items-center"
                                                                                        x-show="active === {{ $index }}">
                                                                                        <span class="w-12 text-right" x-text="{{ $index }} + 1"></span>
                                                                                        <span class="w-4 text-center">/</span>
                                                                                        <span class="w-12 text-left"
                                                                                            x-text="{{ count($post_gallery['result']) }}"></span>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        @endif
                                                                        
                                                                        <div class="fixed right-1 z-30 mb-4 md:mb-2 transform flex justify-center">
                                                                            <div class="flex items-center justify-start w-12 md:ml-16">
                                                                                <button type="button"
                                                                                    class="text-white font-bold w-12 h-12 rounded-full focus:outline-none flex items-center justify-center"
                                                                                    style="background-color: rgba(230, 230, 230, 0.4);"
                                                                                    @click="active = active === slides.length - 1 ? 0 : active + 1">
                                                                                    <img src="{{ asset('storage/icons/arrow-right-solid 1.svg') }}" class="w-6 h-6">
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                    </div>
                                                </div>
                                                @endif
                                            {{-- </div> --}}
                                            {{-- <div class="grid p-5 justify-items-center">
                                                <p class="text-[18px] font-bold">Jhonny Wilson</p>
                                                <p class="text-[18px] -mt-2">{{$review->created_at->format('j F, Y')}}
                                                </p>
                                                <div class="flex p-2">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex pt-3"><span
                                                            class="text-[16px] font-semibold ">{{$review->star_rating}}.0</span>
                                                        <span class="text-[16px]"> /5.0</span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div
                                                class="grid justify-start justify-items-center lg:-ml-[15rem] xl:-ml-[23rem] p-5 md:-ml-[10rem]">
                                                <p class="text-[18px]">{{ $review->comments }}</p>
                                            </div> --}}
                                        </div>
                                    </div>

                                    {{-- ulasan mobile --}}
                                    <div class="block lg:hidden md:hidden">
                                        <div
                                            class="grid grid-cols-1 divide-y rounded-lg shadow-xl justify-items-center border border-gray-300">
                                            <div class="grid p-5 justify-items-center">
                                                <p class="text-sm md:text-[18px] font-bold my-3">{{$review->frist_name}}</p>
                                                <p class="text-sm md:text-[18px] -mt-2">{{\Carbon\carbon::parse($review->created_at)->toFormattedDateString()}}</p>
                                                <div class="flex p-2">
                                                    <img src="{{ asset('storage/icons/Star 1.png') }}"
                                                        class="w-[30px] h-[30px] mt-[0.2rem] mr-1 inline-flex"
                                                        alt="rating" title="Rating">
                                                    <p class="flex pt-3"><span
                                                            class="text-[16px] font-semibold ">{{$review->star_rating}}</span>
                                                        <span class="text-[16px]"> /5.0</span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="grid justify-start justify-items-center p-5">
                                                <p class="text-sm md:text-[18px]"> {{ $review->comments }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
            </div>
        </div>
        @endif
    </div>

    {{-- Ads Mobile --}}
    <div class="block sm:hidden mt-10">
        <p class="text-xl font-semibold mb-3 text-center sm:text-left">Article (Ads)</p>
        <div class="w-[306px] shadow-xl divide-y rounded-lg p-5 mx-5 my-5">
            @foreach ($news as $iklan => $item)
            {{-- {{ dd($item) }} --}}
            <div class="grid grid-cols-2">
                <div class="p-3">
                    <img src="{{ isset($item->thumbnail) ? Storage::url($item->thumbnail) : 'https://via.placeholder.com/109x109' }}"
                        alt="tailwind logo" class="rounded-xl object-cover object-center" />
                </div>

                <a href="{{ route('newsletter.show', $item->slug) }}" class="p-3 ">
                    <h2 class="font-bold">{{ $item->title }}</h2>
                    <div class="truncate">
                        {!! $item->description !!}
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>

    <footer>
        <x-destindonesia.footer></x-destindonesia.footer>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script>
        var carouselWisataSwiper = new Swiper(".carousel-wisata-swiper", {
            loop: true,
            slidesPerView: 1,
            navigation: {
                enabled: true,
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            breakpoints: {
                768: {
                    slidesPerView: 3,
                    navigation: {
                        enabled: true,
                    }
                },
                1280: {
                    slidesPerView: 5,
                    navigation: {
                        enabled: false,
                    }
                },
            }
    });
    </script>
    <script>
        $('#datepickerTransfer').datepicker();
    </script>

    <script>
        $('#timepickerTransfer').timepicker();
    </script>
    <script>

        function inginKesana(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })

            $.ajax({
                type:'POST',
                url:"{{route('ingin-kesana-submit')}}",
                cache: false,
                dataType:"json",
                data:{
                    objek:'wisata'
                },
                success:function(resp){
                    var json = resp;
                    if(resp.succses){
                        var count = json.data;
                        console.log(count)
                        $("#dropdown").hide();
                        $("#been-there").html(`${count} Orang`)
                    }
                },
                error:function(data){
                    console.log("error:",data)
                }
            })
        }

        function pernahKesana(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })

            $.ajax({
                type:'POST',
                url:"{{route('pernah-kesana-submit')}}",
                cache: false,
                dataType:"json",
                data:{
                    objek:'wisata'
                },
                success:function(resp){
                    var json = resp;
                    if(resp.succses){
                        var count = json.data;
                        console.log(count)
                        $("#dropdown").hide();
                        $("#pijakan").html(`${count} Traveler`)
                    }
                },
                error:function(data){
                    console.log("error:",data)
                }
            })
        }

        $("#dropdownDefaultButton").on("click",function(){
            $("#dropdown").toggle()
        })
        
        $("#ingin-kesana").on("click",function(){
            inginKesana();
        });
        
        $("#pernah-kesana").on("click",function(){
            pernahKesana()
        });
</script>
</body>

</html>